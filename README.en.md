# JUnit Test Skeleton of The Project Sync10

## 1. Test Code 

### 1.1 Create own JUnit source package

You will create a sub-package under the package com.cwc.test.
For example, Zhanjie creates his test-package as `com.cwc.text.zj`.

### 1.2 Copy JUnit test skeleton code

You may copy files from the package `com.cwc.test.demo`, then  customize these files to fit your tests. These files are:

- BaseTestCase.java

- TestSuite.java
 
- applicationContext.xml

- DemoTestCase1.java

- DemoTestCase2.java

**Note that: you should modify the files under your test package, but not change those in com.cwc.test.demo !!!**

### 1.3. Simple Use

1. Edit the file `BaseTestCase.java`，find the following line：

		@ContextConfiguration("classpath:/com/cwc/test/demo/applicationContext.xml")
		
	Change the word `demo` to your the name of your sub-package，eg:
	
		@ContextConfiguration("classpath:/com/cwc/test/zj/applicationContext.xml")
		

2. Edit the file `applicationContext.xml`，add your bean description that will be tested. For example, if you need test the class  `com.cwc.app.floor.api.zj.FloorLocationMgrZJ`, then add following lines:

		<bean id="floorLocationMgrZJ" class="com.cwc.app.floor.api.zj.FloorLocationMgrZJ" depends-on="dbUtilAutoTran">
  			<property name="dbUtilAutoTran" ref="dbUtilAutoTran"/>
  		</bean>
  		
  	The dependencies description, please reference the files under `WebRoot/WEB-INF/conf`
  
  
3. Edit `DemoTestCase1.java` or `DemoTestCase2.java`, add the properties that will be injected by Spring Environment, eg:

		@Resource
		private FloorLocationMgrZJ floorLocationMgrZJ;
		
4. Edit `DemoTestCase1.java` or `DemoTestCase2.java`, add declarations of your unit-test methods. For example, if you will test the business method `FloorLocationMgrZJ.getLocationAreaByPsid`, then you can add following defination:

		@Test
		public void test_getLocationAreaByPsid() throws Exception{
			DBRow[] rows = floorLocationMgrZJ.getLocationAreaByPsid(100000);
			assertTrue(rows != null && rows.length>0);
			//... Other logic for check the return value
		}
		
5. If needed, you can add more TestCase classes. like `DemoTestCase1` and `DemoTestCase2`, your new TestCase class must extend the class `BaseTestCase` from your test package. For example, you add a TestCase class `MyTestCase`:

		public class MyTestCase extends BaseTestCase {
			@Resource
			private FloorLocationMgrZJ floorLocationMgrZJ;
			
			@Test
			public void test_getLocationAreaByPsid() throws Exception{
				DBRow[] rows = floorLocationMgrZJ.getLocationAreaByPsid(100000);
				assertTrue(rows != null && rows.length>0);
				//... Other logic for check the return value
			}
		}

	And, edit the class `TestSuite` from your sub-package, find the following：

		@SuiteClasses({ DemoTestCase1.class,DemoTestCase2.class })
		
	then change this line to：

		@SuiteClasses({ DemoTestCase1.class,DemoTestCase2.class,MyTestCase.class })



6. Customize configuations: By default, the skeleton code uses the properties from `com/cwc/test/env.properties`:
	
		db.host=localhost     		#the host/IP of the business database
		db.name=1you_zhanjie  		#the name of the database
		db.username=root      		#the user name of the database
		db.password=root	  		#the password of the database
		logserv.host=192.168.1.12	#the log server
		neo4j.router=192.168.1.53	#the router address of the GraphDB
		
If you need customize these configurations, **PLEASE DON'T CHANGE THIS FILE**!!! But you can do that according the following steps:
	
- Under your test package, eg. `com.cwc.test.zj`, create file `env.properties`
	
- Add properties that will override the defauls to this file, eg. if you need override the `db.name`  to `1you`, then add a line like `db.name=1you`
	
- Edit the file `applicationContext.xml` under your sub-package, change the fragment for `propertyConfigurer`, and add the reference to your property file.
	
			<bean id="propertyConfigurer"
	   			class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
	   		<property name="systemPropertiesModeName" value="SYSTEM_PROPERTIES_MODE_OVERRIDE" />
	    	<property name="locations">
		        <list>
		            <value>classpath:/com/cwc/test/env.properties</value>
		            <value>classpath:/com/cwc/test/zj/env.properties</value>
		        </list>
	    	</property>
			</bean>
		
So, the properties in your own `env.properties` will be used to override the defaults.

For example, Zhanjie would edit his `applicationContext.xml`, and add declarations：

		……
		<bean id="productStoreMgr" class="com.cwc.app.floor.api.FloorProductStoreMgr">
			<property name="dbUtilAutoTran" ref="dbUtilAutoTran" />
			<property name="restBaseUrl" value="http://${neo4j.router}" />
		</bean>
		……

In Above code, the value of property `neo4j.router` will be the customized value from `env.properties` under his package：

		neo4j.router=192.168.1.53
 


### 1.4 Execute Tests


#### Execute Single TestCase

In your Eclipse IDE, right-click the TestCase class under your test package in Package Explorer Window (eg, `DemoTestCase1` or `DemoTestCase2`), then select `Run As -> JUnit Test`

#### Execute a series of tests (TestSuite)

In your Eclipse IDE, right-click the `TestSuite` class under your test package in Package Explorer Window, the select `Run As -> JUnit Test`.


### 1.5 The basic of Spring+JUnit 4


#### 1.5.1 The TestCase class

TestCase class use Java Annotation to define test methods. A Test method is a instance mehtod that annotated by `@Test`, eg:

	import org.junit.Test;
	......
	@Test public void myTestCase() { ... }
	

In test method, you can call the method （eg，a business POJO）and check the return value. JUnit framework provide a lot of assertion utilities:

	@Test public void myTestCase(){
		ProductStoreLogBean psLog = new ProductStoreLogBean();
		psLog.setOid(random(1000000, 100000));
		...
		String returnId = logMgr.availableProductStoreLog(psLog, false);
		assertTrue(returnId != null && returnId.length() > 0);
	}
	

#### 1.5.2 Annotations

Normally, you can check the return value by your code or ignore the return value. If you expect that a test method should throw a kind of Exception, you can declare like this:

	@Test(expected = SQLException.class)
	public void myDatabaseErrorTestCase(){ ... }
	

Test methods specified by what is the longest time should be allowed to run, if the Test running time than the specified number of milliseconds, junit think Test fails. 

 	@Test(timeout=1000) 
	 public void selfXMLReader(){ 
			……
	 }

If you want to ignore some methods at that time, you can add an `Ignore`:

	@Ignore 
	@Test 
	public void myTestCase() { ... }
	

#### 1.5.3 Fixture

What is the Fixture ？ It means the resources or data that will be used in test methods.

1. Using org,junit.Before for doing the initial operations before every test  
2. Using org.junit.After for doing the finalized operations after every test

eg:

	//initial resources or data before every test method execution 
 	@Before public void init(){ …… } 

 	//release resources or data after every test method execution
 	@After public void destroy(){ …… }

 
If you need do initialization before/after all test methods, then use:   

1. org,junit.BeforeClass 
2. org.junit.AfterClass  

Note that: you must use these annotations on static methods, eg：

 	@BeforeClass public static void dbInit(){ …… } 
	
	@AfterClass public static void dbClose(){ …… }


#### 1.5.4 Test Suite

In actual project, with the development of the project schedule, the unit test class will be more and more, but until now we still can only run a separate a test class, it is certainly not feasible in the actual project practice. In order to solve this problem, JUnit provides a batch run test class method, called a test suite. In this way, every time need to prove the validity of system functions, only perform one or more the test suite. The test suite of writing is very simple, you only need to follow the following rules:

1. Create a TestSuite class
2. Add annotation `org.junit.runner.RunWith` and `org.junit.runners.Suite.SuiteClasses`
3. Pass org.junit.runners.Suite as argument to the annotation RunWith
4. Pass TestCase classes as arguments to SuiteClasses
 
eg:

 	import org.junit.runner.RunWith; 
 	import org.junit.runners.Suite; 
	……

 
 	@RunWith(Suite.class) 
 	@Suite.SuiteClasses({TestCase1.class,TestCase2.class,TestCase3.class}) 
 	public class RunAllUtilTestsSuite { 
 	}
 	

#### 1.5.5 The integration of JUnit4 and Spring 

JUnit test method is performed by test runner. The JUnit provides a default test runner, but JUnit allows other runners. The Spring framework  takes advantage of this mechanism, to provide its test runner, so as to make JUnit to test the Spring IOC container management of business objects.


	@RunWith(SpringJUnit4ClassRunner.class)
	@ContextConfiguration("classpath:/com/cwc/test/demo/applicationContext.xml")
	public class DemoTestCase1 extends BaseTestCase {
	
		@Resource	//automatically resolve this bean reference
		private FloorAdminMgr floorAdminMgr;  
		
		@Test
		public void test_1() throws Exception{
			long adid = 100025;
			String account = "hanlong";
			DBRow row = floorAdminMgr.getDetailAdmin(adid);
			assertEquals(row.getString("account"),account);
		}
	}
	
By optimizing this：

1. We defined a TestCase as a base class, with `@RunWith` and `@ContextConfiguration` annotated.
2. All other TestCases extend this base class, so they need not add `@RunWith` and `@ContextConfiguration` annotation.


