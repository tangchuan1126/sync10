<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../include.jsp"%>
<!-- �˴��޸�ΪΪ��ʹ��art������������Ƕ��һ��iframe  -->
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<TITLE><%=systemConfig.getStringConfigValue("webtitle")%> &#8482; </TITLE>
<style type="text/css">
	* { padding:0; margin:0; }
	html, body { height:100%; border:none 0; }
	#iframe { width:100%; height:100%; border:none 0; }
</style>
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/window/jquery.window.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="js/window/jquery.window.css" />
<link rel="stylesheet" type="text/css" href="js/sticky/sticky.full.css" />
<script src="js/window/jquery.window.js" type="text/javascript"></script>

<script type="text/javascript" src="js/sticky/sticky.full.js"></script>
<script type="text/javascript">
 
//dwr.engine._errorHandler = function(message, ex) {dwr.engine._debug("Error: " + ex.name + ", " + ex.message, true);};
//ҳ�����ѹ���.ϵͳһ��½��Ӧ��ȥ��ѯ��Ŀǰû����ɵ�����
//����������Ϊ���˴�����������ô����dwr���͵�����ǰ̨��ҳ��.��ʾ����
var windowSchedule ;
var windowBill ;
var windowPictureShow ;
function openWindowAtBottom(){
    //����ǸĴ����Ѿ�����ʾ����ô�ٵ����ʱ��Ӧ����ˢ�������ҳ��
    //�����͵Ĵ���Ҫ�Լ����������ŵ�λ��
    
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/schedule_management/notify_schedule.html"; 
    if(windowSchedule){
			 $.window.getWindow(windowSchedule.getWindowId()).setUrl(uri);
    }else{
			var winWidth = ($(window).width())*1 - 320 - 2;
			var winHeight = ($(window).height())*1 - 240 -2;
			windowSchedule = $.window({
				title: "VVME",
				url: uri,
				resizable: false,
				width: 320,
				height: 240,
				maxWidth: 400,
				maxHeight: 300,
				x:winWidth,
				y:winHeight,
				resizable: false,
				maximizable: false,
				minimizable: false,
				showFooter: false,
				scrollable:false,
				withinBrowserWindow:true,
				checkBoundary:true,
				onClose:function(){windowSchedule = false;}
			});
	    
		 
    }
}
function mainFrameOpenUrl(url){
    window.frames["iframe"].frames["main"].location.href=url;
}
var settings = {
	'speed' : 'fast',	// animations: fast, slow, or integer
	'duplicates' : true,	// true or false
	'autoclose' : false // integer or false
};
jQuery(function($){
    
})
// ϵͳ������ʾ content ֧��html.�����html��������ô�������������дjavascript����
function showSystemNotifyContent(content){
   
    if(content.length > 0){
   		 $.sticky(content,settings);
    }
}
function sheduleAClick(schedule_id , is_schedule){
    var url =  '<%=ConfigBean.getStringValue("systenFolder")%>' +"administrator/schedule_management/show_schedule.html";
	url += "?schedule_id="+schedule_id+"&is_schedule="+is_schedule+"&from=out";
	mainFrameOpenUrl(url);
}
function openBillBy(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/add_bill.html"; 
    if(windowBill){
			 $.window.getWindow(windowBill.getWindowId()).setUrl(uri);
    }else{
			 
			windowSchedule = $.window({
				title: "Invoice",
				url: uri,
				resizable: false,
				width: 1100,
				height: 600,
				showModal:true,
				x:-1,
				y:-1,
				showFooter: false,
			 
				withinBrowserWindow:true,
				checkBoundary:true,
				onClose:function(){windowSchedule = false;}
			});
    }
}
//在线查看图片的页面
function openPictureOnlineShow(obj){
	//如果已经存在那么就要刷新这个页面。不过不是的话那么就打开重新建立一个window
	//如果是分辨率小于1440那么就
	var fixWidth = 1100, fixHeight = 700; 
	if($(window).width() * 1 < 1400){
	    fixWidth = 900;
	    fixHeight = 500 ;
	}
	 
	
	if(!obj){return ;}
	var fileWithType = obj.file_with_type;
	var param = jQuery.param(obj);
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_show.html?" +param; 
    //如果fileWithType为52（即checkIn模块 使用新的图片查看 -------wfh
	if(fileWithType==52){
		uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + 'administrator/file/picture_online_show_check_in.html?'+param;
	//	$.artDialog.open(uri , {title: "Show Pictures",width:fixWidth,height:fixHeight, lock: true,opacity: 0.3,fixed: true});
	}
		if(windowPictureShow){
		    $.window.getWindow(windowPictureShow.getWindowId()).setUrl(uri);
		}else{
			    windowSchedule = $.window({
					title: "Photo Preview",
					url: uri,
					resizable: false,
					width: fixWidth,
					height: fixHeight,
					showModal:true,
					x:-1,
					y:-1,
					showFooter: false,
				 
					withinBrowserWindow:true,
					checkBoundary:true,
					onClose:function(){windowSchedule = false;}
				});
		}
	
	
    
}
//OFfice文件在线阅读
function openOfficeFileOnlineShow(file_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/customerservice_qa/show_file_online.html?file_id="+file_id; 
	 windowSchedule = $.window({
		title: "File look up",
		url: uri,
		resizable: false,
		width: 800,
		height: 530,
		showModal:true,
		x:-1,
		y:0,
		showFooter: false,
	 
		withinBrowserWindow:true,
		checkBoundary:true,
		onClose:function(){}
	});
} 
function openUrl(uri){
 	 windowSchedule = $.window({
		title: "Webcam",
		url: uri,
		resizable: false,
		width: 860,
		height: 660,
		showModal:true,
		x:-1,
		y:-1,
		showFooter: false,
		withinBrowserWindow:true,
		checkBoundary:true,
		onClose:function(){}
	});
} 
/**
 * 打开android打印的任务列表
 */
function openPrintTaskList(who){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/print/print_task_list.html?who="+who; 
    
			 
			windowSchedule = $.window({
				title: "Print Task",
				url: uri,
				resizable: false,
				width: 350,
				height: 600,
				showModal:true,
				x:-1,
				y:-1,
				showFooter: false,
			 
				withinBrowserWindow:true,
				checkBoundary:true,
				onClose:function(){windowSchedule = false;}
			});
    
}
function openPrintTaskPreview(uri){
			windowSchedule = $.window({
				title: "Task Preview",
				url: uri,
				resizable: false,
				width: 900,
				height: 500,
				showModal:true,
				x:-1,
				y:-1,
				showFooter: false,
			 
				withinBrowserWindow:true,
				checkBoundary:true,
				onClose:function(){windowSchedule = false;}
			});
    
}
</script>
</head>
<body>
 <div id="galleria" style=""></div>
	<iframe id="iframe" name="iframe" src="frameset.html"></iframe>
</body>
</html>
 