<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	TDate tDate = new TDate();
	String input_en_date = StringUtil.getString(request,"end_date",tDate.formatDate("yyyy-MM-dd"));
	tDate.addDay(-30);
	String input_st_date = StringUtil.getString(request,"start_date",tDate.formatDate("yyyy-MM-dd"));
	
	long catalog_id = StringUtil.getLong(request,"catalog_id");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script language="javascript" src="../../common.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>
		<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
		<!---// load the mcDropdown CSS stylesheet //--->
		<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
		<script src="../js/zebra/zebra.js" type="text/javascript"></script>
		<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
		
		<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
		<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

		<style type="text/css" media="all">
		@import "../js/thickbox/global.css";
		@import "../js/thickbox/thickbox.css";
		</style>

		<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
		<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
		<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript">

$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#search_product_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		updownSelect:true,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
});

function ajaxLoadDataPage(param,url)
{
	$.ajax({
				url: url,
				type: 'post',
				dataType: 'html',
				//timeout: 60000,
				cache:false,
				data:param,
				
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				
				error: function(){
					$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
				},
				
				success: function(html){
					$.unblockUI();
					$("#dataList").html(html);
					//alert(html);
					onLoadInitZebraTable();//表格斑马线
					//$.scrollTo(0,500);//页面向上滚动
				}
			});
}

	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br/>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="65%">
			<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:1050px;">
			<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
		  		<tr>
		    		<td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
						<ul id="productLinemenu" class="mcdropdown_menu">
			             <li rel="0">所有产品线</li>
			             <%
							  DBRow p1[] = productLineMgrTJH.getAllProductLine();
							  for (int i=0; i<p1.length; i++)
							  {
									out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
									 
									out.println("</li>");
							  }
						  %>
			           </ul>
			           <input type="text" name="productLine" id="productLine" value="" />
			           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
					</td>
		 
		    <td height="29" bgcolor="#eeeeee">
			来源：<select name="order_source" id="order_source" style="border:1px #CCCCCC solid" >
          		<option value="">全部来源</option>
				<option value="DIRECTPAY ">DIRECTPAY</option>
				<option value="EBAY">EBAY</option>
				<option value="MANUAL">MANUAL</option>
				<option value="WEBSITE">WEBSITE</option>
        	</select>
	     	&nbsp;&nbsp;
	     	发货仓库：<select id="cid">
	     			 <option value="0">全部仓库</option>
	     			 <%
	     			 	DBRow[] storageRows = catalogMgr.getProductStorageCatalogTree();
						String qx;
						
						for ( int i=0; i<storageRows.length; i++ )
						{
							if ( storageRows[i].get("parentid",0) != 0 )
							 {
							 	qx = "├ ";
							 }
							 else
							 {
							 	qx = "";
							 }
					%>
          <option value="<%=storageRows[i].getString("id")%>"> 
						     <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageRows[i].get("level",0))%>
						     <%=qx%>
						     <%=storageRows[i].getString("title")%>          </option>
						<%
						}
						%>
	     			 </select>
	     			 <input name="groupWith" type="radio" id="groupWith" checked="checked" value="1"/>求和统计<input type="radio" id="groupWith" name="groupWith" value="2"/>分散统计&nbsp;&nbsp;&nbsp;<input type="button" onclick="exportSalesDeliveryAnalysis('')" class="long-button-export" value="导出">
				  </td>
		  </tr>
		  <tr>
		  	<td id="categorymenu_td" width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;"> 
		           <ul id="categorymenu" class="mcdropdown_menu">
					  <li rel="0">所有分类</li>
					  <%
						  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
						  for (int i=0; i<c1.length; i++)
						  {
								out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
					
								  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
								  if (c2.length>0)
								  {
								  		out.println("<ul>");	
								  }
								  for (int ii=0; ii<c2.length; ii++)
								  {
										out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
										
											DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
											  if (c3.length>0)
											  {
													out.println("<ul>");	
											  }
												for (int iii=0; iii<c3.length; iii++)
												{
														out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
														out.println("</li>");
												}
											  if (c3.length>0)
											  {
													out.println("</ul>");	
											  }
											  
										out.println("</li>");				
								  }
								  if (c2.length>0)
								  {
								  		out.println("</ul>");	
								  }
								  
								out.println("</li>");
						  }
						  %>
				</ul>
		  <input type="text" name="category" id="category" value="" />
		  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"/>
		</td>
		<td height="29" bgcolor="#eeeeee">
			<select id="sale_area" onchange="getAreaCountryByCaId(this.value)">
						  		 <option value="0">全部区域</option>
						  		<%
						  			DBRow[] areas = productMgr.getAllCountryAreas(null);
						  			for(int i = 0;i<areas.length;i++)
						  			{
								%>
									<option value="<%=areas[i].get("ca_id",0l)%>"><%=areas[i].getString("name")%></option>
								<%
						  			}
						  		%>
						  	</select>
								&nbsp;&nbsp;&nbsp;
						      <select name="ccid_hidden" style="display: none" id="ccid_hidden" onChange="getStorageProvinceByCcid(this.value);">
							  <option value="0">全部国家</option>
						      </select>
						      &nbsp;&nbsp;&nbsp;
						      <select name="pro_id" id="pro_id" style="display: none">
						      	<option value="0">全部区域</option>
						      </select>
		</td>
		  </tr>
		  <tr>
		  	<td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
		  		商品名称： <input type="text" name="search_product_name" id="search_product_name" style="width:350px;" onclick="cleanProductLine('');"/>
		  	</td>
		  	<td height="29" bgcolor="#eeeeee">
		  		发货开始时间：<input name="input_st_date" type="text" id="input_st_date" size="10" value="<%=input_st_date%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
		    	结束时间：<input name="input_en_date" type="text" id="input_en_date" size="10" value="<%=input_en_date%>" readonly="readonly" /> &nbsp;&nbsp;&nbsp;
		    每日显示:<input type="radio" name="day_source" value="sum_quantity" checked="checked"/>发货数<input type="radio" name="day_source" value="sum_saleroom"/>销售额<input type="radio" name="day_source" value="sum_profit"/>利润
		    <input name="Submit" type="button" class="button_long_search" value="查询" onClick="search('sales_delivery_analysi_table.html','')" />
		    <script>
		    	$("#input_st_date").date_input();
				$("#input_en_date").date_input();
		    </script>
		  	</td>
		  </tr>
		</table>
	</div>
		</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br/>

<div id="dataList"></div>
<form name="export_form"></form>
<script type="text/javascript">

$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_pcid").val(id);
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
				}

});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
					if(id==<%=pro_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
					
				}

});

<%
		if (catalog_id>0)
		{
		%>
		$("#category").mcDropdown("#categorymenu").setValue(<%=catalog_id%>);
		<%
		}
		else
		{
		%>
		$("#category").mcDropdown("#categorymenu").setValue(0);
		<%
		}
		%> 
		
		<%
		if (pro_line_id!=0)
		{
		%>
		$("#productLine").mcDropdown("#productLinemenu").setValue('<%=pro_line_id%>');
		<%
		}
		else
		{
		%>
		$("#productLine").mcDropdown("#productLinemenu").setValue(0);
		<%
		}
		%> 
		
		function cleanProductLine(divId)
		{
			$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
			$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
		}
		
		function cleanSearchKey(divId)
		{
			$("#"+divId+"search_product_name").val("");
		}
		
		//销售区域切换国家
function getAreaCountryByCaId(ca_id)
{

		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getAreaCountrysJSON.action",
				{ca_id:ca_id},
				function callback(data)
				{ 
					$("#ccid_hidden").clearAll();
					$("#ccid_hidden").addOption("全部国家","0");
					
					if (data!="")
					{
						$("#ccid_hidden").css("display","");
						$("#pro_id").css("display","none");
						$("#pro_id").clearAll();
						
						$.each(data,function(i){
							$("#ccid_hidden").addOption(data[i].c_country,data[i].ccid);
						});
					}
					else
					{
						$("#ccid_hidden").css("display","none");
						$("#pro_id").css("display","none");
					}
				}
		);
}

//国家地区切换
function getStorageProvinceByCcid(ccid)
{

		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("全部区域","0");
					
					if (data!="")
					{
						$("#pro_id").css("display","");
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					else
					{
						$("#pro_id").css("display","none");
					}
					
					if (ccid>0)
					{
						$("#pro_id").addOption("Others","-1");
					}
				}
		);
}

$("#search_product_name").keydown(function(event){
	if (event.keyCode==13)
	{
		search('sales_delivery_analysi_table.html','');
	}
});

function search(url,cmd)
{
	var param = "product_name="+$("#"+cmd+"search_product_name").val()+"&catalog_id="+$("#"+cmd+"filter_pcid").val()+"&pro_line_id="+$("#"+cmd+"filter_productLine").val()+"&input_st_date="+$("#"+cmd+"input_st_date").val()+"&input_en_date="+$("#"+cmd+"input_en_date").val()+"&ca_id="+$("#sale_area").val()+"&ccid="+$("#ccid_hidden").val()+"&pro_id="+$("#pro_id").val()+"&type="+$('input[name="groupWith"]:checked').val()+"&order_source="+$("#order_source").val()+"&cid="+$("#cid").val()+"&day_source="+$('input[name="day_source"]:checked').val();
	ajaxLoadDataPage(param,url);
}

function goPage(p,url,cmd)
{
	var param = p+"&product_name="+$("#"+cmd+"search_product_name").val()+"&catalog_id="+$("#"+cmd+"filter_pcid").val()+"&pro_line_id="+$("#"+cmd+"filter_productLine").val()+"&input_st_date="+$("#"+cmd+"input_st_date").val()+"&input_en_date="+$("#"+cmd+"input_en_date").val()+"&ca_id="+$("#sale_area").val()+"&ccid="+$("#ccid_hidden").val()+"&pro_id="+$("#pro_id").val()+"&type="+$('input[name="groupWith"]:checked').val()+"&order_source="+$("#order_source").val()+"&cid="+$("#cid").val()+"&day_source="+$('input[name="day_source"]:checked').val();
	ajaxLoadDataPage(param,url);
}

function exportSalesDeliveryAnalysis(cmd)
{
		var para = "product_name="+$("#"+cmd+"search_product_name").val()+"&catalog_id="+$("#"+cmd+"filter_pcid").val()+"&pro_line_id="+$("#"+cmd+"filter_productLine").val()+"&input_st_date="+$("#"+cmd+"input_st_date").val()+"&input_en_date="+$("#"+cmd+"input_en_date").val()+"&ca_id="+$("#sale_area").val()+"&ccid="+$("#ccid_hidden").val()+"&pro_id="+$("#pro_id").val()+"&type="+$('input[name="groupWith"]:checked').val()+"&order_source="+$("#order_source").val()+"&cid="+$("#cid").val()+"&day_source="+$('input[name="day_source"]:checked').val();
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administraor/product_sales/exportSalesDeliveryAnalysis.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					beforeSend:function(request){
					},
					
					error: function(){
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.export_form.action=date["fileurl"];
							document.export_form.submit();
						}
						else
						{
							alert("该统计无数据，无法导出");
						}
					}
				});
}
</script>
</body>
</html>
