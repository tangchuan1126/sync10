<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String start_sales_date = StringUtil.getString(request,"start_date_sales");
String end_sales_date = StringUtil.getString(request,"end_date_sales");

String start_need_date = StringUtil.getString(request,"input_st_date_need");
String end_need_date = StringUtil.getString(request,"input_en_date_need");

TDate tDate = new TDate();

if (start_sales_date.equals("") && end_sales_date.equals("") && start_need_date.equals("") && end_need_date.equals(""))
{
	start_sales_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	end_sales_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	
	start_need_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	end_need_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script language="javascript" src="../../common.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>


<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
		
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript">

function onLoadInit()
{
	$("#input_st_date").date_input();
	$("#input_en_date").date_input();
	$("#input_st_date_need").date_input();
	$("#input_en_date_need").date_input();
}

function statsProductSales()
{
	var st_date = $("#input_st_date").val();
	var st = st_date.split("-");
	var stdate = new Date();
	stdate.setFullYear(st[0],(st[1]-1),st[2]);
	
	var en_date = $("#input_en_date").val();
	var en = en_date.split("-");
	var endate = new Date();
	endate.setFullYear(en[0],(en[1]-1),en[2]);
	
	var space_date = 1+ Math.floor((endate.getTime()-stdate.getTime())/(1000*24*3600));
	
	$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true 
			
				//fadeOut:  2000
			};		
	var isbreak = false;	
	for(i=1;i<space_date+1;i++)//从1开始为了第一个有进度条，+2因为从差额天并未算选中当天
	{
		if(isbreak)
		{
			alert("break");
			break;
		}
		if(space_date==1)
		{
			$.blockUI({ message: '<span style="font-size:13px;font-weight:bold;color:#666666">&nbsp;<img src="../imgs/long_loading.gif" width="80%"  height="14px" align="left" /><br/><img src="../imgs/sending.gif" align="absmiddle"/>&nbsp;计算'+stdate.getFullYear()+"-"+(stdate.getMonth()+1)+"-"+stdate.getDate()+'数据中.....</span>'});	
		}
		else
		{
			var line = Math.floor(i/space_date*100);
			$.blockUI({ message: '<span style="font-size:13px;font-weight:bold;color:#666666">&nbsp;<img src="../imgs/long_loading.gif" width="'+line+'%" height="14px" align="left"/><br/>计算'+stdate.getFullYear()+"-"+(stdate.getMonth()+1)+"-"+stdate.getDate()+'数据中.....</span>'});
		}
		
		
		var start_date = "start_date_sales="+(stdate.getFullYear()+"-"+(stdate.getMonth()+1)+"-"+stdate.getDate());
		var end_date = "end_date_sales="+(stdate.getFullYear()+"-"+(stdate.getMonth()+1)+"-"+stdate.getDate());
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_sales/statsProductSales.action',
			type: 'post',
			async:false,
			dataType: 'json',
			cache:false,
			data:start_date+'&'+end_date,
			
			beforeSend:function(request){
			},
			
			error: function(){
				isbreak = true;
				alert("数据读取出错！");
			},
			
			success: function(data){
				if(data.result == "true")
				{
					//$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">商品销售量统计成功！</span>' });
					stdate.setDate(stdate.getDate()+1);
				}
				else
				{
					isbreak = true;
					$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">商品销售量统计失败！</span>' });	
					alert("商品销售量统计失败，请重新选择一段发货时间！");
				}
			}	
		});
	}
	
	$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">商品销售量统计成功！</span>' });
	
	setTimeout("$.unblockUI()",2000);
	//$.unblockUI();
}

function statsProductSalesAll()
{
	var st_date = "start_date_sales="+$("#input_st_date").val();
	var en_date = "end_date_sales="+$("#input_en_date").val();
	
	$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true 
			
				//fadeOut:  2000
			};
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_sales/statsProductSales.action',
		type: 'post',
		dataType: 'json',
		cache:false,
		data:st_date+'&'+en_date,
		
		beforeSend:function(request){
		},
		
		error: function(){
			alert("数据读取出错！");
		},
		
		success: function(data){
			if(data.result == "true")
			{
				$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">商品销售量统计成功！</span>' });
				$.unblockUI();
				alert("商品销售量统计成功！");
			}
			else
			{
				$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">商品销售量统计失败！</span>' });	
				alert("商品销售量统计失败，请重新选择一段发货时间！");
			}

		}	
			
	});
	
	//document.stats_sales_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_sales/statsProductSales.action";
	//document.stats_sales_form.start_date_sales.value = document.search_sales_form.input_st_date.value;
	//document.stats_sales_form.end_date_sales.value = document.search_sales_form.input_en_date.value;
	//document.stats_sales_form.submit();
}

function statsProductNeed()
{
	var st_date = "start_date_need="+$("#input_st_date_need").val();
	var en_date = "end_date_need="+$("#input_en_date_need").val();
	
	var st_date = $("#input_st_date_need").val();
	var st = st_date.split("-");
	var stdate = new Date();
	stdate.setFullYear(st[0],(st[1]-1),st[2]);//月份要减1
	
	var en_date = $("#input_en_date_need").val();
	var en = en_date.split("-");
	var endate = new Date();
	endate.setFullYear(en[0],(en[1]-1),en[2]);//月份要减1
	
	var space_date = 1+ Math.floor((endate.getTime()-stdate.getTime())/(1000*24*3600));
	
	$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true 
			
				//fadeOut:  2000
			};		
	var isbreak = false;	
	for(i=1;i<space_date+1;i++)//从1开始为了第一个有进度条，+2因为从差额天并未算选中当天
	{
		if(isbreak)
		{
			alert("break");
			break;
		}
		if(space_date==1)
		{
			$.blockUI({ message: '<span style="font-size:13px;font-weight:bold;color:#666666">&nbsp;<img src="../imgs/long_loading.gif" width="80%"  height="14px" align="left" /><br/><img src="../imgs/sending.gif" align="absmiddle"/>&nbsp;计算'+stdate.getFullYear()+"-"+(stdate.getMonth()+1)+"-"+stdate.getDate()+'数据中.....</span>'});	
		}
		else
		{
			var line = Math.floor(i/space_date*100);
			$.blockUI({ message: '<span style="font-size:13px;font-weight:bold;color:#666666">&nbsp;<img src="../imgs/long_loading.gif" width="'+line+'%" height="14px" align="left"/><br/>计算'+stdate.getFullYear()+"-"+(stdate.getMonth()+1)+"-"+stdate.getDate()+'数据中.....</span>'});
		}
		
		
		var start_date = "start_date_need="+(stdate.getFullYear()+"-"+(stdate.getMonth()+1)+"-"+stdate.getDate());
		var end_date = "end_date_need="+(stdate.getFullYear()+"-"+(stdate.getMonth()+1)+"-"+stdate.getDate());
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order_process/statsProductNeedQuantity.action',
			type: 'post',
			async:false,
			dataType: 'json',
			cache:false,
			data:start_date+'&'+end_date,
			
			beforeSend:function(request){
			},
			
			error: function(){
				alert("数据读取出错！");
			},
			
			success: function(data){
				if(data.result == "true")
				{
					//$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">商品销售量统计成功！</span>' });
					stdate.setDate(stdate.getDate()+1);
				}
				else
				{
					isbreak = true;
					$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">商品需求量统计失败！</span>' });	
					alert("商品需求量统计失败，请重新选择一段发货时间！");
				}
			}	
		});
	}
	
	$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">商品需求量统计成功！</span>' });
	
	setTimeout("$.unblockUI()",2000);
}

function statsProductNeedAll()
{
	var st_date = "start_date_need="+$("#input_st_date_need").val();
	var en_date = "end_date_need="+$("#input_en_date_need").val();
	var sid = "ps_id="+document.search_need_form.cid.value;
	
	$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '400px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true
			
				//fadeOut:  5000
			};
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order_process/statsProductNeedQuantity.action',
		type: 'post',
		dataType: 'json',
		cache:false,
		data:st_date+'&'+en_date+'&'+sid,
		
		beforeSend:function(request){
		},
		
		error: function(){
			alert("数据读取出错！");
		},
		
		success: function(data){
			if(data.result == "true")
			{
				$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">商品需求量统计成功！</span>' });
				$.unblockUI();
				alert("商品需求量统计成功！");
			}
			else
			{
				$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">商品需求量统计失败！</span>' });	
				alert("商品需求量统计失败，请重新选择一段发货时间！");
			}
		}	
			
	});
}

function addDay()
{
	var st_date = $("#input_st_date").val();
	var st = st_date.split("-");
	var stdate = new Date();
	stdate.setFullYear(st[0],(st[1]-1),st[2]);
	alert(stdate);
	var en_date = $("#input_en_date").val();
	var en = en_date.split("-");
	var endate = new Date();
	endate.setFullYear(en[0],(en[1]-1),en[2]);
	
	var space_date = 1+ Math.floor((endate.getTime()-stdate.getTime())/(1000*24*3600));
	
	var isbreak = false;	
	for(i=1;i<space_date+1;i++)//从1开始为了第一个有进度条，+2因为从差额天并未算选中当天
	{
		if(isbreak=="true")
		{
			alert("break");
			break;
		}
		if(space_date==1)
		{
			$.blockUI({ message: '<span style="font-size:13px;font-weight:bold;color:#666666">&nbsp;<img src="../imgs/long_loading.gif" width="80%"  height="14px" align="left" /><br/><img src="../imgs/sending.gif" align="absmiddle"/>&nbsp;计算'+stdate.getFullYear()+"-"+stdate.getMonth()+"-"+stdate.getDate()+'数据中.....</span>'});	
		}
		else
		{
			var line = Math.floor(i/space_date*100);
			$.blockUI({ message: '<span style="font-size:13px;font-weight:bold;color:#666666">&nbsp;<img src="../imgs/long_loading.gif" width="'+line+'%" height="14px" align="left"/><br/>计算'+stdate.getFullYear()+"-"+stdate.getMonth()+"-"+stdate.getDate()+'数据中.....</span>'});
		}
		alert(stdate.getFullYear()+"-"+stdate.getMonth()+"-"+stdate.getDate());
		stdate.setDate(stdate.getDate()+1);
	}
	
	$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">商品需求量统计成功！</span>' });
	
	setTimeout("$.unblockUI()",2000);
}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInit()">
<br/>
<form method="post" name="stats_need_form">
	<input type="hidden" name="start_date_need">
	<input type="hidden" name="end_date_need">
	<input type="hidden" name="ps_id">
</form>

<form method="post" name="stats_sales_form">
	<input type="hidden" name="start_date_sales">
	<input type="hidden" name="end_date_sales">
</form>

<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购应用  » 商品数据运算    </td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
<td>

<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
  	<td align="center" class="page-title">商品需求数据计算</td>
  </tr>
  <tr> 
      <td width="49%" height="30">
	   <form action="product_quantity_count.html" method="get" name="search_need_form">
	   发货时间：
		<input type="hidden" name="cmd">
              开始时间：<input name="input_st_date_need" type="text" id="input_st_date_need" size="9" value="<%=start_need_date%>" readonly="readonly">&nbsp; &nbsp;&nbsp; &nbsp;
			  结束时间：<input name="input_en_date_need" type="text" id="input_en_date_need" size="9" value="<%=end_need_date%>" readonly="readonly">&nbsp; &nbsp;&nbsp; &nbsp;  
	          <label>
	          <input name="Submit" type="button" class="button_long_refresh" onClick="statsProductNeed()" value=" 重新计算 ">
      </label>
       </form>	  </td>
  </tr>
</table>
</div>
<br>
<br>
<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
  	<td align="center" class="page-title">商品销售数据计算</td>
  </tr>
  <tr> 
      <td width="49%" height="30">
	   <form  method="post" name="search_sales_form">
	   发货时间：

			开始时间：&nbsp;<input name="input_st_date" type="text" id="input_st_date" size="9" value="<%=start_sales_date%>" readonly="readonly"> 
             &nbsp;&nbsp;&nbsp;&nbsp;
			 结束时间：<input name="input_en_date" type="text" id="input_en_date" size="9" value="<%=end_sales_date%>" readonly="readonly"> &nbsp;&nbsp;&nbsp;&nbsp;	
	          <input name="Submit" type="button" class="button_long_refresh" onClick="statsProductSales()" value=" 重新计算 ">
       </form>	  </td>
  </tr>
</table>
</div>
</td></tr>
</table>
<br>
</body>
</html>
