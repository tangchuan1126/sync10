<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%

	long catalog_id = StringUtil.getLong(request,"catalog_id");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
	String product_name = StringUtil.getString(request,"product_name");

	String cmd = StringUtil.getString(request,"cmd");
	long ccid = StringUtil.getLong(request,"ccid");
	long pro_id = StringUtil.getLong(request,"pro_id");
	long ca_id = StringUtil.getLong(request,"ca_id");
	int type = StringUtil.getInt(request,"type");//1代表根据选择地域求和统计，2代表根据选择地域的下一级地域分布统计
	String order_source = StringUtil.getString(request,"order_source");
	long cid = StringUtil.getLong(request,"cid",0l);//发货仓库ID
	
	String day_source = StringUtil.getString(request,"day_source");

	TDate tDate = new TDate();
	String input_en_date = StringUtil.getString(request,"input_en_date",tDate.formatDate("yyyy-MM-dd"));
	tDate.addDay(-30);
	String input_st_date = StringUtil.getString(request,"input_st_date",tDate.formatDate("yyyy-MM-dd"));
	
	
	
	
	if (input_st_date.equals("") && input_en_date.equals(""))
	{
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
				
	}
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(40);
	
	DBRow rows [];
	DBRow[] DateRow= ordersProcessMgrQLL.getDate(input_st_date,input_en_date);
	rows = productSalesMgrTJH.getAllProductSalesByCondition(input_st_date,input_en_date,catalog_id,pro_line_id,product_name,ca_id,ccid,pro_id,cid,type,order_source,day_source,pc);
	
	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/select.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>

</head>
<script language="javascript">

</script>
<body>


<%
			if(rows!=null && rows.length!=0&&!rows[0].getString("date_0").equals(""))
			{
				%>
				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable" > 
				<tr> 
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">所属产品线</th>
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">商品分类</th>
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">产品名称</th>
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">发货仓库</th>
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">来源</th>
					  <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">销售区域</th>
					  <%
					  	if(ca_id>0)
					  	{
					  %>
					  <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">销售国家</th>
					  <%		
					  	}
					  %>
					  <%
					  	if(ccid>0)
					  	{
					  %>
					  <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">销售地区</th>
					  <%		
					  	}
					  %>
					 <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">总发货数</th>
					 <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">平均发货数</th>
					 <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">商品总成本</th>
					 <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">总运费</th>
					 <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">总重量</th>
					 <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">总销售额</th>
					 <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">总利润</th>
					 <%
						if(DateRow!=null && DateRow.length!=0 )
						{
							for(int j=0; j<DateRow.length; j++ )
							{
							%>
								<th  align="center"  class="right-title" style="vertical-align: center;text-align: center;"><%=DateRow[j].getString("date")%></th>
							<% 
							}
						}					 
					  %>
				</tr>
				<% 
					for(int i=0; i<rows.length;i++)
					{
				%>
					<tr>
						 <td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= rows[i].getString("pl_name")%>&nbsp;</td>
						 <td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= rows[i].getString("title")%></td>
						 <td width="10%" nowrap="nowrap" align="left" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= rows[i].getString("p_name")%></td>
						 <td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%=cid==0?"ALL":rows[i].getString("storage_title")%></td>
						 <td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= order_source.equals("")?"ALL":rows[i].getString("order_source")%></td>
						 <td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'>
						 	<% 
						 		if(ca_id==0&&type==1)
						 		{
						 			out.print("ALL");
						 		}
						 		else
						 		{
						 			out.print(rows[i].getString("area_name"));
						 		}
						 		
						 	%>
						 </td>
						 <% 
						 	if(ca_id>0)
						 	{
						 %>
						 	<td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
						 	<% 
						 		if(ccid==0&&type==1)
						 		{
						 			out.print("ALL");
						 		}
						 		else
						 		{
						 			out.print(rows[i].getString("c_country"));
						 		}
						 		
						 	%>
						 	</td>
						 <%
						 	}
						 %>
						 
						 <% 
						 	if(ccid>0)
						 	{
						 %>
						 	<td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
						 	<% 
						 		if(pro_id==0&&type==1)
						 		{
						 			out.print("ALL");
						 		}
						 		else
						 		{
						 			out.print(rows[i].getString("pro_name"));
						 		}
						 		
						 	%>
						 	</td>
						 <%
						 	}
						 %>
						 <td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= rows[i].getString("sum_quantity")%>  </td>
						 <td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%=MoneyUtil.round(rows[i].get("sum_quantity",0d)/DateRow.length,2)%></td>
						 <td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%=rows[i].get("sum_product_cost",0d)%></td>
						 <td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%=rows[i].get("sum_freight",0d)%></td>
						 <td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%=rows[i].get("sum_weight",0d)%></td>
						 <td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%=rows[i].get("sum_saleroom",0d)%></td>
						 <td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%=rows[i].get("sum_profit",0d)%></td>
						 <%
							 for(int j=0; j<DateRow.length; j++ )
								{
									if(!rows[i].getString("date_"+j).equals(""))
									{
								%>
									<td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= rows[i].getString("date_"+j)%>  </td>
								<% 
									}
								}
						  %>
						 </tr> 
					<% 
						}
				%>
				 </table>
				<% 
			}
		 %>
<br />

<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:goPage('p=1','sales_delivery_analysi_table.html','')",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:goPage('p="+pre+"','sales_delivery_analysi_table.html','')",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:goPage('p="+next+"','sales_delivery_analysi_table.html','')",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:goPage('p="+pc.getPageCount()+"','sales_delivery_analysi_table.html','')",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:goPage('p='document.getElementById('jump_p2').value+'','sales_delivery_analysi_table.html')" value="GO"/>
    </td>
  </tr>
</table>
</body>
</html>



