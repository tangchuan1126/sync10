<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
	long catalog_id = StringUtil.getLong(request,"catalog_id");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
	String product_name = StringUtil.getString(request,"product_name");
	String input_st_date = StringUtil.getString(request,"start_date");
	String input_en_date = StringUtil.getString(request,"end_date");
	String order_source = StringUtil.getString(request,"order_source");
	String monetary_unit = StringUtil.getString(request,"monetary");
	String key = StringUtil.getString(request,"key");
	String cmd = StringUtil.getString(request,"cmd");
	
	long cid = StringUtil.getLong(request,"cid");
	TDate tDate = new TDate();

	if (input_st_date.equals("") && input_en_date.equals(""))
	{
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		
		
	}
	
	productSalesMgrTJH.statsAllCountryProductSales(catalog_id,pro_line_id,cid,cmd,product_name,order_source,monetary_unit,input_st_date,input_en_date);
	
	DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
 %>
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>标题文档</title>

<link rel="stylesheet" type="text/css" href="../js/fusionMaps/dimming.css" />
<link rel="stylesheet" type="text/css" href="../js/fusionMaps/Style.css" />
<script language="javascript" src="../js/fusionMaps/JSClass/FW.js"></script>
<script type="text/javascript" src="../js/fusionMaps/JSClass/FusionMaps.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">


<script language="javascript">
function onLoadInit()
{
	$("#input_st_date").date_input();
	$("#input_en_date").date_input();
}

	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
	
function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"search_product_name").val("");
}
</script>

</head>

<body onLoad="onLoadInit();">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购应用 »   产品销售分布图</td>
  </tr>
</table>
<br>
<form method="post" name="stats_form">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="65%">
	
	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
	
	<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
			<ul id="productLinemenu" class="mcdropdown_menu">
             <li rel="0">所有产品线</li>
             <%
				  DBRow p1[] = productLineMgrTJH.getAllProductLine();
				  for (int i=0; i<p1.length; i++)
				  {
						out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
						 
						out.println("</li>");
				  }
			  %>
           </ul>
           <input type="text" name="productLine" id="productLine" value="" />
           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/></td>
    <td bgcolor="#eeeeee">
	<!-- <textarea name="all_catalog" class="input-line" id="all_catalog"></textarea> -->
           &nbsp;
           <font style="font-size: 14px;">商品名称：</font><input type="text" name="search_key" id="search_key" value="<%=product_name%>" style="width:200px;" onclick="cancelCatalogAndProductLine()"/>&nbsp;&nbsp;&nbsp;&nbsp;
		   <select name="order_source" id="order_source" style="border:1px #CCCCCC solid" >
          		<option value="">全部来源</option>
				<option value="DIRECTPAY "<%=order_source.equals("DIRECTPAY")?"selected":""%>>DIRECTPAY</option>
				<option value="EBAY" <%=order_source.equals("EBAY")?"selected":""%>>EBAY</option>
				<option value="MANUAL" <%=order_source.equals("MANUAL")?"selected":""%>>MANUAL</option>
				<option value="WEBSITE" <%=order_source.equals("WEBSITE")?"selected":""%>>WEBSITE</option>
        	</select>
		   
	</td>
  </tr>
  <tr>
  	<td id="categorymenu_td" width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;"> 
			  <ul id="categorymenu" class="mcdropdown_menu">
			  <li rel="0">所有分类</li>
			  <%
				  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
				  for (int i=0; i<c1.length; i++)
				  {
						out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
			
						  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
						  if (c2.length>0)
						  {
						  		out.println("<ul>");	
						  }
						  for (int ii=0; ii<c2.length; ii++)
						  {
								out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
								
									DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
									  if (c3.length>0)
									  {
											out.println("<ul>");	
									  }
										for (int iii=0; iii<c3.length; iii++)
										{
												out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
												out.println("</li>");
										}
									  if (c3.length>0)
									  {
											out.println("</ul>");	
									  }
									  
								out.println("</li>");				
						  }
						  if (c2.length>0)
						  {
						  		out.println("</ul>");	
						  }
						  
						out.println("</li>");
				  }
				  %>
			</ul>
			<input type="text" name="category" id="category" value="" />
	  		<input type="hidden" name="filter_pcid" id="filter_pcid" value="0"/>
</td>
<td valign="top" style="padding-left: 10px;">
&nbsp;<font style="font-size: 14px;">货币形式：<input type="radio" <%=monetary_unit.equals("RMB")?"":"checked='checked'" %> name="monetary_unit" id="monetary_unit" value="USD"/>美金<input type="radio"  name="monetary_unit" id="monetary_unit" value="RMB" <%=monetary_unit.equals("RMB")?"checked='checked'":"" %> />人民币</font>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select name="filter_cid" id="filter_cid">
				<option value="0">所有仓库</option>
				<%
				String qx;
				
				for ( int i=0; i<treeRows.length; i++ )
				{
					if ( treeRows[i].get("parentid",0) != 0 )
					 {
					 	qx = "├ ";
					 }
					 else
					 {
					 	qx = "";
					 }
				%>
				          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==cid?"selected":""%>> 
				          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
				          <%=qx%>
				          <%=treeRows[i].getString("title")%>          </option>
				          <%
				}
				%>
        </select>
</td>
  </tr>
  <tr>
  	<td bgcolor="#eeeeee" colspan="2">
发货开始时间：<input name="input_st_date" type="text" id="input_st_date" size="9" value="<%=input_st_date%>" readonly="readonly">&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
    	结束时间：<input name="input_en_date" type="text" id="input_en_date" size="9" value="<%=input_en_date%>" readonly="readonly">&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
<input name="Submit3" type="button" class="long-button" value="查看销售额分布" onClick="searchProductSalesRoom()" /> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
<input name="Submit3" type="button" class="long-button" value="查看成本分布" onClick="searchProductCost()" />&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
<input name="Submit3" type="button" class="long-button" value="查看利润分布" onClick="searchProductProfit()" />
   </td>
  </tr>
</table>	
	</div>
	
	
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
</form>
<br />
<script type="text/javascript">

	function searchProductSalesRoom()
	{
		
		var catalog_id = $("#filter_pcid").val();
		var pro_line_id = $("#filter_productLine").val();
		var productId = $("#search_key").val();
		var order_source = $("#order_source").val();
		var en_date = $("#input_en_date").val();
		var st_date = $("#input_st_date").val();
		var cid = $("#filter_cid").val()
		document.stats_sales_form.cid.value = cid;
		var monetary = $("#monetary_unit:checked").val();
		
		document.stats_sales_form.cmd.value="salesRoom";
		
		document.stats_sales_form.catalog_id.value = catalog_id;
		document.stats_sales_form.pro_line_id.value = pro_line_id;
		document.stats_sales_form.product_name.value = productId;
		document.stats_sales_form.order_source.value = order_source;
		document.stats_sales_form.start_date.value = st_date;
		document.stats_sales_form.end_date.value = en_date;
		document.stats_sales_form.monetary.value = monetary;
		
		document.stats_sales_form.submit();
		

	}
	
	function searchProductCost()
	{
		
		
		var catalog_id = $("#filter_pcid").val();
		var pro_line_id = $("#filter_productLine").val();
		var productId = $("#search_key").val();
		var order_source = $("#order_source").val();
		var en_date = $("#input_en_date").val();
		var st_date = $("#input_st_date").val();
		var monetary = $("#monetary_unit:checked").val();
		var cid = $("#filter_cid").val()
		document.stats_sales_form.cid.value = cid;
		document.stats_sales_form.monetary.value = monetary;
		document.stats_sales_form.cmd.value="cost";
		
		document.stats_sales_form.catalog_id.value = catalog_id;
		document.stats_sales_form.pro_line_id.value = pro_line_id;
		document.stats_sales_form.product_name.value = productId;
		document.stats_sales_form.order_source.value = order_source;
		document.stats_sales_form.start_date.value=st_date;
		document.stats_sales_form.end_date.value=en_date;
		document.stats_sales_form.submit();
		
	}
	
	function searchProductProfit()
	{
		
		var catalog_id = $("#filter_pcid").val();
		var pro_line_id = $("#filter_productLine").val();
		var productId = $("#search_key").val();
		var order_source = $("#order_source").val();
		var en_date = $("#input_en_date").val();
		var st_date = $("#input_st_date").val();
		var monetary = $("#monetary_unit:checked").val();
		var cid = $("#filter_cid").val()
		document.stats_sales_form.cid.value = cid;
		document.stats_sales_form.monetary.value = monetary;
		
		document.stats_sales_form.cmd.value="profit";

		document.stats_sales_form.catalog_id.value = catalog_id;
		document.stats_sales_form.pro_line_id.value = pro_line_id;
		document.stats_sales_form.product_name.value = productId;
		document.stats_sales_form.order_source.value = order_source;
		document.stats_sales_form.start_date.value=st_date;
		document.stats_sales_form.end_date.value=en_date;
		document.stats_sales_form.submit();
		
		
	}
	
	function drillDown(fusion_id,cmd,catalog_id,product_line_id,productId,orderSource,monetary,st_date,en_date,cid)
	{
		
	var f_id = "fusion_id="+fusion_id;
 	var c_id="catalog_id="+catalog_id;
 	var p_id = "p_line_id="+product_line_id;
 	var param = "cmd="+cmd;
 	var source = "order_source="+orderSource;
 	var product_Id = "product_id="+productId;
 	var startDate = "start_date="+st_date;
 	var endDate = "end_date="+en_date;
 	var monetary_unit = "monetary="+monetary;
 	var cid = "cid="+cid;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_sales/GetFusionMapsByProductSalesRoomJSON.action',
		//url:dataUrl,
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:f_id+'&'+c_id+'&'+param+'&'+c_id+'&'+p_id+'&'+product_Id+'&'+source+'&'+monetary_unit+'&'+startDate+'&'+endDate+'&'+cid,
		
		beforeSend:function(request){
		},
		
		error: function(){
			alert("数据读取出错！");
		},
		
		success: function(data){
			var dv = document.getElementById("pmDrillDownTitle");
			dv.innerHTML = " Map of " + data.c_country;
			var map1 = new FusionMaps(data.map_flash, "pmIdUSDrillDown", "650", "450", "0", "1");
			map1.setDataURL("fusionMaps/mapsXml/province.xml");
			map1.render("pmIdUSDrillDown");

		}	
			
	});
	displayFloatingDiv('drilledDownWindow',158,20,975,500);
	setWindow(158,150,960,500); 
	zoomWindow();	
	}	
	
	
	$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#search_key").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	$("#search_key").keydown(function(event){
		if (event.keyCode==13)
		{
			//search();
		}
	});

});
	
</script>

<form method="post" name="stats_sales_form">
	<input type="hidden" name="catalog_id">
	<input type="hidden" name="pro_line_id">
	<input type="hidden" name="cmd" id="cmd"/>
	<input type="hidden" name="product_name">
	<input type="hidden" name="order_source">
	<input type="hidden" name="start_date">
	<input type="hidden" name="end_date">
	<input type="hidden" name="monetary">
	<input type="hidden" name="cid"/>
</form>

<div id="drilledDownWindow" class="fWindow">
	<input type="hidden" name="fusion_id" id="fusion_id" />
	<div style='width:960px;height:550px;overflow:hidden;background:#FFFFFF;'>
		<table cellspacing="0" cellpadding="0" border="0">
			<tbody>
				<tr>
					<td align="center" valign="top" rowspan="3">
						<div id="pmIdUSDrillDown" style="overflow:auto;'">
						</div>
					</td>
				</tr>
				<tr>
					<td align="right" valign="top">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div id="dimmer" class="dimmer"></div>

<div id="showDiv" style="display: block">
<table border="0" cellSpacing="0" cellPadding="0" width="970">
  <tbody>
  <tr>
    <td bgColor="#ffffff" height="3"></td></tr>
  <tr>
    <td bgColor="#ffffff" height="3"></td></tr>
  <tr>
    <td>
      <table class="borderDarkBlue" border="0" cellSpacing="0" cellPadding="0" width="975">
        <tbody>
        <tr>
          <td vAlign="center" align="middle">
            <table style="TEXT-ALIGN: center" class="BorderWhite" border="0" cellSpacing="0" cellPadding="0" width="973">
              <tbody>
	              <tr>
	                <td class="bluetr" height="28"><span id="pmRootTitle" class="textBoldLight">Map of WorldWidthCountries [Please click on a county to 
	                  drill down to that county]</span></td></tr>
	              <tr>
					<td align="center" valign="top">
					<!--<div id="mapdiv" style="overflow: hidden;">  	</div>-->
					<table cellspacing="0" cellpadding="0" border="0">
					<tbody>
					<tr>
					<td>
						<div id="pmIdUSDrillRoot" align="center" style="overflow: hidden;">
						<script type="text/javascript">
							var map1 = new FusionMaps("fusionMaps/maps/FCMap_WorldwithCountries.swf", "pmIdUSDrillRoot", "1024", "660", "0", "1");
							map1.setDataURL("fusionMaps/mapsXml/worlds.xml");
							map1.render("pmIdUSDrillRoot");
						</script>
						</div>
					</td>
					</tr>
	               </tbody>
				</table>
		

			</td>
			</tr>
             </tbody>
            </table>
           </td>
          </tr>
          </tbody>
         </table>
       </td>
     </tr>
  <tr>
    <td bgColor="#ffffff" height="3"></td>
  </tr>
  <tr>
    <td bgColor="#ffffff" height="3">&nbsp;
    </td>
  </tr>
 </tbody>
</table>
</div>
<script type="text/javascript">
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}

});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#filter_productLine").val(id);
					if(id==<%=pro_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"",0);
					}
					if(id!=0)
					{
						cleanSearchKey("");
					}
				}
});

<%
if (catalog_id>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=catalog_id%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 

<%
if (pro_line_id!=0)
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue(<%=pro_line_id%>);
<%
}
else
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue(0);
<%
}
%> 
	function cancelCatalogAndProductLine()
		{
			$("#category").mcDropdown("#categorymenu").setValue(0);
			$("#productLine").mcDropdown("#pLinemenu").setValue(0);
		}
</script>
</body>
</html>




