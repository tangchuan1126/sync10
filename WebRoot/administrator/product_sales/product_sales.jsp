<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String product_name = StringUtil.getString(request,"product_name");
long pro_line_id = StringUtil.getLong(request,"pro_line_id");
String cmd = StringUtil.getString(request,"cmd");
long catalog_id = StringUtil.getLong(request,"catalog_id");

TDate tDate = new TDate();
String input_en_date = StringUtil.getString(request,"end_date",tDate.formatDate("yyyy-MM-dd"));
tDate.addDay(-30);
String input_st_date = StringUtil.getString(request,"start_date",tDate.formatDate("yyyy-MM-dd"));

String order_source = StringUtil.getString(request,"order_source");


if (input_st_date.equals("") && input_en_date.equals(""))
{
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			
}
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(40);

DBRow rows [];

rows = productSalesMgrTJH.getAllProductSalesByCondition(input_st_date,input_en_date,catalog_id,pro_line_id,product_name,order_source,pc);

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script language="javascript" src="../../common.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>
		<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
		<!---// load the mcDropdown CSS stylesheet //--->
		<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
		<script src="../js/zebra/zebra.js" type="text/javascript"></script>
		<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
		<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
		<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript">

$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#search_product_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		updownSelect:true,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	$("#search_product_name").keydown(function(event){
		if (event.keyCode==13)
		{
			statsProductSales();
		}
	});
	
});

function statsProductSales()
{
	var product_name = $("#search_product_name").val();
	var pro_line_id = $("#filter_productLine").val();
	var catalog_id = $("#filter_pcid").val();
	var order_source = $("#order_source").val();
	
	document.stats_form.cmd.value = "filter";
	document.stats_form.product_name.value = product_name;
	document.stats_form.pro_line_id.value = pro_line_id;
	document.stats_form.catalog_id.value = catalog_id;
	document.stats_form.order_source.value = order_source;
	
	document.stats_form.start_date.value=$("#input_st_date").val();
	document.stats_form.end_date.value=$("#input_en_date").val();
	
	document.stats_form.submit();
}

	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br/>
<form method="post" name="stats_form">
	<input type="hidden" name="start_date">
	<input type="hidden" name="end_date">
	<input type="hidden" name="cmd">
	<input type="hidden" name="product_name">
	<input type="hidden" name="pro_line_id">
	<input type="hidden" name="catalog_id">
	<input type="hidden" name="order_source">
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购应用  »    产品销售分布</td>
  </tr>
</table>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="65%">
			<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
			<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
		  		<tr>
		    		<td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
						<ul id="productLinemenu" class="mcdropdown_menu">
			             <li rel="0">所有产品线</li>
			             <%
							  DBRow p1[] = productLineMgrTJH.getAllProductLine();
							  for (int i=0; i<p1.length; i++)
							  {
									out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
									 
									out.println("</li>");
							  }
						  %>
			           </ul>
			           <input type="text" name="productLine" id="productLine" value="" />
			           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
					</td>
		 
		    <td height="29" bgcolor="#eeeeee">
			
		          商品名称： <input type="text" name="search_product_name" id="search_product_name" value="<%=product_name%>" style="width:200px;" onclick="cleanProductLine('')"/>
		          &nbsp;&nbsp;
			来源：<select name="order_source" id="order_source" style="border:1px #CCCCCC solid" >
          		<option value="">全部来源</option>
				<option value="DIRECTPAY "<%=order_source.equals("DIRECTPAY")?"selected":""%>>DIRECTPAY</option>
				<option value="EBAY" <%=order_source.equals("EBAY")?"selected":""%>>EBAY</option>
				<option value="MANUAL" <%=order_source.equals("MANUAL")?"selected":""%>>MANUAL</option>
				<option value="WEBSITE" <%=order_source.equals("WEBSITE")?"selected":""%>>WEBSITE</option>
        	</select>
				  </td>
		  </tr>
		  <tr>
		  	<td id="categorymenu_td" width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;"> 
		           <ul id="categorymenu" class="mcdropdown_menu">
					  <li rel="0">所有分类</li>
					  <%
						  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
						  for (int i=0; i<c1.length; i++)
						  {
								out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
					
								  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
								  if (c2.length>0)
								  {
								  		out.println("<ul>");	
								  }
								  for (int ii=0; ii<c2.length; ii++)
								  {
										out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
										
											DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
											  if (c3.length>0)
											  {
													out.println("<ul>");	
											  }
												for (int iii=0; iii<c3.length; iii++)
												{
														out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
														out.println("</li>");
												}
											  if (c3.length>0)
											  {
													out.println("</ul>");	
											  }
											  
										out.println("</li>");				
								  }
								  if (c2.length>0)
								  {
								  		out.println("</ul>");	
								  }
								  
								out.println("</li>");
						  }
						  %>
				</ul>
		  <input type="text" name="category" id="category" value="" />
		  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"/>
		</td>
		<td height="29" bgcolor="#eeeeee">
		    	发货开始时间：<input name="input_st_date" type="text" id="input_st_date" size="10" value="<%=input_st_date%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
		    	结束时间：<input name="input_en_date" type="text" id="input_en_date" size="10" value="<%=input_en_date%>" readonly="readonly" /> &nbsp;&nbsp;&nbsp;
		    <input name="Submit" type="button" class="button_long_search" value="查询" onClick="statsProductSales()" />
		    <script>
		    	$("#input_st_date").date_input();
				$("#input_en_date").date_input();
		    </script>
		    </p></td>
		  </tr>
		</table>
	</div>
		</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
  <tr>
    <td></td>
  </tr>
</table>
<form name="listForm" method="post">
	<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	    <tr> 
			<th width="5%" class="right-title" style="text-align:center">订单号</th>
			<th width="6%" class="right-title" style="text-align:center">来源</th>
	        <th width="8%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">商品名称</th>
	        <th width="6%"  class="right-title" style="vertical-align: center;text-align: center;">销售数</th>
	        <th width="8%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">进单时间</th>
	        <th width="9%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">发货时间</th>
	        <th width="7%"  class="right-title" style="vertical-align: center;text-align: center;">商品总成本</th>
	        <th width="7%"  class="right-title" style="vertical-align: center;text-align: center;">总运费</th>
	        <th width="7%"  class="right-title" style="vertical-align: center;text-align: center;">总重量</th>
	        <th width="9%"  class="right-title" style="vertical-align: center;text-align: center;">销售额</th>
	        <th width="6%"  class="right-title" style="vertical-align: center;text-align: center;">发货仓库</th>
	        <th width="11%"  class="right-title" style="vertical-align: center;text-align: center;">国家</th>
	        <th width="6%"  class="right-title" style="vertical-align: center;text-align: center;">省、州</th>
			<th width="5%"  class="right-title" style="vertical-align: center;text-align: center;">利润</th>
	    </tr>
	
	    <%
	for ( int i=0; i<rows.length; i++ )
	{
	
	%>
	    <tr  > 
		  <td style='word-break:break-all;' align="center">
		  	<%=rows[i].getString("oid") %>
		  </td>
		  <td style='word-break:break-all;' align="center">
		  	<%=rows[i].getString("order_source") %>
		  </td>
	      <td height="70" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
			<%
					DBRow product = orderProcessMgr.getProductById(rows[i].get("product_id",0l));
					if(product != null)
					{
						out.print(product.getString("p_name"));
					}
				 %>
		 </td>
	      <td align="center" valign="middle" style='word-break:break-all;' >      	
	      	<%=rows[i].get("quantity",0d)%>
      	  </td>
	      <td align="left" valign="middle">
		  	 <%=rows[i].getString("single_date").substring(0,19)%>&nbsp;
		  </td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("delivery_date").substring(0,19)%>&nbsp;</td>
	      <td align="center" valign="middle"   ><%=rows[i].get("product_cost",0d)%>&nbsp;</td>
		  <td align="center" valign="middle"><%=rows[i].get("freight",0d)%>&nbsp;</td>
	      <td align="center" valign="middle"   ><%=rows[i].get("weight",0d)%>&nbsp;</td>
	      <td align="center" valign="middle"  ><%=rows[i].get("saleroom",0d)%>&nbsp;</td>
	      <td align="center" valign="middle">
	      <%
	      	DBRow storage = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("storage_id",0l));
	      	if(storage !=null)
	      	{
	      		out.print(storage.getString("title"));
	      	}
	      %>&nbsp;
	      </td>
	      <td align="center" valign="middle"  >
	      	<%
	      		DBRow nation = supplierMgrTJH.getDetailCountry(rows[i].get("country_id",0l));
	      		if(nation != null)
	      		{
	      			out.print(nation.getString("c_country"));
	      		}
	      	 %>&nbsp;</td>
	      <td align="left" valign="middle"  >
		  	 <%
	      		DBRow province = supplierMgrTJH.getDetailProvinceById(rows[i].get("province_id",0l));
	      		if(province != null)
	      		{
	      			out.print(province.getString("pro_name"));
	      		}
	      	 %>
		  &nbsp;
	      </td>
	      <td align="center" valign="middle"> <%=rows[i].get("profit",0d)  %></td>
	    </tr>
	    <%
	}
	%>
	  
	</table>
</form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="product_sales.html">
    <input type="hidden" name="p">
    <input type="hidden" name="product_name" value="<%=product_name %>">
    <input type="hidden" name="start_date" value="<%=input_st_date %>">
    <input type="hidden" name="end_date" value="<%=input_en_date %>">
    <input type="hidden" name="catalog_id" value="<%=catalog_id %>">
    <input type="hidden" name="cmd" value="<%=cmd %>">
    <input type="hidden" name="pro_line_id" value="<%=pro_line_id %>">
    <input type="hidden" name="order_source" value="<%=order_source %>">
  </form>
  <tr>
        <td height="28" align="right" valign="middle" class="turn-page-table"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<script type="text/javascript">

$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_pcid").val(id);
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
				}

});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
					if(id==<%=pro_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
					
				}

});

<%
		if (catalog_id>0)
		{
		%>
		$("#category").mcDropdown("#categorymenu").setValue(<%=catalog_id%>);
		<%
		}
		else
		{
		%>
		$("#category").mcDropdown("#categorymenu").setValue(0);
		<%
		}
		%> 
		
		<%
		if (pro_line_id!=0)
		{
		%>
		$("#productLine").mcDropdown("#productLinemenu").setValue('<%=pro_line_id%>');
		<%
		}
		else
		{
		%>
		$("#productLine").mcDropdown("#productLinemenu").setValue(0);
		<%
		}
		%> 
		
		function cleanProductLine(divId)
		{
			$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
			$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
		}
		
		function cleanSearchKey(divId)
		{
			$("#"+divId+"search_product_name").val("");
		}
</script>
</body>
</html>
