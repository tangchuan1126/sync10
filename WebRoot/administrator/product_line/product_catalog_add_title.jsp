<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 	long adminId=adminLoggerBean.getAdid();


 	//根据登录帐号判断是否为客户
 	Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
 	DBRow[] titles;
 	if(bl){
 		titles=adminMgrZwb.getProprietaryByadminId(adminId);
 	}else{
 		titles=mgrZwb.selectAllTitle();
 	}

     long productCatalogId=StringUtil.getLong(request,"productCatalogId");
     String productCatalogName=StringUtil.getString(request,"productCatalogName");
     
     DBRow[] catalogTitles=proprietaryMgrZyj.findProprietaryByAdidAndProductCatagoryId(0L, productCatalogId, 0L, null, request);
     	//mgrZwb.selectTitleByProductCatalog(productCatalogId);
 %>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<title>Manage Product Category Linked Title</title>

<script type="text/javascript">
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

jQuery(function($){
	/*
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
        $("#tabs").tabs("select",0);  //默认选中第一个
        */
})
</script>

</head>
<body onLoad="onLoadInitZebraTable();">
<div align="left" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
     <font style="font-weight:bold;">Current Product Category:</font><font  style="font-weight:bold;size:16px;color:#00F">&nbsp;<%=productCatalogName %></font>
</div>
<br/>
<div id="tabs">
<%--	<ul>--%>
<%--		<li><a href="#update">Manage</a></li>	 --%>
<%--	</ul>--%>
	<div id="update">
	     <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     	<tr>
	     		<td width="310px" height="30px" bgcolor="#dddddd" style="padding-left:20px;border-right:1px solid #eeeeee;font-weight:bold" align="">Category Linked Title</td>
	     		<td bgcolor="#dddddd" style="padding-left:5px;font-weight:bold" align="center">Current User Linked Title</td>
	     	</tr>
	     </table>
         <div align="left" style=" margin-top:2px; height:390px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				    <td>
					    <div style="background-color:#FFF;width:250px; border:2px #dddddd solid; height:385px; float:left; overflow:auto">
			        		 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
			        		 		<!-- 循环已经选择TITLE  -->
				        		 	<input type="hidden" id="has_title" value=""/>
						     		<%for(int i=0;i<catalogTitles.length;i++){ %>
									<tr>
										<td  id="<%=catalogTitles[i].getString("title_id") %>" style="border-bottom:1px solid #dddddd;cursor:pointer;" width="20%" height="30px" align="center" onclick="choiceHasTitle(this.id)">
									 		<%=catalogTitles[i].getString("title_name") %>
									 	</td>
									</tr>
									<%} %>
							 </table>
			      		</div>
				    </td>
				    <td align="center">
				    	<div style="width:155px">
				    	<a style="cursor:pointer">
				    		<img alt="" src="./img/add_jt.gif" onclick="managerTitle('del','<%=productCatalogId %>')" />
				    	</a>
				    	<br><br><br>
				    	<a style="cursor:pointer">
				    		<img alt="" src="./img/det_jt.gif" onclick="managerTitle('add','<%=productCatalogId %>')"/>
				    	</a>
				    	</div>
				    </td>
				    <td>
				   	    <div style="background-color:#FFF;width:250px; border:2px #dddddd solid; height:385px; float:left; overflow:auto">
			        		 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
					        	   <!-- 循环未选择TITLE  -->
				        		 	<input type="hidden" id="unhas_title" value=""/>
						       		<%for(int i=0;i<titles.length;i++){ 
							       		boolean notLinked = true;
							       		for(int b=0;b<catalogTitles.length;b++){
						       				if(catalogTitles[b].getString("title_id").equals(titles[i].getString("title_id"))){
						       					notLinked = false;
						       					break;
						       				}
						       			}		
						       			if(notLinked){
						       		%>
									<tr>
										<td  id="<%=titles[i].getString("title_id") %>" style="border-bottom:1px solid #dddddd;cursor:pointer;" width="20%" height="30px" align="center" onclick="choiceUnhasTitle(this.id)">
											<%=titles[i].getString("title_name") %>
										</td>
									</tr>
									<%}
									} %>
							 </table>
			      		</div>
				    </td>
				</tr>
			</table>
		</div>         
    </div>
</div>

  
</body>
</html>
<script>
function choiceHasTitle(id){
	
	if((','+$("#has_title").val()+',').indexOf(','+id+',')==-1){
		$("#"+id).css("background-color","#E6F3C5");
		if($("#has_title").val()==''){
			$("#has_title").val(id);
		}else{
			$("#has_title").val($("#has_title").val()+','+id);
		}
	}else{
		$("#"+id).css("background-color","white");
		$("#has_title").val($("#has_title").val().replace(","+id+",",","));
		$("#has_title").val($("#has_title").val().replace(","+id,""));
		$("#has_title").val($("#has_title").val().replace(id+",",""));
		$("#has_title").val($("#has_title").val().replace(id,""));
	}
	
}

function choiceUnhasTitle(id){
	
	if((','+$("#unhas_title").val()+',').indexOf(','+id+',')==-1){
		$("#"+id).css("background-color","#E6F3C5");
		if($("#unhas_title").val()==''){
			$("#unhas_title").val(id);
		}else{
			$("#unhas_title").val($("#unhas_title").val()+','+id);
		}
	}else{
		$("#"+id).css("background-color","white");
		$("#unhas_title").val($("#unhas_title").val().replace(","+id+",",","));
		$("#unhas_title").val($("#unhas_title").val().replace(","+id,""));
		$("#unhas_title").val($("#unhas_title").val().replace(id+",",""));
		$("#unhas_title").val($("#unhas_title").val().replace(id,""));
	}	
	
}
function managerTitle(flag,id){

	var hasTitle = $("#has_title").val();
	var unhasTitle = $("#unhas_title").val();
	
	if(flag=='add'){
		
		if(hasTitle == ''){
			showMessage("Please select title to delete","alert");
			return false;
		}
		
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/title/DetTitleProductCatalogAction.action',
	  		dataType:'json',
	  		data:"productCatalogId="+id+"&titleId="+hasTitle,
	  		beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
	  		success:function(msg){
	  			if(msg.pd=="error"){
					showMessage("It Can't be deleted","error");
		  		}else{	
	  				location.reload();	
		  		}
	  			
	  		},	
	  		error:function(){
				showMessage("System error","error");
			}
	  		
    	});
	}else{
		
		if(unhasTitle == ''){
			
			showMessage("Please select title to add","alert");
			return false;
		}
		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/title/AddTitleProductCatalogAction.action',
	 		dataType:'json',
  			data:"productCatalogId="+id+"&titleId="+unhasTitle,
	  		beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
	  		success:function(msg){
	  			location.reload();
	  		},
	  		error:function(){
				showMessage("System error","error");
			}
    	});
	}
	$("#unhas_title").val('');
	$("#has_title").val('');
	$.unblockUI();
}


</script>