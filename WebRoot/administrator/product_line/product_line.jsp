<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
String title_id = StringUtil.getString(request, "title_id");
DBRow[] titles;
DBRow [] productLines;
titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true, 0L, "", null, request);  //客户登录
productLines = proprietaryMgrZyj.findProductLinesByTitleId(true, 0L, "", title_id, 0, 0, null, request);
%>
<html>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Product Line</title>

<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../js/art/skins/simple.css"  />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="../buttons.css"  />
<style type="text/css">
	.aui_titleBar{  border: 1px #DDDDDD solid;}
	.aui_header{height: 40px;line-height: 40px;}
	.aui_title{
	  height: 40px;
	  width: 100%;
	  font-size: 1.2em !important;
	  margin-left: 20px;
	  font-weight: 700;
	  text-indent: 0;
	}
	.aui_close{
		color:#fff !important;
		text-decoration:none !important;
	}  
	
	.breadnav {
            padding:0 30px; height:25px;margin-bottom: 25px;
        }
        .breadcrumb{
          font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
          font-size: 14px;
          line-height: 1.42857143;
        }
        .breadcrumb {
          margin-top:0;
	  padding: 8px 15px;
          margin-bottom: 20px;
          list-style: none;
          background-color: #fff;
          border-radius: 4px;
        }

        .breadcrumb > li {
          display: inline-block;
        }

        .breadcrumb > .active {
          color: #777;
        }

        .breadcrumb > li + li:before {
            padding: 0 5px;
            color: #ccc;
            content: "/\00a0";
        }
        .breadcrumb a{
		      color: #337ab7;
                text-decoration: none;	
		}
</style>
 
<script language="JavaScript1.2">

$(function(){
	$(".chzn-select").chosen({no_results_text: "没有该选项:"});
});

function addProductLinePage(parentId){
	
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_line_add.html";
	$.artDialog.open(url , {title: 'Add Product Line',width:'500px',height:'350px', lock: true,opacity: 0.3,fixed: true});
}

function addProductLine(parentId){
	
	$.prompt(
	"<div id='title'>增加产品线</div><br />新建类别<br>产品线名称：<input name='name' type='text' id='name' style='width:300px;'><br>",
	{
	      submit: checkAddAssetsCategory,
		  callback: 
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/addProductLineAction.action";
						document.add_form.name.value = f.name;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function checkAddAssetsCategory(v,m,f){
	
	if (v=="y"){
		if(f.name == ""){
			alert("请选填写产品线名称");
			return false;
		}
		return true;
	}
}

function delProductLine(id,name){
	
	$.artDialog({
	    content: 'Are you sure you want to delete this product line?',
	    icon: 'question',
	    width: 250,
	    height: 100,
	    lock: true,
	    opacity: 0.3,
	    title:'Delete Product Line',
	    okVal: 'Confirm',
	    ok: function () {
			$("#productLineId").val(id);
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/delProductLine.action',
				data:$("#del_form").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "true"){
						showMessage("Product Line '"+name+"' Delete Successfully","success");
						setTimeout("refreshWindow()", 1000);
					}else{
						showMessage("Product Line '"+name+"' Can't Delete, It's Have Product","alert");
					}
				},
				error:function(){
					showMessage("System error","error");
				}
			})
	    },
	    cancelVal: 'Cancel',
	    cancel: function(){
	    	
		}
	});
}

function transferToCategory(productLineId,productLineName){
	
	$("#idByTransfer").val(productLineId);
	$("#nameByTransfer").val(productLineName);
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/transferToCategory.action',
		data:$("#transfer_form").serialize(),
		dataType:'json',
		type:'post',
		success:function(data){
			if(data && data.flag == "0"){
				transferProductCatalogPage($("#idByTransfer").val(), $("#nameByTransfer").val());
			}else{
				showMessage("Can not Transfer To Product Category! Current Product Line has Three Level Category","alert");
			}
		},
		error:function(){
			showMessage("System error","error");
		}
	})	
}

function transferProductCatalogPage(productLineId, productLineName){
	
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_category_transfer.html?productLineId="+productLineId+"&productLineName="+productLineName;
	$.artDialog.open(url , {title: 'Transfer To Product Category',width:'600px',height:'350px', lock: true,opacity: 0.3,fixed: true});
}

function cleanProductCatalog(pro_line_id,catalog_id){
	$.artDialog({
	    content: 'Are you sure to delete this category and its sub-categories move out from this product line?',
	    icon: 'question',
	    width: 250,
	    height: 100,
	    title:'Remove Product Category',
	    okVal: 'Confirm',
	    ok: function () {
	    	document.clean_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/cleanProductCatalog.action";
			document.clean_form.product_line_id.value = pro_line_id;
			document.clean_form.catalog_id.value = catalog_id;
			document.clean_form.submit();
	    },
	    cancelVal: 'Cancel',
	    cancel: function(){
	    	
		}
	});
}

function updateProductLine(id){
	
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/update_product_line.html?id="+id;
	$.artDialog.open(url , {title: 'Rename Product Line',width:'500px',height:'350px', lock: true,opacity: 0.3,fixed: true});
}

function updateProductCategory(id){
	$.prompt(
		"<div id='title'>Update Porduct Line</div><br />Product Line Name：<input name='name' type='text' id='name' style='width:300px;'><br>",
		{
			submit: checkAddAssetsCategory,
			loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/getDetailProductLineJSON.action",
							{id:id},//{name:"test",age:20},
							function callback(data)
							{
								$("#name").setSelectedValue(data.name);
							}
					);
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/modProductLineAction.action";
						document.mod_form.id.value=id;
						document.mod_form.name.value = f.name;		
						document.mod_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { Submit: "y", Cancel: "n" }
	});
}

function addAssetsCategory(productLineId,productLineName){
	
	$.artDialog.open("add_product_line.html?product_line_id="+productLineId, {title: "Manage Porduct Category for Product Line:["+productLineName+"]",width:'600px',height:'350px',fixed:true, lock: true,opacity: 0.3});
}

function closeWin(){
	
	$.artDialog.close();
}

function changeTitle(){
	
	$("#changeTitleForm").submit();
}

function refreshWindow(){
	
	window.location.reload();
}
</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
/*
* ESO 搜索
*/
.eso_search_parent {
	position: absolute;
	width: 0px;
	height: 0px;
	z-index: 1;
}

.eso_search_icon {
	position: absolute;
	left: 372px;
	top: 5px;
	width: 55px;
	height: 30px;
	z-index: 1;
	visibility: visible;
}

.eso_search_icon img {
	width: 26px;
	height: 26px;
	border: 0;
}

.eso_search_input {
	background: url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width: 408px;
	height: 36px;
	padding-top: 3px;
	padding-left: 3px;
	margin-bottom: 5px;
}

.eso_search_input input {
	background: url(../imgs/search_bg.jpg) repeat 0 0;
	width: 400px;
	height: 30px;
	font-weight: bold;
	border: 1px #bdbdbd solid;
	font-size: 17px;
	font-family: Arial;
	color: #333333;
}

.onMouseHover tr:hover {

	background: #E6F3C5;
	border-bottom: 1px #c5c5c5 dotted;
}

.onMouseHover .zebraTable {
	border-collapse: collapse;
}

.heightLight a.node,.heightLight{
	color: #ED3D14;
	font-weight: bolder;
}

.buttons-group {
  display: none;
  list-style: none;
  padding: 0;
  margin: 0;
  zoom: 1;
}

-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background: #f0f0f0;">
<!-- <div style="border-bottom:1px solid #CFCFCF; height:25px;padding-top:15px;margin-left: 30px;">
		<img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">
		<span style="font-size: 13px;font-weight: bold"> Setup » Product Line</span>
</div> 
<div class="breadnav">
<ol class="breadcrumb">
            <li><a href="#">Setup</a></li>
            <li class="active">Product Line</li>
        </ol> 
</div>-->

<form action="" id="del_form">
	<input type="hidden" name="id" id="productLineId">
</form>
<form action="" id="transfer_form">
	<input type="hidden" name="nameByTransfer" id="nameByTransfer" >
	<input type="hidden" name="idByTransfer" id="idByTransfer"> 
</form>
<form method="post" name="mod_form">
	<input type="hidden" name="name">
	<input type="hidden" name="id"> 
</form>
<form method="post" name="add_form">
	<input type="hidden" name="name">
	<input type="hidden" name="catalog_id">
	<input type="hidden" name="product_line_id">
	<input type="hidden" name="id"> 
</form>
<form action="post" name="clean_form">
	<input type="hidden" name="product_line_id"/>
	<input type="hidden" name="catalog_id"/>
</form>
<br/>
<div style="margin-left: 30px; margin-right: 30px;padding: 10px; box-shadow: 0 0 5px #939393; background: #fff; margin-bottom: 8px; height: 40px;">

	<div>
		<div class="eso_search_parent">
			<div class="eso_search_icon">
				<a> <img style="cursor: pointer;" id="eso_search"
					src="../imgs/query.png">
				</a>
			</div>
		</div>

		<div style="position: absolute; width: 0px; height: 0px; z-index: 1;left:425px;">
			<!-- <input style="position: relative; left: 425px; top: 8px;" name="Submit" type="button" class="theme-button-add"
				onClick="addProductLinePage(0)" value="Add Product Line"> -->
				
			<a style="position: relative; left: 30px; top: 2px;" name="Submit" class="buttons  primary big"
				onClick="addProductLinePage(0)"><i class="icon-plus"></i>&nbsp;Add Product Line</a>   
		</div>

		<div class="eso_search_input">
			<input id="search_key" name="search_key" type="text" class="ui-autocomplete-input" autocomplete="off" role="textbox"
				aria-autocomplete="list" aria-haspopup="true">
		</div>
	</div>
</div>

<div style="margin-top: 22px; margin-left: 30px; margin-right: 30px; padding: 10px; box-shadow: 0 0 5px #939393; background: #fff; margin-bottom: 8px;">
	<form name="listForm" method="post">
		
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
			<tr> 
				<th width="90%" class="left-title" colspan="3" style="height: 34px;">Product Line Name</th>
		        <th width="10%" class="right-title" style="height: 34px;vertical-align: center;text-align: center;">Contains Product Qty</th>
			</tr>			
			<tr>
				<td colspan="4" class="onMouseHover">
				<script type="text/javascript">
				d = new dTree('d');
				d.add('0','-1','Product Line</td><td align="center" valign="middle" width="15%">&nbsp;</td><td align="center" valign="middle" width="20%">&nbsp;</td><td align="center" valign="middle" width="45%"><span id="hiddendTreeRoot"></span></td></tr></table>','','');
							
				<%
					if(productLines.length != 0){
						for(int i=0;i<productLines.length;i++){
							
							int count = 0;
							
							//DBRow [] catalogs = productLineMgrTJH.getProductCatalogByProductLineId(productLines[i].get("id",0l));
							DBRow [] catalogs = proprietaryMgrZyj.findProductCatagorysByTitleId(true, 0L, productLines[i].get("id",""),"","", title_id, 0, 0, null, request);
							
							for(int j=0;j<catalogs.length;j++){
								if(catalogs[j].get("parentid",0l)==0){
									int num = productMgrZyj.getProductsByCategoryid(catalogs[j].get("id",0l),null);
									count += num;
								}
							}
							
				%>
					d.add('<%=productLines[i].getString("id")%>'
							,'0'
							,'<%=productLines[i].getString("name")%></td><td align="left" valign="middle" width="60%"><div class="buttons-group"><input name="Submit32" type="button" class="buttons" onClick="delProductLine(<%=productLines[i].getString("id")%>,\'<%=productLines[i].getString("name")%>\')" value="Del">&nbsp;&nbsp;<input name="Submit3" type="button" class="buttons" onClick="updateProductLine(<%=productLines[i].getString("id")%>)" value="Rename">&nbsp;&nbsp;<input name="Submit" type="button" class="buttons" onClick="addAssetsCategory(<%=productLines[i].getString("id")%>,\'<%=productLines[i].getString("name")%>\')" value="Related Category">&nbsp;&nbsp;<input class="buttons" name="Submit32" type="button" value="Transfer To Product Category" onClick="transferToCategory(<%=productLines[i].getString("id")%>,\'<%=productLines[i].getString("name")%>\')"/>&nbsp;&nbsp;<input type="button" class="buttons" value="Title/Customer" onClick="showTitle(<%=productLines[i].getString("id")%>,\'<%=productLines[i].getString("name")%>\')"/></div></td><td align="center" valign="middle" width="10%">&nbsp;<%=count%></td></tr></table>'
							,''
							,'<%=productLines[i].getString("name")%>');
				<%
							for(int j=0;j<catalogs.length;j++){
								if(catalogs[j].get("parentid",0l)==0){
									int num = productMgrZyj.getProductsByCategoryid(catalogs[j].get("id",0l),null);
									
				%>
					d.add('<%=catalogs[j].getString("id")+"-"+productLines[i].getString("id")%>'
							,'<%=productLines[i].getString("id")%>'
							,'<%=catalogs[j].getString("title")%></td><td align="left" valign="middle" width="60%"><div class="buttons-group"><%=catalogs[j].get("product_line_id",0l)==productLines[i].get("id",0l)?"<input class=\"buttons\" type=\"button\" value=\"Remove Relations\" onclick=\"cleanProductCatalog("+productLines[i].get("id",0l)+","+catalogs[j].get("id",0l)+")\"/>":"&nbsp;"%></div></td><td align="center" valign="middle" width="10%"><%=num%></td></tr></table>'
							,''
							,'<%=catalogs[j].getString("title")%>');
				<%				
								}
								else
								{
				%>
					d.add('<%=catalogs[j].getString("id")+"-"+productLines[i].getString("id")%>'
							,'<%=catalogs[j].getString("parentid")+"-"+productLines[i].getString("id")%>'
							,'<%=catalogs[j].getString("title")%></td><td align="left" valign="middle" width="60%"></td><td align="center" valign="middle" width="10%"><%=productMgrZyj.getProductsByCategoryid(catalogs[j].get("id",0l),null)%></td></tr></table>'
							,''
							,'<%=catalogs[j].getString("title")%>');
				<%
								}
							}
						}
					}
				%>
				document.write(d);
				
				//隐藏跟节点
				$("#hiddendTreeRoot").parent().parent().parent().parent().hide();
				
				$("#search_key").on("change",function(evt){
					
					var txt = $(evt.target).val().trim().toLowerCase();
					
					var array = d.aNodes;
					
					d.closeAll();
					
					for(var i in array){
						
						var name = array[i].title.toLowerCase();
						
						$("#id"+array[i]._ai).parent().removeClass("heightLight");
						
						if(txt!=='' && name.indexOf(txt)>=0){
							
							$("#id"+array[i]._ai).parent().addClass("heightLight");
							
							d.openTo(array[i].id,false);
						}
					}
				});
				
				$("#eso_search").on("click",function(evt){
					
					var txt = $("#search_key").val().trim().toLowerCase();
					
					var array = d.aNodes;
					
					d.closeAll();
					
					for(var i in array){
						
						var name = array[i].title.toLowerCase();
						
						$("#id"+array[i]._ai).parent().removeClass("heightLight");
						
						if(txt!=='' && name.indexOf(txt)>=0){
							
							$("#id"+array[i]._ai).parent().addClass("heightLight");
							
							d.openTo(array[i].id,false);
						}
					}
				});
				</script>
				<% if(productLines.length == 0){ %>
					
					<div style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Data.</div>
				<% }%>
				</td>
			</tr>
		</table>
	</form>
</div>
<br>
<form action="" name="download_form" id="download_form">
</form>
</body>
</html>
<script>

$(document).ready(function() {
	
	$('.onMouseHover .dTreeNode').bind('mouseover', function(evt) {
		
		$(evt.target).parents(".dTreeNode").find(".buttons-group").show();
	});
	
	$('.onMouseHover .dTreeNode').bind('mouseout', function(evt) {
		
		$(evt.target).parents(".dTreeNode").find(".buttons-group").hide();
	});
});

(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

function addTitle(id,name){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_line_add_title.html?product_line_id="+id+"&product_line_name="+name; 
	 $.artDialog.open(uri , {title: 'Manage Product Line Linked Title',width:'700px',height:'505px', lock: true,opacity: 0.3,fixed: true});
}

function down(){

	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var title_id=$('#title_id').val();
	var para='title_id='+title_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/product_line/AjaxDownLineAndTitleAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();       //遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}	
		}
	});
}
function upload(_target){
	var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"xls",
	     limitSize:2,
	     target:_target,
	     limitNum:1
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: 'Upload Files',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames){
	if($.trim(fileNames).length > 0 ){
	  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product_line/dialog_product_line_title_upload.html?fileName='+fileNames; 
	  $.artDialog.open(uri , {title: '上传TITLE',width:'600px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	}
}

function showTitle(id,name){
	
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_line_show_title.html?id="+id+"&name="+name;
	$.artDialog.open(url,{title:'Title/Customer',width:'600px',height:'450px',lock:true,opacity:0.3,fixed:true});
}

</script>
