<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

	
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="JavaScript1.2">

function addProductLine(parentId)
{
	$.prompt(
	
	"<div id='title'>增加产品线</div><br />新建类别<br>产品线名称：<input name='name' type='text' id='name' style='width:300px;'><br>",
	{
	      submit: checkAddAssetsCategory,
		  callback: 
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/addProductLineAction.action";
						document.add_form.name.value = f.name;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function checkAddAssetsCategory(v,m,f)
{
	if (v=="y")
	{
		 if(f.name == "")
		 {
				alert("请选填写产品线名称");
				return false;
		 }
		 return true;
	}

}

function delProductLine(id){
	if (confirm("确认操作吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/delProductLine.action";
		document.del_form.id.value = id;
		document.del_form.submit();
	}
}

function updateProductCategory(id){
	$.prompt(
		"<div id='title'>修改分类</div><br />产品线名称：<input name='name' type='text' id='name' style='width:300px;'><br>",
		{
			submit: checkAddAssetsCategory,
			loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/getDetailProductLineJSON.action",
							{id:id},//{name:"test",age:20},
							function callback(data)
							{
								$("#name").setSelectedValue(data.name);
							}
					);
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/modProductLineAction.action";
						document.mod_form.id.value=id;
						document.mod_form.name.value = f.name;		
						document.mod_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}
function addAssetsCategory(productLineId)
{
	tb_show('增加产品类别','add_product_line.html?product_line_id='+productLineId+'&TB_iframe=true&height=350&width=550',false);
}
function closeWin()
{
	tb_remove();
}
</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 基础数据管理 »   产品线定义</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="id" >

</form>
<form method="post" name="mod_form">
<input type="hidden" name="name">
<input type="hidden" name="id"> 
</form>

<form method="post" name="add_form">
<input type="hidden" name="name">
<input type="hidden" name="catalog_id">
<input type="hidden" name="product_line_id">
<input type="hidden" name="id"> 
</form>


<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
		      <tr> 
		        <th width="45%" class="left-title" colspan="2">产品线名称</th>
		        <th class="right-title" width="20%" style="vertical-align: center;text-align: center;">编号</th>
		        <th width="8%" class="right-title" style="vertical-align: center;text-align: center;">产品类别数量</th>
		        <th width="30%" class="right-title">&nbsp;</th>
		      </tr>
			
			<tr>
				<td colspan="5">
					<script type="text/javascript">
						d = new dTree('d');
						d.add('0','-1','产品线定义</td><td align="center" valign="middle" width="20%">&nbsp;</td><td align="center" valign="middle" width="8%">&nbsp;</td><td align="center" valign="middle" width="30%"><input name="Submit" type="button" class="long-button-add" onClick="addProductLine(0)" value=" 增加产品线"></td></tr></table>');
						<%
							DBRow [] productLines = productLineMgrTJH.getAllProductLine();
							if(productLines.length != 0)
							{
							for(int i=0;i<productLines.length;i++)
							{
								DBRow [] catalogs = productLineMgrTJH.getProductCatalogByProductLineId(productLines[i].get("id",0l));
								if(catalogs.length != 0)
								{
						%>
						d.add('<%=productLines[i].getString("id")%>','0','<%=productLines[i].getString("name")%></td><td align="center" valign="middle" width="20%"><%=productLines[i].getString("id")%></td><td align="center" valign="middle" width="8%">&nbsp;</td><td align="center" valign="middle" width="25%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delProductLine(<%=productLines[i].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateProductCategory(<%=productLines[i].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=productLines[i].getString("id")%>)" value=" 增加类别"></td></tr></table>','');
						<%
									for(int ii=0;ii<catalogs.length;ii++)
									{
										DBRow [] c1 = assetsCategoryMgr.getProductCatalogByParentId(catalogs[ii].get("id",0l));
										if(c1.length != 0)
										{
										
						%>
						d.add('<%=catalogs[ii].getString("id")+"A" %>','<%=productLines[i].getString("id")%>','<%=catalogs[ii].getString("title")%></td><td align="center" valign="middle" width="20%"><%=catalogs[ii].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(catalogs[ii].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%">&nbsp;</td></tr></table>','');
						<%
											for(int j=0;j<c1.length;j++)
											{
												DBRow [] c2 = assetsCategoryMgr.getProductCatalogByParentId(c1[j].get("id",0l));
												if(c2.length != 0)
												{
										
						%>
						d.add('<%=c1[j].getString("id")+"B" %>','<%=catalogs[ii].getString("id")+"A"%>','<%=c1[j].getString("title")%></td><td align="center" valign="middle" width="20%"><%=c1[j].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(c1[j].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%">&nbsp;</td></tr></table>','');
						<%
													for(int jj=0;jj<c2.length;jj++)
													{
														DBRow [] c3 = assetsCategoryMgr.getProductCatalogByParentId(c2[jj].get("id",0l));
														if(c3.length != 0)
														{
															for(int k=0;k<c3.length;k++)
															{
						%>
						d.add('<%=c3[k].getString("id")+"C"%>','<%=c2[jj].getString("id")+"D"%>','<%=c3[k].getString("title")%></td><td align="center" valign="middle" width="20%"><%=c3[k].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(c3[k].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%">&nbsp;</td></tr></table>','');
						<%
															}
						%>
						d.add('<%=c2[jj].getString("id")+"D"%>','<%=c1[j].getString("id")+"B"%>','<%=c2[jj].getString("title")%></td><td align="center" valign="middle" width="20%"><%=c2[jj].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(c2[jj].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%">&nbsp;</td></tr></table>','');
						<%
														}
														else
														{
						%>
						d.add('<%=c2[jj].getString("id")+"D"%>','<%=c1[j].getString("id")+"B"%>','<%=c2[jj].getString("title")%></td><td align="center" valign="middle" width="20%"><%=c2[jj].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(c2[jj].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%">&nbsp;</td></tr></table>','');
						<%		
														}
													}
												}
												else
												{
						%>
						d.add('<%=c1[j].getString("id")+"B" %>','<%=catalogs[ii].getString("id")+"A"%>','<%=c1[j].getString("title")%></td><td align="center" valign="middle" width="20%"><%=c1[j].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(c1[j].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%">&nbsp;</td></tr></table>','');
						<%
												}
											}
										}
										else
										{
						%>
						d.add('<%=catalogs[ii].getString("id")+"A" %>','<%=productLines[i].getString("id")%>','<%=catalogs[ii].getString("title")%></td><td align="center" valign="middle" width="20%"><%=catalogs[ii].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(catalogs[ii].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%">&nbsp;</td></tr></table>','');
						<%
										}
									}
								}
								else
								{
						%>
						d.add('<%=productLines[i].getString("id") %>','0','<%=productLines[i].getString("name")%></td><td align="center" width="20%">&nbsp;</td><td align="center" valign="middle" width="8%"><%=productLines[i].getString("id")%></td><td align="center" valign="middle" width="30%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delProductLine(<%=productLines[i].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateProductCategory(<%=productLines[i].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=productLines[i].getString("id")%>)" value=" 增加产品类别"></td></tr></table>','');
						<%
							    }
							}
						}
						%>
						document.write(d);
				
					</script>
				</td>
			</tr>
	</table>
</form>

<br>
<br>
<br>
</body>
</html>
