<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long id = StringUtil.getLong(request,"id");
String name = StringUtil.getString(request,"name");

DBRow[] lineTitles = proprietaryMgrZyj.findProprietaryByAdidAndProductLineId(0L, id, 0L, null, request);

%>
<html>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Product Line Show Title</title>
<style type="text/css">

.panel table{width:100%;}

.panel table tr:nth-child(2n){
	
	background:#eeeeee;
}

.panel {
	
	margin-bottom: 20px;
	background-color: #fff;
	border: 1px #CFCFCF solid;
	-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
	box-shadow: 0 1px 1px rgba(0,0,0,.05);
	margin: 7px;
}

.panel .panelTitle{
	
	background: #f1f1f1;
	height: 40px;
	line-height: 40px;
	padding:0 10px;
	border-bottom: 1px #CFCFCF solid;
}

.panel .panelTitle .title-left{
	
	float: left;
}

.panel .panelTitle .title-right{
	float: right;
}

.clear{
	clear: both;
}

.right-title{

	height:40px;
	background-color:#fff;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	
	<div class="panel">
		<div class="panelTitle">
			<div class="title-left">
				Product Line Link Title : <span style="  font-weight: bold;"><%=name %></span>
			</div>
		</div>
		<table>
			
			<%for(DBRow oneResult:lineTitles){%>
			<tr style="line-height: 26px; font-size:16px;">
				<td id="<%=oneResult.getString("title_id")%>" style="padding-left:3px;">
					<%=oneResult.getString("title_name")%>
				</td>
			</tr>
			<%} %>
		</table>
	</div>
</body>
</html>