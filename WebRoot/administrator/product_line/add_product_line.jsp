<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
long product_line_id = StringUtil.getLong(request,"product_line_id");
DBRow row = productLineMgrTJH.getProductLineById(product_line_id);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add Product Category</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet" />
<link rel="stylesheet" href="../js/select2-4.0.0-rc.2/dist/css/select2.css">

<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<script type="text/javascript" src="../js/select2-4.0.0-rc.2/dist/js/select2.min.js"></script>

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<style type="text/css">
.input-line {
	width: 200px;
	font-size: 12px;
}
.STYLE3 {
	font-size: 12px;
	color: #666666;
	width: 25%;
}
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="2" align="center" valign="top">
								
<table width="100%" border="0" cellspacing="5" cellpadding="2" style="padding-top: 12px;">
	<tr height="30">
		<td class="STYLE3" align="right" style="font-weight: bold;">Product Line Name :</td>
		<td align="left" valign="middle" style="width: 75%"><%=row.getString("name")%>
			<input name="name" value="<%=row.getString("name")%>" type="hidden" readonly="readonly" class="input-line" id="name">
		</td>
	</tr>
	
	<tr height="30">
		<td class="STYLE3" align="right" style="font-weight: bold;">Product Category :</td>
		<td align="left" valign="middle" style="  font-size: 14px;">
		
			<select id="categorymenu" style="width:300px;" multiple="multiple">
               	<%
					DBRow c1[] = proprietaryMgrZyj.findProductCatagoryParentsByTitleId(true, 0, "", "0", "", "", 0, 0, null, request);
					for (int i = 0; i < c1.length; i++) {
						
						String selected = "";
						
						if(product_line_id == c1[i].get("product_line_id", -1L)){
							
							selected = "selected";
						}
				%>
				<option value="<%=c1[i].get("id", 0l) %>" <%=selected %>><%=c1[i].getString("title") %></option>
				
				<%}%>
           	</select>
		</td>
	</tr>
</table>
								
			</td>
		</tr>
		<tr>
			<td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
			<td width="49%" align="right" class="win-bottom-line">
				<a name="Submit2" value="Submit" class="buttons primary big" onClick="save();">Submit</a> &nbsp;<hidden></hidden>
				<a name="Submit2" value="Cancel" class="buttons big" onClick="$.artDialog.close();">Cancel</a>
			</td>
		</tr>
	</table>
<script>

function save(){
	
	var categoryMenu = $("#categorymenu").val();
	
	parent.document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/addProductCatalogByLineId.action";
	parent.document.add_form.catalog_id.value = categoryMenu;
	parent.document.add_form.product_line_id.value =<%=product_line_id%>;
	parent.document.add_form.submit();
}

$(document).ready(function(){
	
	$("#categorymenu").select2({
	    placeholder: "Select Product Category"
	});
});
</script>
</body>
</html>
