<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>

<%@ include file="../../include.jsp"%> 
<%

DBRow wholesellDiscount[] = quoteMgr.getWholeSellDiscounts(null);
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单报价</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--

.zebraTable tr td a:link {
	color:#000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:visited {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:hover {
	color: #000000;
	text-decoration: underline;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:active {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
} 

form 
{
	padding:0px;
	margin:0px;
}

-->
</style>

<script>
function closeTBWin()
{
	tb_remove();
}

function trueOrder(qoid,ps_id,sc_id,ccid)
{

		document.record_form.qoid.value=qoid;
		document.record_form.ps_id.value=ps_id;			
		document.record_form.sc_id.value=sc_id;		
		document.record_form.ccid.value=ccid;				
		document.record_form.submit();
}

function create()
{
	tb_show('创建折扣优惠',"wholesell_discount_add.html?TB_iframe=true&height=500&width=850",false);
}

function mod(wsd_id)
{
	tb_show('修改折扣优惠',"wholesell_discount_mod.html?wsd_id="+wsd_id+"&TB_iframe=true&height=500&width=850",false);
}

function del(wsd_id,name)
{
	if ( confirm("确定删除 "+name+"？") )
	{
		document.del_wholesell_discount_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/delWholeSellDiscountAction.action";
		document.del_wholesell_discount_form.wsd_id.value = wsd_id;								
		document.del_wholesell_discount_form.submit();	
	}
}

function service_quote_discount_range()
{
	tb_show('设置客服优惠折扣幅度',"../quote/service_quote_discount_range.html?TB_iframe=true&height=500&width=450",false);
}

function changeWholeSellPriceZoom()
{
	document.newform.wholesell_price_zoom.value = $("#wholesell_price_zoom_input").val();
	document.newform.submit();
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable();">
<form action="" method="post" name="add_wholesell_discount_form" id="add_wholesell_discount_form">
<input type="hidden" name="name" >
<input type="hidden" name="discount_policy">
<input type="hidden" name="summary" >
<input type="hidden" name="imp_class" >
</form>

<form action="" method="post" name="mod_wholesell_discount_form" id="mod_wholesell_discount_form">
<input type="hidden" name="wsd_id" >
<input type="hidden" name="name" >
<input type="hidden" name="discount_policy">
<input type="hidden" name="summary" >
<input type="hidden" name="imp_class" >
</form>

<form action="" method="post" name="del_wholesell_discount_form" id="del_wholesell_discount_form">
<input type="hidden" name="wsd_id" >
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="newform">
 <input type="hidden" name="wholesell_price_zoom" >
</form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 订单管理 »   报价设置</td>
  </tr>
</table>


<br>
<table width="98%" height="38" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td align="left" valign="top"><input name="Submit3" type="button" class="long-long-button-add" value="创建折扣优惠" onClick="create()">
     &nbsp;&nbsp;&nbsp; 
      <input name="Submit4" type="button" class="long-button" value="设置客服折扣" onClick="service_quote_discount_range()">
	  &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;原价放大
	  <label>
	  <input name="wholesell_price_zoom_input" type="text" id="wholesell_price_zoom_input" style="width:50px;" value="<%=systemConfig.getStringConfigValue("wholesell_price_zoom")%>">
	  倍
	  <input name="Submit5" type="submit" class="short-short-button-ok" value="保存" onClick="changeWholeSellPriceZoom()">
	  </label></td>
  </tr>
</table>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
<thead>
<tr>
<th width="19%"  class="right-title"  style="text-align: left;" >名称</th>
    <th width="35%" align="left" valign="middle" class="right-title"  style="text-align: left;" >折扣优惠规则</th>
    <th width="33%" align="left" valign="middle" class="right-title"  style="text-align: left;" >描述</th>
    <th width="13%"   class="right-title" style="text-align: left;">&nbsp;</th>
</tr>
</thead>
<%
for (int i=0; i<wholesellDiscount.length; i++)
{
%>
  <tr>
 <td height="40" align="left" valign="middle" ><%=wholesellDiscount[i].getString("name")%></td>
    <td align="left" valign="middle" style="padding-top:10px;padding-bottom:10px;line-height:18px;color:#990000;font-size:13px;font-family: Arial">
	<div style="border-bottom:#CCCCCC 1px solid;color:#666666;margin-bottom:5px;padding-bottom:5px;font-weight:bold;width:96%"><img src="../imgs/action.gif" width="16" height="16"> <%=wholesellDiscount[i].getString("imp_class")%></div>
	<%=StringUtil.ascii2Html(wholesellDiscount[i].getString("discount_policy"))%></td>
    <td align="left" valign="middle"  ><%=StringUtil.ascii2Html(wholesellDiscount[i].getString("summary"))%></td>
    <td align="center" valign="middle"  >
      <input name="Submit" type="button" class="short-short-button-mod" value="修改" onClick="mod(<%=wholesellDiscount[i].getString("wsd_id")%>)">
	  <%
	  if (wholesellDiscount[i].getString("name").trim().equals("GeneralPolicy")==false)
	  {
	  %>
 &nbsp;&nbsp;&nbsp;
      <input name="Submit2" type="button" class="short-short-button-del" value="删除" onClick="del(<%=wholesellDiscount[i].getString("wsd_id")%>,'<%=wholesellDiscount[i].getString("name")%>')">
	  <%
	  }
	  %>	 </td>
  </tr>
<%
}
%>
</table>
<br>
</body>

</html>