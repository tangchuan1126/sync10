<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.PaymentMethodKey" %>
<jsp:useBean id="payTypeKeyZr" class="com.cwc.app.key.PaymentMethodKey"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>交易管理</title>

<!-- 基本css 和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>

<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<style type="text/css">
	div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:500px;height:30px;background:white;margin-top:200px;line-height:30px;font-size:14px;}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
 input.noborder{border:none;border-bottom:1px solid silver;color:silver;width:250px;}
 span.tradeId{color:#666666;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:20px;}
 .legend{font-size:12px;font-weight:bold;color:#000000;background:none;}
 span.fixAddressLeft{width:55px;display:block;float:left;text-align:right;border:0px solid red;}
 span.fixAddressRight{display:block;text-align:left;float:left;text-indent:5px;}
 p{clear:both;}
 
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:29px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<style type="text/css">
	body{text-align:left;}
</style>

<%
	String cmd = StringUtil.getString(request,"cmd");
	String search_key = StringUtil.getString(request,"search_key");
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(systemConfig.getIntConfigValue("order_page_count"));
	
	
	DBRow[] rows =  null ;
	
	
	
	String getAccountInfo = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/GetAccountInfoAction.action";
	TDate date = new TDate();
	date.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
	String	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
	String 	input_st_date =	 date.getStringYear()+"-"+date.getStringMonth()+"-"+date.getStringDay();
	long relation_id = StringUtil.getLong(request,"relation_id");
	
	String reasonCode = StringUtil.getString(request, "reason_code");
	String paymentStatus = StringUtil.getString(request, "payment_status");
	String caseType = StringUtil.getString(request,"case_type");
	int search_mode = StringUtil.getInt(request,"search_mode");
	
	int isCreateOrder  =  -1;
	int payMentChannel =  -1 ;
	
	if(cmd.length() < 1)
	{
		rows = orderMgrZr.getAllTrade(pc);
	}
	if(cmd.equals("filter")){
		rows = orderMgrZr.filterTrade(request,pc);
		String end = StringUtil.getString(request,"end");
		if(end.length() > 0){
			input_en_date = end;
		}
		String st = StringUtil.getString(request,"st");
		if(st.length() > 0 ){
			input_st_date = st;
		}
		 isCreateOrder = StringUtil.getInt(request,"is_create_order");
		 payMentChannel =   StringUtil.getInt(request,"payment_channel");	
	}
	if(cmd.equals("search"))
	{
		rows = tradeMgrZJ.searchTrades(search_key,search_mode,pc);
	}
%>
<script type="text/javascript">
var tip = []; 
$().ready(function() {
	
	addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/trade/getSearchTradeJSON.action",
			"merge_field","trade_id");
			
	 //搜索条支持回车
	$("#search_key").keydown(function(event){
		if (event.keyCode==13)
		{
			search();
		}
	});
});
	

 jQuery(function($){
	 // 初始化 sel
	  init();
 
 })
  
 
 function addNew(){  
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/add_trade.html";
	 $.artDialog.open(uri , {title: '新增交易',width:'1100px',height:'530px', lock: true,opacity: 0.3});
 }
 function ajax(){
	  
	 var s =  '<%= ConfigBean.getStringValue("systenFolder")%>';
 
 
	 

 
	 var url2 = "action/IamVisionari/notify.action";
	 var url1 = "action/IamVisionari/paypalNotify.action";
	var baowen_ = "?"+$("#baowen_").val();
	 
 
	 	$.ajax({
		 	url: s+url1+ baowen_,
			dataType:'json',	
		 	success:function(data){
		 		 
		  	}
		})
 }
 function inputIn(_this,value){
		var node = $(_this);
		if(node.val() === value){
			node.css("color","black").val("");
		}
	 }
 function outInput(_this,value){
	var node = $(_this);
	if($.trim(node.val()).length < 1) {
		node.val( value).css("color","silver");
	}
		
 }
 
 function init(){
		
		$("#st").date_input();
		$("#en").date_input();
		$("option[value='"+<%=isCreateOrder %>+"']",$("#is_create_order")).attr("selected",true);
		$("#search_key").val('<%=search_key%>');
	 
		if('<%= cmd %>' != "filter")
		{
			 $("#pageFormIsCreateOrder").val("-1");
			 $("#pageFormCmd").val("");
		}
		else
		{
			 if('<%= paymentStatus%>'.length > 0 ){
				$("#payment_status option[value='"+'<%= paymentStatus%>'+"']").attr("selected",true);
			 }
			 if('<%= caseType%>'.length > 0){
				 $("#case_type option[value='"+'<%= caseType%>'+"']").attr("selected",true);
			 }
			if('<%= reasonCode%>'){
				$("#reason_code option[value='"+'<%= reasonCode%>'+"']").attr("selected",true);	
			}
			$("#pageFormPaymentStatus").val('<%= paymentStatus%>');
			$("#pageFormReasonCode").val('<%= reasonCode%>');
			$("#pageFormCaseType").val('<%=caseType%>');
			
			
		};
		 
 }
 // 主要是form 表单的提交。然后把一些数据组织好receiver_email 收款人,business收款账号
	function filter(){
 		 
 	 	$("#filterForm").submit();
 	 	$(".cssDivschedule").css("display","block");
	}
 
 function search()
 {
 	var val = $("#search_key").val();
	
	if (val=="")
	{
		alert("你好像忘记填写关键词了？");
	}
	else
	{
		val = val.replace(/\"/g,'');
		var val_search = "\""+val.toLowerCase()+"\"";
		$("#search_key").val(val_search);
		document.search_form.search_key.value = val_search;
		document.search_form.search_mode.value = 1;
 		document.search_form.submit();
	}
 }
 

 
 function searchRightButton()
{
	var val = $("#search_key").val();
	
	if (val=="")
	{
		alert("你好像忘记填写关键词了？");
	}
	else
	{
		val = val.replace(/\"/g,'');
		$("#search_key").val(val);
		document.search_form.search_key.value = val;
		document.search_form.search_mode.value = 2;
 		document.search_form.submit();
	}
}

function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) {  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  

	document.getElementById("eso_search").onmouseup=function(oEvent) {  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
         	searchRightButton();
    	}  
	}  
}
 
 function relationOrder(trade_id)
 {
 	$.artDialog.open("relation_orders.html?oid="+trade_id, {title: "交易信息",width:'870px',height:'500px',fixed:true, lock: true,opacity: 0.3});
 }
 function goThisForm(value){
	 $("#filterFormP").val(value);
	 filter();
 }
</script>



</head>
<body  onload = "onLoadInitZebraTable()">
<!-- 遮盖层 -->
  <div class="cssDivschedule">
 		<div class="innerDiv">
 			请稍等.......
 		</div>
 </div>
 <form action="" name="search_form">
 	<input type="hidden" name="cmd" value="search"/>
 	<input type="hidden" name="search_key"/>
 	<input type="hidden" name="search_mode"/>
 </form>
 <div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
		 <ul>
			 <li><a href="#tool">常用工具</a></li>
			 <li><a href="#filter">过滤</a></li>		 
		 </ul>
		 <div id="tool" style="height:40px;overflow:hidden;">
			<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
				   <div id="easy_search_father">
				   <div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"></a></div>
				   </div>
					<table width="485" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td width="418">
							<div  class="search_shadow_bg">
								<input type="text" name="search_key" id="search_key" value="" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333">
							</div>
						</td>
					    <td width="67">
						 <a href="javascript:void(0)" id="search_help" style="padding:0px;color: #000000;text-decoration: none;"><img src="../imgs/help.gif" border="0" align="absmiddle" border="0"></a>
						</td>
					  </tr>
					</table>
					<script>eso_button_even();</script>		
				</td>
	              <td width="40%" ></td>
	              <td width="17%" align="right" valign="middle">
					<!--  
				 	 <a href="javascript:tb_show('手工创建订单','add.html?TB_iframe=true&height=500&width=850',false);"><img src="../imgs/create_order.jpg" border="0"></a>	
				  	-->	  
				  	<input type="button" value="新增" class="long-button-add" onclick="addNew()"/>
				  	<input type="button" value="ces"  class="long-button-add" onclick= "ajax();"/>
				 </td>
	            </tr>
	          </table>
		 </div>
		 <div id="filter" style="height:70px;">
		 <form action="" method="post" id="filterForm">
		 	<input type="hidden" name="cmd" value="filter" />
		 	 <input type="hidden" name="p" value="" id="filterFormP"/>
		 	 
		 		<table>
		 			<tr>
		 				<td>时间:</td>
		 				<td><input type="text" value="<%= input_st_date  %>" id="st" name="st" style="width:109px;"/> &nbsp;&nbsp;<input type="text" id="en" name="end" value="<%=input_en_date %>" style="width:100px;"/></td>
		 				<td>交易类型:</td>
		 				<td>
		 					<select name="is_create_order" id="is_create_order">
		 						<option value="-1">全部</option>
		 						<option value="1">建单类</option>
		 						<option value="0">关联类</option>
		 					</select>
		 				</td>
		 				<td>
		 					关联Order:
		 				</td>
		 				<td>
		 					<input type="text" name="relation_id" id="relation_id" style="width:100px;" value='<%=relation_id %>'/>
		 				</td>
		 				
		 			</tr>
		 			
		 		</table>
 			 	<table>
		 	 		<tr>
		 	 			 
		 	 			 <td>
		 	 			 	<select id="payment_status" name="payment_status">
		 	 			 		<option value="all">PayMent Status</option>
		 	 			 		<option value="Completed">Completed</option>
		 	 			 		<option value="Cancel">Cancel</option>
		 	 			 		<option value="Canceled_Reversal">Canceled_Reversal</option>
		 	 			 		<option value="Denied">Denied</option>
		 	 			 		<option value="Failed">Failed</option>
		 	 			 		<option value="Pending">Pending</option>
		 	 			 		<option value="Refunded">Refunded</option>
		 	 			 		<option value="Reversed">Reversed</option>
		 	 			 	</select>
		 	 			 </td>
		 	 		 	<td>
		 	 		 		&nbsp;
		 	 		 		<select id="case_type" name="case_type">
		 	 		 			<option value="all">Case Type</option>
		 	 		 			<option value="chargeback">Chargeback</option>
		 	 		 			<option value="complaint">Complaint</option>
		 	 		 			<option value="dispute">Dispute</option>
		 	 		 		</select>
		 	 		 	</td>
		 	 			 <td>
		 	 			 	&nbsp;
		 	 			 	 <select id="reason_code" name="reason_code">
		 	 			 		<option value="all">Reason Code</option>
		 	 			 		<option value="admin_fraud_reversal">Admin Fraud Reversal</option>
		 	 			 		<option value="admin_reversal">Admin Reversal</option>
		 	 			 		<option value="buyer_complaint">Buyer Complaint</option>
		 	 			 		<option value="chargeback">Chargeback</option>
		 	 			 		<option value="chargeback_settlement">Chargeback Settlement</option>
		 	 			 		<option value="duplicate">Duplicate</option>
		 	 			 		<option value="merchandise">Merchandise</option>
		 	 			 		
		 	 			 		<option value="non_receipt">Non Receipt</option>
		 	 			 		<option value="not_as_described">Not As Described </option>
		 	 			 		<option value="refund">Refund</option>
		 	 			 		<option value="special">Special</option>
		 	 			 		<option value="unauthorized">Unauthorized</option>
		 	 			 	</select>
		 	 			 </td>
		 	 			 <td>
		 					&nbsp;<a href="javascript:filter();"><img width="95" height="20" border="0" align="absmiddle" src="../imgs/adv_search.gif"></a>
		 				</td>
		 	 		</tr>
		 		</table>
					
					 
					
			 
		</form>
		 </div>
 </div>
 	<script type="text/javascript">
 	$("#tabs").tabs({
		cache: true,
		cookie: { expires: 30000 } ,
	});
 	</script>
  
 		     <textarea rows="" cols="" id="baowen_" style="width:500px;height:100px;"></textarea>     
  
		<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"  isNeed="true" class="zebraTable" style="margin-left:3px;margin-top:5px;">
			  <tr> 
			  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">交易信息</th>
			  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">收款人信息</th>
			  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">付款信息</th>
		        <th width="25%" style="vertical-align: center;text-align: center;" class="right-title">地址信息</th>
		      </tr>
		      <tbody>
		      <%
		      	if(null != rows && rows.length > 0){
		      			for(DBRow row : rows){
		      %> 		
		      
		      	<tr>
		      		<td>
		      			<!-- 交易信息 -->
		      			<!--  关联的交易可以改为green的框。如果是主交易那么是有个小人在上面的 -->
		      			
		      			<%
		      				boolean isCreate = row.get("is_create_order",0) > 0 ? true:false;
		      				String borderColor = isCreate?"#993300" : "#f60";
		      				
		      			%>
		      			
		      			<fieldset   class="set" style="border:2px solid <%= borderColor %>;">
		  					 <legend class="legend"> 
		  					 	<%= row.getString("mc_currency")  %>&nbsp;<%=row.get("mc_gross",0.0f) %>&nbsp;|&nbsp;<a href="javascript:relationOrder(<%=row.get("relation_id",0l)%>)"><%= row.getString("payment_status") %> </a>
		  					 	 <%
								 String ipAddress = row.getString("ip_address");
							 		if(ipAddress.length() > 0){
							 			String[] array = ipAddress.split("-");
							 			if(array.length >= 2){
							 				ipAddress = array[0];
							 			}else{
							 				ipAddress = "";
							 			}
							 		} 
							 		
								   if(row.get("payment_channel",-1) == (payTypeKeyZr.CC)){
	 							 		if(ipAddress.split("\\.").length == 4){
								 			out.print("<a href='http://www.123cha.com/ip/?q="+ipAddress+"' target='_blank'><img src='../imgs/visa.jpg' width='22' height='14' border='0' align='absmiddle'></a>");
								 		}else{
								 			out.print(payTypeKeyZr.getStatusById(row.get("payment_channel",-1)));
								 		}
								   }else if(row.get("payment_channel",-1) == (payTypeKeyZr.PP) && ipAddress.split("\\.").length == 4){
									   out.print("<a href='http://www.123cha.com/ip/?q="+ipAddress+"' target='_blank'>"+payTypeKeyZr.getStatusById(row.get("payment_channel",-1))+"</a>");
								   }else{
									   out.print(payTypeKeyZr.getStatusById(row.get("payment_channel",-1)));
								   }
							 %> 
		  					 
		  					  </legend>
		  					 <p style="text-indent:20px;margin-top:5px;">
		  					 	<span class="tradeId"><%= row.get("trade_id",0l) %></span><span class="tradeId">&nbsp;-&nbsp;</span><span class="tradeId"><%= row.get("relation_id",0l) %></span> 
		  					 	<% if(isCreate) {%>
		  					 <!-- 	<img width="16" height="16" border="0" src="../imgs/order_client.gif"> -->
		  					 		<span><img width="14" height="14" align="absmiddle" title="创建订单" src="../imgs/nav_text.jpg"></span>
		  					 	<%} %>
		  					 </p>
		  					 
		  					 
		  					 <p>
			  					 <span style="color:green;font-weight:bold;margin-left:10px;"> 
			  						 <%= row.getString("order_source") %>
			  					 </span>
			  					 <span style="color:#f60;font-weight:bold;">&nbsp;|&nbsp;<%=row.getString("payment_type")%></span>
		  					</p>
		  					<%
		  						if(row.getString("txn_id").length() > 0){
		  					%>
		  					<p>
		  						<span style="margin-left:26px;">Txn Id:</span>
		  						<span><%=row.getString("txn_id")%></span>
		  					</p>
		  					<%} %>
		  					<%if(!isCreate){ %>
			  		 			<p>
			  		 				<span>Parent Txn:</span>
			  		 				<span><%= row.getString("parent_txn_id") %></span>
			  		 			</p>
		  		 			<%} %>
		  					 
 							 <p style="clear:both;"><span style="display:block;float:left;"><img style="margin-left:20px;margin-top:2px;" src="../imgs/alarm-clock--arrow.png" title="创建时间"></span><span style="float:left;">&nbsp;&nbsp;<%=row.getString("post_date") %></span></p>
							
						</fieldset>
		      		</td>
		      		<td>
		      			<!-- 收款人信息 -->
		      			
						<p>
							<span>收款人:</span>
							<span><%=row.getString("receiver_email") %></span>
						</p>
						<p>
							<span>收款账号:</span>
							<span><%=row.getString("business") %></span>
						</p>
						<p>
							<span>对应平台收款Id:</span>
							<span><%=row.getString("receiver_id") %></span>
						</p>
					</td>
		      		<td>
		      		<%
		      		  	//根据client_id 去获取 客户名字等信息
		      		  	DBRow clientRow =  null ; //orderMgrZr.getClientInfoByEmail(row.getString("payer_email",""));
		      		%>
		      			<!-- 付款信息 -->
		      			<p>
		      				<span style="display:block;float:left;"><img width="14" height="14" border="0" src="../imgs/account.gif" title="客户">&nbsp;&nbsp;</span>
		      				<span style="display:block;float:left;"><%= row.getString("client_id") %></span>
		      			</p>
 		  			 
		      			<p>
		      				<span style="display:block;float:left;"><img width="14" height="14" border="0" src="../imgs/order_client.gif" title="Payer Email">&nbsp;&nbsp;</span>
		      				<span style="display:block;float:left;"><%= row.getString("payer_email") %></span>
		      			</p>
		      			<%
		      				StringBuffer clientInfo = new StringBuffer();
		      				if(clientRow != null){
		      					clientInfo.append(clientRow.getString("first_name")+"&nbsp;").append(clientRow.getString("last_name"))
		      					.append("&nbsp;-&nbsp;").append("<span style='color:green;font-weight:bold;'>"+clientRow.getString("payer_status")+"</span>");
		      				}
		      			%>
		      			 
		      			 <p style="font-weight:bold;">
		      			 	 <%= clientInfo.toString() %>
		      			 </p>
		      			  <% 
		      			  	//payment_status
		      			  	String payment_status = row.getString("payment_status");
		      			  	if(payment_status.length() > 0){
		      			  %>
		      			  	<p>PayMent Status : <%= payment_status %></p>
		      			  <%}%>
		      			  <%
		      			  	String reason_code = row.getString("reason_code");
		      			  	if(reason_code.length() > 0){
		      			  %>
		      			  	<p>Reason Code : <%= reason_code %></p>
		      			  <%} %>
		      			  <%
		      			  	String case_type = row.getString("case_type");
		      			  	if(case_type.length() > 0){
		      			  %>
		      			  	<p>Case Type : <%= case_type %></p>
		      			  <%} %>
		      			  <%
		      			  	String case_id = row.getString("case_id");
		      			  	if(case_id.length() > 0){
		      			  %>
		      			  	<p>Case Id : <%= case_id %></p>
		      			  <%} %>
		      		</td>
		      		<td style="padding-top:5px;padding-bottom:5px;">
		      			<!-- 地址信息 delivery -->
		      			<br />
		      			<p>
		      				<span class="fixAddressLeft alert-text">Name:</span>
		      				<span class="fixAddressRight"><%=row.getString("delivery_address_name") %></span>
		      			</p>
		      			<p>
		      				<span class="fixAddressLeft alert-text ">Street:</span>
		      				<span class="fixAddressRight"><%=row.getString("delivery_address_street") %></span>
		      			</p>
		      			<p>
		      				<span class="alert-text fixAddressLeft">City:</span>
		      				<span class="fixAddressRight"><%=row.getString("delivery_address_city") %></span>
		      			</p>
		      			<p>
		      				<span class="alert-text fixAddressLeft">State:</span>
		      				<span class="fixAddressRight"><%=row.getString("delivery_address_state") %></span>
		      			</p>
 						<p>
 							<span class="alert-text fixAddressLeft">Zip:</span>
 							<span class="fixAddressRight"><%=row.getString("delivery_address_zip") %></span>
 						</p>
 						<p>
 							<span class="alert-text fixAddressLeft">Country:</span>
 							<span class="fixAddressRight"><%=row.getString("delivery_address_country") %></span>
 						</p>
 						<p>
 							<span class="alert-text fixAddressLeft">Code:</span>
 							<span class="fixAddressRight"><%=row.getString("delivery_address_country_code") %></span>
 						</p>
 						<p>
 							<span class="alert-text fixAddressLeft">Phone:</span>
 							<span class="fixAddressRight"><%=row.getString("contact_phone") %></span>
 						</p>
 						<p>
 							<span class="alert-text fixAddressLeft">Status:</span>
 							<span class="fixAddressRight"><%=row.getString("delivery_address_status") %></span>
 						</p>
 						<br />
		      		</td>
		      	</tr>
		      	<% 
		      			}
		      	}else {
		      	%>
		      		<tr style="background:#E6F3C5;">
   						<td colspan="4" style="text-align:center;height:120px;">无数据</td>
   					</tr>
		      	<%	
		      	}
		      %>
		       
		      </tbody>
		</table>
<form action="" id="pageForm">
	<input type="hidden" name="p" id="pageCount"/>
	<input type="hidden" id="pageFormCmd" name="cmd" value="filter" />
	 
	<input type="hidden" id="pageFormIsCreateOrder" name="is_create_order" value='<%= StringUtil.getInt(request,"is_create_order") %>'/>
	<input type="hidden" id="pageFormCaseType" name="case_type" value='<%=caseType %>'>
	<input type="hidden" id="pageFormPaymentStatus" name="payment_status" value='<%=paymentStatus %>'>
	
	<input type="hidden" id="pageFormReasonCode" name="reason_code" value='<%=reasonCode %>'>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" style="margin-left:3px;">  
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:goThisForm(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:goThisForm(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:goThisForm(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:goThisForm(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
    </td>
  </tr>
</table>
	</body>
</html>