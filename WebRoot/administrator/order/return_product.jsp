<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String cmd = StringUtil.getString(request,"cmd");

long rp_id = StringUtil.getLong(request,"rp_id");

DBRow returnProduct = productReturnOrderMgrZJ.getDetailReturnProduct(rp_id);

long oid = returnProduct.get("oid",0l); 

DBRow[] orderItems = orderMgr.getPOrderItemsByOid(oid); 

cartReturn.clearCart(session);
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>退货</title>

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>



<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<style type="text/css">
<!--
td
{
	font-size:12px;
}
.STYLE1 {color: #FF0000}

form
{
	padding:0px;
	margin:0px;
}

-->
</style>

<script>
function showProductUnion(pc_id)
{
	if($("#"+pc_id).css("display")=="none")
	{
		$("#"+pc_id).css("display","");
	}
	else
	{
		$("#"+pc_id).css("display","none");
	}
}

function allNoCheck(id)
{
	$("[value*='"+id+"_']").attr("checked",false);
	if($("[value="+id+"]").attr("checked"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/Put2CartReturn.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:
			{
				type:"select",
				value:id
			},
			
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			
			error: function(){
				loadCart(1);
				$("#action_info").text("商品添加失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException")
				{
					alert("商品不存在，不能添加到订单！");
					$("#p_name").select();
				}
				else if(msg=="ProductDataErrorException")
				{
					alert("商品基础数据错误，不能添加到订单！请联系调度");
					$("#p_name").select();
				}
				else if (msg=="ok")
				{
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("1");
					$("#reason").val("");
					loadCartReturn();
				}
				else
				{
					loadCartReturn();
					alert(msg);
				}
			}
		});	
	}
	else
	{
		delProductFromCartSelect(id);
	}
}

function productNotCheck(obj)
{
	$("[value="+obj.name+"]").attr("checked",false);
	
	if($(obj).attr("checked"))
	{
		$("[id="+obj.id+"]").attr("checked",true);
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/Put2CartReturn.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:
			{
				type:"select",
				value:obj.value
			},
			
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			
			error: function(){
				loadCartReturn();
				$("#action_info").text("商品添加失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException")
				{
					alert("商品不存在，不能添加到订单！");
					$("#p_name").select();
				}
				else if(msg=="ProductDataErrorException")
				{
					alert("商品基础数据错误，不能添加到订单！请联系调度");
					$("#p_name").select();
				}
				else if (msg=="ok")
				{
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("1");
					$("#reason").val("");
					loadCartReturn();
				}
				else
				{
					loadCartReturn();
					alert(msg);
				}
			}
		});	
	}
	else
	{
		delProductFromCartSelect(obj.id);
	}
}


$().ready(function() {

	show_button();
	function log(event, data, formatted) {

		
	}
	
	$("#p_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 410,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];   
			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name+" ["+data[i].unit_name+"]["+data[i].p_code+"]",
    				value:data[i].p_name+" ["+data[i].unit_name+"]["+data[i].p_code+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	loadCartReturn();
	ajaxCartForm();
	
});


//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   //beforeSubmit:  // pre-submit callback    
			//	function(){
			//		loadCart();
			//	}	
		  // ,  
		   success:       // post-submit callback  
				function(){
					loadCartReturn();
					cartProQuanHasBeChange = false;
				}
		       
	};
	

   $('#cart_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}

function loadCartReturn()
{
	//alert(para);

	$.ajax({
		url: 'mini_cart_return.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		
		beforeSend:function(request){
			
		},
		
		error: function(){
			alert("加载购物车失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
		}
	});
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 

function checkForm()
{
	var theForm = document.search_union_form;
	
	if(theForm.p_name.value=="")
	{
		alert("商品名不能为空");
		return(false);
	}
	else if(theForm.quantity.value=="")
	{
		alert("数量不能为空");
		return(false);
	}
	else if(!isNum(theForm.quantity.value))
	{
		alert("请正确填写数量");
		return(false);
	}
	else
	{

				//先检查购物车中的商品在当前仓库是否已经建库
				var para = "type=add&p_name="+theForm.p_name.value+"&quantity="+theForm.quantity.value;
			
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/Put2CartReturn.action',
					type: 'post',
					dataType: 'html',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(){
						alert("添加商品失败！");
					},
					
					success: function(msg)
					{				
						if (msg=="ProductNotExistException")
						{
							alert("商品不存在！");
						}
						else if (msg=="ProductUnionSetCanBeProductException")
						{
							alert("套装不能作为定制商品");
						}
						else if (msg=="ProductNotCreateStorageException")
						{
							//
						}
						else if (msg=="ok")
						{
							//商品成功加入购物车后，要清空输入框
							theForm.p_name.value = "";
							theForm.quantity.value = "";
							loadCartReturn();
							return(false);
						}
						else
						{
							alert(msg);
						}

					}
				});	
				

	}
}


function delProductFromCart(pid,p_name,product_type)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/RemoveProductFromCartReturn.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"pid="+pid+"&product_type="+product_type,
					
			error: function(){
				alert("商品删除失败！");
			},
			
			success: function(msg){
				loadCartReturn();
			}
		});	
	}
}

function delProductFromCartSelect(pid)
{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/RemoveProductFromCartReturn.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"pid="+pid,
					
			error: function(){
				alert("商品删除失败！");
			},
			
			success: function(msg){
				loadCartReturn();
			}
		});	
}

var cartProQuanHasBeChange = false;
var cartIsEmpty;

function onChangeQuantity(obj)
{
	if ( !isNum(obj.value) )
	{
		alert("请正确填写数字");
		return(false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}

function post(oid,cmd)
{
	if ( $("input[@name=return_product_flag]:checked").val()==1&&$("#return_product_reason").val()==0 )
	{
		alert("请选择退货原因");
	}
	else if ($("input[@name=return_product_flag]:checked").val()==1&&$("#ps_id").val()==0)
	{
		alert("请选择退货仓库");
	}	
	else if ($("input[@name=return_product_flag]:checked").val()==1&&cartIsEmpty)
	{
		alert("请添加需要退货的商品");
	}
	else if (cartProQuanHasBeChange)
	{
		alert("请先保存修改数据");
	}
	else if ($('#note').val()=="")
	{
		alert("请填写备注");
	}
	else
	{
//		if ( cmd=="markNormalRefunding" )
//		{
//			parent.markNormalRefundingSubmit($('#note').val(),oid,$("input[@name=return_product_flag]:checked").val(),$("#return_product_reason").val(),$("#ps_id").val());
//		}
//		else if ( cmd=="markNormalWarrantying" )
//		{
			//parent.markNormalWarrantyingSubmit($('#note').val(),oid,$("input[@name=return_product_flag]:checked").val(),$("#return_product_reason").val(),$("#ps_id").val());
			$("#note1").val($('#note').val());
			$("#oid1").val(oid);
			$("#return_product_flag1").val($("input[@name=return_product_flag]:checked").val());
			$("#return_product_reason1").val($("#return_product_reason").val());
			$("#ps_id1").val($("#ps_id").val());
			$("#listForm").submit();
//		}
		
	}
}

function show_button()
{
	var flag = $("input[@name=return_product_flag]:checked").val();
	if(flag==0)
	{
		$("#return_product_row").hide();
		$("#okButton").val("确定");
	}
	if(flag==1)
	{
		$("#return_product_row").show();
		$("#okButton").val("下一步");
	}

}

</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form id="listForm" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markNormalWarrantying.action">
	<input id="note1" name="note" type="hidden"/>
	<input id="oid1" name="oid" type="hidden" />
	<input id="return_product_flag1" name="return_product_flag" type="hidden"/>
	<input id="return_product_reason1" name="return_product_reason" type="hidden" />
	<input id="ps_id1" name="ps_id" type="hidden"/>
	<input id="rp_id" name="rp_id" type="hidden" value="<%=rp_id%>"/>
</form>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><br>
    <table width="95%" border="0" align="center" cellpadding="7" cellspacing="0">
  <tr>
    <td width="11%" ><strong>是否退货</strong></td>
    <td width="89%" >
	
      <input name="return_product_flag" id="return_product_flag_0" type="radio" value="0" checked onClick="show_button()"> 
	  <label for="return_product_flag_0">
      不需要	  </label>

      <input type="radio" name="return_product_flag" id="return_product_flag_1" value="1" onClick="show_button()">
	  <label for="return_product_flag_1">
      <span class="STYLE1">需要</span>	   </label>	</td>
  </tr>
  
  <tr id="return_product_row" style="display:none">
    <td >&nbsp;</td>
    <td >
			 <fieldset style="border:1px #999999 solid;padding:10px;-webkit-border-radius:4px;-moz-border-radius:4px;width:580px">
<legend style="font-size:15px;font-weight:normal;color:#666666;">添加需要退货商品</legend>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
   <tr>
    <td width="50%" style="padding-bottom:10px;">
	 	<table border="0" cellpadding="0" cellspacing="0">
					  					<%
					  						for(int i = 0;i<orderItems.length;i++)
					  						{
					  							DBRow product = productMgr.getDetailProductByPcid(orderItems[i].get("pid",0l));
					  							
					  							DBRow[] productUnions = productMgr.getProductUnionsBySetPid(product.get("pc_id",0l));
					  					%>
					  					<tr>
					  						<td style="font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold">
					  							<% 
					  							if(productUnions.length>0)
					  							{
					  							%>
					  							<span onclick="showProductUnion(<%=product.get("pc_id",0l)%>)" style="text-decoration:underline;cursor:pointer;"><%=product.getString("p_name")%></span>
					  							<%
					  							}
					  							else
					  							{
					  							%>
					  							<%=product.getString("p_name")%>
					  							<%
					  							}
					  							%>
					  							
					  						</td>
					  						<td align="left">
					  							<%
					  								if(product.get("orignal_pc_id",0)==0)
					  								{
					  							%>
					  							<input type="checkbox" value="<%=product.get("pc_id",0l)%>" onclick="allNoCheck(this.value)"/>
					  							<%
					  								}
					  							%>
					  						</td>
					  					</tr>
					  					<% 
					  							if(productUnions.length>0)
					  							{
					  					%>
					  					<tr id="<%=product.get("pc_id",0l)%>" style="display:none">
					  						<td>
					  							<table border="0" cellpadding="0" cellspacing="0">
					  								<%
					  									for(int j = 0;j<productUnions.length;j++)
					  									{
					  										DBRow unionProduct = productMgr.getDetailProductByPcid(productUnions[j].get("pid",0l));
					  								%>
					  								<tr>
					  									<td><%=unionProduct.getString("p_name")%></td>
					  									<td><input onclick="productNotCheck(this)" name="<%=product.get("pc_id",0l)%>" id="<%=unionProduct.get("pc_id",0l)%>" type="checkbox" value="<%=product.get("pc_id",0l)+"_"+unionProduct.get("pc_id",0l)%>"/></td>
					  								</tr>
					  								<%
					  									}
					  								%>
					  							</table>
					  						</td>
					  					</tr>
					  					<%
					  							}
					  					%>
					  					
					  					<%
					  						}
					  					%>
					  				</table>
	</td>
    <td width="50%" valign="top" align="right" style="padding-bottom:10px;"><strong>退货仓库</strong>      <select name="ps_id" id="ps_id">
        <option value="0">请选择...</option>
        <%
	  DBRow treeRows[] = catalogMgr.getProductReturnStorageCatalogTree();
	String qx;
	
	for ( int i=0; i<treeRows.length; i++ )
	{
		if ( treeRows[i].get("parentid",0) != 0 )
		 {
			qx = "├ ";
		 }
		 else
		 {
			qx = "";
		 }
%>
          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==0?"selected":""%>> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>          </option>
          <%
}
%>
	
      </select></td>
   </tr>
  <tr>
    <td colspan="2" bgcolor="#eeeeee" style="padding-left:10px;padding-left:toppx;padding-bottom:5px;border-bottom:1px #999999 solid">
	<form name="search_union_form" method="post" action="">
	  商品
	    <input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:150px;"/> 
	    数量
        <input name="quantity" type="text" id="quantity"  style="color:#FF3300;width:40px;"/>
&nbsp;&nbsp;
      <input name="Submit" type="button" class="short-button" value="添加" onClick="checkForm()">	
	  </form>	  </td>
  </tr>
  <tr>
    <td colspan="2">
	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/BatchModifyProductsQuantityFromCarReturn.action" method="post" name="cart_form" id="cart_form">
	<div id="mini_cart_page" style="color:#CCCCCC">数据加载中……</div>
	</form>	</td>
  </tr>
</table>
             </fieldset>	
	</td>
  </tr>
  <tr>
    <td ><strong>备注信息</strong></td>
    <td > <textarea name="note" id="note" style="width:600px;height:150px;border:1px #999999 solid"></textarea> </td>
  </tr>
  <tr>
    <td colspan="2" style="font-size:15px;font-weight:bold">&nbsp;</td>
  </tr>
</table>
	
	</td></tr>
  <tr>
    <td align="right" valign="middle" class="win-bottom-line">
      <input name="Submit" id="okButton" type="button" class="normal-green" value="下一步" onClick="post(<%=oid%>,'<%=cmd%>')">
      <input name="cancel" type="button" class="normal-white" value="取消" onClick="$.artDialog.closeTBWin();">
    </td>
  </tr>
</table>

</body>

</html>