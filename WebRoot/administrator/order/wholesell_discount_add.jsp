<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>创建折扣优惠</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script>
function addWholesellDiscount()
{
	var f = document.add_wholesell_discount_form;
	
	if (f.name.value == "")
	 {
		alert("请填写名称");
	 }
	 else if (f.discount_policy.value == "")
	 {
		alert("请填写折扣优惠规则");
	 }
	 else if (f.summary.value == "")
	 {
		alert("请填写描述");
	 }
	 else if (f.imp_class.value == "")
	 {
		alert("请填写实现策略类");
	 }
	 else
	 {
	 	//验证重名
			var para = "name="+f.name.value+"&imp_class="+f.imp_class.value+"&wsd_id=0&discount_policy="+f.discount_policy.value;
		
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/validateWholesellPolicyNameAndRule.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("网络错误，请重试！");
				},
				
				success: function(msg){

					if (msg.indexOf("ErrorWholeSellDiscountRuleException")>=0)
					{
						alert("折扣优惠规则价格段不连续\n"+msg.split(":")[1]);
					}
					else if (msg=="DuplicateWholeSellPolicyNameException")
					{ 
						alert("名称已经存在，请修改");	
					}
					else
					{
						parent.document.add_wholesell_discount_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/addWholeSellDiscount.action";
						parent.document.add_wholesell_discount_form.discount_policy.value = f.discount_policy.value;		
						parent.document.add_wholesell_discount_form.name.value = f.name.value;		
						parent.document.add_wholesell_discount_form.summary.value = f.summary.value;		
						parent.document.add_wholesell_discount_form.imp_class.value = f.imp_class.value;										
						parent.document.add_wholesell_discount_form.submit();	
					}
				}
			});
	 }
}


</script>
<style type="text/css">
<!--
.STYLE1 {color: #999999}

-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_wholesell_discount_form" method="post" action="" >

<table width="97%" border="0" align="center" cellpadding="2" cellspacing="5">

  <tr>
    <td width="15%" align="right" valign="middle" class="text-line" >&nbsp;</td>
    <td width="85%" align="left" valign="middle" >&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle" class=" STYLE2" >名称</td>
    <td align="left" valign="middle" ><input name="name" type="text" id="name" style="width:400px;"></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >折扣优惠规则<br>
      <span class="STYLE1">(金额单位都为美元)</span></td>
    <td align="left" valign="middle" ><textarea name="discount_policy"  id="discount_policy" style="width:400px;height:200px;"></textarea></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >描述</td>
    <td align="left" valign="middle" ><textarea name="summary"  id="summary" style="width:400px;height:100px;"></textarea></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >实现策略类</td>
    <td align="left" valign="middle" ><input name="imp_class" type="text" id="imp_class"   style="width:400px;"></td>
  </tr>
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="创建" class="normal-green-long" onClick="addWholesellDiscount();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeTBWin();">
	</td>
  </tr>
</table> 
</body>
</html>
