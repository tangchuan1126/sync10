<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="com.cwc.fedex.FedexClient"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.cwc.shipping.ShippingInfoBean"%>
<%@ include file="../../../include.jsp"%>
<%@ page import="com.cwc.usps.UspsResponseErrorException"%>
<%
response.setHeader("Pragma","No-cache");   
response.setHeader("Cache-Control","no-cache");   
response.setDateHeader("Expires", 0);  

HashMap countryCodeHM = orderMgr.getAllCountryCodeHM();

long oid = StringUtil.getLong(request,"oid");
long handle = StringUtil.getLong(request,"handle");
long handle_status = StringUtil.getLong(request,"handle_status");
long invoice_id = StringUtil.getLong(request,"invoice_id");

long ccid = StringUtil.getLong(request,"ccid");
long sc_id = StringUtil.getLong(request,"sc_id");
if (sc_id==0)
{
	sc_id = Long.parseLong( request.getAttribute("sc_id").toString() );
}

//获得订单锁
DBRow orderLock = OrderLock.lockOrder(session,oid,OrderLock.PRINT);
if (orderLock!=null)
{
	out.println("<script>");
	out.println("alert('"+orderLock.getString("operator")+"正在"+OrderLock.getMsg(orderLock.getString("operate_type"))+"，请与其联系后再操作')");
	out.println("</script>");
}
		
String rePrint = "oid="+oid+"&handle_status="+handle_status+"&handle="+handle+"&ccid="+ccid+"&invoice_id="+invoice_id+"&sc_id="+sc_id;

DBRow order = orderMgr.getDetailPOrderByOid(oid);
DBRow detailInvoice; 

long pro_id = order.get("pro_id",0l);

double total_mc_gross = 0;//累计订单总支付金额
total_mc_gross =+ order.get("mc_gross",0d);

//订单状态不为：正常 、 有货，禁止打印
if (order.get("handle_status",0)!=HandStatusleKey.NORMAL||order.get("product_status",0)!=ProductStatusKey.IN_STORE)
{
	out.println("<span style='color:red;font-weight:bold'>订单状态不正确，禁止打印！</span>");
	return;
}

//兼容旧订单情况
if ( order.getString("inv_dog").equals("")&&order.getString("inv_rfe").equals("") )
{
	detailInvoice = orderMgr.getDetailInvoiceTemplate(invoice_id);
}
else
{
	detailInvoice = new DBRow();
	detailInvoice.add("dog",order.getString("inv_dog"));
	detailInvoice.add("rfe",order.getString("inv_rfe"));
	detailInvoice.add("uv",order.getString("inv_uv"));
	detailInvoice.add("tv",order.getString("inv_tv"));
	detailInvoice.add("di_id",order.get("inv_di_id",0l));
}


DBRow detailDelivererInfo;
if (detailInvoice.get("di_id",0l)==0)
{
	detailDelivererInfo = orderMgr.getRandomDelivererInfo(2);
}
else
{
	detailDelivererInfo = orderMgr.getDetailDelivererInfo(detailInvoice.get("di_id",0l));
}


 
if (order==null)
{ 
	out.println("缺少订单号");
	return;
}




//计算订单成本
boolean promptFlag = false;
float total_weight = 0;
float total_weight_org = 0;

try
{
	total_weight_org = orderMgr.compareOrderCost(oid,sc_id,ccid);
	//System.out.println(total_weight);
}
catch (LostMoneyException e)//亏本
{
	//暂时不检测成本
	//promptFlag = true;
	total_weight_org = orderMgr.getOrderWeightByOid(oid);
}
catch (CountryOutSizeException e)//国家不到达
{
	out.println("国家不能送达");
	return;
}
catch (WeightCrossException e)//国家不到达
{
	out.println("重量段设置交叉");
	return;
}
catch (WeightOutSizeException e)//国家不到达
{
	out.println("重量段超出范围");
	return;
}
catch (CurrencyNotExistException e)//汇率不存在
{
	out.println("汇率不存在");
	return;
}
total_weight = orderMgr.calculateWayBillWeight(sc_id,total_weight_org);

//计算运费
ShippingInfoBean shippingInfoBean = expressMgr.getShippingFee(sc_id, total_weight_org, ccid,pro_id);

String wayBillNo = order.getString("ems_id");
DBRow sonOrderDeliveryNotes[] = orderMgr.getSonOrderDeliveryNoteByParentOid(oid);


//计算子订单商品总数
DBRow sonOrderItems[] = orderMgr.getSonOrderItemsByParentOid(oid);
double totalSonOrderQuantity = 0;
for (int i=0; i<sonOrderItems.length; i++)
{
	totalSonOrderQuantity += sonOrderItems[i].get("quantity",0d);
}

//提交到USPS服务器
boolean uspsResponseError = false;
String uspsResponseErrorDescription = "";

if (wayBillNo.equals(""))
{
	FedexClient fedexClient = new FedexClient();
	
	String toTel = "0";
	if ( order.getString("tel").equals("")==false )
	{
	 toTel = order.getString("tel");
	}
	
	fedexClient.setFromName(detailDelivererInfo.getString("CompanyName"));
	fedexClient.setFromAddress(detailDelivererInfo.getString("AddressLine1")+detailDelivererInfo.getString("AddressLine2")+detailDelivererInfo.getString("AddressLine3"));
	fedexClient.setFromCity(detailDelivererInfo.getString("City"));
	fedexClient.setFromState(detailDelivererInfo.getString("DivisionCode"));
	fedexClient.setFromZip(detailDelivererInfo.getString("PostalCode"));
	fedexClient.setFromTel(detailDelivererInfo.getString("PhoneNumber"));
	
	fedexClient.setToName(order.getString("address_name"));
	fedexClient.setToAddress(order.getString("address_street"));
	fedexClient.setToCity(order.getString("address_city"));
	
	String address_state = order.getString("address_state");
	if(pro_id!=-1&&pro_id!=0)
	{
		DBRow province;
		if(order.getString("address_city").toLowerCase().equals("washington"))
		{
			province = new DBRow();
			province.add("p_code","DC");
		}
		else
		{
			province = productMgr.getDetailProvinceByProId(pro_id);
		}
		
		
		address_state = province.getString("p_code");
	}
	
	fedexClient.setToState(address_state);
	fedexClient.setToZip(order.getString("address_zip"));
	fedexClient.setToTel(toTel);
	fedexClient.setWeightLB(String.valueOf(MoneyUtil.round(total_weight*2.2, 1)));

	try
	{
		fedexClient.commit();
	}
	catch (UspsResponseErrorException e)//地址错误
	{
		uspsResponseError = true;
		uspsResponseErrorDescription = e.getMessage();
	}


	wayBillNo = fedexClient.getWaybillNo();  
}


String backurl = StringUtil.getCurrentURL(request);
if ( backurl.indexOf("oid")==-1 )
{
	backurl += "?oid="+oid+"&handle="+handle+"&handle_status="+handle_status+"&invoice_id="+invoice_id+"&ccid="+ccid+"&sc_id="+sc_id;
}



%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>打印FEDEX</title>

<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<link type="text/css" href="../../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />
<link href="../../comm.css" rel="stylesheet" type="text/css">

		
<script>
var invoiceWidth = 210;
var invoiceHeight = 297;
var productListWidth = 210;
var productListHeight = 220;
var waybillWidth = 94;
var waybillHeight = 145;


function printWayBill()
{
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"1744907 4 in x 6 in");
	visionariPrinter.SET_PRINT_COPIES(1);
	visionariPrinter.SET_PRINTER_INDEX("LabelPrinter");
	//visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("wayBillContainer").innerHTML);
	visionariPrinter.ADD_PRINT_IMAGE(0,0,352,528,"<img src='../FedexPrintLabel/shipping_label.<%=wayBillNo%>_0.png' border=0>");
	visionariPrinter.SET_PRINT_STYLEA(1,"Stretch",2);
		
	if ( visionariPrinter.PRINT() )
	{
		//更新运单号
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/PrintUpdateWaybillNumberAjax.action',
			type: 'post',
			dataType: 'html',
			timeout: 30000,
			cache:false,
			data:"oid=<%=oid%>&waybill_no=<%=wayBillNo%>",
								
			beforeSend:function(request){
			},
								
			error: function(){
				alert("打印运单失败，请重新打印");
			},
								
			success: function(msg){
				if (msg!=0)
				{
					alert("打印运单失败，请重新打印");
				}
			}
		});
			
			return(true);
		}		
}

function printInvoice()
{	
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
	visionariPrinter.SET_PRINT_COPIES(2);
	visionariPrinter.SET_PRINTER_INDEX("LaserPrinter");
	visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("invoiceContainer").innerHTML);
	return( visionariPrinter.PRINT() );
}

function printProductList()
{
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,-1,-1,"A4");
	visionariPrinter.SET_PRINT_COPIES(2);
	visionariPrinter.SET_PRINTER_INDEX("LaserPrinter");
	visionariPrinter.ADD_PRINT_TABLE(getPrintMmPx(10),0,getPrintMmPx(productListWidth),getPrintMmPx(productListHeight),document.getElementById("productListContainer").innerHTML);

	if ( visionariPrinter.PRINT() )
	{
		document.finish_print_form.submit();
		return( true );
	}
}

function mergePrint()
{
	if (printWayBill())
	{
		printProductList();
	}
}



function promptAlert()
{
	$.prompt("<div style='font-size:18px;font-weight:bold;color:red'>订单:"+document.print_form.oid.value+" 打印失败</div>请关闭窗口，重新打印！",
	{
		  submit:  function(){},
		  loaded: function(){},
		  callback: function(){},
		  overlayspeed:"fast",
		  buttons: { 取消: "n" }
	});
}

var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}  


function printVerifyAddress(oid)
{
	$.prompt(
	
	"<div id='title'>疑问地址提交客服处理[订单:"+oid+"]</div><br /><textarea name='verifyAddressNoteWinText' id='verifyAddressNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.verifyAddressNoteWinText == "")
						  {
							   alert("请填写备注");						
						  }
						  else
						  {
						  		var verify_address_note = "<%=uspsResponseErrorDescription%>\n"+f.verifyAddressNoteWinText;
								
								var para = "oid="+oid+"&verify_address_note="+verify_address_note;
							
								$.ajax({
									url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/printVerifyAddressAjax.action',
									type: 'post',
									dataType: 'html',
									timeout: 30000,
									cache:false,
									data:para,
									
									beforeSend:function(request){
									},
									
									error: function(){
										alert("提交出错，请重试！");
									},
									
									success: function(html){
										if (html=="error")
										{
											alert("提交出错，请重试！");
										}
										else
										{
											window.close();
										}
									}
								});
						  }
						  
						  return false;
					}
					
					 return true;
					
				}
		  ,
   		  loaded:
				function ()
				{

				}
		  ,

		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}
</script>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive();" style="background:#ffffff">

    <form name="finish_print_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/FinishPrintUpdateHandle.action">
		<input type="hidden" name="oid" value="<%=oid%>">
		<input type="hidden" name="handle" value="<%=handle%>">
		<input type="hidden" name="handle_status" value="<%=handle_status%>">
		<input type="hidden" name="printType" value="FEDEX">
		<input type="hidden" name="ems_id" value="<%=wayBillNo%>">
		<input type="hidden" name="sc_id" value="<%=sc_id%>">
		<input type="hidden" name="ccid" value="<%=ccid%>">
		<%
		rePrint = rePrint.replaceAll("&","|");
		%>
      <input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/finish_print.html?oid=<%=oid%>&rePrint=<%=StringUtil.getCurrentURI(request)+"?"+rePrint%>">
    </form> 
	
<%
if ( !uspsResponseError )
{
%>



 <table width="923" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td >
		
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="60%" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算重量: <%=MoneyUtil.round(total_weight, 2)%>Kg</td>
        <td width="40%" rowspan="2" align="right"><input name="Submit2" type="button" class="long-button-print" onClick="mergePrint()" value="联合打印"></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算运费: ￥<%=shippingInfoBean.getShippingFee()%></td>
        </tr>
    </table>
		
		
		</td>
      </tr>
      <tr>
        <td>


<div class="demo" >
<div id="tabs">
<ul>
		<li><a href="#waybill">打印运单</a></li>
		
		<li><a href="#productList">装箱单</a></li>	 
</ul>


<div id="waybill">
<input name="Submit" type="button" class="short-short-button-print" onClick="printWayBill()" value="打印">

<div id="wayBillContainer" > 
	<div id="print_label"><img src='../FedexPrintLabel/shipping_label.<%=wayBillNo%>_0.png' border=0 style="width:352px;"></div>
</div>

<script>
createContainer("wayBillContainer",15,50,waybillWidth,waybillHeight);
setComponentPos('print_label',0,0);
</script>
</div>



<div id="productList">


<div id="productList">
<input name="Submit" type="button" class="short-short-button-print" onClick="printProductList()" value="打印">

<div id="productListContainer">
<style>
td
{
	font-size:11px;
	font-family:Arial, Helvetica, sans-serif;
}
.barcode2
{
	font-family: C39HrP48DmTt;font-size:40px;
}
.barcode
{
	font-family: C39HrP24DlTt;font-size:32px;
}
</style>





<div id="productlist_invoice_table4">
	<%
		float weight = total_weight;
		
		double tmp = MoneyUtil.round(weight,3) * 1000;
		int tmp2 = (int)tmp;
		String tmp3 = String.valueOf(tmp2);
		String orgStr = "00000";
		orgStr = orgStr.substring(0,orgStr.length()-tmp3.length());
		
			%>	 <table width="660" border="0" cellpadding="0" cellspacing="0">
	 
	 <thead>
  <tr>
    <td colspan="4" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="52%">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div style="margin-left:10px;font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>
		&nbsp;<span style="font-size:12px;color:#999999">WWW.VVME.COM</span></div>	</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
 <tr>
            <td style="padding-left:10px;"><strong>WayBill NO</strong>: <%=wayBillNo%> </td>
          </tr>
          <tr>
            <td style="padding-left:10px;" >
                <img src="/barbecue/barcode?data=<%=wayBillNo%>&width=1&height=35&type=code39" />
            </td>
          </tr>
          </table></td>
        <td width="48%" align="right"><table width="224" height="30" border="0" cellpadding="2" cellspacing="0">
          <tr>
            <td align="right" ><strong>Order NO</strong>: <%=order.getString("oid")%></td>
          </tr>
          <tr>
            <td align="right" >
                 <img src="/barbecue/barcode?data=<%=order.getString("oid")%>&width=1&height=35&type=code39" />
            </td>
          </tr>
         
          <tr>
            <td align="right" ><strong>VVMELA</strong></td>
          </tr>
          <tr>
            <td align="right" >
            	 <img src="/barbecue/barcode?data=<%=orgStr.concat(tmp3)%>&width=1&height=35&type=code39" />
            </td>
          </tr>
        </table></td>
      </tr>
	  
	        <tr>
	          <td>&nbsp;</td>
	          <td align="right">&nbsp;</td>
	          </tr>
	        <tr>
    </table></td>
    </tr>
			   <%
if ( order.getString("delivery_note").equals("")==false||sonOrderDeliveryNotes.length>0 )
{
%>
  <tr>
    <td colspan="4" style="padding-bottom:5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="74%">
			<strong>* NOTE</strong>:
	<%=order.getString("delivery_note")%>
<%
if (sonOrderDeliveryNotes.length>0)
{
	for (int dii=0; dii<sonOrderDeliveryNotes.length; dii++)
	{
		if (sonOrderDeliveryNotes[dii].getString("delivery_note").equals(""))
		{
			continue;
		}
		out.println("<br>"+sonOrderDeliveryNotes[dii].getString("delivery_note"));
	}
}
%>
		</td>
        <td width="26%" align="right">&nbsp;</td>
      </tr>
    </table></td>
    </tr>
<%
}
%>
  <tr>
    <td colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;border:1px #000000 solid;background:#000000;color:#FFFFFF;">QTY.</td>
    <td bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;padding-left:5px;bold;border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;">ITEM</td>
    <td bgcolor="#FFFFFF" style="border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;font-family:Arial;padding-left:5px;font-weight:bold">PARTS</td>
  </tr>
  </thead>
  

  <%
DBRow orderItems[] = orderMgr.getPOrderItemsByOid(oid);
for (int zz=0; zz<orderItems.length; zz++)
{
%>
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;">	   			<%
					out.println(orderItems[zz].get("quantity",0f));
					%>	</td>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;border-left:0px;">
			   			<%
					if (orderItems[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(orderItems[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:12px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
				   	if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&orderItems[zz].get("lacking",0)==1)
					{
						out.println("<span style='color:#FF0000;'> [缺货]</span>");
					}
			
					
					out.println(orderItems[zz].getString("name"));
					if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(" ●");
					}
					

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(orderItems[zz].get("pid",0l));//获得定制套装原来标准套装的ID
						DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
						ArrayList oldUnionProsList = new ArrayList();//原来套装所包含的商品
						HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
						for (int kk=0; kk<oldUnionPros.length; kk++)
						{
							oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
							oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
						}
						
						DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( orderItems[zz].get("pid",0l) );
						for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
						{
							out.println("<tr>");
	
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
							
							//如果商品不存在于原来的套装之中，则加*提示
							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
							{
								out.println(" *");
							}
							
							out.println("</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
							
							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
							{
								out.println(" *");
							}

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(orderItems[zz].get("pid",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
						{
							out.println("<tr>");
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</tbody>
<%
}
%> 



 <%
	long preOid = 0;
	for (int zz=0; zz<sonOrderItems.length; zz++)
	{
		if (sonOrderItems[zz].get("oid",0l)!=preOid)
		{
			total_mc_gross += orderMgr.getDetailPOrderByOid(sonOrderItems[zz].get("oid",0l)).get("mc_gross",0d);
		}
		preOid = sonOrderItems[zz].get("oid",0l);
%>
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;border:1px #000000 solid;border-top:0px;">
					   <%
					out.println(sonOrderItems[zz].get("quantity",0f));
				   %>		</td>
    <td width="29" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:11px;border:1px #000000 solid;border-top:0px;;border-left:0px;">
		   			<%
					if (sonOrderItems[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(sonOrderItems[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:11px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
						if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&sonOrderItems[zz].get("lacking",0)==1)
						{
							out.println("<span style='color:#FF0000;'> [缺货]</span>");
						}
				
						out.println(sonOrderItems[zz].getString("name"));
						if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
						{
							out.println(" ●");
						}		

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
							out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
							DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(sonOrderItems[zz].get("pid",0l));//获得定制套装原来标准套装的ID
							DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
							ArrayList oldUnionProsList = new ArrayList();
							HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
							for (int kk=0; kk<oldUnionPros.length; kk++)
							{
								oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
								oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
							}
						
							DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( sonOrderItems[zz].get("pid",0l) );
							for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
							{
							out.println("<tr>");
	
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));

							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
							{
								out.println(" *");
							}
							
							out.println("</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));

							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
							{
								out.println(" *");
							}
							
							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(sonOrderItems[zz].get("pid",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
						{
							out.println("<tr>");
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</tbody>
<%
}
%> 






  <tfoot>  
  <tr>
    <td colspan="4" bgcolor="#FFFFFF">

      <table width="660" border="0" cellspacing="0" cellpadding="3">
       <tr>
         <td width="453" align="left" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">EW: 
		 <%=MoneyUtil.round(orderMgr.calculateWayBillWeight4ST(total_weight_org), 2)%>Kg		</td>
         <td width="454" align="right" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">
		  		<%=order.getString("order_source")%></td>
       </tr>
     </table>	</td>
  </tr>
  </tfoot>
</table>

	 
</div>
</div></div>
	<script>
createContainer("productListContainer",15,50,productListWidth,productListHeight);

setComponentPos('productlist_invoice_table4',8,10);

</script>
</div>





</div></div>



		
		</td>
      </tr>
    </table>








<%
}
else
{
%>


	
<div style="padding:10px;color:#FF0000;font-weight:bold;font-size:15px;"><%=uspsResponseErrorDescription%></div>
<br>

<div style="padding:10px;color:#FF0000;font-weight:bold;font-size:15px;">
  <input type="button" name="Submit" value="  地址问题提交客服处理  " onClick="printVerifyAddress(<%=oid%>);">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="Submit" value=" 关闭打印 " onClick="window.close();">
</div>

<%
}
%>




<script>
$("#tabs").tabs({	
	cookie: { expires: 30000 } 
});
</script>





</body>
</html>
