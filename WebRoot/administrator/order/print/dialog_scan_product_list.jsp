<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../../include.jsp"%> 
<html>
<head>
<title>扫描货物打印</title>

<!--  基本样式和javascript -->
<link type="text/css" href="../../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.autocomplete.css" />

<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<%
     //根据出库单id 查询运单
     long out_id=StringUtil.getLong(request,"out_id");
     long out_for_type=StringUtil.getLong(request,"out_for_type");  //出库单 运单状态
     String create_time=StringUtil.getString(request,"create_time");    //出库单创建时间
	 DBRow[] waybill=waybillMgrZwb.findWaybillByOutId(out_id);
%>
<script  type="text/jscript">
window.onload = function(){
	   document.getElementById('scanProduct').focus();
}

$(function(){
	document.onkeydown = function(e){
	    var ev = document.all ? window.event : e;
	    if(ev.keyCode==13) {
	    	var scanId=$('#scanProduct').val();
	    	$('#scanProduct').val('');
	    	$('#cue').val('');
	    	printProduct(scanId);
	    }
	}
}); 

//根据仓库id 查询快递id
function return_scId(ps_id){
	var scIds;
    $.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getExpressCompanysByPsIdJSON.action',
		dataType:'json',
		data:{ps_id:ps_id},
		success:function(data){
			scIds=data;
	 	}
	});
    return scIds;
}

//equick
function printEquick(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_equick_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}

//DHL
function printHtml(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_dhl_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}
//fedex-p
function printFedexP(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_fedexIE_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}
//fedex-g
function printFedexG(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_fedex_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}
//uspsp
function printUspsp(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_uspsp_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}
//uspsf
function printUspsf(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_uspsf_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}
//ems
function printEms(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_ems_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}

var rightItems=[];
var leftItems=[];
var printProductOp=0;

//识别货物打印
function printProduct(scanId){
	if(printProductOp==0){
		var pd=oneVone(scanId);
		if(!pd){
			var printProductJson=oneVn(scanId);             //查出右侧数据
			rightItems=setRightItems(printProductJson);     //生成右侧数组
			if(rightItems!=0){
				setLeftItems(scanId);	       	                //生成左侧数组
				judgment();                                     //比较右侧数组和左侧数组 并显示
			}
		}
	}else{
		setLeftItems(scanId);	       	                //生成左侧数组
		judgment();                                     //比较右侧数组和左侧数组 并显示
	}
}

//一票一件打印
function oneVone(scanId){ 
	var panduan=false;
	$.ajax({
	    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/AjaxPrintWaybillByProductAction.action?outId=<%=out_id%>&scanId='+scanId,
		dataType:'json',
		async:false,
		success:function(json){
			if(json.waybill_order_id!=undefined){
               var id=json.waybill_order_id;
               var waybill_id=json.waybill_order_id;
               var kuaidi=json.sc_id;
               //打印方法
               if(kuaidi==100143 || kuaidi==100002){                                      //DHL快递
					 //打印运单方法
					  var html=printHtml(waybill_id);
					  $('#printTitle').html(html);
					  var hh=loadDHLPage();
					  $("#prolist").html(hh);
					  var op=mergePrint();
					  if(op==true){
						   $('#print'+id).html('<span style="color:green">已打印</span>');
			               $('#cue').css("color","green");
			               $('#cue').html("找到运单");            
			               panduan=true;       
			               printProductOp=0; 
					   }
				 }else if(kuaidi==100144 || kuaidi==100003){                              //ems
					   var html=printEms(waybill_id);            
	                   $('#printTitle').html(html);
	                   var op=mergePrint();
					   if(op==true){
						   $('#print'+id).html('<span style="color:green">已打印</span>');
				           $('#cue').css("color","green");
				           $('#cue').html("找到运单");            
				           panduan=true;         
				           printProductOp=0; 
					   }
				 }else if(kuaidi==100024 || kuaidi==100021 || kuaidi==22 || kuaidi==23){   //fedex-p  fedex-e
	                   var html=printFedexP(waybill_id);            
	                   $('#printTitle').html(html);
	                   var op=mergePrint();
					   if(op==true){
						   $('#print'+id).html('<span style="color:green">已打印</span>');
			               $('#cue').css("color","green");
			               $('#cue').html("找到运单");            
			               panduan=true;         
			               printProductOp=0; 		           
					   }
				 }else if(kuaidi==100012){                                                 //equick
					   var html= printEquick(waybill_id);            
	                   $('#printTitle').html(html);
	                   var op=mergePrint();
					   if(op==true){
						   $('#print'+id).html('<span style="color:green">已打印</span>');
			               $('#cue').css("color","green");
			               $('#cue').html("找到运单");            
			               panduan=true;         
			               printProductOp=0; 
					   }
				 }else if(kuaidi==100008 || kuaidi==100025){                               //fedex-G
					   var html=printFedexG(waybill_id);
					   $('#printTitle').html(html);
					   var op=mergePrint();
					   if(op==true){
						   $('#print'+id).html('<span style="color:green">已打印</span>');
			               $('#cue').css("color","green");
			               $('#cue').html("找到运单");            
			               panduan=true;         
			               printProductOp=0; 
					   }
				 }else if(kuaidi==100006 || kuaidi==100028 || kuaidi==100014 || kuaidi==100029){     //usps-p
					   var html=printUspsp(waybill_id);
					   $('#printTitle').html(html);
					   var op=mergePrint();
					   if(op==true){
						   $('#print'+id).html('<span style="color:green">已打印</span>');
			               $('#cue').css("color","green");
			               $('#cue').html("找到运单");            
			               panduan=true;         
			               printProductOp=0; 
					   }
				 }else if(kuaidi==100009 || kuaidi==100027 || kuaidi==100013 || kuaidi==100030){      //usps-f
					   var html=printUspsf(waybill_id);
					   $('#printTitle').html(html);
					   var op=mergePrint();
					   if(op==true){      //打印成功
						   $('#print'+id).html('<span style="color:green">已打印</span>');
			               $('#cue').css("color","green");
			               $('#cue').html("找到运单");            
			               panduan=true;         
			               printProductOp=0; 
					   }
				 }
			}				
	    } 
    });
    return panduan;
}

//一票多件 商品的详细
function oneVn(scanId){
	var data;
	$.ajax({
	    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/AjaxPrintWaybillByProductMuchAction.action?outId=<%=out_id%>&scanId='+scanId,
		dataType:'json',
		async:false,
		success:function(json){
			data=json	
		}
    });
    if(data.length==0){
    	$('#cue').show();
		$('#cue').css("color","red");
		$('#cue').html("没有找到包含该货物的运单");
    }else{
    	$('#cue').show();
    	 $('#cue').css("color","green");
         $('#cue').html("找到包含多件商品的运单");
    	 printProductOp=1;
    }
   return data
}
//判断左侧数组和右侧数组并显示
function judgment(){
	var leftHtml='';
	var rightHtml='';
	var waybillId='';
	var sc_id='';
	//确定右侧
	for(var a=0;a<rightItems.length;a++){        //循环右侧
		//左侧 显示和右侧显示分开
		waybillId=rightItems[a].waybill_id;
		sc_id=rightItems[a].sc_id;
		rightHtml+='<div style="width:400px; float:left; clear:left;" align="center" id="'+rightItems[a].productId+'">&nbsp;'+
				   '</div>'+
				   '<div style="width:420px; float:left;" align="center" >'+
				  	  '<span style="color:blue;font-size:14px;" id="'+rightItems[a].productId+'name">'+rightItems[a].productName+'</span><span style="color:blue;font-size:14px;" id="'+rightItems[a].productId+'xx">&nbsp;x<span id="'+rightItems[a].productId+'count">'+rightItems[a].count+'</span></span>'+		
				   '</div>';
	}
	$('#oneVnWaybillId').html("运单 ID："+waybillId);
	$('#printRight').html(rightHtml);
    //确定左侧
     for(var a=0;a<rightItems.length;a++){       //循环右侧
   		 for(var b=0;b<leftItems.length;b++){    //循环左侧
			if(rightItems[a].productId==leftItems[b].id){
				leftHtml='<span style="color:green;font-size:14px;">'+rightItems[a].productName+'</span><span style="color:green;font-size:14px;">&nbsp;x'+leftItems[b].count+'</span>';
				$('#'+rightItems[a].productId).html(leftHtml);
				var num=$('#'+rightItems[a].productId+'count').html();
				num=num-leftItems[b].count;
				if(num <= 0){
					$('#'+rightItems[a].productId+'name').html('');
			    	$('#'+rightItems[a].productId+'xx').html('');
				}else{
					$('#'+rightItems[a].productId+'count').html(num);
				}
			}else{
               continue;
			}
		}
	}
	$('#oneVn').show();    //显示运单
	whetherOrNotPrint(sc_id,waybillId);   //判断是否可以打印
}

//判断左右数组是否 货物相等 数量相等 如果相等就可以打印了
function whetherOrNotPrint(kuaidi,waybill_id){
    if(rightItems.length==leftItems.length){    //判断两边不同类货物是否相等
		for(var a=0;a<rightItems.length;a++){
			var op=isCon(leftItems,rightItems[a].productId,rightItems[a].count);
			if(!op){    //如果op为假 不包含 不相等
				$('#cue').css("color","red");
				$('#cue').html('扫描货物与运单上数量不符');
				return false;
			}
		}
		//可以执行打印
		if(kuaidi==100143 || kuaidi==100002){                                   //DHL快递
			 //打印运单方法
			  var html=printHtml(waybill_id);
			  $('#printTitle').html(html);
			   var hh=loadDHLPage();
			   $("#prolist").html(hh);
			   var op=mergePrint();
			   if(op==true){
				   printProductOp=0;     //初始化扫描货物
				   $('#oneVn').hide(); //隐藏已达运单详细
				   $('#cue').hide();
				   rightItems=[];
				   leftItems=[];
			   }
		 }else if(kuaidi==100144 || kuaidi==100003){                              //ems
				var html=printEms(waybill_id);            
                $('#printTitle').html(html);
                var op=mergePrint();
				if(op==true){
				   printProductOp=0;     //初始化扫描货物
				   $('#oneVn').hide(); //隐藏已达运单详细
				   $('#cue').hide();
				   rightItems=[];
				   leftItems=[];
				}
		 }else if(kuaidi==100024 || kuaidi==100021 || kuaidi==22 || kuaidi==23){   //fedex-p  fedex-e
            var html=printFedexP(waybill_id);            
            $('#printTitle').html(html);
            var op=mergePrint();
			   if(op==true){
				   printProductOp=0;     //初始化扫描货物
				   $('#oneVn').hide(); //隐藏已达运单详细
				   $('#cue').hide();
				   rightItems=[];
				   leftItems=[];
			   }
		 }else if(kuaidi==100012){                                                 //equick
			 var html= printEquick(waybill_id);            
            $('#printTitle').html(html);
            var op=mergePrint();
			   if(op==true){
				   printProductOp=0;     //初始化扫描货物
				   $('#oneVn').hide(); //隐藏已达运单详细
				   rightItems=[];
				   leftItems=[];
			   }
		 }else if(kuaidi==100008 || kuaidi==100025){                               //fedex-G
			 var html=printFedexG(waybill_id);
			 $('#printTitle').html(html);
			 var op=mergePrint();
			   if(op==true){
				   printProductOp=0;     //初始化扫描货物
				   $('#oneVn').hide(); //隐藏已达运单详细
				   $('#cue').hide();
				   rightItems=[];
				   leftItems=[];
			   }
		 }else if(kuaidi==100006 || kuaidi==100028 || kuaidi==100014 || kuaidi==100029){     //usps-p
			 var html=printUspsp(waybill_id);
			 $('#printTitle').html(html);
			 var op=mergePrint();
			   if(op==true){
				   printProductOp=0;     //初始化扫描货物
				   $('#oneVn').hide(); //隐藏已达运单详细
				   $('#cue').hide();
				   rightItems=[];
				   leftItems=[];
			   }
		 }else if(kuaidi==100009 || kuaidi==100027 || kuaidi==100013 || kuaidi==100030){      //usps-f
			 var html=printUspsf(waybill_id);
			 $('#printTitle').html(html);
			 var op=mergePrint();
			   if(op==true){
				   printProductOp=0;     //初始化扫描货物
				   $('#oneVn').hide(); //隐藏已达运单详细
				   $('#cue').hide();
				   rightItems=[];
				   leftItems=[];
			   }
		 }
    }
}
function isCon(arr, val,count){
	for(var i=0; i<arr.length; i++){
		if(arr[i].id == val && arr[i].count == count)
			return true;
	}
	return false;
}

//创建右侧数组
function setRightItems(json){
	var waybill_items=[];
	for(var i=0;i<json.length;i++){
		var waybill={};
		waybill.count=json[i].quantity.substring(0,json[i].quantity.indexOf('.'));
		waybill.productId=json[i].pc_id;
		waybill.productName=json[i].p_name;	
		waybill.waybill_id=json[i].waybill_order_id;
		waybill.sc_id=json[i].sc_id;
		waybill_items.push(waybill);
	}
	return waybill_items;
}
//左侧数组
function setLeftItems(scanId){
	//判断右侧是否 有该货物
	var panduan=false;
	for(var a=0;a<rightItems.length;a++){
       if(rightItems[a].productId==scanId){    //说明这张运单包含这货
      	    panduan=true;
      	    break;     
       }
	}
	//判断左侧是否 存在 第一次不用判断
	if(panduan){
		if(leftItems.length==0){
			var waybill={};
			waybill.id=scanId;
			waybill.count=1;
			leftItems.push(waybill);
		}else{
			//判断左侧是否存在 该货  如果存在 数量加1 否则 直接加上该货物
			var op=false;
			for(var b=0;b<leftItems.length;b++){
				if(leftItems[b].id==scanId){    //说明这张运单包含这货
					leftItems[b].count++; 
			      	op=true;
			      	break;  
			    }
			}
			if(!op){
				var waybill={};
				waybill.id=scanId;
				waybill.count=1;
				leftItems.push(waybill);
			}
		}
	}else{
		$('#cue').css("color","red");
        $('#cue').html('该运单不包含该货物');
	}
	printProductOp=1;
}

//重新扫描
function newlyScan(){
	$('#oneVn').hide();
	$('#cue').hide();
	rightItems=[];
	leftItems=[];
	printProductOp=0;
	$('#scanProduct').focus();
}
</script>
</head>
<body>
	<fieldset class="set" style="border:2px solid #993300;">
<legend>
	<span style="font-size:20px; font-weight: bold;"> 出库单ID：<%=out_id %></span>
</legend>
	<div style="height:20px; line-height:30px; padding-left:5px;">
		<div style="width:580px; float:left;">
			出库单状态：
			<% String message=waybillMgrZwb.printStatusOutOrder(out_id); %>
			<%if(message.equals("都未打印")){ %>
			    <span style="color:red">都未打印</span>
			<%}else if(message.equals("部分打印")){ %>
				<span style="color:red">部分打印</span>
			<%}else if(message.equals("全部打印")){ %>
				<span style="color:green">全部打印</span>
			<%} %>
		</div>
	</div>
	<p style="text-align:left;text-indent:5px;font-weight:normal;color:#999; clear: left">
		创建时间：<%=create_time.substring(5,16)%>
	</p>
	<div align="center">
		<div id="outOrder" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:843px;">
			扫描货物码：<input type="text" value="" id="scanProduct" />&nbsp;&nbsp;<span id="cue"></span>
		</div>
	</div>
	<div style="display:none" id="oneVn">
		<fieldset style="border:2px solid red; width:825px;">
					<legend>
						<span style="font-size:20px;font-weight:bold;color:red" id="oneVnWaybillId"></span>
					</legend>
					<!--  
					<div>
						<div style="width:400px; float:left; clear:left;" >
						 	<span style="margin-left: 50px;">已扫描货物：</span>
				        </div>
				        <div style="width:400px; float:left;" >
				  	  		<span style="margin-left: 50px;">需扫描货物：</span>		
				   		</div>
					</div>
					-->
					<div id="printRight">
  	    	  	    </div>
  	    	  	    <div style="padding-top:40px" align="right" >
  	    	  	  		<span onclick="newlyScan()" style="cursor:pointer;color:red">重新扫描</span> 
  	    	  	    </div>
	   </fieldset>
	</div>	
	<table width="99%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" isNeed="true" isBottom="true" style="margin-top:5px;" >
		<thead>
			<tr>
				<th width="6%"  class="right-title"  style="text-align: center;">运单号</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">仓库</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">快递</th>
			    <th width="16%" class="right-title"  style="text-align: center;">创建时间</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">打印状态</th>
			</tr>
		</thead>
	    <tbody>
	     <% if(null != waybill && waybill.length > 0){  %>
		 <%   for(int i = 0; i<waybill.length; i++ ){ %>
	    	<tr id="av<%=waybill[i].get("waybill_id",0l) %>" >
	    		<td align="center" height="25px;">
	    			<span style="font-size:13px; font-weight: bold;">
	    				<%=waybill[i].get("waybill_id",0l)%>
	    			</span>
	    		</td>
	    		<td align="center"><%= catalogMgr.getDetailProductStorageCatalogById(waybill[i].get("ps_id",0l)).getString("title")%> 仓库</td>
	    		<td align="center">
	    			<% DBRow companys = waybillMgrZwb.findExpressDetailed(waybill[i].get("sc_id",0l));%>
	    			<%=companys.getString("name") %>
	    		</td>
	    		<td align="center">
	    			<%String showTime = waybill[i].getString("create_date"); %>
	    			<%= showTime.substring(5,16)%>
	    		</td>
	    		<td align="center" id="print<%=waybill[i].get("waybill_id",0l) %>">
	    		    <%long status=waybill[i].get("status",0l);%>
	    		    <%if(status==0){ %>
	    		    	<span style="color:red">未打印</span>
	    		    <%}else if(status==1){ %>
	    		   	    <span style="color:green">已打印</span>
	    		    <%}else if(status==2){ %>
	    		        <span style="color:green">已发货</span>
    		        <%}else{ %>
    		        	<span>取消</span>
    		        <%}%>
	    		</td>
	    	</tr>
	     <%}} %>
	    </tbody>
	</table>
</fieldset>	
<div id="printTitle" style="display:none">

</div>
</body>
</html>