<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.WaybillInternalOperKey"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.key.WaybillTrackeTypeKey"%>
<%@page import="com.cwc.app.key.SystemTrackingKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../../include.jsp"%> 

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.autocomplete.css" />

<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<%
     //根据出库单id 查询运单
     long out_id=StringUtil.getLong(request,"out_id");
     long out_for_type=StringUtil.getLong(request,"out_for_type");  //出库单 运单状态
     String create_time=StringUtil.getString(request,"create_time");    //出库单创建时间
	 DBRow[] waybill=waybillMgrZwb.findWaybillByOutId(out_id);
%>
<script>
$(function(){
	document.onkeydown = function(e){
	    var ev = document.all ? window.event : e;
	    if(ev.keyCode==13) {
			println();
	    }
	}
}); 
function println(){
	var  pr =$("input[name='pr']");
	for(var i=0; i<pr.length;i++ ){
		 if($(pr[i]).is(":focus")){
			 var value=$(pr[i]).val();
			 var waybill_id=$(pr[i]).attr("id");
			 var kuaidi=$(pr[i]).attr("kuaidi");
			 var cangku=$(pr[i]).attr("cangku");
			 if(value!="成功"){
				 if(kuaidi==100143 || kuaidi==100002){                                   //DHL快递
					 //打印运单方法
					  var html=printHtml(waybill_id);
					  $('#printTitle').html(html);
					  var hh=loadDHLPage();
					  $("#prolist").html(hh);
					  var op=mergePrint();       //联合打印
					  if(op==true){
						  $(pr[i]).val("成功");   //设置打印的值为成功
					  }
				 }else if(kuaidi==100144 || kuaidi==100003){                              //ems
						var html=printEms(waybill_id);            
	                    $('#printTitle').html(html);
	                    var op=mergePrint();       //联合打印
						   if(op==true){
							  $(pr[i]).val("成功");   //设置打印的值为成功
						   }
				 }else if(kuaidi==100024 || kuaidi==100021 || kuaidi==22 || kuaidi==23){   //fedex-p  fedex-e
                     var html=printFedexP(waybill_id);            
                     $('#printTitle').html(html);
                      var op=mergePrint();       //联合打印
					   if(op==true){
						  $(pr[i]).val("成功");   //设置打印的值为成功
					   }
				 }else if(kuaidi==100012){                                                 //equick
					 var html= printEquick(waybill_id);            
                     $('#printTitle').html(html);
                     var op=mergePrint();       //联合打印
					   if(op==true){
						  $(pr[i]).val("成功");   //设置打印的值为成功
					   }
				 }else if(kuaidi==100008 || kuaidi==100025){                               //fedex-G
					 var html=printFedexG(waybill_id);
					 $('#printTitle').html(html);
					  var op=mergePrint();       //联合打印
					   if(op==true){
						  $(pr[i]).val("成功");   //设置打印的值为成功
					   }
				 }else if(kuaidi==100006 || kuaidi==100028 || kuaidi==100014 || kuaidi==100029){     //usps-p   endicia
					 var html=printUspsp(waybill_id);
					 $('#printTitle').html(html);
					  var op=mergePrint();       //联合打印
					   if(op==true){
						  $(pr[i]).val("成功");   //设置打印的值为成功
					   }
				 }else if(kuaidi==100009 || kuaidi==100027 || kuaidi==100013 || kuaidi==100030){      //usps-f   endicia-first
					 var html=printUspsf(waybill_id);
					 $('#printTitle').html(html);
					 var op=mergePrint();       //联合打印
					   if(op==true){
						  $(pr[i]).val("成功");   //设置打印的值为成功
					   }
				 }
			 }else{
				 $('#av'+$(pr[i]).attr("id")).remove();
				 //下面的input获得焦点
				 var avid=$(pr[i+1]).attr("id");
				 $('#'+avid).focus();
				 break;
			 }
		 }
	}
 }

//根据仓库id 查询快递id
function return_scId(ps_id){
	var scIds;
    $.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getExpressCompanysByPsIdJSON.action',
		dataType:'json',
		data:{ps_id:ps_id},
		success:function(data){
			scIds=data;
	 	}
	});
    return scIds;
}

//equick
function printEquick(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_equick_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}

//DHL
function printHtml(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_dhl_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}
//fedex-p
function printFedexP(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_fedexIE_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}
//fedex-g
function printFedexG(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_fedex_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}
//uspsp
function printUspsp(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_uspsp_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}
//uspsf
function printUspsf(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_uspsf_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}
//ems
function printEms(waybill_id){
	var ht="";
	 $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/waybill_ems_print.html?waybill_id='+waybill_id,
			dataType:'html',
			async:false,
			success:function(html){
				ht=html;							
		    } 
	  });
	return ht;
}

//打印当前出库单
function printOut(out_id){
	visionariPrinter.PRINT_INIT("出库单");
	visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
	visionariPrinter.ADD_PRINT_TBURL("2cm","1cm","90%","90%","../../../waybill/out_list_print.html?out_id="+out_id);
	if(visionariPrinter.PREVIEW()>0){
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/outbound/closeOutbound.action',
				type: 'post',
				dataType: 'html',
				timeout: 30000,
				cache:false,
				data:"out_id="+out_id,				
			beforeSend:function(request){
			},					
			error: function(){
				alert("打印出库单失败，请重新打印");
			},					
			success: function(msg){
				if (msg!="ok"){
					alert("打印出库单失败，请重新打印");
				}
			}
		});
	}
}
//识别货物打印

function printProduct(out_id,out_for_type,create_time){
	//打开扫描货物打印
	var ur='<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/dialog_scan_product_list.html?out_id='+out_id+'&out_for_type='+out_for_type+'&create_time='+create_time;
	//$.artDialog.open(ur, {title: '扫描货物打印',width:'900px',height:'500px', lock: true});
	window.location.href=ur;
	$.artDialog.opener.changeTitle();
}

</script>
</head>
<body>
<fieldset class="set" style="border:2px solid #993300;">
<legend>
	<span style="font-size:20px; font-weight: bold;"> 拣货单ID：<%=out_id %></span>
</legend>
	<div style="height:20px; line-height:30px; padding-left:5px;">
		<div style="width:580px; float:left;">
			出库单状态：
			<% String message=waybillMgrZwb.printStatusOutOrder(out_id); %>
			<%if(message.equals("都未打印")){ %>
			    <span style="color:red">都未打印</span>
			<%}else if(message.equals("部分打印")){ %>
				<span style="color:red">部分打印</span>
			<%}else if(message.equals("全部打印")){ %>
				<span style="color:green">全部打印</span>
			<%} %>
		</div>
		<div style="width:250px; float:left">
			<input class="long-button-mod"   type="button"  value="打印拣货单"   onclick="printOut(<%=out_id %>)">
			<input class="long-long-button"  type="button"  value="扫描货物打印" onclick="printProduct(<%=out_id %>,<%=out_for_type%>,'<%=create_time%>')">
		</div>
	</div>
	<p style="text-align:left;text-indent:5px;font-weight:normal;color:#999; clear: left">
		创建时间：<%=create_time.substring(5,16)%>
	</p>
	<table width="99%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" isNeed="true" isBottom="true" style="margin-top:5px;" >
		<thead>
			<tr>
				<th width="6%"  class="right-title"  style="text-align: center;">运单号</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">仓库</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">快递</th>
			    <th width="16%" class="right-title"  style="text-align: center;">创建时间</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">打印状态</th>
			    <th width="10%" class="right-title"  style="text-align: center;">打印操作</th>
			</tr>
		</thead>
	    <tbody>
	     <% if(null != waybill && waybill.length > 0){  %>
		 <%   for(int i = 0; i<waybill.length; i++ ){ %>
	    	<tr id="av<%=waybill[i].get("waybill_id",0l) %>" >
	    		<td align="center" height="25px;">
	    			<span style="font-size:13px; font-weight: bold;">
	    				<%=waybill[i].get("waybill_id",0l)%>
	    			</span>
	    		</td>
	    		<td align="center"><%= catalogMgr.getDetailProductStorageCatalogById(waybill[i].get("ps_id",0l)).getString("title")%> 仓库</td>
	    		<td align="center">
	    			<% DBRow companys = waybillMgrZwb.findExpressDetailed(waybill[i].get("sc_id",0l));%>
	    			<%=companys.getString("name") %>
	    		</td>
	    		<td align="center">
	    			<%String showTime = waybill[i].getString("create_date"); %>
	    			<%= showTime.substring(5,16)%>
	    		</td>
	    		<td align="center">
	    		    <%long status=waybill[i].get("status",0l);%>
	    		    <%if(status==0){ %>
	    		    	<span style="color:red">未打印</span>
	    		    <%}else if(status==1){ %>
	    		   	    <span style="color:green">已打印</span>
	    		    <%}else if(status==2){ %>
	    		        <span style="color:green">已发货</span>
    		        <%}else{ %>
    		        	 <span>取消</span>
    		        <%}%>
	    		</td>
	    		<td align="center">
	    			<input id="<%=waybill[i].get("waybill_id",0l) %>" cangku="<%=waybill[i].get("ps_id",0l)  %>" kuaidi="<%=waybill[i].get("sc_id",0l)  %>" readonly="true" type="text" value="打印" size="3" name="pr"/>
	    		</td>
	    	</tr>
	     <%}} %>
	    </tbody>
	</table>
</fieldset>	
<div id="printTitle" style="display:none">

</div>
</body>
</html>