<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@ include file="../../../include.jsp"%>
<%
	long waybill_id = StringUtil.getLong(request,"waybill_id");
DBRow wayBill = wayBillMgrZJ.getDetailInfoWayBillById(waybill_id);

String rePrint = StringUtil.deCodeBackURL(StringUtil.getString(request,"rePrint"));
rePrint = StringUtil.replaceString(rePrint,"|","&");
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adid = adminLoggerBean.getAdid(); 
boolean printSuccessful = false;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>完成打印</title>
<style type="text/css">
<!--
.STYLE3 {color: #66AA00}
td
{
	font-size:12px;
}
-->
</style>
<%
	String updateWayBillStatusAction  = ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/UpdateWayBillStatusAction.action";
%>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript">
	function refreshParent(){
		$.artDialog.close();
		$.artDialog.opener.closeWindowAndRefresh && $.artDialog.opener.closeWindowAndRefresh();
 	}
 	function updateWayBillToPrint(){
 	 	$.ajax({
 	 	 	url:'<%= updateWayBillStatusAction %>',
 	 	 	dataType:'text',
 	 	 	data:jQuery.param({waybill_id:'<%= waybill_id%>',status:'1',adid:'<%= adid%>'}),
 	 	 	success:function(data){
 	 	 	 	if(data == "success"){
 	 	 			window.location.reload();
 	 	 	 	}else{alert("程序错误,请稍后再试");}
 	 	 	}
 	 	})
 	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<br>
<br>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="middle" style="font-family:'黑体';font-size:25px;">内部运单：<%=waybill_id%>&nbsp;&nbsp; 快递：<%=expressMgr.getDetailCompany(wayBill.get("sc_id",0l)).getString("name")%></td>
  </tr>
  <tr>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle">
<%
	 		 String[] array = {"待打印","已打印","已发货","取消","拆分中"};
	 		 String wayBillState =  array[wayBill.get("status", 0)];
%>
	 	</td>
  </tr>
  <tr> 
    <td align="center" valign="middle"><br>
    <span style="font-size:14px;">当前运单状态: <span style="color:#f60;font-weight:bold;"><%= wayBillState %></span></span>
    <br>
    <br></td>
  </tr>
  <tr>
    <td align="center" valign="middle">
    	<input type="button" value="更新到已打印" onclick="updateWayBillToPrint()"/>
    	<input type="button" value="关闭" onclick="refreshParent();"/>
    </td>
  </tr>
</table>
</body>
</html>
 