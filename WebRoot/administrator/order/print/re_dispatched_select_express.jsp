<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.SystemConfig"%>
<%@page import="com.cwc.app.util.Config"%>
<%@page import="com.cwc.app.key.WayBillFromKey"%>
<%@ include file="../../../include.jsp"%>
<%@ page import="com.cwc.app.order.print.ExpressPrintBean"%>
<%
// 将request中的DetailProduct 放入session 
// 将request中的weight 放入session 


cartWaybill.clearCart(session);
session.putValue("weight",request.getAttribute("weight"));
session.putValue(Config.wayBillSession,(ArrayList)request.getAttribute("detailproduct"));
session.putValue("waybillid",request.getAttribute("waybillid"));
session.putValue("ids",request.getAttribute("ids"));
session.putValue("numbers",request.getAttribute("numbers"));
ExpressPrintBean expressPrintBean[] = (ExpressPrintBean[])request.getAttribute("expressPrintBean");
//long ps_id = Long.parseLong(request.getAttribute("ps_id").toString());
DBRow wayBill = (DBRow) request.getAttribute("wayBillOrder");
//排序，找出价格最低的快递
long minPriceScId = 0;
double minPrice = 0; 

ExpressPrintBean noSelectExpressPrintBean[] = (ExpressPrintBean[])request.getAttribute("noSelectExpressPrintBean");

for (int i=0; i<expressPrintBean.length; i++)
{
	if(expressPrintBean[i].getShippingFee()>0)
	{
		if (i==0||!(minPrice>0))
		{
				minPrice = expressPrintBean[i].getShippingFee();
				minPriceScId = expressPrintBean[i].getScID();
				continue;
		}
		
		if (expressPrintBean[i].getShippingFee()<minPrice)
		{
			minPrice = expressPrintBean[i].getShippingFee();
			minPriceScId = expressPrintBean[i].getScID();
		}
	}
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>请选择打印快递</title>
<style>
*{font-family: Arial,Helvetica,sans-serif;padding:0px;margin:0px;font-size:12px;}
.print-button
{
	width:130px;
	height:40px;
	font-size:20px;
	font-weight:bold;
	font-family:Arial, Helvetica, sans-serif;
	border:2px #999999 solid;
	background:#f8f8f8;
	color:#666666;
	float:left;
	margin-right:10px;
	cursor: pointer;
}

form
{
	padding:0px;
	margin:0px;
}

.warning
{
	border-bottom:1px #cccccc solid;background:#FFFFCC;color:#666666;font-size:15px;font-weight:bold;
}

.error
{
	border-bottom:1px #cccccc solid;background:#CC0000;color:#ffffff;font-size:15px;font-weight:bold;
}
  div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
	div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:400px;height:30px;background:white;margin-top:40px;line-height:30px;font-size:14px;}
	.rad{ border-bottom-right-radius: 4px;border-bottom-left-radius: 4px; border-top-right-radius: 4px; border-top-left-radius: 4px;}
 	div.fee{font-size:12px;cursor:pointer;display:block;height:35px;line-height:35px;text-align:center;font-weight:bold;border:1px solid silver;width:140px;float:left;margin-left:5px;margin-top:2px;background:#f8f8f8;}
	div.fee:hover{background:white;}
	div.fee:visited{background:none;}
	div.fee.feeon{background:#f60;color:white;}
	
	div.noselect{font-size:12px;cursor:pointer;display:block;height:35px;line-height:35px;text-align:center;font-weight:bold;border:1px solid silver;width:140px;float:left;margin-left:5px;margin-top:2px;background:#FF33CC;}
	div.noselect:hover{background:white;}
	div.noselect:visited{background:none;}
	div.noselect.feeon{background:#f60;color:white;}
</style>
<script type="text/javascript" src="../../administrator/js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../../administrator/js/select.js"></script>
<script type="text/javascript" src="../../administrator/js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../../administrator/js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

 <script type="text/javascript">
 jQuery(function($){
	 var minDiv = $( "#div_"+'<%= minPriceScId %>');
 
	 minDiv.addClass("feeon");
})
 function isNum(keyW)
{
	var reg=  /^([1-9]|[1-9][0-9])$/;
	return( reg.test(keyW) );
 } 
 
function printItAjax(sc_id,printPage,useType,shipping_cost,_this,event)
{
	// 让选中的 高亮
	var div = $(_this);
	var divs = $(".rad").removeClass("feeon");
	div.addClass("feeon");
	
	if(event.button ==2)//右键
	{
		printMoreAjax(sc_id,printPage,useType,shipping_cost);
	}
	else//认为是左键
	{
		printOneAjax(sc_id,printPage,useType,shipping_cost);
	}
}

function isIntNum(keyW)
{
	var reg=  /^([1-9])$/;
	return( reg.test(keyW) );
 } 

function printOneAjax(sc_id,printPage,useType,shipping_cost)//一票一件
{
		if(!parseFloat(shipping_cost)>0)
		{
			$.prompt(
			"<div id='title'>输入运费</div><br>&nbsp;&nbsp;所用运费 &nbsp;&nbsp;&nbsp;<input name='proShippingCost' type='text' id='proShippingCost' value='0.00' style='width:200px;'>格式：0.00",
			{
			      submit: 
						function (v,m,f)
						{
							if (v=="y")
							{
								if (!isNum(f.proShippingCost))
								{
									alert("请正确输入运费");
									return(false);
								}
							}
						}
				  ,
		   		  loaded:
				  
						function()
						{
							
						}
				  
				  ,
				  callback: 
						function (v,m,f)
						{
							if (v=="y")
							{
								printAjax(sc_id,printPage,useType,f.proShippingCost,1,"recipient");
							}
						}
				  ,
				  overlayspeed:"fast",
				  buttons: { 增加: "y", 取消: "n" }
			});
		}
		else
		{
			printAjax(sc_id,printPage,useType,shipping_cost,1,"recipient");
		}	
}
function printMoreAjax(sc_id,printPage,useType,shipping_cost)//一票多件
{	
	if(sc_id==100021||sc_id==100022||sc_id==100002||sc_id==100143||sc_id==100023||sc_id==100024)
		{
			$.prompt(
			"<div id='title'>一票多件</div><br>&nbsp;&nbsp;一票几件 &nbsp;&nbsp;&nbsp;<input name='proPkcount' type='text' id='proPkcount' value='1' style='width:200px;'>格式：1-99<br/><br/>&nbsp;&nbsp;支付关税&nbsp;&nbsp;&nbsp;<select id='proDTP' name='proDTP'><option value='sender'>Sender 发件人</option><option selected='selected' value='recipient'>Recipient 收件人</otpion></select><br/><br/>&nbsp;&nbsp;英文备注<br/><textarea cols='55' id='english_note' name='english_note'></textarea>",
			{
			      submit: 
						function (v,m,f)
						{
							if (v=="y")
							{
								if (!isIntNum(f.proPkcount))
								{
									alert("请正确输入件数");
									return(false);
								}
							}
						}
				  ,
		   		  loaded:
				  
						function ()
						{
							
						}
				  
				  ,
				  callback: 
				  
						function (v,m,f)
						{
							if (v=="y")
							{
								printAjax(sc_id,printPage,useType,shipping_cost,f.proPkcount,f.proDTP,f.english_note);
							}
						}
				  ,
				  overlayspeed:"fast",
				  buttons: { 增加: "y", 取消: "n" }
			});
	}
}
 
 function printAjax(sc_id,printPage,useType,shipping_cost,pkcount,dtp,english_note)
{		 
		var para = "waybill_from_type=<%=WayBillFromKey.Record%>&dtp="+dtp+"&material="+$("#material").val()+"&inv_di_id="+$("#inv_di_id").val()+"&delivery_note="+$("#delivery_note").val()+"&shipping_cost="+shipping_cost+"&mail_piece_shape="+useType+"&old_waybill_id="+$("#old_waybill_id").val()+"&handle="+$("#handle").val()+"&handle_status="+$("#handle_status").val()+"&invoice_id="+$("#inv_di_id").val()+"&ccid="+$("#ccid").val()+"&pro_id="+$("#pro_id").val()+"&sc_id="+sc_id+"&ps_id="+$("#ps_id").val()+"&inv_dog="+$("#inv_dog").val()+"&inv_rfe="+$("#inv_rfe").val()+"&inv_uv="+$("#inv_uv").val()+"&inv_tv="+$("#inv_tv").val()+"&hs_code="+$("#hs_code").val()+"&pkcount="+pkcount;
		$(".cssDivschedule").css("display","block");
	 
 		$.ajax({
 				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/'+printPage,
 				type: 'post',
 				dataType: 'html',
 				timeout: 60000,
 				cache:false,
 				async:false,
 				data:
 				{
 					english_note:english_note,
 					waybill_from_type:<%=WayBillFromKey.Replace%>,
 					dtp:dtp,
 					material:$("#material").val(),
 					inv_di_id:$("#inv_di_id").val(),
 					delivery_note:$("#delivery_note").val(),
 					shipping_cost:shipping_cost,
 					mail_piece_shape:useType,
 					old_waybill_id:$("#old_waybill_id").val(),
 					handle:$("#handle").val(),
 					handle_status:$("#handle_status").val(),
 					invoice_id:$("#inv_di_id").val(),
 					ccid:$("#ccid").val(),
 					pro_id:$("#pro_id").val(),
 					sc_id:sc_id,
 					ps_id:$("#ps_id").val(),
 					inv_dog:$("#inv_dog").val(),
 					inv_rfe:$("#inv_rfe").val(),
 					inv_uv:$("#inv_uv").val(),
 					inv_tv:$("#inv_tv").val(),
 					hs_code:$("#hs_code").val(),
 					pkcount:pkcount,
					inv_dog_chinese:$("#inv_dog_chinese").val(),
					inv_rfe_chinese:$("#inv_rfe_chinese").val(),
					material_chinese:$("#material_chinese").val()
 				},
 				beforeSend:function(request){ 					
 				},
 				error: function(){
 				},
 				
 				success: function(msg){
 					$(".cssDivschedule").fadeOut("slow");
 					if(msg=="ok")
 					{
 						parent.location.href = 'waybill_index.html'
 					}
 					else if(msg=="close")
 					{
	
 						 parent.location.href = 'waybill_index.html'
 					}
 					else
 					{
 						if(msg.indexOf(".xml")>-1)
 						{
 							window.location.href = msg;
 						}
 						else
 						{
 							alert(msg);
 							parent.location.href = 'waybill_index.html'
 						}
 						
 					}
 				}
 			});	
 }
 </script>
 

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" oncontextmenu="return false;">
<div class="cssDivschedule">
		<div class="innerDiv">
 			正在生成运单.......
 		</div>
</div>
<form action="" id="sub">
<input type="hidden" id="old_waybill_id" name="old_waybill_id" value='<%=wayBill.getString("waybill_id") %>'/>
<input type="hidden" id="inv_di_id" name="inv_di_id" value='<%=wayBill.getString("inv_di_id") %>'/>
<input type="hidden" id="delivery_note"  name="delivery_note" value='<%=wayBill.getString("delivery_note") %>'/>
<input type="hidden" id="shipping_cost"   name="shipping_cost" value='<%=wayBill.getString("shipping_cost") %>'/>
<input type="hidden" id="mail_piece_shape"  name="mail_piece_shape" value='<%=wayBill.getString("mail_piece_shape") %>'/>
<input type="hidden" id="handle" name="handle" value='<%=wayBill.getString("handle") %>'/>
<input type="hidden" id="handle_status" name="handle_status" value='<%=wayBill.getString("handle_status") %>'/>
<input type="hidden" id="ccid"  name="ccid"  value='<%=wayBill.getString("ccid") %>'/>
<input type="hidden" id="pro_id"  name="pro_id" value='<%=wayBill.getString("pro_id") %>'/>
<input type="hidden" id="invoice_id"  name="invoice_id" value='<%=wayBill.getString("inv_di_id") %>'/>
<input type="hidden" id="sc_id"  name="sc_id" value='<%=wayBill.getString("sc_id") %>'/>
<input type="hidden" id="ps_id" name="ps_id"  value='<%=wayBill.getString("ps_id") %>'/>
<input type="hidden" id="inv_dog"  name="inv_dog" value='<%=wayBill.getString("inv_dog") %>'/>
<input type="hidden" id="inv_rfe" name="inv_rfe"  value='<%=wayBill.getString("inv_rfe") %>'/>
<input type="hidden" id="inv_uv"  name="inv_uv"  value='<%=wayBill.getString("inv_uv") %>'/>
<input type="hidden" id="inv_tv"  name="inv_tv"  value='<%=wayBill.getString("inv_tv") %>'/>
<input type="hidden" id="hs_code" name="hs_code" value='<%=wayBill.getString("hs_code")%>'/>
<input type="hidden" id="material" name="material" value="<%=wayBill.getString("material")%>"/>
<input type="hidden" id="material_chinese" name="material_chinese" value="<%=wayBill.getString("material_chinese")%>"/>
<input type="hidden" id="inv_dog_chinese"  name="inv_dog_chinese" value='<%=wayBill.getString("inv_dog_chinese") %>'/>
<input type="hidden" id="inv_rfe_chinese" name="inv_rfe_chinese"  value='<%=wayBill.getString("inv_rfe_chinese") %>'/>
</form>		 
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
			    <td height="30" align="center" valign="middle">&nbsp;</td>
			  </tr>
			  <tr>
			    <td align="center" valign="middle" style="padding-left:50px;">
				<%
				for (int i=0; expressPrintBean.length>0&&i<expressPrintBean.length; i++)
				{
				%>
				  <div class="fee rad" id="div_<%=expressPrintBean[i].getScID()%>" onmousedown="printItAjax(<%=expressPrintBean[i].getScID()%>,'<%=expressPrintBean[i].getPage().replaceAll(".jsp", ".html")%>','<%=expressPrintBean[i].getUseType()%>',<%=expressPrintBean[i].getShippingFee()%>,this,event)"  ><%=expressPrintBean[i].getName()%> 
			 
				  <span style="font-size:12px;font-weight:normal">￥<span class="fee_value"><%=expressPrintBean[i].getShippingFee()%></span></span></div>
			 
				<%
				}
				%>
				
				<!-- 不推荐使用快递 -->
				<%
				for (int i=0; noSelectExpressPrintBean.length>0&&i<noSelectExpressPrintBean.length; i++)
				{
				%>
			     
				  <div class="noselect rad" style="border:1px solid silver;" id="div_<%=noSelectExpressPrintBean[i].getScID()%>" onmousedown="printItAjax(<%=noSelectExpressPrintBean[i].getScID()%>,'<%=noSelectExpressPrintBean[i].getPage().replaceAll(".jsp", ".html")%>','<%=noSelectExpressPrintBean[i].getUseType()%>',<%=noSelectExpressPrintBean[i].getShippingFee()%>,this,event)">
						<span style="margin:0px auto;display:block;float:left;height:20px;line-height:20px;border:0px solid silver;text-align:center;width:100%;">
					 
					 		<span><%=noSelectExpressPrintBean[i].getName()%> </span>
					 		<span>￥<span class="fee_value"><%=noSelectExpressPrintBean[i].getShippingFee()%></span></span>
					 	</span>
					
				  <%
				  	if(!noSelectExpressPrintBean[i].getUseType().equals(""))
				  	{
				  %>
				  	 	<span style="font-size:10px;display:block;clear:both;border:0px solid silver;height:15px;line-height:15px;">
				  			<%=noSelectExpressPrintBean[i].getUseType()%>
					  	</span>
			
				  <%
				  	}
				  %>
				  
				 </div>
 
				<%
				}
				%>	  
				</td>
			  </tr>
		</table>
 
</body>
</html>
