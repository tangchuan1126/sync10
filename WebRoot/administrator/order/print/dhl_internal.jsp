<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.api.AdminMgr,com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="com.cwc.shipping.ShippingInfoBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../../include.jsp"%> 

<%
	//orderMgr
	String type = StringUtil.getString(request,"type");
	long id = StringUtil.getLong(request,"id");
	
	DBRow row;
	String trackingNumber = "";
	String hs_code = "";
	float weight = 0;
	int NumberOfPieces = 1;
	if(type.toUpperCase().equals("D"))
	{
		row = deliveryMgrZJ.getDetailDeliveryOrder(id);
		trackingNumber = row.getString("waybill_number");
		hs_code = row.getString("hs_code");
		weight = row.get("weight",0f);
	}
	else if(type.toUpperCase().equals("T"))
	{
		row = transportMgrZJ.getDetailTransportById(id);
		trackingNumber = row.getString("transport_waybill_number");
		hs_code = row.getString("hs_code");
		weight = row.get("weight",0f);
	}
	else 
	{
		row = new DBRow();
	}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>打印DHL</title>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<link type="text/css" href="../../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />
<link href="../../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script>
var invoiceWidth = 210;
var invoiceHeight = 297;
var productListWidth = 210;
var productListHeight = 220;

function writeTracking(obj)
{
	document.getElementById("tracking").innerText = obj.value;
}

function loadDHLPage()
{
	var page = "../TransformXMLtoHTML/HTML/<%=trackingNumber%>.html";

	$.ajax({
		url: page,
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,	
		
		error: function(){
			$("#prolist").html("<span style='color:red;font-weight:bold;font-size:14px;'>错误：DHL运单页面不存在("+page+")</span>");
		},
		
		success: function(html){
			$("#prolist").html(html);
		}
	});
}
function closeWindowAndRefresh(){
	$.artDialog.opener.refresh && $.artDialog.opener.refresh();
	$.artDialog.close();
}
function printWayBill()
{
	visionariPrinter.PRINT_INIT("打印运单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
	visionariPrinter.SET_PRINT_COPIES(<%=String.valueOf(NumberOfPieces+1)%>);
	visionariPrinter.SET_PRINTER_INDEX("LaserPrinter");
	visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("wayBillContainer").innerHTML);
		
	if ( visionariPrinter.PRINT() )
	{
		return(true);
	}		
}

function printInvoice()
{	
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
	visionariPrinter.SET_PRINT_COPIES(<%=String.valueOf(NumberOfPieces+1)%>);
	visionariPrinter.SET_PRINTER_INDEX("LaserPrinter");
	visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("invoiceContainer").innerHTML);
	return( visionariPrinter.PRINT() );
}

function printProductList()
{
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,-1,-1,"A4");
	visionariPrinter.SET_PRINT_COPIES(2);
	visionariPrinter.SET_PRINTER_INDEX("LaserPrinter");
	visionariPrinter.ADD_PRINT_TABLE(getPrintMmPx(10),0,getPrintMmPx(productListWidth),getPrintMmPx(productListHeight),document.getElementById("productListContainer").innerHTML);

	if ( visionariPrinter.PRINT() )
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/finish_print.html';
		uri += "?waybill_id=" + $("#waybill_id_").val();
		$.artDialog.open(uri, {title: '打印运单',width:'460px',height:'200px',fixed:true, lock: true,opacity: 0.3});
		//document.finish_print_form.submit();
		return( true );
	}
}

function mergePrint()
{
	if (printWayBill())
	{
		if (printInvoice())
		{
			printProductList();
		}
	}
}

</script>

<style>

</style>




</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="loadDHLPage()">

<div id="keep_alive" style="display:none"></div>
<table width="923" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td >
		
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="30%" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算重量: <%=weight%>Kg
		

		</td>
		<td width="30%" style="">
		</td>
        <td width="40%"   align="right">
        <!-- 
        	<tst:authentication bindAction="com.cwc.app.api.zj.WayBillMgrZJ.printWayBill">
        		<input name="Submit2" type="button" class="long-button-print" onClick="mergePrint()" value="联合打印">
        	</tst:authentication>
        -->
        </td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;"></td>
        </tr>
    </table>
		
		
		</td>
      </tr>
      <tr>
        <td>


<div class="demo" >
<div id="tabs">
<ul>
		<li><a href="#waybill">打印运单</a></li>
		<li><a href="#invoice">打印发票</a></li>	 
		<li><a href="#productList">装箱单</a></li>	 
</ul>


<div id="waybill">
<tst:authentication bindAction="com.cwc.app.api.zj.WayBillMgrZJ.printWayBill">
<input name="Submit" type="button" class="short-short-button-print" onClick="printWayBill()" value="打印">
</tst:authentication>
<div id="wayBillContainer" > 


<div id="dhl_print">
<SPAN id="loadifo"  style="color:#FF0000"></SPAN><DIV id="prolist"></DIV>
<div style="font-size:15px;font-weight:bold;">
VAT:1111960571<br>
HS:<%=hs_code%>
</div>

</div>


</div>





<!------------------------------------------------->
</div>

<script>
createContainer("wayBillContainer",15,50,invoiceWidth,invoiceHeight);
setComponentPos('dhl_print',10,1);
</script>

<!-- 运单结束 -->

<div id="invoice">
	<%
		if(!row.getString("invoice_path").equals(""))
		{
	%>
		<a href="../../../<%=row.getString("invoice_path")%>">下载</a>
	<%	
		}
	%>
</div>
<!-- 发票结束 --->

<div id="productList">
</div>
</div></div>



		
		</td>
      </tr>
    </table>
	
	
	
	
<script>
$("#tabs").tabs({	
	cookie: { expires: 30000 } 
});
</script>




</body>
</html>
