<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="../../../include.jsp"%>
<%
	long waybill_id = StringUtil.getLong(request,"waybill_id");
	// StringUtil.getLong(request,"waybill_id");

	DBRow waybillOrder = wayBillMgrZJ.getDetailInfoWayBillById(waybill_id);
	
	DBRow[] waybill_items = wayBillMgrZJ.getWayBillOrderItems(waybill_id);
	
	int NumberOfPieces = 0;
	
	DBRow detailDelivererInfo = new DBRow();
	
  
	
	if (waybillOrder.get("inv_di_id",0l)==0)
	{
		detailDelivererInfo = new DBRow();
	}
	else
	{
		detailDelivererInfo = orderMgr.getDetailDelivererInfo(waybillOrder.get("inv_di_id",0l));
	}
	 
	float quantity = 0;
	for(int i = 0;i<waybill_items.length;i++)
	{
		quantity += waybill_items[i].get("quantity",0f);
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>上门自提打印</title>

<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<link type="text/css" href="../../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />
<link href="../../comm.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		
		
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script>
var invoiceWidth = 210;
var invoiceHeight = 297;
var productListWidth = 210;
var productListHeight = 220;
function closeWindowAndRefresh(){
	$.artDialog.opener.refresh && $.artDialog.opener.refresh();
	$.artDialog.close();
}
function printInvoice()
{	
		visionariPrinter.PRINT_INIT("打印订单");
		visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
		visionariPrinter.SET_PRINT_COPIES(1);
		visionariPrinter.SET_PRINTER_INDEX("LaserPrinter");
		visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("invoiceContainer").innerHTML);
		if(visionariPrinter.PRINT())
		{
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/printWayBill.action',
				type: 'post',
				dataType: 'html',
				timeout: 30000,
				cache:false,
				data:"waybill_id=<%=waybill_id%>",
								
				beforeSend:function(request){
				},
								
				error: function(){
					alert("打印运单失败，请重新打印");
				},
								
				success: function(msg){
					if (msg!=0)
					{
						alert("打印运单失败，请重新打印");
					}
				}
			});
		}
		
		return( visionariPrinter.PRINT() );
}

function downLoadPdf(trackingNumber)
{
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/ePacketDownLoad.action',
				type: 'post',
				dataType: 'json',
				timeout: 30000,
				cache:false,
				data:{trackingNumber:trackingNumber},
								
				beforeSend:function(request){
				},
								
				error: function(){
					alert("从EPacket下载失败");
				},
								
				success: function(msg){
					if(msg.result == "ok")
					{
						alert("下载成功，请重新打印");
					}
				}
			});
}

function printProductList()
{
		visionariPrinter.PRINT_INIT("打印订单");
		visionariPrinter.SET_PRINT_PAGESIZE(3,-1,-1,"A4");
		visionariPrinter.SET_PRINT_COPIES(2);
		visionariPrinter.SET_PRINTER_INDEX("LaserPrinter");
		visionariPrinter.ADD_PRINT_TABLE(getPrintMmPx(10),0,getPrintMmPx(productListWidth),getPrintMmPx(productListHeight),document.getElementById("productListContainer").innerHTML);
		
		if ( visionariPrinter.PRINT() )
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/finish_print.html';
			uri += "?waybill_id=" + $("#waybill_id_").val();
			$.artDialog.open(uri, {title: '打印运单',width:'460px',height:'200px',fixed:true, lock: true,opacity: 0.3});
			//document.finish_print_form.submit();
			return( true );
		}

}


function mergePrint()
{
		if (printInvoice())
		{
			printProductList();
		}
}


function promptAlert()
{
	$.prompt("<div style='font-size:18px;font-weight:bold;color:red'>订单:"+document.print_form.oid.value+" 打印失败</div>请关闭窗口，重新打印！",
	{
		  submit:  function(){},
		  loaded: function(){},
		  callback: function(){},
		  overlayspeed:"fast",
		  buttons: { 取消: "n" }
	});
}

var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
} 

function openPdf(url)
{
	$.artDialog.open(url, {title: '打印运单',width:'800px',height:'600px',fixed:true, lock: true,opacity: 0.3});
}

</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<div id="keep_alive" style="display:none"></div>
   <form name="finish_print_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/finish_print.html">
		<input type="hidden" name="waybill_id" id="waybill_id_" value="<%=waybill_id%>">
		<input type="hidden" name="rePrint" value="<%=StringUtil.getCurrentURI(request)+"?waybill_id"+waybill_id%>">
 </form>

<form action="../EPacket/<%=waybillOrder.getString("tracking_number")%>.pdf" name="download_form">
</form>
 
<table width="923" border="0" cellpadding="8" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="30%" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算重量:  <%= waybillOrder.get("all_weight",0.0f) %>Kg</td>
        <td width="30%">
		<%
	 		 String[] array = {"待打印","已打印","已发货","取消","拆分中"};
	 		 String wayBillState =  array[waybillOrder.get("status", 0)];
	 	%>
	 		<% if(!wayBillState.equals("待打印")){%>
				<span style="font-weight:bold;font-size:12px;">注意  : &nbsp;该运单 [ <%= waybillOrder.getString("waybill_id")%> ]   <span style="color:red;"><%= wayBillState%></span></span>
			<%}%>
		</td>
        <td width="40%" rowspan="2" align="right">
			<tst:authentication bindAction="com.cwc.app.api.zj.WayBillMgrZJ.printWayBill">
        		<input name="Submit2" type="button" class="long-button-print" onClick="mergePrint()" value="联合打印">
        	</tst:authentication>
		</td>
      </tr>
      <tr>
        <td style="color:#999999">&nbsp;
		 
		</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td>
	
	
<div class="demo" >
<div id="tabs">
<ul>
		<li><a href="#waybill">打印运单</a></li>
		<li><a href="#invoice">打印发票</a></li>	 
		<li><a href="#productList">装箱单</a></li>	 
</ul>
<div id="waybill">
	<input  type="button" class="short-short-button-print" value="打印" onclick="openPdf('<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/EPacket/<%=waybillOrder.getString("tracking_number")%>.pdf')"/>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/EPacket/<%=waybillOrder.getString("tracking_number")%>.pdf">下载</a>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<input  type="button" class="long-button-export" value="文件重置" onclick="downLoadPdf('<%=waybillOrder.getString("tracking_number")%>')"/>
</div>
<div id="invoice">
<input name="Submit" type="button" class="short-short-button-print" onClick="printInvoice()" value="打印">
<div id="invoiceContainer">
<style>
td
{
	font-size:11px;
	font-family:Arial, Helvetica, sans-serif;
}
.title-bg
{
	color:#FFFFFF;
	font-weight:bold;
	background:#000000;
	font-size:11px;
}
</style>


<div id="invoice_title" style="font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>&nbsp;
<span style="font-size:12px;color:#999999">WWW.VVME.COM</span>
</div>
<div id="invoice_title2" style="font-size:27px;font-weight:bold;font-family: Arial">INVOICE</div>
<div id="invoice_line">
  <hr style="border:1px #000000 solid;height:1px;width:660px;">
</div>

<div id="invoice_table1">
<table width="320" height="60" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">
         <tr>
           <td bordercolor="#FFFFFF"  style="font-weight:bold">CONSIGNEE</td>
         </tr>
         <tr>
           <td width="178" bordercolor="#FFFFFF"  style="font-size:11px;"><%=waybillOrder.getString("address_name")%> <br>
             <%=waybillOrder.getString("address_street")%>
             <%=waybillOrder.getString("address_city")%>,<%=waybillOrder.getString("address_state")%>,<%=waybillOrder.getString("address_zip")%>,<%=waybillOrder.getString("address_country")%><br>
			 Phone: <%=waybillOrder.getString("tel")%><br>
             Email: <%=waybillOrder.getString("client_id")%>			 </td>
      </tr>
    </table>
</div>

<div id="invoice_table2">
<table width="320" height="30" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">
          <tr>
            <td align="right" bordercolor="#FFFFFF">WAYBILL NO: <%=waybillOrder.getString("waybill_id")%></td>
          </tr>
          
          <tr>
            <td align="right" bordercolor="#FFFFFF">AIRWAY BILL NO: <%=waybillOrder.getString("tracking_number")%></td>
          </tr>
          <tr>
            <td align="right" bordercolor="#FFFFFF">CARRIER: EPacket</td>
          </tr>
           
          <tr>
            <td align="right" bordercolor="#FFFFFF">REASON FOR EXPORT: <%=StringUtil.ascii2Html( waybillOrder.getString("inv_rfe") )%></td>
          </tr>
          <tr>
            <td align="right" bordercolor="#FFFFFF">INVOICE TIME: <%=DateUtil.NowStrGoble()%></td>
          </tr>
          <tr>
            <td  align="right" bordercolor="#FFFFFF">COUNTRY: <%=waybillOrder.getString("address_country")%></td>
          </tr>
    </table>
</div>


<div id="invoice_table3">
<table width="320" height="60" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">

           <tr>
             <td bordercolor="#FFFFFF"  style="font-weight:bold">CONSIGNOR</td>
           </tr>
           <tr>
             <td width="179" bordercolor="#FFFFFF"  style="font-size:11px;">
			 <%=detailDelivererInfo.getString("CompanyName")%><br>

			 <%=detailDelivererInfo.getString("AddressLine1")%><br>
<%=detailDelivererInfo.getString("AddressLine2")%><br>
<%=detailDelivererInfo.getString("AddressLine3")%><br>
<%=detailDelivererInfo.getString("City")%>/<%=detailDelivererInfo.getString("CountryName")%><br>
Phone: <%=detailDelivererInfo.getString("PhoneNumber")%></td>
      </tr>
           <tr>
             <td bordercolor="#FFFFFF"  style="table-layout: fixed; word-wrap:break-word;word-break:break-all"></td>
      </tr>
    </table>
</div>

<div id="invoice_table4">
<table width="660" border="1" cellpadding="3" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse; ">
       <tr>
         <td width="83" height="17" align="center" valign="middle" class="title-bg"><strong>NO. OF ITEMS </strong></td>
         <td width="160" align="center" valign="middle" class="title-bg"><strong>DESCRIPTION OF GOODS </strong></td>
         <td width="136" align="center" valign="middle" class="title-bg"><strong>ORIGINAL COUNTRY </strong></td>
         <td width="122" align="center" valign="middle" class="title-bg"><strong>UNIT VALUE (USD) </strong></td>
         <td width="117" align="center" valign="middle" class="title-bg"><strong>TOTAL VALUE (USD) </strong></td>
       </tr>
       <tr>
         <td width="83" height="20" align="center" valign="middle"><%=quantity%> Unit</td>
         <td width="160" align="center" valign="middle"><%=StringUtil.ascii2Html( waybillOrder.getString("inv_dog") )%>         </td>
         <td width="136" align="center" valign="middle">CHINA</td>
         <td width="122" align="center" valign="middle">
		 <%
		 if (waybillOrder.getString("inv_tv").equals(""))
		 {
		 	out.println("$"+waybillOrder.getString("inv_uv"));
		 }
		 %>		 </td>
         <td align="center" valign="middle">
		 		 <%
		 if (!waybillOrder.getString("inv_tv").equals(""))
		 {
		 	out.println("$"+waybillOrder.getString("inv_tv") );
		 }
		 else
		 {
		 	out.println("$"+StringUtil.formatNumber("#0.00", Double.parseDouble(waybillOrder.getString("inv_uv"))*(quantity) ) );
		 }
		 %>		 </td>
       </tr>
    </table>
	 
	 <br>
	 <table width="660" height="97" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse;">

             <tr>
               <td height="71"  align="left" valign="top" bgcolor="#FFFFFF" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                   <td width="14%" align="center" valign="middle"  class="title-bg" style="padding:2px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;border-left:1px #000000 solid;"><strong>QUANTITY</strong></td>
                   <td width="14%" align="center" valign="middle"  class="title-bg" style="padding:2px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;"><strong>HSCODE</strong></td>
                   <td width="14%" align="center" valign="middle"  class="title-bg" style="padding:2px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;"><strong>MATERIAL</strong></td>
                   <td width="58%"  class="title-bg"  style="padding:5px;border-bottom:1px #000000 solid;padding-left:10px;border-right:1px #000000 solid;"><strong>ITEM</strong></td>
                 </tr>
<%

if (waybill_items.length>0)
{
	for (int zz=0; zz<waybill_items.length; zz++)
	{
%>
                 <tr>
                   <td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;border-bottom:1px #000000 solid;border-left:1px #000000 solid;">
				   			<%
					out.println(waybill_items[zz].get("quantity",0f)+waybill_items[zz].getString("unit_name")+"<br>");
					%>	
				   </td>
					<td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;border-bottom:1px #000000 solid;">
						<%=waybillOrder.getString("hs_code")%>
					</td>
					<td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;border-bottom:1px #000000 solid;">
						<%=waybillOrder.getString("material")%>&nbsp;
					</td>
                   <td  style="padding:2px;padding-left:10px;border-right:1px #000000 solid;border-bottom:1px #000000 solid;">
				   <%
					out.println("Part# "+waybill_items[zz].getString("pc_id"));
					if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(" ●");
					}
					

				   %>
				   
				   </td>
                 </tr>
<%
	}	
%>			 
<%
}
%>





               </table></td>
             </tr>
    </table>
	 <table width="660" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="right" style="font-size:10px;">
		I declare that the above information is true and correct to the best of my knowledge.
		</td>
      </tr>
    </table>
</div>




</div>



<script>
createContainer("invoiceContainer",15,50,invoiceWidth,invoiceHeight);

setComponentPos('invoice_title',10,10);
setComponentPos('invoice_title2',155,10);
setComponentPos('invoice_line',11,22);
setComponentPos('invoice_table3',11,29);
setComponentPos('invoice_table2',101,29);
setComponentPos('invoice_table1',11,60);
setComponentPos('invoice_table4',11,90);

</script>

</div>
<!--- 发票结束 -->






<div id="productList">
<input name="Submit" type="button" class="short-short-button-print" onClick="printProductList()" value="打印">

<div id="productListContainer">
<style>
td
{
	font-size:11px;
	font-family:Arial, Helvetica, sans-serif;
}
.barcode2
{
	font-family: C39HrP48DmTt;font-size:30px;
}
.barcode
{
	font-family: C39HrP24DlTt;font-size:32px;
}
</style>





<div id="productlist_invoice_table4">

	 <table width="660" border="0" cellpadding="0" cellspacing="0">
	 
	 <thead>
  <tr>
    <td colspan="4" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="52%">
		<div style="margin-left:10px;font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>
		&nbsp;<span style="font-size:12px;color:#999999">WWW.VVME.COM</span></div>	
		<div style="margin-left:10px;margin-top:40px;font-size:11px;font-family:Arial"><%=DateUtil.NowStr()%></div>
			</td>
        <td width="48%" align="right"><table width="224" height="30" border="0" cellpadding="2" cellspacing="0">
          <tr>
            <td align="right" ><strong>WayBill NO</strong>: <%=waybillOrder.getString("waybill_id")%></td>
          </tr>
          <tr>
            <td align="right" >
           		  <img src="/barbecue/barcode?data=<%=waybillOrder.getString("waybill_id")%>&width=1&height=35&type=code39" />
            </td>
          </tr>
          <tr>
            <td align="right" ><strong>AIRWAY BILL NO</strong>: <%=waybillOrder.getString("tracking_number")%> </td>
          </tr>
          <tr>
            <td align="right" >
                <img src="/barbecue/barcode?data=<%=waybillOrder.getString("tracking_number")%>&width=1&height=35&type=code39" />
            </td>
          </tr>
        </table></td>
      </tr>
	  
	        <tr>
	          <td>&nbsp;</td>
	          <td align="right">&nbsp;</td>
	          </tr>
	       
    </table></td>
    </tr>
			   <%
if ( waybillOrder.getString("delivery_note").equals("")==false)
{
%>
  <tr>
    <td colspan="4" style="padding-bottom:5px;">
	* NOTE:
	<%=waybillOrder.getString("delivery_note")%>
 	</td>
    </tr>
<%
}
%>
  <tr>
    <td colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;border:1px #000000 solid;background:#000000;color:#FFFFFF;">QTY.</td>
    <td bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;padding-left:5px;bold;border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;">ITEM</td>
    <td bgcolor="#FFFFFF" style="border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;font-family:Arial;padding-left:5px;font-weight:bold">PARTS</td>
  </tr>
  </thead>
  

  <%
for (int zz=0; zz<waybill_items.length; zz++)
{
%>
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;">	   			<%
					out.println(waybill_items[zz].get("quantity",0f));
					%>	</td>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;border-left:0px;">
			   			<%
					if (waybill_items[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(waybill_items[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:12px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
			
					
					out.println(waybill_items[zz].getString("p_name"));
					if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(" ●");
					}
					

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						// unno
						DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(waybill_items[zz].get("pc_id",0l));//获得定制套装原来标准套装的ID
						DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
						ArrayList oldUnionProsList = new ArrayList();//原来套装所包含的商品
						HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
						if(null != oldUnionPros ){
							for (int kk=0; kk<oldUnionPros.length; kk++)
							{
								oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
								oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
							}
						}
						
						DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( waybill_items[zz].get("pc_id",0l) );
						for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
						{
							out.println("<tr>");
	
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
							
							//如果商品不存在于原来的套装之中，则加*提示
							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
							{
								out.println(" *");
							}
							
							out.println("</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
							
							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
							{
								out.println(" *");
							}

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(waybill_items[zz].get("pc_id",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
						{
							out.println("<tr>");
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</tbody>
<%
}
%> 
 
 

  <tfoot>  
  <tr>
    <td colspan="4" bgcolor="#FFFFFF">

      <table width="660" border="0" cellspacing="0" cellpadding="3">
       <tr>
         <td width="453" align="left" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">EW: 
		 <%=MoneyUtil.round(orderMgr.calculateWayBillWeight4ST(Float.parseFloat(waybillOrder.getString("all_weight"))), 2)%>Kg		</td>
         <td width="454" align="right" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">
		<%=waybillOrder.getString("order_source")%>		 </td>
       </tr>
     </table>	</td>
  </tr>
  </tfoot>
</table>

	 
</div>
</div></div>
	<script>
createContainer("productListContainer",15,50,productListWidth,productListHeight);

setComponentPos('productlist_invoice_table4',8,10);

</script>



</div>



<!--- tab结束 -->
</div></div>
	

	
	</td>
  </tr>
</table>




<script>
//提交疑问成本订单
 
	
	
		  
					
							
							
							
								
								
						

		  


	$("#tabs").tabs({});
</script>









</body>
</html>
