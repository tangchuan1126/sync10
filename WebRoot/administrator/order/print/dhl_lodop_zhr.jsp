<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.api.AdminMgr,com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="com.cwc.shipping.ShippingInfoBean"%>
<%@ include file="../../../include.jsp"%> 

<%
	long waybill_id = StringUtil.getLong(request,"waybill_id");
	DBRow waybillOrder = wayBillMgrZJ.getWayBillOrderItems(waybill_id);
	
	DBRow[] waybill_items = wayBillMgrZJ.getWayBillOrderItems(waybill_id);
	
	int NumberOfPieces = 0;
	
	DBRow detailDelivererInfo = new DBRow();
	
	DBRow detailDelivererInfo;
	
//	if (waybillOrder.get("inv_di_id",0l)==0)
//	{
//		detailDelivererInfo = orderMgr.getRandomDelivererInfoByPsId(waybillOrder.get("ps_id",0l));
//	}
//	else
//	{
		detailDelivererInfo = orderMgr.getDetailDelivererInfo(waybillOrder.get("inv_di_id",0l));
//	}
	
	float quantity = 0;
	for(int i = 0;i<waybill_items.length;i++)
	{
		quantity += waybill_items[i].get("quantity",0f);
	}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>打印DHL</title>
<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<link type="text/css" href="../../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />
<link href="../../comm.css" rel="stylesheet" type="text/css">

<script>
var invoiceWidth = 210;
var invoiceHeight = 297;
var productListWidth = 210;
var productListHeight = 220;

function writeTracking(obj)
{
	document.getElementById("tracking").innerText = obj.value;
}

function loadDHLPage()
{
	var page = "../TransformXMLtoHTML/HTML/<%=waybillOrder.getString("tracking_number")%>.html";

	$.ajax({
		url: page,
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,	
		
		error: function(){
			$("#prolist").html("<span style='color:red;font-weight:bold;font-size:14px;'>错误：DHL运单页面不存在("+page+")</span>");
		},
		
		success: function(html){
			$("#prolist").html(html);
		}
	});
}

function printWayBill()
{
	visionariPrinter.PRINT_INIT("打印运单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
	visionariPrinter.SET_PRINT_COPIES(<%=String.valueOf(NumberOfPieces+1)%>);
	visionariPrinter.SET_PRINTER_INDEX("HP LaserJet P2015 Series PCL 6");
	visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("wayBillContainer").innerHTML);
		
	if ( visionariPrinter.PRINT() )
	{
		return(true);
	}		
}

function printInvoice()
{	
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
	visionariPrinter.SET_PRINT_COPIES(<%=String.valueOf(NumberOfPieces+1)%>);
	visionariPrinter.SET_PRINTER_INDEX("HP LaserJet P2015 Series PCL 6");
	visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("invoiceContainer").innerHTML);
	return( visionariPrinter.PRINT() );
}

function printProductList()
{
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,-1,-1,"A4");
	visionariPrinter.SET_PRINT_COPIES(2);
	visionariPrinter.SET_PRINTER_INDEX("HP LaserJet P2015 Series PCL 6");
	visionariPrinter.ADD_PRINT_TABLE(getPrintMmPx(10),0,getPrintMmPx(productListWidth),getPrintMmPx(productListHeight),document.getElementById("productListContainer").innerHTML);

	if ( visionariPrinter.PRINT() )
	{
		return( true );
	}
}

function mergePrint()
{
	if (printWayBill())
	{
		if (printInvoice())
		{
			printProductList();
		}
	}
}

</script>

<style>

</style>




</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="loadDHLPage()">

<div id="keep_alive" style="display:none"></div>
<table width="923" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td >
		
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="60%" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算重量: <%=waybillOrder.getString("all_weight")%>Kg
		

		</td>
        <td width="40%" rowspan="2" align="right"><input name="Submit2" type="button" class="long-button-print" onClick="mergePrint()" value="联合打印"></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算运费: ￥<%=waybillOrder.getString("shipping_cost")%></td>
        </tr>
    </table>
		
		
		</td>
      </tr>
      <tr>
        <td>


<div class="demo" >
<div id="tabs">
<ul>
		<li><a href="#waybill">打印运单</a></li>
		<li><a href="#invoice">打印发票</a></li>	 
		<li><a href="#productList">装箱单</a></li>	 
</ul>


<div id="waybill">

<input name="Submit" type="button" class="short-short-button-print" onClick="printWayBill()" value="打印">

<div id="wayBillContainer" > 


<div id="dhl_print">
<SPAN id=loadifo  style="color:#FF0000"></SPAN><DIV id=prolist></DIV>
<div style="font-size:15px;font-weight:bold;">
VAT:1111960571<br>
HS:<%=orderMgr.getHsCode(oid)%>
</div>
<%
if ( waybillOrder.getString("delivery_note").equals("")==false)
{
%>
<br>#NOTE:
<%=waybillOrder.getString("delivery_note")%>
<%
}
%>
</div>


</div>





<!------------------------------------------------->
</div>

<script>
createContainer("wayBillContainer",15,50,invoiceWidth,invoiceHeight);
setComponentPos('dhl_print',10,18);
</script>

<!-- 运单结束 -->

<div id="invoice">
<input name="Submit" type="button" class="short-short-button-print" onClick="printInvoice()" value="打印">
<div id="invoiceContainer">
<style>
td
{
	font-size:11px;
	font-family:Arial, Helvetica, sans-serif;
}
.title-bg
{
	color:#FFFFFF;
	font-weight:bold;
	background:#000000;
	font-size:11px;
}
</style>


<div id="invoice_title" style="font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>&nbsp;
<span style="font-size:12px;color:#999999">WWW.VVME.COM</span>
</div>
<div id="invoice_title2" style="font-size:27px;font-weight:bold;font-family: Arial">INVOICE</div>
<div id="invoice_line">
  <hr style="border:1px #000000 solid;height:1px;width:660px;">
</div>

<div id="invoice_table1">
<table width="320" height="60" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">
         <tr>
           <td bordercolor="#FFFFFF"  style="font-weight:bold">CONSIGNEE</td>
         </tr>
         <tr>
           <td width="178" bordercolor="#FFFFFF"  style="font-size:11px;"><%=waybillOrder.getString("address_name")%> <br>
             <%=waybillOrder.getString("address_street")%>
             <%=waybillOrder.getString("address_city")%>,<%=waybillOrder.getString("address_state")%>,<%=waybillOrder.getString("address_zip")%>,<%=waybillOrder.getString("address_country")%><br>
			 Phone: <%=waybillOrder.getString("tel")%><br>
             Email: <%=waybillOrder.getString("client_id")%>			 </td>
      </tr>
    </table>
</div>

<div id="invoice_table2">
<table width="320" height="30" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">
          <tr>
            <td align="right" bordercolor="#FFFFFF">WAYBILLORDER NO: <%=waybillOrder.getString("oid")%></td>
          </tr>
          
          <tr>
            <td align="right" bordercolor="#FFFFFF">AIRWAY BILL NO: <%=waybillOrder.getString("tracking_number")%></td>
          </tr>
          <tr>
            <td align="right" bordercolor="#FFFFFF">CARRIER: DHL</td>
          </tr>
           
          <tr>
            <td align="right" bordercolor="#FFFFFF">REASON FOR EXPORT: <%=StringUtil.ascii2Html( detailInvoice.getString("rfe") )%></td>
          </tr>
          <tr>
            <td align="right" bordercolor="#FFFFFF">INVOICE TIME: <%=DateUtil.NowStrGoble()%></td>
          </tr>
          <tr>
            <td  align="right" bordercolor="#FFFFFF">COUNTRY: <%=waybillOrder.getString("address_country")%></td>
          </tr>
    </table>
</div>


<div id="invoice_table3">
<table width="320" height="60" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">

           <tr>
             <td bordercolor="#FFFFFF"  style="font-weight:bold">CONSIGNOR</td>
           </tr>
           <tr>
             <td width="179" bordercolor="#FFFFFF"  style="font-size:11px;">
			 <%=detailDelivererInfo.getString("CompanyName")%><br>

			 <%=detailDelivererInfo.getString("AddressLine1")%><br>
<%=detailDelivererInfo.getString("AddressLine2")%><br>
<%=detailDelivererInfo.getString("AddressLine3")%><br>
<%=detailDelivererInfo.getString("City")%>/<%=detailDelivererInfo.getString("CountryName")%><br>
Phone: <%=detailDelivererInfo.getString("PhoneNumber")%></td>
      </tr>
           <tr>
             <td bordercolor="#FFFFFF"  style="table-layout: fixed; word-wrap:break-word;word-break:break-all"></td>
      </tr>
    </table>
</div>

<div id="invoice_table4">
<table width="660" border="1" cellpadding="3" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse; ">
       <tr>
         <td width="83" height="17" align="center" valign="middle" class="title-bg"><strong>NO. OF ITEMS </strong></td>
         <td width="160" align="center" valign="middle" class="title-bg"><strong>DESCRIPTION OF GOODS </strong></td>
         <td width="136" align="center" valign="middle" class="title-bg"><strong>ORIGINAL COUNTRY </strong></td>
         <td width="122" align="center" valign="middle" class="title-bg"><strong>UNIT VALUE (USD) </strong></td>
         <td width="117" align="center" valign="middle" class="title-bg"><strong>TOTAL VALUE (USD) </strong></td>
       </tr>
       <tr>
         <td width="83" height="20" align="center" valign="middle"><%=quantity%> Unit</td>
         <td width="160" align="center" valign="middle"><%=StringUtil.ascii2Html( detailInvoice.getString("dog") )%>         </td>
         <td width="136" align="center" valign="middle">CHINA</td>
         <td width="122" align="center" valign="middle">
		 <%
		 if (detailInvoice.getString("tv").equals(""))
		 {
		 	out.println("$"+detailInvoice.getString("uv"));
		 }
		 %>		 </td>
         <td align="center" valign="middle">
		 		 <%
		 if (!detailInvoice.getString("tv").equals(""))
		 {
		 	out.println("$"+detailInvoice.getString("tv") );
		 }
		 else
		 {
		 	out.println("$"+StringUtil.formatNumber("#0.00", Double.parseDouble(detailInvoice.getString("uv"))*(quantity) ) );
		 }
		 %>		 </td>
       </tr>
    </table>
	 
	 <br>
	 <table width="660" height="97" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse;">

             <tr>
               <td height="71"  align="left" valign="top" bgcolor="#FFFFFF" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                   <td width="14%" align="center" valign="middle"  class="title-bg" style="padding:2px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;"><strong>QUANTITY</strong></td>
                   <td width="86%"  class="title-bg"  style="padding:5px;border-bottom:1px #000000 solid;padding-left:10px;"><strong>ITEM</strong></td>
                 </tr>
<%

if (waybill_items.length>0)
{
	for (int zz=0; zz<waybill_items.length; zz++)
	{
%>
                 <tr>
                   <td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;">
				   			<%
					out.println(waybill_items[zz].get("quantity",0f)+waybill_items[zz].getString("unit_name")+"<br>");
					%>	
				   </td>

                   <td  style="padding:2px;padding-left:10px;">
				   <%
					out.println("Part# "+waybill_items[zz].getString("pc_id"));
					if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(" ●");
					}
					

				   %>
				   
				   </td>
                 </tr>
<%
	}	
%>			 
<%
}
%>





               </table></td>
             </tr>
    </table>
	 <table width="660" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="right" style="font-size:10px;">
		I declare that the above information is true and correct to the best of my knowledge.
		</td>
      </tr>
    </table>
</div>




</div>



<script>
createContainer("invoiceContainer",15,50,invoiceWidth,invoiceHeight);

setComponentPos('invoice_title',10,10);
setComponentPos('invoice_title2',155,10);
setComponentPos('invoice_line',11,22);
setComponentPos('invoice_table3',11,29);
setComponentPos('invoice_table2',101,29);
setComponentPos('invoice_table1',11,60);
setComponentPos('invoice_table4',11,90);

</script>

</div>
<!-- 发票结束 --->

<div id="productList">


<div id="productList">
<input name="Submit" type="button" class="short-short-button-print" onClick="printProductList()" value="打印">

<div id="productListContainer">
<style>
td
{
	font-size:11px;
	font-family:Arial, Helvetica, sans-serif;
}
.barcode2
{
	font-family: C39HrP48DmTt;font-size:30px;
}
.barcode
{
	font-family: C39HrP24DlTt;font-size:32px;
}
</style>





<div id="productlist_invoice_table4">

	 <table width="660" border="0" cellpadding="0" cellspacing="0">
	 
	 <thead>
  <tr>
    <td colspan="4" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="52%">
		<div style="margin-left:10px;font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>
		&nbsp;<span style="font-size:12px;color:#999999">WWW.VVME.COM</span></div>	
		<div style="margin-left:10px;margin-top:40px;font-size:11px;font-family:Arial"><%=DateUtil.NowStr()%></div>
			</td>
        <td width="48%" align="right"><table width="224" height="30" border="0" cellpadding="2" cellspacing="0">
          <tr>
            <td align="right" ><strong>Order NO</strong>: <%=order.getString("oid")%></td>
          </tr>
          <tr>
            <td align="right" >
            	 <img src="/barbecue/barcode?data=<%=order.getString("oid")%>&width=1&height=35&type=code39" />
            </td>
          </tr>
          <tr>
            <td align="right" ><strong>WayBill NO</strong>: <%=input_text_airWayBillNumber%> </td>
          </tr>
          <tr>
            <td align="right" >
             <img src="/barbecue/barcode?data=<%=input_text_airWayBillNumber%>&width=1&height=35&type=code39" />
            </td>
          </tr>
        </table></td>
      </tr>
	  
	        <tr>
	          <td>&nbsp;</td>
	          <td align="right">&nbsp;</td>
	          </tr>
	        <tr>
    </table></td>
    </tr>
			   <%
if ( order.getString("delivery_note").equals("")==false||sonOrderDeliveryNotes.length>0 )
{
%>
  <tr>
    <td colspan="4" style="padding-bottom:5px;">
	* NOTE:
	<%=order.getString("delivery_note")%>
<%
if (sonOrderDeliveryNotes.length>0)
{
	for (int dii=0; dii<sonOrderDeliveryNotes.length; dii++)
	{
		if (sonOrderDeliveryNotes[dii].getString("delivery_note").equals(""))
		{
			continue;
		}
		out.println("<br>"+sonOrderDeliveryNotes[dii].getString("delivery_note"));
	}
}
%>	</td>
    </tr>
<%
}
%>
  <tr>
    <td colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;border:1px #000000 solid;background:#000000;color:#FFFFFF;">QTY.</td>
    <td bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;padding-left:5px;bold;border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;">ITEM</td>
    <td bgcolor="#FFFFFF" style="border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;font-family:Arial;padding-left:5px;font-weight:bold">PARTS</td>
  </tr>
  </thead>
  

  <%
for (int zz=0; zz<orderItems.length; zz++)
{
%>
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;">	   			<%
					out.println(orderItems[zz].get("quantity",0f));
					%>	</td>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;border-left:0px;">
			   			<%
					if (orderItems[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(orderItems[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:12px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
				   	if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&orderItems[zz].get("lacking",0)==1)
					{
						out.println("<span style='color:#FF0000;'> [缺货]</span>");
					}
			
					
					out.println(orderItems[zz].getString("name"));
					if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(" ●");
					}
					

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(orderItems[zz].get("pid",0l));//获得定制套装原来标准套装的ID
						DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
						ArrayList oldUnionProsList = new ArrayList();//原来套装所包含的商品
						HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
						for (int kk=0; kk<oldUnionPros.length; kk++)
						{
							oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
							oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
						}
						
						DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( orderItems[zz].get("pid",0l) );
						for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
						{
							out.println("<tr>");
	
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
							
							//如果商品不存在于原来的套装之中，则加*提示
							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
							{
								out.println(" *");
							}
							
							out.println("</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
							
							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
							{
								out.println(" *");
							}

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(orderItems[zz].get("pid",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
						{
							out.println("<tr>");
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</tbody>
<%
}
%> 



 <%
	long preOid = 0;
	for (int zz=0; zz<sonOrderItems.length; zz++)
	{
		if (sonOrderItems[zz].get("oid",0l)!=preOid)
		{
			total_mc_gross += orderMgr.getDetailPOrderByOid(sonOrderItems[zz].get("oid",0l)).get("mc_gross",0d);
		}
		preOid = sonOrderItems[zz].get("oid",0l);
%>
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;border:1px #000000 solid;border-top:0px;">
					   <%
					out.println(sonOrderItems[zz].get("quantity",0f));
				   %>		</td>
    <td width="29" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:11px;border:1px #000000 solid;border-top:0px;;border-left:0px;">
		   			<%
					if (sonOrderItems[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(sonOrderItems[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:11px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
						if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&sonOrderItems[zz].get("lacking",0)==1)
						{
							out.println("<span style='color:#FF0000;'> [缺货]</span>");
						}
				
						out.println(sonOrderItems[zz].getString("name"));
						if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
						{
							out.println(" ●");
						}		

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
							out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
							DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(sonOrderItems[zz].get("pid",0l));//获得定制套装原来标准套装的ID
							DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
							ArrayList oldUnionProsList = new ArrayList();
							HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
							for (int kk=0; kk<oldUnionPros.length; kk++)
							{
								oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
								oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
							}
						
							DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( sonOrderItems[zz].get("pid",0l) );
							for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
							{
							out.println("<tr>");
	
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));

							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
							{
								out.println(" *");
							}
							
							out.println("</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));

							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
							{
								out.println(" *");
							}
							
							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(sonOrderItems[zz].get("pid",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
						{
							out.println("<tr>");
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</tbody>
<%
}
%> 






  <tfoot>  
  <tr>
    <td colspan="4" bgcolor="#FFFFFF">

      <table width="660" border="0" cellspacing="0" cellpadding="3">
       <tr>
         <td width="453" align="left" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">EW: 
		 <%=MoneyUtil.round(orderMgr.calculateWayBillWeight4ST(total_weight_org), 2)%>Kg		</td>
         <td width="454" align="right" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">
		<%=order.getString("order_source")%>		 </td>
       </tr>
     </table>	</td>
  </tr>
  </tfoot>
</table>

	 
</div>
</div></div>
	<script>
createContainer("productListContainer",15,50,productListWidth,productListHeight);

setComponentPos('productlist_invoice_table4',8,10);

</script>



</div>



</div></div>



		
		</td>
      </tr>
    </table>
	
	
	
	
<script>
$("#tabs").tabs({	
	cookie: { expires: 30000 } 
});
	
	
//提交疑问成本订单
function promptDoubtCost()
{
	$.prompt(
	
	"<div id='title'>亏本订单[单号<%=oid%>]</div><br />是否提交客服处理？",
	
	{
		  submit:
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						var para = "oid=<%=oid%>&sc_id=<%=sc_id%>&ccid=<%=ccid%>";
					
						$.ajax({
							url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markDoubtCostAjax.action',
							type: 'post',
							dataType: 'html',
							timeout: 60000,
							cache:false,
							data:para,
							
							beforeSend:function(request){
							},
							
							error: function(){
								alert("网络错误，请重试！");
							},
							
							success: function(html){
								
								if (html=="ok")
								{
									window.close();
								}
								else
								{
									alert("提交失败！");
								}
								
							}
						});
						
						return(false);

					}
					else if (v=="n")
					{
						window.close();
					}
				}
			,		  
		  
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 关闭打印: "n" }
	});
}

if ( "<%=promptFlag%>"=="true" )
{
	promptDoubtCost();
}

function promptDoubtDestination(dest)
{
	$.prompt(
	
	"<div id='title'>投递地址异常，请提交客服处理！</div>",
	
	{
		  submit:
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						var para = "verify_address_note=Destination "+dest+"&oid=<%=oid%>&sc_id=<%=sc_id%>&ccid=<%=ccid%>";
					
						$.ajax({
							url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/printVerifyAddressAjax.action',
							type: 'post',
							dataType: 'html',
							timeout: 60000,
							cache:false,
							data:para,
							
							beforeSend:function(request){
							},
							
							error: function(){
								alert("网络错误，请重试！");
							},
							
							success: function(html){
								
								if (html=="ok")
								{
									window.close();
								}
								else
								{
									alert("提交失败！");
								}
								
							}
						});
						
						return(false);

					}
					else if (v=="n")
					{
						window.close();
					}
				}
			,		  
		  
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 关闭打印: "n" }
	});
}


if ( "<%=promptDestinationFlag%>"=="true" )
{
	promptDoubtDestination('<%=destination%>');
}
</script>




</body>
</html>
