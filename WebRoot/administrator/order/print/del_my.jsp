<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="../../../include.jsp"%>
<%
long oid = StringUtil.getLong(request,"oid");
long handle = StringUtil.getLong(request,"handle");
long handle_status = StringUtil.getLong(request,"handle_status");
long invoice_id = StringUtil.getLong(request,"invoice_id");
long ccid = StringUtil.getLong(request,"ccid");
long sc_id = StringUtil.getLong(request,"sc_id");
if (sc_id==0)
{
	sc_id = Long.parseLong( request.getAttribute("sc_id").toString() );
}



double total_mc_gross = 0;//累计订单总支付金额

DBRow order = orderMgr.getDetailPOrderByOid(oid);
DBRow detailInvoice;

total_mc_gross =+ order.get("mc_gross",0d);

//订单状态不为：正常 、 有货，禁止打印
if (order.get("handle_status",0)!=HandStatusleKey.NORMAL||order.get("product_status",0)!=ProductStatusKey.IN_STORE)
{
	out.println("<span style='color:red;font-weight:bold'>订单状态不正确，禁止打印！</span>");
	return;
}

//获得订单锁
DBRow orderLock = OrderLock.lockOrder(session,oid,OrderLock.PRINT);
if (orderLock!=null)
{
	out.println("<script>");
	out.println("alert('"+orderLock.getString("operator")+"正在"+OrderLock.getMsg(orderLock.getString("operate_type"))+"，请与其联系后再操作')");
	out.println("</script>");
}

String rePrint = "oid="+oid+"&handle_status="+handle_status+"&handle="+handle+"&ccid="+ccid+"&invoice_id="+invoice_id+"&sc_id="+sc_id;

//计算订单成本，已发货订单重新打印不需要重新计算
boolean promptFlag = false;
float total_weight = 0;
float total_weight_org = 0;

try
{ 
	total_weight_org = orderMgr.compareOrderCost(oid,sc_id,ccid);
}
catch (LostMoneyException e)//亏本
{
	promptFlag = true;
}
catch (CountryOutSizeException e)//国家不到达
{
	out.println("国家不能送达");
	return;
}
catch (WeightCrossException e)//
{
	out.println("重量段设置交叉");
	return;
}
catch (WeightOutSizeException e)//
{
	out.println("重量段超出范围");
	return;
}
catch (CurrencyNotExistException e)//
{
	out.println("汇率不存在");
	return;
}
total_weight = orderMgr.calculateWayBillWeight(sc_id,total_weight_org);



//兼容旧订单情况
if ( order.getString("inv_dog").equals("")&&order.getString("inv_rfe").equals("") )
{
	detailInvoice = orderMgr.getDetailInvoiceTemplate(invoice_id);
}
else
{

	detailInvoice = new DBRow();
	detailInvoice.add("dog",order.getString("inv_dog"));
	detailInvoice.add("rfe",order.getString("inv_rfe"));
	detailInvoice.add("uv",order.getString("inv_uv"));
	detailInvoice.add("tv",order.getString("inv_tv"));
	detailInvoice.add("di_id",order.get("inv_di_id",0l));
}
 
//System.out.println("detailInvoice:"+detailInvoice);

DBRow detailDelivererInfo;
if (detailInvoice.get("di_id",0l)==0)
{
	detailDelivererInfo = orderMgr.getRandomDelivererInfo(1);
}
else
{
	detailDelivererInfo = orderMgr.getDetailDelivererInfo(detailInvoice.get("di_id",0l));
}

//System.out.println("detailDelivererInfo:"+detailInvoice);

if (order==null)
{ 
	out.println("缺少订单号");
	return;
}
DBRow sonOrderDeliveryNotes[] = orderMgr.getSonOrderDeliveryNoteByParentOid(oid);

//计算子订单商品总数
DBRow sonOrderItems[] = orderMgr.getSonOrderItemsByParentOid(oid);
double totalSonOrderQuantity = 0;
for (int i=0; i<sonOrderItems.length; i++)
{
	totalSonOrderQuantity += sonOrderItems[i].get("quantity",0d);
}


String backurl = StringUtil.getCurrentURL(request);
if ( backurl.indexOf("oid")==-1 )
{
	backurl += "?oid="+oid+"&handle="+handle+"&handle_status="+handle_status+"&invoice_id="+invoice_id+"&ccid="+ccid+"&sc_id="+sc_id;
}

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>上门自提打印</title>

<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<link type="text/css" href="../../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />
<link href="../../comm.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		
		
<script>
var invoiceWidth = 210;
var invoiceHeight = 297;
var productListWidth = 210;
var productListHeight = 220;

function printInvoice()
{	
		visionariPrinter.PRINT_INIT("打印订单");
		visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
		visionariPrinter.SET_PRINT_COPIES(1);
		visionariPrinter.SET_PRINTER_INDEX("HP LaserJet P2015 Series PCL 6");
		visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("invoiceContainer").innerHTML);
		return( visionariPrinter.PRINT() );
}

function printProductList()
{
		visionariPrinter.PRINT_INIT("打印订单");
		visionariPrinter.SET_PRINT_PAGESIZE(3,-1,-1,"A4");
		visionariPrinter.SET_PRINT_COPIES(2);
		visionariPrinter.SET_PRINTER_INDEX("HP LaserJet P2015 Series PCL 6");
		visionariPrinter.ADD_PRINT_TABLE(getPrintMmPx(10),0,getPrintMmPx(productListWidth),getPrintMmPx(productListHeight),document.getElementById("productListContainer").innerHTML);
		
		if ( visionariPrinter.PRINT() )
		{
			document.finish_print_form.submit();
			return( true );
		}

}


function mergePrint()
{
		if (printInvoice())
		{
			printProductList();
		}
}


function promptAlert()
{
	$.prompt("<div style='font-size:18px;font-weight:bold;color:red'>订单:"+document.print_form.oid.value+" 打印失败</div>请关闭窗口，重新打印！",
	{
		  submit:  function(){},
		  loaded: function(){},
		  callback: function(){},
		  overlayspeed:"fast",
		  buttons: { 取消: "n" }
	});
}

var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}
  
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<div id="keep_alive" style="display:none"></div>
    <form name="finish_print_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/FinishPrintUpdateHandle.action">
		<input type="hidden" name="oid" value="<%=oid%>">
		<input type="hidden" name="handle" value="<%=handle%>">
		<input type="hidden" name="handle_status" value="<%=handle_status%>">
		<input type="hidden" name="printType" value="DELMY">
		<input type="hidden" name="ems_id" value="?">
		<input type="hidden" name="sc_id" value="<%=sc_id%>">
		<input type="hidden" name="ccid" value="<%=ccid%>">

		<%
		rePrint = rePrint.replaceAll("&","|");
		%>
      <input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/finish_print.html?oid=<%=oid%>&rePrint=<%=StringUtil.getCurrentURI(request)+"?"+rePrint%>">
    </form> 
	
	
 
<table width="923" border="0" cellpadding="8" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="60%" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算重量: <%=MoneyUtil.round(total_weight, 2)%>Kg</td>
        <td width="40%" rowspan="2" align="right"><input name="Submit2" type="button" class="long-button-print" onClick="mergePrint()" value="联合打印"></td>
      </tr>
      <tr>
        <td style="color:#999999">&nbsp;
		 
		</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td>
	
	
<div class="demo" >
<div id="tabs">
<ul>
		
		<li><a href="#invoice">打印发票</a></li>	 
		<li><a href="#productList">装箱单</a></li>	 
</ul>

<%
DBRow orderItems[] = orderMgr.getPOrderItemsByOid(oid);
%>





<div id="invoice">
<input name="Submit" type="button" class="short-short-button-print" onClick="printInvoice()" value="打印">

<div id="invoiceContainer">
<style>
.barcode
{
	font-family: C39HrP24DlTt;font-size:32px;
}
td
{
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
}
.title-bg
{
	color:#FFFFFF;
	font-weight:bold;
	background:#000000;
}
</style>

<div id="invoice_title" style="font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>&nbsp;
<span style="font-size:12px;color:#999999">WWW.VVME.COM</span>
</div>
<div id="invoice_title2" style="font-size:27px;font-weight:bold;font-family: Arial">INVOICE</div>
<div id="invoice_line">
  <hr style="border:1px #000000 solid;height:1px;width:660px;">
</div>

<div id="invoice_table1">
<table width="320" height="60" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">
         <tr>
           <td bordercolor="#FFFFFF"  style="font-weight:bold">CONSIGNEE</td>
         </tr>
         <tr>
           <td width="178" bordercolor="#FFFFFF"  style="font-size:11px;"><%=order.getString("address_name")%> <br>
             <%=order.getString("address_street")%>
             <%=order.getString("address_city")%>,<%=order.getString("address_state")%>,<%=order.getString("address_zip")%>,<%=order.getString("address_country")%><br>
			 Phone: <%=order.getString("tel")%><br>
             Email: <%=order.getString("client_id")%>			 </td>
      </tr>
    </table>
</div>

<div id="invoice_table2">
<table width="320" height="30" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">
          <tr>
            <td align="right" bordercolor="#FFFFFF">ORDER NO: <%=order.getString("oid")%></td>
          </tr>
          
          <tr>
            <td align="right" bordercolor="#FFFFFF">CARRIER: 自提 </td>
          </tr>
           
          <tr>
            <td align="right" bordercolor="#FFFFFF">REASON FOR EXPORT: <%=StringUtil.ascii2Html( detailInvoice.getString("rfe") )%></td>
          </tr>
          <tr>
            <td align="right" bordercolor="#FFFFFF">INVOICE TIME: <%=DateUtil.NowStrGoble()%></td>
          </tr>
          <tr>
            <td  align="right" bordercolor="#FFFFFF">COUNTRY: <%=order.getString("address_country")%></td>
          </tr>
    </table>
</div>



<div id="invoice_table3">
<table width="320" height="60" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">

           <tr>
             <td bordercolor="#FFFFFF"  style="font-weight:bold">CONSIGNOR</td>
           </tr>
           <tr>
             <td width="179" bordercolor="#FFFFFF"  style="font-size:11px;">
			 <%=detailDelivererInfo.getString("CompanyName")%><br>

			 <%=detailDelivererInfo.getString("AddressLine1")%><br>
<%=detailDelivererInfo.getString("AddressLine2")%><br>
<%=detailDelivererInfo.getString("AddressLine3")%><br>
<%=detailDelivererInfo.getString("City")%>/<%=detailDelivererInfo.getString("CountryName")%><br>
Phone: <%=detailDelivererInfo.getString("PhoneNumber")%></td>
      </tr>
           <tr>
             <td bordercolor="#FFFFFF"  style="table-layout: fixed; word-wrap:break-word;word-break:break-all"></td>
      </tr>
    </table>
</div>

<div id="invoice_table4">
<table width="660" border="1" cellpadding="3" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse; ">
       <tr>
         <td width="83" class="title-bg" height="17" align="center" valign="middle">NO. OF ITEMS</td>
         <td width="160" align="center" class="title-bg"  valign="middle">DESCRIPTION OF GOODS </td>
         <td width="136" align="center" class="title-bg"  valign="middle">ORIGINAL COUNTRY</td>
         <td width="122" align="center" class="title-bg"  valign="middle">UNIT VALUE (USD)</td>
         <td width="117" align="center" class="title-bg"  valign="middle">TOTAL VALUE (USD)</td>
       </tr>
       <tr>
         <td width="83" height="20" align="center" valign="middle"><%=order.get("quantity",0f)+totalSonOrderQuantity%> Unit</td>
         <td width="160" align="center" valign="middle"><%=StringUtil.ascii2Html( detailInvoice.getString("dog") )%>         </td>
         <td width="136" align="center" valign="middle">CHINA</td>
         <td width="122" align="center" valign="middle">
		 <%
		 if (detailInvoice.getString("tv").equals(""))
		 {
		 	out.println("$"+detailInvoice.getString("uv"));
		 }
		 %>		 </td>
         <td align="center" valign="middle">
		 		 <%
		 if (!detailInvoice.getString("tv").equals(""))
		 {
		 	out.println("$"+detailInvoice.getString("tv") );
		 }
		 else
		 {
		 	out.println("$"+StringUtil.formatNumber("#0.00", Double.parseDouble(detailInvoice.getString("uv"))*(order.get("quantity",0d)+totalSonOrderQuantity) ) );
		 }
		 %>		 </td>
       </tr>
    </table>
	 
	 <br>
	 <table width="660" height="97" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse; ">
			
             <tr>
               <td height="71"  align="left" valign="top" bgcolor="#FFFFFF" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                   <td width="14%" align="center"  class="title-bg" valign="middle" style="padding:2px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">QUANTITY</td>
                   <td width="86%"  class="title-bg"  style="padding:5px;border-bottom:1px #000000 solid;padding-left:10px;">ITEM</td>
                 </tr>
<%
if (orderItems.length>0)
{
	for (int zz=0; zz<orderItems.length; zz++)
	{
%>
                 <tr>
                   <td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;">
				   			<%
					out.println(orderItems[zz].get("quantity",0f)+orderItems[zz].getString("unit_name")+"<br>");
					%>	
				   </td>

                   <td  style="padding:2px;padding-left:10px;">
				   <%
				   	if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&orderItems[zz].get("lacking",0)==1)
					{
						out.println("<span style='color:#FF0000;'> [缺货]</span>");
					}
			
					
					if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println("<");
					}
					out.println("Part# "+orderItems[zz].getString("pid"));
					if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(">");
					}
					
					
				   %>
				   
				   </td>
                 </tr>
<%
	}	
%>

<%
	for (int zz=0; zz<sonOrderItems.length; zz++)
	{
%>
				   <tr>
                   <td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;">
				   <%
					out.println("<span style='color:#000000'>"+sonOrderItems[zz].get("quantity",0f)+sonOrderItems[zz].getString("unit_name")+"</span><br>");
				   %>
				   </td>
				   
				    <td  style="padding:2px;padding-left:10px;">
					<%
						if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&sonOrderItems[zz].get("lacking",0)==1)
						{
							out.println("<span style='color:#FF0000;'> [缺货]</span>");
						}
				
						if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
						{
							out.println("<");
						}
						 out.println("Part# "+sonOrderItems[zz].getString("pid"));
						if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
						{
							out.println(">");
						}
					
					%>
				   </td>
                 </tr>   
				 
<%
}
}
%>





               </table></td>
             </tr>
    </table>
	 <table width="660" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="right" style="font-size:10px;">
		I declare that the above information is true and correct to the best of my knowledge.
		</td>
      </tr>
    </table>
</div>
</div>
</div>
	<script>
createContainer("invoiceContainer",15,50,invoiceWidth,invoiceHeight);
setComponentPos('invoice_title',10,10);
setComponentPos('invoice_title2',155,10);

setComponentPos('invoice_line',11,22);
setComponentPos('invoice_table3',11,29);
setComponentPos('invoice_table2',101,29);
setComponentPos('invoice_table1',11,60);
setComponentPos('invoice_table4',11,85);

</script>
<!--- 发票结束 -->






<div id="productList">
<input name="Submit" type="button" class="short-short-button-print" onClick="printProductList()" value="打印">

<div id="productListContainer">
<style>
td
{
	font-size:11px;
	font-family:Arial, Helvetica, sans-serif;
}
.barcode2
{
	font-family: C39HrP48DmTt;font-size:30px;
}
.barcode
{
	font-family: C39HrP24DlTt;font-size:32px;
}
</style>





<div id="productlist_invoice_table4">

	 <table width="660" border="0" cellpadding="0" cellspacing="0">
	 
	 <thead>
  <tr>
    <td colspan="4" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="52%">
		<div style="margin-left:10px;font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>
		&nbsp;<span style="font-size:12px;color:#999999">WWW.VVME.COM</span></div>		</td>
        <td width="48%" align="right"><table width="224" height="30" border="0" cellpadding="2" cellspacing="0">
          <tr>
            <td align="right" ><strong>Order NO</strong>: <%=order.getString("oid")%></td>
          </tr>
          <tr>
            <td align="right" >
             <img src="/barbecue/barcode?data=<%=order.getString("oid")%>&width=1&height=35&type=code39" />
            </td>
          </tr>
        </table></td>
      </tr>
	  
	        <tr>
	          <td>&nbsp;</td>
	          <td align="right">&nbsp;</td>
	          </tr>
	        <tr>
    </table></td>
    </tr>
			   <%
if ( order.getString("delivery_note").equals("")==false||sonOrderDeliveryNotes.length>0 )
{
%>
  <tr>
    <td colspan="4" style="padding-bottom:5px;">
	* NOTE:
	<%=order.getString("delivery_note")%>
<%
if (sonOrderDeliveryNotes.length>0)
{
	for (int dii=0; dii<sonOrderDeliveryNotes.length; dii++)
	{
		if (sonOrderDeliveryNotes[dii].getString("delivery_note").equals(""))
		{
			continue;
		}
		out.println("<br>"+sonOrderDeliveryNotes[dii].getString("delivery_note"));
	}
}
%>	</td>
    </tr>
<%
}
%>
  <tr>
    <td colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;border:1px #000000 solid;background:#000000;color:#FFFFFF;">QTY.</td>
    <td bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;padding-left:5px;bold;border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;">ITEM</td>
    <td bgcolor="#FFFFFF" style="border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;font-family:Arial;padding-left:5px;font-weight:bold">PARTS</td>
  </tr>
  </thead>
  

  <%
for (int zz=0; zz<orderItems.length; zz++)
{
%>
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;">	   			<%
					out.println(orderItems[zz].get("quantity",0f));
					%>	</td>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;border-left:0px;">
			   			<%
					if (orderItems[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(orderItems[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:12px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
				   	if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&orderItems[zz].get("lacking",0)==1)
					{
						out.println("<span style='color:#FF0000;'> [缺货]</span>");
					}
			
					
					out.println(orderItems[zz].getString("name"));
					if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(" ●");
					}
					

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(orderItems[zz].get("pid",0l));//获得定制套装原来标准套装的ID
						DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
						ArrayList oldUnionProsList = new ArrayList();//原来套装所包含的商品
						HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
						for (int kk=0; kk<oldUnionPros.length; kk++)
						{
							oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
							oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
						}
						
						DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( orderItems[zz].get("pid",0l) );
						for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
						{
							out.println("<tr>");
	
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
							
							//如果商品不存在于原来的套装之中，则加*提示
							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
							{
								out.println(" *");
							}
							
							out.println("</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
							
							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
							{
								out.println(" *");
							}

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(orderItems[zz].get("pid",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
						{
							out.println("<tr>");
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</tbody>
<%
}
%> 



 <%
	long preOid = 0;
	for (int zz=0; zz<sonOrderItems.length; zz++)
	{
		if (sonOrderItems[zz].get("oid",0l)!=preOid)
		{
			total_mc_gross += orderMgr.getDetailPOrderByOid(sonOrderItems[zz].get("oid",0l)).get("mc_gross",0d);
		}
		preOid = sonOrderItems[zz].get("oid",0l);
%>
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;border:1px #000000 solid;border-top:0px;">
					   <%
					out.println(sonOrderItems[zz].get("quantity",0f));
				   %>		</td>
    <td width="29" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:11px;border:1px #000000 solid;border-top:0px;;border-left:0px;">
		   			<%
					if (sonOrderItems[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(sonOrderItems[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:11px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
						if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&sonOrderItems[zz].get("lacking",0)==1)
						{
							out.println("<span style='color:#FF0000;'> [缺货]</span>");
						}
				
						out.println(sonOrderItems[zz].getString("name"));
						if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
						{
							out.println(" ●");
						}		

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
							out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
							DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(sonOrderItems[zz].get("pid",0l));//获得定制套装原来标准套装的ID
							DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
							ArrayList oldUnionProsList = new ArrayList();
							HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
							for (int kk=0; kk<oldUnionPros.length; kk++)
							{
								oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
								oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
							}
						
							DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( sonOrderItems[zz].get("pid",0l) );
							for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
							{
							out.println("<tr>");
	
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));

							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
							{
								out.println(" *");
							}
							
							out.println("</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));

							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
							{
								out.println(" *");
							}
							
							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(sonOrderItems[zz].get("pid",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
						{
							out.println("<tr>");
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</tbody>
<%
}
%> 






  <tfoot>  
  <tr>
    <td colspan="4" bgcolor="#FFFFFF">

      <table width="660" border="0" cellspacing="0" cellpadding="3">
       <tr>
         <td width="453" align="left" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">EW: 
		 <%=MoneyUtil.round(orderMgr.calculateWayBillWeight4ST(total_weight_org), 2)%>Kg		</td>
         <td width="454" align="right" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">
		  Total Amount:$<%=MoneyUtil.round(total_mc_gross,2)%>		 </td>
       </tr>
     </table>	</td>
  </tr>
  </tfoot>
</table>

	 
</div>
</div></div>
	<script>
createContainer("productListContainer",15,50,productListWidth,productListHeight);

setComponentPos('productlist_invoice_table4',8,10);

</script>



</div>



<!--- tab结束 -->
</div></div>
	

	
	</td>
  </tr>
</table>




<script>
//提交疑问成本订单
function promptDoubtCost()
{
	$.prompt(
	
	"<div id='title'>亏本订单[单号<%=oid%>]</div><br />是否提交客服处理？",
	
	{
		  submit:
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						var para = "oid=<%=oid%>&sc_id=<%=sc_id%>&ccid=<%=ccid%>";
					
						$.ajax({
							url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markDoubtCostAjax.action',
							type: 'post',
							dataType: 'html',
							timeout: 60000,
							cache:false,
							data:para,
							
							beforeSend:function(request){
							},
							
							error: function(){
								alert("网络错误，请重试！");
							},
							
							success: function(html){
								
								if (html=="ok")
								{
									window.close();
								}
								else
								{
									alert("提交失败！");
								}
								
							}
						});
						
						return(false);

					}
					else if (v=="n")
					{
						window.close();
					}
				}
			,		  
		  
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 关闭打印: "n" }
	});
}

if ( "<%=promptFlag%>"=="true" )
{
	promptDoubtCost();
}

	$("#tabs").tabs({	
		cookie: { expires: 30000 } 
	});
</script>









</body>
</html>
