<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="com.cwc.fedex.FedexClient"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.lang.Math"%>
<%@ page import="com.cwc.shipping.ShippingInfoBean"%>
<%@ include file="../../../include.jsp"%>
<%@ page import="com.cwc.usps.UspsResponseErrorException"%>
<%
response.setHeader("Pragma","No-cache");   
response.setHeader("Cache-Control","no-cache");   
response.setDateHeader("Expires", 0);  

long waybill_id = StringUtil.getLong(request,"waybill_id");
DBRow waybillOrder = wayBillMgrZJ.getDetailInfoWayBillById(waybill_id);

DBRow[] waybill_items = wayBillMgrZJ.getWayBillOrderItems(waybill_id);


HashMap countryCodeHM = orderMgr.getAllCountryCodeHM();

long ccid = Long.parseLong(waybillOrder.getString("ccid"));   //StringUtil.getLong(request,"ccid");
long sc_id = Long.parseLong(waybillOrder.getString("sc_id"));  //StringUtil.getLong(request,"sc_id");


//获得订单锁
		

 
DBRow detailInvoice; 

long pro_id = waybillOrder.get("pro_id",0l);

 

 


String wayBillNo = waybillOrder.getString("tracking_number");
//提交到USPS服务器
boolean uspsResponseError = false;
String uspsResponseErrorDescription = "";
 
%>
<html>
<head>
<title>打印FEDEX</title>	
<script>
var invoiceWidth = 210;
var invoiceHeight = 297;
var productListWidth = 210;
var productListHeight = 220;
var waybillWidth = 94;
var waybillHeight = 145;
function closeWindowAndRefresh(){
	$.artDialog.opener.refresh && $.artDialog.opener.refresh();
	$.artDialog.close();
}

function printWayBill()
{
		var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
		var printer = "LabelPrinter";
		
		var printerExist = "false";
		
		for(var i = 0;i<printer_count;i++)
		{
			if(printer==visionariPrinter.GET_PRINTER_NAME(i))
			{
				printerExist = "true";
				break;
			}
		}
		
		if(printerExist=="false")	
		{
			printer = visionariPrinter.SELECT_PRINTER()
			if(printer !=-1)
			{
				visionariPrinter.PRINT_INITA("3mm",0,"102mm","152mm","打印运单");
				visionariPrinter.SET_PRINT_COPIES(1);
				visionariPrinter.SET_PRINTER_INDEX(printer);
				visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
				//visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("wayBillContainer").innerHTML);
				visionariPrinter.ADD_PRINT_IMAGE(0,0,800,1200,"<img src='../FedexPrintLabel/shipping_label.<%=wayBillNo%>_0.png' border=0>");
				visionariPrinter.SET_PRINT_STYLEA(1,"Stretch",2);
					
				if (visionariPrinter.PRINT())
				{
					 window.setTimeout(function(){  
		    	            window.self.focus();  
		    	     },2000); 
					//更新运单号
					$.ajax({
							url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/printWayBill.action',
							type: 'post',
							dataType: 'html',
							timeout: 30000,
							cache:false,
							data:"waybill_id=<%=waybill_id%>",
											
						beforeSend:function(request){
						},
											
						error: function(){
							alert("打印运单失败，请重新打印");
						},
											
						success: function(msg){
							if (msg!=0)
							{
								alert("打印运单失败，请重新打印");
							}
						}
					});
						
					return(true);
				}
			}
		}
		else
		{
			visionariPrinter.PRINT_INITA("3mm",0,"102mm","152mm","打印运单");
			visionariPrinter.SET_PRINT_COPIES(1);
			visionariPrinter.SET_PRINTER_INDEX(printer);
			visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
			//visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("wayBillContainer").innerHTML);
			visionariPrinter.ADD_PRINT_IMAGE(0,0,800,1200,"<img src='../FedexPrintLabel/shipping_label.<%=wayBillNo%>_0.png' border=0>");
			visionariPrinter.SET_PRINT_STYLEA(1,"Stretch",2);
				
			if (visionariPrinter.PRINT())
			{
				 window.setTimeout(function(){  
	    	            window.self.focus();  
	    	     },2000); 
				//更新运单号
				$.ajax({
						url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/printWayBill.action',
						type: 'post',
						dataType: 'html',
						timeout: 30000,
						cache:false,
						data:"waybill_id=<%=waybill_id%>",
										
					beforeSend:function(request){
					},
										
					error: function(){
						alert("打印运单失败，请重新打印");
					},
										
					success: function(msg){
						if (msg!=0)
						{
							alert("打印运单失败，请重新打印");
						}
					}
				});
					
				return(true);
			}	
		}	
}

function printInvoice()
{	
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
	visionariPrinter.SET_PRINT_COPIES(2);
	visionariPrinter.SET_PRINTER_INDEX("LaserPrinter");
	visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("invoiceContainer").innerHTML);
	return( visionariPrinter.PRINT() );
}

function printProductList(){
	//获取打印机名字列表
	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
	 //判断是否有该名字的打印机
	var printer = "LabelPrinter";
	var printerExist = "false";
	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){
		//判断该打印机里是否配置了纸张大小
		var paper="102X152";
	    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
	    var str=strResult.split(",");
	    var status=false;
	    for(var i=0;i<str.length;i++){
               if(str[i]==paper){
                  status=true;
               }
		}
       if(status==true){
    	     visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","装箱单");
	         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");	
	         visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  	       
	         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","125mm",document.getElementById("zhuangxiang").innerHTML);
	         visionariPrinter.ADD_PRINT_HTM("141mm",0,"100%","100%",document.getElementById("foot").innerHTML);
	         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
	         visionariPrinter.ADD_PRINT_TEXT(550,100,"100%","100%","Page:#/&");
	         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);
	    	 visionariPrinter.SET_PRINT_COPIES(1);//打印分数
    		 visionariPrinter.PRINT();  //PREVIEW
    			
       }else{
        	$.artDialog.confirm("打印机配置纸张大小里没有符合的大小.....", function(){
				 this.close();
    			}, function(){
			});
       }	
 	}else{
 		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消
			 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","装箱单");
	         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");	       
	         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","125mm",document.getElementById("zhuangxiang").innerHTML);
	         visionariPrinter.ADD_PRINT_HTM("141mm",0,"100%","100%",document.getElementById("foot").innerHTML);
	         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
	         visionariPrinter.ADD_PRINT_TEXT(550,100,"100%","100%","Page:#/&");
	         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);
	    	 visionariPrinter.SET_PRINT_COPIES(1);//打印分数
			 visionariPrinter.PRINT();
		}		
 	}
}

function mergePrint(){
	var op=false;
	if (printWayBill()){
		printProductList();
		op=true;
	}
	return op;
}



function promptAlert()
{
	$.prompt("<div style='font-size:18px;font-weight:bold;color:red'>订单:"+document.print_form.oid.value+" 打印失败</div>请关闭窗口，重新打印！",
	{
		  submit:  function(){},
		  loaded: function(){},
		  callback: function(){},
		  overlayspeed:"fast",
		  buttons: { 取消: "n" }
	});
}

var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}  


function printVerifyAddress(oid)
{
	$.prompt(
	
	"<div id='title'>疑问地址提交客服处理[订单:"+oid+"]</div><br /><textarea name='verifyAddressNoteWinText' id='verifyAddressNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.verifyAddressNoteWinText == "")
						  {
							   alert("请填写备注");						
						  }
						  else
						  {
						  		var verify_address_note = "<%=uspsResponseErrorDescription%>\n"+f.verifyAddressNoteWinText;
								
								var para = "oid="+oid+"&verify_address_note="+verify_address_note;
							
								$.ajax({
									url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/printVerifyAddressAjax.action',
									type: 'post',
									dataType: 'html',
									timeout: 30000,
									cache:false,
									data:para,
									
									beforeSend:function(request){
									},
									
									error: function(){
										alert("提交出错，请重试！");
									},
									
									success: function(html){
										if (html=="error")
										{
											alert("提交出错，请重试！");
										}
										else
										{
											window.close();
										}
									}
								});
						  }
						  
						  return false;
					}
					
					 return true;
					
				}
		  ,
   		  loaded:
				function ()
				{

				}
		  ,

		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}
</script>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive();" style="background:#ffffff">
 <form name="finish_print_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/finish_print.html">
		<input type="hidden" id="waybill_id_" name="waybill_id" value="<%=waybill_id%>">
		<input type="hidden" name="rePrint" value="<%=StringUtil.getCurrentURI(request)+"?waybill_id"+waybill_id%>">
 </form>
 <table width="923" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td >
		
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
	        <td width="30%" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算重量: <%=waybillOrder.getString("all_weight")%>Kg
			</td>
			<td width="30%">
			<%
		 		 String[] array = {"待打印","已打印","已发货","取消","拆分中"};
		 		 String wayBillState =  array[waybillOrder.get("status", 0)];
		 	%>
		 		<% if(!wayBillState.equals("待打印")){%>
					<span style="font-weight:bold;font-size:12px;">注意  : &nbsp;该运单 [ <%= waybillOrder.getString("waybill_id")%> ]   <span style="color:red;"><%= wayBillState%></span></span>
				<%}%>
			</td>
	        <td width="40%"   align="right">
			<tst:authentication bindAction="com.cwc.app.api.zj.WayBillMgrZJ.printWayBill">
        		<input name="Submit2" type="button" class="long-button-print" onClick="mergePrint()" value="联合打印">
        	</tst:authentication>
		</td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算运费: ￥<%=waybillOrder.getString("shipping_cost")%></td>
        </tr>
    </table>
		
		
		</td>
      </tr>
      <tr>
        <td>


<div class="demo" >
<div id="tabs">
<ul>
		<li><a href="#waybill">打印运单</a></li>
		
		<li><a href="#productList">装箱单</a></li>	 
</ul>


<div id="waybill">
<tst:authentication bindAction="com.cwc.app.api.zj.WayBillMgrZJ.printWayBill">
<input name="Submit" type="button" class="short-short-button-print" onClick="printWayBill()" value="打印">
</tst:authentication>

<div id="wayBillContainer" > 
	<div id="print_label"><img src='../FedexPrintLabel/shipping_label.<%=wayBillNo%>_0.png' border="0" style="width:352px;"></div>
</div>

<script>
createContainer("wayBillContainer",15,50,waybillWidth,waybillHeight);
setComponentPos('print_label',0,0);
</script>
</div>



<div id="productList">


<div id="productList">
<input name="Submit" type="button" class="short-short-button-print" onClick="printProductList()" value="打印">
<%
	float weight = waybillOrder.get("all_weight",0.0f);
	double tmp = MoneyUtil.round(weight,3) * 1000;
	int tmp2 = (int)tmp;
	String tmp3 = String.valueOf(tmp2);
	String orgStr = "00000";
	orgStr = orgStr.substring(0,orgStr.length()-tmp3.length());
%>	

<div id="zhuangxiang" style="display:block; margin:0px auto; width:368px; border:1px red solid">
    <table width="368px"  border="0" cellspacing="0" cellpadding="0" >
		<thead>
			<tr>
			    <td colspan="4" style="border-left:0px;">
			    	<div style="width:50%; float:left">
			    		<span style="font-size:22px; font-weight: bold; font-family:Arial">Visionari LLC</span><br/>
				        <span style="font-size:10px;color:#999999; font-family:Arial">WWW.VVME.COM</span><br/>
				    	<span style="font-size:12px; font-family:Arial;word-break:break-all">FEDEX#<%=waybillOrder.getString("tracking_number")%></span><br/>
				    	<span style="font-size:10px; font-family:Arial"><%=DateUtil.NowStr()%></span>
			    	</div>
			    	<div style="width:50%; float:left; line-height: 20px;" align="center">
			    		<span style="font-size:12px; font-family:Arial">WayBill NO: <%=waybillOrder.getString("waybill_id")%></span><br/>
					    <img src="/barbecue/barcode?data=<%=waybillOrder.getString("waybill_id")%>&width=1&height=38&type=code39" />
			    	</div>   
			    </td>
			</tr>
		 </thead>
		 <% for (int zz=0; zz<waybill_items.length; zz++){%>
		    <%if(zz==0){%>
			    <tr>
				    <td  align="left" style="border-left:1px #000 solid;border-right:1px #000 solid; border-bottom:1px #000 solid;background-color: #CCCCCC;border-top:1px #000 solid;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial"><%=waybill_items[zz].getString("pc_id") %></span>
				    </td>	
				    <td align="left" width="230px;" style="background-color: #CCCCCC;border-right:1px #000 solid; border-bottom:1px #000 solid;word-break:break-all;border-top:1px #000 solid;">
				   		 <span style="font-size:12px; font-weight: bold;  font-family:Arial">
				   		   <%
								out.println(waybill_items[zz].getString("p_name"));
								if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL){
									out.println(" ●");
								}
							%>
				   		 </span>
				    </td>	
					<td width="30px;" align="center" style="border-top:1px #000 solid;background-color: #CCCCCC; border-bottom:1px #000 solid;border-top:1px #000 solid;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial;"><%out.println(waybill_items[zz].get("quantity",0f));%></span>
				    </td>   
				    <td align="center" style="background-color: #CCCCCC;border-left:1px #000 solid; border-right:1px #000 solid; border-bottom:1px #000 solid;border-top:1px #000 solid;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial">
					    	<%
								if (waybill_items[zz].getString("unit_name").equals("")){
									out.println("&nbsp;");
								}else{
									out.println(waybill_items[zz].getString("unit_name"));
								}
						     %>	
				    	</span>
				    </td>
				</tr>
		    <%}else{%>
			    <tr>
				    <td align="left" width="50px;" style="border-left:1px #000 solid;border-right:1px #000 solid; border-bottom:1px #000 solid;background-color: #CCCCCC;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial"><%=waybill_items[zz].getString("pc_id") %></span>
				    </td>	
				    <td align="left" width="230px;" style="background-color: #CCCCCC;border-right:1px #000 solid; border-bottom:1px #000 solid;word-break:break-all;">
				   		 <span style="font-size:12px; font-weight: bold;  font-family:Arial">
				   		   <%
								out.println(waybill_items[zz].getString("p_name"));
								if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL){
									out.println(" ●");
								}
								out.println("<br/>");
							%>
				   		 </span>
				    </td>	
					<td  align="center" style="background-color: #CCCCCC; border-bottom:1px #000 solid;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial;"><%out.println(waybill_items[zz].get("quantity",0f));%></span>
				    </td>   
				    <td align="center" style="background-color: #CCCCCC;border-left:1px #000 solid; border-right:1px #000 solid; border-bottom:1px #000 solid;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial">
					    	<%
								if (waybill_items[zz].getString("unit_name").equals("")){
									out.println("&nbsp;");
								}else{
									out.println(waybill_items[zz].getString("unit_name"));
								}
						     %>	
				    	</span>
				    </td>
				</tr>
		    <%}%>
		   
			<%
			if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM){
						DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(waybill_items[zz].get("pc_id",0l));//获得定制套装原来标准套装的ID
						DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
						ArrayList oldUnionProsList = new ArrayList();//原来套装所包含的商品
						HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
						if(null != oldUnionPros ){
							for (int kk=0; kk<oldUnionPros.length; kk++)
							{
								oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
								oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
							}
						}
						DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( waybill_items[zz].get("pc_id",0l) );
						
						for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++){
							out.println("<tr>");
	
							
							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))){
								out.println("<td align='right' style='border-right:1px #000 solid;border-left:1px #000 solid; border-bottom:1px #000 solid;font-size:11px;'>"+customProductsInSet[customProductsInSet_i].getString("pc_id")+"*</td><td style='border-right:1px #000 solid;border-bottom:1px #000 solid; font-size:9px;word-break:break-all;font-family:Arial;padding-left:10px;' width='230px'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
							}else{
								out.println("<td align='right' style='border-right:1px #000 solid;border-left:1px #000 solid; border-bottom:1px #000 solid;font-size:11px;'>"+customProductsInSet[customProductsInSet_i].getString("pc_id")+"</td><td style='border-right:1px #000 solid;border-bottom:1px #000 solid; font-size:9px;word-break:break-all;font-family:Arial;padding-left:10px;' width='230px'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
							}
							//如果商品不存在于原来的套装之中，则加*提示
							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))){
								out.println(" *");
							}
							out.println("</td>");
							
							out.println("<td colspan='2' align='center' style='border-bottom:1px #000 solid;border-right:1px #000 solid;font-size:10px;font-family:Verdana;'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
							
							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f)){
								out.println(" *");
							}

							out.println("</td></tr>");						
						}
						if(zz==waybill_items.length-1){
					    	  
					    }else{
					    	   out.println("<tr><td colspan='4' height='5px' style='border-bottom:1px #000 solid;'></td></tr>");
					    }
			}else if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL){
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(waybill_items[zz].get("pc_id",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++){
							out.println("<tr>");
							out.println("<td align='right' style='border-right:1px #000 solid;border-left:1px #000 solid;border-bottom:1px #000 solid;font-size:11px;'>"+productsInSet[productsInSet_i].getString("pc_id")+"</td><td style='border-right:1px #000 solid;border-bottom:1px #000 solid;font-size:10px;word-break:break-all;font-family:Arial;padding-left:10px;' width='230px;'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							out.println("<td colspan='2' align='center' style='border-bottom:1px #000 solid;border-right:1px #000 solid;font-size:10px;font-family:Verdana;'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));
							out.println("</td></tr>");
						}
						if(zz==waybill_items.length-1){
					    	  
					    }else{
					    	   out.println("<tr><td colspan='4' height='5px' style='border-bottom:1px #000 solid;'></td></tr>");
					    }
			}else{
				       if(zz==waybill_items.length-1){
				    	   out.println("&nbsp;");
				       }else{
				    	   out.println("<tr><td colspan='4' height='5px' style='border-bottom:1px #000 solid;'></td></tr>");
				       }
						
			}%>
		 <%} %>
	</table>
</div>
<div id="foot" style="display: none">
<div style="border-top:1px #000 solid;">
	<div style=" font-size:11px; font-family:Arial; width:30%; float:left">
		EW: <%=MoneyUtil.round(orderMgr.calculateWayBillWeight4ST(Float.parseFloat(waybillOrder.getString("all_weight"))), 2)%>Kg
	</div>
	<div style="font-size:11px; font-family:Arial; width:70%; padding-top:2px; float:left" align="right">
		<img src="/barbecue/barcode?data=<%="9VVMELA"+orgStr.concat(tmp3)+"9"%>&width=1&height=20&type=code39"/>
	</div>
</div>	
</div>

<div id="productListContainer" style="display: none">
<style>
td
{
	font-size:11px;
	font-family:Arial, Helvetica, sans-serif;
}
.barcode2
{
	font-family: C39HrP48DmTt;font-size:40px;
}
.barcode
{
	font-family: C39HrP24DlTt;font-size:32px;
}
</style>





<div id="productlist_invoice_table4">
	 
<table width="660" border="0" cellpadding="0" cellspacing="0">
	 
	 <thead>
  <tr>
    <td colspan="4" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="52%">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><div style="margin-left:10px;font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>
		&nbsp;<span style="font-size:12px;color:#999999">WWW.VVME.COM</span></div>	</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
 <tr>
            <td style="padding-left:10px;"><strong>AIRWAY BILL NO</strong>: <%=wayBillNo%> </td>
          </tr>
          <tr>
            <td style="padding-left:10px;" >
                <img src="/barbecue/barcode?data=<%=wayBillNo%>&width=1&height=35&type=code39" />
            </td>
          </tr>
          </table></td>
        <td width="48%" align="right"><table width="224" height="30" border="0" cellpadding="2" cellspacing="0">
          <tr>
            <td align="right" ><strong>WayBill NO</strong>: <%=waybillOrder.getString("waybill_id")%></td>
          </tr>
          <tr>
            <td align="right" >
            	<img src="/barbecue/barcode?data=<%=waybillOrder.getString("waybill_id")%>&width=1&height=35&type=code39" />
            </td>
          </tr>
         
          <tr>
            <td align="right" ><strong>VVMELA</strong></td>
          </tr>
          <tr>
            <td align="right" >
            	<img src="/barbecue/barcode?data=<%="9VVMELA"+orgStr.concat(tmp3)+"9"%>&width=1&height=35&type=code39"/>
            </td>
          </tr>
        </table></td>
      </tr>
	  
	        <tr>
	          <td>&nbsp;</td>
	          <td align="right">&nbsp;</td>
	          </tr>
    </table></td>
    </tr>
			   <%
if ( waybillOrder.getString("delivery_note").equals("")==false)
{
%>
  <tr>
    <td colspan="4" style="padding-bottom:5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="74%">
			<strong>* NOTE</strong>:
	<%=waybillOrder.getString("delivery_note")%>
 
		</td>
        <td width="26%" align="right">&nbsp;</td>
      </tr>
    </table></td>
    </tr>
<%
}
%>
  <tr>
    <td colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;border:1px #000000 solid;background:#000000;color:#FFFFFF;">QTY.</td>
    <td bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;padding-left:5px;bold;border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;">ITEM</td>
    <td bgcolor="#FFFFFF" style="border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;font-family:Arial;padding-left:5px;font-weight:bold">PARTS</td>
  </tr>
  </thead>
  <tbody>

  <%
for (int zz=0; zz<waybill_items.length; zz++)
{
%>

  <tr>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;">	   			<%
					out.println(waybill_items[zz].get("quantity",0f));
					%>	</td>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;border-left:0px;">
			   			<%
					if (waybill_items[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(waybill_items[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:12px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
				   	 
			
					
					out.println(waybill_items[zz].getString("p_name"));
					if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(" ●");
					}
					

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(waybill_items[zz].get("pc_id",0l));//获得定制套装原来标准套装的ID
						DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
						ArrayList oldUnionProsList = new ArrayList();//原来套装所包含的商品
						HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
						if(null != oldUnionPros ){
							for (int kk=0; kk<oldUnionPros.length; kk++)
							{
								oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
								oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
							}
						}
						DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( waybill_items[zz].get("pc_id",0l) );
						if(null != customProductsInSet){
							for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
							{
								out.println("<tr>");
		
								out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
								
								//如果商品不存在于原来的套装之中，则加*提示
								if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
								{
									out.println(" *");
								}
								
								out.println("</td>");
								
								out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
								
								if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
								{
									out.println(" *");
								}
	
								out.println("</td></tr>");
							}
							out.println("</table>");
						}
		}
	else if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(waybill_items[zz].get("pc_id",0l));
						if(null != productsInSet){
							for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
							{
								out.println("<tr>");
								out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
								
								out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));
	
								out.println("</td></tr>");
							}
						
							out.println("</table>");
						}
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>

<%
}
%> 

</tbody>

  
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;border:1px #000000 solid;border-top:0px;">
			 
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:11px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
 	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 	</td>
  </tr>
</tbody>

  <tfoot>  
  <tr>
    <td colspan="4" bgcolor="#FFFFFF">

      <table width="660" border="0" cellspacing="0" cellpadding="3">
       <tr>
         <td width="453" align="left" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">EW: 
		 <%=waybillOrder.getString("all_weight")%>Kg		</td>
         <td width="454" align="right" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">
		 </td>
       </tr>
     </table>	</td>
  </tr>
  </tfoot>
</table>

	 
</div>
</div></div>
	<script>
createContainer("productListContainer",15,50,productListWidth,productListHeight);

setComponentPos('productlist_invoice_table4',8,10);

</script>
</div>
</div></div>
		</td>
      </tr>
    </table>


<script>
$("#tabs").tabs({	
});
</script>
</body>
</html>
