<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.WaybillInternalOperKey"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.key.WaybillTrackeTypeKey"%>
<%@page import="com.cwc.app.key.SystemTrackingKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../../include.jsp"%> 

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.autocomplete.css" />

<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<%
     //根据出库单id 查询运单
     long out_id=StringUtil.getLong(request,"out_id");
     long out_for_type=StringUtil.getLong(request,"out_for_type");  //出库单 运单状态
     String create_time=StringUtil.getString(request,"create_time");    //出库单创建时间
	 DBRow[] transport=transportMgrZwb.selectTransportByOutId(out_id);
%>
<script>
function transportPrintOut(out_id){
	visionariPrinter.PRINT_INIT("出库单");
	visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
	visionariPrinter.ADD_PRINT_TBURL("1cm","1cm","90%","90%","../../../waybill/out_transport_list_print.html?out_id="+out_id);
	visionariPrinter.PREVIEW();

	//var ur='<%=ConfigBean.getStringValue("systenFolder")%>waybill/out_transport_list_print.html?out_id='+out_id;
	//$.artDialog.open(ur, {title: '扫描货物打印',width:'900px',height:'500px', lock: true});
}
</script>
</head>
<body>
<fieldset class="set" style="border:2px solid #993300;">
<legend>
	<span style="font-size:20px; font-weight: bold;">拣货单ID：<%=out_id %></span>
</legend>
	<div style="height:20px; line-height:30px; padding-left:5px;">
		<div style="width:580px; float:left;">
			&nbsp;
		</div>
		<div style="width:250px; float:left">
			<input class="long-button-mod"   type="button"  value="打印拣货单"   onclick="transportPrintOut(<%=out_id %>)">
		</div>
	</div>
	<p style="text-align:left;text-indent:5px;font-weight:normal;color:#999; clear: left">
		创建时间：<%=create_time.substring(5,16)%>
	</p>
	<table width="99%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" isNeed="true" isBottom="true" style="margin-top:5px;" >
		<thead>
			<tr>
				<th width="6%"  class="right-title"  style="text-align: center;">转运单号</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">出货仓库</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">收货仓库</th>
			    <th width="16%" class="right-title"  style="text-align: center;">创建时间</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">转运单创建人</th>
			</tr>
		</thead>
	    <tbody>
	     <% if(null != transport && transport.length > 0){  %>
		 <%   for(int i = 0; i<transport.length; i++ ){ %>
	    	<tr id="av<%=transport[i].get("transport_id",0l) %>" >
	    		<td align="center" height="25px;">
	    			<span style="font-size:13px; font-weight: bold;">
	    				<%=transport[i].get("transport_id",0l)%>
	    			</span>
	    		</td>
	    		<td align="center"><%=transport[i].get("send_psid",0l)%></td>
	    		<td align="center"><%=transport[i].get("receive_psid",0l)%></td>
	    		<td align="center"><%=transport[i].getString("transport_date").substring(5,16)%></td>
	    		<td align="center"><%=transport[i].getString("create_account")%></td>
	    	</tr>
	     <%}} %>
	    </tbody>
	</table>
</fieldset>	
<div id="printTitle" style="display:none">

</div>
</body>
</html>