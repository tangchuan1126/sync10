<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.SystemConfig"%>
<%@page import="com.cwc.app.key.WayBillFromKey"%>
<%@ include file="../../../include.jsp"%>
<%@ page import="com.cwc.app.order.print.ExpressPrintBean"%>
<%
long oid = StringUtil.getLong(request,"oid");
long handle = StringUtil.getLong(request,"handle");
long handle_status = StringUtil.getLong(request,"handle_status");
long invoice_id = StringUtil.getLong(request,"invoice_id");
long ccid = StringUtil.getLong(request,"ccid");
long pro_id = StringUtil.getLong(request,"pro_id");
long ps_id = StringUtil.getLong(request,"ps_id");


ExpressPrintBean expressPrintBean[] = (ExpressPrintBean[])request.getAttribute("expressPrintBean");

ExpressPrintBean noSelectExpressPrintBean[] = (ExpressPrintBean[])request.getAttribute("noSelectExpressPrintBean");
String islacking = (String)request.getAttribute("islacking");
//long ps_id = Long.parseLong(request.getAttribute("ps_id").toString());

//排序，找出价格最低的快递
long minPriceScId = 0;
double minPrice = 0; 

for (int i=0; i<expressPrintBean.length; i++)
{
	if(expressPrintBean[i].getShippingFee()>0)
	{
		if (i==0||!(minPrice>0))
		{
				minPrice = expressPrintBean[i].getShippingFee();
				minPriceScId = expressPrintBean[i].getScID();
				continue;
		}
		
		if (expressPrintBean[i].getShippingFee()<minPrice)
		{
			minPrice = expressPrintBean[i].getShippingFee();
			minPriceScId = expressPrintBean[i].getScID();
		}
	}
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>请选择打印快递</title>
<style>
*{font-family: Arial,Helvetica,sans-serif;padding:0px;margin:0px;font-size:12px;}
.print-button
{
	width:105px;
	height:45px;
	font-size:20px;
	font-weight:bold;
	font-family:Arial, Helvetica, sans-serif;
	border:2px #999999 solid;
	background:#f8f8f8;
	color:#666666;
	float:left;
	margin-right:10px;
	cursor: pointer;
}

form
{
	padding:0px;
	margin:0px;
}

.warning
{
	border-bottom:1px #cccccc solid;background:#FFFFCC;color:#666666;font-size:15px;font-weight:bold;
}

.error
{
	border-bottom:1px #cccccc solid;background:#CC0000;color:#ffffff;font-size:15px;font-weight:bold;
}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
	.rad{ border-bottom-right-radius: 4px;border-bottom-left-radius: 4px; border-top-right-radius: 4px; border-top-left-radius: 4px;}
	div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:400px;height:30px;background:white;margin-top:60px;line-height:30px;font-size:14px;}
	
	div.fee{font-size:12px;cursor:pointer;display:block;height:35px;line-height:35px;text-align:center;font-weight:bold;border:1px solid silver;width:140px;float:left;margin-left:5px;margin-top:2px;background:#f8f8f8;}
	div.fee:hover{background:white;}
	div.fee:visited{background:none;}
	div.fee.feeon{background:#f60;color:white;}
	
	div.noselect{font-size:12px;cursor:pointer;display:block;height:35px;line-height:35px;text-align:center;font-weight:bold;border:1px solid silver;width:140px;float:left;margin-left:5px;margin-top:2px;background:#FF33CC;}
	div.noselect:hover{background:white;}
	div.noselect:visited{background:none;}
	div.noselect.feeon{background:#f60;color:white;}
</style>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../../js/select.js"></script>

<link rel="stylesheet" type="text/css" href="../../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<script>
function printIt(sc_id,printPage)
{
	document.print_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/"+printPage;
	document.print_form.sc_id.value = sc_id;
	document.print_form.submit();
}

function isNum(keyW)
{
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 

function checkInvoice(inv_uv,inv_tv,inv_dog,inv_rfe)
{
	if (inv_uv==""&&inv_tv==""&&inv_dog==""&&inv_rfe=="")
	{
		if ($("#invoice_product_type").val()==0)
		{
			alert("请选择一个发票模板");
			return(true);
		}
	}
	else
	{
		if(inv_uv==""&&inv_tv=="")
		{
			alert("Unit Value和Total Value不能同时为空！");
			return(true);
		}
		else if ( inv_uv!=""&&!isNum(inv_uv) )
		{
			alert("请正确填写Unit Value！");
			return(true);
		}
		else if(inv_dog=="")
		{
			alert("Description of Goods不能为空！");
			return(true);
		}
		else if(inv_rfe=="")
		{
			alert("Reason for Export不能为空！");
			return(true);
		}
		else if($("#inv_dog_chinese").val()==""||$("#inv_rfe_chinese").val()==""||$("#material_chinese").val()=="")
		{
			alert("发票中文描述异常为空,请重新选择发票");
			return(true);
		}
	}
	return(false);
}

 function isIntNum(keyW)
{
	var reg=  /^([1-9]|[1-9][0-9])$/;
	return(reg.test(keyW));
 } 

function printOneAjax(sc_id,printPage,useType,shipping_cost)//一票一件
{
	if(checkInvoice($("#inv_uv").val(),$("#inv_tv").val(),$("#inv_dog").val(),$("#inv_rfe").val()))
	{
	
	}
	else
	{
		if(!parseFloat(shipping_cost)>0)
		{
			$.prompt(
			"<div id='title'>输入运费</div><br>&nbsp;&nbsp;所用运费 &nbsp;&nbsp;&nbsp;<input name='proShippingCost' type='text' id='proShippingCost' value='0.00' style='width:200px;'>格式：0.00",
			{
			      submit: 
						function (v,m,f)
						{
							if (v=="y")
							{
								if (!isNum(f.proShippingCost))
								{
									alert("请正确输入运费");
									return(false);
								}
							}
						}
				  ,
		   		  loaded:
				  
						function()
						{
							
						}
				  
				  ,
				  callback: 
						function (v,m,f)
						{
							if (v=="y")
							{
								printAjax(sc_id,printPage,useType,f.proShippingCost,1,"recipient");
							}
						}
				  ,
				  overlayspeed:"fast",
				  buttons: { 增加: "y", 取消: "n" }
			});
		}
		else
		{
			printAjax(sc_id,printPage,useType,shipping_cost,1,"recipient");
		}	
	}
}
function printMoreAjax(sc_id,printPage,useType,shipping_cost)//一票多件
{	
	if(checkInvoice($("#inv_uv").val(),$("#inv_tv").val(),$("#inv_dog").val(),$("#inv_rfe").val()))
	{
	
	}
	else
	{
		if(sc_id==100021||sc_id==100022||sc_id==100002||sc_id==100143||sc_id==100023||sc_id==100024)
		{
			$.prompt(
			"<div id='title'>一票多件</div><br>&nbsp;&nbsp;一票几件 &nbsp;&nbsp;&nbsp;<input name='proPkcount' type='text' id='proPkcount' value='1' style='width:200px;'>格式：1-99<br/><br/>&nbsp;&nbsp;支付关税&nbsp;&nbsp;&nbsp;<select id='proDTP' name='proDTP'><option value='sender'>Sender 发件人</option><option selected='selected' value='recipient'>Recipient 收件人</otpion></select><br/><br/>&nbsp;&nbsp;英文备注<br/><textarea cols='55' id='english_note' name='english_note'></textarea>",
			{
			      submit: 
						function (v,m,f)
						{
							if (v=="y")
							{
								if (!isIntNum(f.proPkcount))
								{
									alert("请正确输入件数");
									return(false);
								}
							}
						}
				  ,
		   		  loaded:
				  
						function ()
						{
							
						}
				  
				  ,
				  callback: 
				  
						function (v,m,f)
						{
							if (v=="y")
							{
								printAjax(sc_id,printPage,useType,shipping_cost,f.proPkcount,f.proDTP,f.english_note);
							}
						}
				  ,
				  overlayspeed:"fast",
				  buttons: { 增加: "y", 取消: "n" }
			});
		}
	}
}

function printItAjax(sc_id,printPage,useType,shipping_cost,_this,event)
{
	// 让选中的 高亮
	var div = $(_this);
	var divs = $(".rad").removeClass("feeon");
	div.addClass("feeon");
	var islacking = '<%=islacking%>';
	
	var isCommunication = "true";
	
	if(islacking=="true")
	{
		if(!confirm("运单缺货，生成运单后需提出拆货要求"))
		{
			isCommunication  = "false";
		}
	}
	
	if(isCommunication=="true")
	{
		if(event.button ==2)//右键
		{
			printMoreAjax(sc_id,printPage,useType,shipping_cost);
		}
		else//认为是左键
		{
			printOneAjax(sc_id,printPage,useType,shipping_cost);
		}
	}
}

function printAjax(sc_id,printPage,useType,shipping_cost,pkcount,dtp,english_note)
{ 
	 	$(".cssDivschedule").css("display","block");
 
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/'+printPage,
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				async:false,
				data:
				{
					waybill_from_type:<%=WayBillFromKey.Record%>,
					dtp:dtp,
					english_note:english_note,
					material:$("#material").val(),
					inv_di_id:$("#inv_di_id").val(),
					delivery_note:$("#delivery_note").val(),
					shipping_cost:shipping_cost,
					mail_piece_shape:useType,
					oid:<%=oid%>,
					handle:<%=handle%>,
					handle_status:<%=handle_status%>,
					invoice_id:$("#inv_di_id").val(),
					ccid:<%=ccid%>,
					pro_id:<%=pro_id%>,
					sc_id:sc_id,
					ps_id:<%=ps_id%>,
					inv_dog:$("#inv_dog").val(),
					inv_rfe:$("#inv_rfe").val(),
					inv_uv:$("#inv_uv").val(),
					inv_tv:$("#inv_tv").val(),
					hs_code:$("#hs_code").val(),
					pkcount:pkcount,
					inv_dog_chinese:$("#inv_dog_chinese").val(),
					inv_rfe_chinese:$("#inv_rfe_chinese").val(),
					material_chinese:$("#material_chinese").val()
				},
				
				beforeSend:function(request){
					//$.blockUI({ message: '<img src="../../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">保存中...</span>' });
				},
				
				error: function(data){
					alert(data);
				},
				
				success: function(data)
				{
					var msg = data.msg;
					$(".cssDivschedule").fadeOut("slow");
					if(msg=="ok")
					{
						$.artDialog.opener.closeTBWin();
						$.artDialog.close();
						
						//parent.closeTBWin();
					}
					else if(msg=="stockout")
					{
						$.artDialog.opener.closeTBWin();
						$.artDialog.close();
						//window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/split_advice_order.html?waybill_id="+data.waybill_id+"&ps_id=<%=ps_id%>"
					}					
					else if(msg=="close")
					{
						parent.closeTBWinRefrech();
					}
					else
					{
						if(msg.indexOf(".xml")>-1)
						{
							window.location.href = msg;
						}
						else
						{
							alert(msg);
							$.artDialog.opener.closeTBWin();
							$.artDialog.close();
							
							//parent.closeTBWin();
						}
						
					}
				}
			});
}

function changeInvoice(obj)
{
	if(obj.value !=0)
	{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailInvoiceTemplateJSON.action",
			{invoice_id:obj.value},
			function callback(data){   
				$("#inv_dog").val(data.dog);
				$("#inv_rfe").val(data.rfe);
				$("#inv_uv").val(data.uv);
				$("#inv_tv").val(data.tv);
				$("#hs_code").val(data.hs_code);
				$("#inv_di_id").val(data.di_id);
				$("#material").val(data.material);
				
				$("#inv_dog_chinese").val(data.dog_chinese);
				$("#inv_rfe_chinese").val(data.rfe_chinese);
				$("#material_chinese").val(data.material_chinese);
				
				var delivery = "Company:<font size=\"2px;\">"+data.companyname+"</font><br/>Address:<font size=\"2px;\">"+data.addressline1+"&nbsp;&nbsp;"+data.addressline2+"&nbsp;&nbsp;"+data.addressline3+"</font><br/>City/Country:<font size=\"2px;\">"+data.city+"/"+data.countryname+"</font>&nbsp;&nbsp;&nbsp;PhoneNumber:<font size=\"2px;\">"+data.phonenumber+"</font>";
				
				$("#delivery_info").html(delivery);
			}
		);
	}
}

function getInvoiceByPsid(ps_id)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/getInvoiceByPsIdJSON.action",
				{ps_id:ps_id},
				function callback(data)
				{  
					if (data!="")
					{
						$("#invoice_product_type").clearAll();
						$("#invoice_product_type").addOption("请选择...",0);
						$.each(data,function(i){
							$("#invoice_product_type").addOption(data[i].t_name,data[i].invoice_id);
						});
					}
				}
		);
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" oncontextmenu="return false;" >
<div class="cssDivschedule">
		<div class="innerDiv">
 			正在生成运单.......
 		</div>
</div>
<form name="print_form" method="post" action="">
<input type="hidden" name="oid" value="<%=oid%>">
<input type="hidden" name="handle" value="<%=handle%>">
<input type="hidden" name="handle_status" value="<%=handle_status%>">
<input type="hidden" name="invoice_id" value="<%=invoice_id%>">
<input type="hidden" name="ccid" value="<%=ccid%>">
<input type="hidden" name="pro_id" value="<%=pro_id%>">
<input type="hidden" name="sc_id">
<input type="hidden" name="ps_id" value="<%=ps_id%>"/>

<input type="hidden" name="inv_dog_chinese" id="inv_dog_chinese"/>
<input type="hidden" name="inv_rfe_chinese" id="inv_rfe_chinese"/>
<input type="hidden" name="material_chinese" id="material_chinese"/>

		<table width="98%" border="0" cellspacing="0" cellpadding="3" style="padding-top:2px;">
            <tr>
              <td height="28" colspan="2" align="left" valign="middle" bgcolor="#eeeeee" style="padding-left:5px;">
				参考模板 
                <select name="invoice_product_type" id="invoice_product_type" onChange="changeInvoice(this)">
		  		</select>			  
		  	 </td>
             <td align="right" valign="middle" bgcolor="#eeeeee" style="padding-right:5px;">&nbsp;</td>
            </tr>
			<tr>
				<td colspan="3" id="delivery_info">
					<table width="100%" border="0" cellspacing="0" cellpadding="3" style="padding-top:10px;">
						<tr>
							<td>
								
							</td>
						</tr>
					</table>
				</td>
			</tr>

            <tr>
              <td width="25%" align="left" valign="top"  style="padding-top:7px;">
				  <input type="hidden" name="inv_di_id" id="inv_di_id" value="">
				  Unit Value (US $)<br>
				  <input type="text" name="inv_uv" id="inv_uv" value="">
			  </td>
              <td width="38%" rowspan="2" align="left" valign="top"  style="padding-top:7px;">Reason for Export<br>
                <textarea name="inv_rfe" id="inv_rfe" style="width:230px;height:60px;font-size:12px;"></textarea>
                </td>
              <td width="37%" rowspan="2" align="left" valign="top"  style="padding-top:7px;">
			  Description of Goods<br>
                <textarea name="inv_dog" id="inv_dog" style="width:230px;height:60px;font-size:12px;"></textarea>
                </td>
            </tr>
            <tr>
            	<td>&nbsp;</td>
            </tr>
            <tr>
              <td align="left" valign="top" >
			  Total Value (US $)<br>
			  <input type="text" name="inv_tv" id="inv_tv" value="">
			  </td>
			  <td>
			  	HTS CODE<br/>
				<input style="width: 230px;" type="text" name="hs_code" id="hs_code" value=""/>			  
			  </td>
			  <td>
            		Material<br/>
            		<input style="width: 230px;"  type="text" name="material" id="material" value=""/>
            	</td>
            </tr>
            <tr>
            <br />
              <td height="28" colspan="2" align="left" valign="middle" bgcolor="#eeeeee" style="padding-left:5px;">
               	配送备注
              </td>
              <td align="left" valign="middle" bgcolor="#eeeeee" style="padding-right:5px;">&nbsp;</td>
            </tr>
             
            
            <tr>
            	<td colspan="3" align="left">
          			<textarea name="delivery_note" id="delivery_note" style="margin-top:10px;width:550px;height:50px;font-size:12px;">
          			</textarea>
            	</td>
            </tr>
          </table>
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
			    <td height="5" align="center" valign="middle">&nbsp;</td>
			  </tr>
			  <tr>
			    <td align="center" valign="middle" style="padding-left:20px;">
				<%
				for (int i=0; expressPrintBean.length>0&&i<expressPrintBean.length; i++)
				{
				%>
			     
				  <div class="fee rad" style="border:1px solid silver;" id="div_<%=expressPrintBean[i].getScID()%>" onmousedown="printItAjax(<%=expressPrintBean[i].getScID()%>,'<%=expressPrintBean[i].getPage().replaceAll(".jsp", ".html")%>','<%=expressPrintBean[i].getUseType()%>',<%=expressPrintBean[i].getShippingFee()%>,this,event)" style="background:<%=minPriceScId==expressPrintBean[i].getScID()?"#C1E285":"#eeeeee"%>">
						<span style="margin:0px auto;display:block;float:left;height:20px;line-height:20px;border:0px solid silver;text-align:center;width:100%;">
					 
					 		<span><%=expressPrintBean[i].getName()%> </span>
					 		<span>￥<span class="fee_value"><%=expressPrintBean[i].getShippingFee()%></span></span>
					 	</span>
					
				  <%
				  	if(!expressPrintBean[i].getUseType().equals(""))
				  	{
				  %>
				  	 	<span style="font-size:10px;display:block;clear:both;border:0px solid silver;height:15px;line-height:15px;">
				  			<%=expressPrintBean[i].getUseType()%>
					  	</span>
			
				  <%
				  	}
				  %>
				  
				 </div>
 
				<%
				}
				%>

				<!-- 不推荐使用快递 -->
				<%
				for (int i=0; noSelectExpressPrintBean.length>0&&i<noSelectExpressPrintBean.length; i++)
				{
				%>
			     
				  <div class="noselect rad" style="border:1px solid silver;" id="div_<%=noSelectExpressPrintBean[i].getScID()%>" onmousedown="printItAjax(<%=noSelectExpressPrintBean[i].getScID()%>,'<%=noSelectExpressPrintBean[i].getPage().replaceAll(".jsp", ".html")%>','<%=noSelectExpressPrintBean[i].getUseType()%>',<%=noSelectExpressPrintBean[i].getShippingFee()%>,this,event)">
						<span style="margin:0px auto;display:block;float:left;height:20px;line-height:20px;border:0px solid silver;text-align:center;width:100%;">
					 
					 		<span><%=noSelectExpressPrintBean[i].getName()%> </span>
					 		<span>￥<span class="fee_value"><%=noSelectExpressPrintBean[i].getShippingFee()%></span></span>
					 	</span>
					
				  <%
				  	if(!noSelectExpressPrintBean[i].getUseType().equals(""))
				  	{
				  %>
				  	 	<span style="font-size:10px;display:block;clear:both;border:0px solid silver;height:15px;line-height:15px;">
				  			<%=noSelectExpressPrintBean[i].getUseType()%>
					  	</span>
			
				  <%
				  	}
				  %>
				  
				 </div>
 
				<%
				}
				%>
				</td>
			  </tr>
		</table>
<script type="text/javascript">
   getInvoiceByPsid(<%=ps_id%>);
   jQuery(function(){
		// 把不是0 的最小的选中
	   var minDiv = $( "#div_"+'<%= minPriceScId %>');
		 minDiv.addClass("feeon");
	})
</script>
</form>
</body>
</html>
