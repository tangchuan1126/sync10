<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="com.cwc.fedex.FedexClient"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.cwc.shipping.ShippingInfoBean"%>
<%@ include file="../../../include.jsp"%>
<%@ page import="com.cwc.usps.UspsResponseErrorException"%>
<%
	response.setHeader("Pragma","No-cache");   
	response.setHeader("Cache-Control","no-cache");   
	response.setDateHeader("Expires", 0);  

	String type = StringUtil.getString(request,"type");
	long id = StringUtil.getLong(request,"id");
	
	DBRow row;
	String wayBillNo = "";
	String hs_code = "";
	float weight = 0;
	int pkcount = 1;
	
	
	if(type.toUpperCase().equals("D"))
	{
		row = deliveryMgrZJ.getDetailDeliveryOrder(id);
		wayBillNo = row.getString("waybill_number");
		hs_code = row.getString("hs_code");
		weight = row.get("weight",0f);
		pkcount  = row.get("pkcount",0);
	}
	else if(type.toUpperCase().equals("T"))
	{
		row = transportMgrZJ.getDetailTransportById(id);
		wayBillNo = row.getString("transport_waybill_number");
		hs_code = row.getString("hs_code");
		weight = row.get("weight",0f);
		pkcount  = row.get("pkcount",0);
	}
	else 
	{
		row = new DBRow();
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>打印FEDEX</title>

<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<link type="text/css" href="../../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />
<link href="../../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>
		
<script>
var invoiceWidth = 210;
var invoiceHeight = 297;
var productListWidth = 210;
var productListHeight = 220;
var waybillWidth = 94;
var waybillHeight = 145;
function closeWindowAndRefresh(){
	$.artDialog.opener.refresh && $.artDialog.opener.refresh();
	$.artDialog.close();
}

function printWayBill()
{
		var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
		var printer = "LabelPrinter";
		
		var printerExist = "false";
		var isPrinted = "false";
		for(var i = 0;i<printer_count;i++)
		{
			if(printer==visionariPrinter.GET_PRINTER_NAME(i))
			{
				printerExist = "true";
				break;
			}
		}
		
		if(printerExist=="false")	
		{
			printer = visionariPrinter.SELECT_PRINTER()
			if(printer !=-1)
			{
				for(var i =0;i<<%=pkcount%>;i++)
				{
					isPrinted = "false";	
					
					visionariPrinter.PRINT_INIT("3mm",0,"102mm","152mm","打印运单");
					visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
					visionariPrinter.SET_PRINT_COPIES(1);
					visionariPrinter.SET_PRINTER_INDEX(printer);
					visionariPrinter.ADD_PRINT_IMAGE(0,0,800,1200,"<img src='../FedexInternationalPrintLabel/OUTBOUND_LABEL_<%=wayBillNo%>_0_"+(i+1)+".png' border=0>");
					visionariPrinter.SET_PRINT_STYLEA(1,"Stretch",2);
					
					if(visionariPrinter.PRINT())
					{
						isPrinted = "true";
					}	
					else
					{
						isPrinted = "false";
						break;
					}
				}
				return(true);
			}
			
		}
		else
		{
			
			for(var i =0;i<<%=pkcount%>;i++)
			{
				isPrinted = "false";	
				
				visionariPrinter.PRINT_INIT("3mm",0,"102mm","152mm","打印运单");
				visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
				visionariPrinter.SET_PRINT_COPIES(1);
				visionariPrinter.SET_PRINTER_INDEX(printer);
				visionariPrinter.ADD_PRINT_IMAGE(0,0,800,1200,"<img src='../FedexInternationalPrintLabel/OUTBOUND_LABEL_<%=wayBillNo%>_0_"+(i+1)+".png' border=0>");
				visionariPrinter.SET_PRINT_STYLEA(1,"Stretch",2);
				
				if(visionariPrinter.PRINT())
				{
					isPrinted = "true";
				}	
				else
				{
					isPrinted = "false";
					break;
				}
			}
			return(true);
		}		
}

function printWayBillBottom()
{
	visionariPrinter.PRINT_INIT("3mm",0,"102mm","152mm","打印底联");
	visionariPrinter.SET_PRINT_COPIES(1);
	visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
	visionariPrinter.SET_PRINTER_INDEX("LabelPrinter");
	//visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("wayBillContainer").innerHTML);
	visionariPrinter.ADD_PRINT_IMAGE(0,0,800,1200,"<img src='../FedexInternationalPrintLabel/AUXILIARY_LABEL_<%=wayBillNo%>_0_0.png' border=0>");
	visionariPrinter.SET_PRINT_STYLEA(1,"Stretch",2);
	return(visionariPrinter.PRINT());
}


function printInvoice()
{	
	visionariPrinter.PRINT_INIT("打印发票");
	visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
	visionariPrinter.SET_PRINT_COPIES(2);
	visionariPrinter.SET_PRINTER_INDEX("LaserPrinter");
	visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("invoiceContainer").innerHTML);
	return( visionariPrinter.PRINT() );
}

function printProductList()
{
	visionariPrinter.PRINT_INIT("打印装箱单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,-1,-1,"A4");
	visionariPrinter.SET_PRINT_COPIES(2);
	visionariPrinter.SET_PRINTER_INDEX("LaserPrinter");
	visionariPrinter.ADD_PRINT_TABLE(getPrintMmPx(10),0,getPrintMmPx(productListWidth),getPrintMmPx(productListHeight),document.getElementById("productListContainer").innerHTML);

	if ( visionariPrinter.PRINT() )
	{
		return( true );
	}
}

function mergePrint()
{
	if (printWayBill())
	{
		if(printWayBillBottom())
		{
			if (printInvoice())
			{
				printProductList();
			}
		}
		
	}
}


var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}  
</script>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive();" style="background:#ffffff">
 <table width="923" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td >
		
	<table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="30%" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算重量: <%=weight%>Kg
      </tr>
    </table>
		
		
		</td>
      </tr>
      <tr>
        <td>


<div class="demo" >
<div id="tabs">
<ul>
		<li><a href="#waybill">打印运单</a></li>
		<li><a href="#waybillBottom">运单底联</a></li>
		<li><a href="#invoice">发票</a></li>
		<li><a href="#productList">装箱单</a></li>	 
</ul>


<div id="waybill">
<tst:authentication bindAction="com.cwc.app.api.zj.WayBillMgrZJ.printWayBill">
<input name="Submit" type="button" class="short-short-button-print" onClick="printWayBill()" value="打印">
</tst:authentication>


 	<%
 		for(int i=0;i<pkcount;i++)
 		{
 	%>
 		<div id="wayBillContainer<%=i%>" >
 			<div id="print_label<%=i%>"><img src='../FedexInternationalPrintLabel/OUTBOUND_LABEL_<%=wayBillNo%>_0_<%=i+1%>.png' border=0 style="width:352px;"></div>	
 		</div>
 		
 		<script>
			createContainer("wayBillContainer<%=i%>",15,(50+waybillHeight*<%=i%>),waybillWidth,waybillHeight);
			setComponentPos('print_label<%=i%>',0,1);
		</script>
 	<%
 		}
 	%>
</div>
<!-- 运单结束 -->

<div id="waybillBottom">
<tst:authentication bindAction="com.cwc.app.api.zj.WayBillMgrZJ.printWayBill">
<input name="Submit" type="button" class="short-short-button-print" onClick="printWayBillBottom()" value="打印">
</tst:authentication>
 		<div id="waybillBottomContainer" >
 			<div id="print_label_bottom"><img src='../FedexInternationalPrintLabel/AUXILIARY_LABEL_<%=wayBillNo%>_0_0.png' border=0 style="width:352px;"></div>	
 		</div>
 		
 		<script>
			createContainer("waybillBottomContainer",15,50,waybillWidth,waybillHeight);
			setComponentPos('print_label_bottom',0,0);
		</script>
</div>
<!-- 运单底联结束 -->
<div id="invoice">
	<%
		if(!row.getString("invoice_path").equals(""))
		{
	%>
		<a href="../../../<%=row.getString("invoice_path")%>">下载</a>
	<%	
		}
	%>
	
</div>
<!-- 发票结束 --->
<div id="productList">

</div>
</div></div>
		</td>
      </tr>
    </table>


<script>
$("#tabs").tabs({	
});
</script>





</body>
</html>
