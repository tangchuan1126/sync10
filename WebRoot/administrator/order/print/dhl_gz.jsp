<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.api.AdminMgr,com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="com.cwc.shipping.ShippingInfoBean"%>
<%@ include file="../../../include.jsp"%> 

<%
 	response.setHeader("Pragma","No-cache");   
 response.setHeader("Cache-Control","no-cache");   
 response.setDateHeader("Expires", 0);  

 HashMap countryCodeHM = orderMgr.getAllCountryCodeHM();
  
 long oid = StringUtil.getLong(request,"oid");
 long handle = StringUtil.getLong(request,"handle");
 long handle_status = StringUtil.getLong(request,"handle_status");
 long invoice_id = StringUtil.getLong(request,"invoice_id");
 long ccid = StringUtil.getLong(request,"ccid");
 long sc_id = StringUtil.getLong(request,"sc_id");
 if (sc_id==0)
 {
 	sc_id = Long.parseLong( request.getAttribute("sc_id").toString() );
 }

 //获得订单锁
 DBRow orderLock = OrderLock.lockOrder(session,oid,OrderLock.PRINT);
 if (orderLock!=null)
 {
 	out.println("<script>");
 	out.println("alert('"+orderLock.getString("operator")+"正在"+OrderLock.getMsg(orderLock.getString("operate_type"))+"，请与其联系后再操作')");
 	out.println("</script>");
 }

 String rePrint = "oid="+oid+"&handle_status="+handle_status+"&handle="+handle+"&ccid="+ccid+"&invoice_id="+invoice_id+"&sc_id="+sc_id;

 DBRow order = orderMgr.getDetailPOrderByOid(oid);
 DBRow detailInvoice ;

 long pro_id = order.get("pro_id",0l);

 double total_mc_gross = 0;//累计订单总支付金额
 total_mc_gross =+ order.get("mc_gross",0d);

 //订单状态不为：正常 、 有货，禁止打印
 if (order.get("handle_status",0)!=HandStatusleKey.NORMAL||order.get("product_status",0)!=ProductStatusKey.IN_STORE)
 {
 	out.println("<span style='color:red;font-weight:bold'>订单状态不正确，禁止打印！</span>");
 	return;
 }


 //计算订单成本
 boolean promptFlag = false;
 boolean promptDestinationFlag = false;
 float total_weight = 0;
 float total_weight_org = 0;

 try
 {
 	total_weight_org = orderMgr.compareOrderCost(oid,sc_id,ccid);
 }
 catch (LostMoneyException e)//国家不到达
 {
 	promptFlag = true;
 }
 catch (CountryOutSizeException e)//国家不到达
 {
 	out.println("国家不能送达");
 	return;
 }
 catch (WeightCrossException e)//国家不到达
 {
 	out.println("重量段设置交叉");
 	return;
 }
 catch (WeightOutSizeException e)//国家不到达
 {
 	out.println("重量段超出范围");
 	return;
 }
 catch (CurrencyNotExistException e)//汇率不存在
 {
 	out.println("汇率不存在");
 	return;
 }
 total_weight = orderMgr.calculateWayBillWeight(sc_id,total_weight_org);
 //计算运费
 ShippingInfoBean shippingInfoBean = expressMgr.getShippingFee(sc_id, total_weight_org, ccid,pro_id);


 //兼容旧订单情况
 if ( order.getString("inv_dog").equals("") )
 {
 	detailInvoice = orderMgr.getDetailInvoiceTemplate(invoice_id);
 }
 else
 {
 	//新订单的发票信息都是记录在订单上的，所以需要直接使用订单上的数据
 	detailInvoice = new DBRow();
 	detailInvoice.add("dog",order.getString("inv_dog"));
 	detailInvoice.add("rfe",order.getString("inv_rfe"));
 	detailInvoice.add("uv",order.getString("inv_uv"));
 	detailInvoice.add("tv",order.getString("inv_tv"));
 	detailInvoice.add("di_id",order.get("inv_di_id",0l));//递送地址模板ID，在抄单的时候就被记录到订单里
 }

 //检查发票内容是否正确
 if (detailInvoice==null)
 { 
 	out.println("订单发票内容不正确，请联系客服修正！");
 	return;
 }

 //获得发件人信息
 DBRow detailDelivererInfo;
 if (detailInvoice.get("di_id",0l)==0)
 {
 	detailDelivererInfo = orderMgr.getRandomDelivererInfoByPsId(order.get("ps_id",0l));
 }
 else
 {
 	detailDelivererInfo = orderMgr.getDetailDelivererInfo(detailInvoice.get("di_id",0l));
 }


 if (order==null)
 { 
 	out.println("缺少订单号");
 	return;
 }

 DBRow sonOrderDeliveryNotes[] = orderMgr.getSonOrderDeliveryNoteByParentOid(oid);

 String country = "";
 String ac = order.getString("address_country").toLowerCase();
 String address_state = order.getString("address_state");
 String address_state_tmp = order.getString("address_state").toLowerCase();

 //计算子订单商品总数
 DBRow sonOrderItems[] = orderMgr.getSonOrderItemsByParentOid(oid);
 double totalSonOrderQuantity = 0;
 for (int i=0; i<sonOrderItems.length; i++)
 {
 	totalSonOrderQuantity += sonOrderItems[i].get("quantity",0d);
 }


 float tmp123 = 19.87f;
 if ( order.getString("address_country").trim().equalsIgnoreCase("brazil") )
 {
   	tmp123 = 29.87f;
 }

 AdminMgr am = new AdminMgr();
 AdminLoginBean adminInfo = am.getAdminLoginBean(session);

 String destination = null;
 String airWayBillNumber;
 String input_text_airWayBillNumber = order.getString("ems_id");
 int NumberOfPieces = (int)((Float.parseFloat(order.getString("quantity"))-1.0)/10.0)+1;


 try
 {
 	
 	if ( need2LoadNewWayBill(order.getString("ems_id")) )
 	{
 	
 	//提交DHL运单信息
 	String ConsigneeContactPhoneNumber;
 	
 	if (order.getString("tel").equals(""))
 	{
 		ConsigneeContactPhoneNumber = "Not Provided";
 	}
 	else
 	{
 		ConsigneeContactPhoneNumber = order.getString("tel");
 	}
 	
 	
 	//System.out.println("---------------"+countryCodeHM.get(order.getString("address_country").toLowerCase()));
 	
 	DHLClient dhlClient = new DHLClient();
 			
 	dhlClient.setConsigneeCompanyName(order.getString("address_name"));
 	dhlClient.setConsigneeAddressLine(order.getString("address_street"));
 	dhlClient.setConsigneeCity(order.getString("address_city"));
 	dhlClient.setConsigneeDivisionCode(order.getString("address_state"));
 	dhlClient.setConsigneePostalCode( order.getString("address_zip"));
 	dhlClient.setConsigneeCountryCode( (String)countryCodeHM.get(order.getString("address_country").toLowerCase()));
 	
 	
 	
 	dhlClient.setConsigneeCountryName(order.getString("address_country"));
 	dhlClient.setConsigneeContactPersonName(order.getString("address_name"));
 	dhlClient.setConsigneeContactPhoneNumber( ConsigneeContactPhoneNumber);
 	dhlClient.setConsigneeContactEmailFrom(order.getString("business"));
 	dhlClient.setConsigneeContactEmailTo(order.getString("client_id"));
 	dhlClient.setDutiableDeclaredValue(StringUtil.formatNumber("0.00",order.get("quantity",0f)*tmp123));
 	dhlClient.setShipmentDetailsNumberOfPieces(String.valueOf(NumberOfPieces));
 	dhlClient.setShipmentDetailsWeight(0.5f);
 	
 	//设置发件人信息
 	dhlClient.setDev_CompanyName(detailDelivererInfo.getString("CompanyName"));
 	dhlClient.setDev_City(detailDelivererInfo.getString("City"));
 	dhlClient.setDev_DivisionCode(detailDelivererInfo.getString("DivisionCode"));
 	dhlClient.setDev_PostalCode(detailDelivererInfo.getString("PostalCode"));
 	dhlClient.setDev_CountryCode(detailDelivererInfo.getString("CountryCode"));
 	dhlClient.setDev_CountryName(detailDelivererInfo.getString("CountryName"));
 	
 	dhlClient.setDev_AddressLine1(detailDelivererInfo.getString("AddressLine1"));
 	dhlClient.setDev_AddressLine2(detailDelivererInfo.getString("AddressLine2"));
 	dhlClient.setDev_AddressLine3(detailDelivererInfo.getString("AddressLine3"));
 	dhlClient.setDev_PhoneNumber(detailDelivererInfo.getString("PhoneNumber"));
 	
 	
 	//dhlClient.setShipmentDetailsWeight(1.5f*order.get("quantity",0f));
 	
 	//weight 1.5 8 n
 	
 	dhlClient.setOid( String.valueOf(oid) );
 	
 	airWayBillNumber = dhlClient.commit();
 	destination = dhlClient.getDestination();
 	
 	new BarcodeDecoder(airWayBillNumber);
 	
 	
 	if (airWayBillNumber.equals("0000000000"))
 	{
 		response.sendRedirect("../TransformXMLtoHTML/ResponseXMLS/"+dhlClient.getErrorPage());
 		return;
 	}
 	
 	input_text_airWayBillNumber = airWayBillNumber + input_text_airWayBillNumber;
 	
 	}
 	else
 	{
 		airWayBillNumber = order.getString("ems_id").split("-")[0];
 	}

 }
 catch (Exception e)
 {
 	if ( e.getMessage().indexOf("Server returned HTTP response code: 502 for URL: http://xmlpi.dhl-usa.com/XMLShippingServlet")>=0 )
 	{
 		out.println("<span style='color:red;font-size:15px;font-weight:bold;line-height:25px;'>错误信息："+e.getMessage()+"<br>DHL服务器异常，请与DHL联系！</span>");
 		return;
 	}
 	else
 	{
 		throw e;
 	}
 }
 //检测投递地址
 String need2ValidateDest[] = new String[] {"CLE"};
 String cc = detailInvoice.getString("t_name");
 for (int kkk=0; destination!=null&&kkk<need2ValidateDest.length; kkk++)
 {
 	if (destination.equals(need2ValidateDest[kkk]))
 	{
 		promptDestinationFlag = true;
 		break;
 	}
 }

 String backurl = StringUtil.getCurrentURL(request);
 if ( backurl.indexOf("oid")==-1 )
 {
 	backurl += "?oid="+oid+"&handle="+handle+"&handle_status="+handle_status+"&invoice_id="+invoice_id+"&ccid="+ccid+"&sc_id="+sc_id;
 }
 %>



<%!public boolean need2LoadNewWayBill(String wayBillNumber)
{

	if (wayBillNumber.equals("")||wayBillNumber.indexOf("-")==0)
	{
		return(true);
	}
	else
	{
		return(false);
	}
}%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>打印DHL</title>
<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<link type="text/css" href="../../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />
<link href="../../comm.css" rel="stylesheet" type="text/css">

<script>
var invoiceWidth = 210;
var invoiceHeight = 297;
var productListWidth = 210;
var productListHeight = 220;

function writeTracking(obj)
{
	document.getElementById("tracking").innerText = obj.value;
}

function loadDHLPage()
{
	var page = "../TransformXMLtoHTML/HTML/<%=airWayBillNumber%>.html";

	$.ajax({
		url: page,
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,	
		
		error: function(){
			$("#prolist").html("<span style='color:red;font-weight:bold;font-size:14px;'>错误：DHL运单页面不存在("+page+")</span>");
		},
		
		success: function(html){
			$("#prolist").html(html);
		}
	});
}

function printWayBill()
{
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
	visionariPrinter.SET_PRINT_COPIES(<%=String.valueOf(NumberOfPieces+1)%>);
	visionariPrinter.SET_PRINTER_INDEX("HP LaserJet P2015 Series PCL 6");
	visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("wayBillContainer").innerHTML);
		
	if ( visionariPrinter.PRINT() )
	{
		//更新运单号
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/PrintUpdateWaybillNumberAjax.action',
			type: 'post',
			dataType: 'html',
			timeout: 30000,
			cache:false,
			data:"oid=<%=oid%>&waybill_no=<%=input_text_airWayBillNumber%>",
								
			beforeSend:function(request){
			},
								
			error: function(){
				alert("打印运单失败，请重新打印");
			},
								
			success: function(msg){
				if (msg!=0)
				{
					alert("打印运单失败，请重新打印");
				}
			}
		});
			
			return(true);
		}		
}

function printInvoice()
{	
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
	visionariPrinter.SET_PRINT_COPIES(<%=String.valueOf(NumberOfPieces+1)%>);
	visionariPrinter.SET_PRINTER_INDEX("HP LaserJet P2015 Series PCL 6");
	visionariPrinter.ADD_PRINT_HTM(0,0,invoiceWidth,invoiceHeight,document.getElementById("invoiceContainer").innerHTML);
	return( visionariPrinter.PRINT() );
}

function printProductList()
{
	visionariPrinter.PRINT_INIT("打印订单");
	visionariPrinter.SET_PRINT_PAGESIZE(3,-1,-1,"A4");
	visionariPrinter.SET_PRINT_COPIES(2);
	visionariPrinter.SET_PRINTER_INDEX("HP LaserJet P2015 Series PCL 6");
	visionariPrinter.ADD_PRINT_TABLE(getPrintMmPx(10),0,getPrintMmPx(productListWidth),getPrintMmPx(productListHeight),document.getElementById("productListContainer").innerHTML);

	if ( visionariPrinter.PRINT() )
	{
		document.finish_print_form.submit();
		return( true );
	}
}

function mergePrint()
{
	if (printWayBill())
	{
		if (printInvoice())
		{
			printProductList();
		}
	}
}

</script>

<style>

</style>




</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="loadDHLPage()">

<div id="keep_alive" style="display:none"></div>
    <form name="finish_print_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/FinishPrintUpdateHandle.action">
		<input type="hidden" name="oid" value="<%=oid%>">
		<input type="hidden" name="handle" value="<%=handle%>">
		<input type="hidden" name="handle_status" value="<%=handle_status%>">
		<input type="hidden" name="printType" value="DHL">
		<input type="hidden" name="ems_id" value="<%=input_text_airWayBillNumber%>">
		<input type="hidden" name="sc_id" value="<%=sc_id%>">
		<input type="hidden" name="ccid" value="<%=ccid%>">
				
		<%
		rePrint = rePrint.replaceAll("&","|");
		%>
      <input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/finish_print.html?oid=<%=oid%>&rePrint=<%=StringUtil.getCurrentURI(request)+"?"+rePrint%>">
    </form> 
	
	
    <form name="print_form" method="post">
      <input type="hidden" name="oid" value="<%=oid%>">
      <input type="hidden" name="ems_id">
      <input type="hidden" name="backurl" value="<%=backurl%>">
	  	  <input type="hidden" name="handle" value="<%=handle%>">
	  <input type="hidden" name="handle_status" value="<%=handle_status%>">
	  <input type="hidden" name="printType" value="DHL">
    </form> 





    <table width="923" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td >
		
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="60%" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算重量: <%=MoneyUtil.round(total_weight, 2)%>Kg</td>
        <td width="40%" rowspan="2" align="right"><input name="Submit2" type="button" class="long-button-print" onClick="mergePrint()" value="联合打印"></td>
      </tr>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">
		估算运费: ￥<%=shippingInfoBean.getShippingFee()%>
		</td>
        </tr>
    </table>
		
		
		</td>
      </tr>
      <tr>
        <td>


<div class="demo" >
<div id="tabs">
<ul>
		<li><a href="#waybill">打印运单</a></li>
		<li><a href="#invoice">打印发票</a></li>	 
		<li><a href="#productList">装箱单</a></li>	 
</ul>


<div id="waybill">

<input name="Submit" type="button" class="short-short-button-print" onClick="printWayBill()" value="打印">

<div id="wayBillContainer" > 


<div id="dhl_print">
<SPAN id=loadifo  style="color:#FF0000"></SPAN><DIV id=prolist></DIV>
<div style="font-size:15px;font-weight:bold;">
VAT:1111960571<br>
HS:<%=orderMgr.getHsCode(oid)%>
</div>
<%
if ( order.getString("delivery_note").equals("")==false||sonOrderDeliveryNotes.length>0 )
{
%>
<br>#NOTE:
<%=order.getString("delivery_note")%>
<%
for (int dii=0; dii<sonOrderDeliveryNotes.length; dii++)
{
	if (sonOrderDeliveryNotes[dii].getString("delivery_note").equals(""))
	{
		continue;
	}
	out.println("<br>"+sonOrderDeliveryNotes[dii].getString("delivery_note"));
}
%>

<%
}
%>
</div>


</div>





<!------------------------------------------------->
</div>

<script>
createContainer("wayBillContainer",15,50,invoiceWidth,invoiceHeight);
setComponentPos('dhl_print',10,18);
</script>

<!-- 运单结束 -->

<div id="invoice">
<input name="Submit" type="button" class="short-short-button-print" onClick="printInvoice()" value="打印">
<div id="invoiceContainer">
<style>
td
{
	font-size:11px;
	font-family:Arial, Helvetica, sans-serif;
}
.title-bg
{
	color:#FFFFFF;
	font-weight:bold;
	background:#000000;
	font-size:11px;
}
</style>


<div id="invoice_title" style="font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>&nbsp;
<span style="font-size:12px;color:#999999">WWW.VVME.COM</span>
</div>
<div id="invoice_title2" style="font-size:27px;font-weight:bold;font-family: Arial">INVOICE</div>
<div id="invoice_line">
  <hr style="border:1px #000000 solid;height:1px;width:660px;">
</div>

<div id="invoice_table1">
<table width="320" height="60" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">
         <tr>
           <td bordercolor="#FFFFFF"  style="font-weight:bold">CONSIGNEE</td>
         </tr>
         <tr>
           <td width="178" bordercolor="#FFFFFF"  style="font-size:11px;"><%=order.getString("address_name")%> <br>
             <%=order.getString("address_street")%>
             <%=order.getString("address_city")%>,<%=order.getString("address_state")%>,<%=order.getString("address_zip")%>,<%=order.getString("address_country")%><br>
			 Phone: <%=order.getString("tel")%><br>
             Email: <%=order.getString("client_id")%>			 </td>
      </tr>
    </table>
</div>

<div id="invoice_table2">
<table width="320" height="30" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">
          <tr>
            <td align="right" bordercolor="#FFFFFF">ORDER NO: <%=order.getString("oid")%></td>
          </tr>
          
          <tr>
            <td align="right" bordercolor="#FFFFFF">AIRWAY BILL NO: <%=airWayBillNumber%></td>
          </tr>
          <tr>
            <td align="right" bordercolor="#FFFFFF">CARRIER: DHL</td>
          </tr>
           
          <tr>
            <td align="right" bordercolor="#FFFFFF">REASON FOR EXPORT: <%=StringUtil.ascii2Html( detailInvoice.getString("rfe") )%></td>
          </tr>
          <tr>
            <td align="right" bordercolor="#FFFFFF">INVOICE TIME: <%=DateUtil.NowStrGoble()%></td>
          </tr>
          <tr>
            <td  align="right" bordercolor="#FFFFFF">COUNTRY: <%=order.getString("address_country")%></td>
          </tr>
    </table>
</div>


<div id="invoice_table3">
<table width="320" height="60" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">

           <tr>
             <td bordercolor="#FFFFFF"  style="font-weight:bold">CONSIGNOR</td>
           </tr>
           <tr>
             <td width="179" bordercolor="#FFFFFF"  style="font-size:11px;">
			 <%=detailDelivererInfo.getString("CompanyName")%><br>

			 <%=detailDelivererInfo.getString("AddressLine1")%><br>
<%=detailDelivererInfo.getString("AddressLine2")%><br>
<%=detailDelivererInfo.getString("AddressLine3")%><br>
<%=detailDelivererInfo.getString("City")%>/<%=detailDelivererInfo.getString("CountryName")%><br>
Phone: <%=detailDelivererInfo.getString("PhoneNumber")%></td>
      </tr>
           <tr>
             <td bordercolor="#FFFFFF"  style="table-layout: fixed; word-wrap:break-word;word-break:break-all"></td>
      </tr>
    </table>
</div>

<div id="invoice_table4">
<table width="660" border="1" cellpadding="3" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse; ">
       <tr>
         <td width="83" height="17" align="center" valign="middle" class="title-bg"><strong>NO. OF ITEMS </strong></td>
         <td width="160" align="center" valign="middle" class="title-bg"><strong>DESCRIPTION OF GOODS </strong></td>
         <td width="136" align="center" valign="middle" class="title-bg"><strong>ORIGINAL COUNTRY </strong></td>
         <td width="122" align="center" valign="middle" class="title-bg"><strong>UNIT VALUE (USD) </strong></td>
         <td width="117" align="center" valign="middle" class="title-bg"><strong>TOTAL VALUE (USD) </strong></td>
       </tr>
       <tr>
         <td width="83" height="20" align="center" valign="middle"><%=order.get("quantity",0f)+totalSonOrderQuantity%> Unit</td>
         <td width="160" align="center" valign="middle"><%=StringUtil.ascii2Html( detailInvoice.getString("dog") )%>         </td>
         <td width="136" align="center" valign="middle">CHINA</td>
         <td width="122" align="center" valign="middle">
		 <%
		 if (detailInvoice.getString("tv").equals(""))
		 {
		 	out.println("$"+detailInvoice.getString("uv"));
		 }
		 %>		 </td>
         <td align="center" valign="middle">
		 		 <%
		 if (!detailInvoice.getString("tv").equals(""))
		 {
		 	out.println("$"+detailInvoice.getString("tv") );
		 }
		 else
		 {
		 	out.println("$"+StringUtil.formatNumber("#0.00", Double.parseDouble(detailInvoice.getString("uv"))*(order.get("quantity",0d)+totalSonOrderQuantity) ) );
		 }
		 %>		 </td>
       </tr>
    </table>
	 
	 <br>
	 <table width="660" height="97" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse;">

             <tr>
               <td height="71"  align="left" valign="top" bgcolor="#FFFFFF" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                   <td width="14%" align="center" valign="middle"  class="title-bg" style="padding:2px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;"><strong>QUANTITY</strong></td>
                   <td width="86%"  class="title-bg"  style="padding:5px;border-bottom:1px #000000 solid;padding-left:10px;"><strong>ITEM</strong></td>
                 </tr>
<%
DBRow orderItems[] = orderMgr.getPOrderItemsByOid(oid);

if (orderItems.length>0)
{
	for (int zz=0; zz<orderItems.length; zz++)
	{
%>
                 <tr>
                   <td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;">
				   			<%
					out.println(orderItems[zz].get("quantity",0f)+orderItems[zz].getString("unit_name")+"<br>");
					%>	
				   </td>

                   <td  style="padding:2px;padding-left:10px;">
				   <%
				   	if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&orderItems[zz].get("lacking",0)==1)
					{
						out.println("<span style='color:#FF0000;'> [缺货]</span>");
					}
			
					 out.println("Part# "+orderItems[zz].getString("pid"));
					if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(" ●");
					}
					

				   %>
				   
				   </td>
                 </tr>
<%
	}	
%>

<%
	for (int zz=0; zz<sonOrderItems.length; zz++)
	{
%>
				   <tr>
                   <td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;">
				   <%
					out.println("<span style='color:#000000'>"+sonOrderItems[zz].get("quantity",0f)+sonOrderItems[zz].getString("unit_name")+"</span><br>");
				   %>
				   </td>
				   
				    <td  style="padding:2px;padding-left:10px;">
					<%
						if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&sonOrderItems[zz].get("lacking",0)==1)
						{
							out.println("<span style='color:#FF0000;'> [缺货]</span>");
						}
				

						 out.println("Part# "+sonOrderItems[zz].getString("pid"));
						if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
						{
							out.println(" ●");
						}

					%>
				   </td>
                 </tr>   
				 
<%
}
}
%>





               </table></td>
             </tr>
    </table>
	 <table width="660" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="right" style="font-size:10px;">
		I declare that the above information is true and correct to the best of my knowledge.
		</td>
      </tr>
    </table>
</div>




</div>



<script>
createContainer("invoiceContainer",15,50,invoiceWidth,invoiceHeight);

setComponentPos('invoice_title',10,10);
setComponentPos('invoice_title2',155,10);
setComponentPos('invoice_line',11,22);
setComponentPos('invoice_table3',11,29);
setComponentPos('invoice_table2',101,29);
setComponentPos('invoice_table1',11,60);
setComponentPos('invoice_table4',11,90);

</script>

</div>
<!-- 发票结束 --->

<div id="productList">


<div id="productList">
<input name="Submit" type="button" class="short-short-button-print" onClick="printProductList()" value="打印">

<div id="productListContainer">
<style>
td
{
	font-size:11px;
	font-family:Arial, Helvetica, sans-serif;
}
.barcode2
{
	font-family: C39HrP48DmTt;font-size:30px;
}
.barcode
{
	font-family: C39HrP24DlTt;font-size:32px;
}
</style>





<div id="productlist_invoice_table4">

	 <table width="660" border="0" cellpadding="0" cellspacing="0">
	 
	 <thead>
  <tr>
    <td colspan="4" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="52%">
		<div style="margin-left:10px;font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>
		&nbsp;<span style="font-size:12px;color:#999999">WWW.VVME.COM</span></div>
		<div style="margin-left:10px;margin-top:40px;font-size:11px;font-family:Arial"><%=DateUtil.NowStr()%></div>
				</td>
        <td width="48%" align="right"><table width="224" height="30" border="0" cellpadding="2" cellspacing="0">
          <tr>
            <td align="right" ><strong>Order NO</strong>: <%=order.getString("oid")%></td>
          </tr>
          <tr>
            <td align="right" >
             <img src="/barbecue/barcode?data=<%=order.getString("oid")%>&width=1&height=35&type=code39" />
            </td>
          </tr>
          <tr>
            <td align="right" ><strong>WayBill NO</strong>: <%=input_text_airWayBillNumber%> </td>
          </tr>
          <tr>
            <td align="right" >
   
            	 <img src="/barbecue/barcode?data=<%=input_text_airWayBillNumber%>&width=1&height=35&type=code39" />
            </td>
          </tr>
        </table></td>
      </tr>
	  
	        <tr>
	          <td>&nbsp;</td>
	          <td align="right">&nbsp;</td>
	          </tr>
	        <tr>
    </table></td>
    </tr>
			   <%
if ( order.getString("delivery_note").equals("")==false||sonOrderDeliveryNotes.length>0 )
{
%>
  <tr>
    <td colspan="4" style="padding-bottom:5px;">
	* NOTE:
	<%=order.getString("delivery_note")%>
<%
if (sonOrderDeliveryNotes.length>0)
{
	for (int dii=0; dii<sonOrderDeliveryNotes.length; dii++)
	{
		if (sonOrderDeliveryNotes[dii].getString("delivery_note").equals(""))
		{
			continue;
		}
		out.println("<br>"+sonOrderDeliveryNotes[dii].getString("delivery_note"));
	}
}
%>	</td>
    </tr>
<%
}
%>
  <tr>
    <td colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;border:1px #000000 solid;background:#000000;color:#FFFFFF;">QTY.</td>
    <td bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;padding-left:5px;bold;border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;">ITEM</td>
    <td bgcolor="#FFFFFF" style="border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;font-family:Arial;padding-left:5px;font-weight:bold">PARTS</td>
  </tr>
  </thead>
  

  <%
for (int zz=0; zz<orderItems.length; zz++)
{
%>
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;">	   			<%
					out.println(orderItems[zz].get("quantity",0f));
					%>	</td>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;border-left:0px;">
			   			<%
					if (orderItems[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(orderItems[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:12px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
				   	if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&orderItems[zz].get("lacking",0)==1)
					{
						out.println("<span style='color:#FF0000;'> [缺货]</span>");
					}
			
					
					out.println(orderItems[zz].getString("name"));
					if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(" ●");
					}
					

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(orderItems[zz].get("pid",0l));//获得定制套装原来标准套装的ID
						DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
						ArrayList oldUnionProsList = new ArrayList();//原来套装所包含的商品
						HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
						for (int kk=0; kk<oldUnionPros.length; kk++)
						{
							oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
							oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
						}
						
						DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( orderItems[zz].get("pid",0l) );
						for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
						{
							out.println("<tr>");
	
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
							
							//如果商品不存在于原来的套装之中，则加*提示
							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
							{
								out.println(" *");
							}
							
							out.println("</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
							
							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
							{
								out.println(" *");
							}

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(orderItems[zz].get("pid",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
						{
							out.println("<tr>");
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</tbody>
<%
}
%> 



 <%
	long preOid = 0;
	for (int zz=0; zz<sonOrderItems.length; zz++)
	{
		if (sonOrderItems[zz].get("oid",0l)!=preOid)
		{
			total_mc_gross += orderMgr.getDetailPOrderByOid(sonOrderItems[zz].get("oid",0l)).get("mc_gross",0d);
		}
		preOid = sonOrderItems[zz].get("oid",0l);
%>
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;border:1px #000000 solid;border-top:0px;">
					   <%
					out.println(sonOrderItems[zz].get("quantity",0f));
				   %>		</td>
    <td width="29" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:11px;border:1px #000000 solid;border-top:0px;;border-left:0px;">
		   			<%
					if (sonOrderItems[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(sonOrderItems[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:11px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
						if (order.get("product_status",0)==ProductStatusKey.STORE_OUT&&sonOrderItems[zz].get("lacking",0)==1)
						{
							out.println("<span style='color:#FF0000;'> [缺货]</span>");
						}
				
						out.println(sonOrderItems[zz].getString("name"));
						if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
						{
							out.println(" ●");
						}		

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
							out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
							DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(sonOrderItems[zz].get("pid",0l));//获得定制套装原来标准套装的ID
							DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
							ArrayList oldUnionProsList = new ArrayList();
							HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
							for (int kk=0; kk<oldUnionPros.length; kk++)
							{
								oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
								oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
							}
						
							DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( sonOrderItems[zz].get("pid",0l) );
							for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
							{
							out.println("<tr>");
	
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));

							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
							{
								out.println(" *");
							}
							
							out.println("</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));

							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
							{
								out.println(" *");
							}
							
							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(sonOrderItems[zz].get("pid",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
						{
							out.println("<tr>");
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</tbody>
<%
}
%> 






  <tfoot>  
  <tr>
    <td colspan="4" bgcolor="#FFFFFF">

      <table width="660" border="0" cellspacing="0" cellpadding="3">
       <tr>
         <td width="453" align="left" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">EW: 
		 <%=MoneyUtil.round(orderMgr.calculateWayBillWeight4ST(total_weight_org), 2)%>Kg		</td>
         <td width="454" align="right" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">
		<%=order.getString("order_source")%>		 </td>
       </tr>
     </table>	</td>
  </tr>
  </tfoot>
</table>

	 
</div>
</div></div>
	<script>
createContainer("productListContainer",15,50,productListWidth,productListHeight);

setComponentPos('productlist_invoice_table4',8,10);

</script>



</div>



</div></div>



		
		</td>
      </tr>
    </table>
	
	
	
	
<script>
$("#tabs").tabs({	
	cookie: { expires: 30000 } 
});
	
	
//提交疑问成本订单
function promptDoubtCost()
{
	$.prompt(
	
	"<div id='title'>亏本订单[单号<%=oid%>]</div><br />是否提交客服处理？",
	
	{
		  submit:
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						var para = "oid=<%=oid%>&sc_id=<%=sc_id%>&ccid=<%=ccid%>";
					
						$.ajax({
							url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markDoubtCostAjax.action',
							type: 'post',
							dataType: 'html',
							timeout: 60000,
							cache:false,
							data:para,
							
							beforeSend:function(request){
							},
							
							error: function(){
								alert("网络错误，请重试！");
							},
							
							success: function(html){
								
								if (html=="ok")
								{
									window.close();
								}
								else
								{
									alert("提交失败！");
								}
								
							}
						});
						
						return(false);

					}
					else if (v=="n")
					{
						window.close();
					}
				}
			,		  
		  
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 关闭打印: "n" }
	});
}

if ( "<%=promptFlag%>"=="true" )
{
	promptDoubtCost();
}

function promptDoubtDestination(dest)
{
	$.prompt(
	"<div id='title'>投递地址异常，请提交客服处理！</div>",
	{
		  submit:
				function (v,m,f)
				{
					if (v=="y")
					{
						var para = "verify_address_note=Destination "+dest+"&oid=<%=oid%>&sc_id=<%=sc_id%>&ccid=<%=ccid%>";
						$.ajax({
							url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/printVerifyAddressAjax.action',
							type: 'post',
							dataType: 'html',
							timeout: 60000,
							cache:false,
							data:para,
							beforeSend:function(request){
							},
							error: function(){
								alert("网络错误，请重试！");
							},
							success: function(html){
								if (html=="ok")
								{
									window.close();
								}
								else
								{
									alert("提交失败！");
								}
							}
						});
						return(false);
					}
					else if (v=="n")
					{
						//window.close();
						alert("请确定不是HID系列商品再打印");
					}
				}
			,		  
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 继续打印: "n" }
	});
}
if ( "<%=promptDestinationFlag%>"=="true" )
{
	promptDoubtDestination('<%=destination%>');
}
</script>





</body>
</html>
