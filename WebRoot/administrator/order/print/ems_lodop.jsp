<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.cwc.shipping.ShippingInfoBean"%>
<%@ include file="../../../include.jsp"%>
<%
//orderMgr
	long waybill_id = StringUtil.getLong(request,"waybill_id");
	// StringUtil.getLong(request,"waybill_id");

	DBRow waybillOrder = wayBillMgrZJ.getDetailInfoWayBillById(waybill_id);
	
	DBRow[] waybill_items = wayBillMgrZJ.getWayBillOrderItems(waybill_id);
	
	int NumberOfPieces = 0;
	
	DBRow detailDelivererInfo = new DBRow();
	
  
	
	if (waybillOrder.get("inv_di_id",0l)==0)
	{
		detailDelivererInfo = new DBRow();
	}
	else
	{
		detailDelivererInfo = orderMgr.getDetailDelivererInfo(waybillOrder.get("inv_di_id",0l));
	}
	 
	float quantity = 0;
	for(int i = 0;i<waybill_items.length;i++)
	{
		quantity += waybill_items[i].get("quantity",0f);
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>打印EMS</title>

<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../../js/print/m.js"></script>

<link type="text/css" href="../../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />
<link href="../../comm.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />



<script>
var waybillWidth = 215;
var waybillHeight = 170;
var invoiceWidth = 210;
var invoiceHeight = 297;
var productListWidth = 210;
var productListHeight = 220;

function initPrint()
{

}

function printWayBill()
{
	if ( checkWaybillNum() )
	{
		visionariPrinter.PRINT_INIT("打印订单");
		visionariPrinter.SET_PRINT_PAGESIZE(3,0,0,"A4");
		visionariPrinter.SET_PRINT_COPIES(1);
		visionariPrinter.SET_PRINTER_INDEX("Oki 5530SC");
		visionariPrinter.ADD_PRINT_HTM(0,0,waybillWidth,waybillHeight,document.getElementById("waybillContainer").innerHTML);
		
		if (visionariPrinter.PRINT())
		{
			//更新运单号
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/UploadTrackingNumberToWayBill.action',
				type: 'post',
				dataType: 'html',
				timeout: 30000,
				cache:false,
				data:"waybill_id=<%=waybill_id%>&tracking_number="+document.getElementById("ems_id1").value,
								
				beforeSend:function(request){
				},
								
				error: function(){
					alert("打印运单失败，请重新打印");
				},
								
				success: function(msg){
					if (msg!=0)
					{
						alert("打印运单失败，请重新打印");
					}
				}
			});
			
			return(true);
		}	
	}
	else
	{
		return(false);
	}
}

function printInvoice(){	
	if ( checkWaybillNum() ){
		//获取打印机名字列表
		var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
		 //判断是否有该名字的打印机
		var printer = "LabelPrinter";
		var printerExist = "false";
		var containPrinter = printer;
		for(var i = 0;i<printer_count;i++){
			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
				printerExist = "true";
				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
				break;
			}
		}
		if(printerExist=="true"){
			//判断该打印机里是否配置了纸张大小
			var paper="102X152";
		    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
		    var str=strResult.split(",");
		    var status=false;
		    for(var i=0;i<str.length;i++){
	               if(str[i]==paper){
	                  status=true;
	               }
			}
		   if(status==true){
			     visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","发票");
		         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");	
		         visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  	       
		         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","70mm",document.getElementById("fapiao").innerHTML);
		         visionariPrinter.ADD_PRINT_HTM("140mm",0,"100%","100%",document.getElementById("fapiaofoot").innerHTML);
		         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
		         visionariPrinter.ADD_PRINT_TEXT(557,170,"100%","100%","Page:#/&");
		         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);
		    	 visionariPrinter.SET_PRINT_COPIES(2);//打印分数
			     return (visionariPrinter.PRINT()); //PREVIEW
	       }else{
	        	$.artDialog.confirm("打印机配置纸张大小里没有符合的大小.....", function(){
					 this.close();
	    			}, function(){
				});
	       }	
	 	}else{
	 		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
			if(op!=-1){ //判断是否点了取消
				 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","发票");
		         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");		       
		         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","70mm",document.getElementById("fapiao").innerHTML);
		         visionariPrinter.ADD_PRINT_HTM("140mm",0,"100%","100%",document.getElementById("fapiaofoot").innerHTML);
		         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
		         visionariPrinter.ADD_PRINT_TEXT(557,170,"100%","100%","Page:#/&");
		         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);
		    	 visionariPrinter.SET_PRINT_COPIES(2);//打印分数
			     return (visionariPrinter.PRINT());
			}		
	 	}
	}else{
		return(false);
	}
}

function printProductList(){
	if ( checkWaybillNum() ){

		//获取打印机名字列表
		var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
		 //判断是否有该名字的打印机
		var printer = "LabelPrinter";
		var printerExist = "false";
		var containPrinter = printer;
		for(var i = 0;i<printer_count;i++){
			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
				printerExist = "true";
				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
				break;
			}
		}
		if(printerExist=="true"){
			//判断该打印机里是否配置了纸张大小
			var paper="102X152";
		    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
		    var str=strResult.split(",");
		    var status=false;
		    for(var i=0;i<str.length;i++){
	               if(str[i]==paper){
	                  status=true;
	               }
			}
		   if(status==true){
			     visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","装箱单");
		         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");	
		         visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  	       
		         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","125mm",document.getElementById("zhuangxiang").innerHTML);
		         visionariPrinter.ADD_PRINT_HTM("143mm",0,"100%","100%",document.getElementById("foot").innerHTML);
		         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
		         visionariPrinter.ADD_PRINT_TEXT(557,170,"100%","100%","Page:#/&");
		         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);
		    	 visionariPrinter.SET_PRINT_COPIES(2);//打印分数
				if (visionariPrinter.PRINT()){ //PREVIEW
					document.finish_print_form.submit();
					return( true );
				}
	       }else{
	        	$.artDialog.confirm("打印机配置纸张大小里没有符合的大小.....", function(){
					 this.close();
	    			}, function(){
				});
	       }	
	 	}else{
	 		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
			if(op!=-1){ //判断是否点了取消
				 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","装箱单");
		         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");		       
		         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","125mm",document.getElementById("zhuangxiang").innerHTML);
		         visionariPrinter.ADD_PRINT_HTM("143mm",0,"100%","100%",document.getElementById("foot").innerHTML);
		         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
		         visionariPrinter.ADD_PRINT_TEXT(557,170,"100%","100%","Page:#/&");
		         visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);
		    	 visionariPrinter.SET_PRINT_COPIES(2);//打印分数
				if (visionariPrinter.PRINT()){
					document.finish_print_form.submit();
					return( true );
				}
			}		
	 	}
	}else{
		return(false);
	}
}

function checkWaybillNum()
{
	if ( document.getElementById("ems_id1").value=="" )
	{
		alert("运单号不能为空");
		return(false);
	}
	else if ( <%=quantity%>==0 )
	{
		alert("数量为0，不能打印");
		return(false);
	}
	else
	{
		return(true);
	}
}

function mergePrint()
{
	if (printWayBill())
	{
		if (printInvoice())
		{
			printProductList();
		}
	}
}

function synEmsNo()
{
	var no2=$("#ems_id1").val();
	document.getElementById("syn_ems_no1").innerHTML = document.getElementById("ems_id1").value;
	document.getElementById("syn_ems_no2").innerHTML = '<img src="/barbecue/barcode?data='+no2+'&width=1&height=35&type=code39"  />';
	document.getElementById("syn_ems_no3").innerHTML = document.getElementById("ems_id1").value;
}

</script>

<style>
#waybillContainer
{
	background-attachment: scroll;
	background: url(images/ems.jpg);
	background-repeat: no-repeat;
	background-position: 0px 60px;
}
</style>



</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="synEmsNo();" >
<br>
<form name="finish_print_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/finish_print.html">
		<input type="hidden" name="waybill_id" value="<%=waybill_id%>">
		<input type="hidden" name="rePrint" value="<%=StringUtil.getCurrentURI(request)+"?waybill_id"+waybill_id%>">
 </form>
<table width="923" border="0" cellpadding="8" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="60%" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;font-size:15px;">估算重量: <%=waybillOrder.getString("all_weight")%>Kg
		&nbsp;&nbsp;&nbsp; 
		估算运费: ￥<%=waybillOrder.getString("shipping_cost")%>
		</td>
        <td width="40%" rowspan="2" align="right">
			<tst:authentication bindAction="com.cwc.app.api.zj.WayBillMgrZJ.printWayBill">
        		<input name="Submit2" type="button" class="long-button-print" onClick="mergePrint()" value="联合打印">
        	</tst:authentication>
		</td>
      </tr>
      <tr>
        <td style="color:#999999">
		 运单 <input name="ems_id1" type="text" id="ems_id1" value="<%=waybillOrder.getString("tracking_number")%>" size="50" onKeyDown="synEmsNo()" onKeyUp="synEmsNo()">  
		</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td>
	
	
<div class="demo" >
<div id="tabs">
<ul>
		<li><a href="#waybill">打印运单</a></li>
		<li><a href="#invoice">打印发票</a></li>	 
		<li><a href="#productList">装箱单</a></li>	 
</ul>


<div id="waybill">
<tst:authentication bindAction="com.cwc.app.api.zj.WayBillMgrZJ.printWayBill">
<input name="Submit" type="button" class="short-short-button-print" onClick="printWayBill()" value="打印">
</tst:authentication>

<div id="waybillContainer" >
<style>
div
{
	font-size:12px;	font-family:Verdana;font-weight:normal;
}
</style>

<div id="x_name" ><%=waybillOrder.getString("address_name")%></div>
<div id="dev_ccompany_name" >VVME</div>
<div id="dev_country">China</div>

<div id="rev_country"><%=waybillOrder.getString("address_country")%></div>
<div id="rev_address_name" ><%=waybillOrder.getString("address_name")%></div>
<div id="rev_address_street"><%=waybillOrder.getString("address_street")%></div>
<div id="rev_address_city"><%=waybillOrder.getString("address_city")%></div>
<div id="rev_address_state"><%=waybillOrder.getString("address_state")%></div>
<div id="rev_address_address_zip"><%=waybillOrder.getString("address_zip")%></div>
<div id="rev_address_country"><%=waybillOrder.getString("address_country")%></div>
<div id="rev_address_address_zip2"><%=waybillOrder.getString("address_zip")%></div>
<div id="rev_address_city2" ><%=waybillOrder.getString("address_city")%></div>

<div id="rev_tel" ><%=waybillOrder.getString("tel")%></div>

<div id="car_parts" >CAR PARTs&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <%=quantity%> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
					  	 <%
		 if (waybillOrder.getString("inv_tv").equals(""))
		 {
		 	out.println( StringUtil.formatNumber("#0.00",Double.parseDouble(waybillOrder.getString("inv_uv"))*(quantity)));
		 }
		 %>
		 
		 		 		 <%
		 if (!waybillOrder.getString("inv_tv").equals(""))
		 {
		 	out.println( waybillOrder.getString("inv_tv") );
		 }
		 %>
</div>

<div id="car_con" >
<%
DBRow orderItems[] = wayBillMgrZJ.getWayBillOrderItems(waybill_id);

if (orderItems.length>0)
{
	for (int zz=0; zz<orderItems.length; zz++)
	{
		if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
		{
			out.println("<");
		}
		out.println(orderItems[zz].getString("p_name"));
		if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
		{
			out.println(">");
		}
		out.println(" x "+orderItems[zz].get("quantity",0f)+orderItems[zz].getString("unit_name")+"<br>");
	}
}
%>

</div>

<div id="dev_email" >
        Email: 
                    <%=waybillOrder.getString("client_id")%>
                    <br>
                  INVOICE ATTACHED <%=DateUtil.FormatDatetime("MM-dd hh:mm")%>
</div>

</div></div>

<script>
createContainer("waybillContainer",15,50,waybillWidth,waybillHeight);
setComponentPos('x_name',130,18);
setComponentPos('dev_ccompany_name',18,30);
setComponentPos('dev_country',65,38,3,10);
setComponentPos('rev_address_city2',110,32);
setComponentPos('rev_country',150,32);
setComponentPos('rev_address_name',130,38);
setComponentPos('rev_address_street',115,48);
setComponentPos('rev_address_city',115,52);
setComponentPos('rev_address_state',115,56);
setComponentPos('rev_address_address_zip',165,56);
setComponentPos('rev_address_country',115,60);
setComponentPos('rev_address_address_zip2',110,70);
setComponentPos('rev_tel',150,70);
setComponentPos('car_parts',2,101);
setComponentPos('car_con',2,105);
setComponentPos('dev_email',110,130);
</script>
<!--- 运单结束 -->





<div id="invoice">
<input name="Submit" type="button" class="short-short-button-print" onClick="printInvoice()" value="打印">

<div id="fapiao" style="display:block; margin:0px auto; width:368px;  border:1px red solid">
	<table width="368px"  border="0" cellspacing="0" cellpadding="0">
	<thead>
		 <tr>
		    <td colspan="2" align="left" style="border-bottom:2px #000 solid;">
		    	<span style="font-size:16px; font-weight: bold; font-family:Arial">Visionari LLC</span><br/>
		    	<span style="font-size:10px; color:#CCC; font-family:Arial">WWW.VVME.COM</span>
		    </td>
		    <td colspan="2" align="right" style="border-bottom:2px #000 solid;">
		    	<span style="font-size:16px; font-family:Arial">INVOICE</span>
		    </td>
		  </tr>
		  <tr>
		    <td colspan="2" align="left">
		    	<span style="font-size:13px; font-family:Arial">CONSIGNEE</span><br/>
		   	  	<span style="font-size:10px; font-family:Arial">
			   	  	<%=waybillOrder.getString("address_name")%> <br/>
		             <%=waybillOrder.getString("address_street")%> <br/>
		             <%=waybillOrder.getString("address_city")%>,<%=waybillOrder.getString("address_state")%>,<%=waybillOrder.getString("address_zip")%>,<%=waybillOrder.getString("address_country")%><br>
					 Phone: <%=waybillOrder.getString("tel")%><br>
		             Email: <%=waybillOrder.getString("client_id")%>
	             </span>
		    </td>	 
		    <td colspan="2" align="right">
		    	<span style="font-size:10px; font-family:Arial">
		      	<br/>
		      	    WAYBILL NO: <%=waybillOrder.getString("waybill_id")%><br/>
		      	    AIRWAY BILL NO: <%=waybillOrder.getString("tracking_number")%><br/>
		      	    CARRIER: EMS<br/>
		      	    REASON FOR EXPORT: <%=StringUtil.ascii2Html( waybillOrder.getString("inv_rfe") )%><br/>
		      	    INVOICE TIME: <%=DateUtil.NowStrGoble()%><br/>
		      	    COUNTRY: <%=waybillOrder.getString("address_country")%>
		      	</span>
		    </td>
		  </tr>
		  <tr>
		    <td colspan="2" align="left">
		    	 <span style="font-size:13px; font-family:Arial">CONSIGNOR</span><br/>
	   		     <span style="font-size:10px; font-family:Arial">
	   		        <%=detailDelivererInfo.getString("CompanyName")%><br>
					<%=detailDelivererInfo.getString("AddressLine1")%><br>
					<%=detailDelivererInfo.getString("AddressLine2")%><br>
					<%=detailDelivererInfo.getString("AddressLine3")%><br>
					<%=detailDelivererInfo.getString("City")%>/<%=detailDelivererInfo.getString("CountryName")%><br>
					Phone: <%=detailDelivererInfo.getString("PhoneNumber")%>
	   		     </span>
		    </td>
		    <td colspan="2">&nbsp;</td>
		  </tr>
		  <tr>
		    <td colspan="4">
		    	<table width="100%"  cellspacing="0" cellpadding="0"  bordercolor="#000000" >
			   			<tr>
			   				<td style="background-color:#CCC;border-left:1px #000 solid;border-top:1px #000 solid;border-bottom:1px #000 solid; " align="center">
			   					<span style="font-size:9px; font-weight: bold; font-family:Arial" >NO. OF ITEMS</span>
			   				</td>
			   				<td style="background-color:#CCC;border-left:1px #000 solid;border-top:1px #000 solid;border-bottom:1px #000 solid;" align="center">
			   					<span style="font-size:9px; font-weight: bold; font-family:Arial">DESCRIPTION OF GOOD</span>
			   				</td>
			   				<td style="background-color:#CCC;border-left:1px #000 solid;border-top:1px #000 solid;border-bottom:1px #000 solid;" align="center">
			   					<span style="font-size:9px; font-weight: bold; font-family:Arial">ORIGINAL COUNTRY</span>
			   				</td>
			   				<td style="background-color:#CCC;border-left:1px #000 solid;border-top:1px #000 solid;border-bottom:1px #000 solid;" align="center">
			   					<span style="font-size:9px; font-weight: bold; font-family:Arial">UNIT VALUE (USD)</span>
			   				</td>
			   				<td style="background-color:#CCC;border-left:1px #000 solid;border-top:1px #000 solid;border-bottom:1px #000 solid;border-right:1px #000 solid;" align="center">
			   					<span style="font-size:9px; font-weight: bold; font-family:Arial">TOTAL VALUE (USD)</span>
			   				</td>
			   			</tr>
			   			<tr>
							<td align="center" style="border-left:1px #000 solid;border-bottom:1px #000 solid;">
								<span style="font-size:9px; font-family:Arial"><%=quantity%> Unit</span>
							</td>
			   				<td align="center" style="border-left:1px #000 solid;border-bottom:1px #000 solid;">
			   					<span style="font-size:9px; font-family:Arial"><%=StringUtil.ascii2Html( waybillOrder.getString("inv_dog"))%></span></td>
			   				<td align="center" style="border-left:1px #000 solid;border-bottom:1px #000 solid;">
			   					<span style="font-size:9px; font-family:Arial">CHINA</span>
			   				</td>
			   				<td align="center" style="border-left:1px #000 solid;border-bottom:1px #000 solid;">
			   					<span style="font-size:9px; font-family:Arial">
			   					 <%
									 if (waybillOrder.getString("inv_tv").equals("")){
									 	out.println("$"+waybillOrder.getString("inv_uv"));
									 }
								 %>	
			   					</span>
			   				</td>
			   				<td align="center" style="border-left:1px #000 solid;border-bottom:1px #000 solid;border-right:1px #000 solid">
				   				<span style="font-size:9px; font-family:Arial">
				   					 <%
										 if (!waybillOrder.getString("inv_tv").equals("")){
										 	out.println("$"+waybillOrder.getString("inv_tv") );
										 }else{
										 	out.println("$"+StringUtil.formatNumber("#0.00", Double.parseDouble(waybillOrder.getString("inv_uv"))*(quantity) ) );
										 }
									 %>	
				   				</span>
			   				</td>
			   			</tr>
			   		</table>
		    </td>
		  </tr>
		  <tr>
		  	  <td colspan="4">&nbsp;</td>	
		  </tr>
		  <tr>
			    <td align="center" style="background-color:#CCC; border-left:1px #000 solid;border-top:1px #000 solid;border-bottom:1px #000 solid;">
			    	<span style="font-size:9px; font-weight: bold; font-family:Arial ">QUANTITY</span>
			    </td>
			    <td align="center" style="background-color:#CCC;border-left:1px #000 solid;border-top:1px #000 solid;border-bottom:1px #000 solid;">
			    	<span style="font-size:9px; font-weight: bold; font-family:Arial">HSCODE</span>
			    </td>
			    <td align="center" style="background-color:#CCC;border-left:1px #000 solid;border-top:1px #000 solid;border-bottom:1px #000 solid;">
			    	<span style="font-size:9px; font-weight: bold; font-family:Arial ">MATERIAL</span>
			    </td>
			    <td align="center" style="background-color:#CCC;border-left:1px #000 solid;border-top:1px #000 solid;border-right:1px #000 solid;border-bottom:1px #000 solid;">
			    	<span style="font-size:9px; font-weight: bold; font-family:Arial ">ITEM</span>
			    </td>
		  </tr>
</thead>		  

		<%
			if (waybill_items.length>0){
				for (int zz=0; zz<waybill_items.length; zz++){
		%>
		  <tr>
			    <td align="center" style="border-left:1px #000 solid;border-bottom:1px #000 solid;">
			    	<span style="font-size:9px; font-family:Arial">
						<%out.println(waybill_items[zz].get("quantity",0f)+waybill_items[zz].getString("unit_name")+"<br>");%>	
					</span>
			    </td>
			    <td align="center" style="border-left:1px #000 solid;border-bottom:1px #000 solid;">
			    	<span style="font-size:9px; font-family:Arial"><%=waybillOrder.getString("hs_code")%></span>
			    </td>
			    <td align="center" style="border-left:1px #000 solid;border-bottom:1px #000 solid;">
			    	<span style="font-size:9px; font-family:Arial"><%=waybillOrder.getString("material")%>&nbsp;</span>
			    </td>
			    <td align="center" style="border-left:1px #000 solid;border-bottom:1px #000 solid;border-right:1px #000 solid;">
			    	<span style="font-size:9px; font-family:Arial">
	   					<%
							out.println("Part# "+waybill_items[zz].getString("pc_id"));
							if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL){
								out.println(" ●");
							}
						%>
			   		</span>
			    </td>
		  </tr>
		  	<%}	%>			 
		<%}%>
		
	</table>
</div>
<div id="fapiaofoot" style="display: none">
	<span style="font-size:10px; font-family:Arial" >
		  I declare that the above information is true and correct to the best of my knowledge.
	</span>
</div>

<div id="invoiceContainer" style="display: none">
<style>
.barcode{
	font-family: C39HrP24DlTt;font-size:32px;
}
td{
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
}
.title-bg{
	color:#FFFFFF;
	font-weight:bold;
	background:#000000;
}
</style>

<div id="invoice_title" style="font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>&nbsp;
<span style="font-size:12px;color:#999999">WWW.VVME.COM</span>
</div>
<div id="invoice_title2" style="font-size:27px;font-weight:bold;font-family: Arial">INVOICE</div>
<div id="invoice_line">
  <hr style="border:1px #000000 solid;height:1px;width:660px;">
</div>

<div id="invoice_table1">
<table width="320" height="60" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">
         <tr>
           <td bordercolor="#FFFFFF"  style="font-weight:bold">CONSIGNEE</td>
         </tr>
         <tr>
           <td width="178" bordercolor="#FFFFFF"  style="font-size:11px;"><%=waybillOrder.getString("address_name")%> <br>
             <%=waybillOrder.getString("address_street")%>
             <%=waybillOrder.getString("address_city")%>,<%=waybillOrder.getString("address_state")%>,<%=waybillOrder.getString("address_zip")%>,<%=waybillOrder.getString("address_country")%><br>
			 Phone: <%=waybillOrder.getString("tel")%><br>
             Email: <%=waybillOrder.getString("client_id")%>			 </td>
      </tr>
    </table>
</div>

<div id="invoice_table2">
<table width="320" height="30" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">
          <tr>
            <td align="right" bordercolor="#FFFFFF">ORDER NO: <%=waybillOrder.getString("waybill_id")%></td>
          </tr>
          
          <tr>
            <td align="right" bordercolor="#FFFFFF">AIRWAY BILL NO:<span id="syn_ems_no1"></span></td>
          </tr>
          <tr>
            <td align="right" bordercolor="#FFFFFF">CARRIER: EMS</td>
          </tr>
           
          <tr>
            <td align="right" bordercolor="#FFFFFF">REASON FOR EXPORT: <%=StringUtil.ascii2Html( waybillOrder.getString("inv_rfe") )%></td>
          </tr>
          <tr>
            <td align="right" bordercolor="#FFFFFF">INVOICE TIME: <%=DateUtil.NowStrGoble()%></td>
          </tr>
          <tr>
            <td  align="right" bordercolor="#FFFFFF">COUNTRY: <%=waybillOrder.getString("address_country")%></td>
          </tr>
    </table>
</div>



<div id="invoice_table3">
<table width="320" height="60" border="0" cellpadding="2" cellspacing="0" bordercolor="#000000">

           <tr>
             <td bordercolor="#FFFFFF"  style="font-weight:bold">CONSIGNOR</td>
           </tr>
           <tr>
             <td width="179" bordercolor="#FFFFFF"  style="font-size:11px;">
			 <%=detailDelivererInfo.getString("CompanyName")%><br>

			 <%=detailDelivererInfo.getString("AddressLine1")%><br>
			 <%=detailDelivererInfo.getString("AddressLine2")%><br>
			 <%=detailDelivererInfo.getString("AddressLine3")%><br>
			 <%=detailDelivererInfo.getString("City")%>/<%=detailDelivererInfo.getString("CountryName")%><br>
			 Phone: <%=detailDelivererInfo.getString("PhoneNumber")%></td>
      </tr>
           <tr>
             <td bordercolor="#FFFFFF"  style="table-layout: fixed; word-wrap:break-word;word-break:break-all"></td>
      </tr>
    </table>
</div>

<div id="invoice_table4">
<table width="660" border="1" cellpadding="3" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse; ">
       <tr>
         <td width="83" class="title-bg" height="17" align="center" valign="middle">NO. OF ITEMS</td>
         <td width="160" align="center" class="title-bg"  valign="middle">DESCRIPTION OF GOODS </td>
         <td width="136" align="center" class="title-bg"  valign="middle">ORIGINAL COUNTRY</td>
         <td width="122" align="center" class="title-bg"  valign="middle">UNIT VALUE (USD)</td>
         <td width="117" align="center" class="title-bg"  valign="middle">TOTAL VALUE (USD)</td>
       </tr>
       <tr>
         <td width="83" height="20" align="center" valign="middle"><%=quantity%> Unit</td>
         <td width="160" align="center" valign="middle"><%=StringUtil.ascii2Html( waybillOrder.getString("inv_dog") )%>         </td>
         <td width="136" align="center" valign="middle">CHINA</td>
         <td width="122" align="center" valign="middle">
		 <%
		 if (waybillOrder.getString("inv_tv").equals(""))
		 {
		 	out.println("$"+waybillOrder.getString("inv_uv"));
		 }
		 %>		 </td>
         <td align="center" valign="middle">
		 		 <%
		 if (!waybillOrder.getString("inv_tv").equals(""))
		 {
		 	out.println("$"+waybillOrder.getString("inv_tv") );
		 }
		 else
		 {
		 	out.println("$"+StringUtil.formatNumber("#0.00", Double.parseDouble(waybillOrder.getString("inv_uv"))*(quantity)));
		 }
		 %>		 </td>
       </tr>
    </table>
	 
	 <br>
	 <table width="660" height="97" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse; ">
			
             <tr>
               <td height="71"  align="left" valign="top" bgcolor="#FFFFFF" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                   <td width="14%" align="center" valign="middle"  class="title-bg" style="padding:2px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;border-left:1px #000000 solid;"><strong>QUANTITY</strong></td>
                   <td width="14%" align="center" valign="middle"  class="title-bg" style="padding:2px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;"><strong>HSCODE</strong></td>
                   <td width="14%" align="center" valign="middle"  class="title-bg" style="padding:2px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;"><strong>MATERIAL</strong></td>
                   <td width="58%"  class="title-bg"  style="padding:5px;border-bottom:1px #000000 solid;padding-left:10px;border-right:1px #000000 solid;"><strong>ITEM</strong></td>
                 </tr>
<%
if (orderItems.length>0)
{
	for (int zz=0; zz<orderItems.length; zz++)
	{
%>
                 <tr>
                   <td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;border-bottom:1px #000000 solid;border-left:1px #000000 solid;">
				   			<%
					out.println(orderItems[zz].get("quantity",0f)+orderItems[zz].getString("unit_name")+"<br>");
					%>	
				   </td>
					<td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;border-bottom:1px #000000 solid;">
						<%=waybillOrder.getString("hs_code")%>
					</td>
					<td align="center" valign="middle"  style="padding:2px;border-right:1px #000000 solid;border-bottom:1px #000000 solid;">
						<%=waybillOrder.getString("material")%>&nbsp;
					</td>
                   <td  style="padding:2px;padding-left:10px;border-right:1px #000000 solid;border-bottom:1px #000000 solid;">
				   <%
					if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println("<");
					}
					out.println("Part# "+orderItems[zz].getString("pid"));
					if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(">");
					}
				   %>
				   
				   </td>
                 </tr>
<%
	}	
}
%>





               </table></td>
             </tr>
    </table>
	 <table width="660" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="right" style="font-size:10px;">
		I declare that the above information is true and correct to the best of my knowledge.
		</td>
      </tr>
    </table>
</div>
</div>
</div>
	<script>
createContainer("invoiceContainer",15,50,invoiceWidth,invoiceHeight);
setComponentPos('invoice_title',10,10);
setComponentPos('invoice_title2',155,10);

setComponentPos('invoice_line',11,22);
setComponentPos('invoice_table3',11,29);
setComponentPos('invoice_table2',101,29);
setComponentPos('invoice_table1',11,60);
setComponentPos('invoice_table4',11,85);

</script>
<!--- 发票结束 -->






<div id="productList">
<input name="Submit" type="button" class="short-short-button-print" onClick="printProductList()" value="打印">

<div id="zhuangxiang" style="display:block; margin:0px auto; width:368px; border:1px red solid">
    <table width="368px"  border="0" cellspacing="0" cellpadding="0" >
		<thead>
			<tr>
			    <td colspan="4" style="border-left:0px;">
			    	<div style="width:50%; float:left">
			    		<span style="font-size:22px; font-weight: bold; font-family:Arial">Visionari LLC</span><br/>
				        <span style="font-size:10px;color:#999999; font-family:Arial">WWW.VVME.COM</span><br/>
				    	<span style="font-size:12px; font-family:Arial;word-break:break-all">EMS#<%=waybillOrder.getString("tracking_number")%></span><br/>
				    	<span style="font-size:10px; font-family:Arial"><%=DateUtil.NowStr()%></span>
			    	</div>
			    	<div style="width:50%; float:left; line-height: 20px;" align="center">
			    		<span style="font-size:12px; font-family:Arial">WayBill NO: <%=waybillOrder.getString("waybill_id")%></span><br/>
					    <img src="/barbecue/barcode?data=<%=waybillOrder.getString("waybill_id")%>&width=1&height=38&type=code39" />
			    	</div>   
			    </td>
			</tr>
		 </thead>
		 <% for (int zz=0; zz<waybill_items.length; zz++){%>
		    <%if(zz==0){%>
			    <tr>
				    <td  align="left" style="border-left:1px #000 solid;border-right:1px #000 solid; border-bottom:1px #000 solid;background-color: #CCCCCC;border-top:1px #000 solid;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial"><%=waybill_items[zz].getString("pc_id") %></span>
				    </td>	
				    <td align="left" width="230px;" style="background-color: #CCCCCC;border-right:1px #000 solid; border-bottom:1px #000 solid;word-break:break-all;border-top:1px #000 solid;">
				   		 <span style="font-size:12px; font-weight: bold;  font-family:Arial">
				   		   <%
								out.println(waybill_items[zz].getString("p_name"));
								if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL){
									out.println(" ●");
								}
							%>
				   		 </span>
				    </td>	
					<td width="30px;" align="center" style="border-top:1px #000 solid;background-color: #CCCCCC; border-bottom:1px #000 solid;border-top:1px #000 solid;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial;"><%out.println(waybill_items[zz].get("quantity",0f));%></span>
				    </td>   
				    <td align="center" style="background-color: #CCCCCC;border-left:1px #000 solid; border-right:1px #000 solid; border-bottom:1px #000 solid;border-top:1px #000 solid;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial">
					    	<%
								if (waybill_items[zz].getString("unit_name").equals("")){
									out.println("&nbsp;");
								}else{
									out.println(waybill_items[zz].getString("unit_name"));
								}
						     %>	
				    	</span>
				    </td>
				</tr>
		    <%}else{%>
			    <tr>
				    <td align="left" width="50px;" style="border-left:1px #000 solid;border-right:1px #000 solid; border-bottom:1px #000 solid;background-color: #CCCCCC;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial"><%=waybill_items[zz].getString("pc_id") %></span>
				    </td>	
				    <td align="left" width="230px;" style="background-color: #CCCCCC;border-right:1px #000 solid; border-bottom:1px #000 solid;word-break:break-all;">
				   		 <span style="font-size:12px; font-weight: bold;  font-family:Arial">
				   		   <%
								out.println(waybill_items[zz].getString("p_name"));
								if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL){
									out.println(" ●");
								}
								out.println("<br/>");
							%>
				   		 </span>
				    </td>	
					<td  align="center" style="background-color: #CCCCCC; border-bottom:1px #000 solid;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial;"><%out.println(waybill_items[zz].get("quantity",0f));%></span>
				    </td>   
				    <td align="center" style="background-color: #CCCCCC;border-left:1px #000 solid; border-right:1px #000 solid; border-bottom:1px #000 solid;">
				    	<span style="font-size:11px; font-weight: bold;  font-family:Arial">
					    	<%
								if (waybill_items[zz].getString("unit_name").equals("")){
									out.println("&nbsp;");
								}else{
									out.println(waybill_items[zz].getString("unit_name"));
								}
						     %>	
				    	</span>
				    </td>
				</tr>
		    <%}%>
		   
			<%
			if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM){
						DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(waybill_items[zz].get("pc_id",0l));//获得定制套装原来标准套装的ID
						DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
						ArrayList oldUnionProsList = new ArrayList();//原来套装所包含的商品
						HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
						if(null != oldUnionPros ){
							for (int kk=0; kk<oldUnionPros.length; kk++)
							{
								oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
								oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
							}
						}
						DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( waybill_items[zz].get("pc_id",0l) );
						
						for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++){
							out.println("<tr>");
	
							
							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))){
								out.println("<td align='right' style='border-right:1px #000 solid;border-left:1px #000 solid; border-bottom:1px #000 solid;font-size:11px;'>"+customProductsInSet[customProductsInSet_i].getString("pc_id")+"*</td><td style='border-right:1px #000 solid;border-bottom:1px #000 solid; font-size:9px;word-break:break-all;font-family:Arial;padding-left:10px;' width='230px'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
							}else{
								out.println("<td align='right' style='border-right:1px #000 solid;border-left:1px #000 solid; border-bottom:1px #000 solid;font-size:11px;'>"+customProductsInSet[customProductsInSet_i].getString("pc_id")+"</td><td style='border-right:1px #000 solid;border-bottom:1px #000 solid; font-size:9px;word-break:break-all;font-family:Arial;padding-left:10px;' width='230px'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
							}
							//如果商品不存在于原来的套装之中，则加*提示
							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))){
								out.println(" *");
							}
							out.println("</td>");
							
							out.println("<td colspan='2' align='center' style='border-bottom:1px #000 solid;border-right:1px #000 solid;font-size:10px;font-family:Verdana;'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
							
							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f)){
								out.println(" *");
							}

							out.println("</td></tr>");						
						}
						if(zz==waybill_items.length-1){
					    	  
					    }else{
					    	   out.println("<tr><td colspan='4' height='5px' style='border-bottom:1px #000 solid;'></td></tr>");
					    }
			}else if (waybill_items[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL){
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(waybill_items[zz].get("pc_id",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++){
							out.println("<tr>");
							out.println("<td align='right' style='border-right:1px #000 solid;border-left:1px #000 solid;border-bottom:1px #000 solid;font-size:11px;'>"+productsInSet[productsInSet_i].getString("pc_id")+"</td><td style='border-right:1px #000 solid;border-bottom:1px #000 solid;font-size:10px;word-break:break-all;font-family:Arial;padding-left:10px;' width='230px;'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							out.println("<td colspan='2' align='center' style='border-bottom:1px #000 solid;border-right:1px #000 solid;font-size:10px;font-family:Verdana;'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));
							out.println("</td></tr>");
						}
						if(zz==waybill_items.length-1){
					    	  
					    }else{
					    	   out.println("<tr><td colspan='4' height='5px' style='border-bottom:1px #000 solid;'></td></tr>");
					    }
			}else{
				       if(zz==waybill_items.length-1){
				    	   out.println("&nbsp;");
				       }else{
				    	   out.println("<tr><td colspan='4' height='5px' style='border-bottom:1px #000 solid;'></td></tr>");
				       }
						
			}%>
		 <%} %>
	</table>
</div>
<div id="foot" style="display: none">
<div style="border-top:1px #000 solid;">
	<div style=" font-size:11px; font-family:Arial; width:50%; float:left">
		EW: <%=MoneyUtil.round(orderMgr.calculateWayBillWeight4ST(Float.parseFloat(waybillOrder.getString("all_weight"))), 2)%>Kg
	</div>
	<div style="font-size:11px; font-family:Arial; width:50%; float:left" align="right">
		<%=waybillOrder.getString("order_source")%>
	</div>
</div>	
</div>

<div id="productListContainer" style="display: none">
<style>
td
{
	font-size:11px;
	font-family:Arial, Helvetica, sans-serif;
}
.barcode2
{
	font-family: C39HrP48DmTt;font-size:30px;
}
.barcode
{
	font-family: C39HrP24DlTt;font-size:32px;
}
</style>





<div id="productlist_invoice_table4">

	 <table width="660" border="0" cellpadding="0" cellspacing="0">
	 
	 <thead>
  <tr>
    <td colspan="4" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="52%">
		<div style="margin-left:10px;font-size:27px;font-weight:bold;font-family: Arial;line-height:20px;">Visionari LLC<br>
		&nbsp;<span style="font-size:12px;color:#999999">WWW.VVME.COM</span></div>
		<div style="margin-left:10px;margin-top:40px;font-size:11px;font-family:Arial"><%=DateUtil.NowStr()%></div>
				</td>
        <td width="48%" align="right"><table width="224" height="30" border="0" cellpadding="2" cellspacing="0">
          <tr>
            <td align="right" ><strong>Order NO</strong>: <%=waybillOrder.getString("waybill_id")%></td>
          </tr>
          <tr>
            <td align="right" >
           		 <img src="/barbecue/barcode?data=<%=waybillOrder.getString("waybill_id")%>&width=1&height=35&type=code39" />
            </td>
          </tr>
          <tr>
            <td align="right" ><strong>WayBill NO</strong>: <span id="syn_ems_no3"></span></td>
          </tr>
          <tr>
            <td align="right" id="syn_ems_no2">

            </td>
          </tr>
        </table></td>
      </tr>
	  
	        <tr>
	          <td>&nbsp;</td>
	          <td align="right">&nbsp;</td>
	          </tr>
    </table></td>
    </tr>
			   <%
if ( waybillOrder.getString("delivery_note").equals("")==false)
{
%>
  <tr>
    <td colspan="4" style="padding-bottom:5px;">
	* NOTE:
	<%=waybillOrder.getString("delivery_note")%>
	</td>
  </tr>
<%
}
%>
  <tr>
    <td colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;border:1px #000000 solid;background:#000000;color:#FFFFFF;">QTY.</td>
    <td bgcolor="#FFFFFF" style="font-size:12px;font-family:Arial;font-weight:bold;padding-left:5px;bold;border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;">ITEM</td>
    <td bgcolor="#FFFFFF" style="border:1px #000000 solid;bold;border-left:1px;background:#000000;color:#FFFFFF;font-family:Arial;padding-left:5px;font-weight:bold">PARTS</td>
  </tr>
  </thead>
  

  <%
for (int zz=0; zz<orderItems.length; zz++)
{
%>
<tbody>
  <tr>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;">	   			<%
					out.println(orderItems[zz].get("quantity",0f));
					%>	</td>
    <td width="29" align="center" valign="middle" style="font-size:11px;border:1px #000000 solid;border-top:0px;border-left:0px;">
			   			<%
					if (orderItems[zz].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(orderItems[zz].getString("unit_name"));
					}
				   %>	
	</td>
    <td width="309" bgcolor="#FFFFFF" style="font-size:12px;padding-left:5px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	 <%
					out.println(orderItems[zz].getString("p_name"));
					if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
					{
						out.println(" ●");
					}
					

				   %>	</td>
    <td width="293" bgcolor="#FFFFFF" style="font-size:11px;border-bottom:1px #000000 solid;border-right:1px #000000 solid;">
	<%
	if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(orderItems[zz].get("pc_id",0l));//获得定制套装原来标准套装的ID
						DBRow oldUnionPros[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));
						ArrayList oldUnionProsList = new ArrayList();//原来套装所包含的商品
						HashMap oldUnionProsQuantMap = new HashMap();//原来套装所包含商品的数量
						for (int kk=0; kk<oldUnionPros.length; kk++)
						{
							oldUnionProsList.add(oldUnionPros[kk].getString("pc_id"));
							oldUnionProsQuantMap.put(oldUnionPros[kk].getString("pc_id"),oldUnionPros[kk].get("quantity",0f));
						}
						
						DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( orderItems[zz].get("pc_id",0l) );
						for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
						{
							out.println("<tr>");
	
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("p_name"));
							
							//如果商品不存在于原来的套装之中，则加*提示
							if (!oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id")))
							{
								out.println(" *");
							}
							
							out.println("</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
							
							if (oldUnionProsList.contains(customProductsInSet[customProductsInSet_i].getString("pc_id"))&& StringUtil.getFloat(oldUnionProsQuantMap.get(customProductsInSet[customProductsInSet_i].getString("pc_id")).toString())!=customProductsInSet[customProductsInSet_i].get("quantity",0f))
							{
								out.println(" *");
							}

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
	{
						out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
						
						DBRow productsInSet[] = productMgr.getProductsInSetBySetPid(orderItems[zz].get("pc_id",0l));
						for (int productsInSet_i=0;productsInSet_i<productsInSet.length;productsInSet_i++)
						{
							out.println("<tr>");
							out.println("<td width='75%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("p_name")+"</td>");
							
							out.println("<td width='25%' style='border-bottom:1px solid #000000'>"+productsInSet[productsInSet_i].getString("quantity")+" "+productsInSet[productsInSet_i].getString("unit_name"));

							out.println("</td></tr>");
						}
						out.println("</table>");
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</tbody>
<%
}
%> 
  <tfoot>  
  <tr>
    <td colspan="4" bgcolor="#FFFFFF">

      <table width="660" border="0" cellspacing="0" cellpadding="3">
       <tr>
         <td width="453" align="left" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">EW: 
		 <%=waybillOrder.get("all_weight",0f)%>Kg		</td>
         <td width="454" align="right" style="padding:2px;padding-left:5px;font-weight:bold;font-size:12px;background:#000000;color:#FFFFFF;">
		 </td>
       </tr>
     </table>	</td>
  </tr>
  </tfoot>
</table>

	 
</div>
</div></div>
	<script>
createContainer("productListContainer",15,50,productListWidth,productListHeight);

setComponentPos('productlist_invoice_table4',8,10);

</script>



</div>



<!--- tab结束 -->
</div></div>
	

	
	</td>
  </tr>
</table>

<script>
	$("#tabs").tabs({	
		cookie: { expires: 30000 } 
	});
</script>
</body>
</html>
