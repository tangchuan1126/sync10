<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:res="http://www.dhl.com" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
	<xsl:template match="/">
		<HTML>
			<HEAD>
			<script src="js_content3.js" language="javascript1.3"></script>
			<TITLE>Waybill</TITLE>

            <SCRIPT>
        function fnPrintInstruction ( numberOfAWBCopies, numberOfPieces)
		{
			var message = "" ;
			var errorMessage = "";
			var numberOfPrintCopies = numberOfAWBCopies + numberOfPieces ;

			if ( numberOfPrintCopies > 1 )
			{
				message = new String( JS0139 + "\n 1. " + JS0142 );
				var search = /\{0\}/g ;
				message = message.replace( search, numberOfPrintCopies ) ;

				if ( numberOfAWBCopies == 0  )
				{
					message = new String( message + "\n 2. " + JS0143 + " " + JS0145 + "\n 3. " + JS0149 );
					var search = /\{0\}/g ;
					message = message.replace( search, numberOfPrintCopies  ) ;
				}
				else
				{
					if ( numberOfPieces == 1 )
					{
						message = new String( message + "\n 2. " + JS0143 + " " + JS0147 + "\n 3 " + JS0149 );
						var search = /\{0\}/g ;
						message = message.replace( search, numberOfPrintCopies ) ;
					}
					else
					{
						message = new String( message + "\n 2. " + JS0143 + " " + JS0147 + " " + JS0148 + "\n 3. " + JS0149 );
						var search = /\{0\}/g ;
						message = message.replace( search,  numberOfAWBCopies + 1  ) ;
					}
				}
			}
			else
			{
				message = new String( JS0139 + "\n 1. " + JS0140 );
				message = new String( message + "\n 2. " + JS0143 + " " + JS0144 + "\n 3. " + JS0149 );
			}
			alert(message) ;
		}	
            </SCRIPT>
			</HEAD>

			<xsl:variable name="AirWayBill" select="res:ShipmentValidateResponse/AirwayBillNumber"/>
			<xsl:variable name="OriginArea" select="res:ShipmentValidateResponse/OriginServiceArea/ServiceAreaCode"/>
			<xsl:variable name="DestinationArea" select="res:ShipmentValidateResponse/DestinationServiceArea/ServiceAreaCode"/>
			<!-- xsl:variable name="CustomerID" select="res:ShipmentValidateResponse/CustomerID"/-->
			<xsl:variable name="GlobalProductCode" select="res:ShipmentValidateResponse/GlobalProductCode"/>
			<xsl:variable name="BarcodeProductCode" select="res:ShipmentValidateResponse/GlobalProductCode"/>
			<xsl:variable name="BARCODE_DOMESTIC" select=" 'N' "/>
			<xsl:variable name="BARCODE_EXPRESS_DOCUMENT" select=" 'X' "/>
			<xsl:variable name="BARCODE_INTERNATIONAL_DOCUMENT" select=" 'D' "/>
			<xsl:variable name="BARCODE_WPX" select=" 'P' "/>
			<xsl:variable name="SECOND_DAY_G" select=" 'G' "/>
			<xsl:variable name="SECOND_DAY_Y" select=" 'Y' "/>
			<xsl:variable name="GROUND" select=" 'T' "/>
			<xsl:variable name="DOCUMENT" select=" 'D' "/>
			<xsl:variable name="EXPRESS_DOCUMENT" select=" 'X' "/>
			<xsl:variable name="WPX" select=" 'W' "/>
			<xsl:variable name="CountryCodeUS" select=" 'US' "/>
			<xsl:variable name="ConsigneeCountry" select="res:ShipmentValidateResponse/Consignee/CountryCode"/>
			<xsl:variable name="ShipperCountry" select="res:ShipmentValidateResponse/Shipper/CountryCode"/>
			<xsl:variable name="WeightUnit" select="res:ShipmentValidateResponse/WeightUnit"/>
			<xsl:variable name="LHPOriginRoutingCode" select="res:ShipmentValidateResponse/LHPOrigCd"/>
			<xsl:variable name="LHPDestinationRoutingCode" select="res:ShipmentValidateResponse/LHPDestCd"/>
			<xsl:variable name="FALSE" select=" '0' "/>
			<xsl:variable name="TRUE" select=" '1' "/>
			<xsl:variable name="YES" select=" 'Y' "/>
			<xsl:variable name="NO" select=" 'N' "/>
			<xsl:variable name="RECIPENT_BILLING_TYPE" select=" 'R' "/>
			<xsl:variable name="CREDIT_CARD_BILLING_TYPE" select=" 'C' "/>
			<xsl:variable name="TRANSPORT_COLLECT" select=" 'TCA' "/>
			<xsl:variable name="SST_SATURDAY" select=" 'S' "/>
			<xsl:variable name="SST_DUTY_PAID" select=" 'Q' "/>
			<xsl:variable name="SST_INSURANCE" select=" 'I' "/>
			<xsl:variable name="SST_ON_FORWARDING" select=" 'O' "/>
			<xsl:variable name="SST_PROOF_OF_DELIVERY" select=" 'T' "/>
			<xsl:variable name="BLANK_SPACE" select=" ' ' "/>
			<xsl:variable name="EXPORT_REPAIR_RETURN" select=" 'R' "/>
			<xsl:variable name="EXPORT_PERMANENT" select=" 'P' "/>
			<xsl:variable name="EXPORT_TEMPORARY" select=" 'T' "/>
			<xsl:variable name="REPAIR_RETURN" select=" 'Re-Export' "/>
			<xsl:variable name="PERMANENT" select=" 'Permanent' "/>
			<xsl:variable name="TEMPORARY" select=" 'Temporary' "/>
			<xsl:variable name="LBS" select=" 'lbs' "/>
			<xsl:variable name="KG" select=" 'kgs' "/>
			<xsl:variable name="WEIGHTUNIT_LBS" select=" 'L' "/>
			<xsl:variable name="WEIGHTUNIT_KG" select=" 'K' "/>
			<xsl:variable name="NULL" select=" '' "/>
			<xsl:variable name="NUMBER_OF_COPIES" select="5"/>
			<xsl:variable name="NUMBER_OF_PIECES" select="res:ShipmentValidateResponse/Piece"/>
			<xsl:variable name="SHIPPER_COUNTRY_CODE" select="res:ShipmentValidateResponse/Shipper/CountryCode"/>
			<xsl:variable name="LicensePlate" select="res:ShipmentValidateResponse/Pieces/Piece/LicensePlate"/>
			<xsl:variable name="DataIdentifier" select="res:ShipmentValidateResponse/Pieces/Piece/DataIdentifier"/>


			<body bgcolor="#FFFFFF" link="#0000FF" vlink="#FF0000" text="#000000" >
            <form>
				<div align="left">
					<table width="615" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="100">
								<table width="100" height="44" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<xsl:variable name="wpxProductCode" select=" 'P' "/>
										<xsl:choose>
											<xsl:when test="$GlobalProductCode=$wpxProductCode">
												<xsl:variable name="productGif" select=" '../TransformXMLtoHTML/HTML/images/wpx.gif' "/>
												<td align="center">
													<img height="44" width="114" src="{$productGif}" border="0"/>
												</td>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="$GlobalProductCode='U'">
														<xsl:variable name="productGif" select=" 'images/ecx.gif' "/>
														<td align="center">
															<img height="44" width="114" src="{$productGif}" border="0"/>
														</td>
													</xsl:when>
													<xsl:otherwise>
														<xsl:choose>
															<xsl:when test="$GlobalProductCode='K'">
																<xsl:variable name="productGif" select=" 'images/tdk.gif' "/>
																<td align="center">
																	<img height="44" width="114" src="{$productGif}" border="0"/>
																</td>
															</xsl:when>
															<!-- added for terra implementation-->
														<xsl:otherwise>
															<xsl:choose>
																<xsl:when test="$GlobalProductCode='T'">
																	<xsl:variable name="productGif" select=" 'images/tdt.gif' "/>
																	<td align="center">
																		<img height="44" width="114" src="{$productGif}" border="0"/>
																	</td>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:choose>
																        <xsl:when test="$GlobalProductCode='U'">
																	        <xsl:variable name="productGif" select=" 'images/ecx.gif' "/>
																			<td align="center">
																				<img height="44" width="114" src="{$productGif}" border="0"/>
																			</td>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:choose>
																				<xsl:when test="$GlobalProductCode='X'">
																					<xsl:variable name="productGif" select=" 'images/xdx.gif' "/>
																					<td align="center">
																						<img height="44" width="114" src="{$productGif}" border="0"/>
																					</td>
																				</xsl:when>
																				<!--end  added for terra implementation-->
																				<xsl:otherwise>
																					<xsl:choose>
																						<xsl:when test="$GlobalProductCode='N'">
																							<xsl:variable name="productGif" select=" 'images/dom.gif' "/>
																							<td align="center">
																								<img height="44" width="114" src="{$productGif}" border="0"/>
																							</td>
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:variable name="productGif" select=" 'images/dox.gif' "/>
																							<td align="center">
																								<img height="44" width="114" src="{$productGif}" border="0"/>
																							</td>
																						</xsl:otherwise>
																						<!-- added for terra implementation closing tag for above additions-->
																					</xsl:choose>
																				</xsl:otherwise>
																			</xsl:choose>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:otherwise>
													<!--end  added for terra implementation-->
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</tr>
								</table>
							</td>
							<td width="295" align="center">
								<font size="2">
									<xsl:for-each select="res:ShipmentValidateResponse/SpecialService">
										<xsl:variable name="SpecialServiceTypeValue" select="SpecialServiceType"/>
										<xsl:choose>
											<xsl:when test="$SpecialServiceTypeValue=$SST_SATURDAY">
											<xsl:if test="position() &gt; 1">
											 <b>,</b>
											 </xsl:if>
											<b> SATURDAY DELIVERY</b>
											</xsl:when>
											<xsl:when test="$SpecialServiceTypeValue=$SST_DUTY_PAID">
												<xsl:if test="position() &gt; 1">
												<b>,</b>
												</xsl:if>		
												<b>DDP</b>
											</xsl:when>
											<xsl:when test="$SpecialServiceTypeValue=$SST_INSURANCE">
											<xsl:if test="position() &gt; 1">
											 <b>,</b>
											 </xsl:if>		
											<b> SVP</b>
											</xsl:when>
											<xsl:when test="$SpecialServiceTypeValue=$SST_ON_FORWARDING"/>
											<xsl:when test="$SpecialServiceTypeValue=$SST_PROOF_OF_DELIVERY">
												<xsl:if test="position() &gt; 1">
												<b>,</b>
												</xsl:if>
												<b>POD</b>
											</xsl:when>
										</xsl:choose>
									</xsl:for-each>
									<xsl:variable name="responseBillingCode" select="res:ShipmentValidateResponse/BillingCode"/>
									<xsl:variable name="requestBillingCode" select="res:ShipmentValidateResponse/Billing/BillingAccountNumber"/>
									<xsl:variable name="requestBillingType" select="res:ShipmentValidateResponse/Billing/ShippingPaymentType"/>
									<xsl:choose>
										<xsl:when test="$responseBillingCode=$TRANSPORT_COLLECT">
											<xsl:choose>
												<xsl:when test="$requestBillingCode=null">
													<br>
														<b>TRANSPORT COLLECT -- PC</b>
													</br>
												</xsl:when>
												<xsl:otherwise>
													<xsl:if test="string-length($requestBillingCode) = 0 ">
														<br>
															<b>TRANSPORT COLLECT -- PC</b>
														</br>
													</xsl:if>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:otherwise>
											<xsl:choose>
												<xsl:when test="$requestBillingType=$RECIPENT_BILLING_TYPE">
													<xsl:if test="string-length($requestBillingCode) != 0 ">
														<br>
															<b>Bill to Acct#: <xsl:value-of select="$requestBillingCode"/>
															</b>
														</br>
													</xsl:if>
												</xsl:when>
												<xsl:when test="$requestBillingType=$CREDIT_CARD_BILLING_TYPE">
													<xsl:if test="string-length($requestBillingCode) != 0 ">
														<br>
															<b>Bill to Credit Card</b>
														</br>
													</xsl:if>
												</xsl:when>
											</xsl:choose>
										</xsl:otherwise>
									</xsl:choose>
								</font>
							</td>
							<td width="190">
								<img src="../TransformXMLtoHTML/HTML/images/dhl-logo.gif" width="161" height="45"/>
							</td>
						</tr>
						<tr valign="top">
							<td>
								<br/>
								<br>
									<b>Destination:</b>
								</br>
								<font size="7">
										<b>
											<xsl:value-of select="res:ShipmentValidateResponse/DestinationServiceArea/ServiceAreaCode"/>
										</b>
								</font>
								<br/>
								<br/>
								<br>
									<font size="4">
										<b>Waybill:</b>
									</font>
								</br>
							</td>
							<td>
								<br/>
								<center>
									<!-- xsl:choose>
										<xsl:when test="$GlobalProductCode=$EXPRESS_DOCUMENT" -->
											<xsl:variable name="EXBarcodeProductCode" select="$BARCODE_EXPRESS_DOCUMENT"/>
											<xsl:variable name="OriginDestinationServiceAreaPNG" select="concat('BarCode/ORGDEST_', $OriginArea,$DestinationArea,$GlobalProductCode, '.png')"/>
											<img height="110" width="400" align="top" src="../TransformXMLtoHTML/HTML/{$OriginDestinationServiceAreaPNG}" border="0"/>
										<!-- /xsl:when>
										<xsl:otherwise>
											<xsl:choose>
												<xsl:when test="$GlobalProductCode=$DOCUMENT">
													<xsl:variable name="DOCBarcodeProductCode" select="$BARCODE_DOMESTIC"/>
													<xsl:variable name="OriginDestinationServiceAreaPNG" select="concat('html/BarCode/ORGDEST_', $OriginArea,$DestinationArea,$DOCBarcodeProductCode, '.png')"/>
													<img height="110" width="245" align="top" src="{$OriginDestinationServiceAreaPNG}" border="0"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:choose>
														<xsl:when test="$GlobalProductCode=$WPX">
															<xsl:variable name="WPXBarcodeProductCode" select="$BARCODE_WPX"/>
															<xsl:variable name="OriginDestinationServiceAreaPNG" select="concat('html/BarCode/ORGDEST_', $OriginArea,$DestinationArea,$WPXBarcodeProductCode, '.png')"/>
															<img height="110" width="245" align="top" src="{$OriginDestinationServiceAreaPNG}" border="0"/>
														</xsl:when>
														
														<xsl:otherwise>
															<xsl:variable name="TerraBarcodeProductCode" select="$GlobalProductCode"/>
															<xsl:variable name="OriginDestinationServiceAreaPNG" select="concat('html/BarCode/ORGDEST_', $OriginArea,$DestinationArea,$TerraBarcodeProductCode, '.png')"/>
															<img height="110" width="245" align="top" src="{$OriginDestinationServiceAreaPNG}" border="0"/>
														</xsl:otherwise>
														
													</xsl:choose>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:otherwise>
									</xsl:choose -->
								</center>
								<br/>
								<font size="5">
									<b>
										<xsl:value-of select="res:ShipmentValidateResponse/AirwayBillNumber"/>
									</b>
								</font>
								<br/>
							</td>
							<td rowspan="3">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr><td>
										<font size="3">
											<br></br><b>Acct: <xsl:value-of select="res:ShipmentValidateResponse/Shipper/ShipperID"/>
											</b>
										</font>
									</td></tr>
									<tr><td>
										<font size="3">
											<b>Date: 
											<xsl:value-of select="res:ShipmentValidateResponse/ShipmentDate"/>
											<!--xsl:value-of select="res:ShipmentValidateResponse/EprocInfo/AWBShipDate"/>
											 <xsl:variable name="shipmentDate"  select="res:ShipmentValidateResponse/EprocInfo/AWBShipDate"/> 
											<xsl:variable name="dd">
											 	 <xsl:value-of select="substring($shipmentDate,9,2)" /> 
											 </xsl:variable>
											<xsl:variable name="mm">
											 	 <xsl:value-of select="substring($shipmentDate,6,2)" /> 
											 </xsl:variable>
											<xsl:variable name="yy">
											 	 <xsl:value-of select="substring($shipmentDate,1,4)" /> 
											 </xsl:variable>
											 <xsl:value-of select="concat($dd,'/',$mm,'/',$yy)" /--> 
											 </b>
										</font>
									</td></tr>
									<tr><td>
										<br>
											<font size="2">
												<b>Ref: </b>
											</font>
											<xsl:for-each select="res:ShipmentValidateResponse/Reference">
												<font size="2">
													<b>
														<xsl:value-of select="ReferenceID"/>
													</b></font>
											</xsl:for-each>
										</br>
									</td></tr>
									<!-- added for terra implementation-->
									<tr><td>
										<font size="6">
											<b>
												<xsl:value-of select="res:ShipmentValidateResponse/EDeliveryDt"/>
											</b>
										</font>
									</td></tr>
									<xsl:choose>
										<xsl:when test="$LHPDestinationRoutingCode!=$NULL and $LHPOriginRoutingCode!=$NULL ">
											<tr><td>
												<font size="3">
													<b>Routing:</b>
												</font>
											</td></tr>
											<tr><td>
												<font size="6">
													<b>
														<xsl:value-of select="res:ShipmentValidateResponse/LHPOrigCd"/>/<xsl:value-of select="res:ShipmentValidateResponse/LHPDestCd"/>
													</b>
												</font>
											</td></tr>
										</xsl:when>
									</xsl:choose>
									<!-- end added for terra implementation-->
									<tr><td>
										<xsl:choose>
											<xsl:when test="$ConsigneeCountry=$ShipperCountry"/>
											<xsl:otherwise>
												<xsl:variable name="ShipperEIN" select="res:ShipmentValidateResponse/Dutiable/ShipperEIN"/>
												<xsl:if test="string-length($ShipperEIN) != 0 ">
													<font size="2">
														<b>Shpr EIN: </b>
														<xsl:value-of select="$ShipperEIN"/>
													</font>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</td></tr>
									<tr><td>
										<xsl:choose>
											<xsl:when test="$ConsigneeCountry=$ShipperCountry"/>
											<xsl:otherwise>
												<xsl:variable name="CustomValue" select="res:ShipmentValidateResponse/Dutiable/DeclaredValu"/>
												<xsl:variable name="CurrencyCode" select="res:ShipmentValidateResponse/Dutiable/DeclaredCurrency"/>
												<xsl:if test="string-length($CustomValue) != 0 ">
													<font size="2">
														<b>Customs Value: </b>
														<xsl:value-of select="$CustomValue"/>
														<xsl:if test="string-length($CurrencyCode) != 0 ">
															<xsl:value-of select="$BLANK_SPACE"/>
															<xsl:value-of select="$CurrencyCode"/>
														</xsl:if>
													</font>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</td></tr>
									<tr>
									</tr>
								</table>
							</td>
						</tr>
						</table>
						<table width="615" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="420" align="center" >
								<xsl:variable name="AirWayBillPNG" select="concat('BarCode/AWB_', $AirWayBill, '.png')"/>
								<img height="110" width="400" align="top" src="../TransformXMLtoHTML/HTML/{$AirWayBillPNG}" border="0"/>
							</td>
							<td rowspan="3" >
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr><td>
										<font size="2">
											<b>Weight: </b>
											<xsl:value-of select="res:ShipmentValidateResponse/ChargeableWeight"/>
											<xsl:value-of select="$BLANK_SPACE"/>
											<xsl:choose>
												<xsl:when test="$WeightUnit=$WEIGHTUNIT_LBS">
													<xsl:value-of select="$LBS"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:choose>
														<xsl:when test="$WeightUnit=$WEIGHTUNIT_KG">
															<xsl:value-of select="$KG"/>
														</xsl:when>
													</xsl:choose>
												</xsl:otherwise>
											</xsl:choose>
										</font>
									</td></tr>
									<tr><td>
										<xsl:variable name="DimWeight" select="res:ShipmentValidateResponse/DimensionalWeight"/>
										<xsl:if test="string-length($DimWeight) != 0 ">
											<xsl:choose>
												<xsl:when test="$DimWeight= $BLANK_SPACE"/>
												<xsl:otherwise>
													<font size="2">
														<b>Dim Wt: </b>
														<xsl:value-of select="res:ShipmentValidateResponse/DimensionalWeight"/>
														<xsl:value-of select="$BLANK_SPACE"/>
														<xsl:choose>
															<xsl:when test="$WeightUnit=$WEIGHTUNIT_LBS">
																<xsl:value-of select="$LBS"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:choose>
																	<xsl:when test="$WeightUnit=$WEIGHTUNIT_KG">
																		<xsl:value-of select="$KG"/>
																	</xsl:when>
																</xsl:choose>
															</xsl:otherwise>
														</xsl:choose>
													</font>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</td></tr>
									<br/>
									<tr><td>
										<font size="4">
											<b>Origin: <xsl:value-of select="res:ShipmentValidateResponse/OriginServiceArea/ServiceAreaCode"/>
											</b>
										</font>
									</td></tr>
									<tr><td>
										<font size="4">
											<b>Pcs: <xsl:value-of select="res:ShipmentValidateResponse/Piece"/>
											</b>
										</font>
									</td></tr>
								</table>
							</td>
						</tr>
					</table>
					<br/>
					<table width="615" cellpadding="0" cellspacing="0">
						<tr>
							<td width="25" valign="top">
								<font size="2">
									<b>FR:</b>
								</font>								
							</td>
							<td width="235" valign="top">
								<table width="100%" cellpadding="0" cellspacing="0">
									<font size="0">
										<tr><td>
											<font size="1"><xsl:value-of select="res:ShipmentValidateResponse/Shipper/CompanyName"/></font>
										</td></tr>
										<tr><td>
											<font size="1"><xsl:value-of select="res:ShipmentValidateResponse/Shipper/Contact/PersonName"/></font>
										</td></tr>
										<tr><td>
											<xsl:variable name="shipperAddress1" select="res:ShipmentValidateResponse/Shipper/AddressLine[position()=1]"/>
											<xsl:if test="string-length($shipperAddress1) != 0 ">
												<font size="1"><xsl:value-of select="res:ShipmentValidateResponse/Shipper/AddressLine[position()=1]"/><xsl:value-of select="$BLANK_SPACE"/></font>
                                                                        </xsl:if>
											<xsl:variable name="shipperAddress2" select="res:ShipmentValidateResponse/Shipper/AddressLine[position()=2]"/>
											<xsl:if test="string-length($shipperAddress2) != 0 ">
											<br>
												<font size="1"><xsl:value-of select="res:ShipmentValidateResponse/Shipper/AddressLine[position()=2]"/></font>
                                                                     </br>   </xsl:if>
											<xsl:variable name="shipperAddress3" select="res:ShipmentValidateResponse/Shipper/AddressLine[position()=3]"/>
											<xsl:if test="string-length($shipperAddress3) != 0 ">
											
												<font size="1"><xsl:value-of select="res:ShipmentValidateResponse/Shipper/AddressLine[position()=3]"/></font>
									        </xsl:if>
										</td></tr>
										<tr><td><font size="1">
											<xsl:value-of select="res:ShipmentValidateResponse/Shipper/City"/>,<xsl:value-of select="$BLANK_SPACE"/>
											<xsl:variable name="DivisionCode" select="res:ShipmentValidateResponse/Shipper/DivisionCode"/>
											<!--Special handling for JAXB if division code is null then we set it as dummy -->
											<xsl:if test="$DivisionCode != 'dummy'">
												<xsl:value-of select="res:ShipmentValidateResponse/Shipper/DivisionCode"/>
											 </xsl:if>
										</font></td></tr>
										<tr><td><font size="1">
											<xsl:value-of select="res:ShipmentValidateResponse/Shipper/PostalCode"/><xsl:value-of select="$BLANK_SPACE"/>
                                                                                        <xsl:value-of select="res:ShipmentValidateResponse/Shipper/CountryName"/>
										</font></td></tr>
												

										<tr><td><font size="1">
											<b>Ph: </b>
											<xsl:variable name="shipperPhoneNumber" select="res:ShipmentValidateResponse/Shipper/Contact/PhoneNumber" >
											</xsl:variable>
											 	<xsl:value-of select="res:ShipmentValidateResponse/Shipper/Contact/PhoneNumber" /> 
											<xsl:variable name="phoneExtension" select="res:ShipmentValidateResponse/Shipper/Contact/PhoneExtension"/>
											<xsl:if test="string-length($phoneExtension) != 0 ">
												ext.<xsl:value-of select="$BLANK_SPACE"/><xsl:value-of select="res:ShipmentValidateResponse/Shipper/Contact/PhoneExtension"/>
                                                                        </xsl:if>
									<xsl:variable name="FaxNumber" select="res:ShipmentValidateResponse/Shipper/Contact/FaxNumber"/>
									<xsl:if test = "string-length($FaxNumber) != 0 ">		
											<br></br>
											<b> Fax: </b>
											<xsl:value-of select="res:ShipmentValidateResponse/Shipper/Contact/FaxNumber"/>
									</xsl:if>
										</font></td></tr>
									</font>
								</table>
							</td>
							<td width="25" valign="top">
								<font size="2">
									<b>TO:</b>
								</font>
							</td>
							<td width="330" valign="top">
								<table width="100%" cellpadding="0" cellspacing="0">
									<font size="3">
										<tr><td>
											<b>
												<xsl:value-of select="res:ShipmentValidateResponse/Consignee/CompanyName"/>
											</b>
										</td></tr>
										<tr><td>
											<b>
												<xsl:value-of select="res:ShipmentValidateResponse/Consignee/Contact/PersonName"/>
											</b>
										</td></tr>
										<tr><td>
											<xsl:variable name="consigneeAddress1" select="res:ShipmentValidateResponse/Consignee/AddressLine[position()=1]"/>
											<xsl:if test="string-length($consigneeAddress1) != 0 ">
												<b>
													<xsl:value-of select="res:ShipmentValidateResponse/Consignee/AddressLine[position()=1]"/></b><xsl:value-of select="$BLANK_SPACE"/>
											</xsl:if>
											<xsl:variable name="consigneeAddress2" select="res:ShipmentValidateResponse/Consignee/AddressLine[position()=2]"/>
											<xsl:if test="string-length($consigneeAddress2) != 0 ">
											<br>	<b>
													<xsl:value-of select="res:ShipmentValidateResponse/Consignee/AddressLine[position()=2]"/>
                                                                                        </b></br>
											</xsl:if>
											<xsl:variable name="consigneeAddress3" select="res:ShipmentValidateResponse/Consignee/AddressLine[position()=3]"/>
											<xsl:if test="string-length($consigneeAddress3) != 0 ">
												<b>
													<xsl:value-of select="res:ShipmentValidateResponse/Consignee/AddressLine[position()=3]"/>
                                                                                        </b> 
											</xsl:if>
										</td></tr>
										<tr><td>
											<b>
												<xsl:value-of select="res:ShipmentValidateResponse/Consignee/City"/></b>
											<xsl:variable name="divisionCd" select="res:ShipmentValidateResponse/Consignee/DivisionCode"/>
											<xsl:if test="string-length($divisionCd) != 0 ">
												<b>,
												<xsl:value-of select="$BLANK_SPACE"/><xsl:value-of select="res:ShipmentValidateResponse/Consignee/DivisionCode"/></b>
											</xsl:if>
										</td></tr>
										<tr><td>
											<xsl:variable name="postCd" select="res:ShipmentValidateResponse/Consignee/PostalCode"/>
											<xsl:if test="string-length($postCd) != 0 ">
												<b>
													<xsl:value-of select="res:ShipmentValidateResponse/Consignee/PostalCode"/><xsl:value-of select="$BLANK_SPACE"/></b>
											</xsl:if>
											<b>
												<xsl:value-of select="res:ShipmentValidateResponse/Consignee/CountryName"/>
											</b>
										</td></tr>
										<tr><td>
											<b>Ph: </b> 
											
												<xsl:value-of select="res:ShipmentValidateResponse/Consignee/Contact/PhoneNumber"/> 	
												<xsl:variable name="ConsigneephoneExtension" select="res:ShipmentValidateResponse/Consignee/Contact/PhoneExtension"/>
												<xsl:if test="string-length($ConsigneephoneExtension) != 0 ">
													ext.<xsl:value-of select="$BLANK_SPACE"/><xsl:value-of select="res:ShipmentValidateResponse/Consignee/Contact/PhoneExtension"/>
             		                                                           </xsl:if>
                                                                                <xsl:variable name="FaxNumber" select="res:ShipmentValidateResponse/Consignee/Contact/FaxNumber"/>
										<xsl:if test = "string-length($FaxNumber) != 0 ">		
										<br></br>
										<b> Fax: </b><xsl:value-of select="res:ShipmentValidateResponse/Consignee/Contact/FaxNumber"/>
										</xsl:if>	
										</td></tr>
									</font>
								</table>
							</td>
						</tr>
					</table>
					
					<table width="615" cellpadding="0" cellspacing="0" >
						<tr>
							<td>
								<br>
								</br>
							</td>
						</tr>
						<tr>
							<td>
								<hr size="3"/>
							</td>
						</tr>
					</table>
					<table width="615" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<font size="4">
										<b>LicensePlate:</b>
								</font>
							</td>
							<td align="center">
								<xsl:variable name="LicensePlatePNG" select="concat('BarCode/LicensePlate_', $AirWayBill, '.png')"/>
								<img height="94" width="406" align="top" src="../TransformXMLtoHTML/HTML/{$LicensePlatePNG}" border="0"/>
								<br/>
								<font size="2">
									(<xsl:value-of select="$DataIdentifier"/>)<xsl:value-of select="$LicensePlate"/>
								</font>
								
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<hr size="3"/>
							</td>
						</tr>
					</table>
					
					
					<table width="615" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<font size="2">
									<b>Description: </b>
									<xsl:value-of select="res:ShipmentValidateResponse/Contents"/>
								</font>
							</td>
						</tr>
						<tr>
							<td>
								<hr/>
							</td>
						</tr>
						<tr>
							<td align="center">
								<font size="1" face="Arial">
									<em>
										<b>Fold here. Affix to package using DHL plastic pouch.</b>
									</em>
								</font>
							</td>
						</tr>
						<xsl:choose>
							<xsl:when test="$ConsigneeCountry=$ShipperCountry"/>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="$GlobalProductCode=$WPX">
										<xsl:choose>
											<xsl:when test="$ShipperCountry=$CountryCodeUS">
												<tr>
													<td>
														<font size="1">
															<br>These commodities, technology or software, were exported from the United States in accordance with the Export Administration regulations.  Diversion contrary to U.S. law prohibited.</br>
														</font>
													</td>
												</tr>
											</xsl:when>
										</xsl:choose>
									</xsl:when>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
						
                        <tr>
                            <td>
                                <font size="1">
                                    <br>
                                    </br>
                                </font>
                            </td>
                        </tr>
                        </table>

                        <br></br>
					
                        <table width="615" cellpadding="0" cellspacing="0" >
						<tr>
							<td valign="top">
								
									<font size="2">
										<b>Authorised Representative: </b>
									</font>
									<font size="1">
										<xsl:value-of select="res:ShipmentValidateResponse/Shipper/Contact/PersonName"/>
										<br></br>
									</font>
								
							</td>
							<xsl:if test="$SHIPPER_COUNTRY_CODE='AU'">
								<td>
									<font size="2">
												Destination duties/taxes(if left blank, receiver pays)
												<br></br>Receiver[ ]        Sender[ ]									
									</font>										
								</td>
							</xsl:if>
						</tr>
						<tr>
							<td colspan="2">
								<hr size="3"/>
							</td>
						</tr>
					</table>
				</div>
                </form>
			</body>
		</HTML>
	</xsl:template>
</xsl:stylesheet>
