<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
cartWarranty.flush(session);
DBRow cartProducts[] = cartWarranty.getDetailProducts();

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script>

</script>

<style>
a.del-cart-product:link {
	color:#FF0000;
	text-decoration: none;
}
a.del-cart-product:visited {
	color: #FF0000;
	text-decoration: none;
}
a.del-cart-product:hover {
	color: #FF0000;
	text-decoration: underline;

}
a.del-cart-product:active {
	color: #FF0000;
	text-decoration: none;

}
</style> 

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
  
<%
if (cartProducts.length==0)
{
%>
      <tr>
        <td colspan="4" align="center" valign="middle" style="color:#999999"><<暂无商品>>
		<script>cartIsEmpty=true;</script>		</td>
      </tr>
<%
} 
else
{
%>
      
	  
<%
String bgColor;
for (int i=0; i<cartProducts.length; i++)
{
	if (i%2==0)
	{
		bgColor = "#FFFFFF";
	}
	else
	{
		bgColor = "#f8f8f8";
	}
	
%>

      <tr bgcolor="<%=bgColor%>" >
        <td width="55%"  style="padding-left:10px;padding-top:5px;padding-bottom:5px;">

		<%=cartProducts[i].getString("p_name")%> 
		
		<%
		
		if (cartProducts[i].get("cart_product_type",0)==CartQuote.UNION_STANDARD)
		{
			out.println("[<a href='javascript:void(0)' onclick='customProduct("+cartProducts[i].getString("cart_pid")+",\""+cartProducts[i].getString("p_name")+"\",\"add\")'>定制</a>]");
		}

		if (cartProducts[i].get("cart_product_type",0)==CartQuote.UNION_CUSTOM)
		{
			out.println("[<a href='javascript:void(0)' onclick='customProduct("+cartProducts[i].getString("cart_pid")+",\""+cartProducts[i].getString("p_name")+"\",\"mod\")'>修改</a>]");
		}
		%>

		<%
		if (cartProducts[i].get("cart_product_type",0)==CartQuote.UNION_CUSTOM)
		{
			out.println("<div style='color:#999999;padding-left:10px;'>");
			DBRow customProductsInSet[] = customCartWarranty.getFinalDetailProducts(session, StringUtil.getLong(cartProducts[i].getString("cart_pid")) );
			for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
			{
				out.println("├ "+customProductsInSet[customProductsInSet_i].getString("p_name")+" x "+customProductsInSet[customProductsInSet_i].getString("cart_quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name")+"<br>");
			}
			out.println("</div>");
		}
		%>
		
		<input type="hidden" name="pids" id="pids" value="<%=cartProducts[i].getString("cart_pid")%>">
		<input type="hidden" name="product_type" id="product_type" value="<%=cartProducts[i].getString("cart_product_type")%>">	</td>
        <td width="6%" align="center">&nbsp;</td>
        <td width="25%">

		<input name="quantitys" type="text" id="quantitys" style="width:40px;" value="<%=cartProducts[i].getString("cart_quantity")%>" onChange="return onChangeQuantity(this)"  /> 

		
        <%=cartProducts[i].getString("unit_name")%> </td>
        <td width="14%" align="center" valign="middle">

		<a href="javascript:void(0)" onClick="delProductFromCart(<%=cartProducts[i].getString("cart_pid")%>,'<%=cartProducts[i].getString("p_name")%>',<%=cartProducts[i].getString("cart_product_type")%>)" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>		</td>
      </tr>
<%
} 
%>
      <tr>
        <td height="50" style="border-top:2px #eeeeee solid"><input name="Submit433" type="submit" class="short-button" value="保存修改"> 
         
          <script>cartIsEmpty=false;</script></td>
        <td height="50" style="border-top:2px #eeeeee solid">&nbsp;</td>
        <td colspan="2" align="right" valign="middle" style="border-top:2px #eeeeee solid">&nbsp;</td>
      </tr>
<%
}

//long ent = System.currentTimeMillis();
//System.out.println("Cart Page time:"+(ent-stt));
%>
</table>

</body>
</html>
