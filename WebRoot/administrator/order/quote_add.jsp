<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String cmd = StringUtil.getString(request,"cmd");
long qoid = StringUtil.getLong(request,"qoid");

String action = ConfigBean.getStringValue("systenFolder")+"action/administrator/quote/createQuote.action";

String client_id = StringUtil.getString(request,"client_id");
long ccid = StringUtil.getLong(request,"ccid");
long pro_id = StringUtil.getLong(request,"pro_id");

int target_type = StringUtil.getInt(request,"target_type");
long cc_id = StringUtil.getLong(request,"cc_id");


DBRow detailQuoteOrder = null;

if (cmd.equals("modify"))
{
	detailQuoteOrder = quoteMgr.getDetailQuoteByQoid(qoid);
	action = ConfigBean.getStringValue("systenFolder")+"action/administrator/quote/modifyQuote.action";
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>

<script> 
$().ready(function() {
	ajaxCartForm();
});


//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   //beforeSubmit:  // pre-submit callback    
			//	function(){
			//		loadCart();
			//	}	
		  // ,  
   		   beforeSubmit:  // pre-submit callback    
				function(formData, jqForm, options)
				{
				
						var theForm = document.add_handle_form;

						if (theForm.client_id.value=="")
						{
							alert("请填写Email");
							return(false);
						}
						else if (theForm.client_id.value.indexOf("@")==-1) 
						{
							alert("请正确填写Email");
							return(false);
						}
						else if (theForm.ccid.value==0)
						{
							alert("请选择国家");
							return(false);
						}	
						else if (theForm.pro_id.value==0)
						{
							alert("请选择State");
							return(false);
						}	
						/**		
						else if (theForm.address_name.value=="")
						{
							alert("请填写Name");
							return(false);
						}
						else if (theForm.address_street.value=="")
						{
							alert("请填写Street");
							return(false);
						}
						else if (theForm.address_city.value=="")
						{
							alert("请填写City");
							return(false);
						}
						else if (theForm.address_state.value=="")
						{
							alert("请填写State");
							return(false);
						}											
						else if (theForm.address_zip.value=="")
						{
							alert("请填写Zip");
							return(false);
						}
						**/
						else
						{
							return(true);
						}
				}	
			,
		   success:       // post-submit callback  
				function(responseText, statusText)
				{
					<%
						if (cmd.equals("modify"))
						{
					%>
								if ( responseText=="ok" )
								{
									window.parent.location.reload();
									parent.closeTBWin();
								}
					<%
						}
						else
						{
					%>
								var qoid = responseText*1;
			
								if ( qoid>0 )
								{
									<%
									if (cmd.equals("crm"))
									{
									%>
									parent.tb_show('添加报价商品 [编号：'+qoid+']',"../order/quote_record_order.html?qoid="+qoid+"&TB_iframe=true&height=500&width=850",false);
									<%
									}
									else
									{
									%>
									parent.tb_show('添加报价商品 [编号：'+qoid+']',"quote_record_order.html?qoid="+qoid+"&TB_iframe=true&height=500&width=850",false);
									<%
									}
									%>
									
								}
								else
								{
									alert("创建订单失败！");
								}
					<%
						}
					%>
				}
		       
	};
	

   $('#add_handle_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}


function isNum(keyW)
{
	var reg=  /^(-[0-9]|[0-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 

//国家地区切换
function getStorageProvinceByCcid(ccid,pro_id)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("请选择......","");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
						
						if (pro_id>0)
						{
							$("#pro_id").setSelectedValue(pro_id);
						}
					}	
					
						if (ccid>0)
						{
							$("#pro_id").addOption("Others","-1");
						}					
				}
		);
}

</script>
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}

form
{
	padding:0px;
	margin:0px;
}

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_quote.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="add_handle_form" id="add_handle_form" method="post" action="<%=action%>" >
<input type="hidden" name="qoid" value="<%=qoid%>">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="target_type" value="<%=target_type%>">
<input type="hidden" name="cc_id" value="<%=cc_id%>">
  <br>
  <table width="93%" border="0" cellpadding="0" cellspacing="0" style="padding-left:40px;padding-top:20px;">
  <tr>
    <td>
	 <fieldset style="border:2px #cccccc solid;padding:15px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">客户资料</legend>
	
<table width="100%" border="0" cellspacing="3" cellpadding="2">
  
  <tr> 
      <td width="11%" height="30" align="right" class="STYLE2">Email</td>
      <td width="2%">&nbsp;</td>
    <td colspan="2"><input name="client_id" type="text" id="client_id" value="<%=detailQuoteOrder==null?client_id:detailQuoteOrder.getString("client_id")%>" size="40"></td>
    </tr>
  


    <tr> 
    <td height="30" align="right" class="STYLE2">Country</td>
    <td>&nbsp;</td>
    <td colspan="2">
	<%
	DBRow countrycode[] = orderMgr.getAllCountryCode();
	String selectBg="#ffffff";
	String preLetter="";
	%>
      <select name="ccid" id="ccid" onChange="getStorageProvinceByCcid(this.value,0)">
	  <option value="0">选择国家...</option>
	  <%
	  String selectStr = "";
	  
	  for (int i=0; i<countrycode.length; i++)
	  {
	  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
		{
			if (selectBg.equals("#eeeeee"))
			{
				selectBg = "#ffffff";
			}
			else
			{
				selectBg = "#eeeeee";
			}
		}  	
		
		if ( (detailQuoteOrder!=null&&countrycode[i].get("ccid",0l)==detailQuoteOrder.get("ccid",0l))|| (ccid>0&&countrycode[i].get("ccid",0l)==ccid) )
		{
			selectStr = " selected ";
		}
		else
		{
			selectStr = "";
		}
		
		preLetter = countrycode[i].getString("c_country").substring(0,1);
	  %>
	    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>" <%=selectStr%>><%=countrycode[i].getString("c_country")%></option>
	  <%
	  }
	  %>
      </select>    </td>
    </tr>
	
  <tr> 
      <td width="11%" height="30" align="right" class="STYLE2">State</td>
      <td width="2%">&nbsp;</td>
      <td colspan="2">
	  <select name="pro_id" id="pro_id"  >
	  </select>
	  </td>
  </tr>
	
   <tr>
    <td height="30" align="right" >Name<br></td>
    <td>&nbsp;</td>
    <td colspan="2"><input name="address_name" type="text" id="address_name" size="40"  value="<%=detailQuoteOrder==null?"":detailQuoteOrder.getString("address_name")%>" ></td>
    </tr>
   <tr>
    <td height="30" align="right" >Street<br></td>
    <td>&nbsp;</td>
    <td><input name="address_street" type="text" id="address_street" size="40"   value="<%=detailQuoteOrder==null?"":detailQuoteOrder.getString("address_street")%>" ></td>
    <td>&nbsp;报价备注</td>
   </tr>
    <tr>
    <td height="30" align="right" >City<br></td>
    <td>&nbsp;</td>
    <td width="49%"><input name="address_city" type="text" id="address_city" size="40"   value="<%=detailQuoteOrder==null?"":detailQuoteOrder.getString("address_city")%>" ></td>
    <td width="38%" rowspan="4" align="left" valign="top">
      <textarea name="note" id="note" style="width:300px;height:90px;"><%=detailQuoteOrder==null?"":detailQuoteOrder.getString("note")%></textarea>
    </td>
    </tr>
    <tr>
    <td height="30" align="right" >State<br></td>
    <td>&nbsp;</td>
    <td><input name="address_state" type="text" id="address_state" size="40"  value="<%=detailQuoteOrder==null?"":detailQuoteOrder.getString("address_state")%>" ></td>
    </tr>
  <tr> 
    <td height="30" align="right" >Zip</td>
    <td>&nbsp;</td>
    <td><input name="address_zip" type="text" id="address_zip" size="40"  value="<%=detailQuoteOrder==null?"":detailQuoteOrder.getString("address_zip")%>" ></td>
    </tr>

    <tr>
      <td height="30" align="right">Tel</td>
      <td>&nbsp;</td>
      <td><input name="tel" type="text" id="tel" size="40"  value="<%=detailQuoteOrder==null?"":detailQuoteOrder.getString("tel")%>" ></td>
      </tr>
</table>

	
	</fieldset>
	</td>
  </tr>

</table>

<br>
<br>
<br>
<table width="93%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21%">&nbsp;</td>
    <td>	
	<%
	if (cmd.equals("modify"))
	{
	%>
	<input name="Submit22" type="submit" class="normal-green" value="保存修改" onClick="parent.closeTBWin();">
	<%
	}
	else
	{
	%>
	 <input name="Submit" type="submit" class="create_order_button" value=" ">
	<%
	}
	%>
	
	 </td>
    <td width="17%" align="left" valign="middle">
           <input name="Submit2" type="button" class="normal-white" value="取消" onClick="parent.closeTBWin();">
     </td>
  </tr>
</table>

</form>
<script>
//如果是修改，则选中state
<%
if (detailQuoteOrder!=null)
{
%>
getStorageProvinceByCcid(<%=detailQuoteOrder.get("ccid",0l)%>,<%=detailQuoteOrder.get("pro_id",0l)%>);
<%
}

if (ccid>0&&pro_id>0)
{
%>
getStorageProvinceByCcid(<%=ccid%>,<%=pro_id%>);
<%
}
%>



</script>
</body>
</html>
