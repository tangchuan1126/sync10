<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 

<html>
<head>
<base target="top">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>上传订单附件</title>
<%
long oid = StringUtil.getLong(request,"oid");

if (oid>0)
{
	session.setAttribute("append_oid",String.valueOf(oid));
}

if ( request.getMethod().equalsIgnoreCase("post") )
{

	String name = orderMgr.uploadOrderAppend(request,session.getAttribute("append_oid").toString());

	out.println("<script>");
	if ( name.indexOf(".") > 0)
	{ 
		out.println("alert('上传成功！')");
		out.println("window.returnValue='"+name+"'");
		out.println("window.close()");

		session.removeAttribute("append_oid");
	}
	else
	{
		out.println("alert('" + name + "')");
	}
	out.println("</script>");

}
%>

<script>
function checkF(theForm)
{	
	if (theForm.file.value=="")
	{	
		alert("请选择附件");
		return(false);
	}

	return(true);
}

</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.STYLE1 {
	color: #0000FF;
	font-weight: bold;
	font-size:25px;
}

td
{
	font-size:14px;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellpadding="0" cellspacing="5">
  <form action="" method="post" enctype="multipart/form-data" name="form1" onSubmit="return checkF(this)" target="_self">
  <input type="hidden" name="oid" value="<%=oid%>">
    <tr> 
      <td height="85" align="center">上传附件到订单 <span class="STYLE1"><%=oid%></span></td>
    </tr>
    <tr> 
      <td align="center" valign="middle"> <input name="file" type="file"  size="15">
      &nbsp; <input name="Submit" type="submit" class="short-button" value=" 上 传 "> </td>
    </tr>
  </form>
</table>
</body>
</html>
