<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<jsp:useBean id="orderTaskKey" class="com.cwc.app.key.OrderTaskKey"/>
<%@ include file="../../include.jsp"%> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

 long oid = StringUtil.getLong(request,"oid");

 DBRow ordersTasks[];
 ordersTasks = taskMgr.getOrderTasksByOid(oid,null);
 %> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单任务</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--

.zebraTable tr td a:link {
	color:#000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:visited {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:hover {
	color: #000000;
	text-decoration: underline;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:active {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}



-->
</style>

<script>

function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.st.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.en.value = date;
}


</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable();">

<br>
<table width="100%" border="0" align="center"  cellpadding="0" cellspacing="0">
	<tr>
		<td align="center">
			<table width="95%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
<thead>
<tr>
<th width="12%"   class="right-title"  style="text-align: center;">执行操作</th>
    <th width="8%"  class="left-title" style="text-align: center;">任务原因</th>
    <th width="14%"  class="right-title"  style="text-align: center;" >任务备注</th>
    <th width="12%" class="right-title"  style="text-align: center;" >创建人/日期</th>
    <th width="10%" class="right-title"  style="text-align: center;" >实际执行人</th>
    <th width="9%"  class="right-title"  style="text-align: center;" >状态</th>
</tr>
</thead>
<%
String storageColor = "";
for (int i=0; i<ordersTasks.length; i++)
{
	if ( ordersTasks[i].get("ps_id",0l)==100000l )
	{
		storageColor = "#000000";
	}
	else
	{
		storageColor = "#0000FF";
	}
%>
  <tr>
 <td align="center" valign="middle" nowrap style="color:#CC0000" ><%=ordersTasks[i].getString("t_operation")%></td>
    <td height="40" align="center" valign="middle" nowrap  ><%=orderTaskKey.getReasonById(ordersTasks[i].get("t_reason",0))%></td>
    <td align="center" valign="middle" nowrap  ><%=ordersTasks[i].getString("t_note")%>&nbsp;</td>
    <td align="center" valign="middle" nowrap  ><%=ordersTasks[i].getString("create_admin")%><br>
<%=ordersTasks[i].getString("create_date")%></td>
    <td align="center" valign="middle" nowrap  >
	<%
	if (ordersTasks[i].getString("t_operator").equals("")==false)
	{
		out.println(ordersTasks[i].getString("t_operator")+"<br>"+ordersTasks[i].getString("t_operate_date"));
	}
	%>
&nbsp;	</td>
    <td align="center" valign="middle" nowrap  ><%=orderTaskKey.getStatusById(ordersTasks[i].get("t_status",0))%></td>
  </tr>
<%
}
%>
</table>

		</td>
	</tr>
</table>
</body>

</html>