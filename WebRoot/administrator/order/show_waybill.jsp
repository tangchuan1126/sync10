<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@ include file="../../include.jsp"%> 
<%
	long oid =  StringUtil.getLong(request,"oid");
	DBRow[] waybill_order = wayBillMgrZJ.getWayBillByOId(oid,null);
	
	WayBillOrderStatusKey wayOrderStatusKey = new WayBillOrderStatusKey();
	WaybillInternalTrackingKey waybillInternalTrackingKey = new WaybillInternalTrackingKey();
	
	ProductStatusKey productStatus = new ProductStatusKey();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
table.appendTable{border-collapse:collapse;width:100%;}
td.addAttr{background:#E6F3C5;line-height:22px;height:22px;border-top:1px solid silver;}
</style>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<script type="text/javascript">

	function openPrint(waybill_id,print_page){
	  
		  var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+print_page+"?waybill_id="+waybill_id;
			$.artDialog.open(uri, {title: '打印运单',width:'960px',height:'500px', lock: true,opacity: 0.3});
	 }
	  
	function puchaseDeliveryCount(pc_id,ps_id,type)
	{
		$.artDialog.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/purchase_delivery_count.html?pc_id="+pc_id+"&ps_id="+ps_id+"&type="+type,{title: "采购及在途数",width:'600px',height:'400px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function showWaybillLog(waybill_id)
	{
		$.artDialog.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/waybill/show_waybill_log.html?waybill_id="+waybill_id,{title: "运单日志",width:'600px',height:'400px',fixed:true, lock: true,opacity: 0.3});
	}
	function waybillServiceOrder(wid){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/waybill_apply_service_order.html?oid=<%=oid%>&wid='+wid;
	    $.artDialog.open(uri,{title: "创建服务,运单["+wid+"]",width:'1100px',height:'530px',fixed:true, lock: true,opacity: 0.3});
	}
	function showServiceList(wid)
 	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_list_for_order.html?oid=<%=oid%>&wid='+wid;
		$.artDialog.open(uri , {title: '服务列表',width:'1000px',height:'600', lock: true,opacity: 0.3,fixed: true});
 	}
	</script>
</head>

<body >
<form name="transfer_form" method="post">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
	<input type="hidden" name="transfer_type"/>
	<input type="hidden" name="money_status"/>
</form>
<form action="" method="post" name="followup_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
</form>
	
<table cellpadding="3" cellspacing="6" style="width:100%;border-collapse:collapse;">
					<% 
						for(int i=0;i<waybill_order.length;i++)
						{
					%>
					<tr style="background:#E6F3C5;line-height:22px;height:22px;">
						<td style="text-align:left;">
							<%= waybill_order[i].get("waybill_id",0)%>&nbsp;&nbsp;
							<%= productStatus.getOrderHandleById(waybill_order[i].getString("product_status"))%>
						</td>
						<td>
							&nbsp;&nbsp;&nbsp;&nbsp;<%=catalogMgr.getDetailProductStorageCatalogById(waybill_order[i].get("ps_id",0l)).getString("title")%> 
						</td>
						<% 
							String tracking_status = waybillInternalTrackingKey.getWaybillInternalTrack(waybill_order[i].getString("internal_tracking_status"));
  							
  							DBRow shipping_company = expressMgr.getDetailCompany(waybill_order[i].get("sc_id",0l));
  							String company_name = shipping_company.getString("name");
						%>
						<td><font style="font-weight: bold;"><%=company_name%></font></td>
						<td> <a href="javascript:openPrint('<%= waybill_order[i].getString("waybill_id")%>','<%= expressMgr.getDetailCompany(waybill_order[i].get("sc_id",0l)).getString("print_page")%>')" style="color:green;" ><%=waybill_order[i].getString("tracking_number")%></a></td>
						<td nowrap="nowrap" colspan="2">
							<font style="">
								<%
  						
			  						long print_cost = waybill_order[i].get("print_cost",0l)/1000;
			  						
			  						long print_cost_hour = print_cost/3600;
			   						long print_cost_minute = print_cost%3600/60;
			   						long print_cost_second = print_cost%60/60;   						
			  						if(print_cost>0)
			  						{
			  							out.print("打印用时："+print_cost_hour+"时"+print_cost_minute+"分"+print_cost_second+"秒");
			  						}
			  						
			  						long delivery_cost = waybill_order[i].get("delivery_cost",0l)/1000;
  						
			  						long delivery_cost_hour = delivery_cost/3600;
			   						long delivery_cost_minute = delivery_cost%3600/60;
			   						long delivery_cost_second = delivery_cost%60/60;   						
			  						
			  						if(delivery_cost>0)
			  						{
			  							out.print("&nbsp;&nbsp;配货用时:"+delivery_cost_hour+"时"+delivery_cost_minute+"分"+delivery_cost_second+"秒");
			  						}
  						
  								%>
							</font>
							<!-- 申请服务 -->
							<input type="button" value="申请服务" class="short-button" onclick="waybillServiceOrder('<%=waybill_order[i].get("waybill_id",0l) %>')"/>
							<%
								String serviceOrdersBtnStyle = "style='color:red;'";
								if(serviceOrderMgrZyj.getServiceOrderCountByWaybillId(waybill_order[i].get("waybill_id",0l)) > 0)
								{
									serviceOrdersBtnStyle = "style='color:green;'";
								}
							%>
								<input type="button" value="服务列表" class="short-button" onclick="showServiceList('<%=waybill_order[i].get("waybill_id",0l) %>')" <%=serviceOrdersBtnStyle %>/>
						</td>
					</tr>
					<tr style="margin:0px;padding:0px;background:#E6F3C5;line-height:22px;height:22px;">
								<!-- 用于显示后面添加的字段 -->
								<%
  							//在这里判断当前的状态是 什么然后选择那些count he state 
  							
  							
			  							DBRow userInfo = null ;
			  							 String userName = "";
			  							String[] arrayCount = {"create_account","print_account","delivery_account","cancel_account","split_account"};
			  							String[] arrayTime = {"create_date","print_date","delivery_date","cancel_date","split_date"};
			  							int w = waybill_order[i].get("status",0);
			  						 
			  							long countId = waybill_order[i].get(arrayCount[w],0l);
			  							String dateTime = waybill_order[i].getString(arrayTime[w]);
			  							userInfo = waybillMgrZR.getUserNameById(countId);
			  							if(userInfo != null ){
			  								  userName =  userInfo.getString("employe_name");
			  							}
			  					
			  							dateTime = dateTime.length() >= 19 ? dateTime.substring(0,19):dateTime;
			  					 
			  					%>
								<td colspan="6">
									<table class="appendTable">
										<tr>
											<td class="addAttr" width="15%">运单重量:<%=waybill_order[i].get("all_weight",0.0f) %>Kg</td>
											
											<td class="addAttr"  width="15%">发货重量:<%=waybill_order[i].get("delivery_weight",0.0f) %>Kg</td>
											<td class="addAttr"  width="15%">总运费: ￥<%=waybill_order[i].get("shipping_cost",0.0f) %></td>
											<td class="addAttr"  width="15%">
												<table>
													<tr>
														<td align="right">状态:</td>
														<td align="left">
															<span style="color:red;">
																<%=wayOrderStatusKey.getStatusById(waybill_order[i].get("status",0))%>
															</span>
														</td>
													</tr>
													<%
														//if(waybill_order[i].get("status",0)==WayBillOrderStatusKey.SHIPPED&&!tracking_status.equals(""))
														if(waybill_order[i].get("status",0)==WayBillOrderStatusKey.SHIPPED)
														{
															String trackingUrl = trackingUrl = shipping_company.getString("tracking_url").replace("[trackingNumber]",waybill_order[i].getString("tracking_number"));
													%>
														<tr>
															<td align="right" nowrap="nowrap">运输状态:</td>
															<td align="left" nowrap="nowrap">
																<a href="<%=trackingUrl%>" target="_blank"><%=tracking_status%></a>
																<a href="javascript:showWaybillLog(<%=waybill_order[i].get("waybill_id",0)%>)"><img src="../imgs/modify.gif" width="12" height="12" border="0" alt="运单日志"/></a>
															</td>
														</tr>
													<%
														}
													%>
												</table>
											</td>
											<td class="addAttr">操作人：<%=userName %></td>
											<td class="addAttr">时间: <%= dateTime%></td>
										</tr>
									</table>
								</td>
								
					</tr>
						
					<%
							DBRow[] waybillItems = wayBillMgrZJ.getWayBillOrderItems(waybill_order[i].get("waybill_id",0l));
					%>
							<tr>
								<td colspan="6">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" >
									<%
										for(int j=0;j<waybillItems.length;j++)
										{
											String backgroundColor = ( j % 2 == 1 ? "#f9f9f9":"white");
									 %>
									<tr style="line-height:22px;height:22px;background:<%=backgroundColor%>">
										<td width="80px" align="center" class="topBorder"><%=waybillItems[j].get("oid",0l)%></td>
										<td nowrap="nowrap" align="left" class="topBorder">
											<%=waybillItems[j].getString("p_name")%><font style="font-weight: bolder">&nbsp;&nbsp;X&nbsp;&nbsp;</font><%=waybillItems[j].get("quantity",0f)%>
											<%
					  	 						if(waybillItems[j].get("product_status",0)==ProductStatusKey.STORE_OUT)
					  	 						{
					  	 					%>
  	 										<a href="javascript:puchaseDeliveryCount(<%=waybillItems[j].get("pc_id",0l)%>,<%=waybill_order[i].get("ps_id",0l)%>,'deliveryCount')">[<%=productStatus.getOrderHandleById(waybillItems[j].getString("product_status"))%>]</a>
					  	 					<%
					  	 						}
					  	 					%>
										</td>
									</tr>
									<%
										}
									%>
								</table>
								</td>
							</tr>
							
							
					<%
						}
					%>
					
				</table>
</body>
</html>



