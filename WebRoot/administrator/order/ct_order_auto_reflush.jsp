<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.zr.OrderMgrZr"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@ include file="../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<jsp:useBean id="tracingOrderKey" class="com.cwc.app.key.TracingOrderKey"/>
<jsp:useBean id="handStatusleKey" class="com.cwc.app.key.HandStatusleKey"/>
<jsp:useBean id="productStatusKey" class="com.cwc.app.key.ProductStatusKey"/>
<jsp:useBean id="orderRateKey" class="com.cwc.app.key.OrderRateKey"/>
<%@ page import="com.cwc.app.beans.AdminLoginBean,com.cwc.app.key.AfterServiceKey,com.cwc.app.api.OrderMgr"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

String val = StringUtil.getString(request,"val");
String st = StringUtil.getString(request,"st");
String en = StringUtil.getString(request,"en");
String p = StringUtil.getString(request,"p","0");
String business = StringUtil.getString(request,"business");
int handle = StringUtil.getInt(request,"handle"); 
int after_service_status = StringUtil.getInt(request,"after_service_status"); 
int handle_status = StringUtil.getInt(request,"handle_status",0);
int product_status = StringUtil.getInt(request,"product_status",-1);

int product_type_int = StringUtil.getInt(request,"product_type_int");
 
String cmd = StringUtil.getString(request,"cmd");
int search_field = StringUtil.getInt(request,"search_field");
String p_name = StringUtil.getString(request,"p_name");
long filter_ps_id = StringUtil.getLong(request,"filter_ps_id");
int trace_type = StringUtil.getInt(request,"trace_type");
long order_note_adid = StringUtil.getLong(request,"order_note_adid"); 
int order_note_trace_type = StringUtil.getInt(request,"order_note_trace_type");
String order_source = StringUtil.getString(request,"order_source");

long product_line_id = StringUtil.getLong(request,"product_line_id",0l);
long catalog_id = StringUtil.getLong(request,"catalog_id",0l);

//获得仓库所属名称
DBRow detailStorage = catalogMgr.getDetailProductStorageCatalogById(adminLoggerBean.getPs_id());

WaybillInternalTrackingKey  waybillInternalTrackingKey = new WaybillInternalTrackingKey();
%> 
<html>  
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单管理</title>

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<%-- ***********************  --%> 

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<%-- Frank ****************** --%>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<%-- ***********************  --%>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<style type="text/css">
	body{text-align:left;}
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	


<!-- 引入右键弹出下拉框 -->
<script src="../js/contextMenu/jquery.contextMenu.js" type="text/javascript"></script>
<link type="text/css" href="../js/contextMenu/jquery.contextMenu.css" rel="stylesheet"/>
<script language="javascript">
$().ready(function() {
	<%-- Frank ****************** --%>
	addAutoComplete($("#p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info", 
			"p_name");
	
	addAutoComplete($("#tracking_send_order_p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info", 
			"p_name");
	<%-- ***********************  --%>
			
		

	
		
	$("#st").date_input();
	$("#en").date_input();
	
	$("#statistics_order_st").date_input();
	$("#statistics_order_en").date_input();
	
	$("#tracking_send_order_st").date_input();
	$("#tracking_send_order_en").date_input();
	

	$('#search_help').poshytip({
		content:"<span style='color:#666666'><strong>回车</strong>：精确匹配搜索<br><strong>右键</strong>：模糊匹配搜索(可同时搜索多个空格分隔关键词)<br><strong>范围</strong>：姓名、邮件、订单号、Transaction ID、eBay帐号、ItemNumber、运单号、国家</span>",
		className: 'tip-yellowsimple',
		showOn: 'hover',
		alignTo: 'target',
		alignX: 'right',
		alignY: 'center',
		showTimeout:0,
		offsetX: 0,
		offsetY: 5
	});


	//搜索条支持回车
	//监控统计选项卡中的商品回车查询
	$("#p_name").keydown(function(event){
		if (event.keyCode==13)
		{
			statisticsOrder();
		}
	});
    //发货追踪选项卡中的商品回车查询
	$("#tracking_send_order_p_name").keydown(function(event){
		if (event.keyCode==13)
		{
			trackingOrder();
		}
	});
	
	$("#search_key").keydown(function(event){
		if (event.keyCode==13)
		{
			searchOrder();
		}
	});

	$("#search_question_p_name").keydown(function(event){
		if (event.keyCode==13)
		{
			//
		}
	});
	
	$("#search_question_key").keydown(function(event){
		if (event.keyCode==13)
		{
			//searchQuestion();
		}
	});
		
});

function showOrderNote(oid,handle,after_service_status,handle_status,product_status,buyer_ip){
	var url = "oid="+oid+"&handle="+handle+"&after_service_status="+after_service_status+"&handle_status="+handle_status+"&product_status="+product_status+"&buyer_ip="+buyer_ip;
	$.artDialog.open("add_order_note.html?"+url, {title: "订单日志",width:'870px',height:'500px',fixed:true, lock: true,opacity: 0.3});

}

//填写订单备注
function addOrderNote(oid,handle,after_service_status,handle_status,product_status,buyer_ip)
{
	var url = "oid="+oid+"&handle="+handle+"&after_service_status="+after_service_status+"&handle_status="+handle_status+"&product_status="+product_status+"&buyer_ip="+buyer_ip;
	$.artDialog.open("add_note.html?"+url, {title: "添加Order Note",width:'420px',height:'280px',fixed:true, lock: true,opacity: 0.3});
	 return ;
	var trace_type_radio = "";
	//跟据订单状态，提供不同跟踪类型
	if (handle_status==<%=handStatusleKey.DOUBT%>)
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.DOUBT_TRACE%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.DOUBT_TRACE) )%> &nbsp;";
	}
	else if ( handle_status==<%=handStatusleKey.DOUBT_ADDRESS%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.DOUBT_ADDRESS_TRACE%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.DOUBT_ADDRESS_TRACE) )%> &nbsp;";
	}
	else if ( handle_status==<%=handStatusleKey.DOUBT_PAY%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.DOUBT_PAY_TRACE%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.DOUBT_PAY_TRACE) )%> &nbsp;";
	}
	
	
	//缺货跟进
	if ( product_status==<%=productStatusKey.STORE_OUT%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.LACKING_TRACE%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.LACKING_TRACE) )%> &nbsp;";
	}
	
	
	
	//售后问题
	if ( after_service_status==<%=AfterServiceKey.NORMAL_REFUNDING%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.NORMAL_REFUNDING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.NORMAL_REFUNDING) )%> &nbsp;";
	}
	if ( after_service_status==<%=AfterServiceKey.DISPUTE_REFUNDING%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.DISPUTE_REFUNDING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.DISPUTE_REFUNDING) )%> &nbsp;";
	}
	if ( after_service_status==<%=AfterServiceKey.PART_REFUNDED%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.PART_REFUNDED%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.PART_REFUNDED) )%> &nbsp;";
	}
	if ( after_service_status==<%=AfterServiceKey.ALL_REFUNDED%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.ALL_REFUNDED%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.ALL_REFUNDED) )%> &nbsp;";
	}	
	if ( after_service_status==<%=AfterServiceKey.NORMAL_WARRANTYING%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.NORMAL_WARRANTYING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.NORMAL_WARRANTYING) )%> &nbsp;";
	}	
	if ( after_service_status==<%=AfterServiceKey.DISPUTE_WARRANTYING%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.DISPUTE_WARRANTYING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.DISPUTE_WARRANTYING) )%> &nbsp;";
	}
	if ( after_service_status==<%=AfterServiceKey.WARRANTIED%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.WARRANTIED%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.WARRANTIED) )%> &nbsp;";
	}
	if ( after_service_status==<%=AfterServiceKey.OTHER_DISPUTING%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.OTHER_DISPUTING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.OTHER_DISPUTING) )%> &nbsp;";
	}

	if ( after_service_status==<%=AfterServiceKey.FREE_WARRANTIED%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.FREE_WARRANTIED%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.FREE_WARRANTIED) )%> &nbsp;";
	}
	if ( after_service_status==<%=AfterServiceKey.FAILURE_REFUND%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.FAILURE_REFUND%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.FAILURE_REFUND) )%> &nbsp;";
	}
	if ( after_service_status==<%=AfterServiceKey.FAILURE_WARRANTY%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.FAILURE_WARRANTY%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.FAILURE_WARRANTY) )%> &nbsp;";
	}
	if ( after_service_status==<%=AfterServiceKey.FAILURE_DISPUTING%> )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.FAILURE_DISPUTING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.FAILURE_DISPUTING) )%> &nbsp;";
	}
	
	
	
	if ( buyer_ip!="" )
	{
		trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.CREDIT_VERIFY%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.CREDIT_VERIFY) )%> &nbsp;";
	}
	
	trace_type_radio += "<input type='radio' name='tracingTypeWinText' id='tracingTypeWinText' value='<%=tracingOrderKey.OTHERS%>' /><span style='color:#FF6600'><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.OTHERS) )%></span> &nbsp;";

	


	$.prompt(
	
	"<div id='title'>订单跟进[单号:"+oid+"]</div><br /> "+trace_type_radio+"  <br><textarea name='orderNoteWinText' id='orderNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: promptCheckNote,
   		  //loaded:promptOnload,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/addPOrderNote.action";
						document.listForm.note.value = f.orderNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.trace_type.value = f.tracingTypeWinText;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function promptCheckNote(v,m,f)
{
	if (v=="y")
	{
		  //an = m.children('#orderNoteWinText');
          if ( typeof(f.tracingTypeWinText)=="undefined")
		  {
				alert("请选择跟进类型");
				return false;
		 }							
		 else  if(f.orderNoteWinText == "")
		  {
			   alert("请填写备注");
			   
				return false;
		  }
		  return true;
	}
}
function refreshWindow(){
	window.location.reload();
}
//修改备注
function modOrderNote(oid,on_id)
{
	$.prompt(
	
	"<div id='title'>修改备注[订单:"+oid+"]</div><br /><textarea name='modOrderNoteWinText' id='modOrderNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.modOrderNoteWinText == "")
						  {
							   alert("请填写备注");
							   
								return false;
						  }
						  return true;
					}
				}
		  ,
   		  loaded:
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailOrderNoteJSON.action",
							{on_id:on_id},
							function callback(data)
							{   
								$("#modOrderNoteWinText").val(data.note);
							}
					);

				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/modOrderNote.action";
						document.listForm.note.value = f.modOrderNoteWinText;
						document.listForm.on_id.value = on_id;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

//修改运单号
function modOrderDeliveryNo(oid)
{
	var old_ems_id;
	
	$.prompt(
	
	"<div id='title'>修改运单号[订单:"+oid+"]</div><br /><input name='ems_idWinText' type='text' id='ems_idWinText'  style='width:200px;'>",
	
	{
		
		
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.ems_idWinText == "")
						  {
							   alert("请填写运单号");
							   
								return false;
						  }
						  return true;
					}
				}
		  ,
   		  loaded:
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailOrderDeliveryInfoByOidJSON.action",
							{oid:oid},
							function callback(data)
							{   
								$("#ems_idWinText").val(data.ems_id);
								old_ems_id = data.ems_id;
							}
					);

				}
		  , 
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.oid.value = oid;
						document.listForm.ems_id.value = f.ems_idWinText;
						document.listForm.old_ems_id.value = old_ems_id;
						document.listForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/modPOrderProductInfo.action";
						document.listForm.submit();	
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function duplicateDelivery(oid)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailOrderJSON.action",
			{oid:oid},
				function callback(data)
				{
					if (data=="")
					{
						alert("订单不存在！");
					}
					else
					{
						$("#address_nameWinText").val(data.address_name);
						$("#address_streetWinText").setSelectedValue(data.address_street);
						$("#address_cityWinText").val(data.address_city);
						$("#address_stateWinText").val(data.address_state);
						$("#address_zipWinText").val(data.address_zip);
						$("#telWinText").val(data.tel);
					}
				}
			);
}





//修改详细递送信息
function modOrderDeliveryInfo(oid)
{
	tb_show('修改配送信息[订单:'+oid+']','mod_order_delivery.html?oid='+oid+'&TB_iframe=true&height=350&width=300',false);
}
function modOrderDeliveryInfoZHJ(oid)
{
	$.prompt(
	
	"<div id='title'>修改递送信息[订单:"+oid+"]</div><br><span style='font-weight:normal'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;从订单 <input name='duplicate_oid' type='text' id='duplicate_oid'  style='width:60px;color:red' onChange=\"duplicateDelivery(this.value)\"> 复制地址信息</span><br />Name<br><input name='address_nameWinText' type='text' id='address_nameWinText'  style='width:300px;'><br>Street<br><input name='address_streetWinText' type='text' id='address_streetWinText'  style='width:300px;'><br>City<br><input name='address_cityWinText' type='text' id='address_cityWinText'  style='width:300px;'><br>State<br><select name='pro_id' id='pro_id'></select><input name='address_stateWinText' type='text' id='address_stateWinText'  style='width:300px;'><br>Zip<br><input name='address_zipWinText' type='text' id='address_zipWinText'  style='width:300px;'><br>Tel<br><input name='telWinText' type='text' id='telWinText'  style='width:300px;'><input name='country_id' type='hidden' id='country_id'/>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{

						  if(f.address_nameWinText == "")
						  {
							   alert("请填写Name");
							   
								return false;
						  }
						  else if(f.address_streetWinText == "")
						  {
							   alert("请填写Street");
							   
								return false;
						  }
						  else if(f.address_cityWinText == "")
						  {
							   alert("请填写City");
							   
								return false;
						  }
						  else if(f.address_stateWinText == "")
						  {
							   alert("请填写State");
							   
								return false;
						  }
						  else if(f.address_zipWinText == "")
						  {
							   alert("请填写Zip");
							   
								return false;
						  }

						  
						  return true;
					}
				}
		  ,
   		  loaded:
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailOrderDeliveryInfoByOidJSON.action",
							{oid:oid},
							function callback(data)
							{   
								$("#address_nameWinText").val(data.address_name);
								$("#address_streetWinText").val(data.address_street);
								$("#address_cityWinText").val(data.address_city);
								$("#address_stateWinText").val(data.address_state);
								$("#address_zipWinText").val(data.address_zip);
								$("#telWinText").val(data.tel);
								$("#country_id").val(data.ccid);
								if(data.ccid!=0)
								{
									getStorageProvinceByCcid(data.ccid);
								}
								
							}
					);

				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.tel.value = f.telWinText;
						document.listForm.oid.value = oid;
						document.listForm.address_name.value = f.address_nameWinText;
						document.listForm.address_street.value = f.address_streetWinText;
						document.listForm.address_city.value = f.address_cityWinText;
						document.listForm.address_state.value = f.address_stateWinText;
						document.listForm.address_zip.value = f.address_zipWinText;
						document.listForm.ccid.value = f.country_id;
						
						document.listForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/modPOrderDeliveryInfo.action";
						document.listForm.submit();	
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

//国家地区切换
function getStorageProvinceByCcid(ccid)
{

		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#pro_id").addOption("Others","-1");
					}		
				}
		);
}


//手工添加订单


function go2page(p) 
{
	
	ajaxLoadCtOrderPage("&cmd="+document.dataForm.cmd.value+"&p="+p+"&val="+document.dataForm.val.value+"&st="+document.dataForm.st.value+"&en="+document.dataForm.en.value+"&business="+document.dataForm.business.value+"&handle="+document.dataForm.handle.value+"&handle_status="+document.dataForm.handle_status.value+"&product_status="+document.dataForm.product_status.value+"&product_type_int="+document.dataForm.product_type_int.value+"&ps_id="+document.dataForm.ps_id.value+"&search_field="+document.dataForm.search_field.value+"&p_name="+document.dataForm.p_name.value+"&filter_ps_id="+document.dataForm.filter_ps_id.value+"&trace_type="+document.dataForm.trace_type.value+"&order_note_adid="+document.dataForm.order_note_adid.value+"&order_note_trace_type="+document.dataForm.order_note_trace_type.value+"&order_source="+document.dataForm.order_source.value+"&bad_feedback_flag="+document.dataForm.bad_feedback_flag.value+"&after_service_status="+document.dataForm.after_service_status.value+"&product_line_id="+document.dataForm.product_line_id.value+"&catalog_id="+document.dataForm.catalog_id.value+"&ca_id="+document.dataForm.ca_id.value+"&ccid="+document.dataForm.ccid.value+"&pro_id="+document.dataForm.pro_id.value+"&cost="+document.dataForm.cost.value+"&cost_type="+document.dataForm.cost_type.value+"&unfinished="+document.dataForm.unfinished.value+"&internal_tracking_status="+document.dataForm.internal_tracking_status.value+"&tracking_date_type="+document.dataForm.tracking_date_type.value+"&last_day="+document.dataForm.last_day.value+"&search_mode="+document.dataForm.search_mode.value);
}

function del()
{
	if (confirm("确认你的操作吗？"))
	{
		return(true);
	}
	else
	{
		return(false);
	}
}




function selectOneElement()
{
	if ( document.listForm.oid_sl.length>0 )
	{
		for (i=0; i<document.listForm.oid_sl.length; i++)
		{
			if(document.listForm.oid_sl[i].checked)
			{
				return(true);
			}
		}
		return(false);
	}
	else
	{
		if(document.listForm.oid_sl.checked)
		{
			return(true);
		}
		return(false);
	}
	
}

function selectAll(cSelect)
{

}

function searchOrder()
{
	var val = $("#search_key").val();
	
	if (val=="")
	{
		alert("你好像忘记填写关键词了？");
	}
	else
	{
		val = val.replace(/\"/g,'');
		var val_search = "\""+val.toLowerCase()+"\"";
		$("#search_key").val(val_search);
		//ajaxLoadCtOrderPage("val="+val+"&cmd=search&search_field=0");
		ajaxLoadCtOrderPage("val="+val+"&cmd=search&search_field=1&search_mode=1"); //Frank, 无论鼠标左右键，改为全部走Lucene
	}
}

function searchOrderRightButton()
{
	var val = $("#search_key").val();
	
	if (val=="")
	{
		alert("你好像忘记填写关键词了？");
	}
	else
	{
		val = val.replace(/\"/g,'');
		$("#search_key").val(val);
		ajaxLoadCtOrderPage("val="+val+"&cmd=search&search_field=1&search_mode=2");
	}
}

function searchEmail(val)
{
	
	var search_field = 1;

	if (val=="")
	{
		alert("请填写关键词");
	}
	else
	{
		var val_text = "\""+val.toLowerCase()+"\"";
		$("#search_key").val(val_text);
		ajaxLoadCtOrderPage("val="+val+"&cmd=search&search_field="+search_field+"&search_mode=1");
	}
}

function searchEbayID(val)
{
	
	var search_field = 1;

	if (val=="")
	{
		alert("请填写关键词");
	}
	else
	{
		var val_text = "\""+val.toLowerCase()+"\"";
		$("#search_key").val(val_text);
		ajaxLoadCtOrderPage("val="+val+"&cmd=search&search_field="+search_field+"&search_mode=1");
	}
}

function searchName(val)
{
	
	var search_field = 1;

	if (val=="")
	{
		alert("请填写关键词");
	}
	else
	{
		var val_text = "\""+val.toLowerCase()+"\"";
		$("#search_key").val(val_text);
		ajaxLoadCtOrderPage("val="+val+"&cmd=search&search_field="+search_field+"&search_mode=1");
	}
}

function searchOid(val)
{
	
	var search_field = 1;

	if (val=="")
	{
		alert("请填写关键词");
	}
	else
	{
		ajaxLoadCtOrderPage("val="+val+"&cmd=search&search_field="+search_field+"&search_mode=1");
	}
}

function searchMultiField(val)
{
	
	var search_field = 1;

	if (val=="")
	{
		alert("请填写关键词");
	}
	else
	{
		val = "\""+val.toLowerCase()+"\"";
		ajaxLoadCtOrderPage("val="+val+"&cmd=search&search_field="+search_field+"&search_mode=1");
	}
}



function getWait4PrintOrders()
{
	var st = document.filterForm.st.value;
	var en = document.filterForm.en.value;
	var business = document.filterForm.business.value;
	var ps_id = <%=adminLoggerBean.getPs_id()%>;
	var product_type_int = document.filterForm.product_type_int.value;
	
	ajaxLoadWait4PrintOrdersCount();
	ajaxLoadCtOrderPage("ps_id="+ps_id+"&business="+business+"&st="+st+"&en="+en+"&product_type_int="+product_type_int+"&cmd=print");
}



function filter(flag)
{
	var st = document.filterForm.st.value;
	var en = document.filterForm.en.value;
	var business = document.filterForm.business.value;
	var product_type_int = document.filterForm.product_type_int.value;
	
	var p = <%=p%>;
	
	var handle_status = $("#handle_status").val();
	var after_service_status = $("#after_service_status").val();
	var handle = $("#handle").val();
	var product_status = $("#product_status").val();
	var p_name = $("#p_name").val();
	var filter_ps_id = $("#filter_ps_id").val();
	var order_note_adid = $("#order_note_adid").val();
	var order_note_trace_type = $("#order_note_trace_type").val();
	var order_source = $("#order_source").val();
	var bad_feedback_flag = $("#bad_feedback_flag").val();

	//如果是售后状态（进行时）查询，需要把起始日期调小，以免老订单查不到
	if (after_service_status==6||after_service_status==7||after_service_status==10||after_service_status==11||after_service_status==13)
	{
		st = "2000-1-1";
	}
	
	
	
	var para = "st="+st+"&en="+en+"&business="+business+"&handle="+handle+"&handle_status="+handle_status+"&product_status="+product_status+"&product_type_int="+product_type_int+"&p_name="+p_name+"&filter_ps_id="+filter_ps_id+"&trace_type=<%=trace_type%>&order_note_adid="+order_note_adid+"&order_note_trace_type="+order_note_trace_type+"&order_source="+order_source+"&bad_feedback_flag="+bad_feedback_flag+"&after_service_status="+after_service_status;
	
	//默认载入页面和backurl返回
	if (flag==0)
	{
		para = para + "&p="+p;
	}
	var isProductExit = "true";

	if(p_name.length>0)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductByPnameJSON.action',
				type: 'post',
				async:false,
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){
					
				},
				
				error: function(e){
					isProductExit = false;
				},
				
				success: function(data)
				{
					if(data.product_id=="0")
					{
						isProductExit = "false";
					}
				}
			});
	}

	if(isProductExit=="true")
	{
		ajaxLoadCtOrderPage(para);
	}
	else
	{
		alert(p_name+"无此商品！")
	}

}

function internalTracingChange(last_day)
{
	var intLastDay = parseInt(last_day);
	if(intLastDay==last_day&&intLastDay>0)
	{
		$("#internal_tracking_status").val(<%=WaybillInternalTrackingKey.NotYetDeliveryComplete%>);
	}
}

function cleanInternalTracking()
{
	$("#last_day").val("");
}

function trackingOrder()
{
	var st = document.tracking_send_order.st.value;
	var en = document.tracking_send_order.en.value;
	
	var p = <%=p%>;
	
	var product_line_id = $("#tracking_send_order_filter_productLine").val();
	var catalog_id = $("#tracking_send_order_filter_pcid").val();
	var ca_id = $("#tracking_send_order_sale_area").val();
	var ccid = $("#tracking_send_order_ccid_hidden").val();
	var pro_id = $("#tracking_send_order_pro_id").val();
	
	var p_name = $("#tracking_send_order_p_name").val();
	
	var tracking_date_type = $("#tracking_date_type").val();
	var last_day = $("#last_day").val();
	var internal_tracking_status = $("#internal_tracking_status").val();
	
	var para = "st="+st+"&en="+en+"&cmd=trackingOrder&p_name="+p_name+"&product_line_id="+product_line_id+"&catalog_id="+catalog_id+"&ca_id="+ca_id+"&ccid="+ccid+"&pro_id="+pro_id+"&tracking_date_type="+tracking_date_type+"&last_day="+last_day+"&internal_tracking_status="+internal_tracking_status;
	
	var isProductExit = "true";

	if(p_name.length>0)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductByPnameJSON.action',
				type: 'post',
				async:false,
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){
					
				},
				
				error: function(e){
					isProductExit = false;
				},
				
				success: function(data)
				{
					if(data.product_id=="0")
					{
						isProductExit = "false";
					}
				}
			});
	}

	if(isProductExit=="true")
	{
		ajaxLoadCtOrderPage(para);
	}
	else
	{
		alert(p_name+"无此商品！")
	}
	
}


function statisticsOrder()
{
	var st = document.statistics_order.st.value;
	var en = document.statistics_order.en.value;
	
	var p = <%=p%>;
	
	var product_line_id = $("#statistics_order_filter_productLine").val();
	var catalog_id = $("#statistics_order_filter_pcid").val();
	var ca_id = $("#sale_area").val();
	var ccid = $("#ccid_hidden").val();
	var pro_id = $("#pro_id").val();
	
	var p_name = $("#p_name").val();
	
	var cost = $("#cost").val();
	var cost_type = $("input[name='cost_type']:checked").val();
	var unfinished = $("#unfinished").val();

	var para = "st="+st+"&en="+en+"&cmd=statisticsOrder&p_name="+p_name+"&product_line_id="+product_line_id+"&catalog_id="+catalog_id+"&ca_id="+ca_id+"&ccid="+ccid+"&pro_id="+pro_id+"&cost="+cost+"&cost_type="+cost_type+"&unfinished="+unfinished;
	//默认载入页面和backurl返回
//	if (flag==0)
//	{
//		para = para + "&p="+p;
//	}
	var isProductExit = "true";

	if(p_name.length>0)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductByPnameJSON.action',
				type: 'post',
				async:false,
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){
					
				},
				
				error: function(e){
					isProductExit = false;
				},
				
				success: function(data)
				{
					if(data.product_id=="0")
					{
						isProductExit = "false";
					}
				}
			});
	}

	if(isProductExit=="true")
	{
		ajaxLoadCtOrderPage(para);
	}
	else
	{
		alert(p_name+"无此商品！")
	}

}

function exportPaypalOrder()
{
	document.filterForm.st.value = document.filterForm.st_year.value+"-"+document.filterForm.st_month.value+"-"+document.filterForm.st_day.value
	document.filterForm.en.value = document.filterForm.en_year.value+"-"+document.filterForm.en_month.value+"-"+document.filterForm.en_day.value
	document.filterForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>exportPorder2excel";
	document.filterForm.submit();
}


function batchModifyHandleStatus()
{
	if ( selectOneElement() )
	{
		if ( confirm("确认执行批量修改状态吗？") )
		{
		
			if ( document.getElementById("handle_status_batch").value.split("-")[1]=="handlestatus" )
			{
				document.listForm.batch_modify_handle_type.value = "handlestatus";
			}
			else
			{
				document.listForm.batch_modify_handle_type.value = "handle";
			}

			document.listForm.mod_order_handle_status_note.value = document.getElementById("handle_status_batch").options[document.getElementById("handle_status_batch").selectedIndex].text;			//	写到操作日志里面的备注
			document.listForm.handle_status.value = document.getElementById("handle_status_batch").value.split("-")[0];
			document.listForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/modPOrderHandleStatus.action";
			document.listForm.submit();
		}
	}
	else
	{
		alert("请先选择一个订单");
	}
}




function delOrder()
{
	oid = menuOid;
	
	if ( confirm("确认删除订单["+oid+"]？") )
	{
		document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/delOrder.action";
		document.listForm.oid.value = oid;
		document.listForm.submit();
	}
}


function cancelOrder()
{
	oid = menuOid;

	$.prompt(
	
	"<div id='title'>取消原因[订单:"+oid+"]</div><br /><textarea name='cancelOrderNoteWinText' id='cancelOrderNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.cancelOrderNoteWinText  == "")
						  {
							   alert("请填写取消原因");
							   
								return false;
						  }
						  return true;
					}

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/cancelOrder.action";
						document.listForm.note.value = "取消："+f.cancelOrderNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}

function hangUpOrder()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>挂起原因[订单:"+oid+"]</div><br /><textarea name='hangUpOrderNoteWinText' id='hangUpOrderNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.hangUpOrderNoteWinText  == "")
						  {
							   alert("请填写挂起原因");
							   
								return false;
						  }
						  return true;
					}

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/hangUpLongDoubtOrder.action";
						document.listForm.note.value = "挂起："+f.hangUpOrderNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}


function archiveOrder(oid)
{
	$.prompt(
	
	"<div id='title'>归档原因[订单:"+oid+"]</div><br /><textarea name='archiveOrderNoteWinText' id='archiveOrderNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.archiveOrderNoteWinText == "")
						  {
							   alert("请填写归档原因");
							   
								return false;
						  }
						  return true;
						}

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/archiveOrder.action";
						document.listForm.note.value = "归档："+f.archiveOrderNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function uploadOrderAppend(oid)
{
	document.getElementById('append_oid').value = oid;
	var result = window.showModalDialog('upload_b_if.html',document,"dialogWidth:20;dialogHeight:13;status:no;help:no");

	if ( !(typeof result == 'undefined') )
	{
		window.location.reload();
	}

}


function delOrderAppend(oid,append_name)
{
	if ( confirm("确认删除订单 "+oid+" 附件？") )
	{
		document.del_order_append_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/delOrderAppend.action";
		document.del_order_append_form.oid.value=oid;
		document.del_order_append_form.append_name.value=append_name;
		document.del_order_append_form.submit();
	}
}

function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.st.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.en.value = date;
}

function orderLogs(oid)
{
	tb_show('订单处理日志','logs.html?oid='+oid+'&height=400&width=750',false);
}

function relationOrder1(txn_id,buyer_ip)
{
	tb_show('相关订单',"relation_orders.html?txn_id="+txn_id+"&buyer_ip="+buyer_ip+"&height=400&width=850",false);
}

function relationOrder(oid)
{
	$.artDialog.open("relation_orders.html?oid="+oid, {title: "交易信息",width:'870px',height:'500px',fixed:true, lock: true,opacity: 0.3});
}

function lackingList(oid,ps_id)
{
	tb_show('缺货清单',"lacking_list.html?oid="+oid+"&ps_id="+ps_id+"&height=300&width=500",false);
}
 //抄单
function recordOrder(oid,handle,txn_id,orderSource)
{
	var flag = true;
	if ( handle>1 )
	{
		if ( !confirm("该订单已抄，确认继续抄单？") )
		{
			flag = false;
		}
	}
		
	if (flag)
	{
		$.artDialog.open("new_record_order.html?oid="+oid+"&txn_id="+txn_id+"&orderSource="+orderSource, {title: "抄单["+'订单号' + oid +"]",width:'870px',height:'500px',fixed:true, lock: true,opacity: 0.3});
		
		//tb_show('抄单 [订单号：'+oid+']',+"&TB_iframe=true&height=500&width=850",false);
	}
}

function showWaybill(oid){
	//tb_show('查看运单[内部订单号：'+oid+']',"show_waybill.html?oid="+oid+"&TB_iframe=true&height=500&width=850",false);
	var uri = 'show_waybill.html';
	uri += "?oid=" + oid;
	$.artDialog.open(uri, {title: "["+'内部订单号' + oid +"]",width:'750px',height:'400px',fixed:true, lock: true,opacity: 0.3});
}

function closeTBWin()
{
	tb_remove();
}

function closeTBWinRefrech()
{
	tb_remove();
	window.location.reload();
}

function closeTBWinSearchOrder()
{
	tb_remove();
	
	var val = $("#search_key").val();
	if (val=="")
	{
		window.location.reload();
	}
	else
	{
		searchOrder();
	}
	
}

function modelListTask(oid)
{
	tb_show('订单任务记录','model_list_task.html?oid='+oid+'&TB_iframe=true&height=400&width=850',false);
}





function trueOrder(oid,product_type,tel,ccid,note,ps_id,inv_dog,inv_uv,inv_tv,inv_rfe,inv_di_id,delivery_note,parentid,recom_express,pro_id)
{

	//如果是合并订单，检查主订单发货仓库是否跟子订单一致

			var para = "ps_id="+ps_id+"&oid="+oid+"&parentid="+parentid;
		
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/checkDeliveryStorageAjax.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					//alert("提交失败，请重试！");
				},
				
				success: function(html){

					if (html=="ok")
					{
						document.cart_form.oid.value=oid;
						document.cart_form.ps_id.value=ps_id;
						document.cart_form.product_type.value=product_type;
						document.cart_form.tel.value=tel;
						document.cart_form.record_order_note.value="完成抄单"+note;
						document.cart_form.handle_status.value=<%=HandStatusleKey.NORMAL%>;//正常
						document.cart_form.ccid.value=ccid;
						document.cart_form.inv_dog.value=inv_dog;
						document.cart_form.inv_uv.value=inv_uv;
						document.cart_form.inv_tv.value=inv_tv;
						document.cart_form.inv_rfe.value=inv_rfe;
						document.cart_form.inv_di_id.value=inv_di_id;
						document.cart_form.delivery_note.value=delivery_note;
						document.cart_form.recom_express.value=recom_express;
						document.cart_form.pro_id.value=pro_id;
						document.cart_form.submit();
					}
					else
					{
						alert("父订单与子订单发货仓库不一致！");
					}
				}
			});
}



function doubtTypeOrder(oid,product_type,tel,ccid,note,ps_id,inv_dog,inv_uv,inv_tv,inv_rfe,inv_di_id,delivery_note,handle_status,pro_id)
{
		document.cart_form.oid.value=oid;
		document.cart_form.ps_id.value=ps_id;
		document.cart_form.product_type.value=product_type;
		document.cart_form.tel.value=tel;
		document.cart_form.handle_status.value=handle_status;//存在疑问
		document.cart_form.record_order_note.value=note;
		document.cart_form.order_error_email.value=-1;//不再发邮件
		document.cart_form.ccid.value=ccid;
		document.cart_form.inv_dog.value=inv_dog;
		document.cart_form.inv_uv.value=inv_uv;
		document.cart_form.inv_tv.value=inv_tv;
		document.cart_form.inv_rfe.value=inv_rfe;
		document.cart_form.inv_di_id.value=inv_di_id;
		document.cart_form.delivery_note.value=delivery_note;
		document.cart_form.pro_id.value=pro_id;
		document.cart_form.submit();		
}

function parentDoubtTypeOrder(oid,tel,ccid,note,handle_status,pro_id,address_name,address_street,address_city,address_zip)
{
		document.save_form.oid.value=oid;
		document.save_form.tel.value=tel;
		document.save_form.handle_status.value = handle_status;//存在疑问
		document.save_form.record_order_note.value=note;
		document.save_form.order_error_email.value=-1;//不再发邮件
		document.save_form.ccid.value=ccid;
		document.save_form.pro_id.value=pro_id;
		document.save_form.address_name.value =address_name;
		document.save_form.address_street.value = address_street;
		document.save_form.address_city.value = address_city;
		document.save_form.address_zip.value = address_zip; 
		document.save_form.submit();	
}



function mergeOrder(oid)
{
	$.prompt(
	
	"<div id='title'>合并订单["+oid+"]到……</div><br />父订单号&nbsp;<input name='proTextParentId' type='text' id='proTextParentId' style='width:150px;'>",
	
	{
	      submit: 
		  	function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.proTextParentId == "")
						  {
							   alert("请填写父订单号");
							   
								return false;
						  }
						  return true;
					}
				}
		  ,
   		  //loaded:promptOnload,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						//先验证两个订单的递送人信息是否一致
						$.post("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/validateMergeorderDeliveryInfo.action", 
							{ oid: oid, parent_oid: f.proTextParentId },
						
							function (data, textStatus)
							{
								if (data=="false")
								{
									if (confirm("合并的订单收件人信息不一致，确认合并吗？"))
									{
										document.merge_order_form.oid.value = oid;
										document.merge_order_form.parent_oid.value = f.proTextParentId;
										document.merge_order_form.submit();		
									}
								}
								else
								{
										document.merge_order_form.oid.value = oid;
										document.merge_order_form.parent_oid.value = f.proTextParentId;
										document.merge_order_form.submit();
								}
							}
						
						, "html");
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function undoMergeOrder(oid,parent_oid)
{
	if (confirm("确认取消合并？"))
	{
		document.undo_merge_order_form.oid.value = oid;
		document.undo_merge_order_form.parent_oid.value = parent_oid;
		document.undo_merge_order_form.submit();	
	}
}


function verifyOrderCost(oid)
{
	$.prompt(
	
	"<div id='title'>审核订单成本[单号:"+oid+"]</div><br /><textarea name='orderVerifyNoteWinText' id='orderVerifyNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.orderVerifyNoteWinText=="")
						{
							alert("请填写备注");
							return(false);
						}
					}
				}
		  ,
   		  
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.verify_order_cost_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/verifyOrderCost.action";
						document.verify_order_cost_form.verify_note.value = f.orderVerifyNoteWinText;
						document.verify_order_cost_form.oid.value = oid;
						document.verify_order_cost_form.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 审核通过: "y", 取消: "n" }
	});
}




function tb_show_callback(url)
{

}

function tb_remove_callback(url)
{

}


function getOrderWeight(oid)
{
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getOrderWeightAjax.action',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:'oid='+oid,
		
		beforeSend:function(request){
					},
		
		error: function(){
			
		},
		
		success: function(data){
			var weight_kg = data + "Kg";
			var weight_lb = Number(data*1*2.2).toFixed(1) + "Lb";//磅
			
			$("#order_weight_"+oid).html(weight_kg+"&nbsp;,&nbsp;"+weight_lb);
		}
	});
}

function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) {  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  

	document.getElementById("eso_search").onmouseup=function(oEvent) {  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
         	searchOrderRightButton();
    	}  
	}  
}

$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
		    '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
    		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		
		// 设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.8' 
		},
	baseZ: 99999, 
    centerX: true,
    centerY: true, 
	
		fadeOut:  1000
};

//平滑上翻
jQuery.fn.topLink = function(settings) {
	settings = jQuery.extend({
		min: 1,
		fadeSpeed: 200
	}, settings);
	return this.each(function() {
		//listen for scroll
		var el = $(this);
		el.hide(); //in case the user forgot
		$(window).scroll(function() {
			if($(window).scrollTop() >= settings.min)
			{
				el.fadeIn(settings.fadeSpeed);
			}
			else
			{
				el.fadeOut(settings.fadeSpeed);
			}
		});
	});
};


function preFiveTracking(oid)
{
	document.getElementById("all_tracking_"+oid).style.display = "none";
	document.getElementById("pre_five_tracking_"+oid).style.display = "";

	//$("#pre_five_tracking_button_"+oid).addClass("aaaaaa");
	//$("#all_tracking_button_"+oid).addClass("bbbbbb");
}

function allTracking(oid)
{
	document.getElementById("all_tracking_"+oid).style.display = "";
	document.getElementById("pre_five_tracking_"+oid).style.display = "none";

	//$("#pre_five_tracking_button_"+oid).addClass("bbbbbb");
	//$("#all_tracking_button_"+oid).addClass("aaaaaa");	
}

var menuOid = 0;
function funMenu(obj,oid,handle,after_service_status,handle_status,bad_feedback_flag,txn_id,mc_gross,currencyCode)//差评标记
{
	//菜单ID必须每次更换，否则会有缓存造成的菜单不更新
 	var randomStr = Math.random()+"";
	randomStr = randomStr.substring(5,12);
	var menuObj = "mm_"+randomStr;

	menuOid = oid;
	
	var x = $(obj).offset().left;
	var y = $(obj).offset().top;

	var menuContext = "";
	
	menuContext += "<div id='"+menuObj+"' class='easyui-menu' style='width:120px;'>";

	if ( handle!=<%=HandleKey.DELIVERIED%>&&handle_status!=<%=HandStatusleKey.CANCEL%>&&handle_status!=<%=HandStatusleKey.ARCHIVE%>&&!(handle>=<%=AfterServiceKey.NORMAL_REFUNDING%>&&handle<=<%=AfterServiceKey.FREE_SHIPPING_FEE_WARRANTY%>) )
	{
		if (handle_status!=<%=HandStatusleKey.ADDITION_MONEY%>)
		{
			menuContext += "<div  icon='icon-hangup' onclick='addOrderInvoice()'>订单In</div>";
		}
		
		<tst:authentication bindAction="com.cwc.app.api.OrderMgr.hangUpLongDoubtOrder">
		menuContext += "<div onclick='hangUpOrder()'>挂起</div>";
		</tst:authentication>
	}

	
	if ( handle==<%=HandleKey.DELIVERIED%>||(after_service_status>=<%=AfterServiceKey.NORMAL_REFUNDING%>&&after_service_status<=<%=AfterServiceKey.FREE_SHIPPING_FEE_WARRANTY%>) )
	{


	
	if (handle==<%=HandleKey.DELIVERIED%>)
	{
		menuContext += "<div onclick='markNormalWarrantying()'>";
		menuContext += "<span>申请服务</span>";
		menuContext += "</div>";
	}
	
		
	if ( (handle==<%=HandleKey.DELIVERIED%>&&after_service_status==0)||after_service_status==<%=AfterServiceKey.OTHER_DISPUTING%> )
	{
		menuContext += "<div >";
		menuContext += "	<span>其他问题</span>";
		menuContext += "	<div style='width:120px;'>";
		if (after_service_status!=<%=AfterServiceKey.OTHER_DISPUTING%>)
		{
			menuContext += "		<div icon='icon-other-warranty' onclick='markOtherDispute()'>其他争议</div>";
			menuContext += "<div class='menu-sep'></div>";
		}
		
		if (after_service_status==<%=AfterServiceKey.OTHER_DISPUTING%>)
		{
			menuContext += "		<div   onclick='markFailureDispute()'>争议退款</div>";
			menuContext += "		<div   onclick='markBadDispute()'>争议差评</div>";
			menuContext += "		<div icon='icon-redo'   onclick='cancelDispute()'>撤销争议</div>";
		}
		
		menuContext += "	</div>";
		menuContext += "</div>";	
	}

	}

	menuContext += "<div class='menu-sep'></div>";

	//不等于取消、已发货、归档
	if ( handle!=<%=HandleKey.DELIVERIED%>&&handle_status!=<%=HandStatusleKey.CANCEL%>&&handle_status!=<%=HandStatusleKey.ARCHIVE%>&&!(after_service_status>=<%=AfterServiceKey.NORMAL_REFUNDING%>&&after_service_status<=<%=AfterServiceKey.FREE_SHIPPING_FEE_WARRANTY%>) )
	{
		<tst:authentication bindAction="com.cwc.app.api.OrderMgr.cancelOrder">
		menuContext += "<div icon='icon-cancel-2' onClick='cancelOrder()'>取消</div>";
		</tst:authentication>
		
		<tst:authentication bindAction="com.cwc.app.api.OrderMgr.delOrder">
		menuContext += "<div icon='icon-cancel' onClick='delOrder()'>删除</div>";
		</tst:authentication>
	}



<tst:authentication bindAction="com.cwc.app.api.OrderMgr.markBadFeedBack">
	if ( bad_feedback_flag==0 ) 
	{
		menuContext += "<div class='menu-sep'></div>";
		menuContext += "<div>";
		menuContext += "<span>评价订单</span>";
		menuContext += "	<div style='width:120px;'>";
		
		menuContext += "<div  onclick=\"badFeedBack("+menuOid+",<%=orderRateKey.BAD%>,'<%=orderRateKey.getOrderRateStatusById(orderRateKey.BAD)%>')\">差评</div>";
		menuContext += "<div  onclick=\"badFeedBack("+menuOid+",<%=orderRateKey.NORMAL%>,'<%=orderRateKey.getOrderRateStatusById(orderRateKey.NORMAL)%>')\">中评</div>";
		menuContext += "<div  onclick=\"badFeedBack("+menuOid+",<%=orderRateKey.PAYPAL_COMPLAIN%>,'<%=orderRateKey.getOrderRateStatusById(orderRateKey.PAYPAL_COMPLAIN)%>')\">PayPal投诉</div>";
		menuContext += "<div  onclick=\"badFeedBack("+menuOid+",<%=orderRateKey.EBAY_COMPLAIN%>,'<%=orderRateKey.getOrderRateStatusById(orderRateKey.EBAY_COMPLAIN)%>')\">EBay投诉</div>";
		
		menuContext += "</div>";
		menuContext += "</div>";
	}
</tst:authentication>
if(txn_id!=""&&mc_gross!="")
{
		menuContext += "<div class='menu-sep'></div>";
		menuContext += "<div onclick='applyRefund(\""+txn_id+"\",\""+mc_gross+"\",\""+currencyCode+"\")'>";
		menuContext += "<span >退款申请</span>";
		menuContext += "</div>";
}	
		
	 	
	menuContext += "</div>";
	

	$('#system_menu').hide();
	$('#system_menu').html(menuContext);	


	$("<scri"+"pt>"+"</scr"+"ipt>").attr({src:'../js/easyui/jquery.easyui.menu.js',type:'text/javascript',id:'load'}).appendTo($('head').remove('#loadScript'));

	$('#'+menuObj).menu('show',{
		left: x,
		top: y+20
	});
}


function applyRefund(txn_id,mc_gross,currencyCode)
{
	oid = menuOid;
	//tb_show('退款申请','startRefund.html?oid='+oid+'&txn_id='+txn_id+'&mc_gross='+mc_gross+'&currencyCode='+currencyCode+'TB_iframe=true&height=300&width=500',false);
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/refund/apply_refund.html?oid="+oid+"&txn_id="+txn_id+"&mc_gross="+mc_gross+"&currencyCode="+currencyCode;
  	 $.artDialog.open(uri , {title: '退款申请',width:'550px',height:'330px', lock: true,opacity: 0.3,fixed:true});
}
function printMenu(oid,handle,handle_status,product_status,parent_oid,ccid,pro_id,ps_id)
{
	tb_show('更多快递','more_print.html?oid='+oid+'&handle='+handle+'&handle_status='+handle_status+'&product_status='+product_status+'&parent_oid='+parent_oid+'&ccid='+ccid+'&pro_id='+pro_id+'&ps_id='+ps_id+'&TB_iframe=true&height=300&width=650',false);	
}




function addOrderInvoice(){
	var oid = menuOid;
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/convert_order_to_bill.html?order_id=" + oid;
	 $.artDialog.open(uri , {title: '订单 ['+oid+'] 补款',width:'1100px',height:'530px', lock: true,opacity: 0.3,fixed:true});

}

//正常退款中
function markNormalRefunding()
{
	oid = menuOid;

	//tb_show('正常退款[订单:'+oid+']',"return_product.html?oid="+oid+"&cmd=markNormalRefunding&TB_iframe=true&height=500&width=850",false);
	
	$.artDialog.open("return_product.html?oid="+oid+"&cmd=markNormalRefunding", {title: '正常退款[订单:'+oid+']',width:'850px',height:'530px', lock: true,opacity: 0.3,fixed:true});	
}

function markNormalRefundingSubmit(note,oid,return_product_flag,return_product_reason,ps_id)
{
	document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markNormalRefunding.action";
	document.listForm.note.value = note;
	document.listForm.oid.value = oid;
	document.listForm.ps_id.value = ps_id;
	document.listForm.return_product_flag.value = return_product_flag;
	document.listForm.return_product_reason.value = return_product_reason;
	document.listForm.submit();	
}

function modReturnProductListAction(rp_id,oid,ps_id)
{
	document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/ModReturnProductList.action";
	document.listForm.rp_id.value = rp_id;
	document.listForm.oid.value = oid;
	document.listForm.ps_id.value = ps_id;
	document.listForm.submit();	
}

function viewRefunding(rp_id)
{

	//tb_show('退货清单',"return_product_view.html?rp_id="+rp_id+"&TB_iframe=true&height=500&width=650",false);
	
	$.artDialog.open("return_product_view.html?rp_id="+rp_id, {title: '退货清单',width:'1100px',height:'530px', lock: true,opacity: 0.3,fixed:true});	
}

function modReturnProductList(rp_id,oid)
{
	
	$.artDialog.open("return_product_mod.html?rp_id="+rp_id+"&oid="+oid, {title: '修改退货清单',width:'1100px',height:'530px', lock: true,opacity: 0.3,fixed:true});
	
	//tb_show('修改退货清单',"return_product_mod.html?rp_id="+rp_id+"&oid="+oid+"&TB_iframe=true&height=500&width=650",false);	
}



//争议退款
function markDisputeRefunding()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>争议退款[订单:"+oid+"]</div><br /><textarea name='normalRefundingNoteWinText' id='normalRefundingNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.normalRefundingNoteWinText  == "")
						  {
							   alert("请填写争议退款原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markDisputeRefunding.action";
						document.listForm.note.value = f.normalRefundingNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}


//已部分退款
function markPartRefunded()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>部分退款[订单:"+oid+"]</div><br /><textarea name='normalRefundingNoteWinText' id='normalRefundingNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.normalRefundingNoteWinText  == "")
						  {
							   alert("请填写部分退款原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markPartRefunded.action";
						document.listForm.note.value = f.normalRefundingNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}


//已全部退款
function markAllRefunded()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>全部退款[订单:"+oid+"]</div><br /><textarea name='normalRefundingNoteWinText' id='normalRefundingNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.normalRefundingNoteWinText  == "")
						  {
							   alert("请填写全部退款原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markAllRefunded.action";
						document.listForm.note.value = f.normalRefundingNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}


//正常质保
function markNormalWarrantying()
{
	oid = menuOid;
	
	$.artDialog.open("../warranty_return/return_product_season.html?oid="+oid, {title: '创建服务,订单['+oid+']',width:'1100px',height:'530px', lock: true,opacity: 0.3});
	//$.artDialog.open("return_product.html?oid="+oid+"&cmd=markNormalWarrantying", {title: '正常质保[订单:'+oid+']',width:'800px',height:'500px', lock: true,opacity: 0.3});
}

function markNormalWarrantyingSubmit(note,oid,return_product_flag,return_product_reason,ps_id)
{
	document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markNormalWarrantying.action";
	document.listForm.note.value = note;
	document.listForm.oid.value = oid;
	document.listForm.ps_id.value = ps_id;
	document.listForm.return_product_flag.value = return_product_flag;
	document.listForm.return_product_reason.value = return_product_reason;
	document.listForm.submit();	
}








//争议质保
function markDisputeWarrantying()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>争议质保[订单:"+oid+"]</div><br /><textarea name='normalRefundingNoteWinText' id='normalRefundingNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.normalRefundingNoteWinText  == "")
						  {
							   alert("请填写争议质保原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markDisputeWarrantying.action";
						document.listForm.note.value = f.normalRefundingNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}



function markOtherDispute()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>其他争议[订单:"+oid+"]</div><br /><textarea name='normalRefundingNoteWinText' id='normalRefundingNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.normalRefundingNoteWinText  == "")
						  {
							   alert("请填写其他争议原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markOtherDispute.action";
						document.listForm.note.value = f.normalRefundingNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}



function cancelRefund()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>撤销退款[订单:"+oid+"]</div><br /><textarea name='normalRefundingNoteWinText' id='normalRefundingNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.normalRefundingNoteWinText  == "")
						  {
							   alert("请填写撤销退款原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/cancelRefund.action";
						document.listForm.note.value = f.normalRefundingNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}


function cancelWarranty()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>撤销质保[订单:"+oid+"]</div><br /><textarea name='normalRefundingNoteWinText' id='normalRefundingNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.normalRefundingNoteWinText  == "")
						  {
							   alert("请填写撤销质保原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/cancelWarranty.action";
						document.listForm.note.value = f.normalRefundingNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}



function cancelDispute()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>撤销争议[订单:"+oid+"]</div><br /><textarea name='normalRefundingNoteWinText' id='normalRefundingNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.normalRefundingNoteWinText  == "")
						  {
							   alert("请填写撤销争议原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/cancelDispute.action";
						document.listForm.note.value = f.normalRefundingNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}

function markFailureRefund()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>退款失败[订单:"+oid+"]</div><br /><textarea name='markFailureRefundNoteWinText' id='markFailureRefundNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.markFailureRefundNoteWinText  == "")
						  {
							   alert("请填写退款失败原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markFailureRefund.action";
						document.listForm.note.value = f.markFailureRefundNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}


function markFreeWarranty()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>无偿质保[订单:"+oid+"]</div><br /><textarea name='markFreeWarrantyNoteWinText' id='markFreeWarrantyNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.markFreeWarrantyNoteWinText  == "")
						  {
							   alert("请填写无偿质保原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markFreeWarranty.action";
						document.listForm.note.value = f.markFreeWarrantyNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}




function markFailureWarranty()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>质保失败[订单:"+oid+"]</div><br /><textarea name='markFailureWarrantyNoteWinText' id='markFailureWarrantyNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.markFailureWarrantyNoteWinText  == "")
						  {
							   alert("请填写质保失败原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markFailureWarranty.action";
						document.listForm.note.value = f.markFailureWarrantyNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}




function markFailureDispute()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>争议失败[订单:"+oid+"]</div><br /><textarea name='markFailureDisputeNoteWinText' id='markFailureDisputeNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.markFailureDisputeNoteWinText  == "")
						  {
							   alert("请填写争议失败原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markFailureDispute.action";
						document.listForm.note.value = f.markFailureDisputeNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}





function markBadRefund()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>退款差评[订单:"+oid+"]</div><br /><textarea name='markBadRefundNoteWinText' id='markBadRefundNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.markBadRefundNoteWinText  == "")
						  {
							   alert("请填写退款差评原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markBadRefund.action";
						document.listForm.note.value = f.markBadRefundNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}





function markBadWarranty()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>质保差评[订单:"+oid+"]</div><br /><textarea name='markBadWarrantyNoteWinText' id='markBadWarrantyNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.markBadWarrantyNoteWinText  == "")
						  {
							   alert("请填写质保差评原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markBadWarranty.action";
						document.listForm.note.value = f.markBadWarrantyNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}





function markBadDispute()
{
	oid = menuOid;
	
	$.prompt(
	
	"<div id='title'>争议差评[订单:"+oid+"]</div><br /><textarea name='markBadDisputeNoteWinText' id='markBadDisputeNoteWinText'  style='width:320px;height:120px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.markBadDisputeNoteWinText  == "")
						  {
							   alert("请填写争议差评原因");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/markBadDispute.action";
						document.listForm.note.value = f.markBadDisputeNoteWinText;
						document.listForm.oid.value = oid;
						document.listForm.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}

function unDoAdditionMoneyOrder(oid)
{
	document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/unDoAdditionMoneyOrderAction.action";
	document.listForm.oid.value = oid;
	document.listForm.submit();		
}

function cancelReturn(rp_id)
{
	if (confirm("确定顾客不需要退货吗？"))
	{
		document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/CancelReturn.action";
		document.listForm.rp_id.value = rp_id;
		document.listForm.submit();	
	}
}

function searchQuestion()
{		
	if ($("#search_question_key").val()==""&&$("#search_question_p_name").val()=="")
	{
		document.filter_search_form.cmd.value = "filter";		
		document.filter_search_form.pcid.value = $("#filter_pcid").val();
		document.filter_search_form.submit();
	}
	else
	{
		document.filter_search_form.cmd.value = "search";		
		document.filter_search_form.key.value = $("#search_question_key").val();		
		document.filter_search_form.pname.value = $("#search_question_p_name").val();	
		document.filter_search_form.pcid.value = $("#filter_pcid").val();
		document.filter_search_form.submit();
	}
}

//差评
//差评zhangrui
function badFeedBack(oid,status,title)
{
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/bad_feedback.html?oid="+ oid+"&status="+status; 

 	$.artDialog.open(uri , {title: '登记'+title,width:'600px',height:'330px', lock: true,opacity: 0.3,fixed: true});
  
 //tb_show('登记'+title,'bad_feedback.html?oid='+oid+'&status='+status+'&TB_iframe=true&height=300&width=600',false);
}

function getDetailOrderWeight(oid)
{
	var old_ems_id;
	
	$.prompt(
	
	"<div id='title'>详细重量["+oid+"]</div><br /><textarea name='proDetailOrderWeight' id='proDetailOrderWeight'  style='width:320px;height:70px;border:0px;' readonly=true>数据加载中......</textarea> ",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  return true;
					}
				}
		  ,
   		  loaded:
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/GetDetailOrderWeightJSON.action",
							{oid:oid},
							function callback(data)
							{   
								var msg = "";
								
								msg += "原始重量："+data.org_weight+" Kg,"+data.org_weight_lb+" Lb\n";
								msg += "运费重量："+data.ship_weight+" Kg,"+data.ship_weight_lb+" Lb\n";
								msg += "打印重量："+data.print_weight+" Kg,"+data.print_weight_lb+" Lb\n";
								msg += "实际重量："+data.actual_weight+" Kg,"+data.actual_weight_lb+" Lb";
								
								$("#proDetailOrderWeight").val(msg);
							}
							
					);

				}
		  , 
		  callback: 
		  
				function (v,m,f)
				{

				}
		  ,
		  overlayspeed:"fast",
		  buttons: {  关闭: "n" }
	});
}

function getDetailOrderCost(oid)
{
	var old_ems_id;
	
	$.prompt(
	
	"<div id='title'>详细成本["+oid+"]</div><br /><textarea name='proDetailOrderCost' id='proDetailOrderCost'  style='width:320px;height:70px;border:0px;' readonly=true>数据加载中......</textarea> ",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  return true;
					}
				}
		  ,
   		  loaded:
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/GetDetailOrderCostJSON.action",
							{oid:oid},
							function callback(data)
							{   
								var msg = "";
								
								msg += "商品修正成本："+data.product_cost+" RMB，商品原始成本："+data.product_cost_cul+" RMB\n";
								msg += "运费修正成本："+data.shipping_cost+" RMB，运费原始成本："+data.shipping_cost_cul+" RMB";
																
								$("#proDetailOrderCost").val(msg);
							}
							
					);

				}
		  , 
		  callback: 
		  
				function (v,m,f)
				{

				}
		  ,
		  overlayspeed:"fast",
		  buttons: {  关闭: "n" }
	});
}

function clickSearchKey()
{
	var keywords = $("#search_key").val();
	keywords = keywords.replace(/\"/g,'');
	//keywords = keywords.replace("\"",""); 
	$("#search_key").val(keywords);
}

	//销售区域切换国家
function getAreaCountryByCaId(ca_id,div)
{

		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getAreaCountrysJSON.action",
				{ca_id:ca_id},
				function callback(data)
				{ 
					$("#"+div+"ccid_hidden").clearAll();
					$("#"+div+"ccid_hidden").addOption("全部国家","0");
					
					if (data!="")
					{
						$("#"+div+"ccid_hidden").css("display","");
						$("#"+div+"pro_id").css("display","none");
						$("#"+div+"pro_id").clearAll();
						
						$.each(data,function(i){
							$("#"+div+"ccid_hidden").addOption(data[i].c_country,data[i].ccid);
						});
					}
					else
					{
						$("#"+div+"ccid_hidden").css("display","none");
						$("#"+div+"pro_id").css("display","none");
					}
				}
		);
}

//国家地区切换
function getStorageProvinceByCcid(ccid,div)
{

		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#"+div+"pro_id").clearAll();
					$("#"+div+"pro_id").addOption("全部区域","0");
					
					if (data!="")
					{
						$("#"+div+"pro_id").css("display","");
						$.each(data,function(i){
							$("#"+div+"pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					else
					{
						$("#"+div+"pro_id").css("display","none");
					}
					
					if (ccid>0)
					{
						$("#"+div+"pro_id").addOption("Others","-1");
					}
				}
		);
}

function createTrade()
{
  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/add_trade.html";
  
  
  $.artDialog.open(uri , {title: '新增交易',width:'1100px',height:'530px', lock: true,opacity: 0.3});
}
</script>

<script language="JavaScript" type="text/JavaScript">
<!-- 
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
<style type="text/css">
<!--
.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:29px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">


<br>
<form name="filter_search_form" method="post" target="_blank"  action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/customerservice_qa/hasanswer_question.html">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="pname"/>
<input type="hidden" name="key" >
<input type="hidden" name="pcid"/>
</form>
 

<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	
	
	
	
	<div class="demo" >

<div id="tabs">
<ul>
		<li><a href="#usual_tools">常用工具</a></li>
		<li><a href="#advan_search">高级搜索</a></li>
		<li><a href="#statistics_order">监控统计</a></li>
		<li><a href="#tracking_send_order">发货追踪</a></li>
		<tst:authentication bindAction="com.cwc.app.api.OrderMgr.modPOrderHandleStatus">
		<li><a href="#special_search">特殊操作</a></li>	
		 </tst:authentication>		
		 
		 <li><a href="#search_question">产品问题搜索</a></li>
		 
</ul>

		<div id="usual_tools">
		
          <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="30%" style="padding-top:3px;">
			   <div id="easy_search_father">
			   <div id="easy_search"><a href="javascript:searchOrder()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"></a></div>
			   </div>
				<table width="485" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				    <td width="418">
								  <div  class="search_shadow_bg">
							  <input onclick="clickSearchKey()" type="text" name="search_key" id="search_key" value="<%=val%>" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333">
							  </div>	</td>
				    <td width="67">
					 <a href="javascript:void(0)" id="search_help" style="padding:0px;color: #000000;text-decoration: none;"><img src="../imgs/help.gif" border="0" align="absmiddle" border="0"></a>
					</td>
				  </tr>
				</table>
				<script>eso_button_even();</script>		
			</td>
              <td width="45%" ></td>
              <td width="12%" align="right" valign="middle">
				<!--  
			 	 <a href="javascript:tb_show('手工创建订单','add.html?TB_iframe=true&height=500&width=850',false);"><img src="../imgs/create_order.jpg" border="0"></a>	
			  	-->	  
			  	<a href="javascript:createTrade();"><img src="../imgs/create_order.jpg" border="0"></a>
			 </td>
            </tr>
          </table>
		   
		</div>

 
 		<div id="advan_search">

<form name="filterForm" action="" method="post">
<table width="100%" height="61" border="0" cellpadding="1" cellspacing="0">
  
    <tr>
      <td width="95%" height="30" >
	  
	  <%
TDate tDate = new TDate();
tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

String input_st_date,input_en_date;
if ( st.equals("") )
{	
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}
else
{	
	input_st_date = st;
}


if ( en.equals("") )
{	
	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
}
else
{	
	input_en_date = en;
}
%>
        开始
 		<input name="st" type="text" id="st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
              
        &nbsp; 
        <select name="business" style="border:1px #CCCCCC solid" >
          <option value="">全部帐号</option>
          <%
String businessl[] = systemConfig.getStringConfigValue("business").split(",");
for (int i=0; i<businessl.length; i++)
{
%>
          <option value="<%=businessl[i]%>" <%=business.equals(businessl[i])?"selected":""%>> <%=businessl[i]%> </option>
          <%
}
%>
        </select>
       <input type="hidden" name="product_type_int" value="0"><!-- 该参数不再使用 -->
        &nbsp;
        <select name="filter_ps_id" id="filter_ps_id" style="border:1px #CCCCCC solid" >
          <option value="0">所有仓库</option>
          <%
DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();
String qx;

for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
	 
	 if (treeRows[i].get("level",0)>1)
	 {
	 	continue;
	 }
%>
          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==filter_ps_id?"selected":""%>> <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%> <%=qx%> <%=treeRows[i].getString("title")%> </option>
          <%
}
%>
        </select>
         &nbsp;
        <select name="handle_status" id="handle_status" style="border:1px #CCCCCC solid" >
            <option value="<%=HandStatusleKey.NORMAL%>" <%=handle_status==HandStatusleKey.NORMAL?"selected":""%>>正常</option>
            <option value="<%=HandStatusleKey.DOUBT%>" <%=handle_status==HandStatusleKey.DOUBT?"selected":""%>>疑问 订单</option>
            <option value="<%=HandStatusleKey.DOUBT_ADDRESS%>" <%=handle_status==HandStatusleKey.DOUBT_ADDRESS?"selected":""%>>疑问 地址</option>
            <option value="<%=HandStatusleKey.DOUBT_PAY%>" <%=handle_status==HandStatusleKey.DOUBT_PAY?"selected":""%>>疑问 付款</option>
            <option value="<%=HandStatusleKey.ARCHIVE%>" <%=handle_status==HandStatusleKey.ARCHIVE?"selected":""%>>归档 </option>
            <option value="<%=HandStatusleKey.CANCEL%>" <%=handle_status==HandStatusleKey.CANCEL?"selected":""%>>取消</option>
			<option value="<%=HandStatusleKey.HANG_UP%>" <%=handle_status==HandStatusleKey.HANG_UP?"selected":""%>>挂起</option>
			<option value="<%=HandStatusleKey.PRINT_DOUBT%>" <%=handle_status==HandStatusleKey.PRINT_DOUBT?"selected":""%>>打印疑问</option>
			<option value="<%=HandStatusleKey.ADDITION_MONEY%>" <%=handle_status==HandStatusleKey.ADDITION_MONEY?"selected":""%>>补钱</option>
            <option value="-1"  <%=handle_status==-1?"selected":""%>>全部</option>
          </select>
        &nbsp;
        <select name="handle" id="handle" style="border:1px #CCCCCC solid" >
          <option value="0" <%=handle==0?"selected":""%>>订单流程</option>
          <option value="<%=HandleKey.WAIT4_RECORD%>" <%=handle==HandleKey.WAIT4_RECORD?"selected":""%>>待抄单</option>
          <option value="<%=HandleKey.WAIT4_PRINT%>" <%=handle==HandleKey.WAIT4_OUTBOUND?"selected":""%>>待出库 </option>
          <option value="<%=HandleKey.PRINTED%>" <%=handle==HandleKey.OUTBOUNDING?"selected":""%>>出库中 </option>
          <option value="<%=HandleKey.WAITING_DELIVERY%>" <%=handle==HandleKey.ALLOUTBOUND?"selected":""%>>全部出库</option>
		  <option value="<%=HandleKey.DELIVERIED%>" <%=handle==HandleKey.DELIVERIED?"selected":""%>>已发货</option>		 
          <option value="<%=HandleKey.VERIFY_COST%>" <%=handle==HandleKey.VERIFY_COST?"selected":""%>>成本审核</option>
        </select>
            
        
		
		   </td>
      <td width="5%" rowspan="2" align="center" valign="middle" >
	    <div id="print-button" onClick="getWait4PrintOrders()" class="print-button"><span style="font-size:16px;font-weight:bold;color:#666666"><%=detailStorage.getString("title")%></span><br>
	        <span style="font-size:12px;font-weight:normal;color:#999999">需要打印订单</span><span id="Wait4PrintOrdersCount" style="font-family: Arial;font-size: 11px;font-weight:normal;color:#999999">(?)</span></div>	
	  </td>
    </tr>
    <tr>
      <td height="30"  > 
        <input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
		&nbsp;
        <select name="order_note_adid" id="order_note_adid" style="border:1px #CCCCCC solid" >
          <option value="0">选择跟进人</option>
                <%
DBRow admins[] = adminMgr.getAllAdmin(null);
for (int i=0; i<admins.length; i++)
{
%>
                <option value="<%=admins[i].getString("adid")%>" <%=admins[i].get("adid",0l)==order_note_adid?"selected":""%>> 
                <%=admins[i].getString("account")%>                </option>
                <%
}
%>
        </select>
		&nbsp;
<select name="order_note_trace_type" id="order_note_trace_type" style="border:1px #CCCCCC solid" >
          <option value="-1">跟进类型  </option>
<%
ArrayList tracingOrderKeyWrapAl = tracingOrderKey.getTracingOrderKeysType();
for (int ii=0; ii<tracingOrderKeyWrapAl.size(); ii++)
{
String typeKey = tracingOrderKeyWrapAl.get(ii).toString();
out.println("<option value='-2' style='background:#000000;color:#ffffff'>"+typeKey+"</option>");
ArrayList tracingOrderKeyAl = tracingOrderKey.getTracingOrderKeys( tracingOrderKey.getTracingOrderType(typeKey) );
for (int i=0; i<tracingOrderKeyAl.size(); i++)
{
	if ( tracingOrderKeyAl.get(i).toString().equals(String.valueOf(tracingOrderKey.PAYPAL))||tracingOrderKeyAl.get(i).toString().equals(String.valueOf(tracingOrderKey.ORDER_COST_VERIFY)) )
	{
		continue;
	}
%>
	<option value="<%=tracingOrderKeyAl.get(i)%>"><%=tracingOrderKey.getTracingOrderKeyById( tracingOrderKeyAl.get(i).toString() )%></option>
<%
}
}
%>

 <tst:authentication bindAction="com.cwc.app.api.OrderMgr.verifyOrderCost">	  
 <option value="<%=tracingOrderKey.ORDER_COST_VERIFY%>"><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.ORDER_COST_VERIFY) )%></option>
 </tst:authentication>	  
        </select>		
		&nbsp;
        <select name="bad_feedback_flag" id="bad_feedback_flag" style="border:1px #CCCCCC solid" >
          <option value="0">评价</option>
 			<option value="<%=orderRateKey.BAD%>"><%=orderRateKey.getOrderRateStatusById(orderRateKey.BAD)%></option>
			<option value="<%=orderRateKey.NORMAL%>"><%=orderRateKey.getOrderRateStatusById(orderRateKey.NORMAL)%></option>
			<option value="<%=orderRateKey.EBAY_COMPLAIN%>"><%=orderRateKey.getOrderRateStatusById(orderRateKey.EBAY_COMPLAIN)%></option>
			<option value="<%=orderRateKey.PAYPAL_COMPLAIN%>"><%=orderRateKey.getOrderRateStatusById(orderRateKey.PAYPAL_COMPLAIN)%></option>
        </select>
	 	&nbsp;
        <select name="after_service_status" id="after_service_status" style="border:1px #CCCCCC solid" >
          <option value="0" <%=after_service_status==0?"selected":""%>>全部</option>

		  <option value="<%=AfterServiceKey.NORMAL_REFUNDING%>" <%=after_service_status==AfterServiceKey.NORMAL_REFUNDING?"selected":""%>>正常退款中</option>
		  <option value="<%=AfterServiceKey.DISPUTE_REFUNDING%>" <%=after_service_status==AfterServiceKey.DISPUTE_REFUNDING?"selected":""%>>争议退款中</option>
		  <option value="<%=AfterServiceKey.PART_REFUNDED%>" <%=after_service_status==AfterServiceKey.PART_REFUNDED?"selected":""%>>已部分退款</option>
		  <option value="<%=AfterServiceKey.ALL_REFUNDED%>" <%=after_service_status==AfterServiceKey.ALL_REFUNDED?"selected":""%>>已全部退款</option>
		  <option value="<%=AfterServiceKey.FAILURE_REFUND%>" <%=after_service_status==AfterServiceKey.FAILURE_REFUND?"selected":""%>>退款失败</option>
		  <option value="<%=AfterServiceKey.BAD_REFUND%>" <%=after_service_status==AfterServiceKey.BAD_REFUND?"selected":""%>>退款差评</option>
		  
		  <option value="<%=AfterServiceKey.NORMAL_WARRANTYING%>" <%=after_service_status==AfterServiceKey.NORMAL_WARRANTYING?"selected":""%>>正常质保中</option>
		  <option value="<%=AfterServiceKey.DISPUTE_WARRANTYING%>" <%=after_service_status==AfterServiceKey.DISPUTE_WARRANTYING?"selected":""%>>争议质保中</option>
		  <option value="<%=AfterServiceKey.WARRANTIED%>" <%=after_service_status==AfterServiceKey.WARRANTIED?"selected":""%>>有偿质保</option>
		  <option value="<%=AfterServiceKey.FREE_WARRANTIED%>" <%=after_service_status==AfterServiceKey.FREE_WARRANTIED?"selected":""%>>无偿质保</option>
		   <option value="<%=AfterServiceKey.FREE_SHIPPING_FEE_WARRANTY%>" <%=after_service_status==AfterServiceKey.FREE_SHIPPING_FEE_WARRANTY?"selected":""%>>免运费质保</option>
		  <option value="<%=AfterServiceKey.FAILURE_WARRANTY%>" <%=after_service_status==AfterServiceKey.FAILURE_WARRANTY?"selected":""%>>质保失败</option>
		  <option value="<%=AfterServiceKey.BAD_WARRANTY%>" <%=after_service_status==AfterServiceKey.BAD_WARRANTY?"selected":""%>>质保差评</option>

		  <option value="<%=AfterServiceKey.OTHER_DISPUTING%>" <%=after_service_status==AfterServiceKey.OTHER_DISPUTING?"selected":""%>>其他争议</option>	
		  <option value="<%=AfterServiceKey.FAILURE_DISPUTING%>" <%=after_service_status==AfterServiceKey.FAILURE_DISPUTING?"selected":""%>>争议失败</option>		
		  <option value="<%=AfterServiceKey.BAD_DISPUTING%>" <%=after_service_status==AfterServiceKey.BAD_DISPUTING?"selected":""%>>争议差评</option>	

        </select>
        
		&nbsp;订单来源 
<select name="order_source" id="order_source" style="border:1px #CCCCCC solid" >
        <option value="">全部来源</option>
		<option value="<%=OrderMgr.ORDER_SOURCE_EBAY%>"><%=OrderMgr.ORDER_SOURCE_EBAY%></option>
		<option value="<%=OrderMgr.ORDER_SOURCE_WEBSITE%>"><%=OrderMgr.ORDER_SOURCE_WEBSITE%></option>
		<option value="<%=OrderMgr.ORDER_SOURCE_DIRECTPAY%>"><%=OrderMgr.ORDER_SOURCE_DIRECTPAY%></option>
		<option value="<%=OrderMgr.ORDER_SOURCE_MANUAL%>"><%=OrderMgr.ORDER_SOURCE_MANUAL%></option>
		<option value="<%=OrderMgr.ORDER_SOURCE_QUOTE%>"><%=OrderMgr.ORDER_SOURCE_QUOTE%></option>
		<option value="<%=OrderMgr.ORDER_SOURCE_WANRRANTY%>"><%=OrderMgr.ORDER_SOURCE_WANRRANTY%></option>
		<option value="<%=OrderMgr.ORDER_SOURCE_PAY%>"><%=OrderMgr.ORDER_SOURCE_PAY%></option>
		<option value="<%=OrderMgr.ORDER_SPLIT%>"><%=OrderMgr.ORDER_SPLIT%></option>
		<option value="<%=OrderMgrZr.ORDER_SOURCE_INVOICE%>"><%=OrderMgrZr.ORDER_SOURCE_INVOICE%></option>
        </select>
        &nbsp;&nbsp;
        <a href="javascript:filter(1)"><img src="../imgs/adv_search.gif" width="95" height="20" border="0" align="absmiddle"></a> 	</td>
      </tr>
</table>
  </form>
		</div>
		
		<div id="statistics_order">
			<form action="" name="statistics_order">
			<table cellpadding="2" border="0" cellspacing="5">
				<tr>
						<td><input name="st" type="text" id="statistics_order_st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />&nbsp;至	<input name="en" type="text" id="statistics_order_en"  value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" /></td>
						<td><input type="radio" name="cost_type" value="1"/>抄单用时<input name="cost_type" type="radio" value="2"/>完成用时 &nbsp;&nbsp;大于<input size="3"  style="width:30px;" id="cost" name="cost" value=""/>&nbsp;小时</td>
						<td>未完成大于<input style="width:30px;" type="text" name="unfinished" id="unfinished" size="2"/>天</td>
						<td><a href="javascript:statisticsOrder()"><img src="../imgs/adv_search.gif" width="95" height="20" border="0" align="absmiddle"></a></td>
					</tr>
				</table>
				<table >
											 
					<tr>
						<td>商品:<input type="text" id="p_name" name="p_name" onclick="cleanProductLine('statistics_order_')" style="width:180px;" value="<%=p_name%>"/></td>
 			 		<td>
 			 			<select id="sale_area" onchange="getAreaCountryByCaId(this.value,'')">
						  		 <option value="0">全部区域</option>
						  		<%
						  			DBRow[] areas = productMgr.getAllCountryAreas(null);
						  			for(int i = 0;i<areas.length;i++)
						  			{
								%>
									<option value="<%=areas[i].get("ca_id",0l)%>"><%=areas[i].getString("name")%></option>
								<%
						  			}
						  		%>
						  	</select>
									 
						      <select name="ccid_hidden" style="display: none" id="ccid_hidden" onChange="getStorageProvinceByCcid(this.value,'');">
							  <option value="0">全部国家</option>
						      </select>
							     
						      <select name="pro_id" id="pro_id" style="display: none">
						      	<option value="0">全部区域</option>
						      </select>
 			 		</td>
				</tr>
				</table>
				<!--  产品线 产品分类 -->
				<table cellpadding="2" border="0" cellspacing="5">
				<tr>
					<td width="423" align="left" bgcolor="">
	 			 					<ul id="statistics_order_productLinemenu" class="mcdropdown_menu">
						             <li rel="0">所有产品线</li>
						             <%
										  DBRow p1[] = productLineMgrTJH.getAllProductLine();
										  for (int i=0; i<p1.length; i++){
												out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
												out.println("</li>");
										  }
									  %>
						           </ul>
						      <input type="text" name="statistics_order_productLine" id="statistics_order_productLine" value="" />
						      <input type="hidden" name="statistics_order_filter_productLine" id="statistics_order_filter_productLine" value="0"/>
					</td>
							<td id="statistics_order_categorymenu_td" width="423" align="left" valign="bottom"   style=""> 
						           <ul id="statistics_order_categorymenu" class="mcdropdown_menu">
									  <li rel="0">所有分类</li>
									  <%
										  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
										  for (int i=0; i<c1.length; i++)
										  {
												out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
									
												  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
												  if (c2.length>0)
												  {
												  		out.println("<ul>");	
												  }
												  for (int ii=0; ii<c2.length; ii++)
												  {
														out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
														
															DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
															  if (c3.length>0)
															  {
																	out.println("<ul>");	
															  }
																for (int iii=0; iii<c3.length; iii++)
																{
																		out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																		out.println("</li>");
																}
															  if (c3.length>0)
															  {
																	out.println("</ul>");	
															  }
															  
														out.println("</li>");				
												  }
												  if (c2.length>0)
												  {
												  		out.println("</ul>");	
												  }
												  
												out.println("</li>");
										  }
										  %>
									</ul>
							  <input type="text" name="statistics_order_category" id="statistics_order_category" value="" />
							  <input type="hidden" name="filter_pcid" id="statistics_order_filter_pcid" value="0"/>
 			 		</td>
				</tr>
			</table>
			</form>
		</div>
		<div id="tracking_send_order">
			<form action="" name="tracking_send_order">
			<table cellpadding="2" border="0" cellspacing="5">
					<tr>
						<td><input name="st" type="text" id="tracking_send_order_st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />&nbsp;至	<input name="en" type="text" id="tracking_send_order_en"  value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" /></td>
						<td>
							<strong>|</strong>&nbsp;超过
							<select name="tracking_date_type" id="tracking_date_type">
								<option value="1">进单时间</option>
								<option value="2">发货时间</option>
							</select>
							<input type="text" name="last_day" id="last_day" style="width: 20px;" onchange="internalTracingChange(this.value)"/>天未妥投&nbsp;
							<strong>|</strong>&nbsp;
							派送状态:
							<select name="internal_tracking_status" id="internal_tracking_status" style="border:1px #CCCCCC solid" onchange="cleanInternalTracking()">
									          <option value="-1">运输状态</option>
												<%
						   							ArrayList waybillInternalTrack = waybillInternalTrackingKey.getWaybillInternalTrack();
						   							
						   							for(int i = 0;i<waybillInternalTrack.size();i++)
						   							{
						   						%>
						   						<option value="<%=waybillInternalTrack.get(i)%>"><%=waybillInternalTrackingKey.getWaybillInternalTrack(waybillInternalTrack.get(i).toString())%></option>
						   						<%
						   							}
						   						%>
							</select>
						</td>
						<td><a href="javascript:trackingOrder()"><img src="../imgs/adv_search.gif" width="95" height="20" border="0" align="absmiddle"></a></td>
					</tr>
				</table>
				<table >
											 
					<tr>
						<td>商品:<input type="text" id="tracking_send_order_p_name" name="tracking_send_order_p_name" onclick="cleanProductLine('tracking_send_order_')" style="width:180px;" value="<%=p_name%>"/></td>
 			 		<td>
 			 			<select id="tracking_send_order_sale_area" onchange="getAreaCountryByCaId(this.value,'tracking_send_order_')">
						  		 <option value="0">全部区域</option>
						  		<%
						  			for(int i = 0;i<areas.length;i++)
						  			{
								%>
									<option value="<%=areas[i].get("ca_id",0l)%>"><%=areas[i].getString("name")%></option>
								<%
						  			}
						  		%>
						  	</select>
									 
						      <select name="tracking_send_order_ccid_hidden" style="display: none" id="tracking_send_order_ccid_hidden" onChange="getStorageProvinceByCcid(this.value,'tracking_send_order_');">
							  <option value="0">全部国家</option>
						      </select>
							     
						      <select name="tracking_send_order_pro_id" id="tracking_send_order_pro_id" style="display: none">
						      	<option value="0">全部区域</option>
						      </select>
 			 		</td>
				</tr>
				</table>
				<!--  产品线 产品分类 -->
				<table cellpadding="2" border="0" cellspacing="5">
				<tr>
					<td width="423" align="left" bgcolor="">
	 			 					<ul id="tracking_send_order_productLinemenu" class="mcdropdown_menu">
						             <li rel="0">所有产品线</li>
						             <%
										  
										  for (int i=0; i<p1.length; i++){
												out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
												out.println("</li>");
										  }
									  %>
						           </ul>
						      <input type="text" name="tracking_send_order_productLine" id="tracking_send_order_productLine" value="" />
						      <input type="hidden" name="tracking_send_order_filter_productLine" id="tracking_send_order_filter_productLine" value="0"/>
					</td>
							<td id="tracking_send_order_categorymenu_td" width="423" align="left" valign="bottom"   style=""> 
						           <ul id="tracking_send_order_categorymenu" class="mcdropdown_menu">
									  <li rel="0">所有分类</li>
									  <%
										  
										  for (int i=0; i<c1.length; i++)
										  {
												out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
									
												  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
												  if (c2.length>0)
												  {
												  		out.println("<ul>");	
												  }
												  for (int ii=0; ii<c2.length; ii++)
												  {
														out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
														
															DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
															  if (c3.length>0)
															  {
																	out.println("<ul>");	
															  }
																for (int iii=0; iii<c3.length; iii++)
																{
																		out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																		out.println("</li>");
																}
															  if (c3.length>0)
															  {
																	out.println("</ul>");	
															  }
															  
														out.println("</li>");				
												  }
												  if (c2.length>0)
												  {
												  		out.println("</ul>");	
												  }
												  
												out.println("</li>");
										  }
										  %>
									</ul>
							  <input type="text" name="tracking_send_order_category" id="tracking_send_order_category" value="" />
							  <input type="hidden" name="filter_pcid" id="tracking_send_order_filter_pcid" value="0"/>
 			 		</td>
				</tr>
			</table>
			</form>
		</div>
<tst:authentication bindAction="com.cwc.app.api.OrderMgr.modPOrderHandleStatus">
<div id="special_search">
<select name="handle_status_batch" id="handle_status_batch">
                <!--
				<option value="0-handlestatus" selected>正常</option>
                <option value="1-handlestatus">疑问</option>
                <option value="2-handlestatus">归档</option>-->
				
				<!-- <option value="1-handle">待抄单</option> -->
				<option value="2-handle">待打印</option>
				<option value="3-handle">已打印</option>
				<option value="4-handle">已发货</option> 
            </select> 
			&nbsp;&nbsp;
			<input name="Submit222" type="button" class="long-long-button-ok" onClick="batchModifyHandleStatus()" value="修改订单状态">
</div>
	 </tst:authentication>	
	
	
	
	
	<div id="search_question" >
		<table width="99%" border="0" align="center" cellpadding="5" cellspacing="5">
		  <tr>
		    <td width="423" rowspan="2" align="left" style="padding-left:10px;">
			<div style="padding-bottom:5px;color:#666666;font-size:12px;">
			可以选择一个商品分类：
			</div>
			
			<ul id="categorymenu" class="mcdropdown_menu">
			  <li rel="0">所有分类</li>
			  <%
			  DBRow c11[] = catalogMgr.getProductCatalogByParentId(0,null);
			  for (int i=0; i<c11.length; i++)
			  {
					out.println("<li rel='"+c11[i].get("id",0l)+"'> "+c11[i].getString("title"));
		
					  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c11[i].get("id",0l),null);
					  if (c2.length>0)
					  {
					  		out.println("<ul>");	
					  }
					  for (int ii=0; ii<c2.length; ii++)
					  {
							out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
							
								DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
								  if (c3.length>0)
								  {
										out.println("<ul>");	
								  }
									for (int iii=0; iii<c3.length; iii++)
									{
											out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
											out.println("</li>");
									}
								  if (c3.length>0)
								  {
										out.println("</ul>");	
								  }
								  
							out.println("</li>");				
					  }
					  if (c2.length>0)
					  {
					  		out.println("</ul>");	
					  }
					  
					out.println("</li>");
			  }
			  %>
		</ul>
			  <input type="text" name="category" id="category" value="" />
			  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />
			  
		
		<script>
		$("#category").mcDropdown("#categorymenu",{
				allowParentSelect:true,
				  select: 
				  
						function (id,name)
						{
							$("#filter_pcid").val(id);
						}
		
		});
		$("#category").mcDropdown("#categorymenu").setValue(0);
		</script>	</td>
		    <td height="25px;" align="left" valign="top"  >&nbsp;&nbsp;商品名称&nbsp;&nbsp;&nbsp;<input type="text" id="search_question_p_name" name="search_question_p_name"  style="color:#FF3300;width:255px;font-family:Arial, Helvetica, sans-serif;font-weight:normal" value="" />&nbsp;&nbsp;&nbsp;
		      <input name="Submit4" type="button" class="button_long_search" value="搜索" onClick="searchQuestion()"></td>
		  </tr>
		  <tr>
		    <td valign="bottom">&nbsp;&nbsp;
		      &nbsp;&nbsp;关键字&nbsp;&nbsp;&nbsp;<input type="text" name="search_question_key" id="search_question_key" value="" style="color:#FF3300;width:255px;font-family:Arial, Helvetica, sans-serif;font-weight:normal" >&nbsp;&nbsp;&nbsp;
 			<input name="Submit44" type="button" class="long-button-add" value="新建问题" onClick="window.open('../customerservice_qa/add_question.html')">
	   
			</td>
		  </tr>
		</table>
	</div>	
	
	
	
	
	
	</div>	</div>	
	

	
	
	
	</td>
  </tr>
</table>

<div id="system_menu">
</div>
			
			
<script>
	$("#tabs").tabs({	
		cookie: { expires: 30000 } 
	});
</script><br>

<div id="growlMsg" class="jGrowl bottom-right"></div>


<DIV id="prolist"></DIV>

<form name="refundForm" method="post" >
<input type="hidden" name="note"/>
<input type="hidden" name="refundType"/>
<input type="hidden" name="amount"/>
<input type="hidden" name="tranID"/>
<input type="hidden" name="userName"/>
<input type="hidden" name="oid"/>
<input type="hidden" name="userAccount">
<input type="hidden" name="totalAmount"/>
<input type="hidden" name="currencyCode">
</form>
</body>

</html>
<script>
var t=null;
var interval = <%=systemConfig.getStringConfigValue("order_flush_second")%>;
var leaveSec = interval;

function leaveTime()
{
	leaveSec = leaveSec-1000;
	document.getElementById("leavetime").innerHTML = leaveSec/1000+"秒";
	setTimeout("leaveTime()",1000);
}

//订单监控提示
var monitorTraceOrdersTimer = null;
function monitorTraceOrders()
{
	$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getMonitorTraceCountJSonAction.action",
		{nothing:0},
		function callback(data)
		{
			//急需处理订单
			var doubtordercount = data.doubtordercount*1;
			var doubtaddressordercount = data.doubtaddressordercount*1;
			var doubtpayordercount = data.doubtpayordercount*1;
			var lackingordercount = data.lackingordercount*1;
			
			var verifycostordercount = data.verifycostordercount*1;
			var printdoubtordercount = data.printdoubtordercount*1;
			
			var disputerefundcount = data.disputerefundcount*1;
			var disputewarrantycount = data.disputewarrantycount*1;
			var otherdisputecount = data.otherdisputecount*1;

			var msg = "";

			if ( (doubtordercount+doubtaddressordercount+doubtpayordercount+lackingordercount+verifycostordercount+printdoubtordercount+disputerefundcount+disputewarrantycount+otherdisputecount)>0 )
			{
				if (doubtordercount>0)
				{
					msg += "疑问订单: <a href='ct_order_auto_reflush.html?cmd=trace&trace_type=1' target='_blank'>"+doubtordercount+"</a><br>";
				}
				if (doubtaddressordercount>0)
				{
					msg += "疑问地址: <a href='ct_order_auto_reflush.html?cmd=trace&trace_type=2' target='_blank'>"+doubtaddressordercount+"</a><br>";
				}
				if (doubtpayordercount>0)
				{
					msg += "疑问付款: <a href='ct_order_auto_reflush.html?cmd=trace&trace_type=3' target='_blank'>"+doubtpayordercount+"</a><br>";
				}
				if (printdoubtordercount>0)
				{
					msg += "打印疑问: <a href='ct_order_auto_reflush.html?cmd=trace&trace_type=5' target='_blank'>"+printdoubtordercount+"</a><br>";
				}
				//////////
				if (disputerefundcount+disputewarrantycount+otherdisputecount>0)
				{
					msg += "..................................................<br>";
				}
				if (disputerefundcount>0)
				{
					msg += "争议退款: <a href='ct_order_auto_reflush.html?cmd=trace&trace_type=6' target='_blank'>"+disputerefundcount+"</a><br>";
				}
				if (disputewarrantycount>0)
				{
					msg += "争议质保: <a href='ct_order_auto_reflush.html?cmd=trace&trace_type=7' target='_blank'>"+disputewarrantycount+"</a><br>";
				}
				if (otherdisputecount>0)
				{
					msg += "其他争议: <a href='ct_order_auto_reflush.html?cmd=trace&trace_type=8' target='_blank'>"+otherdisputecount+"</a><br>";
				}
				///////////
				msg += "..................................................<br>";
				
				<tst:authentication bindAction="com.cwc.app.api.OrderMgr.verifyOrderCost">
				if (verifycostordercount>0)
				{
					msg += "成本审核: <a href='ct_order_auto_reflush.html?cmd=trace&trace_type=100' target='_blank'>"+verifycostordercount+"</a><br>";
				}
				</tst:authentication>
				
				if (lackingordercount>0)
				{
					msg += "缺货: <a href='ct_order_auto_reflush.html?cmd=trace&trace_type=4' target='_blank'>"+lackingordercount+"</a><br>";
				}
				
				jGrowlPrompt("急需跟进订单",msg);
			}
		}
	);
	
	clearTimeout(monitorTraceOrdersTimer);
	monitorTraceOrdersTimer = setTimeout("monitorTraceOrders()",1000*60*30);
		
}

monitorTraceOrders();

function jGrowlPrompt(title,msg)
{
	$('#growlMsg').jGrowl("<br><br>"+msg, { 
		header: title+'<hr>',
		theme:  'manilla',
		speed:  'slow',
		sticky: true, 
		beforeOpen: function(e,m,o) {
			//alert("beforeOpen:");
		},
		open: function(e,m,o) {
			//alert("open:");
		},
		beforeClose: function(e,m,o) {
			//alert("beforeClose:");
		},
		close: function(e,m,o) {
			//alert("close:");
		}
	});
}





//jquery重写加载函数

function ajaxLoadCtOrderPage(para)
{
	//alert(para);

	$.ajax({
		url: 'ct_order.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request){
			$.blockUI({ message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		
		error: function(){
			$.blockUI({ message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
		},
		
		success: function(html){
			$.unblockUI();
			$("#prolist").html(html);
			//onLoadInitZebraTable();//表格斑马线
			ajaxLoadWait4PrintOrdersCount();//得到待打印订单数
			$.scrollTo(0,500);//页面向上滚动
			//差评按钮提示
			$('a#rate_bad').poshytip({
				className: 'tip-yellowsimple',
				showOn: 'hover',
				alignTo: 'target',
				alignX: 'right',
				alignY: 'center',
				showTimeout:0,
				offsetX: 0,
				offsetY: 5,
		
				content: function(updateCallback) {
					var oid = $(this).attr('rel');
					
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/GetOrderBadFeedBackJSON.action",
							{oid:oid},
							function callback(data)
							{
								var badFeedBackStr = "";
								
								$.each(data, function(key, val) {
									badFeedBackStr += (key+1) +"、"+ data[key].name+"<br>";
								});

								updateCallback(badFeedBackStr);
							}
					);

					return '稍等一下……';
				}
			});
		}
	});

	clearTimeout(t);
	t = setTimeout("ajaxLoadCtOrderPage('"+para+"')",interval);

}


function ajaxLoadWait4PrintOrdersCount()
{
	var st = document.filterForm.st.value;
	var en = document.filterForm.en.value;
	var ps_id = <%=adminLoggerBean.getPs_id()%>;

	var para = "ps_id="+ps_id+"&st="+st+"&en="+en;

	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getWait4PrintOrdersCountAjax.action',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request){
		},
		
		error: function(){
			$("#Wait4PrintOrdersCount").html("(?)");
		},
		
		success: function(html){
			if (html=="error")
			{
				$("#Wait4PrintOrdersCount").html("(?)");
			}
			else
			{
				$("#Wait4PrintOrdersCount").html("("+html+")");
			}
		}
	});

}



<%
//这里处理默认打开页面和backurl返回路径

if ( cmd.equals("search") )
{
%>
	ajaxLoadCtOrderPage("cmd=search&val=<%=val%>&p=<%=p%>&search_field=<%=search_field%>");
<%
}
else if ( cmd.equals("trace") )
{
%>
	ajaxLoadCtOrderPage("cmd=trace&trace_type=<%=trace_type%>");
<%
}
else 
{
	
%>
	filter(0);//默认打开页面调用
<%
}
%>


function showRecordSection(hidden_section,hidden_content)
{
	//document.getElementById(hidden_section).style.display="";
	document.getElementById(hidden_content).style.display="";
	//document.getElementById(hidden_content).innerHTML = "hi啊";
	//document.getElementById(hidden_content).innerHtml = document.getElementById("recordTable").innerHtml;
	//alert(document.getElementById(hidden_content).innerHtml);
	//document.getElementById(hidden_content).innerHtml = "hi啊";
}

function hiddRecordSection(id)
{
	document.getElementById(id).display="none";
}


function showRecordContent(i,oid)
{
	if ( document.getElementById("hidden_content"+i).style.display=="" )
	{
		hiddRecordContent(i);
	}
	else
	{
		document.getElementById("hidden_content"+i).style.display="";
		//document.getElementById("hidden_content"+i).innerHTML = recordTable(i,oid);
	}
}

function hiddRecordContent(i)
{
	//document.getElementById("hidden_content"+i).innerHTML = "";
	document.getElementById("hidden_content"+i).style.display="none";
}


$().ready(function() {

	function log(event, data, formatted) {
		
	}

	<%-- Frank ****************** --%>
	addAutoComplete($("#search_question_p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"p_name");
		
	addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/order/getSearchOrdersJson.action",
			"merge_field","oid");  //suggest的提示，使用merge_field字段，而点选以后上到搜索框里面的是订单号orderid

				

			 		  
			
		


	addAutoComplete($("#search_question_key"),
		

			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/getKeyIndex.action",
		

				

			 		  
			
			"keyword");
	<%-- ***********************  --%>
		
});

	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id+"&divId="+divId;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#"+divId+"categorymenu_td").html(html);
				}
			});
	}

$("#statistics_order_category").mcDropdown("#statistics_order_categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#statistics_order_filter_pcid").val(id);
				}
});

$("#statistics_order_productLine").mcDropdown("#statistics_order_productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#statistics_order_filter_productLine").val(id);
					if(id==<%=product_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"statistics_order_",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"statistics_order_",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
				}
});

$("#tracking_send_order_category").mcDropdown("#tracking_send_order_categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#tracking_send_order_filter_pcid").val(id);
				}
});

$("#tracking_send_order_productLine").mcDropdown("#tracking_send_order_productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#tracking_send_order_filter_productLine").val(id);
					if(id==<%=product_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"tracking_send_order_",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"tracking_send_order_",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
				}
});
<%
if (catalog_id>0)
{
%>

$("#statistics_order_category").mcDropdown("#statistics_order_categorymenu").setValue(<%=catalog_id%>);
$("#tracking_send_order_category").mcDropdown("#tracking_send_order_categorymenu").setValue(<%=catalog_id%>);
<%
}
else
{
%>
$("#statistics_order_category").mcDropdown("#statistics_order_categorymenu").setValue(0);
$("#tracking_send_order_category").mcDropdown("#tracking_send_order_categorymenu").setValue(0);
<%
}
%> 

<%
if (product_line_id!=0)
{
%>
$("#statistics_order_productLine").mcDropdown("#statistics_order_productLinemenu").setValue(<%=product_line_id%>);
$("#tracking_send_order_productLine").mcDropdown("#tracking_send_order_productLinemenu").setValue(<%=product_line_id%>);
<%
}
else
{
%>
$("#statistics_order_productLine").mcDropdown("#statistics_order_productLinemenu").setValue(0);
$("#tracking_send_order_productLine").mcDropdown("#tracking_send_order_productLinemenu").setValue(0);
<%
}
%>

function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"p_name").val("");
}
</script>