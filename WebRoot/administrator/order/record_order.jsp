<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.api.OrderLock"%>
<%@ page import="com.cwc.app.api.qll.VertualTerminalMgrQLL"%>
<%
long oid = StringUtil.getLong(request,"oid");
//获得订单锁
DBRow orderLock = OrderLock.lockOrder(session,oid,OrderLock.RECORD);
if (orderLock!=null)
{
	out.println("<script>");
	out.println("alert('"+orderLock.getString("operator")+"正在"+OrderLock.getMsg(orderLock.getString("operate_type"))+"，请与其联系后再操作')");
	out.println("</script>");
}

int refresh = StringUtil.getInt(request,"refresh");

DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);
orderMgr.initCartFromOrder(request,oid);

String client_id = detailOrder.getString("client_id");
String name = detailOrder.getString("address_name");

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script language="javascript" src="../../common.js"></script>
 
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>		
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">



<script type='text/javascript' src='../js/jquery.form.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css?v=1" />


		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<style>
.ebayTitle
{
	font-weight:bold; 
	font-size:12px;
	color:#0066FF;
}
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>




<script>
 
function showTran()
{
	if ( document.getElementById("tranForm").style.display == "" )
	{
		document.getElementById("showTran").innerHTML = "显示交易信息";
		document.getElementById("tranForm").style.display="none";
	}
	else
	{
		document.getElementById("showTran").innerHTML = "隐藏";
		document.getElementById("tranForm").style.display="";
	}
	
}
 
function customProduct(pid,p_name,cmd)
{
	tb_show('定制套装',"../product/custom_product.html?cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&TB_iframe=true&height=300&width=400",false);
}

$().ready(function() {

	function log(event, data, formatted) {

		
	}
	
	/**
	function formatItem(row) {
		return(row[0]);
	}
	
	function formatResult(row) {
		return row[0].replace(/(<.+?>)/gi, '');
	}
	**/

	$("#p_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		scroll:false,
		updownSelect: true,
		selectFirst: false,
		
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name+" --- ["+data[i].p_code+"] --- ["+data[i].unit_name+"]",
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   

		  	 return rows;   
			 },   

		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	}).result(function(event, item) {
  		//$("#quantity").focus();
		
	});

	
	//加载购物车
	loadCart();
	//绑定购物车表单AJAX
	ajaxCartForm();

	
	$("#p_name").keydown(function(event){
		if (event.keyCode==13)
		{
			$("#quantity").select();
		}
	});

	$("#quantity").keydown(function(event){
		if (event.keyCode==13)
		{
			put2Cart();
		}
	});
	
	getStorageProvinceByCcid(<%=detailOrder.get("ccid",0l)%>);//初始化地区	
});

//=================== 购物车 =========================

function loadCart()
{
	//alert(para);
	
	$.ajax({
		url: 'mini_cart.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		
		beforeSend:function(request){
			$("#action_info").text("加载购物车......");
		},
		
		error: function(){
			$("#action_info").text("加载购物车失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
			$("#action_info").text("");
			$("#p_name").focus();
		}
	});
	
	preCalcuOrder(<%=oid%>,$("#ccid_hidden").val(),$("#pro_id").val());
}


function put2Cart()
{
	var p_name = $("#p_name").val();
	var quantity = $("#quantity").val();

	
	 if (p_name=="")
	{
		alert("请填写商品名称");
		$("#p_name").focus();
	}
	else if (quantity=="")
	{
		alert("请填写数量");
	}
	else if ( !isNum(quantity) )
	{
		alert("请正确填写数量");
		$("#quantity").select();
	}
	else if (quantity<=0)
	{
		alert("数量必须大于0");
		$("#quantity").select();
	}
	else
	{
		var para = "p_name="+p_name+"&quantity="+quantity;
	
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/put2Cart.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			
			error: function(){
				loadCart();
				$("#action_info").text("商品添加失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException")
				{
					alert("商品不存在，不能添加到订单！");
					$("#p_name").select();
				}
				else if(msg=="ProductDataErrorException")
				{
					alert("商品基础数据错误，不能添加到订单！请联系调度");
					$("#p_name").select();
				}
				else if (msg=="ok")
				{
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("1");
					loadCart();
				}
				else
				{
					loadCart();
					alert(msg);
				}
			}
		});	
	}
}

function delProductFromCart(pid,p_name,product_type)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/removeProductFromCart.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"pid="+pid+"&product_type="+product_type,
					
			error: function(){
				loadCart();
				$("#action_info").text("商品删除失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart();
			}
		});	
	}
}


function cleanCart()
{
	if (confirm("确定清空购物车？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/cleanCart.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			
			error: function(){
				loadCart();
				$("#action_info").text("清空购物车失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart();
			}
		});	
	}
}

//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   //beforeSubmit:  // pre-submit callback    
			//	function(){
			//		loadCart();
			//	}	
		  // ,  
		   success:       // post-submit callback  
				function(){
					loadCart();
					cartProQuanHasBeChange = false;
				}
		       
	};
	

   $('#cart_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}

var cartProQuanHasBeChange = false;
function onChangeQuantity(obj)
{
	if ( !isNum(obj.value) )
	{
		alert("请正确填写数字");
		return(false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}


function isNum(keyW)
{
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
//=================== 购物车 =========================


var cartIsEmpty = <%=cart.isEmpty(session)%>;

function checkCart()
{
	if (cartIsEmpty)
	{	
		alert("请先添加商品到订单");
		return(false);
	}
	else
	{
		return(true);
	}
}

//除了正常抄单，购物车不能为空外，其他状态抄单都可以为空
function trueOrder()
{

	if (checkCart())
	{
		if (cartProQuanHasBeChange)
		{
			alert("请先保存购物车修改商品的数量");
		}
		else if (checkSelectCountry())
		{
			alert("请选择递送国家");
		}
		else if ($("#pro_id").val()==0)
		{
			alert("请选择地区");
		}
		else if ( checkInvoice($("#inv_uv").val(),$("#inv_tv").val(),$("#inv_dog").val(),$("#inv_rfe").val()) )
		{
		}
		else if ( typeof($("input[name='ps_id']:checked").val()) == 'undefined')
		{
			alert("请选择发货仓库");
		}
		else if ( $("#delivery_note").val()!=""&&/.*[\u4e00-\u9fa5]+.*$/.test($("#delivery_note").val()) )
		{
			alert("配送备注不能包含中文");
		}	
		else
		{			
			var note = ""
					if (note!="")
					{
						note = "："+note;
					}
					
					parent.trueOrder(<%=oid%>,$("#invoice_product_type").val(),$("#tel").val(),$('#ccid_hidden').val(),note,$("input[name='ps_id']:checked").val(),$("#inv_dog").val(),$("#inv_uv").val(),$("#inv_tv").val(),$("#inv_rfe").val(),$("#inv_di_id").val(),$("#delivery_note").val(),<%=detailOrder.get("parent_oid",0l)%>,$("input[name='recom_express']:checked").val(),$("#pro_id").val());
		}
	}
}



function doubtTypeOrder()
{
				
		if (cartProQuanHasBeChange)
		{
			alert("请先保存购物车修改商品的数量");
		}
		else if (checkSelectCountry())
		{
			alert("请选择递送国家");
		}
		else if ($("#pro_id").val()==0)
		{
			alert("请选择地区");
		}
		else if ( typeof($("input[name='ps_id']:checked").val()) == 'undefined')
		{
			alert("请选择发货仓库");
		}	
		else
		{

				$.prompt(
				
				"<div id='title'>疑问订单</div><br /><span style='font-size:13px;font-weight:bold;'><input type='radio' name='handleStatusWinText' id='handleStatusWinText' value='<%=HandStatusleKey.DOUBT%>' />商品问题 &nbsp; <input type='radio' id='handleStatusWinText'  name='handleStatusWinText' value='<%=HandStatusleKey.DOUBT_ADDRESS%>' />地址问题 &nbsp;  <input type='radio' id='handleStatusWinText'  name='handleStatusWinText' value='<%=HandStatusleKey.DOUBT_PAY%>' />付款问题</span><br><textarea name='OrderNoteWinText' id='OrderNoteWinText'  style='width:320px;height:60px;'></textarea>",
				
				{
					  submit: 
							function (v,m,f)
							{
								if (v=="y")
								{
									if ( typeof(f.handleStatusWinText)=="undefined")
									{
										alert("请选择疑问类型");
									    return false;
									}
									else if(f.OrderNoteWinText  == "")
									{
									   alert("请填写问题备注");
									    return false;
									}
									return true;
								}
			
							}
					  ,
			
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
									var outOrderNoteWinText = f.OrderNoteWinText;
																
									parent.doubtTypeOrder(<%=oid%>,$("#invoice_product_type").val(),$("#tel").val(),$('#ccid_hidden').val(),outOrderNoteWinText,$("input[name='ps_id']:checked").val(),$("#inv_dog").val(),$("#inv_uv").val(),$("#inv_tv").val(),$("#inv_rfe").val(),$("#inv_di_id").val(),$("#delivery_note").val(),f.handleStatusWinText,$("#pro_id").val());
								}
							}
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
				
			
		}
		

}


function checkInvoice(inv_uv,inv_tv,inv_dog,inv_rfe)
{
	if (inv_uv==""&&inv_tv==""&&inv_dog==""&&inv_rfe=="")
	{
		if ($("#invoice_product_type").val()==0)
		{
			alert("请选择一个发票模板");
			return(true);
		}
	}
	else
	{
		if(inv_uv==""&&inv_tv=="")
		{
			alert("Unit Value和Total Value不能同时为空！");
			return(true);
		}
		else if ( inv_uv!=""&&!isNum(inv_uv) )
		{
			alert("请正确填写Unit Value！");
			return(true);
		}
		else if(inv_dog=="")
		{
			alert("Description of Goods不能为空！");
			return(true);
		}
		else if(inv_rfe=="")
		{
			alert("Reason for Export不能为空！");
			return(true);
		}
	}
	

	
	return(false);
}


function checkSelectCountry()
{
	if ( $('#ccid_hidden').val()==0 ) 
	{
		return(true);
	}
	else
	{
		return(false);		
	}
}

function closeTBWin()
{
	tb_remove();
}

function changeCountry(obj)
{
	
}

function changeInvoice(obj)
{
	$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailInvoiceTemplateJSON.action",
		{invoice_id:obj.value},
		function callback(data)
		{   
			$("#inv_dog").val(data.dog);
			$("#inv_rfe").val(data.rfe);
			$("#inv_uv").val(data.uv);
			$("#inv_tv").val(data.tv);
			
			$("#inv_di_id").val(data.di_id);
			
		}
	);
}

function pasteEmail()
{
	if ( document.getElementById("emailContainer").style.display == "" )
	{
		document.getElementById("paste_email").innerHTML = "粘贴邮件内容";
		document.getElementById("emailContainer").style.display = "none";
	}
	else
	{
		document.getElementById("paste_email").innerHTML = "隐藏";
		document.getElementById("emailContainer").style.display = "";
	}
}


function getInvoiceByPsid(ps_id)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/getInvoiceByPsIdJSON.action",
				{ps_id:ps_id},
				function callback(data)
				{  
					if (data!="")
					{
						$("#invoice_product_type").clearAll();
						$("#invoice_product_type").addOption("请选择...",0);
						$.each(data,function(i){
							$("#invoice_product_type").addOption(data[i].t_name,data[i].invoice_id);
						});
						$("#invoice_product_type").setSelectedValue(<%=detailOrder.get("product_type",0l)%>);
					}
				}
		);
}

function cleanInvoice()
{
			$("#inv_dog").val("");
			$("#inv_rfe").val("");
			$("#inv_uv").val("");
			$("#inv_tv").val("");
}

function validateZipCode(ps_id,zipCode)
{
	//只对美国仓库检查邮编
	if (ps_id==100043)
	{

		var para = "zipCode="+zipCode;

		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/validateUSAZipCodeAjax.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			
			error: function(){
				alert("检测邮编出错");
			},
			
			success: function(msg){
				if (msg=="false")
				{
					modOrderZip(zipCode);
				}
			}
		});	
		
	}
}

function modOrderZip(zipCode)
{
	$.prompt(
	
	"<div id='title'>修改递送信息[订单:"+<%=oid%>+"]</div><br/><img src='../imgs/product/warring.gif' width='16' height='15' align='absmiddle'><span style='color:#black;font-size:12px;font-style:normal;font-weight:normal'>&nbsp;邮编["+zipCode+"]不正确，请先修正！<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;正确格式：五位纯数字(例如：60459-1234修正为60459)</span><br/><br/>Zip<br><input name='address_zipWinText' type='text' id='address_zipWinText'  style='width:300px;'>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{

						  if(f.address_zipWinText == "")
						  {
							   alert("请填写邮编");
							   return false;
						  }

						  var   reg =/^\d{5}$/;
						  if(!reg.test(f.address_zipWinText))
						  {
						  		alert("邮编格式不正确")
						  		return false;
						  }
						  return true;
					}
				}
		  ,
   		  loaded:
				function ()
				{ 
					$("#address_zipWinText").val(zipCode);

				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.modZipForm.address_zip.value = f.address_zipWinText;
						document.modZipForm.refresh.value = "1";
						document.modZipForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/modOrderZip.action";
						document.modZipForm.submit();	
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function isRefresh()
{
	<%
		if(refresh==0)
		{
	%>
		parent.closeTBWin();
	<%		
		}
		else
		{
	%>
		parent.closeTBWinRefrech();
	<%
		}
	%>
}

//强制填写好地址，才允许抄单
function validateDeliveryInfo()
{
	var closeWin = false;
	
	if ( "<%=detailOrder.getString("address_name")%>" == "" )
	{
		closeWin = true;
	}
	else if ( "<%=StringUtil.replaceString(StringUtil.replaceString(StringUtil.replaceString(detailOrder.getString("address_street"), "\n", ""), "\r", ""),"\"","")%>" == "" )
	{
		closeWin = true;
	}
	else if ( "<%=detailOrder.getString("address_city")%>" == "" )
	{
		closeWin = true;
	}
	else if ( "<%=detailOrder.getString("address_state").trim()%>" == "" )
	{
		closeWin = true;
	}
	else if ( "<%=detailOrder.getString("address_zip")%>" == "" )
	{
		closeWin = true;
	}
	
	if (closeWin)
	{
		alert("请先填写完整收货地址信息再抄单");
		parent.closeTBWin();
	}
}
validateDeliveryInfo();


//非正常状态订单抄单需要提示
function abnormalPaymentStatus()
{
	if ( confirm("该订单 <%=detailOrder.getString("payment_status")%> 状态，是否确定抄单？") )
	{
	}
	else
	{
		parent.closeTBWin();
	}
}

<%
if ( detailOrder.getString("payment_status").toLowerCase().equals("completed")==false&&detailOrder.getString("payment_status").equals("")==false )
{
%>
abnormalPaymentStatus();
<%
}
%>

function hasBeCreditVerify()
{

		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/hasBeCreditVerify.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"oid=<%=oid%>",
			
			error: function(){
				
			},
			
			success: function(msg){
			
				if (msg=="false")
				{
					alert("请先验证信用卡支付后再抄单！");
					parent.closeTBWin();
				}
			}
		});	
}


function preCalcuOrder(oid,select_ccid,select_pro_id)
{
	var para = "oid="+oid+"&select_ccid="+select_ccid+"&select_pro_id="+select_pro_id;

	$.ajax({
		url: 'pre_calcu_order.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request){
			$("#pre_calcu_order_info").text("正在计算......");
		},
		
		error: function(){
			$("#pre_calcu_order_info").text("计算失败！");
		},
		
		success: function(html){
			$("#pre_calcu_order_page").html(html);
			$("#pre_calcu_order_info").text("");
		}
	});
}

//国家地区切换
function getStorageProvinceByCcid(ccid)
{

		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#pro_id").addOption("Others","-1");
					}


<%
long jsProID = 0;

//如果订单已经设置了省份，则返回
if (detailOrder.get("pro_id",0l)>0)
{
	jsProID = detailOrder.get("pro_id",0l);
}
//如果是初次抄单，则系统根据省份代码自动配对
DBRow detailProvince = productMgr.getDetailProvinceByNationIDPCode(detailOrder.get("ccid",0l),detailOrder.getString("address_state"));
if (detailProvince!=null)
{
	jsProID = detailProvince.get("pro_id",0l);
}

%>


					var pro_id = "<%=jsProID%>";
					if (pro_id!=""&&pro_id!=0)
					{
						$("#pro_id").setSelectedValue(pro_id);
					}			
				}
		);
}

//验证网站订单信息是否已经同步
<%
 if(StringUtil.getString(request,"orderSource").equals("WEBSITE"))
 {
	 DBRow websitetranInfo = tranInfoMgrQLL.getTranInfo(detailOrder.getString("txn_id"));
	 if(websitetranInfo==null&&DateUtil.getDate2LongTime(DateUtil.NowStr())<(DateUtil.getDate2LongTime(detailOrder.getString("post_date"))+(10*60*1000)))
	 {
%>
	 alert("网站订单详细信息没有同步完成，请稍候抄单");
 	parent.closeTBWin();
<%
	 }
 }
%>


<%
//暂时屏蔽，不使用
//String buyer_ip = tranInfoMgrQLL.getBuyerIp(detailOrder.getString("txn_id"),detailOrder.getString("order_source"),detailOrder.getString("note"));

//if ( buyer_ip.equals("")==false )
//{
%>
	//hasBeCreditVerify();	
<%
//}
%>		
</script>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="modZipForm" action="" method="post">
	<input type="hidden" id="address_zip" name="address_zip" value=""/>
	<input type="hidden" id="order_id" name="order_id" value="<%=oid%>"/>
	<input type="hidden" id="refresh" name="refresh" value=""/>
	<input type="hidden" id="backurl" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/record_order.html?oid=<%=oid%>"/>
</form>
<table width='95%' border='0' cellpadding='0' cellspacing='0'  style="padding-left:14px;"> 
  <input type='hidden' name='oid' id='oid' value='1'>
  <tr>
    <td >
		<table width='100%' border='0' cellspacing='0' cellpadding='3'>
			<tr>
			  <td align='center' valign='middle'>&nbsp;</td>
			  <td width="1298">&nbsp;</td>
			</tr>
			<tr>
			  <td colspan="2" align='left' valign='middle' style="padding-left:15px;padding-right:15px;">
				<fieldset style="border:3px #cccccc solid;padding:15px;-webkit-border-radius:7px;-moz-border-radius:7px;">
				<legend style="font-size:15px;font-weight:bold;color:#000000;font-family: Verdana">订单商品</legend>
					<table width="98%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="11%" height="35" align="center" valign="middle" bgcolor="#D3EB9A" style="padding-left:5px;">搜索商品</td>
						<td width="89%" valign="middle" bgcolor="#D3EB9A"><input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold"/>数量 
							<input name="quantity" type="text" id="quantity"  style="color:#FF3300;width:40px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold" value="1"/>
							&nbsp;<input name="Submit3" type="button" class="long-button" value="添加到订单" onClick="put2Cart()"> &nbsp;&nbsp;(多使用回车，效率更高哦)    
					     </td>
					  </tr>
					  <tr>
						<td colspan="2" align="left" valign="middle" style="padding-top:10px;color:#999999">
						<p>HID：<BR>
						  1. HID 美国仓库有货但是带尖括号的需要改到北京仓库发货。<BR>
						  2. 55W   灯泡北京仓库发货，不要抄到美国仓库。<BR>
						  PRJ:<BR>
						  1. 投影机V01, V02,V06 在美国仓库发货；V05在广州仓库发货。<BR>
						  2.   如果客户既买了投影机又买了幕布，需要合并订单在美国仓库发货的，不要合并订单，请直接抄单。<BR>
						  DVR:<BR>
						  1.DVR美国订单抄到美国仓库，其他国家DVR订单抄到北京仓库。
						</p>
						  备注：<BR>
						  1.抄到美国仓库的订单地址必须都是美国境内的。<BR>
						  2.   美国仓库缺货HID，DVR统一改到北京仓库，PRJ统一改到广州仓库。<BR clear="all">
						  </td>
						</tr>
	
					  	<tr>
							<td colspan="2" align="left" valign="middle" style="padding-top:10px;">
							  <img src="../imgs/paste_email.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="javascript:pasteEmail()"><span id="paste_email">粘贴邮件内容										                                   </span></a>
									<%
									if(StringUtil.getString(request,"orderSource").equals("WEBSITE"))
									{
									%>
									<img src="../imgs/paste_email.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="#" onClick="showTran()"  >  <span id="showTran">显示														                                    交易	信息</span></a>
									<%
									 }
									 %>
									 <%
									 if(StringUtil.getString(request,"orderSource").equals(com.cwc.app.api.OrderMgr.ORDER_SOURCE_WEBSITE))
									{
								 		String tranID = StringUtil.getString(request,"txn_id");
									  %>
	<div id="tranForm" style="display:none" ><br/>
				<table width="100%" border="0"  cellspacing="1" align="center" cellpadding="5"   bgcolor="#cccccc" >
								 <%
										String rs[] = tranInfoMgrQLL.getProductInfo(tranID);
										if(rs!=null && rs.length!=0)	 		
										{
										 %>
											<tr >
												 <td width="30%"  height="20" align="center"  ><span class="ebayTitle">name</span></td>
												 <td align="center" width="8%"  ><span class="ebayTitle">quantity</span></td>
												 <td align="center" width="30%"  ><span class="ebayTitle">attributes</span></td>
												 <td width="25%"   align="center"  ><span class="ebayTitle">model</span></td>
												 <td width="15%" align="center"><span class="ebayTitle">price</span></td>
											 </tr>
										 <%
											for(int i=0;i<rs.length;i+=5)
											{	 			 		
											 %>
												<tr >
													<td  align="center" height="25" bgcolor="#eeeeee"><%=rs[i]%></td>
													<td align="center" bgcolor="#eeeeee"><%=rs[i+1]%></td>
													<td align="lift" bgcolor="#eeeeee" ><%=rs[i+2]%></td>
													<td align="center" bgcolor="#eeeeee" ><%=rs[i+3]%></td>
													<td align="center" bgcolor="#eeeeee" ><%=rs[i+4]%></td>
												</tr>
											 <% 
											 } 			
										
									}									
								   %>	
					</table>
			<br/>	 
				<table width="100%"     border="0"    align="center" cellpadding="5" cellspacing="0" bgcolor="#cccccc" > 
				   
							<%
									DBRow rows = tranInfoMgrQLL.getTranInfo(tranID);
									if(rows!=null)
									{
								%>
						  <tr>
							   <td     bgcolor="#eeeeee"height="25"   colspan="4">
								<span class="ebayTitle">customer comment :</span><%=rows.getString("customer_comment")%> 
							   </td>
						   </tr>
						   <tr>
						   <td  bgcolor="#FFFFFF"  height="25"    colspan="4">
							<span class="ebayTitle">customer address :</span>  <%=rows.getString("cutomer_country")%>&nbsp;<%=rows.getString("cutomer_state")%>&nbsp;<%=rows.getString("cutomer_city")%>&nbsp;<%=rows.getString("cutomer_company")%>&nbsp;<%=rows.getString("cutomer_name")%>&nbsp;<%=rows.getString("cutomer_suburb")%>&nbsp;<%=rows.getString("cutomer_street_address")%>&nbsp;<%=rows.getString("cutomer_postcode")%>
						   </td>
						   </tr>
						   <tr>
							<td  bgcolor="#eeeeee" height="25"   colspan="4">
							<span class="ebayTitle">billing address :</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=rows.getString("billing_country")%>&nbsp;<%=rows.getString("billing_state")%>&nbsp;<%=rows.getString("billing_city")%>&nbsp;<%=rows.getString("billing_company")%>&nbsp;<%=rows.getString("billing_name")%>&nbsp;<%=rows.getString("billing_suburb")%>&nbsp;<%=rows.getString("billing_street_address")%>&nbsp;<%=rows.getString("billing_postcode")%></td>
						   </tr>
						   <tr>
						   <td bgcolor="#FFFFFF"  height="25"   colspan="4">
						   <span class="ebayTitle">delivery address:</span> &nbsp;&nbsp;&nbsp;&nbsp;
							 <%=rows.getString("delivery_country")%>&nbsp;<%=rows.getString("delivery_state")%>&nbsp;<%=rows.getString("delivery_city")%>&nbsp;<%=rows.getString("delivery_company")%>&nbsp;<%=rows.getString("delivery_name")%>&nbsp;<%=rows.getString("delivery_suburb")%>&nbsp;<%=rows.getString("delivery_street_address")%>&nbsp;<%=rows.getString("delivery_postcode")%>
						   </td>
						   </tr>
							<tr>
							<td width="50%" colspan="2" height="25"  align="left" bgcolor="#eeeeee"  > <span class="ebayTitle">tran id: </span>&nbsp; <%=rows.getString("tran_id")  %></td>
								 <td colspan="2" align="left" height="25" bgcolor="#eeeeee"    ><span class="ebayTitle">currency:</span>&nbsp;<%=rows.getString("currency")  %> </td>   
							</tr>
							<tr>
								<td colspan="2"   align="left"  height="25" bgcolor="#FFFFFF"    ><span class="ebayTitle">price:</span>&nbsp;<%=rows.getString("ot_subtotal")  %></td>
								 <td  colspan="2" align="left"  height="25" bgcolor="#FFFFFF"  ><span class="ebayTitle">shipping:</span>&nbsp;<%=rows.getString("ot_shipping")  %></td>
							</tr>
							<tr>
								 <td colspan="2" align="left"  height="25" bgcolor="#eeeeee"><span class="ebayTitle">total:</span>&nbsp;<%=rows.getString("ot_total")  %></td>
								 <td colspan="2" align="left"  height="25"bgcolor="#eeeeee"><span class="ebayTitle">date purchased:</span>&nbsp;<%=rows.getString("date_purchased")%></td>
							</tr>
						   <tr>	
								<td colspan="2" align="left"  height="25" bgcolor="#FFFFFF"><span class="ebayTitle">payment method:</span>&nbsp;<%=rows.getString("payment_method")%></td>
								<td colspan="2" align="left"  height="25"bgcolor="#FFFFFF"><span class="ebayTitle">payment type:</span>&nbsp;<%=rows.getString("payment_type")%></td>
						   </tr>
						   <tr>
								<td colspan="2" align="left"  height="25"bgcolor="#eeeeee"><span class="ebayTitle">invoice id:</span>&nbsp;<%=rows.getString("invoice_id")%></td>
								<td colspan="2" align="left"  height="25"bgcolor="#eeeeee"><span class="ebayTitle">ip:</span>&nbsp;<%=rows.getString("ip_address")%></td>
						   </tr>
						<tr>
					 <td colspan="2" align="left" height="25"bgcolor="#FFFFFF"><span class="ebayTitle">customer telephone:</span>&nbsp;&nbsp;<%=rows.getString("cutomer_telephone")	%></td>
						<td colspan="2" align="left"  height="25"bgcolor="#FFFFFF">&nbsp; </td>
					    </tr> 
					    <tr>
						 <td colspan="2" align="left" height="25"bgcolor="#eeeeee"><span class="ebayTitle">cc_number:</span>&nbsp;&nbsp;<%=rows.getString("cc_number")	%></td>
						<td colspan="2" align="left"  height="25"bgcolor="#eeeeee"><span class="ebayTitle">cc_type:</span>&nbsp;&nbsp;<%=rows.getString("cc_type")	%></td>
					    </tr>
							<% 
							} %>  
			   </table>      
	</div>
	<%} %>
	 
    		 
 <br/>
    <textarea  style="width:750px;height:200px;display:none;border:1px #999999 dashed;color:#666666;padding:5px;" id="emailContainer"></textarea>
	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/batchModifyProductsQuantityFromCart.action" method="post" name="cart_form" id="cart_form">
	<div id="mini_cart_page"></div>
</form>	</td>
    </tr>
	
  <tr>
    <td colspan="2" align="right" valign="middle" style="color:#FF0000" id="action_info"></td>
  </tr>
  <tr><td colspan="2" align="left" valign="middle" style="padding-top:10px;color:#999999">
  <% 
		DBRow rowOrderPending = vertualTerminalMgrQLL.getPaymentStatus(oid);
		if(rowOrderPending!=null&&rowOrderPending.getString("payment_status").equals("Pending"))
		{
			if(rowOrderPending.getString("pending_reason")!="" && !rowOrderPending.getString("pending_reason").equals(""))
			{
	 %>
		<p style="color:#CC0000;font-size:13px"  ><strong> <%=rowOrderPending.getString("pending_reason") %> pending reasion:</strong></p>   
	    <%   }    %>
	
		<% if(rowOrderPending.getString("fraud_management_pending_filters")!="" && !rowOrderPending.getString("fraud_management_pending_filters").equals(""))
			{
				String filters[]=rowOrderPending.getString("fraud_management_pending_filters").split("\n");
		%>
		<p style="font-size:13px"> 
			<%for(int n=0;n<filters.length;n++)
			 {
			 %>
			 	 <%=n+1 %>:<%=filters[n] %>
			 <% 	
			 }	
			%> </p>	
		<% 	
			}	
		%>			
	<%
		}
	 %>
  </td></tr>
</table>
            </fieldset></td>
        </tr>
      
	  


    <tr> 
                <td colspan="2" align="left" valign="top" style="padding-left:15px;padding-right:15px;padding-top:10px;padding-bottom:10px;">
				<fieldset style="border:3px #cccccc solid;padding:15px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:bold;color:#000000;font-family: Verdana">收货信息</legend>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
		<%
		if (detailOrder.getString("note").equals("")==false)
		{
		%>
        <tr>
          <td colspan="2" style="padding:5px;font-size:12px;color:#CC0000;border:1px #CCCCCC solid;background:#FFFFE1">
<img src="../imgs/client.gif" width="21" height="25" align="absmiddle"> &nbsp; <%=detailOrder.getString("note")%>		  </td>
        </tr>
		<%
		}
		%>
        <tr>
          <td width="36%" height="25" align="left" valign="middle" bgcolor="#eeeeee" style="padding-left:5px;">
		  <span class="ebayTitle">Ebay ID:</span> <%=detailOrder.getString("auction_buyer_id")%>		  </td>
          <td width="64%" align="left" valign="middle" bgcolor="#eeeeee">
		  <span class="ebayTitle">Payer Email:</span> <%=detailOrder.getString("client_id")%>		  </td>
        </tr>
        <tr>
          <td height="25" align="left" valign="middle" style="padding-bottom:3px;padding-left:5px;" >
		 <span class="ebayTitle">Item Number:</span> <%=detailOrder.getString("item_number")%>		  </td>
          <td align="left" valign="middle" style="padding-bottom:3px;">
		    <span class="ebayTitle">Item Name:</span> <%=detailOrder.getString("item_name")%></td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#eeeeee" style="padding-left:5px;"><span class="ebayTitle">Name:</span><%=StringUtil.replaceString(detailOrder.getString("address_name"),"\""," ")%></td>
          <td height="30" align="left" valign="middle" bgcolor="#eeeeee">		  <span class="ebayTitle">Address:</span>
		  <%=detailOrder.getString("address_street")%>,
		  <%=detailOrder.getString("address_city")%>,
		  <%=detailOrder.getString("address_state")%>
		  <%=detailOrder.getString("address_zip")%></td>
        </tr>
        <tr>
          <td align="left" valign="middle" style="padding-left:5px;">
		  <span class="ebayTitle">Tel:</span>
		  &nbsp;
		   <input name='tel' id='tel' type='text' size='30' value="<%=detailOrder.getString("tel")%>"/>
			  </td>
          <td height="30" align="left" valign="middle"><span class="ebayTitle">Country:</span>

<%
	DBRow countrycode[] = orderMgr.getAllCountryCode();
	String selectBg="#ffffff";
	String preLetter="";
	%>
      <select name="ccid_hidden" id="ccid_hidden" onChange="getStorageProvinceByCcid(this.value);preCalcuOrder(<%=oid%>,this.value,0);">
	  <option value="0">请选择...</option>
	  <%
	  for (int i=0; i<countrycode.length; i++)
	  {
	  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
		{
			if (selectBg.equals("#eeeeee"))
			{
				selectBg = "#ffffff";
			}
			else
			{
				selectBg = "#eeeeee";
			}
		}  	
		
		preLetter = countrycode[i].getString("c_country").substring(0,1);
	  %>
	    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>" <%=countrycode[i].get("ccid",0l)==detailOrder.get("ccid",0l)?"selected":""%>><%=countrycode[i].getString("c_country")%></option>
	  <%
	  }
	  %>
      </select>		
 
[<%=detailOrder.getString("address_country")%>] </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#eeeeee">&nbsp;</td>
          <td height="30" align="left" valign="middle" bgcolor="#eeeeee"><span class="ebayTitle">State:</span>
		  &nbsp;&nbsp;&nbsp;&nbsp;

      <select name="pro_id" id="pro_id" onChange="preCalcuOrder(<%=oid%>,$('#ccid_hidden').val(),this.value);">
      </select> [<%=detailOrder.getString("address_state")%>]		  </td>
        </tr>
      </table>
</fieldset>
				</td>
	          </tr>

	  
	  
	  
	  
	  
	  	          <tr> 
                <td colspan="2" align="left" valign="top" style="padding-left:15px;padding-right:15px;padding-top:10px;padding-bottom:10px;">
				<fieldset style="border:3px #cccccc solid;padding:15px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:bold;color:#000000;font-family: Verdana">发货仓库</legend>
<div id="pre_calcu_order_page"></div>
<div id="pre_calcu_order_info"></div>
</fieldset>
				</td>
	          </tr>
	  
        <tr>
          <td colspan="2" align='center' valign='middle' style="padding-left:15px;padding-right:15px;">
		  
<fieldset style="border:3px #cccccc solid;padding:15px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:bold;color:#000000;font-family: Verdana">订单发票</legend>
			  
<table width="98%" border="0" cellspacing="0" cellpadding="3">

            <tr>
              <td height="28" colspan="2" align="left" valign="middle" bgcolor="#eeeeee" style="padding-left:5px;">



参考模板 
                <select name="invoice_product_type" id="invoice_product_type" onChange="changeInvoice(this)">
		  </select>			  </td>
              <td align="right" valign="middle" bgcolor="#eeeeee" style="padding-right:5px;">&nbsp;		 	  		    </td>
            </tr>


            <tr>
              <td width="25%" align="left" valign="top"  style="padding-top:7px;">
			  <input type="hidden" name="inv_di_id" id="inv_di_id" value="<%=detailOrder.get("inv_di_id",0l)%>">
			  Unit Value (US $)<br>
			  <input type="text" name="inv_uv" id="inv_uv" value="<%=detailOrder.getString("inv_uv")%>">		 			  </td>
              <td width="38%" rowspan="2" align="left" valign="top"  style="padding-top:7px;">Reason for Export<br>
                <textarea name="inv_rfe" id="inv_rfe" style="width:230px;height:60px;font-size:12px;"><%=detailOrder.getString("inv_rfe")%></textarea></td>
              <td width="37%" rowspan="2" align="left" valign="top"  style="padding-top:7px;">
			  Description of Goods<br>
                <textarea name="inv_dog" id="inv_dog" style="width:230px;height:60px;font-size:12px;"><%=detailOrder.getString("inv_dog")%></textarea>			  </td>
            </tr>
            <tr>
              <td align="left" valign="top" >
			  Total Value (US $)<br>
			  <input type="text" name="inv_tv" id="inv_tv" value="<%=detailOrder.getString("inv_tv")%>">			  </td>
              </tr>
          </table>		
		  </fieldset>		  		  </td>
        </tr>
		

		
        <tr> 
          <td width='44' align='center' valign='middle'>&nbsp;</td>
          <td>&nbsp; </td>
        </tr>
		
		
		
        <tr> 
          <td height="25" align='center' valign='middle' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>配送备注<br>
          <textarea name="delivery_note" id="delivery_note" style="width:300px;height:50px;font-size:12px;"><%=detailOrder.getString("delivery_note")%></textarea></td>
        </tr>
        

        <tr>
          <td align='center' valign='middle'>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        
        <tr> 
          <td align='center' valign='middle'>&nbsp;</td>
          <td>
<table width="771" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="699" align="left">
                  <a href="javascript:trueOrder()"><img src="../imgs/normal_record_order.jpg" width="119" height="35" border="0"></a>
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="javascript:doubtTypeOrder()"><img src="../imgs/doubt_record_order.jpg" width="119" height="35" border="0"></a></td>
                <td width="72" align="center" valign="middle"><a href="javascript:isRefresh()"><img src="../imgs/close_record_order.jpg" width="51" height="35" border="0"></a></td>
              </tr>
            </table></td>
        </tr>
      </table>	  
  </td>
  </tr>
</table>

<br>
<br>
<br>
<br>
<br>
<br>
</body>
</html>
