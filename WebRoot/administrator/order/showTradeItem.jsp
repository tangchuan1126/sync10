<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.PaymentMethodKey" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单报价</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />


<!-- 引入Art -->

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<%
long oid = StringUtil.getLong(request,"oid");
DBRow[] tradeItems = null;
if(oid != 0l){
	tradeItems = orderMgrZr.getTradeItemByOid(oid);
}
 





%>

<style type="text/css">
	table.zebraTable thead th{font-size:14px;}
	table.zebraTable td {word-break:break-all;line-height:25px;height:25px;}
	td.fix_center{text-align:center;}
</style>
 
<script type="text/javascript">

</script>


</head>
<body onload = "onLoadInitZebraTable()">
 	
 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"   class="zebraTable" style="margin-left:3px;margin-top:5px;">
 		 
	 		<tr>
	 			<th width="45%" style="vertical-align: center;text-align: center;" class="right-title">Name</th>
	 		
	 			<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Shipping</th>
	 			<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Handling</th>
	 			<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Tax</th>
	 			<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Quantity</th>
	 			<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Gross</th>
	 		</tr>
  
 				<%
 					if(tradeItems != null && tradeItems.length > 0 ){
 						for(DBRow row : tradeItems){
 			    %>
  				 <tr>
 			    		<td><%= row.getString("item_name") %></td>
 			    		
 			    		<td class="fix_center"><%= row.get("mc_shipping",0.0d) %></td>
 			    		<td class="fix_center"><%= row.get("mc_handling",0.0d) %></td>
 			    		<td class="fix_center"><%= row.get("tax",0.0d) %></td>
 			    		<td class="fix_center"><%= row.get("quantity",0.0d) %></td>
 			    		<td class="fix_center"><%= row.get("mc_gross",0.0d) %></td>
 			    	</tr>	   
 				  <% 			
 							
 						}
 					}else{
 				%>
 				<tr>
	 				<td colspan="6" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	 			</tr>	
 					 
 				<% 		
 					}
 				%>
 			</tr>
 		 
 	</table>
 	
 	
 	
</body>

</html>