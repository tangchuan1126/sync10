<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 
	<style type="text/css">
		*{margin:0px;padding:0px;font-size:12px;}
		table.shop{border:1px solid silver;width:98%;border-collapse:collapse ;margin:0px auto;margin-top:5px;}
		table.shop th {font-size:12px;border:1px solid silver;line-height:25px;height:25px;background:#E6F3C5;}
		table.shop td{border:1px solid silver;text-indent:10px;line-height:22px;height:22px;}
		input.txt{border:none;border-bottom:1px solid silver;width:80px;}
		span.add , span.delete{margin-left:8px;width:16px;height:16px;display:block;float:left;border:0px solid silver;cursor:pointer;background:url("../imgs/add.png")}
		span.delete{background:url("../imgs/standard_msg_error.gif")}
	</style>
	<%
		DBRow[] rows = null;
		long billId = StringUtil.getLong(request,"bill_id");
	 	DBRow billRow = new DBRow();
	 	if(billId != 0l){
	 		billRow = orderMgrZr.getDetailById(billId,"bill_order","bill_id");
	 		rows =  billMgrZr.getItemsByBillId(billId);
	 	} 
	%>
	</head>
	<body>
	 


		<script type="text/javascript">
			window.parent.clearAndMoveDiv && window.parent.clearAndMoveDiv();
			var c = 1;
			jQuery(function($){
				  
			
				 countTotalPrice();
			 
				 $(".countQuantity").live("keyup",function(){
			 
					var _this = $(this);
					fixInputValue(_this,"int");
					countTotalPrice();
				 })
				 $(".countPrice").live("keyup",function(){
					var _this = $(this);
					fixInputValue(_this);
					countTotalPrice();

				 })
			 	 if('<%= billId %>' * 1 != 0){
					 $("#discount").val('<%= billRow.get("total_discount",0.0d)%>');
					 $("#rate option[target='"+'<%= billRow.getString("rate_type")%>'+"']").attr("selected",true);
					// alert( $("#rate option[target='"+'<%= billRow.getString("rate_type")%>'+"']").length);
				}
			 
		 
			})
			 function fixInputValue(_this,intFlag){
					var value = _this.val();
					var fixValue ;
					if(intFlag){
						fixValue = value.replace(/[^0-9]/g,'');
						_this.val(fixValue * 1);
					}else{
						
						 fixValue = value.replace(/[^0-9.]/g,'');
						if(fixValue.length < 1){
							fixValue = 0;
						}
						_this.val(fixValue);
					}
					
			}
			function addTr(){
		 
				var tbody = $("#tbody");
			  	 
				tbody.append(createTr());
				fixInputName();
			}
			 function deleteTr(_this){
				 if($("tr",$("#tbody")).length <= 1){alert("not delete ...") ; return ;};
				var node = $(_this).parent().parent();
				node.remove();
				fixInputName();
				countTotalPrice();
			}
			function createTr(){
				var tr ="<tr>";
				tr += "<td><input type='hidden' value='0' name='item_id' class='item_id' /><input type='text' class='txt item_number' style='width:200px;' name='item_number' value='ItemNumber'></td>";
				tr += "<td><input type='text' class='txt name'   style='width:150px;' name='name' value='ItemName'/></td>";
				tr += "<td><input type='text' class='txt countPrice' name='actual_price' value='0.0' /></td>";
				tr += "<td><input type='text' class='txt countQuantity' name='quantity' value='0' /></td>" ;
				tr += "<td><input type='text' class='txt amount' name='amount' value='0'></td>"
				tr += "<td><span class='delete' onclick='deleteTr(this)'></span></td></tr>";
				return $(tr);
			}
			function fixInputName(){
				var array = ['item_id','item_number','name','actual_price','quantity','amount'];
				 var tbody = $("#tbody");
				 var trs = $("tr",tbody);
				 var length = trs.length ;
				 trs.each(function(index){
					var tr = $(this);
					var inputs  = $("input",tr);
					for(var j = 0 , count = inputs.length ;j < count ;j++){
							var _input  = $(inputs[j]);
							_input.attr("name",array[j]+"_" + (index+1));
					}
					 
				 })
			}
			function fixAddAndRemoveButton(){
				
			}
			function countTotalPrice(_this){
				var node = $("#discount");
				var nodeValue = parseFloat(node.val());
				var trs = $("tr",$("#tbody"));
				var sum = 0 ;
				var total_quantity = 0 ;
				trs.each(function(){
					var tr = $(this);
					var countPrice = parseFloat($(".countPrice",tr).val());
					var countQuantity = parseFloat($(".countQuantity",tr).val());
					var value = countPrice * countQuantity;
				 	$(".amount",tr).val(decimal(value,2));
					sum += (value);
					total_quantity += countQuantity;
				})
				$("#total_quantity").val(total_quantity);
				$("#subtotal").val(sum);
				if(_this){
					fixInputValue($(_this));
				}
				
				if(sum < nodeValue){
					 alert("error");
					 node.val("0");
					 nodeValue = 0;
					 
				}
				if(!_this){
					node.val("0")
				 }
				 var shipping_fee = $("#shipping_fee").val();
				 shipping_fee = parseFloat(shipping_fee);
				$("#total").val(decimal(sum - nodeValue + shipping_fee  ,4));
			}
			function countDiscount(_this){
				if(_this){
					fixInputValue($(_this));
				}
				var trs = $("tr",$("#tbody"));
				 
				var sum = 0 ;
				trs.each(function(){
					var tr = $(this);
					var countPrice = parseFloat($(".countPrice",tr).val());
					var countQuantity = parseFloat($(".countQuantity",tr).val());
					sum += (countPrice * countQuantity);
				});
				 
				var  amount = $("#total").val();
				if(parseFloat(amount) >  sum){alert("Total Amount > All Items");countTotalPrice(); return; }
				var value = sum - amount;
				$("#discount").val(decimal(parseFloat(value) ,4));


			}
			function decimal(num,v){
				var vv = Math.pow(10,v);
				return Math.round(num*vv)/vv;
			} 
		 
			function volidate(){
			 var trs = $("tr",$("#tbody"));
			 var flag = false;
			 for(var index = 0 ; index < trs.length ; index++ ){
				 var _this = $(trs[index]);
				 var countPrice = parseFloat($(".countPrice",_this).val());
				 var countQuantity = parseFloat($(".countQuantity",_this).val());
				 if(countPrice * countQuantity <= 0 ){
					alert("error");
					 return false;
				 };
			 }
			
			}
			function showData(){
				var ebayForm = $("#ebayForm");
				alert(ebayForm.serialize());	
			}
			function countTotalPriceByShipping(){
				
				var shipping_fee = $("#shipping_fee");
				fixInputValue(shipping_fee);
				var value = parseFloat(shipping_fee.val());
				var total = $("#total").val();
				var subtotal = parseFloat($("#subtotal").val());
				var discount =parseFloat($("#discount").val());
				$("#total").val(decimal( value+ subtotal - discount,2));
				
			}
		</script>
		<div style="margin:0px auto;">
		 
			<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="ebayForm" target="_blank">
				
					<!-- 系统需要的字段 -->
					<input type="hidden" id="bill_type" name="bill_type" value="1"/>
					<input type="hidden" id="total_quantity" />
					
					
					 

				<table class="shop" width="80%">
					<thead>
					<tr >
						<th width="23%">Item Number</th>
						<th width="30%">Item Name</th>
						<th width="13%">Item Price</th>
						<th width="13%">Quantity</th>
						<th width="13">Amount</th>
						<th width="8%"><span class='add' onclick="addTr()"></span></th>
					 </tr>
					 </thead>
					 <tbody id="tbody">
					 <%if(billId == 0l){ %>
						 <tr>
							<td><input type='hidden' name="item_id_1" value="0" class="item_id"/><input type="text" class="txt item_number" style="width:200px;"  name="item_number_1" value="item_number_1"/></td>
							<td><input type="text" class="txt name" style="width:150px;" name="name_1" value="item_name 1111"/></td>
							<td><input type="text" class="txt countPrice" name="actual_price_1" value="3.0" /></td>
							<td><input type="text" class="txt countQuantity" name="quantity_1" value="2" /></td>
							<td><input type="text" class="txt amount" name="amount_1" value="0"/></td>
							<td>
								<span class="delete" onclick="deleteTr(this)"></span>
							</td>
						 </tr> 
					<%}else{
						if(rows != null && rows.length > 0){
							
							 for(int index = 0 , count = rows.length ; index < count ; index++ ){
					%>
						<tr>
							<td>
							<input type="hidden" class="item_id" name="item_id_<%=index+1 %>" value="<%=rows[index].getString("bill_item_id") %>"/>
							<input type="text" class="txt item_number" style="width:200px;"  name="item_number_<%=index+1 %>" value="<%= rows[index].getString("item_number") %>"/></td>
							<td><input type="text" class="txt name" style="width:150px;" name="name_<%= index+1 %>" value="<%=rows[index].getString("name") %>"/></td>
							<td><input type="text" class="txt countPrice" name="actual_price_<%=index+1 %>" value="<%= rows[index].get("actual_price",0.0d) %>" /></td>
							<td><input type="text" class="txt countQuantity" name="quantity_<%=index+1 %>" value="<%=rows[index].get("quantity",0.0d) %>" /></td>
 							<td><input type="text" class="txt amount" name="amount_<%=index+1 %>" value="<%=rows[index].get("amount",0.0d) %>"/></td>
						    <td><span class="delete" onclick="deleteTr(this)"></span></td>
						</tr>
					<% 		 
							 }
						}
						
					 %> 
					<%} %>
						
					 </tbody>
					 <tfoot>
					 <tr>
					 	<td colspan="6" style="text-align:right;">
					 		 Subtotal: <input type="text" id="subtotal"  name="" class="txt" value="<%= billRow.get("subtotal",0.0d) %>"/>
					 	</td>
					 </tr>
						<tr>
							<td colspan="6" style="text-align:right;">
								Total Discount :  <input type="text" id="discount" name=""  onkeyup="countTotalPrice(this)" value="<%=billRow.get("total_discount",2.0d) %>" class="txt" />
							</td>
						</tr>
						<tr>
							<td colspan="6" style="text-align:right;">
								Shipping/handling:  <input type="text" id="shipping_fee" name=""  onkeyup="countTotalPriceByShipping();" value="<%=billRow.get("shipping_fee",0.0d)  %>" class="txt"/>
							</td>
						</tr>
						<tr>
							<td colspan="6" style="text-align:right;">
								Total:  <input type="text" id="total" name="" onkeyup="countDiscount()" value="<%=billRow.get("save",0.0d)  %>" class="txt"/>
							</td>
						</tr>
						<tr>
							<td style="height:50px;">
							Currency:
								<select id="rate">
									<option value="1" target="RMB">人民币 (RMB)</option>
									<option value="<%=systemConfig.getStringConfigValue("USD")%>" target="USD" > <%=systemConfig.getStringConfigDescription("USD")%>  (USD)  </option> 
									<option value="<%=systemConfig.getStringConfigValue("AUD")%>" target="AUD"> <%=systemConfig.getStringConfigDescription("AUD")%>  (AUD)  </option> 
									<option value="<%=systemConfig.getStringConfigValue("EUR")%>" target="EUR"> <%=systemConfig.getStringConfigDescription("EUR")%>  (EUR)  </option> 
									<option value="<%=systemConfig.getStringConfigValue("CAD")%>" target="CAD"> <%=systemConfig.getStringConfigDescription("CAD")%>  (CAD)  </option> 
									<option value="<%=systemConfig.getStringConfigValue("CHF")%>" target="CHF"> <%=systemConfig.getStringConfigDescription("CHF")%>  (CHF)  </option> 
									<option value="<%=systemConfig.getStringConfigValue("CZK")%>" target="CZK"> <%=systemConfig.getStringConfigDescription("CZK")%>  (CZK)  </option> 
									<option value="<%=systemConfig.getStringConfigValue("GBP")%>" target="GBP"> <%=systemConfig.getStringConfigDescription("GBP")%>  (GBP)  </option> 
									<option value="<%=systemConfig.getStringConfigValue("NOK")%>" target="NOK"> <%=systemConfig.getStringConfigDescription("NOK")%>  (NOK)  </option> 
								</select>
							</td>
							<td colspan="5" style="text-align:right;">
								<input type="button" value="保存/并预览" class="normal-green-long" style="cursor:pointer;"/>
								<input type="button" value="预览" class="normal-green-long" style="cursor:pointer;" onclick="gotoShow();"/>
 
							</td>
						</tr>
					 </tfoot>
				</table>
				
			</form>
		</div>
	</body>
</html>