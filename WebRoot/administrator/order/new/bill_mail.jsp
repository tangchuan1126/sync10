<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.emailtemplate.velocity.quote.BillEmailPageString"%>
<%@ include file="../../../include.jsp"%> 
<%
 long billId = StringUtil.getLong(request,"bill_id");

 BillEmailPageString billEmailPage = new  BillEmailPageString(billId);
 com.cwc.app.page.core.VelocityTemplateString.getInstance().getPageContent(billEmailPage);

 String subject = billEmailPage.getTemplateTitle();

 String emailContent = billEmailPage.getBody();
	 	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../../js/jquery.form.js'></script>
<script type="text/javascript" src="../../js/select.js"></script>

<script type='text/javascript' src='../../js/autocomplete/jquery.bgiframe.min.js'></script>	
<script type="text/javascript" src="../../js/blockui/jquery.blockUI.233.js"></script>
<link rel="stylesheet" type="text/css" href="../../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script>
function sendEmail(billId)
{
	$.blockUI.defaults = { 
		css: { 
			padding:        '10px',
			margin:         0,
			width:          '200px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #aaa',
			backgroundColor:'#fff'
		},
		
		// 设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.8' 
		},

    centerX: true,
    centerY: true, 
	
		fadeOut:  2000
	};
	
	$.blockUI({ message: '<img src="../../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:blue">正在发送，请稍后......</span>'});
	
	var para = "bill_id="+billId;
		
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/bill/SendBillViaEmailAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false, 
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("网络错误，请重试！");
					$.unblockUI();
				},
				
				success: function(data){

					if (data.flag=="success")
					{
						$.blockUI({ message: '<img src="../../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">邮件发送成功！</span>' });
						window.setTimeout(function(){$.artDialog && $.artDialog.close();},1500);
					}
					else
					{
						$.blockUI({ message: '<img src="../../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">邮件发送失败！</span>' });
						window.setTimeout(function(){$.artDialog && $.artDialog.close();},1500);
						
					}
				}
			});

}
</script>
<style type="text/css">
<!--
.sendmail {
	background-attachment: fixed;
	background: url(../../imgs/product/sendemail.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 42px;
	width: 114px;
	color: #000000;
	border:0px;
	font-size:15px;
	font-weight:normal;
	font-family:宋体;
	margin-left:5px;
	margin-right:5px;
	padding-left:40px;
	padding-top:10px;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="email_form" id="email_form" method="post" action="" >
<input type="hidden" name="bill_id" value="<%=billId%>">

  <table width="93%" border="0" cellpadding="0" cellspacing="0" style="padding-left:40px;padding-top:10px;">
  <tr>
    <td>
	 <fieldset style="border:2px #cccccc solid;padding:15px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">邮件内容</legend>
	
<table width="100%" border="0" cellspacing="3" cellpadding="2">
  
  <tr> 
      <td width="5%" height="30" align="center" bgcolor="#eeeeee" class="STYLE2">标题</td>
      <td width="2%">&nbsp;</td>
    <td width="93%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="82%" style="font-size:17px;color:#333333;font-weight:bold;font-family:Arial, Helvetica, sans-serif">
		<%=subject%></td>
        <td width="14%" align="right" style="font-family:'黑体';font-size:20px;color:#666666;padding-right:10px;">附件<br>
          <span style="font-size:12px;font-weight:normal;font-family:'宋体';">报价单</span></td>
        <td width="4%" align="right"><a href="javascript:parent.exportPDF(<%=billId%>)" title="VisionariQuote<%=billId%>.pdf" ><img src="../../imgs/pdf_logo.jpg" width="37" height="37" border="0"></a></td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td height="23" align="center" bgcolor="#eeeeee" class="STYLE2">正文</td>
    <td>&nbsp;</td>
    <td align="left" valign="top" >
	<DIV id="oMTData"   style="height:320px;; overflow:auto; overflow-x:hidden;">
<%=emailContent%>	
</DIV>
</td>
  </tr>
</table>

	
	</fieldset>
	</td>
  </tr>

</table>

<br>
<table width="93%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%">&nbsp;</td>
    <td width="19%"><input name="Submit" type="button" class="sendmail" value="立即发送" onClick="sendEmail(<%=billId%>)"></td>
    <td width="67%" align="left" valign="bottom">
      <input name="Submit2" type="button" class="normal-white" value="取消" onClick="$.artDialog.close();">    </td>
  </tr>
</table>

</form>
</body>
</html>
