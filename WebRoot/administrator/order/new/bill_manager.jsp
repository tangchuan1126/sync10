<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.PaymentMethodKey,java.util.ArrayList,com.cwc.app.key.BillTypeKey,com.cwc.app.key.InvoiceStateKey,com.cwc.app.key.BillStateKey,com.cwc.app.key.BillTypeKey" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>账单管理</title>

<!-- 基本css 和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
 
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />


<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<style type="text/css">
	div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:500px;height:30px;background:white;margin-top:200px;line-height:30px;font-size:14px;}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	ul.setItem{list-style-type:none;margin-left:10px;margin-top:-2px;margin-bottom:5px;font-size:12px;font-weight:normal;}
	 ul.setItem li{border:0px solid silver;height:14px;line-height:14px;}
	 span.valueName{width:40%;border:0px solid red;display:block;float:left;text-align:right;}
span.valueSpan{width:56%;border:0px solid green;float:left;overflow:hidden;text-align:left;text-indent:4px;}
span.stateName{width:15%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
.title{font-size:12px;color:green;font-weight:blod;}
.fixTdP p{height:17px;}
.ui-tabs{z-index:1;}
input.noborder{width:250px;border:none;border-bottom:1px solid silver;color:silver;}
 body{text-align:left;}
 </style>
<%
	PaymentMethodKey methodKey = new PaymentMethodKey();
	BillTypeKey typeKey = new BillTypeKey();
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(10);
	DBRow[] rows = null ;
	String comefrom = StringUtil.getString(request,"comefrom");
	String  searchkey = StringUtil.getString(request,"searchkey");
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	
 	long loginAdid = adminLoggerBean.getAdid(); 
	TDate date = new TDate();
	date.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
	String	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
	String 	input_st_date =	 date.getStringYear()+"-"+date.getStringMonth()+"-"+date.getStringDay();
	int key_type = StringUtil.getInt(request, "key_type");
	int bill_type = StringUtil.getInt(request, "bill_type");
	int account_payee = StringUtil.getInt(request, "account_payee");
	String account = StringUtil.getString(request, "account");
	String account_name = StringUtil.getString(request, "account_name");
	int search_mode = StringUtil.getInt(request,"search_mode");
	
	if(comefrom.equals("tool"))
	{
		rows = billMgrZr.getBillBySearchKey(searchkey,search_mode,pc);
		
	}
	else if(comefrom.equals("filter"))
	{
		input_en_date = StringUtil.getString(request,"end");
		input_st_date = StringUtil.getString(request,"st");
		
		rows = billMgrZr.filterBill(request,pc);	
	}
	else
	{
		rows = billMgrZr.getAllBill(pc);
	}
	
	
 
	/// key 
	BillTypeKey billTypeKey = new BillTypeKey();
	BillStateKey billState = new BillStateKey();
	String deleteBillAction =  ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/DeleteBillAction.action";
	String createInvoiceAction =  ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/CreateInvoiceByBillIdAction.action";
	InvoiceStateKey invoiceStateKey = new InvoiceStateKey();
	String sendInvoiceAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/SendInvoiceByBillIdAction.action";
	String updateInvoiceAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/UpdateInvoiceByBillIdAction.action";
	String getAccountInfo = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/GetAccountInfoAction.action";
%>
<script type="text/javascript">
 var tip = []; 
 jQuery(function($){

	// alert('<%=key_type %>' + " " + '<%=bill_type %>' + " " + '<%=account_payee%>'+ " " + '<%= account%>' + " " + '<%= account_name%>');
	 addAutoComplete($("#searchkey"),
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/bill/GetSearchBillJSONAction.action",
				"merge_field","bill_id");

		 
	 
	 if('<%= comefrom%>' === "tool")
	 {
		 $("#searchkey").val('<%=searchkey%>');
		 $("#comefrom_page").val("tool");
		 $("#searchkey_page").val('<%= searchkey%>');
	 }
    
	 init();
 })
 function ajaxGetAccountInfo(key,account_payee){
	 var td = $("#account_name_td");
	 td.html("<img src='../imgs/ajax-loader.gif' style='margin-top:4px;margin-left:4px;'/>");
	$.ajax({
		url:'<%= getAccountInfo%>' + "?key="+key,
		dataType:'json',
		success:function(data){
			 $("img",td).remove();
			
			if(data){
				var selected = createMemberSelect(data);
				var node = $(selected);
				if(account_payee * 1 != -1){
					$("option[target='"+'<%= account_payee%>'+"']",node).attr("selected",true);
				}
				if('<%= account%>'.length > 0){
					var o = getTipDetail(key*1);
					$("option[target='-2']",node).attr("selected",true);
					var htmlAccount =  "收款人:"+"<input type='text' value='"+'<%= account%>'+"' name='account' onfocus=inputIn(this,'"+o["account"]+"') onblur=outInput(this,'"+o["account"]+"')  class='noborder' />";
					var htmlAccountName = "收款账号:" + "<input type='text' value='"+'<%= account_name%>'+"' name='account_name' onfocus=inputIn(this,'"+o["account_name"]+"') onblur=outInput(this,'"+o["account_name"]+"') class='noborder' />"
					$("#account_show").html(htmlAccount);
					$("#account_name_show").html(htmlAccountName);
				}
				td.append(node);
			  	$("#account_name").chosen({no_results_text: "没有该人"}).change(function(){
			  		var htmlAccount  = "" ;
			  		var htmlAccountName = "";
					var value = ($(this).val()) ;
					if(value * 1 == -2){
						//手工输入  在Td中添加两个input
						var o = getTipDetail(key*1);
						 htmlAccount =  "收款人:"+"<input type='text' value='"+o["account"]+"' name='account' onfocus=inputIn(this,'"+o["account"]+"') onblur=outInput(this,'"+o["account"]+"')  class='noborder' />";
						 htmlAccountName = "收款账号:" + "<input type='text' value='"+o["account_name"]+"' name='account_name' onfocus=inputIn(this,'"+o["account_name"]+"') onblur=outInput(this,'"+o["account_name"]+"') class='noborder' />"
						$("#account_show").html(htmlAccount);
						$("#account_name_show").html(htmlAccountName);
					}else if(value * 1 == -1){
						$("#account_show").html("");
						$("#account_name_show").html("");
					}else{
						var option = $("option:selected",$("#account_name"));
						// account_name
						var optionHtml = option.html();
						/*
						htmlAccount = "收款人:" +value ;
						htmlAccountName = "&nbsp;&nbsp;收款账号:"+optionHtml;

						$("#account_show").html(htmlAccount);
						$("#account_name_show").html(htmlAccountName);
						*/
						$("#account_show").html("");
						$("#account_name_show").html("");
						$("#account_payee").val(option.attr("target"));
					//	alert(option.attr("target"));
					}
				 });
			  	$(".chzn-search input").width(110);
			} 
		}
	})	
 }
	function inputIn(_this,value){
		var node = $(_this);
		if(node.val() === value){
			node.css("color","black").val("");
		}
	 }
	 function outInput(_this,value){
		var node = $(_this);
		if($.trim(node.val()).length < 1) {
			node.val( value).css("color","silver");
		}
			
	 }
 
 function createMemberSelect(data){
		var selected = ' <select data-placeholder="收款人" class="chzn-select" style="width:150px;" id="account_name">';
		var options = "<option value='-1'>选择收款人</option><option value='-2'>手工输入</option>";
		for(var index = 0 , count = data.length ; index < count ; index++ ){
			if(data[index]["is_tip"] * 1 == 0)
			{
				options += "<option target='"+data[index]["id"]+"' value='"+data[index]["account"]+"'>"+data[index]["account_name"]+"</option>";
			}else{
				var  o = new Object();
				o = {
					key_type:data[index]["key_type"],
					account_name:"*"+data[index]["account_name"],
					account:"*"+data[index]["account"]
				};
				tip.push(o)
			}
		}
		options += "</select>";
		selected += options;
 
		return selected;

	 }
 function getTipDetail(payid) {
	if(tip && tip.length > 0 ){
		for(var index = 0 , count = tip.length ; index < count ; index++ ){
			var o = tip[index];
			if(o["key_type"] * 1  == payid * 1){
				return o;
			}
		}
	}
	return {account:"",account_name:""};
 }
 function init(){
	 // 回显数据
	 var longAdid = <%= loginAdid%>  *1 ;
		 
	if('<%= comefrom%>' === "filter"){
		initFilter();
		
	 }
	 
	$("#st").date_input();
	$("#end").date_input();
	
	//搜索条支持回车
	$("#searchkey").keydown(function(event)
	{
		if (event.keyCode==13)
		{
			search();
		}
	});
	 
 }
 function initFilter(){
	var billState = $("#billState");
	var billStatus = <%= StringUtil.getInt(request,"bill_status")%> * 1 ;
	$("option[value='"+billStatus+"']",billState).attr("selected",true);
	if(billStatus == <%= BillStateKey.bargain%> * 1){
		var porderId = '<%= StringUtil.getString(request,"porder_id")%>';
		$("#spanBillState").html("关联Order:<input type='text' name='porder_id' style='width:120px;' value='"+porderId+"'/>");
	}
	var createAdid = <%= StringUtil.getLong(request,"create_adid")%> * 1 ;
	 
	if(createAdid != 0){
		$("option[value='"+createAdid+"']",$("#create_adid")).attr("selected",true);
	}
	 

	// 回显bill_type,sel
	$("#bill_type option[value='"+<%=bill_type %>+"']").attr("selected",true);
	$("#sel option[value='"+<%=key_type%>+"']").attr("selected",true);
	if('<%= key_type%>' * 1 != -1){
		ajaxGetAccountInfo('<%= key_type%>' * 1,'<%= account_payee%>');
	}
	 
	
 }
 function addNew(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/add_bill.html"; 
	 $.artDialog.open(uri , {title: '新增账单',width:'1100px',height:'530px', lock: true,opacity: 0.3,fixed: true});
 }
 function refreshWindow(){
	 
	window.location.reload();
 }
 
 function updateBill(bill_type,billId){
	 var uri 
 	 if(bill_type == '<%= BillTypeKey.order%>'){
 		 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/convert_order_to_bill.html?bill_id="+ billId; 
 	 }else{
 		 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/add_bill.html?bill_id="+ billId; 
 	 }
	 $.artDialog.open(uri , {title: "[ "+billId+" ]账单修改",width:'1100px',height:'530px', lock: true,opacity: 0.3,fixed: true,cancel: function () {
		 $.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/cleanCartQuote.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				error: function(){
					showMessage("clear session failed","error");
				},
				success: function(msg){
				 
				}
			});	
	    }});
 }
 function deleteBill(billId){
	if(billId && billId.length > 0 ){
		$.artDialog.confirm('你确认取消操作？', function(){
			ajaxDelete(billId);
		}, function(){
		});
	}
 }
 function ajaxDelete(billId){
	$.ajax({
		url:'<%= deleteBillAction%>' +"?bill_id=" + billId,
		dataType:'text',
		success:function(data){
			if(data && data === "success"){
				showMessage("取消成功!","succeed")
				window.location.reload();
			}else{
				showMessage("系统错误.","error");
			}			
	    }
	})
 }
 function createInvoice(billId,cmd){
	 var title =(cmd === "create") ? "创建":"创建并且发送";
	 lockWindow('warning',title+"Invoice <span style='color:green;'>[ "+billId+" ]</span>");
	 $.ajax({
		url:'<%= createInvoiceAction%>' + "?bill_id=" + billId +"&cmd="+cmd,
		dataType:'json',
		error:function(XMLHttpRequest, textStatus, errorThrown){
			$.artDialog.list['infoDialog'] && $.artDialog.list['infoDialog'].close();
			 showMessage("系统错误.","error");
		},
		success:function(data){
			$.artDialog.list['infoDialog'] && $.artDialog.list['infoDialog'].close();
			 if(data && data.flag){
				 if(data.flag === "success"){
					 showMessage(title+"Invoice成功.","succeed");
					 window.location.reload();
				 }else{
					 lockWindow('error',data.message);
				}
			 }else{
				 showMessage("系统错误.","error");
			}
		}	
	 })
	 
 }
 function sendInvoice(invoice_state,billId){
	 	if(invoice_state*1 == '<%=invoiceStateKey.NewAdd%>'){
			showMessage("请先创建Invoice","alert");
			return ;
		 }
		 if(invoice_state*1 == '<%=invoiceStateKey.SendInvoiceed%>'){
				showMessage("该Invoice已经发送","alert");
				return ;
		 }	
		 
		 lockWindow('warning',"正在发送Invoice <span style='color:green;'>[ "+billId+" ]</span>");
		$.ajax({
			url:'<%= sendInvoiceAction%>' + "?bill_id="+billId,
			dataType:'json',
			error:function(XMLHttpRequest, textStatus, errorThrown){
				$.artDialog.list['infoDialog'] && $.artDialog.list['infoDialog'].close();
			 	showMessage("系统错误.","error");
			},
			success:function(data){
				$.artDialog.list['infoDialog'] && $.artDialog.list['infoDialog'].close();
				if(data){
					if(data.flag === "success"){
						showMessage("发送Invoice成功.","succeed");
						window.location.reload();
					}else{
						lockWindow('error',data.message);
					}
				}else{
					showMessage("系统错误请稍后再试","error");
				}
		 	}
		})
 }
 function updateInvoice(billId){
	 lockWindow('warning',"正在更新Invoice <span style='color:green;'>[ "+billId+" ]信息</span>");
		$.ajax({
			url:'<%= updateInvoiceAction%>' + "?bill_id="+billId,
			dataType:'json',
			error:function(XMLHttpRequest, textStatus, errorThrown){
				$.artDialog.list['infoDialog'] && $.artDialog.list['infoDialog'].close();
			 	showMessage("系统错误.","error");
			},
			success:function(data){
				$.artDialog.list['infoDialog'] && $.artDialog.list['infoDialog'].close();
				if(data){
					if(data.flag === "success"){
						showMessage("更新Invoice成功.","succeed");
						window.location.reload();
					}else{
						lockWindow('error',data.message);
					}
				}else{
					showMessage("系统错误请稍后再试","error");
				}
		 	}
		})
 }
 function lockWindow(type,text){
	 $.artDialog({
		    id:'infoDialog',
		    lock: true,
		    background: '#808080', // 背景色
		    opacity: 0.87,	// 透明度
		    content: text || '请稍等.......'  ,
		    icon: type || 'warning' 
		});
 }

 function go(number){
		$("#pageCount").val(number);
		
		if('<%= comefrom%>' === "filter"){
		 
			var filterForm = $("#filterForm");
			filterForm.append("<input type='hidden' name='p' value='"+number+"'/>")
			filterForm.submit();
		}else{
			$("#pageForm").submit();
		}
	}
 
	function valueValidate(_this){
		var node =  $(_this);
		var value = node.val();
		node.val(value.replace(/[^0-9]/g,''));
	}
	
	function search()
	{
		var searchkey = $("#searchkey").val();
		
		searchkey = searchkey.replace(/\"/g,'');
		var val_search = "\""+searchkey+"\"";
		$("#searchkey").val(val_search);
		
		if($.trim(searchkey).length < 1)
		{
			showMessage("请先输入查询条件","alert");
			return ;
		}
		var toolForm = $("#toolForm");
		document.toolForm.search_mode.value = 1;
		toolForm.submit();
	}
	
function searchRightButton()
{
	var val = $("#searchkey").val();
	
	if (val=="")
	{
		alert("你好像忘记填写关键词了？");
	}
	else
	{
		val = val.replace(/\"/g,'');
		$("#searchkey").val(val);
		document.toolForm.searchkey.value = val;
		document.toolForm.search_mode.value = 2;
 		document.toolForm.submit();
	}
}

function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) {  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  

	document.getElementById("eso_search").onmouseup=function(oEvent) {  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
         	searchRightButton();
    	}  
	}  
}

	function changeState(){
		var billState = $("#billState").val();
		var spanBillState = $("#spanBillState");
		 
		if(billState * 1 == 1 * '<%= BillStateKey.bargain%>'){
			spanBillState.html("关联Order:<input type='text' name='porder_id' style='width:120px;'/>");
		}else{
			 
			spanBillState.html("");
		}
	}
	
	function filterBill(){
		var filterForm = $("#filterForm");
		// 验证如果是手动输入收款人和收款账号的好。要检查是不是都输入了
	 	var account = $("input[name='account']");
	 	var accountName = $("input[name='account_name']");
	 	if(account.length > 0 && accountName.length > 0){
			if(account.val().length < 1 || account.val().indexOf("*") != -1){
				showMessage("输入收款人","alert")
				return ;
			}
			if(accountName.val().length < 1 || accountName.val().indexOf("*") != -1){
				showMessage("输入收款账号","alert")
				return ;
			}
		}
	 	 
	 
		filterForm.submit();
	}
	

</script>
<style type="text/css">
.search_shadow_bg
{
	background:url(../../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:29px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>


</head>
<!-- 
	1.对于新创建的Bill 那么只有create invoice 和 创建并发送
	2.对于已经create invoice 那么有 send invoice
	3.对于send invoice 那么只应该有更新

 -->
<body  onload = "onLoadInitZebraTable()">
	<!-- 遮盖层 -->
	  <div class="cssDivschedule">
	 		<div class="innerDiv">
	 			请稍等.......
	 		</div>
	 </div>

	 <div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
			 <ul>
				 <li><a href="#tool">常用工具</a></li>
				 <li><a href="#filter">过滤</a></li>		 
			 </ul>
			 <div id="tool">
		 
				 <form action="" id="toolForm" name="toolForm">
					 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
			            <tr>
			              <td width="30%" style="padding-top:3px;">
						   <div id="easy_search_father">
						   <div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"></a></div>
						   </div>
							<table width="485" border="0" cellspacing="0" cellpadding="0">
							  <tr>
							    <td width="418">
									<div  class="search_shadow_bg">
										<input type="text" name="searchkey" id="searchkey" value="" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333">
									</div>
								</td>
							    <td width="67">
								 <a href="javascript:void(0)" id="search_help" style="padding:0px;color: #000000;text-decoration: none;"><img src="../imgs/help.gif" border="0" align="absmiddle" border="0"></a>
								</td>
							  </tr>
							</table>
							<script>eso_button_even();</script>		
						</td>
			              <td width="40%" ></td>
			              <td width="17%" align="right" valign="middle">
							<input type="button" value="新增" class="long-button-add" onclick="addNew()"/>
						 </td>
			            </tr>
			          </table>
			          <input type="hidden" name="search_mode" id="search_mode"/>
			          <input type="hidden" name="comefrom" id="comefrom" value="tool"/>
			 	 </form>
			 
				 	 <div>
			    	
				</div>
				 
			 </div>
			 <div id="filter" style="">
		  
				 <form id="filterForm">
				 	<input type="hidden" name="comefrom" value="filter"/>
				 	开始:<input type="text" name="st" value="<%= input_st_date %>" id="st" style="width:80px;"/>&nbsp;-&nbsp;<input type="text" name="end" id="end" value="<%= input_en_date %>" style="width:80px;"/>
				 	 <select name="create_adid" id="create_adid">
				 				<option value="-1">全部客服报价人</option>
				 			<%
								DBRow admins[] = adminMgr.getAllAdmin(null);
								for (int i=0; i<admins.length; i++){
							%>
								   <option value="<%=admins[i].get("adid",0l)%>" > 
								    <%=admins[i].getString("account")%>  </option>
							 <%
								}
								%>
				 			</select>
				 		 <select id="bill_type" name="bill_type">
				 				<option value="-1">账单全部类型</option>
				 			<%
				 				DBRow typeKeyRow =	typeKey.getRow();
				 				List<String> fieldName = typeKeyRow.getFieldNames();
				 				for(String name : fieldName){
				 					%>
				 					<option value='<%=name %>'><%= typeKey.getStatusById(Integer.parseInt(name)) %></option>
				 					<% 		
				 				}
				 			%>
				 		</select>
					 	 <select id="billState" name="bill_status" onchange="changeState();">
					 			<option value="-1">账单全部状态</option>
					 			 <%
					 				List<String> state = billState.getStatus();
					 			 	for(String s : state){
					 			 	%>
					 			 		<option value="<%= s %>"><%= billState.getStatusById(Integer.parseInt(s)) %></option>
					 			 	<% 						 			 		
					 			 	}
					 			 %>
					 		</select>
					 		<span id="spanBillState">
				 			</span><br />
				 			<br />
				 			<table>
				 				<tr>
				 					<td>
					 					<select data-placeholder="收款款方式" class="chzn-select" style="width:120px;" name="key_type" id="sel">
											<option value="-1">选择收款方式</option>
									 
											<%
												PaymentMethodKey key = new PaymentMethodKey();
												ArrayList<String> payMentList=  key.getStatus();
												for(String name : payMentList){
											%>
													<option value="<%=name %>"><%=key.getStatusById(Integer.parseInt(name)) %></option>
											<% 	 
												}
											%>
											 
										</select>
									</td>
									<td id="account_name_td">
									</td>
									<td id="account_show">
									</td>
									<td id="account_name_show">
									</td>
				 					<td>
				 						<input type="hidden" name="account_payee" id="account_payee"/>
				 					  	<input id="seachByCondition" class="button_long_refresh" onclick="filterBill();" type="button" value="过 滤" name="seachByCondition" />
				 					</td>
				 				</tr>
				 			</table>
				 			 
				 
				 	</form>
				 
			 </div>
	 </div>
 		
 		<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="true" class="zebraTable" style="margin-left:3px;margin-top:5px;">
			
			  <tr> 
			  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">综合信息</th>
			  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">收款人信息</th>
			  	<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">商品详细</th>
		        <th width="25%" style="vertical-align: center;text-align: center;" class="right-title">费用信息</th>
		    	<th width="5%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
		      </tr>
		      <tbody>
		      <%
				if(rows != null && rows.length > 0 ){
					for(DBRow row : rows){
							DBRow userInfo = null;
						    userInfo = waybillMgrZR.getUserNameById(row.get("create_adid",0l));
						    String userName = "";
							if(userInfo != null ){
								  userName =  userInfo.getString("employe_name");
							}
							 
			 %>
			 	<tr>
			 		<td>
			 		 	<!-- 综合信息  -->
			 		 	<fieldset class="set" style="padding-bottom:4px;border:2px solid #993300;">
			  				 <legend>
									<span class="title">
										<span style="font-weight:bold;">[<%= row.get("bill_id",0l) %>] &nbsp;
										<%=billTypeKey.getStatusById(row.get("bill_type",0)) %>
										<% if(row.get("bill_type",0) == billTypeKey.order ){
										%>
											<%=  row.get("porder_id",0l)%>
										<% 	
										}
										%>
										
										</span>
										&nbsp;账单  &nbsp;
									</span>
									 
							</legend>
						 	<%
						 		// 读取收款人的基本信息
						 		DBRow accountRow = orderMgrZr.getDetailById(row.get("account_payee",0l),"account_payee","id");
						 	
						 	%>
							<p style="padding-left:11%;">
								<span style="font-weight:bold;font-size:14px;"><%= methodKey.getStatusById(row.get("key_type",0)) %></span>	<br />
								 <%=  accountRow != null ? accountRow.getString("account_name"):row.getString("account_name")%><br />
								 <%=  accountRow != null ? accountRow.getString("account"):row.getString("account")%><br />
							</p>
							<p>
			  					<span class="alert-text stateName">状态:</span>
			  					<span class="stateValue" style="font-weight:bold;">
			  						<%= billState.getStatusById(row.get("bill_status",1))%>
			  						<%
			  							if(row.get("bill_status",1) == billState.bargain ){
			  							%>
			  							&nbsp;&nbsp;&nbsp;<%= row.get("porder_id",0l)%>&nbsp;&nbsp;<img title="相关订单" border="0" width="14" height="14" src="../imgs/nav_text.jpg">
			  						<% 	 
			  							}
			  						%>
			  					</span>
			  				</p>
			  				<%if(row.get("key_type",0) == PaymentMethodKey.PP){  %>
								 <p>
								 	<span class="alert-text stateName">Invoice</span>
								 	<span class="stateValue"><%=invoiceStateKey.getStatusById(row.get("invoice_state",1)) %></span>
								  
								 </p>
							 <%} %>
			  				<p>
			  					<span class="alert-text stateName"><img title="操作人" src="../imgs/order_client.gif" /></span>
			  					<span class="stateValue"><%=userName %></span>
			  				</p>
			  				<p>
			  					<span class="alert-text stateName"><img  title="创建时间" src="../imgs/alarm-clock--arrow.png" /></span>
			  					<span class="stateValue"><%= row.getString("create_date")%></span>
			  				</p>
			  				
			  			</fieldset>
			 		</td>
			 		<td style="padding-bottom:5px;" class="fixTdP">
			 			 <!-- 地址信息 -->
			 			 	<p style="margin-top:5px;">Client Id : <span style="font-weight:bold;"><%= row.getString("client_id")%></span></p>
						 	 <p>	
			  					<span class="alert-text valueName">Name:</span>
			  					<span class="valueSpan"><%= row.getString("address_name")%></span>
							 </p>
							  <p>	
			  					<span class="alert-text valueName">Street:</span>
			  					<span class="valueSpan"><%= row.getString("address_street")%></span>
							 </p>
							  <p>	
			  					<span class="alert-text valueName">City:</span>
			  					<span class="valueSpan"><%= row.getString("address_city")%></span>
							 </p>
							  <p>	
			  					<span class="alert-text valueName">State:</span>
			  					<span class="valueSpan"><%= row.getString("address_state")%></span>
							 </p>
							  <p>	
			  					<span class="alert-text valueName">Zip:</span>
			  					<span class="valueSpan"><%= row.getString("address_zip")%></span>
							 </p>
			  				<p>	
			  					<span class="alert-text valueName">Tel:</span>
			  					<span class="valueSpan"><%=row.getString("tel")%></span>
							 </p>
							 <p>	
			  					<span class="alert-text valueName">Country:</span>
			  					<span class="valueSpan"><%= row.getString("address_country")%></span>
							 </p>
			 		</td>
			 		<td>
			 		<!--  商品信息 -->
			 		<fieldset class="set" >
							<legend style="font-size:12px;font-weight:bold;color:#000000;background:none;">
								<span style="font-size:13px;color:#000000;font-weight:bold"></span>
								<!--  
								<span style="font-weight:bold"> <%=row.get("subtotal",0.0d) %>  <%=row.getString("rate_type") %> / </span>
								<span style="font-weight:bold;color:#999999"> 0.0 RMB </span>
								-->
								<span class="alert-text">&nbsp;&nbsp;件数</span>
								<span style="font-weight:bold">: <%=row.get("total_quantity",0.0d) %></span>
							</legend>
							<%
								DBRow[] items = billMgrZr.getItemsByBillId(row.get("bill_id",0l));
								if(items != null  && items.length > 0){
									for(int index = 0 , count = items.length ; index < count ; index++ ){
							 %>
							 		 <%=items[index].getString("name") %> x	<span style="color:blue"><%=items[index].get("quantity",0.0f) %> <%=items[index].getString("unit_name") %></span><br />
									 <%
									 	if(items[index].get("product_type",0)==CartQuote.UNION_CUSTOM ){
									 		// 查询
									 		DBRow[] setInItem =	productMgr.getProductUnionsBySetPid(items[index].get("pc_id",0));
									 		if(setInItem != null && setInItem.length > 0){
									 %>
									 			<ul class="setItem">
									 				<%
									 					for(int j = 0 , length = setInItem.length ; j < length ; j++ ){	
									 				%>
									 					<li>├<%=setInItem[j].getString("p_name") %> x <%=setInItem[j].get("quantity",0.0f) %></li>
									 				<% 			
									 					}
									 				%>
									 			</ul>
									 <% 			
									 		}
									 	}	 
									 %>	
							 <% 					
									}
								}
							%>
							 
						</fieldset>
			 		</td>
			 		<td>
			 			<!-- 钱的信息 -->
			 			<fieldset class="set" >
			 				<legend style="font-size:12px;font-weight:bold;color:#000000;background:none;">
			 					货币种类:<span><%= row.getString("rate_type") %></span>&nbsp;&nbsp;<span class="alert-text">&nbsp;&nbsp;汇率:</span><span style="color:green;"><%=row.get("rate",0.0d) %></span>
			 				</legend>
			 				快递公司：<span style="color:bluel;">
			 				<%
			 					DBRow entity = orderMgrZr.getDetailById(row.get("sc_id",0l),"shipping_company","sc_id") ;
			 					out.println(entity != null ? entity.getString("name"):"");
			 				%></span><br />
			 				发货仓库：<span style="color:bluel;"><%
			 					DBRow PsRow = orderMgrZr.getDetailById(row.get("ps_id",0l),"product_storage_catalog","id");
			 					out.println(PsRow != null ? PsRow.getString("title"):"");
			 				%></span><br />
			 				重量：<span><%=row.get("total_weight",0.0d) %> <span style="font-weight:bold;color:green;"><%= row.getString("weight_type") %></span>&nbsp;</span>
			 				运费：<span style="color:blue;"><%= row.get("shipping_fee",0.0d)%></span><br />
			 				折扣：<span><%= row.get("total_discount",0.0d)%></span> <br />
			 				商品：<span><%= row.get("subtotal",0.0d)%></span><br />
			 				总价：<span><%= row.get("save",0.0d)%></span> <br />
			 			</fieldset>
			 			
			 		</td>
			 		<td>
			 		<!-- 
			 			0. 只有是Paypal的才有发送这些东西
			 			1. 如果是成交状态那么是没有修改删除。
			 			2. 如果是删除那么只能够是改变bill_state。为取消
			 			
			 		 -->
			 		  <%
			 		   boolean hasOper = billMgrZr.hasAuthority(loginAdid,row.get("create_adid",0l));
			 		   if(row.get("bill_status",0) == billState.quote && row.get("key_type",0) == PaymentMethodKey.PP && hasOper){ %>
			 		  
			 			<input type="button" value="修改" class="short-short-button-mod" onclick="updateBill('<%= row.get("bill_type",0l) %>','<%=row.get("bill_id",0l) %>')"/>	
  			 			
			 			<%	
			 				int invoiceState =  row.get("invoice_state",1);
			 		
			 				//invoiceState = invoiceState > 3? invoiceState - 3:invoiceState;
			 				if(invoiceState == invoiceStateKey.NewAdd){
			 					// 新创建	
			 			%>
			 				<!--  <input type="button" value="创建" class="short-short-button-damage" onclick="createInvoice('<%=row.get("bill_id",0l) %>','create')"/>-->
   			 				<input type="button" value="创建发送" class="short-button" onclick="createInvoice('<%=row.get("bill_id",0l) %>','createandsend')"/>
			 			<% 		
			 				}else if(invoiceState == invoiceStateKey.CreateInvoiceed){
			 			%>
			 				<!--  	<input type="button" value="发送" class="short-short-button-cart" onclick="sendInvoice('<%=row.get("invoice_state",1) %>','<%=row.get("bill_id",0l) %>')"/>-->
			 			<% 		
			 				}else if(invoiceState >= invoiceStateKey.NotUpdateBillInvoice){
			 			%>
			 				<input type="button" value="更新" class="short-short-button-cart" onclick="updateInvoice('<%=row.get("bill_id",0l) %>')" />
			 			<% 		
			 				}
			 		  }
			 			%>
			 			<%if(row.get("bill_status",0) == billState.quote && hasOper){ %>
			 				<input type="button" value="取消" class="short-short-button-del" onclick="deleteBill('<%=row.get("bill_id",0l) %>')"/>	
			 			 <%} %>
						<input type="button" value="详细信息" class="short-button" onclick="location='new/bill_detail.html?bill_id=<%= row.get("bill_id",0l) %>'" />
			 		</td>
			 	</tr>	
			<% 		
					}
				}else{
			%>
				<tr>
	 				<td colspan="5" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	 			</tr>	 
			<% 	
				}
			 %>	
		      </tbody>
 		</table>
 	<form action="" id="pageForm">
		<input type="hidden" name="p" id="pageCount"/>
		<input type="hidden" name="comefrom" id="comefrom_page"/>
		<input type="hidden" name="searchkey" id="searchkey_page"/>
	</form>
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  
	  <tr>
	    <td height="28" align="right" valign="middle"><%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
	      跳转到
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
	        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
	    </td>
	  </tr>
	</table>
 	<script type="text/javascript">
 	$("#tabs").tabs({
 		cache: true,
		cookie: { expires: 30000 } 
 	 });
	jQuery(function($){
		 $("#sel").chosen({no_results_text: "没有匹配形式"}).change(function(){
			  key =  $(this).val() ;
			  ajaxGetAccountInfo($(this).val());
		 });
	})
	
 	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	} 
 	</script>
 	 
	</body>
</html>