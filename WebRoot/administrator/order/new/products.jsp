<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../../include.jsp"%> 
<%@ page import="com.cwc.app.util.Config" %>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%
String cmd = StringUtil.getString(request,"cmd");
 
 
long billId = StringUtil.getLong(request,"bill_id");
DBRow billRow = new DBRow();
if(billId != 0l){
	billRow = orderMgrZr.getDetailById(billId,"bill_order","bill_id");
}
long ps_id = 0l;
if(billId != 0l){
	ps_id = StringUtil.getLong(request,"ps_id");
	double rate = StringUtil.getDouble(request,"rate");
	orderMgrZr.addItemsInCart(session,billId,ps_id,billRow.get("rate",1.0d));
}
long ccid =  StringUtil.getLong(request,"ccid"); 	// 国家
long pro_id = StringUtil.getLong(request,"pro_id"); // 省份
String countryName = StringUtil.getString(request,"country_name");
String addressStateName = StringUtil.getString(request,"address_state_name");
String getFeeAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/GetShippingFeeAction.action";

Object object =  session.getAttribute(Config.cartQuoteSession +"_ps_id");
 
if(object != null){
	ps_id = (Long) object;
}
 
%>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>

 
 
 

 

<style>
.ebayTitle
{
	font-weight:bold;
	font-size:11px;
	color:#666666;
}
 
.save
{
	background-attachment: fixed;
	background: url(../imgs/save_quote2.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 40px;
	width: 121px;
	color: #000000;
	border: 0px;
	 
}
span.fee{font-size:12px;cursor:pointer;display:block;height:35px;line-height:35px;text-align:center;font-weight:bold;border:1px solid silver;width:120px;float:left;margin-left:5px;margin-top:2px;background:#f8f8f8;}
span.fee:hover{background:white;}
span.fee:visited{background:none;}
span.fee.feeon{background:#f60;color:white;}
 span.bottom,span.top{display:block;width:100%;text-align:center;height:17px;}
 span.top{margin-top:-8px;}
 
 
 span.noselect{font-size:12px;cursor:pointer;display:block;height:35px;line-height:35px;text-align:center;font-weight:bold;border:1px solid silver;width:140px;float:left;margin-left:5px;margin-top:2px;background:#FF33CC;}
 span.noselect:hover{background:white;}
 span.noselect:visited{background:none;}
 span.noselect.feeon{background:#f60;color:white;}
</style>






<script>
function customProduct(pid,ccid,p_name,cmd)
{
	//tb_show('定制套装',"../product/custom_product.html?cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&TB_iframe=true&height=300&width=400",false);
	//tb_show('定制套装',"../product/custom_product_quote.html?ccid="+ccid+"&cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&ps_id="+$("#ps_id").val()+"&ps_name="+$("#ps_id").getSelectedText()+"&TB_iframe=true&height=300&width=400",false);
 
	var come = ('<%= billId%>' * 1 != 0)? "bill_update":"";
 
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/custom_product_quote.html?ccid="+ccid+"&cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&ps_id="+$("#ps_id").val()+"&ps_name="+$("#ps_id").getSelectedText()+"&come="+come; 
	 $.artDialog.open(uri , {title: '定制商品',width:'400px',height:'300px', lock: true,opacity: 0.3});
}

$().ready(function() {
	addAutoComplete($("#p_name"),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");
	//修改状态，初始化下快递列表
 
	initForm();		
	//加载购物车
	 loadCart();
	 
	


});
function initForm(){
	var ps_id = $("#ps_id");
	var value = <%= ps_id%>;
	$("option[value='"+value+"']",ps_id).attr("selected",true);



	// update 
	if('<%= billId%>' * 1 != 0){
		// 创建一个shipping_fee , rate , weight-type,
		 
		$("option[target='"+'<%= billRow.getString("rate_type")%>'+"']",$("#rate")).attr("selected",true);
 		 var options = $("option","#weight-type");
 		 options.attr("selected",false);
 		 for(var index = 0 , count = options.length ; index < count ; index++ ){
				var node = $(options[index]);
				if(node.html() === '<%= billRow.getString("weight_type")%>'){
					node.attr("selected",true);
				}
 	 	 }

 	 	
	}
	

	
	
}
//=================== 购物车 =========================
 
function loadCart(operateType){
		var bill_id = '<%= billId%>';
		var node = $(".b_fee",$(".feeon"));
		 
		var selected_fee = (!node?0.0:node.parent().parent().attr("value"));
		var weight_type = $("#weight-type").val();
		var length_type = $("#length-type").val();
		operateType = operateType ? operateType :"";
	var object = {ps_id:$("#ps_id").val(),operate_type:operateType,bill_id:bill_id,ccid:'<%=ccid%>',pro_id:'<%=pro_id%>',rate:$("#rate").val(),'target':$("option:selected","#rate").attr("target"),'selected_fee':selected_fee,'weight_type':weight_type,'length_type':length_type};
	$.ajax({
		url: 'new/products_list.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:jQuery.param(object),
		beforeSend:function(request){
			$("#action_info").text("计算中......");
		},
		error: function(){
			$("#action_info").text("计算失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
			$("#action_info").text("");
		}
	});
}

 
function put2Cart(price_type){
	var p_name = $("#p_name").val();
	var quantity = $("#quantity").val();
	var ps_id = $("#ps_id").val();
	var sc_id = $("#sc_id").val();
	var price_type = price_type;
	
	if (ps_id==0){
		showMessage("请选择发货仓库","alert");
	}
	else if (p_name==""){
		showMessage("请填写商品名称","alert");
	}
	else if (quantity==""){
		showMessage("请填写数量","alert");
	}
	else if ( !isNum(quantity) ){
		showMessage("请正确填写数量","alert");
	}
	else if (quantity<=0){
		showMessage("数量必须大于0","alert");
	}
	else{
		var para = "p_name="+p_name+"&quantity="+quantity+"&ps_id="+ps_id+"&ccid=<%=ccid%>&price_type="+price_type;

		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/put2CartQuote.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			error: function(){
				loadCart();
				$("#action_info").text("商品添加失败！");
			},
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException"){
					showMessage("商品不存在，不能添加到订单！","alert");
				}
				else if (msg=="ProductNotProfitException"){
					showMessage("商品没有设置毛利，不能销售","alert");
				}
				else if (msg=="ProductNotCreateStorageException")
				{
					showMessage("["+$("#ps_id").getSelectedText()+"] 不发 ["+p_name+"]","alert");
				}
				else if (msg=="ok"){
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("");
					
					loadCart("add");
					removeFeeSpan();
				}
				else{
					loadCart();
					alert(msg);
				}
			}
		});	
	}
}


function delProductFromCart(pid,p_name,product_type)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/removeProductFromCartQuote.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"pid="+pid+"&product_type="+product_type,
					
			error: function(){
				$("#action_info").text("商品删除失败！");
				loadCart();
			},
			
			success: function(msg){
				$("#action_info").text("");
				removeFeeSpan();
				loadCart("delete"); 
			}
		});	
	}
}


function cleanCart()
{
	if (confirm("确定清空购物车？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/cleanCartQuote.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			
			error: function(){
				$("#action_info").text("清空购物车失败！");
				loadCart();
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart("delete");
			}
		});	
	}
}
 

var cartProQuanHasBeChange = false;
 


function isNum(keyW)
{
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
//=================== 购物车 =========================


var cartIsEmpty = <%=cart.isEmpty(session)%>;

function checkCart()
{
	if (cartIsEmpty)
	{	
		alert("请先添加商品");
		return(false);
	}
	else
	{
		return(true);
	}
}

 
function getExpressCompanysByScId4Change(ps_id,ccid,pro_id){
		var session_ps_id =  $("#parent_session_id").val();
		if(session_ps_id != "0" && ps_id != "0"){
			if(ps_id !=session_ps_id ){
				 // 表示的是 改变了ps_id
				 var flag = window.confirm("去确定改变了发货仓库?")
				 if(flag){
					 //改变Session中的值。然后让list 的刷新.改变过后是否计算了运费 ，如果是的话就要把Span去掉
					 $.ajax({
						 url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/UpdateSessionPsIdAction.action',
						 dataType:'text',
						 data:jQuery.param({ps_id:ps_id}),
						 success:function(data){
						 	if(data === "success"){
						 		$("#parent_session_id").val(ps_id);
						 		 loadCart();
						 		 removeFeeSpan();
							}
						 }
					 })
					
				}else{
					// 不改变里面 的值
					$("option[value='"+session_ps_id+"']",$("#ps_id")).attr("selected",true);
				}
			} 
		}else{
			$("#parent_session_id").val(ps_id);
		}  
}
// 移除计算出来的运费
function removeFeeSpan(){
	$("#feeId span").remove();
}
function loadByRate(){
	// 如果是已经计算了运费的。在切换国家的时候把 。运费重新计算
	// 如果是有一个选中的时候那么就要改变计算save值
	var spans = $(".fee",$("#feeId"));
	 if(spans.length > 0){
		spans.each(function(){
			var node = $(this);
			var value = parseFloat(node.attr("value"));
			var b_fee =  $(".b_fee",node);
			b_fee.html(convertRmb(value));
			
		});
		var feeon = $(".feeon");
		if( feeon.length > 0 ){
			$("#feeId span").removeClass("feeon");
			feeon.addClass("feeon");
		} 
		 
	 }

	 //不用loadCart 直接调用Js方法改变
	 
	loadCart("byrate");
	// changeRateByJs();
	
	
	
	
}
function changeRateByJs(){
	$(".eachProduct").each(function(){
			
	});
	 
	alert("....1");
}
 
// 计算运费 . 检查仓库、快递是rate不是有值
function getExpressCompany(){
	var trs = $(".eachProduct");
	if(trs.length < 1){
		showMessage("请先添加数据","alert");
		return ;
	}
	var ps_id = $("#ps_id").val() * 1;
	if( ps_id ==0 ){
		showMessage("请选择仓库","alert");
		return ;
	}
	// 读取值
	var ccid =  <%= ccid%>;
	var pro_id =  <%= pro_id%>;
 
	var cart_product_types = "" , cart_pids ="", cart_quantitys ="";
	trs.each(function(){
		var node = $(this);
		cart_product_types += ","+$(".cart_product_type",node).val()*1; 
		cart_pids += "," + $(".cart_pid",node).val()*1;
		cart_quantitys += ","+ $(".inputQutity",node).val()*1;
		 
	})
	var object = {
		ps_id:ps_id,
		ccid:ccid,
		pro_id:pro_id,
		cart_product_types:cart_product_types.substr(1),
		cart_quantitys:cart_quantitys.substr(1),
		cart_pids:cart_pids.substr(1)
	}
	addDiv();
 	$.ajax({
		url:'<%= getFeeAction%>',
		dataType:'json',
		data:jQuery.param(object),
		success:function(data){
 			clearDiv();
			if(data){
				addFeeInTd(data);
				var fee = $(".fee");
				if(fee.length >0){
					selectedMinValueInFee();
				}
			 
			}else{
				showMessage("系统错误","error");
			}
		},
		error:function (XMLHttpRequest, textStatus, errorThrown){
			clearDiv();
			showMessage("系统错误","error");
			
		}
 	 	
 	})
		
}
function selectedMinValueInFee(){

	var fee = $(".fee");
	var minValue = 0 ;
	var minSpanFee  ;
	if(fee.length > 0){
		// 选取一个不等于0的最小值
		for(var index = 0 , count = fee.length ; index < count ; index++ ){
				var value = parseFloat($(".b_fee",$(fee.get(index))).html());
				if(value > 0 ){
					minValue = value;
					minSpanFee = $(fee.get(index));
					break;
				}
		}
	}
	// 找出最小值
	for(var index = 0 , count = fee.length ; index < count; index++ ){
		var value = parseFloat($(".b_fee",$(fee.get(index))).html());
		if(value < minValue && value > 0){
			minValue = value;
			minSpanFee = $(fee.get(index));
		}
	}
	selectFee(minSpanFee);
}
function convertRmb(value){
	var selected = $("#rate").val();
	
	if(selected != "1"){
		selected = parseFloat(selected);
		var vv = Math.pow(10,2);
		value = Math.round((value/selected)*vv)/vv;
		 
	}
	return value;
}
// 将信息添加到Td当中
function addFeeInTd(objs){
	if(objs.length > 0){
		var currency = $("#rate option:selected").attr("target") ;
		
		var td = $("#feeId");
		td.html("");
 		for(var index =0 , count = objs.length ; index < count ; index++ ){
 	 		//<span style="" class="ui-corner-all fee"> </span>	
 	 		
 	 		var auto = objs[index]["select_auto"];
 	 		var classese = "ui-corner-all fee";
 	 		if(auto == "false")
 	 		{
 	 			classese = "ui-corner-all noselect";
 	 		}
 	 		var span =  $("<span class='"+classese+"' scid='"+objs[index]["sc_id"]+"' value='"+parseFloat(objs[index]["shipping_fee"])+"' onclick='selectFee(this)'><span class='top'>"+objs[index]["name"] + " : <b class='b_fee'>" + convertRmb(parseFloat(objs[index]["shipping_fee"]))+"</b> "+currency+"</span><span class='bottom'>"+objs[index]["use_type"]+"</span></span>")  
 	 		td.append(span);
 	 	 }
	 
	}else{
		$("#feeId").html("&nbsp;");
		 
	}
}
function selectFee(_this){

	var node = $(_this);
	$("#feeId span").removeClass("feeon");
	node.addClass("feeon");
	var value = $(".b_fee",node).html();
 
	$("#shipping_fee").val(parseFloat(value));
	countSave();
}


 
// 重量单位的换算
function changeWeight(_this){
	var node = $(_this);
	var weight = $("#total_weight").val();
	 
	var type = $("option:selected",node).html();
	var value = node.val();
	if(value === "1"){
		$("#show_weight").html("发货重量：" + weight + type);
	}else{
		parentWeightType(weight,value,type);
	}
}
function parentWeightType(weight,value,type){
	weight = parseFloat(weight);
	var vv = Math.pow(10,2);
	if(!type){type = $("option:selected",$("#weight-type")).html();}
	var result = Math.round((parseFloat(value)*weight)*vv)/vv;
	$("#show_weight").html("发货重量：" + result + type);
}
//长度单位的换算
function changeLength(_this){
	loadCart();
}
function parentChangeLengthType(){
	$("#length_").html($("option:selected",$("#length-type")).html());
}
</script>
<style type="text/css">
<!--
.STYLE1 {
	color: #FFFFFF;
	font-weight: bold;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
 <input type="hidden" id="parent_session_id" value="<%=ps_id %>"/>
  <input type="hidden" id="ccid" value ='<%=ccid %>'/>
 <input type="hidden" id="pro_id_" value='<%=pro_id %>'/>
 <input type="hidden" id="bill_type" value='2'/>
  <div style="border:0px solid silver;padding-bottom:5px;">
 	<p style="padding:4px;">
 		<img width="16" height="16" border="0" align="absmiddle" src="../imgs/paste_email.png" />
		<a id="pasteEmail_a" href="javascript:pasteEmail()" _target="open"><span id="paste_email">粘贴邮件内容 </span></a>
	</p>
<textarea id="textareaEmail" style="border: 1px dashed #999999;color: #666666;height: 200px;width:80%;display:none;">	
</textarea>
</div>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" style="">
	  <tr>
		    <td colspan="2" align="left" valign="top">
				<table width='99%'  cellpadding='0' cellspacing='0'  style="padding-left:0px;border:1px solid silver;padding-top:10px;"> 
				 	 <tr>
				   		 <td >
								<table width='100%' border='0' cellspacing='0' cellpadding='3' style="margin:0px auto;">
							         
							        <tr>
							          <td colspan="2"   style="padding-left:15px;padding-right:15px;line-height:30px;height:30px;padding-top:5px; ">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
													  <tr>
													    <td height="35" align="left" valign="middle" style="font-size:13px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;color:#666666;border-top:#999999 1px solid;border-left:#999999 1px solid;border-right:#999999 1px solid;padding-left:10px;">
														<span style="color:#666666">发货仓库</span>
														  <select name="ps_id" id="ps_id"  onChange="getExpressCompanysByScId4Change(this.value,<%=ccid%>,<%=pro_id%>);">
														    <option value="0">请选择...</option>
														    <%
															DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();
															for ( int i=0; i<treeRows.length; i++ )
															{
																if ( treeRows[i].get("parentid",0) > 0 )
																 {
																 	continue;
																 }
															
															%>
														    <option value="<%=treeRows[i].getString("id")%>"> 
														      <%=treeRows[i].getString("title")%>
														      </option>
														    <%
															}
															%>
														    </select>
									 
															
															 
														    &nbsp;&nbsp;&nbsp;&nbsp;						
															<span style="color:#666666">递送国家</span>&nbsp;&nbsp;
															<span style="font-size:14px;color:#990099"><%= countryName %></span>
															&nbsp;&nbsp;&nbsp;&nbsp;
															<span style="color:#666666">递送城市</span>&nbsp;&nbsp;
															<span style="font-size:14px;color:#990099"><%= addressStateName%></span>
														</td>
													  </tr>
													  <tr>
														    <td height="35" align="left" valign="middle" bgcolor="#999999" style="padding-left:10px;"><span class="STYLE1">搜索商品</span>
														      <input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:12px;"/>
														     &nbsp;&nbsp;
															  <span class="STYLE1">数量</span> 
														        <input type="text" id="quantity" name="quantity"  style="color:#FF3300;width:40px;font-size:12;"/>&nbsp;
														     	<input name="Submit3" type="button" class="long-button-green" value="添加普通商品" onClick="put2Cart(<%=CartQuote.NORMAL_PRICE%>)">
														 		<input name="Submit432" type="button" class="short-short-button-redtext" value="清空" onClick="cleanCart()" />	
														     	 
																<select name="rate" id="rate" onchange="loadByRate()">
																		<option value="1" target="RMB">人民币 (RMB)</option>
																		<option selected value="<%=systemConfig.getStringConfigValue("USD")%>" target="USD" > <%=systemConfig.getStringConfigDescription("USD")%>  (USD)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("AUD")%>" target="AUD"> <%=systemConfig.getStringConfigDescription("AUD")%>  (AUD)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("EUR")%>" target="EUR"> <%=systemConfig.getStringConfigDescription("EUR")%>  (EUR)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("CAD")%>" target="CAD"> <%=systemConfig.getStringConfigDescription("CAD")%>  (CAD)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("CHF")%>" target="CHF"> <%=systemConfig.getStringConfigDescription("CHF")%>  (CHF)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("CZK")%>" target="CZK"> <%=systemConfig.getStringConfigDescription("CZK")%>  (CZK)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("GBP")%>" target="GBP"> <%=systemConfig.getStringConfigDescription("GBP")%>  (GBP)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("NOK")%>" target="NOK"> <%=systemConfig.getStringConfigDescription("NOK")%>  (NOK)  </option> 
																</select>
																<select id="weight-type" onchange="changeWeight(this);" style="width:50px;">
																	<option value="1">KG</option>
																	<option value="2.20462">Lbs</option>
																	<option value="35.274">Oz</option>
																</select>
																
																<select id="length-type" style="width:50px;" onchange="changeLength(this);">
																	<option value="1">mm</option>
																	<option value="0.03937">inch</option>
																	 
																</select>
															</td>
													    </tr>
													 
													
													  <tr>
														    <td align="left" valign="middle" style="">
																<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/batchModifyProductsQuantityFromCartQuote.action" method="post" name="cart_form" id="cart_form">
																	<div id="mini_cart_page"></div>
																</form>	
															</td>
													  </tr>
														
													  <tr>
													    <td align="right" valign="middle" style="color:#FF0000" id="action_info"></td>
													  </tr>
													 
							           </td>
							        </tr>
							        
							  </td>
							  </tr>
						</table>	
					</td>
		  		</tr>
			 	 <tr>
				    <td   align="left" valign="middle" class="win-bottom-line">
				    	<div style="float:left;">
					 		<input type="button" name="Submit2" id="fee_button" value="计算运费" class="normal-green-long" onClick="getExpressCompany();">
					 	</div>
					 	<div id="show_weight" style="float:left;border:0px solid red;height:32px;line-height:32px;font-weight:bold;">
								 
						</div>
						<div id="feeId" style="float:left;border:0px solid silver;height:32px;line-height:32px;">
							
						</div>
						<div style="float:left;">
 
							<input type="button" value="预览" class="normal-green-long" style="cursor:pointer;" onclick="gotoShow();"/>
						</div>
					</td>
			   		 
			  	</tr>
				</table>
			</td>
		</tr>
</table>
 
</body>
</html>
