<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../../include.jsp"%> 
<%@ page import="com.cwc.app.util.Config" %>
<%
long billId = StringUtil.getLong(request,"bill_id");
DBRow billRow = new DBRow();
if(billId != 0l){
	billRow = orderMgrZr.getDetailById(billId,"bill_order","bill_id");
}
long sc_id = 0l;
long ccid =   StringUtil.getLong(request,"ccid");
long pro_id = StringUtil.getLong(request,"pro_id");
double rate = StringUtil.getDouble(request,"rate");
String target = StringUtil.getString(request,"target");

String operateType = StringUtil.getString(request,"operate_type");

long ps_id = StringUtil.getLong(request,"ps_id");
String cmd = "";
String client_id = StringUtil.getString(request,"client_id");
double selected_fee	= StringUtil.getDouble(request,"selected_fee");
double weight_type = StringUtil.getDouble(request,"weight_type");
double length_type = StringUtil.getDouble(request,"length_type");
DBRow cartProducts[] = orderMgrZr.flushNew(session,client_id,sc_id,ccid,pro_id,ps_id);
 
Object object =  session.getAttribute(Config.cartQuoteSession +"_ps_id");
 
if(object != null){
	ps_id = (Long) object;
}

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
 

<script>

</script>

<style>
a.del-cart-product:link {
	color:#FF0000;
	text-decoration: none;
}
a.del-cart-product:visited {
	color: #FF0000;
	text-decoration: none;
}
a.del-cart-product:hover {
	color: #FF0000;
	text-decoration: underline;

}
a.del-cart-product:active {
	color: #FF0000;
	text-decoration: none;

}
td
{
	font-family:Arial, Helvetica, sans-serif;
	
}

.sum-data
{
	font-size:17px;
	font-weight:bold;	
}
input.inputQutity{width:30px;border:none;border-bottom:1px solid silver;background:none;}
input.inputPrice{width:80px;border:none;border-bottom:1px solid silver;background:none;}
td.fix{border-top:1px solid silver;text-align:left;text-indent:5px;}
table{border-collapse:collapse;}
thead.list th{background:#eeeeee;font-size:12px;border:1px solid silver;text-align:center;height:30px;line-height:30px;}
tr.eachProduct td{border-bottom:1px solid silver;line-height:25px;height:25px;}
td.center{text-align:center;}
</style> 
<script type="text/javascript">
 
	jQuery(function($){

		 fixValues();
	})
	function initForm(){
 		//update
 		 
 	  
 		 if('<%= billId%>' * 1 != 0){
 	 		 if('<%= cartProducts.length > 0%>' === "true"){
	 			if(parseFloat('<%= billRow.get("shipping_fee",0.0d)%>') > 0){
	 	 			 
	 			 	//$("#shipping_fee").val('<%= billRow.get("shipping_fee",0.0d)%>');
	 	 	 	}	
	 	 	  
	 	 		$("#total_discount").val('<%= billRow.get("total_discount",0.0d)%>');
	 	 	 //	$("#save").html('<%=billRow.get("save",0.0d) %>');
 	 		 }else{
 	 	 		 $("#weight").val('<%= billRow.get("total_weight",0.0d)%>');
				// 构造一个Array 
				// var span =  $("<span class='ui-corner-all fee' scid='"+objs[index]["sc_id"]+"' value='"+parseFloat(objs[index]["shipping_fee"])+"' onclick='selectFee(this)'><span class='top'>"+objs[index]["name"] + " : <b class='b_fee'>" + convertRmb(parseFloat(objs[index]["shipping_fee"]))+"</b></span><span class='bottom'>"+objs[index]["use_type"]+"</span></span>");
			
				var array = [] ;
				var  o = new Object();
				o["sc_id"] = '<%= billRow.get("sc_id",0l)%>';
				 
				 
				o["shipping_fee"] = parseFloat('<%= billRow.get("rate",0.0d)%>') * parseFloat('<%= billRow.get("total_weight",0.0d)%>');
			 
				o["convert_fee"] = '<%= billRow.get("shipping_fee",0.0d)%>';
				array.push(o);
  	 	 		addFeeInTd(array);
 	 	 	}
  	 	 	
 	   	//	$("#subtotal").html('<%=billRow.get("subtotal",0.0d) %>');
 	  	//	$("#total_weight").val('<%= billRow.get("total_weight",0.0d)%>');
 	  	//	$("#totalCost").html('<%= billRow.get("product_cost",0.0d)%>');
 	  		 
  	 	 }
     }
 	function initRate(){
		// 

		var rate = '<%= rate%>';
	 
		var rates = $(".rate");
		if(rate === "1.0"){
			 
			return ;
		}
		rates.each(function(){
			var node = $(this);
		 
			if(!node.html()){
				var value = convertRMBToRate(node.val());
				node.val(value);
			}else{
				var value = convertRMBToRate(node.html());
				node.html(value);
			}
			
		})
		
	}
	function convertRMBToRate(value){
		var rate = '<%= rate%>';
		return decimal(parseFloat(value) / rate,2);
	}
	function fixValues(){
		$(".inputQutity").each(function(){
			var node = $(this);
			var value = node.val() * 1;
			node.val(value);
		})
		$(".fixIntValue").each(function(){
			var node = $(this);
			var value = node.html() * 1;
			if('<%= length_type%>' != "1"){
				value = value * parseFloat('<%= length_type%>');
				value = decimal(value,2);
			}
			node.html(value);
		})
		// 计算每一行的小计
		parentChangeLengthType();
		
		$(".count_row_price").each(function(){
			var node = $(this);
			var tr = node.parent().parent();
			var quantity = $(".inputQutity",tr).val() * 1;
		 
			 
			var price = parseFloat($(".inputPrice",tr).val()) ;
			var value = quantity * price;
			 
			node.val(decimal(value,2));
		})
	}
	
	function decimal(num,v){
		var vv = Math.pow(10,v);
		return Math.round(num*vv)/vv;
	} 
	function fixIntValue(_this){
		var node = $(_this);
		var value = node.val();
		var fixValue = value.replace(/[^0-9]/g,'');
		node.val(fixValue * 1);
		countRowValue(node.parent().parent());
	}
	function fixFloatValue(_this,flag){
		var node = $(_this);
		var value = node.val();
		var fixValue = value.replace(/[^0-9.]/g,'');
			if(fixValue.length < 1){
				fixValue = 0;
			}
		node.val(fixValue); 
		countRowValue(node.parent().parent());
	}
	// 计算每一行的小计 
	function countRowValue(nodeTr){
		var inputPrice = $(".inputPrice",nodeTr).val() * 1;
		var inputQutity = $(".inputQutity",nodeTr).val();
		var count_row_price = $(".count_row_price",nodeTr);
		count_row_price.val(decimal(inputPrice * inputQutity,2));
		countTotalValues();
	}
	//小计输入的时候 1.计算出来不能小于 成本。2根据计算出来的值去改变实际报价的值
	function fixCountRow(_this){
		var node = $(_this);
		var value = node.val();
		var fixValue = value.replace(/[^0-9.]/g,'');
			if(fixValue.length < 1){
				fixValue = 0;
		}
		node.val(fixValue); 
		// 检查是不是小于了成本
		var parent = node.parent().parent();
		var count = $(".inputQutity",parent).val() * 1;
		var totalCost =  count * $(".unit_price",parent).val();
 		/*if(decimal(parseFloat(fixValue),2) < totalCost ){
			showMessage("输入值不能比成本还低","alert");
			在读取实际报价的值 * 数量
			node.val(decimal(count * parseFloat($(".inputPrice",parent).val()),2));
 	 		return ;
 		}*/
 		// 计算实际的报价 输入的值 /数量
 		$(".inputPrice",parent).val(decimal(fixValue/count,2)); 
 		countTotalValues();
		
	}
	function countDiscount(){
		var subtotal = parseFloat($("#subtotal").html());
		var total_discount = parseFloat($("#total_discount").val());
		var shipping_fee =parseFloat($("#shipping_fee").val());
		var save = parseFloat($("#save").val());
		$("#total_discount").val(decimal(subtotal+shipping_fee-save,2));
		 
	} 
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <form action="" id="itemsForm">
  	<input type="hidden" id="total_weight" value="0"/>
<table width="100%">
  
 	<thead class="list">
      <tr>
        <th width="22%">商品</th>
        <th width="10%">库存</th>
        <th width="13%">规格(<span id="length_">mm</span>)</th>
        <th width="10%">成本</th>
        <th width="10%">建议</th>
        <th width="10%">实际</th>
        <th width="10%">数量</th>
        <th width="10%">小计</th>
        <th width="5%"></th>
      </tr>
     </thead>
	 <%
	 	 if(cartProducts != null && cartProducts.length > 0){ 	
	 		int index = 0 ;
	 		 for(DBRow row : cartProducts){		 
	 			 String bgColor = (index % 2 == 0 ?"#FFFFFF":"#f8f8f8");
	 			 index++;	  
	 %>
	 	<tr class="eachProduct" bgcolor="<%=bgColor%>">
	 		<td style=""> 
	 		<input type="hidden" class="cart_product_type" name="product_type_<%=index %>" value='<%=row.get("cart_product_type",0) %>'/>
	 		<input type="hidden" class="cart_pid" name="pc_id_<%=index %>" value='<%=row.get("cart_pid",0) %>'>
	 		<input type="hidden" class="cart_weight" name="weight_<%=index %>" value='<%=row.get("weight",0.0f) %>' />
	 		<input type="hidden" class="unit_price" name="unit_price_<%=index %>" value='<%= row.get("unit_price",0.0f) %>'/>
	 		<input type="hidden" class="advice_price" value='<%=row.get("advice_price",0.0f) %>'/>
	 		<input type="hidden" name="unit_name_<%=index %>" value='<%=row.getString("unit_name") %>'/>
	 		<input type="hidden" name="name_<%=index %>" value='<%=row.getString("p_name") %>' />
	 	    <input type="hidden" name="item_id_<%=index %>" value='<%=row.get("bill_item_id",0l) %>' />
	 		
	 		<input type="hidden" id="totalQuantity" value=""/>
	 		
	 		<!-- 商品名称 -->
	 			<%=row.getString("p_name") %>
	 			<% 	
	 			if (row.get("cart_product_type",0)==CartQuote.UNION_STANDARD){
	 				out.println("[<a href='javascript:void(0)' onclick='customProduct("+row.getString("cart_pid")+","+ccid+",\""+row.getString("p_name")+"\",\"add\")'>定制</a>]");
	 			}
				if (row.get("cart_product_type",0)==CartQuote.UNION_CUSTOM){
	 				out.println("[<a href='javascript:void(0)' onclick='customProduct("+row.getString("cart_pid")+","+ccid+",\""+row.getString("p_name")+"\",\"mod\")'>修改</a>]");
	 			}
	 			
	 			
	 			%>
	 			<%
	 			if (row.get("cart_product_type",0)==CartQuote.UNION_CUSTOM )
	 			{
	 				 
	 				out.println("<ul class='itemDetail'>");
	 				DBRow customProductsInSet[] = customCartQuote.getFinalDetailProducts(session, StringUtil.getLong(row.getString("cart_pid")) );
	 				if(customProductsInSet != null){
		 				for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++){
		 					out.println("<li>├  "+customProductsInSet[customProductsInSet_i].getString("p_name")+" x "+customProductsInSet[customProductsInSet_i].getString("cart_quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name")+"</li>");
		 				}
	 				}
	 				out.println("</ul>");
	 			}
	 			%>
	 		</td>
	 		<td class="center">
	 			<!--  计算库存。 在上面切换发货仓库的时候下面跟着改变 -->
	 			<% 
	 				DBRow detail =	productMgr.getDetailProductProductStorageByPcid(ps_id,row.get("cart_pid",0l));
	 				out.println(detail != null ? detail.get("store_count",0.0f) : 0);
	 			%>
	 			
	 		</td>
	 		<td class="center">
	 			<!-- 商品规格 -->
	 			 <span class="fixIntValue"><%=row.get("length",0.0f) %></span> X  <span class="fixIntValue"><%=row.get("width",0.0f) %></span> X  <span class="fixIntValue"><%=row.get("heigth",0.0f) %></span>
	 		</td>
	 		<td class="center">
	 			<!-- 成本 -->
	 			<span class="rate"><%=row.getString("unit_price") %></span>&nbsp;/&nbsp;<span style="font-weight:bold;color:#f60;"><%=row.getString("unit_name") %> </span>
	 		</td>
	 		
	 		<td class="center">
	 			<!-- 建议报价 -->
	 			<span class="rate"><%=row.getString("advice_price") %></span>
	 			 
	 			
	 		</td>
	 		<td class="center">
	 			<!-- 实际报价 -->
	 			<input type="text" value='<%=row.getString("actual_price") %>' onkeyup="fixFloatValue(this)" name="actual_price_<%=index %>" class="inputPrice rate"/>
	 		</td>
	 		<td class="center">
	 			<!-- 数量 -->
	 			<input type="input" class="inputQutity" onkeyup="fixIntValue(this)" name="quantity_<%=index %>" value='<%=row.getString("cart_quantity") %>'/> &nbsp;&nbsp;<span style="color:#f60;font-weight:bold;"><%=row.getString("unit_name") %></span>
	 		</td>
	 		<td class="center">
	 		<!-- 小计 -->
	 			<input type="text" class="count_row_price rate" style="width: 80px;border:none;border-bottom:1px solid silver;background:none;"  name="amount_<%=index %>" value="" onkeyup="fixCountRow(this)"/>

	 		</td>
	 		<td width="7%" align="center" valign="middle">
				<a href="javascript:void(0)" onClick="delProductFromCart(<%=row.getString("cart_pid")%>,'<%=row.getString("p_name")%>',<%=row.getString("cart_product_type")%>)" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>	
			</td>
	 	</tr>
	 	
	 <%  
	 		 }
	 %>
	 	 
	 	<tr>
	 		<td colspan="5" style="border:1px solid silver;">
	 			<table style="float:right;margin-right:20px;">
	 				<tr>
	 					<td style="width:92%;text-align:right;">成本总计:</td>
	 					<td><span id="totalCost"></span></td>
	 				</tr>
	 				<tr>
	 					<td style="width:92%;text-align:right;">建议总计:</td>
	 					<td><span id="totalAdvice"></span></td>
	 				</tr>
	 				<tr>
	 					<td style="width:92%;text-align:right;">实际总计:</td>
	 					<td><span id="totalActualAdvice" class="rate"></span></td>
	 				</tr>
	 				<tr>
	 					<td></td><td></td>
	 				</tr>
	 			</table>
	 		</td>
	 		<td colspan="4" style="border:1px solid silver;">
	 			<table style="float:right;margin-right:20px;">
		 			<tr>
		 				<td align="right" style="width:50%;">Subtotal:</td>
		 				<td>
		 					<span id="subtotal" class="rate">0.0</span>
		 				 </td>
		 			</tr>
	 				<tr>
		 				<td align="right">Discount:</td>
		 				<td>
		 					<input type="text" id="total_discount" class="" style="width:50px;" value="0.0" onkeyup="fixFloatValuePure(this);checkDiscountIsBig();countSave();"/>&nbsp;
		 					<select>
		 						<option value="0"><%=target %></option>
		 						<option value="1">%</option>
	 						</select>
	 					 </td>
		 			</tr>
	 				<tr>
		 				<td align="right">Shipping/handling:</td>
		 				<td><input id="shipping_fee" type="text" class="rate" style="width:80px;" value="<%=selected_fee %>" onkeyup="fixFloatValuePure(this);countSave();" /></td>
		 			</tr>
		 			<tr>
		 				<td align="right">Total:</td>
		 				<td><!-- <span id="save" class="rate"></span> --> <input type="text" id="save" class="rate" style="width:50px;" onkeyup="fixFloatValuePure(this);countDiscount();"/></td>
		 				
		 			</tr>
	 			</table>
	 		</td>
	 	</tr>
	 <% 
	 	 }else{
	 %>		 
	 	<tr>
	 		<td colspan="9" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	 	</tr>	 
	 <% 		 
	 	 }
	 %>
	  </table>
	 </form>
 		<script type="text/javascript">
 		function countTotalValues(){
 	 		var totalWeight = 0 ;
 			var totalCost = 0 ; //总成本
 			var totalAdvice = 0;//总建议报价
 			var totalActualAdvice = 0; // 总实际报价
 			var tr = $(".eachProduct");
 			var totalQuantity = 0 ;
 			tr.each(function(){
 				 var node = $(this);
 			
 				 var cost = $(".unit_price",node).val();
 				 var actualAdvice = parseFloat($(".inputPrice",node).val()) ;
 				 var advice_price = $(".advice_price",node).val();
 				 var quantity = $(".inputQutity",node).val() * 1;
 				 var cart_weight =parseFloat($(".cart_weight",node).val());
 				totalQuantity += quantity;
  				 totalAdvice +=  convertRMBToRate(parseFloat(advice_price)) * quantity;
 				 totalCost +=convertRMBToRate(parseFloat(cost)) * quantity;
 				 totalWeight += cart_weight * quantity;
 				 totalActualAdvice +=  parseFloat(actualAdvice) * quantity ;
 			})
 			var nodeWeight = $("#weight");
 		 	if(totalWeight < 0.1){
 		 		totalWeight = nodeWeight.val();
 	 		 }
 			nodeWeight.val(decimal(totalWeight,2));
 			$("#total_weight").val(decimal(totalWeight,2));
 			if(tr.length > 0 ){
 				nodeWeight.attr("disabled",true);
 	 		}else{
 	 			$("#feeId span").remove();
 	 			nodeWeight.attr("disabled",false);
 	 	 	}
 			 
 			var ta = decimal(totalAdvice,2);
 			$("#totalAdvice").html(ta);
 			$("#totalCost").html(decimal(totalCost,2));
 			var t = decimal(totalActualAdvice,2);
 			$("#totalActualAdvice").html(t);
 			$("#subtotal").html(t);
 			$("#totalQuantity").val(totalQuantity);
 			countSave();
 			parentWeightType(totalWeight,'<%= weight_type%>');
 	 	 
 		}

 		function fixFloatValuePure(_this){
 			var node = $(_this);
 			var value = node.val();
 			var fixValue = value.replace(/[^0-9.]/g,'');
 			if(fixValue.length < 1){
 				fixValue = 0;
 	 		} 
 	 		
 			node.val(fixValue); 
 	 	}
 		//计算总的费用 如果是没有选择快递那么就是为红色的。有选择了快递就是绿色的
 	 	function countSave(){
 	 	 	var subtotal = parseFloat($.trim($("#subtotal").html()));
 	 	 	var totalDiscount = parseFloat($.trim($("#total_discount").val()));
 	 	 	var shippingFee = parseFloat($.trim($("#shipping_fee").val()));
			var feeSpanSelected = $("#feeId span.feeon").length < 1 ? false:true;
			 
			var t = subtotal - totalDiscount + shippingFee;
			if(feeSpanSelected){
				$("#save").css("color","green");
			}else{
				$("#save").css("color","red");
			}
			$("#save").val(decimal(t,2));
 	 	}
 	 	function checkDiscountIsBig(){ 
 	 	 	var totalDiscount = parseFloat($("#totalDiscount").val());
 	 	 	var subtotal = parseFloat($("#subtotal").html());
 	 	 	if(totalDiscount > subtotal){
 	 	 	 	showMessage("打折不能大于实际总价","alert");
 	 	 	 	$("#totalDiscount").val(0);
 	 	 	}
 	 	 	
 	 	 }
	 	 
 	
	 	 
 	   
  
 	 
 		 
 		
 		if(<%= billId%> * 1 != 0){
			// 要
			if('<%= operateType.length() < 1 %>' ==="true"){	
				// 第一次进来的时候是读取数据库的值
				initForm();
			}else if('<%= operateType%>' === "byrate"){
				 // 先转成RMB然后在转成对应的Rate数
				 var rmbTotalDiscount = (parseFloat(<%= billRow.get("rate",0.0d)%> * <%= billRow.get("total_discount",0.0d)%>));
				 var rmbTotalShipping = (parseFloat(<%= billRow.get("rate",0.0d)%> * <%= billRow.get("shipping_fee",0.0d)%>));
				 var rateTotalDiscount = convertRMBToRate(rmbTotalDiscount);
				 var rateTotalShiiping = convertRMBToRate(rmbTotalShipping);
				 $("#total_discount").val(rateTotalDiscount);
				 $("#shipping_fee").val(rateTotalShiiping);
				$("#save").val(decimal((parseFloat($("#subtotal").html()) + rateTotalDiscount +rateTotalShiiping),2));
				$("#weight").val('<%= billRow.get("total_weight",0.0d)%>');
			
				// alert(convertRMBToRate(rmbTotalDiscount));
			}else if('<%=operateType %>' === "byweight"){
				 $("#weight").val('<%= billRow.get("total_weight",0.0d)%>');
			
			}else{
				//保存打折
				//$("#total_discount").val('<%= billRow.get("total_discount",0.0d)%>');
			}
	 	}
 		initRate();
 		countTotalValues();
 		
 		</script>

</body>
</html>
