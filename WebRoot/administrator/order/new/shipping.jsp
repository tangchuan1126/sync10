<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../../include.jsp"%> 
<%@ page import="com.cwc.app.util.Config" %>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%
String cmd = StringUtil.getString(request,"cmd");
 
long billId = StringUtil.getLong(request,"bill_id");
DBRow billRow = new DBRow();
if(billId != 0l){
	billRow = orderMgrZr.getDetailById(billId,"bill_order","bill_id");
}
long ps_id = 0l;
if(billId != 0l){
	ps_id = StringUtil.getLong(request,"ps_id");
	double rate = StringUtil.getDouble(request,"rate");
	orderMgrZr.addItemsInCart(session,billId,ps_id,billRow.get("rate",1.0d));
}
long ccid =  StringUtil.getLong(request,"ccid"); 	// 国家
 
long pro_id = StringUtil.getLong(request,"pro_id"); // 省份
String countryName = StringUtil.getString(request,"country_name");
String addressStateName = StringUtil.getString(request,"address_state_name");
String getFeeAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/GetShippingFeeAction.action";
String getFeeByWeight = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/GetShippingFeeByWeightAction.action";

Object object =  session.getAttribute(Config.cartQuoteSession +"_ps_id");
 
if(object != null){
	ps_id = (Long) object;
}
 
%>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>shipping</title>

<style>
.ebayTitle
{  
	font-weight:bold;
	font-size:11px;
	color:#666666;
}
 
.save
{
	background-attachment: fixed;
	background: url(../imgs/save_quote2.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 40px;
	width: 121px;
	color: #000000;
	border: 0px;
	 
}
span.fee{font-size:12px;cursor:pointer;display:block;height:35px;line-height:35px;text-align:center;font-weight:bold;border:1px solid silver;width:120px;float:left;margin-left:5px;margin-top:2px;}
span.fee:hover{background:white;}
span.fee:visited{background:none;}
span.bottom,span.top{display:block;width:100%;text-align:center;height:17px;}
 span.top{margin-top:-8px;}
span.fee.feeon{background:#f60;color:white;}

span.noselect{font-size:12px;cursor:pointer;display:block;height:35px;line-height:35px;text-align:center;font-weight:bold;border:1px solid silver;width:140px;float:left;margin-left:5px;margin-top:2px;background:#FF33CC;}
 span.noselect:hover{background:white;}
 span.noselect:visited{background:none;}
 span.noselect.feeon{background:#f60;color:white;}
</style>






<script>
function customProduct(pid,ccid,p_name,cmd)
{
	//tb_show('定制套装',"../product/custom_product.html?cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&TB_iframe=true&height=300&width=400",false);
	//tb_show('定制套装',"../product/custom_product_quote.html?ccid="+ccid+"&cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&ps_id="+$("#ps_id").val()+"&ps_name="+$("#ps_id").getSelectedText()+"&TB_iframe=true&height=300&width=400",false);
	var come = ('<%= billId%>' * 1 != 0)? "bill_update":"";
	 
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/custom_product_quote.html?ccid="+ccid+"&cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&ps_id="+$("#ps_id").val()+"&ps_name="+$("#ps_id").getSelectedText()+"&come="+come; 
	 $.artDialog.open(uri , {title: '定制商品',width:'400px',height:'300px', lock: true,opacity: 0.3});
}

$().ready(function() {

	addAutoComplete($("#p_name"),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");
	
 
	
	//修改状态，初始化下快递列表
 initForm();
			
	//加载购物车
	 loadCart();
	//绑定购物车表单AJAX
 	//9-12
	 changeCountry();


});
function initForm(){
	var ps_id = $("#ps_id");


	var value = <%= ps_id%>;
	$("option[value='"+value+"']",ps_id).attr("selected",true);
	// 如果是已经有了购物车中的商品的话那么就把weight 设置为disabl
	// update 
	if('<%= billId%>' * 1 != 0){
		// 创建一个shipping_fee , rate , weight-type,
		 
		$("option[target='"+'<%= billRow.getString("rate_type")%>'+"']",$("#rate")).attr("selected",true);
 		 var options = $("option","#weight-type");
 		 options.attr("selected",false);
 		 for(var index = 0 , count = options.length ; index < count ; index++ ){
				var node = $(options[index]);
				if(node.html() === '<%= billRow.getString("weight_type")%>'){
					node.attr("selected",true);
				}
 	 	 }

 	 	
	}
	
	
}
//=================== 购物车 =========================

function loadCart(operate_type){
	var bill_id = '<%= billId%>';
	var node = $(".b_fee",$(".feeon"));
	 
	var selected_fee = (!node?0.0:node.parent().parent().attr("value"));
	var ccid = $("#ccid").val();
	var proId = $("#pro_id_").val();
	 
	if($.trim(ccid).length < 1 || $.trim(ccid) * 1 == 0 || proId * 1 == 0){
		showMessage("请先选择国家和地区","alert");
		
		return ;
	}
	operate_type = operate_type ?operate_type :"";
	var weight_type = $("#weight-type").val();
	var length_type = $("#length-type").val();
//var object = {operate_type:operate_type,ccid:'<%=ccid%>',pro_id:'<%=pro_id%>',rate:$("#rate").val(),'target':$("option:selected","#rate").attr("target"),'selected_fee':selected_fee,'weight_type':weight_type,'length_type':length_type};
var object = {ps_id:$("#ps_id").val(),operate_type:operate_type,bill_id:bill_id,ccid:ccid,pro_id:proId,rate:$("#rate").val(),'target':$("option:selected","#rate").attr("target"),'selected_fee':selected_fee,'weight_type':weight_type,'length_type':length_type};
 
	$.ajax({
		url: 'new/shipping_list.html',
	//url: 'new/products_list.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:jQuery.param(object),
		beforeSend:function(request){
			$("#action_info").text("计算中......");
		},
		error: function(){
			$("#action_info").text("计算失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
			$("#action_info").text("");
		}
	});
}

 
function put2Cart(price_type){
	var p_name = $("#p_name").val();
	var quantity = $("#quantity").val();
	var ps_id = $("#ps_id").val();
	var sc_id = $("#sc_id").val();
	var ccid= $("#ccid").val();
	var price_type = price_type;
	
	if (ps_id==0){
		showMessage("请选择发货仓库","alert");
	}
	else if (p_name==""){
		showMessage("请填写商品名称","alert");
	}
	else if (quantity==""){
		showMessage("请填写数量","alert");
	}
	else if ( !isNum(quantity) ){
		showMessage("请正确填写数量","alert");
	}
	else if (quantity<=0){
		showMessage("数量必须大于0","alert");
	}
	else{
		var para = "p_name="+p_name+"&quantity="+quantity+"&ps_id="+ps_id+"&ccid="+ccid+"&price_type="+price_type;

		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/put2CartQuote.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			error: function(){
				loadCart("add");
				$("#action_info").text("商品添加失败！");
			},
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException"){
					showMessage("商品不存在，不能添加到订单！","alert");
				}
				else if (msg=="ProductNotProfitException"){
					showMessage("商品没有设置毛利，不能销售","alert");
				}
				else if (msg=="ProductNotCreateStorageException")
				{
					showMessage("["+$("#ps_id").getSelectedText()+"] 不发 ["+p_name+"]","alert");
				}
				else if (msg=="ok"){
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("");
					
					loadCart("add");
					removeFeeSpan();
				}
				else{
					loadCart("add");
					alert(msg);
				}
			}
		});	
	}
}


function delProductFromCart(pid,p_name,product_type)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/removeProductFromCartQuote.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"pid="+pid+"&product_type="+product_type,
					
			error: function(){
				$("#action_info").text("商品删除失败！");
				loadCart("delete");
			},
			
			success: function(msg){
				$("#action_info").text("");
				removeFeeSpan();
				loadCart("delete");
			}
		});	
	}
}


function cleanCart()
{
	if (confirm("确定清空购物车？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/cleanCartQuote.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			
			error: function(){
				$("#action_info").text("清空购物车失败！");
				loadCart("delete");
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart("delete");
			}
		});	
	}
}
 
 
 


function isNum(keyW)
{
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
//=================== 购物车 =========================


var cartIsEmpty = <%=cart.isEmpty(session)%>;

function checkCart()
{
	if (cartIsEmpty)
	{	
		alert("请先添加商品");
		return(false);
	}
	else
	{
		return(true);
	}
}

 



function closeTBWin(){
	$.artDialog.close();
}



 
 

function modifyCartManagerDiscount()
{
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/modifyCartManagerDiscount.action',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:"manager_discount="+$("#manager_discount").val(),
		
		beforeSend:function(request){
		},
		
		error: function(){
			alert("网络错误，请重试！");
		},
		
		success: function(html){
			loadCart("delete");
		}
	});
}

function getExpressCompanysByScId4Change(ps_id){
		var session_ps_id =  $("#parent_session_id").val();
		if(session_ps_id != "0" && ps_id != "0"){
			if(ps_id !=session_ps_id ){
				 // 表示的是 改变了ps_id
				 var flag = window.confirm("确定改变发货仓库?")
				 if(flag){
					 //改变Session中的值。然后让list 的刷新.改变过后是否计算了运费 ，如果是的话就要把Span去掉
					 $.ajax({
						 url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/UpdateSessionPsIdAction.action',
						 dataType:'text',
						 data:jQuery.param({ps_id:ps_id}),
						 success:function(data){
						 	if(data === "success"){
						 		$("#parent_session_id").val(ps_id);
						 		 loadCart("add");
						 		 removeFeeSpan();
							}
						 }
					 })
					
				}else{
					// 不改变里面 的值
					$("option[value='"+session_ps_id+"']",$("#ps_id")).attr("selected",true);
				}
			} 
		}else{
			$("#parent_session_id").val(ps_id);
		}  
}
// 移除计算出来的运费
function removeFeeSpan(){
	$("#feeId span").remove();
}
 
// 计算运费 . 检查仓库.如果是输入了重量的数值那么就要 用另外的Action去进行计算


function getExpressCompany(){
	var trs = $(".eachProduct");
	var weight = parseFloat($("#weight").val());
	
	if(trs.length < 1 && weight <= 0){
		showMessage("请先添加数据或者是输入重量","alert");
		return ;
	}
	var ps_id = $("#ps_id").val() * 1;
	if( ps_id ==0 ){
		showMessage("请选择仓库","alert");
		return ;
	}
	// 读取值
	var ccid =  $("#ccid").val();
	var pro_id =  $("#pro_id_").val();
 
	var cart_product_types = "" , cart_pids ="", cart_quantitys ="";
	trs.each(function(){
		var node = $(this);
		cart_product_types += ","+$(".cart_product_type",node).val()*1; 
		cart_pids += "," + $(".cart_pid",node).val()*1;
		cart_quantitys += ","+ $(".inputQutity",node).val()*1;
		 
	})
	var object ;
	var  uri ;
	if(weight && weight > 0 && trs.length < 1){
		uri = '<%= getFeeByWeight%>';
		object = {
				ps_id:ps_id,
				ccid:ccid,
				pro_id:pro_id,
				weight: weight
			}
	}else{
		uri = '<%= getFeeAction%>';
		object = {
				ps_id:ps_id,
				ccid:ccid,
				pro_id:pro_id,
				cart_product_types:cart_product_types.substr(1),
				cart_quantitys:cart_quantitys.substr(1),
				cart_pids:cart_pids.substr(1)
			}
	}
	addDiv();
	
 	$.ajax({
		url:uri,
		dataType:'json',
		data:jQuery.param(object),
		success:function(data){
 			clearDiv();
			if(data){
				addFeeInTd(data);
				var fee = $(".fee");
				if(fee.length > 0 ){
					// 选出不等于0 的。最小数
					selectedMinValueInFee();
					 
				}
			}else{
				showMessage("系统错误","error");
			}
		},
		error:function (XMLHttpRequest, textStatus, errorThrown){
			clearDiv();
			showMessage("系统错误","error");
			
		}
 	 	
 	})
		
}
function selectedMinValueInFee(){

	var fee = $(".fee");
	var minValue = 0 ;
	var minSpanFee  ;
	if(fee.length > 0){
		// 选取一个不等于0的最小值
		for(var index = 0 , count = fee.length ; index < count ; index++ ){
				var value = parseFloat($(".b_fee",$(fee.get(index))).html());
				if(value > 0 ){
					minValue = value;
					minSpanFee = $(fee.get(index));
					break;
				}
		}
	}
	// 找出最小值
	for(var index = 0 , count = fee.length ; index < count; index++ ){
		var value = parseFloat($(".b_fee",$(fee.get(index))).html());
		if(value < minValue && value > 0){
			minValue = value;
			minSpanFee = $(fee.get(index));
		}
	}
	selectFee(minSpanFee);
}

function loadByRate(){
	// 如果是已经计算了运费的。在切换国家的时候把 。运费重新计算
	// 如果是有一个选中的时候那么就要改变计算save值
	var spans = $(".fee",$("#feeId"));
	 if(spans.length > 0){
		spans.each(function(){
			var node = $(this);
			var value = parseFloat(node.attr("value"));
			var b_fee =  $(".b_fee",node);
			b_fee.html(convertRmb(value));
			
		});
		var feeon = $(".feeon");
		if( feeon.length > 0 ){
			$("#feeId span").removeClass("feeon");
			feeon.addClass("feeon");
		} 
		 
	 }
	loadCart("byrate");
	
}
// 将信息添加到Td当中
function addFeeInTd(objs){
	if(objs.length > 0){
		var currency = $("#rate option:selected").attr("target") ;
		
		var td = $("#feeId");
		td.html("");
 		for(var index =0 , count = objs.length ; index < count ; index++ ){
 	 		//<span style="" class="ui-corner-all fee"> </span>	
 	 		
 	 		var auto = objs[index]["select_auto"];
 	 		var classese = "ui-corner-all fee";
 	 		if(auto == "false")
 	 		{
 	 			classese = "ui-corner-all noselect";
 	 		}
 	 		var span =  $("<span class='"+classese+"' scid='"+objs[index]["sc_id"]+"' value='"+parseFloat(objs[index]["shipping_fee"])+"' onclick='selectFee(this)'><span class='top'>"+objs[index]["name"] + " : <b class='b_fee'>" + convertRmb(parseFloat(objs[index]["shipping_fee"]))+"</b> "+currency+"</span><span class='bottom'>"+objs[index]["use_type"]+"</span></span>")  
 	 		td.append(span);
 	 	 }
	 
	}else{
		$("#feeId").html("&nbsp;");
		 
	}
}

function addFeeInTd2(objs){
	if(objs.length > 0){
	var currency = $("#rate option:selected").attr("target") ;
		var td = $("#feeId");
		td.html("");
 		for(var index =0 , count = objs.length ; index < count ; index++ ){
 	 		//<span style="" class="ui-corner-all fee"> </span>	
 	 		if(objs[index]["name"] != "error"){
 	 	 		var fixValue = objs[index]["convert_fee"]?objs[index]["convert_fee"]:convertRmb(parseFloat(objs[index]["shipping_fee"]));
 	 	 		var fixClass = objs[index]["convert_fee"]?"feeon":"";
 	 	 		var span =  $("<span class='ui-corner-all fee "+ fixClass +"' scid='"+objs[index]["sc_id"]+"' value='"+parseFloat(objs[index]["shipping_fee"])+"' onclick='selectFee(this)'><span class='top'>"+(objs[index]["name"]?objs[index]["name"]+"  :":"")+ "  <b class='b_fee'>" + fixValue+"</b>"+currency+"</span><span class='bottom'>"+(objs[index]["use_type"]?objs[index]["use_type"]:"")+"</span></span>");
  	 			td.append(span);
 	 		}
 	 	 }
	 
	}else{
		$("#feeId").html("&nbsp;");
		 
	}
}

function convertRmb(value){
	var selected = $("#rate").val();
	
	if(selected != "1"){
		selected = parseFloat(selected);
		var vv = Math.pow(10,2);
		value = Math.round((value/selected)*vv)/vv;
		 
	}
	return value;
}

function selectFee(_this){

	var node = $(_this);
	$("#feeId span").removeClass("feeon");
	node.addClass("feeon");
	var value = $(".b_fee",node).html();
	$("#shipping_fee").val(parseFloat(value));
	
	
	countSave();
}
function fixWeightFloatValue(_this){
	var node = $(_this);
	var value = node.val();
	var fixValue = value.replace(/[^0-9.]/g,'');
		if(fixValue.length < 1){
			fixValue = 0.0;
		}
	//node.val(fixValue); 
	 
}
function setProId(){
	removeAfterProIdInput();
	var node = $("#contryArea");
	var value = node.val();
 
	if(value*1 != 10929){
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:value},
				function callback(data){ 
					$("#proId").attr("disabled",false);
					$("#proId").clearAll();
					$("#proId").addOption("请选择......","0");
					if (data!=""){
						$.each(data,function(i){
							$("#proId").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					if (value>0){
						$("#proId").addOption("手工输入","-1");
					}
				}
			);
		 
			 
	 }
}

 
function address(){
	 var node = $("#proId");
	 var value = node.val();

	 if(value*1 == -1){
		 addInputAfterProid(node);
	 }else{
		 removeAfterProIdInput();
	}
}
function addInputAfterProid(node){
	 var input  = "<input type='text'   id='address_state_input' />";
	 $(input).insertAfter(node);
}
function removeAfterProIdInput(){
	$("#address_state_input").remove();
}
function selectedArea(){
	var areaCountry = $("#areaCountry");
	if(areaCountry.css("display") === "none"){
		areaCountry.css("display","block");
		
		return ;
	}else{
		var cuntryArea = $("#contryArea").val();
		var proId = $("#proId").val();
		var cuntryName = $("option:selected",$("#contryArea")).html();
		var proName = "";
		if(proId *1 == 0){
			showMessage("请选择或者输入省份!","alert");
			return ;
		}
		if(proId * 1 == -1){
			proName = $("#address_state_input").val();
		}else{
			proName = $("option:selected",$("#proId")).html();
		}
		 
		if($.trim(proName).length < 1){
			showMessage("请输入省份!","alert");
			return ;
		}
		 
		 //把显示或者是隐藏的值都替换掉
		$("#ccid").val(cuntryArea);
		
		$("#pro_id_").val(proId);
	 
	 	$("#showCountryName").html(cuntryName);
	 	$("#showProName").html(proName);
		areaCountry.css("display","none");
		removeFeeSpan();
		loadCart();
	
	}
}
 
function ccIdAndProIdError(){
	$("#ccid").val("");
	$("#pro_id").val("");
}
//重量单位的换算
function changeWeight(_this){
	var node = $(_this);
	var weight = $("#total_weight").val();
	var weightNode = $("#weight");
	if(parseFloat(weight) <= 0){
		weight = weightNode.val();
		$("#total_weight").val(weight);
	}
	var type = $("option:selected",node).html();
	var value = node.val();
	if(value === "1"){
		weightNode.val(weight);
		$("#weight_type").html(type);
	}else{
		parentWeightType(weight,value,type);
	}
}
function parentWeightType(weight,value,type){
	weight = parseFloat(weight);
	var vv = Math.pow(10,2);
	if(!type){type = $("option:selected",$("#weight-type")).html();}
	var result = Math.round((parseFloat(value)*weight)*vv)/vv;
	$("#weight").val(result);
	$("#weight_type").html(type);
}
//长度单位的换算
function changeLength(_this){
	loadCart();
}
function parentChangeLengthType(){
	$("#length_").html($("option:selected",$("#length-type")).html());
}
function changeCountry(){
	// 如果已经是在编辑的时候那么就return 
	var spanCountry = $("#spanCountry");
 
	if($("#appendCountry").length> 0){
		return ;
	}
	var options =  $("#ccid_hidden").html();
	var value = $.trim($("#showCountryName").html());
	var s = options;
	var objects = $(s).attr("selected",false);
	var id = 0;
 	for(var index = 0 , count = objects.length ; index < count ; index++ ){
 	 	var node = $(objects[index]);
		 if(node.html() === value){
			 node.attr("selected",true);
			 id = (node.attr("value"));
			 break;
		 };
   }
	var select = $("<select style='width:180px;' id='appendCountry' onchange='appendCountryChange(this)'></select>").append(s);
	$("#showCountryName").html(select);
	$("option:[value='"+id+"']",select).attr("selected",true);
	//创建城市的下拉框   然后判断城市的值是不是存在。如果不存在那么就要生成Input 把值写在limian
 	var showProName = $("#showProName").html()+"";
	// 9-12
 	showProName =  (showProName == "null" ? "" : showProName);
	getProSelected(id,showProName);
}
	function appendCountryChange(){
		var value = $("#appendCountry").val();
		getProSelected(value,'',true);
	}
	function getProSelected(value,showProName,flag){
		if(value*1 != 10929){
			$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
					{ccid:value},
					function callback(data){ 
						var select = $("<select id='appendSelectedPro' onchange='appendProChange(this)'></select>");
						select.addOption("请选择......","0");
						if (data!=""){
							$.each(data,function(i){
								select.addOption(data[i].pro_name,data[i].pro_id);
							});
						}
						if (value>0){
							select.addOption("手工输入","-1");
						}
						$("#showProName").html(select);
						if(!flag){
							selectedPro(select,showProName);
						}
					}
				);	 
		 }
	}
	function appendProChange(_this){
		var node = $(_this);
		var value = node.val();
		if(value === "-1"){
			$("#showProName").append("&nbsp;<input type='text' value='' onblur='setProAndCountry(this)' id='appendInputPro'/>");
		}else if(value === "0"){
			var appendInputPro = $("#appendInputPro");
			appendInputPro.length > 0 && appendInputPro.remove();
		}else{
			// 如果是选中一个 省份,那么就要让值重新显示在上面
			showResult("selected");
			
		}
		 
	}
	// 让省份 选中。如果是有的话,没有的时候那么就生成一个<input >
	function selectedPro(select,showProName){
		var options = $("option" ,select);
		var id =  -1 ;
		for(var index = 0 , count = options.length ; index < count ;index++ ){
			var node = $(options[index]);
			if(node.html() === showProName){
				id = node.attr("value");
				break;
			}
		}
		$("option:[value='"+id+"']",select).attr("selected",true);
		if(id == -1){
			$("#showProName").append("&nbsp;<input type='text' id='appendInputPro' onblur='setProAndCountry(this)' value='"+showProName+"' />")
		}
	}
	function setProAndCountry(_this){
		var input  = $(_this);
		if($.trim(input.val()).length <= 0 ){
			showMessage("请先输入省份","alert");
			return ;
		}else{
			
			showResult("input")
		}
	}
	function showResult(target){
		// 显示Value	
		var appendCountryNode = $("#appendCountry");
		var appendSelectedNode = $("#appendSelectedPro");
		var appendCountry = $("option:selected",appendCountryNode).html();
		var countryId = appendCountryNode.val();
		var proId = 0 ;
		var appendSelectedPro ;
		if(target === "selected"){
			// selected
			appendSelectedPro = $("option:selected",appendSelectedNode).html()
			proId = appendSelectedNode.val();
		}else{
			// input 
			appendSelectedPro = $("#appendInputPro").val();
			proId = -1;
		}
		$("#ccid").val(countryId);
		$("#pro_id_").val(proId);
		// 9-12
	 	//$("#showCountryName").html(appendCountry);
	 	//$("#showProName").html(appendSelectedPro);
		loadCart("byweight");
		setAddBillJspValue(countryId,proId,appendSelectedPro);
	};
	function setAddBillJspValue(countryId,proId,html){
		// 同步到add_bill.jsp
		$("option[value='"+countryId+"']",$("#ccid_hidden")).attr("selected",true);
		// 选中 Pro
		setPro_id(false,proId+"",html);
	}
</script>
<style type="text/css">
<!--
.STYLE1 {
	color: #FFFFFF;
	font-weight: bold;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
 <input type="hidden" id="parent_session_id" value="<%=ps_id %>"/>
 <input type="hidden" id="ccid" value ='<%=ccid %>'/>
 <input type="hidden" id="pro_id_" value='<%=pro_id %>'/>
  <input type="hidden" id="bill_type" value='3'/>
<div style="border:0px solid silver;padding-bottom:5px;">
 	<p style="padding:4px;">
 		<img width="16" height="16" border="0" align="absmiddle" src="../imgs/paste_email.png" />
		<a id="pasteEmail_a" href="javascript:pasteEmail()" _target="open"><span id="paste_email">粘贴邮件内容 </span></a>
	</p>
<textarea id="textareaEmail" style="border: 1px dashed #999999;color: #666666;height: 200px;width:80%;display:none;">	
</textarea>
</div>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" style="">
	  <tr>
		    <td colspan="2" align="left" valign="top">
				<table width='99%'  cellpadding='0' cellspacing='0'  style="padding-left:0px;padding-top:10px;"> 
				 	 <tr>
				   		 <td >
								<table width='100%' border='0' cellspacing='0' cellpadding='3' style="margin:0px auto;">
							         
							        <tr>
							          <td colspan="2"   style="line-height:30px;height:30px;padding-top:5px; ">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
													  <tr>
														<td height="35" align="left" valign="middle" style="font-size:13px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;color:#666666;border:#999999 1px solid;padding-left:10px;">
														 	<span style="float:left;display:block;">
																<span style="color:#666666">发货仓库</span>
																  <select name="ps_id" id="ps_id"  onChange="getExpressCompanysByScId4Change(this.value);">
																    <option value="0">请选择...</option>
																    <%
																	DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();
																	for ( int i=0; i<treeRows.length; i++ )
																	{
																		if ( treeRows[i].get("parentid",0) > 0 )
																		 {
																		 	continue;
																		 }
																	
																	%>
																    <option value="<%=treeRows[i].getString("id")%>"> 
																      <%=treeRows[i].getString("title")%>
																      </option>
																    <%
																	}
																	%>
																    </select>
											  						&nbsp;&nbsp;&nbsp;&nbsp;
																	<span style="color:#666666;margin-left:5px;cursor:pointer;" id="spanCountry" onclick="changeCountry()">递送国家</span>
																	<span style="font-size:14px;color:#990099" id="showCountryName"><%= countryName %></span>
																	&nbsp;&nbsp;&nbsp;&nbsp;
																	<span style="color:#666666;margin-left:5px;">递送城市</span>&nbsp;&nbsp;
																	<span style="font-size:14px;color:#990099" id="showProName"><%= addressStateName%></span>
																 
																	</span>
																	<span >
																		
																		<span id="areaCountry" style="display:none;float:left;margin-left:10px;">
																			<%
																				DBRow countrycode[] = orderMgr.getAllCountryCode();
																				String selectBg="#ffffff";
																				String preLetter="";
																			%>
																	      	 <select id="contryArea" onChange="setProId();">
																		 	 	<option value="0">请选择...</option>
																				  <%
																				  for (int i=0; i<countrycode.length; i++){
																				  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
																						if (selectBg.equals("#eeeeee")){
																							selectBg = "#ffffff";
																						}else{
																							selectBg = "#eeeeee";
																						}
																					}  	
																					preLetter = countrycode[i].getString("c_country").substring(0,1);
																				  %>
																		  		  <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
																				  <%
																				  }
																				%>
															     			 </select>
															     			 <select  disabled id="proId" onchange="address()" style="margin-right:5px;"></select>
																		</span>
																		 
																		
																	</span>
																</td>
															</tr>
														</table>
													</td>
														
														 
												</tr>
													  <tr>
														    <td height="35" align="left" valign="middle" bgcolor="#999999" style="padding-left:10px;"><span class="STYLE1">搜索商品</span>
														      <input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:12px;"/>
														     &nbsp;&nbsp;
															  <span class="STYLE1">数量</span> 
														        <input type="text" id="quantity" name="quantity"  style="color:#FF3300;width:40px;font-size:12;"/>&nbsp;
														     	<input name="Submit3" type="button" class="long-button-green" value="添加普通商品" onClick="put2Cart(<%=CartQuote.NORMAL_PRICE%>)">
																<input name="Submit432" type="button" class="short-short-button-redtext" value="清空" onClick="cleanCart()" />	
																<select name="rate" id="rate" onchange="loadByRate()">
																		<option value="1" target="RMB">人民币 (RMB)</option>
																		<option selected value="<%=systemConfig.getStringConfigValue("USD")%>" target="USD" > <%=systemConfig.getStringConfigDescription("USD")%>  (USD)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("AUD")%>" target="AUD"> <%=systemConfig.getStringConfigDescription("AUD")%>  (AUD)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("EUR")%>" target="EUR"> <%=systemConfig.getStringConfigDescription("EUR")%>  (EUR)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("CAD")%>" target="CAD"> <%=systemConfig.getStringConfigDescription("CAD")%>  (CAD)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("CHF")%>" target="CHF"> <%=systemConfig.getStringConfigDescription("CHF")%>  (CHF)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("CZK")%>" target="CZK"> <%=systemConfig.getStringConfigDescription("CZK")%>  (CZK)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("GBP")%>" target="GBP"> <%=systemConfig.getStringConfigDescription("GBP")%>  (GBP)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("NOK")%>" target="NOK"> <%=systemConfig.getStringConfigDescription("NOK")%>  (NOK)  </option> 
																		
																</select>
																<select id="weight-type" onchange="changeWeight(this);" style="width:50px;">
																			<option value="1">KG</option>
																			<option value="2.20462">Lbs</option>
																			<option value="35.274">Oz</option>
																		</select>
																		
																		<select id="length-type" style="width:50px;" onchange="changeLength(this);">
																			<option value="1">mm</option>
																			<option value="0.03937">inch</option>
																		</select>
															</td>
													    </tr>
													 
													
													  <tr>
														    <td align="left" valign="middle" style="">
																<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/batchModifyProductsQuantityFromCartQuote.action" method="post" name="cart_form" id="cart_form">
																	<div id="mini_cart_page"></div>
																</form>	
															</td>
													  </tr>
													  <tr>
													    <td align="right" valign="middle" style="color:#FF0000" id="action_info"></td>
													  </tr>
							           </td>
							        </tr>
							  </td>
							  </tr>
						</table>	
					</td>
		  		</tr>
			 	 <tr>
				    <td   align="left" valign="middle" class="win-bottom-line">
				    	<div style="float:left;">
					 		<input type="button" name="Submit2" id="fee_button" value="计算运费" class="normal-green-long" onClick="getExpressCompany();">
					 	</div>
					 	<div id="show_weight" style="float:left;border:0px solid red;height:32px;line-height:32px;font-weight:bold;">
							发货重量:&nbsp;<input type="text" id="weight"  value="<%= billRow.get("total_weight",0.0d) %>" style="width:50px;" onkeyup="fixWeightFloatValue(this);removeFeeSpan();"/> <span id="weight_type"> Kg </span> 
						</div>
						<div id="feeId" style="float:left;border:0px solid silver;height:32px;line-height:32px;">
							
						</div>
						<div style="float:left;">
 
							<input type="button" value="预览" class="normal-green-long" style="cursor:pointer;" onclick="gotoShow()"/>
						</div>
					</td>
			  	</tr>
			  	 
				</table>
			</td>
		</tr>
</table>
 
</body>
</html>
