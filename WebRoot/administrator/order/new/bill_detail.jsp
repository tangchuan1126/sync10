<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.List,com.cwc.app.key.PaymentMethodKey,java.util.ArrayList,com.cwc.app.key.BillTypeKey,com.cwc.app.key.InvoiceStateKey,com.cwc.app.key.BillStateKey" %>

<%@ include file="../../../include.jsp"%>
<%
	long billId = StringUtil.getLong(request,"bill_id");
long qoid = 0l;
boolean isPayForOrder = false;
boolean isEbay = false;
DBRow billOrder = null;
DBRow billAccount = null;
if(billId != 0l){
	billOrder = orderMgrZr.getDetailById(billId,"bill_order","bill_id");
}
DBRow[] billOrderItem =  billMgrZr.getItemsByBillId(billId);
// 这里进行判断如果是PayForOrder 或者是 PayforShippingFee 应该特殊的处理
if(billOrder != null){
	isEbay =  billOrder.get("bill_type", 0) == BillTypeKey.ebay ? true : false;
	if(billOrderItem.length < 1){
		if(billOrder.get("bill_type",0) == BillTypeKey.shipping){
	billOrderItem = new DBRow[1];
	DBRow shippingItem = new DBRow();
	shippingItem.add("quantity",1.0d);
	shippingItem.add("unit_name","");
	shippingItem.add("name","For Shipping Fee");
	billOrderItem[0] = shippingItem;
		}
		if(billOrder.get("bill_type",0) == BillTypeKey.order){
	isPayForOrder = true;
	billOrderItem = new DBRow[1];
	DBRow shippingItem = new DBRow();
	shippingItem.add("quantity",1.0d);
	shippingItem.add("unit_name","");
	shippingItem.add("name","Pay For Order["+billOrder.get("porder_id",0l)+"]");
	shippingItem.add("actual_price",billOrder.get("order_fee",0.0d));
	shippingItem.add("amount",billOrder.get("order_fee",0.0d));
	billOrderItem[0] = shippingItem;
		}
	}
}
billAccount = adminMgr.getDetailAdmin(billOrder.get("create_adid",0l));
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
long loginAdid = adminLoggerBean.getAdid(); 
int limitRowLength = 13;

float mm_pix = 3.78f;

float left_float = 17f;
float top_float = 17f;

float print_range_width = 215f*mm_pix;
float print_range_height = 297f*mm_pix;
float print_range_left = left_float*mm_pix;
float print_range_top = top_float*mm_pix;
BillStateKey billState = new BillStateKey();
InvoiceStateKey invoiceStateKey = new InvoiceStateKey();
String sendInvoiceAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/SendInvoiceByBillIdAction.action";
String updateInvoiceAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/UpdateInvoiceByBillIdAction.action";
String createInvoiceAction =  ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/CreateInvoiceByBillIdAction.action";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>报价单</title>
<link href="../../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<style type="text/css" media="all">
@import "../../js/thickbox/global.css";
@import "../../js/thickbox/thickbox.css";
</style>
<link rel="stylesheet" type="text/css" href="../../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link rel="alternate stylesheet" type="text/css" href="../../js/thickbox/1024.css" title="1024 x 768" />
<script src="../../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../../js/thickbox/global.js" type="text/javascript"></script>

<link type="text/css" href="../../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script> 
 
 
function updateBill(bill_type,billId){
	 var uri 
	 if(bill_type == '<%= BillTypeKey.order%>'){
		 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/convert_order_to_bill.html?bill_id="+ billId; 
	 }else{
		 var billTypeInt = bill_type * 1;
		 var update_target ;
		 if(billTypeInt * 1 == 1){
			 update_target = "li_ebay";
		 }
		 if(billTypeInt * 1 == 2){
			 update_target = "li_products";
		 }
		 if(billTypeInt * 1 == 3){
			 update_target = "li_shipping_";
		 }
		 if(billTypeInt * 1 == 4){
			 update_target = "li_warranty";
		 }
		 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/add_bill.html?bill_id="+ billId + "&update_target="+update_target; 
	 }
	 $.artDialog.open(uri , {title: "[ "+billId+" ]账单修改",width:'1100px',height:'530px', lock: true,opacity: 0.3,fixed: true,cancel: function () {
		 
		 $.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/cleanCartQuote.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				error: function(){
					showMessage("clear session failed","error");
				},
				success: function(msg){
				 
				}
			});	
	    }});
}

function exportPDF(billId)
{
	window.location = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/bill/ExportPDFBillAction.action?bill_id="+billId;
}
function emailBill(billId){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/new/bill_mail.html?bill_id="+billId; 
	 $.artDialog.open(uri , {title: '发送账单',width:'850px',height:'530px', lock: true,opacity: 0.3});
}
function refreshWindow(){
	window.location.reload();
}


function createInvoice(billId,cmd){
	 var title =(cmd === "create") ? "创建":"创建并且发送";
	 lockWindow('warning',title+"Invoice <span style='color:green;'>[ "+billId+" ]</span>");
	 $.ajax({
		url:'<%= createInvoiceAction%>' + "?bill_id=" + billId +"&cmd="+cmd,
		dataType:'json',
		error:function(XMLHttpRequest, textStatus, errorThrown){
			$.artDialog.list['infoDialog'] && $.artDialog.list['infoDialog'].close();
			 showMessage("系统错误.","error");
		},
		success:function(data){
			$.artDialog.list['infoDialog'] && $.artDialog.list['infoDialog'].close();
			 if(data && data.flag){
				 if(data.flag === "success"){
					 showMessage(title+"Invoice成功.","succeed");
					 window.setTimeout(function(){window.location.reload();},1500);
				 
				 }else{
					 lockWindow('error',data.message);
				}
			 }else{
				 showMessage("系统错误.","error");
			}
		}	
	 })
	 
}
function updateInvoice(billId){
	 lockWindow('warning',"正在更新Invoice <span style='color:green;'>[ "+billId+" ]信息</span>");
		$.ajax({
			url:'<%= updateInvoiceAction%>' + "?bill_id="+billId,
			dataType:'json',
			error:function(XMLHttpRequest, textStatus, errorThrown){
				$.artDialog.list['infoDialog'] && $.artDialog.list['infoDialog'].close();
			 	alert("系统错误.");
			},
			success:function(data){
				$.artDialog.list['infoDialog'] && $.artDialog.list['infoDialog'].close();
				if(data){
					if(data.flag === "success"){
						alert("更新Invoice成功.");
						window.setTimeout(function(){window.location.reload();},1500);
			
					}else{
						lockWindow('error',data.message);
					}
				}else{
					alert("系统错误请稍后再试");
				}
		 	}
		})
}
function lockWindow(type,text){
	 $.artDialog({
		    id:'infoDialog',
		    lock: true,
		    background: '#808080', // 背景色
		    opacity: 0.87,	// 透明度
		    content: text || '请稍等.......'  ,
		    icon: type || 'warning' 
		});
}
 
</script>

<style>
 
td
{
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
}
.quote_to
{
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	color:#000000;
}

.quote_normal
{
	font-size:13px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	color:#C19859;
}

.quote_text
{
	font-size:10px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:normal;
	color:#000000;
}

.quote_thanks
{
	font-size:17px;
	font-family:Franklin Gothic Medium;
	font-weight:bold;
	color:#C19859;
	font-style: italic;
}

form
{
	padding:0px;
	margin:0px;
}

.export-pdf {
	background-attachment: fixed;
	background: url(../../imgs/product/export_pdf_button.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 42px;
	width: 114px;
	color: #000000;
	border:0px;
	font-size:15px;
	font-weight:normal;
	font-family:宋体;
	margin-left:5px;
	margin-right:5px;
	padding-left:40px;
	padding-top:10px;
}

.sendmail {
	background-attachment: fixed;
	background: url(../../imgs/product/sendemail.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 42px;
	width: 114px;
	color: #000000;
	border:0px;
	font-size:15px;
	font-weight:normal;
	font-family:宋体;
	margin-left:5px;
	margin-right:5px;
	padding-left:40px;
	padding-top:10px;
}

</style>

<style media=print>  
.noPrint{display:none;}<!--用本样式在打印时隐藏非打印项目-->  
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/recordOrderQuote.action" method="post" name="record_form" id="record_form">
<input type="hidden" name="qoid" >
<input type="hidden" name="ps_id">
<em><input type="hidden" name="backurl" value='<%=StringUtil.getCurrentURL(request)%>'></em>	
<input type="hidden" name="sc_id" >
<input type="hidden" name="ccid" >
<input type="hidden" name="pro_id" >
<input type="hidden" name="client_id">
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" style="padding-left:20px;padding-top:20px;"><br>
    <table width="750" height="950" border="0" cellpadding="0" cellspacing="1" bgcolor="#997339" id="quote_table">
  <tr>
    <td align="left" valign="top" bgcolor="#FAF7F2" style="padding-left:29px;padding-top:30px;"><table id="quote_title" width="690" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="70%" height="50" align="left" valign="middle" style="border-bottom:#C19859 solid 1px"><img src="../../imgs/system_logo.gif" width="192" height="20"></td>
          <td width="30%" align="right" valign="middle" style="font-size:30px;font-family:Franklin Gothic Medium;color:#C19859;border-bottom:#C19859 solid 1px">Invoice:<%=billId%></td>
        </tr>
      <tr>
        <td colspan="2" align="right" valign="middle" class="quote_to"><br>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="50%" align="left" valign="top" class="quote_to">
			 	<%
			 		if(billMgrZr.hasAuthority(adminLoggerBean.getAdid(),billOrder.get("create_adid",0l))){
			 	%>
			 	 <a href="javascript:updateBill(<%= billOrder.get("bill_type",0)%>,<%= billOrder.get("bill_id",0) %>)"><img src="../../imgs/application_edit.png" width="16" height="16" border="0" align="absmiddle">修改</a>
			 	
			 	<%} %>
			 	 
			</td>
            <td width="50%" align="right" valign="middle" class="quote_to">
						

          Date: <%= billOrder.getString("create_date") %> <br>
          Expiration Date:  <%= billOrder.getString("expiration_date") %>	
			</td>
          </tr>
        </table></td>
          </tr>
      <tr>
        <td colspan="2" align="right" valign="middle" class="quote_normal" style="padding-right:250px;">To</td>
          </tr>
      <tr>
        <td colspan="2" align="right" valign="middle"  class="quote_to"> <%=billOrder.getString("address_name")%><br>
        <%=billOrder.getString("address_street")%><br>
          <%=billOrder.getString("address_city")%>,<%=billOrder.getString("address_state")%>,<%=billOrder.getString("address_zip")%>,<%=orderMgr.getDetailCountryCodeByCcid(billOrder.get("ccid",0l)).getString("c_country")%><br>
          <%=billOrder.getString("tel")%><br>
          [<%=billOrder.getString("client_id")%>]
       </td>
        </tr>
      </table>
        <br>
      <br>
      <table id="quote_sales" width="690" border="0" cellpadding="1" cellspacing="1" bgcolor="#997339">
        <tr>
          <td width="437" align="left" valign="middle" bgcolor="#FAF7F2" class="quote_normal" style="padding-left:5px;">Salesperson</td>
            <td width="126"  align="center" valign="middle" bgcolor="#FAF7F2" class="quote_normal">Shipping Method</td>
            <td width="117"  align="center" valign="middle" bgcolor="#FAF7F2" class="quote_normal">Processing Time </td>
          </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF" class="quote_text"  style="padding-left:5px;">
		  <%=billAccount.getString("account")%>,<%=billAccount.getString("email")%>
		  <br>
MSN: <%=billAccount.getString("msn")%>
		  
	
		  </td>
            <td  align="center" valign="middle" bgcolor="#FFFFFF" class="quote_text" ><%
			DBRow detailCompany = expressMgr.getDetailCompany(billOrder.get("sc_id",0l));
			if (detailCompany==null)
			{
				out.println("?");
			}
			else
			{
				out.println(detailCompany.getString("name"));
			}
			%></td>
            <td  align="center" valign="middle" bgcolor="#FFFFFF" class="quote_text" >1 DAY </td>
          </tr>
        </table>
        <br>
      <table id="quote_products" width="690" border="0" cellpadding="0" cellspacing="1" bgcolor="#997339">
        <tr>
          <td align="center" valign="middle" bgcolor="#FAF7F2" class="quote_normal"><table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td width="15%" align="center" valign="middle" class="quote_normal">Qty</td>
              <td width="45%" align="left" valign="middle" class="quote_normal">Item #</td>
              <td width="17%" align="center" valign="middle" class="quote_normal" style="text-indent:20px;">&nbsp;&nbsp;&nbsp;Actual Price</td>
              <td width="22%" align="center" valign="middle" class="quote_normal">Total</td>
            </tr>
          </table></td>
          </tr>
        <tr>
          <td align="center" valign="middle" bgcolor="#FFFFFF" class="quote_text" ><table width="100%" border="0" cellspacing="0" cellpadding="2">
<%
int dataLen,spaceLen;

if (limitRowLength - billOrderItem.length>0)
{
	dataLen = billOrderItem.length;
	spaceLen = limitRowLength - billOrderItem.length;
}
else
{
	dataLen = billOrderItem.length;
	spaceLen = 0;
}

double subtotal = 0;
double total = 0;
for (int i=0; i<dataLen; i++)
{
	total = MoneyUtil.mul(billOrderItem[i].get("quantity",0d), billOrderItem[i].get("quote_price",0d));
	subtotal = MoneyUtil.add(subtotal, total);
%>		  
            <tr>
              <td width="15%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px"><%=billOrderItem[i].get("quantity",0f)%> <%=billOrderItem[i].getString("unit_name")%></td>
              <td width="50%" align="left" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">
			  <%=billOrderItem[i].getString("name")%>
			  <%=isEbay? " Number : "+billOrderItem[i].getString("item_number"):"" %>
			  
<%
			if (billOrderItem[i].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
			{
				out.println("<div style='color:#000000;padding-left:10px;font-weight:normal'>");
			 
				DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( billOrderItem[i].get("pc_id",0l) );
			 
				for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
				{
					out.println("├ "+customProductsInSet[customProductsInSet_i].getString("p_name")+" x "+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name")+"<br>");
				}
				out.println("</div>");
			}
%>			  </td>
              <td width="12%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">$<%=billOrderItem[i].get("actual_price",0d)%></td>
              <td width="22%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 0px">$<%=billOrderItem[i].get("amount",0d)%></td>
              
               
            </tr>
<%
}


for (int i=0; i<spaceLen; i++)
{
%>		  
            <tr>
              <td width="15%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">&nbsp;</td>
              <td width="45%" align="left" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">&nbsp;</td>
              <td width="17%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">&nbsp;</td>
              <td width="22%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 0px">&nbsp;</td>
               
            </tr>
<%
}
%>

          </table></td>
          </tr>
      </table>
      <table id="quote_products" width="690" border="0" cellpadding="0" cellspacing="0" bgcolor="#997339">
        
        <tr>
          <td align="center" valign="middle" bgcolor="#FFFFFF" class="quote_text" ><table width="100%" border="0" cellspacing="0" cellpadding="2">
           	 
              <tr>
                
                <td  align="right" valign="middle" bgcolor="#FAF7F2" class="quote_normal" style="width:478px;" >Subtotal&nbsp;&nbsp;</td>
                <td  align="center" valign="middle" class="quote_text" style="border-left:#997339 solid 1px;border-bottom:#997339 solid 1px;border-right:#997339 solid 1px;width:101px;">$<%=billOrder.get("subtotal",0d)%></td>
              </tr>
              <tr>
                <td  align="right" valign="middle" bgcolor="#FAF7F2" class="quote_normal" >Shipping Fee &nbsp;&nbsp;</td>
                <td align="center" valign="middle" class="quote_text" style="border-left:#997339 solid 1px;border-bottom:#997339 solid 1px;border-right:#997339 solid 1px;">$<%=MoneyUtil.round(billOrder.get("shipping_fee",0d),2)%></td>
              </tr>
              <tr>
             	 <td   align="right" valign="middle" bgcolor="#FAF7F2" class="quote_normal" >Discount&nbsp;&nbsp;</td>
              		<td align="center" valign="middle" class="quote_text" style="border-left:#997339 solid 1px;border-bottom:#997339 solid 1px;border-right:#997339 solid 1px;">
			  		$<%=billOrder.get("total_discount",0d)%>
			  </td>
	
            </tr>
            <tr>
              <td   align="right" valign="middle" bgcolor="#FAF7F2" class="quote_normal" >Total&nbsp;&nbsp;</td>
                <td align="center" valign="middle" class="quote_text" style="border-left:#997339 solid 1px;border-bottom:#997339 solid 1px;border-right:#997339 solid 1px;">$<%=billOrder.get("save",0d)%></td>
                </tr>
           

          </table></td>
        </tr>
      </table>
      
      <br>
     <strong> Note :</strong> <br/>
 <textarea style="border:1px solid #997339;padding:4px;width:690px;margin-top:5px;margin-bottom:5px;overflow-y:auto;">
<%= billOrder.getString("note") %>
 </textarea>
      <br />
      <table id="quote_products" width="690" border="0" cellpadding="0" cellspacing="1" bgcolor="#997339" style="padding-top:5px;padding-bottom:5px;">
        
        <tr>
          <td height="80" align="center" valign="top" bgcolor="#FFFFFF" class="quote_text" ><table width="99%" border="0" cellspacing="0" cellpadding="4">
            <tr>
              <td>Payment Instruction</td>
            </tr>
            <tr>
              <td class="quote_text"><br>
                1.Go to http://www.vvme.com/payment.<br />
                2.Input  your  invoice NO. and  client  Email  on  the " Pay For Invoice" label.<br /> 
                3.Click  the  "Next" label  to check  ,after  check out  your  invoice.<br />
                4.Then you  can  choose  paypal  or  credit card  to  finish  your  payment.
			 </td>
            </tr>
            
          </table></td>
        </tr>
      </table>
      <br>
      <br>
      <br>
      <table width="690" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="607" align="center" valign="middle" class="quote_thanks">Thank you for your business!</td>
        </tr>
        </table>
      <br>
      <br>
      <table width="690" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="607" align="center" valign="middle" class="quote_text">Visionari LLC &nbsp;&nbsp;  2570 Corporate Place 	E103,Monterey Park 	CA 	91754 	US  Phone:2139081217 sales@vvme.com  </td>
        </tr>
        </table>
        <br></td>
  </tr>
  </table>


    <table width="750" height="23" border="0" cellpadding="0" cellspacing="0" class="noPrint" >
  <tr>
    <td colspan="2" >&nbsp;</td>
    </tr>
  <tr>
    <td >
	 
	<input name="Submit2" type="button" class="export-pdf" value="导出PDF" onClick="exportPDF(<%=billId%>)" />
	&nbsp;&nbsp;&nbsp;&nbsp;
	<input name="Submit3" type="button" class="sendmail" value="邮件发送" onClick="emailBill(<%=billId %>)" />
	<% 
	   	boolean hasOper = billMgrZr.hasAuthority(loginAdid,billOrder.get("create_adid",0l));
		if(billOrder.get("bill_status",0) == billState.quote && billOrder.get("key_type",0) == PaymentMethodKey.PP && hasOper){
	%> 
		<%	
			int invoiceState =  billOrder.get("invoice_state",1);
			//invoiceState = invoiceState > 3? invoiceState - 3:invoiceState;
			if(invoiceState == invoiceStateKey.NewAdd){
			 					 
		 %>
		  <input name="" value="创建发送" class="sendmail" style="padding-left:9px;" type="button" onclick="createInvoice('<%=billOrder.get("bill_id",0l) %>','createandsend')"/>
		 	
 
 		 	
		 	<% 		
 				}else if(invoiceState == invoiceStateKey.CreateInvoiceed){
 			%>
 	 			<% 		
 				}else if(invoiceState >= invoiceStateKey.NotUpdateBillInvoice){
 			%>
 			 	<input name="" value="更新Invoice" class="sendmail" style="padding-left:9px;" type="button" onclick="updateInvoice('<%=billOrder.get("bill_id",0l) %>')"/>
 				
  			<% 		
 				}
 		  }
 			%>
		
 
	 
	</td>
    <td width="205" align="right" ><input name="Submit" type="button" class="normal-white" value="返回" onClick="javascript:history.go(-1)"></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  </table>
	
	
	</td></tr>
</table>



</body>
<script type="text/javascript">

function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
 
	 var  _self = $("body"),
	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	 
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
 }
} 
</script>
</html>
