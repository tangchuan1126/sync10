<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.InvoiceStateKey"%>
<%@ include file="../../../include.jsp"%> 
<%@ page import="com.cwc.app.key.PaymentMethodKey,java.util.ArrayList,com.cwc.app.key.BillTypeKey,java.util.ArrayList,com.cwc.app.key.InvoiceStateKey" %>
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单报价</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">

@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
 
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>



<%
	long oid = StringUtil.getLong(request,"oid");
	DBRow order = orderMgrZr.getDetailById(oid,"porder","oid");
	String sendEbayMessageAction =  ConfigBean.getStringValue("systenFolder") + "action/administrator/order/SendEbayMessage.action";
%>
 
 
<style type="text/css">
 div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{text-align:center;border:1px solid silver;margin:0px auto;margin-top:30px;width:500px;height:30px;background:white;margin-top:200px;line-height:30px;font-size:14px;}
</style>
 
<script type="text/javascript">
  function submitMy(){
      var content = $("#content").val();
		if(content.length < 1){
			alert("请先输入内容!!!");
			return   ;
		}
      $(".cssDivschedule").css("display","block");
		$.ajax({
			url:'<%= sendEbayMessageAction%>',
			dataType:'json',
			data:$("#myform").serialize(),
			success:function(data){
			    $(".cssDivschedule").css("display","none");
				if(data && data.flag === "success"){
				    $.artDialog && $.artDialog.close();	
				}else{
					if(data.message&&data.message.length > 0){
						alert(data.message);
					}else{alert("系统错误");}
					
				}
			}
		})
  }
 function cancel(){$.artDialog && $.artDialog.close();}
</script>
 
</head>
<body>
 <div class="cssDivschedule">
 		<div class="ui-corner-all innerDiv" style="margin-top:90px;">
 			正在发送信息中.....
 		</div>
 </div>
 
 	<form id="myform">
 		<input type="hidden" name="oid" value='<%=order.getString("oid") %>' />
 		<input type="hidden" name="seller_id"   value='<%= order.getString("seller_id") %>'/>
 		<input type="hidden" name="item_number" value='<%= order.getString("item_number") %>'/>
 		<input type="hidden" name="item_name" value='<%= order.getString("item_name") %>' />
 		<input type="hidden" name="buyer_id" value='<%= order.getString("auction_buyer_id") %>' />
      <table width="98%" border="0" cellspacing="7" cellpadding="2">
		<tr>
			<td width="18%"  style="font-weight:bold;text-align:right;">ItemName &nbsp;:</td>
			<td><span style="color:blue; font-weight: bold;"><%= order.getString("item_name") %></span></td>
		</tr>
		<tr>
			<td width="18%"   style="font-weight:bold;text-align:right;">Item Id &nbsp;:</td>
			<td><span style="color:blue; font-weight: bold;"><%= order.getString("item_number") %></span></td>
		</tr>
		<tr>
			<td width="18%"   valign="middle" style="font-weight:bold;text-align:right;">SellerId &nbsp;:</td>
			<td><span style="color:#99CC00;font-size:15px;font-weight:bold"><%=order.getString("seller_id")%></span></td>
		</tr>
		<tr>
			<td width="18%" style="font-weight:bold;text-align:right;">Content &nbsp;: </td>
	 
			<td>
<textarea style="width:527px;height:150px;" name="content" id="content">
</textarea>
			</td>
			
		</tr>
		<tr>
			<td colspan="2" style="text-align:center;">
				<input type="button" value="确定" class="normal-green" onclick="submitMy();"/>
				<input type="button" value="取消" class="normal-white" onclick="cancel();"/>
			</td>
		</tr>
	</table>
	</form>
</body>

</html>