<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="currencyKey" class="com.cwc.app.key.CurrencyKey"/>
<%
int type = StringUtil.getInt(request,"type");
long warranty_oid = StringUtil.getLong(request,"warranty_oid");
cartWarranty.clearCart(session);

DBRow detailOldOrder = orderMgr.getDetailPOrderByOid(warranty_oid);
long pro_id = detailOldOrder.get("pro_id",0l);

String actionPath;

if (type==1)
{
	actionPath = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/markWarrantied.action";
}
else if (type==2)//无偿质保
{
	actionPath = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/markFreeWarranty.action";
}
else//免运费质保
{
	actionPath = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/markFeeShippingFeeWarranty.action";
}
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>退货</title>

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>



<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<style type="text/css">
<!--
td
{
	font-size:12px;
}

form
{
	padding:0px;
	margin:0px;
}


-->
</style>

<script>


$().ready(function() {

	function log(event, data, formatted) {

		
	}
	
	$("#p_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 410,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];   
			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name+" ["+data[i].unit_name+"]["+data[i].p_code+"]",
    				value:data[i].p_name+" ["+data[i].unit_name+"]["+data[i].p_code+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	loadCartReturn();
	ajaxCartForm();
	getStorageProvinceByCcid(<%=detailOldOrder.get("ccid",0l)%>,<%=detailOldOrder.get("pro_id",0l)%>);
	
});


//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   //beforeSubmit:  // pre-submit callback    
			//	function(){
			//		loadCart();
			//	}	
		  // ,  
		   success:       // post-submit callback  
				function(){
					loadCartReturn();
					cartProQuanHasBeChange = false;
				}
		       
	};
	

   $('#cart_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}

function loadCartReturn()
{
	//alert(para);

	$.ajax({
		url: 'mini_cart_warranty.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		
		beforeSend:function(request){
			
		},
		
		error: function(){
			alert("加载购物车失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
		}
	});
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 

function checkForm()
{
	var theForm = document.search_union_form;
	
	if(theForm.p_name.value=="")
	{
		alert("商品名不能为空");
		return(false);
	}
	else if(theForm.quantity.value=="")
	{
		alert("数量不能为空");
		return(false);
	}
	else if(!isNum(theForm.quantity.value))
	{
		alert("请正确填写数量");
		return(false);
	}
	else
	{

				//先检查购物车中的商品在当前仓库是否已经建库
				var para = "p_name="+theForm.p_name.value+"&quantity="+theForm.quantity.value;
			
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/Put2CartWarranty.action',
					type: 'post',
					dataType: 'html',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(){
						alert("添加商品失败！");
					},
					
					success: function(msg)
					{				
						if (msg=="ProductNotExistException")
						{
							alert("商品不存在！");
						}
						else if (msg=="ProductUnionSetCanBeProductException")
						{
							alert("套装不能作为定制商品");
						}
						else if (msg=="ProductNotCreateStorageException")
						{
							//
						}
						else if (msg=="ok")
						{
							//商品成功加入购物车后，要清空输入框
							theForm.p_name.value = "";
							theForm.quantity.value = "";
							loadCartReturn();
							return(false);
						}
						else
						{
							alert(msg);
						}

					}
				});	
				

	}
}


function delProductFromCart(pid,p_name,product_type)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/RemoveProductFromCartWarranty.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"pid="+pid+"&product_type="+product_type,
					
			error: function(){
				alert("商品删除失败！");
			},
			
			success: function(msg){
				loadCartReturn();
			}
		});	
	}
}

var cartProQuanHasBeChange = false;
var cartIsEmpty = <%=cart.isEmpty(session)%>;

function onChangeQuantity(obj)
{
	if ( !isNum(obj.value) )
	{
		alert("请正确填写数字");
		return(false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}

function save()
{
	if ( $("#reason_input").val()==-1 )
	{
		alert("请选择质保原因");
	}
	else if (cartIsEmpty)
	{
		alert("请添加重发商品");
	}
	else if ( $("#quantity_input").val()=="" )
	{
		alert("请填写件数");
	}
	else if ( $("#mc_gross_input").val()=="" )
	{
		alert("请填写付款金额");
	}
	else if ( $("#ccid_input").val()=="" )
	{
		alert("请选择国家");
	}
	else if ( $("#mc_currency_input").val()=="" )
	{
		alert("请选择货币");
	}
	else if ( $("#note_input").val()=="" )
	{
		alert("请填写质保原因");
	}
	else
	{

			parent.document.create_warranty_form.action = "<%=actionPath%>";
			parent.document.create_warranty_form.business.value = $("#business_input").val();
			parent.document.create_warranty_form.quantity.value = $("#quantity_input").val();
			parent.document.create_warranty_form.mc_gross.value = $("#mc_gross_input").val();
			parent.document.create_warranty_form.mc_currency.value = $("#mc_currency_input").val();
			parent.document.create_warranty_form.reason.value = $("#reason_input").val();
			parent.document.create_warranty_form.oid.value = <%=warranty_oid%>;
			parent.document.create_warranty_form.type.value = <%=type%>;
			
			parent.document.create_warranty_form.ccid.value = $("#ccid_input").val();
			parent.document.create_warranty_form.pro_id.value = $("#pro_id_input").val();
			parent.document.create_warranty_form.address_name.value = $("#address_name_input").val();
			parent.document.create_warranty_form.address_street.value = $("#address_street_input").val();
			parent.document.create_warranty_form.address_city.value = $("#address_city_input").val();
			parent.document.create_warranty_form.address_state.value = $("#address_state_input").val();
			parent.document.create_warranty_form.address_zip.value = $("#address_zip_input").val();
			parent.document.create_warranty_form.tel.value = $("#tel_input").val();

			
			parent.document.create_warranty_form.submit();
	}
		
	
}

function customProduct(pid,p_name,cmd)
{
	tb_show('定制套装',"../product/custom_product_warranty.html?cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&ps_id="+$("#ps_id").val()+"&TB_iframe=true&height=300&width=400",false);
}

function closeTBWin()
{
	tb_remove();
}

//国家地区切换
function getStorageProvinceByCcid(ccid,pro_id)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id_input").clearAll();
					$("#pro_id_input").addOption("请选择State......","");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#pro_id_input").addOption(data[i].pro_name,data[i].pro_id);
						});
					}		

					if (ccid>0)
					{
						$("#pro_id_input").addOption("Others","-1");
					}

					if (pro_id>0)
					{
						$("#pro_id_input").setSelectedValue(pro_id);
					}

				}
		);
}

</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
				
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><br>
    <table width="95%" border="0" align="center" cellpadding="7" cellspacing="0">
  
  
  <tr id="return_product_row" >
    <td align="center" bgcolor="#eeeeee" >质保原因</td>
    <td bgcolor="#eeeeee" style="padding-top:10px;padding-bottom:10px;">
	<select name="reason_input" id="reason_input">
	  <option value="-1">请选择...</option>
	  <%
	  DBRow warranty_reason[] = orderMgr.getAllWarrantyReasons();
	  for (int i=0; i<warranty_reason.length; i++)
	  {
	  %>
	  <option value="<%=warranty_reason[i].getString("id")%>"><%=warranty_reason[i].getString("value")%></option>
	  <%
	  }
	  %>
	    </select>	</td>
  </tr>
  <tr id="return_product_row" >
    <td width="11%" align="center" >重发商品</td>
    <td width="89%" style="padding-top:10px;padding-bottom:10px;">
			 <fieldset style="border:1px #999999 solid;padding:10px;-webkit-border-radius:4px;-moz-border-radius:4px;width:580px">
<legend style="font-size:15px;font-weight:normal;color:#666666;">添加重发商品</legend>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
   
  <tr>
    <td width="100%" colspan="2" bgcolor="#eeeeee" style="padding-left:10px;padding-left:toppx;padding-bottom:5px;border-bottom:1px #999999 solid">
	<form name="search_union_form" method="post" action="">
	  商品
	    <input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:150px;"/> 
	    数量
        <input name="quantity" type="text" id="quantity"  style="color:#FF3300;width:40px;"/>
&nbsp;&nbsp;
      <input name="Submit" type="button" class="short-button" value="添加" onClick="checkForm()">	
	  </form>	  </td>
  </tr>
  <tr>
    <td colspan="2">
	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/BatchModifyProductsQuantityFromCartWarranty.action" method="post" name="cart_form" id="cart_form">
	<div id="mini_cart_page" style="color:#CCCCCC">数据加载中……</div>
	</form>	</td>
  </tr>
</table>
             </fieldset>	</td>
  </tr>
  <tr>
    <td height="35" align="center" bgcolor="#eeeeee" >付款帐号</td>
    <td bgcolor="#eeeeee" ><select name="business_input" id="business_input">
	  <option value="0">选择帐号...</option>
                <%
String businessl[] = systemConfig.getStringConfigValue("business").split(",");
for (int i=0; i<businessl.length; i++)
{
%>
                <option value="<%=businessl[i]%>" <%=businessl[i].equals("warranty@vvme.com")?"selected":""%>> 
                <%=businessl[i]%>                </option>
                <%
}
%>
      </select></td>
  </tr>
  <tr>
    <td height="35" align="center" >件数</td>
    <td ><input name="quantity_input" type="text" id="quantity_input" size="15"></td>
  </tr>
    <tr>
    <td height="35" align="center" bgcolor="#eeeeee" >付款金额</td>
    <td bgcolor="#eeeeee" ><input name="mc_gross_input" type="text" id="mc_gross_input" value="" size="15">
		&nbsp;
<select name="mc_currency_input" id="mc_currency_input">
        <option value="">选择货币...</option>
        <%
ArrayList currency = currencyKey.getCurrency();
for (int i=0; i<currency.size(); i++)
{
%>
        <option value="<%=currencyKey.getCurrencyById( currency.get(i).toString() )%>" ><%=currency.get(i)%></option>
<%
}
%>
      </select>	</td>
  </tr>


 <tr>
    <td height="35" align="center" >Country</td>
    <td >
	<%
	DBRow countrycode[] = orderMgr.getAllCountryCode();
	String selectBg="#ffffff";
	String preLetter="";
	%> 
      <select name="ccid_input" id="ccid_input" onChange="getStorageProvinceByCcid(this.value,0)" >
	  <option value="0">选择国家...</option>
	  <%
	  for (int i=0; i<countrycode.length; i++)
	  {
	  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
		{
			if (selectBg.equals("#eeeeee"))
			{
				selectBg = "#ffffff";
			}
			else
			{
				selectBg = "#eeeeee";
			}
		}  	
		
		preLetter = countrycode[i].getString("c_country").substring(0,1);
	  %>
	    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>" <%=detailOldOrder.get("ccid",0l)==countrycode[i].get("ccid",0l)?"selected":""%>><%=countrycode[i].getString("c_country")%></option>
	  <%
	  }
	  %>
      </select>
	  &nbsp;&nbsp;
	  <select name="pro_id_input" id="pro_id_input"  >
	  </select>   
	</td>
  </tr>
    <tr>
    <td height="35" align="center" bgcolor="#eeeeee" >Name</td>
    <td bgcolor="#eeeeee" ><input name="address_name_input" type="text" id="address_name_input" size="40" value="<%=detailOldOrder.getString("address_name")%>"></td>
  </tr>
    <tr>
    <td height="35" align="center" >Street</td>
    <td ><input name="address_street_input" type="text" id="address_street_input" size="40" value="<%=detailOldOrder.getString("address_street")%>"></td>
  </tr>
    <tr>
    <td height="35" align="center" bgcolor="#eeeeee" >City</td>
    <td bgcolor="#eeeeee" ><input name="address_city_input" type="text" id="address_city_input" size="40" value="<%=detailOldOrder.getString("address_city")%>"></td>
  </tr>
    <tr>
    <td height="35" align="center" >State</td>
    <td ><input name="address_state_input" type="text" id="address_state_input" size="40" value="<%=detailOldOrder.getString("address_state")%>"></td>
  </tr>
    <tr>
    <td height="35" align="center" bgcolor="#eeeeee" >Zip</td>
    <td bgcolor="#eeeeee" ><input name="address_zip_input" type="text" id="address_zip_input" size="40" value="<%=detailOldOrder.getString("address_zip")%>"></td>
  </tr>
    <tr>
    <td height="35" align="center" >Tel</td>
    <td ><input name="tel_input" type="text" id="tel_input" size="40" value="<%=detailOldOrder.getString("tel")%>"></td>
  </tr>




</table>
	
	<br>
	<br>
	<br></td></tr>
  <tr>
    <td align="right" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green" value="确定" onClick="save()">
      <input name="cancel" type="button" class="normal-white" value="取消" onClick="parent.closeTBWin();">
    </td>
  </tr>
</table>

</body>

</html>