<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="returnProductKey" class="com.cwc.app.key.ReturnProductKey"/>
<%
long rp_id = StringUtil.getLong(request,"rp_id");
DBRow returnProduct = orderMgr.getDetailReturnProductByRpId(rp_id);
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>退货</title>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<script type='text/javascript' src='../js/jquery.form.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />
<script type="text/javascript">
	function cancel(){
		parent && parent.closeTBWin && parent.closeTBWin();
		$.artDialog.opener && $.artDialog.opener.refreshWindow();
		$.artDialog && $.artDialog.close();
	}
</script>
<style type="text/css">
<!--
td
{
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
}

form
{
	padding:0px;
	margin:0px;
}

.title
{
	font-size:15px;
	
}
.STYLE1 {color: #0066FF}
-->
</style>

<script>
function post(oid)
{

}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><table width="620" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td width="50%" ><table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td colspan="2" align="center" valign="middle" class="title" style="padding-bottom:10px;"><%=returnProductKey.getReturnProductStatusById(returnProduct.get("status",0))%>
				<%
		if ( returnProduct.getString("create_date").equals(returnProduct.getString("handle_date")) )
		{
			
		}
		else
		{
			out.println(" ("+returnProduct.getString("handle_date")+")");
		}
		%>		</td>
        </tr>
      <tr>
        <td width="61%" class="title"><span class="STYLE1"><strong>退货单号</strong>：</span><%=rp_id%></td>
        <td width="39%" class="title"><span class="STYLE1"><strong>创建日期</strong>：</span><%=returnProduct.getString("create_date")%></td>
      </tr>
      <tr>
        <td class="title"><span class="STYLE1"><strong>退货原因</strong>：</span><%=orderMgr.getReturnProductReasonById(returnProduct.get("return_product_reason",0))%></td>
        <td class="title"><span class="STYLE1"><strong>退货仓库</strong>：</span><%=catalogMgr.getDetailProductStorageCatalogById(returnProduct.get("ps_id",0l)).getString("title")%></td>
      </tr>
      
    </table></td>
  </tr>
  <%
  if (returnProduct.getString("note").equals("")==false)
  {
  %>
  <tr>
    <td style="border:1px #000000 solid;color:#000000;background:#FFFFCC">退货数量异常：<%=returnProduct.getString("note")%></td>
  </tr>
  <%
  }
  %>
  <tr>
    <td >
	<br>

			 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">主商品信息</legend>
<%
DBRow returnProducts[] = orderMgr.getReturnProductItemsByRpId(rp_id,null);
%>
<table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
<%
for (int i=0; i<returnProducts.length; i++)
{
%>
  <tr>
    <td width="69%" style="font-family:Arial;font-weight:bold"><%=returnProducts[i].getString("p_name")%></td>
    <td width="31%" style="font-family:Arial;font-weight:bold"><%=returnProducts[i].getString("quantity")%> <%=returnProducts[i].getString("unit_name")%> </td>
  </tr>
<%
}
%>
</table>
			 </fieldset>			 	</td>
  </tr>
  
  <tr>
    <td ><br>

	
			 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">散件商品信息</legend>
<%
DBRow returnSubProducts[] = orderMgr.getReturnProductSubItemsByRpId( rp_id,null);
%>
<table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td height="23" style="font-family:Arial;font-weight:bold;border-bottom:1px #cccccc solid;background:#eeeeee">&nbsp;</td>
    <td style="font-family:Arial;font-weight:bold;border-bottom:1px #cccccc solid;background:#eeeeee">&nbsp;</td>
    <td align="center" valign="middle" style="font-family:Arial;border-bottom:1px #cccccc solid;background:#eeeeee">完好</td>
    <td align="center" valign="middle" style="font-family:Arial;border-bottom:1px #cccccc solid;background:#eeeeee">功能残损</td>
    <td align="center" valign="middle" style="font-family:Arial;border-bottom:1px #cccccc solid;background:#eeeeee">外观残损</td>
  </tr>
<%
for (int i=0; i<returnSubProducts.length; i++)
{
%>

  <tr>
    <td width="44%" height="25" style="font-family:Arial;font-weight:bold"><%=returnSubProducts[i].getString("p_name")%></td>
    <td width="15%" style="font-family:Arial;font-weight:bold"><%=returnSubProducts[i].getString("quantity")%> <%=returnSubProducts[i].getString("unit_name")%> </td>
    <td width="13%" align="center" valign="middle" style="font-family:Arial;font-weight:bold"><%=returnSubProducts[i].getString("quality_nor")%></td>
    <td width="14%" align="center" valign="middle" style="font-family:Arial;font-weight:bold;<%=returnSubProducts[i].get("quality_damage",0f)>0?"color:#FF0000":""%>""><%=returnSubProducts[i].getString("quality_damage")%></td>
    <td width="14%" align="center" valign="middle" style="font-family:Arial;font-weight:bold;<%=returnSubProducts[i].get("quality_package_damage",0f)>0?"color:#FF0000":""%>""><%=returnSubProducts[i].getString("quality_package_damage")%></td>
  </tr>
<%
}
%>
</table>
			 </fieldset>	</td>
  </tr>
</table>
	
	</td></tr>
  <tr>
    <td align="right" valign="middle" class="win-bottom-line"><input name="cancel" type="button" class="normal-white" value="关闭" onClick="cancel();">
    </td>
  </tr>
</table>

</body>

</html>