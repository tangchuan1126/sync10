<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="handStatusleKey" class="com.cwc.app.key.HandStatusleKey"/>
<head>
	<style type="text/css">
		td.topBorder {border-top:1px solid silver;}
	</style>
</head>
<%
	cartWaybill.clearCart(session);
	long oid = StringUtil.getLong(request,"oid");
	DBRow orderDetail = orderMgr.getDetailOrderDeliveryInfoByOid(oid);
	DBRow[] resembleOrders = orderMgrZJ.getResembleOrderByOid(oid);
	ProductStatusKey productStatusKey = new ProductStatusKey();
	
	long[] oids = new long[resembleOrders.length];
%>
    <table width="100%" cellpadding="0" cellspacing="0"    style="border:1px solid silver;">
		<tr>
			<td width="40%" valign="top">
				<div style="min-height:200px;">
				<%
					for(int i = 0;i<resembleOrders.length;i++)
					{
						oids[i] = resembleOrders[i].get("oid",0l);
				%>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" >
						<tr style="height:22px;line-height:22px;<%=oid==resembleOrders[i].get("oid",0l)?"background:#E6C3C5;":"background:#E6F3C5;" %>">
							<td colspan="4" style="padding-right:4px;">
								&nbsp;&nbsp;&nbsp;&nbsp;<font color="blue"><%=resembleOrders[i].getString("order_source")%></font>:<%=resembleOrders[i].get("oid",0l)%>
								&nbsp;&nbsp;<%=handStatusleKey.getOrderHandleById(resembleOrders[i].getString("handle_status"))%>
								<%
									if(resembleOrders[i].get("handle_status",0)==HandStatusleKey.NORMAL)
									{
								%>
									<input id="all_<%=resembleOrders[i].get("oid",0l)%>" type="checkbox" <%=resembleOrders[i].get("oid",0l)==oid?"checked=\"checked\"":""%> value="<%=resembleOrders[i].get("oid",0l) %>" onclick="changeCheck(this.value)" style="float:right;margin-top:3px;" />
								<%
									}
								%>
								
							</td>
						</tr>
					
					<%
						DBRow[] orderItems = orderMgr.waitQuantityNotZero(resembleOrders[i].get("oid",0l));
						for(int j=0;j<orderItems.length;j++)
						{
							String backgroundColor = ( j % 2 == 1 ? "#f9f9f9":"white");
							 
					%>	
						<tr style="height:22px;line-height:22px;background:<%=backgroundColor %>">
							<td class="topBorder" style="padding-left:15px; padding-top:5px; width: 120px;" nowrap="nowrap" align="left"><div style="overflow:inherit;width:100px;"><%=orderItems[j].getString("name")%></div></td>
							<td align="right" class="topBorder" style="width:110px;">
								<input type="hidden" id="<%=orderItems[j].get("iid",0l)+"_wait_quantity"%>" value="<%=orderItems[j].get("wait_quantity",0f)%>"/>
								<font style="font-weight: bold;font-family: fantasy"><%=orderItems[j].getString("quantity")%></font>
								&nbsp;&nbsp;
								<input <%=resembleOrders[i].get("handle_status",0)==HandStatusleKey.NORMAL?"":"readonly='readonly'"%>  type="text" id="<%=orderItems[j].get("iid",0l)+"_wait"%>" style="width: 40px;" value="<%=orderItems[j].get("wait_quantity",0f)%>" onchange="changeQuantity(this.value,<%=orderItems[j].get("iid",0l)%>)"/>
							</td>
							<td align="left" style="width:15px;" class="topBorder" >
								<%=orderItems[j].getString("unit_name")%>
							</td>
							<td align="right" valign="middle" style="width:15px;vertical-align:middle;" class="topBorder" >
							
							<%
							if(orderItems[j].get("wait_quantity",0f)>0)
							{
								if(resembleOrders[i].get("handle_status",0)==HandStatusleKey.NORMAL)
								{
								
							%>
							<input id="<%=orderItems[j].get("oid",0l)+"_"+orderItems[j].get("iid",0l)%>" name="<%=orderItems[j].get("oid",0l)+"_"+orderItems[j].get("iid",0l)%>" type="checkbox" value="<%=orderItems[j].get("iid",0l)%>" onclick="checkItemChange(this.value,this.id)"/>
							<%
								}
								else
								{
									out.print("&nbsp;");
								}
							}
							%>
								
							</td>
						</tr>
						<%
						}
						%>
					</table>
				<%
					}
				%>
				 </div>
			</td>
			
			<td style="width:5%;border-left:1px solid silver; border-right:1px solid silver;">
				&nbsp;&nbsp;
			</td>
			<td align="center" style="width:45%;">
               <!--  拆分后的信息 -->
          
			
				  <div id="pre_calcu_order_page"></div>
				<div id="pre_calcu_order_info"></div>
			 
			</td>
		</tr>
		<tr>
			<td colspan="3" class="topBorder">
	 
				
				
					<table cellpadding="3" cellspacing="6" style="width:100%;border-collapse:collapse;">
					<% 
						DBRow[] waybill_order = wayBillMgrZJ.getWayBillOrdersByOids(oids);
						for(int i=0;i<waybill_order.length;i++)
						{
					%>
					<tr style="background:#E6F3C5;line-height:22px;height:22px;">
						<td>
							&nbsp;&nbsp;&nbsp;&nbsp;<%=catalogMgr.getDetailProductStorageCatalogById(waybill_order[i].get("ps_id",0l)).getString("title")%> 
						</td>
						<td><font style="font-weight: bold;font-style: italic"><%=expressMgr.getDetailCompany(waybill_order[i].get("sc_id",0l)).getString("name") %></font></td>
						<td><%=waybill_order[i].getString("tracking_number")%></td>
						<td nowrap="nowrap" style="font-weight:bold"><%=waybill_order[i].getString("address_name")%></td>
						<td nowrap="nowrap"><font style="font-style:oblique"><%=waybill_order[i].getString("address_street")+" "+waybill_order[i].getString("address_zip")%></font></td>
						<td>
							<%
								if(!backUpStorageMgrZJ.existsSplitRequireDetailForWaybill(waybill_order[i].get("waybill_id",0l)))
  								{
							%>
								<a href="javascript:void(0)" onClick="cancelWayBill(<%=waybill_order[i].get("waybill_id",0l)%>,'<%=waybill_order[i].getString("tracking_number")%>')"><img src="../imgs/del.gif" width="12" height="12" border="0"></a>
							<%
								}
							%>
							&nbsp;
						</td>
					</tr>
						
					<%
							DBRow[] waybillItems = wayBillMgrZJ.getWayBillOrderItems(waybill_order[i].get("waybill_id",0l));
					%>
							<tr>
								<td colspan="6">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" >
									<%
										for(int j=0;j<waybillItems.length;j++)
										{
											String backgroundColor = ( j % 2 == 1 ? "#f9f9f9":"white");
									 %>
									<tr style="line-height:22px;height:22px;background:<%=backgroundColor%>">
										<td width="80px;" align="center" class="topBorder"><%=waybillItems[j].get("oid",0l)%></td>
										<td nowrap="nowrap" align="left" class="topBorder">
											<%=waybillItems[j].getString("p_name")%><font style="font-weight: bolder">&nbsp;&nbsp;X&nbsp;&nbsp;</font><%=waybillItems[j].get("quantity",0f)%>
										</td>
									</tr>
									<%
										}
									%>
								</table>
								</td>
							</tr>
					<%
						}
					%>
					
				</table>
			 
			</td>
		</tr>
	</table>
	<table width="100%" cellpadding="0" cellspacing="0" style="padding-top: 5px;">
		<tr>
			<td></td>
			<td align="right">&nbsp;</td>
		</tr>
	</table>
	<form action="print/print_center.html" name="express_form">
		<input type="hidden" name="ps_id" id="ps_id"/>
		<input type="hidden" name="islacking" id="islacking"/>
		<input type="hidden" name="ccid" value="<%=orderDetail.get("ccid",0l)%>"/>
		<input type="hidden" name="pro_id" value="<%=orderDetail.get("pro_id",0l)%>"/>
		<input type="hidden" name="oid" value="<%=oid%>"/>
	</form>
	<script type="text/javascript">
 
		function changeCheck(id)
		{
			if($("[value="+id+"]").attr("checked"))
			{
				$("[name*='"+id+"_']").attr("checked",true);
				$("[name*='"+id+"_']").each(function(){
					$("#"+this.value+"_wait").val($("#"+this.value+"_wait_quantity").val());
					
					putToWaybillForItem(this.value);//放入购物车
				});
			}
			else
			{
				$("[name*='"+id+"_']").attr("checked",false);
				
				
				$("[name*='"+id+"_']").each(function(){
					$("#"+this.value+"_wait").val("0.0");
					removeToWaybillForItem(this.value)//移除购物车
				});
				
			}
		}
		
		function allCheck()
		{
				$("[id*='all_']").attr("checked",true);
				$("[id*='all_']").each(function(){
					changeCheck(this.value);
				});
		}
		
		function changeQuantity(quantity,id)
		{
			var wait_quantity = $("#"+id+"_wait_quantity").val();
			
			if(parseFloat(quantity)>parseFloat(wait_quantity))
			{
				alert("数量大于购买量！");
				$("#"+id+"_wait").val(wait_quantity);
			}
			else
			{	
				if(quantity>0)
				{
					$("[name$='_"+id+"']").attr("checked",true);
					
				}
				else
				{
					$("[name$='_"+id+"']").attr("checked",true);
					
					if(quantity<0)
					{
						$("#"+id+"_wait").val("0.0");
					}
				}
				putToWaybillForItem(id);
				//ajax修改购物车数量
			}
		}
		
		function checkItemChange(order_item_id,id)
		{
			if($("#"+id+"").attr("checked"))
			{
				$("#"+order_item_id+"_wait").val($("#"+order_item_id+"_wait_quantity").val());
				putToWaybillForItem(order_item_id);
			}
			else
			{
				$("#"+order_item_id+"_wait").val("0.0");
				removeToWaybillForItem(order_item_id);
			}
		}
		
		function putToWaybillForItem(order_item_id)
		{
			para = "order_item_id="+order_item_id+"&quantity="+$("#"+order_item_id+"_wait").val();
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/putToWayBillCart.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				async:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(msg){
					if(msg=="ok")
					{
						preCalcuOrder(order_item_id)
					}
				}
			});	
		}
		
		function removeToWaybillForItem(order_item_id)
		{
			para = "order_item_id="+order_item_id;
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/removeToWayBIllCart.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				async:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(msg){
					if(msg=="ok")
					{
						preCalcuOrder(order_item_id)
					}
				}
			});	
		}
		
		function preCalcuOrder(order_item_id,id)
		{
			var para = "order_item_id="+order_item_id+"&select_ccid=<%=orderDetail.get("ccid",0l)%>&select_pro_id=<%=orderDetail.get("pro_id",0l)%>";
			$.ajax({
				url: 'pre_calcu_order.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
					$("#pre_calcu_order_info").text("正在计算......");
				},
				
				error: function(){
					$("#pre_calcu_order_info").text("计算失败！");
				},
				
				success: function(html){
					$("#pre_calcu_order_page").html(html);
					$("#pre_calcu_order_info").text("");
				}
			});
		}
		
		function psSelect2(ps_id,islacking)
		{
			if(islacking)
			{
				$("#select_exp").attr("disabled","disabled");
			}
			else
			{
				$("#select_exp").removeAttr("disabled");
			}
			document.express_form.ps_id.value = ps_id;
		}
		
		function psSelect(ps_id,islacking)
		{
			
			//if(islacking)
			//{
			//	orderLacking(ps_id);
			//}
			//else
			//{
			//	submit(ps_id);
			//}
			
			//document.express_form.ps_id.value = ps_id;
			submit(ps_id,islacking);
		}
		
		function submit1()
		{
			document.express_form.submit();
		}
		
		function submit(ps_id,islacking)
		{
			//$("#ps_id").val()
			$.artDialog.open("print/print_center.html?islacking="+islacking+"&ps_id="+ps_id+"&ccid=<%=orderDetail.get("ccid",0l)%>&pro_id=<%=orderDetail.get("pro_id",0l)%>&oid=<%=oid%>", {title: "选择快递",width:'700px',height:'460px',fixed:true, lock: true,opacity: 0.3});
			//tb_show('选择快递',"print/print_center.html?ps_id="+ps_id+"&ccid=<%=orderDetail.get("ccid",0l)%>&pro_id=<%=orderDetail.get("pro_id",0l)%>&oid=<%=oid%>&TB_iframe=true&height=350&width=700",false);
		}
		
		function splitAdviceOrder(ps_id)
		{
			$.artDialog.open("../product/split_advice_order.html?ps_id="+ps_id, {title: "订单拆分商品",width:'700px',height:'460px',fixed:true, lock: true,opacity: 0.3});
		}
		
		function closeTBWin()
		{
			cleanWayBill();
			loadWaybill();
			//tb_remove();
		}
		
		function orderLacking(ps_id)
		{
			if (confirm("确认产生缺货通知吗？"))
			{
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/orderLacking.action',
					type: 'post',
					dataType: 'html',
					timeout: 60000,
					cache:false,
					data:"ps_id="+ps_id,
							
					error: function(){
					},
					
					success: function(msg){
						if(msg=="close")
						{
							parent.closeTBWinRefrech();
						}
					}
				});	
			}
		}
		
		function cancelWayBill(waybill_id,trancing_number)
		{
			if(confirm("确定要取消"+trancing_number+"运单吗？"))
			{
				var para = "waybill_id="+waybill_id+"&oid=<%=oid%>";//传Oid是为了初始化订单购物车
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/CancelWayBillOrder.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">取消中...</span>' });
					},
					
					error: function(){
					},
					
					success: function(data)
					{
						$.unblockUI();
						if(data.result=="ok")
						{
							cleanWayBill();
				 			loadWaybill();
						}
						else if(data.result=="WayBillOrderPrintedException")
						{
							alert("运单已打印，请联系库房由库房取消！");
						}
					}
				});
			}
			
		}
		allCheck();
		//changeCheck(<%=oid%>);
	</script>