<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.api.OrderMgr"%>
<%@page import="com.cwc.app.api.zr.OrderMgrZr"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.PaymentMethodKey,java.util.ArrayList,com.cwc.app.key.BillTypeKey,java.util.ArrayList,com.cwc.app.key.InvoiceStateKey" %>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单报价</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<style type="text/css">
	*{padding:0px;margin:0px;}
	div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:500px;height:30px;background:white;margin-top:200px;line-height:30px;font-size:14px;}
   table tr td{line-height:25px;height:25px;}
   .td_left{border:1px solid red;width:100px;text-align:right;}
   .set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	span.addNewSpan{display:block;width:70px;height:20px;float:right;}
	ul.addressList{list-style-type:none;margin-left:10px;}
	ul.addressList li {text-indent:5px;line-height:25px;height:25px;margin-top:2px;border-bottom:1px dotted silver;cursor:pointer;}
	
	p.selectType{border:1px solid silver;background:#E6F3C5;height:35px;line-height:35px;}
	ul.type{border:1px solid silver;background:#E6F3C5;list-style-type:none;height:35px;line-height:35px;}
	ul.type li {margin-top:4px;float:left;border:1px solid silver;width:100px;line-height:25px;height:25px;margin-left:10px;text-align:center;cursor:pointer;}
	ul.type li.on{background:white;}
	span.button{-moz-transition: all 0.218s ease 0s; -moz-user-select: none;background: -moz-linear-gradient(center top , #F5F5F5, #F1F1F1) repeat scroll 0 0 #F5F5F5;border: 1px solid rgba(0, 0, 0, 0.1);
    border-radius: 2px 2px 2px 2px;color: #444444; cursor: pointer;font-size: 14px;font-weight: bold;height: 27px;line-height: 27px; min-width: 54px;
    outline: medium none;padding: 0 8px;text-align: center;display:block;margin-top:10px;
   }
   p.say {margin-top:8px;width:500px;}
   .infoList table td{}
   table.addressInfo td.right{text-align:right;font-weight:bold;color:#0066FF;}
   table.basic{border-collapse :collapse ;border:1px solid silver;}
   
   table.basic th{background:#e8f1fa;width:200px;font-size:12px; border:1px solid silver;}
   table.basic td{background:white;width:200px;text-indent:30px;border:1px solid silver;}
   table.itemTable{border-collapse :collapse ;border:1px solid silver;width:100%;}
   table.itemTable th{background:#e8f1fa;font-size:12px;font-weight:normal;line-height:35px;height:35px;border:1px solid silver;}
   table.itemTable td{line-height:30px;height:30px;border:1px solid silver;}
   table.itemTable td.name{text-indent:15px;}
   table.itemTable td.t{text-indent:10px;}
   table.itemTable tfoot  td{height:25px;border:none;text-align:center;}
   ul.itemDetail{list-style-type:none;margin-left:20px;margin-top:-2px;margin-bottom:5px;}
   ul.itemDetail li{border:0px solid silver;height:14px;line-height:14px;}
   input.noborder{width:150px;border:none;border-bottom:1px solid silver;}
   td.f{text-indent:5px;}
</style>
<%
	String getAccountInfo = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/GetAccountInfoAction.action";
	String getAddressInfoList = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/GetAddressInfoListAction.action";
	String saveBillByOrder = ConfigBean.getStringValue("systenFolder")+"action/administrator/bill/AddBillByOrderAction.action";
	long billId = StringUtil.getLong(request,"bill_id");
	DBRow row = new DBRow();
	long orderId = 0l;
	long adid = 0l;
	boolean isUpdate = (billId != 0l? true:false);
	int  invoice_state = 0 ;
	// 在update 的时候payer_email 来自账单
	// 在create 的时候payer_email 来自订单
	String payer_email =  "";
	if(isUpdate){
		row = orderMgrZr.getDetailById(billId,"bill_order","bill_id");
		orderId =  row.get("porder_id",0l);
		adid = row.get("create_adid",0l);
		invoice_state = InvoiceStateKey.NotUpdateBillInvoice;
		payer_email = row.getString("payer_email");
	}else{
		orderId  = StringUtil.getLong(request,"order_id");
		row = orderMgrZr.getDetailById(orderId,"porder","oid");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		adid = adminLoggerBean.getAdid();
		invoice_state = InvoiceStateKey.NewAdd;
		payer_email = row.getString("client_id");
	}
 
	DBRow userInfo = waybillMgrZR.getUserNameById(adid);
%>
<script type="text/javascript">
	var tip = []; 
	jQuery(function($){
		 initForm();
		 $("#sel").chosen({no_results_text: "没有匹配形式"}).change(function(){
			 var key =  $(this).val() ;
			 
			 if(key * 1 == -1){
				 //把后面的信息都移除
				 $("#memberDiv").html("");
			 }else{
				 $("#memberDiv").html("");
				 ajaxGetAccountInfo($(this).val());
			 }
			 if(key * 1 == <%= PaymentMethodKey.PP%>){
				 
				$("#payerEmailIsShow").css("display","");
			}else{
				$("#payerEmailIsShow").css("display","none");
			}
		 });
		 $(".chzn-search input").width(110);
	})
	function initForm(){
		var ccid = '<%= row.getString("ccid")%>';
	 
		var pro_id = '<%= row.getString("pro_id")%>';
		$("#pro_id option[value='"+pro_id+"']").attr("selected",true);
		$("#ccid_hidden option[value='"+ccid+"']").attr("selected",true);
		setPro_id('',pro_id,'<%= row.getString("address_state")%>');
		var isUpdate = '<%= isUpdate%>' === "true" ?  true:false;
		 
		if(isUpdate){
			initFormUpdate();
		}
		
	}
	
	function initFormUpdate(){
		//收款方式
		$("#account_name").html('<%= row.getString("account_name")%>');
		$("#account").html('<%= row.getString("account")%>');
		$("#rate option[target='"+'<%= row.getString("rate_type")%>'+"']").attr("selected",true);
		$("#create_time").html('<%= row.getString("create_date")%>')
		 // 是Update并且是Paypal的
		if('<%= row.getString("payer_email")%>'.length > 0){
			$("#payerEmailIsShow").css("display","");
		}
		 
	}

	function setPro_id(fillFlag,val,html){
		removeAfterProIdInput();
		var node = $("#ccid_hidden");
		var value = node.val();
	 
		if(value*1 != 10929){
			$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
					{ccid:value},
					function callback(data){ 
						$("#pro_id").attr("disabled",false);
						$("#pro_id").clearAll();
						$("#pro_id").addOption("请选择......","0");
						if (data!=""){
							$.each(data,function(i){
								$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
							});
						}
						if (value>0){
							$("#pro_id").addOption("手工输入","-1");
						}
						$("#addBillProSpan").html($("#pro_id"));
						// 如果是这个下面有 对应的那么就选中 如果是么有的那么就是 和点击手工输入一样的操作
						if(val){
							if(val === "-1"){
								$("option[value='"+val+"']",$("#pro_id")).attr("selected",true);							 
								$("#addBillProSpan").append($("<input type='text' id='address_state_input' value='"+html+"' />")); 
							}else{
								$("option[value='"+val+"']",$("#pro_id")).attr("selected",true);
								removeAfterProIdInput();
							}
						}
					}
					
				);
			if(!fillFlag){
				if(value * 1 != 0 ){
					// 得到Option
					var option = $("option:selected",node);
					$("#address_country_code").val(option.attr("code"));
				}else{
					$("#address_country_code").val("");
				}
			}
		 }
	 }
	function ajaxGetAccountInfo(key){
		 var div = $("#memberDiv");
		 div.append("<img src='../imgs/ajax-loader.gif' style='margin-top:4px;margin-left:4px;'/>")
		$.ajax({
			url:'<%= getAccountInfo%>' + "?key="+key,
			dataType:'json',
			success:function(data){
				 $("img",div).remove();
				if(data){
					var selected = createMemberSelect(data);
					div.append($(selected));
				  	$("#member").chosen({no_results_text: "没有该人"}).change(function(){
				  		$("#key_type_input").val(key);
						var value = ($(this).val());
						if(value != "-1"){
							if(value == "-2"){
								//-2 表示的是手工输入
								var key_type = ($("#sel").val());
								var tipAccountName = "";
								var tipAcccount = "";
								for(var index = 0 , count = tip.length ; index < count ; index++ ){
									if(key_type  === tip[index]["key_type"]){
										tipAccountName = "*"+tip[index]["account_name"];
										tipAcccount = "*"+tip[index]["account"];
										break;
									}	 
								}
								$("#account_name").html("<input type='text' class='noborder' onfocus=inputIn(this,'"+tipAccountName+"') onblur=outInput(this,'"+tipAccountName+"') value='"+tipAccountName+"' />");
								$("#account").html("<input type='text' class='noborder' onfocus=inputIn(this,'"+tipAcccount+"') onblur=outInput(this,'"+tipAcccount+"') value='"+tipAcccount+"'/>")
								$("#account_payee").val("-2");
							}else{
								var option = $("option:selected",$("#member"));
								var optionHtml = option.html();
								var target = option.attr("target");
								$("#account_name").html(optionHtml);
								$("#account").html(value);
								$("#account_payee").val(target);
							}
						}
					 });
				  	$(".chzn-search input").width(110);
				} 
			}
		})	
	 }
	 function inputIn(_this,value){
		var node = $(_this);
		if(node.val() === value){
			node.css("color","black").val("");
		}
	 }
	 function outInput(_this,value){
		var node = $(_this);
		if($.trim(node.val()).length < 1) {
			node.val( value).css("color","silver");
		}
			
	 }
	 function createMemberSelect(data){
			var selected = ' <select data-placeholder="收款人" class="chzn-select" style="width:120px;" id="member">';
			var options = "<option value='-1'>选择收款人</option><option value='-2'>手工输入</option>";
			for(var index = 0 , count = data.length ; index < count ; index++ ){
				if(data[index]["is_tip"]*1==0)
				{
					options += "<option target='"+data[index]["id"]+"' value='"+data[index]["account"]+"'>"+data[index]["account_name"]+"</option>";
				}else{
					var  o = new Object();
					o = {
						key_type:data[index]["key_type"],
						account_name:data[index]["account_name"],
						account:data[index]["account"]
					};
					tip.push(o)
				}
			}
			options += "</select>";
			selected += options;
			return selected;

		 }
	 function removeAfterProIdInput(){
			$("#address_state").remove();
		}
	 function fixAddress(){
		 var node = $("#pro_id");
		 var value = node.val();
	 
		 if(value*1 == -1){
			 addInputAfterProid(node);
		 }else{
			 removeAfterProIdInput();
		}
	 }
	 
	 function addInputAfterProid(node){
		 var input  = "<input type='text'   id='address_state_input' />";
		 $(input).insertAfter(node);
	 }
	 function setPayerEmailByHand(){
	 	 	 
			var client_id = $("#client_id");
			var payer_email = $("#payer_email");
			payer_email.val(client_id.val());
	 	 	 
  }
</script>


</head>
<body>
<!-- 遮盖层 -->
  <div class="cssDivschedule">
 		<div class="innerDiv">
 			请稍等.......
 		</div>
 </div>
 <div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
		 <ul>
			 <li><a href="#bill">基本信息</a></li>
			   
			 <li><a href="#yulan">预览</a></li>	
		 </ul>
		 <form id="baseinfo">
		 <div id="bill" style="height:450px;overflow:auto;text-align:left;width:98%;">
			 <div style="width:40%;float:left;">
			 <input type="hidden" value="<%= row.get("bill_id",0l) %>"  name="bill_id"/>
			 <input type="hidden" value="<%= invoice_state %>" name="invoice_state" />
			 <input type="hidden"  value="5" name="bill_type"/>
			 <input type="hidden" value="<%= row.getString("key_type") %>" id="key_type_input" name="key_type" /> 
			 <input type="hidden" value="<%= row.getString("account_payee") %>" id="account_payee" name="account_payee" />
			 <input type="hidden"  name="porder_id" value="<%=orderId %>"/>
			 	<div style="">
				 	<fieldset class="set" style="border:2px solid #993300;padding:10px;">
				  		<legend>收款信息</legend>
				 		<img src="../imgs/visionari.gif" />
					 	<h3 style="color: #333333;margin-top: 10px;">Visionari LLC</h3>
					 	<p><span id="employe_name" style="font-weight:bold;"> <%= (userInfo != null?userInfo.getString("employe_name"):"")%></span><span style="margin-left:10px;" id="create_time"><%=DateUtil.NowStr() %></span></p>
						 <p>
						    	<div style="float:left;">
								    	<select data-placeholder="收款款方式" class="chzn-select" style="width:120px;" id="sel">
											<option value="-1">选择收款方式</option>
											<%
												PaymentMethodKey key = new PaymentMethodKey();
												ArrayList<String> payMentList=  key.getStatus();
												for(String name : payMentList){
											%>
													<option value="<%=name %>"><%=key.getStatusById(Integer.parseInt(name)) %></option>
											<% 	 
												}
											%>
										</select>
								</div>
									<div style="float:left;" id="memberDiv">
									 
									</div>
						    </p>
					    <p style="clear:both;">&nbsp;&nbsp;&nbsp;收款人:<span id="account_name"></span></p>
					    <p style="margin-top: 5px;">收款账户:<span id="account"></span></p>
					</fieldset>
			 	</div>
			 	<div style="text-align:left;">
			 		 <span style="font-weight:bold;color:#f60;">Note to recipient</span><br/>
<textarea id="note" style="height:60px;width:95%;margin-left:5px;" name="note">
<%= row.getString("note") %>
</textarea>
			 		<p>Characters: 1000</p>
			 	</div>	
			</div>
		 	<div style="width:57%;float:left;">
		 		<fieldset class="set" style="border:2px solid #993300;padding:5px;">
			  		<legend>地址信息</legend>
			 		<div>
 			 			 <!--  client_id,address_name,address_street,address_city,address_zip -->
						<table class="addressInfo">
							<tr>
								<td class="right">Client ID :</td>
								<td>&nbsp;<input type="text" value="<%= row.getString("client_id") %>" id="client_id" onkeyup="setPayerEmailByHand();" style="width:200px;" name="client_id" onblur=""/></td>
								 
							</tr>
							<tr id="payerEmailIsShow" style="display:none;">
								<td class="right">Payer Email:</td>
								<td>&nbsp;<input type="text" value="<%= payer_email %>" id="payer_email"  style="width:200px;" name="payer_email" onblur=""/></td>
							</tr>
							<tr>
								<td class="right">Name :</td>
								<td>&nbsp;<input type="text" name="address_name"  value="<%=row.getString("address_name") %>"/></td>
								
							</tr>
							 <tr>
								<td class="right">Street :</td>
								<td>&nbsp;<input type="text" name="address_street"  value="<%= row.getString("address_street") %>"/></td>
							</tr>
							 <tr>
								<td class="right">City :</td>
								<td>&nbsp;<input type="text" name="address_city" value="<%= row.getString("address_city") %>"/></td>
							</tr>
							<tr>
								<td class="right">Zip :</td>
								<td>&nbsp;<input type="text" name="address_zip" value="<%= row.getString("address_zip") %>"/></td>
							</tr>
							 <tr>
								<td class="right">Country :</td>
								<td>&nbsp;
									 
						<!-- 国家省份信息 -->
							<%
								DBRow countrycode[] = orderMgr.getAllCountryCode();
								String selectBg="#ffffff";
								
								String preLetter="";
							%>
					      	 <select  id="ccid_hidden"  name="ccid" onChange="removeAfterProIdInput();setPro_id();">
						 	 	<option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++){
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
										if (selectBg.equals("#eeeeee")){
											selectBg = "#ffffff";
										}else{
											selectBg = "#eeeeee";
										}
									}  	
									preLetter = countrycode[i].getString("c_country").substring(0,1);
								  %>
						  		  <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								  <%
								  }
								%>
			     			 </select>[United States]  
								</td>
								
							</tr>
							 <tr>
								<td class="right">State :</td>
								<td> 
									&nbsp;
									<span id="addBillProSpan">
										<select  disabled id="pro_id" name="pro_id" onchange="fixAddress()" style="margin-right:5px;"></select>
									</span>
								</td>
								 
							</tr>
							 
							 <tr>
								<td class="right">Tel :</td>
								<td><input type="text"  id="tel" name="tel" value="<%= row.getString("tel") %>"/></td>
							</tr>
							<input type="hidden" id="address_country_code" name="address_country_code" value="<%= row.getString("address_country_code") %>" />
						</table>
						
						<p>
							<ul class="addressList">
 							</ul>
						</p>
			 			
			 		</div>
			 	 
				</fieldset>
																Rate :<select name="rate" id="rate">
																		<option value="1" target="RMB">人民币 (RMB)</option>
																		<option value="<%=systemConfig.getStringConfigValue("USD")%>" target="USD" selected="selected"> <%=systemConfig.getStringConfigDescription("USD")%>  (USD)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("AUD")%>" target="AUD" > <%=systemConfig.getStringConfigDescription("AUD")%>  (AUD)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("EUR")%>" target="EUR"> <%=systemConfig.getStringConfigDescription("EUR")%>  (EUR)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("CAD")%>" target="CAD"> <%=systemConfig.getStringConfigDescription("CAD")%>  (CAD)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("CHF")%>" target="CHF"> <%=systemConfig.getStringConfigDescription("CHF")%>  (CHF)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("CZK")%>" target="CZK"> <%=systemConfig.getStringConfigDescription("CZK")%>  (CZK)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("GBP")%>" target="GBP"> <%=systemConfig.getStringConfigDescription("GBP")%>  (GBP)  </option> 
																		<option value="<%=systemConfig.getStringConfigValue("NOK")%>" target="NOK"> <%=systemConfig.getStringConfigDescription("NOK")%>  (NOK)  </option> 
																</select>
						补偿金额:<input type="text" name="order_fee" id="order_fee" value="<%= row.get("order_fee",0.0d) %>"/>
						 
		 	</div>
		 	<div style="clear:both;">
		 		<span><b>Memo </b>(your recipient won't see this)</span>  
<textarea name="memo_note" id="memo_note" style="width:95%;margin-left:5px;height:35px;">	
<%= row.getString("memo_note") %>
</textarea>
		 		<p>Characters: 150</p>
		 	</div>
		 	 
		 	 
			 
		 </div>
	 </form>
		 <div id="yulan" style="height:580px;">
			  <div style="width:95%;border:1px solid silver;margin:0px auto;padding:20px;">
			  		 <div style="width:40%;float:left;border:0px solid green;text-align:left;margin-top:20px;">
			  		 	<img src="../imgs/visionari.gif" />
					 	<h3 style="color: #333333;margin-top: 10px;">Visionari LLC</h3>
					 	<p><span id="employe_name_" style="font-weight:bold;"><%= (userInfo != null?userInfo.getString("employe_name"):"")%></span><span style="margin-left:10px;" id="create_time_"><%=DateUtil.NowStr() %></span></p>
					    <p style="" id="account_name_"></p>
					    <p style="margin-top: 10px;" id="account_"></p>
					   
			  		 </div>
			  		 <div style="width:55%;float:right;border:0px solid red;margin-bottom:30px;">
			  			<h1 style="color: #CCCCCC;text-transform:uppercase;font-size:16px;text-align:right;margin-right:20px;margin-top:10px;">Invoice</h1>
			  		 	<table id="invoiceDetails" class="basic" summary="Invoice details" style="float:right;margin-top:20px; ">
							<tbody>
								<tr>
									<th>Invoice number</th>
									<td><%= billId != 0l?billId+"":"XXXXXXXXXXXXXXXX" %>&nbsp;</td>
								</tr>
								<tr>
									<th>Invoice date</th>
									<td><%=DateUtil.NowStr() %>&nbsp;</td>
								</tr>
								<tr>
									<th>Payment terms</th>
									<td>Due on receipt</td>
								</tr>
								
								<tr>
									<th>Due date</th>
									<td><%=DateUtil.NowStr() %>&nbsp;</td>
								</tr>
							</tbody>
						</table>
			  		 </div>
			  		 <div style="clear:both;border:0px solid silver;margin-top:20px; " id="sendto">
			  		  
			  		 	<p class="ui-corner-all" style="width:90%;background:#e8f1fa;padding:5px;text-indent:10px;line-height:35px;height:35px;text-align:left;">
			  		 		 <span style="font-weight:bold;font-size:14px;">Send To : </span> <span id="addressInfoStr"></span>
			  		 	</p>
			  		 </div>
			  		 <div style="border:0px solid silver;margin-top:20px;">
			  		 	 <table class="itemTable">
			  		 			<thead>
			  		 				<tr>
			  		 					<th width="60%">Description</th>
			  		 					<th width="16%">Quantity</th>
			  		 					<th width="16%">Unit price</th>
			  		 					<th>Amount</th>
			  		 				</tr>
			  		 			</thead>
			  		 			<tbody id="tbodyView">
			  		 	 
			  		 			</tbody>
			  		 			<tfoot>
			  		 				<tr>
			  		 					<td></td>
			  		 					<td colspan="3" style="border-left:1px solid silver;">
			  		 						 <table style="width:100%;border-collapse:collapse;">
			  		 						 	<tr style="background:#e8f1fa">
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Rate Type</td>
			  		 						 		<td id="rate_type_td">RMB</td>
			  		 						 	</tr>
			  		 						 	<tr >
			  		 						 		<td style="width:50%;font-weight:bold;text-align:left;text-indent:20px;" id="">Subtotal</td>
			  		 						 		<td id="subtotal_td">$0.00</td>
			  		 						 	</tr>
			  		 						 	<tr style="background:#e8f1fa">
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >TotalDiscount</td>
			  		 						 		<td id="total_discount_td">$0.00</td>
			  		 						 	</tr>
			  		 							<tr >
			  		 						 		<td id="" style="width:50%;font-weight:bold;text-align:left;text-indent:20px;" >Shipping Fee</td>
			  		 						 		<td id="shipping_fee_td">$0.00</td>
			  		 						 	</tr>
			  		 						 	<tr style="background:#e8f1fa">
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Save</td>
			  		 						 		<td id="save_td">$0.00</td>
			  		 						 	</tr>
			  		 						 </table>
			  		 					</td>
			  		 					
			  		 				</tr>
			  		 			</tfoot>
			  		 	 </table>
			  		 </div>
			 </div> 
			 <div style="margin-top:10px;text-align:center; background: none repeat scroll 0 0 #F8F8F8;border-top: 1px solid #CCCCCC;line-height:50px;height:50px;">
			   	<input type="button" value="保存" onclick="save()" class="normal-green-long" style="cursor:pointer;margin-top:10px;" />
			 </div>
		 </div>
 </div>
 	<script type="text/javascript">
 	$("#tabs").tabs({
	 
		select: function(event, ui) {
			if( ui.index  * 1 == 1){
				// 初始化预览界面的值
			 
				addSendToInInvoice();	
				addAccountInInvoice();		
				addNoItemsInInvoice();
			}
		}
	});
 	function addAccountInInvoice(){
 	 	var account_name = $("#account_name").html();
 	 	var account = $("#account").html();
 	 	var account_payee = $("#account_payee").val();
 	 	if(account_payee === "-2"){
 	 	 	account_name = $("input",$("#account_name")).val();
 	 	 	account = $("input",$("#account")).val();
 	 	}
 	 	$("#account_name_").html(account_name);
		$("#account_").html(account);
 	 	
 	}
 	function addSendToInInvoice(){
		var baseForm = $("#baseinfo");
		//address_name,address_street,address_city,address_zip,state,country
		var str = "";
		str += getInputValueByNameAndForm("address_name",baseForm) + "," ;
		str += getInputValueByNameAndForm("address_street",baseForm) + "," ;
		str += getInputValueByNameAndForm("address_city",baseForm) + "," ;
		
		var address_country_node = $("#ccid_hidden");
		var address_country = $("option:selected",address_country_node).html();
		var address_state_node = $("#pro_id");
		var address_state  ;
		if(address_state_node.val() * 1 == -1){
			address_state = $("#address_state_input").val();
		}else{
			address_state = $("option:selected",address_state_node).html();
		}
		str += address_state + "," + address_country;
		$("#addressInfoStr").html(str); 
	 
 	 	
 	}
 	function getInputValueByNameAndForm(name,node){
		return  $("input[name='"+name+"']",node).val();
 	}
	function showValues(){
		var form = $("#baseinfo");
 
		var key_type = $("#key_type_input").val();
		var o = new Object();
		var account_name = "";
		var account = "";
		var accountNode = $("#account");
		var accountNameNode = $("#account_name");
		if(accountNode.find("input").length > 0 ){
		 
			account = accountNode.find("input").val();
			account_name = accountNameNode.find("input").val();
		}else{
			account =  accountNode.html();
			account_name = accountNameNode.html();
		}
	}
	// 保存bill 并且发送Invoice
	function save(){
		//先检查 是不是 验证了
		
		var form = $("#baseinfo");
		var str = form.serialize();
		var o = new Object();
		var account_name = "";
		var account = "";
		var accountNode = $("#account");
		var accountNameNode = $("#account_name");
		
		if(accountNode.find("input").length > 0 ){
		 
			account = accountNode.find("input").val();
			account_name = accountNameNode.find("input").val();
		}else{
			account =  accountNode.html();
			account_name = accountNameNode.html();
		}
		var account_payee = $("#account_payee").val();
		o["account"] = account;
		o["account_name"] = account_name;
		var rate_type = $("#rate option:selected").attr("target");
		o["rate_type"] = rate_type;	

		//
		var address_country_node = $("#ccid_hidden");
		var address_country = $("option:selected",address_country_node).html();
		var address_state_node = $("#pro_id");
		var address_state  ;
		if(address_state_node.val() * 1 == -1){
			address_state = $("#address_state_input").val();
		}else{
			address_state = $("option:selected",address_state_node).html();
		}
		o["address_state"] = address_state;
		o["address_country"] = address_country;
		
		
		str += "&"+ jQuery.param(o);
  


		$.ajax({
			url:'<%= saveBillByOrder%>',
			data:str,
			success:function(data){
				if(data === "success"){
					showMessage("修改成功","alert");
					$.artDialog.close();
					$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
				}else{
					showMessage("系统错误!","error");
				}		
			}
		})
	}
	// add no products 
	function addNoItemsInInvoice(){
 		var tbody = $("#tbodyView"); 
 	 	$("tr",tbody).remove();
 	 	var totalValue = $("#order_fee").val();
 	 	var tr = "<tr><td class='f'>For Order</td><td class='f'>1</td><td class='f'>"+totalValue+"</td><td class='f'>"+totalValue+"</td></tr>";

		$("#subtotal_td").html(totalValue);
		$("#save_td").html(totalValue);
		$("#rate_type_td").html($("#rate option:selected").attr("target"));
			
 	 	tbody.append(tr);
 	}
	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
 	</script>
</body>

</html>