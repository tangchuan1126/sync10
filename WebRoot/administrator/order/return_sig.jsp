<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="returnProductKey" class="com.cwc.app.key.ReturnProductKey"/>
<%@ include file="../../include.jsp"%> 
<%
 	long rp_id = StringUtil.getLong(request,"rp_id");
 long oid = StringUtil.getLong(request,"oid");

 DBRow detail = orderMgr.getDetailReturnProductByRpId(rp_id);
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 %> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单任务</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>


<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<script>
$().ready(function() {

	function log(event, data, formatted) {	
	}
	

	ajaxadd_handle_formForm();
	
});

//把表单ajax化
function ajaxadd_handle_formForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		  beforeSubmit:  // pre-submit callback 
				function(){
						var thrForm = document.add_handle_form;
						var canBeSubmit = true;
						var notMatch = false;
	
						if(typeof thrForm.rpsi_id.length == "undefined")
						{
									var preFix = thrForm.rpsi_id.value;
									var norObj = document.getElementById(preFix+"_nor");
									var damageObj = document.getElementById(preFix+"_damage");
									var packageDamageObj = document.getElementById(preFix+"_package_damage");
									var quantityObj = document.getElementById(preFix+"_quantity");

									var nor = norObj.value*1;
									var damage = damageObj.value*1;
									var package_damage = packageDamageObj.value*1;
									var quantity = quantityObj.value*1;
									
									//先检查是否为数字
									if ( !isNum(nor)||nor<0 )
									{
										alert("数字格式不正确");
										norObj.style.background = "#FFD2D2";
										canBeSubmit = false;
		
									}
									else if ( !isNum(damage)||damage<0 )
									{
										alert("数字格式不正确");
										damageObj.style.background = "#FFD2D2";
										canBeSubmit = false;
						
									}
									else if ( !isNum(package_damage)||package_damage<0 )
									{
										alert("数字格式不正确");
										packageDamageObj.style.background = "#FFD2D2";
										canBeSubmit = false;
		
									}
									
									//再检查完好件和残损件之和不能大于理论退件总数
									if ( nor+damage+package_damage!=quantity||nor+damage+package_damage==0  )
									{
										norObj.style.background = "#FFD2D2";
										damageObj.style.background = "#FFD2D2";
										packageDamageObj.style.background = "#FFD2D2";
										notMatch = true;
									}
									if (nor+damage+package_damage==0)
									{
										norObj.style.background = "#FFD2D2";
										damageObj.style.background = "#FFD2D2";
										packageDamageObj.style.background = "#FFD2D2";
										return(false);
									}
						} 
						else
						{
							for (var i=0; i<thrForm.rpsi_id.length; i++)
							{
									//检查每一行的值是否合格
									var preFix = thrForm.rpsi_id[i].value;
									var norObj = document.getElementById(preFix+"_nor");
									var damageObj = document.getElementById(preFix+"_damage");
									var packageDamageObj = document.getElementById(preFix+"_package_damage");
									var quantityObj = document.getElementById(preFix+"_quantity");
									
									var nor = norObj.value*1;
									var damage = damageObj.value*1;
									var package_damage = packageDamageObj.value*1;
									var quantity = quantityObj.value*1;
									
									//先检查是否为数字
									if ( !isNum(nor)||nor<0 )
									{
										alert("数字格式不正确");
										norObj.style.background = "#FFD2D2";
										canBeSubmit = false;
										break;
									}
									else if ( !isNum(damage)||damage<0 )
									{
										alert("数字格式不正确");
										damageObj.style.background = "#FFD2D2";
										canBeSubmit = false;
										break;
									}
									else if ( !isNum(package_damage)||package_damage<0 )
									{
										alert("数字格式不正确");
										packageDamageObj.style.background = "#FFD2D2";
										canBeSubmit = false;
										break;
									}
									
									//再检查完好件和残损件之和不能大于理论退件总数
									if ( nor+damage+package_damage!=quantity  )
									{
										norObj.style.background = "#FFD2D2";
										damageObj.style.background = "#FFD2D2";
										packageDamageObj.style.background = "#FFD2D2";
										notMatch = true;
									}
									
									if (nor+damage+package_damage==0)
									{
										norObj.style.background = "#FFD2D2";
										damageObj.style.background = "#FFD2D2";
										packageDamageObj.style.background = "#FFD2D2";
										return(false);
									}									
							}
						}
						
						//退货数据不正确，需要填写备注
						if (notMatch) 
						{
							$("#note_tr").show();
							if ( $("#note").val()=="" )
							{
								alert("请填写退货异常数据备注");
								return(false);
							}
						}
						
				
						if (canBeSubmit)
						{
							$.blockUI.defaults = { 
								css: { 
									padding:        '10px',
									margin:         0,
									width:          '200px', 
									top:            '45%', 
									left:           '40%', 
									textAlign:      'center', 
									color:          '#000', 
									border:         '3px solid #aaa',
									backgroundColor:'#fff'
								},
								
								// 设置遮罩层的样式
								overlayCSS:  { 
									backgroundColor:'#000', 
									opacity:        '0.8' 
								},
						
							centerX: true,
							centerY: true, 
							
								fadeOut:  2000
							};
							
							$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:000000">正在登记，请稍后......</span>'});
							return(true);
						}
						else
						{
							return(false);
						}
												
				}
				,

		  dataType:"html",
		  success:       // post-submit callback  
				function(msg){
					if (msg=="ok")
					{
						parent.location.reload();
						$.artDialog.close();
					}
					else
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">登记发送失败！</span>' });
					}
				}
		       
	};
	
   $('#add_handle_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}


function isNum(keyW)
{
	var reg=  /^(-[0-9]|[0-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 


</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="add_handle_form" id="add_handle_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/SigReturnProducts.action" >
<input type="hidden" name="rp_id" value="<%=rp_id%>">
<input type="hidden" name="oid" value="<%=oid%>">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
	
	<table width="631" border="0" cellpadding="5" cellspacing="0">
      

      <tr>
        <td align="center" style="font-family:'黑体';font-size:25px;">退货单号：<%=rp_id%></td>
      </tr>
      <tr>
        <td >
		
	<br>

			 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">主商品信息</legend>
<%
DBRow returnProducts[] = orderMgr.getReturnProductItemsByRpId(rp_id,null);
%>
<table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
<%
for (int i=0; i<returnProducts.length; i++)
{
%>
  <tr>
    <td width="69%" style="font-family:Arial;font-weight:bold"><%=returnProducts[i].getString("p_name")%></td>
    <td width="31%" style="font-family:Arial;font-weight:bold"><%=returnProducts[i].getString("quantity")%> <%=returnProducts[i].getString("unit_name")%> </td>
  </tr>
<%
}
%>
</table>
			 </fieldset>	
		
		</td>
      </tr>
      <tr>
        <td width="50%" ><br>
          
          
          <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
  <legend style="font-size:15px;font-weight:normal;color:#999999;">散件商品信息</legend>
  <%
DBRow returnSubProducts[] = orderMgr.getReturnProductSubItemsByRpId( rp_id,null);
%>
  <table width="97%" border="0" align="center" cellpadding="3" cellspacing="1">
      <tr>
      <td height="20" bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">商品名</td>
      <td align="center" bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">数量</td>
      <td align="center" bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">单位</td>
      <td bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">完好</td>
      <td bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">功能残损</td>
      <td bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">外观残损</td>
      </tr>
	
  <%
for (int i=0; i<returnSubProducts.length; i++)
{
%>
<input name="rpsi_id" id="rpsi_id" type="hidden" value="<%=returnSubProducts[i].getString("rpsi_id")%>">
<input name="<%=returnSubProducts[i].getString("rpsi_id")%>_quantity" id="<%=returnSubProducts[i].getString("rpsi_id")%>_quantity" type="hidden" value="<%=returnSubProducts[i].getString("quantity")%>">
<input name="<%=returnSubProducts[i].getString("rpsi_id")%>_pid" id="<%=returnSubProducts[i].getString("rpsi_id")%>_pid" type="hidden" value="<%=returnSubProducts[i].getString("pid")%>">
    <tr>
      <td width="41%" height="25" style="font-family:Arial;font-weight:bold"><%=returnSubProducts[i].getString("p_name")%></td>
      <td width="9%" align="center" style="font-family:Arial;font-weight:bold"><%=returnSubProducts[i].getString("quantity")%></td>
      <td width="9%" align="center" style="font-family:Arial;font-weight:bold"><%=returnSubProducts[i].getString("unit_name")%></td>
      <td width="15%" style="font-family:Arial;font-weight:bold">
        <input name="<%=returnSubProducts[i].getString("rpsi_id")%>_nor" id="<%=returnSubProducts[i].getString("rpsi_id")%>_nor" type="text" style="width:50px;" value="0">      </td>
      <td width="13%" style="font-family:Arial;font-weight:bold"><input name="<%=returnSubProducts[i].getString("rpsi_id")%>_damage" id="<%=returnSubProducts[i].getString("rpsi_id")%>_damage" type="text" style="width:50px;" value="0"></td>
      <td width="13%" style="font-family:Arial;font-weight:bold"><input name="<%=returnSubProducts[i].getString("rpsi_id")%>_package_damage" id="<%=returnSubProducts[i].getString("rpsi_id")%>_package_damage" type="text" style="width:50px;" value="0"></td>
    </tr>
  <%
}
%>
  </table>
		    </fieldset>	</td>
    </tr>
      <tr id="note_tr" style="display:none">
        <td style="padding-left:10px;"><strong>退货异常数据备注：</strong><br>
            <textarea name="note" id="note" style="width:300px;height:100px;"></textarea>        </td>
      </tr>
      <tr>
        <td >&nbsp;</td>
    </tr>
</table>
	
	
	 </td>
  </tr>
  <tr>
    <td align="right" valign="middle" class="win-bottom-line">
	<%
	if (adminLoggerBean.getPs_id()==detail.get("ps_id",0l))
	{
	%>
      <input name="Submit" type="submit" class="normal-green-long" value="登记" >
	<%
	}
	else
	{
	%>
	  <img src="../imgs/product/warring.gif" width="16" height="15"> 不在同一个仓库，禁止登记 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%
	}
	%>
      <input name="cancel" type="button" class="normal-white" value="取消" onClick="$.artDialog.close();">
  </td>
  </tr>
</table>
</form>
</body>

</html>