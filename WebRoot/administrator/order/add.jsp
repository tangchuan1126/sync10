<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.PayTypeKey,com.cwc.app.api.OrderMgr"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%
long warranty_oid = StringUtil.getLong(request,"warranty_oid");
%>
<jsp:useBean id="currencyKey" class="com.cwc.app.key.CurrencyKey"/>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>

<script>
$().ready(function() {
	ajaxCartForm();
});

var clickCancel = 0;
//把表单ajax化 
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   //beforeSubmit:  // pre-submit callback    
			//	function(){
			//		loadCart();
			//	}	 
		  // ,  
		  beforeSend:function(jqXHR, settings)
		  {
						if (clickCancel==1)
						{
							return(false);
						}
						
						var theForm = document.add_handle_form;

						if(theForm.manual_order_type.value==1) 
						{
							if (theForm.manual_order_rel_oid.value=="")
							{
								alert("请填写相关单号");
								return(false);
							}
							
						}
						
						if(theForm.order_source.value=="") 
						{
							alert("请选择订单来源");
							return(false);
						}	
						else if(theForm.pay_type.value==-1) 
						{
							alert("请选择支付类型");
							return(false);
						}
						else if(theForm.pay_type.value==<%=PayTypeKey.Wire%>&&$("#pay_info_sendmoney_account").val()=="") 
						{
							alert("请填写汇款帐号");
							return(false);
						}
						else if(theForm.pay_type.value==<%=PayTypeKey.Check%>&&$("#pay_info_check_account").val()=="") 
						{
							alert("请填写支票号码");
							return(false);
						}
						else if( (theForm.pay_type.value==<%=PayTypeKey.CreditCard%>||theForm.pay_type.value==<%=PayTypeKey.PayPal%>) &&$("#txn_id").val()=="") 
						{
							alert("请填写Transaction ID");
							return(false);
						}
						else if( theForm.pay_type.value==<%=PayTypeKey.ParentOrder%>&&$("#parentoid").val()=="") 
						{
							alert("请填写父单号");
							return(false);
						}
						else if(theForm.business.value==0)  
						{
							alert("请选择收款帐号");
							return(false);
						}
						else if (theForm.client_id.value=="")
						{
							alert("请填写付款人邮件");
							return(false);
						}
						else if (theForm.mc_gross.value=="")
						{
							alert("请填写付款金额");
							return(false);
						}
						else if ( !isNum(theForm.mc_gross.value) )
						{
							alert("请正确填写付款金额数字");
							return(false);
						}
						else if (theForm.mc_currency.value=="")
						{
							alert("请选择货币类型");
							return(false);
						}	
						else if (theForm.quantity.value=="")
						{
							alert("请填写购买数量");
							return(false);
						}
						else if ( !isNum(theForm.quantity.value) )
						{
							alert("请正确填写购买数量");
							return(false);
						}				
						else if (theForm.ccid.value==0)
						{
							alert("请选择国家");
							return(false);
						}
						else if (theForm.pro_id.value==""||theForm.pro_id.value==0)
						{
							alert("请选择地区");
							return(false);
						}
						else if (theForm.address_name.value=="")
						{
							alert("请填写Name");
							return(false);
						}
						else if (theForm.address_street.value=="")
						{
							alert("请填写Street");
							return(false);
						}
						else if (theForm.address_city.value=="")
						{
							alert("请填写City");
							return(false);
						}
						else if (theForm.address_state.value=="")
						{
							alert("请填写State");
							return(false);
						}											
						else if (theForm.address_zip.value=="")
						{
							alert("请填写Zip");
							return(false);
						}											
						else
						{
							//组织支付信息(暂时不起作用，以后调试)
							var pay_info="";
							
							if(theForm.pay_type.value==<%=PayTypeKey.Wire%>) 
							{
								pay_info = "汇款帐号："+$("#pay_info_sendmoney_account").val();
							}
							else if(theForm.pay_type.value==<%=PayTypeKey.Check%>) 
							{
								pay_info = "支票号码："+$("#pay_info_check_account").val();
							}
							//theForm.pay_info.value = "卡卡";
							
							return(true);
						}
		  }
		  ,
   		   beforeSubmit:  // pre-submit callback    
				function(formData, jqForm, options)
				{			
				}	
			,
		   success:       // post-submit callback  
				function(responseText, statusText)
				{			

					if ( responseText.indexOf("oid")>=0 )
					{
						var oid = responseText.split("-")[1];
						parent.tb_show('抄单 [订单号：'+oid+']',"new_record_order.html?oid="+oid+"&TB_iframe=true&height=500&width=850",false);
					}
					else if ( responseText.indexOf("txnid")>=0 )
					{
						var txn_id = responseText.split("-")[1];
						alert("TransID:"+txn_id+" 订单已经存在，不能重复创建！");
					}
					else
					{
						alert("创建订单失败！");
					}
				}
		       
	};
	

   $('#add_handle_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}


function isNum(keyW)
{
	var reg=  /^(-[0-9]|[0-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
 
function getDetailOrder(oid)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailOrderJSON.action",
			{oid:oid},
				function callback(data)
				{
					if (data=="")
					{
						alert("单号不存在！");
						
						$("#client_id").val("");
						$("#ccid").setSelectedValue(0);
						$("#pro_id").setSelectedValue(0);
						$("#address_name").val("");
						$("#address_street").val("");
						$("#address_city").val("");
						$("#address_state").val("");
						$("#address_zip").val("");
						$("#tel").val("");
					}
					else
					{
						$("#client_id").val(data.client_id);
						$("#ccid").setSelectedValue(data.ccid);
						getStorageProvinceByCcid(data.ccid,data.pro_id);
						$("#address_name").val(data.address_name);
						$("#address_street").val(data.address_street);
						$("#address_city").val(data.address_city);
						$("#address_state").val(data.address_state);
						$("#address_zip").val(data.address_zip);
						$("#tel").val(data.tel);
					}
				}
			);
}

//国家地区切换
function getStorageProvinceByCcid(ccid,pro_id)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("请选择......","");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
						
						if (pro_id>0)
						{
							$("#pro_id").setSelectedValue(pro_id);
						}
					}		

					if (ccid>0)
					{
						$("#pro_id").addOption("Others","-1");
					}
						
				}
		);
}

function showPayInfoSendmoneyAccountTable()
{
	$("#pay_info_sendmoney_account_table").show();
	$("#pay_info_check_account_table").hide();
	$("#txn_id_table").hide();
	$("#parentoid_table").hide();
}

function showPayInfoCheckAccountTable()
{
	$("#pay_info_sendmoney_account_table").hide();
	$("#pay_info_check_account_table").show();
	$("#txn_id_table").hide();
	$("#parentoid_table").hide();
}

function showTxnIdTable()
{
	$("#pay_info_sendmoney_account_table").hide();
	$("#pay_info_check_account_table").hide();
	$("#txn_id_table").show();
	$("#parentoid_table").hide();
}

function showParentoidTable()
{
	$("#pay_info_sendmoney_account_table").hide();
	$("#pay_info_check_account_table").hide();
	$("#txn_id_table").hide();
	$("#parentoid_table").show();
}

function changePayType(val)
{
	if (val==<%=PayTypeKey.Wire%>)
	{
		showPayInfoSendmoneyAccountTable();
	}
	else if (val==<%=PayTypeKey.Check%>)
	{
		showPayInfoCheckAccountTable();
	}
	else if (val==<%=PayTypeKey.PayPal%>||val==<%=PayTypeKey.CreditCard%>)
	{
		showTxnIdTable();
	}
	else if (val==<%=PayTypeKey.ParentOrder%>)
	{
		showParentoidTable();
	}
	else if (val==<%=PayTypeKey.Cash%>)
	{
		$("#pay_info_check_account_table").hide();
		$("#pay_info_sendmoney_account_table").hide();
		$("#txn_id_table").hide();
		$("#parentoid_table").hide();
	}
}

function changeManualOrderType(val)
{
	if (val==0)
	{
		$("#manual_order_rel_oid_table").hide();
	}
	else if (val==1)
	{
		$("#manual_order_rel_oid_table").show();
	}
	else
	{
		$("#manual_order_rel_oid_table").hide();
	}
}
</script>
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="add_handle_form" id="add_handle_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/addPOrderByHandle.action" >
<input type="hidden"  name="pay_info" id="pay_info" >
<input type="hidden"  name="manual_order_type" id="manual_order_type" value="0">

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>  
    <td align="center" valign="top">
	


<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:35px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">订单信息</legend>

<table width="100%" border="0" cellspacing="5" cellpadding="0">

        <tr>
          <td align="right" class="STYLE2">订单来源</td>
          <td>&nbsp;</td>
          <td><select name="order_source" id="order_source" >
            <option value="">请选择...</option>
            <option value="<%=OrderMgr.ORDER_SOURCE_PAY%>"><%=OrderMgr.ORDER_SOURCE_PAY%></option>
			<option value="<%=OrderMgr.ORDER_SPLIT%>"><%=OrderMgr.ORDER_SPLIT%></option>
			<option value="<%=OrderMgr.ORDER_LOCAL_BUY%>"><%=OrderMgr.ORDER_LOCAL_BUY%></option>
			<option value="<%=OrderMgr.ORDER_DIRECT_BUY%>"><%=OrderMgr.ORDER_DIRECT_BUY%></option>
			
			<option value="<%=OrderMgr.ORDER_SOURCE_EBAY%>"><%=OrderMgr.ORDER_SOURCE_EBAY%></option>
			<option value="<%=OrderMgr.ORDER_AMAZON%>"><%=OrderMgr.ORDER_AMAZON%></option>
			<option value="<%=OrderMgr.ORDER_ECRATER%>"><%=OrderMgr.ORDER_ECRATER%></option>
			<option value="<%=OrderMgr.ORDER_BONANZA%>"><%=OrderMgr.ORDER_BONANZA%></option>
          </select></td>
          <td width="30%" rowspan="6" align="left" valign="top" style="border:1px #cccccc solid;background:#f8f8f8;padding:10px;line-height:17px;color:#666666"><img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"><span style="font-weight:bold;font-size:12px;"> 温馨提示</span><br>
            <br>
            点击“下一步”，代表订单已经生成，您可以接着抄单，或者稍后再抄单。注意请勿重复创建订单。</td>
        </tr>
        <tr>
          <td align="right" class="STYLE2">支付类型</td>
          <td>&nbsp;</td>
          <td><select name="pay_type" id="pay_type" onChange="changePayType(this.value)">
            <option value="-1">请选择...</option>
          <option value="<%=PayTypeKey.Wire%>"><%=payTypeKey.getStatusById(PayTypeKey.Wire)%></option>
		  <option value="<%=PayTypeKey.Cash%>"><%=payTypeKey.getStatusById(PayTypeKey.Cash)%></option>
		  <option value="<%=PayTypeKey.Check%>"><%=payTypeKey.getStatusById(PayTypeKey.Check)%></option>
		  <option value="<%=PayTypeKey.PayPal%>"><%=payTypeKey.getStatusById(PayTypeKey.PayPal)%></option>
		  <option value="<%=PayTypeKey.CreditCard%>"><%=payTypeKey.getStatusById(PayTypeKey.CreditCard)%></option>
		  <option value="<%=PayTypeKey.ParentOrder%>"><%=payTypeKey.getStatusById(PayTypeKey.ParentOrder)%></option>
          </select>
            <br>
            <table width="100%" border="0" cellspacing="0" cellpadding="3" id="pay_info_sendmoney_account_table" style="display:none" >
              <tr>
                <td width="15%" align="left" valign="middle" bgcolor="#eeeeee">汇款帐号</td>
                <td width="85%" bgcolor="#eeeeee"><input name="pay_info_sendmoney_account" type="text" id="pay_info_sendmoney_account" size="40"></td>
              </tr>
            </table>
			<table width="100%" border="0" cellspacing="0" cellpadding="3" id="pay_info_check_account_table" style="display:none">
              <tr>
                <td width="15%" align="left" valign="middle" bgcolor="#eeeeee">支票号码</td>
                <td width="85%" bgcolor="#eeeeee"><input name="pay_info_check_account" type="text" id="pay_info_check_account" size="40"></td>
              </tr>
            </table>
			<table width="100%" border="0" cellspacing="0" cellpadding="3" id="txn_id_table" style="display:none">
              <tr>
                <td width="15%" align="left" valign="middle" bgcolor="#eeeeee">Transaction ID</td>
                <td width="85%" bgcolor="#eeeeee"><input name="txn_id" type="text" id="txn_id" size="40"></td>
              </tr>
            </table>
			<table width="100%" border="0" cellspacing="0" cellpadding="3" id="parentoid_table" style="display:none">
              <tr>
                <td width="15%" align="left" valign="middle" bgcolor="#eeeeee">父单号</td>
                <td width="85%" bgcolor="#eeeeee"><input name="parentoid" type="text" id="parentoid" size="40" onChange="getDetailOrder(this.value)"></td>
              </tr>
            </table>
						</td>
        </tr>

        <tr> 
	
    <td width="12%" align="right"><span class="STYLE2">收款帐号</span></td>
      <td width="3%">&nbsp;</td>
      <td>
	  <select name="business">
	  <option value="0">选择帐号...</option>
                <%
String businessl[] = systemConfig.getStringConfigValue("business").split(",");
for (int i=0; i<businessl.length; i++)
{
%>
                <option value="<%=businessl[i]%>"> 
                <%=businessl[i]%>                </option>
                <%
}
%>
      </select>	  </td>
    </tr>
  <tr> 
      <td align="right" class="STYLE2">付款人邮件</td>
      <td>&nbsp;</td>
    <td><input name="client_id" type="text" id="client_id" size="40"></td>
    </tr>

  <tr>
    <td align="right" class="STYLE2">付款金额</td>
    <td>&nbsp;</td>
    <td width="55%"><input name="mc_gross" type="text" id="mc_gross" size="15">
	&nbsp;
<select name="mc_currency">
        <option value="">选择货币...</option>
        <%
ArrayList currency = currencyKey.getCurrency();
for (int i=0; i<currency.size(); i++)
{
%>
        <option value="<%=currencyKey.getCurrencyById( currency.get(i).toString() )%>"><%=currency.get(i)%></option>
<%
}
%>
      </select>	  </td>
    <td width="0%">&nbsp;</td>
  </tr>
    <tr> 
      <td align="right" class="STYLE2">购买数量</td>
      <td>&nbsp;</td>
    <td><input name="quantity" type="text" id="quantity" size="15"></td>
</table>
</fieldset>	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
			 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">递送信息</legend>
	
	<table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr> 
    <td width="12%" align="right" class="STYLE2">Country</td>
    <td width="2%">&nbsp;</td>
    <td width="56%">
	<%
	DBRow countrycode[] = orderMgr.getAllCountryCode();
	String selectBg="#ffffff";
	String preLetter="";
	%>
      <select name="ccid" id="ccid" onChange="getStorageProvinceByCcid(this.value,0)">
	  <option value="0">请选择...</option>
	  <%
	  for (int i=0; i<countrycode.length; i++)
	  {
	  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
		{
			if (selectBg.equals("#eeeeee"))
			{
				selectBg = "#ffffff";
			}
			else
			{
				selectBg = "#eeeeee";
			}
		}  	
		
		preLetter = countrycode[i].getString("c_country").substring(0,1);
		

	  %>
	    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
	  <%
	  }
	  %>
      </select>    </td>
    <td width="30%">&nbsp;</td>
    </tr>
	

	
    <tr>
      <td align="right" class="STYLE2">State</td>
      <td>&nbsp;</td>
      <td>
	  <select name="pro_id" id="pro_id"  >
	  </select>
	  </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
    <td align="right" class="STYLE2">Name<br></td>
    <td>&nbsp;</td>
    <td><input name="address_name" type="text" id="address_name" size="40"></td>
    <td>&nbsp;</td>
   </tr>
   <tr>
    <td align="right" class="STYLE2">Street<br></td>
    <td>&nbsp;</td>
    <td><input name="address_street" type="text" id="address_street" size="40"></td>
    <td>&nbsp;</td>
   </tr>
    <tr>
    <td align="right" class="STYLE2">City<br></td>
    <td>&nbsp;</td>
    <td><input name="address_city" type="text" id="address_city" size="40"></td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td align="right" class="STYLE2">State<br></td>
    <td>&nbsp;</td>
    <td><input name="address_state" type="text" id="address_state" size="40"></td>
    <td>&nbsp;</td>
    </tr>
  <tr> 
    <td align="right" class="STYLE2">Zip<br></td>
    <td>&nbsp;</td>
    <td><input name="address_zip" type="text" id="address_zip" size="40"></td>
    <td>&nbsp;</td>
  </tr>

    <tr>
      <td align="right"><strong>Tel</strong></td>
      <td>&nbsp;</td>
      <td colspan="2"><input name="tel" type="text" id="tel" size="40"></td>
    </tr>
</table>
	</fieldset>	</td>
  </tr>
  <tr>
    <td style="padding-top:10px;"><table width="98%" border="0" cellspacing="3" cellpadding="2">
      <tr>
        <td width="211">&nbsp;</td>
        <td width="1009">        </td>
      </tr>
    </table></td>
  </tr>
</table>


	
	
	 </td>
  </tr>
  <tr>
    <td align="right" valign="middle" class="win-bottom-line"><label>
      <input name="Submit" type="submit" class="normal-green-long" value="下一步" >
      <input name="cancel" type="button" class="normal-white" value="取消" onClick="clickCancel=1;parent.closeTBWin();void(0);">
    </label></td>
  </tr>
</table>
</form>
</body>
</html>
