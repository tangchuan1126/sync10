<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String st = StringUtil.getString(request,"st");
String en = StringUtil.getString(request,"en");

String business = StringUtil.getString(request,"business");
int product_type_int = StringUtil.getInt(request,"product_type_int");


DBRow orderDate[];
DBRow ordersCount;
DBRow normalOrdersCount ;
DBRow pendingOrdersCount ;
DBRow archiveOrdersCount ;
DBRow notPrintOrdersCount ;
DBRow statPrintedOrdersCount ;
DBRow statDeliveryOrdersCount ;

if (st.equals("")||en.equals(""))
{
 orderDate = new DBRow[0];
 ordersCount = new DBRow();
 normalOrdersCount = new DBRow();
 pendingOrdersCount = new DBRow();
 archiveOrdersCount = new DBRow();
 notPrintOrdersCount = new DBRow();
 statPrintedOrdersCount = new DBRow();
 statDeliveryOrdersCount = new DBRow();
}
else
{
 orderDate = orderMgr.getTimeRangeOrders( st, en, business, product_type_int);
 ordersCount = orderMgr.getStatOrdersCount( st, en, business, product_type_int);
 normalOrdersCount = orderMgr.getStatNormalOrdersCount( st, en, business, product_type_int);
 pendingOrdersCount = orderMgr.getStatPendingOrdersCount( st, en, business, product_type_int);
 archiveOrdersCount = orderMgr.getStatArchiveOrdersCount( st, en, business, product_type_int);
 notPrintOrdersCount = orderMgr.getStatNotPrintOrdersCount( st, en, business, product_type_int);
 statPrintedOrdersCount = orderMgr.getStatPrintedOrdersCount( st, en, business, product_type_int);
 statDeliveryOrdersCount = orderMgr.getStatDeliveryOrdersCount( st, en, business, product_type_int);

}
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>



<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--

.zebraTable tr td a:link {
	color:#000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:visited {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:hover {
	color: #000000;
	text-decoration: underline;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:active {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}



-->
</style>

<script>

function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.st.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.en.value = date;
}


</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable();">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 订单管理 »   订单监控</td>
  </tr>
</table>
<br>
<table width="98%" height="29" border="0" align="center" cellpadding="1" cellspacing="0">
  <tr align="left" valign="middle"> 
    <td width="77%"><table width="100%" border="0" cellspacing="0" cellpadding="1">
        <form name="filterForm" action="" method="post">

          <tr> 
            <td> 
<%
TDate tDate = new TDate();
tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

String input_st_date,input_en_date;
if ( st.equals("") )
{	
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}
else
{	
	input_st_date = st;
}


if ( en.equals("") )
{	
	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
}
else
{	
	input_en_date = en;
}
%>
			开始 
              <input name="st" type="text" id="st" size="9" value="<%=input_st_date%>" readonly> 
              <script language="JavaScript" type="text/javascript" >
    <!--
    	stfooCalendar = new dynCalendar('stfooCalendar', 'stcalendarCallback', '../js/images/');
    //-->
    </script> 
              &nbsp; &nbsp; &nbsp;结束 
              <input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  readonly> 
              <script language="JavaScript" type="text/javascript">
    <!--
    	enfooCalendar = new dynCalendar('enfooCalendar', 'encalendarCallback', '../js/images/');
    //-->
	</script>
			
			
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
      
			  
			   <select name="business">
                <%
String businessl[] = systemConfig.getStringConfigValue("business").split(",");
for (int i=0; i<businessl.length; i++)
{
%>
                <option value="<%=businessl[i]%>" <%=business.equals(businessl[i])?"selected":""%>> 
                <%=businessl[i]%>                </option>
                <%
}
%>
              </select>
			  
			  			   <select name="product_type_int">
						   <option value="0">全部发票</option>
                <%
String product_type[] = systemConfig.getStringConfigValue("product_type").split(",");
String keyv[] = null;
for (int kk=0; kk<product_type.length; kk++)
{
	keyv = product_type[kk].split("=");
%>
                <option value="<%=keyv[1]%>" <%=product_type_int==Integer.parseInt(keyv[1])?"selected":""%>> 
                <%=keyv[0]%>
                </option>
                <%
}
%>
              </select>
			  			   <label>
			  			   <input name="Submit" type="submit" class="long-long-button-stat" value="  重新统计  ">
		      </label></td>
          </tr>
        </form>
    </table></td>
  </tr>
</table>

<br>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
<thead>
<tr>
    <th style="text-align: center;"  class="left-title">日期</th>
    <th  style="text-align: center;"   class="right-title">订单总数</th>
    <th  style="text-align: center;"  class="right-title"  >未抄单</th>
    <th  style="text-align: center;"  class="right-title" >疑问订单</th>
    <th  style="text-align: center;"  class="right-title" >归档订单</th>
    <th  style="text-align: center;" class="right-title" >未打印</th>
    <th  style="text-align: center;"  class="right-title" >打印待发货</th>
    <th style="text-align: center;"   class="right-title">已发货</th>
</tr>
</thead>
  <%

  for (int i=0; i<orderDate.length; i++)
  {

  %>

  <tr>
    <td height="40" align="center" valign="middle"  > <%=orderDate[i].getString("d")%></td>
    <td align="center" valign="middle"  > <a  href="ct_order_auto_reflush.html?val=&st=<%=orderDate[i].getString("d")%>&en=<%=orderDate[i].getString("d")%>&p=1&business=<%=business%>&handle=0&handle_status=-1&product_type_int=<%=product_type_int%>" target="_blank"><%=ordersCount.get(orderDate[i].getString("d"),0)%></a></td>
    <td align="center" valign="middle"  > <a href="ct_order_auto_reflush.html?val=&st=<%=orderDate[i].getString("d")%>&en=<%=orderDate[i].getString("d")%>&p=1&business=<%=business%>&handle=1&handle_status=0&product_type_int=<%=product_type_int%>" target="_blank"><%=normalOrdersCount.get(orderDate[i].getString("d"),0)%></a></td>
    <td align="center" valign="middle"  > <a href="ct_order_auto_reflush.html?val=&st=<%=orderDate[i].getString("d")%>&en=<%=orderDate[i].getString("d")%>&p=1&business=<%=business%>&handle=0&handle_status=1&product_type_int=<%=product_type_int%>" target="_blank"><%=pendingOrdersCount.get(orderDate[i].getString("d"),0)%></a></td>
    <td align="center" valign="middle"   ><a href="ct_order_auto_reflush.html?val=&st=<%=orderDate[i].getString("d")%>&en=<%=orderDate[i].getString("d")%>&p=1&business=<%=business%>&handle=0&handle_status=2&product_type_int=<%=product_type_int%>" target="_blank"><%=archiveOrdersCount.get(orderDate[i].getString("d"),0)%></a></td>
    <td align="center" valign="middle"  > <a href="ct_order_auto_reflush.html?val=&st=<%=orderDate[i].getString("d")%>&en=<%=orderDate[i].getString("d")%>&p=1&business=<%=business%>&handle=2&handle_status=0&product_type_int=<%=product_type_int%>" target="_blank"><%=notPrintOrdersCount.get(orderDate[i].getString("d"),0)%></a></td>
    <td align="center" valign="middle"  > <a href="ct_order_auto_reflush.html?val=&st=<%=orderDate[i].getString("d")%>&en=<%=orderDate[i].getString("d")%>&p=1&business=<%=business%>&handle=3&handle_status=-1&product_type_int=<%=product_type_int%>" target="_blank"><%=statPrintedOrdersCount.get(orderDate[i].getString("d"),0)%></a></td>
    <td align="center" valign="middle"  > <a href="ct_order_auto_reflush.html?val=&st=<%=orderDate[i].getString("d")%>&en=<%=orderDate[i].getString("d")%>&p=1&business=<%=business%>&handle=4&handle_status=-1&product_type_int=<%=product_type_int%>" target="_blank"><%=statDeliveryOrdersCount.get(orderDate[i].getString("d"),0)%></a></td>
  </tr>
  <%
  }
  

  %>
</table>
<br>
<br>
</body>

</html>