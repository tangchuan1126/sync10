<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ include file="../../include.jsp"%> 
<%
String cmd = StringUtil.getString(request,"cmd");
int search_field = StringUtil.getInt(request,"search_field");
String search_key = StringUtil.getString(request,"search_key");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

DBRow warrantyOrders[] = null;

if (cmd.equals("search"))
{
	if (search_field==0)
	{
		warrantyOrders = orderMgr.getWarrantyOrdersByPayerEmail(search_key,pc);
	}
}
else
{
	warrantyOrders = orderMgr.getAllWarrantyOrders(pc);
}

%> 

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单任务</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--

.zebraTable tr td a:link {
	color:#000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:visited {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:hover {
	color: #000000;
	text-decoration: underline;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:active {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}



-->
</style>

<script>
function detail(wo_id)
{	
	tb_show('质保单详细','detail_warranty_order.html?wo_id='+wo_id+'&height=400&width=750',false);
}

function checkForm(theForm)
{
	if (theForm.search_key.value=="")
	{
		alert("请填写关键词");
		return(false);
	}
	else
	{
		return(true);
	}
}

function linkWarranty(wo_id,oid)
{
	$.prompt(
	
	"<div id='title'>有偿质保单号</div><br /><input name='markWarrantiedOid' type='text' id='markWarrantiedOid' style='width:150px;'>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.markWarrantiedOid  == "")
						  {
							   alert("请填写有偿质保单号");
							   
								return false;
						  }
						  return true;
					} 

				}
		  ,

		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.link_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/LinkWarrantied.action";
						document.link_form.addition_money_oid.value = f.markWarrantiedOid;
						document.link_form.oid.value = oid;
						document.link_form.wo_id.value = wo_id;
						document.link_form.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable();">
<br>
<form name="link_form" method="post">
<input type="hidden" name="addition_money_oid" >
<input type="hidden" name="oid" >
<input type="hidden" name="wo_id" >
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 订单管理 »   质保订单</td>
  </tr>
</table>
<br>
<form name="form1" method="post" action="ct_warranty_order.html" onSubmit="return checkForm(this)">
<input type="hidden" name="cmd" value="search">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	关键词
	&nbsp;&nbsp;
	<input name="search_key" type="text" id="search_key" value="">
          &nbsp;&nbsp;

          <select name="search_field">
            <option value="0">顾客邮件</option>
            <option value="1">原始单号</option>
            <option value="2">编号</option>
          </select>
         
		 &nbsp;&nbsp;
          <input name="Submit2" type="submit" class="button_long_search" value="搜索">
      </td>
  </tr>
</table>
</form>
<br>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
<thead>
<tr>
<th width="8%"   class="right-title"  style="text-align: center;">编号</th>
 <th width="8%"   class="right-title"  style="text-align: center;">顾客</th>
 <th width="8%"   class="right-title"  style="text-align: center;">原始订单</th>
 <th width="8%"  class="right-title"  style="text-align: center;" >质保订单</th>
    <th width="8%"  class="right-title"  style="text-align: center;" >创建人</th>
    <th width="11%"  class="left-title" style="text-align: center;">创建日期</th>
    <th width="16%"  class="right-title"  style="text-align: center;" >质保原因</th>
    <th width="11%"  class="right-title"  style="text-align: center;" >支付方式</th>
    <th width="9%"  class="right-title"  style="text-align: center;" >状态</th>
    <th width="13%"   class="right-title" style="text-align: center;">&nbsp;</th>
</tr>
</thead>
<%

for (int i=0; i<warrantyOrders.length; i++)
{
DBRow order = orderMgr.getDetailPOrderByOid(warrantyOrders[i].get("oid",0l));
%>
  <tr>
 <td align="center" valign="middle"  ><%=warrantyOrders[i].getString("wo_id")%></td>
 <td align="center" valign="middle"  ><%=order.getString("client_id")%></td>
 <td align="center" valign="middle"  ><a target="_blank" href="ct_order_auto_reflush.html?val=<%=warrantyOrders[i].getString("oid")%>&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3"><%=warrantyOrders[i].getString("oid")%></a></td>
 <td align="center" valign="middle"  >
	<%
	if (warrantyOrders[i].get("dev_oid",0l)==0)
	{
	%>
	<a href="javascript:linkWarranty(<%=warrantyOrders[i].getString("wo_id")%>,<%=warrantyOrders[i].getString("oid")%>);"><img src="../imgs/add.png" width="16" height="16" border="0" ></a>
	<%
	}
	else
	{
		out.println("<a target='_blank' href='ct_order_auto_reflush.html?val="+warrantyOrders[i].getString("dev_oid")+"&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3'>"+warrantyOrders[i].get("dev_oid",0l)+"</a>");
	}
	%>	</td>
    <td align="center" valign="middle"  ><%=adminMgr.getDetailAdmin(warrantyOrders[i].get("creater",0l)).getString("account")%></td>
    <td height="40" align="center" valign="middle"  ><%=warrantyOrders[i].getString("create_date")%></td>
    <td align="center" valign="middle"  ><%=orderMgr.getWarrantyReasonById(warrantyOrders[i].get("reason",0))%></td>
    <td align="center" valign="middle"  >
	<%
	if (warrantyOrders[i].get("pay_type",0)>=0)
	{
		out.println(payTypeKey.getStatusById(warrantyOrders[i].get("pay_type",0)));
	}
	else
	{
		out.println("无需支付");
	}
	%>	</td>
    <td align="center" valign="middle"  >
	<%
	if (warrantyOrders[i].get("dev_oid",0l)==0)
	{
		out.println("<font color=red>待付款</font>");
	}
	else
	{
		out.println("<font color=green>完成</font>");
	}
	%>	</td>
    <td align="center" valign="middle"  >
      <input name="Submit" type="button" class="short-short-button" value="详细" onClick="tb_show('详细任务内容','detail_warranty_order.html?wo_id=<%=warrantyOrders[i].getString("wo_id")%>&TB_iframe=true&height=450&width=800',false);">
	  <%
	  if (warrantyOrders[i].get("pay_type",0)==payTypeKey.PayPal)
	  {
	  %>
	   &nbsp;&nbsp;
      <input name="Submit3" type="button" class="short-button-green" value="付款链接"> 
	  <%
	  }
	  %>
    </td>
  </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
    <input type="hidden" name="p">
	<input type="hidden" name="cmd" value="<%=cmd%>">
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
</body>

</html>