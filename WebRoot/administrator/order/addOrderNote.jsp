<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TracingOrderKey"%>
<%@ include file="../../include.jsp"%> 
 <jsp:useBean id="tracingOrderKey" class="com.cwc.app.key.TracingOrderKey"/>
<jsp:useBean id="handStatusleKey" class="com.cwc.app.key.HandStatusleKey"/>
<jsp:useBean id="productStatusKey" class="com.cwc.app.key.ProductStatusKey"/>
<jsp:useBean id="orderRateKey" class="com.cwc.app.key.OrderRateKey"/>
<jsp:useBean id="returnProductKey" class="com.cwc.app.key.ReturnProductKey"/>
<%@ page import="com.cwc.app.beans.AdminLoginBean,com.cwc.app.key.AfterServiceKey,com.cwc.app.api.OrderMgr"%>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add Order Note</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<style type="text/css">
*{margin:0px;padding:0px;}
.zebraTable td{border-bottom: 1px solid #dddddd;padding-left:10px;}
.zebraTable tr.alt td {background: #f9f9f9;}
.zebraTable tr.over td{background: #E6F3C5;}
</style>
<%
	//oid,handle,after_service_status,handle_status,product_status,buyer_ip
long oid = StringUtil.getLong(request,"oid");
long handle = StringUtil.getLong(request,"handle");
long after_service_status = StringUtil.getLong(request,"after_service_status");
long handle_status = StringUtil.getLong(request,"handle_status");
long product_status = StringUtil.getLong(request,"product_status");
String buyer_ip = StringUtil.getString(request,"buyer_ip");
DBRow orderNotes[] = orderMgr.getPOrderNoteByOid(oid);
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
%>
<script type="text/javascript">
	var url = "oid=" + <%= oid%> +"&handle="+<%= handle%>+"&after_service_status="+<%= after_service_status%>+"&handle_status="+<%= handle_status%>;
		url += "&product_status="+<%= product_status%>+"&buyer_ip="+'<%= buyer_ip%>';
	jQuery(function($){
 	 	$(".opr").each(function(){
			var _this = $(this);
			if($.trim(_this.html()).length < 1){
				_this.html("&nbsp;")
			};
		})
	 
	})	
	 
	  function onLoadInitZebraTable(){
		$(".zebraTable tbody tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
		$(".zebraTable tbody tr:odd").addClass("alt");
	}
	function addOrderNote(){
		$.artDialog.open("add_note.html?"+url, {title: "添加Order Note",width:'420px',height:'280px',fixed:true, lock: true,opacity: 0.3});
		
	}
	function refreshWindow(){
		window.location.reload();
	}
	function modOrderNote(oid,on_id){
		$.artDialog.open("add_note.html?"+url+"&on_id="+on_id, {title: "修改Order Note",width:'420px',height:'280px',fixed:true, lock: true,opacity: 0.3});
	}
	function viewRefunding(rp_id){
		$.artDialog.open("return_product_view.html?rp_id="+rp_id, {title: "退货清单",width:'700px',height:'540px',fixed:true, lock: true,opacity: 0.3});
	}
	function modReturnProductList(rp_id,oid){
		$.artDialog.open("return_product_mod.html?rp_id="+rp_id+"&oid="+oid, {title: "修改退货清单",width:'700px',height:'540px',fixed:true, lock: true,opacity: 0.3});

 	}
		
 </script>
</head>
<body  onload = "onLoadInitZebraTable()">
  
 	<!-- 下面显示添加Order Note的页面 -->
  
  
 	<table  width="98%" border="0" align="center" cellpadding="1" cellspacing="0"   class="zebraTable" style="margin-left:3px;margin-top:5px;">
 		<thead>
 		     <tr> 
			  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">跟进人</th>
			  	<th width="17%" style="vertical-align: center;text-align: center;" class="right-title">跟进类型</th>
			  	<th width="18%" style="vertical-align: center;text-align: center;" class="right-title">跟进时间</th>
		        <th width="35%" style="vertical-align: center;text-align: center;" class="right-title">跟进内容</th>
		        <th width="23%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
		      </tr>
		 </thead>
		 <tbody width="98%" border="0" align="center" cellpadding="1" cellspacing="0"   class="zebraTable" style="margin-left:3px;margin-top:5px;">
		 	<%
		 		if(orderNotes != null && orderNotes.length > 0){
		 			for(int index = orderNotes.length-1 ; index >= 0 ; index-- ){
		 			DBRow row = orderNotes[index]; 
		 			String normalNoteBg = "";
		 			if (row.get("trace_type",0)==tracingOrderKey.RECORD_DOUBT_ORDER||row.get("trace_type",0)==tracingOrderKey.DOUBT_TRACE ){
						normalNoteBg = "#CC0099";
					}else if ( row.get("trace_type",0)==tracingOrderKey.RECORD_DOUBT_ADDRESS_ORDER||row.get("trace_type",0)==tracingOrderKey.DOUBT_ADDRESS_TRACE ){
						normalNoteBg = "#3366CC";
					}else if ( row.get("trace_type",0)==tracingOrderKey.RECORD_DOUBT_PAY_ORDER||row.get("trace_type",0)==tracingOrderKey.DOUBT_PAY_TRACE ){
						normalNoteBg = "#FF0000";
					}else if ( row.get("trace_type",0)==tracingOrderKey.LACKING_TRACE ){
						normalNoteBg = "#666666";
					}else if ( row.get("trace_type",0)==tracingOrderKey.RECORD_LACKING_ORDER ){
						normalNoteBg = "#666666";
					}else if ( row.get("trace_type",0)==tracingOrderKey.ORDER_COST_VERIFY ){
						normalNoteBg = "#339966";
					}else{
						normalNoteBg="#FF9900";
					}
		 	%>
		 		<tr style="height:25px;line-height:25px;">
		 			<td><%=  row.getString("account")%></td>
		 			<td>[<span style='color:<%= normalNoteBg%>'><%=tracingOrderKey.getTracingOrderKeyById(row.getString("trace_type"))%></span>]</td>
		 			<td><%=  row.getString("post_date")%></td>
		 			<td><%=  StringUtil.ascii2Html(row.getString("note"))%>&nbsp;&nbsp;</td>
		 			<td class="opr">
  		 			<%
						if (row.get("adid", 0l)==adminLoggerBean.getAdid()){
					%>
							<a href="javascript:modOrderNote(<%= oid%>,<%=row.getString("on_id")%>)"><img src="../imgs/modify.gif" width="12" height="12" border="0" alt="修改备注"></a> 
					<%
						}
					%>
		 			<%
					if ( (tracingOrderKey.NORMAL_REFUNDING==row.get("trace_type",0) || tracingOrderKey.NORMAL_WARRANTYING==row.get("trace_type",0) || tracingOrderKey.RETURN_PRODUCT == row.get("trace_type",0))&& row.get("rel_id",0l)>0 )
					{
					%>
						<!-- 根据集中特殊情况，显示相关追中连接 -->
						<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/return_product_list.html?cmd=search&key=<%=row.get("rel_id",0l)%>&search_mode=1' target="_blank">[&nbsp;<%=row.get("rel_id",0l) %>&nbsp;]</a>
					<%
		 			}
		 			else if(TracingOrderKey.CREATE_WARRANTY==row.get("trace_type",0))
		 			{
		 			%>
							<a href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_list.html?cmd=search&search_mode=1&key='<%=row.get("rel_id",0l) %>'" target="_blank" style="cursor: pointer;">[&nbsp;<%=row.get("rel_id",0l) %>&nbsp;]</a>
					<%
	 				}
					else if ( (tracingOrderKey.FREE_SHIPPING_FEE_WARRANTY==row.get("trace_type",0)||tracingOrderKey.FREE_WARRANTIED==row.get("trace_type",0)||tracingOrderKey.OTHERS==row.get("trace_type",0)||tracingOrderKey.WARRANTIED==row.get("trace_type",0))&&row.get("rel_id",0l)>0 )
					{
					%>
	 				<div  class="order-note-title">
	 					<span class="order-note-text">相关订单：<%=row.get("rel_id",0l)%></span>&nbsp;&nbsp;	
	 					<a  target="_blank" href="ct_order_auto_reflush.html?val=<%= row.get("rel_id",0l)%>&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3"><img src="../imgs/application-blue.png" width="14" height="14" align="absmiddle" border="0"></a>
 					</div>
	 				<%
	 				}
	 				%>
		 			 
		 			</td>
		 		</tr>
		 	<% 	
		 			}
		 			
		 			
		 		}else{
		 	%>
		 		<tr style="background:#E6F3C5;">
   						<td colspan="5" style="text-align:center;height:120px;">无数据</td>
   				 </tr>
		 	<% 		
		 		}
		 	%>
		 
		 </tbody>
 	 
 	</table>
</body>
</html>
