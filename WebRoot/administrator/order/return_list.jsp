<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ReturnProductKey"%>
<%@page import="com.fr.base.Inter"%>
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<jsp:useBean id="returnProductKey" class="com.cwc.app.key.ReturnProductKey"/>
<jsp:useBean id="returnProductStatusKey" class="com.cwc.app.key.ReturnProductStatusKey"/>
<%@ include file="../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%
String cmd = StringUtil.getString(request,"cmd");
long rp_id = StringUtil.getLong(request,"rp_id");

int status = StringUtil.getInt(request,"status");
int product_status = StringUtil.getInt(request,"product_status");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

DBRow returnInfo[];

if (cmd.equals("search"))
{
	returnInfo = orderMgr.searchReturnProductByRpId(rp_id,pc);
}
else if(cmd.equals("filter"))
{
	returnInfo = productReturnOrderMgrZJ.filterReturnProduct(status,product_status,pc);
}
else
{
	returnInfo = orderMgr.getAllReturnProduct(pc);
}

WarrantyTypeKey warrantyTypeKey = new WarrantyTypeKey(); 

%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单任务</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<style type="text/css">

	
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}

</style>

<script>
 
 

function returnUploadFile(rp_id)
{
	//$.artDialog.open("../warranty_return/return_product_upload.html?rp_id="+rp_id , {title: '文件形式确认退货',width:'auto',height:'auto', lock: true,opacity: 0.3,fixed:true});
	
	$.artDialog.open("../warranty_return/return_product_upload.html?rp_id="+rp_id , {title: '文件形式确认退货',width:'400px',height:'330px', lock: true,opacity: 0.3,fixed:true});
}

function addNew(rp_id,oid)
{
	 var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/add_bill.html?rp_id="+rp_id+"&oid="+oid;
	 
	 $.artDialog.open(uri,{title: '创建质保账单',width:'1100px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}

function refreshWindow()
{
	window.location.reload();
}

function ref()
{
	window.location.reload();
}

function checkForm(thrForm)
{
	if (thrForm.rp_id.value=="")
	{
		alert("请填写退货单号");
		return(false);
	}
	else
	{
		return(true);
	}
}

function sigReturn(rp_id,oid)
{
	$.artDialog.open("return_sig.html?rp_id="+rp_id+"&oid="+oid,{title: '退货登记',width:'650px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}


function modReturnProductList(rp_id,oid)
{
	$.artDialog.open("return_product_mod.html?rp_id="+rp_id+"&oid="+oid,{title: '修改退货清单',width:'650px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}

function modReturnProductListAction(rp_id,oid,ps_id)
{
	document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/ModReturnProductList.action";
	document.listForm.rp_id.value = rp_id;
	document.listForm.oid.value = oid;
	document.listForm.ps_id.value = ps_id;
	document.listForm.submit();	
}

function search()
{
	document.search_form.submit();
}

<%
String qx;
String selectStr = "";
DBRow storageTreeRows[] = catalogMgr.getProductStorageCatalogTree();

selectStr += "<select name='ps_id' id='ps_id'>";
selectStr += "<option value='0'>选择仓库...</option>";

for ( int i=0; i<storageTreeRows.length; i++ )
{
	if ( storageTreeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
          selectStr += "<option value="+storageTreeRows[i].getString("id")+"> ";
          selectStr += Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageTreeRows[i].get("level",0))+qx;
		  selectStr += storageTreeRows[i].getString("title");
		  selectStr += "</option>";
          
    
}
selectStr += "</select>";
%>



function modWareHouse(rp_id)
{
	$.prompt(
	
	"<div id='title'>修改退货仓库</div><br><%=selectStr%>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{

						  if(f.ps_id == 0)
						  {
							   alert("请选择仓库");
							   
								return false;
						  }

						  
						  return true;
					}
				}
		  ,
   		  loaded:
				function ()
				{

				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{					
						document.mod_warehouse_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/ModReturnProductWareHouse.action";
						document.mod_warehouse_form.rp_id.value = rp_id;
						document.mod_warehouse_form.ps_id.value = f.ps_id;
						document.mod_warehouse_form.submit();
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 保存: "y", 取消: "n" }
	});
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload = "">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   退货管理</td>
  </tr>
</table>
<br>
<form name="mod_warehouse_form" method="post" >
<input type="hidden" name="rp_id" >
<input type="hidden" name="ps_id" >
</form>


<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#warranty_search">常用工具</a></li>
			<li><a href="#warranty_filter">高级搜索</a></li>
			<li><a href="#warranty_followup">需跟进</a></li>
		</ul>
		<div id="warranty_search">
			<form name="search_form" method="post" action="return_list.html" onSubmit="return checkForm(this)">
			<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
									 <input name="rp_id" type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="rp_id" onkeydown="if(event.keyCode==13)search()" value="<%=rp_id==0?"":rp_id%>"/>
									</div>
								</td>
								<td width="67">
									 
								</td>
							</tr>
						</table>
				</td>
	              <td width="45%"></td>
	              <td width="12%" align="right" valign="middle">
				  </td>
	            </tr>
	          </table>
			
			<input type="hidden" name="cmd" value="search">
			</form>
		</div>
		
		<div id="warranty_filter">
			<form name="form1" method="post" action="return_list.html">
			<input name="cmd" value="filter" type="hidden"/>
		    <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table>
							<tr>
								<td>
									<select id="status" name="status">
										<option value="-1">处理流程</option>
										<%
											ArrayList statusList = returnProductKey.getReturnProductStatus();
											
											for(int i =0;i<statusList.size();i++)
											{
										%>
										<option <%=statusList.get(i).toString().equals(String.valueOf(status))?"selected=\"selected\"":""%> value="<%=statusList.get(i)%>"><%=returnProductKey.getReturnProductStatusById(Integer.parseInt(statusList.get(i).toString()))%></option>
										<%
											} 
										%>
									</select>
								&nbsp;&nbsp;&nbsp;&nbsp;
									<select id="product_status" name="product_status">
										<option value="-1">货物状态</option>
										<%
											ArrayList productStatusList = returnProductStatusKey.getReturnProductsStatus();
											
											for(int i = 0;i<productStatusList.size();i++)
											{
										%>
										<option <%=productStatusList.get(i).toString().equals(String.valueOf(product_status))?"selected=\"selected\"":""%> value="<%=productStatusList.get(i)%>"><%=returnProductStatusKey.getReturnProductsStatusById(Integer.parseInt(productStatusList.get(i).toString())) %></option>
										<%
											}
										%>
									</select>
								&nbsp;&nbsp;&nbsp;
									<input type="submit" class="button_long_refresh" value="过滤"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</form>
		</div>
		<div id="warranty_followup">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<script>
$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>
<br>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" isNeed="true" class="zebraTable" id="stat_table" >
<thead>
<tr>
	<th width="20%" class="right-title"  style="text-align:center;">服务单号</th>
   
    <th width="25%"  class="right-title"  style="text-align: center;">客户反馈</th>
    <th width="35%"  class="right-title"  style="text-align: center;">退货情况</th>
    <th  width=""  class="right-title" style="text-align: center;">操作</th>
</tr>
</thead>
<%
String storageColor = "";
for (int i=0; i<returnInfo.length; i++)
{

%>
  <tr>
 <td align="center" valign="middle" style="font-weight:bold" nowrap="nowrap">
 			<fieldset  class="set" style="border:2px solid #993300;text-align:left">
		  		<legend>
					<span style="" class="title"><%=returnInfo[i].get("rp_id",0l)%></span>
					<strong>|</strong>
  					<span class="stateValue"><%=null!=adminMgrLL.getAdminById(returnInfo[i].getString("create_user"))?adminMgrLL.getAdminById(returnInfo[i].getString("create_user")).getString("employe_name"):""%></span>
					<strong>|</strong>
					<%=returnProductKey.getReturnProductStatusById(returnInfo[i].get("status",0))%>
				</legend>
  				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
  					<span class="alert-text stateName"><img  title="创建时间" src="../imgs/alarm-clock--arrow.png"/></span>
  					<span class="stateValue"><%=returnInfo[i].getString("create_date").substring(5,16)%></span>
  				</p>
  					<p style="margin-top:-12px;">原始订单:<a href="ct_order_auto_reflush.html?val=<%=returnInfo[i].getString("oid")%>&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3" target="_blank"><%=returnInfo[i].getString("oid")%></a></p>
				<%
					if(returnInfo[i].get("bill_id",0l)>0)
					{
				%>
					<p style="margin-top:-12px;">账单:<a target="_blank" href="bill_manager.html?comefrom=tool&searchkey=<%=returnInfo[i].get("bill_id",0l)%>"><%=returnInfo[i].get("bill_id",0l)%></a>
					
				<%
						if(returnInfo[i].get("warranty_oid",0l)>0){
							 %>
							&rarr; 订单:<a href="ct_order_auto_reflush.html?val=<%=returnInfo[i].getString("warranty_oid")%>&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3" target="_blank"><%=returnInfo[i].getString("warranty_oid")%></a>
							 <% 
						}
					}
				%>
				
				 </p>
		  	</fieldset>
  		 
 	</td>
 	  <%
    		DBRow productStoreCatalog = catalogMgr.getDetailProductStorageCatalogById(returnInfo[i].get("ps_id",0l));
    		if(productStoreCatalog==null)
    		{
    			productStoreCatalog = new DBRow();
    		}
    	%>
 	 
    <td align="center" valign="middle" nowrap="nowrap">
    	<fieldset class="set" style="padding-bottom:4px;text-align: left;">
			<legend style="font-size:12px;font-weight:normal;">反馈信息</legend>
				<%
					DBRow[] returnProductReason = productReturnOrderMgrZJ.getAllReturnReason(returnInfo[i].get("rp_id",0l));
				%>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<%
					for(int j=0;j<returnProductReason.length;j++)
					{
				%>
					<tr>
						<td style="border-width:0px;background-color:none;cursor:pointer;" nowrap="nowrap" align="left" title="<%=returnProductReason[j].getString("return_reason")%>"><%=productMgr.getDetailProductByPcid(returnProductReason[j].get("pc_id",0l)).getString("p_name")%></td>
						<td style="border-width:0px;background-color:none;" nowrap="nowrap" align="right""><%=returnProductReason[j].get("quantity",0f)%></td>
					</tr>
				<%
					}
				%>
				</table>
		</fieldset>
    </td>
    <td align="center" valign="middle">
    	<fieldset class="set" style="padding-bottom:4px;text-align: left;">
			<legend style="font-size:12px;">
			<!-- GZ | 李登云 | 状态 | 时间  -->
			<%=productStoreCatalog.getString("title")%>
			<%
	    		if(returnInfo[i].get("status",0)==ReturnProductKey.WAITING||returnInfo[i].get("status",0)==ReturnProductKey.WAITINGPRODUCT){
	    	%>
	    		<a href="javascript:modReturnProductList(<%=returnInfo[i].get("rp_id",0l)%>,<%=returnInfo[i].get("oid",0l)%>)"><img src="../imgs/application_edit.png" width="14" height="14" border="0" align="middle"></a>
	    	<%
	    		}
		 	%>
			| <%=returnProductStatusKey.getReturnProductsStatusById(returnInfo[i].get("product_status",0))%>
			
			<%if(returnInfo[i].getString("handle_user").length() > 0){ %>
			| <%=null!=adminMgrLL.getAdminById(returnInfo[i].getString("handle_user"))?adminMgrLL.getAdminById(returnInfo[i].getString("handle_user")).getString("employe_name").trim():""%>
			<%} %>
		 
			<%
				if ( returnInfo[i].getString("handle_date").equals(returnInfo[i].getString("create_date"))||returnInfo[i].getString("handle_date").equals(""))
				{
					 
				}
				else
				{
					out.println(" | "+ returnInfo[i].getString("handle_date").substring(5,16));
				}
		 %>
			</legend>
				<%
				DBRow returnProducts[] = orderMgr.getReturnProductItemsByRpId(returnInfo[i].get("rp_id",0l),null);
				%>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<%
				for (int q=0; q<returnProducts.length; q++)
				{
				%>
				  <tr>
				    <td style="border-width:0px;background-color:none;" nowrap="nowrap"><%=returnProducts[q].getString("p_name")%></td>
				    <td style="border-width:0px;background-color:none;" nowrap="nowrap" align="right""><%=returnProducts[q].getString("quantity")%> <%=returnProducts[q].getString("unit_name")%> </td>
				  </tr>
				<%
				}
				%>
				</table>
			 </fieldset>
    </td>
    <td align="center" valign="middle" style="padding-top: 5px;padding-bottom: 5px;">
      <input name="Submit" type="button" class="long-button" value="退货详细" onClick="location='return_detail.html?rp_id=<%=returnInfo[i].getString("rp_id")%>'">
      
	  <%
	  if (returnInfo[i].get("product_status",0)==returnProductStatusKey.Waiting)
	  {
	  %>
	  <tst:authentication bindAction="com.cwc.app.api.zj.ProductReturnOrderMgrZJ.returnProductForFile">
	  <br/><br/><input name="Submit" type="button" class="long-button-mod" value="上传图片" onClick="returnUploadFile(<%=returnInfo[i].getString("rp_id")%>)">
	  </tst:authentication>
	  <br/><br/><input name="Submit" type="button" class="long-button-mod" value="退货登记" onClick="sigReturn(<%=returnInfo[i].getString("rp_id")%>,<%=returnInfo[i].getString("oid")%>)">
	  <%
	  }
	  else if(returnInfo[i].get("product_status",0)==returnProductStatusKey.GetImg)
	  {
	  %>
	  <tst:authentication bindAction="com.cwc.app.api.zj.ProductReturnOrderMgrZJ.returnProductForFile">
	  <br/><br/><input name="Submit" type="button" class="long-button-mod" value="上传图片" onClick="returnUploadFile(<%=returnInfo[i].getString("rp_id")%>)">
	  </tst:authentication>
	  <%
	  }
	  %>
	  	<tst:authentication bindAction="com.cwc.app.api.zj.ProductReturnOrderMgrZJ.returnProductForFile">
	  	<br/><br/><input name="Submit" type="button" class="long-button-add" value="创建账单" onClick="addNew(<%=returnInfo[i].get("rp_id",0l)%>,<%=returnInfo[i].get("oid",0l)%>)">
	  	</tst:authentication>
	  </td>
  </tr>
  <tr class="split"><td colspan="4"></td></tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
    <input type="hidden" name="p">
	<input type="hidden" name="cmd" value="<%=cmd%>">
	<input type="hidden" name="rp_id" value="<%=rp_id%>">
	<input type="hidden" name="status" value="<%=status%>">
	<input type="hidden" name="product_status" value="<%=product_status%>">
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
<form name="listForm" method="post" target="_self">
	<input type="hidden" name="oid">
	<input type="hidden" name="rp_id">
	<input type="hidden" name="ps_id">
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>"/>
</form>
</body>

</html>