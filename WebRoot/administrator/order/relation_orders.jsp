<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>关联订单</title>
<style>
td
{
	font-size:12px;
}
</style>
<%
	String txn_id = "";
	String ip_address = "";
	long oid = StringUtil.getLong(request,"oid");
	DBRow relationOrders[] = orderMgrZr.getAllRelationTradeByOid(oid);
	String rowColor;
	boolean ifPending=false;
%>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="847" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
  <tr>
    <td width="99" height="25" align="center" valign="middle" bgcolor="eeeeee"><strong>Payment_Status</strong></td>
    <td width="97" align="center" valign="middle" bgcolor="eeeeee"><strong>Payment_Type</strong></td>
    <td width="99" align="center" valign="middle" bgcolor="eeeeee"><strong>Reason_Code</strong></td>
    <td width="117" align="center" valign="middle" bgcolor="eeeeee"><strong>Case_Id</strong></td>
    <td width="109" align="center" valign="middle" bgcolor="eeeeee"><strong>Case_Type</strong></td>
    <td width="112" align="center" valign="middle" bgcolor="eeeeee"><strong>Post_Date</strong></td>
  </tr>
<%
for (int i=0;i<relationOrders.length; i++)
{
    if(relationOrders[i].getString("payment_status").contains("Pending")&&!relationOrders[i].getString("payment_type").contains("echeck"))
    {
    	ifPending=true;
    }
	if (i%2==0)
	{
		rowColor = "#ffffff";
	}
	else
	{
		rowColor = "#DFDF82";
	}
%>  
  <tr>
    <td height="25" align="center" valign="middle" bgcolor="<%=rowColor%>"><%=relationOrders[i].getString("payment_status")%></td>
    <td align="center" valign="middle" bgcolor="<%=rowColor%>"><%=relationOrders[i].getString("payment_type")%></td>
    <td align="center" valign="middle" bgcolor="<%=rowColor%>"><%=relationOrders[i].getString("reason_code")%></td>
    <td align="center" valign="middle" bgcolor="<%=rowColor%>"><%=relationOrders[i].getString("case_id")%></td>
    <td align="center" valign="middle" bgcolor="<%=rowColor%>"><%=relationOrders[i].getString("case_type")%></td>
    <td align="center" valign="middle" bgcolor="<%=rowColor%>"><%=relationOrders[i].getString("post_date")%></td>
  </tr>
<%
	txn_id = relationOrders[i].getString("txn_id");
	ip_address = relationOrders[i].getString("ip_address");
}
%>
</table>

<script language="javascript"> 
function execute(action)
{

		var isSure=confirm("你确定"+action+"操作么？");
		if(isSure)
		{
		    var param = "txn_id=<%=txn_id%>"+"&action="+action;
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span  font-size:13px;font-weight:bold;color:#666666">正在向paypal进行请求操作，请稍后......</span>' });
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/managePendingAction.action',
				//url:dataUrl,
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:param, 
				
				beforeSend:function(request){
				},
				
				error: function(data){
					$.unblockUI();
				 	alert("网络问题，请稍后重试...");
				 	parent.closeWin();
				},
				
				success: function(data){
						if(data["rs"]=="Success")
						{
							$.unblockUI();
							alert("操作成功！") ;
							$.artDialog.close();
						}else
						{
							$.unblockUI();
							alert("操作失败！\n原因:"+data["reasion"]+"\n请根据失败原因重新选择操作！");
						}
				} 
			});
			 
		}	
}
</script>

 <br/>
<%
	if(ifPending==true && ip_address.equals("")==false)
	{
	%> <table  width="847" border="0" align="center" cellpadding="5" cellspacing="1" >
			<tr >
				<td align="center"  bgcolor="#ffffff">
				&nbsp;&nbsp;&nbsp;&nbsp;<input name="executeAccept"  id="executeAccept" type="button" value="Accept" class="normal-green"  onclick="execute(this.value)"/>&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="executeDeny"  id="executeDeny" type="button" value="Deny"  class="normal-white"  onclick="execute(this.value)"/>
				</td>
			</tr>
	   </table>
			<% 
	}
%>
</body>
</html>