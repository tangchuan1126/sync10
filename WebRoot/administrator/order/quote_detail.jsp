<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="quoteKey" class="com.cwc.app.key.QuoteKey"/>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../include.jsp"%>
<%
	long qoid = StringUtil.getLong(request,"qoid");
DBRow detailQuoteOrder = quoteMgr.getDetailQuoteByQoid(qoid);
DBRow quoteOrderItems[] = quoteMgr.getQuoteItemsByQoid(qoid,null);
DBRow detailQuoteAccount = adminMgr.getDetailAdminByAccount(detailQuoteOrder.getString("create_account"));

AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

int limitRowLength = 13;

float mm_pix = 3.78f;

float left_float = 17f;
float top_float = 17f;

float print_range_width = 215f*mm_pix;
float print_range_height = 297f*mm_pix;
float print_range_left = left_float*mm_pix;
float print_range_top = top_float*mm_pix;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>报价单</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>


<script> 

function printInit()
{
	xprint.printing.header = "" //页眉 
	xprint.printing.footer = "" //页脚
	xprint.printing.portrait = true //控制横打还是竖打，true为竖打，false为横打
	xprint.printing.leftMargin = 0 //左边距 
	xprint.printing.topMargin = 0 //上边距 
	xprint.printing.rightMargin = 0 //右边距  
	xprint.printing.bottomMargin = 0 //下边距 
	xprint.printing.portrait = true
}

function doPrint()
{
	beforePrint();  

	xprint.printing.Print(false); //直接打印，true:弹出选择打印机窗口,false:直接打印 
	
	afterPrint()
}

function beforePrint()
{
	document.getElementById("container").style.border = "0px";
	document.getElementById("container").style.top = "0px";
	document.getElementById("container").style.left = "0px";
}

function afterPrint()
{
	document.getElementById("container").style.border = "1px #999999 solid";
	document.getElementById("container").style.top = "<%=print_range_top%>px";
	document.getElementById("container").style.left = "<%=print_range_left%>px";
}

function pos(obj,x,y)
{
	obj.style.position="absolute";

	obj.style.left=x*<%=mm_pix%>;
	obj.style.top=y*<%=mm_pix%>;

}

function printQuote()
{
	xprint.printing.printer="Brother MFC-7420 Printer111";
	xprint.printing.paperSize = "A4"; //纸张
	doPrint();
}

function closeTBWin()
{
	tb_remove();
}

function trueOrder(qoid,ps_id,sc_id,ccid,client_id,pro_id)
{

		document.record_form.qoid.value=qoid;
		document.record_form.ps_id.value=ps_id;			
		document.record_form.sc_id.value=sc_id;		
		document.record_form.ccid.value=ccid;			
		document.record_form.client_id.value=client_id;	
		document.record_form.pro_id.value=pro_id;						
		document.record_form.submit();
}

function closeTBWin()
{
	tb_remove();
}

function exportPDF(qoid)
{
	window.location = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/ExportPDFQuote.action?qoid="+qoid;
}
</script>

<style>
#container
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height%>px;

	position:absolute;
	left:<%=print_range_left%>px;
	top:<%=print_range_top%>px;
	clip:rect(0,<%=print_range_width%>,<%=print_range_height%>,0);
	font-size:12px;
	font-weight:normal;
	border:1px #999999 solid;
	font-family:Verdana;
}

td
{
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
}
.quote_to
{
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	color:#000000;
}

.quote_normal
{
	font-size:13px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	color:#C19859;
}

.quote_text
{
	font-size:10px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:normal;
	color:#000000;
}

.quote_thanks
{
	font-size:17px;
	font-family:Franklin Gothic Medium;
	font-weight:bold;
	color:#C19859;
	font-style: italic;
}

form
{
	padding:0px;
	margin:0px;
}

.export-pdf {
	background-attachment: fixed;
	background: url(../imgs/product/export_pdf_button.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 42px;
	width: 114px;
	color: #000000;
	border:0px;
	font-size:15px;
	font-weight:normal;
	font-family:宋体;
	margin-left:5px;
	margin-right:5px;
	padding-left:40px;
	padding-top:10px;
}

.sendmail {
	background-attachment: fixed;
	background: url(../imgs/product/sendemail.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 42px;
	width: 114px;
	color: #000000;
	border:0px;
	font-size:15px;
	font-weight:normal;
	font-family:宋体;
	margin-left:5px;
	margin-right:5px;
	padding-left:40px;
	padding-top:10px;
}

</style>

<style media=print>  
.noPrint{display:none;}<!--用本样式在打印时隐藏非打印项目-->  
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/recordOrderQuote.action" method="post" name="record_form" id="record_form">
<input type="hidden" name="qoid" >
<input type="hidden" name="ps_id">
<em><input type="hidden" name="backurl" value='<%=StringUtil.getCurrentURL(request)%>'></em>	
<input type="hidden" name="sc_id" >
<input type="hidden" name="ccid" >
<input type="hidden" name="pro_id" >
<input type="hidden" name="client_id">
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" style="padding-left:20px;padding-top:20px;"><br>
    <table width="750" height="950" border="0" cellpadding="0" cellspacing="1" bgcolor="#997339" id="quote_table">
  <tr>
    <td align="left" valign="top" bgcolor="#FAF7F2" style="padding-left:29px;padding-top:30px;"><table id="quote_title" width="690" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="70%" height="50" align="left" valign="middle" style="border-bottom:#C19859 solid 1px"><img src="../imgs/system_logo.gif" width="192" height="20"></td>
          <td width="30%" align="right" valign="middle" style="font-size:30px;font-family:Franklin Gothic Medium;color:#C19859;border-bottom:#C19859 solid 1px">Quote:<%=qoid%></td>
        </tr>
      <tr>
        <td colspan="2" align="right" valign="middle" class="quote_to"><br>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="50%" align="left" valign="top" class="quote_to">
			<%
			if (adminLoggerBean.getAccount().equals(detailQuoteOrder.getString("create_account"))&&detailQuoteOrder.get("quote_status",0)==quoteKey.WAITING)
			{
			%>
			<a href="javascript:tb_show('修改报价单','quote_add.html?qoid=<%=qoid%>&cmd=modify&TB_iframe=true&height=500&width=850',false);"><img src="../imgs/application_edit.png" width="16" height="16" border="0" align="absmiddle">修改</a>
			<%
			}
			%>
			</td>
            <td width="50%" align="right" valign="middle" class="quote_to">
						

          Date: <%=detailQuoteOrder.getString("post_date")%><br>
          Expiration Date: <%=detailQuoteOrder.getString("expiration_date")%>	
			</td>
          </tr>
        </table></td>
          </tr>
      <tr>
        <td colspan="2" align="right" valign="middle" class="quote_normal" style="padding-right:250px;">To</td>
          </tr>
      <tr>
        <td colspan="2" align="right" valign="middle"  class="quote_to"><%=detailQuoteOrder.getString("address_name")%><br>
          <%=detailQuoteOrder.getString("address_street")%><br>
          <%=detailQuoteOrder.getString("address_city")%>,<%=detailQuoteOrder.getString("address_state")%>,<%=detailQuoteOrder.getString("address_zip")%>,<%=orderMgr.getDetailCountryCodeByCcid(detailQuoteOrder.get("ccid",0l)).getString("c_country")%><br>
          <%=detailQuoteOrder.getString("tel")%><br>
          [<%=detailQuoteOrder.getString("client_id")%>]</td>
        </tr>
      </table>
        <br>
      <br>
      <table id="quote_sales" width="690" border="0" cellpadding="1" cellspacing="1" bgcolor="#997339">
        <tr>
          <td width="437" align="left" valign="middle" bgcolor="#FAF7F2" class="quote_normal" style="padding-left:5px;">Salesperson</td>
            <td width="126"  align="center" valign="middle" bgcolor="#FAF7F2" class="quote_normal">Shipping Method</td>
            <td width="117"  align="center" valign="middle" bgcolor="#FAF7F2" class="quote_normal">Processing Time </td>
          </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF" class="quote_text"  style="padding-left:5px;">
		  <%=detailQuoteOrder.getString("create_account")%>,<%=detailQuoteAccount.getString("email")%>
		  <br>
MSN: <%=detailQuoteAccount.getString("msn")%>
		  
	
		  </td>
            <td  align="center" valign="middle" bgcolor="#FFFFFF" class="quote_text" ><%
			DBRow detailCompany = expressMgr.getDetailCompany(detailQuoteOrder.get("sc_id",0l));
			if (detailCompany==null)
			{
				out.println("?");
			}
			else
			{
				out.println(detailCompany.getString("name"));
			}
			%></td>
            <td  align="center" valign="middle" bgcolor="#FFFFFF" class="quote_text" >1 DAY </td>
          </tr>
        </table>
        <br>
      <table id="quote_products" width="690" border="0" cellpadding="0" cellspacing="1" bgcolor="#997339">
        <tr>
          <td align="center" valign="middle" bgcolor="#FAF7F2" class="quote_normal"><table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td width="9%" align="center" valign="middle" class="quote_normal">Qty</td>
              <td width="38%" align="left" valign="middle" class="quote_normal">Item #</td>
              <td width="12%" align="center" valign="middle" class="quote_normal">Org Price </td>
              <td width="13%" align="center" valign="middle" class="quote_normal">On sale</td>
              <td width="13%" align="center" valign="middle" class="quote_normal">Discount</td>
              <td width="15%" align="center" valign="middle" class="quote_normal">Total</td>
            </tr>
          </table></td>
          </tr>
        <tr>
          <td align="center" valign="middle" bgcolor="#FFFFFF" class="quote_text" ><table width="100%" border="0" cellspacing="0" cellpadding="2">
<%
int dataLen,spaceLen;

if (limitRowLength - quoteOrderItems.length>0)
{
	dataLen = quoteOrderItems.length;
	spaceLen = limitRowLength - quoteOrderItems.length;
}
else
{
	dataLen = quoteOrderItems.length;
	spaceLen = 0;
}

double subtotal = 0;
double total = 0;
for (int i=0; i<dataLen; i++)
{
	total = MoneyUtil.mul(quoteOrderItems[i].get("quantity",0d), quoteOrderItems[i].get("quote_price",0d));
	subtotal = MoneyUtil.add(subtotal, total);
%>		  
            <tr>
              <td width="9%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px"><%=quoteOrderItems[i].get("quantity",0f)%> <%=quoteOrderItems[i].getString("unit_name")%></td>
              <td width="38%" align="left" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">
			  <%=quoteOrderItems[i].getString("name")%>
			  <%
			  if (quoteOrderItems[i].get("price_type",0)==CartQuote.SPECIAL_PRICE)
			  {
			  	out.println("<font color=red>(特价)</font>");
			  }
			  %>

<%
			if (quoteOrderItems[i].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
			{
				out.println("<div style='color:#000000;padding-left:10px;font-weight:normal'>");
				DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( quoteOrderItems[i].get("pid",0l) );
				for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
				{
					out.println("├ "+customProductsInSet[customProductsInSet_i].getString("p_name")+" x "+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name")+"<br>");
				}
				out.println("</div>");
			}
%>			  </td>
              <td width="12%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">$<%=quoteOrderItems[i].get("lowest_price",0d)%></td>
              <td width="13%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">$<%=quoteOrderItems[i].get("quote_price",0d)%></td>
              <td width="13%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px"><%=MoneyUtil.round(String.valueOf((10-quoteOrderItems[i].get("discount",0d)) *10), 1)%>%OFF</td>
              <td width="15%" align="center" valign="middle" class="quote_text">$<%=total%></td>
            </tr>
<%
}


for (int i=0; i<spaceLen; i++)
{
%>		  
            <tr>
              <td width="9%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">&nbsp;</td>
              <td width="38%" align="left" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">&nbsp;</td>
              <td width="12%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">&nbsp;</td>
              <td width="13%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">&nbsp;</td>
              <td width="13%" align="center" valign="middle" class="quote_text" style="border-right:#997339 solid 1px">&nbsp;</td>
              <td width="15%" align="center" valign="middle" class="quote_text">&nbsp;</td>
            </tr>
<%
}
%>

          </table></td>
          </tr>
      </table>
      <table id="quote_products" width="690" border="0" cellpadding="0" cellspacing="0" bgcolor="#997339">
        
        <tr>
          <td align="center" valign="middle" bgcolor="#FFFFFF" class="quote_text" ><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td width="262" align="left" valign="middle" bgcolor="#FAF7F2" class="quote_normal" style="padding-right:" >
							<%
			if ( (adminLoggerBean.getAccount().equals(detailQuoteOrder.getString("create_account")) || adminLoggerBean.getAdgid()==100006l) &&detailQuoteOrder.get("quote_status",0)==quoteKey.WAITING) 
			{
			%>
				<a href="javascript:tb_show('修改报价单商品','quote_record_order.html?qoid=<%=qoid%>&cmd=modify&TB_iframe=true&height=500&width=850',false);"><img src="../imgs/application_edit.png" width="16" height="16" border="0" align="absmiddle">修改</a>
				<%
				}
				%>				</td>
                <td width="315" align="right" valign="middle" bgcolor="#FAF7F2" class="quote_normal" style="padding-right:" >Subtotal&nbsp;&nbsp;</td>
                <td width="101" align="center" valign="middle" class="quote_text" style="border-left:#997339 solid 1px;border-bottom:#997339 solid 1px;border-right:#997339 solid 1px;">$<%=detailQuoteOrder.get("product_cost",0d)%></td>
              </tr>
              <tr>
                <td colspan="2" align="right" valign="middle" bgcolor="#FAF7F2" class="quote_normal" >Shipping Fee &nbsp;&nbsp;</td>
                <td align="center" valign="middle" class="quote_text" style="border-left:#997339 solid 1px;border-bottom:#997339 solid 1px;border-right:#997339 solid 1px;">$<%=MoneyUtil.round(detailQuoteOrder.get("shipping_cost",0d),2)%></td>
              </tr>
            <tr>
              <td colspan="2" align="right" valign="middle" bgcolor="#FAF7F2" class="quote_normal" >Total&nbsp;&nbsp;</td>
                <td align="center" valign="middle" class="quote_text" style="border-left:#997339 solid 1px;border-bottom:#997339 solid 1px;border-right:#997339 solid 1px;">$<%=detailQuoteOrder.get("total_cost",0d)%></td>
                </tr>
            <tr>
              <td colspan="2" align="right" valign="middle" bgcolor="#FAF7F2" class="quote_normal" >Saving&nbsp;&nbsp;</td>
              <td align="center" valign="middle" class="quote_text" style="border-left:#997339 solid 1px;border-bottom:#997339 solid 1px;border-right:#997339 solid 1px;">
			  $<%=detailQuoteOrder.get("saving",0d)%>
			  </td>
	
            </tr>

          </table></td>
        </tr>
      </table>
      <br>
      <table id="quote_products" width="690" border="0" cellpadding="0" cellspacing="1" bgcolor="#997339" style="padding-top:5px;padding-bottom:5px;">
        
        <tr>
          <td height="80" align="center" valign="top" bgcolor="#FFFFFF" class="quote_text" ><table width="99%" border="0" cellspacing="0" cellpadding="4">
            <tr>
              <td>Payment Instruction</td>
            </tr>
            <tr>
              <td class="quote_text"><br>
                1. Go to <a href="http://www.vvme.com/index.php?main_page=page_directpayment" target="_blank">http://www.vvme.com/index.php?main_page=page_directpayment</a><br>
                2. In the 'Amount of Payment' field, fill in the order total in USD.<br>
                3. In the Product(s) field, fill in the quotation number as shown on the upper right corner of the Quotation Sheet.<br>
                4. In the Name field, fill in your name.<br>
                5. In the Phone No. field, fill in your phone number or leave it blank.<br>
                6. Click the Pay By PayPal or Credit Card button and you will be redirected to PayPal to log into your PayPal account to make the<br>
                payment.</td>
            </tr>
            
          </table></td>
        </tr>
      </table>
      <br>
      <br>
      <br>
      <table width="690" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="607" align="center" valign="middle" class="quote_thanks">Thank you for your business!</td>
        </tr>
        </table>
      <br>
      <br>
      <table width="690" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="607" align="center" valign="middle" class="quote_text">Visionari LLC &nbsp;&nbsp;  2570 Corporate Place 	E103,Monterey Park 	CA 	91754 	US  Phone:2139081217 sales@vvme.com  </td>
        </tr>
        </table>
        <br></td>
  </tr>
  </table>


    <table width="750" height="23" border="0" cellpadding="0" cellspacing="0" class="noPrint" >
  <tr>
    <td colspan="2" >&nbsp;</td>
    </tr>
  <tr>
    <td >
	<%
	if (detailQuoteOrder.get("quote_status",0)!=quoteKey.CANCEL)
	{
	%>
	<input name="Submit2" type="button" class="export-pdf" value="导出PDF" onClick="exportPDF(<%=qoid%>)">
	&nbsp;&nbsp;&nbsp;&nbsp;
	<input name="Submit3" type="button" class="sendmail" value="邮件发送" onClick="tb_show('邮件发送报价单(附件)','quote_email.html?qoid=<%=qoid%>&TB_iframe=true&height=500&width=850',false);">
	<%
	}
	%>
	</td>
    <td width="205" align="right" ><input name="Submit" type="button" class="normal-white" value="返回" onClick="window.location='quote.html'"></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  </table>
	
	
	</td></tr>
</table>



</body>
</html>
