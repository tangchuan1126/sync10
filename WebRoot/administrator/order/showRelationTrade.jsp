<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.InvoiceStateKey"%>
<%@page import="com.cwc.app.api.zr.OrderMgrZr"%>
<jsp:useBean id="payTypeKeyZr" class="com.cwc.app.key.PaymentMethodKey"/>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.PaymentMethodKey,java.util.ArrayList,com.cwc.app.key.BillTypeKey,java.util.ArrayList,com.cwc.app.key.InvoiceStateKey" %>
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单报价</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<style type="text/css" media="all">

@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<style type="text/css">
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
span.tradeId{color:#666666;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:20px;}
 .legend{font-size:12px;font-weight:bold;color:#000000;background:none;}
 span.fixAddressLeft{width:55px;display:block;float:left;text-align:right;border:0px solid red;}
 span.fixAddressRight{display:block;text-align:left;float:left;text-indent:5px;}
 p{clear:both;}
</style>
<%
long oid = StringUtil.getLong(request,"oid");
DBRow[] rows = null; 
if(oid != 0l){
	rows = orderMgrZr.getAllRelationTradeByOid(oid);
}
%>

<style type="text/css">
 
</style>
</head>
 <body>
 
 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"   class="zebraTable" style="margin-left:3px;margin-top:5px;">
			  <tr> 
			  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">交易信息</th>
			  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">收款人信息</th>
			  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">付款人信息</th>
		        <th width="25%" style="vertical-align: center;text-align: center;" class="right-title">地址信息</th>
		      </tr>
		       <tbody>
		      <%
		      	if(null != rows && rows.length > 0){
		      			for(DBRow row : rows){
		      %> 		
		      
		      	<tr>
		      		<td>
		      			<!-- 交易信息 -->
		      			<!--  关联的交易可以改为green的框。如果是主交易那么是有个小人在上面的 -->
		      			
		      			<%
		      				boolean isCreate = row.get("is_create_order",0) > 0 ? true:false;
		      				String borderColor = isCreate?"#993300" : "#f60";
		      				
		      			%>
		      			
		      			<fieldset   class="set" style="border:2px solid <%= borderColor %>;">
		  					 <legend class="legend"> 
		  					 	<%= row.getString("mc_currency")  %>&nbsp;<%=row.get("mc_gross",0.0f) %>&nbsp;|&nbsp; <%= row.getString("payment_status") %>  
		  					 	 <%
								 String ipAddress = row.getString("ip_address");
							 		if(ipAddress.length() > 0){
							 			String[] array = ipAddress.split("-");
							 			if(array.length >= 2){
							 				ipAddress = array[0];
							 			}else{
							 				ipAddress = "";
							 			}
							 		} 
							 		
								   if(row.get("payment_channel",-1) == (payTypeKeyZr.CC)){
	 							 		if(ipAddress.split("\\.").length == 4){
								 			out.print("<a href='http://www.123cha.com/ip/?q="+ipAddress+"' target='_blank'><img src='../imgs/visa.jpg' width='22' height='14' border='0' align='absmiddle'></a>");
								 		}else{
								 			out.print(payTypeKeyZr.getStatusById(row.get("payment_channel",-1)));
								 		}
								   }else if(row.get("payment_channel",-1) == (payTypeKeyZr.PP) && ipAddress.split("\\.").length == 4){
									   out.print("<a href='http://www.123cha.com/ip/?q="+ipAddress+"' target='_blank'>"+payTypeKeyZr.getStatusById(row.get("payment_channel",-1))+"</a>");
								   }else{
									   out.print(payTypeKeyZr.getStatusById(row.get("payment_channel",-1)));
								   }
							 %> 
		  					 
		  					  </legend>
		  					 <p style="text-indent:20px;margin-top:5px;">
		  					 	<span class="tradeId"><%= row.get("trade_id",0l) %></span><span class="tradeId">&nbsp;-&nbsp;</span><span class="tradeId"><%= row.get("relation_id",0l) %></span> 
		  					 	<% if(isCreate) {%>
		  					 <!-- 	<img width="16" height="16" border="0" src="../imgs/order_client.gif"> -->
		  					 		<span><img width="14" height="14" align="absmiddle" title="创建订单" src="../imgs/nav_text.jpg"></span>
		  					 	<%} %>
		  					 </p>
		  					 
		  					 
		  					 <p>
			  					 <span style="color:green;font-weight:bold;margin-left:10px;"> 
			  						 <%= row.getString("order_source") %>
			  					 </span>
			  					 <span style="color:#f60;font-weight:bold;">&nbsp;|&nbsp;<%=row.getString("payment_type")%></span>
		  					</p>
		  					<%
		  						if(row.getString("txn_id").length() > 0){
		  					%>
		  					<p>
		  						<span style="margin-left:26px;">Txn Id:</span>
		  						<span><%=row.getString("txn_id")%></span>
		  					</p>
		  					<%} %>
		  					<%if(!isCreate){ %>
			  		 			<p>
			  		 				<span>Parent Txn:</span>
			  		 				<span><%= row.getString("parent_txn_id") %></span>
			  		 			</p>
		  		 			<%} %>
		  					 
 							 <p style="clear:both;"><span style="display:block;float:left;"><img style="margin-left:20px;margin-top:2px;" src="../imgs/alarm-clock--arrow.png" title="创建时间"></span><span style="float:left;">&nbsp;&nbsp;<%=row.getString("post_date") %></span></p>
							
						</fieldset>
		      		</td>
		      		<td>
		      			<!-- 收款人信息 -->
		      			
						<p>
							<span>收款人:</span>
							<span><%=row.getString("receiver_email") %></span>
						</p>
						<p>
							<span>收款账号:</span>
							<span><%=row.getString("business") %></span>
						</p>
						<p>
							<span>对应平台收款Id:</span>
							<span><%=row.getString("receiver_id") %></span>
						</p>
					</td>
		      		<td>
		      		<%
		      		  	//根据client_id 去获取 客户名字等信息
		      		  	DBRow clientRow = orderMgrZr.getClientInfoByEmail(row.getString("payer_email",""));
		      		%>
		      			<!-- 付款信息 -->
		      			<p>
		      				<span style="display:block;float:left;"><img width="14" height="14" border="0" src="../imgs/account.gif" title="客户">&nbsp;&nbsp;</span>
		      				<span style="display:block;float:left;"><%= row.getString("client_id") %></span>
		      			</p>
 		  			 
		      			<p>
		      				<span style="display:block;float:left;"><img width="14" height="14" border="0" src="../imgs/order_client.gif" title="Payer Email">&nbsp;&nbsp;</span>
		      				<span style="display:block;float:left;"><%= row.getString("payer_email") %></span>
		      			</p>
		      			<%
		      				StringBuffer clientInfo = new StringBuffer();
		      				if(clientRow != null){
		      					clientInfo.append(clientRow.getString("first_name")+"&nbsp;").append(clientRow.getString("last_name"))
		      					.append("&nbsp;-&nbsp;").append("<span style='color:green;font-weight:bold;'>"+clientRow.getString("payer_status")+"</span>");
		      				}
		      			%>
		      			 
		      			 <p style="font-weight:bold;">
		      			 	 <%= clientInfo.toString() %>
		      			 </p>
		      		</td>
		      		<td style="padding-top:5px;padding-bottom:5px;">
		      			<!-- 地址信息 delivery -->
		      			<br />
		      			<p>
		      				<span class="fixAddressLeft alert-text">Name:</span>
		      				<span class="fixAddressRight"><%=row.getString("delivery_address_name") %></span>
		      			</p>
		      			<p>
		      				<span class="fixAddressLeft alert-text ">Street:</span>
		      				<span class="fixAddressRight"><%=row.getString("delivery_address_street") %></span>
		      			</p>
		      			<p>
		      				<span class="alert-text fixAddressLeft">City:</span>
		      				<span class="fixAddressRight"><%=row.getString("delivery_address_city") %></span>
		      			</p>
		      			<p>
		      				<span class="alert-text fixAddressLeft">State:</span>
		      				<span class="fixAddressRight"><%=row.getString("delivery_address_state") %></span>
		      			</p>
 						<p>
 							<span class="alert-text fixAddressLeft">Zip:</span>
 							<span class="fixAddressRight"><%=row.getString("delivery_address_zip") %></span>
 						</p>
 						<p>
 							<span class="alert-text fixAddressLeft">Country:</span>
 							<span class="fixAddressRight"><%=row.getString("delivery_address_country") %></span>
 						</p>
 						<p>
 							<span class="alert-text fixAddressLeft">Code:</span>
 							<span class="fixAddressRight"><%=row.getString("delivery_address_country_code") %></span>
 						</p>
 						<p>
 							<span class="alert-text fixAddressLeft">Phone:</span>
 							<span class="fixAddressRight"><%=row.getString("contact_phone") %></span>
 						</p>
 						<p>
 							<span class="alert-text fixAddressLeft">Status:</span>
 							<span class="fixAddressRight"><%=row.getString("delivery_address_status") %></span>
 						</p>
 						<br />
		      		</td>
		      	</tr>
		      	<% 
		      			}
		      	}else {
		      	%>
		      		<tr style="background:#E6F3C5;">
   						<td colspan="4" style="text-align:center;height:120px;">无数据</td>
   					</tr>
		      	<%	
		      	}
		      %>
		       
		      </tbody>
		</table>
 
 
</body>

</html>