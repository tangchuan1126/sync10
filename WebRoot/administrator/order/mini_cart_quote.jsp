<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long sc_id = StringUtil.getLong(request,"sc_id");
long ccid = StringUtil.getLong(request,"ccid");
long pro_id = StringUtil.getLong(request,"pro_id");
long ps_id = StringUtil.getLong(request,"ps_id");
String cmd = StringUtil.getString(request,"cmd");
String client_id = StringUtil.getString(request,"client_id");

cartQuote.flush(session,client_id,sc_id,ccid,pro_id,ps_id);
DBRow cartProducts[] = cartQuote.getDetailProducts();

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script>

</script>

<style>
a.del-cart-product:link {
	color:#FF0000;
	text-decoration: none;
}
a.del-cart-product:visited {
	color: #FF0000;
	text-decoration: none;
}
a.del-cart-product:hover {
	color: #FF0000;
	text-decoration: underline;

}
a.del-cart-product:active {
	color: #FF0000;
	text-decoration: none;

}
td
{
	font-family:Arial, Helvetica, sans-serif;
	
}

.sum-data
{
	font-size:17px;
	font-weight:bold;
	
}
</style> 

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
  
<%
if (cartProducts.length==0)
{
%>
      <tr>
        <td colspan="7" align="center" valign="middle" style="color:#999999"><<暂无商品>>
		<script>cartIsEmpty=true;</script>		</td>
      </tr>
<%
} 
else
{
%>
      <tr >
        <td height="30" bgcolor="#eeeeee" style="padding-left:10px;border-bottom:1px #CCCCCC solid"><strong>商品</strong></td>
        <td align="left" bgcolor="#eeeeee"  style="border-bottom:1px #CCCCCC solid"><strong>数量</strong></td>
        <td align="left" bgcolor="#eeeeee" style="border-bottom:1px #CCCCCC solid"><strong>原价</strong></td>
        <td align="left" bgcolor="#eeeeee" style="border-bottom:1px #CCCCCC solid"><strong>折扣价</strong></td>
        <td align="left" bgcolor="#eeeeee" style="border-bottom:1px #CCCCCC solid"><strong>折扣</strong></td>
        <td align="left" bgcolor="#eeeeee" style="border-bottom:1px #CCCCCC solid"><strong>小计</strong></td>
        <td align="center" valign="middle" bgcolor="#eeeeee"  style="border-bottom:1px #CCCCCC solid">&nbsp;</td>
      </tr>
	  
<%
String bgColor;

for (int i=0; i<cartProducts.length; i++)
{
	if (i%2==0)
	{
		bgColor = "#FFFFFF";
	}
	else
	{
		bgColor = "#f8f8f8";
	}

%>

      <tr bgcolor="<%=bgColor%>" >
        <td width="39%" height="35" bgcolor="<%=bgColor%>"  style="padding-left:10px;padding-top:5px;padding-bottom:5px;">
		
		<%=cartProducts[i].get("cart_product_type",0)==CartQuote.UNION_STANDARD_MANUAL?"<":""%>
		<%=cartProducts[i].getString("p_name")%>
		<%=cartProducts[i].get("cart_product_type",0)==CartQuote.UNION_STANDARD_MANUAL?">":""%>
		
		<%
		if (cartProducts[i].get("price_type",0)==CartQuote.SPECIAL_PRICE)
		{
			out.println("<font color=red>(特价)<font>");
		}
		
		if (cartProducts[i].get("cart_product_type",0)==CartQuote.UNION_STANDARD&&cartProducts[i].get("price_type",0)==CartQuote.NORMAL_PRICE)
		{
			out.println("[<a href='javascript:void(0)' onclick='customProduct("+cartProducts[i].getString("cart_pid")+","+ccid+",\""+cartProducts[i].getString("p_name")+"\",\"add\")'>定制</a>]");
		}

		if (cartProducts[i].get("cart_product_type",0)==CartQuote.UNION_CUSTOM)
		{
			out.println("[<a href='javascript:void(0)' onclick='customProduct("+cartProducts[i].getString("cart_pid")+","+ccid+",\""+cartProducts[i].getString("p_name")+"\",\"mod\")'>修改</a>]");
		}
		%>

		<%
		if (cartProducts[i].get("cart_product_type",0)==CartQuote.UNION_CUSTOM)
		{
			out.println("<div style='color:#999999;padding-left:10px;'>");
			DBRow customProductsInSet[] = customCartQuote.getFinalDetailProducts(session, StringUtil.getLong(cartProducts[i].getString("cart_pid")) );
			for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
			{
				out.println("├ "+customProductsInSet[customProductsInSet_i].getString("p_name")+" x "+customProductsInSet[customProductsInSet_i].getString("cart_quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name")+"<br>");
			}
			out.println("</div>");
		}
		%>


		<input type="hidden" name="pids" id="pids" value="<%=cartProducts[i].getString("cart_pid")%>">
		<input type="hidden" name="product_type" id="product_type" value="<%=cartProducts[i].getString("cart_product_type")%>">	
		<input type="hidden" name="price_type" id="price_type" value="<%=cartProducts[i].getString("price_type")%>">
			</td>
        <td width="14%" align="left">
		<%
		if ( cartProducts[i].get("cart_product_type",0)==CartQuote.UNION_STANDARD_MANUAL )
		{
		%>
		<%=cartProducts[i].getString("cart_quantity")%>
		<input type="hidden" name="quantitys" id="quantitys" value="<%=cartProducts[i].getString("cart_quantity")%>">
		<%
		}
		else
		{
		%>
		<input name="quantitys" type="text" id="quantitys" style="width:40px;" value="<%=cartProducts[i].getString("cart_quantity")%>" onChange="return onChangeQuantity(this)"  /> 
		<%
		}
		%>
		
        <span style="color:#666666"><%=cartProducts[i].getString("unit_name")%></span>		</td>
        <td width="10%" align="left" style="font-weight:bold;font-size:14px;color:#999999">$<%=cartProducts[i].getString("lowest_price")%>
          	    </td>
        <td width="10%" align="left" style="font-weight:bold;font-size:14px;">$
		<%
		if (cartProducts[i].get("price_type",0)==CartQuote.NORMAL_PRICE)
		{
		%>
		<%=cartProducts[i].getString("quote_price")%>
		<input name="quote_prices" type="hidden" id="quote_prices_<%=i%>"   value="<%=cartProducts[i].getString("quote_price")%>" style="width:50px;"/> 
		<%
		}
		else
		{
		%>
		<input name="quote_prices" type="text" id="quote_prices_<%=i%>"   value="<%=cartProducts[i].getString("quote_price")%>" style="width:50px;"/> 
		<%
		}
		%>

		</td>
        <td width="10%" align="left" style="font-weight:bold;font-size:14px;color:#990000">
		
		<%
		if ( StringUtil.getFloat(cartProducts[i].getString("discount"))==10 )
		{
			out.println("<span style='font-size:12px;color:#999999;font-weight:normal;'>无</span>");
		}
		else
		{
		%>
		<%=cartProducts[i].getString("discount")%><span style="font-size:11px;color:#666666;font-weight:normal;">折</span>
		<%
		}
		%>		</td>
        <td width="10%" align="left" style="font-weight:bold;font-size:14px;">$<%=cartProducts[i].getString("total")%></td>
        <td width="7%" align="center" valign="middle">

		<a href="javascript:void(0)" onClick="delProductFromCart(<%=cartProducts[i].getString("cart_pid")%>,'<%=cartProducts[i].getString("p_name")%>',<%=cartProducts[i].getString("cart_product_type")%>)" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>		</td>
      </tr>
<%
} 
%>
  
      <tr>
        <td  colspan="7" style="border-top:2px #cccccc solid"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="38%" height="50">
			<input name="Submit433" id="flush_modify"  type="submit" class="short-button" value="应用修改" onClick="$('#flush_modify').poshytip('hide');"> 
		
		<script>
				$('#flush_modify').poshytip({
				content:"请先点击保存修改",
				className: 'tip-yellowsimple',
				showOn: 'none',
				alignTo: 'target',
				alignX: 'center',
				alignY: 'bottom',
				showTimeout:0,
				offsetX: 0,
				offsetY: 5
			});

			getExpressCompanysByScId($("#ps_id").val(),<%=ccid%>,<%=pro_id%>,<%=cartQuote.getTotalWeight()%>);//刷新适用快递
			total_weight = <%=cartQuote.getTotalWeight()%>;
		</script>
		
          &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
        <input name="Submit432" type="button" class="short-short-button-redtext" value="清空" onClick="cleanCart()">
        <script>cartIsEmpty=false;</script>			</td>
            <td width="62%" align="right" valign="middle">
			
			<span style="color:#990000">[订单类别：<%=cartQuote.getOrderCatalog()%>]</span> &nbsp;&nbsp;&nbsp;&nbsp;

       
		<%
		String product_fee = String.valueOf(cartQuote.getProductFee());
		String shipping_fee = String.valueOf(cartQuote.getShippingFee());
		String total_fee = String.valueOf(cartQuote.getTotalPrice());
		String special_product_fee = String.valueOf(cartQuote.getSumSpecialPrice());
		//不能递送等原因
		if (cartQuote.getShippingFee()==0)
		{
			product_fee = "?";
			shipping_fee = "?";
			total_fee = "?";
			special_product_fee = "?";
		}
		%>
		普通商品：<span class="sum-data">$<%=product_fee%></span>&nbsp;&nbsp; 
		<%
		if (StringUtil.getDouble(special_product_fee)>0)
		{
		%>
		特价商品：<span class="sum-data">$<%=special_product_fee%></span>&nbsp;&nbsp;  
		<%
		}
		%>
		 
		
		运费：<span class="sum-data">$<%=shipping_fee%></span>&nbsp;&nbsp; 总计：<span class="sum-data">$<%=total_fee%></span>
		<br>
最终折扣：<span class="sum-data"><%=MoneyUtil.round(cartQuote.getFinalDiscount()*10,1)%>折 ,</span> 节省：<span class="sum-data">$<%=cartQuote.getSaving()%></span>
					</td>
          </tr>
          <tr>
            <td height="50">&nbsp;</td>
            <td align="right" valign="middle"> 
			  调整折扣
			    <select name="manager_discount" id="manager_discount" onChange="modifyCartManagerDiscount()">
            <option value="1" selected>请选择...</option>
			<option value="<%=cartQuote.getSystemDiscount()%>" ><%=MoneyUtil.round(cartQuote.getSystemDiscount()*10,1)%>折</option>
<%
float managerDiscount[] = quoteMgr.calculateManagerDiscounts(session,Float.parseFloat(String.valueOf(cartQuote.getSystemDiscount()))); 
for (int z=0; z<managerDiscount.length; z++)
{

%>
			<option value="<%=managerDiscount[z]%>" ><%=MoneyUtil.round(managerDiscount[z]*10,1)%>折</option>
<%
}
%>
          </select>	
			</td>
          </tr>
        </table></td>
      </tr>
<%
}

//long ent = System.currentTimeMillis();
//System.out.println("Cart Page time:"+(ent-stt));
%>
</table>

</body>
</html>
