<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.exception.product.ProductNotExistException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.api.zr.OrderMgrZr"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.key.TracingOrderKey"%>
<%@page import="com.cwc.app.key.SystemTrackingKey"%>
<jsp:useBean id="handleKey" class="com.cwc.app.key.HandleKey"/>
<jsp:useBean id="handStatusleKey" class="com.cwc.app.key.HandStatusleKey"/>
<jsp:useBean id="productStatusKey" class="com.cwc.app.key.ProductStatusKey"/>
<jsp:useBean id="tracingOrderKey" class="com.cwc.app.key.TracingOrderKey"/>
<jsp:useBean id="returnProductKey" class="com.cwc.app.key.ReturnProductKey"/>
<jsp:useBean id="orderRateKey" class="com.cwc.app.key.OrderRateKey"/>
<jsp:useBean id="afterServiceKey" class="com.cwc.app.key.AfterServiceKey"/>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<jsp:useBean id="payTypeKeyZr" class="com.cwc.app.key.PaymentMethodKey"/>
<%@ page import="com.cwc.app.key.OrderTaskKey,com.cwc.app.key.AfterServiceKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
 	//long stt = System.currentTimeMillis(); 
 try
 {
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

 int p = StringUtil.getInt(request,"p");
 PageCtrl pc = new PageCtrl();
 pc.setPageNo(p);
 pc.setPageSize(systemConfig.getIntConfigValue("order_page_count"));
 DBRow rows[] = null;

 String cmd = StringUtil.getString(request,"cmd");
 int search_field = StringUtil.getInt(request,"search_field");
 int search_mode = StringUtil.getInt(request,"search_mode");
  
 String val = StringUtil.getString(request,"val");
 String st = StringUtil.getString(request,"st");
 String en = StringUtil.getString(request,"en");
 String business = StringUtil.getString(request,"business");
 String p_name = StringUtil.getString(request,"p_name");
 int handle = StringUtil.getInt(request,"handle");
 int after_service_status = StringUtil.getInt(request,"after_service_status");
 int product_type_int = StringUtil.getInt(request,"product_type_int");//发票类型
 int product_status = StringUtil.getInt(request,"product_status");
 long ps_id = StringUtil.getLong(request,"ps_id");
 long filter_ps_id = StringUtil.getLong(request,"filter_ps_id");
 int trace_type = StringUtil.getInt(request,"trace_type");

 int handle_status = StringUtil.getInt(request,"handle_status");
 long order_note_adid = StringUtil.getLong(request,"order_note_adid");
 int order_note_trace_type = StringUtil.getInt(request,"order_note_trace_type");
 String order_source = StringUtil.getString(request,"order_source");
 int bad_feedback_flag = StringUtil.getInt(request,"bad_feedback_flag");


 long product_line_id = StringUtil.getLong(request,"product_line_id");
 long catalog_id = StringUtil.getLong(request,"catalog_id");
 long ca_id = StringUtil.getLong(request,"ca_id",0l);
 long ccid = StringUtil.getLong(request,"ccid",0l);
 long pro_id = StringUtil.getLong(request,"pro_id",0l);
 int cost = StringUtil.getInt(request,"cost",-1);
 int cost_type = StringUtil.getInt(request,"cost_type");
 int unfinished = StringUtil.getInt(request,"unfinished",-1);

 int internal_tracking_status = StringUtil.getInt(request,"internal_tracking_status");
 int tracking_date_type = StringUtil.getInt(request,"tracking_date_type");
 int last_day = StringUtil.getInt(request,"last_day");
  
 if ( cmd.equals("search") )    
 { 

 	rows = orderMgr.getSearchOrders(search_field,val,search_mode,pc);

 } 
 else if ( cmd.equals("print") )
 {
 	rows = orderMgr.getWait4PrintOrders(ps_id,business,product_type_int,st,en,pc);
 }
 else if ( cmd.equals("trace") ) 
 {
 	if (trace_type==SystemTrackingKey.DoubtOrder)
 	{
 		rows = orderMgr.getTraceDoubtOrders(pc);
 	}
 	else if (trace_type==SystemTrackingKey.DoubtAddress)
 	{
 		rows = orderMgr.getTraceDoubtAddressOrders(pc);
 	}
 	else if (trace_type==SystemTrackingKey.DoubtPay)
 	{
 		rows = orderMgr.getTraceDoubtPayOrders(pc);
 	}
 	else  if (trace_type==SystemTrackingKey.DeliveryException)
 	{
 		rows = orderMgr.getDeliveryExceptionOrder(pc);
 	}	
 	else  if (trace_type==SystemTrackingKey.CustomsException)
 	{
 		rows = orderMgr.getCustomExceptionOrder(pc);
 	}	
 	else  if (trace_type==SystemTrackingKey.NotYetDeliveryed)
 	{
 		rows = orderMgr.getNotYetDeliveryedOrder(pc);
 	}	
 	else  if ( trace_type==7 )
 	{
 		rows = orderMgr.getTraceDisputeWarrantyOrders(adminLoggerBean.getAdgid(),pc);
 	}	
 	else  if ( trace_type==8 )
 	{
 		rows = orderMgr.getTraceOtherDisputeOrders(adminLoggerBean.getAdgid(),pc);
 	}		
 	else 
 	{
 		rows = orderMgr.getVerifyCostOrders(adminLoggerBean.getAdgid(),pc);
 	}	
 }
 else if (cmd.equals("statisticsOrder"))
 {
 	rows = orderMgr.statisticsOrder(st,en,p_name,product_line_id,catalog_id,ca_id,ccid,pro_id,cost,cost_type,unfinished,pc);
 }
 else if(cmd.equals("trackingOrder"))
 {
 	rows = orderMgrZJ.trackingOrder(st,en,product_line_id,catalog_id,p_name,ca_id,ccid,pro_id,internal_tracking_status,tracking_date_type,last_day,3,pc);
 }
 else 
 {
 	rows = orderMgr.getOrdersByFilter(request,pc);
 }


 rows = orderMgr.colorFilter(rows);

 String backurl = StringUtil.enCodeBackURL(ConfigBean.getStringValue("systenFolder")+"administrator/order/ct_order_auto_reflush.html?val="+val+"&st="+st+"&en="+en+"&p="+p+"&business="+business+"&handle="+handle+"&after_service_status="+after_service_status+"&handle_status="+handle_status+"&product_type_int="+product_type_int+"&product_status="+product_status+"&ps_id="+ps_id+"&filter_ps_id="+filter_ps_id+"&search_field="+search_field+"&search_mode="+search_mode)+"&cmd="+cmd+"&trace_type="+trace_type+"&order_note_adid="+order_note_adid;

 //System.out.println("=>"+backurl);
 TDate tDate = new TDate();

 WaybillInternalTrackingKey waybillInternalTrackingKey = new WaybillInternalTrackingKey();
 %>

<%
//long ent = System.currentTimeMillis();
//System.out.println("Order Interface:"+(ent-stt));
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script type="text/javascript">
	 function getMoreTrade(oid){
			 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/showRelationTrade.html?oid="+oid;
			 $.artDialog.open(uri , {title: '查看Order[ '+oid+' ] 所有关联交易',width:'1100px',height:'530px', lock: true,opacity: 0.3});
	}
	jQuery(function($){
		// 如果是窗口的宽度小于 1249那么就要把 table 的宽度设置为130%;
		if($(window).width() * 1 < 1240){
			$("#mainTable").width("135%");
		}else{
			$("#mainTable").width("98%");
		}
		//右键弹出下拉框
		var cmenu3 = $(".can_sendMessage");
		cmenu3.each(function(){
			var node = $(this);
			var order_id = node.attr("order_id");
			var item_number = node.attr("item_number");
			var menu = [
					
				{"<span style='font-size:15px;'>Send Ebay Message</span>":{}},
				$.contextMenu.separator,
				{'Send Message':{
				    icon:'../js/contextMenu/page_white_add.png',
				    onclick:function(){
				    	openSendMessageUrl(order_id );
				    }
				}},
				{'Get EbaySeller':{
				    icon:'../js/contextMenu/page_white_add.png',
				    onclick:function(){
				   		 getEbaySellerId(order_id,item_number,node );
				    }
				}},
				{'Update Mpn':{
			        icon:'../js/contextMenu/page_white_add.png',
			        onclick:function(){
			          updateMpn(order_id,item_number,node );
			        }
			    }}
			]
			 node.contextMenu(menu,{theme:'vista'}); 
		})
	   
		
	})
	function getEbaySellerId(oid,itemNumber,node){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"action/administrator/order/GetSellerIdAction.action";
		$.ajax({
			url:uri+"?oid="+oid+"&item_number="+itemNumber,
			dataType:'json',
			beforeSend:function(request){
				$.blockUI({ message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			error: function(){
			    $.unblockUI();
			    alert("获取失败,注意检查ItemNumber是否正确");
 			},
			success:function(data){
			    $.unblockUI();
					if(data.flag === "success"){
						//window.location.reload();
						//页面不用刷新然后把读取出来的Seller_id放在后面
						var span = $("<span style='color:#99CC00;font-size:15px;font-weight:bold'></span>").html(data.seller_id);
					 
						node.after(span);
					}else if(data.flag === "error"){alert(data.message);}
					else{alert("获取失败,注意检查ItemNumber是否正确");}
			}
		})

	}
	
	 function updateMpn(oid,itemNumber,node)
	 {
		  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"action/administrator/order/UpdateMpnAction.action";
		  $.ajax({
			   url:uri+"?oid="+oid,
			   dataType:'json',
			   beforeSend:function(request){
			    $.blockUI({ message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			   },
			   error: function(){
			       $.unblockUI();
			       alert("更新Mpn失败.");
			   },
			   success:function(data){
			       $.unblockUI();
			    // if(data.flag === "success"){
			    //  alert("更新成功");
			    // }else if(data.flag === "error"){alert("更新Mpn失败.");}
			    // else{alert("更新Mpn失败.");}
			   }
		  })
	 }
 
	$(window).resize(function(){
		if($(window).width() * 1 < 1240){
			$("#mainTable").width("135%");
		}else{
			$("#mainTable").width("98%");
		}
	})
	function showTradeItem(oid){
		if(oid * 1 != 0){
			 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/show_trade_item.html?oid="+oid;
			 $.artDialog.open(uri , {title: '查看Trade Item[ '+oid+' ]',width:'800px',height:'430px', lock: true,opacity: 0.3});
		}
	}
	function openSendMessageUrl(order_id){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/send_ebay_message.html?oid="+order_id;
	    $.artDialog.open(uri , {title: 'Send Ebay Message',width:'700px',height:'280px', lock: true,opacity: 0.3,fixed: true});
 	}
 	function showServiceList(oid)
 	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_list_for_order.html?oid='+oid;
		$.artDialog.open(uri , {title: '订单['+oid+']的服务列表',width:'1000px',height:'600', lock: true,opacity: 0.3,fixed: true});
 	}
</script>
<style type="text/css">
	span.fixAddressLeft{width:45px;display:block;float:left;text-align:right;border:0px solid red;}
	span.fixAddressRight{display:block;text-align:left;float:left;text-indent:5px;}
	p{clear:both;}
	.set{border:2px #999999 solid;padding:7px;width:250px;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:normal;-webkit-border-radius:5px;-moz-border-radius:5px;}
	.legend{font-size:12px;font-weight:bold;color:#000000;background:#ffffff;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr> 
    <td width="53%" height="28" valign="middle">&nbsp;</td>
    <td width="47%" align="right" valign="middle"> 
      <% 
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go2page(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go2page(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go2page(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go2page(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p" id="jump_p" type="text" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
    <input name="Submit" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go2page(document.getElementById('jump_p').value)" value="GO"></td>
  </tr>
</table>
<form  method="post" name="verify_order_cost_form" id="verify_order_cost_form">
<input type="hidden" name="oid">
<input type="hidden" name="verify_note">
<input type="hidden" name="backurl" value="<%=backurl%>">
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/mergeOrder.action" method="post" name="merge_order_form" id="merge_order_form">
<input type="hidden" name="oid">
<input type="hidden" name="parent_oid">
<input type="hidden" name="backurl" value="<%=backurl%>">
</form>


<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/undoMergeOrder.action" method="post" name="undo_merge_order_form" id="undo_merge_order_form">
<input type="hidden" name="oid">
<input type="hidden" name="parent_oid">
<input type="hidden" name="backurl" value="<%=backurl%>">
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/recordOrder.action" method="post" name="cart_form" id="cart_form">
	<input type="hidden" name="oid">
	<input type="hidden" name="product_type">
	<input type="hidden" name="tel">
	<input type="hidden" name="handle_status">
	<input type="hidden" name="record_order_note">
	<input type="hidden" name="order_error_email">
	<input type="hidden" name="ps_id">
	<input type="hidden" name="backurl" value="<%=backurl%>">	
	<input type="hidden" name="ccid" >
	<input type="hidden" name="inv_dog" >
	<input type="hidden" name="inv_uv" >
	<input type="hidden" name="inv_tv" >
	<input type="hidden" name="inv_rfe" >
	<input type="hidden" name="inv_di_id" >
	<input type="hidden" name="delivery_note" >
	<input type="hidden" name="recom_express">
	<input type="hidden" name="pro_id">
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/recordOrder.action" method="post" name="save_form" id="save_form">
	<input type="hidden" name="oid">
	<input type="hidden" name="tel">
	<input type="hidden" name="handle_status">
	<input type="hidden" name="record_order_note">
	<input type="hidden" name="order_error_email">
	<input type="hidden" name="backurl" value="<%=backurl%>">	
	<input type="hidden" name="ccid" >
	<input type="hidden" name="pro_id">
	<input type="hidden" name="address_street"/>
	<input type="hidden" name="address_city"/>
	<input type="hidden" name="address_zip"/>
	<input type="hidden" name="address_name"/>
</form>

<form name="addorder_form" method="post" >
	<input type="hidden" name="business">
	<input type="hidden" name="client_id">
	<input type="hidden" name="quantity">
	<input type="hidden" name="mc_gross">
	<input type="hidden" name="type">
	<input type="hidden" name="address_name">
	<input type="hidden" name="address_street">
	<input type="hidden" name="address_city">
	<input type="hidden" name="address_state">
	<input type="hidden" name="address_zip">
	<input type="hidden" name="ccid">
	<input type="hidden" name="tel">
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")+"administrator/order/ct_order_auto_reflush.html"%>">	
</form>

<form name="del_order_append_form" method="post" >
    <input type="hidden" name="oid">
	<input type="hidden" name="append_name">
	<input type="hidden" name="backurl" value="<%=backurl%>">	
</form>

<form name="create_warranty_form" method="post" >
<input type="hidden" id="business" name="business"/> 
<input type="hidden" id="quantity" name="quantity"/> 
<input type="hidden" id="mc_gross" name="mc_gross"/> 
<input type="hidden" id="mc_currency" name="mc_currency"/> 
<input type="hidden" id="address_name" name="address_name"/> 
<input type="hidden" id="address_street" name="address_street"/> 
<input type="hidden" id="address_city" name="address_city"/> 
<input type="hidden" id="address_state" name="address_state"/> 
<input type="hidden" id="address_zip" name="address_zip"/>
<input type="hidden" id="tel" name="tel"/>
<input type="hidden" id="reason" name="reason"/>
<input type="hidden" id="ccid" name="ccid"/>
<input type="hidden" id="pro_id" name="pro_id"/>
<input type="hidden" id="oid" name="oid" />
<input type="hidden" id="type" name="type" /> 

<input type="hidden" name="backurl" value="<%=backurl%>">	
</form>


  <form name="listForm" method="post" target="_self">
    <input type="hidden" name="txn_id">
    <input type="hidden" name="note">
    <input type="hidden" name="handle_status">
	 <input type="hidden" name="handle">
	<input type="hidden" name="tel">
	<input type="hidden" name="oid">
	<input type="hidden" name="quantity">
	<input type="hidden" name="type">
	<input type="hidden" name="ems_id">
	<input type="hidden" name="old_ems_id">
    <input type="hidden" name="backurl" value="<%=backurl%>">	
	<input type="hidden" name="address_name">
	<input type="hidden" name="address_street">
	<input type="hidden" name="address_city">
	<input type="hidden" name="address_state">
	<input type="hidden" name="address_zip"> 
	<input type="hidden" name="address_country_code">
	<input type="hidden" name="address_country">
	<input type="hidden" name="record_order_note">
	<input type="hidden" name="mc_gross">
	<input type="hidden" name="order_error_email">
	<input type="hidden" name="append_oid" id="append_oid" >
	<input type="hidden" name="product_type">
	<input type="hidden" name="batch_modify_handle_type">
	<input type="hidden" name="mod_order_handle_status_note">
	<input type="hidden" name="invoice_id">
	<input type="hidden" name="ccid">
	<input type="hidden" name="on_id">
	<input type="hidden" name="trace_type">
	<input type="hidden" name="return_product_flag">
	<input type="hidden" name="rp_id">
	<input type="hidden" name="addition_money_oid">
	<input type="hidden" name="return_product_reason">
	<input type="hidden" name="ps_id">
	<input type="hidden" name="pro_id"/>

	

<table width="98%" border="0" align="center" id="mainTable" cellpadding="0" cellspacing="0"  class="zebraTable" >
  
      <tr> 
        <th width="3%" class="left-title">&nbsp;</th>
        <th width="15%" style="vertical-align: center;text-align: left;" class="right-title">订单摘要</th>
        <th width="7%"   style="vertical-align: center;text-align: left;"  class="right-title">订单跟进</th>
        <th width="27%"   style="vertical-align: center;text-align: left;"  class="right-title">商品信息</th>
        <th width="27%"   style="vertical-align: center;text-align: left;"  class="right-title">递送信息</th>
        <th width="22%"  style="vertical-align: center;text-align: left;"  class="right-title">相关交易</th>
      </tr>
 
    <%
String product_type[] = systemConfig.getStringConfigValue("product_type").split(",");
for ( int i=0; i<rows.length; i++ )
{
String buyer_ip = tranInfoMgrQLL.getBuyerIp(rows[i].getString("txn_id"),rows[i].getString("order_source"),rows[i].getString("note")).trim();
//System.out.println("->"+buyer_ip);
%>
	<input type="hidden" value="<%=rows[i].getString("handle")%>" name="handle_hidden<%=i%>" id="handle_hidden<%=i%>">
    <tr > 
      <td  height="162" align="center" valign="middle"  bgcolor="<%=rows[i].getString("handle_order_color")%>"  > 
        <input name="oid_sl" id="oid_sl<%=i%>" type="checkbox" value="<%=rows[i].getString("oid")%>"> <%=rows[i].getString("handle_order_color")%>      </td>
      <td   height="250" align="left" valign="middle"  style='word-break:break-all;line-height:18px;'  > 
	  <div style="padding-bottom:8px;"> 
	  <a href="javascript:orderLogs(<%=rows[i].getString("oid")%>)" style="color:#666666;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:20px;text-decoration:none;"><%=rows[i].getString("oid")%></a> 
	  
	  <%
			if(rows[i].get("is_pay_for_order",0) == 1){
				out.print("<span style='color:red;font-weight:bold;cursor:pointer;' title='补款中'>补</span>");
			}
			if(rows[i].get("is_pay_for_order",0) == 2){
				out.println("<span style='color:green;font-weight:bold;cursor:pointer;' title='补款完成'>补</span>");
			}
			if(rows[i].get("is_pay_for_order",0) == 0){
				out.println("&nbsp;&nbsp;&nbsp;");
			}
		%>
	  <%
		String disputeWarning = "";
		String payment_status_style = "";
		if ( rows[i].getString("payment_status").equals("Pending") )
		{
			payment_status_style = "pending-style";
		}
		else
		{
			//争议订单
			if (rows[i].get("dispute_flag",0)==1)
			{
				payment_status_style = "psci-style";
				disputeWarning = "<img border='0' src='../imgs/exclamation-diamond-frame.png' align='absmiddle' alt='争议订单'>";
			}
			else
			{
				payment_status_style = "pscr-style";
				disputeWarning = "";
			}
		}
		%>
		
		<a href="javascript:relationOrder(<%=rows[i].get("oid",0l)%>)" class="<%=payment_status_style%>"><%=disputeWarning%> 
        <%=rows[i].getString("payment_status")%></a> 
		
	  	<%
	if ( rows[i].get("bad_feedback_flag",0)>0 )
	{
		String bad_feedback_bg = "#333333";
		if ( rows[i].get("bad_feedback_flag",0)==orderRateKey.NORMAL )
		{
			bad_feedback_bg = "#CC9900";
		}
		else if ( rows[i].get("bad_feedback_flag",0)==orderRateKey.EBAY_COMPLAIN||rows[i].get("bad_feedback_flag",0)==orderRateKey.PAYPAL_COMPLAIN )
		{
			bad_feedback_bg = "#CC0000";
		}
	%>	
	&nbsp;&nbsp;<img src="../imgs/rate_bad.gif" width="19" height="14"> <a href="javascript:void(0);" style="color:#FFFFFF;background:<%=bad_feedback_bg%>;font-size:12px;padding:2px;-webkit-border-radius:3px;-moz-border-radius:3px;text-decoration: none;" id="rate_bad" rel="<%=rows[i].get("oid",0l)%>"><%=orderRateKey.getOrderRateStatusById(rows[i].get("bad_feedback_flag",0))%></a>
	<%
	}
	%>	
	  </div>
	  <div style="padding:1px;border-bottom:0px #cccccc solid;font-weight:bold;width:150px;margin-bottom:5px;font-size:13px;">
	   <%=handStatusleKey.getOrderHandleById(rows[i].getString("handle_status"))%> |
		  <span style="<%=rows[i].get("parent_oid",0)==-2?"text-decoration: line-through;font-weight:normal;background:#FFFF00":""%>">
			 <%=handleKey.getOrderHandleById(rows[i].getString("handle"))%>
			<%
				if(rows[i].get("handle",0)==HandleKey.DELIVERIED)
				{
			%>
				|<%=waybillInternalTrackingKey.getWaybillInternalTrack(String.valueOf(rows[i].get("internal_tracking_status",0)))%>
			<%
				} 
			%>
		  </span>	
			 
			<%
			if (rows[i].get("after_service_status",0)>0)
			{
			%>
			
		  
		  <br>
		  
		  <span style="background:#FF0000;color:#FFFFFF;font-weight:normal;">售后状态</span><span style="font-weight:normal;background:#dddddd">
			 <%=afterServiceKey.getOrderHandleById(rows[i].getString("after_service_status"))%>		 </span>	
			<%
			}
			%> 
			 
			 </div>

        		
		
		<%
		if (rows[i].getString("order_source").equalsIgnoreCase(com.cwc.app.api.zr.OrderMgrZr.ORDER_SOURCE_EBAY))
		{
		%>
			<img item_number = '<%=rows[i].getString("item_number") %>' order_id='<%=rows[i].getString("oid") %>' class="can_sendMessage" src="../imgs/ebay_logo.gif" width="48" height="19" align="absmiddle"> <span style="color:#99CC00;font-size:15px;font-weight:bold"><%=rows[i].getString("seller_id")%></span>
		<%
		}
		else
		{
		%>
		<strong><%=rows[i].getString("order_source")%></strong>
		<%
		}
		%>
		
		
		
		<span style="color:#999999">
		 <br>
		 <!--  
		客户:	<%=rows[i].getString("client_id")%>
		 -->
		</span>
        
 

<div style="margin-top:5px;margin-bottom:5px;width:95%;border-bottom:1px #cccccc solid"></div>
		<img src="../imgs/order_client.gif" width="14" height="14" align="absmiddle"> <%=rows[i].getString("first_name")%> <%=rows[i].getString("last_name")%> (<%=rows[i].getString("post_date")%>)
	
	<%
if (!rows[i].getString("print_date").equals(""))
{
%> 	
		<br>
		<img src="../imgs/order_printer.gif" width="16" height="16" align="absmiddle"> <%=rows[i].getString("print_date")%> <span style="color:#999999">[<%=rows[i].getString("print_manager")%>]</span>
<%
}
%>


<%
if (!rows[i].getString("ems_id").equals(""))
{
%>
<div style="margin-top:5px;margin-bottom:5px;width:95%;border-bottom:1px #cccccc solid"></div>
	  
	  <div style="margin-top:5px;">
<span style="font-family:Arial, Helvetica, sans-serif;font-size:13px;"><%=rows[i].getString("ems_id")%></span>
<a href="javascript:showWaybill(<%=rows[i].get("oid",0l)%>)"><img src="../imgs/order_shipping.gif" width="22" height="15" border="0" align="absmiddle"></a> 
<!--
<tst:authentication bindAction="com.cwc.app.api.OrderMgr.modPOrderProductInfo">
<a href="javascript:modOrderDeliveryNo(<%=rows[i].getString("oid")%>)"><img src="../imgs/modify.gif" width="12" height="12" border="0" align="absmiddle"></a>
</tst:authentication>
 -->
<%
}
%>       
</div>

	<div style="margin-top:5px;margin-bottom:5px;width:95%;border-bottom:1px #cccccc solid"></div>
  </td>
      <td align="left" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;padding-right:10px;' >
	  
	  <table width="100%" border="0" cellspacing="0" cellpadding="3">
	  
	  
<%
int displayNum = 5;

String normalNoteBg="";
DBRow orderNotes[] = orderMgr.getPOrderNoteByOid(rows[i].get("oid",0l));

if (orderNotes.length>displayNum)
{	
%>
  <tr>
    <td width="80" height="20" align="center" valign="middle" class="aaaaaa" id="pre_five_tracking_button_<%=rows[i].getString("oid")%>"><a href="javascript:preFiveTracking(<%=rows[i].getString("oid")%>)">最近5次</a></td>
    <td width="80" align="center" valign="middle" class="bbbbbb"   id="all_tracking_button_<%=rows[i].getString("oid")%>">
    	<!--  <a href="javascript:allTracking(<%=rows[i].getString("oid")%>)">全部跟进(<%=orderNotes.length%>)</a>-->
    	<a href="javascript:showOrderNote(<%=rows[i].get("oid",0l)%>,<%=rows[i].get("handle",0)%>,<%=rows[i].get("after_service_status",0)%>,<%=rows[i].get("handle_status",0)%>,<%=rows[i].get("product_status",0)%>,'<%=buyer_ip%>');">全部跟进(<%=orderNotes.length%>)</a>
    </td>
    <td  style="border-bottom:1px #cccccc solid;background:#ffffff;">&nbsp;</td>
  </tr>
<%
}
%>
  
  <tr  id="pre_five_tracking_<%=rows[i].getString("oid")%>">
    <th colspan="3" align="left" valign="middle"  style="padding:0px;font-weight:normal">
     <%
	  	if (rows[i].getString("order_source").equals(com.cwc.app.api.OrderMgr.ORDER_SOURCE_WEBSITE)&&buyer_ip.equals("")==false)
		{
	  	 %>
	  	
	  	 <fieldset id="fielsetCheckAddr_<%=rows[i].get("oid",0l)%>" style="-moz-border-radius:8px; border: 1px solid rgb(153, 153, 153); padding: 7px;  margin-top: 10px; margin-bottom: 10px; line-height: 17px; font-weight: bold; border-radius: 5px 5px 5px 5px;display:none;width: 220px;">
	  	  	 <span id="checkRS<%=rows[i].get("oid",0l) %>"></span>
		  			<script type="text/javascript">
							var para = "txn_id=<%=rows[i].getString("txn_id")%>";
							$.ajax({
								url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/checkAddressAction.action',
								type: 'post',
								dataType: 'json',
								timeout: 60000,
								cache:false,
								data:para,
								beforeSend:function(request){
								},
								error: function(){
									
								},
								success: function(data)
								{
									if (data.rs=="dif")
									{
										document.getElementById("fielsetCheckAddr_<%=rows[i].get("oid",0l) %>").style.display="";
										document.getElementById("checkRS<%=rows[i].get("oid",0l) %>").innerHTML = "<span style='padding:1px;font-size:12px;background:#FF0000;color:#FFFFFF;font-weight:normal'>[提示：疑问付款状态] </span><br/><span style='font-size:12px;font-weight:normal'><strong>Customer Addr:</strong> "+data.customer+"<br/><strong>Billing Addr:</strong> "+" "+data.billing+"<br/><strong>Delivery Addr:</strong> "+data.delivery+"</span>";
									}
									else if(data.rs=="different")
									{
										document.getElementById("fielsetCheckAddr_<%=rows[i].get("oid",0l) %>").style.display="";
										document.getElementById("checkRS<%=rows[i].get("oid",0l) %>").innerHTML = "<span style='padding:1px;font-size:12px;background:#FF0000;color:#FFFFFF;font-weight:normal'>[提示：疑问付款状态] </span><br/><span style='font-size:12px;font-weight:normal'>"+data.note+"</span>"; 
									}else
									{
										document.getElementById("fielsetCheckAddr_<%=rows[i].get("oid",0l) %>").style.display="none";
									}
								}
							});
				</script>
		  	 </fieldset>
	  <%} %>
	 <br/>
	 


<%

		for (int jj=0;jj<orderNotes.length; jj++)
		{
			if (orderNotes.length-displayNum>=0 && jj<orderNotes.length-displayNum)
			{
				continue;
			}
			
			//normalNoteBg = "#CC0099";//normalNoteBg = "#FF6600";
			if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.RECORD_DOUBT_ORDER||orderNotes[jj].get("trace_type",0)==tracingOrderKey.DOUBT_TRACE )
			{
				normalNoteBg = "#CC0099";
			}
			else if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.RECORD_DOUBT_ADDRESS_ORDER||orderNotes[jj].get("trace_type",0)==tracingOrderKey.DOUBT_ADDRESS_TRACE )
			{
				normalNoteBg = "#3366CC";
			}
			else if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.RECORD_DOUBT_PAY_ORDER||orderNotes[jj].get("trace_type",0)==tracingOrderKey.DOUBT_PAY_TRACE )
			{
				normalNoteBg = "#FF0000";
			}
			else if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.LACKING_TRACE )
			{
				normalNoteBg = "#666666";
			}
			else if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.RECORD_LACKING_ORDER )
			{
				normalNoteBg = "#666666";
			}
			else if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.ORDER_COST_VERIFY )
			{
				normalNoteBg = "#339966";
			}
			else
			{
				normalNoteBg="#FF9900";
			}
		%>
		<div style="font-size:12px;margin-bottom:3px;line-height:15px;word-wrap:break-word;width:220px;"><span style="font-size:12px;color:#ffffff;background:<%=normalNoteBg%>">[<%=tracingOrderKey.getTracingOrderKeyById(orderNotes[jj].getString("trace_type"))%>]  <strong><%=orderNotes[jj].getString("account")%></strong></span> &nbsp;
				<%
		if (orderNotes[jj].get("adid", 0l)==adminLoggerBean.getAdid())
		{
		%>
		<a href="javascript:modOrderNote(<%=rows[i].get("oid",0l)%>,<%=orderNotes[jj].getString("on_id")%>)"><img src="../imgs/modify.gif" width="12" height="12" border="0" alt="修改备注"></a> 
		<%
		}
		%>
			
		<br> <span style="color:#999999;font-size:11px;font-farmliy:Verdana"><%=orderNotes[jj].getString("post_date")%></span>
			
		<br>
		<%=StringUtil.ascii2Html(orderNotes[jj].getString("note"))%>&nbsp;
		<%
		//||tracingOrderKey.RETURN_PRODUCT==orderNotes[jj].get("trace_type",0):登记退货时，OrderMgr.sigReturnProducts
		//tracingOrderKey.NORMAL_REFUNDING==orderNotes[jj].get("trace_type",0)||:正常退款中，OrderMgr.markNormalRefunding
		if ((TracingOrderKey.NORMAL_REFUNDING==orderNotes[jj].get("trace_type",0)||TracingOrderKey.NORMAL_WARRANTYING==orderNotes[jj].get("trace_type",0)||TracingOrderKey.RETURN_PRODUCT==orderNotes[jj].get("trace_type",0))&&orderNotes[jj].get("rel_id",0l)>0 )
		{
		%>
			<!-- 根据集中特殊情况，显示相关追中连接 -->
			<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/return_product_list.html?cmd=search&key=<%=orderNotes[jj].get("rel_id",0l)%>&search_mode=1' target="_blank">[&nbsp;<%=orderNotes[jj].get("rel_id",0l) %>&nbsp;]</a>
		<%
		}
		else if(TracingOrderKey.CREATE_WARRANTY==orderNotes[jj].get("trace_type",0))
		{
		%>
			<a href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_list.html?cmd=search&search_mode=1&key='<%=orderNotes[jj].get("rel_id",0l) %>'" target="_blank" style="cursor: pointer;">[&nbsp;<%=orderNotes[jj].get("rel_id",0l) %>&nbsp;]</a>
		<%
		}
		else if ( (TracingOrderKey.FREE_SHIPPING_FEE_WARRANTY==orderNotes[jj].get("trace_type",0)||TracingOrderKey.FREE_WARRANTIED==orderNotes[jj].get("trace_type",0)||TracingOrderKey.OTHERS==orderNotes[jj].get("trace_type",0)||TracingOrderKey.WARRANTIED==orderNotes[jj].get("trace_type",0))&&orderNotes[jj].get("rel_id",0l)>0 )
		{
		%>
			<div  class="order-note-title">
			<span class="order-note-text">相关订单：<%=orderNotes[jj].get("rel_id",0l)%></span>			&nbsp;&nbsp;&nbsp;	
			<a  target="_blank" href="ct_order_auto_reflush.html?val=<%=orderNotes[jj].get("rel_id",0l)%>&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3"><img src="../imgs/application-blue.png" width="14" height="14" align="absmiddle" border="0"></a>
			 </div>
		<%
		}
		%>
		<style>
		.order-note-title
		{
			margin-bottom:10px;
			margin-top:5px;
			padding:0px;
		}
		.order-note-text
		{
			color:#3366FF;
			font-weight:bold;
		}
		</style>

		</div>
		
		
		<%
		}
		%>
	



		
		  
	
	
	
	</th>
    </tr>
  <tr style="display:none" id="all_tracking_<%=rows[i].getString("oid")%>">
    <td colspan="3" align="left" valign="middle"  style="padding:0px;font-weight:normal;border:0px;">
	<br>

	<%
	for (int jj=0;jj<orderNotes.length; jj++)
		{
			
			//normalNoteBg = "#CC0099";//normalNoteBg = "#FF6600";
			if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.RECORD_DOUBT_ORDER||orderNotes[jj].get("trace_type",0)==tracingOrderKey.DOUBT_TRACE )
			{
				normalNoteBg = "#CC0099";
			}
			else if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.RECORD_DOUBT_ADDRESS_ORDER||orderNotes[jj].get("trace_type",0)==tracingOrderKey.DOUBT_ADDRESS_TRACE )
			{
				normalNoteBg = "#3366CC";
			}
			else if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.RECORD_DOUBT_PAY_ORDER||orderNotes[jj].get("trace_type",0)==tracingOrderKey.DOUBT_PAY_TRACE )
			{
				normalNoteBg = "#FF0000";
			}
			else if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.LACKING_TRACE )
			{
				normalNoteBg = "#666666";
			}
			else if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.RECORD_LACKING_ORDER )
			{
				normalNoteBg = "#666666";
			}
			else if ( orderNotes[jj].get("trace_type",0)==tracingOrderKey.ORDER_COST_VERIFY )
			{
				normalNoteBg = "#339966";
			}
			else
			{
				normalNoteBg="#FF9900";
			}
		%>
		<div style="font-size:12px;margin-bottom:3px;line-height:15px;word-wrap:break-word;width:220px;"><span style="font-size:12px;color:#ffffff;background:<%=normalNoteBg%>">[<%=tracingOrderKey.getTracingOrderKeyById(orderNotes[jj].getString("trace_type"))%>]  <strong><%=orderNotes[jj].getString("account")%></strong></span> &nbsp;
		<%
			if (orderNotes[jj].get("adid", 0l)==adminLoggerBean.getAdid())
			{
		%>
			<a href="javascript:modOrderNote(<%=rows[i].get("oid",0l)%>,<%=orderNotes[jj].getString("on_id")%>)"><img src="../imgs/modify.gif" width="12" height="12" border="0" alt="修改备注"></a> 
		<%
			}
		%>
		
		<br> <span style="color:#999999;font-size:11px;font-farmliy:Verdana"><%=orderNotes[jj].getString("post_date")%></span>
			
		<br>
		<%=StringUtil.ascii2Html(orderNotes[jj].getString("note"))%>&nbsp;
		<%
		//||tracingOrderKey.RETURN_PRODUCT==orderNotes[jj].get("trace_type",0):登记退货时，OrderMgr.sigReturnProducts
		//tracingOrderKey.NORMAL_REFUNDING==orderNotes[jj].get("trace_type",0)|| : OrderMgr.markNormalRefunding
		if ((TracingOrderKey.NORMAL_REFUNDING==orderNotes[jj].get("trace_type",0)||TracingOrderKey.NORMAL_WARRANTYING==orderNotes[jj].get("trace_type",0)||TracingOrderKey.RETURN_PRODUCT==orderNotes[jj].get("trace_type",0))&&orderNotes[jj].get("rel_id",0l)>0 )
		{
		%>
			<!-- 根据集中特殊情况，显示相关追中连接 -->
			<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/return_product_list.html?cmd=search&key=<%=orderNotes[jj].get("rel_id",0l)%>&search_mode=1' target="_blank">[&nbsp;<%=orderNotes[jj].get("rel_id",0l) %>&nbsp;]</a>
		<%
		}
		else if(TracingOrderKey.CREATE_WARRANTY==orderNotes[jj].get("trace_type",0))
		{
		%>
			<a href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_list.html?cmd=search&search_mode=1&key='<%=orderNotes[jj].get("rel_id",0l) %>'" target="_blank" style="cursor: pointer;">[&nbsp;<%=orderNotes[jj].get("rel_id",0l) %>&nbsp;]</a>
		<%
		}
		else if ( (TracingOrderKey.FREE_SHIPPING_FEE_WARRANTY==orderNotes[jj].get("trace_type",0)||TracingOrderKey.FREE_WARRANTIED==orderNotes[jj].get("trace_type",0)||TracingOrderKey.OTHERS==orderNotes[jj].get("trace_type",0)||TracingOrderKey.WARRANTIED==orderNotes[jj].get("trace_type",0))&&orderNotes[jj].get("rel_id",0l)>0 )
		{
		%>
			<div  class="order-note-title">
		
			<span class="order-note-text">相关订单：<%=orderNotes[jj].get("rel_id",0l)%></span>			&nbsp;&nbsp;&nbsp;	
			<a  target="_blank" href="ct_order_auto_reflush.html?val=<%=orderNotes[jj].get("rel_id",0l)%>&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3"><img src="../imgs/application-blue.png" width="14" height="14" align="absmiddle" border="0"></a>
			
			 </div>
		<%
		}
		%>
		
		
		
		
		</div>
		
		<%
		}
		%>
	
	
	
	</td>
    </tr>
</table>

	  
	  	
	
		

		
		
	 </td>
      <td align="left" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' > 


<%
if (rows[i].getString("auction_buyer_id").equals("")==false)
{
%>
 <%=rows[i].getString("auction_buyer_id")%> <a href="javascript:searchEbayID('<%=rows[i].getString("auction_buyer_id")%>')"><img src="../imgs/search.png" width="14" height="14" border="0" align="absmiddle"></a><br>
<%
}
%>

<strong><%=rows[i].getString("client_id")%></strong> <a href="javascript:searchEmail('<%=rows[i].getString("client_id")%>')"><img src="../imgs/search.png" width="14" height="14" border="0" align="absmiddle"></a>

&nbsp;<span id="badreview<%=rows[i].get("oid",0l) %>"></span>
<script type="text/javascript">
	var para = "client_id=<%=rows[i].getString("client_id")%>";
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/ajaxBadReviewCount.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					
				},
				
				success: function(data)
				{
					var countStr = "";
				
					if(data.bad+0>0)
					{
						countStr += data.bad+"差评 ";
					}
					if(data.normal+0>0)
					{
						countStr += data.normal+"中评 ";
					}
					if(data.ebay_complain+0>0)
					{
						countStr += data.ebay_complain+"eBay投诉 ";
					}
					if(data.paypal_complain+0>0)
					{
						countStr += data.paypal_complain+"PayPal投诉 ";
					}
					
					if (countStr!="")
					{
						document.getElementById("badreview<%=rows[i].get("oid",0l) %>").innerHTML = "<span style='padding:1px;font-size:12px;background:#FF0000;color:#FFFFFF;font-weight:bold'>["+countStr+"]</span>";
					}
				}
			});
</script>
&nbsp;

<br>
  
<span class="alert-text" style="font-size:11px;">Item Number:</span> <%=rows[i].getString("item_number")%><br>
<span class="alert-text" style="font-size:11px;">Item Name:</span> <%=rows[i].getString("item_name")%>
<%
if (rows[i].getString("order_source").equals("WEBSITE")==false&&rows[i].getString("note").equals("")==false)
{
%>
<div style="color:#CC0000;border:1px #CCCCCC solid;background:#FFFFE1;padding:5px;width:95%;margin-top:5px;font-size:12px;font-family:Arial, Helvetica, sans-serif"><img src="../imgs/client.gif" width="21" height="25" align="absmiddle"> 
&nbsp; 
<%=rows[i].getString("note")%>
</div>
<%
} 
%>
<br>



<%
String devStorage = "";
String devStorageBG = "#ffffff";
DBRow detailDevStorage = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("ps_id",0l));
if (detailDevStorage!=null)
{
	devStorage = detailDevStorage.getString("title");
}
if (devStorage.equals(""))
{
	devStorage = "?";
}
%>


	  <fieldset style="border:2px #999999 solid;padding:7px;width:200px;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
<legend style="font-size:12px;font-weight:bold;color:#000000;background:<%=devStorageBG%>;"><span style="font-size:13px;color:#000000;font-weight:bold"></span>
<span style="font-weight:bold">
      
        <%
        	DecimalFormat df = new DecimalFormat("###############.##"); 
        	String value =  df.format(rows[i].get("mc_gross",0.0d)) ;
        %>
       <%=value%> 
		<%=rows[i].getString("mc_currency")%>	
		/ </span>
		<span style="font-weight:bold;color:#999999">
		<%=rows[i].get("mc_gross_rmb",0d)%> RMB
		</span>
		<a style="color:#993300;" href="javascript:showTradeItem('<%= rows[i].get("oid",0l) %>')">
	    	<span class="alert-text">&nbsp;件数</span> 
		<span style="font-weight:bold">: <%=rows[i].get("quantity",0f)%></span>
		</a>
</legend>



		
		
		
<%
float sonOrderQuantity = 0;


DBRow orderItems[] = orderMgr.getPOrderItemsByOid(rows[i].get("oid",0l));



DBRow sonOrderItems[] = orderMgr.getSonOrderItemsByParentOid(rows[i].get("oid",0l));
DBRow sonOrderDeliveryNotes[] = orderMgr.getSonOrderDeliveryNoteByParentOid(rows[i].get("oid",0l));

if (orderItems.length>0)
{
	for (int zz=0; zz<orderItems.length; zz++)
	{

		if (orderItems[zz].get("lacking",0)==ProductStatusKey.STORE_OUT)
		{
			out.println("<span style='color:#FF0000;'> [缺]</span>");
		}

		
		if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
		{
			out.println("<span style='color:#FF6600'><");
		}
		out.println(orderItems[zz].getString("name"));
		if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
		{
			out.println("></span>");
		}
		out.println(" x <span style='color:blue'>"+orderItems[zz].get("quantity",0f)+" "+orderItems[zz].getString("unit_name")+"</span><br>");

		if (orderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
		{
			out.println("<div style='color:#999999;padding-left:10px;font-weight:normal'>");
			DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( orderItems[zz].get("pid",0l) );
			for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
			{
				out.println("├ "+customProductsInSet[customProductsInSet_i].getString("p_name")+" x "+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name")+"<br>");
			}
			out.println("</div>");
		}
	}
	 
	for (int zz=0; zz<sonOrderItems.length; zz++)
	{
		sonOrderQuantity += sonOrderItems[zz].get("quantity",0f);
		
		if (sonOrderItems[zz].get("lacking",0)==ProductStatusKey.STORE_OUT)
		{
			out.println("<span style='color:#FF0000;'> [缺]</span>");
		}

		if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
		{
			out.println("<span style='color:#FF6600'><");
		}
		out.println(sonOrderItems[zz].getString("name"));
		if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
		{
			out.println("></span>");
		}
		
		out.println(" x <span style='color:blue'>"+sonOrderItems[zz].get("quantity",0f)+" "+sonOrderItems[zz].getString("unit_name")+"</span> <span style='color:#990000;font-weight:normal'>["+sonOrderItems[zz].getString("oid")+"]</span><br>");
		
		if (sonOrderItems[zz].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
		{
			out.println("<div style='color:#999999;padding-left:10px;font-weight:normal'>");
			DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( sonOrderItems[zz].get("pid",0l) );
			for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
			{
				out.println("├ "+customProductsInSet[customProductsInSet_i].getString("p_name")+" x "+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name")+"<br>");
			}
			out.println("</div>");
		}
	}
}
else
{
	out.println(StringUtil.ascii2Html(rows[i].getString("type")));
}
%>

<%
if ( rows[i].getString("delivery_note").equals("")==false||sonOrderDeliveryNotes.length>0 )
{
%>
<div style="font-weight:normal;background:#999999;color:#FFFFFF;padding:2px;margin-top:5px;">
<%=rows[i].getString("delivery_note")%>
<%
for (int dii=0; dii<sonOrderDeliveryNotes.length; dii++)
{
	if (sonOrderDeliveryNotes[dii].getString("delivery_note").equals(""))
	{
		continue;
	}
	out.println("<br>"+sonOrderDeliveryNotes[dii].getString("delivery_note"));
}
%>

</div>	
<%
}
%>	
		</fieldset>

		
		
		
		
		<input type="hidden" name="product_type_info<%=rows[i].getString("oid")%>" id="product_type_info<%=rows[i].getString("oid")%>" value="<%=rows[i].get("product_type",0)%>">
    
	 
	 
	 
			
		
		
		
		<br>

		  </td>
      <td align="left" valign="middle"  style='word-break:break-all;'> 
	  
	  
	      <p>
		        <span class="alert-text fixAddressLeft">Name: </span>
		        <span class="fixAddressRight">
		        	<%=StringUtil.replaceString(rows[i].getString("address_name"),"\""," ")%>
					<a href="javascript:searchName('<%=rows[i].getString("address_name")%>')"><img src="../imgs/search.png" width="14" height="14" border="0" align="absmiddle"></a> <br> 
        </span>
	      </p>
	      <p>
	        <span class="alert-text fixAddressLeft">Street:</span>
	        <span class="fixAddressRight"><%=rows[i].getString("address_street")%>  </span>
	      </p>
	      <p>
	        <span class="alert-text fixAddressLeft">City: </span><span class="fixAddressRight"><%=rows[i].getString("address_city")%></span>
          </p>
        <p>
         <span class="alert-text fixAddressLeft">State: </span><span class="fixAddressRight"><%=rows[i].getString("address_state")%></span>
		</p>
		<p>
          <span class="alert-text fixAddressLeft">Zip: </span><span class="fixAddressRight"><%=rows[i].getString("address_zip")%></span>
		</p>
		<p>
        	<span class="alert-text fixAddressLeft">Tel: </span> <span id="tel_<%=i%>" class="fixAddressRight"> <%=rows[i].getString("tel")%></span>
		</p>
		<p>
		 <span class="alert-text fixAddressLeft">Country: </span>
		
		<%
		if (rows[i].get("ccid",0l)==0)
		{
		%>
				<span class="fixAddressRight" style="background:#FF0000;font-size:14px;color:#ffffff;font-weight:bold"><%=rows[i].getString("address_country")%> - <%=rows[i].getString("c_code")%></span>
		<%
		}
		else
		{
		%>
				<span class="fixAddressRight" style="font-size:14px;color:#000000;text-decoration: none"><%=rows[i].getString("address_country")%> - <%=rows[i].getString("c_code")%></span>
		<%
		}
		%>
		
		 </p>
		 <p>
        <span class="alert-text fixAddressLeft">Status: </span>
        <span class="fixAddressRight">
	        <font color="<%=rows[i].getString("address_status_color")%>">
	        <strong> 
	        	<%=rows[i].getString("address_status")%>
	        </strong>
		</font> 
		</span>
		
		
        <!-- 
		<tst:authentication bindAction="com.cwc.app.api.OrderMgr.modPOrderDeliveryInfo">
        <input name="Submit42" type="button" class="long-button-mod" onClick="modOrderDeliveryInfo(<%=rows[i].getString("oid")%>,<%=i%>)" value="修改详细">
		</tst:authentication>	  
		 -->
		 </p>
		</td>
		<td  align="left" valign="middle"  style='word-break:break-all;' > 
			<!-- 读取与这个订单想关联的交易 -->
			<%
				DBRow[] relationTrade = orderMgrZr.getRelationTradeByOid(rows[i].get("oid",0l));
				if(relationTrade != null && relationTrade.length > 0){
					for(DBRow tradeRow :  relationTrade){
						String  borderColor = tradeRow.get("is_create_order",0) == 1 ? "border-color:green;":"";
						String fontColor = "";
						if(tradeRow.getString("parent_txn_id").length() > 0){
							fontColor = tradeRow.getString("parent_txn_id").length() > 0 ? "color:red;":"";
							borderColor = "border-color:red;";
						}
			%>
					<fieldset style="<%=borderColor %>" class="set" >
						<legend class="legend">
							<span style="font-size:13px;color:#000000;font-weight:bold"></span>
							<span style="font-weight:bold"> <%=  tradeRow.get("mc_gross",0.0d)%> <%=  tradeRow.getString("mc_currency")%></span>
							<span style="font-weight:bold;"><span style="<%=fontColor %>"> <%=  tradeRow.getString("payment_status")%> </span>|
							 <%
							 String ipAddress = tradeRow.getString("ip_address");
						 		if(ipAddress.length() > 0){
						 			String[] array = ipAddress.split("-");
						 			if(array.length >= 2){
						 				ipAddress = array[0];
						 			}else{
						 				ipAddress = "";
						 			}
						 		} 
						 		
							   if(tradeRow.get("payment_channel",-1) == (payTypeKeyZr.CC)){
 							 		if(ipAddress.split("\\.").length == 4){
							 			out.print("<a href='http://www.123cha.com/ip/?q="+ipAddress+"' target='_blank'><img src='../imgs/visa.jpg' width='22' height='14' border='0' align='absmiddle'></a>");
							 		}else{
							 			out.print(payTypeKeyZr.getStatusById(tradeRow.get("payment_channel",-1)));
							 		}
							   }else if(tradeRow.get("payment_channel",-1) == (payTypeKeyZr.PP) && ipAddress.split("\\.").length == 4){
								   out.print("<a href='http://www.123cha.com/ip/?q="+ipAddress+"' target='_blank'>"+payTypeKeyZr.getStatusById(tradeRow.get("payment_channel",-1))+"</a>");
							   }else{
								   out.print(payTypeKeyZr.getStatusById(tradeRow.get("payment_channel",-1)));
							   }
							 %> 
							</span>  						
							 <%
 							 %>
						</legend>
				 		<!-- 显示基本的信息 -->
				 		<p>
				 			 <span>付款账号: <%= tradeRow.getString("payer_email") %>
				 			 </span><br/>
				 			 <span>收款人:<%= tradeRow.getString("receiver_email") %></span><br /><span>收款账号: <%= tradeRow.getString("business") %></span>
				 			<br />
				 			<%
				 				if(tradeRow.getString("txn_id").length() > 0 ){
				 					%>
				 					<span>TxnId :<%=tradeRow.getString("txn_id") %></span><br />
				 					<% 
				 				}
				 			%>
				 			<span>Date :<%=tradeRow.getString("post_date") %></span><br />
				 			<%
				 				if(tradeRow.getString("parent_txn_id").length() > 0 || tradeRow.getString("reason_code").length() > 0 ){
				 					// 表示的是关联交易
				 					if(tradeRow.getString("reason_code").length() > 0 )	{
				 						%>
				 						<span>Reason Code :<%= tradeRow.getString("reason_code") %> </span><br />
				 						<%
				 					}
				 					if(tradeRow.getString("case_id").length() > 0 )	{
				 						%>
				 						<span>Case ID :<%= tradeRow.getString("case_id") %> </span><br />
				 						<%
				 					}
				 					if(tradeRow.getString("case_type").length() > 0 )	{
				 						%>
				 						<span>Case Type :<%= tradeRow.getString("case_type") %> </span><br />
				 						<%
				 					}
				 				}
				 			%>
				 		</p>
					</fieldset>
			<% 			
					}
				}
			%>
			 <a href="javascript:getMoreTrade('<%=rows[i].get("oid",0l) %>');">更多详情</a>
		</td>
    </tr>
    <tr >
      <th height="29" colspan="6" align="left" valign="middle" class="line-bg"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <th width="400" valign="middle" style="text-align:left;padding-left:10px;padding-right:10px;border-right:1px solid #999999;">
		  					<%
			//不等于取消、已发货、待审核、已部分退款、全部退款、正常退款中、争议退款中、正常质保中、争议质保中、已质保、其他争议
			if(rows[i].get("handle",0)==HandleKey.ALLOUTBOUND||rows[i].get("handle",0)!=HandleKey.DELIVERIED&&rows[i].get("handle_status",0)!=HandStatusleKey.CANCEL&&rows[i].get("handle",0)!=HandleKey.VERIFY_COST&&!(rows[i].get("handle",0)>=AfterServiceKey.NORMAL_REFUNDING)&&rows[i].get("handle",0)<=AfterServiceKey.FAILURE_DISPUTING&&rows[i].get("handle_status",0)!=HandStatusleKey.ADDITION_MONEY)
			{
			%>
			
			<%
			if (rows[i].get("handle_status",0)!=handStatusleKey.ARCHIVE)
			{
			%>
			<tst:authentication bindAction="com.cwc.app.api.OrderMgr.recordOrder">
				<input name="Submit32" type="button" class="short-short-button-cart" onClick="recordOrder(<%=rows[i].getString("oid")%>,<%=rows[i].getString("handle")%>, '<%=rows[i].getString("txn_id")%>','<%=rows[i].getString("order_source")%>')"  value="抄单" >
			</tst:authentication>	
				&nbsp;&nbsp;&nbsp;		
			<%
			}
			%>
	

			<%
			if (rows[i].get("parent_oid",0l)>0)
			{
			%>
			<tst:authentication bindAction="com.cwc.app.api.OrderMgr.undoMergeOrder">
			<input name="Submit33222" type="button" class="short-short-button-unmerge" onClick="undoMergeOrder(<%=rows[i].get("oid",0l)%>,<%=rows[i].get("parent_oid",0l)%>)"  value="解合" >
			</tst:authentication>
			<%
			}
			else if (rows[i].get("parent_oid",0l)==0&&rows[i].get("handle_status", 0)!=HandStatusleKey.ARCHIVE&&rows[i].get("handle_status", 0)!=HandStatusleKey.DOUBT&&rows[i].get("handle_status", 0)!=HandStatusleKey.DOUBT_ADDRESS&&rows[i].get("handle", 0)!=handleKey.VERIFY_COST&&rows[i].get("handle_status", 0)!=HandStatusleKey.DOUBT_PAY)
			{
			%>
			<!-- 
			<tst:authentication bindAction="com.cwc.app.api.OrderMgr.mergeOrder">
			<input name="Submit33222" type="button" class="short-short-button-merge" onClick="mergeOrder(<%=rows[i].get("oid",0l)%>)"  value="合并" >
			</tst:authentication>
			-->
			<%
			}
			%>		 
					&nbsp;&nbsp;&nbsp;
			<%
			}
			%> 
			
	
			
<%
			if (rows[i].get("handle", 0)==handleKey.VERIFY_COST)
			{
			%>
			<tst:authentication bindAction="com.cwc.app.api.OrderMgr.verifyOrderCost">
			<input name="Submit33" type="button" class="short-short-button-redtext" onClick="verifyOrderCost(<%=rows[i].get("oid",0l)%>)"  value="审核"  style="font-weight:bold">
			</tst:authentication>
			&nbsp;&nbsp;&nbsp;
			<%
			}
			else
			{
			%>
			
			<%
			}
			%>
			
			
			

			<%
			//不等于取消、归档，部分退款、全部退款
			//if (rows[i].get("handle_status",0)!=HandStatusleKey.CANCEL&&rows[i].get("handle_status", 0)!=HandStatusleKey.ARCHIVE&&rows[i].get("handle",0)!=HandleKey.PART_REFUNDED&&rows[i].get("handle",0)!=HandleKey.ALL_REFUNDED&&rows[i].get("handle",0)!=HandleKey.FAILURE_REFUND&&rows[i].get("handle",0)!=HandleKey.FAILURE_WARRANTY&&rows[i].get("handle",0)!=HandleKey.FAILURE_DISPUTING&&rows[i].get("handle",0)!=HandleKey.BAD_DISPUTING&&rows[i].get("handle",0)!=HandleKey.BAD_WARRANTY&&rows[i].get("handle",0)!=HandleKey.BAD_REFUND&&rows[i].get("handle_status",0)!=HandStatusleKey.ADDITION_MONEY)
			if (rows[i].get("handle",0)!=AfterServiceKey.PART_REFUNDED&&rows[i].get("handle",0)!=AfterServiceKey.FAILURE_REFUND&&rows[i].get("handle",0)!=AfterServiceKey.FAILURE_WARRANTY&&rows[i].get("handle",0)!=AfterServiceKey.FAILURE_DISPUTING&&rows[i].get("handle",0)!=AfterServiceKey.BAD_DISPUTING&&rows[i].get("handle",0)!=AfterServiceKey.BAD_WARRANTY&&rows[i].get("handle",0)!=AfterServiceKey.BAD_REFUND&&rows[i].get("handle_status",0)!=HandStatusleKey.ADDITION_MONEY)
			{
			%>

			<input type="button" name="Submit" value=" " onClick="funMenu(this,<%=rows[i].get("oid",0l)%>,<%=rows[i].get("handle",0)%>,<%=rows[i].get("after_service_status",0)%>,<%=rows[i].get("handle_status",0)%>,<%=rows[i].get("bad_feedback_flag",0l)%>,'<%=rows[i].getString("txn_id")%>', '<%=rows[i].getString("mc_gross")%>','<%=rows[i].getString("mc_currency") %>')" id="menu_button_<%=rows[i].get("oid",0l)%>" class="more-fun-short-button">
			<%
			}
			%>


			<%
			if (rows[i].get("handle_status",0)==handStatusleKey.ADDITION_MONEY)
			{
			%>
			<tst:authentication bindAction="com.cwc.app.api.OrderMgr.unDoAdditionMoneyOrder">
			<input name="Submit32" type="button" class="long-button" onClick='unDoAdditionMoneyOrder(<%=rows[i].getString("oid")%>)'  value="取消补钱" >
			</tst:authentication>	
				&nbsp;&nbsp;&nbsp;		
			<%
			}
			%>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- 	<input type="button"   style="border:0px;background:url('../imgs/dingdanrizhi.jpg');width:70px;" onclick="showOrderNote(<%=rows[i].get("oid",0l)%>,<%=rows[i].get("handle",0)%>,<%=rows[i].get("after_service_status",0)%>,<%=rows[i].get("handle_status",0)%>,<%=rows[i].get("product_status",0)%>,'<%=buyer_ip%>');"/> -->
			  	&nbsp;&nbsp; 
				<tst:authentication bindAction="com.cwc.app.api.OrderMgr.addPOrderNote">
				<%
					String orderTrackingImg = "dindangenjin.jpg";
			
//					if (rows[i].get("trace_flag",0)==1&&tDate.getDateTime(rows[i].getString("trace_date"))<=tDate.getDateTime())
//					{
//						orderTrackingImg = "jixugenjin.jpg";
//					}
				%>
			 	<input type="button" onclick="addOrderNote(<%=rows[i].get("oid",0l)%>,<%=rows[i].get("handle",0)%>,<%=rows[i].get("after_service_status",0)%>,<%=rows[i].get("handle_status",0)%>,<%=rows[i].get("product_status",0)%>,'<%=buyer_ip%>')" style="border:0px;background:url('../imgs/<%=orderTrackingImg%>');width:70px;"/>
				</tst:authentication>
				<%
					String serviceOrdersBtnStyle = "style='color:red;'";
					if(serviceOrderMgrZyj.getServiceOrderCountByPOrderId(rows[i].get("oid",0l)) > 0)
					{
						serviceOrdersBtnStyle = "style='color:green;'";
					}
				%>
				<input class="short-button" type="button" value="查看服务" onclick="showServiceList('<%=rows[i].get("oid",0l) %>')" <%=serviceOrdersBtnStyle %>/>
			</th>

          <th style="text-align:left;padding-left:10px;">
			&nbsp;
		  </th>
        </tr>
      </table></th>
    </tr>
    
    <%
}
%>
</table>
  </form>
  
    <form name="dataForm"  method="get">
          <input type="hidden" name="p">
          <input type="hidden" name="val" value="<%=val%>">
		  <input type="hidden" name="st" value="<%=st%>">
		  <input type="hidden" name="en" value="<%=en%>">
		  <input type="hidden" name="business" value="<%=business%>">
		  <input type="hidden" name="handle" value="<%=handle%>">
		  <input type="hidden" name="handle_status" value="<%=handle_status%>">
		  <input type="hidden" name="product_type_int" value="<%=product_type_int%>">
		  <input type="hidden" name="cmd" value="<%=cmd%>">
		  <input type="hidden" name="product_status" value="<%=product_status%>">
		  <input type="hidden" name="ps_id" value="<%=ps_id%>">
		  <input type="hidden" name="search_field" value="<%=search_field%>">
		  <input type="hidden" name="search_mode" value="<%=search_mode%>"/>
		  <input type="hidden" name="p_name" value="<%=p_name%>">
		  <input type="hidden" name="filter_ps_id" value="<%=filter_ps_id%>">
		  <input type="hidden" name="trace_type" value="<%=trace_type%>">
		  <input type="hidden" name="order_note_adid" value="<%=order_note_adid%>">
		  <input type="hidden" name="order_note_trace_type" value="<%=order_note_trace_type%>">
		  <input type="hidden" name="order_source" value="<%=order_source%>">
		  <input type="hidden" name="bad_feedback_flag" value="<%=bad_feedback_flag%>">
		  <input type="hidden" name="after_service_status" value="<%=after_service_status%>">
		  <input type="hidden" name="product_line_id" value="<%=product_line_id%>"/>
		  <input type="hidden" name="catalog_id" value="<%=catalog_id%>"/>
		  <input type="hidden" name="ca_id" value="<%=ca_id%>"/>
		  <input type="hidden" name="ccid" value="<%=ccid%>"/>
		  <input type="hidden" name="pro_id" value="<%=pro_id%>"/>
		  <input type="hidden" name="cost" value="<%=cost%>"/>
		  <input type="hidden" name="cost_type" value="<%=cost_type%>"/>
		  <input type="hidden" name="unfinished" value="<%=unfinished%>"/>
		  <input type="hidden" name="internal_tracking_status" value="<%=internal_tracking_status%>"/>
		  <input type="hidden" name="tracking_date_type" value="<%=tracking_date_type%>"/>
		  <input type="hidden" name="last_day" value="<%=last_day%>"/>
  </form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
pre = pc.getPageNo() - 1;
next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go2page(1);",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go2page(" + pre + ");",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go2page(" + next + ");",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go2page(" + pc.getPageCount() + ");",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit2" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go2page(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
<br>

</body>
</html>






<%
}
catch(ProductNotExistException e)
{
	out.print("<font color=\"red\" size=\"72\">无此商品！</font>");
}

//long page_ent = System.currentTimeMillis();
//System.out.println("Order Page:"+(page_ent-stt));
%>

