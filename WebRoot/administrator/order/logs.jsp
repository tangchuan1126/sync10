<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单处理</title>

<link href="../../style.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="715" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cccccc">
  <tr>
    <td width="185" height="25" align="center" valign="middle" bgcolor="eeeeee" style="font-size:12px;"><strong>操作人</strong></td>
    <td width="317" align="center" valign="middle" bgcolor="eeeeee" style="font-size:12px;"><strong>操作</strong></td>
    <td width="191" align="center" valign="middle" bgcolor="eeeeee" style="font-size:12px;"><strong>日期</strong></td>
  </tr>
<%
long oid = StringUtil.getLong(request,"oid");
DBRow logs[] = orderMgr.getPOrderLogByOid(oid,null);

for (int i=0;i<logs.length; i++)
{
%>  
  <tr>
    <td height="25" align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;"><%=logs[i].getString("account")%></td>
    <td align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;"><%=logs[i].getString("operation")%></td>
    <td align="center" valign="middle" bgcolor="#FFFFFF" style="font-size:12px;"><%=logs[i].getString("opost_date")%></td>
  </tr>
<%
}
%>
</table>
</body>
</html>