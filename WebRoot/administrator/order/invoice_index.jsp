<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<html>
<head>
<title>Invoice Index</title>

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

</head>

<body>
	 <div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
			 <ul>
				 <li><a href="#tool">常用工具</a></li>
				 <li><a href="#filter">过滤</a></li>		 
			 </ul>
			 <div id="tool">
				1111
			 </div>
			 <div id="filter" style="">
		  		2222
			 </div>	
	 </div>
	<script type="text/javascript">
		$("#tabs").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
		});
	
	 $("#tabs").tabs("select",0);
	</script>
	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="true" class="zebraTable" style="margin-left:3px;margin-top:5px;">
		 <tr> 
		  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">综合信息</th>
		  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">收款人信息</th>
		  	<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">商品详细</th>
	        <th width="25%" style="vertical-align: center;text-align: center;" class="right-title">费用信息</th>
	    	<th width="5%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
	      </tr>
	      <tr>
	      	<td>11<br/><br/></td>
	      	<td>11</td>
	      	<td>11</td>
	      	<td>11</td>
	      	<td>11</td>
	      </tr>
	</table>
</body>
