<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
 <jsp:useBean id="tracingOrderKey" class="com.cwc.app.key.TracingOrderKey"/>
<jsp:useBean id="handStatusleKey" class="com.cwc.app.key.HandStatusleKey"/>
<jsp:useBean id="productStatusKey" class="com.cwc.app.key.ProductStatusKey"/>
<jsp:useBean id="orderRateKey" class="com.cwc.app.key.OrderRateKey"/>
<jsp:useBean id="returnProductKey" class="com.cwc.app.key.ReturnProductKey"/>
<%@ page import="com.cwc.app.beans.AdminLoginBean,com.cwc.app.key.AfterServiceKey,com.cwc.app.api.OrderMgr"%>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add Order Note</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
 
<style type="text/css">
*{margin:0px;padding:0px;}
div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .title{ background: url("../js/popmenu/pro_title.jpg") no-repeat scroll left center transparent;color: #FFFFFF;display: table;font-size: 14px; height: 27px;padding-left: 5px; padding-top: 3px;width: 356px;line-height:27px;}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:80px;text-align:center;border:1px solid silver;margin:0px auto;width:300px;height:30px;background:white;line-height:30px;font-size:14px;}	
</style>
<%
	//oid,handle,after_service_status,handle_status,product_status,buyer_ip
long oid = StringUtil.getLong(request,"oid");
long handle = StringUtil.getLong(request,"handle");
long after_service_status = StringUtil.getLong(request,"after_service_status");
long handle_status = StringUtil.getLong(request,"handle_status");
long product_status = StringUtil.getLong(request,"product_status");
long on_id = StringUtil.getLong(request,"on_id");
DBRow noteRow = new DBRow() ;
if(on_id != 0l){
	noteRow = orderMgrZr.getDetailById(on_id,"porder_note","on_id"); 
}
String buyer_ip = StringUtil.getString(request,"buyer_ip");

AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
String modOrderNote = ConfigBean.getStringValue("systenFolder") +"action/administrator/order/modOrderNote.action" ;
String addOrderNote = ConfigBean.getStringValue("systenFolder") +"action/administrator/order/addPOrderNote.action";
%>
<script type="text/javascript">
	jQuery(function($){
		init(<%= oid%>,<%= handle%>,<%= after_service_status%>,<%= handle_status%>,<%= product_status%>,'<%= buyer_ip%>')
	 	 
	 
	})	
	function init(oid,handle,after_service_status,handle_status,product_status,buyer_ip){
		var trace_type_radio = "";
		//跟据订单状态，提供不同跟踪类型
		if (handle_status==<%=handStatusleKey.DOUBT%>)
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.DOUBT_TRACE%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.DOUBT_TRACE) )%> &nbsp;";
		}
		else if ( handle_status==<%=handStatusleKey.DOUBT_ADDRESS%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.DOUBT_ADDRESS_TRACE%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.DOUBT_ADDRESS_TRACE) )%> &nbsp;";
		}
		else if ( handle_status==<%=handStatusleKey.DOUBT_PAY%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.DOUBT_PAY_TRACE%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.DOUBT_PAY_TRACE) )%> &nbsp;";
		}
		
		if(handle ==<%=HandleKey.DELIVERIED%>)
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.WAYBILL_TRACKING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.WAYBILL_TRACKING))%> &nbsp;";
		}
		
		//缺货跟进
		if ( product_status==<%=productStatusKey.STORE_OUT%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.LACKING_TRACE%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.LACKING_TRACE) )%> &nbsp;";
		}
		
		
		
		//售后问题
		if ( after_service_status==<%=AfterServiceKey.NORMAL_REFUNDING%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.NORMAL_REFUNDING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.NORMAL_REFUNDING) )%> &nbsp;";
		}
		if ( after_service_status==<%=AfterServiceKey.DISPUTE_REFUNDING%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.DISPUTE_REFUNDING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.DISPUTE_REFUNDING) )%> &nbsp;";
		}
		if ( after_service_status==<%=AfterServiceKey.PART_REFUNDED%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.PART_REFUNDED%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.PART_REFUNDED) )%> &nbsp;";
		}
		if ( after_service_status==<%=AfterServiceKey.ALL_REFUNDED%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.ALL_REFUNDED%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.ALL_REFUNDED) )%> &nbsp;";
		}	
		if ( after_service_status==<%=AfterServiceKey.NORMAL_WARRANTYING%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.NORMAL_WARRANTYING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.NORMAL_WARRANTYING) )%> &nbsp;";
		}	
		if ( after_service_status==<%=AfterServiceKey.DISPUTE_WARRANTYING%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.DISPUTE_WARRANTYING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.DISPUTE_WARRANTYING) )%> &nbsp;";
		}
		if ( after_service_status==<%=AfterServiceKey.WARRANTIED%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.WARRANTIED%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.WARRANTIED) )%> &nbsp;";
		}
		if ( after_service_status==<%=AfterServiceKey.OTHER_DISPUTING%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.OTHER_DISPUTING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.OTHER_DISPUTING) )%> &nbsp;";
		}

		if ( after_service_status==<%=AfterServiceKey.FREE_WARRANTIED%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.FREE_WARRANTIED%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.FREE_WARRANTIED) )%> &nbsp;";
		}
		if ( after_service_status==<%=AfterServiceKey.FAILURE_REFUND%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.FAILURE_REFUND%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.FAILURE_REFUND) )%> &nbsp;";
		}
		if ( after_service_status==<%=AfterServiceKey.FAILURE_WARRANTY%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.FAILURE_WARRANTY%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.FAILURE_WARRANTY) )%> &nbsp;";
		}
		if ( after_service_status==<%=AfterServiceKey.FAILURE_DISPUTING%> )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.FAILURE_DISPUTING%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.FAILURE_DISPUTING) )%> &nbsp;";
		}
		
		
		
		if ( buyer_ip!="" )
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.CREDIT_VERIFY%>' /><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.CREDIT_VERIFY) )%> &nbsp;";
		}
		
		trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=tracingOrderKey.OTHERS%>' /><span style='color:#FF6600'><%=tracingOrderKey.getTracingOrderKeyById( String.valueOf(tracingOrderKey.OTHERS) )%></span> &nbsp;";
		$("#radioNoteType").html(trace_type_radio);
	
	}
	 function cancel(){$.artDialog && $.artDialog.close();}
	 function submitForm(){
		var _url ;
		if('<%= on_id%>' * 1 > 0){
			_url = '<%= modOrderNote%>';
		}else{
			_url = '<%= addOrderNote%>';
		}
		var submitForm = $("#submitForm");
		$(".cssDivschedule").css("display","block");
		 $.ajax({
			url:_url,
			data:submitForm.serialize(),
			dataType:'text',
			success:function(data){
				$(".cssDivschedule").css("display","none");
				$.artDialog.opener && $.artDialog.opener.refreshWindow();
				cancel();
				 
			},
		 
			error:function(data){
				$.artDialog.opener && $.artDialog.opener.refreshWindow();
				cancel();
			}
		})
	}
		function cancel(){
			$.artDialog && $.artDialog.close();
		}
		
 </script>
</head>
<body   >
<!-- 遮盖层 -->
  <div class="cssDivschedule">
 		<div class="innerDiv">
 			请稍等.......
 		</div>
 </div>
 	<!-- 上面显示已经添加的Order的Note -->
 	<!-- 下面显示添加Order Note的页面 -->
 	<div class="title" style="font-weight: bold;margin-bottom:15px;">订单跟进[单号:<%=oid %>]</div>
 	<form id="submitForm">
 		<input type="hidden" name="oid" value="<%=oid %>"/>
 		<input type="hidden" name="on_id" value="<%=on_id %>" >
	 	<p id="radioNoteType"  ></p>
<textarea name='note' id='orderNoteWinText'  style='width:405px;height:147px;margin-top:10px;'>
<%= noteRow.getString("note") %>
</textarea>
	  
	 	 <div class="buttonDiv" style="margin-top:5px;"> 
	 	 	<input type="button" id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil"  onclick="submitForm()" value="提交"> 
			<button id="jqi_state0_button取消" value="n" class="jqidefaultbutton specl" name="jqi_state0_button取消" onclick="cancel();">取消</button>
	 	 </div>
 	</form>
 	<script type="text/javascript">
 	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 		
 	</script>
</body>
</html>
