<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<jsp:useBean id="quoteKey" class="com.cwc.app.key.QuoteKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
 	PageCtrl pc = new PageCtrl();
 pc.setPageNo(StringUtil.getInt(request,"p"));
 pc.setPageSize(30);

 String st = StringUtil.getString(request,"st");
 String en = StringUtil.getString(request,"en");
 String create_account = StringUtil.getString(request,"create_account");
 int quote_status = StringUtil.getInt(request,"quote_status",-1);
 int field = StringUtil.getInt(request,"field");
 String cmd = StringUtil.getString(request,"cmd");
 String key = StringUtil.getString(request,"key");

 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

 DBRow quotes[];

 if (cmd.equals("search"))
 {
 	quotes = quoteMgr.getSearchResult( key, st, en, create_account, quote_status, field, pc);
 }
 else
 {

 	quotes = quoteMgr.getAllQuotes(pc);
 }
 %> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单报价</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />


<style type="text/css">
<!-- 

.zebraTable tr td a:link {
	color:#000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:visited {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:hover {
	color: #000000;
	text-decoration: underline;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:active {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}

form 
{
	padding:0px;
	margin:0px;
}

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_quote.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}

-->
</style>

<script>
function closeTBWin()
{
	tb_remove();
}

function trueOrder(qoid,ps_id,sc_id,ccid,client_id,pro_id)
{

		document.record_form.qoid.value=qoid;
		document.record_form.ps_id.value=ps_id;			
		document.record_form.sc_id.value=sc_id;		
		document.record_form.ccid.value=ccid;			
		document.record_form.client_id.value=client_id;		
		document.record_form.pro_id.value=pro_id;					
		document.record_form.submit();
}

function finishQuote(qoid)
{
	$.prompt(
	
	"<div id='title'>成单[报价订单:"+qoid+"]</div><br />相关订单号 <input type='text' name='oid' id='oid'  style='width:100px;'/>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.oid == "")
						  {
							   alert("请填写相关订单号");
							   
								return false;
						  }
						  return true;
					}
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.finishQuote_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/FinishQuote.action";
						document.finishQuote_form.oid.value = f.oid;
						document.finishQuote_form.qoid.value = qoid;
						document.finishQuote_form.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function modFinishQuote(qoid)
{
	$.prompt(
	
	"<div id='title'>修改成单[报价订单:"+qoid+"]</div><br />相关订单号 <input type='text' name='oid' id='oid'  style='width:100px;'/>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.oid == "")
						  {
							   alert("请填写相关订单号");
							   
								return false;
						  }
						  return true;
					}
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.finishQuote_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/ModFinishQuote.action";
						document.finishQuote_form.oid.value = f.oid;
						document.finishQuote_form.qoid.value = qoid;
						document.finishQuote_form.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function approveQuote(qoid)
{
	$.prompt(
	
	"<div id='title'>成单审核[报价订单:"+qoid+"]</div><br>审核 &nbsp;&nbsp;&nbsp; <input type='radio' name='manager_approve' id='manager_approve' value='1' checked/>正常 &nbsp;&nbsp;&nbsp;<input type='radio' name='manager_approve' id='manager_approve' value='2' />无效<br>  <textarea name='approve_note' id='approve_note' style='width:200px;height:50px;'></textarea><br>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.manager_approve==2&&f.approve_note=="")
						  {
							   alert("请填写备注");
							   
								return false;
						  }

						  return true;
					}
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.approveQuote_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/ApproveQuote.action";
						document.approveQuote_form.manager_approve.value = f.manager_approve;
						document.approveQuote_form.approve_note.value = f.approve_note;
						document.approveQuote_form.qoid.value = qoid;
						document.approveQuote_form.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function cancelQuote(qoid)
{
	if (confirm("确定取消报价单："+qoid+"？"))
	{
	document.cancel_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/CancelQuote.action";
	document.cancel_form.qoid.value = qoid;
	document.cancel_form.submit();	
	}
}

function onLoadInit()
{
	$("#st").date_input();
	$("#en").date_input();
	onLoadInitZebraTable();
}

function myQuote()
{
	document.my_quote_form.submit();
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInit();">
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/recordOrderQuote.action" method="post" name="record_form" id="record_form">
<input type="hidden" name="qoid" />
<input type="hidden" name="ps_id"/>
<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>"/>	
<input type="hidden" name="sc_id" />
<input type="hidden" name="ccid" />
<input type="hidden" name="pro_id" />
<input type="hidden" name="client_id" />
</form>

<form  method="post" name="finishQuote_form" id="finishQuote_form">
<input type="hidden" name="qoid" >
<input type="hidden" name="oid">
</form>

<form  method="post" name="approveQuote_form" id="approveQuote_form">
<input type="hidden" name="qoid" >
<input type="hidden" name="manager_approve">
<input type="hidden" name="approve_note">
</form>

<form  method="post" name="cancel_form" id="cancel_form">
<input type="hidden" name="qoid" >
</form>

<form  method="get" name="my_quote_form" id="my_quote_form">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="st" value="2010-1-1">
<input type="hidden" name="en" value="<%=DateUtil.NowStr().substring(0,10)%>">
<input type="hidden" name="create_account" value="<%=adminLoggerBean.getAccount()%>">
</form>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 订单管理 »   订单报价</td>
  </tr>
</table>


<br>
<table width="98%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="67%">
	  <form name="form1" method="get" action="quote.html">
	  <input type="hidden" name="cmd" value="search">
	<div style="border:2px #dddddd solid;padding:5px;background:#eeeeee;width:702px;-webkit-border-radius:7px;-moz-border-radius:7px;padding-left:15px;">
	
<table width="702" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="568" height="30">
		<%
TDate tDate = new TDate();
tDate.addDay(-30);

String input_st_date,input_en_date;
if ( st.equals("") )
{	
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}
else
{	
	input_st_date = st;
}


if ( en.equals("") )
{	
	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
}
else
{	
	input_en_date = en;
}
%>

		开始
          <input type="text" name="st" id="st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" >
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  结束
          <input type="text" name="en" id="en"  value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" > 
         &nbsp;&nbsp;&nbsp;&nbsp;
		 
		  报价人
     
        <select name="create_account" id="create_account" style="border:1px #CCCCCC solid" >
          <option value="0">全部客服</option>
                <%
DBRow admins[] = adminMgr.getAllAdmin(null);
for (int i=0; i<admins.length; i++)
{
%>
                <option value="<%=admins[i].getString("account")%>" <%=admins[i].getString("account").equals(create_account)?"selected":""%>> 
                <%=admins[i].getString("account")%>                </option>
                <%
}
%>
        </select>
		
          报价状态
          <select name="quote_status" id="quote_status"  style="border:1px #CCCCCC solid" >
            <option value="-1">全部</option>
                <%
ArrayList quoteStatusAL = quoteKey.getQuoteStatus();
for (int i=0; i<quoteStatusAL.size(); i++)
{
%>
                <option value="<%=quoteStatusAL.get(i)%>" <%=StringUtil.getInt(quoteStatusAL.get(i).toString())==quote_status?"selected":""%>> 
                <%=quoteKey.getQuoteStatusById(quoteStatusAL.get(i).toString())%>                </option>
                <%
}
%>
          </select>		</td>
        <td width="122" rowspan="2" align="center" valign="middle" style="border-left:#CCCCCC solid 1px">
		<input name="Submit5" type="button" class="normal-white" value="我的报价" onClick="myQuote()">
		</td>
        </tr> 
      <tr>
        <td height="30">
		<input type="text" name="key" id="key" style="width:250px;" value="<%=key%>">
      &nbsp;&nbsp;&nbsp;
          <select name="field">
		  <option value="0" <%=field==0?"selected":""%>>全部</option>
		   <option value="1"  <%=field==1?"selected":""%>>编号</option>
            <option value="2"  <%=field==2?"selected":""%>>客户邮件</option>
            <option value="3" <%=field==3?"selected":""%>>订单单号</option>
          </select>
&nbsp;&nbsp;&nbsp;
          <input name="Submit2" type="submit" class="long-button" value="搜索">		</td>
        </tr>
    </table>
	
	</div>
    </form>	</td>
    <td width="16%" align="center">&nbsp;</td>
    <td width="17%" align="right">
    <input name="Submit3" type="button" class="create_order_button" value=" " onClick="tb_show('创建报价单','quote_add.html?TB_iframe=true&height=500&width=850',false);">   </td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
<thead>
<tr>
  <th width="6%" align="left"  class="right-title"  style="text-align:center;" >编号</th>
<th width="13%" align="left"  class="right-title"  style="text-align:left;" >客户</th>
    <th width="5%"  class="left-title" style="text-align: center;">系统<br>
      折扣</th>
    <th width="5%"  class="left-title" style="text-align: center;">客服<br>
      折扣</th>
    <th width="5%"  class="left-title" style="text-align: center;">最终<br>
      折扣</th>
    <th width="9%"  class="left-title" style="text-align: center;">总金额</th>
    <th width="7%"  class="right-title"  style="text-align: center;" >报价人</th>
    <th width="15%" class="right-title"  style="text-align: center;" >报价备注</th>
    <th width="8%" class="right-title"  style="text-align: center;" >相关订单</th>
    <th width="6%" class="right-title"  style="text-align: center;" >状态</th>
    <th width="6%" class="right-title"  style="text-align: center;" >成单审核</th>
    <th width="15%"   class="right-title" style="text-align: center;">&nbsp;</th>
</tr>
</thead>
<%
for (int i=0; i<quotes.length; i++)
{
%>
  <tr>
    <td align="center" valign="middle" style="line-height:20px;padding-top:5px;;padding-bottom:5px;color:#999999" ><%=quotes[i].get("qoid",0l)%></td>
 <td height="60" align="left" valign="middle" style="line-height:20px;padding-top:5px;;padding-bottom:5px;color:#333333" ><span style="font-size:12px;font-family:Arial;font-weight:bold"><%=quotes[i].getString("client_id")%></span>
 <br>
 
   <span style="color:#666666"><%=quotes[i].getString("post_date")%></span>   </td>
      <td height="40" align="center" valign="middle"  >
	<%
	if (quotes[i].get("system_discount",0f)==1)
	{
		out.println("<spna style='color:#999999'>无</span>");
	}
	else
	{
		out.println("<spna style='color:#000000'>"+MoneyUtil.round(quotes[i].get("system_discount",0d)*10d,1)+"折</span>");
	}
	%>	  </td>
    <td height="40" align="center" valign="middle"  >
	<%
	if (quotes[i].get("manager_discount",0f)==1)
	{
		out.println("<spna style='color:#999999'>无</span>");
	}
	else
	{
		out.println("<spna style='color:#000000'>"+MoneyUtil.round(quotes[i].get("manager_discount",0d)*10d,1)+"折</span>");
	}
	%>	</td>

	  <td height="40" align="center" valign="middle"  >
	  <%
	if (quotes[i].get("final_discount",0f)==1)
	{   
		out.println("<spna style='color:#999999'>无</span>");
	}
	else
	{
		out.println("<spna style='color:#000000'>"+MoneyUtil.round(quotes[i].get("final_discount",0d)*10d,1)+"折</span>");
	}
	%>	
	  </td>
	  <td height="40" align="center" valign="middle"  style="color:#990000;font-weight:13px;font-weight:bold;font-size:14px;">$<%=quotes[i].get("total_cost",0d)%></td>
    <td align="center" valign="middle"  ><%=quotes[i].getString("create_account")%></td>
    <td align="left" valign="middle" style="color:#000000;padding-top:10px;padding-bottom:10px;" ><%=StringUtil.ascii2Html(quotes[i].getString("note"))%>&nbsp;
	<%
	if (quotes[i].getString("approve_note").equals("")==false)
	{
	%>
	  <div style="border:1px #CC0000 dashed;padding:3px;background:#FFFFCC;color:#CC3300"><img src="../imgs/standard_msg_error.gif" width="16" height="16"> <%=quotes[i].getString("approve_note")%></div>
	  <%
	  }
	  %>	</td>
    <td align="center" valign="middle"  >
	<%
	if ( quotes[i].get("oid",0l)>0 )
	{
	%>
	<a href="http://localhost:81/paypal20/administrator/order/ct_order_auto_reflush.html?val=<%=quotes[i].get("oid",0l)%>&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3" target="_blank" style="text-decoration:underline"><%=quotes[i].get("oid",0l)%></a>
	<%
	}
	else
	{
	%>

	<%
	if (adminLoggerBean.getAccount().equals(quotes[i].getString("create_account"))&&quotes[i].get("quote_status",0)==quoteKey.WAITING&&quoteMgr.getQuoteItemCountByQoid(quotes[i].get("qoid",0l))>0)
	{
	%>
	<input name="Submit42" type="button" class="short-short-button-ok" value="成单" onClick="finishQuote(<%=quotes[i].get("qoid",0l)%>)">
	<%
	}
	}
	%>
	
	
	<!--修改成单 -->
	<tst:authentication bindAction="com.cwc.app.api.QuoteMgr.modFinishQuote">
	<%
	if (quotes[i].get("quote_status",0)==quoteKey.FINISH)
	{
	%>
	<a href="javascript:modFinishQuote(<%=quotes[i].get("qoid",0l)%>)"><img src="../imgs/nav_text.jpg" width="16" height="16" border="0"></a>
	<%
	}
	%>
	</tst:authentication>
	
		&nbsp;</td>
    <td align="center" valign="middle"  >
	<%
	if (quotes[i].get("quote_status",0)==quoteKey.FINISH)
	{
		%>
		<img src="../imgs/standard_msg_ok.gif" width="16" height="16" align="absmiddle">
		<%
	}
	%>	
	
	 <span style="color:#333399"><%=quoteKey.getQuoteStatusById(quotes[i].getString("quote_status"))%></span></td>
    <td align="center" valign="middle"  >
		
	<%
	if (quotes[i].get("quote_status",0)==quoteKey.FINISH)
	{
	
		if ( quotes[i].get("manager_approve",0l)==0 )
		{
			out.println("审核中");
		}
		else if ( quotes[i].get("manager_approve",0l)==1 )
		{
			out.println("<font color=green>正常</font>");
		}
		else if ( quotes[i].get("manager_approve",0l)==2 )
		{
			out.println("<font color=red>无效</font>");
		}
		
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
    <td align="center" valign="middle"  >
      <input name="Submit" type="button" class="short-button" value="详细内容" onClick="location='quote_detail.html?qoid=<%=quotes[i].get("qoid",0l)%>'">
	  
	  <tst:authentication bindAction="com.cwc.app.api.QuoteMgr.approveQuote">
<%
if (quotes[i].get("quote_status",0)==quoteKey.FINISH)
{
%>	
	  &nbsp;&nbsp;
      <input name="Submit4" type="button" class="short-short-button-mod" value="审核" onClick="approveQuote(<%=quotes[i].get("qoid",0l)%>)">
<%
}
%>  
</tst:authentication>

	
	<tst:authentication bindAction="com.cwc.app.api.QuoteMgr.cancelQuote">  
<%
if (adminLoggerBean.getAccount().equals(quotes[i].getString("create_account"))&&quotes[i].get("quote_status",0)==quoteKey.WAITING)
{
%>	  
	    &nbsp;&nbsp;
      <input name="Submit4" type="button" class="short-short-button-del" value="取消" onClick="cancelQuote(<%=quotes[i].get("qoid",0l)%>)">
<%
}
%>
	</tst:authentication>    </td>
  </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
    <input type="hidden" name="p">
	
	<input type="hidden" name="st" value="<%=st%>">
	<input type="hidden" name="en" value="<%=en%>">
	<input type="hidden" name="create_account" value="<%=create_account%>">
	<input type="hidden" name="quote_status" value="<%=quote_status%>">
	<input type="hidden" name="field" value="<%=field%>">
	<input type="hidden" name="cmd" value="<%=cmd%>">
	<input type="hidden" name="key" value="<%=key%>">
	
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
</body>

</html>