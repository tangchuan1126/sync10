<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long warranty_oid = StringUtil.getLong(request,"warranty_oid");
int free_fee = StringUtil.getInt(request,"free_fee");
%>
<jsp:useBean id="currencyKey" class="com.cwc.app.key.CurrencyKey"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>

<script>
$().ready(function() {
	ajaxCartForm();
});

 
//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   //beforeSubmit:  // pre-submit callback    
			//	function(){
			//		loadCart();
			//	}	
		  // ,  
   		   beforeSubmit:  // pre-submit callback    
				function(formData, jqForm, options)
				{
						var theForm = document.add_handle_form;

						if(theForm.manual_order_type.value==1) 
						{
							if (theForm.manual_order_rel_oid.value=="")
							{
								alert("请填写相关单号");
								return(false);
							}
							
						}
						
						if(theForm.manual_order_type.value==-1) 
						{
							alert("请选择订单类型");
							return(false);
						}		
						else if(theForm.business.value==0) 
						{
							alert("请选择付款帐号");
							return(false);
						}
						else if (theForm.client_id.value=="")
						{
							alert("请填写付款人邮件");
							return(false);
						}
						else if (theForm.quantity.value=="")
						{
							alert("请填写件数");
							return(false);
						}
						else if ( !isNum(theForm.quantity.value) )
						{
							alert("请正确填写件数数字");
							return(false);
						}
						else if (theForm.mc_gross.value=="")
						{
							alert("请填写付款金额");
							return(false);
						}
						else if ( !isNum(theForm.mc_gross.value) )
						{
							alert("请正确填写付款金额数字");
							return(false);
						}
						else if (theForm.mc_currency.value=="")
						{
							alert("请选择货币类型");
							return(false);
						}					
						else if (theForm.ccid.value==0)
						{
							alert("请选择国家");
							return(false);
						}
						else if (theForm.address_name.value=="")
						{
							alert("请填写Name");
							return(false);
						}
						else if (theForm.address_street.value=="")
						{
							alert("请填写Street");
							return(false);
						}
						else if (theForm.address_city.value=="")
						{
							alert("请填写City");
							return(false);
						}
						else if (theForm.address_state.value=="")
						{
							alert("请填写State");
							return(false);
						}											
						else if (theForm.address_zip.value=="")
						{
							alert("请填写Zip");
							return(false);
						}											
						else
						{
							return(true);
						}
					
				}	
			,
		   success:       // post-submit callback  
				function(responseText, statusText)
				{
					var oid = responseText*1;
		
					if ( oid>0 )
					{
						parent.tb_show('抄单 [订单号：'+oid+']',"record_order.html?oid="+oid+"&TB_iframe=true&height=500&width=850",false);
					}
					else
					{
						alert("创建订单失败！");
					}
				}
		       
	};
	

   $('#add_handle_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}


function isNum(keyW)
{
	var reg=  /^(-[0-9]|[0-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
 
function getDetailOrder(oid)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailOrderJSON.action",
			{oid:oid},
				function callback(data)
				{
					if (data=="")
					{
						alert("单号不存在！");
						
						$("#client_id").val("");
						$("#ccid").setSelectedValue(0);
						$("#address_name").val("");
						$("#address_street").val("");
						$("#address_city").val("");
						$("#address_state").val("");
						$("#address_zip").val("");
						$("#tel").val("");
					}
					else
					{
						$("#client_id").val(data.client_id);
						$("#ccid").setSelectedValue(data.ccid);
						$("#address_name").val(data.address_name);
						$("#address_street").val(data.address_street);
						$("#address_city").val(data.address_city);
						$("#address_state").val(data.address_state);
						$("#address_zip").val(data.address_zip);
						$("#tel").val(data.tel);
					}
				}
			);
}
</script>
<style type="text/css">
<!--
.STYLE1 {color: #FF0000}

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="getDetailOrder(document.getElementById('manual_order_rel_oid').value)">
<%
String actionPath = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/markFreeWarranty.action";
if (free_fee==1)
{
	actionPath = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/markFeeShippingFeeWarranty.action";
}

%>
<br>
<form name="add_handle_form" id="add_handle_form" method="post" action="<%=actionPath%>" >
<input type="hidden" name="free_fee" value="<%=free_fee%>">
<table width="98%" border="0" cellspacing="3" cellpadding="2">

        <tr style="display:<%=warranty_oid>0?"none":""%>">
          <td align="right" class="STYLE1">订单类型</td>
          <td>&nbsp;</td>
          <td>

		   <select name="manual_order_type">
             <option value="-1">选择...</option>
            <option value="0">原始订单</option>
			<option value="1" <%=warranty_oid>0?"selected":""%>>质保订单</option>
           </select>
	
	相关单号 
		   <input name="manual_order_rel_oid" type="text" id="manual_order_rel_oid" size="15" onChange="getDetailOrder(this.value)" value="<%=warranty_oid>0?warranty_oid:""%>">
	      </td>
          <td width="30%" rowspan="4" align="left" valign="top" style="border:1px #FF0000 dashed;background:#FFFFE1;padding:5px;line-height:20px;">&nbsp;</td>
        </tr>
    <tr> 
	
    <td width="20%" align="right"><span class="STYLE1">付款帐号</span></td>
      <td width="1%">&nbsp;</td>
      <td>
	  <select name="business">
	  <option value="0">选择帐号...</option>
                <%
String businessl[] = systemConfig.getStringConfigValue("business").split(",");
for (int i=0; i<businessl.length; i++)
{
%>
                <option value="<%=businessl[i]%>" <%=businessl[i].equals("warranty@vvme.com")?"selected":""%>> 
                <%=businessl[i]%>                </option>
                <%
}
%>
      </select>	  </td>
    </tr>
  <tr> 
      <td align="right" class="STYLE1">付款人邮件</td>
      <td>&nbsp;</td>
    <td><input name="client_id" type="text" id="client_id" size="40"></td>
    </tr>
  <tr> 
      <td align="right" class="STYLE1">件数</td>
      <td>&nbsp;</td>
    <td><input name="quantity" type="text" id="quantity" size="15"></td>
    </tr>
  <tr>
    <td align="right" class="STYLE1">付款金额</td>
    <td>&nbsp;</td>
    <td width="49%"><input name="mc_gross" type="text" id="mc_gross" value="0" size="15">
	&nbsp;
<select name="mc_currency">
        <option value="">选择货币...</option>
        <%
ArrayList currency = currencyKey.getCurrency();
for (int i=0; i<currency.size(); i++)
{
%>
        <option value="<%=currencyKey.getCurrencyById( currency.get(i).toString() )%>" <%=i==0?"selected":""%>><%=currency.get(i)%></option>
<%
}
%>
      </select>	  </td>
    <td>&nbsp;</td>
  </tr>



    <tr> 
    <td align="right" class="STYLE1">Country</td>
    <td>&nbsp;</td>
    <td>
	<%
	DBRow countrycode[] = orderMgr.getAllCountryCode();
	String selectBg="#ffffff";
	String preLetter="";
	%>
      <select name="ccid" id="ccid" >
	  <option value="0">选择国家...</option>
	  <%
	  for (int i=0; i<countrycode.length; i++)
	  {
	  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
		{
			if (selectBg.equals("#eeeeee"))
			{
				selectBg = "#ffffff";
			}
			else
			{
				selectBg = "#eeeeee";
			}
		}  	
		
		preLetter = countrycode[i].getString("c_country").substring(0,1);
	  %>
	    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
	  <%
	  }
	  %>
      </select>    </td>
    <td>&nbsp;</td>
    </tr>
	
     <tr>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
	
	
   <tr>
    <td align="right" class="STYLE1">Name<br></td>
    <td>&nbsp;</td>
    <td><input name="address_name" type="text" id="address_name" size="40"></td>
    <td>&nbsp;</td>
   </tr>
   <tr>
    <td align="right" class="STYLE1">Street<br></td>
    <td>&nbsp;</td>
    <td><input name="address_street" type="text" id="address_street" size="40"></td>
    <td>&nbsp;</td>
   </tr>
    <tr>
    <td align="right" class="STYLE1">City<br></td>
    <td>&nbsp;</td>
    <td><input name="address_city" type="text" id="address_city" size="40"></td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td align="right" class="STYLE1">State<br></td>
    <td>&nbsp;</td>
    <td><input name="address_state" type="text" id="address_state" size="40"></td>
    <td>&nbsp;</td>
    </tr>
  <tr> 
    <td align="right" class="STYLE1">Zip<br></td>
    <td>&nbsp;</td>
    <td><input name="address_zip" type="text" id="address_zip" size="40"></td>
    <td>&nbsp;</td>
  </tr>

    <tr>
      <td align="right">Tel</td>
      <td>&nbsp;</td>
      <td colspan="2"><input name="tel" type="text" id="tel" size="40"></td>
    </tr>
    <tr> 
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2">
      
      <input type="submit" name="Submit" value="" class="create_order_button">    </td>
  </tr>
</table>
</form>
</body>
</html>
