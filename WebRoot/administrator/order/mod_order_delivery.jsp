<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long oid = StringUtil.getLong(request,"oid");

DBRow orderDeliveryInfo = orderMgr.getDetailOrderDeliveryInfoByOid(oid);

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改订单配送信息</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<script>
	
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE4 {font-size: medium}
-->
</style>

<script type="text/javascript">
	function duplicateDelivery(oid)
	{
			$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailOrderDeliveryInfoByOidJSON.action",
				{oid:oid},
					function callback(data)
					{
						if (data=="")
						{
							alert("订单不存在！");
						}
						else
						{
							$("#address_nameWinText").val(data.address_name);
							$("#address_streetWinText").setSelectedValue(data.address_street);
							$("#address_cityWinText").val(data.address_city);
							$("#address_stateWinText").val(data.address_state);
							$("#address_zipWinText").val(data.address_zip);
							$("#telWinText").val(data.tel);
							getStorageProvinceByCcid(data.ccid,data.pro_id);
						}
					}
				);
	}
	
	function getStorageProvinceByCcid(ccid,pro_id)
	{
	
			$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
					{ccid:ccid},
					function callback(data)
					{ 
						$("#pro_id").clearAll();
						$("#pro_id").addOption("请选择......","0");
						
						if (data!="")
						{
							$.each(data,function(i){
								$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
							});
						}
						
						if (ccid>0)
						{
							$("#pro_id").addOption("手工输入","-1");
						}
	
						
						if (pro_id!=""&&pro_id!=0)
						{
							$("#pro_id").setSelectedValue(pro_id);
							stateDiv(pro_id);
						}			
					}
			);
	}
	
	function modOrderDeliveryInfoSub()
	{
		if($("#address_nameWinText").val() == "")
		{
			alert("请填写Name");
		}
		else if($("#address_streetWinText").val() == "")
		{
			alert("请填写Street");
		}
		else if($("#address_cityWinText").val() == "")
		{
		   	alert("请填写City");
							   
		}
		else if($("#pro_id").val()== ""||$("#pro_id").val()==0)
		{
		  	alert("请选择State");
		}
		else if($("#pro_id").val()==-1&&$("#address_state").val()=="")
		{
			alert("请手工输入State");
			$("#address_state").focus();
		}
		else if($("#address_zipWinText").val() == "")
		{
			alert("请填写Zip");
		}
		else
		{
			parent.document.listForm.oid.value = <%=oid%>;
			parent.document.listForm.tel.value = $("#telWinText").val();
			
			parent.document.listForm.address_name.value = $("#address_nameWinText").val();
			parent.document.listForm.address_street.value = $("#address_streetWinText").val();
			parent.document.listForm.address_city.value = $("#address_cityWinText").val();
			parent.document.listForm.address_zip.value = $("#address_zipWinText").val();
			
			parent.document.listForm.pro_id.value = $("#pro_id").val();
			parent.document.listForm.address_state.value = $("#address_state").val();
						
			parent.document.listForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/modPOrderDeliveryInfo.action";
			parent.document.listForm.submit();	
		}
	}
	
	function stateDiv(pro_id)
	{
		if(pro_id==-1)
		{
			$("#state_div").css("display","");
		}
		else
		{
			$("#state_div").css("display","none");
		}
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<form action="" name="mod_order_deliveryInfo_form">
	</form>
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top" align="center">
                       <table width="95%" align="center" cellpadding="0" cellspacing="0" style="padding-left:10px;">						      
						  <tr>
							  <td  align="right" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	<span style='font-weight:normal'>从订单 <input name='duplicate_oid' type='text' id='duplicate_oid'  style='width:60px;color:red' onChange="duplicateDelivery(this.value)"> 复制地址信息</span>
							  </td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	Name
							  </td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	<input name='address_nameWinText' type='text' id='address_nameWinText' style='width:300px;' value="<%=orderDeliveryInfo.getString("address_name")%>"/>
							  </td>
						  </tr>
						   <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	Street
							  </td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	<input name='address_streetWinText' type='text' id='address_streetWinText'  style='width:300px;' value="<%=orderDeliveryInfo.getString("address_street")%>"/>
							  </td>
						  </tr>
						   <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	City
							  </td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	<input name='address_cityWinText' type='text' id='address_cityWinText'  style='width:300px;' value="<%=orderDeliveryInfo.getString("address_city")%>"/>
							  </td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	State
							  </td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	<select name='pro_id' id='pro_id' onchange="stateDiv(this.value)"></select>
								<div style="padding-top: 10px;" id="state_div">
									<input type="text" name="address_state" id="address_state" value="<%=orderDeliveryInfo.getString("address_state")%>"/>
								</div>
								
							  	<script type="text/javascript">
							  		getStorageProvinceByCcid(<%=orderDeliveryInfo.get("ccid",0)%>,<%=orderDeliveryInfo.get("pro_id",0)%>);
							  	</script>
							  </td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	Zip
							  </td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	<input name='address_zipWinText' type='text' id='address_zipWinText'  style='width:300px;' value="<%=orderDeliveryInfo.getString("address_zip")%>"/>
							  </td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	Tel
							  </td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2">
							  	<input name='telWinText' type='text' id='telWinText'  style='width:300px;' value="<%=orderDeliveryInfo.getString("tel")%>"/>
							  </td>
						  </tr>
						  <tr>
						  	<td>&nbsp;</td>
						  </tr>
					  </table>
				    </td>
				</tr>
				
				<tr>
					<td align="right" valign="bottom">				
						<table width="100%">
							<tr>
								<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
								  <input type="button" name="Submit2" value="保存" class="normal-green" onClick="modOrderDeliveryInfoSub();">
								  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.tb_remove()">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>	
</body>
</html>
