<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long stt = System.currentTimeMillis();
long oid = StringUtil.getLong(request,"oid");
boolean isAjaxDataBase = StringUtil.getBooleanType(StringUtil.getInt(request,"isAjaxDataBase"));
if(isAjaxDataBase)
{
	orderMgr.initCartFromOrder(request,oid);
} 

cart.flush(session);
DBRow cartProducts[] = cart.getDetailProducts();

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script>

</script>

<style>
a.del-cart-product:link {
	color:#FF0000;
	text-decoration: none;
}
a.del-cart-product:visited {
	color: #FF0000;
	text-decoration: none;
}
a.del-cart-product:hover {
	color: #FF0000;
	text-decoration: underline;

}
a.del-cart-product:active {
	color: #FF0000;
	text-decoration: none;

}
</style> 

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
  
<%
if (cartProducts.length==0)
{
%>
      <tr>
        <td colspan="4" align="center" valign="middle" style="color:#999999"><<暂无商品>>
		<script>cartIsEmpty=true;</script>		</td>
      </tr>
<%
} 
else
{
%>
      <tr >
        <td height="30" bgcolor="#eeeeee" style="padding-left:10px;"><strong>商品</strong></td>
        <td align="center" bgcolor="#eeeeee">待发数量</td>
        <td bgcolor="#eeeeee"><strong>数量</strong></td>
        <td align="center" valign="middle" bgcolor="#eeeeee">&nbsp;</td>
      </tr>
	  
<%
String bgColor;
for (int i=0; i<cartProducts.length; i++)
{
	if (i%2==0)
	{
		bgColor = "#FFFFFF";
	}
	else
	{
		bgColor = "#eeeeee";
	}
	
%>

      <tr bgcolor="<%=bgColor%>" >
        <td width="53%" height="25"  style="padding-left:10px;padding-top:5px;padding-bottom:5px;">
		
		<%=cartProducts[i].get("cart_product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL?"<":""%>
		<%=cartProducts[i].getString("p_name")%>
		<%=cartProducts[i].get("cart_product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL?">":""%>
		
		<%
		
		if (cartProducts[i].get("cart_product_type",0)==ProductTypeKey.UNION_STANDARD&&cartProducts[i].get("wait_quantity",0f)==cartProducts[i].get("cart_quantity",0f))
		{
			out.println("[<a href='javascript:void(0)' onclick='customProduct("+cartProducts[i].getString("cart_pid")+",\""+cartProducts[i].getString("p_name")+"\",\"add\")'>定制</a>]");
		}

		if (cartProducts[i].get("cart_product_type",0)==ProductTypeKey.UNION_CUSTOM&&cartProducts[i].get("wait_quantity",0f)==cartProducts[i].get("cart_quantity",0f))
		{
			out.println("[<a href='javascript:void(0)' onclick='customProduct("+cartProducts[i].getString("cart_pid")+",\""+cartProducts[i].getString("p_name")+"\",\"mod\")'>修改</a>]");
		}
		%>

		<%
		if (cartProducts[i].get("cart_product_type",0)==ProductTypeKey.UNION_CUSTOM)
		{
			out.println("<div style='color:#999999;padding-left:10px;'>");
			DBRow customProductsInSet[] = customCart.getFinalDetailProducts(session, StringUtil.getLong(cartProducts[i].getString("cart_pid")) );
			for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
			{
				out.println("├ "+customProductsInSet[customProductsInSet_i].getString("p_name")+" x "+customProductsInSet[customProductsInSet_i].getString("cart_quantity")+customProductsInSet[customProductsInSet_i].getString("unit_name")+"<br>");
			}
			out.println("</div>");
		}
		%>

		<input type="hidden" name="pids" id="pids" value="<%=cartProducts[i].getString("cart_pid")%>">
		<input type="hidden" name="product_type" id="product_type" value="<%=cartProducts[i].getString("cart_product_type")%>">		</td>
        <td width="13%" align="center"><%=cartProducts[i].get("wait_quantity",0f)%></td>
        <td width="12%">
		<%
		if ( cartProducts[i].get("cart_product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL )
		{
		%>
		<%=cartProducts[i].getString("cart_quantity")%>
		<input type="hidden" name="quantitys" id="quantitys" value="<%=cartProducts[i].getString("cart_quantity")%>">
		<%
		}
		else
		{
		%>
		<input name="quantitys" type="text" id="quantitys" style="width:40px;" value="<%=cartProducts[i].getString("cart_quantity")%>" onChange="return onChangeQuantity(this,<%=cartProducts[i].get("cart_quantity",0f)-cartProducts[i].get("wait_quantity",0f)%>)"  /> 
		<%
		}
		%>
		
        <%=cartProducts[i].getString("unit_name")%> </td>
        <td width="11%" align="center" valign="middle">
		<% 
			if(cartProducts[i].get("wait_quantity",0f)==cartProducts[i].get("cart_quantity",0f))//发过货的不允许删除
			{
		%>
		<a href="javascript:void(0)" onClick="delProductFromCart(<%=cartProducts[i].getString("cart_pid")%>,'<%=cartProducts[i].getString("p_name")%>',<%=cartProducts[i].getString("cart_product_type")%>)" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>		</td>
		<% 
			}
		%>
      </tr>
<%
} 
%>
      <tr>
        <td height="50" style="border-top:2px #eeeeee solid"><input name="Submit433" type="submit" class="short-button" value="修改数量">          &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
        <!-- 
        <input name="Submit432" type="button" class="long-button-yellow" value="清空购物车" onClick="cleanCart()">
        -->
        <script>cartIsEmpty=false;</script>
        </td>
        <td height="50" style="border-top:2px #eeeeee solid">&nbsp;</td>
        <td colspan="2" align="right" valign="middle" style="border-top:2px #eeeeee solid">总数：<%=cart.getSumQuantity()%></td>
      </tr>
<%
}

//long ent = System.currentTimeMillis();
//System.out.println("Cart Page time:"+(ent-stt));
%>
</table>

</body>
</html>
