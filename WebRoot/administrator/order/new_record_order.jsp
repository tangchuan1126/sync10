<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.api.OrderLock"%>
<%@ page import="com.cwc.app.api.qll.VertualTerminalMgrQLL"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%

long oid = StringUtil.getLong(request,"oid");
//获得订单锁
DBRow orderLock = OrderLock.lockOrder(session,oid,OrderLock.RECORD);
if (orderLock!=null)
{
	out.println("<script>");
	out.println("alert('"+orderLock.getString("operator")+"正在"+OrderLock.getMsg(orderLock.getString("operate_type"))+"，请与其联系后再操作')");
	out.println("</script>");
}

int refresh = StringUtil.getInt(request,"refresh");

DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);

String tranID = detailOrder.getString("txn_id");

String client_id = detailOrder.getString("client_id");
String name = detailOrder.getString("address_name");

DBRow trade = tradeMgrZJ.getCreateOrderTradeByOid(oid);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<style>
.ebayTitle
{
	color: #0066FF;
    display: block;
    float: left;
    font-size: 12px;
    font-weight: bold;
    width: 100px;
    text-align:right;
}

.tradeTitle
{
	color: #0066FF;
    display: block;
    float: left;
    font-size: 12px;
    font-weight: bold;
    text-align:center;
}
 
</style>


<script>
 
function showTran()
{
	if ( document.getElementById("tranForm").style.display == "" )
	{
		document.getElementById("showTran").innerHTML = "显示交易信息";
		document.getElementById("tranForm").style.display="none";
	}
	else
	{
		document.getElementById("showTran").innerHTML = "隐藏";
		document.getElementById("tranForm").style.display="";
	}
	
}
 
function customProduct(pid,p_name,cmd)
{
	$.artDialog.open("../product/custom_product.html?cmd="+cmd+"&pid="+pid+"&p_name="+p_name, {title: "定制套装",width:'400px',height:'300px',fixed:true, lock: true,opacity: 0.3});
	//tb_show('定制套装',"../product/custom_product.html?cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&TB_iframe=true&height=300&width=400",false);
}

$().ready(function() {

	function log(event, data, formatted) {

		
	}
	
	/**
	function formatItem(row) {
		return(row[0]);
	}
	
	function formatResult(row) {
		return row[0].replace(/(<.+?>)/gi, '');
	}
	**/
	addAutoComplete($("#p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
			"merge_info",
			"p_name");
	
	//加载购物车
	loadCart(1);
	//绑定购物车表单AJAX
	ajaxCartForm();

	
	$("#p_name").keydown(function(event){
		if (event.keyCode==13)
		{
			$("#quantity").select();
		}
	});

	$("#quantity").keydown(function(event){
		if (event.keyCode==13)
		{
			put2Cart();
		}
	});
	
	getStorageProvinceByCcid(<%=detailOrder.get("ccid",0l)%>);//初始化地区	
});

//=================== 购物车 =========================

function loadCart(isAjaxDataBase)
{
	//alert(para);
	
	$.ajax({
		url: 'mini_cart.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:{oid:<%=oid%>,isAjaxDataBase:isAjaxDataBase},
		
		beforeSend:function(request){
			$("#action_info").text("加载购物车......");
		},
		
		error: function(){
			$("#action_info").text("加载购物车失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
			$("#action_info").text("");
			$("#p_name").focus();
		}
	});
	
//	preCalcuOrder(<%=oid%>,$("#ccid_hidden").val(),$("#pro_id").val());
}


function put2Cart()
{
	var p_name = $("#p_name").val();
	var quantity = $("#quantity").val();

	
	 if (p_name=="")
	{
		alert("请填写商品名称");
		$("#p_name").focus();
	}
	else if (quantity=="")
	{
		alert("请填写数量");
	}
	else if (isQuantity(quantity))
	{
		alert("请正确填写数量");
		$("#quantity").select();
	}
	else if (quantity<=0)
	{
		alert("数量必须大于0");
		$("#quantity").select();
	}
	else
	{
		var para = "p_name="+p_name+"&quantity="+quantity;
	
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/put2Cart.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			
			error: function(){
				loadCart(1);
				$("#action_info").text("商品添加失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException")
				{
					alert("商品不存在，不能添加到订单！");
					$("#p_name").select();
				}
				else if(msg=="ProductDataErrorException")
				{
					alert("商品基础数据错误，不能添加到订单！请联系调度");
					$("#p_name").select();
				}
				else if (msg=="ok")
				{
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("1");
					loadCart(0);
				}
				else
				{
					loadCart(1);
					alert(msg);
				}
			}
		});	
	}
}

function delProductFromCart(pid,p_name,product_type)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/removeProductFromCart.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"pid="+pid+"&product_type="+product_type,
					
			error: function(){
				loadCart(1);
				$("#action_info").text("商品删除失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart(0);
			}
		});	
	}
}


function cleanCart()
{
	if (confirm("确定清空购物车？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/cleanCart.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			
			error: function(){
				loadCart(1);
				$("#action_info").text("清空购物车失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart(0);
			}
		});	
	}
}
function parentTrueOrder(oid,tel,ccid,note,parentid,pro_id,address_name,address_city,address_street,address_zip,address_state)
{
	var para = "oid="+oid+"&tel="+tel+"&record_order_note="+note+"&handle_status=<%=HandStatusleKey.NORMAL%>&ccid="+ccid+"&pro_id="+pro_id+"&address_name="+address_name+"&address_city="+address_city+"&address_street="+address_street+"&address_zip="+address_zip+"&address_state="+address_state;
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/newRecordOrder.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			async:false,
			data:para,
			
			beforeSend:function(request){
			},
			
			error: function(){
			},
			
			success: function(msg){
				
			}
		});
}


//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   beforeSubmit:  // pre-submit callback    
				function(){
					return checkQuantity();
				},  
		   async:false,
		   success:       // post-submit callback  
				function(data)
				{
					loadCart(0);
					cartProQuanHasBeChange = false;
				}
		       
	};
	

   $('#cart_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}

var cartProQuanHasBeChange = false;
function onChangeQuantity(obj,send_quantity)
{
	if (isQuantity(obj.value) )
	{
		alert("请正确填写数量");
		return(false);
	}
	else if(parseFloat(obj.value)<send_quantity)
	{
		alert("此商品有运单！请先取消对应的运单");
		obj.value = parseFloat(send_quantity);
		return (false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}


function isNum(keyW)
{
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
}

function isQuantity(key)
{
	var quantity = parseInt(key);
	
	if(quantity!=key||quantity<1)
	{
		return true;
	}
	else
	{
		return false;
	}
} 

function checkQuantity()
{
	var result = "true";
	$("[name='quantitys']").each(
			function()
			{				
				if(isQuantity(this.value))
				{
					result = "false";
				}		 
			}
	);
	if(result =="true")
	{
		return true;
	}
	else
	{
		return false;
	}
}

//=================== 购物车 =========================


var cartIsEmpty = <%=cart.isEmpty(session)%>;

function checkCart()
{
	if (cartIsEmpty)
	{	
		alert("请先添加商品到订单");
		return(false);
	}
	else
	{
		return(true);
	}
}

//除了正常抄单，购物车不能为空外，其他状态抄单都可以为空
function trueOrder()
{

	if (checkCart())
	{
		if (cartProQuanHasBeChange)
		{
			alert("请先保存购物车修改商品的数量");
		}
		else if($("#address_name").val()=="")
		{
			alert("请先填写Name");
			next(1);
		}
		else if($("#address_city").val()=="")
		{
			alert("请先填写City");
			next(1);
		}
		else if($("#address_street").val()=="")
		{
			alert("请先填写Street");
			next(1);
		}
		else if($("#address_zip").val()=="")
		{
			alert("请先填写Zip");
			next(1);
		}
		else if (checkSelectCountry())
		{
			alert("请选择递送国家");
			next(1);
		}
		else if ($("#pro_id").val()==0)
		{
			alert("请选择地区");
			next(1);
		}
		else if ( $("#delivery_note").val()!=""&&/.*[\u4e00-\u9fa5]+.*$/.test($("#delivery_note").val()) )
		{
			alert("配送备注不能包含中文");
		}	
		else
		{			
			var note = ""
					if (note!="")
					{
						note = "："+note;
					}
					
					parentTrueOrder(<%=oid%>,$("#tel").val(),$('#ccid_hidden').val(),note,<%=detailOrder.get("parent_oid",0l)%>,$("#pro_id").val(),$("#address_name").val(),$("#address_city").val(),$("#address_street").val(),$("#address_zip").val(),$("#address_state").val());
		}
	}
}



function doubtTypeOrder(type)
{
		if (cartProQuanHasBeChange)
		{
			alert("请先保存购物车修改商品的数量");
		}
		else if (checkSelectCountry())
		{
			alert("请选择递送国家");
		}
		else if ($("#pro_id").val()==0)
		{
			alert("请选择地区");
		}
		else
		{

				$.prompt(
				
				"<div id='title'>疑问订单</div><br /><span style='font-size:13px;font-weight:bold;'><input type='hidden' name='handleStatusWinText' id='handleStatusWinText' value='"+type+"'/><textarea name='OrderNoteWinText' id='OrderNoteWinText'  style='width:320px;height:60px;'></textarea>",
				
				{
					  submit: 
							function (v,m,f)
							{
								if (v=="y")
								{
									if ( typeof(f.handleStatusWinText)=="undefined")
									{
										alert("请选择疑问类型");
									    return false;
									}
									else if(f.OrderNoteWinText  == "")
									{
									   alert("请填写问题备注");
									    return false;
									}
									return true;
								}
			
							}
					  ,
			
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
									var outOrderNoteWinText = f.OrderNoteWinText;
																
									parent.parentDoubtTypeOrder(<%=oid%>,$("#tel").val(),$('#ccid_hidden').val(),outOrderNoteWinText,f.handleStatusWinText,$("#pro_id").val(),$("#address_name").val(),$("#address_street").val(),$("#address_city").val(),$("#address_zip").val());
								}
							}
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
				
			
		}
		

}


function checkInvoice(inv_uv,inv_tv,inv_dog,inv_rfe)
{
	if (inv_uv==""&&inv_tv==""&&inv_dog==""&&inv_rfe=="")
	{
		if ($("#invoice_product_type").val()==0)
		{
			alert("请选择一个发票模板");
			return(false);
		}
	}
	else
	{
		if(inv_uv==""&&inv_tv=="")
		{
			alert("Unit Value和Total Value不能同时为空！");
			return(false);
		}
		else if ( inv_uv!=""&&!isNum(inv_uv) )
		{
			alert("请正确填写Unit Value！");
			return(false);
		}
		else if(inv_dog=="")
		{
			alert("Description of Goods不能为空！");
			return(false);
		}
		else if(inv_rfe=="")
		{
			alert("Reason for Export不能为空！");
			return(false);
		}
	}
	
	return true;
}


function checkSelectCountry()
{
	if ( $('#ccid_hidden').val()==0 ) 
	{
		return(true);
	}
	else
	{
		return(false);		
	}
}

function closeTBWin()
{
	tb_remove();
}

function changeCountry(obj)
{
	
}

function changeInvoice(obj)
{
	$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailInvoiceTemplateJSON.action",
		{invoice_id:obj.value},
		function callback(data)
		{   
			$("#inv_dog").val(data.dog);
			$("#inv_rfe").val(data.rfe);
			$("#inv_uv").val(data.uv);
			$("#inv_tv").val(data.tv);
			
			$("#inv_di_id").val(data.di_id);
			
		}
	);
}

function pasteEmail()
{
	if ( document.getElementById("emailContainer").style.display == "" )
	{
		document.getElementById("paste_email").innerHTML = "粘贴邮件内容";
		document.getElementById("emailContainer").style.display = "none";
	}
	else
	{
		document.getElementById("paste_email").innerHTML = "隐藏";
		document.getElementById("emailContainer").style.display = "";
	}
}

function cleanInvoice()
{
			$("#inv_dog").val("");
			$("#inv_rfe").val("");
			$("#inv_uv").val("");
			$("#inv_tv").val("");
}

function validateZipCode(ps_id,zipCode)
{
	//只对美国仓库检查邮编
	if (ps_id==100043)
	{

		var para = "zipCode="+zipCode;

		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/validateUSAZipCodeAjax.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			
			error: function(){
				alert("检测邮编出错");
			},
			
			success: function(msg){
				if (msg=="false")
				{
					modOrderZip(zipCode);
				}
			}
		});	
		
	}
}

function modOrderZip(zipCode)
{
	$.prompt(
	
	"<div id='title'>修改递送信息[订单:"+<%=oid%>+"]</div><br/><img src='../imgs/product/warring.gif' width='16' height='15' align='absmiddle'><span style='color:#black;font-size:12px;font-style:normal;font-weight:normal'>&nbsp;邮编["+zipCode+"]不正确，请先修正！<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;正确格式：五位纯数字(例如：60459-1234修正为60459)</span><br/><br/>Zip<br><input name='address_zipWinText' type='text' id='address_zipWinText'  style='width:300px;'>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{

						  if(f.address_zipWinText == "")
						  {
							   alert("请填写邮编");
							   return false;
						  }

						  var   reg =/^\d{5}$/;
						  if(!reg.test(f.address_zipWinText))
						  {
						  		alert("邮编格式不正确")
						  		return false;
						  }
						  return true;
					}
				}
		  ,
   		  loaded:
				function ()
				{ 
					$("#address_zipWinText").val(zipCode);

				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.modZipForm.address_zip.value = f.address_zipWinText;
						document.modZipForm.refresh.value = "1";
						document.modZipForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/modOrderZip.action";
						document.modZipForm.submit();	
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function isRefresh()
{
	<%
		if(refresh==0)
		{
	%>
		parent.closeTBWin();
	<%		
		}
		else
		{
	%>
		parent.closeTBWinRefrech && parent.closeTBWinRefrech();
		$.artDialog.opener && $.artDialog.opener.closeWin && $.artDialog.opener.closeWin();
		$.artDialog.close && $.artDialog.close();
	<%
		}
	%>
}

//强制填写好地址，才允许抄单
function validateDeliveryInfo()
{
	var closeWin = false;
	
	if ( "<%=detailOrder.getString("address_name")%>" == "" )
	{
		closeWin = true;
	}
	else if ( "<%=StringUtil.replaceString(StringUtil.replaceString(StringUtil.replaceString(detailOrder.getString("address_street"), "\n", ""), "\r", ""),"\"","")%>" == "" )
	{
		closeWin = true;
	}
	else if ( "<%=detailOrder.getString("address_city")%>" == "" )
	{
		closeWin = true;
	}
	else if ( "<%=detailOrder.getString("address_state").trim()%>" == "" )
	{
		closeWin = true;
	}
	else if ( "<%=detailOrder.getString("address_zip")%>" == "" )
	{
		closeWin = true;
	}
	
	if (closeWin)
	{
		alert("收货地址信息不完整，请注意！");
		//parent.closeTBWin();
	}
}
validateDeliveryInfo();


//非正常状态订单抄单需要提示
function abnormalPaymentStatus()
{
	if ( confirm("该订单 <%=detailOrder.getString("payment_status")%> 状态，是否确定抄单？") )
	{
	}
	else
	{
		parent.closeTBWin();
	}
}

<%
if ( detailOrder.getString("payment_status").toLowerCase().equals("completed")==false&&detailOrder.getString("payment_status").equals("")==false )
{
%>
abnormalPaymentStatus();
<%
}
%>

function hasBeCreditVerify()
{

		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/hasBeCreditVerify.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"oid=<%=oid%>",
			
			error: function(){
				
			},
			
			success: function(msg){
			
				if (msg=="false")
				{
					alert("请先验证信用卡支付后再抄单！");
					parent.closeTBWin();
				}
			}
		});	
}


function preCalcuOrder(oid,select_ccid,select_pro_id)
{
	var para = "oid="+oid+"&select_ccid="+select_ccid+"&select_pro_id="+select_pro_id;

	$.ajax({
		url: 'pre_calcu_order.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request){
			$("#pre_calcu_order_info").text("正在计算......");
		},
		
		error: function(){
			$("#pre_calcu_order_info").text("计算失败！");
		},
		
		success: function(html){
			$("#pre_calcu_order_page").html(html);
			$("#pre_calcu_order_info").text("");
		}
	});
}

//国家地区切换
function getStorageProvinceByCcid(ccid)
{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#pro_id").addOption("手工输入","-1");
					}
				});
		
<%
long jsProID = 0;

//如果订单已经设置了省份，则返回
if (detailOrder.get("pro_id",0l)>0)
{
	jsProID = detailOrder.get("pro_id",0l);
}
//如果是初次抄单，则系统根据省份代码自动配对
DBRow detailProvince = productMgr.getDetailProvinceByNationIDPCode(detailOrder.get("ccid",0l),detailOrder.getString("address_state"));
if (detailProvince!=null)
{
	jsProID = detailProvince.get("pro_id",0l);
}

%>


					var pro_id = "<%=jsProID%>";
					if (pro_id!=""&&pro_id!=0)
					{
						$("#pro_id").setSelectedValue(pro_id);
						stateDiv(pro_id);
					}
}

//验证网站订单信息是否已经同步
<%
 if(detailOrder.getString("order_source").equals("WEBSITE"))
 {
	 DBRow websitetranInfo = tranInfoMgrQLL.getTranInfo(detailOrder.getString("txn_id"));
	 if(websitetranInfo==null&&DateUtil.getDate2LongTime(DateUtil.NowStr())>(DateUtil.getDate2LongTime(detailOrder.getString("post_date"))+(5*60*1000)))
	 {
%>
	 alert("网站订单详细信息没有同步完成，请稍候抄单");
 	//parent.closeTBWin();
<%
	 }
 }
%>


<%
//暂时屏蔽，不使用
//String buyer_ip = tranInfoMgrQLL.getBuyerIp(detailOrder.getString("txn_id"),detailOrder.getString("order_source"),detailOrder.getString("note"));

//if ( buyer_ip.equals("")==false )
//{
%>
	//hasBeCreditVerify();	
<%
//}
%>

var lastTabs;//上一个tabs的Index


function stateDiv(pro_id)
{
	if(pro_id==-1)
	{
		$("#state_div").css("display","inline");
	}
	else
	{
		$("#state_div").css("display","none");
	}
}

function loadWaybill()
{
	var para = "oid=<%=oid%>";
	$.ajax({
		url: 'create_waybill.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		async:false,
		
		
		beforeSend:function(request){
		},
		
		error: function(respon)
		{
			alert(respon);
		},
		
		success: function(html){
			
			$("#create_waybill").html(html);
		}
	});
}

	function duplicateDelivery(oid)
	{
			$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getDetailOrderDeliveryInfoByOidJSON.action",
				{oid:oid},
					function callback(data)
					{
						if (data=="")
						{
							alert("订单不存在！");
						}
						else
						{
							$("#address_name").val(data.address_name);
							$("#address_street").setSelectedValue(data.address_street);
							$("#address_city").val(data.address_city);
							$("#address_stateWinText").val(data.address_state);
							$("#address_zip").val(data.address_zip);
							$("#tel").val(data.tel);
							getAjaxStorageProvinceByCcid(data.ccid,data.pro_id);
						}
					}
				);
	}
		function getAjaxStorageProvinceByCcid(ccid,pro_id)
		{
			$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
					{ccid:ccid},
					function callback(data)
					{ 
						$("#pro_id").clearAll();
						$("#pro_id").addOption("请选择......","0");
						
						if (data!="")
						{
							$.each(data,function(i){
								$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
							});
						}
						
						if (ccid>0)
						{
							$("#ccid_hidden").setSelectedValue(ccid);
							$("#pro_id").addOption("手工输入","-1");
						}
	
						
						if (pro_id!=""&&pro_id!=0)
						{
							$("#pro_id").setSelectedValue(pro_id);
							stateDiv(pro_id);
						}			
					}
			);
	}
</script>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
 <div class="demo" align="center">
  	 <div id="tabs" style="width: 98%">
  	    <ul>
  	      <li><a href="#orderSource" >订单来源信息</a></li>
  	      <li><a href="#deliveryDetails" >收货信息</a></li>
		  <li><a href="#orderItem" >订单商品</a></li>
		  <li><a href="#storageExpress">商品出库</a></li>
		</ul>
		<div id="orderSource">
			<%
			if (detailOrder.getString("order_source").equals(com.cwc.app.api.OrderMgr.ORDER_SOURCE_EBAY))
			{
			%>
			<img src="../imgs/ebay_logo.gif" width="48" height="19" align="absmiddle">
			<span style="color:#99CC00;font-size:15px;font-weight:bold">
			<strong style="color:blue">|</strong>
			<%=detailOrder.getString("seller_id")%>
			</span>
			<%
			}
			else
			{
			%>
			<strong><%=detailOrder.getString("order_source")%></strong>
			<%
			}
			%>
			<strong style="color:blue">
			| 
			</strong>
			<strong>
			<%=payTypeKey.getStatusById(detailOrder.get("pay_type",0))%>
			</strong>
		 
			
			<strong style="color:blue">|</strong>
			<%=detailOrder.getString("business")%>
			<strong style="color:blue">|</strong>
			<font color="<%=detailOrder.getString("invalid_color")%>"> 
	        	<%=detailOrder.getString("txn_id")%>
		    </font>
			<br/>
			Buyer_ebay_id:<span style="color:blue;font-size:15px;font-weight:bold"><%=detailOrder.getString("auction_buyer_id")%></span>
			<strong style="color:blue">|</strong>
			Client_id:<%=detailOrder.getString("client_id")%>
			<div>
				<br/>
						<table width="100%" border="0"  cellspacing="1" align="center" cellpadding="5"   bgcolor="#cccccc" >
													  <tr>
													  	<td width="10%"  height="20" align="center"  ><span class="tradeTitle">Item #</span></td>
														 <td width="30%"  height="20" align="center"  ><span class="tradeTitle">Item_Name</span></td>
														 <td align="center" width="5%"><span class="tradeTitle">QTY</span></td>
														 <td width="5%" align="center"><span class="tradeTitle">Price</span></td>
														 <td width="10%" align="center"><span class="tradeTitle">Sku</span></td>
														 <td width="10%" align="center"><span class="tradeTitle">Mpn</span></td>
														 <td width="5%" align="center"><span class="tradeTitle">FVF</span></td>
														 <td width="5%" align="center"><span class="tradeTitle">Cur</span></td>
														 <td width="5%" align="center"><span class="tradeTitle">Ship</span></td>
													 </tr>
												 <%
													long trade_id = null!=trade?trade.get("trade_id", 0L):0L;
												 	DBRow[] tradeItems = tradeMgrZJ.getTradeItemsByTradeId(trade_id);
												 	
												 	double total_price = 0;
													for(int i=0;i<tradeItems.length;i++)
													{	 			 		
												%>
														<tr>
															<td align="center" height="25" bgcolor="#eeeeee"><%=tradeItems[i].getString("item_number")%></td>
															<td align="center" height="25" bgcolor="#eeeeee"><%=tradeItems[i].getString("item_name")%></td>
															<td align="center" nowrap="nowrap" bgcolor="#eeeeee"><%=tradeItems[i].get("quantity",0f)%></td>
															<td align="center" nowrap="nowrap" bgcolor="#eeeeee"><%=tradeItems[i].get("mc_gross",0d)%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].getString("sku")%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].getString("mpn")%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].get("final_value_fee",0d)%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].getString("currency")%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].get("mc_shipping",0d)%></td>
														</tr>
												<% 
														total_price += tradeItems[i].get("mc_gross",0d);
													} 			
										   		%>	
							</table>
					<br/>	 
						<table width="100%"     border="0"    align="center" cellpadding="5" cellspacing="0" bgcolor="#cccccc" > 
						   
									<%
											DBRow rows = trade;
											if(rows!=null)
											{
									%>
									<tr>
									<td width="50%" colspan="2" height="25"  align="left" bgcolor="#eeeeee"  > <span class="tradeTitle">tran id: </span>&nbsp; <%=rows.getString("txn_id")  %></td>
										 <td colspan="2" align="left" height="25" bgcolor="#eeeeee"    ><span class="tradeTitle">currency:</span>&nbsp;<%=rows.getString("mc_currency")%> </td>   
									</tr>
									<tr>
										<td colspan="2"   align="left"  height="25" bgcolor="#FFFFFF"    ><span class="tradeTitle">Product_price:</span>&nbsp;<%=MoneyUtil.round(total_price,2)%></td>
										 <td  colspan="2" align="left"  height="25" bgcolor="#FFFFFF"  ><span class="tradeTitle">shipping:</span>&nbsp;<%=rows.get("mc_shipping",0d)%></td>
									</tr>
									<tr>
										 <td colspan="2" align="left"  height="25" bgcolor="#eeeeee"><span class="tradeTitle">total:</span>&nbsp;<%=MoneyUtil.add(total_price,rows.get("mc_shipping",0d))%></td>
										 <td colspan="2" align="left"  height="25"bgcolor="#eeeeee"><span class="tradeTitle">ip:</span>&nbsp;<%=rows.getString("ip_address")%></td>
									</tr>
								   <tr>	
										<td colspan="2" align="left"  height="25" bgcolor="#FFFFFF"><span class="tradeTitle">payment method:</span>&nbsp;<%=rows.getString("payment_method")%></td>
										<td colspan="2" align="left"  height="25"bgcolor="#FFFFFF"><span class="tradeTitle">payment type:</span>&nbsp;<%=rows.getString("payment_type")%></td>
								   </tr>
								<tr>
								 	<td colspan="2" align="left" height="25"bgcolor="#FFFFFF"><span class="tradeTitle">customer telephone:</span>&nbsp;&nbsp;<%=detailOrder.getString("tel")%></td>
									<td colspan="2" align="left"  height="25"bgcolor="#FFFFFF">&nbsp; </td>
							    </tr> 
							    <tr>
									<td colspan="2" align="left" height="25"bgcolor="#eeeeee"><span class="tradeTitle">cc_number:</span>&nbsp;&nbsp;<%=rows.getString("cc_number")	%></td>
									<td colspan="2" align="left"  height="25"bgcolor="#eeeeee"><span class="tradeTitle">cc_type:</span>&nbsp;&nbsp;<%=rows.getString("cc_type")	%></td>
							    </tr>
							    
								  <tr>
									   <td     bgcolor="#eeeeee"height="25"   colspan="4">
										<span class="tradeTitle">customer comment :</span><%=rows.getString("customer_comment")%> 
									   </td>
								   </tr>
								   <tr>
									<td  bgcolor="#eeeeee" height="25"   colspan="4">
									<span class="tradeTitle">billing address :</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=rows.getString("billing_address_name")%>,<%=rows.getString("billing_address_street")%>,<%=rows.getString("billing_address_city")%>,<%=rows.getString("billing_address_state")%>,<%=rows.getString("billing_address_zip")%>,<%=rows.getString("billing_address_country")%></td>
								   </tr>
								   <tr>
								   <td bgcolor="#FFFFFF"  height="25"   colspan="4">
								   <span class="tradeTitle">delivery address:</span> &nbsp;&nbsp;&nbsp;&nbsp;
									 <%=rows.getString("delivery_address_name")%>,<%=rows.getString("delivery_address_street")%>,<%=rows.getString("delivery_address_city")%>,<%=rows.getString("delivery_address_state")%>,<%=rows.getString("delivery_address_zip")%>,<%=rows.getString("delivery_address_country")%>
								   </td>
								   </tr>
									<% 
									} %>  
					   </table>      
			</div>
			<div style="margin-top:10px;">
			      <table width="98%" border="0" cellspacing="0" cellpadding="3">
			      	<tr>
			      		<td align="right"><input type="button" style="font-weight: bold" class="normal-green" onClick="next(1)" value="下一步"/></td>
			      	</tr>
			      </table>
		 		</div>
		</div>
		<div id="deliveryDetails" style="height:430px;">
			<table width="98%" border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td colspan="2" style="padding-bottom: 5px;">
					<span style='font-weight:normal'>从订单 <input name='duplicate_oid' type='text' id='duplicate_oid'  style='width:60px;color:red' onChange="duplicateDelivery(this.value)"> 复制地址信息</span>
				</td>
			</tr>
	        <tr>
	          <td align="left" valign="middle" bgcolor="#eeeeee" style="padding-left:5px;">
	          	<span class="ebayTitle">Name:</span>&nbsp;<input type="text" id="address_name" value="<%=StringUtil.replaceString(detailOrder.getString("address_name"),"\""," ")%>"/>
	          </td>
	          <td height="30" align="left" valign="middle" bgcolor="#eeeeee">
	          	<span class="ebayTitle">City:</span>&nbsp;
			  	<input id="address_city" value="<%=detailOrder.getString("address_city")%>"/>
			  </td>
	        </tr>
	        <tr>
	          <td align="left" valign="middle" bgcolor="#eeeeee" style="padding-left:5px;">
	          	<span class="ebayTitle">Street:</span>&nbsp;<input type="text" id="address_street" value="<%=detailOrder.getString("address_street")%>"/>
	          </td>
	          <td height="30" align="left" valign="middle" bgcolor="#eeeeee">
	          	<span class="ebayTitle">Zip:</span>&nbsp;
			  	<input id="address_zip" value="<%=detailOrder.getString("address_zip")%>"/>
			  </td>
	        </tr>
	        <tr>
	          <td align="left" valign="middle" style="padding-left:5px;">
			  <span class="ebayTitle">Tel:</span>&nbsp;<input name='tel' id='tel' type='text' size='30' style="width:140px;" value="<%=detailOrder.getString("tel")%>"/>
			  </td>
	          <td height="30" align="left" valign="middle"><span class="ebayTitle">Country:</span>&nbsp;
	
	<%
		DBRow countrycode[] = orderMgr.getAllCountryCode();
		String selectBg="#ffffff";
		String preLetter="";
		%>
	      <select name="ccid_hidden" id="ccid_hidden" onChange="getStorageProvinceByCcid(this.value);preCalcuOrder(<%=oid%>,this.value,0);">
		  <option value="0">请选择...</option>
		  <%
		  for (int i=0; i<countrycode.length; i++)
		  {
		  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
			{
				if (selectBg.equals("#eeeeee"))
				{
					selectBg = "#ffffff";
				}
				else
				{
					selectBg = "#eeeeee";
				}
			}  	
			
			preLetter = countrycode[i].getString("c_country").substring(0,1);
		  %>
		    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>" <%=countrycode[i].get("ccid",0l)==detailOrder.get("ccid",0l)?"selected":""%>><%=countrycode[i].getString("c_country")%></option>
		  <%
		  }
		  %>
	      </select>		
	 
	[<%=detailOrder.getString("address_country")%>] </td>
	        </tr>
	        <tr>
	          <td align="left" valign="middle" bgcolor="#eeeeee">&nbsp;</td>
	          <td height="30" align="left" valign="middle" bgcolor="#eeeeee"><span class="ebayTitle">State:</span>
			  &nbsp;
			      <select name="pro_id" id="pro_id" onChange="preCalcuOrder(<%=oid%>,$('#ccid_hidden').val(),this.value);stateDiv(this.value);">
			      </select>		
			      	<div style="padding-top: 10px;display:none;" id="state_div">
						<input type="text" name="address_state" id="address_state" value="<%=detailOrder.getString("address_state")%>"/>
					</div>[<%=detailOrder.getString("address_state")%>]
			 </td>
	        </tr>
	      </table>
	      <br/>
		      	<div style="margin-top:240px;">
			      <table width="98%" border="0" cellspacing="0" cellpadding="3">
			      	<tr>
			      		<td align="center"><input type="button" style="font-weight: bold" class="normal-green" onClick="checkDeliveryInfo(2)" value="下一步"/></td>
			      		<td align="center"><input type="button" style="background-image:url('../imgs/doubt_record_order_address.jpg');width: 119;height:35;border:0px;" onclick="doubtTypeOrder(<%=HandStatusleKey.DOUBT_ADDRESS%>)"/></td>
			      	</tr>
			      </table>
		 		</div>
		</div>
		<div id="orderItem" style="min-height:430px;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="11%" height="35" align="center" valign="middle" bgcolor="#D3EB9A" style="padding-left:5px;">搜索商品</td>
						<td width="89%" valign="middle" bgcolor="#D3EB9A"><input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold"/>数量 
							<input name="quantity" type="text" id="quantity"  style="color:#FF3300;width:40px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold" value="1"/>
							&nbsp;<input name="Submit3" type="button" class="long-button" value="添加到订单" onClick="put2Cart()"> &nbsp;&nbsp;(多使用回车，效率更高哦)    
					     </td>
					  </tr>
						<tr>
							<td colspan="2">
								<br/>
								<table width="100%" border="0"  cellspacing="1" align="center" cellpadding="0"   bgcolor="#cccccc" >
													<tr>
														<td align="left" colspan="4" bgcolor="white">
															<span class="ebayTitle" style="color:#999999">Ebay ID:</span><%=detailOrder.getString("auction_buyer_id")%></td>
														</td>
														<td align="left" colspan="5" bgcolor="white">
															<span class="ebayTitle" style="color:#999999">Client ID:</span> <%=detailOrder.getString("client_id")%>
														</td>
													</tr>
													<%
						if (detailOrder.getString("note").equals("")==false)
						{
						 
						%>
				        <tr>
				          <td colspan="2" style="padding:5px;font-size:12px;color:#CC0000;border:1px #CCCCCC solid;background:#FFFFE1">
				<img src="../imgs/client.gif" width="21" height="25" align="absmiddle"> &nbsp; <%=detailOrder.getString("note")%>
						</td>
				        </tr>
						<%
						}
						%>
													  <tr>
													  	 <td width="13%"  height="20" align="center"  ><span class="tradeTitle">Item #</span></td>
														 <td width="30%"  height="20" align="center"  ><span class="tradeTitle">Item_Name</span></td>
														 <td align="center" width="5%"  ><span class="tradeTitle">QTY</span></td>
														 <td width="5%" align="center"><span class="tradeTitle">Price</span></td>
														 <td width="10%" align="center"><span class="tradeTitle">Sku</span></td>
														 <td width="10%" align="center"><span class="tradeTitle">Mpn</span></td>
														 <td width="5%" align="center"><span class="tradeTitle">FVF</span></td>
														 <td width="5%" align="center"><span class="tradeTitle">Cur</span></td>
														 <td width="5%" align="center"><span class="tradeTitle">Ship</span></td>
													 </tr>
												 <%
													for(int i=0;i<tradeItems.length;i++)
													{	 			 		
												%>
														<tr>
															<td align="center" height="25" bgcolor="#eeeeee"><%=tradeItems[i].getString("item_number")%></td>
															<td align="center" height="25" bgcolor="#eeeeee"><%=tradeItems[i].getString("item_name")%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].get("quantity",0f)%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].get("mc_gross",0d)%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].getString("sku")%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].getString("mpn")%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].get("final_value_fee",0d)%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].getString("currency")%></td>
															<td align="center" bgcolor="#eeeeee"><%=tradeItems[i].get("mc_shipping",0d)%></td>
														</tr>
												<% 
													} 			
										   		%>	
								</table>
							</td>
						</tr>
					  	<tr>
							<td colspan="2" align="left" valign="middle" style="padding-top:10px;">
							  <img src="../imgs/paste_email.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="javascript:pasteEmail()"><span id="paste_email">粘贴邮件内容										                                   </span></a>
									
									<img src="../imgs/paste_email.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="#" onClick="showTran()"  >  <span id="showTran">显示														                                    交易	信息</span></a>
									
	<div id="tranForm" style="display:none">
						<table width="100%"     border="0"    align="center" cellpadding="5" cellspacing="0" bgcolor="#cccccc" > 
						   
									<%
											if(rows!=null)
											{
									%>
									<tr>
									<td width="50%" colspan="2" height="25"  align="left" bgcolor="#eeeeee"  > <span class="tradeTitle">tran id: </span>&nbsp; <%=rows.getString("txn_id")  %></td>
										 <td colspan="2" align="left" height="25" bgcolor="#eeeeee"    ><span class="tradeTitle">currency:</span>&nbsp;<%=rows.getString("mc_currency")%> </td>   
									</tr>
									<tr>
										<td colspan="2"   align="left"  height="25" bgcolor="#FFFFFF"    ><span class="tradeTitle">Product_price:</span>&nbsp;<%=MoneyUtil.round(total_price,2)%></td>
										 <td  colspan="2" align="left"  height="25" bgcolor="#FFFFFF"  ><span class="tradeTitle">shipping:</span>&nbsp;<%=rows.get("mc_shipping",0d)%></td>
									</tr>
									<tr>
										 <td colspan="2" align="left"  height="25" bgcolor="#eeeeee"><span class="tradeTitle">total:</span>&nbsp;<%=MoneyUtil.add(total_price,rows.get("mc_shipping",0d))%></td>
										 <td colspan="2" align="left"  height="25"bgcolor="#eeeeee"><span class="tradeTitle">ip:</span>&nbsp;<%=rows.getString("ip_address")%></td>
									</tr>
								   <tr>	
										<td colspan="2" align="left"  height="25" bgcolor="#FFFFFF"><span class="tradeTitle">payment method:</span>&nbsp;<%=rows.getString("payment_method")%></td>
										<td colspan="2" align="left"  height="25"bgcolor="#FFFFFF"><span class="tradeTitle">payment type:</span>&nbsp;<%=rows.getString("payment_type")%></td>
								   </tr>
								<tr>
								 	<td colspan="2" align="left" height="25"bgcolor="#FFFFFF"><span class="tradeTitle">customer telephone:</span>&nbsp;&nbsp;<%=detailOrder.getString("tel")%></td>
									<td colspan="2" align="left"  height="25"bgcolor="#FFFFFF">&nbsp; </td>
							    </tr> 
							    <tr>
									<td colspan="2" align="left" height="25"bgcolor="#eeeeee"><span class="tradeTitle">cc_number:</span>&nbsp;&nbsp;<%=rows.getString("cc_number")	%></td>
									<td colspan="2" align="left"  height="25"bgcolor="#eeeeee"><span class="tradeTitle">cc_type:</span>&nbsp;&nbsp;<%=rows.getString("cc_type")	%></td>
							    </tr>
							    
								  <tr>
									   <td     bgcolor="#eeeeee"height="25"   colspan="4">
										<span class="tradeTitle">customer comment :</span><%=rows.getString("customer_comment")%> 
									   </td>
								   </tr>
								   <tr>
									<td  bgcolor="#eeeeee" height="25"   colspan="4">
									<span class="tradeTitle">billing address :</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=rows.getString("billing_address_name")%>,<%=rows.getString("billing_address_street")%>,<%=rows.getString("billing_address_city")%>,<%=rows.getString("billing_address_state")%>,<%=rows.getString("billing_address_zip")%>,<%=rows.getString("billing_address_country")%></td>
								   </tr>
								   <tr>
								   <td bgcolor="#FFFFFF"  height="25"   colspan="4">
								   <span class="tradeTitle">delivery address:</span> &nbsp;&nbsp;&nbsp;&nbsp;
									 <%=rows.getString("delivery_address_name")%>,<%=rows.getString("delivery_address_street")%>,<%=rows.getString("delivery_address_city")%>,<%=rows.getString("delivery_address_state")%>,<%=rows.getString("delivery_address_zip")%>,<%=rows.getString("delivery_address_country")%>
								   </td>
								   </tr>
									<% 
									} %>  
					   </table>      
			</div>
 	<br/>
    <textarea  style="width:750px;height:200px;display:none;border:1px #999999 dashed;color:#666666;padding:5px;" id="emailContainer"></textarea>
	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/batchModifyProductsQuantityFromCart.action" method="post" name="cart_form" id="cart_form">
		<div id="mini_cart_page"></div>
	</form>	
	</td>
    </tr>
	
  <tr>
    <td colspan="2" align="right" valign="middle" style="color:#FF0000" id="action_info"></td>
  </tr>
  <tr><td colspan="2" align="left" valign="middle" style="padding-top:10px;color:#999999">
  <% 
		DBRow rowOrderPending = vertualTerminalMgrQLL.getPaymentStatus(oid);
		if(rowOrderPending!=null&&rowOrderPending.getString("payment_status").equals("Pending"))
		{
			if(rowOrderPending.getString("pending_reason")!="" && !rowOrderPending.getString("pending_reason").equals(""))
			{
	 %>
		<p style="color:#CC0000;font-size:13px"  ><strong> <%=rowOrderPending.getString("pending_reason") %> pending reasion:</strong></p>   
	    <%   }    %>
	
		<% if(rowOrderPending.getString("fraud_management_pending_filters")!="" && !rowOrderPending.getString("fraud_management_pending_filters").equals(""))
			{
				String filters[]=rowOrderPending.getString("fraud_management_pending_filters").split("\n");
		%>
		<p style="font-size:13px"> 
			<%for(int n=0;n<filters.length;n++)
			 {
			 %>
			 	 <%=n+1 %>:<%=filters[n] %>
			 <% 	
			 }	
			%> </p>	
		<% 	
			}	
		%>			
	<%
		}
	 %>
  </td></tr>
</table>
	      <table width="98%" border="0" cellspacing="0" cellpadding="3" style="padding-top: 10px;">
	      	<tr>
	      		<td align="center"><input type="button" style="font-weight: bold" class="normal-green" onClick="checkDeliveryInfo(3)" value="下一步"/></td>
	      		<td align="center"><input type="button" style="background-image:url('../imgs/doubt_record_order_item.jpg');width: 119;height:35;border:0px;" onclick="doubtTypeOrder(<%=HandStatusleKey.DOUBT%>)"/></td>
	      	</tr>
	      </table>
		</div>
		<div id="storageExpress" style="padding:0px;min-height:457px;padding-top:4px;" >
			<div id="create_waybill"></div>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><a href="javascript:parent.closeTBWinRefrech && parent.closeTBWinRefrech();$.artDialog.close && $.artDialog.close();"><img src="../imgs/close_record_order.jpg" width="51" height="35" border="0"></a></td>
				</tr>
				
					
				
			</table>
		</div>
		<!--
		<div id="waybill">
			配送备注<br>
          <textarea name="delivery_note" id="delivery_note" style="width:300px;height:50px;font-size:12px;"><%=detailOrder.getString("delivery_note")%></textarea>
		</div>
		 -->
	</div>
</div>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/recordOrder.action" method="post" name="save_form" id="save_form">
	<input type="hidden" name="oid"/>
	<input type="hidden" name="tel"/>
	<input type="hidden" name="handle_status"/>
	<input type="hidden" name="record_order_note"/>
	<input type="hidden" name="order_error_email"/>
	<input type="hidden" name="ps_id"/>
	<input type="hidden" name="backurl" value=""/>	
	<input type="hidden" name="ccid" />
	<input type="hidden" name="pro_id"/>
	<input type="hidden" name="address_street"/>
	<input type="hidden" name="address_city"/>
	<input type="hidden" name="address_zip"/>
	<input type="hidden" name="address_name"/>
</form>
<script>
	$("#tabs").tabs({
		cache: false,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		load: function(event, ui) {onLoadInitZebraTable();}	,
		selected:0,
		show:function(event,ui)
			 {
			 	var isChangeLastTabs = true;
			 	if(lastTabs ==2)
			 	{
			 		saveCart();
			 	}
			 	if(ui.index==2)
			 	{
			 		loadCart(0);
			 	}
			 	if(ui.index==3)
			 	{
			 		cleanWayBill();
			 		checkDeliveryInfo(ui.index);
			 		trueOrder();
			 		loadWaybill();
			 	}
			 			 	
			 	if(isChangeLastTabs)
			 	{
			 		lastTabs = ui.index;
			 	}
			 	
			 }
		});
		
		
	function next(index)
	{
		lastTabs = index;
		$("#tabs").tabs("select",index);
	}
	
	function addressQuestion()
	{
		
	}
	
	function checkDeliveryInfo(index)
	{
		var address_name = $("#address_name").val();
		var address_street = $("#address_street").val();
		var address_city = $("#address_city").val();
		var address_zip = $("#address_zip").val();
		var tel = $("#tel").val();
		var ccid = $("#ccid_hidden").val();
		var pro_id = $("#pro_id").val();
		var address_state = $("#address_state").val();
		
		if(address_name==""||address_street==""||address_city==""||address_zip==""||ccid==0||pro_id==0||(pro_id==-1&&address_state==""))
		{
			alert("地址信息不完整！");
			next(1);
		}
		else if(address_street.length>36)
		{
			alert("地址信息太长，请缩到36个以内（含空格符号）！")
			next(1);
		}
		else
		{
			next(index)
		}
	}
	
	function saveCart()
	{
		$("#cart_form").submit();
	}
	
	function cleanWayBill()
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/cleanWayBIllCart.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			async:false,
			
			beforeSend:function(request){
				
			},
				
			error: function(){
			},
				
			success: function(html){
			}
		});
	}

	// add by zhangrui 
	function showInfo(){
		var div_info = $("#div_info");
		if(div_info.css("display") === "none"){
			div_info.slideDown("slow");
		}else{
			div_info.slideUp("slow");
		};
	}
</script>			
</body>
</html>
