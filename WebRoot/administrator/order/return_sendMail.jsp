<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="returnProductKey" class="com.cwc.app.key.ReturnProductKey"/>
<%@ include file="../../include.jsp"%> 
<%
long rp_id = StringUtil.getLong(request,"rp_id");
long oid = StringUtil.getLong(request,"oid");
DBRow returnProduct = orderMgr.getDetailReturnProductByRpId(rp_id);
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>退单信息</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>


<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<style type="text/css">
<!--
.STYLE1 {font-weight: bold}



-->
</style>

<script>

function sendMail(rp_id)
{
	$("#sendMail").submit();
	//tb_show('退货登记',"return_sig.html?rp_id="+rp_id+"&TB_iframe=true&height=500&width=650",false);	
}

</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  >
<form id="sendMail" action="<%=ConfigBean.getStringValue("systenFolder")%>action/order/RuturnProductSendMailPrecessAction.action" method="post">
	<input  name="rp_id" type="hidden" value="<%=rp_id %>"/>
	<input  name="oid" type="hidden" value="<%=oid %>" />
</form>

<br>
<br>
<table width="100%" border="0" height="93%" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td style="padding-left:30px;padding-top: 30px;" valign="top" >
	

      <table width="620" border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td width="50%" ><table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="61%" height="25" class="title"><span class="STYLE1"><strong>退货单号</strong>：</span><%=rp_id%></td>
        <td width="39%" class="title"><span class="STYLE1"><strong>创建日期</strong>：</span><%=returnProduct.getString("create_date")%></td>
      </tr>
      <tr>
        <td height="25" class="title"><span class="STYLE1"><strong>状态</strong>：</span><%=returnProductKey.getReturnProductStatusById(returnProduct.get("status",0))%></td>
        <td class="title"><span class="STYLE1"><strong>完成日期</strong>：</span>
		<%
		if ( returnProduct.getString("create_date").equals(returnProduct.getString("handle_date")) )
		{
			out.println("&nbsp;");
		}
		else
		{
			out.println(returnProduct.getString("handle_date"));
		}
		%>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td >
	<br>

			 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">主商品信息</legend>
<%
DBRow returnProducts[] = orderMgr.getReturnProductItemsByRpId(rp_id,null);
%>
<table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
<%
for (int i=0; i<returnProducts.length; i++)
{
%>
  <tr>
    <td width="69%" style="font-family:Arial;font-weight:bold"><%=returnProducts[i].getString("p_name")%></td>
    <td width="31%" style="font-family:Arial;font-weight:bold"><%=returnProducts[i].getString("quantity")%> <%=returnProducts[i].getString("unit_name")%> </td>
  </tr>
<%
}
%>
</table>
			 </fieldset>			 	</td>
  </tr>
  
  <tr>
    <td >
    </td>
  </tr>
  <tr>
    <td >&nbsp;</td>
  </tr>
  <tr>
    <td ><strong>退货原因：</strong><%=orderMgr.getReturnProductReasonById(returnProduct.get("return_product_reason",0))%></td>
  </tr>
  <tr>
    <td ><br>
      <br>
      <br></td>
  </tr>
</table>
	
	</td>
	
  </tr>
  <tr>
	<td class="win-bottom-line"  align="right"  valign="middle">

      <input name="Submit" type="button" class="normal-green" value="发送邮件" onClick="sendMail(<%=rp_id%>)">
	  &nbsp;&nbsp;&nbsp;&nbsp;
      <input name="Submit2" type="button" class="normal-white" value="不发送" onClick="$.artDialog.close()">   
    </td>
    </tr>
</table>
</body>

</html>