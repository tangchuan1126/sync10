<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@ page import="com.cwc.app.order.print.ExpressPrintBean"%>
<%@ page import="com.cwc.app.order.print.ExpressPolicyCore"%>
<%
long oid = StringUtil.getLong(request,"oid");
long handle = StringUtil.getLong(request,"handle");
long handle_status = StringUtil.getLong(request,"handle_status");
long invoice_id = StringUtil.getLong(request,"invoice_id");
long ccid = StringUtil.getLong(request,"ccid");

long pro_id = StringUtil.getLong(request,"pro_id");
long ps_id = StringUtil.getLong(request,"ps_id");

float weight = orderMgr.getOrderWeightByOid(oid);

ExpressPolicyCore expressPolicy = (ExpressPolicyCore)MvcUtil.getBeanFromContainer("NotLimitWeightExpressPolicyImp");
expressPolicy.setCcid(ccid);
expressPolicy.setOid(oid);
expressPolicy.setPsId(ps_id);
expressPolicy.setProId(pro_id);
expressPolicy.setWeight(weight);
			
ExpressPrintBean expressPrintBean[] = expressPolicy.getExpressPrintPage();

//排序，找出价格最低的快递
long minPriceScId = 0;
double minPrice = 0; 

for (int i=0; i<expressPrintBean.length; i++)
{
	if (i==0)
	{
		minPrice = expressPrintBean[i].getShippingFee();
		minPriceScId = expressPrintBean[i].getScID();
		continue;
	}
	
	if (expressPrintBean[i].getShippingFee()<minPrice)
	{
		minPrice = expressPrintBean[i].getShippingFee();
		minPriceScId = expressPrintBean[i].getScID();
	}
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>请选择打印快递</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style>
.print-button2
{
	width:130px;
	height:40px;
	font-size:20px;
	font-weight:bold;
	font-family:Arial, Helvetica, sans-serif;
	border:2px #999999 solid;
	background:#f8f8f8;
	color:#666666;
	float:left;
	margin-right:10px;
	cursor: pointer;
}

form
{
	padding:0px;
	margin:0px;
}

.warning
{
	border-bottom:1px #cccccc solid;background:#ffffff;color:#666666;font-size:15px;font-weight:bold;
}

.error
{
	border-bottom:1px #cccccc solid;background:#CC0000;color:#ffffff;font-size:15px;font-weight:bold;
}
</style>

<script>
function printIt(sc_id,printPage)
{
	printPage = "print/"+printPage;
	
	document.print_form.action = printPage;
	document.print_form.sc_id.value = sc_id;
	document.print_form.submit();
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="print_form" method="post" action="" target="_blank">
<input type="hidden" name="oid" value="<%=oid%>">
<input type="hidden" name="handle" value="<%=handle%>">
<input type="hidden" name="handle_status" value="<%=handle_status%>">
<input type="hidden" name="invoice_id" value="<%=invoice_id%>">
<input type="hidden" name="ccid" value="<%=ccid%>">
<input type="hidden" name="sc_id">

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="50" align="center" valign="middle" class="<%=expressPrintBean.length>0?"warning":"error"%>"><%
	if ( expressPrintBean.length>0 )
	{
	%>
	 请选择一个快递
	<%
	}
	else
	{
	%>
	 没有合适发货快递
	<%
	}
	%>	 </td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle" style="padding-left:20px;">
	<%
	for (int i=0; expressPrintBean.length>0&&i<expressPrintBean.length; i++)
	{
	%>
	  
	  <div class="print-button2" onClick="printIt(<%=expressPrintBean[i].getScID()%>,'<%=expressPrintBean[i].getPage().replaceAll(".jsp", ".html")%>')" style="background:<%=minPriceScId==expressPrintBean[i].getScID()?"#C1E285":"#eeeeee"%>"><%=expressPrintBean[i].getName()%> 
	  <br>
	  <span style="font-size:12px;font-weight:normal">￥<%=expressPrintBean[i].getShippingFee()%></span></div>
	  
	<%
	}
	%>	
	   </td>
  </tr>
</table>
	
	</td>
  </tr>
  <tr>
    <td width="49%" align="right" class="win-bottom-line"><input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeTBWin();">	</td>
  </tr>
</table> 

</body>
</html>
 