<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductStatusKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<jsp:useBean id="returnProductKey" class="com.cwc.app.key.ReturnProductKey"/>
<%@ include file="../../include.jsp"%> 
<%
long rp_id = StringUtil.getLong(request,"rp_id");
DBRow returnProduct = orderMgr.getDetailReturnProductByRpId(rp_id);

WarrantyTypeKey warrantyTypeKey = new WarrantyTypeKey(); 
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单任务</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>


<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<style type="text/css">
<!--
.STYLE1 {font-weight: bold}



-->
</style>

<script>
function returnUploadFile(rp_id)
{
	$.artDialog.open("../warranty_return/return_product_upload.html?rp_id="+rp_id , {title: '文件形式确认退货',width:'400px',height:'330px', lock: true,opacity: 0.3,fixed:true});
}

function checkForm(thrForm)
{
	if (thrForm.rp_id.value=="")
	{
		alert("请填写退货单号");
		return(false);
	}
	else
	{
		return(true);
	}
}

function sigReturn(rp_id)
{

	tb_show('退货登记',"return_sig.html?rp_id="+rp_id+"&TB_iframe=true&height=500&width=650",false);	
}

function closeTBWin()
{
	tb_remove();
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  >
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   退货管理 »   退货单详细</td>
  </tr>
</table>

<br>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td style="padding-left:30px;">
		<table width="620" border="0" cellpadding="5" cellspacing="0">
		  <tr>
		    <td width="50%" ><table width="100%" border="0" cellspacing="0" cellpadding="3">
		      <tr>
		        <td width="61%" height="25" class="title"><span class="STYLE1"><strong>退货单号</strong>：</span><%=rp_id%></td>
		        <td width="39%" class="title"><span class="STYLE1"><strong>创建日期</strong>：</span><%=returnProduct.getString("create_date")%></td>
		      </tr>
		      <tr>
		        <td height="25" class="title"><span class="STYLE1"><strong>状态</strong>：</span><%=returnProductKey.getReturnProductStatusById(returnProduct.get("status",0))%></td>
		        <td class="title"><span class="STYLE1"><strong>完成日期</strong>：</span>
				<%
				if ( returnProduct.getString("create_date").equals(returnProduct.getString("handle_date")) )
				{
					out.println("&nbsp;");
				}
				else
				{
					out.println(returnProduct.getString("handle_date"));
				}
				%>		
				</td>
		   </tr>
    	</table>
    </td>
  </tr>
  <tr>
  		<td>
  			<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
				<legend style="font-size:15px;font-weight:normal;color:#999999;">客服反馈信息</legend>
				<%
					DBRow[] returnProductReason = productReturnOrderMgrZJ.getAllReturnReason(rp_id);
				%>
				<table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
				<%
					for(int i=0;i<returnProductReason.length;i++)
					{
				%>
					<tr>
						<td style="font-family:Arial;font-weight:bold"><%=productMgr.getDetailProductByPcid(returnProductReason[i].get("pc_id",0l)).getString("p_name")%></td>
						<td style="font-family:Arial;font-weight:bold"><%=returnProductReason[i].get("quantity",0f)%></td>
						<td style="font-family:Arial;font-weight:bold"><%=warrantyTypeKey.getWarrantyTypeById(returnProductReason[i].getString("warranty_type"))%></td>
						<td style="font-family:Arial;font-weight:bold"><%=returnProductReason[i].getString("return_reason")%></td>
					</tr>
				<%
					}
				%>
				</table>
			</fieldset>
  		</td>
  </tr>
  <tr>
    <td >
	<br>

			<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
				<legend style="font-size:15px;font-weight:normal;color:#999999;">主商品信息</legend>
				<%
				DBRow returnProducts[] = orderMgr.getReturnProductItemsByRpId(rp_id,null);
				%>
				<table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
				<%
				for (int i=0; i<returnProducts.length; i++)
				{
				%>
				  <tr>
				    <td width="69%" style="font-family:Arial;font-weight:bold"><%=returnProducts[i].getString("p_name")%></td>
				    <td width="31%" style="font-family:Arial;font-weight:bold"><%=returnProducts[i].getString("quantity")%> <%=returnProducts[i].getString("unit_name")%> </td>
				  </tr>
				<%
				}
				%>
				</table>
			 </fieldset>			 	</td>
  </tr>
  
  <tr>
    <td ><br>

	
			 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">散件商品信息</legend>
<%
DBRow returnSubProducts[] = orderMgr.getReturnProductSubItemsByRpId( rp_id,null);
%>
<table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td height="22" style="font-family:Arial;font-weight:bold;border-bottom:1px #cccccc solid;background:#eeeeee">&nbsp;</td>
    <td style="font-family:Arial;font-weight:bold;border-bottom:1px #cccccc solid;background:#eeeeee">&nbsp;</td>
    <td align="center" valign="middle" style="font-family:Arial;;border-bottom:1px #cccccc solid;background:#eeeeee">完好</td>
    <td align="center" valign="middle" style="font-family:Arial;;border-bottom:1px #cccccc solid;background:#eeeeee">功能残损</td>
    <td align="center" valign="middle" style="font-family:Arial;;border-bottom:1px #cccccc solid;background:#eeeeee">外观残损</td>
  </tr>
<%
for (int i=0; i<returnSubProducts.length; i++)
{
%>

  <tr>
    <td width="46%" height="25" style="font-family:Arial;font-weight:bold"><%=returnSubProducts[i].getString("p_name")%></td>
    <td width="12%" style="font-family:Arial;font-weight:bold"><%=returnSubProducts[i].getString("quantity")%> <%=returnSubProducts[i].getString("unit_name")%> </td>
    <td width="15%" align="center" valign="middle" style="font-family:Arial;font-weight:bold"><%=returnSubProducts[i].getString("quality_nor")%></td>
    <td width="14%" align="center" valign="middle" style="font-family:Arial;font-weight:bold;<%=returnSubProducts[i].get("quality_damage",0f)>0?"color:#FF0000":""%>">
	<%=returnSubProducts[i].getString("quality_damage")%>	</td>
    <td width="13%" align="center" valign="middle" style="font-family:Arial;font-weight:bold;<%=returnSubProducts[i].get("quality_package_damage",0f)>0?"color:#FF0000":""%>"><%=returnSubProducts[i].getString("quality_package_damage")%></td>
  </tr>
<%
}
%>
</table>
			 </fieldset>	</td>
  </tr>
  <tr>
    <td >&nbsp;</td>
  </tr>
  <%
  	DBRow[] files = fileMgrZJ.getFileByWithIdAndType(rp_id,FileWithTypeKey.ProductReturn);
  	for(int i=0;i<files.length;i++)
  	{
  %>
  	<tr>
    	<td>
    		<%
    			if(i==0)
    			{
    		%>
    			<strong>客户文件：</strong>
    		<%	
    			} 
    		%>
    		
    		<a href="../../returnImg/<%=files[i].getString("file_name")%>"><%=returnProduct.getString("file")%></a>
    	</td>
  	</tr>
  <%
  	}
  %>
  
  
  <tr>
    <td ><br>
      <br>
      <br></td>
  </tr>
  <tr>
    <td >
	  <%
	  if (returnProduct.get("status",0)==returnProductKey.WAITING)
	  {
	  %>
      <input name="Submit" type="button" class="long-button-mod" value="退货登记" onClick="sigReturn(<%=rp_id%>)">	  
  	  <%
	  }
	  %>
	  
	  <%
	  	if(returnProduct.get("product_status",0)==ReturnProductStatusKey.GetImg||returnProduct.get("product_status",0)==ReturnProductStatusKey.Waiting)
	  	{
	  %>
	  <input name="Submit" type="button" class="long-button-mod" value="上传图片" onClick="returnUploadFile(<%=rp_id%>)">
	  <%
	  	}
	  %>
	  &nbsp;&nbsp;&nbsp;&nbsp;
      <input name="Submit2" type="button" class="long-button" value="返回" onClick="history.back()">   </td>
  </tr>
</table>


	
	</td>
  </tr>
</table>
</body>

</html>