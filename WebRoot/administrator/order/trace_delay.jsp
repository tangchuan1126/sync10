<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="tracingOrderKey" class="com.cwc.app.key.TracingOrderKey"/>
<%@ include file="../../include.jsp"%> 
<%
int p = StringUtil.getInt(request,"p");

PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(30);

DBRow rows[] = orderMgr.getTraceDelay(pc);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.search_form.input_st_date.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.search_form.input_en_date.value = date;
}

function onMOBg(row,cl,index)
{
	row.style.background=cl;
}

function filter()
{
	document.search_form.cmd.value="filter";
	document.search_form.submit();
}

function getDetailSysLog(sl_id)
{
	tb_show('详细日志','detail_sys_log.html?sl_id='+sl_id+'&height=550&width=900',false);
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 订单管理 »   超时跟进</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form name="listForm" method="post">


    <tr> 
        <th style="vertical-align: center;text-align: center;" class="left-title">订单号</th>
        <th style="vertical-align: center;text-align: center;" class="right-title">应该跟进日期</th>
        <th width="26%" style="vertical-align: center;text-align: center;" class="right-title">实际跟进日期</th>
        <th width="23%" style="vertical-align: center;text-align: center;" class="right-title">跟进类型</th>
    </tr>

    <%


for ( int i=0; i<rows.length; i++ )
{

%>
    <tr > 
      <td   width="24%" height="30" align="center" valign="middle" style='word-break:break-all;' ><a href="ct_order_auto_reflush.html?val=<%=rows[i].getString("oid")%>&cmd=search&search_field=0" target="_blank"><%=rows[i].getString("oid")%></a></td>
      <td   width="27%" align="center" valign="middle" style='word-break:break-all;' ><%=rows[i].getString("trace_date")%></td>
      <td   align="center" valign="middle" style='word-break:break-all;'><%=rows[i].getString("delay_date")%></td>
      <td   align="center" valign="middle" style='word-break:break-all;'><%=tracingOrderKey.getTracingOrderKeyById(rows[i].getString("trace_type"))%></td>
    </tr>
    <%
}
%>
</form>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p">
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
</body>
</html>
