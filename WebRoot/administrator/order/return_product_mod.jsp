<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long rp_id = StringUtil.getLong(request,"rp_id");
long oid = StringUtil.getLong(request,"oid");
orderMgr.initCartReturn( request, rp_id);
DBRow detailReturnInfo = orderMgr.getDetailReturnProductByRpId(rp_id);
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>退货</title>

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>



<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<style type="text/css">
<!--
td
{
	font-size:12px;
}
.STYLE1 {color: #FF0000}

form
{
	padding:0px;
	margin:0px;
}

-->
</style>

<script>


$().ready(function() {

	function log(event, data, formatted) {

		
	}
	
	$("#p_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 410,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];   
			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name+" ["+data[i].unit_name+"]["+data[i].p_code+"]",
    				value:data[i].p_name+" ["+data[i].unit_name+"]["+data[i].p_code+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	loadCartReturn();
	ajaxCartForm();
	
});


//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   //beforeSubmit:  // pre-submit callback    
			//	function(){
			//		loadCart();
			//	}	
		  // ,  
		   success:       // post-submit callback  
				function(){
					loadCartReturn();
					cartProQuanHasBeChange = false;
				}
		       
	};
	

   $('#cart_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}

function loadCartReturn()
{
	//alert(para);

	$.ajax({
		url: 'mini_cart_return.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		
		beforeSend:function(request){
			
		},
		
		error: function(){
			alert("加载购物车失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
		}
	});
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 

function checkForm()
{
	var theForm = document.search_union_form;
	
	if(theForm.p_name.value=="")
	{
		alert("商品名不能为空");
		return(false);
	}
	else if(theForm.quantity.value=="")
	{
		alert("数量不能为空");
		return(false);
	}
	else if(!isNum(theForm.quantity.value))
	{
		alert("请正确填写数量");
		return(false);
	}
	else
	{

				//先检查购物车中的商品在当前仓库是否已经建库
				var para = "type=add&p_name="+theForm.p_name.value+"&quantity="+theForm.quantity.value;
			
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/Put2CartReturn.action',
					type: 'post',
					dataType: 'html',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(){
						alert("添加商品失败！");
					},
					
					success: function(msg)
					{				
						if (msg=="ProductNotExistException")
						{
							alert("商品不存在！");
						}
						else if (msg=="ProductUnionSetCanBeProductException")
						{
							alert("套装不能作为定制商品");
						}
						else if (msg=="ProductNotCreateStorageException")
						{
							//
						}
						else if (msg=="ok")
						{
							//商品成功加入购物车后，要清空输入框
							theForm.p_name.value = "";
							theForm.quantity.value = "";
							loadCartReturn();
							return(false);
						}
						else
						{
							alert(msg);
						}

					}
				});	
				

	}
}


function delProductFromCart(pid,p_name,product_type)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/RemoveProductFromCartReturn.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"pid="+pid+"&product_type="+product_type,
					
			error: function(){
				alert("商品删除失败！");
			},
			
			success: function(msg){
				loadCartReturn();
			}
		});	
	}
}

var cartProQuanHasBeChange = false;
var cartIsEmpty;

function onChangeQuantity(obj)
{
	if ( !isNum(obj.value) )
	{
		alert("请正确填写数字");
		return(false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}

function post(rp_id,oid)
{
	if ($("#ps_id").val()==0)
	{
		alert("请选择退货仓库");
	}
	else if ($("input[@name=return_product_flag]:checked").val()==1&&cartIsEmpty)
	{
		alert("请添加需要退货的商品");
	}
	else if (cartProQuanHasBeChange)
	{
		alert("请先保存修改数据");
	}
	else
	{
		parent.modReturnProductListAction(rp_id,oid,$("#ps_id").val());
	}
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><br>
    <table width="95%" border="0" align="center" cellpadding="7" cellspacing="0">
  
  
  <tr id="return_product_row" >
    <td width="89%" >
			 <fieldset style="border:1px #999999 solid;padding:10px;-webkit-border-radius:4px;-moz-border-radius:4px;width:620px">
<legend style="font-size:15px;font-weight:normal;color:#666666;">修改需要退货商品</legend>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
 <tr>
    <td width="50%"  style="padding-bottom:10px;"><strong>修改退货仓库</strong>     
       <select name="ps_id" id="ps_id">
        <option value="0">请选择...</option>
        <%
	  DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
	String qx;
	
	for ( int i=0; i<treeRows.length; i++ )
	{
		if ( treeRows[i].get("parentid",0) != 0 )
		 {
			qx = "├ ";
		 }
		 else
		 {
			qx = "";
		 }
%>
          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==detailReturnInfo.get("ps_id",0l)?"selected":""%>> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>          </option>
          <%
}
%>
	
      </select>
	  
	   </td>
   </tr>
   
  <tr>
    <td bgcolor="#eeeeee" style="padding-left:10px;padding-left:toppx;padding-bottom:5px;border-bottom:1px #999999 solid">
	<form name="search_union_form" method="post" action="">
	  商品
	    <input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:150px;"/> 
	    数量
        <input name="quantity" type="text" id="quantity"  style="color:#FF3300;width:40px;"/>
&nbsp;&nbsp;
      <input name="Submit" type="button" class="short-button" value="添加" onClick="checkForm()">	
	  </form>	  </td>
  </tr>
  <tr>
    <td>
	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/BatchModifyProductsQuantityFromCarReturn.action" method="post" name="cart_form" id="cart_form">
	<div id="mini_cart_page" style="color:#CCCCCC">数据加载中……</div>
	</form>	</td>
  </tr>
</table>
             </fieldset>	</td>
  </tr>
  
  <tr>
    <td style="font-size:15px;font-weight:bold">&nbsp;</td>
  </tr>
</table>
	
	</td></tr>
  <tr>
    <td align="right" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green" value="修改" onClick="post(<%=rp_id%>,<%=oid%>)">
      <input name="cancel" type="button" class="normal-white" value="取消" onClick="$.artDialog.close()">
    </td>
  </tr>
</table>

</body>

</html>