<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
long oid = StringUtil.getLong(request,"oid");
long select_ccid = StringUtil.getLong(request,"select_ccid");
long select_pro_id = StringUtil.getLong(request,"select_pro_id");
long ps_id = 0l;
DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);

if(select_ccid==10929)
{
	select_ccid = 11036;
}

//没有选择仓库，则系统自动计算
//抄单一次后，走的路线不同
//System.out.println(select_ccid+ " + "+select_pro_id+" + "+detailOrder.get("ps_id",0l));

if(select_ccid>0) 
{
	ps_id = productMgr.getPriorDeliveryWarehouse(select_ccid,select_pro_id);
}
//else if ()
//{
//	ps_id = productMgr.getPriorDeliveryWarehouse( detailOrder.get("ccid",0l),detailOrder.get("pro_id",0l) );
//}
else
{
	//ps_id = detailOrder.get("ps_id",0l);//修改抄单，仓库已经选择
}
%>
<html>
<head>
<style>
.unSelect
{
	border:2px #cccccc solid;
	padding:2px;
	width:98%;
	background:#FFFFFF;
	-webkit-border-radius:5px;-moz-border-radius:5px; 
}
.beSelect
{
	border:2px #99CC00 solid;
	padding:2px;
	width:98%;
	background:#F3FAE4;
	-webkit-border-radius:5px;-moz-border-radius:5px;
}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单预处理</title>
<link href="../comm.css" rel="stylesheet" type="text/css">

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
<%
ArrayList storageNameList = new ArrayList();
ArrayList storageList = new ArrayList();

storageNameList.add("当地仓库");
storageNameList.add("海外仓库");

storageList.add(catalogMgr.getNativeProductDevStorageCatalogByParentId(0,select_ccid,null));
storageList.add(catalogMgr.getNonNativeProductDevStorageCatalogByParentId(0,select_ccid,null));

for (int ii=0; ii<storageList.size(); ii++)
{
	boolean priorShow = false;
	DBRow stCatalogs[] = (DBRow[])storageList.get(ii);
	if (stCatalogs.length==0)
	{
		continue;
	}
	else
	{
		for(int q = 0;q<stCatalogs.length;q++)
		{
			if(stCatalogs[q].get("id",0l)==ps_id)
			{
				priorShow = true;
			}
		}
	}
%>
	<tr>
	    <td align="center" valign="middle"  >
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		      <tr>
		        <td width="40%" align="center" valign="middle"><hr width="95%" size="1" style="color:#cccccc;"></td>
		        <td width="15%" align="center" valign="middle" style="color:#999999;font-weight:bold"><%=storageNameList.get(ii)%></td>
		        <td width="45%" align="center" valign="middle"><hr width="95%" size="1"  style="color:#cccccc;"></td>
		      </tr>
		    </table>
	    </td>
	</tr>
<%
	if(priorShow==true)
	{
		com.cwc.app.beans.PreCalcuOrderBean priorCalcuOrderBean = productMgr.preCalcuOrder(request,ps_id);
		DBRow priorOrderItems[] = priorCalcuOrderBean.getResult();
%>
		<tr>
			<td align="left" valign="top" style="padding:5px;">
				<fieldset id="fieldset_<%=ps_id%>" class="beSelect"">
					<legend style="font-size:15px;font-weight:bold;color:#333333;font-family:Arial, Helvetica, sans-serif;">
						<input type="radio" name="ps_id" id="ps_id_<%=ps_id%>" value="<%=ps_id%>" onClick="beSelected(<%=ps_id%>);" ondblclick="psSelect(this.value,<%=priorCalcuOrderBean.isOrderIsLacking()%>);">
		 				<label for="ps_id_<%=ps_id%>" style="cursor:hand">
						<%=catalogMgr.getDetailProductStorageCatalogById(ps_id).getString("title")%> 
						warehouse 
						</label>
						<%=priorCalcuOrderBean.isOrderIsLacking()?"<span style='color:red;font-size:12px;'><a style='text-decoration: none;' href='javascript:splitAdviceOrder("+ps_id+")'>[缺货]</a></span>":""%>
					</legend>
					<%
						for (int j=0; j<priorOrderItems.length; j++)
								{
										DBRow detailP;
										if (priorOrderItems[j].get("cart_product_type",0)!=ProductTypeKey.UNION_CUSTOM)
										{
											detailP = productMgr.getDetailProductByPcid(StringUtil.getLong(priorOrderItems[j].getString("cart_pid")));
										}
										else
										{
											detailP = productMgr.getDetailProductCustomByPcPcid(StringUtil.getLong(priorOrderItems[j].getString("cart_pid")));
										}
					%>
						<table width="97%" border="0" cellspacing="1" cellpadding="0">
						  <tr>
						    <td width="79%" style="font-weight:normal;font-family:Arial, Helvetica, sans-serif">
							<%
								if (priorOrderItems[j].get("lacking",0)==ProductStatusKey.STORE_OUT)
													{
														out.println("<span style='color:#FF0000;'> [缺]</span>");
													}
											
													if (priorOrderItems[j].get("cart_product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
													{
														out.println("<span style='color:#FF6600'><");
													}
							%>
							<%=detailP.getString("p_name")%>
							<%
								if (priorOrderItems[j].get("cart_product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
													{
														out.println("></span>");
													}
							%>	
							</td>
				    		<td width="21%"  style="font-family:Arial, Helvetica, sans-serif;color:#999999;font-weight:normal"><%=priorOrderItems[j].getString("cart_quantity")%> <%=detailP.getString("unit_name")%> </td>
				  		</tr>
					</table>
					<%
						}
					%>
				</fieldset>	
			</td>
		</tr>
	<%
		priorShow = false;
		}
		for (int i=0; i<stCatalogs.length; i++)
		{
			if(stCatalogs[i].get("id",0l)!=ps_id)
			{
		com.cwc.app.beans.PreCalcuOrderBean preCalcuOrderBean = productMgr.preCalcuOrder(request,stCatalogs[i].get("id",0l));
		DBRow orderItems[] = preCalcuOrderBean.getResult();
	%>
		<tr>
			<td align="left" valign="top" style="padding:5px;">
		 		<fieldset id="fieldset_<%=stCatalogs[i].getString("id")%>" class="<%=ps_id==stCatalogs[i].get("id",0l)?"beSelect":"unSelect"%>">
					<legend style="font-size:15px;font-weight:bold;color:#333333;font-family:Arial, Helvetica, sans-serif;">
						<input type="radio" name="ps_id" id="ps_id_<%=stCatalogs[i].getString("id")%>" value="<%=stCatalogs[i].getString("id")%>" onClick="beSelected(<%=stCatalogs[i].getString("id")%>);" ondblclick="psSelect(this.value,<%=preCalcuOrderBean.isOrderIsLacking()%>);">
						 
						<label for="ps_id_<%=stCatalogs[i].getString("id")%>" style="cursor:hand">
						<%=stCatalogs[i].getString("title")%> 
						warehouse
						</label>
						<%=preCalcuOrderBean.isOrderIsLacking()?"<span style='color:red;font-size:12px;'><a style='text-decoration: none;' href='javascript:splitAdviceOrder("+stCatalogs[i].getString("id")+")'>[缺货]</a></span>":""%>
					</legend>
					<%
					for (int j=0; j<orderItems.length; j++)
					{
						DBRow detailP;
						if (orderItems[j].get("cart_product_type",0)!=ProductTypeKey.UNION_CUSTOM)
						{
							detailP = productMgr.getDetailProductByPcid(StringUtil.getLong(orderItems[j].getString("cart_pid")));
						}
						else
						{
							detailP = productMgr.getDetailProductCustomByPcPcid(StringUtil.getLong(orderItems[j].getString("cart_pid")));
						}
					%>
					<table width="97%" border="0" cellspacing="1" cellpadding="0">
					  <tr>
					    <td width="79%" style="font-weight:normal;font-family:Arial, Helvetica, sans-serif">
						<%
							if (orderItems[j].get("lacking",0)==ProductStatusKey.STORE_OUT)
							{
								out.println("<span style='color:#FF0000;'> [缺]</span>");
							}
					
							if (orderItems[j].get("cart_product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
							{
								out.println("<span style='color:#FF6600'><");
							}
						%>
						<%=detailP.getString("p_name")%>
						<%
							if (orderItems[j].get("cart_product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
							{
								out.println("></span>");
							}
						%>	
						</td>
				    	<td width="21%"  style="font-family:Arial, Helvetica, sans-serif;color:#999999;font-weight:normal"><%=orderItems[j].getString("cart_quantity")%> <%=detailP.getString("unit_name")%> </td>
				 	  </tr>
					</table>
					<%
					}
					%>
				</fieldset>	
			</td>
 		</tr>
<%
		}
	}
}
%>

</table>
<script>
function recomExpress()
{
	$("#recom_express_tr").show();
}

function beSelected(ps_id)
{
var stCatalogs = "";
<%
DBRow stCatalogs[] = catalogMgr.getProductDevStorageCatalogByParentId(0,null);//不区分海外、当地仓库
for (int i=0; i<stCatalogs.length; i++)
{
%>
stCatalogs += "<%=stCatalogs[i].getString("id")%>,";
<%
}
%>

	document.getElementById("fieldset_"+ps_id).style.cssText = "border:2px #99CC00 solid;background:#F3FAE4;-webkit-border-radius:5px;-moz-border-radius:5px;";
	
	var stCatalogsA = stCatalogs.split(",");
	for (i=0; i<stCatalogsA.length; i++)
	{
		if (stCatalogsA[i]==""||ps_id==stCatalogsA[i]*1)
		{
			continue;
		}

		document.getElementById("fieldset_"+stCatalogsA[i]).style.cssText = "border:2px #cccccc solid;background:#FFFFFF;-webkit-border-radius:5px;-moz-border-radius:5px;";
	}
	
}
</script>
</body>

</html>