<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.OrderTaskKey"%>
<jsp:useBean id="orderTaskKey" class="com.cwc.app.key.OrderTaskKey"/>
<%@ page import="java.util.HashMap"%>
<%
long ot_id = StringUtil.getLong(request,"ot_id");
DBRow detail = taskMgr.getDetailOrderTaskByOtid(ot_id);

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>

<script>
function completeOrderTask()
{
	if ( confirm("确认完成任务？") )
	{
		document.task_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/completeOrderTask.action";
		document.task_form.submit();
	}
}

function unCompleteOrderTask()
{
	if (document.task_form.t_operation_note.value=="")
	{
		alert("请填写任务执行备注");
	}
	else
	{
		if ( confirm("确认不能完成任务？") )
		{
			document.task_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/unCompleteOrderTask.action";
			document.task_form.submit();
		}
	}
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<form action="" method="post" name="task_form" id="task_form">
<input type="hidden" name="ot_id" value="<%=ot_id%>">

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" style="color:#CC0000;font-weight:bold;font-size:15px;" ><%=detail.getString("t_operation")%></td>
  </tr>
</table>
<br>
<table width="95%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#999999">
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF">相关订单号</td>
    <td align="left" valign="middle" bgcolor="#FFFFFF" style='WORD-WRAP: break-word;TABLE-LAYOUT: fixed;word-break:break-all;padding-left:5px;'><%=detail.getString("oid")%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF">任务原因</td>
    <td width="83%" align="left" valign="middle" bgcolor="#FFFFFF" style="padding-left:5px;"><%=orderTaskKey.getReasonById(detail.get("t_reason",0))%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF">任务备注</td>
    <td align="left" valign="middle" bgcolor="#FFFFFF"  style="padding-left:5px;"><%=detail.getString("t_note")%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF">目标执行人</td>
    <td align="left" valign="middle" bgcolor="#FFFFFF"  style="padding-left:5px;">
		<%
	if (detail.get("t_target_admin",0)!=0)
	{
		DBRow detailAdmin = adminMgr.getDetailAdmin(detail.get("t_target_admin",0));
		if (detailAdmin==null)
		{
			out.println("NULL");
		}
		else
		{
			out.println(detailAdmin.getString("account"));
		}
	}
	else if (detail.get("t_target_group",0)!=0)
	{
		DBRow detailRole = adminMgr.getDetailAdminGroup(detail.get("t_target_group",0));
		if (detailRole==null)
		{
			out.println("NULL");
		}
		else
		{
			out.println(detailRole.getString("name")+"(角色)");
		}
	}
	%>	</td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF">创建任务人/日期</td>
    <td align="left" valign="middle" bgcolor="#FFFFFF"  style="padding-left:5px;"><%=detail.getString("create_admin")%> (<%=detail.getString("create_date")%>)</td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF">实际执行人/日期</td>
    <td align="left" valign="middle" bgcolor="#FFFFFF"  style="padding-left:5px;">
		<%
	if (detail.getString("t_operator").equals("")==false)
	{
		out.println(detail.getString("t_operator")+"("+detail.getString("t_operate_date")+")");
	}
	%>	</td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF">任务状态</td>
    <td align="left" valign="middle" bgcolor="#FFFFFF"  style="padding-left:5px;"><%=orderTaskKey.getStatusById(detail.get("t_status",0))%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF">任务执行备注</td>
    <td align="left" valign="middle" bgcolor="#FFFFFF" style="padding:5px;">
	<%
	if (detail.get("t_status",0)!=OrderTaskKey.T_STATUS_WAITING)
	{
		out.println(detail.getString("t_operation_note"));
	}
	else
	{
	%>
	<textarea name="t_operation_note" id="t_operation_note" style="width:500px;height:70px;"></textarea>
	<%
	}
	%>
   </td>
  </tr>
</table>
<%
if (detail.get("t_status",0)==OrderTaskKey.T_STATUS_WAITING)
{
%><br>


<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td width="46%" align="left" valign="middle">
      <input name="Submit" type="button" class="long-button-green" value="完成任务" onClick="completeOrderTask()">
	  &nbsp;&nbsp;&nbsp;&nbsp;
    <input name="Submit2" type="button" class="long-button-yellow" value="不能完成" onClick="unCompleteOrderTask()"> 
	&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td width="54%" align="left" valign="middle"><input type='button'  class="short-button-redtext" name='Submit22' value='关闭窗口'  id="close_tb_window" onClick="tb_remove();"/></td>
  </tr>
</table>
<%
}
%>
</form>
<br>
</body>
</html>
