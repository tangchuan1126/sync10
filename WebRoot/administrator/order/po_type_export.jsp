<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="../../include.jsp" %>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<%@page import="com.cwc.app.key.YesOrNotKey" %>
<%@ page language="java" pageEncoding="UTF8"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/>

 <%
        String serverName = request.getServerName();
        AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
        boolean isadministrator = adminLoggerBean.isAdministrator();  //判断当前登录帐号是不是admin.如果是返回true
        long psId = StringUtil.getLong(request, "ps");//当前账号所属仓库id
        if (psId == 0l) {
            psId = adminLoggerBean.getPs_id();
        }
        DBRow[] rows = checkInMgrZwb.findPOTypeOfDN(psId);
       
        response.setHeader("Content-disposition","attachment; filename="+psId+".xls");
    %>

<html>
<head>
 
</head>
<body>

<table width="100%" border="1" align="center" cellpadding="2" cellspacing="1" style="font-size:12px; border-collapse:collapse;border-spacing:0;border-left:1px solid #888;border-top:1px solid #888;;background:#F0F0F0;">
<tr  class="list" style="background-color:#CCCCCC; line-height:23px">
       <th  class="left-title" style="vertical-align:center;text-align:center;height:23px;">Company ID</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">Order#</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">Load#</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">DN#</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">Account ID</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">PO Type</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">PO#</th>
       
   </tr> 
<%
	if (rows != null && rows.length > 0) {
         for (DBRow row : rows) {%>
         	 <tr style="line-height:23px;">
         	 	    <td style="text-align:center;height:21px;">
			  			<span style="font-size: 12px;bold;font-family:Arial;"><%=row.getString("companyID") %></span>
		  			</td>
		  			 <td   style="text-align:center">
			  			<span style="font-size: 12px;font-family:Arial;text-align:center;"><%=row.getString("orderNo") %></span>
		  			</td>
		  			 <td  style="text-align:center">
			  			<span style="font-size: 12px;font-family:Arial;text-align:center;"><%=row.getString("loadNo") %></span>
		  			</td>
		  			 <td   style="text-align:center">
			  			<span style="font-size: 12px;font-family:Arial;text-align:center;"><%=row.getString("referenceNo") %></span>
		  			</td>
		  			 <td   style="text-align:center">
			  			<span style="font-size: 12px;font-family:Arial;"><%=row.getString("accountID") %></span>
		  			</td>
		  			 <td  style="text-align:center">
			  			<span style="font-size: 12px;font-family:Arial;"><%=row.getString("external_id") %></span>
		  			</td>

            <td  style="text-align:center">
              <span style="font-size: 12px;font-family:Arial;"><%=row.getString("PONo") %></span>
            </td>
         	 </tr>

<%       }   
	} 
%>
	
</body>

</html>
