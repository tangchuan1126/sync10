<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.shipping.ShippingInfoBean"%>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
 	PageCtrl pc = new PageCtrl();
  	
  	String cmd = StringUtil.getString(request,"cmd");
  	long areaId = StringUtil.getLong(request,"ca_id"); 
 	long productCatalog = StringUtil.getLong(request,"filter_acid");                                        
 	long productLine = StringUtil.getLong(request,"productLineId");
 	String pName = StringUtil.getString(request,"productName");
 	double discountRate = StringUtil.getDouble(request,"discount_rate");
 	long productStorageId = StringUtil.getLong(request,"cacul_ps_id");//仓库ID
 	long caculScId = StringUtil.getLong(request,"cacul_sc_id");
 	float startWT = StringUtil.getFloat(request,"startWT");
 	float endWT = StringUtil.getFloat(request,"endWT");
 	float stepWT = StringUtil.getFloat(request,"stepWT");
 	String flag = StringUtil.getString(request,"show_transport_cost");
 	String address = StringUtil.getString(request,"address");
 	
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 	DBRow wholesellDiscount = quotePriceMgrZZZ.getWholesellDiscountByName();
 	String[][] rule = quoteMgr.getWholeSellRule2(wholesellDiscount.getString("discount_policy"));
 	String defaultValue="";
 	
 	if(discountRate!=0.0)
 		defaultValue=String.valueOf(discountRate);
 		
  	DBRow row[] = null;
  	List productList = null;
  	List transportCosts = null;
  	HashMap QuotedMap = null;
  	List catalogs = null;
  	
 	DBRow countryAreas[] = productMgr.getAllCountryAreas(null);
  	DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();
  		
  	if("productLine".equals(cmd))
  	{
  		productList = quotePriceMgrZZZ.getQuoPriceByProductLine(request,false);
  		QuotedMap = (HashMap)productList.get(0);
  		catalogs = (ArrayList)productList.get(1);
  	}
  	else if("productCatalog".equals(cmd))
  	{
  		productList = quotePriceMgrZZZ.getQuotedPriceByProductCatelog(request,false);
  		QuotedMap = (HashMap)productList.get(0);
  		catalogs = (ArrayList)productList.get(1);
  	}
  	else if("productName".equals(cmd))
  	{
  		productList = quotePriceMgrZZZ.getQuoPriceByProductName(request);
  		QuotedMap = (HashMap)productList.get(0);
  		catalogs = (ArrayList)productList.get(1);
  	}
  	
  	if("show".equals(flag))
  	{
  		transportCosts = quotePriceMgrZZZ.createTransportCost(caculScId,areaId,startWT,endWT,stepWT);
  	}
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>


<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />

<style>




.zebraTable tr td a:visited {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:hover {
	color: #000000;
	text-decoration: underline;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:active {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>

<script type="text/javascript">
<!--
  $(document).ready(function(){
  //自动补全初始化
  $("#productName").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	$("#productName").keydown(function(event){
		if (event.keyCode==13)
		{
			search();
		}
	});
  //初始化选项卡
  $("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		select: function(event, ui) {		
			if(ui.index==0)
			{
				showTransportCost("#show_transport_cost","#transport_cost_hide");
			}
			else if(ui.index==1)
			{
				showTransportCost("#show_transport_cost1","#transport_cost_hide1");
			}
			else if(ui.index==2)
			{
				showTransportCost("#show_transport_cost2","#transport_cost_hide2");
			}
			return true;
	    }
	});

	
	//初始化选项卡
  $("#price_tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	
  //产品分类下拉列表初始化
  $("#category").mcDropdown("#categorymenu",{
		allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{					
					$("#filter_acid").val(id);
				}
});
$("#category").mcDropdown("#categorymenu").setValue(0);
//产品线下拉列表初始化	
 $("#productLine").mcDropdown("#productLineMenu",{
		allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{					
					$("#productLineId").val(id);
				}
});
$("#productLine").mcDropdown("#productLineMenu").setValue(0);
initForm();
  });
  
  function initForm()
  {
  	<%
  	
  		
  if("productLine".equals(cmd))
 	{
 		%>
 			
 		$("#productLine").mcDropdown("#productLineMenu").setValue(<%=productLine%>);
		$("#ca_id").val("<%=areaId%>");
		$("#discount_rate").val("<%=defaultValue%>");
		
		if("<%=flag%>"=="show")
		{
			$("#show_transport_cost").attr("checked","checked");
		}
		showTransportCost("#show_transport_cost","#transport_cost_hide");
		
		$("#cacul_ps_id").val("<%=productStorageId%>");
		changeDevStorage("<%=productStorageId%>","#cacul_sc_id","Y");
		$("#startWT").val("<%=startWT%>");
		$("#endWT").val("<%=endWT%>");
		$("#stepWT").val(<%=stepWT%>);
		
 		<%
 	}
 	else if("productCatalog".equals(cmd))
 	{
 		%>
 		
 		$("#category").mcDropdown("#categorymenu").setValue(<%=productCatalog%>);
 		$("#ca_id1").val("<%=areaId%>");
 		$("#discount_rate1").val("<%=defaultValue%>");
 		
 		if("<%=flag%>"=="show")
		{
			$("#show_transport_cost1").attr("checked","checked");
		}
		showTransportCost("#show_transport_cost1","#transport_cost_hide1");
		
		$("#cacul_ps_id1").val("<%=productStorageId%>");
		changeDevStorage("<%=productStorageId%>","#cacul_sc_id1","Y");
		$("#startWT1").val("<%=startWT%>");
		$("#endWT1").val("<%=endWT%>");
		$("#stepWT1").val(<%=stepWT%>);
		
 		<%
 	}
 	else if("productName".equals(cmd))
 	{
 		%>
 		$("#productName").val("<%=pName%>");
		$("#ca_id2").val("<%=areaId%>"); 		
 		$("#discount_rate2").val("<%=defaultValue%>");
 		
 		if("<%=flag%>"=="show")
		{
			$("#show_transport_cost2").attr("checked","checked");
		}
		showTransportCost("#show_transport_cost2","#transport_cost_hide2");
		
		$("#cacul_ps_id2").val("<%=productStorageId%>");
		changeDevStorage("<%=productStorageId%>","#cacul_sc_id2","Y");
		$("#startWT2").val("<%=startWT%>");
		$("#endWT2").val("<%=endWT%>");
		$("#stepWT2").val(<%=stepWT%>);
 		<%
 	}
  	%>
  }
  
  
  //校验产品线生成报价的表单
 function checkProductLineForm()
 {
 	if($("#productLineId").val()==0||$("#productLineId").val().trim()=="0")
	{
	 	alert("请选择产品线！");
	 	return false;
	}
 
 	if($("#ca_id").val()==0||$("#ca_id").val().trim()=="0")
 	{
 		alert("请选择销售区域！");
 		return false;
 	}
 	
	if($("#discount_rate").val().trim()!=""&&$("#discount_rate")!=null)
	{
		if(isNaN($("#discount_rate").val()))
		{
			alert("打折率必须为数字！");
			$("#discount_rate").focus();
			return false;
		}
		else
		{
			if($("#discount_rate").val()<0||$("#discount_rate").val()>1)
			{
				alert("打折率必须在0~1之间！");
				$("#discount_rate").focus();
				return false;
			}
		}
	}
	
	if($("#show_transport_cost").attr("checked"))
	{
		if($("#cacul_ps_id").val()==0||$("#cacul_ps_id").val()=="0")
		{
			alert("请选择发货仓库！");
			return false;
		}
		if($("#cacul_sc_id").val()==0||$("#cacul_sc_id").val()=="0")
		{
			alert("请选择发货快递！");
			return false;
		}
		if($("#startWT").val().trim()==""||$("#startWT").val().trim()==null)
		{
			alert("请输入起始重量！");
			$("#startWT").focus();
			return false;
		}
		else if(isNaN($("#startWT").val().trim()))
		{
			alert("请输入数字！");
			$("#startWT").focus();
			return false;
		}
		else if($("#startWT").val()<0)
		{
			alert("起始重量必须大于等于零！");
			$("#startWT").focus();
			return false;
		}
		if($("#endWT").val().trim()==""||$("#endWT").val().trim()==null)
		{
			alert("请输入结束重量！");
			$("#endWT").focus();
			return false;
		}else if(isNaN($("#endWT").val().trim()))
		{
			alert("请输入数字！");
			$("#endWT").focus();
			return false;
		}
		else if(parseFloat($("#endWT").val())<=parseFloat($("#startWT").val()))
		{
			alert("结束重量必须大于起始重量！");
			$("#endWT").focus();
			return false;
		}
		if($("#stepWT").val().trim()==""||$("#stepWT").val().trim()==null)
		{
			alert("请输入重量步长！");
			$("#stepWT").focus();
			return false;
		}else if(isNaN($("#stepWT").val().trim()))
		{
			alert("请输入数字！");
			$("#stepWT").focus();
			return false;
		}
		else if(parseFloat($("#stepWT").val().trim())<=0)
		{
			alert("重量步长必须大于零！");
			$("#stepWT").focus();
			return false;
		}
		else if(parseFloat($("#stepWT").val().trim())>parseFloat($("#endWT").val().trim())-parseFloat($("#startWT").val().trim()))
		{
			alert("重量步长不能大于结束重量和起始重量之差！");
			$("#stepWT").focus();
			return false;
		}
	} 	
	$("#address").val($("#ca_id").getSelectedText());
 	return true;
 } 
 //校验产品分类生成报价的表单
 function checkProductCatalogForm()
 {
 	if($("#filter_acid").val()==0||$("#filter_acid").val().trim()=="0")
	{
	 	alert("请选择产品分类！");
	 	return false;
	}
 	if($("#ca_id1").val()==0||$("#ca_id1").val().trim()=="0")
 	{
 		alert("请选择销售区域！");
 		return false;
 	}
 
	if($("#discount_rate1").val().trim()!=""&&$("#discount_rate1")!=null)
	{
		if(isNaN($("#discount_rate1").val()))
		{
			alert("打折率必须为数字！");
			$("#discount_rate1").focus();
			return false;
		}
		else
		{
			if($("#discount_rate1").val()<0||$("#discount_rate1").val()>1)
			{
				alert("打折率必须在0~1之间！");
				$("#discount_rate1").focus();
				return false;
			}
		}
	} 
	
	if($("#show_transport_cost1").attr("checked"))
	{
		if($("#cacul_ps_id1").val()==0||$("#cacul_ps_id1").val()=="0")
		{
			alert("请选择发货仓库！");
			return false;
		}
		if($("#cacul_sc_id1").val()==0||$("#cacul_sc_id1").val()=="0")
		{
			alert("请选择发货快递！");
			return false;
		}
		if($("#startWT1").val().trim()==""||$("#startWT1").val().trim()==null)
		{
			alert("请输入起始重量！");
			$("#startWT1").focus();
			return false;
		}
		else if(isNaN($("#startWT1").val().trim()))
		{
			alert("请输入数字！");
			$("#startWT1").focus();
			return false;
		}
		else if($("#startWT1").val()<0)
		{
			alert("起始重量必须大于等于零！");
			$("#startWT1").focus();
			return false;
		}
		if($("#endWT1").val().trim()==""||$("#endWT1").val().trim()==null)
		{
			alert("请输入结束重量！");
			$("#endWT1").focus();
			return false;
		}else if(isNaN($("#endWT1").val().trim()))
		{
			alert("请输入数字！");
			$("#endWT1").focus();
			return false;
		}
		else if(parseFloat($("#endWT1").val())<=parseFloat($("#startWT1").val()))
		{
			alert("结束重量必须大于起始重量！");
			$("#endWT1").focus();
			return false;
		}
		if($("#stepWT1").val().trim()==""||$("#stepWT1").val().trim()==null)
		{
			alert("请输入重量步长！");
			$("#stepWT1").focus();
			return false;
		}else if(isNaN($("#stepWT1").val().trim()))
		{
			alert("请输入数字！");
			$("#stepWT1").focus();
			return false;
		}
		else if($("#stepWT1").val()<0)
		{
			alert("重量步长必须大于等于零！");
			$("#stepWT1").focus();
			return false;
		}
		else if(parseFloat($("#stepWT1").val())>parseFloat($("#endWT1").val().trim())-parseFloat($("#startWT1").val()))
		{
			alert("重量步长不能大于结束重量和起始重量之差！");
			$("#stepWT1").focus();
			return false;
		}
	} 		
	$("#address1").val($("#ca_id1").getSelectedText());
 	return true;
 } 
 //根据产品名称生成报价的表单
 function checkPNameForm()
 {
 	if($("#productName").val()==null||$("#productName").val().trim()=="")
 	{
 		alert("请输入产品名称！");
 		$("#productName").focus();
 		return false;
 	}
  	if($("#ca_id2").val()=="0"||$("#ca_id2").val().trim()==0)
	{
	 	alert("请选择销售区域！");
	 	return false;
	}
	if($("#discount_rate2").val().trim()!=""&&$("#discount_rate1")!=null)
	{
		if(isNaN($("#discount_rate1").val()))
		{
			alert("打折率必须为数字！");
			return false;
		}
		else
		{
			if($("#discount_rate1").val()<0||$("#discount_rate1").val()>1)
			{
				alert("打折率必须在0~1之间！");
				return false;
			}
		}
	} 
	
	if($("#show_transport_cost2").attr("checked"))
	{
				
		if($("#cacul_ps_id2").val()==0||$("#cacul_ps_id2").val()=="0")
		{
			alert("请选择发货仓库！");
			return false;
		}
		if($("#cacul_sc_id2").val()==0||$("#cacul_sc_id2").val()=="0")
		{
			alert("请选择发货快递！");
			return false;
		}
		if($("#startWT2").val().trim()==""||$("#startWT2").val().trim()==null)
		{
			alert("请输入起始重量！");
			$("#startWT2").focus();
			return false;
		}
		else if(isNaN($("#startWT2").val().trim()))
		{
			alert("请输入数字！");
			$("#startWT2").focus();
			return false;
		}
		else if($("#startWT2").val().trim()<0)
		{
			alert("起始重量必须大于等于零！");
			$("#startWT2").focus();
			return false;
		}
		if($("#endWT2").val().trim()==""||$("#endWT2").val().trim()==null)
		{
			alert("请输入结束重量！");
			$("#endWT2").focus();
			return false;
		}else if(isNaN($("#endWT2").val().trim()))
		{
			alert("请输入数字！");
			$("#endWT2").focus();
			return false;
		}
		else if(parseFloat($("#endWT2").val().trim())<parseFloat($("#startWT2").val().trim()))
		{
			
			alert("结束重量必须大于起始重量！");
			$("#endWT2").focus();
			return false;
		}
		if($("#stepWT2").val().trim()==""||$("#stepWT2").val().trim()==null)
		{
			alert("请输入重量步长！");
			$("#stepWT2").focus();
			return false;
		}else if(isNaN($("#stepWT2").val().trim()))
		{
			alert("请输入数字！");
			$("#stepWT2").focus();
			return false;
		}
		else if($("#stepWT2").val().trim()<=0)
		{
			alert("重量步长必须大于零！");
			$("#stepWT2").focus();
			return false;
		}
		else if(parseFloat($("#stepWT2").val().trim())>parseFloat($("#endWT2").val().trim())-parseFloat($("#startWT2").val().trim()))
		{
			alert("重量步长不能大于结束重量和起始重量之差！");
			$("#stepWT2").focus();
			return false;
		}
	} 	
	$("#address2").val($("#ca_id2").getSelectedText());
 	return true;
 
 }

function changeDevStorage(ps_id,cacul_sc_id,isLoad)
{
	$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getExpressCompanysByPsIdJSON.action",
		{ps_id:ps_id},
		function callback(data)
		{   
			$(cacul_sc_id).clearAll();
			$(cacul_sc_id).addOption("请选择......","0");
										
			if (data!="")
			{
				$.each(data,function(i)
				{
					var preStr;
					if (data[i].domestic==1)
					{
						preStr = " [国内]";
					}
					else
					{
						preStr = " <国际>";
					}
					$(cacul_sc_id).addOption(data[i].name+preStr,data[i].sc_id);
				});
				if(isLoad.trim()=="Y")
				{
					$(cacul_sc_id).setSelectedValue("<%=caculScId%>");
				}
			}
			
		}
	);
}
//复选框选中显示运费生成条件
function showTransportCost(show_transport_cost,transport_cost_hide)
{	
	
	if($(show_transport_cost).attr("checked"))
	{
		$(transport_cost_hide).show();
	}
	else
	{
		$(transport_cost_hide).hide();
	}

}

function pageCtrlGo(formName,pName,p,i)
{
	
	$(pName).val(p);
	for(var j=0;j<i;j++)
	{
		var pid="#p"+j;
		var catalog="#catalog"+j;
		$(pName).after("<input type='hidden' name='pValue' value='"+$(catalog).val()+","+$(pid).val()+"'/>");
	}
	$(formName).submit();
}

function exportQuote(cmd)
{	
	if(cmd=="productLine")
	{
		
		$("#productLineId1").val($("#productLineId").val());
		$("#ca_id3").val($("#ca_id").val());
		$("#ca_name").val($("#ca_id option:selected").text());
		$("#discount_rate3").val($("#discount_rate").val());
		$("#cmd1").val(cmd);		
		if(checkProductLineForm())
		{
			$("#productQuotedForm").submit();
		}
	}
	else if(cmd=="productCatalog")
	{
		$("#ca_id3").val($("#ca_id1").val());
		$("#discount_rate3").val($("#discount_rate1").val());
		$("#filter_acid1").val($("#filter_acid").val());
		$("#ca_name").val($("#ca_id1 option:selected").text());
		$("#cmd1").val(cmd);
		if(checkProductCatalogForm())
		{
			$("#productQuotedForm").submit();
		}
	}
	else if(cmd=="productName")
	{
		$("#productName1").val($("#productName").val());
		$("#ca_id3").val($("#ca_id2").val());
		$("#discount_rate3").val($("#discount_rate1").val());
		$("#ca_name").val($("#ca_id2 option:selected").text());
		$("#cmd1").val(cmd);
		if(checkPNameForm())
		{
			$("#productQuotedForm").submit();
		}
		
	}
}

//-->
</script>
</head>

<body onLoad="onLoadInitZebraTable();">		
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" style="padding-top: 5px">
  <tr>
    <td width="80%" class="page-title"  align="left"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle"/>&nbsp;&nbsp; 客户与报价应用 »   生成报价单 </td>
  </tr>
</table>
<form id="productQuotedForm"  action="<%=ConfigBean.getStringValue("systenFolder")%>action/order/ExportProductQuotePrecessAction.action">
	<input type="hidden" id="productLineId1" name="productLineId"/>
	<input type="hidden" name="ca_id" id="ca_id3" />
	<input type="hidden" id="discount_rate3" name="discount_rate" />
	<input type="hidden" id="filter_acid1" name="filter_acid" />
	<input type="hidden" id="productName1" name="productName"/>
	<input type="hidden" id="ca_name" name="ca_name" />
	<input type="hidden" id="userName" name="userName" value="<%=adminLoggerBean.getAccount()%>"/>
	<input type="hidden" id="cmd1" name="cmd" />
</form>
<br/>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	<div class="demo" >

	<div id="tabs">
	<ul>
		<li><a href="#seachByPLine">产品线</a></li>
		<li><a href="#productCatalog">产品分类</a></li>
		<li><a href="#seachByPName">产品名称</a></li>		 
	</ul>
<div id="seachByPLine" >
<form onSubmit="return checkProductLineForm()" action="quoted_price_table.html">	
	<input type="hidden" name="cmd" value="productLine"/>
	   <table width="100%" height="60" border="0" cellpadding="1" cellspacing="0">
		  
		  <tr>		   
		     <td width="40%" height="35" align="left" valign="middle" nowrap="nowrap">
			    <ul id="productLineMenu" class="mcdropdown_menu">
			  <li rel="0">请选择产品线</li>
			  <%
			  DBRow pl[] = quotePriceMgrZZZ.getProductLine();
			  for (int i=0; i<pl.length; i++)
			  {
					out.println("<li rel='"+pl[i].get("id",0l)+"'> "+pl[i].getString("name"));
					out.println("</li>");
			  }
			  %>
			</ul>
		    <input type="text" name="productLine" id="productLine" value="" />
		    <input type="hidden" name="productLineId" id="productLineId" value="0"  />
		      </td>
		      
		      <td valign="middle" align="left" nowrap="nowrap" width="20%" style="padding-left: 5px">		      			              
			  </td>
			  <td width="40%"></td>
		  	</tr>
		  	<tr height="10px"><td></td><td></td><td></td></tr>
		  	<tr>
		  	  <td nowrap="nowrap"> 
		  	  销售区域:
		  	   <select name="ca_id" id="ca_id" >
					<option value="0">请选择...</option>
					<%
					
					for (int i=0; i<countryAreas.length; i++)
					{
					%>
					<option value="<%=countryAreas[i].get("ca_id",0l)%>"><%=countryAreas[i].getString("name")%></option>
					<%
					}
					%>
					<input type="hidden" name="address" id="address" />
		    	</select>&nbsp;&nbsp;
		  利润打折率:
		  	    <input type="text" id="discount_rate" name="discount_rate" size="3" />
		  	    &nbsp;&nbsp;
		  	  <input name="submit" type="submit" class="button_long_refresh" value="生成报价" />
		  	  &nbsp;&nbsp;
		  	  <input type="button" onclick="exportQuote('productLine')" class="button_long_refresh" value="导出报价" />
			</td>
		  	  <td align="left" valign="middle" nowrap="nowrap" style="padding-left: 5px"> 
		  	  </td>
		  	  <td align="left" valign="middle" nowrap="nowrap">
		  	  </td>
		  	</tr>
		  	<tr>
		  		<td>运费:<input type="checkbox" onclick="showTransportCost('#show_transport_cost','#transport_cost_hide')" id="show_transport_cost" name="show_transport_cost" value="show"/></td>
		  		<td></td>
		  		<td></td>		  		
		  	</tr>
		  	<tr >
		  		<td rowspan="3">
		  		<div id="transport_cost_hide" style="display:none;">
			  		<table>
				  		<tr>
					  		<td nowrap="nowrap">
					  		发货仓库
								&nbsp;&nbsp;
					  			<select name="cacul_ps_id" id="cacul_ps_id" onChange="changeDevStorage(this.value,'#cacul_sc_id','no')">
									<option value="0">请选择仓库</option>
							            <%
										String qx;
										
										for ( int i=0; i<treeRows.length; i++ )
										{
											if ( treeRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
										%>
										          <option value="<%=treeRows[i].getString("id")%>" > 
										          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										          <%=qx%>
										          <%=treeRows[i].getString("title")%>          </option>
										          <%
										}
										%>
							     </select>
								 &nbsp;&nbsp;
								 发货快递
								&nbsp;&nbsp;
							      <select name="cacul_sc_id" id="cacul_sc_id">
							        <option value="0">请选择......</option>
							      </select>
					  		</td>
					  		<td nowrap="nowrap" style="padding-left: 10px;">
					  		起始重量(Kg)<input id="startWT" name="startWT"/>~结束 重量(Kg)<input id="endWT" name="endWT"/>&nbsp;&nbsp;重量步长(Kg)<input id="stepWT" name="stepWT" />
					  		</td>
				  		</tr>
			  		</table>
		  		</div>
		  		</td>
		  	</tr>
			</table>
			</form>
 		</div>
 		<div id="productCatalog">
 		<form onSubmit="return checkProductCatalogForm()" action="quoted_price_table.html">	
 			<input type="hidden" name="cmd" value="productCatalog"/>
	   <table width="100%" height="60" border="0" cellpadding="1" cellspacing="0">
		  
		  <tr>		   
		     <td width="40%" height="35" align="left" valign="middle" nowrap="nowrap">
				
		  	  <ul id="categorymenu" class="mcdropdown_menu">
				  <li rel="0">产品类别</li>
				  <%
				  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
	  for (int i=0; i<c1.length; i++)
	  {
			out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));

			  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
			  if (c2.length>0)
			  {
			  		out.println("<ul>");	
			  }
			  for (int ii=0; ii<c2.length; ii++)
			  {
					out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
					
						DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
						  if (c3.length>0)
						  {
								out.println("<ul>");	
						  }
							for (int iii=0; iii<c3.length; iii++)
							{
									out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
									out.println("</li>");
							}
						  if (c3.length>0)
						  {
								out.println("</ul>");	
						  }
						  
					out.println("</li>");				
			  }
			  if (c2.length>0)
			  {
			  		out.println("</ul>");	
			  }
			  
			out.println("</li>");
	  }
				  %>
			 </ul>
			  <input type="text" name="category" id="category" value="" />
			  <input type="hidden" name="filter_acid" id="filter_acid" value="0"  />
					
			  </td>
			  <td valign="middle" align="left" nowrap="nowrap" width="40%">
			  
			  </td>
			  <td></td>
		  	</tr>
		  	<tr height="10px"><td></td><td></td><td></td></tr>
		  	<tr>
		  	  <td nowrap="nowrap"> 
		  	    销售区域:
		  	   <select name="ca_id" id="ca_id1" >
					<option value="0">请选择...</option>
					<%
					for (int i=0; i<countryAreas.length; i++)
					{
					%>
					<option value="<%=countryAreas[i].get("ca_id",0l)%>"><%=countryAreas[i].getString("name")%></option>
					<%
					}
					%>
					<input type="hidden" name="address" id="address1" />
		    	</select>&nbsp;&nbsp;
		  利润打折率:
		  	    <input type="text" id="discount_rate1" name="discount_rate" size="3" />
		  	    &nbsp;&nbsp;
		  	  <input name="submit" type="submit" class="button_long_refresh" value="生成报价" />
		  	  &nbsp;&nbsp;
		  	  <input type="button" onclick="exportQuote('productCatalog')" class="button_long_refresh" value="导出报价" />
		      </td>
		      <td valign="middle" align="left" nowrap="nowrap" width="20%" style="padding-left: 5px" >		  	 	
		  	  	</td>
		  	  <td valign="middle" align="left" style="padding-left: 5px"> 
			  </td>
			
		  	</tr>
		  	<tr>
		  		<td>运费:<input type="checkbox"  onclick="showTransportCost('#show_transport_cost1','#transport_cost_hide1')" id="show_transport_cost1" name="show_transport_cost" value="show"/></td>
		  		<td></td>
		  		<td></td>		  		
		  	</tr>
		  	<tr >
		  		<td rowspan="3">
		  		<div id="transport_cost_hide1" style="display:none;">
			  		<table>
				  		<tr>
					  		<td nowrap="nowrap">
					  		发货仓库
								&nbsp;&nbsp;
					  			<select name="cacul_ps_id" id="cacul_ps_id1" onChange="changeDevStorage(this.value,'#cacul_sc_id1','no')">
									<option value="0">请选择仓库</option>
							            <%
										
										
										for ( int i=0; i<treeRows.length; i++ )
										{
											if ( treeRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
										%>
										          <option value="<%=treeRows[i].getString("id")%>" > 
										          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										          <%=qx%>
										          <%=treeRows[i].getString("title")%>          </option>
										          <%
										}
										%>
							     </select>
								 &nbsp;&nbsp;
								 发货快递
								&nbsp;&nbsp;
							      <select name="cacul_sc_id" id="cacul_sc_id1">
							        <option value="0">请选择......</option>
							      </select>
					  		</td>
					  		<td nowrap="nowrap" style="padding-left: 10px;">
					  		起始重量(Kg)<input id="startWT1" name="startWT"/>~结束重量(Kg)<input id="endWT1" name="endWT"/>&nbsp;&nbsp;重量步长(Kg)<input id="stepWT1" name="stepWT" />
					  		</td>
				  		</tr>
			  		</table>
		  		</div>
		  		</td>
		  	</tr>
			</table>
			</form>
 		</div>
 		<div id="seachByPName">
 	  <form onSubmit="return checkPNameForm()" action="quoted_price_table.html">	
	   <input type="hidden"  name="cmd" value="productName"/>
	   <table width="100%" height="60" border="0" cellpadding="1" cellspacing="0">
		  <tr>		   
		      <td valign="bottom" align="left" nowrap="nowrap" width="40%">
		      		产品名称:<input type="text" id="productName"  size="35" name="productName" value="<%=pName%>"/>
				
		      </td>
		      <td></td><td></td>
		  	</tr>
		  	<tr height="10px"><td></td><td></td><td></td></tr>
		  	<tr>
		  	  <td  nowrap="nowrap"> 
		  	    销售区域:
		  	   <select name="ca_id" id="ca_id2" >
					<option value="0">请选择...</option>
					<%
					for (int i=0; i<countryAreas.length; i++)
					{
					%>
					<option value="<%=countryAreas[i].get("ca_id",0l)%>"><%=countryAreas[i].getString("name")%></option>
					<%
					}
					%>
					<input type="hidden" name="address" id="address2" />
		    	</select>&nbsp;&nbsp;
		  利润打折率:
		  	    <input type="text" id="discount_rate2" name="discount_rate" size="3" />
		  	    &nbsp;&nbsp;
		  	  <input name="submit" type="submit" class="button_long_refresh" value="生成报价" />
		  	   &nbsp;&nbsp;
		  	  <input type="button" onclick="exportQuote('productName')" class="button_long_refresh" value="导出报价" />	 
			 </td>
			  <td width="60%" height="35" align="left" valign="middle" nowrap="nowrap" style="padding-left: 5px" >
			  </td>
		  	  <td style="padding-left: 5px" nowrap="nowrap">
		  	  	
			  </td>
		  	</tr>
		  	<tr>
		  		<td>运费:<input type="checkbox" onclick="showTransportCost('#show_transport_cost2','#transport_cost_hide2')" id="show_transport_cost2" value="show" name="show_transport_cost" /></td>
		  		<td></td>
		  		<td></td>		  		
		  	</tr>
		  	<tr >
		  		<td rowspan="3">
		  		<div id="transport_cost_hide2" style="display:none;">
			  		<table>
				  		<tr>
					  		<td nowrap="nowrap">
					  		 发货仓库
								&nbsp;&nbsp;
					  			<select name="cacul_ps_id" id="cacul_ps_id2" onChange="changeDevStorage(this.value,'#cacul_sc_id2','no')">
									<option value="0">请选择仓库</option>
							            <%
										
										
										for ( int i=0; i<treeRows.length; i++ )
										{
											if ( treeRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
										%>
										          <option value="<%=treeRows[i].getString("id")%>" > 
										          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										          <%=qx%>
										          <%=treeRows[i].getString("title")%>          </option>
										          <%
										}
										%>
							     </select>
								 &nbsp;&nbsp;
								 发货快递
								&nbsp;&nbsp;
							      <select name="cacul_sc_id" id="cacul_sc_id2">
							        <option value="0">请选择......</option>
							      </select>
					  		</td>
					  		<td nowrap="nowrap" style="padding-left: 10px;">
					  		起始重量(Kg)<input id="startWT2" name="startWT"/>~结束重量(Kg)<input id="endWT2" name="endWT"/>&nbsp;&nbsp;重量步长(Kg)<input id="stepWT2" name="stepWT" />
					  		</td>
				  		</tr>
			  		</table>
		  		</div>
		  		</td>
		  	</tr>
			</table>
			</form>
 		</div>
 	</div>
 </div>
	</td>
  </tr>
</table>
<br/>


<div id="productList">

<div class="demo" >

	<div id="price_tabs">
	<ul>				
		<%
		if(catalogs!=null)
		{
			for(int i = 0;i<catalogs.size();i++)
			{
			%>
				<li><a href="#<%="catalogs"+i %>"><%=catalogs.get(i).toString() %>报价</a></li>
			<%
			}
		}
		else
		{
			%>
				<li><a href="#default">商品报价</a></li>
			<%
		}
		if("show".equals(flag))
		{
		 %>
		<li><a href="#transport_cost">运费</a></li>
		<%
		}
		 %>			 
	</ul>
	<%

if(catalogs!=null)
{
	for(int j = 0;j<catalogs.size();j++)
	{
		String mapKey = catalogs.get(j).toString()+"1";
		pc = (PageCtrl)QuotedMap.get(mapKey);
		boolean noType = true;
	 %>
	 <input type="hidden" id="catalog<%=j %>" value="<%=catalogs.get(j).toString() %>"/>
	<div id="<%="catalogs"+j %>">
  <table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" >
     <tr>
       <th class="right-title" width="18%"  style="vertical-align: center;text-align: center;">产品名称</th>
       <th class="right-title" width="10%"  style="vertical-align: center;text-align: center;">单位重量</th>
       <th class="right-title" width="10%" style="vertical-align: center;text-align: center;">产品零售价</th>
       <%
       
       for(String[] ruleDetail:rule)
       {
       	 if(ruleDetail[3]!=null)
       	 {     
	       	   if(ruleDetail[3].equals(catalogs.get(j).toString()))
	       	   {
	       	   		noType = false;
	           %>
	       <th class="right-title" style="vertical-align: center;text-align: center;">$<%=ruleDetail[1] %>~$<%=ruleDetail[2] %></th>
	           <%
	           }
         }         
       }
       
       if(noType)
       {
       		 for(String[] ruleDetail:rule)
	         {
		       	 if(ruleDetail[3]==null)
		       	 {     			       	  
			           %>
			       <th class="right-title" style="vertical-align: center;text-align: center;">$<%=ruleDetail[1] %>~$<%=ruleDetail[2] %></th>
			           <%			      
		         }         
      		 }
       }
        %>
     </tr>

     <%
    
     if(QuotedMap!=null)
     {
     
     	DBRow productQuoted[] = (DBRow[])QuotedMap.get(catalogs.get(j).toString());
	     for(int i = 0;i<productQuoted.length;i++)
	     {	     		   	
	      %>
	     <tr align="center" valign="middle" height="30px" >
		    <td align="left">
		    	<%=productQuoted[i].getString("p_name") %>
		    </td>
		    <td> 
		    	<%=productQuoted[i].get("weight",0f) %>Kg
		    </td>
		    <td>
		    $<%=productQuoted[i].getString("unit_price") %>
		    </td>	 
		    <%
		    
		    for(int k=0;k<rule.length;k++)
	        {     
	        	if(rule[k][3]!=null)
	        	{
			         if(rule[k][3].equals(catalogs.get(j).toString()))
			         {	
			           %>
			       <td >$<%=productQuoted[i].getString("discount_policy"+k) %>       
			       </td>
			           <%
		         	 }
		        }
	       }
	       
	        if(noType)
            {
	       		 for(int k=0;k<rule.length;k++)
		         {
			       	 if(rule[k][3]==null)
			       	 {     			       	  
				         %>
				       <td >$<%=productQuoted[i].getString("discount_policy"+k) %>       
				       </td>
		         		  <%		      
			         }         
	      		 }
            }
	        %>
	    </tr>

		    <%
	    }
	 }
	     %>
  </table>
  <br/>

	<form  id="dataForm<%=j %>" action="quoted_price_table.html" method="post">
		<input type="hidden"  name="cmd" value="<%=cmd %>"/>
		<input type="hidden"  name="ca_id" value="<%=areaId %>"/>	
		<input type="hidden"  name="filter_acid" value="<%=productCatalog%>" />                                        
		<input type="hidden"  name="productLineId" value="<%=productLine %>"  />
		<input type="hidden"  name="productName" value="<%=pName %>" />
		<input type="hidden"  name="discount_rate" value="<%=discountRate %>"/>
		<input type="hidden"  name="show_transport_cost" value="<%=flag %>" />
		<input type="hidden"  name="cacul_ps_id" value="<%=productStorageId %>" />
		<input type="hidden"  name="cacul_sc_id" value="<%=caculScId %>" />
		<input type="hidden"  name="startWT" value="<%=startWT %>" />
		<input type="hidden"  name="endWT" value="<%=endWT %>" />
		<input type="hidden"  name="stepWT" value="<%=stepWT %>" />
		<input type="hidden"  name="address" value="<%=address %>" />
		<input type="hidden"  name="catalog" value="<%=catalogs.get(j).toString() %>" />
		<input type="hidden"  name="p" id="p<%=j %>" value="1" />
	</form>
	<script type="text/javascript">
	 	<!--
		 $("#p<%=j%>").val("<%=pc.getPageNo()%>");			
	 	//-->
	 </script>
  <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:pageCtrlGo('#dataForm"+j+"','#p"+j+"',1,"+catalogs.size()+")",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:pageCtrlGo('#dataForm"+j+"','#p"+j+"'," + pre + ","+catalogs.size()+")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:pageCtrlGo('#dataForm"+j+"','#p"+j+"'," + next + ","+catalogs.size()+")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:pageCtrlGo('#dataForm"+j+"','#p"+j+"'," + pc.getPageCount() + ","+catalogs.size()+")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:pageCtrlGo('#dataForm<%=j %>','#p<%=j %>',document.getElementById('jump_p2').value,<%=catalogs.size()-1 %>,'<%=catalogs.get(j).toString()%>')" value="GO">
    </td>
  </tr>
</table>
  </div>
    <%
 	 }
  }
  else
  {
  %>
  <div id="default">
  <table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" >
 	
     <tr>
       <th class="right-title" width="18%"  style="vertical-align: center;text-align: center;">产品名称</th>
       <th class="right-title" width="10%"  style="vertical-align: center;text-align: center;">单位重量</th>
       <th class="right-title" width="10%" style="vertical-align: center;text-align: center;">产品零售价</th>
       <%
             
       for(String[] ruleDetail:rule)
       {
       	  
           %>
       <th class="right-title" style="vertical-align: center;text-align: center;">$<%=ruleDetail[1] %>~$<%=ruleDetail[2] %></th>
           <%
         
       }
        %>
     </tr>
     </table>
     </div>
  <%
  }
		if("show".equals(flag))
		{
		 %>
  <div id="transport_cost">
       <table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable">
          <tr>
          <th class="right-title" width="8%"  style="vertical-align: center;text-align: center;">快递方式</th>
          <th class="right-title" width="10%"  style="vertical-align: center;text-align: center;">收货区域</th>
          <th class="right-title" width="8%"  style="vertical-align: center;text-align: center;">重量</th>
          <th class="right-title" width="8%"  style="vertical-align: center;text-align: center;">费用</th>
          </tr>
       <%
       if(transportCosts!=null)
       {
       	float weigth=startWT;
	       for(int i=0;i<transportCosts.size();i++)
	       {
	       		ShippingInfoBean shippingInfoBean = (ShippingInfoBean)transportCosts.get(i);
	       		
	       		
	        %>
	          <tr height="30px" align="center" valign="middle">
	          	<td>
	          	<%=shippingInfoBean.getCompanyName() %>
	          		
	          	</td>
	          	<td><%=address %></td>
	          	
	          	<%
	          	if(i==transportCosts.size()-1)
	          	{
	          	 %>
	          	<td><%=endWT%>Kg</td>	
	          	<%
	          	}
	          	else
	          	{
	          	 %>
	          	 
	          	<td><%=(float)(Math.round(weigth*100))/100%>Kg</td>
	          	 <%
	          	 }
	          	  %>
	          	<td>$<%=shippingInfoBean.getShippingFee() %></td>
	          </tr>
	       <% 
	       	weigth=weigth+stepWT;
	       }
       }
       %>
       </table>
  </div>
  <%
  }
   %>
  </div>
  </div>

</div>	
</body>
</html>