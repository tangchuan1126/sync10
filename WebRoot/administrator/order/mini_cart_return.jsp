<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
//long stt = System.currentTimeMillis();

cartReturn.flush(session);
DBRow cartProducts[] = cartReturn.getDetailProducts();

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script>

</script>

<style>
a.del-cart-product:link {
	color:#FF0000;
	text-decoration: none;
}
a.del-cart-product:visited {
	color: #FF0000;
	text-decoration: none;
}
a.del-cart-product:hover {
	color: #FF0000;
	text-decoration: underline;

}
a.del-cart-product:active {
	color: #FF0000;
	text-decoration: none;

}
</style> 

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
  
<%
if (cartProducts.length==0)
{
%>
      <tr>
        <td colspan="4" align="center" valign="middle" style="color:#999999"><<暂无商品>>
		<script>cartIsEmpty=true;</script>		</td>
      </tr>
<%
} 
else
{
%>
	<tr>
		<td>商品名</td><td>数量</td><td>服务单明细(/套装)ID</td>
	</tr>      
	  
<%
String bgColor;
for (int i=0; i<cartProducts.length; i++)
{
	if (i%2==0)
	{
		bgColor = "#FFFFFF";
	}
	else
	{
		bgColor = "#f8f8f8";
	}
	
%>
	
      <tr bgcolor="<%=bgColor%>" >
        <td width="50%"  style="padding-left:10px;padding-top:5px;padding-bottom:5px;">

		<%=cartProducts[i].getString("p_name")%> 

		<input type="hidden" name="pids" id="pids" value="<%=cartProducts[i].getString("cart_pid")%>">
		<input type="hidden" name="product_type" id="product_type" value="<%=cartProducts[i].getString("cart_product_type")%>">	</td>
        <td width="25%">

		<input name="quantitys" type="text" id="quantitys" style="width:40px;" value="<%=cartProducts[i].getString("cart_quantity")%>" onChange="return onChangeQuantity(this)"  /> 

		
        <%=cartProducts[i].getString("unit_name")%> </td>
        <td width="20%">
	       	<%
				String associateProductId = ""+cartProducts[i].get("order_item_id",0L);
				if(!"".equals(associateProductId) && !"0".equals(associateProductId))
				{
					if(0 != cartProducts[i].get("set_or_part_pc_id",0))
					{	
						associateProductId += "_"+cartProducts[i].get("set_or_part_pc_id",0);
					}
				}
				else
				{
					associateProductId = "";
				}
			%>
			<%=associateProductId %>
        </td>
        <td width="5%" align="center" valign="middle">
			<%
        		String value = ""+cartProducts[i].get("order_item_id",0L);
        		//如果是套装cart_pid是商品ID，否则是散件ID
	        	if(0 == cartProducts[i].get("set_or_part_pc_id",0))//套装
				{
	        		value += "_"+cartProducts[i].get("cart_pid",0l)+"_"+cartProducts[i].get("cart_total_count",0F)+"_"+cartProducts[i].get("porder_item_id",0L);
				}
	        	else
	        	{
	        		value += "_"+cartProducts[i].get("set_or_part_pc_id",0l)+"_"+cartProducts[i].get("set_pc_count",0F)
	        			   + "_"+cartProducts[i].get("cart_pid",0l)+"_"+cartProducts[i].get("cart_total_count",0F);
	        	}
        	%>
        	<input type="hidden" name="order_item_id" value='<%=cartProducts[i].get("order_item_id",0L) %>'/>
        	<input type="hidden" name="set_or_part_pc_id" value='<%=cartProducts[i].get("set_or_part_pc_id",0l) %>'/>
		<a href="javascript:void(0)" onClick="delProductFromCart('<%=value%>','<%=cartProducts[i].getString("p_name")%>')" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>		</td>
      </tr>
<%
} 
%>
      <tr>
        <td height="50" style="border-top:2px #eeeeee solid"><input name="Submit433" type="submit" class="short-button" value="保存修改"> 
         
          <script>cartIsEmpty=false;</script></td>
        <td height="50" style="border-top:2px #eeeeee solid">&nbsp;</td>
        <td colspan="2" align="right" valign="middle" style="border-top:2px #eeeeee solid">&nbsp;</td>
      </tr>
<%
}

//long ent = System.currentTimeMillis();
//System.out.println("Cart Page time:"+(ent-stt));
%>
</table>

</body>
</html>
