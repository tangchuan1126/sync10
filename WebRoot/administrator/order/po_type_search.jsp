<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="../../include.jsp" %>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<%@page import="com.cwc.app.key.YesOrNotKey" %>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/>
<html>
<head>
    <!--  基本样式和javascript -->
    <link type="text/css" href="../comm.css" rel="stylesheet"/>
    <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
    <script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
    <!-- table 斑马线 -->
    <script src="../js/zebra/zebra.js" type="text/javascript"></script>
    <link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
    <!-- 引入Art -->
    <link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css"/>
    <script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
    <script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
    <script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
    <!-- 遮罩 -->
    <script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
    <!-- 时间 -->
    <script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>
    <script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
    <link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
    <!-- 提示信息 -->
    <script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
    <link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
    <!-- 选项卡 -->
    <script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
    <script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css"/>
    <link type="text/css" href="../js/tabs/demos.css" rel="stylesheet"/>
    <!-- select2 -->
    <link href="../js/select2-3.5.2/select2.css" rel="stylesheet"/>
    <script src="../js/select2-3.5.2/select2.js"></script>

    <!-- 自动匹配 -->
    <script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
    <script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
    <script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
    <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css"/>
    <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css"/>
    <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css"/>
    <script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

    <%
        String serverName = request.getServerName();
        AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
        boolean isadministrator = adminLoggerBean.isAdministrator();  //判断当前登录帐号是不是admin.如果是返回true
        long psId = StringUtil.getLong(request, "ps");//当前账号所属仓库id
        if (psId == 0l) {
            psId = adminLoggerBean.getPs_id();
        }
        DBRow[] rows = checkInMgrZwb.findPOTypeOfDN(psId);
       

    %>

    <title>pdf batch download</title>
    <style type="text/css">
        .demo {
            margin-bottom: 5px;
        }

        .zebraTable td {
            border-bottom: 1px solid #dddddd;
            padding-left: 0px;
            line-height: 30px;
        }

        .but_input {
            float: left;
            padding-right: 10px;
        }

        .tilte_right {
            width: 8%;
            text-align: right;
        }

        /* 调整 refresh 位置 */
        .adjustRefresh {
            background: url(../check_in/imgs/button_long_refresh_2.png) no-repeat 0 0;
            width: 130px;
            height: 42px;
        }

        .search_input {
            background: url(../imgs/search_bg.jpg) repeat 0 0;
            width: 400px;
            height: 24px;
            font-weight: bold;
            border: 1px #bdbdbd solid;

        }

        .search_shadow_bg {
            background: url(../imgs/search_shadow_bg2.jpg) no-repeat 1px 4px;
            width: 420px;
            height: 43px;
            padding-top: 3px;
            padding-left: 3px;
            margin-bottom: 5px;
        }

        #easy_search_father {
            position: absolute;
            width: 0px;
            height: 0px;
            z-index: 1;
        }

        #easy_search {
            position: absolute;
            left: 318px;
            top: -18px;
            width: 55px;
            height: 30px;
            z-index: 9999;
            visibility: visible;
        }

        .input_index {
            margin-right: 22%;
        }

        .w150 {
            width: 150px;
        }
    </style>

   

</head>
<body>
<div id="pdf_"></div>

<div class="demo" style="width:100%">
    <div id="tabs" style="text-align:right;">
        <input type="button" value="Export to excel" style="cursor:pointer;" onclick=" return saveAsExcel();" />

    </div>
</div>



<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
 <tr>
       <td   style="vertical-align:center;text-align:right;" colspan="6">
       	

       </td>
       
   </tr> 

 <tr>
       <th  class="left-title" style="vertical-align:center;text-align:center;">Company ID</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">Order#</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">Load#</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">DN#</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">Account ID</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">PO Type</th>
       <th  class="left-title" style="vertical-align:center;text-align:center;">PO#</th>
       
   </tr> 
<%
	if (rows != null && rows.length > 0) {
         for (DBRow row : rows) {%>
         	 <tr class="download">
         	 	    <td style="text-align:center">
			  			<span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;"><%=row.getString("companyID") %></span>
		  			</td>
		  			 <td   style="text-align:center">
			  			<span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;text-align:center;"><%=row.getString("orderNo") %></span>
		  			</td>
		  			 <td  style="text-align:center">
			  			<span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;text-align:center;"><%=row.getString("loadNo") %></span>
		  			</td>
		  			 <td   style="text-align:center">
			  			<span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;text-align:center;"><%=row.getString("referenceNo") %></span>
		  			</td>
		  			 <td   style="text-align:center">
			  			<span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;"><%=row.getString("accountID") %></span>
		  			</td>
		  			 <td  style="text-align:center">
			  			<span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;"><%=row.getString("external_id") %></span>
		  			</td>
                     <td  style="text-align:center">
                        <span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;"><%=row.getString("PONo") %></span>
                    </td>
         	 </tr>

<%       }   
	} else {
%>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
	    <tr>
	        <td style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;"
	            colspan="3">NO Data
	        </td>
	    </tr>
	</table>
<%}%>
</body>
<div style="display:none" ;><img src="../js/thickbox/loadingAnimation6.gif"></div>
<script>
	function saveAsExcel(){
		url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/po_type_export.html";
		window.open(url, "height=0,width=0");
	}
</script>
</html>
