<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>关联订单</title>
<style>
td
{
	font-size:12px;
}
</style>
<link href="../../style.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td width="72%" height="25" align="left" valign="middle" bgcolor="eeeeee" style="padding-left:10px;"><strong>商品名称</strong></td>
    <td width="28%" align="center" valign="middle" bgcolor="eeeeee" ><strong>预计到货日期</strong></td>
  </tr>
<%
long oid = StringUtil.getLong(request, "oid");
long ps_id = StringUtil.getLong(request, "ps_id");

DBRow relationOrders[] = orderMgr.getOrderLackingProductListByOid(oid,null);
String rowColor;
for (int i=0;i<relationOrders.length; i++)
{
	if ( productMgr.getDetailProductProductStorageByPName(ps_id,relationOrders[i].getString("p_name")).get("store_count",0f)>0 )
	{
		continue;
	}
	
	if (i%2==0)
	{
		rowColor = "#ffffff";
	}
	else
	{
		rowColor = "#f8f8f8";
	}
%>  
  <tr>
    <td height="25" align="left" valign="middle" bgcolor="<%=rowColor%>" style="padding-left:10px;"><%=relationOrders[i].getString("p_name")%></td>
    <td align="center" valign="middle" bgcolor="<%=rowColor%>">
	<%
	//套装存在于采购单，则判断是否有ETA
	DBRow detailPurchase = purchaseMgr.getDetailPurchaseDetailByPnameNew(relationOrders[i].getString("p_name"),ps_id);
	//System.out.println(detailPurchase+" - "+relationOrders[i].getString("p_name"));
	if (detailPurchase!=null)
	{
		if (detailPurchase.getString("eta").equals(""))
		{
			out.println("<font color=blue>已下采购单</font>");	
		}
		else
		{ 
			out.println(detailPurchase.getString("eta"));
		}
	}
	else
	{
		out.println("");
	}
	%>	
	</td>
  </tr>

<%

//对组合商品拆分，有记录就显示
DBRow productInSets[] = productMgr.getProductUnionsBySetPid(relationOrders[i].get("pid",0l));
for (int ii=0;ii<productInSets.length; ii++)
{
	if ( productMgr.getDetailProductProductStorageByPName(ps_id,productInSets[ii].getString("p_name")).get("store_count",0f)>0 )
	{
		continue;
	}
	
	detailPurchase = purchaseMgr.getDetailPurchaseDetailByPnameNew(productInSets[ii].getString("p_name"),ps_id);
	if (detailPurchase==null)
	{
		continue;
	}
%>

  <tr>
    <td height="25" align="left" valign="middle" bgcolor="<%=rowColor%>" style="padding-left:10px;">&nbsp;&nbsp;&nbsp;├ <%=productInSets[ii].getString("p_name")%></td>
    <td align="center" valign="middle" bgcolor="<%=rowColor%>">
	<%
		if (detailPurchase.getString("eta").equals(""))
		{
			out.println("<font color=blue>已下采购单</font>");	
		}
		else
		{
			out.println(detailPurchase.getString("eta"));
		}
	%>	</td>
  </tr>
<%
}
%>

  

<%
}
%>
</table>
</body>
</html>