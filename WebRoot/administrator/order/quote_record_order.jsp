<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%
String cmd = StringUtil.getString(request,"cmd");
long qoid = StringUtil.getLong(request,"qoid");

quoteMgr.initQuoteCartFromOrder(request,qoid);
DBRow detailQuoteOrder = quoteMgr.getDetailQuoteByQoid(qoid);
long ccid = detailQuoteOrder.get("ccid",0l);
long ps_id = detailQuoteOrder.get("ps_id",0l);
long sc_id = detailQuoteOrder.get("sc_id",0l);
long pro_id = detailQuoteOrder.get("pro_id",0l);
float total_weight = detailQuoteOrder.get("total_weight",0f);

String client_id = detailQuoteOrder.getString("client_id");

%>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type='text/javascript' src='../js/jquery.form.js'></script>


		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
		
		<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">

@import "../js/thickbox/thickbox.css";
</style>

<style>
.ebayTitle
{
	font-weight:bold;
	font-size:11px;
	color:#666666;
}
 
.save
{
	background-attachment: fixed;
	background: url(../imgs/save_quote2.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 40px;
	width: 121px;
	color: #000000;
	border: 0px;
	 
}
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>


<script>
function customProduct(pid,ccid,p_name,cmd)
{
	//
	//tb_show('定制套装',"../product/custom_product_quote.html?ccid="+ccid+"&cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&ps_id="+$("#ps_id").val()+"&ps_name="+$("#ps_id").getSelectedText()+"&TB_iframe=true&height=300&width=400",false);
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/custom_product_quote.html?ccid="+ccid+"&cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&ps_id="+$("#ps_id").val()+"&ps_name="+$("#ps_id").getSelectedText()+"&TB_iframe=true&height=300&width=400"; 
	 $.artDialog.open(uri , {title: '定制套装',width:'400px',height:'300px', lock: true,opacity: 0.3});
}

$().ready(function() {

	function log(event, data, formatted) {

		
	}
	
	/**
	function formatItem(row) {
		return(row[0]);
	}
	
	function formatResult(row) {
		return row[0].replace(/(<.+?>)/gi, '');
	}
	**/

	addAutoComplete($("#p_name"),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");	
	//修改状态，初始化下快递列表
	getExpressCompanysByScId(<%=ps_id%>,<%=ccid%>,<%=pro_id%>,<%=total_weight%>);
			
	//加载购物车
	loadCart(1);
	//绑定购物车表单AJAX
	ajaxCartForm();


});

//=================== 购物车 =========================

function loadCart(n)
{
	var init_sc_id = <%=sc_id%>;
	var sc_id = $("#sc_id").val();
	var ps_id = $("#ps_id").val();


	if (n>0&&init_sc_id>0)
	{
		sc_id = init_sc_id;
	}

	$.ajax({
		url: 'mini_cart_quote.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:"ccid=<%=ccid%>&sc_id="+sc_id+"&qoid=<%=qoid%>&cmd=<%=cmd%>&client_id=<%=client_id%>&pro_id=<%=pro_id%>&ps_id="+ps_id,
		
		beforeSend:function(request){

			$("#action_info").text("计算中......");
		},
		
		error: function(){
			$("#action_info").text("计算失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
			$("#action_info").text("");
		}
	});
}

   
function put2Cart(price_type)
{
	var p_name = $("#p_name").val();
	var quantity = $("#quantity").val();
	var ps_id = $("#ps_id").val();
	var sc_id = $("#sc_id").val();
	var price_type = price_type;
	
	if (ps_id==0)
	{
		$('#ps_id').poshytip('show');
	}
	//else if (sc_id==0)
	//{
	//	$('#sc_id').poshytip('show');
	//}
	else if (p_name=="")
	{
		alert("请填写商品名称");
	}
	else if (quantity=="")
	{
		alert("请填写数量");
	}
	else if ( !isNum(quantity) )
	{
		alert("请正确填写数量");
	}
	else if (quantity<=0)
	{
		alert("数量必须大于0");
	}
	else
	{
		var para = "p_name="+p_name+"&quantity="+quantity+"&ps_id="+ps_id+"&ccid=<%=ccid%>&price_type="+price_type;
	
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/put2CartQuote.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			
			error: function(){
				loadCart();
				$("#action_info").text("商品添加失败！");
				
			},
			
			success: function(msg)
			{
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException")
				{
					alert("商品不存在，不能添加到订单！");
				}
				else if (msg=="ProductNotProfitException")
				{
					alert("商品没有设置毛利，不能销售");
				}
				else if (msg=="ProductNotCreateStorageException")
				{
					alert("["+$("#ps_id").getSelectedText()+"] 不发 ["+p_name+"]");
				}
				else if (msg=="ok")
				{
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("");
					loadCart();
				}
				else
				{
					loadCart();
					alert(msg);
				}
			}
		});	
	}
}


function delProductFromCart(pid,p_name,product_type)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/removeProductFromCartQuote.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"pid="+pid+"&product_type="+product_type,
					
			error: function(){
				$("#action_info").text("商品删除失败！");
				loadCart();
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart();
			}
		});	
	}
}


function cleanCart()
{
	if (confirm("确定清空购物车？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/cleanCartQuote.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			
			error: function(){
				$("#action_info").text("清空购物车失败！");
				loadCart();
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart();
			}
		});	
	}
}

//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   beforeSubmit:  // pre-submit callback    
				function(){
						var ps_id = $("#ps_id").val();
						var sc_id = $("#sc_id").val();
	
						if (ps_id==0)
						{
							$('#ps_id').poshytip('show');
							return(false);
						}
						else if (sc_id==0)
						{
							$('#sc_id').poshytip('show');
							return(false);
						}
						
						return(true);
				}	
		   ,  
		   
		   success:       // post-submit callback  
				function()
				{
					loadCart();
					cartProQuanHasBeChange = false;
				}
		       
	};
	

   $('#cart_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}

var cartProQuanHasBeChange = false;
function onChangeQuantity(obj)
{
	if ( !isNum(obj.value) )
	{
		alert("请正确填写数字");
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}

function onChangeQuotePrice(name,obj,lowest_price)
{

	if ( !isNum(obj.value) )
	{
		alert("请正确填写数字");
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(false);
	}
	else if ( obj.value*1<lowest_price*1 )
	{
		alert(name+" 报价不能低于 $"+lowest_price);		
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}


function isNum(keyW)
{
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
//=================== 购物车 =========================


var cartIsEmpty = <%=cart.isEmpty(session)%>;

function checkCart()
{
	if (cartIsEmpty)
	{	
		alert("请先添加商品");
		return(false);
	}
	else
	{
		return(true);
	}
}

//除了正常抄单，购物车不能为空外，其他状态抄单都可以为空
function trueOrder()
{

	if (checkCart())
	{
		if (cartProQuanHasBeChange)
		{
			$('#flush_modify').poshytip('show');
		}
		else if ($("#ps_id").val()==0)
		{
			$('#ps_id').poshytip('show');
		}
		else if ($("#sc_id").val()==0)
		{
			$('#sc_id').poshytip('show');
		}
		else
		{
				//先检查购物车中的商品在当前仓库是否已经建库
				var ps_id = $("#ps_id").val();
				var sc_id = $("#sc_id").val();
				var qoid = <%=qoid%>;
				var ccid = <%=detailQuoteOrder.get("ccid",0l)%>;
				
				var para = "ps_id="+ps_id+"&sc_id="+sc_id+"&qoid="+qoid+"&ccid="+ccid;
			
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/validateProductsAndDelivery.action',
					type: 'post',
					dataType: 'html',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(){
					},
					
					success: function(msg){
						if (msg!="ok")
						{
							if (msg=="WeightOutSizeException")
							{
								alert("超重");
							}
							else if (msg=="WeightCrossException")
							{
								alert("重量段交叉");
							}
							else if (msg=="CountryOutSizeException")
							{
								alert("国家不能派送，请换其他快递");
							}
							else if (msg=="ProvinceOutSizeException")
							{
								alert("洲不能派送，请重新设置或换其他快递");
							}
							else
							{
								alert("["+$("#ps_id").getSelectedText()+"] 不发 ["+msg+"]");
							}
						}
						else
						{						
							parent.trueOrder(<%=qoid%>,$("#ps_id").val(),sc_id,<%=detailQuoteOrder.get("ccid",0l)%>,"<%=client_id%>",<%=pro_id%>);
						}
					}
				});	
 

		}
	}
}



function closeTBWin()
{
	tb_remove();
}



 
function getExpressCompanysByScId(ps_id,ccid,pro_id,weight)
{

	var old_selected_sc_id = $("#sc_id").val();
	
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/GetMatchExpressCompanysByPsIdJSON.action",
				{ps_id:ps_id,ccid:ccid,pro_id:pro_id,weight:weight},
				function callback(data)
				{  

					if (data!="")
					{
						$("#sc_id").clearAll();
						$("#sc_id").addOption("快递...",0);
						$.each(data,function(i){
							if (data[i].name!="上门自提")
							{
								$("#sc_id").addOption(data[i].name,data[i].sc_id);
							}
						});
						
						$("#sc_id").setSelectedValue(<%=sc_id%>);
						//如果已经选择快递，那么刷新后保持原状
						if (old_selected_sc_id>0)
						{
							$("#sc_id").setSelectedValue(old_selected_sc_id);
						}
					}

				}
		);
}

function modifyCartManagerDiscount()
{
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/modifyCartManagerDiscount.action',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:"manager_discount="+$("#manager_discount").val(),
		
		beforeSend:function(request){
		},
		
		error: function(){
			alert("网络错误，请重试！");
		},
		
		success: function(html){
			loadCart();
		}
	});
}

function getExpressCompanysByScId4Change(ps_id,ccid,pro_id)
{
	getExpressCompanysByScId(ps_id,ccid,pro_id,total_weight);
}

function pasteEmail()
{
	if ( document.getElementById("emailContainer").style.display == "" )
	{
		document.getElementById("paste_email").innerHTML = "粘贴邮件内容";
		document.getElementById("emailContainer").style.display = "none";
	}
	else
	{
		document.getElementById("paste_email").innerHTML = "隐藏";
		document.getElementById("emailContainer").style.display = "";
	}
}

var total_weight = 0;//购物车实时刷新总重数据
</script>
<style type="text/css">
<!--
.STYLE1 {
	color: #FFFFFF;
	font-weight: bold;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >


<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">
	
	
	
	

	
<table width='95%' border='0' cellpadding='0' cellspacing='0'  style="padding-left:0px;"> 
  
  <tr>
    <td >
<table width='100%' border='0' cellspacing='0' cellpadding='3'>
        <tr>
          <td width="3%" align='center' valign='middle'>&nbsp;</td>
          <td width="97%">&nbsp;</td>
        </tr>

        <tr>
          <td colspan="2" align='left' valign='middle' style="padding-left:15px;padding-right:15px;">
            <fieldset style="border:3px #cccccc solid;padding:15px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:bold;color:#000000;font-family: Verdana">报价商品</legend>
<table width="98%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <td height="35" align="left" valign="middle" style="font-size:13px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;color:#666666;border-top:#999999 1px solid;border-left:#999999 1px solid;border-right:#999999 1px solid;padding-left:10px;">

	<span style="color:#666666">发货仓库</span>
	
	  <select name="ps_id" id="ps_id"  onChange="getExpressCompanysByScId4Change(this.value,<%=ccid%>,<%=pro_id%>);$('#ps_id').poshytip('hide');">
	    <option value="0">请选择...</option>
	    <%

DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();
for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) > 0 )
	 {
	 	continue;
	 }

%>
	    <option value="<%=treeRows[i].getString("id")%>" <%=ps_id==treeRows[i].get("id",0l)?"selected":""%>> 
	      
	      <%=treeRows[i].getString("title")%>          </option>
	    <%
}
%>
	    </select>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<span style="color:#666666">发货快递</span>
		
		<select name="sc_id" id="sc_id" onChange="$('#sc_id').poshytip('hide');">
		  <option value="0">请选择...</option>
		  </select>
				<script>
				$('#sc_id').poshytip({
				content:"请选择快递公司",
				className: 'tip-yellowsimple',
				showOn: 'none',
				alignTo: 'target',
				alignX: 'center',
				alignY: 'bottom',
				showTimeout:0,
				offsetX: 0,
				offsetY: 5
			});
			
				$('#ps_id').poshytip({
				content:"请选择发货仓库",
				className: 'tip-yellowsimple',
				showOn: 'none',
				alignTo: 'target',
				alignX: 'center',
				alignY: 'bottom',
				showTimeout:0,
				offsetX: 0,
				offsetY: 5
			});
		        </script>	
		&nbsp;&nbsp;&nbsp;&nbsp;			
				
					<span style="color:#666666">递送国家</span>&nbsp;&nbsp;
	<span style="font-size:14px;color:#990099"><%=orderMgr.getDetailCountryCodeByCcid(ccid).getString("c_country")%></span>
	
&nbsp;&nbsp;&nbsp;&nbsp;
<span style="color:#666666">递送城市</span>&nbsp;&nbsp;
	<span style="font-size:14px;color:#990099"><%=detailQuoteOrder.getString("address_city")%></span>				</td>
    </tr>
  <tr>
    <td height="35" align="left" valign="middle" bgcolor="#999999" style="padding-left:10px;"><span class="STYLE1">搜索商品</span>
      <input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:12px;"/>
     &nbsp;&nbsp;
	  <span class="STYLE1">数量</span> 
        <input type="text" id="quantity" name="quantity"  style="color:#FF3300;width:40px;font-size:12;"/>
&nbsp;
            <input name="Submit3" type="button" class="long-button-green" value="添加普通商品" onClick="put2Cart(<%=CartQuote.NORMAL_PRICE%>)">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input name="Submit3" type="button" class="short-short-button" value="特价" onClick="put2Cart(<%=CartQuote.SPECIAL_PRICE%>)">
			
			</td>
    </tr>
  <tr>
    <td align="left" valign="middle" >
	<img src="../imgs/paste_email.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="javascript:pasteEmail()"><span id="paste_email">粘贴邮件内容										                                   </span></a>

	<textarea  style="width:750px;height:200px;display:none;border:1px #999999 dashed;color:#666666;padding:5px;" id="emailContainer"></textarea>
	</td>
    </tr>

  <tr>
    <td align="left" valign="middle" style="padding-top:10px;">
	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/batchModifyProductsQuantityFromCartQuote.action" method="post" name="cart_form" id="cart_form">
	<div id="mini_cart_page"></div>
</form>	</td>
    </tr>
	
  <tr>
    <td align="right" valign="middle" style="color:#FF0000" id="action_info"></td>
  </tr>
</table>
            </fieldset></td>
        </tr>
        <tr>
          <td align='left' valign='middle' >&nbsp;</td>
          <td align='left' valign='middle' >&nbsp;</td>
        </tr>
      </table>	  
  </td>
  </tr>
</table>	
	
	
	
	</td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;	
	
	</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存报价" class="normal-green-long" onClick="trueOrder();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeTBWin();">
	</td>
  </tr>
</table>





</body>
</html>
