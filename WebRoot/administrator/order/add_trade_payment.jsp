<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.PaymentMethodKey" %>
<%@ page import="com.cwc.app.key.PayTypeKey,com.cwc.app.api.OrderMgr"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<jsp:useBean id="currencyKey" class="com.cwc.app.key.CurrencyKey"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单报价</title>
 
 
<style type="text/css">
*{padding:0px;margin:0px;}
ul.addressList{list-style-type:none;margin-left:10px;border:0px solid red;text-align:left;}
 ul.addressList li {text-indent:5px;line-height:25px;height:25px;margin-top:2px;border-bottom:1px dotted silver;cursor:pointer;}
ul.type{border:1px solid silver;background:#E6F3C5;list-style-type:none;height:35px;line-height:35px;}
	ul.type li {margin-top:4px;float:left;border:1px solid silver;width:100px;line-height:25px;height:25px;margin-left:10px;text-align:center;cursor:pointer;}
	ul.type li.on{background:white;}
	table.addressInfo td.right{text-align:right;font-weight:bold;color:#0066FF;}
 
</style>
<%
	String getAccountInfo = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/GetAccountInfoAction.action";
	String getAddressInfoList = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/GetAddressInfoListAction.action";
	String validatTxnid = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/VolidateTxnIdAction.action";
	String addOrderAndTradeByHandle =  ConfigBean.getStringValue("systenFolder")+"action/administrator/order/AddOrderAndTradeByHandleAction.action";
	 
%>
<script type="text/javascript">
// 验证表单是不是通过了。
 var txnFlag = false;
 var tip = []; 
 jQuery(function($){
	 $("#sel").chosen({no_results_text: "没有匹配形式"}).change(function(){
		 var key =  $(this).val() ;
		 if(key * 1 == -1){
			 //把后面的信息都移除
			 $("#memberDiv").html("");
		 }else{
 
			 $("#payment_method").val($("option:selected",$("#sel")).html());
	 
			 $("#payment_channel").val(key);
			 $("#memberDiv").html("");
			 ajaxGetAccountInfo($(this).val());
			 
			 if($("#sel").val() * 1 == 0){
				var clientId = $("#client_id").val();
				$("#payer_email").val(clientId);
			 }else{
				 $("#payer_email").val("");
			}
		 }
	 });
	 $(".chzn-search input").width(110);
 })
function ajaxGetAccountInfo(key){
	 var div = $("#memberDiv");
	 var txnDiv = $("#txnDiv");
	 
	 if(key * 1 === '<%= PayTypeKey.PayPal%>' * 1){
		 txnDiv.css("display","block");
	 }else{
		 txnDiv.css("display","none");
	}

	 div.append("<img src='../imgs/ajax-loader.gif' style='margin-top:4px;margin-left:4px;'/>")
	 $.ajax({
			url:'<%= getAccountInfo%>' + "?key="+key,
			dataType:'json',
			success:function(data){
				 $("img",div).remove();
				if(data){
					var selected = createMemberSelect(data);
					
					div.append($(selected));
			 
					
				  	$("#member").chosen({no_results_text: "没有该人"}).change(function(){
					  	$("#key_type").val(key)
						var value = ($(this).val());
						if(value != "-1"){
							if(value == "-2"){
								//-2 表示的是手工输入
								var key_type = ($("#sel").val());
								var tipAccountName = "";
								var tipAcccount = "";
								for(var index = 0 , count = tip.length ; index < count ; index++ ){
									if(key_type  === tip[index]["key_type"]){
										tipAccountName = "*"+tip[index]["account_name"];
										tipAcccount = "*"+tip[index]["account"];
										break;
									}	 
								}
								$("#account_name").html("<input type='text' class='noborder' onfocus=inputIn(this,'"+tipAccountName+"') onblur=outInput(this,'"+tipAccountName+"') value='"+tipAccountName+"' />");
								$("#account").html("<input type='text' class='noborder' onfocus=inputIn(this,'"+tipAcccount+"') onblur=outInput(this,'"+tipAcccount+"') value='"+tipAcccount+"'/>")
								$("#account_payee").val("-2");
							}else{
								var option = $("option:selected",$("#member"));
								var optionHtml = option.html();
								var target = option.attr("target");
								$("#account_name").html(optionHtml);
								
								$("#account").html( value);
								$("#account_payee").val(target);
							}
						}else{
							$("#account_name").html("");
							$("#account").html("");
						}
					 });
				  	$(".fixselect input").width(230);
				} 
			}
		})	
 }
 
 function inputIn(_this,value){
	var node = $(_this);
	if(node.val() === value){
		node.css("color","black").val("");
	}
 }
 function outInput(_this,value){
	var node = $(_this);
	if($.trim(node.val()).length < 1) {
		node.val( value).css("color","silver");
	}
		
 }
 
 function createMemberSelect(data){
		var selected = ' <select data-placeholder="收款人" class="chzn-select" style="width:120px;" id="member">';
		var options = "<option value='-1'>选择收款人</option><option value='-2'>手工输入</option>";
		for(var index = 0 , count = data.length ; index < count ; index++ ){
			if(data[index]["is_tip"] * 1 == 0)
			{
				options += "<option target='"+data[index]["id"]+"' value='"+data[index]["account"]+"'>"+data[index]["account_name"]+"</option>";
			}else{
				var  o = new Object();
				o = {
					key_type:data[index]["key_type"],
					account_name:data[index]["account_name"],
					account:data[index]["account"],
					payer_info_tip:data[index]["payer_info_tip"]
				};
				tip.push(o)
			}
		}
		options += "</select>";
		selected += options;
		showTipInSpan();
		return selected;

	 }
 // 在Span 中显示Tip
 function showTipInSpan(){
	 var key_type = ($("#sel").val());
		var tipAccountName = "";
		var tipAcccount = "";
		var payer_info_tip = "";
		for(var index = 0 , count = tip.length ; index < count ; index++ ){
			if(key_type  === tip[index]["key_type"]){
				tipAccountName = "*"+tip[index]["account_name"];
				tipAcccount = "*"+tip[index]["account"];
				payer_info_tip = "*" + tip[index]["payer_info_tip"];
				break;
			}	 
		}
		$("#account_name").html(tipAccountName);
		$("#account").html(tipAcccount);
		
		$("#payer_email").val(payer_info_tip);
		
 }
 function addNew(){
	 
 }
 function getAddressInfoList(){
	 
	 var client_id = $("#client_id").val();
	 if(client_id.length < 1){
		 
		 return ;
	}
	$(".cssDivschedule").css("display","block");
	$.ajax({
		url:'<%= getAddressInfoList%>',
		dataType:'json',
		data:jQuery.param({client_id:client_id}),
		success:function(data){
			$(".cssDivschedule").fadeOut();
			if(data && data.length > 0){
				addAddressInfoList(data);
			} else{
				var ul = $("ul.addressList");
				$("li",ul).remove();
			} 
		}
	})	
		
 }
 	 function closeWindow(){
		$.artDialog.close && $.artDialog.close();
 	 }
	 function addAddressInfoList(data){
		 
			var ul = $("ul.addressList");
			$("li",ul).remove();
			ul.slideUp();
			var lis = "";
			for(var index = 0 , count = data.length ; index < count ; index++ ){
				 
				  lis += "<li><input type='radio' name='radio_address' ";
				  lis += "address_country='"+data[index]["address_country"]+"'";
				  lis += "address_city='" + data[index]["address_city"]+"'";
				  lis += "address_zip='" + data[index]["address_zip"]+"'";
				  lis += "address_state='"+ data[index]["address_state"] +"'";
				  lis += "address_street='"+data[index]["address_street"]+"' address_name='"+data[index]["address_name"]+"' code='"+data[index]["address_country_code"]+"' onclick='fillValue(this)'/>&nbsp; ";
				  lis += data[index]["address_name"]+": "+data[index]["address_street"]+ ","+data[index]["address_city"]+", "+ data[index]["address_state"]+","+ data[index]["address_country"]+"</li>";
			}
			
			ul.append($(lis));
			ul.slideDown();
				
		 }
	 function changeRelationId(){
			var  relation_type = $("#relation_type").val();
			var str = "";
			if(relation_type === "0"){
				
			}else{
				
			}
	 }
	 function removeAfterProIdInput(){$("#address_state_input").remove();}
	 function setPro_id(fillFlag,val,html){
			removeAfterProIdInput();
			var node = $("#ccid_hidden");
			var value = node.val();
		 
			if(value*1 != 10929){
				$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
						{ccid:value},
						function callback(data){ 
							$("#pro_id").attr("disabled",false);
							$("#pro_id").clearAll();
							$("#pro_id").addOption("请选择......","0");
							if (data!=""){
								$.each(data,function(i){
									$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
								});
							}
							if (value>0){
								$("#pro_id").addOption("手工输入","-1");
							}
							$("#addBillProSpan").html($("#pro_id"));
							// 如果是这个下面有 对应的那么就选中 如果是么有的那么就是 和点击手工输入一样的操作
							if(val){
								if(val === "-1"){
									$("option[value='"+val+"']",$("#pro_id")).attr("selected",true);
									$("#addBillProSpan").append($("<input type='text' id='address_state_input' value='"+html+"' />"));
								}else{
									$("option[value='"+val+"']",$("#pro_id")).attr("selected",true);
									removeAfterProIdInput();
								}
							}
						}
						
					);
				if(!fillFlag){
					if(value * 1 != 0 ){
						// 得到Option
						var option = $("option:selected",node);
						$("#address_country_code").val(option.attr("code"));
					}else{
						$("#address_country_code").val("");
					}
				}
			 }
		 }
	 function fixAddress(){
		 var node = $("#pro_id");
		 var value = node.val();
	 	
		 if(value*1 == -1){
			 addInputAfterProid(node);
		 }else{
			 removeAfterProIdInput();
		}
	 }
	 function addInputAfterProid(node){
		 var input  = "<input type='text'   id='address_state_input' />";
		 $(input).insertAfter(node);
	 }
	 function getAddressInfoList(){
	 
		 var client_id = $("#client_id").val();
		 if(client_id.length < 1){
			 return ;
		}
		$(".cssDivschedule").css("display","block");
		$.ajax({
			url:'<%= getAddressInfoList%>',
			dataType:'json',
			data:jQuery.param({client_id:client_id}),
			success:function(data){
				$(".cssDivschedule").fadeOut();
				if(data && data.length > 0){
					addAddressInfoList(data);
				} else{
					var ul = $("ul.addressList");
					$("li",ul).remove();
				}
			}
		})	
			
	 }
	 function addAddressInfoList(data){
		 
		var ul = $("ul.addressList");
		$("li",ul).remove();
		ul.slideUp();
		var lis = "";
		for(var index = 0 , count = data.length ; index < count ; index++ ){
			 
			  lis += "<li><input type='radio' name='radio_address' ";
			  lis += "address_country='"+data[index]["address_country"]+"'";
			  lis += "address_city='" + data[index]["address_city"]+"'";
			  lis += "address_zip='" + data[index]["address_zip"]+"'";
			  lis += "address_state='"+ data[index]["address_state"] +"'";
			  lis += "address_street='"+data[index]["address_street"]+"' address_name='"+data[index]["address_name"]+"' code='"+data[index]["address_country_code"]+"' onclick='fillValue(this)'/>&nbsp; ";
			  lis += data[index]["address_name"]+": "+data[index]["address_street"]+ ","+data[index]["address_city"]+", "+ data[index]["address_state"]+","+ data[index]["address_country"]+"</li>";
		}
		
		ul.append($(lis));
		ul.slideDown();
			
	 }
	 //address_country,address_state,ccid
	 function getAddress(){
		var object = new Object();
		var ccid = $("#ccid_hidden");
		 
		var ccidName = $("option:selected",ccid).html();
		var pro_id = $("#pro_id").val() * 1;
		var proName =  "";
		var address_state_input = $("#address_state_input");
		object["pro_id"] = pro_id;
		if(ccid * 1 != 0){
			object["address_country"] = ccidName;
		}
		if(pro_id ==  -1){
			proName = address_state_input.val();
		}else{
			proName =  $("option:selected",$("#pro_id")).html();
		}
		object["address_state"] = proName;
		return object;
	 }
	
	 function fillValue(_this){
			var node = $(_this);
			var table = $(".addressInfo");
		 	$("input[name='address_name']").val(node.attr("address_name"));
			$("input[name='address_street']").val(node.attr("address_street"));
			$("input[name='address_city']").val(node.attr("address_city"));
			$("input[name='address_country_code']").val(node.attr("code"));
			$("input[name='address_zip']").val(node.attr("address_zip"));
			 // 初始化country 
			 var code = node.attr("code");
			 var selected = $("#ccid_hidden");
			 $("option[code='"+$.trim(code).toUpperCase()+"']",selected).attr("selected",true);
			 setPro_id(true);
		 	
		 }
		// 首先要检查CCID 和 pro_id 是不是有 有的话就可以继续添加没有的话 就不应该添加了。
		// 然后就是异步的添加Trade和Order。成功的时候就要弹出抄单窗口
	 	function changeTab(_this,target){
		 	if(target === "products"){
			 	// 应该是在提交的时候去检查Txn_id 是不是已经存在了
			 	
		 		 var flag = validate();
		 		  
				if(flag){
					//
					var str = "";
					str += $("#baseinfo").serialize() +"&"+ $("#addressForm").serialize() + "&" +jQuery.param(getAddress()) +"&"+getBusiness();	
					// 如果是在Paypal的情况下那么就要验证 txn_id是不是为空。如果是空就应该提示。不是空的时候就要进行验证是不是成功 成功了那么在提交。
					// 如果没有就应该提示
				   var key = $("#sel").val();
					 if(key * 1 === '<%= PayTypeKey.PayPal%>' * 1){
						  var txnId = $("#txn_id");
						  if(txnId.length < 1 || $.trim(txnId.val()).length < 1){
								showMessage("请先输入TxnId","alert");
								return ;
						  }else{
							  checkTxnNotExit(str);
						 }
					 } else{
						 ajaxAddOrderAndTradeByHandle(str);
					}
				  
				
				
				}else{
					$("#product").removeClass("on");
					$("#basic_info").addClass("on");
					return ;
				}
			}
		 	var node = $(_this);
		 	removeTypeListOn();
		 	$(".type li[target='"+target+"']").addClass("on");
		}
	 
		function fixFloatValuePure(_this){
 			var node = $(_this);
 			var value = node.val();
 			var fixValue = value.replace(/[^0-9.]/g,'');
 				 
 			node.val(fixValue); 
 	 	}
 	 	function closeWin(){
		 
			showMessage("订单添加完成","alert");
 	 	}
		function ajaxAddOrderAndTradeByHandle(str){
			$.artDialog.confirm('该交易会生成一个订单,确定添加改交易?', function(){
				$(".cssDivschedule").css("display","block");
				$.ajax({
					url:'<%= addOrderAndTradeByHandle%>',
					dataType:'json',
					data:str,
					error:function(XMLHttpRequest, textStatus, errorThrown){
						$(".cssDivschedule").fadeOut();
						showMessage("系统错误","error");
						$("#basic_info").addClass("on");
						$("#product").removeClass("on");
					},
					success:function(data){
						$(".cssDivschedule").fadeOut();
						if( data && data.flag && data.flag == "success"){
							var oid = data.oid;
							 
							 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/new_record_order.html?oid=" + oid;
							 $.artDialog.open(uri , {title: "抄单 订单号["+oid+"]",width:'880px',height:'500px', lock: true,opacity: 0.3});
						}
						if(data && data.flag == "error"){
							showMessage(data.message,"error");
							$("#basic_info").addClass("on");
							$("#product").removeClass("on");
							
						}				
					}
				}) 
			}, function(){
				$("#basic_info").addClass("on");
				$("#product").removeClass("on");
			});
			 
		 	
			
			 
			
		}
		function removeTypeListOn(){
			$(".type li").removeClass("on");
		}
		//返回收款账号 
		function getBusiness(){
			var object = new Object();
			var node = $("input",$("#account"));
			var nodeName = $("input",$("#account_name")); // 收款人
			if(node.length > 0){
				object["receiver_email"] = nodeName.val();
				object["business"] = node.val();
			}else{
				object["receiver_email"] = $("#account_name").html();
				object["business"] = $("#account").html();
				
			}
   			return jQuery.param(object);
		}
		function validate(){
			
			var flag = true;
			var ccid = $("#ccid_hidden");
			 
			var ccidName = $("option:selected",ccid).html();
			var pro_id = $("#pro_id");
			var proName =  "";
			var address_state_input = $("#address_state_input");
			if(ccid.val() * 1 == 0 ){
				flag = false;
				showMessage("请选择配送国家","alert");
			}
			if(pro_id.val() * 1 == 0){
				flag = false;
				showMessage("请选择配送省份","alert");
			}
			if(pro_id.val() * 1 == -1 && (address_state_input && address_state_input.val().length < 1)){
				flag = false;
				showMessage("请选择配送省份","alert");
			}
	 
			var ids =  ["#client_id","#order_source","#payer_email","#mc_gross","#mc_currency","#quantity"];
			var tips = ["请输入客户","请先选择订单来源","付款账号","付款金额","货币","商品数量"];
			for(var index =  0 , count = ids.length ; index < count ; index++ ){
				if($.trim($(ids[index]).val()).length < 1){
					showMessage(tips[index],"alert");
					flag = false;
					return flag;
				}
			}
			var payer_email = $("#payer_email").val();
			if(payer_email.indexOf("*") != -1){
				showMessage("付款账号","alert");
				return false;
			}
			// 收款人 收款账号
			var account_name = $("#account_name");
			var account = $("#account");
			var inputAccountName = $("input",account_name);
			var accountNameValue = "";
			var accountValue = "";
			if(inputAccountName.length > 0){
				accountValue = $("input",account).val();
				accountNameValue = inputAccountName.val();
			}else{
				accountValue = account.html();
				accountNameValue = account_name.html();
			}	
			if(accountNameValue.indexOf("*") != -1 || $.trim(accountNameValue).length < 1){
				showMessage("请输入收款人","alert");
				return false;
			}
		
			if(accountValue.indexOf("*") != -1 || $.trim(accountValue).length < 1){
				showMessage("请输入收款账号","alert");
				return false;
			}

			return flag ;	
		}
		
		function checkTxnNotExit(str){
			var txnId = $("#txn_id").val();
			 
			if($.trim(txnId).length < 1){
				txnFlag = true;
				return ;
			}else{
				$.ajax({
					url:'<%= validatTxnid%>' + "?txn_id=" + txnId,
					dataType:'text',
					success:function(data){
						if(data === "isexit"){
							showMessage("TxnId[ " +txnId+ " ]已经存在","alert");
						}else{
							ajaxAddOrderAndTradeByHandle(str)
						}
						
					}
				})	
			}
		}

	 // 如果是Ebay的时候是要输入txn_id的
	 function changeOrderSource(){
		var order_source = $("#order_source");
		if(order_source.val() === "EBAY"){
				
		}
		
	 }
	 function selectIsInstant(_this){
		var node = $(_this);
		 
		if(node.val() === "instant"){
			 $("option[value='Completed']",$("#payment_status")).attr("selected",true);
		}
	 }
</script>

</head>
<body>
<!-- 遮盖层 -->
   <div class="cssDivschedule">
 		<div class="innerDiv">
 			请稍后.......
 		</div>
 </div>
		  <div style="position:relative;z-index:10;margin-bottom:10px;">
		  	 <ul class="type" style="text-align:center;">
		  		<li class="ui-corner-all on" id="basic_info" target="baseInfo" onclick="changeTab(this,'baseInfo');">基本信息</li>
		  		<li class="ui-corner-all" id="product" target="products" onclick="changeTab(this,'products');">商品信息</li>
			 </ul>
		  </div>
		 
		  	<div id="baseInfo" style="border:1px solid silver;margin-top:5px;overflow:hidden;height:400px;padding-left:5px;background:white;overflow:auto;">
			  	<div style="float:left;width:45%;">
			  		<form id="baseinfo">
			  		<input type="hidden" id="account_payee" name="account_payee" />
			  		<input type="hidden" name="transaction_type" value="1"/>
			  		<input type="hidden" id="payment_method" name="payment_method" />
			  		<input type="hidden" id="payment_channel" name="payment_channel" />
					  <fieldset class="set" style="border:2px solid #993300;padding:5px;">
						  	  <legend>基本信息</legend>
							  <table  style="border:0px solid red;margin-top:2px;width:100%;">
						 		<tr>
									<td class="td_left">客户 :</td>
									<td>&nbsp;<input type="text" value="" id="client_id"  style="width:200px;" name="client_id" onblur="getAddressInfoList()"/></td>
								</tr>
							  	<tr>
							  		<td class="td_left">订单来源:</td>
							  		<td>
							  			<select name="order_source" id="order_source" onchange="changeOrderSource();" >
								            <option value="">请选择...</option>
								            <option value="<%=OrderMgr.ORDER_SOURCE_PAY%>"><%=OrderMgr.ORDER_SOURCE_PAY%></option>
											<option value="<%=OrderMgr.ORDER_SPLIT%>"><%=OrderMgr.ORDER_SPLIT%></option>
											<option value="<%=OrderMgr.ORDER_LOCAL_BUY%>"><%=OrderMgr.ORDER_LOCAL_BUY%></option>
											<option value="<%=OrderMgr.ORDER_DIRECT_BUY%>"><%=OrderMgr.ORDER_DIRECT_BUY%></option>
											<option value="<%=OrderMgr.ORDER_SOURCE_EBAY%>"><%=OrderMgr.ORDER_SOURCE_EBAY%></option>
											<option value="<%=OrderMgr.ORDER_AMAZON%>"><%=OrderMgr.ORDER_AMAZON%></option>
											<option value="<%=OrderMgr.ORDER_ECRATER%>"><%=OrderMgr.ORDER_ECRATER%></option>
											<option value="<%=OrderMgr.ORDER_BONANZA%>"><%=OrderMgr.ORDER_BONANZA%></option>
								          </select>
							  		</td> 
							  	</tr>
							  	<tr>
							  		<td class="td_left">付款形式:</td>
							  		<td>
							  		<div style="float:left;">
								  		<select data-placeholder="付款形式" class="chzn-select" style="width:120px;" name="pay_type" id="sel">
											<option value="-1">选择付款形式</option>
											<%
												PaymentMethodKey key = new PaymentMethodKey();
												ArrayList<String> payMentList=  key.getStatus();
												for(String name : payMentList){
											%>
													<option value="<%=name %>"><%=key.getStatusById(Integer.parseInt(name)) %></option>
											<% 	 
												}
											%>
										</select>
							  		</div>
							  			
										 <div style="border:0px solid silver;clear:both;display:none;" id="txnDiv">
										 	&nbsp;TxnId : <input style="width:180px;" type="text" name="txn_id" id="txn_id"  width="198px;"/>
										 </div>
							  		</td>
							  	</tr>
							  	<tr>
							  		<td class="td_left">客户付款账号:</td>
							  		<td><input type="text" id="payer_email" name="payer_email" style="width:250px;"/></td>
							  	</tr>
						
							  	<tr>
							  		<td class="td_left">选择收款人:</td>
							  		<td>
							  			<div style="float:left;" id="memberDiv">
													 
										 </div>
							  		</td>
							  	</tr>
							  	<tr>
							  		<td class="td_left">收款人:</td>
							  		<td>
										 <span id="account_name">
										</span>
									</td>
							  	</tr>
							  	<tr>
							  		<td class="td_left">收款账号:</td>
							  		<td><span id="account"></span></td>
							  	</tr>
							  		  	<tr>
							  		<td class="td_left">到账情况:</td>
							  		<td>
								  		类型:<select name="payment_type" id="payment_type" onchange="selectIsInstant(this);">
								  				<option value="instant">instant</option>
								  				<option value="echeck">echeck</option>
								  				<option value="查账中">查账中</option>
								  			</select>
							  			状态:<select name="payment_status" id="payment_status">
							  				<option value="Completed">Completed</option>
							  				<option value="Pending">Pending</option>
							  				 
							  			</select>
							  		</td>
							  	</tr>
							  	<tr>
  									<td class="td_left">付款金额:</td>			
    								<td>
    									<input name="mc_gross" onkeyup="fixFloatValuePure(this)" type="text" id="mc_gross" size="15" />
    										&nbsp;
										<select name="mc_currency" id="mc_currency">
											        <option value="">选择货币...</option>
											        <%
													ArrayList currency = currencyKey.getCurrency();
													for (int i=0; i<currency.size(); i++){
													%>
													    <option value="<%=currencyKey.getCurrencyById( currency.get(i).toString() )%>"><%=currency.get(i)%></option>
													<%
														}
													%>
										</select>	
    									
    								</td>
							  	</tr>
							  	<tr>
							  		<td class="td_left">购买数量:</td>
							  		 <td><input onkeyup="fixFloatValuePure(this)" name="quantity" type="text" id="quantity" size="15"></td>
							  	</tr>
							  	
   
							  </table>
						  </fieldset>
						  </form>
					  </div>
				 <div style="float:left;width:45%;margin-left:5px;">
				  <fieldset class="set" style="border:2px solid #993300;padding:5px;">
				  	  <legend>地址信息</legend>
					 <form id="addressForm">
						  <table class="addressInfo">
									
									<tr>
										<td class="right">Name :</td>
										<td>&nbsp;<input type="text" name="address_name"  /></td>
										
									</tr>
									 <tr>
										<td class="right">Street :</td>
										<td>&nbsp;<input type="text" name="address_street" /></td>
									</tr>
									 <tr>
										<td class="right">City :</td>
										<td>&nbsp;<input type="text" name="address_city" /></td>
									</tr>
									<tr>
										<td class="right">Zip :</td>
										<td>&nbsp;<input type="text" name="address_zip" /></td>
									</tr>
									 <tr>
										<td class="right">Country :</td>
										<td>&nbsp;
											 
								<!-- 国家省份信息 -->
									<%
										DBRow countrycode[] = orderMgr.getAllCountryCode();
										String selectBg="#ffffff";
										String preLetter="";
									%>
							      	 <select  id="ccid_hidden"  name="ccid" onChange="removeAfterProIdInput();setPro_id();">
								 	 	<option value="0">请选择...</option>
										  <%
										  for (int i=0; i<countrycode.length; i++){
										  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
												if (selectBg.equals("#eeeeee")){
													selectBg = "#ffffff";
												}else{
													selectBg = "#eeeeee";
												}
											}  	
											preLetter = countrycode[i].getString("c_country").substring(0,1);
										  %>
								  		  <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
										  <%
										  }
										%>
					     			 </select>
										</td>
										
									</tr>
									 <tr>
										<td class="right">State :</td>
										<td> 
											&nbsp;
											<span id="addBillProSpan">
												<select  disabled id="pro_id" onchange="fixAddress()" style="margin-right:5px;"></select>
											</span>
										</td>
										 
									</tr>
									 <tr>
										<td class="right">Tel :</td>
										<td><input type="text"  id="tel" name="tel"/></td>
									</tr>
									<input type="hidden" id="address_country_code" name="address_country_code" />
								</table>
							</form>
							
							<p>
								<ul class="addressList">
	 							</ul>
							</p>
				  	</fieldset>
			  	</div>
				 
			  </div>
			 
		 </div>
</body>

</html>