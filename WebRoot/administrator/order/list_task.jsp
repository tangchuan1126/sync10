<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<jsp:useBean id="orderTaskKey" class="com.cwc.app.key.OrderTaskKey"/>
<%@ include file="../../include.jsp"%> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

 String st = StringUtil.getString(request,"st");
 String en = StringUtil.getString(request,"en");
 String business = StringUtil.getString(request,"business");

 String cmd = StringUtil.getString(request,"cmd");
 long oid = StringUtil.getLong(request,"oid");

 PageCtrl pc = new PageCtrl();
 pc.setPageNo(StringUtil.getInt(request,"p"));
 pc.setPageSize(30);

 DBRow ordersTasks[];

 if (cmd.equals("search"))
 {
 	ordersTasks = taskMgr.getOrderTasksByOid(oid,pc);
 }
 else 
 {
 	if ( adminLoggerBean.getAdgid()==100014l )
 	{
 		ordersTasks = taskMgr.getOrderTasksByPsId(adminLoggerBean.getPs_id(),pc);
 	}
 	else
 	{
 		ordersTasks = taskMgr.getOrderTasks(pc);
 	}
 	
 }
 %> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单任务</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--

.zebraTable tr td a:link {
	color:#000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:visited {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:hover {
	color: #000000;
	text-decoration: underline;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:active {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}



-->
</style>

<script>

function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.st.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.en.value = date;
}


</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable();">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 订单管理 »   订单任务</td>
  </tr>
</table>
<br>
<form name="form1" method="post" action="">
<input type="hidden" name="cmd" value="search">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>订单号 
      &nbsp;&nbsp;       
          <input name="oid" type="text" id="oid" value="<%=oid==0?"":oid%>">
          &nbsp;&nbsp;    
          <input name="Submit2" type="submit" class="button_long_search" value="搜索">
      </td>
  </tr>
</table>
</form>
<br>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
<thead>
<tr>
<th width="12%"   class="right-title"  style="text-align: center;">执行操作</th>
 <th width="8%"  class="right-title"  style="text-align: center;" >订单号</th>
    <th width="8%"  class="left-title" style="text-align: center;">任务原因</th>
    <th width="14%"  class="right-title"  style="text-align: center;" >任务备注</th>
    <th width="8%"  class="right-title"  style="text-align: center;" >仓库</th>
    <th width="12%" class="right-title"  style="text-align: center;" >创建人/日期</th>
    <th width="11%" class="right-title"  style="text-align: center;" >接收任务人</th>
    <th width="10%" class="right-title"  style="text-align: center;" >实际执行人</th>
    <th width="9%"  class="right-title"  style="text-align: center;" >状态</th>
    <th width="8%"   class="right-title" style="text-align: center;">&nbsp;</th>
</tr>
</thead>
<%
String storageColor = "";
for (int i=0; i<ordersTasks.length; i++)
{
	if ( ordersTasks[i].get("ps_id",0l)==100000l )
	{
		storageColor = "#000000";
	}
	else
	{
		storageColor = "#0000FF";
	}
%>
  <tr>
 <td align="center" valign="middle" style="color:#CC0000;font-weight:bold;" ><%=ordersTasks[i].getString("t_operation")%></td>
<td align="center" valign="middle"  ><a href="ct_order_auto_reflush.html?val=<%=ordersTasks[i].getString("oid")%>&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3" target="_blank"><%=ordersTasks[i].getString("oid")%></a></td>
    <td height="40" align="center" valign="middle"  ><%=orderTaskKey.getReasonById(ordersTasks[i].get("t_reason",0))%></td>
    <td align="center" valign="middle"  ><%=ordersTasks[i].getString("t_note")%>&nbsp;</td>
    <td align="center" valign="middle"  style="color:<%=storageColor%>;font-weight:bold;"><%=ordersTasks[i].get("ps_id",0l)%></td>
    <td align="center" valign="middle"  ><%=ordersTasks[i].getString("create_admin")%><br>
<%=ordersTasks[i].getString("create_date")%></td>
    <td align="center" valign="middle"  >
	<%
	if (ordersTasks[i].get("t_target_admin",0)!=0)
	{
		DBRow detailAdmin = adminMgr.getDetailAdmin(ordersTasks[i].get("t_target_admin",0));
		if (detailAdmin==null)
		{
			out.println("NULL");
		}
		else
		{
			out.println(detailAdmin.getString("account"));
		}
	}
	else if (ordersTasks[i].get("t_target_group",0)!=0)
	{
		DBRow detailRole = adminMgr.getDetailAdminGroup(ordersTasks[i].get("t_target_group",0));
		if (detailRole==null)
		{
			out.println("NULL");
		}
		else
		{
			out.println(detailRole.getString("name")+"(角色)");
		}
	}
	%>	</td>
    <td align="center" valign="middle"  >
	<%
	if (ordersTasks[i].getString("t_operator").equals("")==false)
	{
		out.println(ordersTasks[i].getString("t_operator")+"<br>"+ordersTasks[i].getString("t_operate_date"));
	}
	%>
&nbsp;	</td>
    <td align="center" valign="middle"  ><%=orderTaskKey.getStatusById(ordersTasks[i].get("t_status",0))%></td>
    <td align="center" valign="middle"  >
      <input name="Submit" type="button" class="short-button" value="查看详细" onClick="tb_show('详细任务内容','detail_task.html?ot_id=<%=ordersTasks[i].get("ot_id",0l)%>&height=450&width=800',false);">    </td>
  </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
    <input type="hidden" name="p">
	<input type="hidden" name="cmd" value="<%=cmd%>">
	<input type="hidden" name="oid" value="<%=oid%>">
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
</body>

</html>