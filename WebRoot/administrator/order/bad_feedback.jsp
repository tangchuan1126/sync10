<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="orderRateKey" class="com.cwc.app.key.OrderRateKey"/>
<%@ include file="../../include.jsp"%>
<%
long oid = StringUtil.getLong(request,"oid");
int status = StringUtil.getInt(request,"status");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>登记<%=orderRateKey.getOrderRateStatusById(status)%></title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script>

$().ready(function() {
	ajaxCartForm();
});

//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   beforeSubmit:  // pre-submit callback    
				function(){
					var haveSelect = false;
					var len = document.bad_feedback_form.bad_feedback_reason.length;
					for (i=0; i<len; i++)
					{
						if (document.bad_feedback_form.bad_feedback_reason[i].checked)
						{
							haveSelect = true;
						}
					}

					if ( !haveSelect )
					{
						alert("最少选择一个差评原因");
						return(false);
					}
					else
					{
						return(true);
					}					
				}	
		   ,  
		   success:       // post-submit callback  
				function(){
					parent.location.reload();
					parent.closeTBWin();
				}
	};
	

   $('#bad_feedback_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}
</script>

<style>
form {
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 <form name="bad_feedback_form" id="bad_feedback_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/AddBadFeedBack.action">
 <input  type="hidden" name="oid" value="<%=oid%>">
 <input  type="hidden" name="status" value="<%=status%>">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><br>
     
      <table width="580" border="0" align="center" cellpadding="4" cellspacing="0">
      <tr>
        <td width="19%" style="font-size:20px;font-family:'黑体';color:#999999"><%=orderRateKey.getOrderRateStatusById(status)%>订单</td>
        <td width="81%"  style="font-size:20px;font-family:Arial;color:#990000;font-weight:bold"><%=oid%></td>
      </tr>
      <tr>
        <td style="font-size:20px;font-family:'黑体';color:#999999"><%=orderRateKey.getOrderRateStatusById(status)%>原因</td>
        <td align="left" valign="top"  >
		<%
		DBRow badFeedbackReasons[] = orderMgr.getAllBadFeedBackReasons(); 
		for (int i=0; i<badFeedbackReasons.length; i++)
		{
		%>
			<div style="width:150px;float:left;height:25px;"><input type="checkbox" name="bad_feedback_reason" id="bad_feedback_reason_<%=badFeedbackReasons[i].getString("id")%>" value="<%=badFeedbackReasons[i].getString("id")%>"> <label for="bad_feedback_reason_<%=badFeedbackReasons[i].getString("id")%>"><%=badFeedbackReasons[i].getString("value")%></label></div>
		<%
		}
		%>
		</td>
      </tr>
    </table>
     
	</td>
  </tr>
  <tr>
    <td align="right" valign="middle" class="win-bottom-line">
      <input name="Submit" type="submit" class="normal-green" value="登记" >
      <input name="cancel" type="button" class="normal-white" value="取消" onClick="parent.closeTBWin();">
   </td>
  </tr>
</table>
 </form>
</body>
</html>