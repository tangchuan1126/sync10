<%@page import="com.cwc.app.key.ProductTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long wo_id = StringUtil.getLong(request,"wo_id");
DBRow items[] = orderMgr.getWarrantyOrderItemsByWoId(wo_id);
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>详细质保信息</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script>
$().ready(function() {
});
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

				
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" align="center" valign="middle" style="padding:5px;font-weight:bold;font-size:20px;">质保单 <%=wo_id%> </td>
        </tr>
      <tr>
        <td width="10%" align="center" valign="middle">质保商品</td>
        <td width="90%" style="padding:5px;"><table width="98%" border="0" cellpadding="3" cellspacing="1" bgcolor="#cccccc">
    <tr>
      <td width="9%" align="center" bgcolor="#eeeeee">数量</td>
      <td width="91%" bgcolor="#eeeeee">商品</td>
    </tr>
	  <%
for (int i=0; i<items.length; i++)
{
%>
    <tr>
      <td align="center" bgcolor="#ffffff"><%=items[i].get("quantity",0f)%>
	  		   			<%
					if (items[i].getString("unit_name").equals(""))
					{
						out.println("&nbsp;");
					}
					else
					{
						out.println(items[i].getString("unit_name"));
					}
				   %>
	  </td>
      <td bgcolor="#ffffff"><%=items[i].getString("name")%>
	  
	<%
		if (items[i].get("product_type",0)==ProductTypeKey.UNION_CUSTOM)
		{
							out.println("<table width='100%' border='0' cellspacing='0' cellpadding='3'>");
							
							DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( items[i].get("pid",0l) );
							
							for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
							{
								out.println("<tr>");
		
								out.println("<td width='65%' >&nbsp; ├ "+customProductsInSet[customProductsInSet_i].getString("p_name"));
							
								out.println("</td>");
								out.println("<td width='35%' >"+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
								
					
	
								out.println("</td></tr>");
							}
							out.println("</table>");
		}

		%>	
	  
	  </td>
    </tr>
	<%
	}	
	%>
  </table>		</td>
      </tr>
    </table></td></tr>
  <tr>
    <td align="right" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green" value="确定" onClick="parent.tb_remove();">
    </td>
  </tr>
</table>

</body>

</html>