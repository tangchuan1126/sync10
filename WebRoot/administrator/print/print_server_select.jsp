<%@page import="java.util.HashMap"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.BCSKey"%>
<%@page import="com.cwc.app.key.PrintTaskKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<%
	DBRow[] printServers = androidPrintMgr.getAllPrintServer();
	//得到所有的PrintServer 然后选择
	Map<String,List<DBRow>> fixValueMap =  androidPrintMgrZr.fixAllPrintServer(printServers);
	
	 String checkPrintServerIsInUse =  ConfigBean.getStringValue("systenFolder")+"action/administrator/printServer/CheckPrintServerIsInUseAction.action";

 	
%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Print Sever Select</title>
<!-- 基本css 和javascript -->
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script> 

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<!-- <script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script> -->
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>  
 

 
 

<script type="text/javascript">
var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法

api.button([
          	{
          			name: 'Submit',
          			callback: function () {
          				submitPrintServer();
          				return false ;
          			},
          			focus:true
          			
          	},{name: 'Cancel' }]);
        
 
function submitPrintServer(){
	var $Node = $(".selected") ;
	if($Node.length < 1){
		alert("Select Print Server");
	}else{
 		var print_name = $Node.attr("data-print-name");
 		var print_id = $Node.attr("data-print-id");
 		ajaxGetPrintServerIsInUse(print_id,print_name);
	}
}
function ajaxGetPrintServerIsInUse(print_id,print_name){
	$.ajax({
		url: '<%= checkPrintServerIsInUse%>'+"?print_server_id="+print_id,
		dataType:'json',
		success:function(data){
 			if(data.ret *  1 == 1){
				if(data.isuse ===  "true"){
					$.artDialog.opener  && $.artDialog.opener.alertMessage('<b>  Print server ['+print_name+'] being used.<br />If Server Unappropriated , try after 60s </b>');
					return ;
				}
				$.artDialog && $.artDialog.close();
				$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow(print_id,print_name);
			}else{
				$.artDialog.opener && $.artDialog.opener.alertMessage('<b>  System Error .</b>');
			}
 		},
		error:function(XHR, TS){
			$.artDialog.opener && $.artDialog.opener.alertMessage('<b>  System Error .</b>');
		}
	})
}
 function selectPrintServer(print_server_id){
	var li = $("#a_"+print_server_id);
 	$("li a").removeClass("selected");
	li.addClass("selected");
}

 
</script>


<style type="text/css">
 	a{text-decoration: none;}
 	*{margin:0px;padding:0px;}
 
 	ul.printServer{list-style: none;border:0px solid silver;width:100%;}
 	ul.printServer li{line-height:30px;height:30px; }
 
 	ul.printServer li a{display:block;width:100%;color:black;text-align: left;text-indent: 30px;}
 	ul.printServer li a.even{background-color:#E6F3C5;}
 
 	ul.printServer li a.selected{background-image:url('../imgs/ok.png'); background-repeat :no-repeat;background-position:3px 7px;}
</style>

</head>
  
   <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="text-align: center;width:99%;margin: 0px auto;" >
    	<div style="text-align:center; font-weight:bold; margin-top:5px;border:1px solid silver ; padding:10px;  text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5); border-radius: 4px;background-color: #f2dede;border-color: #eed3d7;    color: #b94a48;">
  	 	 		Select Print Server
  	 	</div>
  	 	<div style="margin-top:4px;">
  	 		 
  	 		 <%
  	 		 	if(fixValueMap != null && fixValueMap.size() > 0 ){
  	 		 		Set<Map.Entry<String, List<DBRow>>> set = fixValueMap.entrySet();
  	 		 		Iterator<Map.Entry<String, List<DBRow>>> it = set.iterator();
  	 		 	 	while(it.hasNext()){
  	 		 	 		Map.Entry<String, List<DBRow>> entry = it.next() ;
  	 		 	 		List<DBRow> printArrays = entry.getValue() ;
  	 		 	 		%>
  	 		 	 			<div style="border: 1px solid silver;margin-top: 2px;">
	  	 		 	 			<h1 style="background: threeddarkshadow;color:white;text-align: left;text-indent: 2px;width: 100%;"><%= entry.getKey() %></h1>
	  	 		 	 			 <ul class="printServer">
						  	 		<%if(printArrays != null && printArrays.size() > 0){ %>
								    		<%
								    		for(int index = 0 , count = printArrays.size() ; index < count ; index++ ){
								    			DBRow row = printArrays.get(index);
								    			String className = (index % 2 == 0 ? "even" : "" );
								    			%>
	 							    				<li><a id="a_<%= row.get("printer_server_id", 0l)%>" data-print-name='<%=row.getString("printer_server_name") %>' data-print-id='<%= row.get("printer_server_id", 0l) %>' class="<%= className%>" href="javascript:void(0)" onclick="selectPrintServer('<%= row.get("printer_server_id", 0l)%>');"><%=row.getString("printer_server_name") %></a></li>
								    			<% 
								    		}
						  	 		 }
							    	%>
					 			</ul>	
				 			</div>
  	 		 	 		<% 
  	 		 	 	}
  	 		 	}
  	 		 %>
   	 	</div>
    	
  </body>
</html>
