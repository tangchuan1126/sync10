<%@page import="com.cwc.app.key.BCSKey"%>
<%@page import="com.cwc.app.key.PrintTaskKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<% 
    	String psId=StringUtil.getString(request,"ps_id");
		DBRow[] allStorageKml = googleMapsMgrCc.getAllStorageKml();
%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print Task</title>
<!-- 基本css 和javascript -->
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script> 

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
 
 

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

   
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

  

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
 
       
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
 
 
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<style type="text/css">
	table{border-collapse:collapse ;}
 	td { border:1px solid silver}
span.stateValue {
    display: block;
    float: left;
    font-weight: normal;
    overflow: hidden;
    text-align: left;
    text-indent: 2px;
    width: 80%;
}

span.stateName {
    float: left;
    font-weight: normal;
    text-align: right;
    width:40px;
}
.alert-text {
    color: #993300;
}
</style>
 
 
 <script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '230px', 
			top:            '35%', 
			left:           '20%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '1px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
</script>

<script type="text/javascript">
var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
jQuery(function(){
	api.button([
              		{
              			name: 'Submit',
              			callback: function () {
              				addServer();
              				return false ;
              			},
              			focus:true
              			
              		},{name: 'Cancel' }]
	        );
	
	if("<%=psId%>"!=""){
		$("#storage").val("<%=psId%>");
	}else{
		$("#storage option:first").prop("selected", 'selected');
	}
	initPs();
})
function initPs(){
	var varPsId = '<%= psId%>' * 1  ;
 	if(varPsId > 0){
		$("#storage option[value='"+varPsId+"']").attr("select",true);
		$("#storage").attr("disabled","true");
	}
}
function addServer(){
	if(validate()){
		ajaxAddServer();
	}
}
function ajaxAddServer(){
	 var title =$("#storage").find("option:selected").text();
	 var psId =$("#storage").val();
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/checkin/PrintServerSetOperAction.action',
		data:$("#addForm").serialize()+"&title="+title+"&ps_id="+psId,
		dataType:'json',
		beforeSend:function(request){
		  $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
			$.unblockUI();
			if(data.ret * 1 == 1){
				windowClose();
			}else{
				if(data.err * 1 == '<%= BCSKey.AndroidPrintServerNameExitsException%>'*1){
					showMessage("Server Name is Exit.","alert");
					$("#printer_server_name").val("").foucs();
				}
			}
		},
		error:function(){
		    $.unblockUI();
		    showMessage("System Eror,Try Later.","error")
		}
	})
	
}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function validate(){
	var printer_server_name = $("#printer_server_name").val();
	var employe_name = $("#employe_name").val();
	if(printer_server_name.length < 1){
		showMessage("Input Server Name . eg (WareHouse)","alert");
		$("#printer_server_name").focus();
		return false ;
	}
	/*if(employe_name.length < 1){
		showMessage("Select Relate User","alert");
		return false;
	}*/
	return true ;
}
function selectUser(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:1, 					// 1表示的 单选
			 user_ids:$("#adid").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShow'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: 'User List',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
};
function setParentUserShow(user_ids , user_names){
 	$("#adid").val(user_ids);
	$("#employe_name").val(user_names);
};

function selectZone(){
 	var storage = $("#storage").val();
 	var area_name = $("#zone").val();
	var area_id = $("input[name=area_id]").val();
	var _title ="Select Zone ";
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/select_zone.html?psId='+storage+'&area_name='+area_name+'&area_id='+area_id;
	$.artDialog.open(url, {title: _title,width:'825px',height:'435px', lock: true,opacity: 0.3});
}
function backFillZone(names,ids,eleId){
	$("#zone").val(names);
	$("#area_id").val(ids);
}
</script>
</head>
  
   <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="text-align: center;" >
   <form id="addForm">
   <input type="hidden" name="Method" value="Add"/>
  	 	 <table style="width: 100%;">
  	 	 	<tr>
  	 	 		<td style="height:40px;font-weight:bold;">&nbsp;Server Name:&nbsp;</td>
  	 	 		<td>&nbsp;<input type="text" name="printer_server_name" id="printer_server_name" style="line-height:25px;" /> <span style="color:red;">*</span>(eg WareHouse)&nbsp;</td>
  	 	 	</tr>
  	 	 	<tr>
  	 	 		<td style="height:40px;font-weight:bold;">&nbsp;Storage:&nbsp;</td>
  	 	 			<td>
							&nbsp;<select id="storage" style="width: 135px;">
									<%
								if (allStorageKml != null) {
									for (int i = 0; i < allStorageKml.length; i++) {
										DBRow kml = allStorageKml[i];
							         %>
									<option value="<%=kml.getString("ps_id")%>" ><%=kml.getString("title")%></option>
									<%
			                         }
								}  
			                         %>
							</select>
					</td>
  	 	 	</tr>
  	 	 	<tr>
					<td style="height:40px;font-weight:bold;" >&nbsp;Zone：&nbsp;</td>
								<td align="left" >&nbsp;<input type="text"
									style="width:300px;line-height:25px;" name="zone" id="zone"
									value="" onfocus="selectZone()" />
								<input type="hidden"
									style="width: 100%;" name="area_id" id="area_id"
									value=""/>
					</td>
			</tr>
  	 	 </table>
  	 	</form>
  </body>
</html>
