<%@page import="org.apache.tomcat.util.http.Cookies"%>
<%@page import="com.cwc.app.key.PrintTaskKey"%>
<%@page import="com.cwc.app.api.zr.AndroidPrintMgr"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>


<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print Task</title>
<!-- 基本css 和javascript -->
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script> 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

 
 
 

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

   
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

  

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
 
<link rel="stylesheet" href="../js/file-upload/css/bootstrap.min.css">
       
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>


<link rel="stylesheet" type="text/css" href="../js/art/skins/idialog.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
 <link rel="stylesheet" href="../js/file-upload/css/bootstrap.min.css">
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
 
 
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>


<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type='text/javascript' src='../../dwr/engine.js'></script>
<script type='text/javascript' src='../../dwr/interface/androidPrintMgr.js'></script>	
<script src="../js/printTask/printTask_copy.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<script type="text/javascript">
dwr.engine._errorHandler = function(message, ex) {};  
</script>
<style type="text/css">
 *{font-size:12px;}
span.stateValue {
    display: block;
    float: left;
    font-weight: normal;
    overflow: hidden;
    text-align: left;
    text-indent: 2px;
    width: 80%;
}

span.stateName {
    float: left;
    font-weight: normal;
    text-align: right;
    width:40px;
}
.alert-text {
    color: #993300;
}
.ui-tabs .ui-tabs-nav li span{font-size:9px;padding: 0.5em 1em;background: url('../imgs/print_task.png');background-repeat:no-repeat;background-position: 7px 4px;color:white;}
</style>
 
 <%
 /**
	0.通过url参数去读取print_id,print_name.如果没有再去读取cookie。
	1.通过request读取Cookie，如果有值，那么就直接显示然后查询。(ajax)
	2.如果cookie里面没有值，那么就需要弹出页面让其选择一个PrintServer
	3.
 */
 	String print_name = StringUtil.getString(request, "print_name");
	String print_id = StringUtil.getString(request, "print_id");
	
  	if(StringUtil.isNull(print_id) || StringUtil.isNull(print_name)){
  		//读取cookie
  		Cookie[]  cookies =	request.getCookies() ;
  		if(cookies != null && cookies.length > 0){
  			for(Cookie cookie : cookies){
  				if(cookie.getName().equals("print_name")){
  					print_name = cookie.getValue() ;
  				}
  				if(cookie.getName().equals("print_id")){
  					print_id = cookie.getValue() ;
  				}
  			}
  		}
  	}
	
 %>

<script type="text/javascript">
 var varPrintId = '<%= print_id %>';
 var varPrintName = '<%= print_name %>' ;
 
 var printTask  ;
 function countPrintTask(data,print){
    	data = eval('('+data+')');
	if(printTask && data && data.adid > 0){
   		printTask.appendData(data); 	
 	}
 }
 function staticPrintFailed(id){
	 if(printTask){
		 printTask.updatePrintTaskState(0);
	 }
}
 if(varPrintId * 1 > 0){
 	 androidPrintMgr.setPrintTaskNofityWho(varPrintId,function(data){});
  }
 jQuery(function(){
	  $("#tabs").tabs({
			cache: true,
			cookie: { expires: 30000 } 
	  });
	  initPrintServer();
 })
  function initPrintServer(){
 
	 if(varPrintId * 1 > 0 && varPrintName.length > 0){
		 printTask =  new PrintTask($("#print_count"),'<%=ConfigBean.getStringValue("systenFolder")%>',window);
 		 ajaxLoadPrintTask(varPrintId);
 	 }else{
 		 openSelectPrintServer();
	 }
 }
 
setInterval("loadPrintTaskInterval()", '<%= AndroidPrintMgr.RefreshTimes%>' * 1000);

 function loadPrintTaskInterval(){
  	if(printTask && printTask.isPrintOver) {
  		ajaxLoadPrintTask( varPrintId);
 	}
 }
 /**
  * 每隔2分钟去执行一次，如果当前 printTask 。是在打印中就不用了.
  * 如果一个请求超过了10s，那么我们就认为他是超时了.
  */

 function ajaxLoadPrintTask(varPrintId){
 
  	var request = $.ajax({
 		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/checkin/LoadPrintTaskAction.action?print_server_id='+varPrintId +"&status="+'<%= PrintTaskKey.ToPrint%>',
 		dataType:'json',
 		success:function(data){
  			 if(data && data.datas.length > 0 ){
   				 printTask.initData(data.datas);
 			 }
  			 if(data && data.printfailed.length > 0 ){
   				 printTask.initFailedData(data.printfailed);
 			 }
  		
 			 if(data){
 				 //是否设置打印服务器在线
   				 if(data.flag === "true"){
  					 androidPrintMgr.setPrintTaskNofityWho(data.who);
  				 }
 			 }
 		},
 		error:function(){
 			request.abort();
 		}
 	}) 
 }
 
 function openSelectPrintServer(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/print/print_server_select.html";
	 $.artDialog.open(uri,{title: "Add Print Server",width:'300px',height:'80%', lock: true,opacity: 0.3,fixed: true,close:function(){
		 tipSelectPrintServer();
	 }});	 
 }
 function alertMessage(message){
	 $.artDialog({
		 lock: true,opacity: 0.3,fixed: true,
	    title: 'Message',
		content: message,
		icon: 'warning',
		cancel: false,
		button:[
		       	{
		       		name: 'Yes',
					callback: function () {
 						return true ;
					},
					focus:true
				} 
		       ]
	    
	});
}
 //提示选择PrintServer
 function tipSelectPrintServer(){
	 if(varPrintId * 1 < 1){
		 $.artDialog({
				 lock: true,opacity: 0.3,fixed: true,
			    title: 'Message',
				content: '<b>Select Print Server First.</b>',
				icon: 'warning',
				cancel: false,
				button:[
				       	{
				       		name: 'Yes',
							callback: function () {
								openSelectPrintServer();
								return true ;
							},
							focus:true
						} 
				       ]
			    
			});
	 }
 
 }
 function refreshWindow(print_id,print_name){
	 var url = window.location.href ;
	 $.cookie('print_id',print_id);
	 $.cookie('print_name',print_name);
	 if(url.indexOf("?") != -1){
		 url = url.substr(0,url.indexOf("?"));
	 }
  	 window.location.href = url+"?print_id="+print_id+"&print_name="+print_name;
 }
 /**
 	里面保存一个print_index .然后通过这个index 去获取URL ，做一些操作.
 */
 function showHtml(_this){
	 var node = $(_this);
	 var print_task_id = (node.attr("task_print_task_id"));
	 var obj = printTask.getPrintTaskById(print_task_id);
	 if(obj && obj.url){
		 $.artDialog.open(obj.url,{title: "Preview",width:'80%',height:'80%', lock: true,opacity: 0.3,fixed: true});	 
	 }
 }
 /**
 	
 */
 function showFailedHtml(_this){
	 var node = $(_this);
	 var print_task_id = (node.attr("task_print_task_id"));
	 var obj = printTask.getPrintFailedTaskById(print_task_id);
	 if(obj && obj.url){
		 $.artDialog.open(obj.url,{title: "Preview",width:'80%',height:'80%', lock: true,opacity: 0.3,fixed: true});	 
	 }
 }
 //提示是否取消当前的任务
 function cancel(_this){
	 var node = $(_this);
	 var task_id = node.attr("task_print_task_id") * 1 ;
	 $.artDialog({
		 	title:'Notify',
		 	lock: true,
		 	opacity: 0.3,
		 	icon:'question',
		    content: "<span style='font-weight:bold;'>Cancel ["+task_id+"] ?</span>",
		    button: [
		        {
		            name: 'YES',
		            callback: function () {
		            	ajaxCancel(task_id);
		            },
		            focus: true
		        },
		        {
		            name: 'NO'
		        }
		    ]
		});
 }
 //提交取消当前的任务
 function ajaxCancel(task_id){
	  $.ajax({
		  url:'<%=ConfigBean.getStringValue("systenFolder")%>action/checkin/PrintTaskOperAction.action',
		  data:'task_id='+task_id+'&Method=cancel',
		  dataType:'json',
		  beforeSend:function(request){
		      $.blockUI({message: 'Submit... '});
		  },
		  success:function(data){
			  $.unblockUI();
			  if(data.ret * 1 == 1){
				  printTask.removePrintFailedTask(task_id);
 			  }
			 
		  },
		  error:function(){
			  $.unblockUI();
		 	  showMessage("System Error.","error");
		  }
	  })
 }
 
 /**
  1.提示是否重新打印该任务
  2.ajax->后台—>推送到前台 .  || 通过在页面上 在json中添加数据的方式去做
 */
 function reprint(_this){
	 var node = $(_this);
	 var print_task_id = node.attr("task_print_task_id") * 1 ;
		 
	 $.artDialog({
		 	title:'Notify',
		 	lock: true,
		 	opacity: 0.3,
		 	icon:'question',
		    content: "<span style='font-weight:bold;'>Reprint ["+print_task_id+"] ?</span>",
		    button: [
		        {
		            name: 'YES',
		            callback: function () {
		            	ajaxReprint(print_task_id);
		            },
		            focus: true
		        },
		        {
		            name: 'NO'
		        }
		    ]
		});
	 
	
 }
 function ajaxReprint(task_id){
	 $.ajax({
		  url:'<%=ConfigBean.getStringValue("systenFolder")%>action/checkin/PrintTaskOperAction.action',
		  data:'task_id='+task_id+'&Method=reprint',
		  dataType:'json',
		  beforeSend:function(request){
		      $.blockUI({message: 'Submit. '});
		  },
		  success:function(data){
			  $.unblockUI();
			  if(data.ret * 1 == 1){
				  printTask.printFailedReprint(task_id);
			  }
		  },
		  error:function(){
			  $.unblockUI();
		 	  showMessage("System Error.","error");
		  }
	  })
 }
 
</script>
</head>
  <!-- 
  zhangrui_self
  		1.printServer 的值应该保存到cookie中
  		2.在没有printServer的时候，默认打开选择printServer,但是他可以进行修改。每次修改过后要重新刷新页面.
   -->
   <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="width: 98%;" onLoad="dwr.engine.setActiveReverseAjax(true);">
   	 <div style="text-align:center; margin-top:5px;border:1px solid silver ; padding-top:10px; padding-bottom:10px; padding-left:6px; width:100%; text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5); border-radius: 4px;background-color: #f2dede;border-color: #eed3d7;    color: #b94a48;">
  	 	 
  	 	 <a herf="javascript:void(0)" onclick="openSelectPrintServer();" style="cursor:pointer;" title="Change Print Server">
			 <span style="text-align: center;font-size:14px;font-weight: bold;color: black;">
			     	PrintServer : <span id="print_server_name"> <%=print_name %></span>
			 </span>
		 </a>
  	 </div>
  	 			<!-- will print template -->
  				<div id="will_print_template" style="border:1px solid silver;padding:10px;margin-top:4px;display:none;">
	 	 		
		 	 	 	<p style="font-weight:bold;font-size:14px;color:green;"> <span class="task_state">WillPrint</span> [<span class="task_print_task_id">12132</span>] </p>
	 	 	 				<span class="task_url" style="display:none;"/></span>
		 	 				  <div style="float:left;width:80%;">
		 	 				     <p class="task_title" style="font-weight:bold;font-size:12px;">Print Title</p>
		 	  	
		 	 				 	  <div style="margin-top:8px;">
		 	 					    <span class="alert-text stateName">
								  		<img src="../imgs/order_client.gif" title="Proposer">
							    	</span>
							    	<span class="stateValue">&nbsp;&nbsp;<span class="task_user_name">Proposer</span></span>
							    	<div style="clear:both;"></div>
			 	 				  </div>
			 	 				  
			 	 				   <div style="margin-top:2px;">
		 	 					    <span class="alert-text stateName">
		 	 					    	From
								    	</span>
							    	<span class="stateValue">&nbsp;&nbsp;<span class="task_from_where">from Where</span></span>
							    	<div style="clear:both;"></div>
			 	 				  </div>
			 	 				  
			 	 				  <div style="margin-top:2px;padding-bottom:2px;">
		 	 					    <span class="alert-text stateName">
		 	 					    	Printer
								    	</span>
							    	<span class="stateValue">&nbsp;&nbsp;<span class="task_who">PrintSeverName</span></span>
							    	<div style="clear:both;"></div>
			 	 				  </div>
		 	  				 </div>
		 	  				 <div class="task_state state_div" style="float:left;line-height:80px;height:80px;border:0px solid silver;width:19%;font-weight:bold;font-size:18px;color:green;">
		 	  				 		WillPrint...
		 	  				 </div>
		 	 			 
	 	 				    <div style="margin-top:4px;clear:both;border-top:1px dashed  silver;padding-top:8px;">
		 	 				 	 <button class="btn btn-info start" task_print_task_id="" onclick="showHtml(this)">
		              					<i class="icon-search icon-white"></i>
		              				Preview
						 		 </button>
		 	 				  </div>
	 	 			</div>
	 	 			<!-- print failed -->
	 	 		<div id="will_print_failed_template" style="border:1px solid silver;padding:10px;margin-top:4px;display:none;" >
	 	 		
		 	 	 	<p style="font-weight:bold;font-size:14px;color:green;"> <span class="task_state">WillPrint</span> [<span class="task_print_task_id">12132</span>] </p>
	 	 	 				<span class="task_url" style="display:none;"/></span>
		 	 				  <div style="float:left;width:80%;">
		 	 				     <p class="task_title" style="font-weight:bold;font-size:12px;">Print Title</p>
		 	  	
		 	 				 	  <div style="margin-top:8px;">
		 	 					    <span class="alert-text stateName">
								  		<img src="../imgs/order_client.gif" title="Proposer">
							    	</span>
							    	<span class="stateValue">&nbsp;&nbsp;<span class="task_user_name">Proposer</span></span>
							    	<div style="clear:both;"></div>
			 	 				  </div>
			 	 				  
			 	 				   <div style="margin-top:2px;">
		 	 					    <span class="alert-text stateName">
		 	 					    	From
								    	</span>
							    	<span class="stateValue">&nbsp;&nbsp;<span class="task_from_where">from Where</span></span>
							    	<div style="clear:both;"></div>
			 	 				  </div>
			 	 				  
			 	 				  <div style="margin-top:2px;padding-bottom:2px;">
		 	 					    <span class="alert-text stateName">
		 	 					    	Printer
								    	</span>
							    	<span class="stateValue">&nbsp;&nbsp;<span class="task_who">PrintSeverName</span></span>
							    	<div style="clear:both;"></div>
			 	 				  </div>
		 	  				 </div>
		 	  			 
		 	 			 
	 	 				    <div style="margin-top:4px;clear:both;border-top:1px dashed  silver;padding-top:8px;">
		 	 				 	 <button class="btn btn-info start" task_print_task_id="" onclick="showFailedHtml(this)">
		              				<i class="icon-search icon-white"></i>
		              					Preview
						 		 </button>
						 		  <button class="btn btn-success start" task_print_task_id="" onclick="reprint(this)">
				              		<i class="icon-plus icon-white"></i>
				              			Reprint
								  </button>
								   <button class="btn btn-warning start" task_print_task_id="" onclick="cancel(this)">
				              			<i class="icon-ban-circle icon-white"></i>
				              			Cancel
								  </button>
		 	 				  </div>
	 	 			</div>
  	 	<div id="tabs" style="width:100%;margin-left:2px;margin-top:3px;">
			 <ul>
				 <li><a href="#print">Printing</a><span id="print_count">0</span></li>
				 <li><a href="#failed">Print Fail</a><span id="print_failed_count">0</span></li>
 			 </ul>
			 <div id="print">
			 	  <p style='padding:10px;border:1px dashed silver'>No Records</p>
		  	 </div>
		  	  <div id="failed">
			 	  <p style='padding:10px;border:1px dashed silver'>No Records</p>
		  	 </div>
	   </div>
 	<div id="androidPrint" style="display:none;"></div>
  </body>
</html>
