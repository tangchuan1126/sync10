<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.ModuleKey"%>
<jsp:useBean id="shortMessageKey" class="com.cwc.app.key.ShortMessageKey"></jsp:useBean>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>短信列表</title>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.filter.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.filter.min.js"></script>	

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<script src="../js/fullcalendar/colorpicker.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/css/colorpicker.css" rel="stylesheet" />
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
	tbody.mytbody tr td{
		line-height:30px;
		height:30px;
	}
</style>
<%

	PageCtrl pc 				= new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(10);
	String cmd					= StringUtil.getString(request, "cmd");
	
	String joinUserId			= StringUtil.getString(request, "join_user_id");
	DBRow[] shortMessages		= shortMessageMgrZyj.getShortMessageList(pc, joinUserId);
	ModuleKey moduleKey			= new ModuleKey();
	DBRow[] userInfoDept		= scheduleMgrZR.getAllUserInfo();	 
	long excuteUserId			= "".equals(StringUtil.getString(request,"excute_user_id"))?0L:Long.parseLong(StringUtil.getString(request,"excute_user_id"));
	DBRow executeUserInfo		= new DBRow();
	if(adminMgr.getDetailAdmin(excuteUserId) != null){
		executeUserInfo			= adminMgr.getDetailAdmin(Long.parseLong(StringUtil.getString(request,"excute_user_id")));
	}
 %>
 <script type="text/javascript">
 	var userInfoDept =  <%= new JsonObject(userInfoDept).toString()%>;
 	var newa = [];
 	 $(function(){
		 $("#tabs").tabs({
				cache: true,
				spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
				cookie: { expires: 30000 } ,
			});
		initDept();
	  });
	function initDept(){
		for(var index = 0 , count =  userInfoDept.length ; index < count ; index++ ){
			var flag = true;
			for(var j = 0 , i = newa.length ; j < i ; j++ ){
				if(newa[j] && (newa[j].deptId === userInfoDept[index].adgid)){
					flag = false;
					break;
				}
			}
			if(flag){ //new dept
				var objs = new Object();
				objs.deptName = userInfoDept[index].name;
				objs.deptId = userInfoDept[index].adgid;
				objs.values = [];
				objs.values.push(userInfoDept[index]); 
				newa.push(objs);
			} else{
				newa[newa.length - 1].values.push(userInfoDept[index]);	
			}
		} 
		 var selected = "<option value='0'>--请选择--</option>";
		 for(var index = 0 , count = newa.length ; index < count ; index++ ){
			var optgroup = "<optgroup label='"+newa[index].deptName+"'>";
			for(var j = 0 , length =  newa[index].values.length ; j < length ; j++ ){
				var option		= "";
				var userIdStr	= $("#joinUserId").val();
				if(userIdStr == newa[index].values[j].adid){
					option = "<option value='"+newa[index].values[j].adid+"' selected='selected'>"+newa[index].values[j].employe_name+"</option>";
				}else{
					option = "<option value='"+newa[index].values[j].adid+"'>"+newa[index].values[j].employe_name+"</option>";
				}
				
				optgroup += option;
			}
			optgroup +="</optgroup>";
			selected += optgroup;
		 } 
		 var $selected = $(selected);
		
		 $selected.find("option[value='"+"<%=executeUserInfo.getString("adid") %>"+"']").attr("disabled",true);
	 	 var join_user_idSelected = $("#join_user_id").append($selected);
	 



		 //任务参与人
		join_user_idSelected.multiselect({
			 checkAllText:'全选',
			 uncheckAllText:'取消',
			 noneSelectedText:'选择任务参与人',
			 selectedText: '# 个人已选择',
			 selectedList: 1,
			 multiple:false,
			 minWidth:400,
			
		 }).multiselectfilter({
			 label:'搜索',
			 placeholder:'输入姓名'
		 });
		
	}
 	function addShortMessage(){
 	 		var fieldValueUrl = '&shmeModuleId='+$("#shmeModuleId").val()+'&shmeBusinessId='+$("#shmeBusinessId").val()+'&shmeBusinessInfo='+$("#shmeBusinessInfo").val();
			var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/short_message/short_message_add.html?1=1"+fieldValueUrl;
			$.artDialog.open(url , {title: '发送短信测试',width:'800px',height:'500px', lock: true,opacity: 0.3,fixed: true});
	};
	function refreshWindow(){
		window.location.reload();
	};
	function go(index){
		var form = $("#pageForm");
		$("input[name='p']",form).val(index);
		form.submit();
	}
 	 
 </script>
</head>
<body onload= "onLoadInitZebraTable()">
	<div id="tabs">
		<ul>
			<li><a href="#tools">常用工具</a></li>
			<li><a href="#search">高级搜索</a></li>
		</ul>
		<div id="tools"> 
			<form action="short_message_list.html" method="post">
				<input type="hidden" name="cmd" value="tools"/>
				
				<input type="submit" value="查询" class="button_long_search"/>
			</form>
		</div>
		<div id="search">
			<form action="short_message_list.html" method="post">
					<input type="hidden" name="cmd" value="search"/>
					<input type="hidden" id="joinUserId" value="<%=joinUserId%>"/>
						设置收信人：<select style='width:300px;' name="join_user_id"  id="join_user_id"> </select>
					<input type="submit" value="查询" class="button_long_search"/>
				</form>
		</div>
	</div>
	

<form id="subForm" action="" method="post">
	<input type="button" class="long-button-add" onclick="addShortMessage()" value="添加"/>
	<input type="hidden" name="shmeModuleId" id="shmeModuleId" value='<%=ModuleKey.PURCHASE_ORDER %>'/>
	<input type="hidden" name="shmeBusinessId" id="shmeBusinessId" value='2'/>
	<input type="hidden" name="shmeBusinessInfo" id="shmeBusinessInfo" value='用户操作的订单号为：or-2312'/>
</form>	

	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"  class="zebraTable" style="margin-left:3px;margin-top:5px;">
		  <tr> 
		  	<th width="13%" style="vertical-align: center;text-align: center;" class="right-title">模块名称</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">业务Id</th>
		  	<th width="13%" style="vertical-align: center;text-align: center;" class="right-title">发送人</th>
		  	<th width="13%" style="vertical-align: center;text-align: center;" class="right-title">发送时间</th>
	        <th width="30%" style="vertical-align: center;text-align: center;" class="right-title">发送内容</th>
	        <th width="21%" style="vertical-align: center;text-align: center;" class="right-title">接收人及接收状态</th>
	      </tr>
	      <tbody class="mytbody">
	      	<%
	      		if(null != shortMessages && shortMessages.length >0 ){
	      			for(DBRow shortMessage:shortMessages){
	       %>			
	      		<tr>
	      			<td><%=moduleKey.getModuleName(shortMessage.get("module_id",0)+"") %></td>
	      			<td><%=(0==shortMessage.get("business_id",0))?"":shortMessage.get("business_id",0) %></td>
	      			<td><%=shortMessageMgrZyj.getUserInfoByUserId(shortMessage.get("send_user",0)).getString("employe_name")     %></td>
	      			<td><%=shortMessage.getString("send_time") %></td>
	      			<td><%=shortMessage.getString("content") %></td>
	      			<td>
	      				<table style="width: 100%">
	      					<%
	      						DBRow[] receivers = shortMessageMgrZyj.getShortMessageReceivers(shortMessage.get("id",0));
	      						if(null != receivers && receivers.length > 0){
	      							for(int i = 0; i < receivers.length; i ++){
	      								DBRow receiver = receivers[i];
	      					%>		
	      							<tr style="width: 100%">
	      								<td style="width: 30%"><%=receiver.getString("receiver_name")  %></td>
	      								<td style="width: 45%"><%=receiver.getString("receiver_phone") %></td>
	      								<td style="width: 25%"><%=shortMessageKey.getShortMessageKeyName(receiver.getString("is_success")) %></td>
	      							</tr>
	      					<%	
	      							}
	      						}
	      					 %>
	      				</table>
	      			</td>
	      		</tr>	
	      			
	      	<%
	      			}
	      		}
	      	 %>
	
		  </tbody>
	</table>
	<form action="" id="pageForm">
			<input type="hidden" name="p" id="pageCount"/>
		
			<input type="hidden" name="cmd" value="<%=cmd %>"/>
	</form>
	  <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
		  <tr>
		    <td height="28" align="right" valign="middle">
		    <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
				%>
		      跳转到
		      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
		        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
		    </td>
		  </tr>
		</table>
</body>
</html>