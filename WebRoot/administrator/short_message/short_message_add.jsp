<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>发送短信测试</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
	div.cssDivschedule{
			width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
			background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
			background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
	
</style>
<script type="text/javascript">
$(function(){
	$(".cssDivschedule").css("display","none");
});
function submitData(formId){
		var formIdStr = "#"+formId;
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/short_message/AddShortMessage.action',
			data:$(formIdStr).serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag == "true"){
					showMessage("添加成功","success");
					setTimeout("windowClose()", 1000);
				}else{
					$(".cssDivschedule").css("display","none");
					showMessage("添加失败","error");
				}
			},
			error:function(){
				showMessage("系统错误","error");
			}
		})
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function addReceiverNameAndPhone(){
	var tr		= $("<tr style='width: 100%'></tr>");
	var td1		= $("<td style='width: 30%'><input id='receiver_name' name='receiver_name'/></td>");
	var td2 	= $("<td style='width: 50%'><input id='receiver_phone' name='receiver_phone'/></td>");
	var td3		= $("<td style='width: 20%'><input type='button' class='short-short-button-del' value='删除' onclick='delTr(this)'/></td>");
	tr.append(td1);
	tr.append(td2);
	tr.append(td3);
	$("#myTable").append(tr);
	
};
function delTr(thisTd){
	var delButParentNode = thisTd.parentNode.parentNode;
	while(delButParentNode.nodeName != 'TR'){
		delButParentNode = delButParentNode.parentNode;
	}
	$(delButParentNode).remove();
};

</script>
</head>
<body>
<div class="cssDivschedule" style=""></div>

	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			测试发送短信1
		</legend>	
		<form action="" id="subForm1">
			<input type="hidden" name="cmd" value="receivers"/>
			<table id="myTable" style="width: 100%">
				<tr style="width: 100%">
					<td style="width: 30%">收信人姓名</td>
					<td style="width: 50%">收信人电话</td>
					<td style="width: 20%">操作</td>
				</tr>
				<tr style="width: 100%">
					<td style="width: 30%"><input id="receiver_name" name="receiver_name"/></td>
					<td style="width: 50%"><input id="receiver_phone" name="receiver_phone"/></td>
					<td style="width: 20%"><input type="button" class="long-button-add" onclick="addReceiverNameAndPhone()" value="添加"/></td>
				</tr>
			</table>
		</form>
	</fieldset>
	<table align="right">
		<tr align="right">
			<td colspan="2"><input type="button" class="normal-green" value="提交" onclick="submitData('subForm1');"/></td>
		</tr>
	</table>
	
	
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			测试发送短信2
		</legend>	
		<form action="" id="subForm2">
			<input type="hidden" name="cmd" value="adgId"/>
			<table id="myTable" style="width: 100%">
				<tr style="width: 100%">
					<td style="width: 40%">选择部门</td>
		             <td align="left" style="width: 60%">
				      <select name="proAdgidName" id="proAdgid">
						  <%
							DBRow adminGroup[] = adminMgr.getAllAdminGroup(null);
							for (int i=0; i<adminGroup.length; i++){
							%>
						    	<option style="background:#fffffffff;" value="<%=adminGroup[i].getString("adgid")%>"><%=adminGroup[i].getString("name")%></option>
						   <%
							}
						  %>
				      </select> 
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<table align="right">
		<tr align="right">
			<td colspan="2"><input type="button" class="normal-green" value="提交" onclick="submitData('subForm2');"/></td>
		</tr>
	</table>
	
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			测试发送短信3
		</legend>	
		<form action="" id="subForm3">
			<input type="hidden" name="cmd" value="adgIdRoleId"/>
			<table id="myTable" style="width: 100%">
				<tr style="width: 100%">
					<td style="width: 40%">选择部门</td>
		             <td align="left" style="width: 60%">
				      <select name="proAdgidName" id="proAdgid">
						  <%
							DBRow[] adminGroup2 = adminMgr.getAllAdminGroup(null);
							for (int i=0; i<adminGroup2.length; i++){
							%>
						    	<option style="background:#fffffffff;" value="<%=adminGroup2[i].getString("adgid")%>"><%=adminGroup2[i].getString("name")%></option>
						   <%
							}
						  %>
				      </select> 
					</td>
				</tr>
				<tr style="width: 100%">
					<td style="width: 40%">选择角色</td>
					<td style="width: 60%">
						<select name="proJsIdName" id="proJsId" >
							<option value='1'>员工</option>
							<option value='5' >副主管</option>
							<option value='10'>主管</option>
						</select>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<table align="right">
		<tr align="right">
			<td colspan="2"><input type="button" class="normal-green" value="提交" onclick="submitData('subForm3');"/></td>
		</tr>
	</table>

	<script type="text/javascript">
	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>