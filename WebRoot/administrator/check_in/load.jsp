<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="reasonKey" class="com.cwc.app.key.LoadConsolidateReasonKey"/> 


<%
	long detailId = StringUtil.getLong(request, "dlo_detail_id");
	DBRow data = checkInMgrWfh.getPalletInfoByDetailId(detailId);
	
%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<head>
	
	
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../js/accordion/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/accordion/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="../js/accordion/jquery-ui.theme.min.css" />
	<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
	div.orders{padding:0px;margin: :0px;}
	div.pallets table td{border:1px solid silver;}
	span.order_info{font-size: 14px;font-weight: bold;}
	.single{}
	.double{}
	td.header{width: 25%;text-align: center;font-size: 14px;font-weight: bold;background: black;color: white;line-height: 25px;height: 25px;}
	</style>
	<script type="text/javascript">
	$(function() {
		$("#load").accordion({
			heightStyle: "content"
		});
		
		$(".orderInfo").click(function(){
			 $(this).next(".pallets").slideToggle("slow");
		})
	});
	
	</script>
</head>
<body style="font-size:12px">
	<div style="font-size:20px ;text-align: center;border: 1px solid silver;padding: 5px;">
		<B><%=moduleKey.getModuleName(data.get("number_type",0)) %>:<%=data.getString("number") %> </B>
	</div>

	 
				<div id="load">
				<% 
				DBRow[] datas =	(DBRow[])data.get("datas",new DBRow[]{});
				if(datas != null && datas.length > 0){
					for(DBRow masterbol : datas){
						DBRow[] orders = (DBRow[])masterbol.get("orders",new DBRow[]{});
					%>
							<h3>MasterBoL:<%=masterbol.getString("master") %> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								Total Pallet : <%=masterbol.get("total_pallets", 0) %>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								Pro No : <%=masterbol.get("pro_no", 0) %>
							</h3>
							<div class="orders" style="padding: 0px;padding: 10px;padding-top: 0px;">
						<%
							if(orders != null && orders.length > 0 ){
								for(DBRow order : orders){
									%>
									<div class="order" style="border:1px solid silver; margin-top:10px;background-color: white; padding: 4px;">
									<div class="orderInfo" style="cursor: pointer;">
										<div style="float: left;width:66% ; height: 54px;border-right:2px dashed  silver;">
											<span class="order_info">Order Number: <font color="#f60"><%=order.getString("order_no") %></font></span><br />
									 		<span class="order_info">Reference No: <%=order.getString("reference_no") %> </span><br />
											<span class="order_info">Pro No:  <%=order.getString("pro_no") %> </span><br />
									 	</div>
									 	<div style="float: left;line-height: 54px;height: 54px;text-align: center;">
									 			&nbsp;&nbsp;&nbsp;<span class="order_info">Pallets: <font color="#f60"><%=order.getString("pallets") %></font></span> 
									 	</div>
									 	<div style="clear: both; "></div>
									 </div>
									 		<div class="pallets" style="margin: 4px;display: none;">
							 				 	<table  style="border-collapse :collapse;border:1px solid silver;width: 100%;">
							 				 		<thead>
							 				 			<td class="header" >Pallet No.</td>
							 				 			<td class="header" >Pallet Type</td>
							 				 			<td class="header" >Scan Time</td>
							 				 			<td class="header" >Consolidate</td>
							 				 		</thead>
								
									 		<% DBRow[] pallets =(DBRow[]) order.get("pallet",new DBRow[]{});
									 			if(pallets != null && pallets.length > 0 ){
									 				List<DBRow> hasConsolidate = new ArrayList<DBRow>();
									 				%>
									 				<% 
									 				for(int pindex = 0 , pcount = pallets.length ; pindex < pcount ; pindex++){
									 					DBRow pallet = pallets[pindex];
									 					String classType = pindex % 2 == 0 ? "single":"double"; 
									 					String palletNumber =  pallet.getString("wms_pallet_number");
									 					if(pallet.get("wms_consolidate_reason", 0) != 0 && !StringUtil.isNull(pallet.getString("wms_consolidate_pallet_number"))){
									 						hasConsolidate.add(pallet);
									 						continue ;
									 					}
									 					
									 					DBRow consolidateRow = loadPaperWorkZr.getConsolidatePalletType(palletNumber, hasConsolidate);
									 					%>
									 						 <tr class="<%=classType  %>">
									 						 	<td style="text-align: center;font-size: 14px"> <%= palletNumber %> </td>
									 						 	<td style="text-align: center;font-size: 14px"> <%=pallet.getString("wms_pallet_type") %></td>
									 						 	<td style="text-align: center;font-size: 14px"> <%=DateUtil.showLocalparseDateTo24Hours(pallet.getString("scan_time"),data.get("ps_id",0l)) %></td>
									 						 	<td style="text-align: center;font-size: 14px">
									 						 		<% 
									 						 		if(consolidateRow != null){	
									 						 			out.print(consolidateRow.getString("wms_pallet_number") + "<br />");
									 						 			out.print("Reason : " + reasonKey.getValueOfKey(consolidateRow.get("wms_consolidate_reason", 0)) );
									 						 		}
									 						 		%> 
									 						 	</td>
									 						 </tr>
 									 					<% 	
									 				}
									 				 
									 			}else{
									 				%>
									 				<tr>
									 					<td style="height: 30px;line-height: 20px;text-align: center;" colspan="3">No Records</td>
									 				</tr>
									 				<%
									 			}
									 			
									 		%>
								 				</table>
									 		</div>	
									</div>
							
									<% 
								}
							}
						%>
								</div>
					<% 
					}		
		%>
		<%}%>
		</div>
		
	
</body>