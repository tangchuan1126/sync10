<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.LableTemplateKey"%>

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<%
	long title_id=StringUtil.getLong(request, "title_id");
	DBRow[] lableRows = checkInMgrZwb.selectTitleLableTemplate(title_id);
	//String load=StringUtil.getString(request, "load");
	long entry_id=StringUtil.getLong(request, "entry_id");
	String jsonString = StringUtil.getString(request, "jsonString");
	String out_seal = StringUtil.getString(request, "out_seal");
%>
<title>Lable Template</title>
<script type="text/javascript">

function createLable(path){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/'+path+'?jsonString=<%=jsonString%>&entry_id=<%=entry_id%>&out_seal=<%=out_seal%>';
	 $.artDialog.open(url, {title: "Webcam List",width:'900px',height:'600px', lock: false,opacity: 0.3,fixed: true});
}
</script>
</head>
<body onLoad="onLoadInitZebraTable()">

<table width="98%" align="center" border="0" cellspacing="0" cellpadding="0">		
	<tr>
		<td>
			<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0"  class="zebraTable" >			
			    <%for(int i=0;i<lableRows.length;i++){ %>			    
				<tr onclick="createLable('<%=lableRows[i].getString("template_path")%>')">
					<td width="220" height="100px;"  align="center" valign="middle"  style="border-right:2px #eeeeee solid;border-bottom:2px #eeeeee solid;">
						<table align="left" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" style="padding-left: 15px;">Name:</td>
								<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=lableRows[i].getString("template_name")%></strong></td>
							</tr>
							<tr>
								<td align="left" style="padding-left: 15px;">Paper:</td>
								<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=lableRows[i].getString("paper")%></strong></td>
							</tr>
							<tr>
								<td align="left" style="padding-left: 15px;">Width:</td>
								<td align="left" nowrap="nowrap" style="padding-left:15px;"><%=lableRows[i].get("print_range_width",0f)%></td>
							</tr>
							<tr>
								<td align="left" style="padding-left: 15px;">Height:</td>
								<td align="left" nowrap="nowrap" style="padding-left:15px;"><%=lableRows[i].get("print_range_height",0f)%></td>
							</tr>
						</table>
					</td>
				</tr>
				<%} %>
			</table>
		</td>
	</tr>		
</table>  
</body>
</html>

