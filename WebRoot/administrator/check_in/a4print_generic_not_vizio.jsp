<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="com.cwc.app.key.RelativeFileKey"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@ include file="../../include.jsp"%>
<%
	//String loadNo=StringUtil.getString(request, "load");
	long entry_id=StringUtil.getLong(request, "entry_id");
	DBRow[] mainRows = checkInMgrZwb.selectAllMain(entry_id, null);
	DBRow mainRow=mainRows.length > 0?mainRows[0]:null;
	String jsonString = StringUtil.getString(request, "jsonString");
	String printName=StringUtil.getString(request,"print_name");
	JSONArray jsons = new JSONArray(jsonString);
	String out_seal=StringUtil.getString(request, "out_seal");

	boolean isPrint = StringUtil.getInt(request, "isprint") == 1; 
	
	String container_no=StringUtil.getString(request, "container_no");   //传过来的货柜号- zwb
	DBRow tractorRow=checkInMgrZwb.findTractorByEntryId(entry_id);   //根据主单据id 查询 车头号  - zwb
	
	String tempShipperKey = entry_id+"_"+RelativeFileKey.BiLLOfLoadingShipper ;			//签字的时候使用 - 张睿
	String tempCarrierKey = entry_id+"_"+RelativeFileKey.BillOfLoadingCarrier ; 		//签字的时候使用 - 张睿
	
%>
<html>
<head>
 <!--  基本样式和javascript -->
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
 
<%if(!isPrint){ %>
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>

<%} %>
<title>a4print_generic_not_vizio</title>
<script type="text/javascript">
//zhangrui start
function getSignImage(parent){
	 
	return {
		shipper_image: $(".shipper_image",parent) ,
		carrier_image: $(".carrier_image",parent)
	}
}
function printPreSignDisplayImage(obj){
	obj.shipper_image.css("display","none");
	obj.carrier_image.css("display","none");
}
/**function printAddSignImage(visionariPrinter , obj){  
	var shipper_image = obj.shipper_image ;
	var carrier_image = obj.carrier_image ;
	var flag = false;
	
	if(shipper_image && shipper_image.length > 0){
	//zhangrui
	//添加一个图片 shipper
	 	visionariPrinter.ADD_PRINT_IMAGE(860,20,150,80,"<img border='0' src='"+shipper_image.attr("src")+"' />");
		shipper_image.css("display","block");
		visionariPrinter.SET_PRINT_STYLEA(0,"Stretch",2);//按原图比例(不变形)缩放模式
		visionariPrinter.SET_PRINT_STYLEA(0,"TransColor","#FFFFFF");
		visionariPrinter.SET_PRINT_STYLEA(0,"PageIndex","First");          //只在第一页显示
	}
	 
	if(carrier_image && carrier_image.length > 0 ){
	//添加一个图片 carrier

	visionariPrinter.ADD_PRINT_IMAGE(870,490,150,80,"<img border='0' src='"+carrier_image.attr("src")+"' />");
	carrier_image.css("display","block");
	visionariPrinter.SET_PRINT_STYLEA(0,"Stretch",2);//按原图比例(不变形)缩放模式
	visionariPrinter.SET_PRINT_STYLEA(0,"TransColor","#FFFFFF"); 
	visionariPrinter.SET_PRINT_STYLEA(0,"PageIndex","First");          //只在第一页显示
	}
	
	if(shipper_image && shipper_image.length > 0 && carrier_image && carrier_image.length > 0){
		flag = true;
	}
	return  flag;
}**/

function printAddSignImage(visionariPrinter , obj){  
  var shipper_image = obj.shipper_image ;
  var carrier_image = obj.carrier_image ;
  var initial_image = obj.initial_image ;
  var flag = false;
  
  if(shipper_image && shipper_image.length > 0){
  //zhangrui
  //添加一个图片 shipper
    visionariPrinter.ADD_PRINT_IMAGE(860,20,150,80,"<img border='0' src='"+shipper_image.attr("src")+"' />");
    shipper_image.css("display","block");
    visionariPrinter.SET_PRINT_STYLEA(0,"Stretch",2);//按原图比例(不变形)缩放模式
    visionariPrinter.SET_PRINT_STYLEA(0,"TransColor","#FFFFFF");
    visionariPrinter.SET_PRINT_STYLEA(0,"PageIndex","First");          //只在第一页显示
  }
  if(carrier_image && carrier_image.length > 0 ){
  //添加一个图片 carrier
    //visionariPrinter.ADD_PRINT_IMAGE(860,505,150,80,"<img border='0' src='"+carrier_image.attr("src")+"' />");
	visionariPrinter.ADD_PRINT_IMAGE(840,520,150,80,"<img border='0' src='"+carrier_image.attr("src")+"' />");
    carrier_image.css("display","block");
    visionariPrinter.SET_PRINT_STYLEA(0,"Stretch",2);//按原图比例(不变形)缩放模式
    visionariPrinter.SET_PRINT_STYLEA(0,"TransColor","#FFFFFF"); 
    visionariPrinter.SET_PRINT_STYLEA(0,"PageIndex","First");          //只在第一页显示
  }
  if(initial_image && initial_image.length > 0 ){
    //添加一个图片 initial_image
      visionariPrinter.ADD_PRINT_IMAGE(870,370,110,80,"<img border='0' src='"+initial_image.attr("src")+"' />");
      carrier_image.css("display","block");
      visionariPrinter.SET_PRINT_STYLEA(0,"Stretch",2);//按原图比例(不变形)缩放模式
      visionariPrinter.SET_PRINT_STYLEA(0,"TransColor","#FFFFFF"); 
      visionariPrinter.SET_PRINT_STYLEA(0,"PageIndex","First");          //只在第一页显示
    }
  
  //添加一个图片 shipper 和  添加一个图片 carrier返回true
  if(shipper_image && shipper_image.length > 0 && carrier_image && carrier_image.length > 0){
    flag = true;
  }
  
  return  flag;
}


//zhangrui end

function androidSign(tempFile,tempKey,signType){
	Android.androidSign(tempFile,tempKey,signType);
 }
 function androidGetImage(relativeFile , tempShipperKey , fileName){
 	 var parent = document.getElementById(relativeFile+"_"+tempShipperKey);
 	 var src = '<%=ConfigBean.getStringValue("systenFolder")%>'+'_fileserv_signature/file/'+fileName;
	 var str = "<img src='"+src+"' width='150px' height='70px' />" ;
	 parent.innerHTML = str ;
  }
 //以上两个方法是在android调用的
 
	function print(){
		var printAll = $('div[name="printAll"]');
		var detail_id = "";//web端打印时，根据entryId和detailId生成pdf使用 。gql 2015/04/24
		
		//var reg =/<!--page-break-start-->([\s\S]*)?<!--page-break-end-->/;
		
		for(var i=0;i<printAll.length;i++){
			detail_id = $('#detailId',printAll[i]).val();//web端打印时，根据entryId和detailId生成pdf使用 。gql 2015/04/24
			var signObj =	getSignImage(printAll[i]);
			printPreSignDisplayImage(signObj);
			
			
			
			
			
			//console.log(strMatch);
			//分页字体
			 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
			 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");
	 
			 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",$('#a1',printAll[i]).html());
			 visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);                //设置次页偏移把区域向下扩
		   
			
			 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a2',printAll[i]).html());
			 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);                //关联
			 //visionariPrinter.SET_PRINT_STYLEA(0,"LinkNewPage",true); //新起一页
						 
			 
			 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a3',printAll[i]).html());	
			 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
			 
			 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a4',printAll[i]).html());	
			 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
			
			 var orderCount = $('#orderLength',printAll[i]).val()*1; //customer order 数据条数
			 var itemCount =$('#itemLength',printAll[i]).val()*1;  //Carrier 数据条数
			 var strHtml = printAll[i].innerHTML;
			 var arr = strHtml.split("<!--page-break-start-->")||[];
			 for(var j=1;j<arr.length;j++){
				 var innerHtml = arr[j].replace("page-break-before:always;","");
				 innerHtml.replace("width:195mm","100%");
				 
				 visionariPrinter.NewPageA();
				 var reg1 =/<!--header\s+start-->([\s\S]*)?<!--header\s+end-->/;
				 var reg2 =/<!--table\s+start-->([\s\S]*)?<!--table\s+end-->/ig;
				 
				 //header 
				 var headerArr = innerHtml.match(reg1)||[];
				 if(headerArr.length>0){
					 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",headerArr[0]);
				 }
				 
				 //body
				 var bodyArr = innerHtml.match(reg2)||[];
				 if(bodyArr.length>0){
					 var strBody = bodyArr[0];
					 var tableArr = strBody.split("<!--table start-->");
					 for(var k=1;k<tableArr.length;k++){
						 var tableInnerArr = tableArr[k].split("<!--table end-->");
						 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",tableInnerArr[0]);
						  visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
					 }
				 }
		     }
			 
			 
			 /**if(orderCount>=9){//order超了
				 visionariPrinter.NewPageA();
				 visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",$('#a5',printAll[i]).html());
				 if((orderCount+itemCount)>=10){//items超了
					 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a6',printAll[i]).html());	
					 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
				 }
			 }else if((orderCount+itemCount)>=10){//order未超，items超了
				 visionariPrinter.NewPageA();
				 visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",$('#a6',printAll[i]).html());	
				 //visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
			 }**/
			 
			 //visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#top',printAll[i]).html());
			 //visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头
			// visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");          //第一页不显示
			 var isSign = printAddSignImage(visionariPrinter,signObj);
			 //visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 
			 //visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
		 
			 visionariPrinter.SET_PRINT_COPIES(1);
	// 		 visionariPrinter.PREVIEW();
			 visionariPrinter.PRINT();
			 
			/* //添加shipper 和carrier签字的图片的才转存pdf
			if(isSign){
			 //downLoad(i);//记录打印的标签，转pdf用
			} */
		
	}
		
	//web端打印时，根据entryId和detailId生成pdf使用 。gql 2015/04/24
	if(detail_id != ""){
// 		console.log("detail_id="+detail_id);
		creatPdf('<%=entry_id%>', detail_id);
	}
}
	
 //查询number对应的detail_id,web端打印时，根据entryId和detailId生成pdf使用 。gql 2015/04/24
	function creatPdf(entry_id,detail_id){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/pdf/createPdfByPdfServerAction.action',
			async:false,
			dataType:'json',
			data:'entry_id='+entry_id+"&detail_id="+detail_id,
			success:function(data){
			},
			error:function(){}
		});
	}
 
	function handlePage1Sum()
	{
		$("#palletSumCarrier").text($("#palletSumOrders").text());
		$("#caseSumCarrier").text($("#caseSumOrders").text());
		$("#caseUnitCarrier").text($("#caseUnitOrders").text());
		$("#weightSumCarrier").text($("#weightSumOrders").text());
		$("#caseSumCustomer").text($("#caseSumOrders").text());
		$("#weightSumCustomer").text($("#weightSumOrders").text());
	}
	
	//发送到另一个服务器操作本地图片，转换pdf
	function androidToPdf(){
		var message = true;
		var printAll = $('div[name="printAll"]');
		for(var i=0;i<printAll.length;i++){
			if(message){
				message =downLoad(i);
			}else{
				downLoad(i);
			}
		}
		
		return message;
	}
	
	//将标签转为图片并上传到服务器
	function downLoad(i) {
		var printAll = $('div[name="printAll"]');

		var signObj =	getSignImage(printAll[i]);
		printPreSignDisplayImage(signObj);
		
		//分页字体
		 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
		 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");
 
         visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",$('#a1',printAll[i]).html());
         visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);                //设置次页偏移把区域向下扩
       
        
         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a2',printAll[i]).html());
         visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);                //关联
        // visionariPrinter.SET_PRINT_STYLEA(0,"LinkNewPage",true); //新起一页
                     
         
         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a3',printAll[i]).html());	
         visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
         
         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a4',printAll[i]).html());	
         visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
        
         var orderCount = $('#orderLength',printAll[i]).val()*1; //customer order 数据条数
         var itemCount =$('#itemLength',printAll[i]).val()*1;  //Carrier 数据条数
         
         if(orderCount>=9){//order超了
        	 visionariPrinter.NewPageA();
        	 visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",$('#a5',printAll[i]).html());
        	 if((orderCount+itemCount)>=10){//items超了
	             visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a6',printAll[i]).html());	
	             visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
        	 }
 		}else if((orderCount+itemCount)>=10){//order未超，items超了
 			 visionariPrinter.NewPageA();
 			 visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",$('#a6',printAll[i]).html());	
        	 //visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
 		}
         
		 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#top',printAll[i]).html());
		 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头
		 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");          //第一页不显示
		 printAddSignImage(visionariPrinter,signObj);
		 visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 
		 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
	 
		 visionariPrinter.SET_PRINT_COPIES(1);
		 
		
       	var filePath="C:/bol.jpg";
       	visionariPrinter.SET_SAVE_MODE("FILE_PROMPT",false);
       	var isSave = visionariPrinter.SAVE_TO_FILE(filePath);
       	var message=false;
       	if(isSave){
	   		var imageBuffer=visionariPrinter.FORMAT("FILE:EncodeBase64",filePath);//将图片转换为EncodeBase64流
	   		var entryId="<%=entry_id%>";
			var type="masterBol_"+(i+1);
			var companyId = $('#companyId',printAll[i]).val();
			var customerId = $('#customerId',printAll[i]).val();
			var loadNo = $('#loadNo',printAll[i]).val();
			//将图片上传到服务器
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/LabelImageUploadAction.action',
				data:'entryId='+entryId+'&type='+type+'&companyId='+companyId+'&customerId='+customerId+'&loadNo='+loadNo+'&imageBuffer='+encodeURIComponent(imageBuffer), 
				dataType:'text',
				type:'post',
				async:false,
				success:function(data){
					var result=$.parseJSON(data);
					if(result.message==="true"){
						message=true;
					}
				}
			});	
       	}
       	
		return message;
}
	
	
	//生成pdf图片，gql 2015/05/04
	function createImg(){
		var printAll = $('div[name="printAll"]');
		for(var i=0;i<printAll.length;i++){
			var signObj =	getSignImage(printAll[i]);
			printPreSignDisplayImage(signObj);
			
			//分页字体
			 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
			 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");
	 
	         visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",$('#a1',printAll[i]).html());
	         visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);                //设置次页偏移把区域向下扩
	        
	         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a2',printAll[i]).html());
	         visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);                //关联
	        // visionariPrinter.SET_PRINT_STYLEA(0,"LinkNewPage",true); //新起一页
	                     
	         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a3',printAll[i]).html());	
	         visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
	         
	         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a4',printAll[i]).html());	
	         visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
	        
	         var orderCount = $('#orderLength',printAll[i]).val()*1; //customer order 数据条数
	         var itemCount =$('#itemLength',printAll[i]).val()*1;  //Carrier 数据条数
	         
	         if(orderCount>=9){//order超了
	        	 visionariPrinter.NewPageA();
	        	 visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",$('#a5',printAll[i]).html());
	        	 if((orderCount+itemCount)>=10){//items超了
		             visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a6',printAll[i]).html());	
		             visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
	        	 }
	 		}else if((orderCount+itemCount)>=10){//order未超，items超了
	 			 visionariPrinter.NewPageA();
	 			 visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",$('#a6',printAll[i]).html());	
	        	 //visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
	 		}
	         
			 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#top',printAll[i]).html());
			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头
			 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");          //第一页不显示
			 var isSign = printAddSignImage(visionariPrinter,signObj);
			 visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 
			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
		 
			 visionariPrinter.SET_PRINT_COPIES(1);
			 visionariPrinter.SET_SAVE_MODE("SAVEAS_IMGFILE_EXENAME",".jpg");
			 visionariPrinter.SAVE_TO_FILE("vvme.jpg");
		}
	}
</script>
</head>
<body onload="handlePage1Sum()">
<%if(!isPrint){ %>
<div style="width:195mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;">
	<input class="short-short-button-print" type="button" onclick="print()" value="Print">
	<input type="button" value="Export"  onclick="createImg()" />
</div>
<%} %>
<!-- 第一个 -->
<%
long adid = StringUtil.getLong(request, "adid");
long ps_id = 0;
if(mainRow!=null){
	ps_id = mainRow.get("ps_id",0l);
}
// if(adid == 0l){
// 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
//     adid = adminLoggerBean.getAdid();
//     ps_id = adminLoggerBean.getPs_id();
// }else{
// 	ps_id = adminMgr.getDetailAdmin(adid).get("ps_id",0l);
// }

   int line_number=0;
  int page_num =1;
  int page_size = 55;
    for(int m=0;m<jsons.length();m++){
    	JSONObject json=jsons.getJSONObject(m);
    	String number=json.getString("number");
    	String checkDataType=json.getString("checkDataType");
		String companyId=json.getString("companyId");
		String customerId=json.getString("customerId");
    	String loadNo=number;
    	String master_bol_nos = json.getString("master_bol_nos");
    	DBRow equipRow = checkInMgrZwb.getEquipmentByTask(entry_id, number);
    	DBRow detailIdRow = printLabelMgrGql.getDetailByEntryId(entry_id, number);//查询number对应的detail_id,web端打印时，根据entryId和detailId生成pdf使用 。gql 2015/04/24
	//DBRow[] masterBols = sqlServerMgr.findMasterBolByLoadNo(loadNo, companyId, customerId);
	//DBRow[] masterBols = sqlServerMgr.findMasterBolsSomeInfoNotOneByLoadNo(loadNo, companyId, customerId);
	//DBRow[] masterSomeInfoRow = sqlServerMgr.findMasterBolsSomeInfoNotOneByLoadNo(loadNo, companyId, customerId);
	DBRow[] masterBols = sqlServerMgr.findMasterBolsSomeInfoOneByMasterBolStr(loadNo,master_bol_nos, companyId, customerId, adid, request);
	DBRow[] orderItemRows = new DBRow[0];
	DBRow[] orderPONoRows = new DBRow[0];
	
	String relativeValue = 	entry_id+"_"+loadNo+"_"+master_bol_nos;	//签字文件 关联的value
	
	//trailer Loader 和freight counted 可选
	int loader=0;
	int freight=0;
	DBRow detailRow = printLabelMgrGql.findLabelTempDetail(entry_id+"", loadNo);//查询trailer Loader 和freight counted的值
	if(detailRow!=null){
		loader = detailRow.get("trailer_loader",0);
		freight = detailRow.get("freight_counted",0);
	}

	int orderLength = 0;
  int itemLength = 0;
  int totalPageNum =0;
	
	if(masterBols.length > 0){
		for(int n = 0; n < masterBols.length; n++)
		{
			DBRow masterSomeInfoRow = masterBols[n];
			DBRow ponosAndItems = sqlServerMgr.findLoadNoOrdersPONoInfoByLoadMasterBolNoStr(loadNo, master_bol_nos, companyId, customerId, adid, request);
			if(ponosAndItems != null)
			{
				orderItemRows = (DBRow[])ponosAndItems.get("order_items", new DBRow[0]);
				orderItemRows = checkInMgrZwb.findOrderLineByBundle(companyId,orderItemRows);
					//sqlServerMgr.findLoadNoOrdersItemInfoGroupByMasterBolNoStr(loadNo,master_bol_nos, companyId,customerId, adid, null);
					//sqlServerMgr.findLoadNoOrdersItemInfoGroupByLoadNo(loadNo, companyId, customerId);
				orderPONoRows = (DBRow[])ponosAndItems.get("ponos", new DBRow[0]);
				if(orderPONoRows!=null){
		           /**for(int lineNo=0;lineNo<orderPONoRows.length;lineNo++){
		               DBRow line = orderPONoRows[lineNo];
		               checkInMgrZwb.findOrderByBundle(companyId,line);
		           }**/
		           orderPONoRows = checkInMgrZwb.findOrderByBundle(companyId,orderPONoRows);
		        }
					//sqlServerMgr.findLoadNoOrdersPONoInfoByLoadMasterBolNoStr(loadNo, master_bol_nos, companyId, customerId, adid, request);
					//sqlServerMgr.findLoadNoOrdersPONoInfoByMasterBolNoStr(loadNo,master_bol_nos, companyId, customerId, adid, null);
					//sqlServerMgr.findLoadNoOrdersPONoInfoByLoadNo(loadNo, companyId, customerId);
			}
			if(orderPONoRows != null)
			{
				orderLength = orderPONoRows.length;
			}
			if(orderItemRows !=null)
			{
				itemLength = orderItemRows.length;
			}
			
			totalPageNum = 1+(orderLength+itemLength)/page_size;
			if((orderLength+itemLength)%page_size>0 && (orderLength+itemLength) >=10){

			totalPageNum = totalPageNum+1;
    }
%>
<!-- loop body start -->
<div id="printAll" name="printAll">
	<input type="hidden" id="orderLength" value="<%=orderLength%>"/>
	<input type="hidden" id="itemLength" value="<%=itemLength%>"/>
	<input type="hidden" id="companyId" value="<%=companyId%>"/>
	<input type="hidden" id="customerId" value="<%=customerId%>"/>
	<input type="hidden" id="loadNo" value="<%=loadNo%>"/>
	<input type="hidden" id="detailId" value="<%=detailIdRow.get("dlo_detail_id",0l)%>"/>
		
<div style="border:1px red solid; width:195mm; margin:0 auto; margin-top:3px;" id="a1">
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tbody>
	       <tr>
	         <td style="border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black; font-size: 12px; font-family: Arial;" align="center" height="36" width="15%"><b>Date:</b><span style="font-style: italic;"><%=DateUtil.showLocalparseDateTo24Hours(DateUtil.NowStr(),ps_id) %></span></td>
	         <td style="border-bottom: 1px solid black; border-top: 1px solid black; font-size: 18px; font-weight: bold; font-family: Arial;" align="center" width="74%">NON NEGOTIABLE BILL OF LADING</td>
	         <td style="border-bottom: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;" align="center" width="11%">PAGE:1/<%=totalPageNum%></td>
	       </tr>
		</tbody>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tbody>
		      <tr>
		        <td style="border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; font-size: 12px; font-family: Arial; " align="left" height="48" width="55%">
		          <div style="background-color:#000; color: #FFF; font-weight:bold;" align="center">SHIP FROM</div>
		          <div style="padding-left: 2px;">&nbsp;<b>Name:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipFromName") %></span></div>
		          <div style="padding-left: 2px;">&nbsp;<b>Address:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("Address1")+" "+masterSomeInfoRow.getString("Address2") %></span></div>
		          <div style="padding-left: 2px;">&nbsp;<b>City/State/Zip:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("City")+","+masterSomeInfoRow.getString("State")+" "+masterSomeInfoRow.getString("ZipCode") %></span></div>
		          <div style="padding-left: 2px;">&nbsp;<b>Phone:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("Phone")+" "+masterSomeInfoRow.getString("Contact") %></span></div>
		          <div>
		            <table cellpadding="0" cellspacing="0" border="0" width="100%">
					  <tbody><tr>
					    <td style="font-size: 12px; font-family: Arial;padding-left: 2px;">&nbsp;<b>SID#:</b>&nbsp;&nbsp;<span style="font-style: italic;"></span></td>
					    <td style="font-size: 12px; font-family: Arial;width:80px;margin-right:10px;"><b>FOB: </b><input style="text-align: center; width: 14px; height: 14px; background: #FFF; border: 2px solid #000;" type="text"></td>
					  </tr>
					</tbody></table>
		          </div>
		          <div style="background-color:#000; color: #FFF;font-weight:bold; clear: left" align="center">SHIP TO</div>
		        </td>
		        <td style=" border-bottom: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;" align="left" valign="top" width="45%">
		          <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Bill of Lading Number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=master_bol_nos%></span></div>
		          <div>&nbsp;</div>
				  <div>&nbsp;</div>
				  <div style="padding-left: 2px;">&nbsp;</div>
		          <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Load No:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=loadNo %></span></div>
		          <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Appointment Date:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=new TDate().getFormateTime(masterSomeInfoRow.getString("AppointmentDate"), "MM/dd/yyyy HH:mm") %></span></div>
		        </td>
		      </tr>
		      <tr>
		        <td style="border-bottom:1px solid black; border-right:1px solid black;border-left:1px solid black;font-size:14px;font-family:Arial;" align="left">
		           <div style="padding-left: 2px; width:425px; border:red solid 0px;white-space:nowrap;overflow:hidden;">
		          
		          &nbsp;<b>Name:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipToName") %></span>
		          &nbsp;<b>Location&nbsp;#:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipToStoreNo") %></span></div>
		          
		          <div style="padding-left: 2px;">&nbsp;<b>Address:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipToAddress1")+" "+masterSomeInfoRow.getString("ShipToAddress2") %></span></div>
		          <div style="padding-left: 2px;">&nbsp;<b>City/State/Zip:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipToCity")+","+masterSomeInfoRow.getString("ShipToState")+" "+masterSomeInfoRow.getString("ShipToZipCode")%></span></div>
		          <div>
		            <table cellpadding="0" cellspacing="0" border="0" width="100%">
					  <tbody><tr>
					    <td style="font-size: 12px;font-family: Arial;padding-left: 2px;">&nbsp;<b>CID#:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipToID")+" "+masterSomeInfoRow.getString("ShipToPhone") %> </span></td>
					    <td style="font-size: 12px;font-family: Arial; width: 80px; margin-right: 10px;"><b> FOB: </b>
					      <input style="text-align: center; width: 14px; height: 14px; background: #FFF; border: 2px solid #000;" type="text"></td>
					  </tr>
					</tbody></table>
		          </div>
		          <div style="background-color:#000; color:#FFF;font-family:Arial; clear: left" align="center">THIRD PARTY FREIGHT CHARGES BILL TO:</div>
		        </td>
		        <%DBRow carrier = sqlServerMgr.findCarrierInfoByCarrierId(masterSomeInfoRow.getString("CarrierID"), masterSomeInfoRow.getString("CompanyID")); %>
		        <td style="border-bottom:1px solid black;font-size:13px;font-family:Arial;border-right:1px solid black;" align="left" valign="top">
		          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">CARRIER NAME: </span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=carrier?carrier.getString("CarrierName"):"" %></span></div>
		          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Trailer Number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=container_no%></span></div>
		          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Tractor Number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=tractorRow?tractorRow.getString("tractor_num"):"" %></span></div>
		          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Driver License No.</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=mainRow?mainRow.getString("gate_driver_liscense"):"" %></span></div>
		          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Seal No.:</span>&nbsp;&nbsp;<span style="font-style: italic;">
		            <%if(!StringUtil.isBlank(out_seal)){%>
	        			<%=out_seal %>
	        		<%}else{%>
	        			<%=null!=mainRow?mainRow.getString("seal"):"" %>
	        		<%} %>
		          </span></div>
		        </td>
		      </tr>
		      <tr>
		        <td style="border-bottom:1px solid black;border-left:1px solid black; border-right:1px solid black;font-size:12px;font-family:Arial;" align="left" valign="top">
		          <div style="padding-left: 2px;">&nbsp;<b>Name:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("BillToName") %></span></div>
		          <div style="padding-left: 2px;">&nbsp;<b>Address:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("BillToAddress1")+" "+masterSomeInfoRow.getString("BillToAddress2") %> </span></div>
		          <div style="padding-left: 2px;">&nbsp;<b>City/State/Zip:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("BillToCity")+","+masterSomeInfoRow.getString("BillToState")+" "+masterSomeInfoRow.getString("BillToZipCode")%></span></div>
		          <div style="border-bottom:1px solid black;"></div>
		          <div style="font-weight: bold;padding-left: 2px;">SPECIAL INSTRUCTIONS:</div>
		          <div style="font-style:italic;">
		          	 <%String masterNote = masterSomeInfoRow.getString("Note"); %>
		          	<%if(StrUtil.isBlank(masterNote)){ %>
		          	<%masterNote = sqlServerMgr.findFirstTimeOrderBolNoteByMasterBols(master_bol_nos, companyId, customerId); %>
		          	<%} %>
		          	 <%if(!StrUtil.isBlank(masterNote)){  %>
				       <%String[] str=StringUtil.replaceEnter(masterNote).split("<br>");  %>
				       <%if(str.length>5){%>
			        	 <%=str[0]%><br><%=str[1]%><br><%=str[2]%><br><%=str[3]%><br><%=str[4]%>
			           <%}else{ %>
			           	 <%for(int p=0;p<str.length;p++){%>
			           	 	<%=str[p] %><br>
			           	 <%} %>
			           <%} %>
			        <%}%>
		          </div>
		        </td>
		        <td style="border-bottom:1px solid black;border-right:1px solid black; font-size:12px;font-family:Arial;" align="left" valign="top">
		          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">SCAC:</span>&nbsp;&nbsp;
					<span style="font-style: italic;">
					<%
						out.println(null!=carrier?carrier.getString("SCACCode"):"" );
			    	%>
				    </span>
				  </div>
		          <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Pro number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ProNo")%></span></div>
		          <div style="border-bottom: 1px solid black" align="center">  
		          	<font style="FONT-SIZE: 18px; font-family: time Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman'; WIDTH: 100%; COLOR: #B4B4B4; LINE-HEIGHT: 150%;">
		          		BAR CODE SPACE
		          	</font>
		          	<div style="padding-left: 2px;">&nbsp;</div>
		          	<div style="padding-left: 2px;">&nbsp;</div>
		          </div>
		          <div style="font-size: 11px;font-weight: bold;margin-left:5px;text-align:left;">Freight Charge Terms:<span style="font-style: italic;">(freight charges are prepaid unless marked otherwise)</span></div>
		          <div style="padding-left:2px; border-bottom:1px solid black;font-weight:bold;padding-bottom:5px; ">
		            <%String freightTerm = masterSomeInfoRow.getString("FreightTerm"); %>
		            Prepaid&nbsp;<%="Prepaid".equals(freightTerm)?"_X_":"____" %>
		            Collect&nbsp;<%="Collect".equals(freightTerm)?"_X_":"____" %>
		            3rd Party&nbsp;<%="Third Party".equals(freightTerm)?"_X_":"____" %>
		          </div>
		          <div style="clear:both">
		          	<div style="float:left;margin-left:5px;">
		          		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		          		<input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000; margin-top:1px;" type="text">
		            	<br><span style="font-size:12px;">(check box)</span>
		            </div>
		            <div style="float:right;margin-bottom:5px; margin-right:30px;">
		            	Master Bill of Lading:with attached<br>underlying  Bills of lading
		            </div>
		          </div>
		        </td>
		      </tr>
		</tbody>
	</table>
</div>
<!-- 头 -->

<!-- 第二个页 -->
<div style="border:1px red solid; width:195mm;  margin:0 auto; margin-top:3px;" id="a2">
	<table cellpadding="0" cellspacing="0"  border="0" width="100%">
	    <thead>
	      <tr>
	        <td colspan="8" align="left" valign="top">
	          <div style="background-color:#000; color:#FFF;font-size:12px;font-family:Arial; font-weight:bold;" align="center">CUSTOMER ORDER INFORMATION</div>
	        </td>
	      </tr>
	      <tr>
	        <td style="font-size:12px;font-family:Arial;font-weight:bold;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="24%">CUSTOMER ORDER NUMBER</td>
	        <td style="font-size:12px;font-family:Arial;font-weight:bold;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%">#PKGS</td>
	        <td style="font-size:12px;font-family:Arial;font-weight:bold;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%">WEIGHT</td>
	        <td colspan="2" style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center" ><span style="font-weight:bold;">PALLET/SLIP</span><br><span style="font-size: 9px;">(CIRCLE ONE)</span></td>
	        <td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;border-right:1px solid black;" align="center" >ADDITIONAL SHIPPER INFO</td>
	      </tr>
	    </thead>
	    <%
	  	int orderPkgSum = 0;
	  	int orderWeight = 0;
	  	for(int j = 0; j < orderLength; j ++)
	  	{
	  		int orderLineCaseSum = orderPONoRows[j].get("orderLineCaseSum", 0);
	  		orderPkgSum += orderLineCaseSum;
	  		int weightItemAndPallet = orderPONoRows[j].get("weightItemAndPallet", 0);
	  		orderWeight += weightItemAndPallet;
	  		String ponoStr = orderPONoRows[j].getString("PONo");
	  		String ShipToStoreNo = orderPONoRows[j].getString("ShipToStoreNo");
	  		if(!StringUtil.isBlank(ShipToStoreNo))
	  		{
	  			ShipToStoreNo = StringUtil.fromStringGetNumber(ShipToStoreNo);
	  			if(!StringUtil.isBlank(ShipToStoreNo))
	  			{
	  				ponoStr += "-"+ShipToStoreNo;
	  			}
	  		}
	  		String DeptNo = orderPONoRows[j].getString("DeptNo");
	  		if(!StringUtil.isBlank(DeptNo))
	  		{
	  			ponoStr += " Dept:"+DeptNo;
	  		}
	  %>
	  <%if(orderLength<9){ %>     
	      <tr>
	        <td style="font-size:12px;font-family:Arial;font-style:italic;border-left:1px solid black;border-bottom:1px solid black;white-space:nowrap" align="center"  width="24%"><%=ponoStr %></td>
	        <td style="font-size:12px;font-family:Arial;font-style:italic;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%"><%=orderLineCaseSum%></td>
	        <td style="font-size:12px;font-family:Arial;font-style:italic;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%"><%=weightItemAndPallet%></td>
	        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="6%">Y</td>
	        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="6%">N</td>
	        <td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;border-right:1px solid black;" align="center" ><%=orderPONoRows[j].getString("ReferenceNo") %></td>
	      </tr>	 
	      <%}%>
	  <%} %>
	  <%if(orderLength>=9){ %>
	      <tr>
	        <td colspan="3" style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  >SEE ATTACHED SUPPLEMENT PAGE</td>
	        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="6%">Y</td>
	        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="6%">N</td>
	 		<td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;border-right:1px solid black;" align="center" >&nbsp;</td>
	      </tr>	
	   <%}%>    		      
	      <tr>
	        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black; font-weight:bold;" align="center"  width="24%">GRAND TOTAL</td>
	        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%"><%=orderPkgSum %></td>
	        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%"><%=orderWeight %></td>
	  		<td colspan="5" style="font-size:12px;font-weight:bold;font-family:Arial;border-left: 1px solid black;border-right:1px solid black;" align="center" bgcolor="#999999">&nbsp;</td>
	      </tr>		      
	</table>	  
</div>
<!-- 第 三个页 -->
<div style="border:1px red solid; width:195mm;  margin:0 auto; margin-top:3px;" id="a3">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	   <thead>
	      <tr>
	        <td colspan="9" style="color:#FFF;font-size:12px;font-family:Arial;" align="center" bgcolor="#000000" height="19" valign="top">
	            CARRIER INFORMATION
	        </td>
	      </tr>     
	      <tr>
	        <td colspan="2" style="font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;" align="center">HANDLING <br>UNIT</td>
	        <td colspan="2" style="font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;" align="center">PACKAGE</td>
	        <td rowspan="2" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%"><span style="font-weight:bold"><span style="font-weight:bold">WEIGH</span>T</span></td>
	        <td rowspan="2" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">H.M.<br>(X)</td>
	        <td rowspan="2" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="38%"><span style="font-weight:bold">COMMODITY DESCRIPTION</span><br>
	        <span style="font-family:Arial;font-size:7px;">Commodities requiring special or 
additional care or attention in handling or stowing must be so marked and
 packaged as to ensure safe transportation with ordinary care.See 
Section 2(e) of NMFC item 360</span></td>
	        <td colspan="2" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center">LTL ONLY</td>
	      </tr>
	      <tr>
	        <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="7%">QTY</td>
	        <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">TYPE</td>
	        <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="7%">QTY</td>
	        <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">TYPE</td>
	        <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="9%">NMFC#</td>
	        <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center" width="9%">CLASS</td>
	      </tr>
	     </thead>  
  <%
  	double orderItemPalletSum = 0d;
  	int orderItemCaseSum = 0;
	int orderItemWeightSum = 0;
	int orderRows = orderLength<1?1:orderLength;
	for(int j = 0; j < orderItemRows.length; j ++)
	   	{
	   		double palletCount = orderItemRows[j].get("palletCount", 0d);
	   		int orderLineCase = orderItemRows[j].get("orderLineCase", 0);
	   		int weightItemAndPallet = orderItemRows[j].get("weightItemAndPallet", 0);
	   		orderItemPalletSum += palletCount;
	   	  	orderItemCaseSum += orderLineCase;
	   		orderItemWeightSum += weightItemAndPallet;
	   		
	   		if((orderRows+itemLength)<10){
   %>    
	     
	      <tbody><tr>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center"><%=0!=palletCount?MoneyUtil.formatDoubleDecimal(palletCount):"-"  %></td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">Plts</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderLineCase%></td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=0!=orderLineCase?"CTNS":"Piece" %></td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=weightItemAndPallet %></td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	       	<td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=!StringUtil.isBlank(orderItemRows[j].getString("CommodityDescription"))?orderItemRows[j].getString("CommodityDescription"):"&nbsp;"%></td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=!StringUtil.isBlank(orderItemRows[j].getString("NMFC"))?orderItemRows[j].getString("NMFC"):"&nbsp;"%></td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=!StringUtil.isBlank(orderItemRows[j].getString("FreightClass"))?orderItemRows[j].getString("FreightClass"):"&nbsp;"%></td>
	      </tr>
	      
	      <%}%>
  <%}%>      
   <%
		if((orderRows+itemLength)>=10){
   %>
  
	      
	      <tr>
	        <td colspan="7" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">SEE ATTACHED SUPPLEMENT PAGE</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	      </tr>
<%}%>	     

<%
  	int blankCount = 0;
		if(orderLength>=9)
		{
			blankCount=7;
		}else if((orderRows+itemLength)>9)
		{
			blankCount= 8-orderLength;
		}else
		{
			blankCount=9-(itemLength+orderLength);
		}
		for(int j = 0; j < blankCount; j ++)
	   	{
   %> 
	      <tr>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
	      </tr>
  <%}%>	      
	     
	      <tr>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center"><%=MoneyUtil.formatDoubleUpInt(orderItemPalletSum) %></td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">Plts</td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderItemCaseSum %></td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999"><%=0!=orderItemCaseSum?"CTNS":"Piece" %></td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderItemWeightSum %></td>
	        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">LBS</td>	     
	        <td colspan="3" style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-weight:bold;" align="center" bgcolor="#999999">GRAND TOTAL</td>
	      </tr>
	    </tbody>
</table>
</div>
<%} %>

<%if(masterBols.length > 0){ %>
<!-- 第四个页面 底部 -->
<div id="a4" style="border:1px red solid; width:195mm;  margin:0 auto; margin-top:3px;" >

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	      <tbody><tr>
	        <td style="border-bottom:1px solid black;border-top:3px solid black; border-right:1px solid black;border-left:1px solid black; font-size:10px;font-family:Arial;" align="left" height="20" valign="top" width="55%">
            	<div style="margin-left:5px;margin-right:5px;margin-bottom:3px;">

                </div>
            </td>
	        <td style="border-bottom:3px solid black;border-right:3px solid black;border-left:3px solid black; border-top:3px solid black;font-family:Arial; font-size:15px;" align="left" valign="top" width="45%">
	          <div><span style="font-weight:bold; padding-left:2px; margin-bottom:10px;">COD Amount:</span> $___________________________</div>
	          <div style="font-weight:bold;text-align:center;margin-top:2px;">Fee Terms:&nbsp;Collect:
	          
	            <input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;" type="text">
				&nbsp;Prepaid:
	          
	            <input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;" type="text">
	          </div>
	          <div style="font-weight:bold;text-align:center;margin-top:2px;"><label for="customer">Customer check acceptable:</label>
	            <input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;" type="text" value="">
	          </div>
	        </td>
	      </tr>
	      <tr>
	        <td colspan="2" style="font-size:12px;font-family:Arial;border-bottom:1px solid black;border-left:1px solid black;border-right:1px solid black;font-weight:bold;" align="left" height="20" valign="top">
	        	<div style="margin-left:5px;margin-right:5px;">NOTE Liability Limitation for loss or damage in this shipment may be applicable. See 49 U.S.C. §14706(c)(1)(A)and (B). </div></td>
	      </tr>        
	      <tr>
	        <td colspan="2">
	        		<table cellpadding="0" cellspacing="0" border="0" width="100%">
				      <tbody><tr>
				        <td  onclick="androidSign('<%=relativeValue %>','<%=tempShipperKey %>','<%=RelativeFileKey.BiLLOfLoadingShipper %>');" style="border-bottom:1px solid black; border-right:1px solid black;border-left:1px solid black;font-size:12px;font-family:Arial;" height="180" valign="top" width="34%">
				          <div style="font-weight:bold; padding-left: 5px;margin-top:5px;">SHIPPER SIGNATURE/DATE</div>
				          <div style="font-size: 9px; padding-left: 5px;">This is to certify that the 
above named materials are properly classified,packaged,marked and 
labeled, and are in proper condition for transportation according to the 
applicable regulations of the DOT.</div>
				          <br>
				      
				             <div id='<%=relativeValue+"_"+tempShipperKey %>'  style="padding-left: 5px; border-bottom:1px solid black;   font-family:Arial;font-size:12px; height:70px;">
				              <%
				             	  DBRow shipperImag = relativeFileMgrZr.queryReativeFile(relativeValue, RelativeFileKey.BiLLOfLoadingShipper);
 				              	  if(shipperImag != null){
				              		  String shipperUrl = shipperImag.getString("relative_file_path");
				              		  %>
				              		  <div style="float:left;">
										<img class="shipper_image" src="<%=ConfigBean.getStringValue("systenFolder")+"_fileserv_signature/file/"+shipperUrl %>" width="150px" height="70px" />
				              		  </div>
				              		  <% 
				              	  }
				              %>
				              
				              <%
				          		if(shipperImag != null && !StringUtil.isNull(shipperImag.getString("create_time")) ){
				          	%>
				          		 <div style="float:right;padding-top:50px;font-size:10px;" >&nbsp;&nbsp; 
				          			<% 
				          				String shipperImagTime = shipperImag.getString("create_time") ;
				          				 if(!shipperImagTime.equals("")){
				          					 out.print( DateUtil.showLocalparseDateToNoYear24Hours(shipperImagTime,ps_id)); 
				          				 }
				          				
				          			%>
				          			&nbsp;&nbsp;
				          		</div>
				          		<% }%>
				          </div>
				          
				          
				          <!--<div style="padding-left: 5px; font-family:Arial;font-size:12px;">
							<div style="float:left;">Signature/Print Name</div>
				          	<div style="float:right;margin-right: 50px;" >Date</div>
		          		  </div>-->
				        </td>
				        <td style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;font-size:12px;font-family:Arial;" valign="top" width="14%">
				          <div style="text-decoration:underline;font-family:Arial;font-size:12px;margin-top:5px;">Trailer Loader:</div>
				          <div style="margin-top:5px;margin-left:5px;text-align:left;font-size:12px;">
<%-- 				            <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg"> --%>
				          	<% if(loader==1){%>	
								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg">          	
							<%}else{ %>	 
								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg">         	
							<% }%> 
				          	By Shipper
				          </div>
				          <div style="margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;">
				            <p>
<%-- 				              <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg">  --%>
				             <% if(loader==2){%>	
								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg">          	
							<%}else{ %>	 
								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg">         	
							<% }%> 
				              By Driver				          </p>
				          </div>
				        </td>
				        <td style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;font-size:12px;font-family:Arial;" valign="top" width="16%">
				          <div style="text-decoration:underline;font-family:Arial;font-size:12px;margin-top:5px;">Freight Counted:</div>
				          <div style="margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;">
				            <p>
<%-- 				             <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg">  --%>
				            <% if(freight==1){%>	
								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg">          	
							<%}else{ %>	 
								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg">         	
							<% }%> 
				              By Shipper			          </p>
				          </div>
				          <div style="margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;">
<%-- 				             <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg"> --%>
				          	<% if(freight==2){%>	
								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg">          	
							<%}else{ %>	 
								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg">         	
							<% }%> 
				          	  By Driver/Pallets said to contain
				          </div>
				           <div style="margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;">
				            <p>
<%-- 								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg"> --%>
				            <% if(freight==3){%>	
								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg">          	
							<%}else{ %>	 
								<img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg">         	
							<% }%> 
				               By Driver/Pieces	</p>
				          </div>				        
				        </td>
				        <td onclick="androidSign('<%=relativeValue %>','<%=tempCarrierKey %>','<%=RelativeFileKey.BillOfLoadingCarrier %>');"  style="border-bottom:1px solid black;border-right:1px solid black;" valign="top" width="36%">
				          <div style="font-weight: bold;font-family:Arial;font-size:12px;margin-left:5px;margin-top:5px;">CARRIER SIGNATURE/PICKUP DATE</div>
				          <div style="font-size:7px;font-family:Arial;margin-left:5px;margin-right:5px;">Carrier 
acknowledges receipt of packages and required placards.Carrier certifies
 emergency response information was made available and/or carrier has 
the DOT emergency response guidebook or equivalent  documentation in the
 vehicle.</div>
				          <div style="font-style: italic;font-size:9px;font-weight: bold;margin:5px 5px 0px 5px;font-family:Arial;">Property described above is received in good order, except as noted.</div>
				   
				           
				          <div  id='<%=relativeValue+"_"+tempCarrierKey %>'  style="border-bottom:1px solid black;font-family:Arial;font-size:12px;height: 70px;">
				          	   <%
				             	  DBRow carrierImag = relativeFileMgrZr.queryReativeFile(relativeValue, RelativeFileKey.BillOfLoadingCarrier);
 				              	  if(carrierImag != null){
				              		  String carrierUrl = carrierImag.getString("relative_file_path");
				              		  %>
										<img class="carrier_image" src="<%=ConfigBean.getStringValue("systenFolder")+"_fileserv_signature/file/"+carrierUrl %>" width="150px" height="70px" />
				              		  <% 
				              	  }
				              %>
				          </div>
				          <div style="font-size:10px;font-family:Arial;margin-left:5px;">Signature/Print Name   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
				          <div>&nbsp;</div>				         
				          <div style="font-weight: bold;font-family:Arial;font-size:12px;margin-left:5px;">
				          	<table width="98%">
	 				        		<tr>
	 				        			<td width="50%" style="font-style: italic;font-size:10px;font-weight: normal;" align="left">Gate In:&nbsp;<%=DateUtil.showLocalparseDateToNoYear24Hours(equipRow.getString("check_in_time"),ps_id)%></td>
	 				        			<td style="font-style: italic;font-size:10px;font-weight: normal;" align="left">
	 				        			<%if(equipRow!=null&&!equipRow.getString("check_in_window_time").equals("")){ %>
	 				        				Window In:&nbsp;<%=DateUtil.showLocalparseDateToNoYear24Hours(equipRow.getString("check_in_window_time"),ps_id)%>
	 				   					<%} %>
	 				        			</td>
	 				        		</tr> 				        		
	 				          </table>
 				        	
 				        	<% 
					          	 String timeIn = ""; //timeIn 的时间是DockCheckIn的时间
					          	 String timeOut = "";//timeOut 的时间是签字的时间create_time
					          	 if(equipRow != null){
					        		 timeIn = DateUtil.showLocalparseDateToNoYear24Hours(equipRow.getString("check_in_warehouse_time"),ps_id);
					        	 	 timeOut = DateUtil.showLocalparseDateToNoYear24Hours(equipRow.getString("check_out_warehouse_time"),ps_id);
					        	  }
 				        	%>
				           <table width="98%">
 				        		<tr>
 				        			<td width="50%" style="font-style: italic;font-size:10px;font-weight: normal;" align="left">Dock In:&nbsp;<%=timeIn %></td>
 				        			<td style="font-style: italic;font-size:10px;font-weight: normal;" align="left">Dock Close:&nbsp;<%=timeOut %></td>
 				        		</tr>
 				        	</table>
				        	
				          
				          </div>
				        </td>
				      </tr>
				    </tbody></table>
	        </td>
	      </tr>
	    </tbody>
</table>







</div>
<%} %>
<% if(orderLength>=9){//order超了 %>
<!-- 第五个 需要分页的customer order -->

	  <%
	  	int orderPkgSum = 0;
	  	int orderWeight = 0;
	  	for(int j = 0; j < orderPONoRows.length; j ++)
	  	{
	  		 if(line_number%page_size==0){
				 
				  page_num++; 
				%>
				  <!--page-break-start-->
				  
				  <div style="border:1px red solid; width:195mm;  margin:0 auto; margin-top:3px;page-break-before:always;" id="top">
					<!--header start-->
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tbody><tr>
					   <td colspan="6" style="border-top: 1px solid black; border-left: 1px solid black;border-right: 1px solid black;border-bottom:1px solid black;" align="center">
					   <table cellpadding="0" cellspacing="0" border="0" width="100%">
					  <tbody><tr>
						<td style="font-size:12px; font-family:Arial;" align="center" height="39" width="17%"><b>Date:</b><span style="font-style: italic;"><%=DateUtil.showLocalparseDateTo24Hours(DateUtil.NowStr(),ps_id) %></span></td>
						<td align="center" width="72%">
						<div style="font-size:18px; font-family:Arial; font-weight: bold;" align="center" >SUPPLEMENT TO THE BILL OF LADING</div>
						  <div style="font-size:14px; font-family:Arial;" align="">&nbsp;&nbsp;&nbsp;&nbsp;<b>BILL OF Lading Number:</b><span style="font-style: italic;"><%=master_bol_nos %></span></div>
						</td>
						 <td align="left" valign="bottom" width="11%">PAGE:<%=page_num%>/<%=totalPageNum%></td>
					  </tr>
					  </tbody></table>
					   </td>
					  </tr>
					 </tbody>
					</table>
					<!--header end-->
				  </div>
					
					<div style="border:1px red solid; width:195mm;  margin:0 auto; margin-top:3px;display: block;" id="a5" >
					<!--table start-->
					<table cellpadding="0" cellspacing="0"  border="0" width="100%">
					  <thead>
					  <tr>
						<td colspan="8" align="left" valign="top">
						<div style="background-color:#000; color:#FFF;font-size:12px;font-family:Arial; font-weight:bold;" align="center">CUSTOMER ORDER INFORMATION</div>
						</td>
					  </tr>
					  <tr>
						<td style="font-size:12px;font-family:Arial;font-weight:bold;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="24%">CUSTOMER ORDER NUMBER</td>
						<td style="font-size:12px;font-family:Arial;font-weight:bold;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%">#PKGS</td>
						<td style="font-size:12px;font-family:Arial;font-weight:bold;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%">WEIGHT</td>
						<td colspan="2" style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center" ><span style="font-weight:bold;">PALLET/SLIP</span><br><span style="font-size: 9px;">(CIRCLE ONE)</span></td>
						<td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;border-right:1px solid black;" align="center" >ADDITIONAL SHIPPER INFO</td>
					  </tr>
					  </thead>
				<%} 
	
			int orderLineCaseSum = orderPONoRows[j].get("orderLineCaseSum", 0);
	  		orderPkgSum += orderLineCaseSum;
	  		int weightItemAndPallet = orderPONoRows[j].get("weightItemAndPallet", 0);
	  		orderWeight += weightItemAndPallet;
	  		String ponoStr = orderPONoRows[j].getString("PONo");
	  		String ShipToStoreNo = orderPONoRows[j].getString("ShipToStoreNo");
	  		if(!StringUtil.isBlank(ShipToStoreNo))
	  		{
	  			ShipToStoreNo = StringUtil.fromStringGetNumber(ShipToStoreNo);
	  			if(!StringUtil.isBlank(ShipToStoreNo))
	  			{
	  				ponoStr += "-"+ShipToStoreNo;
	  			}
	  		}
	  		String DeptNo = orderPONoRows[j].getString("DeptNo");
	  		if(!StringUtil.isBlank(DeptNo))
	  		{
	  			ponoStr += " Dept:"+DeptNo;
	  		}
	  %>
	   <tr>
        <td style="font-size:12px;font-family:Arial;font-style:italic;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="24%"><%=ponoStr %></td>
        <td style="font-size:12px;font-family:Arial;font-style:italic;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%"><%=orderLineCaseSum%></td>
        <td style="font-size:12px;font-family:Arial;font-style:italic;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%"><%=weightItemAndPallet%></td>
        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="6%">Y</td>
        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="6%">N</td>
        <td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;border-right:1px solid black;" align="center" ><%=orderPONoRows[j].getString("ReferenceNo") %></td>
      </tr>	 
	       <%
      if(j == orderPONoRows.length-1){%>
        <tr>
        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;font-weight:bold;" align="center"  width="24%">GRAND TOTAL</td>
        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%"><%=orderPkgSum %></td>
        <td style="font-size:12px;font-family:Arial;border-left:1px solid black;border-bottom:1px solid black;" align="center"  width="12%"><%=orderWeight %></td>
        <td colspan="5" style="font-size:12px;font-weight:bold;font-family:Arial;border-left: 1px solid black;border-right:1px solid black;" align="center" bgcolor="#999999">&nbsp;</td>
      </tr>         
    <%}
    if(j == orderPONoRows.length-1 || (line_number+1)%page_size==0){%>
         </table>
		<!--table end-->		 
      </div>
	    
		 <%if((line_number+1)%page_size==0){%>
		 
				<!--page-break-end--> 
		 <%}
	  
      }
      line_number++;
	 } %>    		      
     

		
	
	<!--</table>	</div>-->
<%} %>
<%if((orderLength+itemLength)>=10){  //items超了 %>
<!-- 第六个 需要分页的carrier information -->

	  <%
			double orderItemPalletSum = 0d;
			int orderItemCaseSum = 0;
			int orderItemWeightSum = 0;
			for(int j = 0; j < orderItemRows.length; j ++){
				 if(j==0 || line_number%page_size==0){
			String pagebreak="";
		   //if(j>0 || page_num==1){
		    if(line_number%page_size==0){
			pagebreak ="page-break-before:always;";
			page_num++; %>
			<!--page-break-start-->
			
			<div style="border:1px red solid; width:195mm;  margin:0 auto; margin-top:3px;page-break-before:always;" id="top" >
			  <!--header start-->
			  <table cellpadding="0" cellspacing="0" border="0" width="100%">
			  <tbody><tr>
				 <td colspan="6" style="border-top: 1px solid black; border-left: 1px solid black;border-right: 1px solid black;border-bottom:1px solid black;" align="center">
				 <table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tbody><tr>
				  <td style="font-size:12px; font-family:Arial;" align="center" height="39" width="17%"><b>Date:</b><span style="font-style: italic;"><%=DateUtil.showLocalparseDateTo24Hours(DateUtil.NowStr(),ps_id) %></span></td>
				  <td align="center" width="72%">
				  <div style="font-size:18px; font-family:Arial; font-weight: bold;" align="center" >SUPPLEMENT TO THE BILL OF LADING</div>
					<div style="font-size:14px; font-family:Arial;" align="">&nbsp;&nbsp;&nbsp;&nbsp;<b>BILL OF Lading Number:</b><span style="font-style: italic;"><%=master_bol_nos %></span></div>
				  </td>
				  <td align="left" valign="bottom" width="11%">PAGE:<%=page_num%>/<%=totalPageNum%></td>
				</tr>
				</tbody></table>
				 </td>
				</tr>
			   </tbody>
			  </table>
			  <!--header end-->
			</div>
			
		 <%}%>
		 <div style="border:1px red solid; width:195mm;  margin:0 auto; margin-top:3px;display: block;" id="a6" >
		<!--table start-->
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		 <thead>
			<tr>
			  <td colspan="9" style="color:#FFF;font-size:12px;font-family:Arial;" align="center" bgcolor="#000000" height="19" valign="top">
				  CARRIER INFORMATION
			  </td>
			</tr>     
			<tr>
			  <td colspan="2" style="font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;" align="center">HANDLING <br>UNIT</td>
			  <td colspan="2" style="font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;" align="center">PACKAGE</td>
			  <td rowspan="2" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%"><span style="font-weight:bold"><span style="font-weight:bold">WEIGH</span>T</span></td>
			  <td rowspan="2" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">H.M.<br>(X)</td>
			  <td rowspan="2" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="38%"><span style="font-weight:bold">COMMODITY DESCRIPTION</span><br>
			  <span style="font-size:7px;font-family:Arial;">Commodities requiring special or 
	additional care or attention in handling or stowing must be so marked and
	 packaged as to ensure safe transportation with ordinary care.See 
	Section 2(e) of NMFC item 360</span></td>
			  <td colspan="2" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center">LTL ONLY</td>
			</tr>
			<tr>
			  <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="7%">QTY</td>
			  <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">TYPE</td>
			  <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="7%">QTY</td>
			  <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">TYPE</td>
			  <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="9%">NMFC#</td>
			  <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center" width="9%">CLASS</td>
			</tr>
		   </thead>
		  
		<%  }
	
	   		double palletCount = orderItemRows[j].get("palletCount", 0d);
	   		int orderLineCase = orderItemRows[j].get("orderLineCase", 0);
	   		int weightItemAndPallet = orderItemRows[j].get("weightItemAndPallet", 0);
	   		orderItemPalletSum += palletCount;
	   	  	orderItemCaseSum += orderLineCase;
	   		orderItemWeightSum += weightItemAndPallet;
	   %>
	  <tr>
    	<td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center"><%=0!=palletCount?MoneyUtil.formatDoubleDecimal(palletCount):"-"  %></td>
        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">Plts</td>
        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderLineCase%></td>
        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=0!=orderLineCase?"CTNS":"Piece" %></td>
        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=weightItemAndPallet %></td>
        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>
       	<td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=!StringUtil.isBlank(orderItemRows[j].getString("CommodityDescription"))?orderItemRows[j].getString("CommodityDescription"):"&nbsp;"%></td>
        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=!StringUtil.isBlank(orderItemRows[j].getString("NMFC"))?orderItemRows[j].getString("NMFC"):"&nbsp;"%></td>
        <td style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=!StringUtil.isBlank(orderItemRows[j].getString("FreightClass"))?orderItemRows[j].getString("FreightClass"):"&nbsp;"%></td>
      </tr>
	  
	   <%
     if(j == orderItemRows.length-1){%>
     
    <tr>
      <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center"><%=MoneyUtil.formatDoubleUpInt(orderItemPalletSum) %></td>
      <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">Plts</td>
      <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderItemCaseSum %></td>
      <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999"><%=0!=orderItemCaseSum?"CTNS":"Piece" %></td>
      <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderItemWeightSum %></td>
      <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">LBS</td>      
      <td colspan="3" style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-weight:bold;" align="center" bgcolor="#999999">GRAND TOTAL</td>
      </tr>
   <%}
      if(j == orderItemRows.length-1 || (line_number+1)%page_size==0){%>
       </table> 
		<!--table end-->	
      </div>
	  <!--page-break-end-->  		
	    
     <%}
   
    line_number++;
	
}%>
	  
<%} %>
</div>
<!-- loop body end -->
<%}} %>
</body>
</html>
<script>

function supportAndroidprint(){
	//获取打印机名字列表
   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
   	 //判断是否有该名字的打印机
   	var printer = "<%=printName%>";
   	var printerExist = "false";
   	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){
		return androidIsPrint(containPrinter);
	}else{
		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消
			return	androidIsPrint(containPrinter);
		}
	}
}


function androidIsPrint(containPrinter){
	var printAll = $('div[name="printAll"]');
	var flag =  true ;
	for(var i=0;i<printAll.length;i++){
		var signObj =	getSignImage(printAll[i]);
		printPreSignDisplayImage(signObj);
		//分页字体
		 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
		 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");
		 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
		 
		 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",$('#a1',printAll[i]).html());
		 visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);                //设置次页偏移把区域向下扩
	   
		
		 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a2',printAll[i]).html());
		 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);                //关联
		// visionariPrinter.SET_PRINT_STYLEA(0,"LinkNewPage",true); //新起一页
					 
		 
		 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a3',printAll[i]).html());	
		 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
		 
		 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a4',printAll[i]).html());	
		 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
		
		 var orderCount = $('#orderLength',printAll[i]).val()*1; //customer order 数据条数
		 var itemCount =$('#itemLength',printAll[i]).val()*1;  //Carrier 数据条数
		 var strHtml = printAll[i].innerHTML;
         var arr = strHtml.split("<!--page-break-start-->")||[];
         for(var j=1;j<arr.length;j++){
				 var innerHtml = arr[j].replace("page-break-before:always;","");
				 innerHtml.replace("width:195mm","100%");
				 
				 visionariPrinter.NewPageA();
				 var reg1 =/<!--header\s+start-->([\s\S]*)?<!--header\s+end-->/;
				 var reg2 =/<!--table\s+start-->([\s\S]*)?<!--table\s+end-->/ig;
				 
				 //header 
				 var headerArr = innerHtml.match(reg1)||[];
				 if(headerArr.length>0){
					 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",headerArr[0]);
				 }
				 
				 //body
				 var bodyArr = innerHtml.match(reg2)||[];
				 if(bodyArr.length>0){
					 var strBody = bodyArr[0];
					 var tableArr = strBody.split("<!--table start-->");
					 for(var k=1;k<tableArr.length;k++){
						 var tableInnerArr = tableArr[k].split("<!--table end-->");
						 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",tableInnerArr[0]);
						  visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
					 }
				 }
		    }
		
		   /**if(orderCount>9)//两个都超了
			{
			 visionariPrinter.NewPageA();
			 visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",$('#a5',printAll[i]).html());
			 
			 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$('#a6',printAll[i]).html());	
			 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
			 
			}else if((orderCount+itemCount)>9)//order未超，items超了
			{
				
				 visionariPrinter.NewPageA();
				 visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",$('#a6',printAll[i]).html());	
			 //visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);
			}**/
		 
		  
			 /**visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#top',printAll[i]).html());
			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头
			 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");          //第一页不显示
			 
			var isSign = printAddSignImage(visionariPrinter,signObj);
			 
			// visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 
			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
			**/
			 visionariPrinter.SET_PRINT_COPIES(1);
			 //visionariPrinter.PREVIEW();
			var isSign = printAddSignImage(visionariPrinter,signObj);
			flag = flag && visionariPrinter.PRINT();
			
			//添加shipper 和carrier签字的图片的才转存pdf
			if(isSign){
			 //downLoad(i);//记录打印的标签，转pdf用
			}
	
	}
	/* url=$('#url_path').val();
	printedLabel(printed_number, entry_id, master_bol_no, url,'1'); */
	
	return flag ;
}
</script>

