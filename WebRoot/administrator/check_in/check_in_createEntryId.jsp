<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.OccupyTypeKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInLiveLoadOrDropOffKey" class="com.cwc.app.key.CheckInLiveLoadOrDropOffKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="occupyTypeKey" class="com.cwc.app.key.OccupyTypeKey"/> 

<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 long ps_id=adminLoggerBean.getPs_id();   
 long adminId=adminLoggerBean.getAdid(); 
 DBRow psRow=adminMgrZwb.findAdminPs(adminId);
 String ps_name	=psRow.get("title","") ; 
 int p =StringUtil.getInt(request,"p");
 long resource_id = StringUtil.getLong(request,"resource_id");
 String resource_name = StringUtil.getString(request,"resource_name");
 long doorId = StringUtil.getLong(request,"doorId");
 int resource_type = StringUtil.getInt(request,"resource_type");
 String doorName = StringUtil.getString(request,"doorName");
 int equipment_type = StringUtil.getInt(request,"equipment_type");
 String equipment_number = StringUtil.getString(request,"equipment_number");
 %>	
<head>
	 
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">
	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
	<style type="text/css" media="all">
	@import "../js/thickbox/thickbox.css";
	</style>
	<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
	<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
	<script src="../js/thickbox/global.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
	<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>


<script type="text/javascript">
var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
api.button([
		{
			name: '提交',
			callback: function () {
				createEntryId();
				return false ;
			},
			focus:true,
			focus:"default"
		},
		{
			name: '取消'
			
			
		}]
	);
function createEntryId(){
	var equipment_number="";
	var equipment_type="";
	if($("#license_plate").val()!=undefined){
		equipment_number =  $("#license_plate").val();
		equipment_type = <%=CheckInTractorOrTrailerTypeKey.TRACTOR%>;
	}
	if($("#trailerNo").val()!=undefined){
		equipment_number =  $("#trailerNo").val();
		equipment_type = <%=CheckInTractorOrTrailerTypeKey.TRAILER %>;
	}
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCheckInCreateEntryIdAction.action',
  		dataType:'json',
  		data:"ps_id="+<%=ps_id%>+"&equipment_number="+equipment_number+"&equipment_type="+equipment_type+"&resource_id="+<%=resource_id%>+"&resource_type="+<%=resource_type%>+"&flag=1",
  		success:function(data){
				if(data.id>0){
					alert("Create Success!");
					parent.location.reload();
					$.artDialog.close();
					
					
				}else{
					alert("Create Fail!");
				}
				
				
  			    	
  		}
    });
}
</script>
</head>
<body>
	<fieldset style="border:2px #cccccc solid;padding:5px;margin:5px;width:95%;">
	  <legend style="font-size:14px;font-weight:normal;color:#999999;">
			<font style="color:red">
			<b>
			<%=occupyTypeKey.getOccupyTypeKeyName(resource_type) %>[<%=resource_name%>]
			
			</b>
			</font>
	  </legend>
	  <table width="98%" border="0" cellspacing="5" cellpadding="2">
	  <%
	  	if(equipment_type==CheckInTractorOrTrailerTypeKey.TRACTOR){
	  		
	  %>
	  <tr>
			<td align="right"> License Plate</td>
			<td>
			   <input  id="license_plate" />
			</td>
		</tr>
	  <%
	  }else	if(equipment_type==CheckInTractorOrTrailerTypeKey.TRAILER){
	  %>
		<tr>
			<td align="right"> Trailer/CTNR</td>
			<td>
				<input id="trailerNo" value=""/>
			</td>
		</tr>
	 <%
	  }
	  %>
	  </table>
	</fieldset>
</body>
