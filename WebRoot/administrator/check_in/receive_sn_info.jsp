<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="reasonKey" class="com.cwc.app.key.LoadConsolidateReasonKey"/> 


<%
	int pageSize = 100;
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long ps_id = adminLoggerBean.getPs_id(); 
	long con_id = StringUtil.getLong(request, "con_id");
	DBRow[] sns = checkInMgrWfh.findSNByConID(con_id,1,pageSize,ps_id);
	
	
%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<head>
	
	
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	
		<script src="../js/showMessage/showMessage.js" type="text/javascript"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	
	<script type="text/javascript" src="../js/accordion/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/accordion/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="../js/accordion/jquery-ui.theme.min.css" />
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	<style type="text/css" media="all">
	@import "../js/thickbox/thickbox.css";
	</style>
	<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
	div.lines{padding:0px;margin: :0px;}
	div.pallets table td{border:1px solid silver;}
	div.pallet_headr{ 
		background: none repeat scroll 0 0 black;
	    color: white;
	    display: inline-block;
	    font-size: 14px;
	    font-weight: bold;
	    height: 30px;
	    line-height: 30px;
	    text-align: center;
	    width: 30%;
	    margin:0px;
	    padding:0px;
	    border-right: 1px solid;
	 }
 	
	span.lineInfo{font-size: 14px;font-weight: bold;}
	.single{ background: #FFFFFF}
	.double{background: #eee}
	td.header{
		width: 31%;
		text-align: center;
		font-size: 14px;
		font-weight: bold;
		background: black;
		color: white;
		line-height: 30px;
		height: 30px;
	}
	</style>
	<script type="text/javascript">
	var pageSize = <%=pageSize%>;  //初始加载个数
	var pageNo = 1;		//初始页码
	$(function() {
		$("#receive").accordion({
			heightStyle: "content"
		});
		
		$(".linesInfo").click(function(){
			 $(this).next(".pallets").slideToggle("slow");
		})
		$(".pallet_line").click(function(){
			 $(this).next("#sns").slideToggle("quick");
		})
		var con_id = "<%=con_id%>";
		$("#load_more").click(function(){
			var parent_nextSN = $("#load_more").parent();
			var nextSN = $("#load_more");
			pageNo++;  //点击之后页面 +1  若没有了，则在-1
			 $.ajax({
  	 			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/ajaxFindSN.action',
  	 			type:'post',
  	 			dataType:'json',
  	 			data:{con_id:con_id,pageSize:pageSize,pageNo:pageNo,ps_id:<%=ps_id%>},
  	 			beforeSend: function(){
  	 				$(nextSN).css("display","none");
  	 				$(parent_nextSN).append('<img src="./imgs/ajax_loader_large.gif"/>');
  	 			},
  	 			success:function(data){
  	 				$(parent_nextSN).children("img").remove();
  	 				$(nextSN).css("display","inline");
  	 				if(data!=null && data.length>0){
  	 					var html = "";
  	 					for(var i=0;i<data.length;i++){
  	 						var class_type = i%2 == 0 ?"single":"double"
  	 						html += "<tr class='"+class_type+"'";
  	 						if(i==0){
  	 						//	html+= ' style="border-top:1px solid red"';
  	 						}
  	 						html+=">";
  	 						html += "<td style='text-align: center;font-size: 14px'>";
  	 						html += "<span >"+ data[i].serial_id +"</span>";
  	 						html += "</td>";
  	 					
  	 						html +='<td style="text-align: center;font-size: 14px">';
								if(data[i].is_damage==1){
									html += '<font color="red" style="font-weight: bold;">D </font>';
								}
		 						html +=data[i].serial_number; 
		 						html += '</td><td style="text-align: center;font-size: 14px">';
		 						html += data[i].update_date;
		 						html += '</td><td style="text-align: center;font-size: 14px">';
		 						html += data[i].employe_name;
		 						html += '</td>';
	     	 					html += "</tr>";
  	 					}
  	 					$("table").append(html);
  	 					window.scrollTo(0,350+document.body.scrollTop);
  	 					if(data.length<pageSize){
  	  	 					$(nextSN).remove();
  	  	 					$(parent_nextSN).html("No more SN!");
  	 					}
  	 				}else{
  	 					pageNo --;
  	 					showMessage("No more SN!","alert");
  	 					$(nextSN).remove();
  	 					$(parent_nextSN).html("No more SN!");
  	 				}
  	 			},
  	 			error:function(){ //失败页码 -1
  	 				pageNo--;
  	 				alert('System error');
  	 			}
  	 			
  	 		});
		});
	});
	
	</script>
</head>
<body style="font-size:12px">
	<div style="border:1px solid silver;width: 100%;position: fixed;background: black;">
		<div class="pallet_headr" style="width:7%;">No.</div>
		<div class="pallet_headr">Serial No.</div>
		<div class="pallet_headr" style="width:31%">Scan Time</div>
		<div class="pallet_headr" style="border:none;width:30%;">Scanner</div>
		
	</div>
				<table border="1px"  bordercolor="#eee" style="border-collapse :collapse;border:1px solid silver;width: 100%;">
			 		<thead style="color:white;">
			 			<td class="header" style="width:7%"></td>
			 			<td class="header" >Serial No.</td>
			 			<td class="header" >Scan Time</td>
			 			<td class="header" >Scanner</td>
			 		</thead>
	 			<% 
	 			if(sns != null && sns.length > 0 ){
	 				for(int pindex = 0 , pcount = sns.length ; pindex < pcount ; pindex++){
	 					DBRow sn = sns[pindex];
	 					String classType = pindex % 2 == 0 ? "single":"double"; 
	 					String serial_number =  sn.getString("serial_number");
	 					%>
	 			<tr class="<%=classType%>" >
 					<td style="text-align: center;font-size: 14px">
 						<span > <%= sn.get("serial_id",0)%></span>
 					</td>
 					<td style="text-align: center;font-size: 14px">
		 						<%
									if(sn.get("is_damage",0l)==1){
								%>
									<font color="red" >(D) </font>
								<%
									}
								%>
		 					<%= serial_number %> 
		 				</td>
		 				<td style="text-align: center;font-size: 14px">
		 					<%=sn.get("update_date", "")%>
		 				</td><td style="text-align: center;font-size: 14px">
		 					 <%=sn.get("employe_name", "") %>
		 				</td>
	 				</tr>
 				<% 	
 					}
	 			}else{
	 				%>
	 				<tr>
	 					<td style="height: 30px;line-height: 20px;text-align: center;" colspan="3">No Records</td>
	 				</tr>
	 				<%
	 			}
	 			
	 		%>
			</table>				 			
		<div style="text-align: center;line-height: 36px;font-size: 14px;font-weight: bold;">
			<%if(sns.length>=pageSize){ %>
				<a id="load_more" href="javascript:void(0)" style="color:bule">View More...</a>
			<%}else{ %>
				No more SN!
			<%} %>
		</div>
</body>