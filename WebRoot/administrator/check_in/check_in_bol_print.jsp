<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 树形结构 -->
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/tree/jquery.js"></script>
<script type="text/javascript" src="../js/tree/jquery.tree.js"></script>
<script type="text/javascript" src="../js/tree/jquery.tree.checkbox.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script> 
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<%
	 long infoId=StringUtil.getLong(request,"infoId");
// 	 DBRow row=checkInMgrZwb.findMainById(infoId);
// 	 String in_seal = StringUtil.getString(request, "in_seal");
// 	 String out_seal = StringUtil.getString(request, "out_seal");
	
// 	 DBRow[] rows=checkInMgrZwb.selectDoorByInfoId(infoId);
//	 DBRow mainRow = checkInMgrZwb.findGateCheckInById(infoId);
//	 long islive = mainRow.get("isLive", 01);
 	 
// 	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
// 	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
// 	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
// 	DBRow[] files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(infoId,FileWithTypeKey.OCCUPANCY_MAIN);
	
// 	boolean loadFlag = false;
// 	boolean ctnFlag = false;
//     DBRow[] detailRow=checkInMgrZwb.findloadingByInfoId(infoId);
//     for(int k=0;k<detailRow.length;k++){
//     	if(!detailRow[k].getString("load_number").equals("")){
//     		loadFlag=true;
//     	}
//     	if(!detailRow[k].getString("ctn_number").equals("")){
//     		ctnFlag = true;
//     	}
//     }

    AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
    DBRow row = checkInMgrZwb.getEntryPrint(infoId,adminLoggerBean);
    DBRow entry = new DBRow();
    DBRow[] details = null;
	if(row!=null){
		entry = (DBRow)row.get("entry");
		details = (DBRow[])entry.get("details");
	}
    
 %>

<style type="text/css">
	html, body { margin:0px; padding:0px; }
	body, td, th, pre, code, select, option, input, textarea { font-family:"Trebuchet MS", Sans-serif; font-size:10pt; }
	.demo {
		 float:left; 
		 margin:15px;
		 border:0px solid gray; 
		 font-family:Verdana;
		 font-size:12px;
		 background:white;
		 width:80%;

	}
</style>
<title>Check in</title>
</head>
<body>
	<fieldset style="border:2px #cccccc solid;padding:15px;margin:20px;width:600px;">
		<legend style="font-size:17px;font-weight:normal;color:#f60;">
				E<%=infoId%>
		</legend>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		<% %>
		
		
		    <td width="50%">
	       			<div id="loadingmask">
				    	 <div class="demo" id="demo_1">
							<ul>
							<%
								if(details!=null && details.length>0){
									for( DBRow r : details){ 
										if(r.get("resources_type", 1)==OccupyTypeKey.DOOR){
							%>
										<li id="door#door#<%=r.get("resources_id",0l)%>#<%=r.getString("resources_name")%>" rel="storage" class="open"><a href="#" style="font-weight:bold; color:green"><ins>&nbsp;</ins>DOOR:<%=r.getString("resources_name")%></a>
									<% }else{%>
										<li id="door#door#<%=r.get("resources_id",0l)%>#<%=r.getString("resources_name")%>" rel="storage" class="open"><a href="#" style="font-weight:bold; color:green"><ins>&nbsp;</ins>Spot:<%=r.getString("resources_name")%></a>
									<% }%>
								<ul>
								 <%
								 	DBRow[] lotRow = (DBRow[])r.get("loads");
									if(lotRow!=null && lotRow.length>0){ 
					    	       		for(int a=0;a<lotRow.length;a++){
					    	       		    if(!lotRow[a].get("number","").equals("")){
					    	     %>
					    	       		           	    	
										<li id="number#<%=r.get("resources_id",0l)%>#<%=lotRow[a].get("number","")%>#<%=r.getString("resources_name")%>#<%=lotRow[a].get("dlo_detail_id",0l)%>#<%=lotRow[a].get("number_type",0)%>" rel="load" >
									    <%if(lotRow[a].get("number_type", 0l)==12 || lotRow[a].get("number_type", 0l)==13 || lotRow[a].get("number_type", 0l)==14){%>
									    		<a href="#"  style="color:red" >
									    <% }else{ %>
											    <a href="#"  style="color:blue" >
										<% } %> 
										<ins>&nbsp;</ins><%=moduleKey.getModuleName(lotRow[a].get("number_type",0))%>:<%=lotRow[a].get("number","")%>
										<%
											if(!lotRow[a].get("order_no","").equals("") && lotRow[a].get("number_type",0)==ModuleKey.CHECK_IN_PONO){
										%>
										   <font color="black">(ORDER:<%=lotRow[a].get("order_no","")%>)</font>
										<% } %>
										<%
											if(!lotRow[a].get("po_no","").equals("") && lotRow[a].get("number_type",0)==ModuleKey.CHECK_IN_ORDER){
										%>
										    <font color="black">(PO:<%=lotRow[a].get("po_no","")%>)</font>
										<%
											}
										%>
											</a><img src="imgs/print_button.jpg" onclick="printBillOfLading('<%=lotRow[a].get("number","")%>',<%=infoId %>,'<%=r.getString("resources_name")%>',<%=lotRow[a].get("number_type",0)%>,'<%=lotRow[a].get("company_id","")%>','<%=lotRow[a].get("customer_id","")%>','<%=lotRow[a].get("order_no","")%>','<%=r.getString("resources_type")%>','<%=lotRow[a].getString("equipment_number")%>','<%=lotRow[a].getString("appointment_date")%>','<%=lotRow[a].getString("out_seal")%>','<%=lotRow[a].get("receipt_no",0)%>')" style="margin-left: 5px;" >
										</li>										
								<%}}} %>
								
								</ul>
									</li>
							<% }}%>
							</ul>
						</div>
					</div>
	    		</div>
		   
		    </td>
		  </tr>
		</table>
	</fieldset>
	
	
</body>
</html>
<script type="text/javascript" class="source">

	$(function () { 
		$("#demo_1").tree({
			ui : {
				theme_name : "checkbox"
			},
			rules:{
				// only nodes of type root can be top level nodes
				valid_children : [ "storage" ],
				multiple:true,	//支持多选
				drag_copy:false,//禁止拷贝

			},		
			types:{
				// all node types inherit the "default" node type
				"default" : {
					deletable : false,
					renameable : false
				},
				"storage" : {
					draggable : false,
					valid_children : [ "load" ],
				},
				"load" : {
					draggable : false,
					valid_children : [ "order" ],
				},
				"order" : {
					valid_children : "none",
					max_depth :-1,
				}
			},
			plugins:{
				checkbox : {}
					}
		});
		
	});

	function myFormSubmit(){         //根据单号加载树
		var in_sealVal=$('#in_sealVal').val();
		var out_sealVal=$('#out_sealVal').val();
	    $('#in_seal').val(in_sealVal);
	    $('#out_seal').val(out_sealVal);
		$('#myform').submit();
	}

	



	function chongzhi(){  //重置树
		$("[rel='storage']").each(function(){jQuery.tree.plugins.checkbox.uncheck(this);});
	}
  

	function printBillOfLading(number,entry_id,door,number_type,companyId,customerId,order,resources_type,containerNo,appointmentDate,outSeal, receipt_no){
// 		console.log(outSeal);
		if(number_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%> || number_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>){
			alert('No data records');
			return false;
		}
		if(number_type==<%=ModuleKey.CHECK_IN_PONO%>|| number_type==<%=ModuleKey.CHECK_IN_LOAD%>|| number_type==<%=ModuleKey.CHECK_IN_ORDER%>){
			var type='PICKUP';
			var print=[];
			var json = {
					checkDataType:type.toUpperCase(),
					number:number,
					door_name:door,
					companyId:companyId,
					customerId:customerId,
					number_type:number_type,
					order:order,
					checkLen:1
			};
			print.push(json);
			var jsonString = JSON.stringify(print);
// 			console.log("containerNo="+containerNo+"    appointmentDate="+appointmentDate);
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?jsonString='+jsonString+'&order_no='+order+'&number='+number+'&number_type='+number_type+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&out_seal='+outSeal+'&CompanyID='+companyId+'&CustomerID='+customerId+'&resources_type='+resources_type+'&containerNo='+containerNo+'&appointmentDate='+appointmentDate;

			$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
		}else{
			var type='delivery';
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?number='+number+'&entry_id='+entry_id+'&door_name='+door+'&number_type='+number_type+'&type='+type+'&CompanyID='+companyId+'&CustomerID='+customerId+'&resources_type='+resources_type+'&containerNo='+containerNo+'&appointmentDate='+appointmentDate+'&receipt_no='+receipt_no;
			$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
		}
	}
	
	
	function setParentCompanyReceiptCustomer(CompanyID, number, CustomerID,entry_id,door,type, bol, ctnr){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?number='+number
		+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&CompanyID='+CompanyID+'&CustomerID='+CustomerID+'&out_seal='+$("#out_seal").val()+'&bol='+bol+'&ctnr='+ctnr;
		$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
	}
	function setParentCompanyLoadCustomer(CompanyID, loadNo, CustomerID,entry_id,door,type)
	{
		var print=[];
		var json = {
				checkDataType:type.toUpperCase(),
				number:loadNo,
				door_name:door,
				companyId:CompanyID,
				customerId:CustomerID,
				checkLen:1
		};
		print.push(json);
		var jsonString = JSON.stringify(print);
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?jsonString='+jsonString+'&number='+loadNo
				+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&CompanyID='+CompanyID+'&CustomerID='+CustomerID+'&out_seal='+$("#out_seal").val();
		$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
	}
	
	
	
	
	
	function printLoad(number,entry_id,door,type){
		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInFindLoadsByLoadNoAction.action',
			data:'loadNo='+number,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				$.unblockUI();       //遮罩关闭
				var print=[];
				var json = {
						checkDataType:type.toUpperCase(),
						number:number,
						door_name:door,
						companyId:'',
						customerId:'',
						checkLen:1
				};
				print.push(json);
				var jsonString = JSON.stringify(print);
				if(data.flag > 1){
					var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/print_load_wms_select.html?loadNo='+number+'&entry_id='+entry_id+'&door_name='+door+'&type='+type;
					$.artDialog.open(uri , {title: "select load",width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
				}else if(data.flag == 1){
					var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?jsonString='+jsonString+'&number='+number+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&out_seal='+'<%=row.get("out_seal","")%>';
					$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
				}else{
					showMessage("No records found","alert");
				}
			},
			error:function(){
				$.unblockUI();       //遮罩关闭
				alert("System error"); 
			}
		 });
	}
	
	function printCtnr(number,entry_id,door,type)
	{
		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInFindReceiptsByBolOrContainerAction.action',
			data:'number='+number,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				$.unblockUI();       //遮罩关闭
				if(data.flag > 1)
				{
					var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/print_receipts_wms_select.html?number='+number+'&entry_id='+entry_id+'&door_name='+door+'&type='+data.search_type;
					$.artDialog.open(uri , {title: "Receipts List Select",width:'550px',height:'300px', lock: true,opacity: 0.3,fixed: true});
				}
				else if(data.flag == 1)
				{
					var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?number='+number+'&entry_id='+entry_id+'&door_name='+door+'&type='+data.search_type;
					$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
				}
				else
				{
					showMessage("No records found","alert");
				}
			},
			error:function(){
				$.unblockUI();       //遮罩关闭
				alert("System error"); 
			}
		 });
	}
	
	function setParentCompanyReceiptCustomer(CompanyID, number, CustomerID,entry_id,door,type, bol, ctnr){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?number='+number
		+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&CompanyID='+CompanyID+'&CustomerID='+CustomerID+'&out_seal='+$("#out_seal").val()+'&bol='+bol+'&ctnr='+ctnr;
		$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
	}
	function setParentCompanyLoadCustomer(CompanyID, loadNo, CustomerID,entry_id,door,type)
	{
		var print=[];
		var json = {
				checkDataType:type.toUpperCase(),
				number:loadNo,
				door_name:door,
				companyId:CompanyID,
				customerId:CustomerID,
				checkLen:1
		};
		print.push(json);
		var jsonString = JSON.stringify(print);
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?jsonString='+jsonString+'&number='+loadNo
				+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&CompanyID='+CompanyID+'&CustomerID='+CustomerID+'&out_seal='+$("#out_seal").val();
		$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
	}
</script>
