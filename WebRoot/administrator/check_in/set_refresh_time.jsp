<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
	String systenFolder = ConfigBean.getStringValue("systenFolder");
    long truck_time=StringUtil.getLong(request, "truck_time");
    long dock_time=StringUtil.getLong(request, "dock_time");
    truck_time=truck_time;
    dock_time=dock_time;
%>
<head>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript"
	src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript"
	src="../js/scrollto/jquery.scrollTo-min.js"></script>


<script type="text/javascript"
	src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css"
	type="text/css" />

<link rel="stylesheet"
	href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css"
	type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script src="../js/maps/verify.js" type="text/javascript"></script>
<script type="text/javascript">
	var systenFolder = "<%=systenFolder%>";
</script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<style type="text/css">
td.topBorder {
	border-top: 1px solid silver;
}
</style>
<style type="text/css">
.ui-datepicker {
	font-size: 0.9em;
}
</style>
<script type="text/javascript">
$(function () { 
	//添加button
	var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	var updateDialog = null;
	var addDialog = null;
	api.button([
			{
				name: 'Submit',
				callback: function () {
					commitData();
					return false;
				},
				focus:true,
				focus:"default"
			},
			{
				name: 'Cancel',
				callback:function (){
					closeWindow();
					return false;
				},
			}] 
		);
});
function verifyTt(){
	  var reg = /^\d{4,}$/; 
	 return  verify('truck_time','truck_time_con','truck_time_del',reg,'','');
}
function verifyDt(){
	  var reg = /^\d{4,}$/; 
	  return  verify('dock_time','dock_time_con','dock_time_del',reg,'','');
}
function closeWindow(){
	$.artDialog.close();
}

function commitData(){
	var tt=verifyTt();
	var dt=verifyDt();
	if(tt=='1'&&dt=='1'){
 	var truck_time=$("#truck_time").val();
 	var dock_time=$("#dock_time").val();
 	var time={"truck":truck_time,"dock":dock_time};
	$.artDialog.opener.setRefreshTime(time);
	}
	closeWindow();
	
}
</script>
</head>
<body>
	<form id="setTime">
		<table border="0" style="width: 100%;height: 100%" cellpadding="0" cellspacing="0">
		<tr>
		<td align="right" width="40%">
		Truck Refresh Time:
		</td>
		<td align="left" width="60%">
		 <input type="text" value="<%=truck_time%>" id="truck_time" style="width: 60%" onchange="this.value=this.value.trim(),verifyTt()"/>
		 <span style="color: red;">(ms)</span>
		 <img id="truck_time_con" src="../imgs/maps/confirm.jpg" style="display: none">
         <img id="truck_time_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
		</td>
		</tr>
		<tr>
		<td align="right" width="40%">
		Dock Refresh Time:
		</td>
		<td align="left" width="60%">
		<input type="text" id="dock_time" value="<%=dock_time %>" style=" width: 60%" onchange="this.value=this.value.trim(),verifyDt()" />
		 <span style="color: red;">(ms)</span> 
		 <img id="dock_time_con" src="../imgs/maps/confirm.jpg" style="display: none">
         <img id="dock_time_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
		</td>
		</tr>
		</table>
	</form>
</body>
