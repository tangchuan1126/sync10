<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@page import="com.cwc.app.key.ModuleKey" %>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/>
<%@ include file="../../include.jsp"%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>

<%
	
	String entry_id = StringUtil.getString(request, "entryId");
	String out_seal = StringUtil.getString(request, "out_seal");
	
	String jsonString = StringUtil.getString(request, "jsonString");
	JSONArray jsons = new JSONArray(jsonString);
	
   
%>
<html>
  <head>
    <title>selectLableTemplate</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">

  <style type="text/css">
  	.lableTemplate{
  		margin-top:2px;
  		height:424px;
  		border:2px #dddddd solid;
  		background:#eeeeee;
  		padding:5px;
  		-webkit-border-radius:7px;
  		-moz-border-radius:7px;
  		overflow-y:auto;
  	}
  	.td_line{border-bottom: 1px solid #dddddd; font-family:Verdana;}
  </style>
 <script type="text/javascript">
  	function changeAllcountingSheet(){
  		var $lables = $("input[name='counting_list']");
  		if($("#changeAllCounting").attr("checked")){
  			$lables.each(function(){
  				$(this).attr("checked",true);
  			});
  		}else{
  			$lables.each(function(){
  				$(this).attr("checked",false);
  			});
  		}
  	}
  	function print_counting_list(){
  		var ses=$("input[name='counting_list']:checked");
  		if(ses.length==0){
  			alert("Please select a label");
  			return false;
  		}
   		for(var i=0;i<ses.length;i++){
  			var number=$(ses[i]).attr('num');
  			var doorName=$(ses[i]).attr('doorName');
  			var companyId=$(ses[i]).attr('companyId');
  			var customerId=$(ses[i]).attr('customerId');
  			var number_type=$(ses[i]).attr('number_type');
  			var order_no=$(ses[i]).attr('order_no');
  			var str = [];
  			var json = {
  				checkDataType:'PICKUP',
  				number:number,
  				door_name:doorName,
  				companyId:companyId,
  				customerId:customerId,
  				number_type:number_type,
  				order:order_no,
  				checkLen:1
  			};
  			str.push(json);
  			var json = JSON.stringify(str);
  			var path='';
  			
  			 if(number_type==<%=ModuleKey.CHECK_IN_LOAD%>){
  				 path='check_in/a4_counting_sheet.html';
    		 }else{
    			 path='check_in/a4_counting_sheet_order.html';
    		 }
  			
  			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>'+path,
				dataType:'html',
				async:false,
				data:'jsonString='+json+'&entry_id=<%=entry_id%>'+'&isprint=1&out_seal=<%=out_seal%>',
				success:function(html){	
					$('#hiddenCountingSheet').html(html);
					print();
				}
			});
  		}
  	}
</script>
</head>
<body>
    <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;" align="right">
    	<input class="long-long-button" type="button" value="Combined Print" onclick="print_counting_list()"/>
    </div>
    <div class="lableTemplate">
   		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td width="30%" class="td_line">&nbsp;</td>
		    <td style="border-bottom: 1px solid #dddddd; font-family:Verdana;border-left: 1px solid #dddddd">
		    	<input onclick="changeAllcountingSheet()" type="checkbox" id="changeAllCounting" name="changeAllCounting" />
		    </td>
		    <td class="td_line">&nbsp;</td>
		    <td class="td_line">&nbsp;</td>
		  </tr>
		</table>
		<%  for(int i=0;i<jsons.length();i++){%>
		<%  JSONObject json=jsons.getJSONObject(i);
				String number=json.getString("number");
		    	String checkDataType=json.getString("checkDataType");
		    	String companyId=json.getString("companyId");
		    	String customerId=json.getString("customerId");
		    	String doorName=json.getString("door_name");
		    	int number_type=json.getInt("number_type");
		    	String order_no=json.getString("order");
		    	if((checkDataType.toUpperCase()).equals("DELIVERY") || number_type==ModuleKey.CHECK_IN_PICKUP_ORTHERS || number_type==ModuleKey.CHECK_IN_DELIVERY_ORTHERS){
		    		continue;
		    	}
		%>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#FFF;">	
			  <tr>
			    <td width="30%" align="center" valign="middle" style="font-size: 18px; font-weight:bold;border-bottom: 1px solid black;">
			    	<div align="left" style="font-size:12px; color:#666;padding-left:20px;"><%=moduleKey.getModuleName(number_type) %>:</div>
			    	<div align="left" style="padding-left:20px;"><%=number %></div>
			    </td>
			    <td style="border-left: 1px solid #dddddd;border-bottom: 1px solid black;">				
			    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td align="left" valign="middle" width="50px;" style="border-bottom:1px solid #dddddd">
					    	<input type="checkbox"  num="<%=number %>"  name="counting_list" companyId="<%=companyId %>" customerId="<%=customerId %>" doorName="<%=doorName %>" onclick="changeAllBtn()" master_bol_nos="" order_no="<%=order_no %>" number_type="<%=number_type %>" />
					    </td>
					    <td width="400px;" style="border-bottom:1px solid #dddddd">
						    <div style="font-size:30px;font-weight:bold;">
						    	<a href="" style="text-decoration:none;outline:none;cursor:default" onclick="" >Counting Sheet</a>
						    </div>
						    <div style="font-size:12px; color:#666">&nbsp;</div>
					    </td>
					    <td style="border-left:1px solid #dddddd;border-bottom:0px solid #dddddd" align="left">
					    	<table>
								<tr><td style="font-size:12px;border-bottom:0px solid #dddddd;color:#666">&nbsp;</td></tr>
								<tr><td style="font-size:12px;border-bottom:0px solid #dddddd;color:#666">&nbsp;</td></tr>
								<tr><td style="font-size:12px;color:#666">&nbsp;</td></tr>
							</table>
					    </td>
					  </tr>
					</table>
			    </td>			
			  </tr>
			  <%} %>
		</table>
    </div>	
    <div id="hiddenCountingSheet" style="display:none">
    
    </div>
</body>
</html>






