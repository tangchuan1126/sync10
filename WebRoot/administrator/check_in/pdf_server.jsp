<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@ include file="../../include.jsp"%>
<html>
<head>
<title>PDF Server</title>
<%
// 	String	nowDate = DateUtil.getStrCurrYear()+"-"+(Integer.parseInt(DateUtil.getStrCurrMonth())+1)+"-"+DateUtil.getStrCurrDay();
%>

<!--  基本样式和javascript -->
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
<link type="text/css" href="comm.css" rel="stylesheet" />
<script src="../common.js" language="javascript"></script>
<script type="text/javascript" src="js/zebra/zebra.js"></script>
<link type="text/css" rel="stylesheet" href="js/zebra/zebra.css">
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.tabs.css" />
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.tabs.js"></script>
<link rel="stylesheet" href="js/file-upload/css/bootstrap.min.css">

<!-- showMessage控件 -->
<script type="text/javascript" src="js/showMessage/showMessage.js"></script>
<link type="text/css" href="js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- Art Dialog -->
<link rel="stylesheet" type="text/css" href="js/art/skins/idialog.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
<script type='text/javascript' src='../dwr/engine.js'></script>
<script type="text/javascript">
	window.onerror=function  ajaxLogin(){
		var content='account=admin&pwd=@dm1n';
		var _request=new XMLHttpRequest();
		var IP=location.hostname;
		var _url='http://'+IP+'/Sync10/action/admin/AccountLoginMgrAction.action';
		_request.onreadystatechange=function(){
			 if (_request.readyState == 4) {
				    if (_request.status == 200) {
				      var str = _request.responseText;
				      console.log("返回数据为："+str);
				      location.replace(window.location.href);
				    }
				    else if(_request.status == 404){
				      console.log("请求资源不存在！");
				    }
				    else {
				      console.log("Ajax请求失败，错误状态为："+_request.status);
				    }
			}
		};
		_request.open('POST',_url,true);
		_request.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		_request.send(content);
	};
 	dwr.engine.setActiveReverseAjax(true);
 	function loadHtml(){
		window.setTimeout(function(){
			$.ajax({
				url:window.location.href,
				timeout:30000,
				type:'get',
				async:false,
				success:function(){
					location.replace(window.location.href);
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){
					loadHtml(); 
				},
			   });
		},30000)
	};
	$(function(){
	    dwr.engine._errorHandler = function(message, ex) {
	    		try{
	    			loadHtml();
	        	}catch(e){
	        		loadHtml();
	        	}
	    };
	    
	    $("#tabs").tabs({
			cache: true,
			cookie: { expires: 30000 } 
	  	});
	    
	    
	 /* 对Date的扩展，将 Date 转化为指定格式的String
	  * 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
	  * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
	  * 例子： (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
	  */
	 Date.prototype.Format = function (fmt) { 
	     var o = {
	         "M+": this.getMonth() + 1, //月份 
	         "d+": this.getDate(), //日 
	         "h+": this.getHours(), //小时 
	         "m+": this.getMinutes(), //分 
	         "s+": this.getSeconds(), //秒 
	         "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	         "S": this.getMilliseconds() //毫秒 
	     };
	     if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	     for (var k in o)
	     if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	     return fmt;
	 }
	 /* END--- 将 Date 转化为指定格式的String ---END*/
	 
	});
	
	
	//根据后台传入参数，创建pdf
	function createPdf(html, id, flag, module, pdfType, adid){
		if(html.length>0){
			var $tr = $("#record-content").children("tr");
			var index = $tr.length+".&nbsp;";
			if($("#record-content").find(".creating").length>0){
				var prevInvoice = $(".creating").attr("id");
				showMessage("invoice "+prevInvoice+" is creating.","alert");
			}else{
				$("#tempHtml").html(html);//临时存放要转为流的html
				var tr = createTr(id, flag);
				$("#record-content").prepend(tr);
				$("#record-content").find(".creating").focus();
				toEncodeBase64(id, flag, module, pdfType, adid);//转码
			}		
		}else{
			showMessage("The PDF content is not exist.","error");
		}
		
	}
	//转码,不分页
	function toEncodeBase64(id, flag, module, pdfType, adid){
// 		console.log("id="+id+"   flag="+flag+"    module="+module+"   pdfType="+pdfType+"   adid="+adid);
		var server = '<font style="color:#B31D32;">Error</font>';
		var html = $("#tempHtml").html();
		visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
        visionariPrinter.SET_PRINT_PAGESIZE(1, 0, 0, "Letter");
        visionariPrinter.ADD_PRINT_TABLE(10, 10, "100%", "100%", html);
        visionariPrinter.SET_PRINT_COPIES(1);
		var filePath="C:/pdfImage.jpg";
		visionariPrinter.SET_SAVE_MODE("FILE_PROMPT",false);
		var isSave =visionariPrinter.SAVE_TO_FILE(filePath);
		
	 	//如果保存成功，转成EncodeBase64流，提交后台
		if(isSave){
			var imageBuffer=visionariPrinter.FORMAT("FILE:EncodeBase64",filePath);//将图片转换为EncodeBase64流
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/pdf/createPdfByPdfServerAction.action',
				async:false,
				data:'module='+module+'&pdfType='+pdfType+'&adid='+adid+'&associate_id='+id+'&imageBuffer='+encodeURIComponent(imageBuffer), 
				dataType:'text',
				type:'post',
				success:function(data){
					data = $.parseJSON(data);
					console.log(data.file_id);
					if(data.file_id){
						var temp = "/Sync10/_fileserv_print/file/"+data.file_id;
						server = '<a href="'+temp+'" title="'+id+'" download="'+id+'.pdf" data-gallery="">'+temp+'</a>';
					}else{
						showMessage("System error.","error");
					}
					addRecord(id, flag, server, module, pdfType, adid);
				},
				error:function(error){
					showMessage("System error.","error");
					addRecord(id, flag, server, module, pdfType, adid);
				}
			});	
       	}else{
       		showMessage("System error.","error");
			addRecord(id, flag, server, module, pdfType, adid);
       	}
	}
	
	
	//pdf创建成功，页面添加一行记录
	function addRecord(id, flag, server, module, pdfType, adid){
		var $creating = $(".creating");
		
		var tr = recordTr(id, flag, server, module, pdfType, adid);
		$("#record-content").prepend(tr);
		$(".creating").remove();
		
		var count = $("#record-content").children("tr").length;//判断有几条记录
		if(count>9){
			$("#print_count").text(count);
		}else{
			$("#print_count").html("&nbsp;"+count+"&nbsp;");
// 			$("#print_count").text("\xa0"+count+"\xa0");
		}
	}
	//重新创建
	function reprint(obj){
		var $parent = $(obj).parents("tr:eq(0)");
		var html = $parent.find("div[name='printHtml']").html();
// 		var index = $(obj).parents("tr:eq(0)").find("td:first ").find("span").text();
		var id = $parent.attr("id");
		var flag = $parent.data("flag");
		var module = $parent.data("module");
		var pdfType = $parent.data("pdftype");
		var adid = $parent.data("adid");
		
		if($("#record-content").find(".creating").length>0){
			var prevInvoice = $(".creating").attr("id");
			showMessage("invoice "+prevInvoice+" is creating.","alert");
		}else{
			$("#tempHtml").html(html);//临时存放要转为流的html
			$parent.remove();
// 			console.log("id="+id+"   flag="+flag+"    module="+module+"   pdfType="+pdfType+"   adid="+adid);
			createPdf(html, id, flag, module, pdfType, adid);
			
			/* var tr = createTr(id, flag);
			$parent.replaceWith(tr);
			$parent.focus();
			toEncodeBase64(id, flag, module, pdfType, adid);//转码 */
		}
	}
	//预览
	function preview(obj){
		var $parent = $(obj).parents("tr:eq(0)");
		var html = $parent.find("div[name='printHtml']").html();
		 $.artDialog({
		 	title:'Preview',
		 	lock: true,
		 	opacity: 0.3,
		 	height:'500px',
		    content: '<div style="height: 500px;overflow-y: auto;padding: 5px 25px 0 10px;">'+html+'</div>',
		});
	}
	
	//正在创建时的tr
	function createTr(id, flag){
		var time = getCurrentDay();
		var createTr = '<tr class="creating" id="'+id+'" data-flag="'+flag+'">'
// 				+'<td><span>'+index+'</span></td>'
				+'<td><span>'+flag+'&nbsp;#'+id+'</span></td>'
				+'<td><span>&nbsp;'+time+'</span></td>'
				+'<td colspan="2" style="height: 60px;">'
				+'<img src="check_in/imgs/create_pdf.gif" alt="creating" />'
				+'</td>'
				+'</tr>';
		return createTr;
	}
	
	//记录的tr
	function recordTr(id, flag, server, module, pdftype, adid){
		var time = getCurrentDay();
		var printHtml = $("#tempHtml").html();
		var recordTr = '<tr id="'+id+'" data-flag="'+flag+'" data-module="'+module+'" data-pdftype="'+pdftype+'" data-adid="'+adid+'">'
				+'<td><span height="60">'+flag+'&nbsp;#'+id+'</span></td>'
				+'<td><span>&nbsp;'+time+'</span></td>'
				+'<td><span>&nbsp;'+server+'</span></td>'
				+'<td>'
				+'<button class="btn btn-info" onclick="reprint(this)">'
				+'<i class="icon-print icon-white"></i>&nbsp;Reprint'
		      	+'</button>&nbsp;'
		      	+'<button class="btn btn-warning" onclick="preview(this)">'
		      	+'<i class="icon-eye-open icon-white"></i>&nbsp;Preview'
		      	+'</button>'
		      	+'<div name="printHtml" style="display: none;">'+printHtml+'</div>'
		      	+'</td>'
		      	+'</tr>';
		return recordTr;
	}
	
	//获取当前时间
	function getCurrentDay(_date){
	  var s = (new Date()).Format("MM/dd/yy hh:mm");
      return s;
	}
</script>


<style type="text/css">
		.aui_content{
			padding: 0 !important;
		}
		.head-div{
			width:100%;
			border:1px #eed3d7 solid;
			background:#f2dede;
			-webkit-border-radius:7px;
			-moz-border-radius:7px;
			border-radius:7px 7px 0 0;
			border-bottom-color:transparent;
			margin:0 auto;
			text-align:center;
		}
		.record-div{
			width:100%;
			border:1px #dddddd solid;
			background:#eeeeee;
			-webkit-border-radius:7px;
			-moz-border-radius:7px;
			border-radius: 0 0 7px 7px;
			margin:0 auto;
			text-align:center;
		}
		.record-content{
			min-height:400px;
			border:0px;
			margin:10px;
			background-color:#FFF;
			overflow-y:auto;
			border-left: 1px solid #bbb;
			border-right: 1px solid #bbb;
		}
		.record-content a{
			color: #0088cc;
  			text-decoration: underline;
		}
		.ui-tabs .ui-tabs-panel {
  			padding: 0;
		}
		.zebraTable>thead>tr>th{
			background-color: #e5e5e5;
		    border-color: #bbbbbb;
		    border-style: solid;
		    border-width: 1px 1px 1px 0;
		    color: #333333;
		    font-size: 12px;
		    font-weight: bold;
		    height: 30px;
		    padding: 5px;
		    text-align: center;
		}
		.zebraTable>tbody>tr>td{
			height: 45px;
  			text-align: center;
		}
		.zebraTable tr:nth-child(even)
		{
			background: #f9f9f9;
		
		}
		.zebraTable tr:hover,
		.zebraTable tr:focus
		{
			background: #E6F3C5;
		
		}
		#print_count,#print_failed_count{
			width: 100px;
     		height: 100px;
     		background: red;
     		-moz-border-radius: 50px;
     		-webkit-border-radius: 50px;
     		border-radius: 50px;
		}
		
		.creating td{
			font-size: 22px;
			font-weight: bold;
			background-color: #fff;
		}
		
	</style>



</head>

<body>
	<div class="head-div">
		<h2>PDF Server</h2>
	</div>
	<div id="tabs" style="width:100%;margin-right:2px;margin-top:3px;">
		 <ul>
			 <li><a href="#print">Record</a><span id="print_count">&nbsp;0&nbsp;</span></li>
			 <!-- <li><a href="#failed">Print Fail</a><span id="print_failed_count">&nbsp;0&nbsp;</span></li> -->
			 </ul>
		 	<div id="print" style="padding: 0 3px 0 2px;">
				<div class="record-div">
					<!-- 处理完pdf 的 记录 -->
					<div class="record-content">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
							<thead>
								<tr>
									<th class="right-title">Number</th>
									<th class="right-title">Time</th>
									<th class="right-title">Server</th>
									<th class="right-title" style="border-right-color: transparent;">Operate</th>
								</tr>
							</thead>
							<tbody id="record-content"></tbody>
						</table>
					</div>
					<!-- 结束 -->
				</div>
	  	 </div>
   </div>
	
	<div id="tempHtml" style="display: none;"></div>
</body>
</html>
