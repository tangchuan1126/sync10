<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>

<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInEntryWaitingOrNotKey" %>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<%@page import="com.fr.base.core.json.JSONArray"%>
<%@page import="com.fr.base.core.json.JSONObject"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="checkInEntryWaitingOrNotKey" class="com.cwc.app.key.CheckInEntryWaitingOrNotKey"/>
<jsp:useBean id="checkInEntryPriorityKey" class="com.cwc.app.key.CheckInEntryPriorityKey"/>
<html>
<head>
<%
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adminId = adminLoggerBean.getAdid();
long id = StringUtil.getLong(request,"entry_id"); 
long psId = adminLoggerBean.getPs_id();

String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/check_in/check_in_window.html?id="+id;
String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
String checkInFileUploadAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/checkin/checkInFileUploadAction.action";

DBRow[] files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(id,FileWithTypeKey.OCCUPANCY_MAIN);

DBRow[] equipments = checkInMgrZyj.windowCheckInFindEquipments(id);
String equipmentsString = StringUtil.convertDBRowArrayToJsonString(equipments);
//查询详细
DBRow row = checkInMgrZwb.findMainById(id);
int priority = row.get("priority", 0); 
//查询spot 下zone
DBRow[] spots = checkInMgrZwb.findSpotArea(psId);
String spotString = StringUtil.convertDBRowArrayToJsonString(spots);
DBRow[] doorZones = checkInMgrZwb.findAllZone(psId);
String doorZoneString = StringUtil.convertDBRowArrayToJsonString(doorZones);

//long window_group_id = 1000002;//warehouse 的分组
DBRow supervisorDefault = checkInMgrZyj.findSupervisors(psId, 0L);
String adids_default = supervisorDefault.getString("adids");
String adnames_default = supervisorDefault.getString("adnames");

//System.out.println(adids_default+","+adnames_default);

%>

<title>Check In Tasks</title>
<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
<style type="text/css">

input:disabled ~ span>label{pointer-events:none;}
#sequence span>label{cursor:pointer;}
.container{

	border:2px #dddddd solid;
	background:#eeeeee;
	/* padding:5px; */
	/* -webkit-border-radius:0px;
	-moz-border-radius:0px; */
	/*width:770px;*/
}

.wrap-search-input{
	position: relative;
	display: inline-block;
}

.icon-search-input{
	background: url(imgs/search.png) no-repeat center center;
	position: absolute;
	cursor: pointer;
	top: 0px;
	right: 0px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.wrap-search-input input:focus+.icon-search-input{
	border-color:#66afe9;
	outline:0;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)
}

.add-button{
	background-attachment: fixed;
	background: url(../imgs/add_18x18.png);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 20px;
	color: #000000;
	cursor:pointer;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
}

.update-button{
	background-attachment: fixed;
	background: url(../imgs/application_edit_18x18.png);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 20px;
	color: #000000;
	cursor:pointer;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
}

.del-button{
	background-attachment: fixed;
	background: url(../imgs/del_append_18x18.gif);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 20px;
	color: #000000;
	cursor:pointer;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
}

.print-button{
	background-attachment: fixed;
	background: url(../imgs/print_18x18.png);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 20px;
	color: #000000;
	cursor:pointer;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
}

.save-button{
	background-attachment: fixed;
	background: url(../imgs/standard_msg_ok_18x18.gif);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 20px;
	color: #000000;
	cursor:pointer;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
}

.cancel-button{
	background-attachment: fixed;
	background: url(../imgs/standard_msg_error_18x18.gif);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 20px;
	color: #000000;
	cursor:pointer;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
}

table.gridtable {
	font-family: verdana, arial, sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
	width: 600px;
}

/* processingStatusTips 表格的样式 */
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
div.table_head{
	position:fixed;
	display:inline-block;
	left:102px;
	width:601px;
	height:32px;
	background:#eee;
	line-height:32px;
	border:1px solid #333;
	font-weight: bold;
	padding:0px
}
div.table_head_td{
	border-right:1px solid #666;
	display:inline-block;
	text-align:center;
}

</style>

</head>
<body>
<input type="hidden" name="appointmentdate" id="appointmentdate" value="<%=!row.getString("appointment_time").equals("")?  DateUtil.showLocalTime(row.getString("appointment_time"), psId) :""%>"/>
<div style="height:390px; border:0px solid red;overflow:auto">
	<div id="Container" class="container">
		
		
	</div>
</div>
<div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;margin-top:2px;">
	<input id="closeNotice" class="normal-green-long" type="button" onclick="closeNotice();" value="Close Notice" >
	<input id="previousStep" class="normal-green" type="button" onclick="tabSelect(1);" value="Previous">
	<input id="_batchPrint" class="normal-green-long" type="button" onclick="batchPrint()" value="Batch Print" >
	<input id="_waitingOrNot" class="normal-green" type="button" onclick="moreSaveTask(<%=CheckInEntryWaitingOrNotKey.WAITING %>,0)" value="Waiting" >
	<input id="_waitingOrNot" class="normal-green" type="button" onclick="moreSaveTask(0,1)" value="Check In" >
</div>
<div id="print" style="display:none">
	
</div>
<div id="print_wms" style="display:none;">
	
</div>
<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/showMessage/showMessage.js" type="text/javascript"></script>
<!-- Tooltip classes -->
<link rel="stylesheet" href="../js/poshytip-1.2/src/tip-yellow/tip-yellow.css" type="text/css" />
<link rel="stylesheet" href="../js/poshytip-1.2/src/tip-violet/tip-violet.css" type="text/css" />
<link rel="stylesheet" href="../js/poshytip-1.2/src/tip-darkgray/tip-darkgray.css" type="text/css" />
<link rel="stylesheet" href="../js/poshytip-1.2/src/tip-skyblue/tip-skyblue.css" type="text/css" />
<link rel="stylesheet" href="../js/poshytip-1.2/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<link rel="stylesheet" href="../js/poshytip-1.2/src/tip-twitter/tip-twitter.css" type="text/css" />
<link rel="stylesheet" href="../js/poshytip-1.2/src/tip-green/tip-green.css" type="text/css" />

<!-- jQuery and the Poshy Tip plugin files -->
<script type="text/javascript" src="../js/poshytip-1.2/src/jquery.poshytip.js"></script>
<script type="text/javascript">


function checkTasks(){
	
	var returnVal = true;
	
	//var obj = $("input[name='saveTask']:visible");
	
	var obj = $("#Container input[name='number']:enabled").parents("div[name='av']").find("input[name='saveTask']");
	
	if(obj.length > 0){
		
		returnVal = saveTask(obj[0],0);
	}
	
	return returnVal;
}

$.fn.searchInput = function(events) {

    return this.each(function() {

    	if(this.nodeName !== 'INPUT'){
    		return;
    	}
    	var _self = $(this);
    	var height = _self.outerHeight();

    	_self.wrap('<div class="wrap-search-input"></div>');

    	var span = $('<span class="icon-search-input"></span>').width(height).height(height)
    					.insertAfter(_self.css('padding-right', height + 'px'));
    	
    	for(var e in events){
    		$.fn.on.call(span, e, events[e]);
    	}
    	_self.on('focus', function(){
    		
    		span.css('border-color', '#66afe9');
    	}).on('blur', function(){
    		
    		span.css('border-color', 'none');
    	});
    });
}

var spotJsonArray = eval('<%=spotString%>');
var doorJsonArray = eval('<%=doorZoneString%>');
var equipmentArray = eval('<%=equipmentsString%>');

var mainContainerNo = '<%=row.getString("gate_container_no")%>';
var firstHasValueContainerNo = '';

(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

var p=0;
   
var in_serch_val='';
     
(function(){
	fantian();
})();

// 点击单据状态时，非 Unprocess 状态显示 Pallet 信息
function processingStatus() {
	$("span[name=processingStatus]").each(function(){
		var detail_id = $(this).data( "detail_id" );
		var num_type = $(this).data( "num_type" );
		var processingStatus = $.trim($(this).text());

		$(this).poshytip({
			
			content: function(updateCallback) {
				if(processingStatus=='Unprocess') {
					updateCallback(content);
				} else {
					window.setTimeout(function() {
						if(detail_id != undefined && $.trim(detail_id) != ''&&num_type != undefined && $.trim(num_type) != '') {
							 $.ajax({
									url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxProcessingStatusAction.action',
									data:'detail_id='+detail_id +"&num_type="+num_type,
									dataType:'json',
									type:'post',
									beforeSend:function(request){
								    
									},
									success:function(data){
										var content;
										if( data != "" && data != null ) {
//											console.log(data);
											var count_name = "";
											var pallet_type = "PalletType";
											var isPickUp = 1;
											if(num_type==<%=moduleKey.CHECK_IN_CTN%> || num_type==<%=moduleKey.CHECK_IN_BOL%> || num_type==<%=moduleKey.CHECK_IN_DELIVERY_ORTHERS%>){
												count_name = "ItemCount";
												isPickUp = 0;
												if(num_type!=<%=moduleKey.CHECK_IN_DELIVERY_ORTHERS%>){
													pallet_type="Qty/Pallet";
												}
											}else if(num_type==<%=moduleKey.CHECK_IN_LOAD%> || num_type==<%=ModuleKey.CHECK_IN_PONO%> || num_type==<%=moduleKey.CHECK_IN_ORDER%> || num_type==<%=moduleKey.CHECK_IN_PICKUP_ORTHERS%>){
												count_name = "PalletCount";
											}
											content = '<div style="width:620px;height:350px;overflow-y:auto;padding:0px;margin:0px;display:inline-block;">';
											/* content += '<div class="table_head">';
											content += '<div class="table_head_td" style="width:108px;">Spot/Door</div>';
											content += '<div class="table_head_td" style="width:132px;">Pallet Number</div>';
											content += '<div class="table_head_td" style="width:108px;">Pallet Type</div>';
											content += '<div class="table_head_td" style="width:114px;">'+count_name+'</div>';
											content += '<div class="table_head_td" style="width:133px;border:none;">Scan Time</div>';
											content += '</div>'; */
											content += '<table class="gridtable" style="margin:0px;left:10px"><tr>'
											+'<th style="width:104px;">Spot/Door</th>'
											+'<th style="width:133px;">PalletNumber</th>'
											+'<th style="width:105px;">'+pallet_type+'</th>'
											+'<th style="width:112px;">'+count_name+'</th>'
											+'<th style="width:134px;">ScanTime</th></tr>';
											if ( data.length > 0 ) {
												for(var i = 0; i<data.length; i++) {
													var resource = data[i].resources_name;
//													console.log(resource);
													if ( data[i].resources_type == 1 ) {
														resource = "Door: " + resource;
													} else {
														resource = "Spot: " + resource;
													}
													
													for(var j = 0; j < data[i].datas.length; j++) {
														var pallet_number = data[i].datas[j].pallet_number != '' ? data[i].datas[j].pallet_number : '';
														var pallet_type = data[i].datas[j].pallet_type;
														var pallet_type_count = data[i].datas[j].pallet_type_count;
														var count_content = "";
														if(isPickUp==1){
															count_content = data[i].datas[j].pallet_type_count;
														}else{
															count_content = "";
															if(data.datas[j].cp_quantity && data.datas[j].cp_quantity>0){
																count_content += " G: "+data.datas[j].normal_qty;
																count_content += " D: "+data.datas[j].damage_qty;
															}
														}
														var scan_time = data[i].datas[j].scan_time;
														
														if ( data[i].datas.length > 1 ) {
															if( j == 0 ) {
																content += '<tr><td rowspan=' + data[i].datas.length + '>' + resource + '</td>';
																content += '<td>' + pallet_number + '</td>'
																 		 + '<td>' + pallet_type + '</td>'
																 		 + '<td>' + count_content + '</td>'
																 		 + '<td>' + scan_time + '</td>';
															} else {
																content += '<tr><td>' + pallet_number + '</td>'
																 + '<td>' + pallet_type + '</td>'
																 + '<td>' + count_content + '</td>'
																 + '<td>' + scan_time + '</td></tr>';	
															}
														} else {
															content += '<tr><td rowspan=' + data[i].datas.length + '>' + resource + '</td>'
																	 + '<td>' + pallet_number + '</td>'
															 		 + '<td>' + pallet_type + '</td>'
															 		 + '<td>' + count_content + '</td>'
															 		 + '<td>' + scan_time + '</td>';															
														}
													}

													content += '</tr>';	
												}			
												
												content += "<table></div>";
											} else {
												if ( data.result == 'noData' ) {
													content = "No Records!";
													updateCallback( content );
												} else {
//													console.log(data);
													var resource = data.resources_name;
													if ( data.resources_type == 1 ) {
														resource = "Door: " + resource;
													} else {
														resource = "Spot: " + resource;
													}
													
													for(var j = 0; j < data.datas.length; j++) {
														var pallet_number = data.datas[j].pallet_number != '' ? data.datas[j].pallet_number : '';
														var pallet_type = data.datas[j].pallet_type;
														var pallet_type_count = data.datas[j].pallet_type_count;
														var scan_time = data.datas[j].scan_time;
														var count_content = "";
														if(isPickUp==1){
															count_content = data.datas[j].pallet_type_count;
														}else{
															count_content = "";
															if(data.datas[j].cp_quantity && data.datas[j].cp_quantity>0){
																count_content += " G: "+data.datas[j].normal_qty;
																count_content += " D: "+data.datas[j].damage_qty;
															}
														}
														if ( data.datas.length > 1 ) {
															if( j == 0 ) {
																content += '<tr><td rowspan=' + data.datas.length + '>' + resource + '</td>';
																content += '<td>' + pallet_number + '</td>'
																 		 + '<td>' + pallet_type + '</td>'
																 		 + '<td>' + count_content + '</td>'
																 		 + '<td>' + scan_time + '</td></tr>';
															} else {
																content += '<tr><td>' + pallet_number + '</td>'
																 + '<td>' + pallet_type + '</td>'
																 + '<td>' + count_content + '</td>'
																 + '<td>' + scan_time + '</td></tr>';	
															}
														} else {
															content += '<tr><td rowspan=' + data.datas.length + '>' + resource + '</td>'
																	 + '<td>' + pallet_number + '</td>'
															 		 + '<td>' + pallet_type + '</td>'
															 		 + '<td>' + count_content + '</td>'
															 		 + '<td>' + scan_time + '</td></tr>';															
														}
													}

												}			
												
													
												}
											content += "</table>";												
											
											updateCallback(content);
										} else {
											if ( data.result == 'noData' ) {
												content = "No Records!";
												updateCallback( content );
											}
										}
										
									},
									error:function(){
										showMessage( "System error!", "error" ); 
									}
								 });		
						}	
					}, 3000 );					
				}
				
				return 'Loading...';
			},
			alignTo: 'target',
			alignY: 'center'
			

			
		});				
	})	
	
}
    
function fantian(){
	
	var id=<%=id%>;
	
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowFindTaskEquipResByEidAction.action',
  		dataType:'json',
  		data:'entry_id='+id,
		error:function(serverresponse, status){
			
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		},
	 	success:function(data){
	 		
			if(data.length>0){
	 			
				for(var i=0;i<data.length;i++){
					
					var entryType='';
					var delivery_checked='';
					var pick_up_checked='';
					var zone_id='';
					var detail_id='';
					var num_id='';
					var door_name='';
					var door_id='';
					var noticeName=data[i].notice_names;
					var noticeNameId=data[i].notice_ids;
					var email=data[i].email;
					var page=data[i].page;
					var message=data[i].message;
					var note=data[i].note;
					var supplier_id='';
					var order=data[i].order_no;
					var num_type='';
					var customer_id='';
					var company_id='';
					var staging_area_id='';
				    var freight_term='';
					var title='';
					var containerNo='';
					var occupancy_type='';
					var spot_id='';
					var door_checked='';
					var spot_checked='';
					var number_status='';
					var number_status_value = '';
					var equipment_id = '';
					var equipment_number = '';
					
					var equipment_type = '';
					var equipment_type_value = '';
					var seal_pick_up = '';
					var appointment_date = '';
					var number_order_status='';
					var order_system_type='';
					var receipt_no='';
					
					if(data[i].receipt_no!=undefined){
						receipt_no =data[i].receipt_no; 
					}
					if(data[i].appointment_date!=undefined){
						appointment_date=data[i].appointment_date.substring(0,19);
					}
					if(data[i].equipment_type!=undefined){
						equipment_type=data[i].equipment_type;
					}
					if(data[i].equipment_type_value!=undefined){
						equipment_type_value=data[i].equipment_type_value;
					}
					if(data[i].seal_pick_up!=undefined){
						seal_pick_up=data[i].seal_pick_up;
					}
					
					if(data[i].equipment_id!=undefined){
						equipment_id=data[i].equipment_id;
					}
					
					if(data[i].equipment_number!=undefined){
						equipment_number=data[i].equipment_number;
					}
					
					if(data[i].number_status!=undefined){
						number_status = data[i].number_status;
						number_status_value = data[i].number_status_value;
					}
					
					if(data[i].occupancy_type!=undefined){
						occupancy_type=data[i].occupancy_type;
					}
					if(occupancy_type==<%=OccupyTypeKey.DOOR%>){
						door_checked='selected';
					}else if(occupancy_type==<%=OccupyTypeKey.SPOT%>){
						spot_checked='selected';
					}
					
					if(data[0].ctn_number!=undefined){
						containerNo=data[0].ctn_number;
					}else{
						containerNo='<%=row.get("gate_container_no","")%>';
					}
					if(data[i].company_id!=undefined){
						company_id=data[i].company_id;
					}
					if(data[i].customer_id!=undefined){
						customer_id=data[i].customer_id;
					}
					if(data[i].number_type!=undefined){
						num_type=data[i].number_type;
					}
					if(data[i].number!=undefined){
						num_id=data[i].number.toUpperCase();
					}
					if(data[i].supplier_id!=undefined){
						supplier_id=data[i].supplier_id;
					}
					if(data[i].staging_area_id!=undefined){
						staging_area_id=data[i].staging_area_id;
					}
					if(data[i].freight_term!=undefined){
						freight_term=data[i].freight_term;
					}
					if(data[i].zone_id!=undefined){
						zone_id=data[i].zone_id;
					}				
					detail_id=data[i].dlo_detail_id;
					if(data[i].occupy_name!=undefined){
						 door_name=data[i].occupy_name;
					}
					if(data[i].rl_id!=undefined){
						 door_id=data[i].rl_id;
					}
					if(num_type==<%=ModuleKey.CHECK_IN_PONO%>){
						title="PO";
					}
					if(num_type==<%=ModuleKey.CHECK_IN_ORDER%>){
						title="ORDER";
					}
					if(num_type==<%=ModuleKey.CHECK_IN_LOAD%>){
						title="LOAD";
					}
					
					if(num_type==<%=ModuleKey.SMALL_PARCEL%>){
						title="Small Parcel";
					}
					
					if(num_type==<%=ModuleKey.CHECK_IN_CTN%>){
						title="CTNR";
					}
					if(num_type==<%=ModuleKey.CHECK_IN_BOL%>){
						title="BOL";
					}
<%-- 					if(num_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%> || num_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>  ){ --%>
// 						title="OTHERS";
// 					}
					if(num_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>){
						title="PickUp";
					}
					if(num_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%>){
						title="Delivery";
					}
					
					
// 					$.ajax({
// 				  		type:'post',
// 				  		async:false,
<%-- 				  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxWindowFindNoticesAction.action', --%>
// 				  		dataType:'json',
// 				  		data:'schedule_id='+detail_id,
// 				  		error:function(serverresponse, status){
							
// 				  			showMessage(errorMessage(serverresponse.responseText),"alert");
// 				  		},
// 					 	success:function(da){
// 					 		if(da.length>0){
// 					 			email=da[0].sms_email_notify;
// 					 			message=da[0].sms_short_notify;
// 					 			page=da[0].schedule_is_note;
// 					 			for(var b=0;b<da.length;b++){					 				
// 					 				noticeName+=da[b].employe_name+',';
// 					 				noticeNameId+=da[b].adid+',';
// 					 			}
// 					 			var notes=da[0].schedule_detail.split("Note：");
// 					 			note=notes[1];
// 					 			if(note==undefined){
// 					 				note='';
// 					 			}
// 					 		}
// 					 	}
// 					 });
					
					//if(noticeName!=''){
						//noticeName=noticeName.substring(0,noticeName.length-1);
						//noticeNameId=noticeNameId.substring(0,noticeNameId.length-1);
					//}	
					
					if(num_type==<%=ModuleKey.CHECK_IN_BOL%> || num_type==<%=ModuleKey.CHECK_IN_CTN%> || num_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS %>){
						delivery_checked='checked';	
						entryType='DELIVERY';
					}else if(num_type==<%=ModuleKey.CHECK_IN_LOAD%> || num_type==<%=ModuleKey.CHECK_IN_PONO%> || num_type==<%=ModuleKey.CHECK_IN_ORDER%> || num_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS %> || num_type==<%=ModuleKey.SMALL_PARCEL%>){
						entryType='PICK UP';
						pick_up_checked='checked';
					}
					if(num_type=='' ){
						if(pick_up_checked=='checked'){
							title="LOAD/PO/ORDER";
						}else{
							title="CTNR/BOL";
						}
					}
					
					var eachTask = true;
					add(
						equipment_number
						,equipment_id
						,eachTask
						,entryType
						,delivery_checked
						,pick_up_checked
						,zone_id
						,detail_id
						,num_id
						,door_name
						,door_id
						,noticeName
						,noticeNameId
						,note
						,num_type
						,customer_id
						,company_id
						,order
						,supplier_id
						,staging_area_id
						,title
						,freight_term
						,containerNo
						,door_checked
						,spot_checked
						,number_status
						,number_status_value
						,equipment_type
						,equipment_type_value
						,seal_pick_up
						,appointment_date
						,receipt_no
					);
				}
				//计算条数
				calculationAv();
				//计算有效的条数
				effectiveNumber();
				//计算序列号
				sequence();
				//计算添加按钮显示的位置
				calcAddTask();
				//非编辑状态
				unEditTask();

		 	}else{
		 		
	 			$("#Container").html('<input type="button" name="addTask" class="short-short-button" value="Add" onclick="yuAdd(this)"/>');
	 		}	
			
		 	processingStatus();
  		}
    });	
}

function yuAdd(){
	for ( var i = 0; i < equipmentArray.length; i++ ) {
		if( equipmentArray[i].equipment_status != 5 ) {
			break;
		} else if ( i == equipmentArray.length - 1 ) {
			showMessage( "Cannot add task, equipment has left!" );
			return;
		}
	}
	if(!equipmentArray){
		
		showMessage("No equipment.","alert");
		return false;
	}
	
	//按钮全部隐藏
	$("#Container input[type='button']").hide();
	
	var entryType='';
	var delivery_checked='';
	var pick_up_checked='';
	var zone_id='';
	var detail_id='';
	var num_id='';
	var door_name='';
	var door_id='';
	var noticeName='';
	var noticeNameId='';
	var note='';
	var customer_id='';
	var company_id='';
	var num_type='';
	var order='';
	var containerNo='';
	var door_checked='';
	var spot_checked='';
	var yu_jia_type=$('#entryType').html();
	var title='';
	var equipment_id = '';
	var equipment_number = '';
	var number_status = '';
	var number_status_value = '';
	
	var equipment_type = '';
	var equipment_type_value = '';
	var seal_pick_up = '';
	var appointment_date = '';
	var number_order_status='';
	var order_system_type='';
	var receipt_no='';
	
	if(yu_jia_type=='DELIVERY'){
		
		delivery_checked='checked';
		entryType='DELIVERY';
		title="CTNR/BOL";
		
	}else if(yu_jia_type == 'PICK UP' ){

		pick_up_checked='checked';
		entryType='PICK UP';
		title="LOAD/PO/ORDER";
	}else{
		
		entryType='NONE';
		title="OTHERS";
	}
	
	//点add时 如果有 通知的人 就默认加上
	var winDiv=$('div[name="av"]');
	
	if(winDiv.length>0){
	
		noticeName = $('input[name="tongzhi"]',winDiv[0]).val();
		noticeNameId = $('input[name="tongzhiid"]',winDiv[0]).val();
		
		//如果有task，默认是上一个task选择的设备
		//containerNo = $('select[name="containerNo"]',winDiv[0]).val();
		equipment_id = $('select[name="containerNo"]',winDiv[0]).val();

		var door_spot = $('select[name="door_spot"]',winDiv[0]).val();
		
		zone_id = $('select[name="allzone"]',winDiv[0]).val();
		door_name = $('input[name="door"]',winDiv[0]).val();
		
		door_id = $('input[name="door"]',winDiv[0]).attr("text");

		if(door_spot==<%=OccupyTypeKey.DOOR%>){
			door_checked='selected';
		}else{
			spot_checked='selected';
		}
	}else{
		
		//没有task时，添加task时，如果只有一个设备，默认是那个设备。
		if(equipmentArray && equipmentArray.length == 1){
			
			equipment_id = equipmentArray[0].equipment_id;
		}
		//noticeName = '<%=adnames_default%>';
		//noticeNameId = '<%=adids_default%>';
	}
	if(noticeNameId=='')
	{
		noticeName = '<%=adnames_default%>';
		noticeNameId = '<%=adids_default%>';
	}

	var eachTask = false;
	add(
		equipment_number
		,equipment_id
		,eachTask
		,entryType
		,delivery_checked
		,pick_up_checked
		,zone_id
		,detail_id
		,num_id
		,door_name
		,door_id
		,noticeName
		,noticeNameId
		,note
		,num_type
		,customer_id
		,company_id
		,order
		,''
		,''
		,title
		,''
		,containerNo
		,door_checked
		,spot_checked
		,number_status
		,number_status_value
		,equipment_type
		,equipment_type_value
		,seal_pick_up
		,appointment_date
		,receipt_no
		
		);
	
	processingStatus();
	//计算条数
	calculationAv();
	//计算序列号
	sequence();
	
	//滚动条下移
	$("#Container").parent("div").scrollTop(0);
	
	// 移除掉离开的设备
	if( arguments[0] != undefined ) {
		var selector = $( 'select[name="containerNo"]:not("disabled")' ).attr( 'id' );
		$( "#" + selector + " option" ).each( function() {
			for ( var j = 0; j < equipmentArray.length; j++ ) {
				if ( equipmentArray[j].equipment_status == 5 && equipmentArray[j].equipment_number == $( this ).val() ) {
					$( "#" + selector + " option" ).get( this.index ).remove();
				}
			}
		} );
	}	
}

function changeEquipmentsType(target){
	
	$(target).parents("div[name='av']").find("span[name='equipment_type']").html($(target).find("option:selected").data("typeval"));
	$(target).parents("div[name='av']").find("input[name='seal_pick_up']").val($(target).find("option:selected").data("seal"));
}

function closeNoticeOper(){
	var entryId = <%=id%>;
	$.ajax( {
  		type: 'post',
  		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCloseNoticeByEidAction.action',
  		dataType: 'json',
  		data: 'entry_id=' + entryId,
		error: function( serverresponse, status ) {
  			showMessage( "System error", "alert" );
  		},
	 	success: function( data ) {
	 		if( data != null && data.result ) {
	 			if ( data.result == "success" ) {
	 				showMessage( "Close Window Schedules Success!", "alert" );
	 			} else if ( data.result == "allClose" ) {
	 				showMessage( "All the window schedule had closed!", "alert" );
	 			}
	 			
	 			setTimeout( function() {
	 				window.parent.location.reload();
	 			}, 1000 );  
	 			
	 		}
	 	}
	} );
}
// 关闭 window schedule
function closeNotice() {
	$.artDialog({
	    content: '<span style="font-size:14px;">Are you sure close the window schedules?</span>',
	    icon: 'warning',
	    width: 350,
	    height: 70,
	    title:'',
	    okVal: 'Confirm',
	    ok: function () {
	    	closeNoticeOper();
	    },
	    cancelVal: 'Cancel',
	    cancel: function(){
	    	
		}
	});	
}



function add(
		equipment_number
		,equipment_id
		,eachTask
		,entryType
		,delivery_checked
		,pick_up_checked
		,zone_id
		,detail_id
		,num_id
		,door_name
		,door_id
		,noticeName
		,noticeNameId
		,note
		,num_type
		,customer_id
		,company_id
		,order
		,supplier_id
		,staging_area_id
		,title
		,freight_term
		,containerNo
		,door_checked
		,spot_checked
		,number_status
		,number_status_value
		,tequipment_type
		,tequipment_type_value
		,tseal_pick_up
		,appointment_date
		,receipt_no
	){
	
	var text_decoration;
	if(number_status_value=="Unprocess") {
		text_decoration="text-decoration: none;";
	} else {
		text_decoration="text-decoration: underline;";
	}
	
	if(title==undefined){
	
		if(pick_up_checked=='checked'){
			title="LOAD/PO/ORDER";
		}else{
			title="CTNR/BOL";
		}
	}
	p++;
	
	var con = $('#Container');
	var html='';
	var raHtml='';
	var staging_zone_html='';
	var zoneHtml='';
	
	//取货还是送货
	if(entryType=='NONE'){
	
		raHtml=
			'<div>'
				+'<input type="radio" name="ra'+p+'" id="ra_pick'+p+'" value="0"/>'
				+'<span name="ckName" style="margin-left:2px;"><label for="ra_pick'+p+'">Pick Up</label></span>'
			+'</div>'
			+'<div>'
				+'<input type="radio" name="ra'+p+'" id="ra_delivery'+p+'" value="1"/>'
				+'<span name="ctnName"style="margin-left:2px;"><label for="ra_delivery'+p+'">Delivery</label></span>'
			+'</div>'	
			+ '<div style="margin-top: 10px;">'
				+ '<span name="processingStatus"  id="processingStatus"  data-detail_id=' + detail_id + ' data-num_type='+num_type+' style="margin-left: 8px; font-weight: bold;'+text_decoration+'">' + number_status_value + '</span>'
			+ '</div>';			
	}else{
	
		raHtml=
			'<div>'
				+'<input type="radio" name="ra'+p+'" id="ra_pick'+p+'" value="0" '+pick_up_checked+' />'
				+'<span name="ckName" style="margin-left:2px;"><label for="ra_pick'+p+'">Pick Up</label></span>'
			+'</div>'
			+'<div>'
				+'<input type="radio" name="ra'+p+'" id="ra_delivery'+p+'" value="1" '+delivery_checked+' />'
				+'<span name="ctnName" style="margin-left:2px;"><label for="ra_delivery'+p+'">Delivery</label></span>'
			+'</div>'
			+ '<div style="margin-top: 10px;">'
				+ '<span name="processingStatus"  id="processingStatus" data-detail_id=' + detail_id +  ' data-num_type='+num_type+' style="margin-left: 8px; font-weight: bold;'+text_decoration+'">' + number_status_value + '</span>'
			+ '</div>';
	}
	
	
	//取货
	if(entryType=='PICK UP'){
	
		staging_zone_html= 
			'<td align="right" height="30px;" id="stagingTitle">'+
				'Staging:&nbsp;'+
			'</td>'+
			'<td id="stagingVal">'+
				'<input style="width:180px;height:24px;" id="staging'+p+'" disabled="disabled" value="'+staging_area_id+'">'+
			'</td>'+
			'<td align="right" id="zoneTitle">'+
				'Zone:&nbsp;'+
			'</td>'+
			'<td id="zoneVal">'+
				'<input style="width:180px;height:24px;" id="staging_zone'+p+'" disabled="disabled">'+
			'</td>'+
			'<td style="width:73px;text-align:right;" id="customerTitle">'+
				'Customer:'+
			'</td>'+
			'<td id="customerVal">'+
				'<input style="width:180px;height:24px;" id="staging_Customer'+p+'" name="staging_Customer" value="'+customer_id+'" disabled="disabled">'+
			'</td>';
	//送货
	}else if(entryType=='NONE'){
		staging_zone_html= 
			'<td align="right" height="30px;" id="stagingTitle">'+
				'Title:&nbsp;'+
			'</td>'+
			'<td id="stagingVal">'+
				'<input style="width:180px;height:24px;" id="searchTitle'+p+'" disabled="disabled" value="'+supplier_id+'">'+
			'</td>'+
			'<td align="right" id="zoneTitle">'+
				'Zone:&nbsp;'+
			'</td>'+
			'<td id="zoneVal">'+
				'<input style="width:180px;height:24px;" id="searchZone'+p+'" disabled="disabled">'+
			'</td>'+
			'<td style="width:73px;text-align:right;" id="customerTitle">'+
				'&nbsp;'+
			'</td>'+
			'<td id="customerVal">'+
			'&nbsp;'+	                                   
			'</td>';
	}else{
		staging_zone_html= 
			'<td align="right" height="30px;" id="stagingTitle">'+
				'Title:&nbsp;'+
			'</td>'+
			'<td id="stagingVal">'+
				'<input style="width:180px;height:24px;" id="searchTitle'+p+'" disabled="disabled" value="'+supplier_id+'">'+
			'</td>'+
			'<td align="right" id="zoneTitle">'+
				'Zone:&nbsp;'+
			'</td>'+
			'<td id="zoneVal">'+
				'<input style="width:180px;height:24px;" id="searchZone'+p+'" disabled="disabled">'+
			'</td>'+
			'<td style="width:73px;text-align:right;" id="customerTitle">'+
				'Receipt No:'+
			'</td>'+
			'<td id="customerVal">'+
			'<input style="width:180px;height:24px;" value="'+(receipt_no==0?"":receipt_no)+'" id="receipt_no'+p+'" disabled="disabled">'+	                                   
			'</td>';
	}
	
	//遍历还是添加
	var eachTaskHtml = '';
	if(eachTask){
		//pickUp or Delivery
		var pickUpOrDelivery = 1;
		if(entryType=='PICK UP')
		{
			pickUpOrDelivery = 0;
		}
		eachTaskHtml = 
			'<input type="button" title="EDIT" name="editTask" class="update-button" onclick="editTask(this);"/>'+
			'<input type="button" title="SAVE" name="saveTask" class="save-button" onclick="saveTask(this,0);" style="display:none;"/><br/><br/>'+
			'<input type="button" title="CANCEL" name="cancelTask" class="cancel-button" onclick="cancelTask(this);" style="display:none;"/>'+
			'<input type="button" title="DELETE" name="detButton" zhi="'+p+'" class="del-button" /><br/><br/>'+
			'<input type="button" title="PRINT" class="print-button" onclick="openLoadTemplate('+p+','+pickUpOrDelivery+')" /><br/><br/>'+
			'<input type="button" title="ADD" name="addTask" class="add-button" onclick="yuAdd(this)"/>';
	}else{
		
		eachTaskHtml = 
			'<input type="button" title="SAVE" name="saveTask" class="save-button" onclick="saveTask(this,0);"/><br/><br/>'
			+'<input type="button" title="CANCEL" name="cancelTask" zhi="'+p+'" class="cancel-button" onclick="cancelTask(this);" /><br/><br/>';
	}
	
	//下拉列表
	var equipment_type = '';
	var equipment_type_value = '';
	var seal_pick_up = '';
	var equipmentsHtml = '<select style="width:180px;height:24px;" name="containerNo" id="containerNo'+p+'" onchange="changeEquipmentsType(this);">';
	
	equipmentsHtml += '<option data-seal="" data-type="" data-typeval="" value="">select...</option>';
	
	var existEquipment = true;
	
	if(equipmentArray){
		for(var i=0;i<equipmentArray.length;i++){
			
			if(equipment_id == equipmentArray[i].equipment_id){
				
				equipmentsHtml += '<option data-seal='+ equipmentArray[i].seal_pick_up +' data-type='+ equipmentArray[i].equipment_type +' data-typeval='+equipmentArray[i].equipment_type_value+' selected value='+equipmentArray[i].equipment_id+'>'+equipmentArray[i].equipment_number+'</option>';
				
				equipment_type = equipmentArray[i].equipment_type;
				equipment_type_value = equipmentArray[i].equipment_type_value;
				seal_pick_up = equipmentArray[i].seal_pick_up;
				
				existEquipment = false;
			}else{
				
				equipmentsHtml += '<option data-seal='+ equipmentArray[i].seal_pick_up +' data-type='+ equipmentArray[i].equipment_type +' data-typeval='+equipmentArray[i].equipment_type_value+' value='+equipmentArray[i].equipment_id+'>'+equipmentArray[i].equipment_number+'</option>';
			}
		}
	}
	//如果task的equipment_id有值，如果那个下拉列表中没有这个equipment_id，把task的equipment_id加到下拉列表中
	if(equipment_id!='' && existEquipment){
		
		equipmentsHtml += '<option data-seal='+tseal_pick_up+' data-type='+tequipment_type+' data-typeval='+tequipment_type_value+' selected value='+equipment_id+'>'+equipment_number+'</option>';
	}
	
	equipmentsHtml +='</select>';
	
	//所有
	html=
		'<div id="av" name="av">'
			+'<table width="100%" border="0" cellspacing="0" cellpadding="0" >'
    			+'<tr>'
		        	+'<td width="9%" style="border-right:1px dashed #999;color:blue;font-weight:bold;" id="sequence" name="sequence" align="center">'
		        		+'<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">'
		        			+'<tr>'
								+'<td style="border-bottom:1px dashed #999;text-align:center;height:30px;">'
									+'<span name="sequence_index">'
										
									+'</span>'
								+'</td>'
		        			+'</tr>'
							+'<tr>'
								+'<td style="height:60px;">'
									+raHtml
								+'</td>'
		        			+'</tr>'
							+'<tr>'
								+'<td style="height:50px;">&nbsp;'
									
								+'</td>'
		        			+'</tr>'
		        		+'</table>'
		        	+'</td>'
					
		        	+'<td width="85%">'
						
						+'<input id="number_status'+p+'" value="'+number_status+'" type="hidden" name="number_status" />'
						+'<input id="companyId'+p+'" value="" type="hidden" />'
						+'<input id="customerId'+p+'" value="" type="hidden" />'
						+'<input id="zone'+p+'" value="'+zone_id+'" type="hidden" />'
						+'<input id="detailid'+p+'" value="'+detail_id+'" type="hidden" />'
						
						+'<table width="100%" border="0" cellspacing="0" cellpadding="0">'
							+'<tr>'
								+'<td style="width:114px;" align="right" height="30px;">'
									+'<span name="title" id="title'+p+'">'+title+':</span>&nbsp;'
								+'</td>'
								+'<td style="width:170;">'
									+'<input style="width:178px;height:24px;" type="text" name="number" id="'+p+'" size="20" value="'+num_id+'" onfocus="validateSelect(this)"/>&nbsp;'
									
									+'<input type="hidden" name="number_type" id="number_type'+p+'" value="'+num_type+'" />'
                                    +'<input type="hidden" name="customer_id" id="customer_id'+p+'" value="'+customer_id+'"/>'
									+'<input type="hidden" name="company_id" id="company_id'+p+'" value="'+company_id+'"/>'
									+'<input type="hidden" name="account_id" id="account_id'+p+'" value="" />'
									+'<input type="hidden" name="order_no" id="order_no'+p+'" value="'+order+'"/>'
                                    +'<input type="hidden" name="po_no" id="po_no'+p+'" value=""/>'
                                    +'<input type="hidden" name="supplier_id" id="supplier_id'+p+'" value="'+supplier_id+'"/>'
                                    +'<input type="hidden" name="staging_area_id" id="staging_area_id'+p+'" value="'+staging_area_id+'"/>'
                                    +'<input type="hidden" name="freight_term" id="freight_term'+p+'" value="'+freight_term+'"/>'
                                    +'<input type="hidden" name="seal_pick_up" id="seal_pick_up'+p+'" value="'+seal_pick_up+'"/>'
                                    +'<input type="hidden" name="number_order_status" id="number_order_status'+p+'" value=""/>'
                                    +'<input type="hidden" name="order_system_type" id="order_system_type'+p+'" value=""/>'
                            //        +'<input type="hidden" name="receipt_no" id="receipt_no'+p+'" value=""/>'
								+'</td>'
                                +'<td style="width:42px;text-align:right;">'+
									'In:&nbsp;'+
								'</td>'+
								'<td width="170px;">'+
									
									equipmentsHtml+
								'</td>'+
								'<td style="width:71px;text-align:left;">'+
									'&nbsp;'+
									'<span name="equipment_type">'+equipment_type_value+'</span>'+
								'</td>'+
								'<td width="170px;">'+
									'&nbsp;'+
									'<span name="number_status_display">'+''+'</span>'+//number_status_value
									/* '<input style="width:180px;height:24px;" type="text" id="tongzhi'+p+'" name="tongzhi" size="20" value="'+noticeName+'" />'+
									'<input type="hidden" id="tongzhi'+p+'Id" name="tongzhiid" value="'+noticeNameId+'" />'+	 */                                   
								'</td>'+
							'</tr>'+
							'<tr>'+
								'<td align="right" height="30px;">'+
									'<select name="door_spot" zhi="'+p+'" id="door_spot'+p+'"><option value="<%=OccupyTypeKey.DOOR%>"'
									+door_checked
									+'>Door</option><option value="<%=OccupyTypeKey.SPOT%>" '
									+spot_checked
									+'>Spot</option></select>:&nbsp;'+
								'</td>'+
								'<td>'+
									'<input style="width:180px;height:24px;" type="text" name="door" id="door'+p+'" size="20" in="'+p+'"  value="'+door_name+'" text="'+door_id+'" />'+
								'</td>'+
								'<td>'+
									'&nbsp;'+
								'</td>'+
								'<td>'+
									'<select style="width:180px;height:24px;" name="allzone" id="door'+p+'zone" zhi="'+p+'"><option value="0">N/A ZONE</option></select>'+
								'</td>'+
								
								'<td style="text-align:right;">'+
									'Supervisor:'+
									/* 'Email:<input id="youjian'+p+'" type="checkbox" checked="checked">'+ */
								'</td>'+
								
								'<td>'+
									'<input style="width:180px;height:24px;" type="text" id="tongzhi'+p+'" name="tongzhi" size="20" value="'+noticeName+'" />'+
									'<input type="hidden" id="tongzhi'+p+'Id" name="tongzhiid" value="'+noticeNameId+'" />'+
									/* '&nbsp;Message:<input id="duanxin'+p+'" type="checkbox" checked="checked">&nbsp;Browser:<input id="yemian'+p+'" type="checkbox" checked="checked">'+	 */                                   
								'</td>'+
							'</tr>' +
							'<tr>'+
								staging_zone_html+
							'</tr>'+
							'<tr>' +
								'<td style="text-align: right;">' +
									'Appointment:'+
								'</td>' +	
								'<td>' +
									'<input style="width: 180px; height: 24px;" type="text" id="appointment_date' + p + '" name="appointment_date" size="20" value="' + appointment_date + '" disabled="disabled"/>' +
								'</td>' +
							'</tr>' +							
							'<tr>'+
								'<td width="" align="right" height="50px;">'+
									'Note:&nbsp;'+
								'</td>'+
								'<td width="" colspan="5">'+
									'<textarea name="textfield" id="beizhu'+p+'" cols="91" rows="1" >'+note+'</textarea>'+
								'</td>'+		                                
							'</tr>'+
						'</table>'+
					'</td>'+
					'<td align="center" width="6%">'+
						eachTaskHtml +
					'</td>'+
				'</tr>'+
			'</table>'+
			'<hr size="1"/>'+
		'</div>';
	
	var $container=$(html).prependTo(con);                     //获得容器
	var $door=$("input[name='door']",$container);               //获得门的文本框
    var $number=$("input[name='number']",$container); 		    //单据id
    var $number_type=$("input[name='number_type']",$container); //单据类型
    var $allzone=$("select[name='allzone']",$container);        //获得zone的下拉
    var $det_button=$("input[name='detButton']",$container);    //获得删除按钮
    //var $cal_button=$("input[name='cancelButton']",$container);
    var $tongzhi=$("input[name='tongzhi']",$container);         //获得通知文本框
    var $ra=$("input[name='ra"+p+"']",$container);              //单选按钮
    var $customer_id=$("input[name='customer_id']",$container); //customer_id
    var $company_id=$("input[name='company_id']",$container); 	//company_id
    var $account_id=$("input[name='account_id']",$container); 	//account_id
    var $order_no=$("input[name='order_no']",$container); 		//order_no
    var $po_no=$("input[name='po_no']",$container); 			//po_no
    var $supplier_id=$("input[name='supplier_id']",$container);
    var $staging_area_id=$("input[name='staging_area_id']",$container);
    var $freight_term=$("input[name='freight_term']",$container);
    //var $find=$("input[name='find']",$container);               //查询按钮
    var $containerNo=$("input[name='containerNo']",$container); //货柜号
    var $door_spot=$("select[name='door_spot']",$container);    //门或停车位下拉列表
    var $number_status=$("input[name='number_status']",$container);    //是否可以删除状态
    
    //门或停车位下拉列表 改变事件
    $door_spot.change(function(){
    	
    	//改变门或停车位时 清空值
    	$door.attr("value","");
    	$door.attr("text","");
    	
    	var zhi=$door_spot.attr("zhi");
   		var zone=$('#door'+p+'zone');
   		$allzone.html('<option value="0">N/A ZONE</option>');
   		 
    	 if($door_spot.val()==<%=OccupyTypeKey.SPOT%>){
    		
 		   	for(var y = 0; y < spotJsonArray.length; y ++){
 		   		if(zone_id==spotJsonArray[y].area_id){
 		   			$allzone.append('<option value='+spotJsonArray[y].area_id+' selected="selected" >'+spotJsonArray[y].area_name+'</option>');
 		   		}else{
 		   			$allzone.append('<option value='+spotJsonArray[y].area_id+'>'+spotJsonArray[y].area_name+'</option>');
 		   		}
 		   	}
 	    }else if($door_spot.val()==<%=OccupyTypeKey.DOOR%>){
 	    	//查询本仓库下zone
//  		    $.ajax({
//  		  		type:'post',
<%--  		  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindAllZoneByPsIdAction.action', --%>
//  		  		dataType:'json',
<%--  		  		data:'psId='+<%=psId%>, --%>
//  		  		error:function(serverresponse, status){
 					
//  		  			showMessage(errorMessage(serverresponse.responseText),"alert");
//  		  		},
//  			 	success:function(data){
					if(doorJsonArray && doorJsonArray.length>0){
						for(var i=0;i<doorJsonArray.length;i++){
							if(zone_id==doorJsonArray[i].area_id){
								$allzone.append('<option value='+doorJsonArray[i].area_id+' selected="selected" >'+doorJsonArray[i].area_name+'</option>');
							}else{
								$allzone.append('<option value='+doorJsonArray[i].area_id+'>'+doorJsonArray[i].area_name+'</option>');
							}
						}
					}
//  		  		}
//  			 });
 	     }
    });
    
    //判断是加载 停车位zone还是 门zone
    if($door_spot.val()==<%=OccupyTypeKey.SPOT%>){
	   	for(var yy = 0; yy < spotJsonArray.length; yy ++){
	   		if(zone_id==spotJsonArray[yy].area_id){
	   			$allzone.append('<option value='+spotJsonArray[yy].area_id+' selected="selected" >'+spotJsonArray[yy].area_name+'</option>');
	   		}else{
	   			$allzone.append('<option value='+spotJsonArray[yy].area_id+'>'+spotJsonArray[yy].area_name+'</option>');
	   		}
	   	}
    }else if($door_spot.val()==<%=OccupyTypeKey.DOOR%>){
    	//查询本仓库下zone
// 	    $.ajax({
// 	  		type:'post',
<%-- 	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindAllZoneByPsIdAction.action', --%>
// 	  		dataType:'json',
<%-- 	  		data:'psId='+<%=psId%>, --%>
// 	  		error:function(serverresponse, status){
	  			
// 	  			showMessage(errorMessage(serverresponse.responseText),"alert");
// 	  		},
// 		 	success:function(data){
				if(doorJsonArray && doorJsonArray.length>0){
					for(var i=0;i<doorJsonArray.length;i++){
						if(zone_id==doorJsonArray[i].area_id){
							$allzone.append('<option value='+doorJsonArray[i].area_id+' selected="selected" >'+doorJsonArray[i].area_name+'</option>');
						}else{
							$allzone.append('<option value='+doorJsonArray[i].area_id+'>'+doorJsonArray[i].area_name+'</option>');
						}
					}
				}
// 	  		}
// 		});
	}
    

    
    //绑定删除事件
    $det_button.click(function(){
		
	    var zhi=$(this).attr("zhi");
	    var detail_id=$('#detailid'+zhi).val();
	    
	    if($number_status.val()!=<%=CheckInChildDocumentsStatusTypeKey.UNPROCESS%> && $number_status.val()!=<%=CheckInChildDocumentsStatusTypeKey.PROCESSING%>){
    		
	    	showMessage("Task is closed.","alert");
    		return false;
    	}
	    
    	//删除操作 弹出窗口 提示 是否删除
    	$.artDialog({
		    content: 'Do you want to delete?',
		    icon: 'question',
		    width: 200,
		    height: 70,
		    title:'',
		    okVal: 'Confirm',
		    ok: function () {
		    	
		    	$.ajax({
		    		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowDeleteTaskByIdAction.action',
		    		type: 'post',
		    		dataType: 'json',
		    		timeout: 60000,
		    		cache:false,
		    		data:"detail_id="+detail_id+"&entry_id=<%=id%>",
		    		async:false,
		    		beforeSend:function(request){
		    		},
		    		error:function(serverresponse, status){
		      			
		      			showMessage(errorMessage(serverresponse.responseText),"alert");
		      		},
		    		success: function(data){
		    			
		    			$.artDialog.opener.monitorCheckInWindowChanged();
		    			loadTasks();
		    		}
		    	});
		    },
		    cancelVal: 'Cancel',
		    cancel: function(){
		    	
			}
		});	    	   		 	    	    	
	});

    $allzone.change(function(){                                 //zone 改变事件
    	var flag=1;
        var zoneId=$(this).val();	
        var num=$number.val();
        var p=$allzone.attr('zhi');
       
         if($door_spot.val()==<%=OccupyTypeKey.SPOT%>){
        	 findSpotByZoneId(3,zoneId,p);     //掉 打开停车位的方法
         }else if($door_spot.val()==<%=OccupyTypeKey.DOOR%>){
        	 findDoorsByZoneId(1,zoneId,$door,$allzone,$number,p);   //绑定change事件  调打开门的方法
         }
    });                                            

    $number.focus(function(){
    	 if($(this).val()){
    		 in_serch_val=$(this).val(); 
    	 } 
    });
	
	$number.keyup(function(event){             //输入转换成大写
		
		this.value=this.value.toUpperCase();
		this.value=trim(this.value);
		var id=$(this).attr("id");
		if(event.keyCode!=13){
			$('#number_type'+id).val('');
		}
	});
	
	$containerNo.keyup(function(){      
		//去空格转大写
		this.value=this.value.toUpperCase();
		this.value=trim(this.value);
	});
	
	
	$number.blur(function(event){
		
		//清空现实的状态值
		$number.parents("div[name='av']").find("span[name='number_status_display']").html("");
		
		in_serch_val = $number.val();
		var id=$(this).attr("id");
		
		var ra = $("input[name='ra"+id+"']:checked").val();
		
		if(!ra){
			
			showMessage("Check PickUp Or Delivery.","alert");
			return false;
		}
		
		$(this).val($(this).val().toUpperCase());
		var type='';
		
		if(ra==0){	
			type=1;
		}
		
		if($door!="" && $number.val()!=""){
			if($number_type.val()=='')
			{
				var n_door = $door.val();
				var n_doorId = $door.attr("text");
				var n_door_spot = $door_spot.val();
				var n_allZone =$allzone.val(); 
				
				$.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/getLoadingAction.action',
					data:'search_number='+$number.val()+'&number_type='+type+'&ps_id=<%=psId%>'+'&mainId='+<%=id%>,
					dataType:'json',
					type:'post',
					beforeSend:function(request){
				      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				    },
				    error:function(serverresponse, status){
			  			
				    	$.unblockUI();
			  			showMessage("System error.","alert");
			  		},
					success:function(data){
						
						$.unblockUI();       //遮罩关闭
						
						$("#receipt_no"+id) && $("#receipt_no"+id).val("");
						$("#staging_Customer"+id) && $("#staging_Customer"+id).val("");
						$("#customer_id"+id) && $("#customer_id"+id).val("");
						$("#staging_area_id"+id) && $("#staging_area_id"+id).val("");
						$("#company_id"+id) && $("#company_id"+id).val("");
						$("#freight_term"+id) && $("#freight_term"+id).val("");
						$("#account_id"+id) && $("#account_id"+id).val("");
						$("#supplier_id"+id) && $("#supplier_id"+id).val("");
						$door_spot.trigger("change");
						
						if(data.num==0){
							
							showMessage("Record not found.","alert");
						//	$allzone.val(1);
							
							$door.val(n_door);
							$door.attr("text",n_doorId);
							$door_spot.val(n_door_spot);
							$allzone.val(n_allZone);
							
							$("#appointment_date"+id).val("");
							$("#number_order_status"+id).val("");
							$("#order_system_type"+id).val(data.system_type);
					//		$("#receipt_no").val("");
							var order_type="";
							if(type==1){
								$('#staging'+id).val("");
								$('#staging_zone'+id).val("");
								order_type=<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>;
								$('#title'+id).html("PickUp :");
							}else{
								$('#searchTitle'+id).val("");
								$('#searchZone'+id).val("");
								order_type=<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%>;
								$( '#title' + id ).html( "Delivery :" );
							}
							$number_type.val(order_type);
							
							//计算有效的条数
							effectiveNumber();
						}
						
						if(data.num==1){
							//自动选择门
							$door_spot.val('<%=OccupyTypeKey.DOOR%>');
							if(type==1){
								if(data.door_name!=undefined){
									$door.val(data.door_name);
									$door.attr("text",data.door_id);
									$allzone.val(data.area_id);
								}else{
									$door.val(n_door);
									$door.attr("text",n_doorId);
									$door_spot.val(n_door_spot);
									$allzone.val(n_allZone);
								}
								$number.val(data.number);
								$('#staging'+id).val(data.stagingareaid);
								$('#staging_zone'+id).val(data.area_name);
							}else{
								if(data.door_name!=undefined){
									$door.val(data.door_name);
									$door.attr("text",data.door_id);
									$allzone.val(data.area_id);
								}else{
									$door.val(n_door);
									$door.attr("text",n_doorId);
									$door_spot.val(n_door_spot);
									$allzone.val(n_allZone);
								}
								
								findZoneByTitleName($('#searchTitle'+id),$('#searchZone'+id),data.supplierid);
							}
							if(data.appointmentdate && id==1){
								$("#appointmentdate").val(data.appointmentdate);
								
							}
							//console.log("is_all_picked1607:"+data.is_all_picked);
							if(data.is_all_picked==2){
								//console.log(data.orders);
								var order_number = '';
								//如果是load
								if(data.search_number_type==<%=ModuleKey.CHECK_IN_LOAD%> || data.search_number_type==<%=ModuleKey.CHECK_IN_PONO%>){
									if(data.orders && data.orders.length>0){
										//遍历order
										for(var i=0;i<data.orders.length;i++){
											//得到没有装满的order
											if(data.orders[i].is_all_picked != 'C'){
												order_number += "Order: " + data.orders[i].orderno+"<br/>"; 
											}
										}
										order_number = order_number.substring(0,order_number.length-5);
									}
								}else if(data.search_number_type==<%=ModuleKey.CHECK_IN_ORDER%>){
									order_number = data.number;
								}
								$.artDialog({content:"<b>"+order_number+"</b>",title:"No scanning",lock: true,opacity: 0.3,fixed: true});
							}
							$("#appointment_date"+id).val(data.appointmentdate && data.appointmentdate.substring(0, 19));
							$("#number_order_status"+id).val(data.status);
							$("#order_system_type"+id).val(data.system_type);
						//	$("#receipt_no"+id).val(data.receipt_no);
							$("#receipt_no"+id) && $("#receipt_no"+id).val(data.receiptno);
							$("#staging_Customer"+id) && $("#staging_Customer"+id).val(data.customerid);
							$company_id.val(data.companyid);
							$customer_id.val(data.customerid);
							$("input[id='staging_Customer"+p+"']").val(data.customerid);
							$account_id.val(data.accountid);
							$supplier_id.val(data.supplierid);
							$staging_area_id.val(data.stagingareaid);
							$number_type.val(data.order_type);
							$freight_term.val(data.freightterm);
							var title=""
							if(data.order_type==<%=ModuleKey.CHECK_IN_PONO%>){
								$order_no.val(data.orderno);
								title="PO";
							}
							if(data.order_type==<%=ModuleKey.CHECK_IN_ORDER%>){
								$po_no.val(data.pono);
								title="ORDER";
	
							}
							if(data.order_type==<%=ModuleKey.CHECK_IN_LOAD%>){
								title="LOAD";
							}
	
							
							if(data.order_type==<%=ModuleKey.CHECK_IN_CTN%>){
								title="CTNR";
								$containerNo.val(data.container_no);
							}
							if(data.order_type==<%=ModuleKey.CHECK_IN_BOL%>){
								title="BOL";
								$containerNo.val(data.container_no);
							}
	<%-- 						if(data.order_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%> || data.order_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>) --%>
	// 						{
	// 							title="OTHERS";
	// 							if(data.container_no!='' && data.container_no!='NA'){$containerNo.val(data.container_no);}
	// 						}
							if(data.order_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>){
								title="PickUp";
							}
							if(data.order_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%>){
								title="Delivery";
							}
							$('#title'+id).html(title+":");
							//计算有效的条数
							effectiveNumber();
						}
						
						if(data.num * 1 == -1){
							//自动选择门
							$door_spot.val('<%=OccupyTypeKey.DOOR%>');
							var order_type = data.pickup&&data.pickup.infos[0].order_type;
							var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/task_number_wms_select_data.html?number='+$number.val()+'&id='+id+'&datas='+escape(jQuery.fn.toJSON(data.pickup))+'&system_type='+data.system_type+'&type='+type+'&number_type='+order_type;
							$.artDialog.open(uri , {id:"__test1xx",title: "Select",width:'1100px',height:'500px', lock: true,opacity: 0.3,fixed: true});//, datas:{aa:1, bb:3}
						}
	
						if(data.flag==1){
							//自动选择门
							$door_spot.val('<%=OccupyTypeKey.DOOR%>');
							if(ra==0){
								title="LOAD/PO/ORDER";
							}else{
								title="CTNR/BOL";
							}
							$('#title'+id).html(title+":");
							showMessage("This is duplicate record.","alert");
							$number.val("");
						}
						findFirstContainerNoHasValue();
					}
				});
			}
		}else{
			if(type==1){
				showMessage("Enter LOAD#/PO#/ORDER#.","alert");
			}else{
				showMessage("Enter CTNR#/BOL#.","alert");
			}	
		}
		 return false;
	})
	
	$number.keypress(function(event){
		
		if(event.keyCode==13){
			
			//清空现实的状态值
			$number.parents("div[name='av']").find("span[name='number_status_display']").html("");
			
			in_serch_val = $number.val();
			var id=$(this).attr("id");
			
			var ra = $("input[name='ra"+id+"']:checked").val();
			
			if(!ra){
				
				showMessage("Check PickUp Or Delivery.","alert");
				return false;
			}
			
			$(this).val($(this).val().toUpperCase());
			var type='';
			
			if(ra==0){	
				type=1;
			}
			
			if($door!="" && $number.val()!=""){
				var n_door = $door.val();
				var n_doorId = $door.attr("text");
				var n_door_spot = $door_spot.val();
				var n_allZone = $allzone.val();
				$.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/getLoadingAction.action',
					data:'search_number='+$number.val()+'&number_type='+type+'&ps_id=<%=psId%>'+'&mainId='+<%=id%>,
					dataType:'json',
					type:'post',
					beforeSend:function(request){
				      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				    },
				    error:function(serverresponse, status){
			  			
				    	$.unblockUI();
			  			showMessage("System error.","alert");
			  		},
					success:function(data){
						
						$.unblockUI();       //遮罩关闭
						
						$("#receipt_no"+id) && $("#receipt_no"+id).val("");
						$("#staging_Customer"+id) && $("#staging_Customer"+id).val("");
						$("#customer_id"+id) && $("#customer_id"+id).val("");
						$("#staging_area_id"+id) && $("#staging_area_id"+id).val("");
						$("#company_id"+id) && $("#company_id"+id).val("");
						$("#freight_term"+id) && $("#freight_term"+id).val("");
						$("#account_id"+id) && $("#account_id"+id).val("");
						$("#supplier_id"+id) && $("#supplier_id"+id).val("");
						$door_spot.trigger("change");
						
						if(data.num==0){
							
							showMessage("Record not found.","alert");
						//	$allzone.val(1);
							
							$door.val(n_door);
							$door.attr("text",n_doorId); 
							$door_spot.val(n_door_spot);
							$allzone.val(n_allZone);
							
							$("#appointment_date"+id).val("");
							$("#number_order_status"+id).val("");
							$("#order_system_type"+id).val(data.system_type);
							$("#receipt_no"+id).val("");
							
							var order_type="";
							if(type==1){
								$('#staging'+id).val("");
								$('#staging_zone'+id).val("");
								order_type=<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>;
								$('#title'+id).html("PickUp :");
							}else{
								$('#searchTitle'+id).val("");
								$('#searchZone'+id).val("");
								order_type=<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%>;
								$('#title'+id).html("Delivery :");
							}
							$number_type.val(order_type);
							//计算有效的条数
							effectiveNumber();
						}
						
						if(data.num==1){
							//自动选择门
						//	console.log("is_all_picked1780:"+data.is_all_picked);
							if(data.is_all_picked==2){
								//console.log(data.orders);
								var order_number = '';
								//如果是load
								if(data.search_number_type==<%=ModuleKey.CHECK_IN_LOAD%> || data.search_number_type==<%=ModuleKey.CHECK_IN_PONO%>){
									if(data.orders && data.orders.length>0){
										//遍历order
										for(var i=0;i<data.orders.length;i++){
											//得到没有装满的order
											if(data.orders[i].is_all_picked != 'C'){
												order_number += "Order: " + data.orders[i].orderno+"<br/>"; 
											}
										}
										order_number = order_number.substring(0,order_number.length-5);
									}
								}else if(data.search_number_type==<%=ModuleKey.CHECK_IN_ORDER%>){
									order_number = data.number;
								}
								$.artDialog({content:"<b>"+order_number+"</b>",title:"No scanning",lock: true,opacity: 0.3,fixed: true});
							}
							
							$door_spot.val('<%=OccupyTypeKey.DOOR%>');
							if(type==1){
								if(data.door_name!=undefined){
									$door.val(data.door_name);
									$door.attr("text",data.door_id);
									$allzone.val(data.area_id);
								}else{
									$door.val(n_door);
									$door.attr("text",n_doorId);
									$door_spot.val(n_door_spot);
									$allzone.val(n_allZone);
								}
								$number.val(data.number);
								$('#staging'+id).val(data.stagingareaid);
								$('#staging_zone'+id).val(data.area_name);
							}else{
								if(data.door_name!=undefined){
									$door.val(data.door_name);
									$door.attr("text",data.door_id);
									$allzone.val(data.area_id);
								}else{
									$door.val(n_door);
									$door.attr("text",n_doorId);
									$door_spot.val(n_door_spot);
									$allzone.val(n_allZone);
								}
								
								findZoneByTitleName($('#searchTitle'+id),$('#searchZone'+id),data.supplierid);
							}
							if(data.appointmentdate && id==1){
								$("#appointmentdate").val(data.appointmentdate);
								
							}
							
							$("#appointment_date"+id).val(data.appointmentdate && data.appointmentdate.substring(0, 19));
							$("#number_order_status"+id).val(data.status);
							$("#order_system_type"+id).val(data.system_type);
						//	$("#receipt_no"+id).val(data.receipt_no);
							$("#receipt_no"+id) && $("#receipt_no"+id).val(data.receiptno);
							$("#staging_Customer"+id) && $("#staging_Customer"+id).val(data.customerid);
							$company_id.val(data.companyid);
							$customer_id.val(data.customerid);
							$("input[id='staging_Customer"+p+"']").val(data.customerid);
							$account_id.val(data.accountid);
							$supplier_id.val(data.supplierid);
							$staging_area_id.val(data.stagingareaid);
							$number_type.val(data.order_type);
							$freight_term.val(data.freightterm);
							var title=""
							if(data.order_type==<%=ModuleKey.CHECK_IN_PONO%>){
								$order_no.val(data.orderno);
								title="PO";
							}
							if(data.order_type==<%=ModuleKey.CHECK_IN_ORDER%>){
								$po_no.val(data.pono);
								title="ORDER";

							}
							if(data.order_type==<%=ModuleKey.CHECK_IN_LOAD%>){
								title="LOAD";
							}

							
							if(data.order_type==<%=ModuleKey.CHECK_IN_CTN%>){
								title="CTNR";
								$containerNo.val(data.container_no);
							}
							if(data.order_type==<%=ModuleKey.CHECK_IN_BOL%>){
								title="BOL";
								$containerNo.val(data.container_no);
							}
<%-- 							if(data.order_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%> || data.order_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>) --%>
// 							{
// 								title="OTHERS";
// 								if(data.container_no!='' && data.container_no!='NA'){$containerNo.val(data.container_no);}
// 							}
							if(data.order_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>){
								title="PickUp";
							}
							if(data.order_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%>){
								title="Delivery";
							}
							$('#title'+id).html(title+":");
							//计算有效的条数
							effectiveNumber();
						}
						
						if(data.num * 1 == -1){
							//自动选择门
							$door_spot.val('<%=OccupyTypeKey.DOOR%>');
							var order_type = data.pickup&&data.pickup.infos[0].order_type;
							var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/task_number_wms_select_data.html?number='+$number.val()+'&id='+id+'&datas='+escape(jQuery.fn.toJSON(data.pickup))+'&system_type='+data.system_type+'&type='+type+'&number_type='+order_type;
							$.artDialog.open(uri , {id:"__test1xx",title: "Select",width:'1100px',height:'500px', lock: true,opacity: 0.3,fixed: true});//, datas:{aa:1, bb:3}
						}

						if(data.flag==1){
							//自动选择门
							$door_spot.val('<%=OccupyTypeKey.DOOR%>');
							if(ra==0){
								title="LOAD/PO/ORDER";
							}else{
								title="CTNR/BOL";
							}
							$('#title'+id).html(title+":");
							showMessage("This is duplicate record.","alert");
							$number.val("");
						}
						findFirstContainerNoHasValue();
					}
				});
				
			}else{
				if(type==1){
					showMessage("Enter LOAD#/PO#/ORDER#.","alert");
				}else{
					showMessage("Enter CTNR#/BOL#.","alert");
				}	
			}
			 return false;
		}
	});

    $ra.click(function(){
		if(this.value==0){
			$("span[name='title']",$container).html('LOAD/PO/ORDER:');
			$("#stagingTitle",$container).html('Staging:&nbsp;');
			$("#stagingVal",$container).html('<input style="width:180px;height:24px;" id="staging'+p+'" disabled="disabled" value="'+staging_area_id+'">');
			$("#zoneTitle",$container).html('Zone:&nbsp;');
			$("#zoneVal",$container).html('<input style="width:180px;height:24px;" id="staging_zone'+p+'" disabled="disabled">');		  
			$("#customerTitle",$container).html('Customer:');
			$("#customerVal",$container).html('<input style="width:180px;height:24px;" id="staging_Customer'+p+'" name="staging_Customer" disabled="disabled">');
		}else{
			var html='CTNR/BOL:';
			$("span[name='title']",$container).html(html);
			$("#stagingTitle",$container).html('Title:&nbsp;');
			$("#stagingVal",$container).html('<input style="width:180px;height:24px;" id="searchTitle'+p+'" disabled="disabled">');
			$("#zoneTitle",$container).html('Zone:&nbsp;');
			$("#zoneVal",$container).html('<input style="width:180px;height:24px;" id="searchZone'+p+'" disabled="disabled">');			  
			$("#customerTitle",$container).html('Receipt No:');
			$("#customerVal",$container).html('<input style="width:180px;height:24px;" id="receipt_no'+p+'" name="receipt_no" disabled="disabled">');
		}
		$number_type.val("");
    });
	
	//绑定通知文本框
	$tongzhi.click(function(){    
		 var id=this.id;
		 var ids=$('#'+id+'Id').val();
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/check_in/check_in_admin_user.html"; 
		 
		 var option = {
			single_check:0, 					// 1表示的 单选
			user_ids:ids, //需要回显的UserId
			not_check_user:"",					//某些人不 会被选中的
			proJsId:"15",						// -1全 部,1普通员工,5副主管,10主管,15主管和副主管
			ps_id:'<%=adminLoggerBean.getPs_id()%>',	
			id:id,					//所属仓库
			handle_method:'setSelectAdminUsers',
			input_id:id,
			group :groupJson.Warehouse	
		 };
		 uri  = uri+"?"+jQuery.param(option);
		 $.artDialog.open(uri , {title: 'User List',width:'700px',height:'85%', lock: true,opacity: 0.3,fixed: true});
	});
	
	$tongzhi.keyup(function(){ 
		$tongzhi.click();
	});
   
    //绑定门和位置
    $door.click(function(){
	
    	var id=$(this).attr("in");
	    var ra=$("input[name='ra"+id+"']:checked",$container).val();
	    	
    	var $num=$("input[name='number']",$container); 
    	if($num.val()=="" && ra==0 ){
    		
			showMessage("Enter LOAD#/PO#/Order#.","alert");
			return false;
    	}
    	if($num.val()=="" && ra==1 ){
    		
    		showMessage("Enter CTNR#/BOL#.","alert");
			return false;
    	}
    	var numVal=$num.val();
    	var numName="";
    	if(ra==0){	
    		numName="LOAD/PO/ORDER";
		}else{	
			numName="CTNR/BOL";	
	    }
    	var inputId=this.id;
    	var mainId=<%=id%>;
    	var zoneId=$allzone.val();
    	var doorId=$(this).attr("text");
    	
    	var door_spot=$door_spot.val();
    
    	if(door_spot==<%=OccupyTypeKey.SPOT%>){
    		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/get_yard_no.html?storageId=<%=psId%>'
    				+'&yc_id='+doorId+'&spotZoneId='+zoneId+'&inputId='+inputId+'&mainId=<%=id %>'; 
    		$.artDialog.open(uri , {title: "Spot",width:'600px',height:'520px', lock: true,opacity: 0.3,fixed: true});
    	}else{
	    	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/door_list.html?zoneId='+zoneId+'&inputValue='+doorId+'&inputId='+inputId+'&associate_id=<%=id %>&mainId=<%=id%>'; 
	    	$.artDialog.open(uri , {title: "Door and Location",width:'700px',height:'470px', lock: true,opacity: 0.3,fixed: true});
    	}  
	});
    
	//查询按钮
    $("#"+p).searchInput({
    	
  		"click":function(){
  			
			if($(this).prev().prop("disabled")){
				
				return false;
				
			}else{
				//清空状态值
				$(this).parents("div[name='av']").find("span[name='number_status_display']").html("")
				
				var id = $(this).prev().attr("id");
				
				var ra=$("input[name='ra"+id+"']:checked").val();
				
				if(!ra){
					
					showMessage("Check PickUp Or Delivery.","alert");
					return false;
				}
				
				var type='';
				
// 				console.log(ra);
				if(ra==0){	
					type=1;
				}
				
				if($door!="" && $number.val()!=""){
					var n_door = $door.val();
					var n_doorId = $door.attr("text");
					var n_door_spot = $door_spot.val();
					var n_allZone = $allzone.val();
					//查看有哪些没有扫sn 没有扫的显示出来
					$.ajax({
						url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/getLoadingAction.action',
						data:'search_number='+$number.val()+'&number_type='+type+'&ps_id=<%=psId%>'+'&mainId='+<%=id%>,
						dataType:'json',
						type:'post',
						beforeSend:function(request){
							$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
						},
						error:function(serverresponse, status){
				  			
							$.unblockUI();
				  			showMessage("System error.","alert");
				  		},
						success:function(data){
					//		console.log(data.receiptno);
							//遮罩关闭
							$.unblockUI();  
							$("#receipt_no"+id) && $("#receipt_no"+id).val("");
							$("#staging_Customer"+id) && $("#staging_Customer"+id).val("");
							$("#customer_id"+id) && $("#customer_id"+id).val("");
							$("#staging_area_id"+id) && $("#staging_area_id"+id).val("");
							$("#company_id"+id) && $("#company_id"+id).val("");
							$("#freight_term"+id) && $("#freight_term"+id).val("");
							$("#account_id"+id) && $("#account_id"+id).val("");
							$("#supplier_id"+id) && $("#supplier_id"+id).val("");
							$door_spot.trigger("change");
							
							if(data.num==0){

								showMessage("Record not found.","alert");
						//		$allzone.val(1); 
								$door.val(n_door);
								$door.attr("text",n_doorId);
								$door_spot.val(n_door_spot);
								$allzone.val(n_allZone);
								
								$("#appointment_date"+id).val("");
								$("#number_order_status"+id).val("");
								$("#order_system_type"+id).val(data.system_type);
								$("#receipt_no").val("");
								var order_type="";
								if(type==1){
									$('#staging'+id).val("");
									$('#staging_zone'+id).val("");
									order_type=<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>;
									$('#title'+id).html("PickUp :");
								}else{
									$('#searchTitle'+id).val("");
									$('#searchZone'+id).val("");
									order_type=<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%>;
									$('#title'+id).html("Delivery :");
								}
								$number_type.val(order_type);
								
								
								//计算有效的条数
								effectiveNumber();
							}
							if(data.num==1){
								//自动选择门
								$door_spot.val('<%=OccupyTypeKey.DOOR%>');
								if(data.is_all_picked==2){
									//console.log(data.orders);
									var order_number = '';
									//如果是load
									if(data.search_number_type==<%=ModuleKey.CHECK_IN_LOAD%> || data.search_number_type==<%=ModuleKey.CHECK_IN_PONO%>){
										if(data.orders && data.orders.length>0){
											//遍历order
											for(var i=0;i<data.orders.length;i++){
												//得到没有装满的order
												if(data.orders[i].is_all_picked != 'C'){
													order_number += "Order: " + data.orders[i].orderno+"<br/>"; 
												}
											}
											order_number = order_number.substring(0,order_number.length-5);
										}
									}else if(data.search_number_type==<%=ModuleKey.CHECK_IN_ORDER%>){
										order_number = data.number;
									}
									$.artDialog({content:"<b>"+order_number+"</b>",title:"No scanning",lock: true,opacity: 0.3,fixed: true});
								}
								if(data.door_name!=undefined){
									$door.val(data.door_name);
									$door.attr("text",data.door_id);
									$allzone.val(data.area_id);
								}else{
									$door.val(n_door);
									$door.attr("text",n_doorId);
									$door_spot.val(n_door_spot);
									$allzone.val(n_allZone);
								}
								
								if(type==1){
									
									$('#staging'+id).val(data.stagingareaid);
									$('#staging_zone'+id).val(data.area_name);
									$number.val(data.number);
								}else{
									
									findZoneByTitleName($('#searchTitle'+id),$('#searchZone'+id),data.supplierid);
								}
								
								if(data.appointmentdate && id==1){
									
									$("#appointmentdate").val(data.appointmentdate);
									
								}
								
								$("#appointment_date"+id).val(data.appointmentdate && data.appointmentdate.substring(0, 19));
								$("#number_order_status"+id).val(data.status);
								$("#order_system_type"+id).val(data.system_type);
						//		$("#receipt_no"+id).val(data.receipt_no);
								$("#receipt_no"+id) && $("#receipt_no"+id).val(data.receiptno);
								$("#staging_Customer"+id) && $("#staging_Customer"+id).val(data.customerid);
								
								$company_id.val(data.companyid);
								$customer_id.val(data.customerid);
								$("input[id='staging_Customer"+p+"']").val(data.customerid);
								$account_id.val(data.accountid);
								$supplier_id.val(data.supplierid);
								$staging_area_id.val(data.stagingareaid);
								$number_type.val(data.order_type);
								$freight_term.val(data.freightterm);
								
								var title=""
								if(data.order_type==<%=ModuleKey.CHECK_IN_PONO%>){
									$order_no.val(data.orderno);
									title="PO";
								}
								if(data.order_type==<%=ModuleKey.CHECK_IN_ORDER%>){
									$po_no.val(data.pono);
									title="ORDER";

								}
								if(data.order_type==<%=ModuleKey.CHECK_IN_LOAD%>){
									title="LOAD";
								}
								
								if(data.order_type==<%=ModuleKey.CHECK_IN_CTN%>){
									title="CTNR";
								}
								if(data.order_type==<%=ModuleKey.CHECK_IN_BOL%>){
									title="BOL";
								}
								if(data.order_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>){
									title="PickUp";
								}
								if(data.order_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%>){
									title="Delivery";
								}
								$('#title'+id).html(title+":");
								//计算有效的条数
								effectiveNumber();
							}
							
							if(data.num * 1 == -1){
								//自动选择门
								$door_spot.val('<%=OccupyTypeKey.DOOR%>');
							//	console.log(data.pickup&&data.pickup.infos[0].order_type);
								var order_type = data.pickup&&data.pickup.infos[0].order_type;
								var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/task_number_wms_select_data.html?number='+$number.val()+'&id='+id+'&datas='+escape(jQuery.fn.toJSON(data.pickup))+'&system_type='+data.system_type+'&type='+type+'&number_type='+order_type; 
								$.artDialog.open(uri , {title: "Select",width:'1100px',height:'500px', lock: true,opacity: 0.3,fixed: true});
							}
							
							if(data.flag==1 && <%=id%> != data.mainid){
								//自动选择门
								$door_spot.val('<%=OccupyTypeKey.DOOR%>');
								if(ra==0){
									title="LOAD/PO/ORDER";
								}else{
									title="CTNR/BOL";
								}
								$('#title'+id).html(title+":");
								showMessage("This is duplicate record.","alert");
								$number.val("");
							}
							
							findFirstContainerNoHasValue();
						}
					});
					
				}else{
					if(type==1){
						showMessage("Enter LOAD#/PO#/ORDER#.","alert");
					}else{
						showMessage("Enter CTNR#/BOL#.","alert");
					}	
				}
				return false;
			}
		}
 	});
}

//自动选择门
function autoChoiceDoor(doorName,flag,result){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindReadyDoorSeachAction.action',
		data:'doorName='+doorName+'&ps_id='+<%=psId%>,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    
		},
		success:function(data){
			if(data.length==0){
				if(flag==0){
					flag=1;
					autoChoiceDoor(Number(doorName)+1,flag,result);
				}else if(flag==1){
					flag=2;
					autoChoiceDoor(Number(doorName)-2,flag,result);
				}
			}else{
				$allzone.val(data[0].area_id);
				$door.val(data[0].doorid);
			}
		},
		error:function(serverresponse, status){
  			
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		}
	 });
}

function setParentUserShow(ids,names,inputId){ //通知回显
	
	
	$("#"+inputId).val(names);
	$("#"+inputId+"Id").val(ids);			
}


function getDialog(in_seal, out_seal) {
	// 点击取消事件后焦点所在位置
	var focusLocation = 0; 
	if((out_seal == '' || out_seal == 'undefined') && (in_seal == '' || in_seal == 'undefined') ) {
		in_seal_click();
		out_seal_click();
		focusLocation = 1;
	} else if(in_seal == '' || in_seal == 'undefined' ) {
		focusLocation = 2;
		in_seal_click();
	} else if(out_seal == '' || out_seal == 'undefined') {
		focusLocation = 3;
		out_seal_click();
	}
	
	var in_seal = $('#in_seal').val();
	var out_seal = $('#out_seal').val();
	$.artDialog({
		title : 'Message',
		width: '300px',
		height: '30%',
	    lock: true,
	    opacity: 0.3,
	    fixed : true,
		content : "<div style='font-size:16px'>Delivery Seal: <strong>" + in_seal + "</strong></div>"
				+ "<div style='font-size:16px'>PickUp: <strong>" + out_seal + "</strong></div>",
		okVal: 'Yes',
	    ok: function () {
	    },
	    cancelVal: 'Cancel',
	    cancel: function(){
			if(focusLocation == 1 ) {
				$('#in_seal').val('');
				$('#out_seal').val('');
				$('#in_seal').focus();
			} else if(focusLocation == 2) {
				$('#in_seal').val('');
				$('#in_seal').focus();
			} else if(focusLocation == 3) {
				$('#out_seal').val('');
				$('#out_seal').focus();
			}
		}
	});
}

function in_seal_click(){
	$('#in_seal').val('NA');
}

function out_seal_click(){
	$('#out_seal').val('NA');
}

function selectData(){       
	
	//提交的时候选择数据
	var $num=$("input[name='number']"); 
    var number='';
	for(var i=0;i<$num.length;i++){
		if(i==$num.length-1){
			number+=$($num[i]).val();
		}else{
			number+=$($num[i]).val()+",";
		}
		var id=$($num[i]).attr("id");
		var doorId=$('#door'+id).attr("text");
		var tongzhiId=$('#tongzhi'+id+'Id').val();
		if(doorId==undefined || doorId==""){
			showMessage("Choose door.","alert");
			return false;
		}
		if(tongzhiId==""){
			showMessage("Choose Supervisor.","alert");
			
			return false;
		}	
	}
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_submit_select_data.html?number='+number;
	$.artDialog.open(uri , {title: "Select Data",width:'700px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}

function errorMessage(val){

	var error1 = val.split("[");
	var error2 = error1[1].split("]");	
	
	return error2[0];
}

//文件上传
function uploadFile(_target){

    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: 'Upload Photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   	}});
}

//在线获取
function onlineScanner(target){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target="+target; 
	$.artDialog.open(uri , {title: 'Online Mutiple',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}

//jquery file up 回调函数
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
	if($.trim(fileNames).length > 0 ){
		$("input[name='file_names']").val(fileNames); 
		var array = fileNames.split(",");
		for(var index = 0 ; index < array.length ; index++ ){
			if(array[index].length>0){
				var a =  createA(array[index]) ;
				$("#add_file").append(a); 
			}
		}
	} 
}

function createA(fileName){
    var id = fileName.substring(0,fileName.length-4);
    var  a = "<tr id='add"+id+"'><td ><p style='color:#439B89' class='new' ><a href='javascript:void(0)' onclick='showTempPictrueOnline(&quot;"+fileName+"&quot;)'  style='color:#439B89' >"+fileName+"</p></td><td><p style='color:#439B89' class='new' >&nbsp;&nbsp;&nbsp;<a  href='javascript:deleteA(&quot;"+id+"&quot;)' style='color:red' >×</p></td></tr>";
    return a ;
}

function deleteA(id){

	$("#add"+id+" *").remove();
	var file_names = $("input[name='file_names']").val(); 
	
	var array = file_names.split(",");
	var lastFile = "";
	for(var index = 0 ; index < array.length ; index++ ){
		if(id==array[index].substring(0,array[index].length-4)){
			
			array[index]="";
				
		}
		lastFile += array[index];
		if(index!=array.length-1 && array[index]!=""){
			lastFile+=",";
		}
	}
	 
	$("input[name='file_names']").val(lastFile);
}

function closeWindow(){
	$.artDialog.close();
}

//图片在线显示  		 
function showPictrueOnline(fileWithType,fileWithId ,currentName){
    var obj = {
  		file_with_type:fileWithType,
  		file_with_id : fileWithId,
  		current_name : currentName ,
  		cmd:"multiFile",
  		table:'file',
  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/check_in"
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
}

//删除图片
function deleteFile(file_id){
	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:'file',file_id:file_id,folder:'check_in',pk:'file_id'},
		success:function(data){
			if(data && data.flag === "success"){
				$("#file_tr_"+file_id).remove();
			}else{
				showMessage("System error.","error");
			}
			
		},
		error:function(serverresponse, status){
  			
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		}
	});
}

$("#relType").change(function(){
	if($(this).val() == '<%=CheckInMainDocumentsRelTypeKey.CTNR%>')
	{
		$('#live').css("display","none");
		$('#liveType').css("display","none");
	}
	else
	{
		$('#liveType').css("display","inline");
	}
});

//编辑主表类型按钮
function editRelType(){

	$('#live').css("display","none");
	$('#liveType').css("display","inline");
	$('#relType').css("display","inline");     //下拉
	$('#entryType').css("display","none");     //状态
	$('#btnEdit').css("display","none");       //编辑按钮
	$('#btnSave').css("display","inline");    //save按钮
	$('#btnCancel').css("display","inline");    //save按钮
}

function cancelRelType(){

	$('#entryType').css("display","inline");     //状态
	$('#relType').css("display","none");     //状态
	$('#live').css("display","inline");     //状态
	$('#liveType').css("display","none");     //状态
	$('#btnEdit').css("display","inline");       //编辑按钮
	$('#btnSave').css("display","none");    //save按钮
	$('#btnCancel').css("display","none");    //save按钮
}

   //更新主表类型
   //TODO 处理pickUp CTNR
function saveRelType(){

	var dlo_id=<%=id%>;
	var relVal=$('#relType').val();	
	var liveVal=$('#liveType').val();
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxUpdateMainRelTypeAction.action',
  		dataType:'json',
  		data:'dlo_id='+dlo_id+'&rel_val='+relVal+'&live_val='+liveVal,
		error:function(serverresponse, status){
  			
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		},
	 	success:function(data){
  			if(data.rel_type==1){
 					$('#entryType').html('DELIVERY');
  			}else if(data.rel_type==2){
  				$('#entryType').html('Pick Up');
  			}else if(data.rel_type==3){
  				$('#entryType').html('BOTH');
  			}else{
  				$('#entryType').html('NONE');
 				}
  			
  			if(data.islive==1){
  				$('#live').html('Live Load');
  			}else if(data.islive==2){
  				$('#live').html('Drop Off');
  			}else if(data.islive==3){
  				$('#live').html('Swap CTNR');
  			}else if(data.islive==4){
  				$('#live').html('PickUp CTNR');
  			}
  			$('#live').css("display","inline");
  		    $('#liveType').css("display","none");
  			
  			$('#btnSave').css("display","none");    //save按钮
			$('#btnEdit').css("display","inline");       //编辑按钮	
			$('#btnCancel').css("display","none");    //save按钮
			$('#relType').css("display","none");     //下拉	
			$('#entryType').css("display","inline");     //状态
			$('#is_live_now').val(liveVal);
			$('#rel_type_now').val(relVal);
  		} 
    });
}

function showTempPictrueOnline(_fileName){

 	var obj = {
		current_name:_fileName ,
	   	table:"temp",
	   	fileNames:$("input[name='file_names']").val(),
	   	base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upl_imags_tmp"
	}
 	if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
		openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
}

function onlineSingleScanner(_target){

    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_single_scanner.html?target="+_target; 
	$.artDialog.open(uri , {title: 'Online Single',width:'300px',height:'170px', lock: true,opacity: 0.3,fixed: true});
}

function findDoorsByZoneId(flag,zoneId,$door,$allzone,$number,p){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindDoorsByZoneIdAction.action',
		data:'zoneId='+zoneId+'&mainId=<%=id%>&ps_id=<%=psId%>&flag='+flag,
		dataType:'json',
		type:'post',
		error:function(serverresponse, status){
  			
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		},
		success:function(data){
			if(data.length>0){
				$door.val(data[0].doorid);
				$door.attr("text",data[0].sd_id);
				$allzone.val(zoneId);
  			}else if(data.length==0 && flag==1){
  				findDoorsByZoneId(2,zoneId,$door,$allzone,$number,p);
  			}else if(data.length==0 && flag==2){
  				$door.val("");
  				$door.attr("text","");
  				$allzone.val("");
  				//openDoor();
  				var id=$door.attr("in");
  			    var ra=$("input[name='ra"+id+"']:checked").val();
  			 
  		    	var numVal=$number.val();
  		    	var numName="";
  		    	if(ra==0){	
  		    		numName="LOAD";
  				}else{
  					var choice= $('#choice'+p).val();     //选择值
  					if(choice==0){			
  						numName="CTN";
  					}else if(choice==1){
  						numName="OTHER";
  					}else{
  						numName="BOL";
  					}		
  			    }
  		    	var inputId=$door.attr('id');
  		    	
  		    	var mainId=<%=id%>;
  		    	var inputValue=$door.attr("text");
  		    	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/door_list.html?mainId='+mainId+'&numVal='+numVal+'&inputId='+inputId+'&inputValue='+inputValue+'&numName='+numName;
  				$.artDialog.open(uri , {title: "Door and Location",width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
  			}
		}
	});
}

function addStyleForText(text){
	
	return '<span style="font-size: 10;font-family:Verdana;">'+text+'</span>';
}

function trim(str){
    return str.replace(/[ ]/g,"");  //去除字符算中的空格
}

function findZoneByTitleName(searchTitle,searchZone,title_name){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindZoneByTitleNameAction.action',
		data:'title_name='+title_name+'&ps_id='+<%=psId%>,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    
		},
		error:function(serverresponse, status){
  			
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		},
		success:function(data){
			var zone="";
			for(var i=0;i<data.length;i++){
				//alert(data[i].area_name);
				if(i==0){
					zone=data[i].area_name+",";
				}
				
				if(i>0 && data[i].area_name!=data[i-1].area_name){
					zone +=data[i].area_name+",";
				}
			
			}
			zone=zone.substr(0,zone.length-1);
			$(searchTitle).val(title_name);
			$(searchZone).val(zone);
		}
	 });
}

//sbb
function openLoadTemplate(id,num){
	
	var number=$('#'+id).val();
	var number_type=$('#number_type'+id).val();
	var companyId=$('#company_id'+id).val();
	var customerId=$('#customer_id'+id).val();
	var order=$('#order_no'+id).val();
	var door=$('#door'+id).val();
	var seal_pick_up = $('#seal_pick_up'+id).val();
	var receipt_no = $("#receipt_no"+id).val();
	//货柜号
	var containerNo = $("#containerNo"+id).find("option:selected").text();
	//预约时间
	var appointmentDate =  $("#appointment_date"+id).val();
// 	console.log("containerNo="+containerNo+"   appointmentDate="+appointmentDate);
	if(seal_pick_up == undefined){
		
		seal_pick_up = "";
	}
	
	var resources_type = $('#door_spot'+id).val();
	
	var entry_id=<%=id%>;
	var type='';
	if(number_type==""){
		showMessage("Search No#.","alert");
		return false;
	}
	if(number_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%> || number_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>){
		showMessage("Record no found.","alert");
		return false;
	}
	if(num==0){
		type='pickup';
	}else{
		type='delivery';
	}
	
	if('' != number)
	{
		if(type=='pickup')
		{
			findLoadsCountByLoadNo(number,entry_id,door,type,companyId,customerId,number_type,order,seal_pick_up,resources_type,containerNo,appointmentDate);
		}
		else if(type=='delivery')
		{
			findReceiptsCountByBolOrCtnNo(number,entry_id,door,type,companyId,customerId,number_type,resources_type,receipt_no);
		}
	}else{
		
		if(type == 'pickup'){
			showMessage("Enter LOAD#/PO#/Order#.","alert");
		}else if(type == 'delivery'){
			showMessage("Enter BOL#/CTNR#","alert");
		}
	} 
}

function setParentCompanyLoadCustomer(CompanyID, loadNo, CustomerID,entry_id,door,type,number_type){
	
	var print=[];
	var json = {
			checkDataType:type.toUpperCase(),
			number:loadNo,
			door_name:door,
			companyId:CompanyID,
			customerId:CustomerID,
			number_type:number_type,
			checkLen:1
	};
	print.push(json);
	var jsonString = JSON.stringify(print);

	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?jsonString='+jsonString+'&number='+loadNo
			+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&CompanyID='+CompanyID+'&CustomerID='+CustomerID+'&out_seal='+$("#out_seal").val();
	$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
}
function setParentCompanyReceiptCustomer(CompanyID, number, CustomerID,entry_id,door,type, bol, ctnr){
	
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?number='+number
	+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&CompanyID='+CompanyID+'&CustomerID='+CustomerID+'&out_seal='+$("#out_seal").val()+'&bol='+bol+'&ctnr='+ctnr;
	$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
}

function findLoadsCountByLoadNo(number,entry_id,door,type,companyId,customerId,number_type,order,seal_pick_up,resources_type,containerNo,appointmentDate){
	var print=[];
	var json = {
		checkDataType:type.toUpperCase(),
		number:number,
		door_name:door,
		companyId:companyId,
		customerId:customerId,
		number_type:number_type,
		order:order,
		checkLen:1
	};
	print.push(json);
	var jsonString = JSON.stringify(print);
// 	console.log("++++++containerNo="+containerNo+"   appointmentDate="+appointmentDate);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?jsonString='+jsonString+'&number='+number+'&number_type='+number_type+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&order_no='+order+'&out_seal='+seal_pick_up+'&CompanyID='+companyId+'&CustomerID='+customerId+'&resources_type='+resources_type+'&containerNo='+containerNo+'&appointmentDate='+appointmentDate;
	$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true,type:type});
}

function findReceiptsCountByBolOrCtnNo(number,entry_id,door,type,companyId,customerId,number_type,resources_type,receipt_no){	
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?number='+number+'&entry_id='+entry_id+'&door_name='+door+'&number_type='+number_type+'&type='+type+'&CompanyID='+companyId+'&CustomerID='+customerId+'&resources_type='+resources_type+'&receipt_no='+receipt_no;
	$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true,type:type});
			
}
var jsonsPrint = [];

//批量打印
function batchPrint(){

	jsonsPrint = [];
	var entry_id=<%=id%>;
	var out_seal=$("#out_seal").val();
	var numbers = $("input[name='number']", $('#Container'));
	var isPrint=true;
	
	for(var i = 0; i < numbers.length; i ++){
	
		var id = $(numbers[i]).attr("id");//p
		var number = $(numbers[i]).attr("value");//load ctnr bol
		var number_type=$('#number_type'+id).val();
		var companyId = $("#company_id"+id).val();
		var customerId = $("#customer_id"+id).val();
		var door_name=$('#door'+id).val();
		var order=$("#order_no"+id).val();
		var receipt_no = $("#receipt_no"+id).val();
		console.log("receipts:"+receipt_no);
		if(number_type==''){
			isPrint=false;
		}

		var checkDataType='';
		if(number_type==<%=ModuleKey.CHECK_IN_CTN%> || number_type==<%=ModuleKey.CHECK_IN_BOL%> ){
			checkDataType='delivery';
		}else{
			checkDataType='pickup';
		}
		var json = {
				checkDataType:checkDataType,
				number:number,
				door_name:door_name,
				companyId:companyId,
				customerId:customerId,
				checkLen:1,
				order:order,
				number_type:number_type,
				receipt_no:receipt_no
		};
		jsonsPrint.push(json);
	}
	
	var jsonString = JSON.stringify(jsonsPrint);
	var para = 'entry_id='+entry_id+'&out_seal='+$("#out_seal").val()+"&jsonString="+jsonString;
	if(isPrint){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list_batch.html?'+para;
		$.artDialog.open(uri , {title: "Batch Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
	}else{
		showMessage("Search No#.","alert");
	}
}

//给显示的Delivery Seal和 PickUp Seal的input添加值NA
function setNA(flag){

	if(flag==1){
		$("#in_seal").val("NA");
	}else{
		$("#out_seal").val("NA");
	}
}

function setParentNumber(CompanyID, number, CustomerID, AccountID, PONo, OrderNo,id,number_type,StagingAreaID,title,freight_term,appointmentDate,order_status,order_system){
	
	$('#customer_id'+id).val(CustomerID);
	$('#staging_Customer'+id).val(CustomerID);
	$('#company_id'+id).val(CompanyID);
	$('#account_id'+id).val(AccountID);
	$('#order_no'+id).val(OrderNo);
	$('#po_no'+id).val(PONo);
	$('#number_type'+id).val(number_type);
	$('#staging_area_id'+id).val(StagingAreaID);
	$('#title'+id).html(title+":");
	$('#'+id).val(number);
	$('#freight_term'+id).val(freight_term);
	$('#appointment_date'+id).val(appointmentDate);
	$("#number_order_status"+id).val(order_status);
	$("#order_system_type"+id).val(order_system);
	//$("#receipt_no"+id).val(receipt_no);
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/CheckInAutoAssignDoorAction.action',
		data:'search_number='+number+'&area_name='+StagingAreaID+'&ps_id=<%=psId%>'+'&mainId='+<%=id%>,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
			$.unblockUI();       //遮罩关闭
		
			$('#door'+id).val(data.door_name);
			$('#door'+id).attr("text",data.door_id);
			$('#allzone'+id).val(data.area_id);
			
			$('#staging'+id).val(StagingAreaID);
			$('#staging_zone'+id).val(data.area_name);
		},
		error:function(serverresponse, status){
			
			$.unblockUI();
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		}
	});
	//计算有效的条数
	effectiveNumber();
}

function setParentNumberDelivery(CompanyID, number, CustomerID,id,number_type,title,appointmentDate,order_status,order_system,supplier,receipt_no){
	
	$('#customer_id'+id).val(CustomerID);
	$('#company_id'+id).val(CompanyID);
	$('#account_id'+id).val('');
	$('#order_no'+id).val('');
	$('#po_no'+id).val('');
	$('#number_type'+id).val(number_type);
	$('#staging_area_id'+id).val('');
	$('#title'+id).html(title+":");
	$('#'+id).val(number);
	$('#freight_term'+id).val('');
	$('#appointment_date'+id).val(appointmentDate);
	$("#number_order_status"+id).val(order_status);
	$("#order_system_type"+id).val(order_system);
	$("#receipt_no"+id).val(receipt_no);
	$("#searchTitle"+id).val(supplier);
	$("#supplier_id"+id).val(supplier);
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/CheckInAutoAssignDoorAction.action',
		data:'search_number='+number+'&title='+supplier+'&ps_id=<%=psId%>'+'&mainId='+<%=id%>,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
			$.unblockUI();       //遮罩关闭
		
			$('#door'+id).val(data.door_name);
			$('#door'+id).attr("text",data.door_id);
			$('#allzone'+id).val(data.area_id);
			
			$('#searchZone'+id).val(data.area_name);
		},
		error:function(serverresponse, status){
			
			$.unblockUI();
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		}
	});
	//计算有效的条数
	effectiveNumber();
}

//计算总条数
function calculationAv(){
	var divs=$('div[name="av"]');
	$('#sumNumber').html(divs.length);
}
//计算有效的条数
function effectiveNumber(){
	var number_type=$('input[name="number_type"]');
	var num=0;
	for(var i=0;i<number_type.length;i++){
		if($(number_type[i]).val()!=0 && $(number_type[i]).val()!==""){
			num++;
		}
	}
	$('#effectiveNumber').html(num);
}

//计算序列号
function sequence(){
	var sequenceNum=$('span[name="sequence_index"]');
	var num=1;
	for(var i=0;i<sequenceNum.length;i++){
		$(sequenceNum[i]).html(num);
		num++;
	}
}

//计算添加按钮
function calcAddTask(){
	
	var tasks = $('input[name="addTask"]');
	tasks.each(function(index,element){
		
		/* if((tasks.length-1)==index){
			$(element).show();
		}else{
			$(element).hide();
		} */
		if(0==index){
			$(element).show();
		}else{
			$(element).hide();
		} 
	});
}

function findFirstContainerNoHasValue(){

	firstHasValueContainerNo = $("div[name=av] select[name=containerNo][value!='']:first").val();
	if(!firstHasValueContainerNo){
	
		firstHasValueContainerNo = '';
	}
}

//停车位回填
function setYardNo(yardId,yardNo,inputId,zone_id){
	$('#'+inputId).val(yardNo);
	$('#'+inputId).attr("text",yardId);
	//$("#"+inputId+"zone").val(zone_id);
	var num=$('#'+inputId).attr('in');
	$('#door'+num+'zone').val(zone_id);
}

//回填门
function getDoor(door,doorName,inputId,zone_id){    
	$('#'+inputId).val(doorName);
	$('#'+inputId).attr("text",door);
	var num=$('#'+inputId).attr('in');
	$('#door'+num+'zone').val(zone_id);
}

//页面验证number是否重复  同类型的 例如 pick up number不能重
function verification(number){
	
}
//post zone 改变时事件
function findSpotByZoneId(flag,spotArea,num){

	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindStorageYardSearchAction.action',
  		dataType:'json',
  		data:"spotArea="+spotArea+"&ps_id=<%=psId%>&flag="+flag+"&mainId="+<%=id%>,
		error:function(serverresponse, status){
			
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		},
  		success:function(data){
  			if(data.length>0){
  				$('#door'+num).val(data[0].yc_no);
  				$('#door'+num).attr("text",data[0].yc_id);
  			}else if(data.length==0 && flag==3){
  				findSpotByZoneId(4,spotArea,num);
  			}else if(data.length==0 && flag==4){
  				$('#door'+p).val('');
  				$('#door'+p).attr("text",'');	  		
  				//打开选择停车位列表
  				var inputId='door'+num;
  				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/get_yard_no.html?storageId=<%=psId%>'+'&spotZoneId='+spotArea+'&inputId='+inputId; 
	    		$.artDialog.open(uri , {title: "Spot",width:'600px',height:'520px', lock: true,opacity: 0.3,fixed: true});
  			}
  		}
    });
}

function unEditTask(){
	
	$("#Container input[type='text']").attr("disabled",true);
	$("#Container input[type='radio']").attr("disabled",true);
	$("#Container select").attr("disabled",true);
	$("#Container input[type='checkbox']").attr("disabled",true);
	$("#Container textarea").attr("disabled","disabled");
	
}

function editTask(evt){
	
	//解锁文本框
	var task = $(evt).parents("div[name='av']");
	var status = task.find("input[name='number_status']").val();
	
	if(status == <%=CheckInChildDocumentsStatusTypeKey.UNPROCESS%> || status == <%=CheckInChildDocumentsStatusTypeKey.PROCESSING%>){
		
		//按钮全部隐藏
		$("#Container input[type='button']").hide();

		//开启保存取消按钮
		$(evt).siblings("input[name='cancelTask']").show();
		$(evt).siblings("input[name='saveTask']").show();
		
		task.find("input[type='text']").not("input[name='appointment_date']").attr("disabled",false);
		task.find("input[type='radio']").attr("disabled",false);
		task.find("select").attr("disabled",false);
		task.find("input[type='checkbox']").attr("disabled",false);
		task.find("textarea").attr("disabled",false);
		
		var adids_default = task.find("input[name='tongzhiid']").val();
		var adnames_default = task.find("input[name='tongzhi']").val();
		if(adids_default == '' || adnames_default == '')
		{
			task.find("input[name='tongzhiid']").val('<%=adids_default%>');
			task.find("input[name='tongzhi']").val('<%=adnames_default%>');
		}
		
	}else{
		
		showMessage("Task is closed.","alert");
	}
}

function cancelTask(evt){
	
	//刷新页面
	loadTasks();
}

function moreSaveTask(wait,check_in){
	
	var target = $("#Container input[name='number']:enabled").parents("div[name='av']").find("input[name='saveTask']");
	
	saveTaskAndClose(target[0],wait,check_in);
	//if(target.length > 0){
/* 	if(saveTask(target[0],wait)){
		
		$.artDialog.close();
	} */
	//}
}

//提交方法
function saveTaskAndClose(evt,wait,check_in){
	evt = $("#Container input[name='number']:enabled").parents("div[name='av']").find("input[name='saveTask']");   //为了配合关闭window窗口重写这个方发
	var error_num='';
	var $num = $(evt).parents("div[name='av']").find("input[name='number']");
	
	var appointmentdate = $('#appointmentdate').val();
	var data={};
	data.items=[]; 
	var mainId=<%=id %>;
	
	data.mainId=mainId;
	data.appointmentdate=appointmentdate;
	data.waiting = wait;
	data.checkin = check_in;
	var prints=[]; 
	var det_detail_id=$('#det_detail_id').val();
	var isSubmit=true;
		
	
	for(var i=0;i<$num.length;i++){
	
		var id=$($num[i]).attr("id");
		
		var doorId=$('#door'+id).attr("text");
		var doorName = $('#door'+id).val();
		var zoneId=$('#door'+id+'zone').val();
		
		var tongzhiId=$('#tongzhi'+id+'Id').val();
		var tongzhiName=$('#tongzhi'+id).val();
		var detailId=$('#detailid'+id).val();
		var ra=$("input[name='ra"+id+"']:checked").val();
		
		//选择Pick Up Or Delivery
		if(!ra){
		
			showMessage("Check PickUp Or Delivery.","alert");
			return false;
		}
		
		var beizhu=$('#beizhu'+id).val();
		var youjian='';
		var duanxin='';
		var yemian='';	
	
		if(tongzhiId==""){
			showMessage("Choose Supervisor.","alert");
			return false;
		}	
		if($('#youjian'+id).attr('checked')=='checked'){
			youjian=1;
		}else{
			youjian=0;
		}
		if($('#duanxin'+id).attr('checked')=='checked'){
			duanxin=1;
		}else{
			duanxin=0;
		}
		if($('#yemian'+id).attr('checked')=='checked'){
			yemian=1;
		}else{
			yemian=0;
		}	
		var number=$($num[i]).val();
		var number_type=$('#number_type'+id).val();	
		var customer_id=$('#customer_id'+id).val();
		var company_id=$('#company_id'+id).val();
		var account_id=$('#account_id'+id).val();
		var order_no=$('#order_no'+id).val();
		var po_no=$('#po_no'+id).val();
		var supplier_id=$('#supplier_id'+id).val();
		var staging_area_id=$('#staging_area_id'+id).val();
		var freight_term=$('#freight_term'+id).val();
		var containerNo=$('#containerNo'+id).val();
		var door_spot=$('#door_spot'+id).val();
		var appointment_date = $("#appointment_date"+id).val();
		var number_order_status=$("#number_order_status"+id).val();
		var receipt_no = $("#receipt_no"+id).val();
		var order_system_type=$("#order_system_type"+id).val();
		
		if(containerNo == ""){
			
			showMessage("Select equipment.","alert");
			return false;
		}
		if($("#containerNo"+id).find("option:selected").text()=='')
		{
			showMessage("Modify equipment.","alert");
			return false;
		}
		
		if(number==""){
			showMessage("Enter LOAD#/PO#/Order#/BOL#/CTN#.","alert");
			return false;
		}
		
		if(doorId == undefined || doorId == "" || doorId == 0){
			
			showMessage("Choose door.","alert");
			return false;
		}
		
		if(number_type==''){
			
			isSubmit=false;
			error_num=number;
		}
		
		var num=0;
		for(var a=0;a<prints.length;a++){
			if(prints[a]==doorId){
				num=1;
			}
		}
		if(num==0){
			prints.push(doorId);
		}
		
		if(containerNo.toUpperCase() == 'NA'){
			containerNo = '';
		}
		var op={};
		op.ra = ra;
		op.occupancy_type=door_spot;
		op.container_no=containerNo;
		op.detailId=detailId;
		op.occupancy_type =door_spot;
		op.container_no = containerNo;
		op.doorId=doorId;
		op.doorName=doorName;
		op.zoneId=zoneId;
		op.number=number;
		op.number_type=number_type;
		op.customer_id=customer_id;
		op.company_id=company_id;
		op.order_no=order_no;
		op.po_no=po_no;
		op.freight_term=freight_term;
		op.tongzhi=tongzhiId;
		op.youjian=youjian;
		op.duanxin=duanxin;
		op.yemian=yemian;
		op.beizhu=beizhu;
		op.tongzhiName=tongzhiName;
		op.account_id=account_id;
		op.supplier_id=supplier_id;
		op.staging_area_id=staging_area_id;
		op.appointment_date = appointment_date;
		op.number_order_status = number_order_status;
		op.order_system_type = order_system_type;
		op.receipt_no= receipt_no;
		data.items.push(op);
	}
	
       if(isSubmit){
    	   var priority_html = 'Priority: <select id="priority">';
    	   priority_html+='<option value="0">NA</option>';
    	   priority_html+='<option <%= priority==checkInEntryPriorityKey.LOW?"selected":""%> value="<%=checkInEntryPriorityKey.LOW%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.LOW)%></option>';
    	   priority_html+='<option <%= priority==checkInEntryPriorityKey.MIDDLE?"selected":""%> value="<%=checkInEntryPriorityKey.MIDDLE%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.MIDDLE)%></option>';
    	   priority_html+='<option <%= priority==checkInEntryPriorityKey.HIGH?"selected":""%> value="<%=checkInEntryPriorityKey.HIGH%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.HIGH)%></option>';
    	   priority_html+='</select>';
    	   if(wait==1||check_in==1){
    		   $.artDialog.list['checkInOrWait'] && $.artDialog.list['checkInOrWait'].close();
    		   
	    	   $.artDialog({
	    		   	title:'priority',
	    		   	lock:true,
	    			opacity:0.3,
	    		   	content:priority_html,
	    		   	okVal: 'Confirm',
	   		   		ok: function () {
	   		    		priority = $("#priority").val();
	   		    		$.ajax({
	   		    	  		type:'post',
	   		    	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowTasksAction.action',
	   		    	  		dataType:'json',
	   		    	  		data:'det_detail_id='+det_detail_id+'&str='+JSON.stringify(data)+'&priority='+priority+'&'+$("#fileform").serialize()+'&is_check_in='+check_in+'',
	   		    			beforeSend:function(request){
	   		    				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   // 遮罩打开
	   		    		    },
	   		    		 	success:function(data){
	
	   		 				$.artDialog.opener.monitorCheckInWindowChanged();
	   		 				$.unblockUI();
	   		 				window.parent.location.reload();
	   		    	  		},
	   		    	  		error:function(serverresponse, status){
	   		    	  			$.unblockUI();
	   		    	  			showMessage(errorMessage(serverresponse.responseText),"alert");
	   		    	  		}
	   		    	    });
	   		   		
	   		    	},
	   		    	cancelVal:'Cancel',
	   		    	cancel:function(){
	   		    		
	   		    	}
	    		   
	    	   });
    	   }else{
    		   $.ajax({
		    	  		type:'post',
		    	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowTasksAction.action',
		    	  		dataType:'json',
		    	  		data:'det_detail_id='+det_detail_id+'&str='+JSON.stringify(data)+'&priority='+priority+'&'+$("#fileform").serialize()+'&is_check_in='+check_in+'',
		    			beforeSend:function(request){
		    				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   // 遮罩打开
		    		    },
		    		 	success:function(data){
			 				$.artDialog.opener.monitorCheckInWindowChanged();
			 				$.unblockUI();
			 				window.parent.location.reload();
		    	  		},
		    	  		error:function(serverresponse, status){
		    	  			$.unblockUI();
		    	  			showMessage(errorMessage(serverresponse.responseText),"alert");
		    	  		}
		    	    });
    	   }
        	
       }else{
       	showMessage(error_num+':\n Search No#.',"alert");
       }
       
       return true;
}

//提交方法
function saveTask(evt,wait){
	
	var error_num='';
	var $num = $(evt).parents("div[name='av']").find("input[name='number']");
	
	var appointmentdate = $('#appointmentdate').val();
	var data={};
	data.items=[]; 
	var mainId=<%=id %>;
	
	data.mainId=mainId;
	data.appointmentdate=appointmentdate;
	data.waiting = wait;
	var prints=[]; 
	var det_detail_id=$('#det_detail_id').val();
	var isSubmit=true;
		
	
	for(var i=0;i<$num.length;i++){
	
		var id=$($num[i]).attr("id");
		
		var doorId=$('#door'+id).attr("text");
		var doorName = $('#door'+id).val();
		var zoneId=$('#door'+id+'zone').val();
		
		var tongzhiId=$('#tongzhi'+id+'Id').val();
		var tongzhiName=$('#tongzhi'+id).val();
		var detailId=$('#detailid'+id).val();
		var ra=$("input[name='ra"+id+"']:checked").val();
		
		//选择Pick Up Or Delivery
		if(!ra){
		
			showMessage("Check PickUp Or Delivery","alert");
			return false;
		}
		
		var beizhu=$('#beizhu'+id).val();
		var youjian='';
		var duanxin='';
		var yemian='';	
	
		if(tongzhiId==""){
			showMessage("Choose Supervisor","alert");
			return false;
		}	
		if($('#youjian'+id).attr('checked')=='checked'){
			youjian=1;
		}else{
			youjian=0;
		}
		if($('#duanxin'+id).attr('checked')=='checked'){
			duanxin=1;
		}else{
			duanxin=0;
		}
		if($('#yemian'+id).attr('checked')=='checked'){
			yemian=1;
		}else{
			yemian=0;
		}	
		var number=$($num[i]).val();
		var number_type=$('#number_type'+id).val();	
		var customer_id=$('#customer_id'+id).val();
		var company_id=$('#company_id'+id).val();
		var account_id=$('#account_id'+id).val();
		var order_no=$('#order_no'+id).val();
		var po_no=$('#po_no'+id).val();
		var supplier_id=$('#supplier_id'+id).val();
		var staging_area_id=$('#staging_area_id'+id).val();
		var freight_term=$('#freight_term'+id).val();
		var containerNo=$('#containerNo'+id).val();
		var door_spot=$('#door_spot'+id).val();
		var appointment_date = $("#appointment_date"+id).val();
		var number_order_status=$("#number_order_status"+id).val();
		var order_system_type=$("#order_system_type"+id).val();
		var receipt_no = $("#receipt_no"+id).val();
		
		if(containerNo == ""){
			
			showMessage("Select equipment.","alert");
			return false;
		}
		if($("#containerNo"+id).find("option:selected").text()=='')
		{
			showMessage("Modify equipment.","alert");
			return false;
		}
		if(number==""){
			showMessage("Enter LOAD#/PO#/Order#/BOL#/CTN#.","alert");
			return false;
		}
		
		if(doorId == undefined || doorId == "" || doorId == 0){
			
			showMessage("Choose door.","alert");
			return false;
		}
		
		if(number_type==''){
			
			isSubmit=false;
			error_num=number;
		}
		
		var num=0;
		for(var a=0;a<prints.length;a++){
			if(prints[a]==doorId){
				num=1;
			}
		}
		if(num==0){
			prints.push(doorId);
		}
		
		if(containerNo.toUpperCase() == 'NA'){
			containerNo = '';
		}
		var op={};
		op.ra = ra;
		op.occupancy_type=door_spot;
		op.container_no=containerNo;
		op.detailId=detailId;
		op.occupancy_type =door_spot;
		op.container_no = containerNo;
		op.doorId=doorId;
		op.doorName=doorName;
		op.zoneId=zoneId;
		op.number=number;
		op.number_type=number_type;
		op.customer_id=customer_id;
		op.company_id=company_id;
		op.order_no=order_no;
		op.po_no=po_no;
		op.freight_term=freight_term;
		op.tongzhi=tongzhiId;
		op.youjian=youjian;
		op.duanxin=duanxin;
		op.yemian=yemian;
		op.beizhu=beizhu;
		op.tongzhiName=tongzhiName;
		op.account_id=account_id;
		op.supplier_id=supplier_id;
		op.staging_area_id=staging_area_id;
		op.appointment_date = appointment_date;
		op.number_order_status = number_order_status;
		op.order_system_type = order_system_type;
		op.receipt_no = receipt_no;
		data.items.push(op);
	}
	
       if(isSubmit){
       	$.ajax({
   	  		type:'post',
   	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowTasksAction.action',
   	  		dataType:'json',
   	  		data:'det_detail_id='+det_detail_id+'&str='+JSON.stringify(data)+'&priority='+priority+'&'+$("#fileform").serialize(),
   			beforeSend:function(request){
   				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   // 遮罩打开
   		    },
   		 	success:function(data){
				loadTasks();
				$.artDialog.opener.monitorCheckInWindowChanged();
				$.unblockUI();
   	  		},
   	  		error:function(serverresponse, status){
   	  			$.unblockUI();
   	  			showMessage(errorMessage(serverresponse.responseText),"alert");
   	  		}
   	    });
       }else{
       	showMessage(error_num+':\n  Search No#',"alert");
       }
       
       return true;
}

function validateSelect(target){
	
	var id = $(target).attr('id');
	
	var ra = $("input[name='ra"+id+"']:checked").val();
	
	if(!ra){
		showMessage("Check PickUp Or Delivery","alert");
	}
}
</script>
</body>
</html>

