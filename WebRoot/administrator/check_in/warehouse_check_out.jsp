<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<%
 	long id=StringUtil.getLong(request,"infoId");
 long status=StringUtil.getLong(request,"status");
 String doorId = StringUtil.getString(request,"doorId");
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 long adminId=adminLoggerBean.getAdid(); 
 DBRow manRow=checkInMgrZwb.findGateCheckInById(id);

 String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/check_in/gate_check_out.html?id="+id;
 String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
 String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
 String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
 String checkInFileUploadAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/checkin/checkInFileUploadAction.action";
 DBRow[] files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(id,FileWithTypeKey.OCCUPANCY_MAIN);
 DBRow[] rows = checkInMgrZwb.findParkAndDoorByEntryId(id);
 %>	
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
	<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
	
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	<!-- 在线阅读 -->
	<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script> 
	<!-- 打印 -->
	<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
	<script type="text/javascript" src="../js/print/m.js"></script>
	 <!-- 遮罩 -->
    <script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
    
	<style type="text/css">
td.topBorder{
	border-top: 1px solid silver;
}
.ui-datepicker{font-size: 0.9em;}
</style>
<script type="text/javascript">

(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

$(function(){
	$("#tabs").tabs({});
	
	$("input[name='checkboxDoor']").attr("checked",true); 
	$("input[name='checkboxPark']").attr("checked",true); 
	
	
});
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	$.artDialog.open(uri , {id:'file_up',title: 'Upload photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
				 //调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   	}});
}
//在线获取
function onlineScanner(target){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target="+target; 
	$.artDialog.open(uri , {title: 'Online Mutiple',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
var onlineNames = "";
//jquery file up 回调函数
function uploadFileCallBack(fileNames,_target){
	if(_target=="onlineScanner"){
		if($.trim(fileNames).length > 0 ){
			var array = fileNames.split(",");
			var lis = "";
			for(var index = 0 ,count = array.length ; index < count ; index++ ){
				var a =  createA(array[index]);
				if(a.indexOf("href") != -1){
				    lis += a;
				}
			}
			var td = $("#over_file_td");
			td.append(lis);
			onlineNames += fileNames+",";
			$("input[name='file_names']").val(onlineNames); 
		} 
	}else{
		    $("p.new").remove();
		    if($.trim(fileNames).length > 0 ){
				$("input[name='file_names']").val(fileNames); 
				var array = fileNames.split(",");
				var lis = "";
				for(var index = 0 ,count = array.length ; index < count ; index++ ){
					var a =  createA(array[index]) ;
					 
					if(a.indexOf("href") != -1){
					    lis += a;
					}
				}
				var td = $("#over_file_td");
				
				td.append(lis); 
			} 
	}		
		$("#submitBtn").focus();
}
function createA(fileName,target){
    var id = fileName.substring(0,fileName.length-4);
    var  a = "<p id='add"+id+"' style='color:#439B89' class='new' ><a id='"+id+"'  href='javascript:void(0)' onclick='showTempPictrueOnline(&quot;"+fileName+"&quot;)' style='color:#439B89' >"+fileName+"</a>&nbsp;&nbsp;<a  href='javascript:deleteA(&quot;"+id+"&quot;)' style='color:red' >×</a></p>";
    return a ;
}

function deleteA(id){
	$("#add"+id).remove();
	var file_names = $("input[name='file_names']").val(); 
	
	 var array = file_names.split(",");
	 var lastFile = "";
	 for(var index = 0 ; index < array.length ; index++ ){
			if(id==array[index].substring(0,array[index].length-4)){
				array[index]="";
				
			}
			lastFile += array[index];
			if(index!=array.length-1 && array[index]!=""){
				lastFile+=",";
			}
	 }
	 
	 $("input[name='file_names']").val(lastFile);
	
}
function closeWindow(){
	$.artDialog.close();
}
//图片在线显示  		 
function showPictrueOnline(fileWithType,fileWithId ,currentName){
    var obj = {
  		file_with_type:fileWithType,
  		file_with_id : fileWithId,
  		current_name : currentName ,
  		cmd:"multiFile",
  		table:'file',
  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/check_in"
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
}
//删除图片
function deleteFile(file_id){
	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:'file',file_id:file_id,folder:'check_in',pk:'file_id'},
		success:function(data){
			if(data && data.flag === "success"){
				$("#file_tr_"+file_id).remove();
			}else{
				showMessage("System error,please try later","error");
			}
			
		},
		error:function(){
			showMessage("System error,please try later","error");
		}
	});
}
function confirm(){
	    $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
		if(onlineNames!="" && onlineNames!=null){
			var onlineNames = $("input[name='file_names']").val(); 
			onlineNames = onlineNames.substring(0,onlineNames.length-1);
			$("input[name='file_names']").val(onlineNames);
		}
		
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/CheckInFileUploadAction.action',
			dataType:'json',
			data:'dlo_id='+<%=id%>+'&createMainId='+<%=adminId%>+'&'+$("#fileform").serialize(),
			success:function(data){	   
			   $.artDialog.opener.releaseDoor(<%=status%>,<%=doorId%>,<%=id%>);
			   $.artDialog && $.artDialog.close();
			}
		});
		
	
}
function showTempPictrueOnline(_fileName){
	   var obj = {
			current_name:_fileName ,
	   		table:"temp",
	   		fileNames:$("input[name='file_names']").val(),
	   		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upl_imags_tmp"
		}
	   if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}
function onlineSingleScanner(_target){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_single_scanner.html?target="+_target; 
$.artDialog.open(uri , {title: 'Online Single',width:'300px',height:'170px', lock: true,opacity: 0.3,fixed: true});

}
function closeWindow(){
	$.artDialog.close();
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">
<fieldset style="border:2px solid #993300;width:96%;">
<legend>
    <!-- <input type="button" value="测试打印" onclick="ceshiPrint(<%=id%>)" /> -->
	<%if(manRow.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.LIVE){%>
		<span style="font-size:20px; font-weight: bold;" id="methods">Live Load</span>
	<%}else if(manRow.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.DROP){%>
		<span style="font-size:20px; font-weight: bold;" id="methods">Drop Off</span>
	<%}else if(manRow.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.SWAP){%>
		<span style="font-size:20px; font-weight: bold;" id="methods">Swap</span>
	<%}else if(manRow.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.PICK_UP){ %>
		<span style="font-size:20px; font-weight: bold;" id="methods">Pick Up</span>
	<%} %>
</legend>
<br>
	
<form id="fileform" method="post" action="<%=checkInFileUploadAction %>">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	    <td colspan="2" align="left" valign="top">
		    <fieldset style="border:2px #cccccc solid;padding:15px;margin:5px;width:93%; height:100px; overflow:auto">
				<legend style="font-size:14px;font-weight:normal;color:#999999;">
						Photo
				</legend>
		    	<table>
		    		<tr>
		    			<td>
		    				<input type="button" id="online1" class="long-button" onclick="onlineScanner('onlineScanner');" value="Multiple Photos" />
		    			</td>
						<td>
                            <input type="button" id="online" class="long-button" onclick="onlineSingleScanner('onlineScanner');" value="Single Photo" />
							<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="Upload Photo" />
						</td>
					</tr>
					<tr>
					</tr>
					<%
					 if(files != null && files.length > 0 ){
			      		for(DBRow file : files){
					%>
					<tr id="file_tr_<%=file.getString("file_id") %>">
						<td><%=file.getString("upload_time").substring(0,10) %></td>
						<td>
						 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
	                 <%if(StringUtil.isPictureFile(file.getString("file_name"))){ %>
				             <p>
				 	            <a style="color:#439B89" id="filename" href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.OCCUPANCY_MAIN%>','<%=id%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
				             </p>
				     <%}else{ %>
								<a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder= check_in'><%=file.getString("file_name") %></a>
					 <%}%>
						</td>
						<td width="30px"></td>
						<td>
							<a  href="javascript:deleteFile('<%=file.get("file_id",0l) %>')" style='color:red' >×</a>
						</td>	
					 </tr>	 
					<%}}%>
					
					<tr>
						<input type="hidden" name="backurl" id="backurl" value="" />
		 				<input type="hidden" name="file_with_type" value="<%=FileWithTypeKey.OCCUPANCY_MAIN %>" />
		 				<input type="hidden" name="sn" id="sn" value="window_check_out"/>
					 	<input type="hidden" name="file_names" id="file_names"/>
					 	<input type="hidden" name="path" value="check_in"/>
					 	<input type="hidden" name="dlo_id" value="<%=id%>"/>
					 	<td></td>
					 	<td id="over_file_td"></td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
</table>
</form>	
</fieldset>	
<table>
	 <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:97%;position:absolute;left;0;bottom:0;">
		 <input type="button"  name="Submit2" value="Check out" class="normal-green" onClick="confirm();" >
		 <input name="Submit2" type="button" class="normal-white" value="Cancel" onClick="closeWindow();">
     </div>
</table>
</body>
