<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.FileWithCheckInClassKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>

<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey" %>

<jsp:useBean id="checkInMainDocumentsRelTypeKey" class="com.cwc.app.key.CheckInMainDocumentsRelTypeKey"/> 
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="fileWithCheckInClassKey" class="com.cwc.app.key.FileWithCheckInClassKey"/> 
<%
 long id=StringUtil.getLong(request,"infoId");
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 long ps_id=adminLoggerBean.getPs_id();   
 DBRow manRow=checkInMgrZwb.findGateCheckInById(id);
 String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/check_in/gate_check_out.html?id="+id;
 String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
 String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
 String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
 String checkInFileUploadAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/checkin/checkInFileUploadAction.action";
 DBRow[] files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(id,FileWithTypeKey.OCCUPANCY_MAIN);
 DBRow rows = checkInMgrZwb.findParkAndDoorByEntryId(id);
 DBRow[] resourcesByEntry = checkInMgrZwb.getCheckOutEntryUseResource(id); 
 %>
 <head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<!-- 引入Art -->
<!-- 	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script> -->
	<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
	<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<!-- 在线阅读 -->
	<script type="text/javascript" src="../js/office_file_online/officeFileOnlineServ.js"></script>
	
	<!-- 图片上传 -->
	<script type="text/javascript" src="../js/picture/picture.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/picture/picture.css" />
	
	<!-- 打印 -->
	<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
	<script type="text/javascript" src="../js/print/m.js"></script>
	 <!-- 遮罩 -->
    <script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
    
    <script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
    <!-- jquery UI 加上 autocomplete -->
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
    
    
	<style type="text/css">
		td.topBorder{
			border-top: 1px solid silver;
		}
		.ui-datepicker{font-size: 0.9em;}
		
		.check {
			width:100%;
			height:30px;
			font-family: Verdana;
			font-size: 14px;
		}
		.tms_info{
			display: inline-block;
		    font-weight: bold;
		    margin-top: 5px;
		}
	</style>
	<!-- script预留区域 -->
<script type="text/javascript">
var validate = false;
	
(function(){
	
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

jQuery(function($){
	addAutoComplete($("#dropTrailerNo"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInCTNRJSONAction.action?flag=1",
			"equipment_number","equipment_number");
	completeSelect( $( "#dropTrailerNo" ), searchTrailerNo );
	clickAutoComplete($("#dropTrailerNo"));	
});
var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	api.button([
		{
			name: 'Check Out',
			callback: function () {
				confirm();
				return false ;
			},
			focus:true,
			focus:"default"
		},
		{
			name: 'Cancel'
			
			
		}]
	);
//当点击文本框时触发事件
//$("#Both input[id='ctnr']")
function clickAutoComplete(jqObj) {
	jqObj.bind('click',function() {
		var val = $.trim($(this).val());
		if(val != ""){
			jqObj.autocomplete( "search" );
		}
	});

}

function completeSelect(jqObj, func) {
	jqObj.autocomplete({ 
		select: function (event, ui) {
			var arg = ui.item.value;
			func(arg);
		}
	});	
}
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file_serv/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	$.artDialog.open(uri , {id:'file_up',title: 'Upload photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    	close:function(){
		// 调用弹出页面的方法,弹出页面的方法是执行父页面的方法
		this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   	}});
}
//在线获取
function onlineScanner(target){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file_serv/picture_online_scanner.html?target="+target; 
	$.artDialog.open(uri , {title: 'Online Mutiple',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function onlineSingleScanner(_target){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file_serv/picture_online_single_scanner.html?target="+_target; 
	$.artDialog.open(uri , {title: 'Online Single',width:'300px',height:'170px', lock: true,opacity: 0.3,fixed: true});
}
var onlineNames = "";
//jquery file up 回调函数
function uploadFileCallBack(fileNames,_target){
	/* if(_target=="onlineScanner"){
		if($.trim(fileNames).length > 0 ){
			var array = fileNames.split(",");
			var lis = "";
			for(var index = 0 ,count = array.length ; index < count ; index++ ){
				var a =  createA(array[index]);
				if(a.indexOf("href") != -1){
				    lis += a;
				}
			}
			var td = $("#over_file_td");
			td.append(lis);
			onlineNames += fileNames+",";
			$("input[name='file_names']").val(onlineNames); 
		} 
	}else{
		    $("p.new").remove();
		    if($.trim(fileNames).length > 0 ){
				$("input[name='file_names']").val(fileNames); 
				var array = fileNames.split(",");
				var lis = "";
				for(var index = 0 ,count = array.length ; index < count ; index++ ){
					var a =  createA(array[index]) ;
					 
					if(a.indexOf("href") != -1){
					    lis += a;
					}
				}
				var td = $("#over_file_td");
				
				td.append(lis); 
			} 
	}		 */
	$("#"+_target).showPicture("appendPicture",fileNames);
	$("#submitBtn").focus();
}
function createA(fileName,target){
    var id = fileName.substring(0,fileName.length-4);
    var  a = "<p id='add"+id+"' style='color:#439B89' class='new' ><a id='"+id+"'  href='javascript:void(0)' onclick='showTempPictrueOnline(&quot;"+fileName+"&quot;)' style='color:#439B89' >"+fileName+"</a>&nbsp;&nbsp;<a  href='javascript:deleteA(&quot;"+id+"&quot;)' style='color:red' >×</a></p>";
    return a ;
}

function deleteA(id){
	$("#add"+id).remove();
	var file_names = $("input[name='file_names']").val(); 
	
	 var array = file_names.split(",");
	 var lastFile = "";
	 for(var index = 0 ; index < array.length ; index++ ){
			if(id==array[index].substring(0,array[index].length-4)){
				array[index]="";
				
			}
			lastFile += array[index];
			if(index!=array.length-1 && array[index]!=""){
				lastFile+=",";
			}
	 }
	 
	 $("input[name='file_names']").val(lastFile);
	
}
function closeWindow(){
	$.artDialog.close();
}
//图片在线显示  		 
function showPictrueOnline(fileWithType,fileWithId ,currentName){
    var obj = {
  		file_with_type:fileWithType,
  		file_with_id : fileWithId,
  		current_name : currentName ,
  		cmd:"multiFile",
  		table:'file',
  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/check_in"
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
}
//删除图片
function deleteFile(file_id){
	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:'file',file_id:file_id,folder:'check_in',pk:'file_id'},
		success:function(data){
			if(data && data.flag === "success"){
				$("#file_tr_"+file_id).remove();
			}else{
				showMessage("System error,please try later","error");
			}
			
		},
		error:function(){
			showMessage("System error,please try later","error");
		}
	});
}
function cancel(){$.artDialog && $.artDialog.close();}

//提交方法
var closeFlag = 0;
function confirm(){
		var sealObj = $("input[name='sealList']");
		 for(var i=0;i<sealObj.length;i++) {
		 	if(sealObj[i].value == ""){
		 		showMessage("Please input Check Seal or click NA button!","alert");	
		 		$(sealObj[i]).focus();
		 		return false;
		 	}else if($(sealObj[i]).attr("val_seal") == "false"){
		 		checkSeal(sealObj[i],0);
		 		return false;
		 	}
		 }
		if(onlineNames!="" && onlineNames!=null){
			var onlineNames = $("input[name='file_names']").val(); 
			onlineNames = onlineNames.substring(0,onlineNames.length-1);
			$("input[name='file_names']").val(onlineNames);
		}
		var jsonObject = {};
		var jsonArray = [];
		// 遍历页面所有选中的checkbox，获取其属性，放入到jsonArray中
		$("input[type=checkbox]:checked").each(function(){
            var arr = 
            {
           		equipment_type : $(this).attr("equipment_type"),
           		equipment_id : $(this).attr("equipment_id"),
           		resources_id : $(this).attr("resources_id"),
           		equipment_no : $(this).attr("equipment_no"),
           		resources_type : $(this).attr("resources_type"),
           		location : $(this).attr("location"),
           		isSearch : $(this).attr("isSearch"),
           		check_in_entry_id : $(this).attr("check_in_entry_id"),
           		seal : $(this).attr("seal")
            };
            // 过滤重复的设备ID
           if(!equipmentInArray($(this).attr("equipment_id"),jsonArray))
        	{
            	jsonArray.push(arr);
        	}
            
		});
		jsonObject.entry_id = $("#entry_id").val();
		jsonObject.jsonArray = jsonArray;
		
		var ps_id = <%=ps_id%>;
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCheckInOutAction.action',
			dataType:'json',
			data:'jsonObject='+jQuery.fn.toJSON(jsonObject)+'&'+$("#fileform").serialize()+"&close_flag="+closeFlag,
			beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
		    },
			success:function(data){
				$.unblockUI();
				if(data.result != undefined && data.result != '' && data.result==0){
					var html = "<div>Please call: "+data.phone_number+"</div>";
					var info_dialog = $.artDialog({content:html,title: 'TMS Info', lock: true,opacity: 0.3,fixed: true,
						button:[
							{
								name:"Check Out",
								callback:function(){
									closeFlag = 1;
									confirm();
								},
								focus:true
							},{
								name:"Cancel",
								callback:function(){
									info_dialog.close();
								}
							}
						
						]
					});
				}else if(data.result != undefined && data.result != '' && data.result==1){
					
					var html = "<div>";
					html+="<span style='display: inline-block;text-align: right;width: 110px;'>Driver Name:</span> <span class='tms_info'>"+(data.driver_name==null?"&nbsp;":data.driver_name)+"</span><br/>";
					html+="<span style='display: inline-block;text-align: right;width: 110px;'>Completed Date:</span> <span class='tms_info'>"+(data.dispatch_date==null?"&nbsp;":data.dispatch_date)+"</span><br/>";
					html+="<span style='display: inline-block;text-align: right;width: 110px;'>Completed Date:</span> <span class='tms_info'>"+(data.completed_date==null?"&nbsp;":data.completed_date)+"</span><br/>";
					html+="<span style='display: inline-block;text-align: right;width: 110px;'>From Wareshouse:</span> <span class='tms_info'>"+(data.from_wareshouse==null?"&nbsp;":data.from_wareshouse)+"</span><br/>";
					html+="<span style='display: inline-block;text-align: right;width: 110px;'>To Wareshouse:</span> <span class='tms_info'>"+(data.to_warehouse==""?"&nbsp;":data.to_warehouse)+"</span><br/>";
					html += "<div>";
					var info_dialog = $.artDialog({content:html,title: 'TMS Info', lock: true,opacity: 0.3,fixed: true,
						button:[
							{
								name:"Check Out",
								callback:function(){
									closeFlag = 1;
									confirm();
								},
								focus:true
							},{
								name:"Cancel",
								callback:function(){
									info_dialog.close();
								}
							}
						
						]
					});
				}else{
					
				   parent.location.reload();        
				   $.artDialog && $.artDialog.close();
				}
			},
			error:function(){
				$.unblockUI();
				showMessage("System error!","alert");
			}
		});
		
}

function showTempPictrueOnline(_fileName){
	   var obj = {
			current_name:_fileName ,
	   		table:"temp",
	   		fileNames:$("input[name='file_names']").val(),
	   		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upl_imags_tmp"
		}
	   if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}
function closeWindow(){
	$.artDialog.close();
}
function searchTrailerNo()
{
	var trailerNo = $("#dropTrailerNo").val();
	if(trailerNo == ""){
		showMessage("Please enter the equipmentNo!","alert");
		return false;
	}
	
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCheckOutSearchEntryIdAction.action',
		dataType:'json',
		data:'trailerNo='+trailerNo,
		success:function(data){
			if(data.check_in_entry_id != "" && data.check_in_entry_id != undefined)
			{
				inseartEquipmentToCheckoutList(data);				
			}else
			{
				creatNewEquipment(trailerNo)
			//	showMessage("Please enter the correct equipmentNo!","alert");
			}
			onLoadInitZebraTable();
			$("#dropTrailerNo").val("");
		}
	});
}
//在checkout 添加equipment的时候，如果设备不存在则提示是否新加一条
function creatNewEquipment(trailerNo){
	var creat_info_dialog = $.artDialog({
		content:"The equipment not found in yard, is add?",
		icon:"question",
		lock: true,
		opacity: 0.3,
		fixed: true,
		title:"tips",
		button:[
		        {
		        	name:"YES",
		        	callback:function(){
		        		console.log("click yes");
		        		$.ajax({
		        			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/creatNewEquipmentOnCheckOut.action',
		        			dataType:"json",
		        			type:"post",
		        			data:{equipment_type:2,equipment_number:trailerNo},
		        			sendbefore:function(){
		        				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		        			},
		        			success:function(data){
		        				$.unblockUI();
		        				inseartEquipmentToCheckoutList(data);	
		        			},
		        			error:function(){
		        				$.unblockUI();
		        				showMessage("system error!","alert");
		        			}
		        			
		        		});
		        	},
		        	focus:true
		        },
		        {
		        	name:"NO",
		        	callback:function(){
		        		creat_info_dialog.close();
		        	}
		        }
		        ]
	});
}

function inseartEquipmentToCheckoutList(data){
	console.log(data);
	var tableObj = $("#search_tab");
	// 创建一个存放 已查询的货柜号LIST
	var trailerArry = new Array();
	var trailerObj = $(".trailerno");
	for(var i = 0; i<trailerObj.length ; i++){
		trailerArry[i] = trailerObj[i].value;
	}
	// 比对当前查询的货柜号 是否存在
	var isExist = inArray(data.equipment_number,trailerArry,false);
	var html = "";
	// 当前查询的货柜号 不存在
	if(!isExist)
	{
		$("tr[id=temp]").remove();
		var sealData = "";
		var is_disabled = data.is_disabled;
		if(is_disabled == "true")
		{ 
			sealData = "<td height='25' align='center' valign='middle' style='word-break:break-all;'>&nbsp;</td>";
    	}
		else
		{
    		sealData = "<td height='25' align='center' valign='middle' style='word-break:break-all;'>"+
    		"<input size='15' type='text' id='listseal_"+data.equipment_id+"' val_seal='false' name='sealList' equipment_id='"+data.equipment_id+"' onBlur='checkSeal(this,0)' onkeyup='daxie(this)' now_seal='"+data.now_seal+"' value=''/>"+
    		"&nbsp;&nbsp;<input type='button'  name='no_seal' id='no_seal' value='NA' class='short-short-button-mod' onClick='checkSeal(this,1);'>"+
    		"</td>";
    	} 
		html  = "<tr>" +
		"<input type='hidden' name='trailerno' class='trailerno' value='"+data.equipment_number+"' />"+
		"<td height='25' align='center' valign='middle' style='word-break:break-all;'><input type='checkbox' checked='checked' seal='' OnClick='selectCheckbox(this);'  entry_id='"+data.check_in_entry_id+"' equipment_type='"+data.equipment_type+"' resources_type='"+data.resources_type+"' resources_id='"+data.resources_id+"' equipment_id='"+data.equipment_id+"' equipment_no='"+data.equipment_number+"' location='"+data.location+"' isSearch='"+data.is_search+"' check_in_entry_id='"+data.check_in_entry_id+"'/></td>" +
		"<td height='25' align='center' valign='middle' style='word-break:break-all;'>"+data.equipment_number+"</td>"  +
		"<td height='25' align='center' valign='middle' style='word-break:break-all;'>"+data.check_in_entry_id+"</td>"  +
		"<td height='25' align='center' valign='middle' style='word-break:break-all;'>&nbsp;"+data.location+"</td>"  +
		"<td height='25' align='center' valign='middle' style='word-break:break-all;'>&nbsp;"+data.carrier+"</td>"  +
		"<input type='hidden' name='location' class='location' value='"+data.location+"' />"+
		"<input type='hidden' name='resources_type' class='resources_type' equipment_type_msg='Trailer' locationValue='"+data.location+"' value='"+data.resources_type+"' />"+
		sealData +
		"</tr>";
		
		tableObj.append(html);
	}else{	//设备已经在列表内了  不能再添加
		showMessage("The equipmentNo you have entered already exists!","alert");
	}
		
}

//选中之后，把隐藏的div中的checkbox选中，取消则取消
function selectCheckbox(obj)
{
	var id = $(obj).attr("equipment_id");
//	var checkObj = $("div[id="+id+"]").find("input[type=checkbox]");
	
	if(!$(obj).attr("checked")){
		//未选中
//		$(checkObj).removeAttr("checked");
		$(obj).removeAttr("checked");
	}else{
		//选中
//		$(checkObj).attr("checked","checked");
		$(obj).attr("checked","checked");
	}
	
}

// 过滤重复的设备ID
function equipmentInArray(equipment_id,equipmentArray)
{    
	if(equipmentArray.length != 0)
	{
		for(var i=0; i<equipmentArray.length; i++)
		{    
			if(equipment_id == equipmentArray[i].equipment_id){
				return true;    
			}    
		}    
		return false;    
	}
	else
	{
		return false;
	}
}


// 检查当前查询的货柜号，是否已存在
function inArray(trailerNo,trailerArry,isPlace)
{    
	if(trailerArry.length != 0)
	{
		for(var i=0; i<trailerArry.length; i++)
		{    
			if(trailerNo == trailerArry[i]){
			 	// 是否需要返回在数组中的位置
				if(isPlace)
				{    
				    return i;    
				}    
				return true;    
			}    
		}    
		return false;    
	}
	else
	{
		return false;
	}
}

function daxie(val){
	val.value=val.value.toUpperCase()
	val.value=trim(val.value);
}
function trim(str){
    return str.replace(/[ ]/g,"");  //去除字符算中的空格
}

//list中的seal 验证 弹出浮层选择
var isDialog = true;
function checkSeal(obj,num){
	if(num==0 && isDialog){
		var now_seal = $(obj).val();
		if(now_seal==''){
			showMessage("Please input Check Seal or click NA button!","alert");	
			window.setTimeout(function(){
				obj.focus();			
			},300);
	 		return false;
		}
		var after_seal = $(obj).attr("now_seal");
		var equipment_id = $(obj).attr("equipment_id");
		var id=$(obj).attr('id');
		if(after_seal !="" && after_seal != now_seal)
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_check_out_select_seal.html?after_seal='+after_seal+'&now_seal='+now_seal+'&input_id='+id+'&equipment_id='+equipment_id; 
			$.artDialog.open(uri , {title: "Please Select The Seal",width:'440px',height:'130px', lock: true,opacity: 0.3,fixed: true});
		}else
		{
			$("input[id='listseal_"+equipment_id+"']").attr("val_seal","true");
		}
	}else{
		$(obj).parent().find("input[type=text]").val("NA");
		var equipment_id = $(obj).parent().find("input[type=text]").attr("equipment_id");
		var checkboxObj = $("input[type='checkbox'][equipment_id='"+equipment_id+"']");
		var after_seal = $("input[id='listseal_"+equipment_id+"']").attr("now_seal");
		var id = $("input[id='listseal_"+equipment_id+"']").attr("id");
		var now_seal = "NA";
		if(after_seal !="" && after_seal != now_seal)
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_check_out_select_seal.html?after_seal='+after_seal+'&now_seal='+now_seal+'&input_id='+id+'&equipment_id='+equipment_id; 
			$.artDialog.open(uri , {title: "Please Select The Seal",width:'440px',height:'130px', lock: true,opacity: 0.3,fixed: true});
		}else{
			$("input[id='listseal_"+equipment_id+"']").attr("val_seal","true");
		}
			//$(checkboxObj).attr("seal","NA");
	}	
}

//鼠标移入 na 按钮
function naIn(){
	isDialog = false;
}
//鼠标离开 na 按钮
function naOut(){
	isDialog = true;
}


//复选框实现单选功能
function check(seal,input_id,equipment_id) {
	$('#'+input_id).val(seal);
	$('#'+input_id).attr("val_seal","true");
	$("input[type='checkbox'][equipment_id='"+equipment_id+"']").attr("seal",seal);
	$("input[id='listseal_"+equipment_id+"']").attr("val_seal","true");
	//提示 是否现在提交
	$.artDialog({
	    content: 'Do You Want To Check Out?',
	    icon: 'question',
	    width: 230,
	    height: 70,
	    title:'',
	    okVal: 'Check Out',
	    ok: function () {		
	    	confirm();   //掉提交的方法
	    },
	    cancelVal: 'Cancel',
	    cancel: function(){
	    	
		}
	});	    	
}

//获得焦点 验证失效
function failure(input_id){
	$(input_id).attr("val_seal","false");
}



function trim(str){ //删除左右两端的空格
	return str.replace(/(^\s*)|(\s*$)/g, "");
}
function fixSeal(outSeal){
	if(outSeal.toUpperCase() == 'NA' || trim(outSeal) == '' || outSeal.toUpperCase() == 'N/A') {
		outSeal = 'NA';
	}
	return outSeal;
}
function loadPhotosFromFileserv(){
	$("#show").showPicture({
		base_path:'<%=ConfigBean.getStringValue("systenFolder")%>',
		limitSize:8,
		file_with_id:'<%=id%>',
		file_with_type:'<%=FileWithTypeKey.OCCUPANCY_MAIN %>'
	});
}

function initPage(){
	onLoadInitZebraTable();
	setTimeout(loadPhotosFromFileserv, 100);
	//loadPhotosFromFileserv();
}
</script>
	
	
</head>

<style type="text/css">

.wrap-search-input{
	position: relative;
	display: inline-block;
}

.icon-search-input{
	background: url(imgs/search.png) no-repeat center center;
	position: absolute;
	cursor: pointer;
	top: 0px;
	right: 0px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.wrap-search-input input:focus+.icon-search-input{
	border-color:#66afe9;
	outline:0;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)
}

</style>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="initPage()">
<fieldset style="border-top:0px solid #fff;width:96%;border-left:0px solid #fff;border-right:0px solid #fff;border-bottom:0px solid #fff;">
	
	<div id="tabs">
		<span style="font-size:13px; font-weight: bold;vertical-align:50%;">Entry ID:<%=id%></span>
		<div>
			<div id="trailor">
		 	  <br/>
		      <table style="width:100%; border:0; align:center; cellpadding:0; cellspacing:0;" class="zebraTable">
		      <tr> 
		      		<th width="5%" class="right-title" style="vertical-align: center;text-align: center;">Choose</th>
		      		<th width="8%" class="right-title" style="vertical-align: center;text-align: center;">Type</th>
		      		<th width="20%" class="right-title" style="vertical-align: center;text-align: center;">Equipment</th>
			        <th width="15%" class="right-title" style="vertical-align: center;text-align: center;">Resources</th>
<!-- 			        <th width="15%" class="right-title" style="vertical-align: center;text-align: center;">Rel Type</th> -->
					<th width="30%" class="right-title" style="vertical-align: center;text-align: center;">Carrier</th>
			        <th width="20%" class="right-title" style="vertical-align: center;text-align: center;">Seal</th>
	    	  </tr>
	    	  
			    <input type="hidden" id="entry_id" name="entry_id" value="<%=id%>" />
	    	    <%for(int i=0;i<resourcesByEntry.length;i++){ %>
			    	<tr>
				    		<%long equipment_type = resourcesByEntry[i].get("equipment_type",0l); %>
				    		<%if(equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER){ %>
				    			<input type="hidden" name="trailerno" class="trailerno" value="<%=resourcesByEntry[i].get("equipment_number","")%>" />
				    		<%} %>
				    		<input type="hidden" name="location" class="location" value="<%=resourcesByEntry[i].get("location","")%>" />
				    		<input type='hidden' name='resources_type' class='resources_type' equipment_type_msg="<%=resourcesByEntry[i].get("equipment_type_msg","")%>" locationValue="<%=resourcesByEntry[i].get("location","")%>" value="<%=resourcesByEntry[i].get("resources_type","")%>" />
			    		<td height="25" align="center" valign="middle" style='word-break:break-all;'>
			    			<input type="checkbox" checked="checked" <%if(equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR){ %> style="display: none;"  <%} %> OnClick='selectCheckbox(this);' seal="<%=resourcesByEntry[i].get("now_seal","")%>" check_in_entry_id="<%=resourcesByEntry[i].get("check_in_entry_id","")%>" isSearch="<%=resourcesByEntry[i].get("isSearch","")%>" entry_id="<%=resourcesByEntry[i].get("check_in_entry_id","")%>" equipment_type="<%=resourcesByEntry[i].get("equipment_type","")%>" resources_type="<%=resourcesByEntry[i].get("resources_type","")%>" resources_id="<%=resourcesByEntry[i].get("resources_id","")%>" equipment_id="<%=resourcesByEntry[i].get("equipment_id","")%>"  equipment_no="<%=resourcesByEntry[i].get("equipment_number","")%>" location="<%=resourcesByEntry[i].get("location","")%>"/>
			    			<%if(equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR){%> &nbsp; <%}%>
			    		</td>
			    		<td height="25" align="center" valign="middle" style='word-break:break-all;'>
			    			<%=resourcesByEntry[i].get("equipment_type_msg","&nbsp;")%>
			    		</td>
			    		<td height="25" align="center" valign="middle" style='word-break:break-all;'><%=resourcesByEntry[i].get("equipment_number","&nbsp;")%></td>
				    	<td height="25" align="center" valign="middle" style='word-break:break-all;'>
				    		<%=resourcesByEntry[i].get("location","&nbsp;")%>
				    	</td>
				    	<td height="25" align="center" valign="middle" style='word-break:break-all;'>
				    		<%=resourcesByEntry[i].get("carrier","&nbsp;")%>
				    	</td>
				    	<%int rel_type= resourcesByEntry[i].get("rel_type",0);%>
<!-- 				    	<td height="25" align="center" valign="middle" style='word-break:break-all;'> -->
<%-- 				    		<%=checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(rel_type) %> --%>
<!-- 				    	</td> -->
				    	<td height="25" align="center" valign="middle" style='word-break:break-all;'>
				    	<%String is_disabled = resourcesByEntry[i].get("is_disabled","");%>
				    	
				    	<%if("true".equals(is_disabled) || rel_type==CheckInMainDocumentsRelTypeKey.DELIVERY){ %>
				    		&nbsp;
				    	<%}else{ %>
				    		<input size="15" type="text" id="listseal_<%=resourcesByEntry[i].get("equipment_id","")%>" val_seal="false" name="sealList" equipment_id="<%=resourcesByEntry[i].get("equipment_id","")%>" onBlur="checkSeal(this,0)" onkeyup="daxie(this)" now_seal="<%=resourcesByEntry[i].get("now_seal","")%>" value="" onfocus="failure(this)"/>
				    		&nbsp;&nbsp;<input type="button"  name="no_seal" id="no_seal" value="NA" class="short-short-button-mod" onClick="checkSeal(this,1);" onmouseover="naIn()" onmouseout="naOut()" >
				    	<%} %>
				    		
				    	</td>
			    	</tr>
		    	<%} %>
		    	
		    </table>
	    </div>
	</div>
	<br />
	<div id="tabs1">
		<!-- 
		<div style="color:red;font-weight:bold; font-family:Verdana; float: left;padding-right:90px;">
		 	  <span id="cs">Trailer/CTNR:</span>&nbsp;&nbsp;
		 	  <input type="text" id="dropTrailerNo" name="dropTrailerNo" onkeyup="daxie(this)" onkeypress="if(event.keyCode==13){searchTrailerNo();return false;}"/>
		 	  <input class="button_long_search" id="search_btn" name="search_btn" type="button" value="Search"  onClick="searchTrailerNo();"/>
		 	</div>
		  -->
		<div style="height: 30px; border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;valign:middle;">
			<div class="wrap-search-input">
				<span id="cs" style="color:red;font-weight:bold;">Trailer/CTNR:</span>&nbsp;&nbsp;
				<input type="text" id="dropTrailerNo" name="dropTrailerNo" onkeyup="daxie(this)" onkeypress="if(event.keyCode==13){searchTrailerNo();return false;}" style="width: 178px; height: 24px; padding-right: 24px;" />
				<span class="icon-search-input" style="width: 24px; height: 24px;" id="search_btn" name="search_btn" onClick="searchTrailerNo();"></span>
			</div>
 	  	  <script>
		 	  $('#dropTrailerNo').focus();
		  </script>
	 	</div>
		<div id="search_div">
		 	  <br/>
		      <table style="width:100%; border:0; align:center; cellpadding:0; cellspacing:0;" class="zebraTable" id="search_tab">
		      <tr> 
		      		<th width="5%" class="right-title" style="vertical-align: center;text-align: center;">Choose</th>
		      		<th width="20%" class="right-title" style="vertical-align: center;text-align: center;">Equipment</th>
		      		<th width="10%" class="right-title" style="vertical-align: center;text-align: center;">Entry ID</th>
			        <th width="15%" class="right-title" style="vertical-align: center;text-align: center;">Resources</th>
			        <th width="30%" class="right-title" style="vertical-align: center;text-align: center;">Carrier</th>
			        <th width="20%" class="right-title" style="vertical-align: center;text-align: center;">Seal</th>
	    	  </tr>
		    	<tr id="temp">
		    		<td style="text-align:center;line-height:60px;height:60px;background:#E6F3C5;border:1px solid silver;" colspan="6"> No Data </td>
		    	</tr>
		    </table>
	    </div>
	</div>
	

<form id="fileform" method="post" action="<%=checkInFileUploadAction %>">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	    <td colspan="2" align="left" valign="top">
		    <fieldset style="border:2px #cccccc solid;padding:15px;margin:5px;width:93%; height:120px; overflow:auto">
				<legend style="font-size:14px;font-weight:normal;color:#999999;">
						Photo
				</legend>
				<div id="show"></div>
		    	<table>
					<tr>
						<input type="hidden" name="backurl" id="backurl" value="" />
		 				<input type="hidden" name="file_with_type" value="<%=FileWithTypeKey.OCCUPANCY_MAIN %>" />
		 				<input type="hidden" name="sn" id="sn" value="gate_check_out"/>
					 	<!-- <input type="hidden" name="file_names" id="file_names"/> -->
					 	<input type="hidden" name="path" value="check_in"/>
					 	<input type="hidden" name="dlo_id" value="<%=id%>"/>
					 	<input type="hidden" name="file_with_class" value="<%=FileWithCheckInClassKey.PhotoGateCheckOut%>"/>
					 	<td></td>
					 	<td id="over_file_td"></td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
	
</table>
</form>	
</div>
<!-- 打印的容器 -->
<div id="av" style="width:368px; display: none"></div>
</fieldset>	
</body>