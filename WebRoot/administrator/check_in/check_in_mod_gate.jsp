<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<%
 	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
 String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
 String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";

 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 long ps_id=adminLoggerBean.getPs_id();   
 long adminId=adminLoggerBean.getAdid(); 
 DBRow psRow=adminMgrZwb.findAdminPs(adminId);
 String ps_name	=psRow.get("title","") ;  
 long mainId=StringUtil.getLong(request,"main_id");
 DBRow mainRow=checkInMgrZwb.selectMainByEntryId(mainId);    					  //主单据
 DBRow[] detailRow=checkInMgrZwb.findAllLoadByMainId(mainRow.get("dlo_id",0l));    //子单据数据
 String bill_id="";
 String type="";
 if(detailRow!=null && detailRow.length>0){
 	if(detailRow[0].getString("load_number")!=""){
 		type="pickup";
 		bill_id=detailRow[0].getString("load_number");
 	}else if(detailRow[0].getString("ctn_number")!=""){
 		type="delivery";
 		bill_id=detailRow[0].getString("ctn_number");
 	}else if(detailRow[0].getString("bol_number")!=""){
 		type="delivery";
 		bill_id=detailRow[0].getString("bol_number");
 	}else if(detailRow[0].getString("others_number")!=""){
 		bill_id=detailRow[0].getString("others_number");
 	}
 }
 DBRow[] files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(mainId,FileWithTypeKey.OCCUPANCY_MAIN);
 %>	
<head>
	
<script type="text/javascript">
(function(){
	 $("#gps_tracker").blur(function(){
		 if( $("#gps_tracker").val()!=""){
			 $.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindAssetByGPSNumberAction.action',
				data:'gps_tracker='+$("#gps_tracker").val(),
				dataType:'json',
				type:'post',
				beforeSend:function(request){
			    
				},
				success:function(data){
					if(data.length==0){
						findGPSTip();
						$("#mark").val(1);
					}else{
						$("#mark").val(0);
						$("#gpsId").val(data.id);
					}
					
				},
				error:function(){
					alert("System error"); 
				}
			 });
		 }
	 });
})();

function findGPSTip(){
	$.artDialog({
	    content: 'GPS not found!do you want to add a new one?',
	    icon: 'question',
	    width: 240,
	    height: 70,
	    title:'Reminder',
	    okVal: 'Yes',
	    ok: function () {
	    	$("#gate_driver_first_name").focus();
	    },
	    cancelVal: 'No',
	    cancel: function(){
	    	$("#gps_tracker").val("");
	    	$("#gps_tracker").focus();
		}
	});
}

function search(flag){
		var search = "";
		if(flag==1){
			
			search = $("#delivery_search").serialize();
			
		}else if(flag==2){
			search = $("#pickup_search").serialize();
		}else{

			search = $("#both_search").serialize();
		}
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/getLoadingAction.action',
			data:search,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				
				if(data.length==0){
					alert("Record not found!");
					$("#driver_liscense").focus();
					$('#add_form')[0].reset();  
					$("#zone").val("N/A");
		//			$("#door").attr("disabled","disabled");
					if($("#tabs").tabs("option","selected")==0){
						if($('#number_type_delivery').val()==<%=GateCheckLoadingTypeKey.CTNR %>){
							$('#gate_container_no').val($('#search_number_delivery').val());
						}
					}else{
						if($('#number_type_both').val()==<%=GateCheckLoadingTypeKey.CTNR %>){
							$('#gate_container_no').val($('#search_number_both').val());
						}
					}	
				}else{
					$('#add_form')[0].reset();  
					if($("#number_type_delivery").val()==2){
						var id = $("#search_number_delivery").val();
						$("#gate_container_no").val(id);
					}
					$("#appointment").val(data[0].time.substring(0,16));
					$("#company_name").val(data[0].carrier);
					if(data[0].area_name){
						$("#zone").val(data[0].area_name);
						$("#zone_id").val(data[0].area_id);
					/**  if(flag==1||flag==2){
						findZoneReadyDoor($("#zone_id").val());
					  }*/
					}else{
						$("#zone").val("N/A");
					}
					$("#driver_liscense").focus();
				}
				
			},
			error:function(){
				alert("System error"); 
			}
		});
	
}
/**function findZoneReadyDoor(zoneId){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindZoneReadyDoorAction.action',
		data:'zoneId='+zoneId,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    
		},
		success:function(data){
			if(data.length>=2){
			
				alert("You have less than 2 vacant doors, please assign Spot.");
				
			}
			
		},
		error:function(){
			alert("System error"); 
		}
	});
}*/
function getYardNo(){
	var yc_no = $("#yc_no").val();
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/get_yard_no.html?storageId=<%=ps_id %>'+'&yc_no='+yc_no; 
	$.artDialog.open(uri , {title: "Spot",width:'600px',height:'520px', lock: true,opacity: 0.3,fixed: true});
	
}
//停车位回填
function setYardNo(yardId,yardNo){
	$("#yc_no").val(yardNo);
	$("#yc_id").val(yardId);
	$("#yc_no").focus();
	$('#door').val("");
	$('#checkInDoorId').val("");
}

//图片在线显示  		 
function showPictrueOnline(fileWithType,fileWithId ,currentName){
  var obj = {
  		file_with_type:fileWithType,
  		file_with_id : fileWithId,
  		current_name : currentName ,
  		cmd:"multiFile",
  		table:'file',
  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/check_in"
	}
  if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
}
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	$.artDialog.open(uri , {id:'file_up',title: 'Upload Photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   	}});
}
//在线获取
function onlineScanner(target){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target="+target; 
	$.artDialog.open(uri , {title: 'Online Photo',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target){
	    $("p.new").remove();
		if($.trim(fileNames).length > 0 ){
			$("input[name='file_names']").val(fileNames); 
			var array = fileNames.split(",");
			for(var index = 0 ; index < array.length ; index++ ){
				if(array[index].length>0){
					var a1 =  createA1(array[index]) ;
					var a2 =  createA2(array[index]) ;
					$("#add_file").append("<tr>"+a1+""+a2+"</tr>"); 
			
				}
			}
		} 
		$("#submitBtn").focus();	
}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var id = fileName.substring(0,fileName.length-4);
    var  a = "<p style='color:#439B89' class='new' ><a id='add"+id+"' href="+uri+" style='color:#439B89' >"+fileName+"</a>&nbsp;&nbsp;<a id='del"+id+"' href='javascript:deleteA(&quot;"+id+"&quot;)' style='color:red' >×</a></p>";
    return a ;
}

function deleteA(id){
	$("#add"+id).remove();
	$("#del"+id).remove();
	var file_names = $("input[name='file_names']").val(); 
	
	 var array = file_names.split(",");
	 var lastFile = "";
	 for(var index = 0 ; index < array.length ; index++ ){
			if(id==array[index].substring(0,array[index].length-4)){
				array[index]="";
				
			}
			lastFile += array[index];
			if(index!=array.length-1 && array[index]!=""){
				lastFile+=",";
			}
	 }
	 
	 $("input[name='file_names']").val(lastFile);
	
}
function closeWindow(){
	$.artDialog.close();
}
function doorEnter(event){
	 if(event.keyCode==13){
		 door();
		 return false;
	}
}
function door(){
	if( $("#door").val()!=""){
		 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxSelectDoorisExistByDoorNameAction.action',
			data:'doorName='+$("#door").val()+'&ps_id='+<%=ps_id%>,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				if($.isEmptyObject(data)){
					alert("Door not found");
					$("#door").val("");
					$("#door").focus();
				}else if(data.occupancy_status==1 || data.occupancy_status==2){
					alert("Door is occupied");
					$("#door").val("");
					$("#door").focus();
				}else{
					openDoor();
				}
				
			},
			error:function(){
				alert("System error"); 
			}
		 });
	 }else{
		 openDoor();
	 }
	
}
</script>
</head>
<body>

<form name="mod_form" id="mod_form" >
	 <table width="100%" height="70%" border="0" cellpadding="0" cellspacing="0">
	 <tr>
	    <td colspan="2" align="left" valign="top">	
			<fieldset style="border:2px #cccccc solid;padding:5px;margin:5px;width:98%;">
			<legend style="font-size:14px;font-weight:normal;color:#999999;">
		GateCheckIn
	</legend>			
	
	  <table width="98%" border="0" cellspacing="5" cellpadding="2">
		<tr>
			<td align="right"> Driver License</td>
			<td>
			   <input name="gate_driver_liscense" id="gate_driver_liscense" value="<%=mainRow.getString("gate_driver_liscense") %>"/>
			</td>
			 <td align="right"> License Plate</td>
			<td>
			   <input name="gate_liscense_plate" id="gate_liscense_plate" value="<%= mainRow.getString("gate_liscense_plate")%>"/>
			</td>
		</tr>
		<tr>
			<td align="right"> Trailer/CTNR</td>
			<td>
				<input name="gate_container_no" id="gate_container_no" value="<%= mainRow.getString("gate_container_no")%>"/>
			</td>
			<td align="right"> Carrier</td>
			<td>
			   <input name="company_name" id="company_name" value="<%= mainRow.getString("company_name")%>"/>
			</td>
		</tr>
		<tr></tr>
		<tr>
			<td align="right"> Spot</td>
			<td> 
				<%if(mainRow.get("yc_id",0l)!=0){ %>
					<%DBRow ycRow=checkInMgrZwb.findStorageYardControl(mainRow.get("yc_id",0l));%>				 
				    <input name="yc_no" id="yc_no"  onclick="getYardNo()" value="<%= ycRow.getString("yc_no")%>" onkeypress="if(event.keyCode==13){getYardNo();return false;}" />
				<%}else{%>
					<input name="yc_no" id="yc_no"  onclick="getYardNo()" value="" onkeypress="if(event.keyCode==13){getYardNo();return false;}" />
		    	<%}%>
		    </td>	
		    <td align="right"> Door</td>
			<td>
				<%if(detailRow!=null && detailRow.length>0){ %>
					<%if(detailRow[0].get("rl_id",0l)!=0){%>
					<%DBRow doorRow=checkInMgrZwb.findDoorById(detailRow[0].get("rl_id",0l));%>
						<input type="text"  id="door"  onclick="openDoor()" onkeypress="doorEnter(event)" value="<%=doorRow.getString("doorId") %>" />
					<%}else{%>
						<input type="text"  id="door"  onclick="openDoor()" onkeypress="doorEnter(event)" />
					<%}%>
				<%}else{%>
					<input type="text"  id="door"  onclick="openDoor()" onkeypress="doorEnter(event)" />
				<%} %>
		    </td>	
		    <td>
		        <input type="hidden" name="yc_id" id="yc_id"  value="<%=mainRow.get("yc_id",0l) %>" />
		        <%if(detailRow!=null && detailRow.length>0){ %>
					<input type="hidden" name="checkInDoorId"  id="checkInDoorId" value="<%=detailRow[0].get("rl_id",0l) %>"/>
				<%}else{ %>
					<input type="hidden" name="checkInDoorId"  id="checkInDoorId"/>
				<%} %>
			</td>
		</tr>
		<tr></tr>
		<tr>
		    <td align="right"> GPS</td>
			<td>
			    <% DBRow gpsRow=checkInMgrZwb.findAssetByGpsId(mainRow.get("gps_tracker",0l));%>
			    <%if(gpsRow!=null){ %>
				<input name="gps_tracker" id="gps_tracker"  value="<%=gpsRow.getString("imei") %>"/>
				<input type="hidden" name="gpsId" id="gpsId" value="<%=gpsRow.getString("id") %>"/>
				<%}else{ %>
				<input name="gps_tracker" id="gps_tracker"  />
				<%}%>
		    </td>
		</tr>
		<tr></tr>
		<tr>
			<td align="right"> Zone</td>
			<td>			
				<%if(detailRow!=null && detailRow.length>0){ %>
					<%if(detailRow[0].get("zone_id",0l)!=0){%>
					<input name="zone" id="zone" disabled="disabled" value="<%= detailRow[0].get("area_name","")%>" />
					<input type="hidden" name="zone_id" id="zone_id" disabled="disabled" value="<%=detailRow[0].get("zone_id",0l)%>"  />
					<%}else{%>
					<input name="zone" id="zone" disabled="disabled" value="N/A"  />
					<%}%>
				<%}else{%>
					<input name="zone" id="zone" disabled="disabled" value="N/A"  />
				<%} %>
			</td>
			<td align="right"> Appiontment</td>
			<td>
				<input  id="appointment_time" name="appointment_time" disabled="disabled"   value="<%=mainRow.getString("appointment_time") %>" />
			</td>
		</tr>
		<tr height="10px"></tr>
		<tr>
		    <td align="right"> First Name</td>
			<td>
				<input name="gate_driver_first_name" id="gate_driver_first_name" value="<%=mainRow.getString("gate_driver_first_name") %>"/>
			</td>
			<td align="right"> Last Name</td>
			<td>
				<input name="gate_driver_last_name" id="gate_driver_last_name" value="<%=mainRow.getString("gate_driver_last_name") %>"/>
			</td>
		</tr>
		<tr>
			<input type="hidden" name="add_ps_id" id="add_ps_id" value="<%=ps_id%>"/>	
			<input type="hidden" name="status" id="status" />
			<input type="hidden" name="type" id="type" />
			<input type="hidden" name="rel_type" id="rel_type" />
			<input type="hidden" name="add_checkin_number" id="add_checkin_number" />
			<input type="hidden" name="mark" id="mark" />
			
			<input type="hidden" name="appointment_time" id="appointment_time" />
			<input type="hidden" name="main_id" value="<%=mainId%>"/>
		</tr>
		
	  </table>
</fieldset>
  </td>
	</tr>
	<tr>
	    <td colspan="2" align="left" valign="top">
		   <fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:95%;">
			<legend style="font-size:14px;font-weight:normal;color:#999999;">
				Photo
			</legend>			
		      <form name="fileform" id="fileform">
			    	<table id="add_file">
			    		<tr>
			    			<td>
			    				<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="Upload Photo" />
			    			</td>
							<td>
								<input type="button" class="long-button" onclick="onlineScanner('onlineScanner');" value="Online Photo" />
							</td>
						</tr>
						<tr>
						</tr>
						<%
						 if(files != null && files.length > 0 ){
				      		for(DBRow file : files){
						%>
						<tr id="file_tr_<%=file.getString("file_id") %>">
							<td><%=file.getString("upload_time").substring(0,10) %></td>
							<td>
							 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
			                <%if(StringUtil.isPictureFile(file.getString("file_name"))){ %> 
					             <p>
					 	            <a style="color:#439B89" id="filename" href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.OCCUPANCY_MAIN%>','<%=mainId%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
					             </p>
					     <%}else{ %>
									<a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder= check_in'><%=file.getString("file_name") %></a>
						 <%}%>
							</td>
							<td width="30px"></td>
							<td>
								<a href="javascript:deleteFile('<%=file.get("file_id",0l) %>')" style='color:red' >×</a>
							</td>	
						 </tr>	 
						<%}}%>
						
						<tr>
			 				<input type="hidden" name="file_with_type" value="<%=FileWithTypeKey.OCCUPANCY_MAIN %>" />
			 				<input type="hidden" name="sn" id="sn" value="check_in"/>
						 	<input type="hidden" name="file_names" id="file_names"/>
						 	<input type="hidden" name="path" value="check_in"/>
						 	<input type="hidden" name="dlo_id" value="<%=mainId%>"/>
						</tr>
			        </table>
		     	</form>
	      </fieldset>
		</td>
	</tr>
	<tr>
	    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
	    <td width="49%" align="right" class="win-bottom-line">
			  <input type="button"  name="Submit2" value="Submit" id="submitBtn" class="normal-green" onClick="modCheckIn();" onkeypress="if(event.keyCode==13){modCheckIn();return false;}">
			 <input name="Submit2" type="button" class="normal-white" value="Cancel" onClick="closeWindow();" onkeypress="if(event.keyCode==13){closeWindow();return false;}">
		</td>
	 </tr>
</table>
</form>	
<!-- 打印的容器 -->
<div id="avp" style="width:368px; display: none"></div>	
</body>
<script>
function modCheckIn(){
	if($("#liscense_plate").val()==""){

		alert("Please enter License Plate");
		$("#liscense_plate").focus();
		return  false;
	}
	if($("#company_name").val()==""){

		alert("Please enter Carrier");
		$("#company_name").focus();
		return  false;
	}
	if($("#gate_container_no").val()==""){

		alert("Please enter Trailer/CTNR");
		$("#gate_container_no").focus();
		return  false;
	}
	if($("#yc_no").val()=="" && $("#door").val()=="" ){

		alert("Please enter Spot or Door");
		$("#yc_no").focus();
		return  false;
	}
	
	if($("#yc_no").val()!="" && $("#door").val()!=""){

		alert("You can't  assign  Spot & door at same time");
		$("#yc_no").focus();
		return  false;
	}
	else{	
		$("#appointment_time").val($("#appointment").val());
		if($("#gps_tracker").val()==""){
			$("#gpsId").val("");
		}
		if($("#yc_no").val()==""){
			$("#yc_id").val("");
		}
		if($("#door").val()==""){
			$("#checkInDoorId").val("");
		}
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/modCheckInAction.action',
			data:$("#mod_form").serialize()+'&'+$("#fileform").serialize(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				var biaoti='';
				var biaotival='';
				if(data.door_id==undefined){
					biaoti='Spot';
					biaotival=data.yc_name;
				}else{						
					biaoti='Loading Dock';
					biaotival=data.door_name;
				}
				var html='<table width="100%" border="0" cellspacing="0" cellpadding="0">'+
							'<tr>'+
						        '<td width="36%" rowspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;" >'+
						        	'<span style="font-weight: bold;">Entry ID</span>'+
						        '</td>'+
						        '<td height="36"  colspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;" >'+
						        	'<span style="font-weight: bold;">DATE</span>'+
						        '</td>'+
						        '<td width="45%" align="center" style="border-bottom:2px solid black;" >'+
						        	'<span style="font-weight: bold;" id="dateShow">'+data.gate_check_in_operate_time.substr(0,10)+'</span>'+
						        '</td>'+
					        '</tr>'+
					        '<tr>'+
						        '<td height="58" colspan="3" align="center" style="border-bottom:2px solid black;" >'+
						            '<span style="font-weight: bold;">'+
							            '<div align="center"><img src="/barbecue/barcode?data='+data.dlo_id+'&width=1&height=38&type=code39" /></div>'+
							            '<div align="center" style="font-size:9px;">'+data.dlo_id+'</div>'+   
						            '</span>'+
						        '</td>'+
					        '</tr>'+
						    '<tr>'+
						        '<td height="30" colspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;">'+
						        	biaoti+
						        '</td>'+
						        '<td height="30" colspan="2" align="center" style="border-bottom:2px solid black;">'+
						        	biaotival+
						        '</td>'+
						     '</tr>'+
						     '<tr>'+
						        '<td height="50" colspan="2" align="center" >&nbsp;</td>'+
						        '<td colspan="2" align="center" >&nbsp;</td>'+
						     '</tr>'+
						      '<tr>'+
						        '<td height="49" colspan="2" align="center">&nbsp;</td>'+
						        '<td colspan="2" align="center">&nbsp;</td>'+
						      '</tr>'+
						      '<tr>'+
						        '<td height="57" colspan="2" align="center">'+
									'&nbsp;'+
						        '</td>'+
						        '<td colspan="2">&nbsp;</td>'+
						      '</tr>'+
						      '<tr>'+
						        '<td height="83" colspan="4">&nbsp;</td>'+
						      '</tr>'+
						      '<tr>'+
						        '<td colspan="2">&nbsp;</td>'+
						        '<td width="5%">&nbsp;</td>'+
						        '<td>&nbsp;</td>'+
						      '</tr>'+
						'</table>';
					$('#avp').html(html);
					 print();
					 parent.location.reload();        
				     $.artDialog && $.artDialog.close();       	
			},
			error:function(){
				alert("System error"); 
			}
		});
	}
	
}

function dianji(ra){
	if(ra.value==0){
        $('#yc_no').css("display","inline");
        $('#door').css("display","none");
        $('#door').val("");
        $("#checkInDoorId").val("");
	}else{
		$('#yc_no').css("display","none");
	    $('#door').css("display","inline");
	    $('#yc_no').val("");
	    $('#yc_id').val("");
	}
}

function openDoor(){
	var doorId=$("#checkInDoorId").val();
	var zoneId=$("#zone_id").val();
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_in_door_list.html?zoneId='+zoneId+'&inputValue='+doorId; 
	$.artDialog.open(uri , {title: "Door and Location",width:'800px',height:'520px', lock: true,opacity: 0.3,fixed: true});
}

function getDoor(door,doorName){
	$('#door').val(doorName);
	$('#checkInDoorId').val(door);
	$("#door").focus();
	$("#yc_no").val("");
	$("#yc_id").val("");
}


function print(){
	 //获取打印机名字列表
	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
   //判断是否有该名字的打印机
	var printer = "LabelPrinter";
	var printerExist = "false";
	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){
			//判断该打印机里是否配置了纸张大小
			var paper="102X152";
		    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
		    var str=strResult.split(",");
		    var status=false;
		    for(var i=0;i<str.length;i++){
                  if(str[i]==paper){
                     status=true;
                  }
			}
		    if(status==true){
              	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Latel");
              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
			     visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",document.getElementById("avp").innerHTML);
				 visionariPrinter.SET_PRINT_COPIES(1);
			     //visionariPrinter.PREVIEW();
				 visionariPrinter.PRINT();
				// parent.location.reload();        
			    // $.artDialog && $.artDialog.close();
          }else{
          	$.artDialog.confirm("No match paper size in printer setting.....", function(){
   				 this.close();
       			}, function(){
   			});
          }	
	}else{
		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消
			visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Latel");
          	visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
            visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
			visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",document.getElementById("avp").innerHTML);
			visionariPrinter.SET_PRINT_COPIES(1);
			//visionariPrinter.PREVIEW();
			visionariPrinter.PRINT();
			//parent.location.reload();        
			//$.artDialog && $.artDialog.close();
		}	
	}
}

function timeOut(){
 	$('#gate_driver_liscense').focus();	
}
 window.setTimeout(timeOut,500);
</script>
