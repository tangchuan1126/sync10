<%@page import="com.cwc.app.key.OccupyTypeKey"%>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey"%>
<%@page import="com.cwc.app.key.SpaceRelationTypeKey"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInLiveLoadOrDropOffKey" class="com.cwc.app.key.CheckInLiveLoadOrDropOffKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="occupyTypeKey" class="com.cwc.app.key.OccupyTypeKey"/> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 long ps_id=adminLoggerBean.getPs_id();   
 long adminId=adminLoggerBean.getAdid(); 
 DBRow psRow=adminMgrZwb.findAdminPs(adminId);
 String ps_name	=psRow.get("title","") ; 
 int p =StringUtil.getInt(request,"p");
 long entryId = StringUtil.getLong(request,"entryId");
 String license_plate = StringUtil.getString(request,"license_plate");
 String trailerNo = StringUtil.getString(request,"trailerNo");
 String cmd = StringUtil.getString(request,"cmd");
 int resource_type =StringUtil.getInt(request,"resource_type");
 long resource_id = StringUtil.getLong(request,"resource_id");
 long dlo_id = StringUtil.getLong(request,"dlo_id");
 String resource_name = StringUtil.getString(request,"resource_name");
 long mainId = StringUtil.getLong(request,"mainId");
 long area_id = StringUtil.getLong(request,"area_id");
 int equipment_type =StringUtil.getInt(request,"equipment_type");
 DBRow[] rows = null;
 if(cmd.equals("filter")){
 	rows = checkInMgrZwb.findMainMesByCondition(ps_id, entryId, license_plate, trailerNo);
 }
 %>	
<head>
	 
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">
	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
	<style type="text/css" media="all">
	@import "../js/thickbox/thickbox.css";
	</style>
	<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
	<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
	<script src="../js/thickbox/global.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
	<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>


<script type="text/javascript">
var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
var updateDialog = null;
var addDialog = null;
api.button([
		{
			name: 'Add',
			callback: function () {
				modifyResourceEquipment();
				return false ;
			},
			focus:true,
			focus:"default"
		},
		{
			name: 'Cancel'
			
			
		}]
	);
jQuery(function($){
	 
	addAutoComplete($("#trailerNo"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInCTNRJSONAction.action?flag=1",
			"gate_container_no","gate_container_no");
	addAutoComplete($("#license_plate"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInLicensePlateJSONAction.action?flag=1",
			"gate_liscense_plate","gate_liscense_plate");
});
$(function(){
	$("#tabs").tabs({});
	$("input[type!='button']").keyup(function(){
		 this.value=this.value.toUpperCase();
		 this.value=trim(this.value);
		// alert(this.value);
		 
	 });
	$("#entryId").keyup(function(){
		if($("#entryId").val()!=""){
			$("#license_plate").val("");  
			$("#trailerNo").val("");
		}
		
	 });
	$("#license_plate").keyup(function(){
		if($("#license_plate").val()!=""){
			$("#entryId").val("");  
			$("#trailerNo").val("");
		}
		
    });
	$("#trailerNo").keyup(function(){
	   if($("#trailerNo").val()!=""){
		   $("#license_plate").val("");  
		   $("#entryId").val("");
	   }
	   
		
    });
	$("#entryId").focus();
	
});
function trim(str){
    return str.replace(/[ ]/g,"");  //去除字符算中的空格
}
</script>
</head>
<body>
	<fieldset style="border:2px #cccccc solid;padding:5px;margin:5px;width:95%;">
	  <legend style="font-size:14px;font-weight:normal;color:#999999;">
			<font style="color:red"><b><%=occupyTypeKey.getOccupyTypeKeyName(resource_type) %>[<%=resource_name%>]</b></font>
	  </legend>
	  <table width="98%" height="20%">
	  <tr width="100%" height="20%" valign="top">
		<td width="100%" height="20%" valign="top">
			<div class="demo" align="center" style="height: 90%">
	  		 	<div id="tabs" style="width: 98%;height: 90%">
					<ul>
						<li><a href="#tractor"><b>Tractor</b></a></li>
						<li><a href="#trailer"><b>Trailer</b></a></li>
					</ul>
				<div id="tractor">
					 <table width="98%" border="0" cellspacing="5" cellpadding="2">
						 <tr>
							<td align="left"> License Plate
							   <input  id="license_plate" value="<%=license_plate%>"/>
							   <input type="button" class="button_long_search" value="Search" onclick="filter();"/>
							</td>
			            </tr>
			            
		             </table>
				</div>
				<div id="trailer">
					 <table width="98%" border="0" cellspacing="5" cellpadding="2">
					      <tr>
								<td align="left"> CTNR
									<input id="trailerNo" value="<%=trailerNo%>">
									<input type="button" class="button_long_search" value="Search" onclick="filter();"/>
								</td>
							</tr>
					</table>
				</div>
			</div>
		</div>
	  </td>
	 </tr>
	</table>
	 
	</fieldset>
	<form name="filter_form" id="filter_form" >
		<input type="hidden" name="ps_id" id="ps_id" value="<%=ps_id%>"/>	
		<input type="hidden" name="entryId"  />
		<input type="hidden" name="license_plate"  />
		<input type="hidden" name="trailerNo"  />
		<input type="hidden" name="dlo_id"  />
		<input type="hidden" name="resource_id"  />
		<input type="hidden" name="resource_name"  />
		<input type="hidden" name="p"  />
		<input type="hidden" name="cmd" />
	</form>	
	
	<br/>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" id="mainData">
    	<tr > 
	       <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Spot</th>
	        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Spot Area</th>
	        <th width="25%" class="left-title" style="vertical-align:center;text-align:center;">Tractor / Trailer</th>
	        <th width="10%" class="left-title" style="vertical-align: center;text-align: center;">Option</th>
		</tr>
		
      	
      	
	</table>
	
</body>
<script>

function filter(){
	$("#mainData tr:not(:first)").remove();
	if( $("#entryId").val()=="" && $("#license_plate").val()=="" && $("#trailerNo").val()==""){
		alert("Please enter at least one");
		return false;
	}else{
		var equipment_type =0 ;
		var equipment_number="";
		if($("#tabs").tabs("option","selected")==0){
			equipment_type = <%=CheckInTractorOrTrailerTypeKey.TRACTOR %>;
			equipment_number = $("#license_plate").val();
		}else if($("#tabs").tabs("option","selected")==1){
			equipment_type = <%=CheckInTractorOrTrailerTypeKey.TRAILER %>;
			equipment_number =  $("#trailerNo").val();
		}
		
		$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindResourceByEquipmentAction.action',
		data:{
			ps_id:$("#ps_id").val(),
			equipment_number:equipment_number,
			p:<%=p%>,
			cmd:"filter"
		},
		dataType:'json',
		type:'post',
		success:function(data){
			var html="";
			if(data.length>0){
				for(var i=0;i<data.length;i++){
						html += '<tr id="equipment_id'+data[i].equipment_id+ '" height="50px" name="equipment_id" equipment_id="'+data[i].equipment_id+ '">';
						html+='<td id="text"  align="center" valign="middle" style="word-break:break-all" >';
						if(!(data[i].location==null)){	
							html+='<font><b>'+data[i].location+'</b></font>' ;

						}else{
							html+="&nbsp;";
						}
						html+='</td>';
						
						html+='<td id="text"  align="center" valign="middle" style="word-break:break-all" >';
						if(!(data[i].area_name==null)){
							html+=data[i].area_name;
						}else{
							html+="&nbsp;";
						}
						html+='</td>';
						
						html+='<td id="text"  align="center" valign="middle" style="word-break:break-all"  >';
						if(data[i].equipment_type==1){	
							html+='<font style="font-weight:bold;">Tractor : </font>'+data[i].equipment_number;
						}else if(data[i].equipment_type==2){	
							html+='<font style="font-weight:bold;">Trailer : </font>'+data[i].equipment_number;
						}else{
							html+="&nbsp;";
						}
						html+='</td>';
						
			
						html+='<td> <img id="'+data[i].equipment_id+ '" width="16" height="16" align="absmiddle" src="../imgs/standard_msg_error.gif"   onclick="clearEequipment('+data[i].equipment_id+ ');"></td></tr>';
					
						
			
				}

				$("#mainData").append(html);
			}else{
				checkin(equipment_type,equipment_number);
			}
			
			
			
		},
		error:function(){
			alert("system error");
		}
		});
	

	}
}
function checkin(equipment_type,equipment_number){
	$.artDialog({
	    content: 'Not fount,Do you want to checkIn ？',
	    icon: 'question',
	    lock:true,
	    width: 260,
	    height: 70,
	    title:'Reminder',
	    okVal: 'Yes',
	    ok: function () {
	    	createEntryId(equipment_type,equipment_number);
	    	
	    },
	    cancelVal: 'No',
	    cancel: function(){
	    	
		}
	});
}
function createEntryId(equipment_type,equipment_number){
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCheckInCreateEntryIdAction.action',
  		dataType:'json',
  		data:"ps_id="+<%=ps_id%>+"&equipment_number="+equipment_number+"&equipment_type="+equipment_type+"&resource_id="+<%=resource_id%>+"&resource_type="+<%=resource_type%>+"&flag=1",
  		success:function(data){
				if(data.id>0){
					alert("Create Success!");
					parent.location.reload();
					$.artDialog.close();
					
					
				}else{
					alert("Create Fail!");
				}
				
				
  			    	
  		}
    });
}
function modifyResourceEquipment(){
	var $equipment_id=$("tr[name='equipment_id']"); 
	var equipment_ids = "";
	for (var i = 0; i < $equipment_id.length; i++) {
		 var equipment_id=$($equipment_id[i]).attr("equipment_id");
		 equipment_ids += equipment_id+",";
	}
	equipment_ids=equipment_ids.substr(0,equipment_ids.length-1);
//	alert(equipment_ids);
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxModifyResourceEquipment.action',
  		dataType:'json',
 // 	   async:false,
  		data:'equipment_ids='+equipment_ids+"&resource_id="+<%=resource_id%>+"&resource_type="+<%=resource_type%>+"&adminId="+<%=adminId%>,
  		success:function(data){
				
  			parent.location.reload();
  			$.artDialog.close();
  		}
    });
		

}
function closeWindow(){
	$.artDialog.close();
}

function clearEequipment(equipment_id){
	$("#equipment_id"+equipment_id).remove();
}

function checkData(license_plate,trailerNo,mainId,doubt){
	
	parent.document.check_data.license_plate.value=license_plate;
	parent.document.check_data.trailerNo.value=trailerNo;
	parent.document.check_data.mainId.value=mainId;
	parent.document.check_data.doubt.value=doubt;
	parent.document.check_data.submit();
	$.artDialog.close();
}
function verifyData(license_plate,trailerNo,mainId,doubt_id){
	$.artDialog({
	    content: 'Do you want to closed other records ?',
	    icon: 'question',
	    lock:true,
	    width: 280,
	    height: 70,
	    title:'Reminder',
	    okVal: 'Yes',
	    ok: function () {
	    	checkData(license_plate,trailerNo,mainId,doubt_id);
			
	    },
	    cancelVal: 'No',
	    cancel: function(){
	    	parent.location.reload();
	    	$.artDialog.close();
		}
	});
}

</script>