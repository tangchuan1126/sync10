<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>

<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder")%>';
</script>
<html>
<head>
 <base href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/">
<!--  基本样式和javascript -->
<link type="text/css" href="comm.css" rel="stylesheet" />
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.autocomplete.css" />

<script src="js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="js/blockui/jquery.blockUI.233.js"></script>

<script>
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();
</script>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
   long adminId=adminLoggerBean.getAdid();
 //  DBRow[] titleRow=checkInMgrZwb.findTitleByAdminId(adminId);
   String jsonString = StringUtil.getString(request, "jsonString");
   String door_name=StringUtil.getString(request,"door_name");
   String type=StringUtil.getString(request,"type");
   String number=StringUtil.getString(request,"number");
   long entry_id=StringUtil.getLong(request,"entry_id");
//   DBRow row=checkInMgrZwb.findGateCheckInById(entry_id);	
   String out_seal = StringUtil.getString(request, "out_seal");
   int number_type=StringUtil.getInt(request,"number_type");
   long order_no=StringUtil.getLong(request, "order_no");
   long resources_type = StringUtil.getLong(request, "resources_type");//占用资源类型，door/spot
   String containerNo=StringUtil.getString(request,"containerNo");//货柜号
   String appointmentDate=StringUtil.getString(request,"appointmentDate");//预约时间 
   long receipt_no = StringUtil.getLong(request, "receipt_no");
//    System.out.print("containerNo="+containerNo+"        appointmentDate="+appointmentDate);
   String load="";
   String ctnr="";
   String bol="";
   if(number_type==ModuleKey.CHECK_IN_LOAD){
	   load=number;
   }
   if(number_type==ModuleKey.CHECK_IN_CTN){
	   ctnr=number;
   }
   if(number_type==ModuleKey.CHECK_IN_BOL){
	   bol=number;
   }
   String CompanyID = StringUtil.getString(request, "CompanyID");
   String CustomerID = StringUtil.getString(request, "CustomerID");
   
%>

<title>Check In</title>
</head>
<body onLoad="onLoadInitZebraTable()">
	<div id="tabs" >
	<ul>
		<%if((type.toUpperCase()).equals("PICKUP")){ %>
			<li><a href="#av1" onclick="loadingLableTemplate()" >BILL OF LADING</a></li>
			<li id="loadingwms"><a href="#av2" onclick="loadingWms()">Loading Ticket</a></li>
			<li><a href="#av4" onclick="counting_sheet()">Counting Sheet</a></li>
			<li><a href="#av5" onclick="packing_list()">Packing List</a></li>
		<%}else{ %>
			<li id="receiptwms"><a href="#av3" onclick="loadReceiptWms()">Receipts Ticket</a>
		<%} %>
	</ul>
	<%if((type.toUpperCase()).equals("PICKUP")){ %>
		<div id="av1">  
			<div id="lableTemplatelist" style="padding:0px 0px 0px 0px"></div>    
		</div> 
	    <div id="av2" align="center" style="overflow:auto;dispaly:none">
	        <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
				<input type="button" class="short-short-button-print"  value="Print"  onclick="printWms()"/>
			</div>
	    	<div id="loadQian" name="loadQian" style="width:368px; display:block; border:0px solid red;" align="center"  >         
	    	</div>
	    </div>
	    <div id="av4">
	    	<div id="counting_sheet"></div>
	    </div>
	    <div id="av5">
	    	<div id="packing_list"></div>
	    </div>
    <%}else{ %>
	    <div id="av3" align="center" style="overflow:auto;dispaly:none">
	        <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
				<input type="button" class="short-short-button-print"  value="Print"  onclick="printReceiptWms()"/>
			</div>
	    	<div id="receiptWms" name="receiptWms" style="width:368px; display:block; border:0px solid red;" align="center"  >       
	    	</div>
	    </div>
    <%} %>
</div>
</body>
</html>
<script>
	var isOpen=true;
	var isOpenCtnr=true;
	var iscoun=true;
	var isShipping=true;
	var isPacking=true;
	var isBill=true;
    function counting_sheet(){
    	 if(iscoun){
    		 if(<%=number_type%>==<%=ModuleKey.CHECK_IN_LOAD%>){
    			 $.blockUI({message: '<img src="js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
        		 $.ajax({
     				url:'<%=ConfigBean.getStringValue("systenFolder")%>check_in/a4_counting_sheet.html',
     				dataType:'html',
     				data:'jsonString=<%=jsonString%>&entryId=<%=entry_id%>&isprint=2&resources_type=<%=resources_type%>&containerNo=<%=containerNo%>&appointmentDate=<%=appointmentDate%>',
     				success:function(html){	
     					$.unblockUI();       //遮罩关闭
     					iscoun=false;
     					$('#counting_sheet').html(html);
     				}
     			});
    		 }else{
    			 $.blockUI({message: '<img src="js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
        		 $.ajax({
     				url:'<%=ConfigBean.getStringValue("systenFolder")%>check_in/a4_counting_sheet_order.html',
     				dataType:'html',
     				data:'jsonString=<%=jsonString%>&entryId=<%=entry_id%>&isprint=2&resources_type=<%=resources_type%>&containerNo=<%=containerNo%>&appointmentDate=<%=appointmentDate%>',
     				success:function(html){	
     					$.unblockUI();       //遮罩关闭
     					iscoun=false;
     					$('#counting_sheet').html(html);
     				}
     			});
    		 }
    		 
    	 }
     }
	
    
     function loadingLableTemplate(){
    	 if(isBill){
    		$.blockUI({message: '<img src="js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
			$.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_select_lableTemplate.html',
					dataType:'html',
					data:'jsonString=<%=jsonString%>&out_seal=<%=out_seal%>&entryId=<%=entry_id%>&container_no=<%=containerNo%>',
					success:function(html){	
						$.unblockUI();       //遮罩关闭
						isBill=false;
						$('#lableTemplatelist').html(html);
						showLoader(); //判断是不是显示Trailer Loader 和 Freight Counted的勾选框
					}
			});
    	}
	}
	 
	 function loadingWms(){
		 if(isOpen){
			 $.blockUI({message: '<img src="js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
			 $.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/print_order_wms.html',
					dataType:'html',
					async:'false',
					data:'entryId=<%=entry_id%>&number=<%=number%>&door_name=<%=door_name%>&CompanyID=<%=CompanyID%>&CustomerID=<%=CustomerID%>&out_seal=<%=out_seal%>&number_type=<%=number_type%>&order_no=<%=order_no%>&resources_type=<%=resources_type%>',
					success:function(html){	
						$.unblockUI();       //遮罩关闭
						isOpen=false;
						$('#loadQian').html(html);
					}
			});
		 }
	 }
	    
	 function loadReceiptWms(){
		 if(isOpenCtnr){
			 $.blockUI({message: '<img src="js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
			 $.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>check_in/print_receipts_wms_by_bol_container.html',
					dataType:'html',
					async:'false',
					data:'entryId=<%=entry_id%>&ctnr=<%=ctnr%>&bol=<%=bol%>&door_name=<%=door_name%>&CompanyID=<%=CompanyID%>&CustomerID=<%=CustomerID%>&out_seal=<%=out_seal%>&receipt_no=<%=receipt_no%>',
					success:function(html){	
						$.unblockUI();       //遮罩关闭
						isOpenCtnr=false;
						$('#receiptWms').html(html);
					}
			});
		 }
	 }
	 
	 function shipping_lable(){
		 if(isShipping){
			 $.blockUI({message: '<img src="js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
			 $.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>check_in/shipping_lable.html',
					dataType:'html',
					async:'false',
					data:'entryId=<%=entry_id%>&load=<%=load%>&CompanyID=<%=CompanyID%>&CustomerID=<%=CustomerID%>',
					success:function(html){	
						$.unblockUI();       //遮罩关闭
						isShipping=false;
						$('#shipping_lable').html(html);
					}
			});
		 }
	 }
	 
	 function packing_list(){
		 if(isPacking){
			 $.blockUI({message: '<img src="js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
			 $.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_select_packing_list_lableTemplate.html',
					dataType:'html',
					async:'false',
					data:'entryId=<%=entry_id%>&jsonString=<%=jsonString%>',
					success:function(html){	
						$.unblockUI();       //遮罩关闭
						isPacking=false;
						$('#packing_list').html(html);
					}
			});
		 }
	 }
    
    
</script>

<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
 $("#tabs").tabs("select",0);
 
</script>

