<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.OccupyStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="checkInDoorStatusTypeKey" class="com.cwc.app.key.OccupyStatusTypeKey"/>
<jsp:useBean id="checkInMainDocumentsRelTypeKey" class="com.cwc.app.key.CheckInMainDocumentsRelTypeKey"/>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/>
<jsp:useBean id="checkInTractorOrTrailerTypeKey" class="com.cwc.app.key.CheckInTractorOrTrailerTypeKey"/>
<jsp:useBean id="checkInLiveLoadOrDropOffKey" class="com.cwc.app.key.CheckInLiveLoadOrDropOffKey"/>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		long psId=adminLoggerBean.getPs_id();  
		long adminId=adminLoggerBean.getAdid();
		DBRow psRow=adminMgrZwb.findAdminPs(adminId);
		String ps_name	=psRow.get("title","") ;  
/**		PageCtrl pc = new PageCtrl();
		int p =StringUtil.getInt(request,"p");
		pc.setPageNo(p);
		pc.setPageSize(2);*/
		String backurl = StringUtil.getString(request,"backurl");
		DBRow[] rows = checkInMgrXj.verifySpot(psId,null);
	    long noPatrolCount = checkInMgrXj.findStoragePatrolTimeByPsId(psId);
%>
<title>Difference</title>
<script  language=javascript> 
function  showTip(tt,event) 
{ 
   var target = event.srcElement || event.target;  
   var  e    =  document.getElementById("pop"); 
   var  t    =  tt.offsetTop;          //TT控件的定位点高 
   var  h    =  tt.clientHeight;    //TT控件本身的高 
   var  l    =  tt.offsetLeft;        //TT控件的定位点宽 
   var  ttyp    =  tt.type;              //TT控件的类型 
   while  (tt  =  tt.offsetParent){t  +=  tt.offsetTop;  l  +=  tt.offsetLeft;} 
   e.style.top    =  (ttyp=="image")?  t  +  h  :  t  -  20;  //层的  Y  坐标 
   e.style.left  =  l  +  1;            //层的  X  坐标 
   e.style.display  =  "block";  //层显示 
   e.innerHTML  =  "<b>"+target.alt+"</b>"; 
   return(false);
} 
</script>
<script type="text/javascript">
$(function(){
	$("#note").blur(function(){
		$("input[id='note']").val($("#note").val());
	});
});

</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <tr>
	    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; Patrol » Verify EntryID</td>
	  </tr>
	</table>
	<br>
	<form name="form1" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCheckInPatrolApproveAction.action?ps_id=<%=psId%>" onSubmit="return checkForm(this)">
	<input type="hidden" name="pa_id" value="">
	<input type="hidden" name="len" value="<%=rows.length%>">
	<input type="hidden" name="backurl" value="<%=backurl%>">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
 	  	    <th width="5%" class="left-title" style="vertical-align: center;text-align: center;"><input type="checkbox" onclick="allCheck(this.checked)" checked="checked"/></th>  
	       	<th width="30%" class="left-title" style="vertical-align:center;text-align:center;">Tractor/Trailer</th>
	       	<th width="30%" class="left-title" style="vertical-align:center;text-align:center;">Time</th>
		</tr>
		<%if(rows != null && rows.length > 0){
      		for(DBRow row : rows){
      	%>
      	<tr height="50px">
 	      	<td  align="center" valign="middle" style='word-break:break-all'>
 	      
      	    	<input name="equipment_ids" type="checkbox" value="<%=row.getString("equipment_id")%>" checked="checked">
      	   
      	    </td>
      	    <td  id="text" align="center" valign="middle" style='word-break:break-all'>
      	     <fieldset style="width:90%; border:2px solid green;" >
	  				<legend>
	  					<span style="color:mediumseagreen;font-size:14px">
	  					<%
	  						 	if(!row.getString("check_in_entry_id").equals(""))
	  						 	{
	  						 		out.print("<font color='#f60'>E"+row.getString("check_in_entry_id")+"</font>");
	  						 	}
	  					 %> 
	  					
	  					 </span>
	  				</legend>
	  		  <table>
  			   		<div style="text-align:left;clear:both;"> 						
  						<div style="width:250px;float:left" align="right"> <font style="font-weight:bold;"><%=checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(row.getString("equipment_type")) %> :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(!row.getString("equipment_number").equals(""))
	  						 	{
	  						 		out.print("<font>"+row.getString("equipment_number")+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;"> 						
  						<div style="width:250px;float:left" align="right"> <font style="font-weight:bold;">Type :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(row.get("equipment_purpose",0)>0)
	  						 	{
	  						 		out.print("<font>"+checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(row.get("equipment_purpose",0))+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;"> 						
  						<div style="width:250px;float:left" align="right"> <font style="font-weight:bold;">Status :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(row.get("rel_type", 0)>0)
	  						 	{
	  						 		out.print("<font>"+checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(row.get("rel_type", 0))+":"+checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(row.get("equipment_status", 0))+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
		  	  </table>
		  	  </fieldset>
      	    </td>
      	    <td id="text" align="center" valign="middle" style='word-break:break-all'>
      	    <fieldset style="width:90%; border:2px solid blue;" >
	  				<legend>
	  				    <a style="color:#f60;font-size:14px;" target="_blank" >
				      		Time
	  					</a>
	  				</legend>
  			<table>
  			   		<div style="text-align:left;clear:both;"> 						
  						<div style="width:250px;float:left" align="right"> <font style="font-weight:bold;">GateCheckInTime :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(!row.getString("check_in_time").equals(""))
	  						 	{
	  						 		out.print("<font>"+DateUtil.showLocalparseDateTo24Hours(row.getString("check_in_time"),psId)+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;"> 						
  						<div style="width:250px;float:left" align="right"> <font style="font-weight:bold;">WindowCheckInTime :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(!row.getString("check_in_window_time").equals(""))
	  						 	{
	  						 		out.print("<font>"+DateUtil.showLocalparseDateTo24Hours(row.getString("check_in_window_time"),psId)+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;"> 						
  						<div style="width:250px;float:left" align="right"> <font style="font-weight:bold;">WarehouseCheckInTime :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(!row.getString("check_in_warehouse_time").equals(""))
	  						 	{
	  						 		out.print("<font>"+DateUtil.showLocalparseDateTo24Hours(row.getString("check_in_warehouse_time"),psId)+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
		  	  </table>
	  		</fieldset>
      	    </td>
      	   	
      	</tr>
		
      	<% 		
      		}
      		
      	}else{
      	%>
      		<tr style="background:#E6F3C5;">
   						<td colspan="10" style="text-align:center;height:120px;">No Data</td>
   		    </tr>
      	<% }%>
	</table>
   <br/>
  <table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td align="center" valign="middle" bgcolor="#eeeeee">
	  <input name="Submit" type="submit" style="vertical-align:top;" class="long-button-redtext" value="Submit Approval"  
	   alt="You must finish patrol all spot and door"  onmouseout="document.all.pop.style.display='none'" <%=rows.length==0?"hidden":""%>> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input name="Submit2" type="button" class="long-button" value="Back" onClick="history.back();">
    </td>
  </tr>
</table>
</form>
<div  id=pop  style=" 
   position:  absolute; 
   width:  290; 
   height:  20; 
   z-index:  99; 
   display:  none; 
   background-color:  #ffffdd;" > 
</div>
</body>
</html>
<script>
function checkForm(theForm)
{
	var equipment_ids = theForm.equipment_ids;
	
	var oneSelected = false;
	
	//先判断是否数组
	if(typeof equipment_ids.length == "undefined")
	{
			if (equipment_ids.checked)
			{
				oneSelected = true;
			}
			
	}
	else
	{
		for (i=0; i<equipment_ids.length; i++)
		{
			if (equipment_ids[i].checked)
			{
				oneSelected = true;
			}
			
		}
	}
	
	if (!oneSelected)
	{
		alert("choice at least one");
		return(false);
	}
	
	if(<%=noPatrolCount%>!=0){
		alert("You must finish patrol all spot and door.");
		return(false);
	}
	
	return(true);
}
function allCheck(all_or_no)
{
	if(all_or_no)
	{
		$("[name$='equipment_ids']").attr("checked",true);
	}
	else
	{
		$("[name$='equipment_ids']").attr("checked",false);
	}
		
}
</script>
