<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.ModuleKey"%>
<html>
<head>

<%
	long infoId=StringUtil.getLong(request,"main_id");
	 DBRow[] rows=checkInMgrZwb.selectDoorByInfoId(infoId);
	 
	 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	 long adminId=adminLoggerBean.getAdid();
	 String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/check_in/check_in_warehouse.html?infoId="+infoId;
	 String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
	 String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	 String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	 String checkInFileUploadAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/checkin/checkInFileUploadAction.action";
	 DBRow[] files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(infoId,FileWithTypeKey.OCCUPANCY_MAIN);
%>


<title>Check In</title>
</head>
<body>
    <div style="background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;border:2px solid #dddddd">
    	<form action="" id="myForm" name="myForm">
    		Entry ID：<%if(infoId==0){%><%}else{%><%=infoId%><%}%>&nbsp;&nbsp;&nbsp;&nbsp;
    	<!--
    		<input type="text" name="infoId" value="<%if(infoId==0){%><%}else{%><%=infoId%><%}%>"/> 
    		<input type="button" value="Confirm" class="long-button-add" onclick="tijiao()"/>
    	-->	
   		</form>	
	</div> 
	<div style="background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;border:2px solid #dddddd;margin-top: 2px;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td width="50%">
		       	
	    		<div style="background-color:white; padding-left: 5px;border-left:2px solid #dddddd;border-top:2px solid #dddddd;border-right:2px solid #dddddd">
	    		    Email:<input name="" type="checkbox" value="" checked="checked">&nbsp;
	    			Message:<input name="" type="checkbox" value="" checked="checked">&nbsp;
	    			Browser:<input name="" type="checkbox" value="" checked="checked"><br/>
	    			<span>Operator：</span><input type="text" size="18"  onclick="tongzhi()" id="userName"/>
	    			<input type="hidden" value="" id="ids"/>
	    			<input class="long-button" type="button" onclick="addTz()" value="Assign">
	    		</div>
	    		<div style="background-color:white;height:377px; overflow:auto;border:2px solid #dddddd">
	       			<div id="loadingmask">
				    	 <div class="demo" id="demo_1">
							<ul>
						        <%for(int i=0;i<rows.length;i++){ %>
								<li id="door#door#<%=rows[i].get("rl_id",0l)%>#<%=rows[i].getString("doorId")%>" rel="storage" class="open"><a href="#" style="font-weight:bold; color:green"><ins>&nbsp;</ins>DOOR:<%=rows[i].getString("doorId")%></a>
									<ul>
								        <%DBRow[] lotRow=checkInMgrZwb.selectLotNumberByDoorId(rows[i].get("rl_id",0l),infoId); %>
			    	       				<%if(lotRow!=null && lotRow.length>0){ %>
				    	       		        <%for(int a=0;a<lotRow.length;a++){%>
				    	       		            <%if(!lotRow[a].get("load_number","").equals("")){ %>
												<li id="load#<%=rows[i].get("rl_id",0l)%>#<%=lotRow[a].get("dlo_detail_id",0l)%>#<%=rows[i].getString("doorId")%>" rel="load" ><a href="#" style="color:red"><ins>&nbsp;</ins>LOAD#:<%=lotRow[a].get("load_number","")%></a>
													<ul>
													    <%DBRow[] ordRow=checkInMgrZwb.findOrderByLoadId(lotRow[a].getString("load_number"));%>
													    <%for(int b=0;b<ordRow.length;b++){ %>
														<li id="order#<%=rows[i].get("rl_id",0l)%>#<%=ordRow[b].get("orderno",0l)%>#<%=rows[i].getString("doorId")%>" rel="order"><a href="#" style="color:#3366CC"><ins>&nbsp;</ins>ORDER#:<%=ordRow[b].get("orderno",0l)%></a></li>
														<%} %>
													</ul>
												</li>
												<%} %>												
										    <%}%>
										    <%for(int n=0;n<lotRow.length;n++){ %>
										        <%if(!lotRow[n].get("ctn_number","").equals("")){ %>
										        	<li id="ctn#<%=rows[i].get("rl_id",0l)%>#<%=lotRow[n].get("dlo_detail_id",0l)%>#<%=rows[i].getString("doorId")%>" rel="load" >
										        		<a href="#" style="color:red"><ins>&nbsp;</ins>CTNR#:<%=lotRow[n].get("ctn_number","")%></a>
										        	</li>
										        <%}%>										    	
										    <%} %>
										    <%for(int p=0;p<lotRow.length;p++){ %>
										        <%if(!lotRow[p].get("bol_number","").equals("")){ %>
										        	<li id="bol#<%=rows[i].get("rl_id",0l)%>#<%=lotRow[p].get("dlo_detail_id",0l)%>#<%=rows[i].getString("doorId")%>" rel="load" >
										        		<a href="#" style="color:red"><ins>&nbsp;</ins>BOL#:<%=lotRow[p].get("bol_number","")%></a>
										        	</li>
										        <%}%>										    	
										    <%} %>
										    <%for(int k=0;k<lotRow.length;k++){ %>
										        <%if(!lotRow[k].get("others_number","").equals("")){ %>
										        	<li id="other#<%=rows[i].get("rl_id",0l)%>#<%=lotRow[k].get("dlo_detail_id",0l)%>#<%=rows[i].getString("doorId")%>" rel="load" >
										        		<a href="#" style="color:red"><ins>&nbsp;</ins>OTHER#:<%=lotRow[k].get("others_number","")%></a>
										        	</li>
										        <%}%>										    	
										    <%} %>
			    	       		        <%}%>
									</ul>
								</li>
					            <%}%>
							</ul>
						</div>
					</div>
	    		</div>
		   
		    </td>
		    <td>
		    	<div id="container" style="margin-left:5px;background-color:white;height:377px;border:2px solid #dddddd;overflow:auto">
					
				</div>
				<div align="right" style="margin-top:5px;background-color:white;border:2px solid #dddddd;margin-left:5px">
					<input class="normal-green" type="button" value="Confirm" onclick="checkInWahouse()">
				</div>
		    </td>
		  </tr>
		</table>
	</div>
</body>
</html>
<script type="text/javascript" class="source">
	$(function () { 
		$("#demo_1").tree({
			ui : {
				theme_name : "checkbox"
			},
			rules:{
				// only nodes of type root can be top level nodes
				valid_children : [ "storage" ],
				multiple:true,	//支持多选
				drag_copy:false,//禁止拷贝

			},		
			types:{
				// all node types inherit the "default" node type
				"default" : {
					deletable : false,
					renameable : false
				},
				"storage" : {
					draggable : false,
					valid_children : [ "load" ],
				},
				"load" : {
					draggable : false,
					valid_children : [ "order" ],
				},
				"order" : {
					valid_children : "none",
					max_depth :-1,
				}
			},
			plugins:{
				checkbox : {}
					}
		});
	});

	function tijiao(){         //根据单号加载树
		$('#myForm').submit();
	}

	function tongzhi(){
		 var ids=$('#ids').val();
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/check_in/check_in_admin_user.html"; 
		 var option = {
				 single_check:0, 					// 1表示的 单选
				 not_check_user:"",					//某些人不 会被选中的
				 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
				 ps_id:'0',					        //所属仓库
				 user_ids:ids,                      //需要回显的UserId
				 handle_method:'setSelectAdminUsers'
		 };
		 uri  = uri+"?"+jQuery.param(option);
		 $.artDialog.open(uri , {title: 'User List',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}

	function setParentUserShow(ids,names,inputId){ //通知回显
		$('#userName').val(names);
		$('#ids').val(ids);		
	}

	function addTz(){      //添加通知
		var userNameVal=$('#userName').val();
		var page=0;
		var mail=0;
		var sms=0;
		//判断邮件 短信 页面是否选中
        if($('#youjian').attr('checked')){
        	 mail=1;
	    }
		if($('#duanxin').attr('checked')){
			 sms=1;
		}
		if($('#yemian').attr('checked')){
			 page=1;
		}	 
	                           
		
		if(userNameVal==''){
			alert("Please choose operator");
			return false;
		}
		var checkedNode = $.tree.plugins.checkbox.get_checked($.tree.reference("#demo_1"));
		if(checkedNode.length==0){
			alert("Please choose order");
			return false;
		}
		var ids=$('#ids').val();
		var names=$('#userName').val();
		var html='';
		//debugger;
		for(var i=0;i<checkedNode.length;i++){
			//alert("通知："+names+",["+checkedNode[i].id+"]");
			var idStr=checkedNode[i].id;
			var items=idStr.split("#");
		//	for(var a=0;a<items.length;a++){
		//		items[a]
		//	}
            var doorId='';
            var loadId='';
            var ctnId='';
            var orderId='';
            var bolId='';
            var otherId='';
            var doorName='';
            
            if(items[0]=='door'){
            	doorId=items[2];
            	doorName=items[3];
            }else if(items[0]=='load'){
            	doorId=items[1];
            	loadId=items[2];
            	doorName=items[3];
            }else if(items[0]=='ctn'){
            	doorId=items[1];
            	ctnId=items[2];
            	doorName=items[3];	
            }else if(items[0]=='order'){
            	doorId=items[1];
            	orderId=items[2];
            	doorName=items[3];
            }else if(items[0]=='bol'){
            	doorId=items[1];
            	bolId=items[2];
            	doorName=items[3];
            }else if(items[0]=='other'){
            	doorId=items[1];
            	otherId=items[2];
            	doorName=items[3];
            }
	        var number_id=0;
	        var type=0;
			if(items[0]!='door'){
				html+='<div><span style="font-size:12px;color:green">notify:</span>'+names+'</div>';
				if(loadId!=''){
					html+='<div style="border-bottom:1px dashed red;"><span style="color:#C90">LOAD:</span><span style="color:red">'+loadId+'</span><br/>From Door:<span style="color:blue">'+doorName+'</span></div>';
					number_id=loadId;
					type=<%=ModuleKey.CHECK_IN_LOAD%>;
				}else if(ctnId!=''){
					html+='<div style="border-bottom:1px dashed red;"><span style="color:#C90">CTNR:</span><span style="color:red">'+ctnId+'</span><br/>From Door:<span style="color:blue">'+doorName+'</span></div>';
					number_id=ctnId;
					type=<%=ModuleKey.CHECK_IN_CTN%>;
				}else if(orderId!=''){
					html+='<div style="border-bottom:1px dashed red;"><span style="color:#C90">ORDER:</span><span style="color:red">'+orderId+'</span><br/>From Door:<span style="color:blue">'+doorName+'</span></div>';
					number_id=orderId;
					type=<%=ModuleKey.CHECK_IN_ORDER%>;
				}else if(bolId!=''){
					html+='<div style="border-bottom:1px dashed red;"><span style="color:#C90">BOL:</span><span style="color:red">'+bolId+'</span><br/>From Door:<span style="color:blue">'+doorName+'</span></div>';
					number_id=bolId;
					type=<%=ModuleKey.CHECK_IN_BOL%>;
				}else if(otherId!=''){
					html+='<div style="border-bottom:1px dashed red;"><span style="color:#C90">OTHER:</span><span style="color:red">'+otherId+'</span><br/>From Door:<span style="color:blue">'+doorName+'</span></div>';
					number_id=otherId;
					type=<%=ModuleKey.CHECK_IN_ORTHERS%>;
				}
			}     
		}
		if(items[0]=='door'){

		}else{
			$(html).appendTo($('#container'));
		}	

		//调通知
		 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCheckInWarehouseNotice.action',
			dataType:'json',
			data:'number_id='+number_id+'&door_id='+doorId+'&page='+page+'&mail='+mail+'&sms='+sms+'&ids='+ids+'&type='+type,
			success:function(data){	 
			
			}
		});
		

		//重置
		chongzhi();
	}

	function chongzhi(){  //重置树
		$("[rel='storage']").each(function(){jQuery.tree.plugins.checkbox.uncheck(this);});
	}
  

	//文件上传
	function uploadFile(_target){
	    var targetNode = $("#"+_target);
	    var fileNames = $("input[name='file_names']").val();
	    var obj  = {
		     reg:"picture_office",
		     limitSize:8,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames && fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: 'Upload photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
	    		 close:function(){
						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
						 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
	   		 }});
	}
	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,target){
		if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
	        $("input[name='file_names']").val(fileNames);
	        var fileform = $("#fileform");
	        $("#backurl").val("<%= backurl%>");
			fileform.submit();
	       
		}
	}

	function closeWindow(){
		$.artDialog.close();
	}
	//图片在线显示  		 
	function showPictrueOnline(fileWithType,fileWithId ,currentName){
	  var obj = {
	  		file_with_type:fileWithType,
	  		file_with_id : fileWithId,
	  		current_name : currentName ,
	  		cmd:"multiFile",
	  		table:'file',
	  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/check_in"
		}
	  if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}
	//删除图片
	function deleteFile(file_id){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'file',file_id:file_id,folder:'check_in',pk:'file_id'},
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}else{
					showMessage("System error,please try later","error");
				}
				
			},
			error:function(){
				showMessage("System error,please try later","error");
			}
		});
	}
	
	(function(){ //加载 window时 的通知数据 回显
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxAllLoadByInfoIdAction.action',
			dataType:'json',
			data:{mainId:<%=infoId%>},
			success:function(data){	
				for(var i=0;i<data.length;i++){			
					if(data[i].load_number!=undefined){
						huixian(data[i].dlo_detail_id,"load");
					}else if(data[i].ctn_number!=undefined){
						huixian(data[i].dlo_detail_id,"ctn");
					}else if(data[i].bol_number!=undefined){
						huixian(data[i].dlo_detail_id,"bol");
					}else if(data[i].others_number!=undefined){
						huixian(data[i].dlo_detail_id,"other");
					}
				}
			}
		});
	})();	

	function huixian(id,type){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxLoadNoticeDataAction.action',
			dataType:'json',
			data:'id='+id+'&type='+type,
			success:function(data){	
                for(var i=0;i<data.length;i++){
                  var html='<div><span style="color:red">Notified:</span>'+data[i].employe_name+'</div>'+
                  		   '<div style="border-bottom:1px dashed red;><span style="color:green"></span>'+data[i].schedule_detail+'</div>';
                  $(html).appendTo($('#container'));
                }
				
			}
		});
	}

	function checkInWahouse(){
        $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxAddWahouseAction.action',
			dataType:'json',
			data:'id=<%=infoId%>',
			success:function(data){	          
				parent.location.reload();        
			    $.artDialog && $.artDialog.close();
			}
		});
	}
	
</script>
