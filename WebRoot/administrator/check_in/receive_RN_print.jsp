<%@page import="com.cwc.app.key.RelativeFileKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@ include file="../../include.jsp"%>
<%
	long receipt_no=StringUtil.getLong(request, "receipt_no");
	String company_id=StringUtil.getString(request, "company_id");	
	String date=StringUtil.getString(request, "date");
	DBRow[] rows=checkInMgrZwb.queryLinesByRNForTicketPrint(receipt_no, company_id);
			
	long adid = StringUtil.getLong(request, "adid");
    	String printName=StringUtil.getString(request,"print_name");
	boolean isPrint = StringUtil.getInt(request, "isprint") == 1;

%>
<html>
<head>
 <!--  基本样式和javascript -->
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
 
<%if(!isPrint){ %>
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<!-- <link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" /> -->
<!-- <script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script> -->
<!-- <script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script> -->
<!-- <script src="js/art/plugins/iframeTools.js" type="text/javascript"></script> -->
<!-- jquery UI 加上 autocomplete -->
<!-- <script type="text/javascript" src="js/autocomplete/jquery.ui.core.js"></script> -->
<!-- <script type="text/javascript" src="js/autocomplete/jquery.ui.widget.js"></script> -->
<!-- <script type="text/javascript" src="js/autocomplete/jquery.ui.position.js"></script> -->
<!-- <script type="text/javascript" src="js/autocomplete/jquery.ui.tabs.js"></script> -->
<!-- <script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.js'></script> -->
<!-- <script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.html.js'></script> -->
<!-- <script type='text/javascript' src='js/autocomplete/autocomplete.js'></script> -->
<!-- <link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.core.css" /> -->
<!-- <link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.theme.css" /> -->
<!-- <link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.tabs.css" /> -->
<!-- <link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.autocomplete.css" /> -->
<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>

<%} %>
<title>RN</title>
<script type="text/javascript">

//web端打印方法
function print(){
	var imgSrc = "<%=ConfigBean.getStringValue("systenFolder") %>receive/oso_logo_1.png";
	var printAll = $('div[name="printAll"]');
	for(var i=0;i<printAll.length;i++){
	 	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Load Label");
 	 	 visionariPrinter.SET_PRINTER_INDEXA ("LabelPrinter");//指定打印机打印 
		 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
	     visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
	     visionariPrinter.ADD_PRINT_TABLE("5%",7,"100%","95%",$(printAll[i]).html());
		 visionariPrinter.SET_PRINT_COPIES(1);//设置打印份数
		
		 visionariPrinter.ADD_PRINT_TEXT("1%","4.5cm","100%","100%","PAGE:#/&"); //页码
		 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
		 visionariPrinter.ADD_PRINT_IMAGE("0.5%","8.5cm","35px","30px","<img border='0' src='"+imgSrc+"' />");
		 visionariPrinter.SET_PRINT_STYLEA(0,"Stretch",2);
		 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页码
 		 //visionariPrinter.PREVIEW();
	 	 visionariPrinter.PRINT();
	}
	
}


</script>
</head>
<body>
<%if(!isPrint && rows.length > 0){ %>
<div style="width:102mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;">
	<input class="short-short-button-print" type="button" onclick="print()" value="Print">
</div>
<%} %>
<br>
<%if(rows.length > 0){ %>
<!-- 第一个 -->
<div align="center">
	<div id="printAll" name="printAll" style="width:368px; display:block; border:1px solid red">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial;">
	    	 <tr>
	            <td height="55" colspan="2" valign="middle" style=" font-size:35px;">
	       	   		<b><span>RN # </span><%=rows[0].get("receipt_no") %>&nbsp;</b>  
	            </td>
	      	</tr>
	        <tr>
	          <td colspan="2" valign="bottom">
	       	   		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                  <tr style="line-height:33px;vertical-align:bottom;">
	                    <td width="61%">
	           		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                          <tr>
	                            <td width="110px;" align="right" style="font-size:17px;"><b>ENTRY ID :</b></td>
	                            <td  style="font-size:17px;"><b> &nbsp;<%=rows[0].get("check_in_entry_id") %>&nbsp;</b></td>
	                          </tr>
	                        </table>
	                    </td>
	                    <td width="39%" align="center" style="font-size:28px;"><b><%=date %></b></td>
	                  </tr>
	                </table>
	            </td>
     		</tr>
           <tr>
	          <td colspan="2" valign="bottom" >
	       	   		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                    <td width="110px;" align="right" style="font-size:17px;"><b>CTNR :</b></td>
	                    <td style="font-size:17px;"><b> &nbsp;<%=rows[0].get("equipment_number") %>&nbsp;</b></td>
	                  </tr>
	                </table>
	            </td>
	       </tr>
           <tr>
            <td colspan="2" valign="bottom" style="border-bottom:2px solid black;">
   		    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="110px;" height="31" align="right" valign="top" style="font-size:17px;"><b>BOL :</b></td>
                    <td valign="top"  style="font-size:17px;"><b> &nbsp;<%=rows[0].get("bol_no") %>&nbsp;</b></td>
                  </tr>
                </table>
            </td>
           </tr>
           
           <%
           		for(DBRow row: rows){
           %>
           		<tr>
		            <td width="55%" height="20px;" valign="bottom" style="border-bottom:1px black solid;font-size:14px;font-weight:bold;">
		           	 <span style="margin-left:20px;">Item # <%=row.get("item_id") %>&nbsp;</span></td>
		            <td width="45%" align="left" valign="bottom"  style="border-bottom:1px black solid;font-size:14px;font-weight:bold;">
		            	 <span >Good QTY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <%=row.get("goods_total", 0) %>&nbsp;</span> </td>
		        </tr>
		        <tr>
		            <td width="55%" height="20px;" valign="bottom" style="border-bottom:1px black solid;font-size:14px;font-weight:bold;">
		           		<span style="margin-left:20px;">LOT &nbsp;# <%=row.get("lot_no") %>&nbsp;</span></td>
		            <td width="45%" align="left" valign="bottom"  style="border-bottom:1px black solid;font-size:14px;font-weight:bold;">
		            	<span>Damage QTY&nbsp;: </span><%=row.get("damage_total", 0) %>&nbsp;</td>
		        </tr>
		        
		        <%
		        	DBRow[] plateRows = checkInMgrZwb.queryPalletsInfoByLineIdForTicketPrint(row.get("receipt_line_id", 0L));
	           		for(DBRow plateRow: plateRows){
	           %>
		        <tr>
		            <td height="23" colspan="2"  style="border-bottom:1px #CCC dashed;">
		       	    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                  <tr>
		                    <td width="80%" height="40px;" valign="middle" style="font-size:8px;">
		                   		<span style="margin-left:10px;font-weight:bold;font-family:Verdana;">
		                   		<% if(plateRow.get("goods_type", 0)==2){ %>
						   			D
						   		<% }else{%>
							   		T
						   		<% }%>
		                   		&nbsp;Pallet #  
		                   		</span><img src="/barbecue/barcode?data=94<%=StringUtil.formatNumber("#000000000000",plateRow.get("container", 0l)) %>&width=1&height=20&type=code39" />
		                        <br /><span style="margin-left:110px;font-family:Verdana;font-weight:bold;"><%=plateRow.get("container", 0l) %>&nbsp;</span>
		                    </td>
		                    <td width="20%" style="font-size:8px;font-weight:bold; font-family:Verdana;"><span>QTY : <%=plateRow.get("normal_qty", 0) + plateRow.get("damage_qty", 0) %>&nbsp;</span></td>
		                  </tr>
		                </table>
		            </td>
		        </tr> 
           <%
           		}
           	}
           %>
           

      </table>
	</div>
</div>
<%} %>
</body>
</html>
<script>
function supportAndroidprint(){
	//获取打印机名字列表
   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
   	 //判断是否有该名字的打印机
   	var printer = "<%=printName%>";
   	var printerExist = "false";
   	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){
		return androidIsPrint(containPrinter);
	}else{
		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消
			return androidIsPrint(containPrinter);
		}
	}
}

function androidIsPrint(containPrinter){
	//alert(containPrinter);
	var printAll = $('div[name="printAll"]');
	var flag = true ;
	var imgSrc = "<%=ConfigBean.getStringValue("systenFolder") %>receive/oso_logo_1.png";

	for(var i=0;i<printAll.length;i++){
        
     		 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Load Label");
		 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
		 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
	    	 visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
	     	 visionariPrinter.ADD_PRINT_TABLE("5%",7,"100%","95%",$(printAll[i]).html());
		 visionariPrinter.SET_PRINT_COPIES(1);//设置打印份数
		
		 visionariPrinter.ADD_PRINT_TEXT("2.5%","4.5cm","100%","100%","PAGE:#/&"); //页码
		 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
		 visionariPrinter.ADD_PRINT_IMAGE("1%","8.5cm","35px","30px","<img border='0' src='"+imgSrc+"' />");
		 //visionariPrinter.SET_PRINT_STYLEA(0,"TransColor","#FFFFFF");
		 visionariPrinter.SET_PRINT_STYLEA(0,"Stretch",2);
		 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页码
// 		 visionariPrinter.PREVIEW();
        
 	 	flag = flag && visionariPrinter.PRINT();
 	 
	}
	return flag ;
}


</script>

