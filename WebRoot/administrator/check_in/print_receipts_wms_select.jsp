<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>

<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<%
String type = StringUtil.getString(request, "type");
String number = StringUtil.getString(request, "number");
String ctnNo = "";
String bolNo = "";
if("BOL".equals(type.toUpperCase()))
{
	bolNo = number;
}
else if("CTNR".equals(type.toUpperCase()))
{
	ctnNo = number;
}

long entry_id = StringUtil.getLong(request, "entry_id");
String door_name = StringUtil.getString(request, "door_name");
//System.out.println("111:"+type+","+number+","+entry_id+","+door_name);
//DBRow[] rows = sqlServerMgr.findReceiptsByBolNoOrContainerNo(bolNo, ctnNo, "", "");
//System.out.println("r:"+rows.length);
%>
<title>select load</title>
<script type="text/javascript">
$(function(){
	var para = 'entry_id=<%=entry_id%>&type=<%=type%>&door_name=<%=door_name%>&number=<%=number%>';
	 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/print_receipts_wms_select_data.html',
			dataType:'html',
			async:false,
			data:para,
			success:function(html){	
				$(html).appendTo($('#av1'));
			}
	});
});
</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<div id="av1"></div>	
</body>
</html>
