<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ApproveStatusKey"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="approveStatusKey" class="com.cwc.app.key.ApproveStatusKey"/>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		long psId=adminLoggerBean.getPs_id();  
		long adminId=adminLoggerBean.getAdid(); 
		PageCtrl pc = new PageCtrl();
		int p =StringUtil.getInt(request,"p");
		pc.setPageNo(p);
		pc.setPageSize(10);
		long ps_id = StringUtil.getLong(request,"ps_id");
		String cmd = StringUtil.getString(request,"cmd");
		long mainId = StringUtil.getLong(request,"mainId");
		long approve_status = StringUtil.getLong(request,"approve_status");
		
		if(ps_id==0){
	ps_id=psId;
		}
	//	if(spotStatus==0){
	//		spotStatus=2;
	//	}
		
		DBRow[] rows = checkInMgrZwb.findCheckInPatrolApprove(ps_id,approve_status, pc);
		DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
		DBRow[] spots = checkInMgrZwb.findSpotArea(ps_id);
%>
<title>Difference Spot</title>
<script type="text/javascript">


</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<div class="demo" style="width:98%">
	  <div id="tabs">
		<ul>
			<li><a href="#checkin_filter">Advanced Search</a></li>
		</ul>
	      <div id="checkin_filter">
	      		<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left">
						<div style="float:left;">
						<select id="ps" name="ps">
							<option value="-1">All Storage</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("id",0l)%>" <%=ps_id==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
							<%
								}
							%>
						</select>
						<select name="approve_status" id="approve_status">
						  <option value="0" selected>All Status</option>
						  <option value="<%=ApproveStatusKey.WAITAPPROVE%>" <%=approve_status==ApproveStatusKey.WAITAPPROVE?"selected":""%>>Waiting</option>
						  <option value="<%=ApproveStatusKey.APPROVE%>" <%=approve_status==ApproveStatusKey.APPROVE?"selected":""%>>Approved</option>
						</select>
						<input type="button" class="button_long_refresh" value="Search" onclick="filter();"/>
					</td>
				    
				</tr>
			</table>
	      </div>
       </div>
       <script>
	       $("#tabs").tabs({
		   	    cache: true,
		   		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		   		cookie: { expires: 30000 } ,
		   		show:function(event,ui)
		   			 {
		   			 	
		   			 }
		   });
       </script>
    </div>
    <br/>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
	      
	        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Submitter</th>
	        <th width="13%" class="left-title" style="vertical-align:center;text-align:center;">Submit Date</th>
	        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">Difference/Approved Amount</th>
	        <th width="12%" class="left-title" style="vertical-align: center;text-align: center;">Status</th>
	        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">Verifier</th>
	        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">Approval Date</th>
	        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">Option</th>
		</tr>
		<%if(rows != null && rows.length > 0){
      		for(DBRow row : rows){
      	%>
      	<tr height="50px">
      	     <td  align="center" valign="middle" style='word-break:break-all'>
      	    <%
      	      if(!row.getString("commit_adid").equals("")){
      	    %>
	      		<%=row.getString("poster") %>
	      	<%
      	      }else{
     	    %>
   	      	&nbsp;
   	      	<%
         	      }
   	      	%>
      	    </td>
      	    <td  align="center" valign="middle" style='word-break:break-all'>
      	    <%
      	      if(!row.getString("post_date").equals("")){
      	    %>
	      		<%=  DateUtil.showLocalparseDateTo24Hours(row.getString("post_date"),psId)%>
	      	<%
      	      }else{
     	    %>
   	      	&nbsp;
   	      	<%
         	      }
   	      	%>
      	    </td>
      	    <td  align="center" valign="middle" style='word-break:break-all'>
      	    <%
      	      if(!row.getString("difference_count").equals("")){
      	    %>
	      		<%=row.getString("difference_count") %>/<%=row.getString("difference_approve_count") %>
	      	<%
      	      }else{
     	    %>
   	      	&nbsp;
   	      	<%
         	      }
   	      	%>
      	    </td>
      	    <td  align="center" valign="middle" style='word-break:break-all'>
      	    <%
      	      if(row.getString("approve_status").equals("1")){
      	    %>
	      		<img src='../imgs/standard_msg_error.gif' width='16' height='16' align='absmiddle'><font color="red"><strong>Waiting</strong></font>
	      	<%
      	      }if(row.getString("approve_status").equals("2")){
      	    %>
      	   		 <img src='../imgs/standard_msg_ok.gif' width='16' height='16' align='absmiddle'> <font color="green"><strong>Approved</strong></font>
	      	<%
      	      }else{
     	    %>
   	      	&nbsp;
   	      	<%
         	      }
   	      	%>
      	    </td>
      	    <td  align="center" valign="middle" style='word-break:break-all'>
      	     <%
      	      if(!row.getString("approve_adid").equals("")){
      	    %>
	      		<%=row.getString("verifier") %>
	      	<%
      	      }else{
     	    %>
   	      	&nbsp;
   	      	<%
         	      }
   	      	%>
      	    </td>
  			<td  align="center" valign="middle" style='word-break:break-all'>
      	     <%
      	      if(!row.getString("approve_date").equals("")){
      	    %>
	      		<%=  DateUtil.showLocalparseDateTo24Hours(row.getString("approve_date"),psId)%>
	      	<%
      	      }else{
     	    %>
   	      	&nbsp;
   	      	<%
         	      }
   	      	%>
      	    </td>
      	    <td  align="center" valign="middle" style='word-break:break-all'>
      	    <%
      	     if ( row.get("approve_status",0)==1 ){
			  
		    %>
      	     	<input type="button"  name="Submit2" value="Approve"  class="short-button" onClick="window.location.href='check_in_diff_spot_details.html?pa_id=<%=row.getString("pa_id")%>&ps_id=<%=row.getString("ps_id")%>&backurl=<%=StringUtil.getCurrentURL(request)%>'"/>
      	     <%
      	     }else{
			  
		    %>	
		    	<input type="button"  name="Submit2" value="View"  class="short-button" onClick="window.location.href='check_in_diff_spot_details.html?pa_id=<%=row.getString("pa_id")%>&ps_id=<%=row.getString("ps_id")%>&flag=1'"/>
		    	
		    <%
			  }
		    %>
      	    </td>    
      	</tr>
		
      	<% 		
      		}
      		
      	}else{
      	%>
      		<tr style="background:#E6F3C5;">
   						<td colspan="10" style="text-align:center;height:120px;">No Data</td>
   		    </tr>
      	<% }%>
	</table>
	<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" action="check_in_diff_spot.html">
	    <input type="hidden" name="p"  value="<%=p%>"/>
		<input type="hidden" name="ps_id" value="<%=ps_id%>"/>
		<input type="hidden" name="approve_status"value="<%=approve_status%>" />
	  </form>
	  <form name="check_data" id="check_data" >
		<input type="hidden" name="mainId"  />
		<input type="hidden" name="cmd" value="verify"/>
	  </form>	
	  <tr>
	    <td height="28" align="right" valign="middle" class="turn-page-table">
	        <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
	      Goto 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
	    </td>
	  </tr>
   </table>
  
	<form action="check_in_diff_spot.html" method="post" name="filter_form">
		<input type="hidden" name="ps_id"/>
		<input type="hidden" name="approve_status" />
	</form>
</body>
</html>
<script>

function filter(){
	
	document.filter_form.ps_id.value = $("#ps").val();
	document.filter_form.approve_status.value = $("#approve_status").val();
	document.filter_form.submit();
}

</script>