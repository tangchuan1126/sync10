<%@page import="com.cwc.app.key.OccupyTypeKey"%>
<%@page import="com.cwc.app.key.SpaceRelationTypeKey"%>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.OccupyStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="checkInDoorStatusTypeKey" class="com.cwc.app.key.OccupyStatusTypeKey"/>
<jsp:useBean id="checkInMainDocumentsRelTypeKey" class="com.cwc.app.key.CheckInMainDocumentsRelTypeKey"/>
<jsp:useBean id="occupyTypeKey" class="com.cwc.app.key.OccupyTypeKey"/> 

<html>
<head>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		long psId=adminLoggerBean.getPs_id();  
		long adminId=adminLoggerBean.getAdid(); 
		PageCtrl pc = new PageCtrl();
		int p =StringUtil.getInt(request,"p");
		pc.setPageNo(p);
		pc.setPageSize(10);
		long ps_id = StringUtil.getLong(request,"ps_id");
		long spot_area = StringUtil.getLong(request,"spot_area");
		int spotStatus = StringUtil.getInt(request,"spotStatus");
		String start_time = StringUtil.getString(request,"start_time");
		String end_time = StringUtil.getString(request,"end_time");
		long dlo_id = StringUtil.getLong(request,"dlo_id");
		String cmd = StringUtil.getString(request,"cmd");
		long spotId = StringUtil.getLong(request,"spotId");
		long searchSpotId = StringUtil.getLong(request,"searchSpotId");
		long searchdlo_id = StringUtil.getLong(request,"searchdlo_id");
		int flag=StringUtil.getInt(request,"flag");
		String license_plate = StringUtil.getString(request,"license_plate");
		String trailerNo = StringUtil.getString(request,"trailerNo");
		long mainId = StringUtil.getLong(request,"mainId");
		if(ps_id==0){
	ps_id=psId;
		}
	//	if(spotStatus==0){
	//		spotStatus=2;
	//	}
	//	System.out.println("cmd---"+cmd);
		if(cmd.equals("check")){
		int doubt=StringUtil.getInt(request,"doubt");
		//int doubt_id = StringUtil.getInt(request, "doubt_id");
	          	checkInMgrZwb.verifyData(adminId,ps_id,mainId,license_plate,trailerNo,1,doubt);
		}
		DBRow[] rows = checkInMgrXj.rtOccupiedSpot(ps_id, spot_area, spotStatus, pc);
%>

<script type="text/javascript">
$(function(){
	$("tr[name='spot_tr'] td[id='text']").click(function(){
		var spotId=$(this).attr("resource_id");
		var dlo_id=$(this).attr("dlo_id");
		var spotName=$(this).attr("yc_no");
		var area_id=$(this).attr("area_id");
		//alert(spotId+","+dlo_id);
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_update_spot.html?spotId='+spotId+'&dlo_id='+dlo_id+'&spotName='+spotName+'&p='+<%=p%>+'&area_id='+area_id; 
		$.artDialog.open(uri , {title: "Spot",width:'1100px',height:'400px', lock: true,opacity: 0.3,fixed: true});
	})
	onLoadInitZebraTable();
});

</script>
</head>
<body>
	<table>
		<tr>
		   	<td align="right"  style='word-break:break-all' >
				<input type="button" class="long-long-150-button" value="View Inconsistencies" onclick="window.location.href='patrol_check_data_details.html?backurl=/Sync10/administrator/check_in/check_in_diff_spot.html'">&nbsp;
   			    <input type="button" class="long-button" value="Again Patrol" onclick="againPatro()">
			</td>
		</tr>
    </table>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
	        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Spot</th>
	        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Spot Area</th>
	        <th width="25%" class="left-title" style="vertical-align:center;text-align:center;">Tractor</th>
	        <th width="25%" class="left-title" style="vertical-align:center;text-align:center;">Trailer</th>
		</tr>
		<%if(rows != null && rows.length > 0){
      		for(DBRow row : rows){
      		   DBRow[]  equipments= checkInMgrXj.getEquipmentBySpotId(row.get("resource_id",0l));
      	%>
      	<tr height="60px"   >
      	    <td align="center" valign="middle" style='word-break:break-all'>
      	           <%if(equipments.length>0){ 
      	           %>
      	           <font color="red" ><b><%=row.get("resource_name","") %></b></font>
      	           <%
      	           }else{
      	           %>
      	            <font color="green"><b><%=row.get("resource_name","") %></b></font>
      	           <%
      	           }
      	           %>
      	           
      	    		 
      	    </td>
      	    <td align="center" valign="middle" style='word-break:break-all'>
      	    		 <%=row.get("area_name","") %>&nbsp;
      	    </td>
      	    <td align="center" valign="middle" style='word-break:break-all'>	 &nbsp;
    		 <%
    		 
    		 if(equipments.length>0){
    			 
    		 
    		    for(DBRow equipment:equipments){
    		    	if(equipment.get("equipment_type", 0)==CheckInTractorOrTrailerTypeKey.TRACTOR){
    		 %>
    		 <div style="text-align:left;clear:both;"> 		
				<div style="width:200px;float:left" align="right"><font color='#f60'>E<%=equipment.get("check_in_entry_id", 0l) %></font>&nbsp;&nbsp;&nbsp;&nbsp;<%=equipment.get("equipment_number", "") %></div>
				<div>
				    &nbsp;&nbsp;<img id="<%=equipment.get("equipment_id", 0) %>" width="16" height="16" align="absmiddle" src="../imgs/standard_msg_error.gif"  onclick="clearEequipment(<%=equipment.get("equipment_id", 0) %>);">
				</div>
  			 </div>
    		  
    		 <%
    		    	}
    		    }
    		 }
    		 %>
    	 </td>
    	 <td align="center" valign="middle" style='word-break:break-all'>	 &nbsp;
    		 <%
    		 
    		 if(equipments.length>0){
    			 
    		 
    		    for(DBRow equipment:equipments){
    		    	if(equipment.get("equipment_type", 0)==CheckInTractorOrTrailerTypeKey.TRAILER){
    		 %>
    		<div style="text-align:left;clear:both;"> 						
				<div style="width:200px;float:left" align="right"><font color='#f60'>E<%=equipment.get("check_in_entry_id", 0l) %></font>&nbsp;&nbsp;&nbsp;&nbsp;<%=equipment.get("equipment_number", "") %></div>
				<div>
				  &nbsp;&nbsp; <img id="<%=equipment.get("equipment_id", 0) %>" width="16" height="16" align="absmiddle" src="../imgs/standard_msg_error.gif"   onclick="clearEequipment(<%=equipment.get("equipment_id", 0) %>);">
				</div>
  			 </div>
    		 <%
    		    	}
    		    }
    		 }
    		 %>
    		
    	 </td>
    
      	</tr>
		<tr class="split">
  			<td colspan="4" style="height:24px;line-height:24px;background-image:url('../imgs/linebg_24.jpg');text-align:right;padding-right:20px;">
  					 <input type="button" class="short-button" value="Add" onclick="addEequipment(<%=CheckInTractorOrTrailerTypeKey.TRAILER %>,<%=row.get("resource_id",0l) %>,'<%=row.get("resource_name","") %>');"/>
					 <input type="button" class="short-button" value="Confirm" onclick="confirm(<%=!row.getString("dlo_id").equals("")?row.getString("dlo_id"):0%>,<%=row.get("resource_id",0l) %>,<%=row.getString("area_id") %>)"/>
  			</td>
  	    </tr>	
      	<% 		
      		}
      		
      	}else{
      	%>
      		<tr style="background:#E6F3C5;">
   						<td colspan="10" style="text-align:center;height:120px;">No Data</td>
   		    </tr>
      	<% }%>
	</table>
	<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	 
	  <tr>
	    <td height="28" align="right" valign="middle" class="turn-page-table">
	        <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
	      Goto 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
	    </td>
	  </tr>
   </table>
	      		

    </div>
	
	
	
	  <form name="mod_form" id="mod_form" >	
	    <input type="hidden" name="searchdlo_id"  />
		<input type="hidden" name="dlo_id"  />
		<input type="hidden" name="spotId"  />
		<input type="hidden" name="searchSpotId"  />
		<input type="hidden" name="flag"  />
		<input type="hidden" name="p"  />
		<input type="hidden" name="cmd" />
		<input type="hidden" name="license_plate" />
		<input type="hidden" name="trailerNo" />
	  </form>	
	  <form name="check_data" id="check_data" action="check_in_rtspot.html">
		<input type="hidden" name="license_plate"  />
		<input type="hidden" name="trailerNo"  />
		<input type="hidden" name="mainId"  />
		<input type="hidden" name="doubt"/>
		<input type="hidden" name="cmd" value="check"/>
	  </form>	
	  <form name="dataForm" action="check_in_rtspot.html">
	    <input type="hidden" name="p" id="p"  value="<%=p%>"/>
	    <input type="hidden" name="spot_area" value="<%=spot_area%>"/>
		<input type="hidden" name="start_time" value="<%=start_time%>"/>
		<input type="hidden" name="end_time" value="<%=end_time%>"/>
		<input type="hidden" name="ps_id" value="<%=ps_id%>"/>
		<input type="hidden" name="spotStatus" value="<%=spotStatus%>"/>
	  </form>
	  
    
</body>
</html>
<script>
function go(num){
	$("#p").val(num);
	showCheckInSpotZone('<%= spot_area%>',num);
}

function againPatro(){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxAgainPatrolAction.action',
		data:{
			ps_id:<%=ps_id%>,
			area_id:<%=spot_area%>
		},
		dataType:'json',
		type:'post',
		success:function(data){
			
			location.href = 'check_in_rtspot.html?p=1&spot_area=<%=spot_area%>&ps_id=<%=ps_id %>&spotStatus=<%=spotStatus%>';
		},
		error:function(){
			alert("系统错误");
		},
	});
}
function createEntryId(spotId,spotName){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_createEntryId.html?spotId='+spotId+'&spotName='+spotName; 
	$.artDialog.open(uri , {title: "Create EntryID",width:'500px',height:'200px', lock: true,opacity: 0.3,fixed: true});
}


function confirm(dlo_id,spotId,area_id){
	
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxConfirmPatrolSpotAction.action',
  		dataType:'json',
  //		async:false,
  		data:"dlo_id="+dlo_id+"&spotId="+spotId+"&area_id="+area_id,
  		success:function(data){
					location.reload();
					$.artDialog.close();
  			    	
  		}
    });
}
function clearEequipment(equipment_id){
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxClearEquipmentAtResourcesAction.action',
  		dataType:'json',
  //		async:false,
  		data:"relation_id="+equipment_id+"&relation_type="+<%=SpaceRelationTypeKey.Equipment %>+"&adminId="+<%=adminId%>,
  		success:function(data){
					location.reload();
					$.artDialog.close();
  			    	
  		}
    });
}

function addEequipment(equipment_type,resource_id,yc_no){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_update_spot.html?resource_id='+resource_id+'&resource_name='+yc_no+'&p='+<%=p%>+'&equipment_type='+equipment_type+'&resource_type='+<%=OccupyTypeKey.SPOT %>;
	$.artDialog.open(uri , {title: "<%=occupyTypeKey.getOccupyTypeKeyName(OccupyTypeKey.SPOT) %>",width:'1100px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}

</script>