<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
	String area_name = StringUtil.getString(request,"area_name");
	Long ps_id = StringUtil.getLong(request,"ps_id");
	DBRow[] titleList = locationAreaXmlImportMgrCc.getAllTitle();
	DBRow[] title =googleMapsMgrCc.getTitleByPsIdAndAreaname(ps_id, area_name);
	DBRow[] areainfo =googleMapsMgrCc.getAreaPositionByName(ps_id, area_name);
	long area_id =areainfo[0].get("area_id",0l);
 
%>
<html>
  <head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
.text-overflow {
	display:block;
	word-break:keep-all;
	white-space:nowrap;
	overflow:hidden;
	text-overflow:ellipsis;
}
.gis_main_button{
	margin-left:15px; padding: 6px 8px; cursor: pointer; display: inline-block; 
	text-align: center; line-height: 1; *padding:4px 10px; *height:2em; letter-spacing:2px; font-family: Tahoma, Arial/9!important; 
	width:auto; overflow:visible; *width:1;   border-radius: 5px; 
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFFFFF', endColorstr='#DDDDDD'); 
	text-shadow: 0px 1px 1px rgba(255, 255, 255, 1);
	box-shadow: 0 1px 0 rgba(255, 255, 255, .7),  0 -1px 0 rgba(0, 0, 0, .09); -moz-transition:-moz-box-shadow linear .2s; 
	-webkit-transition: -webkit-box-shadow linear .2s; transition: box-shadow linear .2s;color: #FFF;
	border: solid 1px #1c6a9e;
	background: #2288cc; 
	}	
</style>
  </head>
  <body>
  	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	    <table style="width: 100%;">
			<input type="hidden" id="area_id" value="<%=area_id %>">
	    	<tr style=" font-weight:bold; font-size:16px; color:#00F;">
	    		<td style="width: 49%;">&nbsp;Existence title</td>
	    		<td style="width: 2%;"></td>
	    		<td style="width: 49%;">&nbsp;Free title</td>
	    	</tr>
	    	<tr>
	    		<td>
					<div id="exitTitle" style="background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto" >
						<%
						if(title.length>0){
							for(int i=0; i<title.length; i++){
								String t = title[i].getString("title_name");
								String id =title[i].getString("title_id");
								%>
								<div title="<%=t %>" style="width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
									<div class="text-overflow"  style="width: 85%; float: left; " ondblclick="removeTitle(this.parentNode,<%=id %>)">
										<%=t %>
										<input type="hidden" id="title_id" value="<%=id %>">
									</div>
									<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/delete_grey.png'); margin: 1px; cursor: pointer;" 
											onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/delete_blue.png&quot;)'" 
											onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/delete_grey.png&quot;)'"
											onclick="removeTitle(this.parentNode,<%=id %>)">
									</div>
								</div>
								<%
							}
						}else{
							%>
							<center>
								no data
							</center>
							<%
						}
						%>
					</div>
				</td>
				<td></td>
	    		<td>
					<div id="notExitTitle" style="background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto" >
					<%
						if(titleList != null && titleList.length>0){
							for(int i=0; i<titleList.length; i++){
								DBRow t = (DBRow)titleList[i];
								%>
								<div title="<%=t.getString("title_name") %>" style="width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
									<div class="text-overflow" style="width: 85%; float: left; " ondblclick="addTitle(this.parentNode)">
										<%=t.getString("title_name") %>
										<input type="hidden" id="title_id" value="<%=t.getString("title_id") %>">
									</div>
									<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_grey.png'); margin: 1px; cursor: pointer;" 
												onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'" 
												onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'"
												onclick="addTitle(this.parentNode)">
									</div>
								</div>
								<%
							}
						}else{
							%>
							<center>
								no data
							</center>
							<%
						}
					%>
					</div>
				</td>
	    	</tr>
	    	<tr>
				<td colspan="3" align="right" class="win-bottom-line">
					<input type="button" name="Submit1" class="gis_main_button" value="Confirm" onClick="confirm();"> 
					<input type="button" name="Submit2" class="gis_main_button" value="Cancel" onClick="closeWindow();">
				</td>
			</tr>
	    </table>
	</div>
	<!-- model -->
	<div id="title_model" style="display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
		<div class="text-overflow"  style="width: 85%; float: left; " ondblclick="removeTitle(this.parentNode)">
			<input type="hidden" id="title_id">
		</div>
		<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/delete_grey.png'); margin: 1px; cursor: pointer;" 
				onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/delete_blue.png&quot;)'" 
				onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/delete_grey.png&quot;)'"
				onclick="removeTitle(this.parentNode)">
		</div>
	</div>
	<div id="title_free_model" style="display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
		<div class="text-overflow" style="width: 85%; float: left; " ondblclick="addTitle(this.parentNode)">
			<input type="hidden" id="title_id">
		</div>
		<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_grey.png'); margin: 1px; cursor: pointer;" 
					onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'" 
					onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'"
					onclick="addTitle(this.parentNode)">
		</div>
	</div>
	<!-- model end-->
  </body>
<script type="text/javascript">
//删除title
function removeTitle(ele){
	if($("#notExitTitle").find("div").length == 0){
		$("#notExitTitle").html("");
	}
	var je = $(ele);
	var id = je.find("#title_id").val();
	var title = je.attr("title");
	var node = creatTitleNode(title,id,true);
	$("#notExitTitle").append(node);
	je.remove();
}
//添加Title
function addTitle(ele){
	if($("#exitTitle").find("div").length == 0){
		$("#exitTitle").html("");
	}
	var je = $(ele);
	var id = je.find("#title_id").val();
	var title = je.attr("title");
	var node = creatTitleNode(title,id,false);
	$("#exitTitle").append(node);
	je.remove();
}
//创建title标签
function creatTitleNode(title,id,isFree){
	var node = null;
	if(isFree){
		node = $("#title_free_model").clone();
	}else{
		node = $("#title_model").clone();
	}
	node.removeAttr("id");
	node.attr("title",title);
	node.find("#title_id").val(id);
	node.find("div").first().append(title);
	node.css("display","");
	return node;
}
function confirm(){
	var titles = "";
	var psId =<%=ps_id%>;
	var ids = "";
	var ts = $("#exitTitle #title_id");
	var area_id =$("#area_id").val();
	for(var i=0; i<ts.length; i++){
		ids += $(ts[i]).val()+",";
		titles +=  $(ts[i].parentNode.parentNode).attr("title")+",";
	}
	if(ids!=""){
		ids = ids.substr(0,ids.length-1);
		titles = titles.substr(0,titles.length-1);
	}
	 $.ajax({
			url: '/Sync10/_gis/addTitle',
			type: 'post',
			dataType: 'json',
			timeout: 6000,
			cache:false,
			data:{"ids":ids,"titles":titles,"area_id":area_id},
			beforeSend:function(request){
			},
			success: function(data){
				showMessage("Save Succeed","Succeed");	
				$.artDialog.opener.jsmap.areaTitle[psId]={};
				//$.artDialog.opener.getAreaTitleAjax(psId);
				$.artDialog.opener.loadStorageLayer(psId,"area,zone-title");
				$.artDialog.close();
			},
			error: function(e){
				showMessage("System error","error");	
			}
	 });
	
}
function closeWindow() {
	$.artDialog.close();
}
</script>
</html>
