<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>

<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<html>
<head>

<%
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adid = adminLoggerBean.getAdid();
String loadNo = StringUtil.getString(request, "loadNo");
long entry_id = StringUtil.getLong(request, "entry_id");
String door_name = StringUtil.getString(request, "door_name");
String type = StringUtil.getString(request, "type");
DBRow[] rows = sqlServerMgr.findLoadCompanyIdCustomerIdByLoadNo(loadNo, adid, request);
if(0 == rows.length)
{
	rows = sqlServerMgr.findOrderCompanyIdCustomerIdByLoadNo(loadNo, adid, request);
}
  
%>
<title>select load</title>
<script type="text/javascript">
function selectLoad(CompanyID, loadNo, CustomerID)
{
	 $.artDialog && $.artDialog.close();
	 $.artDialog.opener.setParentCompanyLoadCustomer  && $.artDialog.opener.setParentCompanyLoadCustomer(CompanyID, loadNo, CustomerID,'<%=entry_id%>','<%=door_name%>','<%=type%>');
}
function changeCursor(obj)
{
	 $(obj).css("cursor","Pointer"); 
}
</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		<tr height="40px">
			<th width="25%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">CompanyID</th>
	        <th width="35%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">LoadNo</th>
	        <th width="40%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">CustomerID</th>
		</tr>
		<%
		if(rows.length > 0)
		{
			for(int i = 0; i < rows.length; i ++)
			{
		%>	
		<tr height="35px" onclick="selectLoad('<%=rows[i].getString("CompanyID") %>','<%=rows[i].getString("LoadNo") %>','<%=rows[i].getString("CustomerID") %>')" onmouseover="changeCursor(this)">
			<td>
				<%=rows[i].getString("CompanyID") %>
			</td>
			<td>
				<%=rows[i].getString("LoadNo") %>
			</td>
			<td>
				<%=rows[i].getString("CustomerID") %>
			</td>
		</tr>		
		<%
			}
		}else{
		%>
			<tr>
				<td colspan="3" style="text-align:center;line-height:50px;height:50px;background:#E6F3C5;border:1px solid silver;">无数据</td>
			</tr>
		<%} %>
	</table>	
</body>
</html>
