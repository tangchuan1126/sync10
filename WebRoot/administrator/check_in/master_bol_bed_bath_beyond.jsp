<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@ include file="../../include.jsp"%>
<%
	//String loadNo=StringUtil.getString(request, "load");
	long entry_id=StringUtil.getLong(request, "entry_id");
	//String companyId=StringUtil.getString(request, "companyId");
	//String customerId=StringUtil.getString(request, "customerId");
	String jsonString = StringUtil.getString(request, "jsonString");
	String printName=StringUtil.getString(request,"print_name");
	JSONArray jsons = new JSONArray(jsonString);
	DBRow[] mainRows = checkInMgrZwb.selectAllMain(entry_id, null);
	DBRow mainRow=mainRows.length > 0?mainRows[0]:null;
	String out_seal=StringUtil.getString(request, "out_seal");
	boolean isPrint = StringUtil.getInt(request, "isprint") == 1;
%>
<html>
<head>

<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">

<%if(!isPrint){ %>

<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
<%} %>
<title>master_bol_bed_bath_beyond</title>
<script type="text/javascript">
function print(){
	     var printHtml=$('div[name="printHtml"]');
	     
	     for(var i=0;i<printHtml.length;i++){
	    	 var a1=$('#a1',printHtml[i]);
	    	 var a2=$('#a2',printHtml[i]);
	    	 var a3=$('#a3',printHtml[i]);	
	    	 //分页字体
	    	 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
	    	 
	    	 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"A4");
	    	 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",a1.html());
	    	 visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);  
	    	
	    	 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a2.html());
	    	 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
	    	 
	    	 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a3.html());
	    	 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
	    	 
	    	 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#page_header').html());
			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   
			 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");         
	    	 //页码
	    	 visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 
	    	 
			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);	
 
	    	 
	    	 visionariPrinter.SET_PRINT_COPIES(1);
	    	 //visionariPrinter.PREVIEW();
	    	 visionariPrinter.PRINT();
	     }		 
}
</script>
</head>

<body>
<div style="width:195mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;margin-top:3px;text-align:right;">
	<input class="short-short-button-print" type="button" onclick="print()" value="Print">
</div>
<%
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adid = adminLoggerBean.getAdid();
for(int m=0;m<jsons.length();m++){
		JSONObject json=jsons.getJSONObject(m);
		String loadNo="";
		String ctnr="";
		String bol="";
		
		String number=json.getString("number");
		String door_name = json.getString("door_name");
		String companyId = json.getString("companyId");
		String customerId = json.getString("customerId");

		String checkDataType=json.getString("checkDataType");
		if(checkDataType.equals("CTNR")){
			ctnr=number;
		}
		if(checkDataType.equals("BOL")){
			bol=number;
		}
		if(checkDataType.equals("LOAD")){
			loadNo=number;			
		}
DBRow[] masterBols = sqlServerMgr.findMasterBolsSomeInfoByLoadNo(loadNo,companyId,customerId, adid, request);
	for(int i = 0; i < masterBols.length; i ++){
		DBRow masterSomeInfoRow = masterBols[i];
%>
<!-- loop body start -->
<div style="border:1px red solid; width:195mm;  margin:0 auto; margin-top:3px;" name="printHtml">

	<div id="a1">
		<div id="page_header" style="display:none;">
			<style type="text/css">
				
				.report_table_header_l{
					border-bottom:1px solid black;
					border-top:1px solid black;
					border-left:1px solid black;
					font-size:12px;
					font-family:Arial;
					padding-left:2px;
				}
				.report_table_header_r{
					border-bottom:1px solid black;
					border-top:1px solid black;
					border-right:1px solid black;
					font-size:12px;
					font-family:Arial;
					padding-left:2px;
				}
				.report_table_header_c{
					border-bottom:1px solid black;
					border-top:1px solid black;
					font-size:18px;
					font-family:Arial;
					font-weight:bold;
				}
				.report_table_title{
					border-bottom:1px solid black;
					border-top:1px solid black;
					
					font-size:12px;
					font-family:Arial;
					background-color:#000; 
					color:#FFF;
				}
				.report_table_content{
					
					font-size:12px;
					font-family:Arial;
					padding-left:2px;
					margin-top: 2px;
				}
				.report_table_pad_brl{
					border-bottom:1px solid black;
					border-right:1px solid black;
					border-left:1px solid black;
					padding-left:2px;
					font-size:12px;
					font-family:Arial;
				}
				.report_checkbox_off{
					text-align:center;
					width:15px;
					height:15px;
					background:#FFF;
					border:2px solid #000;
				}
				
				.report_tdpad_l{
					border-left:1px solid black;
				}
				.report_tdpad_lr{
					border-right:1px solid black;
					border-left:1px solid black;
				}
				
				.variable_font_italic{
					font-style: italic;
					font-size: 13px;
				}
				
			</style>
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20%" height="36" align="left" class="report_table_header_l">
						Date:&nbsp;
						<i style="font-style: italic;"><%=new TDate().getFormateTime(DateUtil.NowStr(), "MM/dd/yyyy") %></i>
					</td>
					<td width="63%" align="center" class="report_table_header_c">
						NON NEGOTIABLE BILL OF LADING
						<br>
						<span style="font-size:12px;font-weight:normal;">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bill of Lading Number:&nbsp;
						</span>
						<span style="font-style: italic;">
						<%=masterSomeInfoRow.get("MasterBOLNo", 0L) %>
						</span>
					</td>
		        	<td width="10%" align="center" class="report_table_header_r">
		        	</td>
				</tr>
		    </table>
		</div>
		
		
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
	      <tbody><tr>
	        <td style="border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black; font-size: 12px; font-family: Arial;" align="center" height="36" width="15%"><b>Date:</b><span style="font-style: italic;"><%=new TDate().getFormateTime(DateUtil.NowStr(), "MM/dd/yyyy") %></span></td>
	        <td style="border-bottom: 1px solid black; border-top: 1px solid black; font-size: 18px; font-weight: bold; font-family: Arial;" align="center" width="74%">NON NEGOTIABLE BILL OF LADING</td>
	        <td style="border-bottom: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;" align="center" width="11%">&nbsp;</td>
	      </tr>
	    </tbody></table>
	    
	    <table cellpadding="0" cellspacing="0" border="0" width="100%">
	      <tbody><tr>
	        <td style="border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; font-size: 12px; font-family: Arial; " align="left" height="48" width="55%">
	          <div style="background-color:#000; color: #FFF; font-weight:bold;" align="center">SHIP FROM</div>
	          <div style="padding-left: 2px;">&nbsp;<b>Name:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipFromName") %></span></div>
	          <div style="padding-left: 2px;">&nbsp;<b>Address:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("Address1")+" "+masterSomeInfoRow.getString("Address2") %></span></div>
	          <div style="padding-left: 2px;">&nbsp;<b>City/State/Zip:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("City")+","+masterSomeInfoRow.getString("State")+" "+masterSomeInfoRow.getString("ZipCode") %></span></div>
	          <div style="padding-left: 2px;">&nbsp;</span></div>
	          <div>
	            <table cellpadding="0" cellspacing="0" border="0" width="100%">
				  <tbody><tr>
				    <td style="font-size: 12px; font-family: Arial;padding-left: 2px;">&nbsp;<b>SID#:</b>&nbsp;&nbsp;<span style="font-style: italic;">&nbsp;</span></td>
				    <td style="font-size: 12px; font-family: Arial;width:80px;margin-right:10px;"><b>FOB: </b><input style="text-align: center; width: 14px; height: 14px; background: #FFF; border: 2px solid #000;" type="text"></td>
				  </tr>
				</tbody></table>
	          </div>
	          <div style="background-color:#000; color: #FFF;font-weight:bold; clear: left" align="center">SHIP TO</div>
	        </td>
	        <td style=" border-bottom: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;" align="left" valign="top" width="45%">
	          <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Bill of Lading Number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.get("MasterBOLNo", 0L) %></span></div>
	          <div>&nbsp;</div>
			  <div>&nbsp;</div>
			  <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">&nbsp;&nbsp;</div>
	          <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Load No:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=loadNo %></span></div>
	          <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Appointment Date:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=DateUtil.parseDateTo12Hour(masterSomeInfoRow.getString("AppointmentDate")) %></span></div>
	        </td>
	      </tr>
	      <tr>
	        <td style="border-bottom:1px solid black; border-right:1px solid black;border-left:1px solid black;font-size:14px;font-family:Arial;" align="left">
	           <div style="padding-left: 2px; width:425px; border:red solid 0px;white-space:nowrap;overflow:hidden;">
	          &nbsp;<b>Name:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipToName") %></span>
             &nbsp;<b>Location&nbsp;#:</b>&nbsp;&nbsp;<span style="font-style: italic;"></span></div>
	          <div style="padding-left: 2px;">&nbsp;<b>Address:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipToAddress1")+" "+masterSomeInfoRow.getString("ShipToAddress2") %></span></div>
	          <div style="padding-left: 2px;">&nbsp;<b>City/State/Zip:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipToCity")+","+masterSomeInfoRow.getString("ShipToState")+" "+masterSomeInfoRow.getString("ShipToZipCode")%></span></div>
	          <div>
	            <table cellpadding="0" cellspacing="0" border="0" width="100%">
				  <tbody><tr>
				    <td style="font-size: 12px;font-family: Arial;padding-left: 2px;">&nbsp;<b>CID#:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("ShipToID")+" "+masterSomeInfoRow.getString("ShipToPhone") %></span></td>
				    <td style="font-size: 12px;font-family: Arial; width: 80px; margin-right: 10px;"><b> FOB: </b>
				      <input style="text-align: center; width: 14px; height: 14px; background: #FFF; border: 2px solid #000;" type="text"></td>
				  </tr>
				</tbody></table>
	          </div>
	          <div style="background-color:#000; color:#FFF;font-family:Arial; clear: left" align="center">THIRD PARTY FREIGHT CHARGES BILL TO:</div>
	        </td>
	        <%DBRow carrier = sqlServerMgr.findCarrierInfoByCarrierId(masterSomeInfoRow.getString("CarrierID"), masterSomeInfoRow.getString("CompanyID")); %>
	        <td style="border-bottom:1px solid black;font-size:13px;font-family:Arial;border-right:1px solid black;" align="left" valign="top">
	          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">CARRIER NAME: </span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=carrier?carrier.getString("CarrierName"):"" %></span></div>
	          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Trailer Number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=mainRow?mainRow.getString("gate_container_no"):"" %></span></div>
	          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Tractor number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=mainRow?mainRow.getString("gate_liscense_plate"):"" %></span></div>
	          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Driver License No:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=mainRow?mainRow.getString("gate_driver_liscense"):"" %></span></div>
	          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Seal No.:</span>&nbsp;&nbsp;<span style="font-style: italic;">
	          	<%if(!StringUtil.isBlank(out_seal))
			        		{
			        		%>
			        			<%=out_seal %>
			        		<%	
			        		}else{%>
			        			<%=null!=mainRow?mainRow.getString("seal"):"" %>
			        		<%} %>
	          </span></div>
	        </td>
	      </tr>
	      <tr>
	        <td style="border-bottom:1px solid black;border-left:1px solid black; border-right:1px solid black;font-size:12px;font-family:Arial;" align="left" valign="top">
	          <div style="padding-left: 2px;">&nbsp;<b>Name:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("BillToName") %></span></div>
	          <div style="padding-left: 2px;">&nbsp;<b>Address:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=masterSomeInfoRow.getString("BillToAddress1")+" "+masterSomeInfoRow.getString("BillToAddress2") %></span></div>
	          <div style="border-bottom:1px solid black;"></div>
	          <div style="font-weight: bold;padding-left: 2px;">SPECIAL INSTRUCTIONS:</div>
	          <div style="font-style:italic;">
	          	<%=StringUtil.replaceEnter(masterSomeInfoRow.getString("Note")) %>
	          </div>
	        </td>
	        <td style="border-bottom:1px solid black;border-right:1px solid black; font-size:12px;font-family:Arial;" align="left" valign="top">
	          <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">SCAC:</span>&nbsp;&nbsp;
				<span style="font-style: italic;">
					<%
					out.println(null!=carrier?carrier.getString("SCACCode"):"" );
			  		%>
			    </span>
			  </div>
	          <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Pro number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=mainRow?mainRow.getString("imei"):""%></span></div>
	          <div style="border-bottom: 1px solid black" align="center">  
	          	<font style="FONT-SIZE: 18px; font-family: time Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman'; WIDTH: 100%; COLOR: #B4B4B4; LINE-HEIGHT: 150%;">
	          		BAR CODE SPACE
	          	</font>
	          	<div>&nbsp;</div>
	          	<div>&nbsp;</div>
	          </div>
	          <div style="font-size: 11px;font-weight: bold;margin-left:5px;text-align:left;">Freight Charge Terms:<span style="font-style: italic;">(freight charges are prepaid unless marked otherwise)</span></div>
	          <div style="padding-left:2px; border-bottom:1px solid black;font-weight:bold;padding-bottom:5px; "> 
			        <%String freightTerm = masterSomeInfoRow.getString("FreightTerm"); %>
	        		<B>Prepaid&nbsp;__</B><%="Prepaid".equals(freightTerm)?"X":"" %><B>__&nbsp;Collect&nbsp;__</B><%="Collect".equals(freightTerm)?"X":"" %><B>__&nbsp;3rd Party&nbsp;__</B><%="Third Party".equals(freightTerm)?"X":"" %><B>__</B>&nbsp;
	          </div>
	          <div style="clear:both">
	          	<div style="float:left;margin-left:5px;">
	          		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	          		<input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000; margin-top:1px;" type="text">
	            	<br><span style="font-size:12px;">(check box)</span>
	            </div>
	            <div style="float:right;margin-bottom:5px; margin-right:30px;">
	            	Master Bill of Lading:with attached<br>underlying  Bills of lading
	            </div>
	          </div>
	        </td>
	      </tr>
	    </tbody></table>
		
	</div>
	
    <!-- CARRIER INFORMATION -->
    <div id="a2">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<thead>
	      		<tr>
	        		<td height="19" colspan="9" align="center" valign="top" bgcolor="#000000" style="color:#FFF;font-size:12px;font-family:Arial;">
	            		<B>CARRIER INFORMATION</B>
	        		</td>
	      		</tr>     
	      		<tr>
	        		<td colspan="2" align="center"  
	        			style="font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;">
	        			<B>HANDLING <br />UNIT</B>
	        		</td>
	        		<td colspan="2" align="center"  
	        			style="font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;">
	        			<B>PACKAGE</B>
	        		</td>
	        		<td width="10%" rowspan="2" align="center"  style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;">
	        			<span style="font-weight:bold">
	        				<span style="font-weight:bold">WEIGHT</span>
	        			</span>
	        		</td>
	        		<td width="5%" rowspan="2" align="center"  
	        			style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;">
	        				H.M.<br />(X)
	        		</td>
	        		<td width="33%" rowspan="2" align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >
	        			<span style="font-weight:bold">
	        			<B>	COMMODITY DESCRIPTION</B>
	        			</span><br />
	        			<span style="font-family:Arial;font-size:6.5px;">
	        				Commodities requiring special or additional care or attention in handling or stowing must be so
	        				marked and packaged as to ensure safe transportation with ordinary care.
	        				See Section 2(e) of NMFC Item 360
	        			</span>
	        		</td>
					<td colspan="2" align="center"  
						style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;">
						<B>LTL ONLY</B>
					</td>
	      		</tr>
	      		<tr>
	        		<td width="8%" align="center" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;">
	        			QTY
	        		</td>
	        		<td width="9%" align="center" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;">
	        			TYPE
	        		</td>
	        		<td width="8%" align="center" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;">
	        			QTY
	        		</td>
	        		<td width="9%" align="center" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;">
	        			TYPE
	        		</td>
	        		<td width="10%" align="center" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;">
	        			NMFC#
	        		</td>
	        		<td width="10%" align="center" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;">
	        			CLASS
	        		</td>
	      		</tr>
	     	</thead>
	     	<%
	     		DBRow itemSumRow = sqlServerMgr.findItemSumCountWeightByMasterBolCompany(masterSomeInfoRow.get("MasterBOLNo", 0L), masterSomeInfoRow.getString("CompanyID"), adid, request);
	     	%>
	     	<tr>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" >
		        	<%=MoneyUtil.formatDoubleUpInt(itemSumRow.get("palletSum", 0d)) %>
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >
		        	Plts
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" >
		        	<%=itemSumRow.get("caseSum", 0) %>
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" >
		        	<%=0!=itemSumRow.get("caseSum", 0)?"CTNS":"Piece" %>
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" >
		        	<%=itemSumRow.get("weigthSum", 0) %>
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >&nbsp;
		        	
		        </td>
		       	<td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" >
		       		<%=itemSumRow.getString("CommodityDescription") %>&nbsp;
		       	</td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" >
		        	<%=itemSumRow.getString("NMFC") %>
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" >
		        	<%=itemSumRow.getString("FreightClass") %>
		        </td>
			</tr>
	     	<tr height="175">
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >&nbsp;
		        	
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >&nbsp;
		        	
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >&nbsp;
		        	
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >&nbsp;
		        	
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >&nbsp;
		        	
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >&nbsp;
		        	
		        </td>
		       	<td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >&nbsp;
		       		
		       	</td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" >&nbsp;
		        	
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid; border-right:1px black solid; font-size:12px;font-family:Arial;" >&nbsp;
					
		        </td>
			</tr>
	      	<tr>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;">
		        	<%=MoneyUtil.formatDoubleUpInt(itemSumRow.get("palletSum", 0d)) %>
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" bgcolor="#999999">&nbsp;
		        	
		    	</td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;">
		        	<%=itemSumRow.get("caseSum", 0) %>
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" bgcolor="#999999">&nbsp;
		        	
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;">
		        	<%=itemSumRow.get("weigthSum", 0) %>
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" bgcolor="#999999">&nbsp;
		        	
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;">
		        	<B>GRAND TOTAL</B>
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" bgcolor="#999999">&nbsp;
		        	
		        </td>
		        <td align="center" style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" bgcolor="#999999">&nbsp;
		        	
		        </td>
	      	</tr>
	    </table>
    </div>
    
    
    
    
    
    
    
    
    
    <!-- end -->
	<div id="a3">
	
	 <table cellpadding="0" cellspacing="0" border="0" width="100%">
	      <tbody><tr>
	        <td width="55%" height="20" align="left" valign="top" style="border-bottom:1px solid black;border-top:3px solid black; border-right:1px solid black;border-left:1px solid black; font-size:10px;font-family:Arial;">
            	<div style="margin:5px 5px 0px 5px;">
            	
                </div>
            </td>
	        <td style="border-bottom:3px solid black;border-right:3px solid black;border-left:3px solid black; border-top:3px solid black;font-family:Arial; font-size:15px;" align="left" valign="top" width="45%">
	          <div><span style="font-weight:bold; padding-left:2px; margin-bottom:10px;">COD Amount:</span> $___________________________</div>
	          <div style="font-weight:bold;text-align:center;margin-top:2px;">Fee Terms:&nbsp;Collect:
	          
	            <input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;" type="text">
				&nbsp;Prepaid:
	          
	            <input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;" type="text">
	          </div>
	          <div style="font-weight:bold;text-align:center;margin-top:2px;"><label for="customer">Customer check acceptable:</label>
	            <input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;" type="text">
	          </div>
	        </td>
	      </tr>
	      <tr>
	        <td colspan="2" style="font-size:12px;font-family:Arial;border-bottom:1px solid black;border-left:1px solid black;border-right:1px solid black;font-weight:bold;" align="left" height="20" valign="top">
	        	<div style="margin-left:5px;margin-right:5px;">NOTE Liability Limitation for loss or damage in this shipment may be applicable. See 49 U.S.C. §14706(c)(1)(A)and (B). </div></td>
	      </tr>       
	      <tr>
	        <td colspan="2">
	        		<table cellpadding="0" cellspacing="0" border="0" width="100%">
				      <tbody><tr>
				        <td style="border-bottom:1px solid black; border-right:1px solid black;border-left:1px solid black;font-size:12px;font-family:Arial;" height="180" valign="top" width="34%">
				          <div style="font-weight:bold; padding-left: 5px;margin-top:5px;">SHIPPER SIGNATURE/DATE</div>
				          <div style="font-size: 9px; padding-left: 5px;">This is to certify that the above named materials are properly classified,packaged,marked and labeled, and are in proper condition for transportation according to the 
applicable regulations of the DOT.</div>
				          <br>
				          <div>&nbsp;</div>
				          <div>&nbsp;</div>
				          <div style="padding-left: 5px;">X</div>
				          <div style="padding-left: 5px; border-bottom:1px solid black;font-family:Arial;font-size:12px;"></div>
				          <div style="padding-left: 5px; font-family:Arial;font-size:12px;">
				          	<div style="float:left;">Signature/Print Name</div>
				          	<div style="float:right;padding-right:50px;">Date</div>
				          </div>
				        </td>
				        <td style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;font-size:12px;font-family:Arial;" valign="top" width="14%">
				          <div style="text-decoration:underline;font-family:Arial;font-size:12px;margin-top:5px;">Trailer Loader:</div>
				          <div style="margin-top:5px;margin-left:5px;text-align:left;font-size:12px;">
				            <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg">
				          	By Shipper
				          </div>
				          <div style="margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;">
				            <p>
				              <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg"> 
				              By Driver				          </p>
				          </div>
				        </td>
				        <td style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;font-size:12px;font-family:Arial;" valign="top" width="16%">
				          <div style="text-decoration:underline;font-family:Arial;font-size:12px;margin-top:5px;">Freight Counted:</div>
				          <div style="margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;">
				            <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg">
				          By Driver/Pieces</div>
				          <div style="margin-left:5px;margin-top:12px;font-family:Arial;font-size:12px;">NO SHIPPER LOADER COUNT</div>
				        </td>
				        <td style="border-bottom:1px solid black;border-right:1px solid black;" valign="top" width="36%">
				          <div style="font-weight: bold;font-family:Arial;font-size:12px;margin-left:5px;margin-top:5px;text-align: left; ">CARRIER SIGNATURE/PICKUP DATE</div>
				          <div style="font-size:7px;font-family:Arial;margin-left:5px;margin-right:5px;">Carrier 
acknowledges receipt of packages and required placards.Carrier certifies
 emergency response information was made available and/or carrier has 
the DOT emergency response guidebook or equivalent  documentation in the
 vehicle.</div>
				          <div style="font-style: italic;font-size:9px;font-weight: bold;margin:5px 5px 0px 5px;font-family:Arial;">Property described above is received in good order, except as noted.</div>
				          <div>&nbsp;</div>
				          <div>&nbsp;</div>
				          <div style="font-size:10px;font-family:Arial;margin-left:5px;">X&nbsp;</div>
				          <div style="border-bottom:1px solid black;font-family:Arial;font-size:12px;"></div>
				          <div style="font-size:10px;font-family:Arial;margin-left:5px;">
				          	<div style="float:left;">Signature/Print Name</div>
				          	<div style="float:right;padding-right:50px;">Date</div>
				          </div>
				          <div>&nbsp;</div>
				          <div>&nbsp;</div>
				          <div style="font-weight: bold;font-family:Arial;font-size:12px;margin-left:5px;">Time In:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time Out:</div>
				        </td>
				      </tr>
				    </tbody></table>
	        </td>
	      </tr>
	    </tbody></table>
	
	
	</div>
</div>
<!-- loop body end -->
<%} %>
<%} %>
</body>
</html>
<script>

function supportAndroidprint(){
	//获取打印机名字列表
   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
   	 //判断是否有该名字的打印机
   	var printer = "<%=printName%>";
   	var printerExist = "false";
   	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){
		return androidIsPrint();
	}else{
		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消
			return androidIsPrint();
		}
	}
}

function androidIsPrint(){
    var printHtml=$('div[name="printHtml"]');
    var flag = true ;
    for(var i=0;i<printHtml.length;i++){
   	 var a1=$('#a1',printHtml[i]);
   	 var a2=$('#a2',printHtml[i]);
   	 var a3=$('#a3',printHtml[i]);	
   	 //分页字体
   	 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
   	 
   	 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"A4");
   	 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",a1.html());
   	 visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);  
   	
   	 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a2.html());
   	 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
   	 
   	 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a3.html());
   	 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
   	 
   	 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#page_header').html());
		 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   
		 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");         
   	 //页码
   	 visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 
   	 
		 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);	

   	 
   	 visionariPrinter.SET_PRINT_COPIES(1);
   	 //visionariPrinter.PREVIEW();
   	flag = flag &&  visionariPrinter.PRINT();
    }		 
    return flag ;
}
</script>