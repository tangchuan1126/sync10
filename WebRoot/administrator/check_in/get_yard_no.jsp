<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey" %>

<%@ include file="../../include.jsp"%>
<%
	int psId = StringUtil.getInt(request,"storageId");
	long yc_id = StringUtil.getLong(request,"yc_id");
	long mainId = StringUtil.getLong(request,"mainId");
	String inputId=StringUtil.getString(request, "inputId");
	DBRow storageKml = googleMapsMgrCc.getStorageKmlByPsId(psId);
	String kmlName = "0";
	if(storageKml != null){
		kmlName = storageKml.getString("kml");
	}
	long spotZoneId = StringUtil.getLong(request,"spotZoneId");
	DBRow[] rows = checkInMgrZwb.getVacancySpot(psId,spotZoneId,mainId);         			 //可用停车位
	DBRow[] spots = checkInMgrZwb.findSpotArea(psId);
	DBRow[] selectUnavailableSpot = checkInMgrZwb.getOccupiedSpot(psId,spotZoneId,mainId);    //占用停车位
	
	
%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<head>
	
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">
	
	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

	<style type="text/css" media="all">
	<%--
	@import "../js/thickbox/global.css";
	
	--%>
	@import "../js/thickbox/thickbox.css";
	</style>
	<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
	<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
	<script src="../js/thickbox/global.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
	<script src="../js/jgrowl/jquery.jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css" />
	<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" />
	
	<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
	<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- GoogleMap -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&sensor=false"></script>
	<script src="../js/maps/GoogleMapsV3.js"></script> 
	<script src="../js/maps/jsmap.js"></script> 
	<!-- show message -->
	<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	
	<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
	</style>
	<script type="text/javascript">
	$(function(){
		$('#yc').focus();
		$("#tabs").tabs({
			select: function(event,ui){
				if(ui.index==0){
					window.setTimeout(function(){$("#yc").focus();},300);
					
				}
				
			}
		});
	});
	
	function closeWindow(){
		$.artDialog.close();
	}
	function searchAreaByYcId(yc){
		var ps_id=$('#ps_id').val();
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindStorageYardSearchAction.action',
	  		dataType:'json',
	  		async:false,
	  		data:"yc="+yc+'&ps_id='+ps_id+'&flag='+0+"&mainId="+<%=mainId%>,
	  		success:function(data){
	  			if(data.length>0){
	  				$("#spotArea").val(data[0].area_id);
	  			}
	  				
	  		}
	    });

	}
	function  confirmParking(flag,yc){
		var yc_no="";
		var yc_id="";
		if(flag==1){
			 yc_no = yc;
			 yc_id = $("#yc_id").val();
		}else{
			 yc_no = $("input[type='radio']:checked").val();
			 yc_id = $("input[type='radio']:checked").attr("id");
		}
		searchAreaByYcId(yc);
		var spotArea=$("#spotArea").val();
		$.artDialog.opener.setYardNo(yc_id,yc_no,'<%=inputId%>',spotArea);
		$.artDialog.close();
	}
	function searhYc(flag){
		var yc=$('#yc').val();
		var ps_id=$('#ps_id').val();
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindStorageYardSearchAction.action',
	  		dataType:'json',
	  		data:"yc="+yc+'&ps_id='+ps_id+"&flag="+flag+"&mainId="+<%=mainId%>,
	  		success:function(data){
	  			if(data.length>0){
	  				$("#search_tb").append('<input name="yc_no" type="hidden" id="yc_id" value="'+data[0].yc_id+'"/>');
		  				confirmParking(1,data[0].yc_no);
		  				return false;
	  			}else{
	  				showMessage("Spot not found or occupied!","alert");
	  				return false;
	  			}
	  		}
	  	});
	}
	function getStorageYardInMap(){
		var width = window.parent.innerWidth;
		var height = window.parent.innerHeight;
		width = parseInt(width*0.9);
		height = parseInt(height*0.9);
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/getParkingDocksInMap.html?storageId='+<%=psId%>+'&type=parking&width='+width+'&height='+height; 
		$.artDialog.open(uri , {title: "Map",width:width+'px',height:height+'px', lock: true,opacity: 0.3,fixed: true});
	}
	function getDoorOrParking(yardId,yardNo,inputId,zoneId){
		$.artDialog.opener.setYardNo(yardId,yardNo,inputId,zoneId);
		$.artDialog && $.artDialog.close();
	}
	
	function search(el,flag){
		var yc=$('#yc').val();
		var ps_id=$('#ps_id').val();
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindStorageYardSearchAction.action',
	  		dataType:'json',
	  		data:"yc="+yc+'&ps_id='+ps_id+"&flag="+flag+"&mainId="+<%=mainId%>,
	  		success:function(data){
	  			var html='';
	  			for(var i=0;i<data.length;i++){
			  			html += '<tr ondblclick="doubleClick(this)" onclick="singleClick(this)" name="yc_tr">'+
			  			'<td style="border-bottom:1px solid #dddddd;width:10px;height:30px" align="center" >'+
			  			'<input name="yc_no" type="radio" id="'+data[i].yc_id+'" value="'+data[i].yc_no+'"/>'+
			  			'</td>'+
			  			'<td style="border-bottom:1px solid #dddddd ;width:150px;height:30px" >'+
			  			'<font color="green">Spot['+data[i].yc_no+']</font>'+
			  			'</td>'
			  			'</tr>';		  			
		  		}
		  		$("#storageYc").html(html);
		  	}
	  	});
	}
	
	//可用门 改变 zone 方法
	function findSpotByZoneId(spotArea){
		$('#yc').val('');
			$.ajax({
		  		type:'post',
		  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindStorageYardSearchAction.action',
		  		dataType:'json',
		  		data:"spotArea="+spotArea+'&ps_id='+<%=psId%>+"&flag="+5+"&mainId="+<%=mainId%>,
		  		success:function(data){
		  			var html='';
		  			for(var i=0;i<data.length;i++){
				  			var yc_id = 0;
				  			if(data[i].yc_id==yc_id){
					  			html += '<tr ondblclick="doubleClick(this)" onclick="singleClick(this)" name="yc_tr" bgcolor="#E6F3C5">'+
					  			'<td style="border-bottom:1px solid #dddddd;width:10px;height:30px" align="center" >'+
					  			'<input name="yc_no" type="radio" id="'+data[i].yc_id+'" checked="checked" value="'+data[i].yc_no+'"/>'+
					  			'</td>'+
					  			'<td style="border-bottom:1px solid #dddddd ;width:150px;height:30px" >'+
					  			'<font color="green">Spot['+data[i].yc_no+']</font>'+
					  			'</td>'
					  			'</tr>';
				  			}else{
					  			html += '<tr ondblclick="doubleClick(this)" onclick="singleClick(this)" name="yc_tr">'+
					  			'<td style="border-bottom:1px solid #dddddd;width:10px;height:30px" align="center" >'+
					  			'<input name="yc_no" type="radio" id="'+data[i].yc_id+'" value="'+data[i].yc_no+'"/>'+
					  			'</td>'+
					  			'<td style="border-bottom:1px solid #dddddd ;width:150px;height:30px" >'+
					  			'<font color="green">Spot['+data[i].yc_no+']</font>'+
					  			'</td>'
					  			'</tr>';
				  			}
		  			}
		  			$("#storageYc").html(html);
		  		}
		 });
	}
	
	//同步
	function tongbu(){
		var spotArea='';
		if($("#tabs").tabs("option","selected")==0){
			spotArea = $("#spotArea").val();  //可用停车位zone
			$("#usespot").val(spotArea);      //占用停车位 zone
		}else{
			spotArea = $("#usespot").val();  //占用停车位 zone
			$("#spotArea").val(spotArea);    //可用停车位 zone
		}
		findSpotByZoneId(spotArea);   //可用门zone
		findUseSpotByZoneId(spotArea);//占用门 zone
	}
	//占用门 改变 zone 方法
	function findUseSpotByZoneId(spotArea){
		
			$.ajax({
		  		type:'post',
		  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindUnavailableSpotAction.action',
		  		dataType:'json',
		  		data:"spotArea="+spotArea+'&ps_id='+<%=psId%>+"&flag="+6+"&mainId="+<%=mainId%>,
		  		success:function(data){
		  			var html='';
		  			if(data.length>0){
		  				for(var i=0;i<data.length;i++){
		  					
								html += '<tr>'+
								'<td style="border-bottom:1px solid #dddddd;" width="" height="30px" align="center">'+
								'<font color="red">Spot['+data[i].yc_no+']</font>'+
								'</td>';							
								if(data[i].detail!=null){
									html += '<td style="border-bottom:1px solid #dddddd" >'+
									'<font>'+data[i].detail+'</font></td>';
								}else{
									html += '<td style="border-bottom:1px solid #dddddd" >'+
									'<font>&nbsp; </font></td>';
								}
								html +='</tr> ';
							
			  			}
		  				
		  			}
			  			$("#usestorageYc").html(html);
		  		}
		    });
	}
	</script>
</head>
<body>
	<div id="tabs">
		<ul>
			<li><a href="#vacancy">Available</a></li>	
			<li><a href="#occupied">Occupied</a></li>	
		</ul>
	    <div id="vacancy">
	    	<form name="search_form">
		       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="search_tb">
			     	<tr>
			     		<td  bgcolor="#dddddd" style="padding-left:5px;font-weight:bold;height:30px">
			     		  <input type="text" id="yc" name="yc" onkeyup="search(this,2);" onkeypress="if(event.keyCode==13){searhYc(3);return false;}"/>&nbsp;
	<!--  	     		  <input type="button"  value="Find" class="button_long_search" onclick="search(this,2);" />-->	
			     		  <select  id="spotArea" onchange="tongbu()" >
									  <option value="">Spot Area</option>
						     		  <%
						     		  		if(spots!=null && spots.length>0){
						     		  			for(int i=0;i<spots.length;i++){
						     		  %>
						     		  <option value="<%=spots[i].get("area_id",0)%>" <%=spots[i].get("area_id",0)==spotZoneId?"selected":""%>> <%=spots[i].get("area_name","")%></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  	  </select>
			     		  <input type="hidden" id="ps_id" name="ps_id" value="<%=psId %>"/>
			     		</td>
			     		<td align="right" bgcolor="#dddddd" style="padding-left:5px;font-weight:bold;height:30px">
			     			<input class="long-button"  type="button" onclick="getStorageYardInMap()" value="Map" >&nbsp;
			     		</td>
			     	</tr>
			    </table>
		    </form>
	    	<div align="center" style=" margin-top:2px; height:404px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">		
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					    <td>
						    <div  style="background-color:#FFF;width:96%; border:2px #dddddd solid; height:360px; float:left; overflow:auto;padding-left:10px;padding-right:10px" >
				        		 <table id="storageYc" width="100%" border="0" cellspacing="0" cellpadding="0" >
				        		 <%if(rows != null && rows.length > 0){
							      		for(DBRow row : rows){
						         %>
						            	<tr ondblclick="doubleClick(this)" onclick="singleClick(this)" name="yc_tr"  >  
						        		   <td  style="border-bottom:1px solid #dddddd;width:10px;height:30px" align="center">
						        	       		<input name="yc_no" type="radio" id="<%=row.getString("yc_id")  %>" <%=yc_id==row.get("yc_id",0)?"checked":""%>  value="<%=row.getString("yc_no") %>"/>
						        	       </td>
						        	       <td style="border-bottom:1px solid #dddddd ;width:150px;height:30px">
												<font color="green">&nbsp;Spot[<%=row.getString("yc_no") %>]</font>
						        	       </td>
						        	     
						        	   </tr>
						        <%
						        	    	 
							      		}
							      	}
						        %>
								 </table>
				      		</div>
					    </td>				   
					</tr>
				</table>
				<input class="normal-green" type="button"  value="Confirm" onclick="confirmParking()" onkeypress="if(event.keyCode==13){confirmParking(2);return false;}">
				<input class="normal-green" type="button"  value="Cancel" onclick="closeWindow();" onkeypress="if(event.keyCode==13){closeWindow();return false;}">
			</div>         
	    </div>
	    <div id="occupied">
	    	 <table width="100%" border="0" cellspacing="0" cellpadding="0" >
		    	<tr>
		     		<td width="100%" height="25px" bgcolor="#dddddd" style="padding-left:5px;border-right:1px solid #eeeeee;font-weight:bold">Spot
		     		 <select  id="usespot" onchange="tongbu()" >
								  <option value="">Spot Area</option>
					     		  <%
					     		  		if(spots!=null && spots.length>0){
					     		  			for(int i=0;i<spots.length;i++){
					     		  %>
					     		  <option value="<%=spots[i].get("area_id",0)%>" <%=spots[i].get("area_id",0)==spotZoneId?"selected":""%>> <%=spots[i].get("area_name","")%></option>
					     		  <%			
					     		  		    }
					     		  		}
					     		  %>
	     		  	  </select>
		     		</td>	     
		     	</tr>
		     </table>
	    	       
	    	 <div align="left" style=" margin-top:2px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">		
		     	<div style="background-color:#FFF;border-bottom:1px solid #dddddd;padding-left:8px;">	
					<table id="usestorageYc" width="100%" border="0" cellspacing="0" cellpadding="0">
					
						<%if(selectUnavailableSpot != null && selectUnavailableSpot.length > 0){
							      		for(DBRow row : selectUnavailableSpot){
						         %>
						            	<tr ondblclick="doubleClick(this)" onclick="singleClick(this)" name="yc_tr"  >
						        	       <td style="border-bottom:1px solid #dddddd" width="" height="30px" align="center">
							        	       <font color="red">Spot[<%=row.getString("yc_no") %>]</font>
						        	       </td>   
							               <td style="border-bottom:1px solid #dddddd ;" ><%=row.get("detail","") %></td>
						        	   </tr>
						        <%
						        	    	 
							      		}
							      	}
						        %>
					</table>
				</div>
			</div>         
	    </div>
	 </div>
</body>
<script type="text/javascript">
	function doubleClick(tr){               //双击
		var ra=$('input[name="yc_no"]',tr);
		var val= ra.val();
		var id=ra.attr("id");
		confirmParking(id,val);
	}

	function singleClick(tr){              //单击
		var yc_tr=$('tr[name="yc_tr"]');
		for(var i=0;i<yc_tr.length;i++){     //先去掉所有颜色
			$(yc_tr[i]).attr('bgcolor',"");
		}
		var ra=$('input[name="yc_no"]',tr);
		$(tr).attr('bgcolor','#E6F3C5');
		ra.attr("checked","checked");
	}
</script>