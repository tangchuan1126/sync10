<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInEntryWaitingOrNotKey" %>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<%@page import="com.fr.base.core.json.JSONArray"%>
<%@page import="com.fr.base.core.json.JSONObject"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="checkInEntryWaitingOrNotKey" class="com.cwc.app.key.CheckInEntryWaitingOrNotKey"/> 
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script> 
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long adminId=adminLoggerBean.getAdid();
	long id=StringUtil.getLong(request,"id");
	
	DBRow psRow=adminMgrZwb.findAdminPs(adminId);
	String ps_name	=psRow.get("title","") ;  
	long psId = adminLoggerBean.getPs_id();
	String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/check_in/check_in_window.html?id="+id;
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	String checkInFileUploadAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/checkin/checkInFileUploadAction.action";
	DBRow[] files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(id,FileWithTypeKey.OCCUPANCY_MAIN);
	//查询详细

	DBRow row=checkInMgrZwb.findMainById(id);
	//查询spot 下zone
	DBRow[] spots = checkInMgrZwb.findSpotArea(psId);
	String spotString = StringUtil.convertDBRowArrayToJsonString(spots);
	
%>

<title>Check In</title>
</head>
<body>
	<div style="height:390px; border:0px solid red;overflow:auto">
	    <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:770px;">
		    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="vertical-align: m">
			  <tr style="height: 27px;">
			  	<td style="border-bottom:1px #999 dashed;width:60px;" align="right">
			  		Entry ID&nbsp;:&nbsp;
			  	</td>
			  	<td style="border-bottom:1px #999 dashed;width:70px;" align="left">
			  		<span id="mId"><%=id %></span>
			  		<input type="hidden" id="det_detail_id" />
			  	</td>
			  	<td style="border-bottom:1px #999 dashed;width:123px;" align="left">
			  		<%if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.PICK_UP){%>
			   		 	      <span id="entryType" style="color:blue;font-weight: bold;display: none;">PICK UP</span>
			   		 	<%}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.DELIVERY){%>
			   		 	 	  <span id="entryType" style="color:blue;font-weight: bold;display: none;">DELIVERY</span>
			   		 	<%}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.BOTH){ %>
			   		 		  <span id="entryType" style="color:blue;font-weight: bold;display: none;">BOTH</span>
			   		 	<%}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.NONE){ %>
			   		 		  <span id="entryType" style="color:blue;font-weight: bold;display: none;">NONE</span>
			   		 	<%}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.PICKUP_CTNR){ %>
			   		 		  <span id="entryType" style="color:blue;font-weight: bold;display: none;">PickUp CTNR</span>
			   		 	<%}else{ %>
			   		 		  <span id="entryType" style="color:blue;font-weight: bold;display: none;">&nbsp;</span>
			   		 	<%} %>
			   		 	<select id="relType" style="width:110px;">		   
						  	<option value="1" <%=row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.DELIVERY?"selected":""%> >Delivery</option>
						  	<option value="2" <%=row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.PICK_UP?"selected":""%>>PICK UP</option>
						  	<option value="3" <%=row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.BOTH?"selected":""%>>Both</option>
						  	<option value="4" <%=row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.NONE?"selected":""%>>None</option>
						  	<option value="5" <%=row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.PICKUP_CTNR?"selected":""%>>PickUp CTNR</option>
						</select>
			  	</td>
			  	<td style="border-bottom:1px #999 dashed;width:130px;" align="left">
			  		<%if(row.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.LIVE){%>
				   			<span style="color:blue;font-weight: bold;display: none;" id="live">Live Load</span>
				   		<%}else if(row.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.DROP){%>
				   			<span style="color:blue;font-weight: bold;display: none;" id="live">Drop Off</span>
			   			<%}else if(row.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.SWAP){%>
			   				<span style="color:blue;font-weight: bold;display: none;" id="live">Swap CTNR</span>
<%-- 			   			<%}else if(row.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.PICK_UP){%> --%>
<!-- 			   				<span style="color:blue;font-weight: bold;" id="live">PickUp CRNR</span> -->
				   		<%}else{%>
				   			<span style="color:blue;font-weight: bold;display: none;" id="live">&nbsp;</span>
				   		<%} %>
				   		<select id="liveType" style="width:100px;">		   
						  	<option value="<%=CheckInLiveLoadOrDropOffKey.LIVE %>" <%=row.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.LIVE?"selected":""%> >Live Load</option>
						  	<option value="<%=CheckInLiveLoadOrDropOffKey.DROP %>" <%=row.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.DROP?"selected":""%>>Drop Off</option>	
						  	<option value="<%=CheckInLiveLoadOrDropOffKey.SWAP %>" <%=row.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.SWAP?"selected":""%>>Swap CTNR</option>	
<%-- 						  	<option value="<%=CheckInLiveLoadOrDropOffKey.PICK_UP %>" <%=row.get("isLive",0l)==CheckInLiveLoadOrDropOffKey.PICK_UP?"selected":""%>>PickUp CTNR</option>				 --%>
						</select>&nbsp;
			  	</td>
			  	<td style="border-bottom:1px #999 dashed;width:80px;" align="right">
			  		Priority&nbsp;:&nbsp;
			  	</td>
			  	<td style="border-bottom:1px #999 dashed;width:75px;" align="left">
		    		<select id="priority">
						<option value="30" <%=row.get("priority",0)==30?"selected":"" %>>30min</option>
						<option value="60" <%=row.get("priority",0)==60?"selected":"" %> >60min</option>
						<option value="120" <%=row.get("priority",0)==120?"selected":"" %> >120min</option>
						<option value="0" <%=row.get("priority",0)==0?"selected":"" %> >NA</option>
		    		</select>
			  	</td>
			  	<td>&nbsp;</td>
			    <td align="right" style="border-bottom:1px #999 dashed;">			    	
			    	<span style="color:green;font-weight:bold;" id="effectiveNumber">0</span>
			    	&nbsp;/&nbsp;
			    	<span style="font-weight:bold;" id="sumNumber">0</span>
			    	&nbsp;&nbsp;
			    	<input type="button" class="long-button-add" value="Add" onclick="yuAdd()" />
			    </td>
			  </tr>
			  <tr style="height: 27px;">
			  	<td style="border-bottom:1px #999 dashed;" align="right">
			  		LP&nbsp;:&nbsp;
			  	</td>
			  	<td style="border-bottom:1px #999 dashed;" colspan="3" align="left">
			  		<%=row.getString("gate_liscense_plate") %>
			  	</td>
			  	<td style="border-bottom:1px #999 dashed;" align="right">
			  		Driver Name&nbsp;:&nbsp;
			  	</td>
			  	<td style="border-bottom:1px #999 dashed;" align="left" colspan="2">
			  		<%=row.getString("gate_driver_name") %>
			  	</td>
			  	<td style="border-bottom:1px #999 dashed;padding-right: 90px;" align="right">
			  		<%if(row.get("waiting", 0) == CheckInEntryWaitingOrNotKey.WAITING){%>
			    		<span style="color: #f60;"><%=checkInEntryWaitingOrNotKey.getCheckInEntryWaitingOrNotKeyValue(row.get("waiting", 0)) %></span>
				   	<% }%>&nbsp;
			  	</td>
			  </tr>
			</table>
			<div style="padding-left: 19px;padding-top: 5px;">
				Delivery Seal: <input type="text" id="in_seal" name="in_seal" value="<%=row.get("in_seal","")%>" onkeyup="this.value=this.value.toUpperCase();"/>
				<input class="long-long-button" type="button" onclick="setNA(1)" value="No Delivery Seal">&nbsp;&nbsp;
				PickUp Seal: <input type="text" id="out_seal" name="out_seal" value="<%=row.get("out_seal","")%>" onkeyup="this.value=this.value.toUpperCase();"/>
				<input class="long-button" type="button" onclick="setNA(2)" value="No PickUp Seal">
			</div> 	    
	    </div>
	    <br/>
	    <div id="Container" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:770px;">
		  
	    </div>
    </div>
    <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;width:770px;height:65px;overflow:auto">
	      <form id="fileform" method="post" action="<%=checkInFileUploadAction %>">
	    	<table id="add_file">
	    		<tr>
	    			<td>
	    				<input type="button" class="long-button" onclick="onlineScanner('onlineScanner');" value="Multiple Photos" />
	    			</td>
					<td>
						<input type="button" id="online" class="long-button" onclick="onlineSingleScanner('onlineScanner');" value="Single Photo" />
						<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="Upload Photo" />
					</td>
				</tr>
				<tr>
				</tr>
				<%
				 if(files != null && files.length > 0 ){
		      		for(DBRow file : files){
				%>
				<tr id="file_tr_<%=file.getString("file_id") %>">
					<td><%=DateUtil.showLocalparseDateTo24Hours(file.getString("upload_time") , psId) %></td>
					<td>
					 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
                 <%if(StringUtil.isPictureFile(file.getString("file_name"))){ %>
			             <p>
			 	            <a style="color:#439B89" id="filename" href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.OCCUPANCY_MAIN%>','<%=id%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
			             </p>
			     <%}else{ %>
							<a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder= check_in'><%=file.getString("file_name") %></a>
				 <%}%>
					</td>
					<td width="30px"></td>
					<td>
						<a  href="javascript:deleteFile('<%=file.get("file_id",0l) %>')" style="color:red" >×</a>
					</td>	
				 </tr>	 
				<%}}%>
				<tr>
					<input type="hidden" name="backurl" id="backurl" value="" />
	 				<input type="hidden" name="file_with_type" value="<%=FileWithTypeKey.OCCUPANCY_MAIN %>" />
	 				<input type="hidden" name="sn" id="sn" value="window_check_in"/>
				 	<input type="hidden" name="file_names" id="file_names"/>
				 	<input type="hidden" name="path" value="check_in"/>
				 	<input type="hidden" name="dlo_id" value="<%=id%>"/>
				 	<input type="hidden" name="creator" value="<%=adminId%>"/>
				 	<input type="hidden" name="appointmentdate" id="appointmentdate" value="<%=!row.getString("appointment_time").equals("")?  DateUtil.showLocalTime(row.getString("appointment_time"), psId) :""%>"/>
				</tr>
	    	  </table>
	      </form>
	</div>
    <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;width:770px;margin-top:2px;">
   		<input class="normal-green-long" type="button" onclick="batchPrint()" value="BatchPrint" >
   		<input class="normal-green" type="button" onclick="tijiao(<%=CheckInEntryWaitingOrNotKey.WAITING %>)" value="Waiting" >
    	<input class="normal-green" type="button" onclick="tijiao()" value="Check In" >
    </div>
    <div id="print" style="display:none">
    	
    </div>
    <div id="print_wms" style="display:none;">
    	
    </div>
</body>
</html>

<script>
var spotJsonArray = eval('<%=spotString%>');
var mainContainerNo = '<%=row.getString("gate_container_no")%>';
var firstHasValueContainerNo = '';
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();
    var p=0;
    var in_serch_val='';
      
    (function(){
		fantian();
	})();

	function yuAdd(){
		var entryType='';
		var delivery_checked='';
		var pick_up_checked='';
		var zone_id='';
		var detail_id='';
		var num_id='';
		var door_name='';
		var door_id='';
		var noticeName='';
		var noticeNameId='';
		var note='';
		var customer_id='';
		var company_id='';
		var num_type='';
		var order='';
		var containerNo='';
		var door_checked='';
		var spot_checked='';
		var yu_jia_type=$('#entryType').html();
		var title='';

		if(yu_jia_type=='DELIVERY'){
			delivery_checked='checked';
			entryType='DELIVERY';
			title="CTNR/BOL";
		}else{
			pick_up_checked='checked';
			entryType='PICK UP';
			title="LOAD/PO/ORDER";
		}
		
		//点add时 如果有 通知的人 就默认加上
		var winDiv=$('div[name="av"]');
		if(winDiv.length>0){
			var tongzhi_name=$('input[name="tongzhi"]',winDiv[0]).val();
			var tongzhi_id=$('input[name="tongzhiid"]',winDiv[0]).val();
			noticeName=tongzhi_name;
			noticeNameId=tongzhi_id;
			var container_no=$('input[name="containerNo"]',winDiv[0]).val();
			containerNo=container_no;
			var door_spot=$('select[name="door_spot"]',winDiv[0]).val();
			if(door_spot==<%=OccupyTypeKey.DOOR%>){
				door_checked='selected';
			}else{
				spot_checked='selected';
			}
		}
		add(entryType,delivery_checked,pick_up_checked,zone_id,detail_id,num_id,door_name,door_id,noticeName,noticeNameId,note,num_type,customer_id,company_id,order,'','',title,'',containerNo,door_checked,spot_checked);
		//计算条数
		calculationAv();
		//计算序列号
		sequence();
	}
	
	function fantian(){
		var id=<%=id%>;
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindOccupancyDetailsWindowAction.action',
	  		dataType:'json',
	  		data:'id='+id,
		 	success:function(data){
				for(var i=0;i<data.length;i++){
				//	alert(data[i].occupy_name);
					var entryType='';
					var delivery_checked='';
					var pick_up_checked='';
					var zone_id='';
					var detail_id='';
					var num_id='';
					var door_name='';
					var door_id='';
					var noticeName='';
					var noticeNameId='';
					var email='';
					var page='';
					var message='';
					var note='';
					var supplier_id='';
					var order=data[i].order_no;
					var num_type='';
					var customer_id='';
					var company_id='';
					var staging_area_id='';
				    var freight_term='';
					var title='';
					var containerNo='';
					var occupancy_type='';
					var spot_id='';
					var door_checked='';
					var spot_checked='';
					var number_status='';
					if(data[i].number_status!=undefined){
						number_status=data[i].number_status;
					}
					
					if(data[i].occupancy_type!=undefined){
						occupancy_type=data[i].occupancy_type;
					}
		
					if(occupancy_type==<%=OccupyTypeKey.DOOR%>){
						door_checked='selected';
					}else if(occupancy_type==<%=OccupyTypeKey.SPOT%>){
						spot_checked='selected';
					}
					
					if(data[0].ctn_number!=undefined){
						containerNo=data[0].ctn_number;
					}else{
						containerNo='<%=row.get("gate_container_no","")%>';
					}
					if(data[i].company_id!=undefined){
						company_id=data[i].company_id;
					}
					if(data[i].customer_id!=undefined){
						customer_id=data[i].customer_id;
					}
					if(data[i].number_type!=undefined){
						num_type=data[i].number_type;
					}
					if(data[i].number!=undefined){
						num_id=data[i].number.toUpperCase();
					}
					if(data[i].supplier_id!=undefined){
						supplier_id=data[i].supplier_id;
					}
					if(data[i].staging_area_id!=undefined){
						staging_area_id=data[i].staging_area_id;
					}
					if(data[i].freight_term!=undefined){
						freight_term=data[i].freight_term;
					}
					if(data[i].zone_id!=undefined){
						zone_id=data[i].zone_id;
					}				
					detail_id=data[i].dlo_detail_id;
					if(data[i].occupy_name!=undefined){
						 door_name=data[i].occupy_name;
					}
					if(data[i].rl_id!=undefined){
						 door_id=data[i].rl_id;
					}
					if(num_type==<%=ModuleKey.CHECK_IN_PONO%>){
						title="PO";
					}
					if(num_type==<%=ModuleKey.CHECK_IN_ORDER%>){
						title="ORDER";
					}
					if(num_type==<%=ModuleKey.CHECK_IN_LOAD%>){
						title="LOAD";
					}
					
					if(num_type==<%=ModuleKey.CHECK_IN_CTN%>){
						title="CTNR";
					}
					if(num_type==<%=ModuleKey.CHECK_IN_BOL%>){
						title="BOL";
					}
					if(num_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%> || num_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>  ){
						title="OTHERS";
					}
					
					$.ajax({
				  		type:'post',
				  		async:false,
				  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxWindowFindNoticesAction.action',
				  		dataType:'json',
				  		data:'schedule_id='+detail_id,
					 	success:function(da){
					 		if(da.length>0){
					 			email=da[0].sms_email_notify;
					 			message=da[0].sms_short_notify;
					 			page=da[0].schedule_is_note;
					 			for(var b=0;b<da.length;b++){					 				
					 				noticeName+=da[b].employe_name+',';
					 				noticeNameId+=da[b].adid+',';
					 			}
					 			var notes=da[0].schedule_detail.split("Note：");
					 			note=notes[1];
					 			if(note==undefined){
					 				note='';
					 			}
					 		}
					 	}
					 });	
					if(noticeName!=''){
						noticeName=noticeName.substring(0,noticeName.length-1);
						noticeNameId=noticeNameId.substring(0,noticeNameId.length-1);
					}	
					if(num_type==<%=ModuleKey.CHECK_IN_BOL%> || num_type==<%=ModuleKey.CHECK_IN_CTN%> || num_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS %>){
						delivery_checked='checked';	
						entryType='DELIVERY';
					}else if(num_type==<%=ModuleKey.CHECK_IN_LOAD%> || num_type==<%=ModuleKey.CHECK_IN_PONO%> || num_type==<%=ModuleKey.CHECK_IN_ORDER%> || num_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS %>){
						entryType='PICK UP';
						pick_up_checked='checked';
					}
					if(num_type=='' ){
						if(pick_up_checked=='checked'){
							title="LOAD/PO/ORDER";
						}else{
							title="CTNR/BOL";
						}
						
						
						
					}
					add(entryType,delivery_checked,pick_up_checked,zone_id,detail_id,num_id,door_name,door_id,noticeName,noticeNameId,note,num_type,customer_id,company_id,order,supplier_id,staging_area_id,title,freight_term,containerNo,door_checked,spot_checked,number_status);
				}
				//计算条数
				calculationAv();
				//计算有效的条数
				effectiveNumber();
				//计算序列号
				sequence();
	  		}
	    });	
		
	}
	
	function add(entryType,delivery_checked,pick_up_checked,zone_id,detail_id,num_id,door_name,door_id,noticeName,noticeNameId,note,num_type,customer_id,company_id,order,supplier_id,staging_area_id,title,freight_term,containerNo,door_checked,spot_checked,number_status){
		findFirstContainerNoHasValue();
		if(containerNo=='')
		{
			if(firstHasValueContainerNo!='')
			{
				containerNo = firstHasValueContainerNo;
			}
			else
			{
				containerNo = mainContainerNo;
			}
		}
		if(title==undefined){
			if(pick_up_checked=='checked'){
				title="LOAD/PO/ORDER";
			}else{
				title="CTNR/BOL";
			}
		}
		
		p++;
		var con=$('#Container');
     //   debugger;
		var html='';
		var raHtml='';
		var staging_zone_html='';
		
		var zoneHtml='';
		
		
		if(entryType=='NONE'){
			raHtml='<span name="ckName" style="margin-left:20px;">Pick Up</span>'+
            		'<input type="radio" name="ra'+p+'" id="ra" value="0" checked="checked" />'+
            		'<span name="ctnName" align="center" >Delivery</span>'+
            		'<input type="radio" name="ra'+p+'" id="ra" value="1" '+delivery_checked+' />';
		}else{
			raHtml='<span name="ckName" style="margin-left:20px;">Pick Up</span>'+
		    		'<input type="radio" name="ra'+p+'" id="ra" value="0" '+pick_up_checked+' />'+
		    		'<span name="ctnName" align="center" >Delivery</span>'+
		    		'<input type="radio" name="ra'+p+'" id="ra" value="1" '+delivery_checked+' />';
		}
		if(entryType=='PICK UP'){
			staging_zone_html= '<td width="110px;" align="right" height="25px;" id="stagingTitle">'+
					        	'Staging Location:&nbsp;'+
					            '</td>'+
					            '<td width="190px;" id="stagingVal">'+
					            	'<input id="staging'+p+'" disabled="disabled" size="20"   value="'+staging_area_id+'">'+
					            '</td>'+
					            '<td width="40px;" align="right" id="zoneTitle">'+
					            	'Zone:&nbsp;'+
					            '</td>'+
					            '<td width="140px;" id="zoneVal">'+
					            	'<input id="staging_zone'+p+'" disabled="disabled" size="20">'+
					            '</td>';
		}else{
				staging_zone_html= '<td width="110px;" align="right" height="25px;" id="stagingTitle">'+
						        		'Title:&nbsp;'+
						            '</td>'+
						            '<td width="190px;" id="stagingVal">'+
						            	'<input id="searchTitle'+p+'" disabled="disabled" size="20" value="'+supplier_id+'">'+
						            '</td>'+
						            '<td width="40px;" align="right" id="zoneTitle">'+
						            	'Zone:&nbsp;'+
						            '</td>'+
						            '<td width="140px;" id="zoneVal">'+
						            	'<input id="searchZone'+p+'" disabled="disabled" size="20">'+
						            '</td>';
		}
	   
			html='<div id="av" name="av">'+
				  '<table width="100%" border="0" cellspacing="0" cellpadding="0" >'+
	    			'<tr>'+
			        	'<td width="20px" style="border-right:1px solid #999;color:blue;font-weight:bold;" id="sequence" name="sequence" align="center"></td>'+
			            '<td width="" >'+
	                             '<table width="100%" border="0" cellspacing="0" cellpadding="0">'+
	                                  '<tr>'+
	                                    '<td colspan="6" style="border-bottom:1px #999 dashed;">'+
	                                   	  '<input id="number_status'+p+'" value="'+number_status+'" type="hidden" name="number_status" />'+
	                                  	  '<input id="companyId'+p+'" value="" type="hidden" />'+
	                                      '<input id="customerId'+p+'" value="" type="hidden" />'+
	                                      '<input id="zone'+p+'" value="'+zone_id+'" type="hidden" />'+
	                                      '<input id="detailid'+p+'" value="'+detail_id+'" type="hidden" />'+
	                                      	raHtml+
	                                     '</td>'+
	                                  '</tr>'+
	                                  '<tr>'+
	                                    '<td width="110px;" align="right" height="30px;"><span name="title" id="title'+p+'">'+title+':</span>&nbsp;</td>'+
	                                    '<td width="190px;"><input type="text" name="number" id="'+p+'"  size="20" value="'+num_id+'" />&nbsp;'+
	                                      '<input type="button"  class="short-short-button-search-email"  id="find'+p+'" value="Find" in="'+p+'"  name="find"/>'+
	                                      '<input type="hidden" name="number_type" id="number_type'+p+'"  size="25" value="'+num_type+'"  />'+
	                                      '<input type="hidden" name="customer_id" id="customer_id'+p+'"  size="25" value="'+customer_id+'"  />'+
	                                      '<input type="hidden" name="company_id" id="company_id'+p+'"  size="25" value="'+company_id+'"  />'+
	                                      '<input type="hidden" name="account_id" id="account_id'+p+'"  size="25" value=""  />'+
	                                      '<input type="hidden" name="order_no" id="order_no'+p+'"  size="25" value="'+order+'"  />'+
	                                      '<input type="hidden" name="po_no" id="po_no'+p+'"  size="25" value=""  />'+
	                                      '<input type="hidden" name="supplier_id" id="supplier_id'+p+'"  size="25" value="'+supplier_id+'"  />'+
	                                      '<input type="hidden" name="staging_area_id" id="staging_area_id'+p+'"  size="25" value="'+staging_area_id+'"  />'+
	                                      '<input type="hidden" name="freight_term" id="freight_term'+p+'"  size="25" value="'+freight_term+'"  /></td>'+
	                                    '<td width="40px;">'+
	                                    	'CTNR:&nbsp;'+
	                                    '</td>'+
	                                    '<td width="140px;">'+
	                                    	'<input type="text" name="containerNo" id="containerNo'+p+'"  size="20" value="'+containerNo+'" />'+
	                                    '</td>'+
	                                    '<td width="">'+
	                                    	'Operator:'+
	                                    '</td>'+
	                                    '<td width="">'+
		                                    '<input type="text" id="tongzhi'+p+'" name="tongzhi" size="20" value="'+noticeName+'" />'+
	    							   		'<input type="hidden" id="tongzhi'+p+'Id" name="tongzhiid" value="'+noticeNameId+'" />'+	                                   
		                                '</td>'+
	                                  '</tr>'+
	                                  '<tr>'+
		                                    '<td width="110px;" align="right" height="25px;">'+
		                                    	'<select name="door_spot" zhi="'+p+'" id="door_spot'+p+'"><option value="<%=OccupyTypeKey.DOOR%>" '+door_checked+' >Door</option><option value="<%=OccupyTypeKey.SPOT%>" '+spot_checked+'>Spot</option></select>&nbsp;'+
		                                    '</td>'+
		                                    '<td width="190px;">'+
		                                    	'<input type="text" name="door" id="door'+p+'" size="20" in="'+p+'"  value="'+door_name+'" text="'+door_id+'" />'+
		                                    '</td>'+
		                                    '<td width="40px;">'+
		                                    	'&nbsp;'+
		                                    '</td>'+
		                                    '<td width="140px;">'+
		                                    	'<select name="allzone" id="door'+p+'zone" zhi="'+p+'"><option value="0">N/A ZONE</option></select>'+
		                                    '</td>'+
		                                    '<td width="">'+
		                                    	'Email:<input id="youjian'+p+'" type="checkbox" checked="checked">'+
		                                    '</td>'+
		                                    '<td width="">'+
			                                    '&nbsp;Message:<input id="duanxin'+p+'" type="checkbox" checked="checked">&nbsp;Browser:<input id="yemian'+p+'" type="checkbox" checked="checked">'+	                                   
			                                '</td>'+
	                                  '</tr>'+
	                                  '<tr>'+
	                                  		staging_zone_html+
		                                    '<td width="">'+
		                                    	'&nbsp;'+
		                                    '</td>'+
		                                    '<td width="">'+
			                                    '&nbsp;'+	                                   
			                                '</td>'+
	                                '</tr>'+
	                                '<tr>'+
			                                '<td width="110px;" align="right" height="30px;">'+
			                                	'Note:&nbsp;'+
			                                '</td>'+
			                                '<td width="190px;" colspan="5">'+
			                                	'<textarea name="textfield" id="beizhu'+p+'" cols="89" rows="1" >'+note+'</textarea>'+
			                                '</td>'+		                                
		                          '</tr>'+
	                             '</table>'+
					  '</td>'+
					  '<td align="center">'+
					   		'<input type="button" value="Del" zhi="'+p+'" class="short-short-button-del" name="detButton" /><br/><br/>'+
                   			'<input type="button" class="short-short-button-print" value="Print" onclick="openLoadTemplate('+p+',0)" />'+
					  '</td>'+
				   '</tr>'+
				'</table>'+
				'<hr size="1"/>'+
			'</div>';
		 if(entryType=='NONE'){
			 if(p<2){
				html='';
			 }		 
		 }
	    
		var $container=$(html).appendTo(con);                       //获得容器
		var $door=$("input[name='door']",$container);               //获得门的文本框
	    var $number=$("input[name='number']",$container); 		    //单据id
	    var $number_type=$("input[name='number_type']",$container); //单据类型
	    var $allzone=$("select[name='allzone']",$container);        //获得zone的下拉
	    var $det_button=$("input[name='detButton']",$container);    //获得删除按钮
	    var $tongzhi=$("input[name='tongzhi']",$container);         //获得通知文本框
	    var $ra=$("input[name='ra"+p+"']",$container);              //单选按钮
	    var $customer_id=$("input[name='customer_id']",$container); //customer_id
	    var $company_id=$("input[name='company_id']",$container); 	//company_id
	    var $account_id=$("input[name='account_id']",$container); 	//account_id
	    var $order_no=$("input[name='order_no']",$container); 		//order_no
	    var $po_no=$("input[name='po_no']",$container); 			//po_no
	    var $supplier_id=$("input[name='supplier_id']",$container);
	    var $staging_area_id=$("input[name='staging_area_id']",$container);
	    var $freight_term=$("input[name='freight_term']",$container);
	    var $find=$("input[name='find']",$container);               //查询按钮
	    var $containerNo=$("input[name='containerNo']",$container); //货柜号
	    var $door_spot=$("select[name='door_spot']",$container);    //门或停车位下拉列表
	    var $number_status=$("input[name='number_status']",$container);    //是否可以删除状态
	    
	    //门或停车位下拉列表 改变事件
	    $door_spot.change(function(){
	    	 $door.val('');      //改变门或停车位时 清空值
	    	 var zhi=$door_spot.attr("zhi");
    		 var zone=$('#door'+p+'zone');
    		 $allzone.html('<option value="0">N/A ZONE</option>');
	    	 if($door_spot.val()==<%=OccupyTypeKey.SPOT%>){
	 		   	for(var y = 0; y < spotJsonArray.length; y ++){
	 		   		if(zone_id==spotJsonArray[y].area_id){
	 		   			$allzone.append('<option value='+spotJsonArray[y].area_id+' selected="selected" >'+spotJsonArray[y].area_name+'</option>');
	 		   		}else{
	 		   			$allzone.append('<option value='+spotJsonArray[y].area_id+'>'+spotJsonArray[y].area_name+'</option>');
	 		   		}
	 		   	}	
	 	    }else if($door_spot.val()==<%=OccupyTypeKey.DOOR%>){
	 	    	//查询本仓库下zone
	 		    $.ajax({
	 		  		type:'post',
	 		  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindAllZoneByPsIdAction.action',
	 		  		dataType:'json',
	 		  		data:'psId='+<%=psId%>,
	 			 	success:function(data){
	 						if(data.length>0){
	 							for(var i=0;i<data.length;i++){
	 								if(zone_id==data[i].area_id){
	 									$allzone.append('<option value='+data[i].area_id+' selected="selected" >'+data[i].area_name+'</option>');
	 								}else{
	 									$allzone.append('<option value='+data[i].area_id+'>'+data[i].area_name+'</option>');
	 								}
	 							}
	 						}
	 		  		}
	 			 });
	 	     }
	    });
	    
	    //判断是加载 停车位zone还是 门zone
	    if($door_spot.val()==<%=OccupyTypeKey.SPOT%>){
		   	for(var yy = 0; yy < spotJsonArray.length; yy ++){
		   		if(zone_id==spotJsonArray[yy].area_id){
		   			$allzone.append('<option value='+spotJsonArray[yy].area_id+' selected="selected" >'+spotJsonArray[yy].area_name+'</option>');
		   		}else{
		   			$allzone.append('<option value='+spotJsonArray[yy].area_id+'>'+spotJsonArray[yy].area_name+'</option>');
		   		}
		   	}
	    }else if($door_spot.val()==<%=OccupyTypeKey.DOOR%>){
	    	//查询本仓库下zone
		    $.ajax({
		  		type:'post',
		  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindAllZoneByPsIdAction.action',
		  		dataType:'json',
		  		data:'psId='+<%=psId%>,
			 	success:function(data){
						if(data.length>0){
							for(var i=0;i<data.length;i++){
								if(zone_id==data[i].area_id){
									$allzone.append('<option value='+data[i].area_id+' selected="selected" >'+data[i].area_name+'</option>');
								}else{
									$allzone.append('<option value='+data[i].area_id+'>'+data[i].area_name+'</option>');
								}
							}
						}
		  		}
			 });
	     }
	    
	    //绑定删除事件
	    $det_button.click(function(){
		    var zhi=$(this).attr("zhi");
		    var detail_id=$('#detailid'+zhi).val();
		    if($number_status.val()==<%=CheckInChildDocumentsStatusTypeKey.CLOSE%> || $number_status.val()==<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION%> || $number_status.val()==<%=CheckInChildDocumentsStatusTypeKey.PARTIALLY%>){
	    		alert("The task cannt delete");
	    		return false;
	    	}
	    	//删除操作 弹出窗口 提示 是否删除
	    	$.artDialog({
			    content: 'do you want to delete?',
			    icon: 'question',
			    width: 200,
			    height: 70,
			    title:'',
			    okVal: 'Confirm',
			    ok: function () {		
			    	if(detail_id!=""){		    		
			    		var det_detail_id=$('#det_detail_id').val();
			    		if(det_detail_id==''){
			    			det_detail_id+=detail_id;
			    		}else{
			    			det_detail_id+=','+detail_id;
			    		}
			    		$('#det_detail_id').val(det_detail_id);
			    		
				    }
			    	$container.remove('#av');	
			    	//计算条数
					calculationAv();
					//计算有效的条数,有numberType
					effectiveNumber();
					//计算序列号
					sequence();
					//计算第一个有值的containerNo
					findFirstContainerNoHasValue();
			    },
			    cancelVal: 'Cancel',
			    cancel: function(){
			    	
				}
			});	    	   		 	    	    	
		});

	    $allzone.change(function(){                                 //zone 改变事件
	    	var flag=1;
	        var zoneId=$(this).val();	
	        var num=$number.val();
	        var p=$allzone.attr('zhi');
	       
	         if($door_spot.val()==<%=OccupyTypeKey.SPOT%>){
	        	 findSpotByZoneId(3,zoneId,p);     //掉 打开停车位的方法
	         }else if($door_spot.val()==<%=OccupyTypeKey.DOOR%>){
	        	 findDoorsByZoneId(1,zoneId,$door,$allzone,$number,p);   //绑定change事件  调打开门的方法
	         }
	    });                                            

	    
	    $number.focus(function(){
	    	 if($(this).val()){
	    		 in_serch_val=$(this).val(); 
	    	 } 
	    });

 		$number.blur(function(){  //子单据id 失去焦点时验证
 			 var id=$(this).attr("id");
 			 var ra=$("input[name='ra"+id+"']:checked").val();
			 
 			 var orderId=$(this).val().toUpperCase();
 			 var order_type='';
 			if(ra==0){
 				order_type=1;	
			}else if(ra==1){
				order_type=2;	
			}
 			//debugger;
 			 var numbers=$('input[name="number"]');
 			 var judge=0;
 			 for(var i=0;i<numbers.length;i++){
 				var numi=$(numbers[i]).attr("id");
 				var rai=$("input[name='ra"+numi+"']:checked").val();
 				if($(this).val()==$(numbers[i]).val() && ra==rai){
  					judge++;
  				}
 			 }
 
	         if(judge>=2){
	        	 showMessage("This is duplicate record!","alert");
	        	 $number.val('');
	        	 $number.focus();
	        	 return false;
	         }
	        
	         
// 			 if($number.val()!="" && in_serch_val!=$number.val() ){   //验证是否重复，如果不重复 清空number_type 需要重新回车查询
// 				 $('#number_type'+id).val('');	
// 	 				$('#customer_id'+id).val('');
// 	 				$('#company_id'+id).val('');
// 	 				$('#account_id'+id).val('');
// 	 				$('#order_no'+id).val('');
// 	 				$('#po_no'+id).val('');
// 	 				$('#freight_term'+id).val('');
	 				
// 				 $.ajax({
//  					  		type:'post',
<%--  					  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCheckInFindBillIsExistAction.action',  --%>
//    					  		dataType:'json',
<%--  					  		data:'orderId='+orderId+'&type='+order_type+'&ps_id='+<%=psId%>,  --%>
//  						 	success:function(data){
//  					  			if($("input[name='number']",$container).attr("id")!='1'&& data.length>0){
//  					  				showMessage("This is duplicate record!","alert");
//  									$("input[name='number'][id!='1']",$container).val("");
//  									$('#number_type'+id).val('');	
//  					 				$('#customer_id'+id).val('');
//  					 				$('#company_id'+id).val('');
//  					 				$('#account_id'+id).val('');
//  					 				$('#order_no'+id).val('');
//  					 				$('#po_no'+id).val('');
//  					 				$('#freight_term'+id).val('');
//  					  			}else{
//  					  				$('#number_type'+id).val('');
//  					  			}
//  					  		}
//  					 });
//  			 }
			 findFirstContainerNoHasValue();
 	    });   
	    
		$number.keyup(function(event){             //输入转换成大写
			 this.value=this.value.toUpperCase();
			 this.value=trim(this.value);
			 var id=$(this).attr("id");
			 if(event.keyCode!=13){
				 $('#number_type'+id).val('');
			 }
		 });
		
		
		$containerNo.keyup(function(){         //去空格转大写
			 this.value=this.value.toUpperCase();
			 this.value=trim(this.value);
		});
		
		$number.keypress(function(event){
			if(event.keyCode==13){
				 in_serch_val=$number.val();
				 var id=$(this).attr("id");
				 var ra=$("input[name='ra"+id+"']:checked").val();
				 $(this).val($(this).val().toUpperCase());
				 var type='';
				 if(ra==0){	
					type=1;
				 }
				if($door!="" && $number.val()!=""){
					$.ajax({
						url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/getLoadingAction.action',
						data:'search_number='+$number.val()+'&number_type='+type+'&ps_id=<%=psId%>'+'&mainId='+<%=id%>,
						dataType:'json',
						type:'post',
						beforeSend:function(request){
					      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
					    },
						success:function(data){
							$.unblockUI();       //遮罩关闭
	//						datas=escape(datas);    //转码
	//debugger;
							if(data.num==0){
								alert("Record not found!");
								$allzone.val(1);
								$door.val("");
								$door.attr("text","");
								var order_type="";
								if(type==1){
									$('#staging'+id).val("");
									$('#staging_zone'+id).val("");
									order_type=<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>;
								}else{
									$('#searchTitle'+id).val("");
									$('#searchZone'+id).val("");
									order_type=<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%>;
								}
								$number_type.val(order_type);
								
								$('#title'+id).html("OTHERS :");
								//计算有效的条数
								effectiveNumber();
							}
							if(data.num==1){
				
								if(type==1){
									$door.val(data.door_name);
									$door.attr("text",data.door_id);
									$allzone.val(data.area_id);
									
									$('#staging'+id).val(data.stagingareaid);
									$('#staging_zone'+id).val(data.area_name);
								}else{
									$door.val(data.door_name);
									$door.attr("text",data.door_id);
									$allzone.val(data.area_id);
									
									findZoneByTitleName($('#searchTitle'+id),$('#searchZone'+id),data.supplierid);
									//$('#searchTitle'+id).val(data.customerid);
									//$('#searchZone'+id).val(data.area_id);
								}
								if(data.appointmentdate && id==1){
									$("#appointmentdate").val(data.appointmentdate);
									
								}
						//		alert($("#appointmentdate").val());
						//		alert(data.container_no);
							      if(data.occupancy_type==<%=OccupyTypeKey.SPOT%>){
									$door_spot.val(<%=OccupyTypeKey.SPOT%>);
									$allzone.html('');
									for(var yy = 0; yy < spotJsonArray.length; yy ++){
								   		if(zone_id==spotJsonArray[yy].area_id){
								   			$allzone.append('<option value='+spotJsonArray[yy].area_id+' selected="selected" >'+spotJsonArray[yy].area_name+'</option>');
								   		}else{
								   			$allzone.append('<option value='+spotJsonArray[yy].area_id+'>'+spotJsonArray[yy].area_name+'</option>');
								   		}
								   	}
							}else{
									$door_spot.val(<%=OccupyTypeKey.DOOR%>);
									$allzone.html('');
									//查询本仓库下zone
								    $.ajax({
								  		type:'post',
								  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindAllZoneByPsIdAction.action',
								  		dataType:'json',
								  		data:'psId='+<%=psId%>,
									 	success:function(data){
												if(data.length>0){
													for(var i=0;i<data.length;i++){
														if(zone_id==data[i].area_id){
															$allzone.append('<option value='+data[i].area_id+' selected="selected" >'+data[i].area_name+'</option>');
														}else{
															$allzone.append('<option value='+data[i].area_id+'>'+data[i].area_name+'</option>');
														}
													}
												}
								  		}
									 });
							}

								$company_id.val(data.companyid);
								$customer_id.val(data.customerid);
								$account_id.val(data.accountid);
								$supplier_id.val(data.supplierid);
								$staging_area_id.val(data.stagingareaid);
								$number_type.val(data.order_type);
								$freight_term.val(data.freightterm);
								var title=""
								if(data.order_type==<%=ModuleKey.CHECK_IN_PONO%>){
									$order_no.val(data.orderno);
									title="PO";
								}
								if(data.order_type==<%=ModuleKey.CHECK_IN_ORDER%>){
									$po_no.val(data.pono);
									title="ORDER";

								}
								if(data.order_type==<%=ModuleKey.CHECK_IN_LOAD%>){
									title="LOAD";
								}

								
								if(data.order_type==<%=ModuleKey.CHECK_IN_CTN%>){
									title="CTNR";
									$containerNo.val(data.container_no);
								}
								if(data.order_type==<%=ModuleKey.CHECK_IN_BOL%>){
									title="BOL";
									$containerNo.val(data.container_no);
								}
								if(data.order_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%> || data.order_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>)
								{
									title="OTHERS";
									if(data.container_no!='' && data.container_no!='NA'){$containerNo.val(data.container_no);}
								}
								$('#title'+id).html(title+":");
								//计算有效的条数
								effectiveNumber();
							}				
							if(data.num * 1 == -1){
								var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/task_number_wms_select_data.html?number='+$number.val()+'&id='+id+'&datas='+escape(jQuery.fn.toJSON(data.pickup)); 
								$.artDialog.open(uri , {title: "Select",width:'800px',height:'500px', lock: true,opacity: 0.3,fixed: true});
							}
							//if(data.flag==1 && (in_serch_val!=$number.val() || <%=id%> != data.mainid)){
							if(data.flag==1){
								if(ra==0){
									title="LOAD/PO/ORDER";
								}else{
									title="CTNR/BOL";
								}
								$('#title'+id).html(title+":");
								showMessage("This is duplicate record!","alert");
								$number.val("");
							}
							findFirstContainerNoHasValue();
						},
						error:function(){
							$.unblockUI();       //遮罩关闭
							alert("System error"); 
						}
					});
				}else{
					if(type==1){
						alert("Please enter LOAD/PO/ORDER number");
					}else{
						alert("Please enter CTNR/BOL number");
					}	
				}
				 return false;
			}
		});
		
		$find.click(function(){
				 var id=$(this).attr("in");
				 var ra=$("input[name='ra"+id+"']:checked").val();

				 var type='';
				 if(ra==0){	
					type=1;
				 }
				 if($door!="" && $number.val()!=""){
						$.ajax({
							url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/getLoadingAction.action',
							data:'search_number='+$number.val()+'&number_type='+type+'&ps_id=<%=psId%>'+'&mainId='+<%=id%>,
							dataType:'json',
							type:'post',
							beforeSend:function(request){
						      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
						    },
							success:function(data){
								$.unblockUI();       //遮罩关闭
		//						datas=escape(datas);    //转码
				//debugger;
								if(data.num==0){
									alert("Record not found!");
									$allzone.val(1);
									$door.val("");
									$door.attr("text","");
									var order_type="";
									if(type==1){
										$('#staging'+id).val("");
										$('#staging_zone'+id).val("");
										order_type=<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>;
									}else{
										$('#searchTitle'+id).val("");
										$('#searchZone'+id).val("");
										order_type=<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%>;
									}
									$number_type.val(order_type);
									
									$('#title'+id).html("OTHERS :");
									//计算有效的条数
									effectiveNumber();
								}
								if(data.num==1){
									if(type==1){
										$door.val(data.door_name);
										$door.attr("text",data.door_id);
										$allzone.val(data.area_id);
										
										$('#staging'+id).val(data.stagingareaid);
										$('#staging_zone'+id).val(data.area_name);
									}else{
										$door.val(data.door_name);
										$door.attr("text",data.door_id);
										$allzone.val(data.area_id);
										
										findZoneByTitleName($('#searchTitle'+id),$('#searchZone'+id),data.supplierid);
									}
									if(data.appointmentdate && id==1){
										$("#appointmentdate").val(data.appointmentdate);
										
									}
									$company_id.val(data.companyid);
									$customer_id.val(data.customerid);
									$account_id.val(data.accountid);
									$supplier_id.val(data.supplierid);
									$staging_area_id.val(data.stagingareaid);
									$number_type.val(data.order_type);
									$freight_term.val(data.freightterm);
									var title=""
									if(data.order_type==<%=ModuleKey.CHECK_IN_PONO%>){
										$order_no.val(data.orderno);
										title="PO";
									}
									if(data.order_type==<%=ModuleKey.CHECK_IN_ORDER%>){
										$po_no.val(data.pono);
										title="ORDER";

									}
									if(data.order_type==<%=ModuleKey.CHECK_IN_LOAD%>){
										title="LOAD";
									}

									
									if(data.order_type==<%=ModuleKey.CHECK_IN_CTN%>){
										title="CTNR";
									}
									if(data.order_type==<%=ModuleKey.CHECK_IN_BOL%>){
										title="BOL";
									}
									$('#title'+id).html(title+":");
									//计算有效的条数
									effectiveNumber();
								}				
								if(data.num * 1 == -1){
									var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/task_number_wms_select_data.html?number='+$number.val()+'&id='+id+'&datas='+escape(jQuery.fn.toJSON(data.pickup)); 
									$.artDialog.open(uri , {title: "Select",width:'800px',height:'500px', lock: true,opacity: 0.3,fixed: true});
								}
								if(data.flag==1 && <%=id%> != data.mainid){
									if(ra==0){
										title="LOAD/PO/ORDER";
									}else{
										title="CTNR/BOL";
									}
									$('#title'+id).html(title+":");
									showMessage("This is duplicate record!","alert");
									$number.val("");
								}
								findFirstContainerNoHasValue();
							},
							error:function(){
								$.unblockUI();       //遮罩关闭
								alert("System error"); 
							}
						});
					}else{
						if(type==1){
							alert("Please enter LOAD/PO/ORDER number");
						}else{
							alert("Please enter CTNR/BOL number");
						}	
					}
					 return false;
		});

		
	    $ra.click(function(){
			if(this.value==0){
				$("span[name='title']",$container).html('LOAD/PO/ORDER:');
				$("#stagingTitle",$container).html('Staging Location:&nbsp;');
				$("#stagingVal",$container).html('<input id="staging'+p+'" disabled="disabled" size="20"   value="'+staging_area_id+'">');
				$("#zoneTitle",$container).html('Zone:&nbsp;');
				$("#zoneVal",$container).html('<input id="staging_zone'+p+'" disabled="disabled" size="20">');		  
			}else{
				var html='CTNR/BOL:';
				$("span[name='title']",$container).html(html);
				$("#stagingTitle",$container).html('Title:&nbsp;');
				$("#stagingVal",$container).html('<input id="searchTitle'+p+'" disabled="disabled" size="20">');
				$("#zoneTitle",$container).html('Zone:&nbsp;');
				$("#zoneVal",$container).html('<input id="searchZone'+p+'" disabled="disabled" size="20">');			  
			}
			 $number_type.val("");
	    });

		$tongzhi.click(function(){    //绑定通知文本框
			 var id=this.id;
			 var ids=$('#'+id+'Id').val();
			 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/check_in/check_in_admin_user.html"; 
			 
			 var option = {
					 single_check:0, 					// 1表示的 单选
					 user_ids:ids, //需要回显的UserId
					 not_check_user:"",					//某些人不 会被选中的
					 proJsId:"15",						// -1全 部,1普通员工,5副主管,10主管,15主管和副主管
					 ps_id:'<%=adminLoggerBean.getPs_id()%>',	
					 id:id,					//所属仓库
					 handle_method:'setSelectAdminUsers'
			 };
			 uri  = uri+"?"+jQuery.param(option);
			 $.artDialog.open(uri , {title: 'User List',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
		});
		$tongzhi.keyup(function(){ 
  			//$door.val("");
			$tongzhi.click();
		});
    
	    
	    $door.click(function(){                       //绑定门和位置
	    	var id=$(this).attr("in");
		    var ra=$("input[name='ra"+id+"']:checked",$container).val();
		    	
	    	var $num=$("input[name='number']",$container); 
	    	if($num.val()=="" && ra==0 ){
				alert("Please enter LOAD number");
				return false;
	    	}
	    	if($num.val()=="" && ra==1 ){
				alert("Please enter CTNR/BOL number");
				return false;
	    	}
	    	var numVal=$num.val();
	    	var numName="";
	    	if(ra==0){	
	    		numName="LOAD/PO/ORDER";
			}else{	
				numName="CTNR/BOL";	
		    }
	    	var inputId=this.id;
	    	var mainId=<%=id%>;
	    	var zoneId=$allzone.val();
	    	var doorId=$(this).attr("text");
	    	var door_spot=$door_spot.val();
	    
	    	if(door_spot==<%=OccupyTypeKey.SPOT%>){
	    		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/get_yard_no.html?storageId=<%=psId%>'
	    				+'&yc_id='+doorId+'&spotZoneId='+zoneId+'&inputId='+inputId+'&mainId=<%=id %>'; 
<%-- 	    				storageId=<%=ps_id%>'+'&yc_id='+yc_id+'&mainId='+<%=mainId%>+'&spotZoneId='+spotZoneId+'&inputId=-1'; --%>
	    		$.artDialog.open(uri , {title: "Spot",width:'600px',height:'520px', lock: true,opacity: 0.3,fixed: true});
	    	}else{
<%-- 		    	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_in_door_list.html?zoneId='+zoneId+'&inputValue='+doorId+'&inputId='+inputId+'&associate_id=<%=id %>';  --%>
		    	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/door_list.html?zoneId='+zoneId+'&inputValue='+doorId+'&inputId='+inputId+'&associate_id=<%=id %>'; 
		    	$.artDialog.open(uri , {title: "Door and Location",width:'700px',height:'470px', lock: true,opacity: 0.3,fixed: true});
	    	}  
		});
	}
	
	
	
	//自动选择门
	function autoChoiceDoor(doorName,flag,result){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindReadyDoorSeachAction.action',
			data:'doorName='+doorName+'&ps_id='+<%=psId%>,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				if(data.length==0){
					if(flag==0){
						flag=1;
						autoChoiceDoor(Number(doorName)+1,flag,result);
					}else if(flag==1){
						flag=2;
						autoChoiceDoor(Number(doorName)-2,flag,result);
					}
				}else{
					$allzone.val(data[0].area_id);
					$door.val(data[0].doorid);
				}
			},
			error:function(){
				alert("System error"); 
			}
		 });
	}

	function setParentUserShow(ids,names,inputId){ //通知回显
		$("#"+inputId).val(names);
		$("#"+inputId+"Id").val(ids);			
	}

	
	function getDialog(in_seal, out_seal) {
		// 点击取消事件后焦点所在位置
		var focusLocation = 0; 
		if((out_seal == '' || out_seal == 'undefined') && (in_seal == '' || in_seal == 'undefined') ) {
			in_seal_click();
			out_seal_click();
			focusLocation = 1;
		} else if(in_seal == '' || in_seal == 'undefined' ) {
			focusLocation = 2;
			in_seal_click();
		} else if(out_seal == '' || out_seal == 'undefined') {
			focusLocation = 3;
			out_seal_click();
		}
		
		
		var in_seal = $('#in_seal').val();
		var out_seal = $('#out_seal').val();
		$.artDialog({
			title : 'Message',
			width: '300px',
			height: '30%',
		    lock: true,
		    opacity: 0.3,
		    fixed : true,
			content : "<div style='font-size:16px'>Delivery Seal: <strong>" + in_seal + "</strong></div>"
					+ "<div style='font-size:16px'>PickUp: <strong>" + out_seal + "</strong></div>",
			okVal: 'Yes',
		    ok: function () {
		    },
		    cancelVal: 'Cancel',
		    cancel: function(){
				if(focusLocation == 1 ) {
					$('#in_seal').val('');
					$('#out_seal').val('');
					$('#in_seal').focus();
				} else if(focusLocation == 2) {
					$('#in_seal').val('');
					$('#in_seal').focus();
				} else if(focusLocation == 3) {
					$('#out_seal').val('');
					$('#out_seal').focus();
				}
			}
		});
	}
	function in_seal_click(){
		$('#in_seal').val('NA');
	}
	
	function out_seal_click(){
		$('#out_seal').val('NA');
	}
	
	function selectData(){       //提交的时候选择数据
		var $num=$("input[name='number']"); 
	    var number='';
		for(var i=0;i<$num.length;i++){
			if(i==$num.length-1){
				number+=$($num[i]).val();
			}else{
				number+=$($num[i]).val()+",";
			}
			var id=$($num[i]).attr("id");
			var doorId=$('#door'+id).attr("text");
			var tongzhiId=$('#tongzhi'+id+'Id').val();
			if(doorId==undefined || doorId==""){
				alert("Please choose door First");
				return false;
			}
			if(tongzhiId==""){
				alert("Please choose operator");
				return false;
			}	
		}
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_submit_select_data.html?number='+number;
		$.artDialog.open(uri , {title: "Select Data",width:'700px',height:'500px', lock: true,opacity: 0.3,fixed: true});
	}
	
	function tijiao(waitingOrNot){                                  //提交方法
		var error_num='';
		//alert(jQuery.fn.toJSON(selectData));
		var priority = $("#priority").val();
		var $num=$("input[name='number']"); 
		var ctn=$("input[name='ctn']");
		if($num.length==0 && $('#relType').val()!='<%=CheckInMainDocumentsRelTypeKey.PICKUP_CTNR%>'){
			showMessage("Please add task number","alert");
			return false;
		}
		if($num.length > 0 && $('#relType').val()=='<%=CheckInMainDocumentsRelTypeKey.PICKUP_CTNR%>'){
			showMessage("Please delete task number","alert");
			return false;
		}
		var in_seal = $('#in_seal').val();
		var out_seal = $('#out_seal').val();
		var appointmentdate = $('#appointmentdate').val();
//		alert(appointmentdate);
		var data={};
		data.items=[]; 
		var mainId=<%=id %>;
		
		data.mainId=mainId;
		data.in_seal=in_seal;
		data.out_seal=out_seal;
		data.appointmentdate=appointmentdate;
		data.rel_type = $('#relType').val();
		data.is_live = $('#liveType').val();
		if(waitingOrNot)
		{
			data.waiting = waitingOrNot;
		}
		else
		{
			data.waiting = 0;
		}
		var prints=[]; 
		var det_detail_id=$('#det_detail_id').val();
		var isSubmit=true;
		for(var i=0;i<$num.length;i++){
			var id=$($num[i]).attr("id");
			var doorId=$('#door'+id).attr("text");
			var doorName=$('#door'+id).val();
			var zoneId=$('#door'+id+'zone').val();
			
			var tongzhiId=$('#tongzhi'+id+'Id').val();
			var tongzhiName=$('#tongzhi'+id).val();
			var detailId=$('#detailid'+id).val();
			var ra=$("input[name='ra"+id+"']:checked").val();
			var beizhu=$('#beizhu'+id).val();
			var youjian='';
			var duanxin='';
			var yemian='';	
		
			if(tongzhiId==""){
				alert("Please choose operator");
				return false;
			}	
			if($('#youjian'+id).attr('checked')=='checked'){
				youjian=1;
			}else{
				youjian=0;
			}
			if($('#duanxin'+id).attr('checked')=='checked'){
				duanxin=1;
			}else{
				duanxin=0;
			}
			if($('#yemian'+id).attr('checked')=='checked'){
				yemian=1;
			}else{
				yemian=0;
			}	
			var number=$($num[i]).val();
			var number_type=$('#number_type'+id).val();	
			var customer_id=$('#customer_id'+id).val();
			var company_id=$('#company_id'+id).val();
			var account_id=$('#account_id'+id).val();
			var order_no=$('#order_no'+id).val();
			var po_no=$('#po_no'+id).val();
			var supplier_id=$('#supplier_id'+id).val();
			var staging_area_id=$('#staging_area_id'+id).val();
			var freight_term=$('#freight_term'+id).val();
			var containerNo=$('#containerNo'+id).val();
			var door_spot=$('#door_spot'+id).val();
			
			if(number==""){
				alert("Please enter LOAD# and CTN#");
				return false;
			}
			if(doorId==undefined || doorId=="" || doorId==0){
				alert("Please choose door First");
				return false;
			}
			if(number_type==''){
				isSubmit=false;
				error_num=number;
			}
			
			var num=0;
			for(var a=0;a<prints.length;a++){
				if(prints[a]==doorId){
					num=1;
				}
			}
			if(num==0){
				prints.push(doorId);
			}
			if(containerNo.toUpperCase() == 'NA')
			{
				containerNo = '';
			}
			var op={};
			op.occupancy_type=door_spot;
			op.container_no=containerNo;
			op.detailId=detailId;
			op.occupancy_type =door_spot;
			op.container_no = containerNo;
			op.doorId=doorId;
			op.doorName=doorName;
			op.zoneId=zoneId;
			op.number=number;
// 			for(var b=0;b<selectData.length;b++){
// 				if(number==selectData[b].number){
			op.number_type=number_type;
			op.customer_id=customer_id;
			op.company_id=company_id;
			op.order_no=order_no;
			op.po_no=po_no;
			op.freight_term=freight_term;
// 				}
// 			}
			op.tongzhi=tongzhiId;
			op.youjian=youjian;
			op.duanxin=duanxin;
			op.yemian=yemian;
			op.beizhu=beizhu;
			op.tongzhiName=tongzhiName;
// 			op.number_type=number_type;
// 			op.customer_id=customer_id;
// 			op.company_id=company_id;
			op.account_id=account_id;

//			op.order_no=order_no;
//			op.po_no=po_no;
			op.supplier_id=supplier_id;
			op.staging_area_id=staging_area_id;
// 			op.order_no=order_no;
// 			op.po_no=po_no;

			data.items.push(op);	
		}
		
		//alert(jQuery.fn.toJSON(data));
        if(isSubmit){
        	$.ajax({
    	  		type:'post',
    	  		
    	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxAddOccupancyDetailsAction.action',
    	  		dataType:'json',
    	  		data:'det_detail_id='+det_detail_id+'&str='+jQuery.fn.toJSON(data)+'&priority='+priority+'&'+$("#fileform").serialize(),
    			beforeSend:function(request){
    				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   // 遮罩打开
    		    },
    		 	success:function(data){
    		 		//alert(data.flag);
	   		 		parent.location.reload();        
	   	   			$.artDialog && $.artDialog.close();	
    	  		},
    	  		error:function(serverresponse, status){
    	  			$.unblockUI();
    	  			showMessage(errorMessage(serverresponse.responseText),"alert");
    	  		}
    	    });
        }else{
        	showMessage(error_num+':\n There is no types of documents!\nPlease click Find button',"alert");
        }
    	
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}

	//文件上传
	function uploadFile(_target){
	    var targetNode = $("#"+_target);
	    var fileNames = $("input[name='file_names']").val();
	    var obj  = {
		     reg:"picture_office",
		     limitSize:8,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames && fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: 'Upload Photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
	    		 close:function(){
						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
						 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
	   	}});
	}
	//在线获取
	function onlineScanner(target){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target="+target; 
		$.artDialog.open(uri , {title: 'Online Mutiple',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}
	
	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,target){
	    $("p.new").remove();
		if($.trim(fileNames).length > 0 ){
			$("input[name='file_names']").val(fileNames); 
			var array = fileNames.split(",");
			for(var index = 0 ; index < array.length ; index++ ){
				if(array[index].length>0){
					var a =  createA(array[index]) ;
					$("#add_file").append(a); 
			
				}
			}
		} 
	}
	function createA(fileName){
	    var id = fileName.substring(0,fileName.length-4);
	    var  a = "<tr id='add"+id+"'><td ><p style='color:#439B89' class='new' ><a href='javascript:void(0)' onclick='showTempPictrueOnline(&quot;"+fileName+"&quot;)'  style='color:#439B89' >"+fileName+"</p></td><td><p style='color:#439B89' class='new' >&nbsp;&nbsp;&nbsp;<a  href='javascript:deleteA(&quot;"+id+"&quot;)' style='color:red' >×</p></td></tr>";
	    return a ;
	}
	function deleteA(id){
		$("#add"+id+" *").remove();
		var file_names = $("input[name='file_names']").val(); 
		
		 var array = file_names.split(",");
		 var lastFile = "";
		 for(var index = 0 ; index < array.length ; index++ ){
				if(id==array[index].substring(0,array[index].length-4)){
					array[index]="";
					
				}
				lastFile += array[index];
				if(index!=array.length-1 && array[index]!=""){
					lastFile+=",";
				}
		 }
		 
		 $("input[name='file_names']").val(lastFile);
		
	}
	function closeWindow(){
		$.artDialog.close();
	}
	//图片在线显示  		 
	function showPictrueOnline(fileWithType,fileWithId ,currentName){
	    var obj = {
	  		file_with_type:fileWithType,
	  		file_with_id : fileWithId,
	  		current_name : currentName ,
	  		cmd:"multiFile",
	  		table:'file',
	  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/check_in"
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}
	//删除图片
	function deleteFile(file_id){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'file',file_id:file_id,folder:'check_in',pk:'file_id'},
			success:function(data){
				if(data && data.flag === "success"){
					$("#file_tr_"+file_id).remove();
				}else{
					showMessage("System error,please try later","error");
				}
				
			},
			error:function(){
				showMessage("System error,please try later","error");
			}
		});
	}
	$("#relType").change(function(){
		if($(this).val() == '<%=CheckInMainDocumentsRelTypeKey.PICKUP_CTNR%>')
		{
			$('#live').css("display","none");
			$('#liveType').css("display","none");
		}
		else
		{
			$('#liveType').css("display","inline");
		}
	});
	//编辑主表类型按钮
	function editRelType(){
		$('#live').css("display","none");
		$('#liveType').css("display","inline");
		$('#relType').css("display","inline");     //下拉
		$('#entryType').css("display","none");     //状态
		$('#btnEdit').css("display","none");       //编辑按钮
		$('#btnSave').css("display","inline");    //save按钮
		$('#btnCancel').css("display","inline");    //save按钮
	}
	function cancelRelType()
	{
		$('#entryType').css("display","inline");     //状态
		$('#relType').css("display","none");     //状态
		$('#live').css("display","inline");     //状态
		$('#liveType').css("display","none");     //状态
		$('#btnEdit').css("display","inline");       //编辑按钮
		$('#btnSave').css("display","none");    //save按钮
		$('#btnCancel').css("display","none");    //save按钮
	}
    //更新主表类型
    //TODO 处理pickUp CTNR
	function saveRelType(){
		var dlo_id=<%=id%>;
		var relVal=$('#relType').val();	
		var liveVal=$('#liveType').val();
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxUpdateMainRelTypeAction.action',
	  		dataType:'json',
	  		data:'dlo_id='+dlo_id+'&rel_val='+relVal+'&live_val='+liveVal,
		 	success:function(data){
	  			if(data.rel_type==1){
  					$('#entryType').html('DELIVERY');
	  			}else if(data.rel_type==2){
	  				$('#entryType').html('Pick Up');
	  			}else if(data.rel_type==3){
	  				$('#entryType').html('BOTH');
	  			}else{
	  				$('#entryType').html('NONE');
  				}
	  			
	  			if(data.islive==1){
	  				$('#live').html('Live Load');
	  			}else if(data.islive==2){
	  				$('#live').html('Drop Off');
	  			}else if(data.islive==3){
	  				$('#live').html('Swap CTNR');
	  			}else if(data.islive==4){
	  				$('#live').html('PickUp CTNR');
	  			}
	  			$('#live').css("display","inline");
	  		    $('#liveType').css("display","none");
	  			
	  			$('#btnSave').css("display","none");    //save按钮
				$('#btnEdit').css("display","inline");       //编辑按钮	
				$('#btnCancel').css("display","none");    //save按钮
				$('#relType').css("display","none");     //下拉	
				$('#entryType').css("display","inline");     //状态
				$('#is_live_now').val(liveVal);
				$('#rel_type_now').val(relVal);
	  		} 
	    });
	}
	function showTempPictrueOnline(_fileName){
	 	   var obj = {
				current_name:_fileName ,
		   		table:"temp",
		   		fileNames:$("input[name='file_names']").val(),
		   		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upl_imags_tmp"
			}
	 	   if(window.top && window.top.openPictureOnlineShow){
				window.top.openPictureOnlineShow(obj);
			}else{
			    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
			}
		}
	function onlineSingleScanner(_target){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_single_scanner.html?target="+_target; 
	$.artDialog.open(uri , {title: 'Online Single',width:'300px',height:'170px', lock: true,opacity: 0.3,fixed: true});
	
	}
	
	function findDoorsByZoneId(flag,zoneId,$door,$allzone,$number,p){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindDoorsByZoneIdAction.action',
			data:'zoneId='+zoneId+'&mainId=<%=id%>&ps_id=<%=psId%>&flag='+flag,
			dataType:'json',
			type:'post',
			success:function(data){
				if(data.length>0){
					$door.val(data[0].doorid);
					$door.attr("text",data[0].sd_id);
					$allzone.val(zoneId);
	  			}else if(data.length==0 && flag==1){
	  				findDoorsByZoneId(2,zoneId,$door,$allzone,$number,p);
	  			}else if(data.length==0 && flag==2){
	  				$door.val("");
	  				$door.attr("text","");
	  				$allzone.val("");
	  				//openDoor();
	  				var id=$door.attr("in");
	  			    var ra=$("input[name='ra"+id+"']:checked").val();
	  			 
	  		    	var numVal=$number.val();
	  		    	var numName="";
	  		    	if(ra==0){	
	  		    		numName="LOAD";
	  				}else{
	  					var choice= $('#choice'+p).val();     //选择值
	  					if(choice==0){			
	  						numName="CTN";
	  					}else if(choice==1){
	  						numName="OTHER";
	  					}else{
	  						numName="BOL";
	  					}		
	  			    }
	  		    	var inputId=$door.attr('id');
	  		    	
	  		    	var mainId=<%=id%>;
	  		    	var inputValue=$door.attr("text");
	  		    	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/door_list.html?mainId='+mainId+'&numVal='+numVal+'&inputId='+inputId+'&inputValue='+inputValue+'&numName='+numName;
	  				$.artDialog.open(uri , {title: "Door and Location",width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	  		
	  			}
			}
		});
	}
	
	
	function addStyleForText(text)
	{
		return '<span style="font-size: 10;font-family:Verdana;">'+text+'</span>';
	}
	
	function trim(str){
	    return str.replace(/[ ]/g,"");  //去除字符算中的空格
	}
	
	function findZoneByTitleName(searchTitle,searchZone,title_name){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindZoneByTitleNameAction.action',
			data:'title_name='+title_name+'&ps_id='+<%=psId%>,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				var zone="";
				for(var i=0;i<data.length;i++){
					//alert(data[i].area_name);
					if(i==0){
						zone=data[i].area_name+",";
					}
					
					if(i>0 && data[i].area_name!=data[i-1].area_name){
						zone +=data[i].area_name+",";
					}
				
				}
				zone=zone.substr(0,zone.length-1);
				$(searchTitle).val(title_name);
				$(searchZone).val(zone);
			},
			error:function(){
				alert("System error"); 
			}
		 });
	}
	
	function openLoadTemplate(id,num){
		
		var number=$('#'+id).val();
		var number_type=$('#number_type'+id).val();
		var companyId=$('#company_id'+id).val();
		var customerId=$('#customer_id'+id).val();
		var order=$('#order_no'+id).val();
		var door=$('#door'+id).val();
		var entry_id=<%=id%>;
		var type='';
		if(number_type==""){
			alert('Document type not found!Please click Find button');
			return false;
		}
		if(number_type==<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%> || number_type==<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>){
			alert('No data records');
			return false;
		}
		if(num==0){
			type='pickup';
		}else{
			type='delivery';
		}
		if('' != number)
		{
			if(type=='pickup')
			{
				findLoadsCountByLoadNo(number,entry_id,door,type,companyId,customerId,number_type,order);
			}
			else if(type=='delivery')
			{
				findReceiptsCountByBolOrCtnNo(number,entry_id,door,type,companyId,customerId,number_type);
			}
		}
		else
		{
			if(type == 'pickup')
			{
				showMessage("please input LOAD","alert");
				
			}
			else if(type == 'delivery')
			{
				showMessage("please input CTNR/BOL","alert");
			}
			
		} 
	}
	function setParentCompanyLoadCustomer(CompanyID, loadNo, CustomerID,entry_id,door,type,number_type)
	{
		var print=[];
		var json = {
				checkDataType:type.toUpperCase(),
				number:loadNo,
				door_name:door,
				companyId:CompanyID,
				customerId:CustomerID,
				number_type:number_type,
				checkLen:1
		};
		print.push(json);
		var jsonString = JSON.stringify(print);
	
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?jsonString='+jsonString+'&number='+loadNo
				+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&CompanyID='+CompanyID+'&CustomerID='+CustomerID+'&out_seal='+$("#out_seal").val();
		$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
	}
	function setParentCompanyReceiptCustomer(CompanyID, number, CustomerID,entry_id,door,type, bol, ctnr)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?number='+number
		+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&CompanyID='+CompanyID+'&CustomerID='+CustomerID+'&out_seal='+$("#out_seal").val()+'&bol='+bol+'&ctnr='+ctnr;
		$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
	}
	function findLoadsCountByLoadNo(number,entry_id,door,type,companyId,customerId,number_type,order)
	{
				var print=[];
				var json = {
						checkDataType:type.toUpperCase(),
						number:number,
						door_name:door,
						companyId:companyId,
						customerId:customerId,
						number_type:number_type,
						order:order,
						checkLen:1
				};
				print.push(json);
				var jsonString = JSON.stringify(print);
				
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?jsonString='+jsonString+'&number='+number+'&number_type='+number_type+'&entry_id='+entry_id+'&door_name='+door+'&type='+type+'&order_no='+order+'&out_seal='+$("#out_seal").val()+'&CompanyID='+companyId+'&CustomerID='+customerId;;
				$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true,type:type});

	}
	function findReceiptsCountByBolOrCtnNo(number,entry_id,door,type,companyId,customerId,number_type){	
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list.html?number='+number+'&entry_id='+entry_id+'&door_name='+door+'&number_type='+number_type+'&type='+type+'&CompanyID='+companyId+'&CustomerID='+customerId;
		$.artDialog.open(uri , {title: "Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true,type:type});
				
	}
	var jsonsPrint = [];
	function batchPrint(){
		jsonsPrint = [];
		var entry_id=<%=id%>;
		var out_seal=$("#out_seal").val();
		var numbers = $("input[name='number']", $('#Container'));
		var isPrint=true;
		for(var i = 0; i < numbers.length; i ++){
			var id = $(numbers[i]).attr("id");//p
			var number = $(numbers[i]).attr("value");//load ctnr bol
			var number_type=$('#number_type'+id).val();
			var companyId = $("#company_id"+id).val();
			var customerId = $("#customer_id"+id).val();
			var door_name=$('#door'+id).val();
			var order=$("#order_no"+id).val();
			if(number_type==''){
				isPrint=false;
			}

			var checkDataType='';
			if(number_type==<%=ModuleKey.CHECK_IN_CTN%> || number_type==<%=ModuleKey.CHECK_IN_BOL%> ){
				checkDataType='delivery';
			}else{
				checkDataType='pickup';
			}
			var json = {
					checkDataType:checkDataType,
					number:number,
					door_name:door_name,
					companyId:companyId,
					customerId:customerId,
					checkLen:1,
					order:order,
					number_type:number_type
			};
			jsonsPrint.push(json);
			//alert(typeSel+","+checkDataType);
		}
		
		var jsonString = JSON.stringify(jsonsPrint);
		var para = 'entry_id='+entry_id+'&out_seal='+$("#out_seal").val()+"&jsonString="+jsonString;
		if(isPrint){
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_print_list_batch.html?'+para;
			$.artDialog.open(uri , {title: "Batch Print List",width:'950px',height:'580px', lock: true,opacity: 0.3,fixed: true});
		}else{
			alert('There is no types of documents!Please click Find button');
		}
		
	}
	
	//给显示的Delivery Seal和 PickUp Seal的input添加值NA
	function setNA(flag)
	{
		if(flag==1){
			$("#in_seal").val("NA");
		}else{
			$("#out_seal").val("NA");
		}
		
		
	}

	function setParentNumber(CompanyID, number, CustomerID, AccountID, PONo, OrderNo,id,number_type,StagingAreaID,title,freight_term){
		
		$('#customer_id'+id).val(CustomerID);
		$('#company_id'+id).val(CompanyID);
		$('#account_id'+id).val(AccountID);
		$('#order_no'+id).val(OrderNo);
		$('#po_no'+id).val(PONo);
		$('#number_type'+id).val(number_type);
		$('#staging_area_id'+id).val(StagingAreaID);
		$('#title'+id).html(title+":");
		$('#'+id).val(number);
		$('#freight_term'+id).val(freight_term);
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/CheckInAutoAssignDoorAction.action',
			data:'search_number='+number+'&area_name='+StagingAreaID+'&ps_id=<%=psId%>'+'&mainId='+<%=id%>,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
			success:function(data){
				$.unblockUI();       //遮罩关闭
			
				$('#door'+id).val(data.door_name);
				$('#door'+id).attr("text",data.door_id);
				$('#allzone'+id).val(data.area_id);
				
				$('#staging'+id).val(StagingAreaID);
				$('#staging_zone'+id).val(data.area_name);

				
			},
			error:function(){
				$.unblockUI();       //遮罩关闭
				alert("System error"); 
			}
		});
		//计算有效的条数
		effectiveNumber();
		
	}
	
	//计算总条数
	function calculationAv(){
		var divs=$('div[name="av"]');
		$('#sumNumber').html(divs.length);
	}
	//计算有效的条数
	function effectiveNumber(){
		var number_type=$('input[name="number_type"]');
		var num=0;
		for(var i=0;i<number_type.length;i++){
			if($(number_type[i]).val()!=0 && $(number_type[i]).val()!==""){
				num++;
			}
		}
		$('#effectiveNumber').html(num);
	}
	//计算序列号
	function sequence(){
		var sequenceNum=$('td[name="sequence"]');
		var num=1;
		for(var i=0;i<sequenceNum.length;i++){
			$(sequenceNum[i]).html(num);
			num++;
		}
	}
	function findFirstContainerNoHasValue()
	{
		firstHasValueContainerNo = $("div[name=av] input[name=containerNo][value!='']:first").val();
		if(!firstHasValueContainerNo)
		{
			firstHasValueContainerNo = '';
		}
		//alert(firstHasValueContainerNo);
	}
	//post zone 改变时事件
	function findSpotByZoneId(flag,spotArea,num){
		//alert(flag);
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindStorageYardSearchAction.action',
	  		dataType:'json',
	  		data:"spotArea="+spotArea+"&ps_id=<%=psId%>&flag="+flag+"&mainId="+<%=id%>,
	  		success:function(data){
	  			if(data.length>0){
	  				$('#door'+num).val(data[0].yc_no);
	  				$('#door'+num).attr("text",data[0].yc_id);
	  			}else if(data.length==0 && flag==3){
	  				findSpotByZoneId(4,spotArea,num);
	  			}else if(data.length==0 && flag==4){
	  				$('#door'+p).val('');
	  				$('#door'+p).attr("text",'');	  		
	  				//打开选择停车位列表
	  				var inputId='door'+num;
	  				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/get_yard_no.html?storageId=<%=psId%>'+'&spotZoneId='+spotArea+'&inputId='+inputId; 
		    		$.artDialog.open(uri , {title: "Spot",width:'600px',height:'520px', lock: true,opacity: 0.3,fixed: true});
	  			}
	  		}
	    });
	}
	
	//停车位回填
	function setYardNo(yardId,yardNo,inputId,zone_id){
		$('#'+inputId).val(yardNo);
		$('#'+inputId).attr("text",yardId);
		//$("#"+inputId+"zone").val(zone_id);
		var num=$('#'+inputId).attr('in');
		$('#door'+num+'zone').val(zone_id);
	}
	//回填门
	function getDoor(door,doorName,inputId,zone_id){    
		$('#'+inputId).val(doorName);
		$('#'+inputId).attr("text",door);
		//$("#"+inputId+"zone").val(zone_id);
		//alert(zone_id);
		var num=$('#'+inputId).attr('in');
		$('#door'+num+'zone').val(zone_id);
	}
	
	//页面验证number是否重复  同类型的 例如 pick up number不能重
	function verification(number){
		
	}
	
</script>
