<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder")%>';
</script>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- GoogleMap -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&sensor=false"></script>
<script src="../js/maps/GoogleMapsV3.js"></script> 
<script src="../js/maps/jsmap.js"></script> 
<!-- show message -->
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 	long adminId=adminLoggerBean.getAdid();
 	long ps_id=adminLoggerBean.getPs_id();
 	long mainId=StringUtil.getLong(request,"mainId");
 	
//   DBRow psRow=adminMgrZwb.findAdminPs(adminId);
//	 DBRow[] rows=checkInMgrZwb.selectAllDoor(psRow.get("ps_id",0l),"",mainId);
     String numVal=StringUtil.getString(request,"numVal");
     String numName=StringUtil.getString(request,"numName");
     String inputId=StringUtil.getString(request,"inputId");
     long inputValue=StringUtil.getLong(request,"inputValue");
     long usezoneId=StringUtil.getLong(request,"usezoneId");
     long cmd=StringUtil.getLong(request,"cmd");
     long zoneId=StringUtil.getLong(request,"zoneId");
//      out.print(inputValue);
     //区域可用门
     DBRow[] zoneReadyRow = checkInMgrZwb.zoneReadyDoor(zoneId,mainId,"");
  
     //区域占用门
     DBRow[] zoneUseRow =checkInMgrZwb.zoneUseDoor(zoneId);
     
     
//   DBRow[] useRow=checkInMgrZwb.getOccupiedDoorOfWindow(psRow.get("ps_id",0l),usezoneId);     //被占用的门
     DBRow[] useRow=checkInMgrZwb.getUnavailableDoor(ps_id,usezoneId);     //被占用的门
     DBRow[] canUseRow=checkInMgrZwb.getVacancyDoor(ps_id,usezoneId,mainId);     //可用的门
     if(cmd==1){
    	 canUseRow=checkInMgrZwb.getVacancyDoor(ps_id,usezoneId,0L);     //查询该区域下的可用门
     }
     
// 	int psId = psRow.get("ps_id",0);
 	DBRow storageKml = googleMapsMgrCc.getStorageKmlByPsId(Integer.parseInt(String.valueOf(ps_id)));
 	String kmlName = "0";
 	if(storageKml != null){
 		kmlName = storageKml.getString("kml");
 	}
 	//查询所有zone
 	DBRow[] allZone =checkInMgrZwb.findAllZone(ps_id);
 %>

<title>Check In</title>
</head>
<body onload="getLocation(<%=inputValue%>,<%=ps_id%>)">
<!-- 	<div align="left" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;"> -->
<!--      	<table width="100%" border="0" cellspacing="0" cellpadding="0"> -->
<!-- 		  <tr> -->
<!-- 		    <td> -->
<%-- 		    	<font style="font-weight:bold;"><%=numName%>:</font><font style="font-weight:bold;size:16px;color:#00F"><%=numVal%></font> --%>
<!-- 		    </td> -->
<!-- 		    <td align="right"><input type="hidden" class="long-button" value="WAITING" onclick="addwait()"  />&nbsp;&nbsp;<input type="button"  value="Map" onclick="getDoorInMap()"  class="long-button" /></td> -->
<!-- 		  </tr> -->
<!-- 		</table> -->
<!-- 	</div> -->
<!-- 	<br/> -->
	<div id="tabs">
		<ul>			 
			<li><a href="#av1">Available Door</a></li>	
			<li><a href="#av2">Occupied Door</a></li>
		</ul>
		<div id="av1">
		     <table width="100%" border="0" cellspacing="0" cellpadding="0" id="search_tb">
		     	<tr>
		     		<td width="400px" height="30px" bgcolor="#dddddd" style="padding-left:5px;border-right:1px solid #eeeeee;font-weight:bold">
		     		  <input type="text" id="seachDoor" onkeyup="seach(1)" onkeypress="if(event.keyCode==13){seach(0);return false;}"/>&nbsp;
		<!--     	  	  <input type="button"  value="Find" class="button_long_search" onclick="seach(1)" />--> 
		     		  <select  id="allzone" onchange="findDoorsByZoneId()" onkeypress="if(allzone.size==1){allzone.size=allzone.length} else{allzone.size=1;$('#seachDoor').focus()}">
		     		  <option value="0">Zone</option>
		     		  <%
		     		  		if(allZone!=null && allZone.length>0){
		     		  			for(int i=0;i<allZone.length;i++){
		     		  %>
		     		  <option value="<%=allZone[i].get("area_id",0)%>"  <%=zoneId==allZone[i].get("area_id",0)?"selected":""%> > <%=allZone[i].get("area_name","") %></option>
		     		  <%			
		     		  		    }
		     		  		}
		     		  %>
		     		  </select>
		     		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		     		  <input type="button"  value="Map" onclick="getDoorInMap()"  class="long-button" style="margin-left:150px;" />
		     		</td>
		     		<td bgcolor="#dddddd" style="padding-left:5px;font-weight:bold;display: none;" align="left">Staging</td>
		     	</tr>
		     </table>
	         <div align="left" style=" margin-top:2px; height:404px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">		
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					    <td>
						    <div style="background-color:#FFF;width:100%; border:2px #dddddd solid; height:360px; float:left; overflow:auto" >
				        		 <table width="100%" border="0" cellspacing="0" cellpadding="0" id="readyDoor">
								 <% 
					        		for(int i=0;i<canUseRow.length;i++){
					        			 if(canUseRow[i].get("sd_id", 0l)==inputValue){
					        		%>
						        	<tr ondblclick="doubleClick(this)" onclick="singleClick(<%=canUseRow[i].get("sd_id", 0l) %>,<%=ps_id%>,this)" name="warehouse_tr" bgcolor="#E6F3C5">
										<td style="border-bottom:1px solid #dddddd;" width="20%" height="30px" align="center">
											<input name="doorId" type="radio" checked="checked" value="<%=canUseRow[i].get("sd_id", 0l) %>" text="<%=canUseRow[i].getString("doorid") %>"  />
										</td>
										<td style="border-bottom:1px solid #dddddd" > Door [<%=canUseRow[i].getString("doorid") %>]</td>
									</tr>
									<% }else{%>
						        	<tr ondblclick="doubleClick(this)" onclick="singleClick(<%=canUseRow[i].get("sd_id", 0l) %>,<%=ps_id %>,this)" name="warehouse_tr">
										<td style="border-bottom:1px solid #dddddd;" width="20%" height="30px" align="center">
											<input name="doorId" type="radio" value="<%=canUseRow[i].get("sd_id", 0l) %>" text="<%=canUseRow[i].getString("doorid") %>"  />
										 </td>
										<td style="border-bottom:1px solid #dddddd" > Door [<%=canUseRow[i].getString("doorid") %>]</td>
									</tr>
					        	<%} }%>
								 </table>
				      		</div>
					    </td>				   
					    <td style="display: none; ">
					   	    <div style="background-color:#FFF;width:240px; border:2px #dddddd solid; height:360px; float:left; overflow:auto">
				        		 <table width="100%" border="0" cellspacing="0" cellpadding="0" id="conTab">
								 </table>
				      		</div>
					    </td>
					</tr>
				</table>
				<div align="right">
					<input class="normal-green" type="button"  value="Confirm" onclick="fantian(2)" >
				</div>
			</div>         
	    </div> 
	    <div id="av2">
	    	 <form name="useDoor">
		    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			     	<tr>
			     		<td width="100%" height="25px" bgcolor="#dddddd" style="padding-left:5px;border-right:1px solid #eeeeee;font-weight:bold">Door
			     		 <input type="text" id="seachOccupiedDoor" onkeyup="seachOccupiedDoorByName(1);" onkeypress="if(event.keyCode==13){seachOccupiedDoorByName(0);return false;}" />&nbsp;
				 		 <input type="button" value="Find" class="button_long_search" onclick="seachOccupiedDoorByName(1);" />
			     		 <select  id="useDoorZone" onchange="findUseDoorsByZoneId()" >
			     		  <option value="0">Select Zone</option>
			     		  <%
			     		  		if(allZone!=null && allZone.length>0){
			     		  			for(int i=0;i<allZone.length;i++){
			     		  %>
			     		  <option value="<%=allZone[i].get("area_id",0)%>"  <%=allZone[i].get("area_id",0)==usezoneId?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
			     		  <%			
			     		  		    }
			     		  		}
			     		  %>
			     		  </select>
			     		  <input  type="hidden" name="numName" id="numName" value="<%=numName%>"/>
			     		  <input  type="hidden" name="numVal" id="numVal" value="<%=numVal%>"/>
			     		  <input  type="hidden" name="zoneId" id="zoneId" value="<%=zoneId%>"/>
			     		  <input  type="hidden" name="inputValue" id="inputValue" value="<%=inputValue%>"/>
			     		  <input  type="hidden" name="inputId" id="inputId" value="<%=inputId%>"/>
			     		  <input  type="hidden" name="usezoneId" id="usezoneId"/>
			     		  <input  type="hidden" name="cmd" id="cmd"/>
			     		</td>	     
			     	</tr>
			     </table>
			 </form>
			
			  <div align="left" style=" margin-top:2px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;" id="occupiedDoor">		
			     <%for(int a=0;a<useRow.length;a++){ %>
		     	 <div style="background-color:#FFF;border-bottom:1px solid #dddddd;padding-left:8px;">
		     	 	<table width="100%" border="0" cellspacing="0" cellpadding="0">
					    <tr height="25px" ondblclick="doubleClick(this)" onclick="singleClick(<%=useRow[a].getString("sd_id")%>,<%=ps_id%>,this)" name="warehouse_tr">
						    <td width="10%" height="30px" align="center">
						    	<%if(useRow[a].get("sd_id", 0l)==inputValue){ %>
						    		<input name="doorId" type="radio" checked="checked" value="<%=useRow[a].getString("sd_id")%>" text="<%=useRow[a].getString("doorid") %>" onclick="getLocation(<%=useRow[a].getString("sd_id") %>,<%=ps_id%>)" />
						    	<%}else{ %>
						    		<input name="doorId" type="radio"  value="<%=useRow[a].getString("sd_id")%>" text="<%=useRow[a].getString("doorid") %>" onclick="getLocation(<%=useRow[a].getString("sd_id") %>,<%=ps_id %>)" />
						    	<%} %>
						    </td>
							<td width="150px;" ><span style="color:red">Door &nbsp;[<%=useRow[a].getString("doorid") %>]</span></td>
					    	<td>&nbsp; <%=useRow[a].getString("detail") %></td>
					  </tr>
					</table>
		     	 </div>			
		     	 <%} %>
		     </div>
	    </div>  
	</div>  
</body>
</html>
<script>

	function zong_double_lick(tr){
		fantianZone(2);
	}
	function zone_single_click(tr){
		var zone_tr=$('tr[name="zone_tr"]');
		for(var i=0;i<zone_tr.length;i++){     //先去掉所有颜色
			$(zone_tr[i]).attr('bgcolor',"");
		}
		var ra=$('input[name="zoneDoorId"]',tr);
		$(tr).attr('bgcolor','#E6F3C5');
		ra.attr("checked","checked");
	}

	jQuery(function($){
		$("#tabs").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
			select: function(event,ui){
				if(ui.index==0){
					window.setTimeout(function(){$("#zoneDoor").focus();},300);
				}else if(ui.index==1){
					window.setTimeout(function(){$("#seachDoor").focus();},300);
				}
			},		
		});
		var cmd=<%=cmd%>;
		if(cmd!=1){
			 $("#tabs").tabs("select",0);  //默认选中第一个
		}
	   
// 	    findDoorsByZoneId();
	});

	function doubleClick(tr){              //双击
  		var ra=$('input[name="doorId"]',tr);
  		fantian(2);
	}

	function singleClick(door_id,ps_id,tr){ 		//单击
		var warehouse_tr=$('tr[name="warehouse_tr"]');
		for(var i=0;i<warehouse_tr.length;i++){     //先去掉所有颜色
			$(warehouse_tr[i]).attr('bgcolor',"");
		}
		$(tr).attr('bgcolor','#E6F3C5');
	    var ra=$('input[name="doorId"]',tr);
	    ra.attr("checked","checked");
	    getLocation(door_id,ps_id);
	}


	function getLocation(door_id,ps_id){                     
		$('#conTab').html('');
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindLocationByDoorIdAction.action',
	  		dataType:'json',
	  		data:"door_id="+door_id+"&ps_id="+ps_id,
	  		success:function(data){
		  		var html='';
	  			for(var i=0;i<data.length;i++){
					html+='<tr>'+
						     '<td style="border-bottom:1px solid #dddddd;" width="20%" height="30px" align="center">&nbsp;</td>'+							     
						     '<td style="border-bottom:1px solid #dddddd">'+data[i].location_name+'</td>'+
						  '</tr>';
	  			}	
	  			$(html).appendTo('#conTab');
	  		}
	    });
	}

	//按doorId area_id ps_id 查询占用的门   geql
	function seachOccupiedDoorByName(flag){
		var seachDoor = $('#seachOccupiedDoor').val();
		var ps_id=<%=ps_id%>;
		var mainId=<%=mainId%>;
		var zone_id=$('#useDoorZone').val();
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindOccupiedDoorSeachAction.action',
	  		dataType:'json',
	  		data:"doorName="+seachDoor+'&ps_id='+ps_id+'&mainId='+mainId+'&zone_id='+zone_id+'&flag='+flag,
	  		success:function(data){
	  			if(flag==1){
			  		var html='';
			  		for(var i=0;i<data.length;i++){
			  			html +='<div style="background-color:#FFF;border-bottom:1px solid #dddddd;padding-left:8px;">'+
							 	' <table width="100%" border="0" cellspacing="0" cellpadding="0">'+
							 	' <tr ondblclick="doubleClick(this)" onclick="singleClick('+data[i].sd_id+',<%=ps_id %>,this)" name="warehouse_tr">' +
								' <td width="10%" height="30px" align="center">';
							if(data[i].sd_id==inputValue){
								html+=' <input name="doorId" type="radio" checked="checked" value="'+data[i].sd_id+'" text="'+data[i].doorid+'" onclick="getLocation('+data[i].sd_id+',<%=ps_id%>)" />';
							}else{
								html+=' <input name="doorId" type="radio" value="'+data[i].sd_id+'" text="'+data[i].doorid+'" onclick="getLocation('+data[i].sd_id+',<%=ps_id %>)" />';
							}
							html+=' </td>'+
							' <td width="150px;" ><span style="color:red">Door &nbsp;['+data[i].doorid+']</span></td>'+
							' <td>&nbsp;'+data[i].detail+'</td>'+
							' </tr>'+
							' </table> </div>';
			  		}
			  		$('#occupiedDoor').html(html);
			  		
	  			}else{
		  	
	  				if(seachDoor!=""){
	  					if(data.length>0){
		  					$("#search_tb").append('<input name="doorId" type="hidden" id="doorId"  value="'+data[0].sd_id+'"/>');
		  						fantian(1,data[0].doorid);
		  				}else{
		  					showMessage("Door["+seachDoor+"] not found","alert");
		  					return false;
		  				}
	  				}
	  			}
	  		}
	    });
	}
	
	function seach(flag){
		var seachDoor=$('#seachDoor').val();
		var ps_id=<%=ps_id%>;
		var mainId=<%=mainId%>;
		var zone_id=$("#allzone").val();
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindReadyDoorSeachAction.action',
	  		dataType:'json',
	  		data:"doorName="+seachDoor+'&ps_id='+ps_id+'&mainId='+mainId+'&zone_id='+zone_id+'&flag='+flag,
	  		success:function(data){
	  		 if(flag==1){
		  		var html='';
		  		for(var i=0;i<data.length;i++){
					html+='<tr ondblclick="doubleClick(this)" onclick="singleClick('+data[i].sd_id+',<%=ps_id %>,this)" name="warehouse_tr">'+
						    '<td style="border-bottom:1px solid #dddddd;" width="10%" height="30px" align="center">'+
						     	'<input name="doorId" type="radio"  value="' + data[i].sd_id + '" text="'+data[i].doorid+'" onclick="getLocation('+data[i].sd_id+',<%=ps_id %>)" />'+
						    ' </td>'+
					        '<td style="border-bottom:1px solid #dddddd" >Door ['+data[i].doorid+']</td>'+
					   '</tr>';
		  		}

		  		$('#readyDoor').html(html);
	  		}else{
  				if(seachDoor!=""){
  					if(data.length>0){
	  						$("#search_tb").append('<input name="doorId" type="hidden" id="doorId"  value="'+data[0].sd_id+'"/>');
	  						fantian(1,data[0].doorid);
	  				}else{
	  					showMessage("Door["+seachDoor+"] not found or occupied","alert");
	  					return false;
	  				} 
  				}
  			}
	  		}
	    });
	}
	
	
	
	
	function searchZoneDoor(){
		var searchZoneDoor = $("#zoneDoor").val();
		var mainId=<%=mainId%>;
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindZoneReadyDoorAction.action',
			data:'zoneId='+<%=zoneId%>+'&doorName='+searchZoneDoor+'&mainId='+mainId,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
  				if(searchZoneDoor!=""){
  					if(data.length>0){
	  						$("#search_tb").html('<input name="zoneDoorId" type="hidden" id="zoneDoorId"  value="'+data[0].sd_id+'"/>');
	  						fantianZone(1,searchZoneDoor);
	  				}else{
	  					showMessage("Door not found","alert");
	  					return false;
	  				} 
  				}
			},
			error:function(){
				showMessage("System error","error");
			}
		});
	}
	function findZoneBydoorName(doorName){
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindReadyDoorSeachAction.action',
	  		dataType:'json',
	  		async:false,
	  		data:"doorName="+doorName+"&ps_id="+<%=ps_id%>+"&mainId="+<%=mainId%>,
	  		success:function(data){
	  			if(data.length>0){
	  				$("#allzone").val(data[0].area_id);
	  			}
	  			
	  		}
	    });
	}
	function fantian(flag,doorName){
		var door = "";
		var dname ="";
		if(flag==1){
			door=$("#doorId").val();
			dname = doorName;
		}else{
			door = $("input[name='doorId']:checked").val();
			dname = $("input[name='doorId']:checked").attr("text");
		}
		if(door==undefined){
			showMessage("Please choose door First","alert");
			return false;
		}
		findZoneBydoorName(dname);
		var zone_id=$("#allzone").val();
		$.artDialog.opener.getDoor(door,dname,'<%=inputId%>',zone_id);
		$.artDialog.close();
	}

	
	function getDoorOrParking(door,doorName,zoneId){
		$.artDialog.opener.getDoor(door,doorName,'<%=inputId%>',zoneId);
		$.artDialog && $.artDialog.close();
	}

	function addwait(){
        var entry_id=<%=mainId%>;
        var zone_id=<%=zoneId%>;
        var order_id='<%=numVal%>';
        var order_type='<%=numName%>';
		//先添加wait 信息 返回id 
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/waiting_list.html?entry_id='+entry_id+'&zone_id='+zone_id+'&order_id='+order_id+'&order_type='+order_type; 
		$.artDialog.open(uri , {title: "Create Waiting list",width:'700px',height:'550px', lock: true,opacity: 0.3,fixed: true});;
		
	}

	function fantianZone(flag,doorName){
	   	if(flag==1){
			var door=$("#zoneDoorId").val();
			var doorName = doorName;
		}else{
			var door = $("input[name='zoneDoorId']:checked").val();
			var doorName = $("input[name='zoneDoorId']:checked").attr("text");
		}
		if(door==undefined){
			showMessage("Please choose door First","alert");
			return false;
		}
		$.artDialog.opener.getDoor(door,doorName,'<%=inputId%>');
		$.artDialog.close();
	}
	function getDoorInMap(){
		var width = window.parent.innerWidth;
		var height = window.parent.innerHeight;
		width = parseInt(width*0.9);
		height = parseInt(height*0.9);
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/getParkingDocksInMap.html?storageId='+<%=ps_id%>+'&type=docks&width='+width+'&height='+height; 
		$.artDialog.open(uri , {title: "Map",width:width+'px',height:height+'px', lock: true,opacity: 0.3,fixed: true});
	}

	function findDoorsByZoneId_1216(){
		$('#seachDoor').val('');//选择区域时，清空输入框
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindDoorsByZoneIdAction.action',
			data:'zoneId='+$("#allzone").val()+"&mainId="+<%=mainId%>+"&ps_id="+<%=ps_id%>,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    },
			success:function(data){
				var html='';
		  		for(var i=0;i<data.length;i++){
		  			if(data[i].sd_id==<%=inputValue%>){
		  				html+='<tr ondblclick="doubleClick(this)" bgcolor="#E6F3C5"  onclick="singleClick('+data[i].sd_id+',<%=ps_id%>,this)" name="warehouse_tr">'+
					    '<td style="border-bottom:1px solid #dddddd;" width="10%" height="30px" align="center">'+
					     	'<input name="doorId" type="radio" checked="checked" value="'+data[i].sd_id+'" text="'+data[i].doorid+'" onclick="getLocation('+data[i].sd_id+',<%=ps_id%>)" />'+
					    ' </td>'+
				        '<td style="border-bottom:1px solid #dddddd" >Door ['+data[i].doorid+']</td>'+
				   '</tr>';
		  			}else{
		  				html+='<tr ondblclick="doubleClick(this)" onclick="singleClick('+data[i].sd_id+',<%=ps_id %>,this)" name="warehouse_tr">'+
					    '<td style="border-bottom:1px solid #dddddd;" width="10%" height="30px" align="center">'+
					     	'<input name="doorId" type="radio"  value="'+data[i].sd_id+'" text="'+data[i].doorid+'" onclick="getLocation('+data[i].sd_id+',<%=ps_id %>)" />'+
					    ' </td>'+
				        '<td style="border-bottom:1px solid #dddddd" >Door ['+data[i].doorid+']</td>'+
				   '</tr>';
		  			}
					
		  		}

		  		$('#readyDoor').html(html);
			},
			error:function(){
			}
		});
	}
	function findDoorsByZoneId(){
		$('#seachDoor').val('');//选择区域时，清空输入框
		$("#useDoorZone").val($("#allzone").val());
		findUseDoorsByZoneId()
	}

	function findUseDoorsByZoneId(){
		document.useDoor.usezoneId.value=$("#useDoorZone").val();
		document.useDoor.zoneId.value=$("#useDoorZone").val();
		document.useDoor.cmd.value='1';
		document.useDoor.inputId.value=$("#inputId").val();
		document.useDoor.submit();
	}
	$('#seachDoor').focus();

</script>
