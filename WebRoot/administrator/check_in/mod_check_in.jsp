<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 long ps_id =  adminLoggerBean.getPs_id();
 long main_id = StringUtil.getLong(request,"main_id");
 String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
 String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
 String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
 DBRow[] rows = checkInMgrZwb.selectAllMain(main_id,null);
 DBRow[] files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(main_id,FileWithTypeKey.OCCUPANCY_MAIN);
 %>	
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<!--  基本样式和javascript -->
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<script language="javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<!-- 在线阅读 -->
	<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script> 
	
	<!-- 打印 -->
	<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
	<script type="text/javascript" src="../js/print/m.js"></script>

	<style type="text/css">
	html, body { margin:0px; padding:0px; }
	body, td, th, pre, code, select, option, input, textarea { font-family:"Trebuchet MS", Sans-serif; font-size:10pt; }
	.demo {
		 float:left; 
		 margin:15px;
		 border:0px solid gray; 
		 font-family:Verdana;
		 font-size:12px;
		 background:white;
		 width:80%;

	}
</style>
	
		
<style type="text/css">
td.topBorder{
	border-top: 1px solid silver;
}
.ui-datepicker{font-size: 0.9em;}
</style>
<script type="text/javascript">


$(function(){
	$("#gps_tracker").blur(function(){
	  if( $("#gps_tracker").val()!=""){
		 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindAssetByGPSNumberAction.action',
			data:'gps_tracker='+$("#gps_tracker").val(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				if(data.length==0){
					alert("GPS is invalid!please Check before Submit.");
					$("#mark").val(1);
				}else{
					$("#gpsId").val(data.id);
					$("#initgpsId").val(0);
				}
				
			},
			error:function(){
				alert("System error"); 
			}
		 });
	   }
	 });


	 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_mod_gate.html',
			dataType:'html',
			async:false,
			data:'main_id=<%=main_id%>',
			success:function(data){	          
				$('#av').html(data);
			}
	 });

	 
});
function modCheckIn(){
	if($("#gate_driver_last_name").val()==""){

		alert("Please enter Driver's Last Name");
		return  false;
	}
	if($("#gate_driver_first_name").val()==""){

		alert("Please enter Driver's First Name");
		return  false;
	}
	if($("#appointment_time").val()==""){

		alert("Please enter Appointment");
		return  false;
	}
	if($("#driver_liscense").val()==""){

		alert("Please enter Driver License");
		return  false;
	}
	if($("#liscense_plate").val()==""){

		alert("Please enter Licese Plate");
		return  false;
	}
	if($("#company_name").val()==""){

		alert("Please enter Cariler");
		return  false;
	}
	if($("#gate_container_no").val()==""){

		alert("Please enter Trailer Number");
		return  false;
	}
	if($("#gps_tracker").val()==""){

		alert("Please enter GPS");
		return  false;
	}

	else{	
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/modCheckInAction.action',
			data:$("#mod_form").serialize()+'&'+$("#fileform").serialize(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				parent.location.reload();        
				
				
			},
			error:function(){
				alert("System error"); 
			}
		});
	}
	
}

function getYardNo(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/get_yard_no.html?storageId=<%=ps_id %>'; 
	$.artDialog.open(uri , {title: "Parking Lot",width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	
}
//停车位回填
function setYardNo(yardId,yardNo){
	$("#yc_no").val(yardNo);
	$("#yc_id").val(yardId);
}
function closeWindow(){
	$.artDialog.close();
}

</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<div id="tabs">
	<ul>
		<li><a href="#av1">GateCheckIn</a></li>	 
	</ul>
	<div id="av1">
		<div style="border:0px solid red" id="av">213123</div>
	</div>
</div>
<script type="text/javascript">
 function modWarehouse(){
	 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_mod_warehouse.html',
			dataType:'html',
			async:false,
			data:'main_id=<%=main_id%>',
			success:function(data){	          
				$('#av3').html(data);
			}
	 });
 }

 function modWindow(){
	 var zone_id=1000;
	 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_mod_window.html',
			dataType:'html',
			async:false,
			data:'id=<%=main_id%>&zone_id='+zone_id,
			success:function(data){	          
				$('#av2').html(data);
			}
	 });
 }
//文件上传
	function uploadFile(_target){
	    var targetNode = $("#"+_target);
	    var fileNames = $("input[name='file_names']").val();
	    var obj  = {
		     reg:"picture_office",
		     limitSize:8,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames && fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: 'Upload Photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
	    		 close:function(){
						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
						 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
	   		 }});
	}
	//Online photo
	function onlineScanner(target){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target="+target; 
		$.artDialog.open(uri , {title: 'Online Photo',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}
	
	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,target){
		    $("p.new").remove();
			if($.trim(fileNames).length > 0 ){
				$("input[name='file_names']").val(fileNames); 
				var array = fileNames.split(",");
				for(var index = 0 ; index < array.length ; index++ ){
					if(array[index].length>0){
						var a1 =  createA1(array[index]) ;
						var a2 =  createA2(array[index]) ;
						$("#add_file").append("<tr>"+a1+""+a2+"</tr>"); 
				
					}
				}
			} 
	}
	function createA1(fileName){
	    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
	    var id = fileName.substring(0,fileName.length-4);
	    var  a = "<td id='add"+id+"'><p style='color:#439B89' class='new' ><a  href="+uri+" style='color:#439B89' >"+fileName+"</p></td>";
	    return a ;
	}
	function createA2(fileName){
	    var id = fileName.substring(0,fileName.length-4);
	    var  a = "<td id='del"+id+"'><p style='color:#439B89' class='new' ><a  href='javascript:deleteA(&quot;"+id+"&quot;)' style='color:red' >×</p></td>";
	    return a ;
	}
	function deleteA(id){
		$("#add"+id+" *").remove();
		$("#del"+id+" *").remove();
		var file_names = $("input[name='file_names']").val(); 
		
		 var array = file_names.split(",");
		 var lastFile = "";
		 for(var index = 0 ; index < array.length ; index++ ){
				if(id==array[index].substring(0,array[index].length-4)){
					array[index]="";
					
				}
				lastFile += array[index];
				if(index!=array.length-1){
					lastFile+=",";
				}
		 }
		 
		 $("input[name='file_names']").val(lastFile);
		
	}
	function closeWindow(){
		$.artDialog.close();
	}
	//图片在线显示  		 
	function showPictrueOnline(fileWithType,fileWithId ,currentName){
	  var obj = {
	  		file_with_type:fileWithType,
	  		file_with_id : fileWithId,
	  		current_name : currentName ,
	  		cmd:"multiFile",
	  		table:'file',
	  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/check_in"
		}
	  if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}
	//删除图片
	function deleteFile(file_id){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'file',file_id:file_id,folder:'check_in',pk:'file_id'},
			success:function(data){
				if(data && data.flag === "success"){
					$("#file_tr_"+file_id).remove();
				}else{
					showMessage("System eroor,please try later","error");
				}
				
			},
			error:function(){
				showMessage("System eroor,please try later","error");
			}
		});
	}
</script>
</body>
<!-- 树形结构 -->
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/tree/jquery.js"></script>
<script type="text/javascript" src="../js/tree/jquery.tree.js"></script>
<script type="text/javascript" src="../js/tree/jquery.tree.checkbox.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- cookie -->
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script>
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	cookie: { expires: 30000 } ,
});

$("#tabs").tabs("select",0);
</script>