<%@page import="com.cwc.app.key.ProcessKey"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey" %>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/> 
<%
	long dlo_id = StringUtil.getLong(request,"dlo_id");
	int associate_process = StringUtil.getInt(request,"associate_process");
	
%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<head>
	
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">
	
	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

	<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
	
	</style>
	<script type="text/javascript">
	
	</script>
	
</head>
<body>
	<div>
		<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:90%;margin-left:30px;">
  					<legend style="font-size:14px;font-weight:normal;color:#999999;">
  						Detail
  					</legend>
  					<%DBRow[] noticedResult = checkInMgrWfh.findScheduleByMainId(dlo_id, associate_process);//查询gate的通知
  						if(noticedResult!=null && noticedResult.length>0){ %>
  					<table width="" border="0" cellspacing="0" cellpadding="0" >
  							<%
	  						 	for(int i=0;i<noticedResult.length;i++){				 	
					 				int numberType = noticedResult[i].get("number_type", 0);
	 	     						DBRow[] users = (DBRow[])noticedResult[i].get("scheduleUser", new DBRow[0]);
					 	     %>
				 	     			<tr >
					 	     			<%if(numberType==moduleKey.CHECK_IN_LOAD||numberType==moduleKey.CHECK_IN_PONO||numberType==moduleKey.CHECK_IN_ORDER||numberType==moduleKey.CHECK_IN_PICKUP_ORTHERS|| numberType == moduleKey.SMALL_PARCEL){ %>
									   	<td style="color:blue;padding:0px;" align="right" width="64px;"><%=moduleKey.getModuleName(noticedResult[i].get("number_type", "")) %>:</td>
									   	<%}else if(numberType==moduleKey.CHECK_IN_BOL||numberType==moduleKey.CHECK_IN_CTN||numberType==moduleKey.CHECK_IN_DELIVERY_ORTHERS){ %>
									   	<td style="color:red;padding:0px;" align="right" width="64px;"><%=moduleKey.getModuleName(noticedResult[i].get("number_type", "")) %>:</td>
									   	<%}else{ %>
									  	<td align="right" style="padding:0px;" width="64px;"></td>
									  	<%} %>
				 	     				<td style="padding:0px;width:150px" height="30px">&nbsp;<%=noticedResult[i].get("number", "") %></td>
				 	     				<td width="500px">
				 	     					<%=noticedResult[i].get("schedule_detail", "") %>
				 	     				</td>
				 	     				
				 	     				<td style="color:#bbb;padding:0px;width:120px">
				 	     				<%if(users!=null&&users.length>0){ 
				 	     					if(!StringUtil.isBlank(users[0].get("schedule_finish_name","")) && noticedResult[i].get("associate_process", 0)!=ProcessKey.CHECK_IN_WINDOW){
				 	     					%>
				 	     						Closed by
				 	     				<%}else if(!StringUtil.isBlank(users[0].get("schedule_finish_name","")) && noticedResult[i].get("associate_process", 0)==ProcessKey.CHECK_IN_WINDOW){
				 	     						out.print("Assigned Task By");
				 	     					}
				 	     				} %>
				 	     				</td>
				 	     				<td width="105px" style="padding-left:5px">
				 	     					<%=users[0].get("schedule_finish_name","") %>
				 	     				
				 	     				</td>
				 	     				
				 	     			</tr> 		
				 	     			<tr>
				 	     				<td style="text-align: right;">Notified:</td>
				 	     				<td style="padding:0px;width:125px;padding: 0 0 0 5px;" colspan="5"> 
				 	     					<%
				 	     						for(int j=0;j<users.length;j++){
												%>
													<b><%=users[j].get("schedule_execute_name","")%></b>
													<%=j==users.length-1?"":"," %>
												<%					 	     							
				 	     						}
				 	     					%>
													
				 	     				</td>
				 	     			</tr>
						 		    	<%-- <div style="clear:both;padding-left: 7	px;">
						  						<span >Notified:</span>
						  						<span ><%=noticedResult[i].getString("employe_name") %>&nbsp;<a href="javascript:void(0)" onclick="detailNotices('<%=noticedResult[i].get("schedule_sub_id",0l) %>')" style="color:green;">Detail</a></span>
					  					</div> --%>
					 									 				
						   	<%	}%>
							</table>
							<%} %>
  				</fieldset>
  			
	</div>
</body>