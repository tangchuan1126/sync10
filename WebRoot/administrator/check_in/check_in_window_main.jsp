<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="checkInEntryPriorityKey" class="com.cwc.app.key.CheckInEntryPriorityKey"/>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link rel="stylesheet" type="text/css" href="../js/picture/picture.css" />
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnlineServ.js"></script> 

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<!-- 时间 -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 
 <link rel="stylesheet" type="text/css" href="../js/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="../js/easyui/themes/icon.css" />

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉框多选 -->
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.filter.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.filter.min.js"></script>	

<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link rel="stylesheet" href="../buttons.css">
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<%
	long entry_id=StringUtil.getLong(request,"entry_id");
	DBRow row=checkInMgrZwb.findMainById(entry_id);
	int priority = row.get("priority", 0);
%>
<script type="text/javascript">
$(function(){
	$.artDialog.open.api.close = function(){
		$.artDialog({
			id:'checkInOrWait',
			title:'Confirm',
			icon:'question',
			content:'Check In or Waiting?',
			lock:true,
			opacity:0.3,
			button:[
				{
		            name: 'Check In',
		            callback: function () {
		            	saveTaskAndClose('',0,1) 
		                return false;
		            }
		        },
		        {
		            name: 'Waiting',
		            callback: function () {
		            	saveTaskAndClose('',1,0)
		            	return false;
		            } 
		        },
		        {
		            name: 'Close',
		            callback: function(){
		            	window.parent.location.reload();
		            }
		        }
			],
		});
		return false;
	}
});
function saveTaskAndClose(evt,wait,check_in){
	
	  var priority_html = 'Priority: <select id="priority">';
	   priority_html+='<option value="0">NA</option>';
	   priority_html+='<option <%= priority==checkInEntryPriorityKey.LOW?"selected":""%> value="<%=checkInEntryPriorityKey.LOW%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.LOW)%></option>';
	   priority_html+='<option <%= priority==checkInEntryPriorityKey.MIDDLE?"selected":""%> value="<%=checkInEntryPriorityKey.MIDDLE%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.MIDDLE)%></option>';
	   priority_html+='<option <%= priority==checkInEntryPriorityKey.HIGH?"selected":""%> value="<%=checkInEntryPriorityKey.HIGH%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.HIGH)%></option>';
	   priority_html+='</select>';
	   
	   $.artDialog.list['checkInOrWait'] && $.artDialog.list['checkInOrWait'].close();
	   
	   $.artDialog({
		   	title:'priority',
		   	lock:true,
			opacity:0.3,
		   	content:priority_html,
		   	okVal: 'Confirm',
	   		ok: function () {
	    	
	    		var priority = $("#priority").val();

	    		var data={};
	    		data.items=[]; 
	    		var mainId=<%=entry_id %>;
	    		
	    		data.mainId=mainId;
	    		data.waiting = wait;
	    		data.checkin = 1;
    	      	$.ajax({
    	  	  		type:'post',
    	  	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowTasksAction.action',
    	  	  		dataType:'json',
    	  	  		data:'str='+JSON.stringify(data)+'&is_check_in='+check_in+'&priority='+priority,
    	  		 	success:function(data){
    			//	$.artDialog.opener.monitorCheckInWindowChanged();
    				
    	  		 		window.parent.location.reload();
    	  	  		},
    	  	  		error:function(serverresponse, status){
    	  	  			showMessage(errorMessage(serverresponse.responseText),"alert");
    	  	  			$("#tabs").tabs("select",2);
    	  	  		//	$.artDialog.close();
    	  	  		}
    	  	    });
	    		
	   		},
	   		cancelVal:'Cancel',
		    cancel:function(){
  		    		
	    	}
    		   
    	   });
	
}
</script>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>/administrator/check_in/json/group.js" type="text/javascript"></script>


<title>Check In</title>
</head>
<body>
<div class="demo" style="width:100%">
	  <div id="tabs">
		<ul>
			<li><a href="#check_in_entry">Driver Info</a></li>
			<li><a href="#check_in_equipments">Tractor/Trailer/CTNR</a></li>
			<li><a href="#check_in_tasks">Tasks</a></li>
			<li><a href="#check_in_photos">Photos</a></li>
		</ul>
		<div id="check_in_entry"></div>
		<div id="check_in_equipments"></div>
		<div id="check_in_tasks"></div>
		<div id="check_in_photos"></div>
	</div>
</div>
<script type="text/javascript">
		
		var tabsIndex = 0;//当前tabs的Index
		var flag_select = false;	// 过滤多次验证
		
		loadCurrentIndexInfo(tabsIndex);
		//var tab = 
		$("#tabs").tabs({
		cache: false,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		load: function(event, ui) {}	,
		selected:0,
		
		show:function(event,ui)
			 {
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   // 遮罩打开
				
				if(tabsIndex != ui.index)
				{
					if(checkLastIndexInfo(tabsIndex, ui.index) == false)
					{
						tabSelect(tabsIndex);
					}
					else
					{
						loadCurrentIndexInfo(ui.index);
						tabsIndex = ui.index;
					}
				}
				/*
				$(this).bind("tabsselect", function (event, ui) {
					if(checkLastIndexInfo(tabsIndex) == false) {
						flag_select = true;
					} else {
						loadCurrentIndexInfo(ui.index);
						tabsIndex = ui.index;
					}
				});					
				if(flag_select) {
					flag_select = false;
					tabSelect(tabsIndex);
					
					return;
				}
				
			
			 	*/
				$.unblockUI();
			 }
		});
		/*
		$("#tabs ul li").mousedown(function(event){  
	        var index = $(this).index();
	        
	        //console.log(tabsIndex+","+index);
	        
	        if (tabsIndex == index)
	            return;  
	        if (!checkLastIndexInfo(tabsIndex, index)) {
	         	return;
	        } else {  
	        	loadCurrentIndexInfo(index);
	        	tabsIndex = index;
	        }  
	    })
	    */
	    function disableOthers(index)
		{
			$("#tabs").tabs({disabled:[1,2,3,4]});
			$("#tabs").tabs('disableTab', index);
		}
		
		function tabSelect(index)
		{
			$("#tabs").tabs("select",index);
		}
		function checkLastIndexInfo(lastIndex, currentIndex)
		{
			if(lastIndex == 0)
			{
				return checkDriverInfo();
			}
			else if(lastIndex == 1)
			{
				return checkEquipments();
			}
			else if(lastIndex == 2)
			{
				return checkTasks();
			} 
			else if(lastIndex == 3) {
				return true;
			}
			return true;
		}
		function loadCurrentIndexInfo(currentIndex)
		{
			if(currentIndex == 0)
			{
				loadDriverInfo();
			}
			else if(currentIndex == 1)
			{
				loadEquipments();
			}
			else if(currentIndex == 2)
			{
				loadTasks();
			}
			else if(currentIndex == 3)
			{
				loadPhotos();
			}
		}

		function loadDriverInfo()
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_entry.html?entry_id=<%=entry_id%>';
			loadCheckInInfos(uri, "check_in_entry");
		}
		function loadEquipments()
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_equipments.html?entry_id=<%=entry_id%>';
			loadCheckInInfos(uri, "check_in_equipments");
		}
		function loadTasks()
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_tasks.html?entry_id=<%=entry_id%>';
			loadCheckInInfos(uri, "check_in_tasks");
		}
		function loadPhotos()
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_photos.html?entry_id=<%=entry_id%>';
			loadCheckInInfos(uri, "check_in_photos");
		}
		function loadCheckInInfos(uri, div)
		{
			$.ajax({
				url:uri,
				dataType:'html',
				async:false,
				data:'entry_id=<%=entry_id%>',
				success:function(html){	
					$('#'+div).html(html);
				}
			});
		}
		function submitLastTabInfos(lastIndex)
		{
			if(lastIndex == 0)
			{
				submitDriverInfo();
			}
		}
		
		function errorMessage(val){
			
			var error1 = val.split("[");
			var error2 = error1[1].split("]");	
			
			return error2[0];
		}
	</script>
</body>
</html>