<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%
	
	long dlo_id = StringUtil.getLong(request, "dlo_id");
	DBRow[] checkInLog = checkInMgrZwb.findCheckInLog(dlo_id);
	long showTimePsId = StringUtil.getLong(request, "ps_id");
%>
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	
	<!-- 打印 -->
	<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
	<script type="text/javascript" src="../js/print/m.js"></script>
	

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">

	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>
	
	
</head>
<body onLoad="onLoadInitZebraTable()" style="margin-top:0px">
<br/>
<br/>
<div style="font-weight: bold;font-size:16px;height:20px;background:#fff;padding-left: 20px; float: left; width:300px;">Entry ID: <%=dlo_id %></div>
<div align="right" style=" padding-right: 20px;">
	<input class="long-button-print" type="button" onclick="printLog()" value="Print">
</div>
<div style="height:10px">&nbsp;</div>
	  <!-- table 斑马线  -->
	  <div id="av1">
	  <table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"  isNeed="false" class="zebraTable mytable" isNeed="false" style="margin-left:3px;margin-top:10px;">
			  <tr> 
			  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Operator</th>
			  	<th width="15%" style="vertical-align: center;text-align: center;" class="right-title">Operation Time</th>
		        <th width="20%" style="vertical-align: center;text-align: center;" class="right-title">Operate</th>
		        <th width="25%" style="vertical-align: center;text-align: center;" class="right-title">Remarks</th>
		        <th width="" style="vertical-align: center;text-align: center;" class="right-title">Data</th>
		      </tr>
	        <%if(checkInLog!=null && checkInLog.length>0){%>
				<%for (int i = 0; i < checkInLog.length; i++) {%>
				    <%String time = checkInLog[i].getString("operator_time");%>
			      	<tr height="30px;">
			      		<td align="center"><%=checkInLog[i].getString("employe_name","") %></td>
			      		<td align="center"><%=DateUtil.showLocalparseDateTo24Hours(time,showTimePsId)%></td>
			      		<td align="center">
			      			<!-- checkout时，记录日志中无此数据，导致页面样式失效，处理其为空时，赋值为空格   by yuanxinyu -->
			      			<%if(checkInLogTypeKey.getCheckInLogTypeKeyValue(checkInLog[i].getString("log_type")) != ""){ %>
			      				<%=checkInLogTypeKey.getCheckInLogTypeKeyValue(checkInLog[i].getString("log_type"))%>
			      			<%}else{ %>
			      				&nbsp;
			      			<%} %>
			      		</td>
						<td align="center"><%=checkInLog[i].getString("note","") %></td>
						<td align="left">
						<%
						String data=checkInLog[i].getString("data");
						if(!data.equals("")){
							String[] str=data.split(",");
							for(int a=0;a<str.length;a++){
						%>	
						   <div style="border-bottom:1px dashed black;"><%= str[a]%></div>
						<%	
							}
						}
						%>&nbsp;
						</td>
			      	</tr>
			   <%}
       	    }%>      	  
	 </table>
	 </div>	
</body>
<script>
	function printLog(){
		 var printHtml=$('#av1').html();
		 
		 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
		 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");
		 
		 visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",printHtml);
		//visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",printHtml);
		 visionariPrinter.SET_PRINT_MODE("PRINT_PAGE_PERCENT","Width:100%;Height:80%");
		// visionariPrinter.SET_PRINT_MODE("PRINT_PAGE_PERCENT","90%");
		 visionariPrinter.SET_PRINT_COPIES(1);
		 
		 //visionariPrinter.PREVIEW();
		 visionariPrinter.PRINT();         		
	}
</script>
