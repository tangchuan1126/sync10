<%@page import="com.cwc.app.key.YesOrNotKey"%>
<%@page import="com.cwc.app.floor.api.wfh.FloorCheckInMgrWfh"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ModuleKey"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/>
<%@page import="com.fr.base.core.json.JSONArray"%>
<%@page import="com.fr.base.core.json.JSONObject"%>
<%@page import="com.cwc.app.beans.AdminLoginBean"%>

<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<!-- <link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script> -->
<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long ps_id = adminLoggerBean.getPs_id(); 
	String numberStr=StringUtil.getString(request, "number");
	int numberType = StringUtil.getInt(request, "number_type");
	String[] numbers=numberStr.split(",");
	int id = StringUtil.getInt(request, "id");
	String datas = StringUtil.getString(request, "datas");
 //	System.out.println(numberStr+","+datas);
	JSONObject jsonObj =   StringUtil.getJsonObject(datas);
	int system_type = StringUtil.getInt(request, "system_type");
	int type = StringUtil.getInt(request, "type");//1时是pickUp
	DBRow[] task = checkInMgrWfh.findTaskByNumber(numberStr, numberType);
%>
<title>Select</title>
<script type="text/javascript">

function selectLoad(CompanyID, number, CustomerID, AccountID,PONo, OrderNo,number_type,StagingAreaID,title,freightTerm,appointmentDate,order_status){
	//console.log("CompanyID:"+CompanyID+",number:"+number+",CustomerID:"+CustomerID+",AccountID:"+AccountID+",PONo:"+PONo
	//+",OrderNo:"+OrderNo+",numberType:"+number_type+",Stating:"+StagingAreaID+",Title:"+title+",FreightTerm:"+freightTerm+",appointmentDate:"+appointmentDate+",order_status:"+order_status+"");
	 $.artDialog.opener.setParentNumber  && $.artDialog.opener.setParentNumber(CompanyID, number, CustomerID, AccountID, PONo, OrderNo,<%=id%>,number_type,StagingAreaID,title,freightTerm,appointmentDate,order_status,'<%=system_type%>');
	 $.artDialog && $.artDialog.close();

}
function changeCursor(obj){
	 $(obj).css("cursor","Pointer"); 
}

</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	
	<div id="tabs">
	<ul>
		<li><a href="#av1">Search : <%=numberStr %></a></li>
	</ul>
	<%if(1==type){ %>
	<div id="av1">
		<div style="height:430px; overflow:auto;">
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
			  <tr style="height:35px;">
<!-- 			    <td style="border:1px solid #BBB; background-color:#E5E5E5; height:35px; font-weight:bold;" align="center">Search No</td> -->
			    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:80px;" align="center">Number</td>
			    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:50px;" align="center">Number<br>Type</td>
			    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:50px;" align="center">Search<br>No Type</td>
			    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:70px;" align="center">CompanyID</td>
			    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:70px;" align="center">CustomerID</td>
			    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:100px;" align="center">ORDER</td>
			    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:90px;" align="center">PO</td>
			    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:50px;" align="center">IsPicked</td>
			    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:100px;" align="center">No Scanning<br> Order</td>
			  </tr>
			  <%if(jsonObj != null){ %>
				<%JSONArray array =  StringUtil.getJsonArrayFromJson(jsonObj,"infos");%>
				<%if(array != null && array.length() > 0 ){%>
				<%for(int a=0;a<array.length();a++){%>
				<%JSONObject jsonItem = StringUtil.getJsonObjectFromArray(array,a);%>
<!-- 			  <tr style="background-color:#f9f9f9" name="seData"> -->
<!-- 			  	<td colspan="6" style="border-bottom:1px solid #BBB;border-left:1px solid #BBB;"> -->
<!-- 			  		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable"> -->
						<tr height="35px" id="<%=a%>" onclick="choice(this)" >
<!-- 							<td style=";" align="center"> -->
<%-- 						  		<span style="color:blue" valign="middle" id="search_number<%=a%>"><%=StringUtil.getJsonString(jsonItem,"search_number")%></span> --%>
<!-- 						  	</td> -->
							<td align="center">
			    				<span  style="color:blue" id="number<%=a%>">
			    				<%=StringUtil.getJsonString(jsonItem,"number")%>
			    				</span>
			    				<input type="hidden" id="number_status<%=a%>" value="<%=StringUtil.getJsonString(jsonItem,"status")%>"/>
							</td>
							<td align="center">
								<input type="hidden" id="number_type<%=a%>" value="<%=StringUtil.getJsonString(jsonItem,"order_type")%>"/>
			    				<span  style="color:blue" id="title<%=a%>">
			    					<%=moduleKey.getModuleName(StringUtil.getJsonString(jsonItem,"order_type"))%>
			    				</span>
							</td>
							<td style="" align="center">
						  		<input type="hidden" id="searhc_number_type<%=a%>" value="<%=StringUtil.getJsonString(jsonItem,"search_number_type")%>"/>
						  		<span valign="middle" id="search_number_type_title<%=a%>"><%=moduleKey.getModuleName(StringUtil.getJsonString(jsonItem,"search_number_type"))%></span>
						  	</td>
							<td align="center">
			    				<span id="company<%=a%>">
			    				<%=StringUtil.getJsonString(jsonItem,"companyid")%>
			    				</span>
							</td>
							<td align="center">
			    				<span id="customer<%=a%>">
			    				<%=StringUtil.getJsonString(jsonItem,"customerid")%>
			    				</span>
							</td>
							<td align="center">
			    				<span  id="order<%=a%>">
			    				<%=StringUtil.getJsonString(jsonItem,"orderno")%>
			    				</span>
			    				
			    				<%if(StringUtil.getJsonInt(jsonItem, "orders_len") > 0){ %>
			    					<%JSONArray loadOrders = StringUtil.getJsonArrayFromJson(jsonItem, "orders"); %>
			    					<%for(int i = 0; i < StringUtil.getJsonInt(jsonItem, "orders_len"); i ++){%>
			    						<%JSONObject order = loadOrders.getJSONObject(i); %>
			    						<%String orderSpan = ""; %>
			    						<%if(numberStr.equals(order.getString("orderno"))){ %>
			    							<%orderSpan = "color:blue;"; %>
			    						<%} %>
			    						<span style="<%=orderSpan%>"><%=order.getInt("orderno")%></span>
			    						<%if(i != StringUtil.getJsonInt(jsonItem, "orders_len")-1){out.println(",");} %>
			    					<%} %>
			    				<%} %>
			    				<%if(StrUtil.isBlank(StringUtil.getJsonString(jsonItem,"orderno")) && StringUtil.getJsonInt(jsonItem, "orders_len")==0 ){out.println("-");}  %>
							</td>
							<td align="center">
			    				<span id="po<%=a%>">
			    				<%=StringUtil.getJsonString(jsonItem,"pono")%>
			    				</span>
			    				<%if(StringUtil.getJsonInt(jsonItem, "ponos_len") > 0){ %>
			    					<%JSONArray loadOrders = StringUtil.getJsonArrayFromJson(jsonItem, "ponos"); %>
			    					<%for(int i = 0; i < StringUtil.getJsonInt(jsonItem, "ponos_len"); i ++){%>
			    						<%JSONObject order = loadOrders.getJSONObject(i); %>
			    						<%String orderSpan = "";%>
			    						<%if(numberStr.equals(order.getString("pono"))){ %>
			    							<%orderSpan = "color:blue;"; %>
			    						<%} %>
			    						<span style="<%=orderSpan%>"><%=order.getString("pono") %></span>
			    						<%if(i != StringUtil.getJsonInt(jsonItem, "ponos_len")-1){out.println(",");} %>
			    					<%} %>
			    				<%}%>
			    				<%if(StrUtil.isBlank(StringUtil.getJsonString(jsonItem,"pono")) && StringUtil.getJsonInt(jsonItem, "ponos_len")==0 ){out.println("-");}  %>
								<span style="display:none" id="accountId<%=a%>"><%=StringUtil.getJsonString(jsonItem,"accountid")%></span>
								<span style="display:none" id="staging<%=a%>"><%=StringUtil.getJsonString(jsonItem,"stagingareaid")%></span>
								<span style="display:none" id="freightTerm<%=a%>"><%=StringUtil.getJsonString(jsonItem,"freightterm")%></span>
								<span style="display:none" id="appointmentDate<%=a%>"><%=StringUtil.getJsonString(jsonItem,"appointmentdate")%></span>
							</td>
							<td align="center">
								<%=new YesOrNotKey().getYesOrNotKeyName(StringUtil.getJsonInt(jsonItem,"is_all_picked")) %>
							</td>
							<td align="center">
			    					<%boolean showFlag = true;
			    					if(StringUtil.getJsonInt(jsonItem,"search_number_type")== moduleKey.CHECK_IN_ORDER){
			    						if(StringUtil.getJsonInt(jsonItem,"is_all_picked") == YesOrNotKey.NO){
			    							showFlag = false;
			    							%>
			    							<span  id="order<%=a%>">
						    				<%=StringUtil.getJsonString(jsonItem,"orderno")%>
						    				</span>
			    							
			    							<%
			    						}
			    					}
									if(StringUtil.getJsonInt(jsonItem, "orders_len") > 0){ %>
			    					<%JSONArray loadOrders = StringUtil.getJsonArrayFromJson(jsonItem, "orders"); 
			    					%>
			    					<%for(int i = 0; i < StringUtil.getJsonInt(jsonItem, "orders_len"); i ++){%>
			    						<%JSONObject order = loadOrders.getJSONObject(i); 
			    						String is_all_picked = StringUtil.getJsonString(order, "is_all_picked");
			    						if("C".equals(is_all_picked)){
			    							continue;
			    						}
			    						showFlag = false;
			    						%>
			    						<%String orderSpan = ""; %>
			    						<%if(numberStr.equals(order.getString("orderno"))){ %>
			    							<%orderSpan = "color:blue;"; %>
			    						<%} %>
			    						<span style="<%=orderSpan%>"><%=order.getInt("orderno")%></span>
			    						<%if(i != StringUtil.getJsonInt(jsonItem, "orders_len")-1){out.println(",");} %>
			    					<%} %>
			    				<%} %>
			    				<%if(showFlag){out.println("-");}  %>
							</td>
						</tr>
<!-- 					</table> -->
<!-- 			  	</td> -->
<!-- 			  </tr> -->
			  <%}
						}
			  	} 
			  %>
			</table>
			</div>	
	</div>
	<%}else{ %>
		<div id="av1">
			<div style="height:430px; overflow:auto;">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
				  <tr style="height:35px;">
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:120px;" align="center">Number</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:70px;" align="center">Number Type</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:70px;" align="center">CompanyID</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:70px;" align="center">CustomerID</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:120px;" align="center">SupplierID</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:90px;" align="center">ReceiptNo</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:93px;" align="center">Appointment</td>
			    	<td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;width:60px;" align="center">Status</td> 
				  </tr>
				  <%if(jsonObj != null){ %>
					<%JSONArray array =  StringUtil.getJsonArrayFromJson(jsonObj,"infos");%>
					<%if(array != null && array.length() > 0 ){%>
					<%for(int a=0;a<array.length();a++){%>
					<%JSONObject jsonItem = StringUtil.getJsonObjectFromArray(array,a);%>
	<!-- 			  <tr style="background-color:#f9f9f9" name="seData"> -->
	<!-- 			  	<td colspan="6" style="border-bottom:1px solid #BBB;border-left:1px solid #BBB;"> -->
	<!-- 			  		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable"> -->
							<tr height="35px" id="<%=a%>" onclick="choice_delivery(this)" >
	<!-- 							<td style=";" align="center"> -->
	<%-- 						  		<span style="color:blue" valign="middle" id="search_number<%=a%>"><%=StringUtil.getJsonString(jsonItem,"search_number")%></span> --%>
	<!-- 						  	</td> -->
								<td align="center">
				    				<span  style="color:blue" id="number<%=a%>">
				    				<%=StringUtil.getJsonString(jsonItem,"number")%>
				    				</span>
				    				<input type="hidden" id="number_status<%=a%>" value="<%=StringUtil.getJsonString(jsonItem,"status")%>"/>
								</td>
								<td align="center">
									<input type="hidden" id="number_type<%=a%>" value="<%=StringUtil.getJsonString(jsonItem,"order_type")%>"/>
				    				<span  style="color:blue" id="title<%=a%>">
				    					<%=moduleKey.getModuleName(StringUtil.getJsonString(jsonItem,"order_type"))%>
				    				</span>
								</td>
								<td align="center">
				    				<span id="company<%=a%>">
				    				<%=StringUtil.getJsonString(jsonItem,"companyid")%>
				    				</span>
								</td>
								<td align="center">
				    				<span id="customer<%=a%>">
				    				<%=StringUtil.getJsonString(jsonItem,"customerid")%>
				    				</span>
								</td>
								<td align="center">
				    				<span id="supplier<%=a%>">
				    					<%=StringUtil.getJsonString(jsonItem,"supplierid")%>
				    				</span>
									<span style="display:none" id="appointmentDate<%=a%>"><%=StringUtil.getJsonString(jsonItem,"appointmentdate")%></span>
								</td>
								<td align="center">
									<span id="receipt_no<%=a%>">
				    					<%=StringUtil.getJsonString(jsonItem,"receiptno")%>
				    				</span>
								</td>
								<td align="center">
								<%if(!StringUtil.isBlank(StringUtil.getJsonString(jsonItem, "appointmentdate"))){
									String appointmentTime = StringUtil.getJsonString(jsonItem, "appointmentdate");
									appointmentTime = appointmentTime.substring(0, appointmentTime.length()-5);
									out.print(appointmentTime);
								}else{
									out.print("&nbsp;");
								}%></td>
								
								<td align="center">
								<%
									int wmsReceiptNo = StringUtil.getJsonInt(jsonItem,"receiptno");
									boolean flag = false;  //是否已经存在这个number&receiptNo
									for(DBRow row:task){
										int receiptNo = row.get("receipt_no", 0);
									//	System.out.println(wmsReceiptNo+"---"+receiptNo);
										if(wmsReceiptNo==receiptNo){
											flag = true;	//已经存在
											break;
										}
											
									}
									if(flag){	//存在为红色
								%>
									<span style="color:red;font-weight: bold;">Used</span>
									<%}else{ %>
									<span style="color:green;font-weight: bold;">Unused</span>
									<%} %>
								</td>
							</tr>
	<!-- 					</table> -->
	<!-- 			  	</td> -->
	<!-- 			  </tr> -->
				  <%}
							}
				  	} 
				  %>
				</table>
				</div>	
		</div>
	
	
	
	<%} %>
</div>
<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});

 $("#tabs").tabs("select",0);
</script>
	
</body>
</html>
<script>
	function changeColor(tr){
		$(tr).css("background-color","#E6F3C5");
	}
	function changeColorB(tr){
		$(tr).css("background-color","#f9f9f9");
	}
	function choice(tr){
		var id=$(tr).attr('id');
		var number=$('#number'+id).html().trim();
	    var number_type=$('#number_type'+id).val().trim();
		var CompanyID=$('#company'+id).html().trim();
		var CustomerID=$('#customer'+id).html().trim();
		var PONo=$('#po'+id).html().trim();
		var OrderNo=$('#order'+id).html().trim();
		var AccountID=$('#accountId'+id).html().trim();
		var StagingAreaID=$('#staging'+id).html().trim();
		var title=$('#title'+id).html().trim();
		var freightTerm=$('#freightTerm'+id).html().trim();
		var appointmentDate=$("#appointmentDate"+id).html().trim();
		var order_status=$("#number_status"+id).val().trim();
		selectLoad(CompanyID, number, CustomerID, AccountID,PONo, OrderNo,number_type,StagingAreaID,title,freightTerm,appointmentDate,order_status);
	}
	function choice_delivery(tr){
		var id=$(tr).attr('id');
		var number=$('#number'+id).html().trim();
	    var number_type=$('#number_type'+id).val().trim();
		var CompanyID=$('#company'+id).html().trim();
		var CustomerID=$('#customer'+id).html().trim();
		var title=$('#title'+id).html().trim();
		var appointmentDate=$("#appointmentDate"+id).html().trim();
		var order_status=$("#number_status"+id).val().trim();
		var supplier = $("#supplier"+id).html().trim();
		var receipt_no = $("#receipt_no"+id).html().trim();
		selectDelivery(CompanyID, number, CustomerID,number_type,title,appointmentDate,order_status,supplier,receipt_no);
	}
	
	function selectDelivery(CompanyID, number, CustomerID,number_type,title,appointmentDate,order_status,supplier,receipt_no)
	{

		$.artDialog.opener.setParentNumberDelivery  && $.artDialog.opener.setParentNumberDelivery(CompanyID, number, CustomerID,<%=id%>,number_type,title,appointmentDate,order_status,'<%=system_type%>',supplier,receipt_no);
		$.artDialog && $.artDialog.close();
	}
	
	function choice_load(tr){
		var id=$(tr).attr('id');
		var num=$(tr).attr('num');
		var number=$('#load'+id+'_load').html().trim();
	    var number_type=$('#number_type'+id+'_load').val().trim();
		var CompanyID=$('#company'+id+'_load').html().trim();
		var CustomerID=$('#customer'+id+'_load').html().trim();
		//var load=$('#load'+id+'_load').html().trim();
		var AccountID=$('#accountId'+id+'_load').html().trim();
		var StagingAreaID=$('#staging'+id+'_load').html().trim();
		var title=$('#title'+id+'_load').html().trim();
		var freightTerm=$('#freightTerm'+id+'_load').html().trim();
		var PONo='';
		var OrderNo='';
		selectLoad(CompanyID, number, CustomerID, AccountID,PONo, OrderNo,number_type,StagingAreaID,title,freightTerm)
	}
</script>
