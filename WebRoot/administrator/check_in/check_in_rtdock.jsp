<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.OccupyStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="occupyStatusTypeKey" class="com.cwc.app.key.OccupyStatusTypeKey"/>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		long psId=adminLoggerBean.getPs_id();  
		long adminId=adminLoggerBean.getAdid(); 
		PageCtrl pc = new PageCtrl();
		int p = StringUtil.getInt(request,"p");
		pc.setPageNo(p);
		pc.setPageSize(10);
		long ps_id = StringUtil.getLong(request,"ps_id");
		long zone_area = StringUtil.getLong(request,"zone_area");
		long doorStatus = StringUtil.getLong(request,"doorStatus");
		long dlo_id = StringUtil.getLong(request,"dlo_id");
		String cmd = StringUtil.getString(request,"cmd");
		long doorId = StringUtil.getLong(request,"doorId");
		long searchDoorId = StringUtil.getLong(request,"searchDoorId");
		long searchdlo_id = StringUtil.getLong(request,"searchdlo_id");
		int flag=StringUtil.getInt(request,"flag");
		String license_plate = StringUtil.getString(request,"license_plate");
		String trailerNo = StringUtil.getString(request,"trailerNo");
		long mainId = StringUtil.getLong(request,"mainId");
		if(ps_id==0){
	ps_id=psId;
		}
/**		if(doorStatus==0){
	doorStatus=2;
		}*/
		if(cmd.equals("check")){
	           checkInMgrZwb.verifyData(adminId,ps_id,mainId,license_plate,trailerNo,1,2);
		}
		if(cmd.equals("againPatrol")){
	           checkInMgrZwb.AgainPatrolDock(ps_id, zone_area);
	      //     checkInMgrZwb.ClearAreaPatrolDoneTime(ps_id, 2, zone_area);
		}
		DBRow[] ps =  checkInMgrZwb.findSelfStorage();
		DBRow[] allZone =checkInMgrZwb.findAllZone(ps_id);
		boolean isDone = true;
		for(int i = 0;i<allZone.length;i++){
	if(allZone[i].get("patrol_time", "").equals("")){
		isDone = false;
	}
		}
%>
<title>Check in</title>
<script type="text/javascript">
$(function(){
	$("#tabs").tabs({
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		show:function(event,ui)
			 {
			 	
			 }
	});
	
	 $("#tabs1").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
			show:function(event,ui)
			 {
				showCheckInDockZone(ui.panel.id);
			 }, 
			select:function(event,ui)
			{
				showCheckInDockZone(ui.panel.id);
			}
	 });
});

function showCheckInDockZone(zone_area,p){
	$.ajax({
		url:'check_in_rtdock_zone.html',
		data:"zone_area="+zone_area+"&p="+(p?p:0)+"&doorStatus="+<%=doorStatus%>+"&ps_id="+<%=ps_id%>,
		dataType:'html',
		type:'post',
		beforeSend:function(request){
	//	  $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(html){
	//    	$.unblockUI();
		   $("#"+zone_area).html(html);
		   
		},
		error:function(){
		}
	});
};


</script>
</head>
<body>
	<div class="demo" style="width:100%">
	  <div id="tabs">
		<ul>
			<li><a href="#checkin_filter">Advanced Search</a></li>
		</ul>
	      <div id="checkin_filter">
	      		<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left">
						<div style="float:left;">
						<select id="ps" name="ps">
							<option value="-1">All Storage</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("ps_id",0l)%>" <%=ps_id==ps[j].get("ps_id",0l)?"selected":"" %>><%=ps[j].getString("ps_name")%></option>
							<%
								}
							%>
						</select>
		     		  	<select id="doorStatus" name="doorStatus" >
							<option value="-1">Door Status</option>
							<option value="<%=OccupyStatusTypeKey.RESERVERED %>" <%=doorStatus==OccupyStatusTypeKey.RESERVERED?"selected":"" %>><%=occupyStatusTypeKey.getOccupyStatusValue(OccupyStatusTypeKey.RESERVERED)%></option>
							<option value="<%=OccupyStatusTypeKey.OCUPIED %>" <%=doorStatus==OccupyStatusTypeKey.OCUPIED?"selected":"" %>><%=occupyStatusTypeKey.getOccupyStatusValue(OccupyStatusTypeKey.OCUPIED)%></option>
							<option value="<%=OccupyStatusTypeKey.RELEASED %>" <%=doorStatus==OccupyStatusTypeKey.RELEASED?"selected":"" %>><%=occupyStatusTypeKey.getOccupyStatusValue(OccupyStatusTypeKey.RELEASED)%></option>
						</select>
						  <input type="button" class="button_long_search" value="Search" onclick="filter();"/>
					</td>
				</tr>
			</table>
	      </div>
       </div>
    </div>
    <br/>
    <div class="demo" style="width:100%">
	  <div id="tabs1">
		<ul>
			<li><a  href="#0" ><%=isDone==true?"<font color='green'>":"<font color='red'>"%>ALL</font></a></li>
			<%
   		  		if(allZone!=null && allZone.length>0){
   		  			for(int i=0;i<allZone.length;i++){
   		    %>
			<li><a   href="#<%=allZone[i].get("area_id",01)%>" ><%=!allZone[i].get("patrol_time","").equals("")?"<font color='green'>"+allZone[i].get("area_name","")+"</font>":"<font color='red'>"+allZone[i].get("area_name","")+"</font>"%></a></li>
			<%
   		  			}
   		  		}
			%>
		</ul>
		<div id="0"></div>
		<%
   		  		if(allZone!=null && allZone.length>0){
   		  			for(int i=0;i<allZone.length;i++){
  		%>
		<div id="<%=allZone[i].get("area_id",01)%>"></div>
		<%
  		  			}
  		  		}
		%>
	    
	  </div>
	</div>
	
  
	<form action="check_in_rtdock.html" method="post" name="filter_form">
		<input type="hidden" name="doorStatus" />
		<input type="hidden" name="ps_id"/>
	</form>
	<form action="check_in_rtdock.html" method="post" name="again_patrol">
		<input type="hidden" name="zone_area" />
		<input type="hidden" name="ps_id" />
		<input type="hidden" name="cmd" />
	</form>
	<form name="check_data" id="check_data" >
		<input type="hidden" name="license_plate"  />
		<input type="hidden" name="trailerNo"  />
		<input type="hidden" name="mainId"  />
		<input type="hidden" name="doubt"  />
		<input type="hidden" name="cmd" value="check"/>
	</form>
</body>
</html>
<script>
function createEntryId(doorId,doorName){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_createEntryId.html?doorId='+doorId+'&doorName='+doorName; 
	$.artDialog.open(uri , {title: "Create EntryID",width:'500px',height:'200px', lock: true,opacity: 0.3,fixed: true});
}
function filter(){
	
	document.filter_form.ps_id.value = $("#ps").val();
	document.filter_form.doorStatus.value = $("#doorStatus").val();
	document.filter_form.submit();
}



</script>