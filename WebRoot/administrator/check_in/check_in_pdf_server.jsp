<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@ include file="../../include.jsp"%>
<html>
<head>

<!--  基本样式和javascript -->
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">

<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js" id="first"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css" />



<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="js/showMessage/showMessage.js"></script>

<script type='text/javascript' src='../dwr/engine.js'></script>

<script type="text/javascript">



window.onerror=function  ajaxLogin(){
	var content='account=admin&pwd=@dm1n';
	var _request=new XMLHttpRequest();
	var IP=location.hostname;
	var _url='http://'+IP+'/Sync10/action/admin/AccountLoginMgrAction.action';
	_request.onreadystatechange=function(){
		 if (_request.readyState == 4) {
			    if (_request.status == 200) {
			      var str = _request.responseText;
			      console.log("返回数据为："+str);
			      location.replace(window.location.href);
			    }
			    else if(_request.status == 404){
			      console.log("请求资源不存在！");
			    }
			    else {
			      console.log("Ajax请求失败，错误状态为："+_request.status);
			    }
		}
	};
	_request.open('POST',_url,true);
	_request.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	_request.send(content);
};
dwr.engine.setActiveReverseAjax(true);
function loadHtml(){
	window.setTimeout(function(){
		$.ajax({
			url:window.location.href,
			timeout:30000,
			type:'get',
			async:false,
			success:function(){
				location.replace(window.location.href);
			},
			error:function(XMLHttpRequest,textStatus,errorThrown){
				loadHtml(); 
			},
		   });
	},30000)
};
$(function(){
	
	/* window.setInterval(function(){
		 $.ajax({
			url:"http://localhost/Sync10/dwr/call/plainpoll/ReverseAjax.dwr",
			timeout:5000,
			type:'post',
			error:function(XMLHttpRequest,textStatus,errorThrown){
					longPolling();
			},
		}); 
		dwr.engine.setActiveReverseAjax(true);
	},1000); */
	
    dwr.engine._errorHandler = function(message, ex) {
    		try{
    			loadHtml();
        	}catch(e){
        		loadHtml();
        	}
    }; 
});

</script>

<title>PDF SERVER</title>
</head>
<body >
	<div style="width:98%;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:center;">
		<h1>PDF SERVER</h1>
		
	</div>
	<div>
		<div style="float: left;font-weight: bold;">
			&nbsp;&nbsp;&nbsp;1、MasterBol &nbsp;&nbsp;&nbsp;2、Bol&nbsp;&nbsp;&nbsp; 3、Counting Sheet
		</div>
		<div align="center">
			EntryID:<input type="text" value="" id="getEntryId"/>
			DetailID:<input type="text" value="" id="getDetailId"/>
			<input id="creatPdf" type="button" name="creatPdf" value="CreatPDF" onclick="diaoYong()"/>
		</div>
	</div>
	<div style="width:98%;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:center;">
		<div style="height:60px; border:0px solid red; margin:10px;background-color:#FFF">
			<!-- 正在生成pdf 的 div -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="creatingPdf">
			 
			</table>
			<!-- 结束 -->
		</div>
	</div>
	<div style="width:98%;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:center;">
		<div style="height:400px; border:0px solid red; margin:10px;background-color:#FFF;overflow:scroll">
			<!-- 处理完pdf 的 记录 -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="endPdf">
			
			</table>
			<!-- 结束 -->
		</div>
	</div>
	<div id="printHtml" style="display: none;"></div>
</body>
</html>
<script>
function diaoYong(){
	var entry_id=$('#getEntryId').val();
	var detail_id=$('#getDetailId').val();
	if(entry_id==""){
		alert('请输入EntryID');
		return false;
	}
	if(detail_id==""){
		alert('请输入DetailID');
		return false;
	}
	createPDF(entry_id,detail_id);
}

	
//android端使用，会传递参数entry_id，web端测试暂时不加
function createPDF(entry_id,detail_id){
	//判断是否成功
	var master_flag = true;
	var bol_flag = true;
	var counting_flag = true;
	var ftp_flag = true;
	var ftp_flag = true;
	var load_no = "";
	var sign = false;//masterBol 默认不签字
	var bol_data=false;
	
//根据参数获取masterbol和bol签的url路径
   $.ajax({
	            url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/getPrintUrlByParam.action',
				async:false,
				dataType:'json',
				data:'entry_id='+entry_id+'&detail_id='+detail_id,
				success:function(data){
					if(data){
						load_no = data.number;
// 						if(load_no!=""){
							var trId = entry_id+"_"+load_no+"_Creating";
							var creating_pdf = '<tr id="'+trId+'"><td width="40%" align="center" valign="middle" height="60px;">'
											+'<span style="font-size: 22px; font-weight:bold;" >Entry ID:'+entry_id+'</span>'
											+'</td>'
											+'<td width="30%" align="center" valign="middle">'
											+'	<span style="font-size: 22px;font-weight:bold;" >Load:'+load_no+'</span>'
											+'</td>'
											+'<td align="center" valign="middle">'
											+'	<span style="font-size: 24px;font-weight:bold; color:green" ><img src="check_in/imgs/create_pdf.gif" style="margin-left: 20px;margin-top:5px; float: left;"> </span>'
											+'</td>'
											+'</tr>';
							$("#creatingPdf").html(creating_pdf);
// 						}
						//将masterbol的签上传pdf
						if(data.masterbolurl){
							for(i=0;i<data.masterbolurl.length;i++){
								var printUrl = data.masterbolurl[i].printurl;
// 								console.log(printUrl);
								$.ajax({
									url:printUrl,
									async:false,
									dataType:'html',
									data:'',
									success:function(html){
				 						$('#printHtml').html(html);
				 						//masterBol是否都上传成功
				 						if(master_flag){
				 							master_flag=androidToPdf();
				 						}else{
				 							androidToPdf();
				 						}
				 						sign = true;
									},
									error:function(){
										showMessage("系统错误","error");
									}
								});
							}
						}

						//将bol的签上传pdf
						if(data.bolurl){
							for(j=0;j<data.bolurl.length;j++){
								var printUrl = data.bolurl[j].printurl;
								$.ajax({
									url:printUrl,
									async:false,
									dataType:'html',
									data:'',
									success:function(html){
				 						$('#printHtml').html(html);
				 						//Bol是否都上传成功
				 						if(bol_flag){
				 							bol_flag=androidToPdf();
// 				 							console.log(bol_flag);
				 						}else{
				 							androidToPdf();
				 						}
				 						bol_data=true;
// 				 						console.log(bol_flag);
									},
									error:function(){
										showMessage("系统错误","error");
									}
								});
							}
						}
					}
					//追加counting sheet图片
					if(data&&(data.masterbolurl||data.bolurl)){
						$.ajax({
							url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/addCountingSheetToPdf.action',
							async:false,
							dataType:'json',
							data:'entry_id='+entry_id+'&detail_id='+detail_id,
							success:function(data){
								var result=$.parseJSON(data);
								//Bol是否都上传成功
		 						if(data.message=="false"){
		 							counting_flag=false;
		 						}
							}
						});
					}
					
// 					flag=true;
				},
				error:function(e){
					showMessage("系统错误","error");
				}
			});
			
		if(load_no!=""){
			 var master="OK";
			 var bol="OK";
			 var counting="OK";
			 var master_color = "green";
			 var bol_color = "green";
			 var c_color = "green";
			if(!master_flag){
				master="Error";
				master_color = "red";
			}else if(!sign){
				master="No Signature";
			}
			
			if(!bol_flag){
				bol="Error";
				bol_color = "red";
			}
			if(!bol_data){
				bol="No Data";
			}
			if(!counting_flag){
				counting="Error";
				c_color = "red";
			}
			
			var trId = entry_id+"_"+load_no+"_End";
			var end_pdf = '<tr id="'+trId+'"><td width="40%" align="center" valign="middle">'
							+'	<span style="font-size: 12px; font-weight:bold;" >Entry ID:'+entry_id+'</span>'
							+'</td>'
							+'<td width="30%" align="center" valign="middle">'
							+'	<span style="font-size: 12px; font-weight:bold;" >Load:'+load_no+'</span>'
							+'</td>'
							+'<td align="center" valign="middle">'
							+'	<span style="font-size: 12px;font-weight:bold; color:red" >End</span>'
							+'</td>'
							
							+'<td align="center" valign="middle">'
							+'	<span style="font-size: 12px;font-weight:bold; color:'+master_color+'" >'+master+'</span>'
							+'</td>'
							+'<td align="center" valign="middle">'
							+'	<span style="font-size: 12px;font-weight:bold; color:'+bol_color+'" >'+bol+'</span>'
							+'</td>'
							+'<td align="center" valign="middle">'
							+'	<span style="font-size: 12px;font-weight:bold; color:'+c_color+'" >'+counting+'</span>'
							+'</td>'
							+'</tr>';
// 			$("#endPdf").remove("#"+trId);
			$("#endPdf").append(end_pdf);
		}
		
		$("#creatingPdf").html("");
	 }
	
</script>
