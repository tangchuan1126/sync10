<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="wmsSearchItemStatusKey" class="com.cwc.app.key.WmsSearchItemStatusKey"></jsp:useBean>
<html>
<head>

<title>print wms</title>
<%
	long entryId = StringUtil.getLong(request, "entryId");
    DBRow mainRow=checkInMgrZwb.findGateCheckInById(entryId);
    String bolNo=StringUtil.getString(request, "bol");
    String ctnNo=StringUtil.getString(request, "ctnr");
    long receipt_no = StringUtil.getLong(request, "receipt_no");
    String door_name=StringUtil.getString(request, "door_name");
    long adid = StringUtil.getLong(request, "adid");
    int occupy_type = OccupyTypeKey.DOOR;
	//DBRow[] receiptsByEntry = checkInMgrZwb.findBolNoContainerNoNotDetailsByEntryId(entryId);
	String CompanyID = StringUtil.getString(request, "CompanyID");
    String CustomerID = StringUtil.getString(request, "CustomerID");
%>
<script>
	function printReceiptWms(){
		var printHtml=$('div[name="printWmsReceipt"]');
		for(var i=0;i<printHtml.length;i++){
			 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Receipts Label");
	      	 visionariPrinter.SET_PRINTER_INDEXA ("LabelPrinter");//指定打印机打印  
	      	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
	         visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			 visionariPrinter.ADD_PRINT_TABLE("0.4cm",7,"100%","100mm",$(printHtml[i]).html());
			 visionariPrinter.SET_PRINT_COPIES(1);
			 visionariPrinter.ADD_PRINT_HTM("14cm",160,"100%","100%",addStyleForText("CONTINUED..."));
			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
			 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","Last");
			//visionariPrinter.PREVIEW();
			visionariPrinter.PRINT(); 
			 
		}
		 
	}
	
	function addStyleForText(text)
	{
		return '<span style="font-size: 10;font-family:Verdana;">'+text+'</span>';
	}
</script>
</head>
<body>
		<% 
		long ps_id = 0;
		if(adid == 0l){
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		    adid = adminLoggerBean.getAdid();
		    ps_id = adminLoggerBean.getPs_id();
		}else{
			ps_id = adminMgr.getDetailAdmin(adid).get("ps_id",0l);
		}
		
			DBRow[] receipts = sqlServerMgr.findReceiptsByReceiptBolNoOrContainerNo(receipt_no,bolNo, ctnNo, CompanyID,CustomerID , adid, request);
			//   findReceiptsByBolNoOrContainerNo
			int count = 0;
			for(int b = 0; b < receipts.length; b ++){
				DBRow receipt = receipts[b];
		 %>
		 		<div id="printWmsReceipt" name="printWmsReceipt" style="width:363px">
		 			<table width="368px" border="0" align="left" cellpadding="0" cellspacing="0">
						<thead>
						  <tr style="width: 100%;">
						  	<td style="width: 100%;" colspan="7">
								<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
									<tr>
										<td width="40%" align="left">
											<span style="font-size: 8;font-weight: bold;font-family: Verdana;" >
												<%=DateUtil.showLocalparseDateTo24Hours(DateUtil.NowStr(),ps_id)+"&nbsp;(&nbsp;"+receipt.getString("CompanyID")+"&nbsp;)" %>
											</span>
										</td>
										<td width="42%" align="left">
											<span style="font-size: 10;font-weight: bold;font-family: Verdana;">RECEIPTS  TICKET</span>
										</td>
										<td width="18%" align="left">
											<span style="font-size: 8;font-family:Verdana;">PAGE:</span>
											<span style="font-size: 8;font-family:Verdana;" tdata='pageNO'>#</span>
											<span style="font-size: 8;font-family:Verdana;">/</span>
											<span style="font-size: 8;font-family:Verdana;" tdata='PageCount'>#</span>
										</td>
									</tr>
								</table>
							</td>
						  </tr>
						  <tr style="width: 100%;">
							<td style="width: 100%;" colspan="7">
								<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
									<tr>
										<td width="31%" align="left">
											<span style="font-size: 8;font-weight: bold;font-family: Verdana;">ENTRY ID:&nbsp;<%=entryId %></span>
										</td>
										<td colspan="2" align="left">
											<span style="font-size: 8;font-weight: bold;font-family: Verdana;">
												<%if(OccupyTypeKey.SPOT==occupy_type){ %>
													SPOT:&nbsp;<%=door_name%>
												<%}else{ %>
													DOCK:&nbsp;<%=door_name %>
												<% }%>
											</span>
										</td>
									 </tr>
									<tr>
										<td width="31%" align="left">
											<span style="font-size: 8;font-weight: bold;font-family: Verdana;">CUSTOMER:&nbsp;<%=receipt.getString("CustomerID") %></span>
										</td>
										<td width="32%" align="left">
											<span style="font-size: 8;font-weight: bold;font-family: Verdana;">PO NO.:&nbsp;<%=receipt.getString("PONo") %></span>
										</td>
										<td width="37%" align="right">
											<span style="font-size: 8;font-weight: bold;font-family: Verdana;">DEVANNED DATE:&nbsp;<%=DateUtil.parseDateFormat(receipt.getString("DevannedDate"), "MM-dd-yy")  %></span>&nbsp;&nbsp;
										</td>
									 </tr>
								</table>
							</td>
						  </tr>
						  <tr style="width: 100%;">
							<td style="width: 100%;" colspan="7">
								<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
									<tr>
										<td colspan="2" align="left">
											<span style="font-size: 8;font-weight: bold;font-family: Verdana;">RECEIPT NO:&nbsp;<%=receipt.get("ReceiptNo", 0L) %></span>
										</td>
									</tr>
									<tr>
										<td width="55%" align="left">
											<span style="font-size: 8;font-weight: bold;font-family: Verdana;">REFERENCE NO:&nbsp;<%=receipt.getString("ReferenceNo") %></span>
										</td>
										<td width="45%" rowspan="2" align="right">
											<img src="/barbecue/barcode?data=<%="90"+ MoneyUtil.fillZeroByRequire(receipt.get("ReceiptNo", 0L), 7)  %>&width=1&height=20&type=code39" />&nbsp;&nbsp;
										</td>
									</tr>
									<tr>
										<td width="55%" align="left">
											<span style="font-size: 8;font-weight: bold;font-family: Verdana;">CONTAINER NO:&nbsp;<%=receipt.getString("ContainerNo") %></span>
										</td>
									</tr>
								 </table>
							</td>
						  </tr>
						  <tr style="width: 100%;">
							 <td style="width: 9%;border-top: 1px solid black; border-bottom: 1px solid black;" align="left"><span style="font-size: 8;font-weight: bold;font-family: Verdana;">LINE</span></td>
						     <td style="width: 24%;border-top: 1px solid black;border-bottom: 1px solid black;" align="left"><span style="font-size: 8;font-weight: bold;font-family: Verdana;">ITEM ID</span></td>
						     <td style="width: 14%;border-top: 1px solid black;border-bottom: 1px solid black;" align="left"><span style="font-size: 8;font-weight: bold;font-family: Verdana;">SUPPLIER</span></td>
						     <td style="width: 20%;border-top: 1px solid black;border-bottom: 1px solid black;" align="left"><span style="font-size: 8;font-weight: bold;font-family: Verdana;">LOT NO.</span></td>
						     <td style="width: 10%;border-top: 1px solid black;border-bottom: 1px solid black;" align="left"><span style="font-size: 8;font-weight: bold;font-family: Verdana;">PLATE</span></td>
						     <td style="width: 14%;border-top: 1px solid black;border-bottom: 1px solid black;" align="left"><span style="font-size: 8;font-weight: bold;font-family: Verdana;">LOCATION</span></td>
						     <td style="width: 9%;border-top: 1px solid black;border-bottom: 1px solid black;" align="right"><span style="font-size: 8;font-weight: bold;font-family: Verdana;">QTY</span></td>
						 </tr>
						</thead>
						  <%	DBRow[] receiptLines = sqlServerMgr.findReceiptLinesPalteByBolNoOrCtnNO(bolNo, ctnNo, receipts[b].get("ReceiptNo", 0), adid, request); %>
						  <%
						  		for(int i = 0; i < receiptLines.length; i ++)
						  		{
						  			DBRow receiptLine = receiptLines[i];
						  			int line = receiptLine.get("LineNo", 0);
						  %>
						  <tr style="width: 100%;">
						    <td align="left" colspan="7" style="padding-top: 3px;">
						    &nbsp;<img src="/barbecue/barcode?data=<%="96"+ MoneyUtil.fillZeroByRequire(receipt.get("ReceiptNo", 0L), 7)+MoneyUtil.fillZeroByRequire(line, 3)  %>&width=1&height=20&type=code39" />
						    </td>
						  </tr>
						  <tr style="width: 100%;">
						    <td align="left" width="9%">
						    	<span style="font-size: 8;font-weight: bold;font-family: Verdana;"><%=MoneyUtil.fillZeroByRequire(++count, 3)%></span>
						    </td>
						    <td align="left" width="24%">
						    	<span style="font-size: 8;font-weight: bold;font-family: Verdana;"><%=receiptLine.getString("ItemID") %></span>
						    </td>
						    <td align="left" width="14%">
						    	<span style="font-size: 8;font-weight: bold;font-family: Verdana;"><%=receiptLine.getString("SupplierID") %></span>
						    </td>
						    <td align="left" width="20%">
						    	<span style="font-size: 8;font-weight: bold;font-family: Verdana;"><%=receiptLine.getString("LotNo") %></span>
						    </td>
						    <td align="left" width="10%">
						    	<span style="font-size: 8;font-weight: bold;font-family: Verdana;"><%=0==receiptLine.get("PlateNo", 0L)?"":receiptLine.get("PlateNo", 0L)+"" %></span>
						    </td>
						    <td align="left" width="14%">
						    	<%
						    		DBRow locationRow = sqlServerMgr.findLocationWmsByPlateNo(receiptLine.get("PlateNo", 0), receipt.getString("CompanyID"));
						    		if(null != locationRow)
						    		{
						    	%>
						    	<span style="font-size: 8;font-weight: bold;font-family: Verdana;"><%=locationRow.getString("LocationID") %></span>
						    	<%		
						    		}
						    		else
						    		{
								%>
						    		&nbsp;
						    	<%		
						    		}
						    	%>
						    </td>
						    <td align="right" width="9%">
						    	<span style="font-size: 8;font-weight: bold;font-family: Verdana;"><%=receiptLine.get("ExpectedQty", 0) %></span>
						    </td>
						  </tr>
						  <%
						  		}
						   %>
						</table>
		 		</div>
		 	<%} %>
 </body>
</html>
<script>
function supportAndroidprint(){
	var printHtml=$('div[name="printWmsReceipt"]');
	for(var i=0;i<printHtml.length;i++){
		 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Receipts Label");
      	 visionariPrinter.SET_PRINTER_INDEXA ("LabelPrinter");//指定打印机打印  
      	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
         visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
		 visionariPrinter.ADD_PRINT_TABLE("0.4cm",7,"100%","100mm",$(printHtml[i]).html());
		 visionariPrinter.SET_PRINT_COPIES(1);
		 visionariPrinter.ADD_PRINT_HTM("14cm",160,"100%","100%",addStyleForText("CONTINUED..."));
		 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
		 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","Last");
		//   visionariPrinter.PREVIEW();
		return visionariPrinter.PRINT(); 
	}
}
</script>