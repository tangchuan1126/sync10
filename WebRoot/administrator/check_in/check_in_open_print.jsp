<%@page import="com.cwc.app.key.EquipmentTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.YesOrNotKey" %>

<%@page import="java.util.List" %>

<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 

<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder")%>';
</script>
<html>
<head>
<!--  基本样式和javascript -->
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
<% boolean isPrint = StringUtil.getInt(request, "isprint") == 1; %>
 <%if(!isPrint){  %>
<link type="text/css" href="comm.css" rel="stylesheet" />
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.autocomplete.css" />

<script src="js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="js/blockui/jquery.blockUI.233.js"></script>
 <%} %>
<%
    long entry_id = StringUtil.getLong(request,"main_id");
	//long psId = StringUtil.getLong(request, "ps_id");
	long psId=0;
	 String printName=StringUtil.getString(request,"print_name");
// 	if(psId <= 0l){
//    		 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
//     	 psId=adminLoggerBean.getPs_id(); 
// 	}
   	DBRow main_row=checkInMgrZwb.findGateCheckInById(entry_id);	//主表信息
    DBRow[] rows = checkInMgrZwb.getEquipmentByEntryId(entry_id);//根据entry 查询出一堆设备
    DBRow spotRow=checkInMgrZwb.getEquipmentUseSpot(entry_id);//停车位
    DBRow[] taskRows = checkInMgrZwb.getTaskUseDoor(entry_id);//任务
    if(main_row!=null){
    	psId=main_row.get("ps_id", 0l);
    }

    //判断是否迟到
	String gate_check_in_time = DateUtil.showLocalTime(main_row.getString("create_time"),psId);
	String appointment_time = "";
	if(taskRows !=null && taskRows.length>0){
		appointment_time = taskRows[0].getString("appointment_date");
	}
// 	System.out.print("appointment_time="+appointment_time);
// 	String appointment_time = main_row.getString("appointment_time");
	String isShow="";
	if(!"".equals(appointment_time)){
		long gate_check_in_min = DateUtil.getDate2LongTime(gate_check_in_time)/60000;//转化为分钟
		long appointment_min = DateUtil.getDate2LongTime(appointment_time)/60000;
		if((gate_check_in_min-appointment_min)>-30 || (gate_check_in_min-appointment_min)<30 ){
			isShow="late";
		}else{
			isShow="early";
		}
	}
// 	System.out.println("isShow="+isShow);
	Boolean isDoor = false;//判断是否选门
%>

<title>Check In</title>
</head>
<body>
	 
	<div id="av1">
		<%if(!isPrint){ %>
		<div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			<input type="button" class="short-short-button-print"  value="Print"  onclick="printGate()"/>
		</div>
		<%} %>
		<br/>
		<div align="center">
			<% 
				if(rows!=null && rows.length>0){ 
					int live_flag = rows[0].get("live_flag", 0);
					if(live_flag==YesOrNotKey.YES){
						DBRow row = rows[0];
			%>
			<!-- LIVE LOAD 的情况 (车尾都是liveload 出一张标签) -->
				<div id="avg" name="avg" style="width:368px; display:block; border:1px solid red">
				<div style="border-bottom:2px solid black;height:30px;padding-top:10px;font-size:18px;font-weight:bold;font-family:Arial;" align="center">Check Out with this Label</div>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" >
						<tr>
					        <td width="35%" rowspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;" >
					        	<span style="font-weight:bold; font-family:Arial;font-size:17px;"><%=row.getString("rel_type_value") %></span>
					        	<br/>
					        	<span style="font-family:Arial;"> Live Load</span>
					        	<br/>
					        	<span style="font-weight:bold;font-size:17px;font-family:Arial;">Entry ID</span>
					        </td>
					        <td width="60%" align="center" colspan="3" style="border-bottom:2px solid black;" >
					        <%if(!row.getString("check_in_time").equals("")){ %>
					        	<div><span style="font-weight: bold;font-family:Arial;font-size:13px;" id="dateShow">Gate Check In Time </span></div>
					        	<div><%=DateUtil.showLocalparseDateTo24Hours(row.getString("check_in_time"),psId)%></div>
					        <%} %>
					        </td>
				        </tr>
				        <tr>
					        <td height="58" colspan="3" align="center" style="border-bottom:2px solid black;" >
					            <span style="font-weight: bold;">
						            <div align="center"><img src="/barbecue/barcode?data=<%=entry_id%>&resolution=203&type=Code39" /></div>
						            <div align="center"><span style="font-size:12px;font-family:Arial;"><%=entry_id%></span></div>
					            </span>
					        </td>
				        </tr>
				        <% 
				        	for(DBRow r:rows){
				        		if(r.get("equipment_type", 0)==EquipmentTypeKey.Tractor){
				        %>
				        	<tr>
						        <td height="30" align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">License Plate</td>
						        <td colspan="3" align="center" style="border-bottom:2px solid black;font-family:Arial;">
						        <%       	 
							       if(!"".equals(r.getString("equipment_number",""))){
							  %>
							   			<div align="center" style="margin-top: 5px;">
							   				<img src="/barbecue/barcode?data=<%=r.getString("equipment_number","").toUpperCase() %>&width=1&height=20&type=Code39" />
							   			</div>
									    <div align="center"><span style="font-size:12px;font-family:Arial;"><%=r.getString("equipment_number","").toUpperCase() %></span></div>
							  <%}else{ %>
						             &nbsp;
						       <%} %>
						        </td>
						     </tr>
				        
				        <% 	}else{ %>
				        	<tr>
						        <td  align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">Container No</td>
						        <td colspan="3" align="center" style="border-bottom:2px solid black;">
						       	     <%if(!r.getString("equipment_number","").equals("")){ %>
							       	 <div align="center" style="margin-top: 5px;"><img src="/barbecue/barcode?data=<%=r.getString("equipment_number","").toUpperCase() %>&width=1&height=20&type=Code39" /></div>
									 <div align="center"><span style="font-size:12px;font-family:Arial;"><%=r.getString("equipment_number","").toUpperCase() %></span></div>
						             <%}else{ %>
						             &nbsp;
						             <%} %>
						        </td>
						      </tr>
				        <%
				        		}
				        	} 
				        %>
					      
				      <%if(row.get("rel_type",0l)!=CheckInMainDocumentsRelTypeKey.DELIVERY && row.get("rel_type",0l)!=CheckInMainDocumentsRelTypeKey.NONE){ %>
					      <%DBRow[] loadingRows=checkInMgrZwb.findloadingByInfoId(entry_id);
					   		String loadNumbers="";		      
					      	for(int a=0;a<loadingRows.length;a++){
					      		if(loadingRows[a].get("number_type",0)>0 && loadingRows[a].get("number_type",0)!=ModuleKey.CHECK_IN_CTN && loadingRows[a].get("number_type",0)!=ModuleKey.CHECK_IN_BOL  && loadingRows[a].get("number_type",0)!=ModuleKey.CHECK_IN_DELIVERY_ORTHERS){			      			
					      			String jisuan=loadNumbers+loadingRows[a].get("number","")+",";
					      			if(jisuan.length()>23){
					      				loadNumbers+="....";
					      				break;
					      			}else{
					      				loadNumbers+=loadingRows[a].get("number","")+",";
					      			}
					      			 
					      		}
					      	}
					      %>	      
						      <tr>
						        <td height="30"  align="center" style="border-bottom:2px solid black; border-right:2px solid black;font-family:Arial;font-size:14px;">
									Load
						        </td>
							    <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px; word-break:break-all" align="center">
							    	<%if(!loadNumbers.equals("")){	 %>
							    		<%String numberShow=loadNumbers.substring(0, loadNumbers.length()-1); %>
							    			<%=numberShow%>
							    	<%} %>
							    	&nbsp;
							    </td>
						      </tr>
					       	  <tr>
						        <td height="30"  align="center" style="border-bottom:2px solid black; border-right:2px solid black;font-family:Arial;font-size:14px;">
									Load Count
						        </td>
							    <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px; word-break:break-all" align="center">
							    	<%=main_row.get("load_count",0L) %>&nbsp;
							    </td>
						      </tr>
					<% }%>
						   
					      <tr>
					        <td height="30"  align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">
								MC / DOT
					        </td>
					        <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px;" align="center"><%=main_row.get("mc_dot","") %>&nbsp;</td>
					      </tr>
					       <tr>
<!-- 					       Company Name 改为 Carrier -->
					        <td height="30" align="center"style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;" >
								Carrier
					        </td>
					        <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px;" align="center">
					        	<%if(!main_row.get("company_name","").equals("")){ %>
					        		<%=main_row.get("company_name","") %>
					        	<%} %> &nbsp;
					        </td>
					      </tr>
					      <%if(spotRow!=null){ %>		   
					    	<tr>
						        <td height="30"  align="center" style="font-family:Arial;font-size:14px; font-weight: bold; border-bottom:2px solid black; border-right:2px solid black;">
						        	Spot
						        </td>
							    <td height="30" colspan="3" align="center" style="font-size: 13px; font-family: Arial;font-weight: bold; border-bottom:2px solid black;">						    					       
							       	<%=spotRow.getString("yc_no")%>
							    </td>
					     	</tr>
						 <%}%>
						 	
						 <%
							DBRow[] pickUpEquipment = checkInMgrZwb.findCTNRResourceByEntryId(entry_id);
					      	if(pickUpEquipment!=null&&pickUpEquipment.length>0){
					     %>
					     <tr>
						        <td height="30"  align="center" style="font-family:Arial;font-size:14px; font-weight: bold; border-bottom:2px solid black; border-right:2px solid black;">
						        	Pick Up
						        </td>
						        <td height="30" colspan="3" align="center" style="font-size: 12px; font-family: Arial;font-weight: bold; border-bottom:2px solid black;">
					     <%	for(DBRow equipment : pickUpEquipment){ %>
						      <div>
						      		&nbsp;<%=equipment.getString("equipment_number") %> &nbsp;To&nbsp;
						      		<%if("".equals(equipment.getString("yc_no"))){ %>
						      			Door【<%=equipment.getString("doorId")%>】
						      		<% }else{%>
								    	Spot【<%=equipment.getString("yc_no") %>】
						      		<% }%>
						      </div>
						 <%} %>
						 		</td>
					     	</tr>
						 <%	} %>	   
					     	
				      	 <%if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.DELIVERY){ %>
						      <tr>
						        <td height="30"  align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">
									Title
						        </td>
						        <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px;" align="center"><%=row.get("title","") %>&nbsp;</td>
						      </tr>
				      	 <%}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.PICK_UP){ %>
					      	 <tr>
						        <td height="30"  align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">
									CustomerID
						        </td>
						        <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px;" align="center"><%=row.get("customer_id","") %>&nbsp;</td>
						      </tr>
					     <%} %>  
					      	
					      <%if(taskRows!=null&&taskRows.length>0){ %>
					       <tr>
					       		<td height="30"  align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">
									Task
						        </td>
						        <td colspan="3" height="30" style="word-break:break-all;border-bottom:2px solid black;font-family:Arial;font-size:11px;text-align: center;" >
							      <% for(DBRow task:taskRows){%>
							      	Door【<%=task.get("doorId", "") %>】&nbsp;<%=task.get("number","") %>&nbsp;&nbsp;&nbsp;
							      	<%isDoor=true; %>
								   <%} %>
						   		</td>
						   </tr>
						 <%} %>	
					      			      
					      <tr>
					       <td height="83" colspan="4" align="center" valign="bottom">&nbsp;
<%-- 						           	<%if(isShow.equals("late")&&!isDoor){ %> --%>
<!-- 						        	   <div style="height:40px;line-height:40px;font-size:13px;font-weight:bold;font-family:Arial;" align="center"> -->
<%-- 											<%if(row.get("rl_id",0l)==0){%>		        	 --%>
<!-- 												Must Window CheckIn       -->
<%-- 							        		<%}else{%>   --%>
<!-- 							        			Must Dock Checkin  -->
<%-- 							        		<%} %> --%>
<!-- 										</div> -->

									<!-- 不是门的显示Must Window CheckIn -->
									<%if(!isDoor){%>
						            	<div style="height:40px;line-height:40px;font-size:13px;font-weight:bold;font-family:Arial;" align="center">
											Must Window CheckIn
										</div>
						            <%}%>
					           
					            	
					            
					        </td>
					      </tr>
					      
					</table>
				</div>
			<%
					}else{
			        	for(int i=0;i<rows.length;i++){
			        		DBRow row = rows[i];
			%>
			<br/>
			<br/>
			<!-- 非LIVE LOAD 的情况 (设备各出个的标签,即有几条设备信息就显示几条 ) -->
				<div id="avg" name="avg" style="width:368px; display:block; border:1px solid red">
				<%if(row.get("equipment_purpose",0)==CheckInLiveLoadOrDropOffKey.DROP && row.get("equipment_type", 0)==EquipmentTypeKey.Container){ %>
					<div style="border-bottom:2px solid black;height:30px;padding-top:10px;font-size:18px;font-weight:bold;font-family:Arial;" align="center">Trailer/CTNR Release Code</div>
				<%}else{ %>
					<div style="border-bottom:2px solid black;height:30px;padding-top:10px;font-size:18px;font-weight:bold;font-family:Arial;" align="center">Check Out with this Label</div>
				<%} %>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" >
						<tr>
					        <td width="35%" rowspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;" >
					        	<span style="font-weight:bold; font-family:Arial;font-size:17px;"><%=row.getString("rel_type_value") %></span>
					        	<br/>
					        	<span style="font-family:Arial;"> <%=row.getString("equipment_purpose_value") %></span>
					        	<br/>
					        	<span style="font-weight:bold;font-size:17px;font-family:Arial;">Entry ID</span>
					        </td>
					        <td width="60%" align="center" colspan="3" style="border-bottom:2px solid black;" >
					        <%if(!row.getString("check_in_time").equals("")){ %>
					        	<div><span style="font-weight: bold;font-family:Arial;font-size:13px;" id="dateShow">Gate Check In Time </span></div>
					        	<div><%=DateUtil.showLocalparseDateTo24Hours(row.getString("check_in_time"),psId)%></div>
					        <%} %>
					        </td>
				        </tr>
				        <tr>
					        <td height="58" colspan="3" align="center" style="border-bottom:2px solid black;" >
					            <span style="font-weight: bold;">
						            <div align="center"><img src="/barbecue/barcode?data=<%=entry_id%>&resolution=203&type=Code39" /></div>
						            <div align="center"><span style="font-size:12px;font-family:Arial;"><%=entry_id%></span></div>
					            </span>
					        </td>
				        </tr>
				        <% 
				        	if(row.get("equipment_type", 0)==EquipmentTypeKey.Tractor){
				        %>
				        	<tr>
						        <td height="30" align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">License Plate</td>
						        <td colspan="3" align="center" style="border-bottom:2px solid black;font-family:Arial;">
						        <%if(!row.getString("equipment_number","").equals("")){ %>
							       	 <div align="center" style="margin-top: 5px;"><img src="/barbecue/barcode?data=<%=row.getString("equipment_number","").toUpperCase() %>&width=1&height=20&type=Code39" /></div>
									 <div align="center"><span style="font-size:12px;font-family:Arial;"><%=row.getString("equipment_number","").toUpperCase() %></span></div>
					             <%}else{ %>
					             	&nbsp;
					             <%} %> 
						        </td>
						     </tr>
				        
				        <% 	}else{ %>
				        	<tr>
						        <td  align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">Container No</td>
						        <td colspan="3" align="center" style="border-bottom:2px solid black;">
						       	     <%if(!row.getString("equipment_number","").equals("")){ %>
							       	 <div align="center" style="margin-top: 5px;"><img src="/barbecue/barcode?data=<%=row.getString("equipment_number","").toUpperCase() %>&width=1&height=20&type=Code39" /></div>
									 <div align="center"><span style="font-size:12px;font-family:Arial;"><%=row.getString("equipment_number","").toUpperCase() %></span></div>
						             <%}else{ %>
						             &nbsp;
						             <%} %>
						        </td>
						      </tr>
				        <%
				        	} 
				        %>
					      
				      <%if(row.get("rel_type",0l)!=CheckInMainDocumentsRelTypeKey.DELIVERY && row.get("rel_type",0l)!=CheckInMainDocumentsRelTypeKey.NONE){ %>
					      <%DBRow[] loadingRows=checkInMgrZwb.findloadingByInfoId(entry_id);
					   		String loadNumbers="";		      
					      	for(int a=0;a<loadingRows.length;a++){
					      		if(loadingRows[a].get("number_type",0)>0 && loadingRows[a].get("number_type",0)!=ModuleKey.CHECK_IN_CTN && loadingRows[a].get("number_type",0)!=ModuleKey.CHECK_IN_BOL  && loadingRows[a].get("number_type",0)!=ModuleKey.CHECK_IN_DELIVERY_ORTHERS){			      			
					      			String jisuan=loadNumbers+loadingRows[a].get("number","")+",";
					      			if(jisuan.length()>23){
					      				loadNumbers+="....";
					      			}else{
					      				loadNumbers+=loadingRows[a].get("number","")+",";
					      			}
					      			 
					      		}
					      	}
					      %>	      
						      <tr>
						        <td height="30"  align="center" style="border-bottom:2px solid black; border-right:2px solid black;font-family:Arial;font-size:14px;">
									Load
						        </td>
							    <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px; word-break:break-all" align="center">
							    	<%if(!loadNumbers.equals("")){	 %>
							    		<%String numberShow=loadNumbers.substring(0, loadNumbers.length()-1); %>
							    			<%=numberShow%>
							    	<%} %>
							    	&nbsp;
							    </td>
						      </tr>
					       	  <tr>
						        <td height="30"  align="center" style="border-bottom:2px solid black; border-right:2px solid black;font-family:Arial;font-size:14px;">
									Load Count
						        </td>
							    <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px; word-break:break-all" align="center">
							    	<%=main_row.get("load_count",0L) %>&nbsp;
							    </td>
						      </tr>
					<% }%>
						   
					      <tr>
					        <td height="30"  align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">
								MC / DOT
					        </td>
					        <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px;" align="center"><%=main_row.get("mc_dot","") %>&nbsp;</td>
					      </tr>
					       <tr>
					        <td height="30" align="center"style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;" >
								Carrier
					        </td>
					        <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px;" align="center">
					        	<%if(!main_row.get("company_name","").equals("")){ %>
					        		<%=main_row.get("company_name","") %>
					        	<%} %> &nbsp;
					        </td>
					      </tr>
					      
					      <%if(spotRow!=null){ %>		   
					    	<tr>
						        <td height="30"  align="center" style="font-family:Arial;font-size:14px; font-weight: bold; border-bottom:2px solid black; border-right:2px solid black;">
						        	Spot
						        </td>
							    <td height="30" colspan="3" align="center" style="font-size: 13px; font-family: Arial;font-weight: bold; border-bottom:2px solid black;">						    					       
							       	<%=spotRow.getString("yc_no")%>
							    </td>
					     	</tr>
						 <%}%>
					     	
				      	 <%if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.DELIVERY){ %>
						      <tr>
						        <td height="30"  align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">
									Title
						        </td>
						        <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px;" align="center"><%=row.get("title","") %>&nbsp;</td>
						      </tr>
				      	 <%}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.PICK_UP){ %>
					      	 <tr>
						        <td height="30"  align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">
									CustomerID
						        </td>
						        <td colspan="3" style="border-bottom:2px solid black;font-family:Arial;font-size:13px;" align="center"><%=row.get("customer_id","") %>&nbsp;</td>
						      </tr>
					     <%} %>  
					      	
					      <%if(taskRows!=null&&taskRows.length>0){ %>
					       <tr>
					       		<td height="30"  align="center" style="border-bottom:2px solid black;border-right:2px solid black;font-family:Arial;font-size:14px;">
									Task
						        </td>
						        <td colspan="3" height="30" style="word-break:break-all;border-bottom:2px solid black;font-family:Arial;font-size:11px;text-align: center;" >
							      <% for(DBRow task:taskRows){%>
							      	Door【<%=task.get("doorId", "") %>】 &nbsp;<%=task.get("number","") %>&nbsp;&nbsp;&nbsp;
								   <%isDoor=true; %>
								   <%} %>
						   		</td>
						   </tr>
						 <%} %>	
					      
					      <%if(row.get("equipment_purpose",0)==CheckInLiveLoadOrDropOffKey.DROP && row.get("equipment_type", 0)==EquipmentTypeKey.Container){ %>
							<tr>
					       		<td height="35" colspan="2" align="center" >&nbsp;</td>
						        <td colspan="2" >&nbsp;</td>
						      </tr>
						      <tr>
						        <td colspan="2">&nbsp;</td>
						        <td width="5%">&nbsp;</td>
						        <td>&nbsp;</td>
						      </tr>
						<%}else{ %>		
					      <tr>
					       <td height="83" colspan="4" align="center" valign="bottom">&nbsp;
					       
<%-- 					           <%if(isShow.equals("late")){ %> --%>
<!-- 					            	<div style="height:40px;line-height:40px;font-size:13px;font-weight:bold;font-family:Arial;" align="center"> -->
<%-- 										<%if(row.get("rl_id",0l)==0&&!isDoor){%>		        	 --%>
<!-- 											Must Window CheckIn       -->
<%-- 						        		<%}else{%>   --%>
<!-- 						        			Must Dock Checkin  -->
<%-- 						        		<%} %> --%>
<!-- 									</div> -->
<%-- 					            <%}else if(!isDoor){%> --%>
<!-- 					            	<div style="height:40px;line-height:40px;font-size:13px;font-weight:bold;font-family:Arial;" align="center"> -->
<!-- 										Must Window CheckIn -->
<!-- 									</div> -->
<%-- 					            <%}%> --%>

								<!-- 不是门的显示Must Window CheckIn -->
									<%if(!isDoor){%>
						            	<div style="height:40px;line-height:40px;font-size:13px;font-weight:bold;font-family:Arial;" align="center"> -->
											Must Window CheckIn
										</div>
						            <%}%>
					        </td>
					      </tr>
					  <%}%>  
					</table>
					<%if(row.get("equipment_purpose",0)==CheckInLiveLoadOrDropOffKey.DROP && row.get("equipment_type", 0)==EquipmentTypeKey.Container){ %>
						<div style="height:40px;line-height:40px;font-size:13px;font-weight:bold; padding-bottom: 18px; font-family:Arial;" valign="top" align="center">
							Passcode is Required for Picking Up CTNR／Trailer
				    	</div>
					<%} %>
					
				</div>
			  
			<% 
					}
				}
			}
			%>
			
			
			
		</div>
	</div>	
 
<script type="text/javascript">
	 
 
 (function(){
		$.blockUI.defaults = {
		 css: { 
		  padding:        '8px',
		  margin:         0,
		  width:          '170px', 
		  top:            '45%', 
		  left:           '40%', 
		  textAlign:      'center', 
		  color:          '#000', 
		  border:         '3px solid #999999',
		  backgroundColor:'#ffffff',
		  '-webkit-border-radius': '10px',
		  '-moz-border-radius':    '10px',
		  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		 },
		 //设置遮罩层的样式
		 overlayCSS:  { 
		  backgroundColor:'#000', 
		  opacity:        '0.6' 
		 },
		 
		 baseZ: 99999, 
		 centerX: true,
		 centerY: true, 
		 fadeOut:  1000,
		 showOverlay: true
		};
	})();
</script>
</body>
</html>
<script>

     function printGate(){
    	   //获取打印机名字列表
    		var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
    	   //判断是否有该名字的打印机
    		var printer = "LabelPrinter";
    		var printerExist = "false";
    		var containPrinter = printer;
    		for(var i = 0;i<printer_count;i++){
    			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
    				printerExist = "true";
    				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
    				break;
    			}
    		}
    		if(printerExist=="true"){
   			     var printHtml=$('div[name="avg"]');
   			     for(var i=0;i<printHtml.length;i++){
		    	    	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Label");
    	              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
    	              	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
    	  				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","99%",$(printHtml[i]).html());
    	  			     visionariPrinter.SET_PRINT_COPIES(1);
    	  				 //visionariPrinter.PREVIEW();
    	  			     visionariPrinter.PRINT();        		
   			     }
   			     $.artDialog && $.artDialog.close();
    		}else{
    			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
    			if(op!=-1){ //判断是否点了取消
    				 var printHtml=$('div[name="avg"]');
    			     for(var i=0;i<printHtml.length;i++){
			    	    	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Label");
	    	              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
	    	              	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
	    	  				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","99%",$(printHtml[i]).html());
	    	  			     visionariPrinter.SET_PRINT_COPIES(1);
	    	  				 //visionariPrinter.PREVIEW();
	    	  			     visionariPrinter.PRINT();        		
    			     }
    			     $.artDialog && $.artDialog.close();
    			}	
    		}
     }
     
     
     function supportAndroidprint(){
    		//获取打印机名字列表
    	   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
    	   	 //判断是否有该名字的打印机
    	   	var printer = "<%=printName%>";
    	   	var printerExist = "false";
    	   	var containPrinter = printer;
    		for(var i = 0;i<printer_count;i++){
    			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
    				printerExist = "true";
    				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
    				break;
    			}
    		}
    		if(printerExist=="true"){
    			return androidIsPrint(containPrinter);
    		}else{
    			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
    			if(op!=-1){ //判断是否点了取消
    				return androidIsPrint(containPrinter);
    			}
    		}
    	}

    	function androidIsPrint(containPrinter){
    		var flag = true 
    		var printHtml=$('div[name="avg"]');
			     for(var i=0;i<printHtml.length;i++){
	    	    	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Label");
	              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
	              	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
	  				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","99%",$(printHtml[i]).html());
	  			     visionariPrinter.SET_PRINT_COPIES(1);
	  				 //visionariPrinter.PREVIEW();
	  			     flag = flag && visionariPrinter.PRINT();
			     }
    		return flag ;
    	}
</script>
