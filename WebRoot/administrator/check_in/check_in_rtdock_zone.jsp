<%@page import="com.cwc.app.key.OccupyTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<%@page import="com.cwc.app.key.SpaceRelationTypeKey"%>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.OccupyStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey"%>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="occupyStatusTypeKey" class="com.cwc.app.key.OccupyStatusTypeKey"/>
<jsp:useBean id="checkInMainDocumentsRelTypeKey" class="com.cwc.app.key.CheckInMainDocumentsRelTypeKey"/>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/>
<jsp:useBean id="occupyTypeKey" class="com.cwc.app.key.OccupyTypeKey"/> 

<html>
<head>


<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		long psId=adminLoggerBean.getPs_id();  
		long adminId=adminLoggerBean.getAdid(); 
		PageCtrl pc = new PageCtrl();
		int p = StringUtil.getInt(request,"p");
		pc.setPageNo(p);
		pc.setPageSize(10);
		long ps_id = StringUtil.getLong(request,"ps_id");
		long zone_area = StringUtil.getLong(request,"zone_area");
		int doorStatus = StringUtil.getInt(request,"doorStatus");
		String start_time = StringUtil.getString(request,"start_time");
		String end_time = StringUtil.getString(request,"end_time");
		long dlo_id = StringUtil.getLong(request,"dlo_id");
		String cmd = StringUtil.getString(request,"cmd");
		long doorId = StringUtil.getLong(request,"doorId");
		long searchDoorId = StringUtil.getLong(request,"searchDoorId");
		long searchdlo_id = StringUtil.getLong(request,"searchdlo_id");
		int flag=StringUtil.getInt(request,"flag");
		String license_plate = StringUtil.getString(request,"license_plate");
		String trailerNo = StringUtil.getString(request,"trailerNo");
		long mainId = StringUtil.getLong(request,"mainId");
		if(ps_id==0){
	ps_id=psId;
		}

		DBRow[] rows = checkInMgrXj.rtOccupiedDoor(ps_id,zone_area,doorStatus,pc);
		DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
		DBRow[] allZone =checkInMgrZwb.findAllZone(ps_id);
%>
<title>Check in</title>
<script type="text/javascript">
$(function(){
	$("tr[name='dock_tr'] td[id='text']").click(function(){
		var doorId=$(this).attr("door_id");
		var dlo_id=$(this).attr("dlo_id");
		var doorName=$(this).attr("door_name");
		var area_id=$(this).attr("area_id");
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_update_dock.html?doorId='+doorId+'&dlo_id='+dlo_id+'&doorName='+doorName+'&p='+<%=p%>+'&area_id='+area_id; 
		$.artDialog.open(uri , {title: "Dock",width:'1200px',height:'500px', lock: true,opacity: 0.3,fixed: true});
	});
	onLoadInitZebraTable();
});

function go(num){
	$("#p").val(num);
	showCheckInDockZone('<%= zone_area%>',num);}

</script>
</head>
<body>
			<table>
				<tr>
				   	<td align="right"  style='word-break:break-all' >
						<input type="button" class="long-long-150-button" value="View Inconsistencies" onClick="window.location.href='patrol_check_data_details.html?backurl=<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_diff_spot.html'"/>&nbsp;
						<input type="button" class="long-button" value="Again Patrol" onclick="againPatrol(<%=psId%>,<%=zone_area%>)"/>&nbsp;
					</td>
				</tr>
			</table>
      		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		    	<tr> 
	        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Door</th>
	        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Door Zone</th>
	        <th width="25%" class="left-title" style="vertical-align:center;text-align:center;">Tractor</th>
	        <th width="25%" class="left-title" style="vertical-align:center;text-align:center;">Trailer</th>
		</tr>
		<%if(rows != null && rows.length > 0){
      		for(DBRow row : rows){
      		   DBRow[]  equipments= checkInMgrXj.getEquipmentByDoorId(row.get("resource_id",0l));
      	%>
      	<tr height="60px"   >
      	    <td align="center" valign="middle" style='word-break:break-all'>
      	           <%if(equipments.length>0){ 
      	           %>
      	           <font color="red" ><b><%=row.get("resource_name","") %></b></font>
      	           <%
      	           }else{
      	           %>
      	            <font color="green"><b><%=row.get("resource_name","") %></b></font>
      	           <%
      	           }
      	           %>
      	           
      	    		 
      	    </td>
      	    <td align="center" valign="middle" style='word-break:break-all'>
      	    		 <%=row.get("area_name","") %>&nbsp;
      	    </td>
      	    <td align="center" valign="middle" style='word-break:break-all'>	 &nbsp;
    		 <%
    		 
    		 if(equipments.length>0){
    			 
    		 
    		    for(DBRow equipment:equipments){
    		    	if(equipment.get("equipment_type", 0)==CheckInTractorOrTrailerTypeKey.TRACTOR){
    		 %>
    		 <div style="text-align:left;clear:both;"> 						
				<div style="width:200px;float:left" align="right"><font color='#f60'>E<%=equipment.get("check_in_entry_id", 0l) %></font>&nbsp;&nbsp;&nbsp;&nbsp;<%=equipment.get("equipment_number", "") %></div>
				<div>
				    &nbsp;&nbsp;<img id="<%=equipment.get("equipment_id", 0) %>" width="16" height="16" align="absmiddle" src="../imgs/standard_msg_error.gif"  onclick="clearEequipment(<%=equipment.get("equipment_id", 0) %>);">
				</div>
  			 </div>
    		  
    		 <%
    		    	}
    		    }
    		 }
    		 %>
    	 </td>
    	 <td align="center" valign="middle" style='word-break:break-all'>	 &nbsp;
    		 <%
    		 
    		 if(equipments.length>0){
    			 
    		 
    		    for(DBRow equipment:equipments){
    		    	if(equipment.get("equipment_type", 0)==CheckInTractorOrTrailerTypeKey.TRAILER){
    		 %>
    		<div style="text-align:left;clear:both;"> 						
				<div style="width:200px;float:left" align="right"><font color='#f60'>E<%=equipment.get("check_in_entry_id", 0l) %></font>&nbsp;&nbsp;&nbsp;&nbsp;<%=equipment.get("equipment_number", "") %></div>
				<div>
				  &nbsp;&nbsp; <img id="<%=equipment.get("equipment_id", 0) %>" width="16" height="16" align="absmiddle" src="../imgs/standard_msg_error.gif"   onclick="clearEequipment(<%=equipment.get("equipment_id", 0) %>);">
				</div>
  			 </div>
    		 <%
    		    	}
    		    }
    		 }
    		 %>
    		
    	 </td>
    
      	</tr>
		<tr class="split">
  			<td colspan="4" style="height:24px;line-height:24px;background-image:url('../imgs/linebg_24.jpg');text-align:right;padding-right:20px;">
  					 <input type="button" class="short-button" value="Add" onclick="addEequipment(<%=CheckInTractorOrTrailerTypeKey.TRAILER %>,<%=row.get("resource_id",0l) %>,'<%=row.get("resource_name","") %>');"/>
					 <input type="button" class="short-button" value="Confirm" onclick="confirm(<%=!row.getString("dlo_id").equals("")?row.getString("dlo_id"):0%>,<%=row.get("resource_id",0l) %>,<%=row.getString("area_id") %>)"/>
  			</td>
  	    </tr>	
      	<% 		
      		}
      		
      	}else{
      	%>
      		<tr style="background:#E6F3C5;">
   						<td colspan="10" style="text-align:center;height:120px;">No Data</td>
   		    </tr>
      	<% }%>

	<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	   <form name="mod_form" id="mod_form" >	
	    <input type="hidden" name="searchdlo_id"  />
		<input type="hidden" name="dlo_id"  />
		<input type="hidden" name="doorId"  />
		<input type="hidden" name="searchDoorId"  />
		<input type="hidden" name="flag"  />
		<input type="hidden" name="p"  />
		<input type="hidden" name="cmd" />
	  </form>	
	 
	  <tr>
	    <td height="28" align="right" valign="middle" class="turn-page-table">
	        <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
	      Goto 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
	    </td>
	  </tr>
   </table>
   <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	   <form name="dataForm" action="check_in_rtdock.html">
	    <input type="hidden" name="p" id="p"/>
	    <input type="hidden" name="zone_area" value="<%=zone_area%>"/>
		<input type="hidden" name="doorStatus" value="<%=doorStatus%>"/>
		<input type="hidden" name="ps_id" value="<%=ps_id%>"/>
	   </form>
    </table>
</body>
</html>
<script>
function createEntryId(doorId,doorName){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_createEntryId.html?doorId='+doorId+'&doorName='+doorName; 
	$.artDialog.open(uri , {title: "Create EntryID",width:'500px',height:'200px', lock: true,opacity: 0.3,fixed: true});
}
function confirm(dlo_id,doorId,area_id){
	
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxConfirmPatrolDockAction.action',
  		dataType:'json',
 // 		async:false,
  		data:"dlo_id="+dlo_id+"&sd_id="+doorId+"&area_id="+area_id,
  		success:function(data){
					location.reload();
					$.artDialog.close();
  			    	
  		}
    });
}

function againPatrol(ps_id,zone_area){
	document.again_patrol.zone_area.value=zone_area;
	document.again_patrol.ps_id.value=ps_id;
	document.again_patrol.cmd.value="againPatrol";
	document.again_patrol.submit();
}
function clearEequipment(equipment_id){
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxClearEquipmentAtResourcesAction.action',
  		dataType:'json',
  //		async:false,
  		data:"relation_id="+equipment_id+"&relation_type="+<%=SpaceRelationTypeKey.Equipment %>+"&adminId="+<%=adminId%>,
  		success:function(data){
					location.reload();
					$.artDialog.close();
  			    	
  		}
    });
}

function addEequipment(equipment_type,resource_id,door_name){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_update_spot.html?resource_id='+resource_id+'&resource_name='+door_name+'&p='+<%=p%>+'&equipment_type='+equipment_type+'&resource_type='+<%=OccupyTypeKey.DOOR %>;
	$.artDialog.open(uri , {title: "<%=occupyTypeKey.getOccupyTypeKeyName(OccupyTypeKey.DOOR) %>",width:'1100px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}

</script>