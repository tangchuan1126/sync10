<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 


<html>
<head>

<title>Shipping Lable</title>
<%
	boolean isPrint = StringUtil.getInt(request, "isprint") == 1;
%>
 <base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
 <%if(!isPrint){ %>
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
<%} %>
<script>
	function print_shipping_lable(){
		var printHtml=$('div[name="shipping_lable"]');
		for(var i=0;i<printHtml.length;i++){
			 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Load Label");
	      	 visionariPrinter.SET_PRINTER_INDEXA ("LabelPrinter");//指定打印机打印  
	      	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
	         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","50mm",$(printHtml[i]).html());
	         visionariPrinter.PREVIEW();
	         //visionariPrinter.PRINT();
		}
	}
</script>
</head>
<body>
<div style="width:210mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;">
	<input class="short-short-button-print" type="button" onclick="print_shipping_lable()" value="Print">
</div>
<br/>	
<%
	long orderNo=StringUtil.getLong(request, "orderNo");
    String customerId=StringUtil.getString(request, "CustomerID");
    String companyId=StringUtil.getString(request, "CompanyID");
    String printName=StringUtil.getString(request,"print_name");
    
  //  long orderNo=155747;
  //  String customerId="VIZIO";
  //  String companyId="W12";

AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adid = adminLoggerBean.getAdid();
	DBRow[] rows = sqlServerMgr.findOrderShipPalletByOrderNo(orderNo, customerId, companyId, adid, request);
	for(int i = 0; i < rows.length; i ++)
	{
		DBRow row = rows[i];
%>
<div id="shipping_lable" name="shipping_lable" style="border:1px solid red; width:363px;margin:0 auto;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<thead>
	  <tr>
	  	<td style="border-bottom:2px solid black;" height="30px;">
	    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	 <tr>
	            <td  width="200px;" align="left">
	                <span style="font-family:Arial;margin-left:10px; font-size:15px;"><b>ORDER NO.:<%=row.get("OrderNo", 0L) %></b></span>
	            </td>
	            <td  align="right" style="padding-right:10px;">
	                <span style="font-family:Arial; font-size:14px;"><b>P/N:</b></span>&nbsp;&nbsp;
	                <span style="font-family:Arial; font-size:14px;" tdata='pageNO'><b>#</b></span>&nbsp;
	                <span style="font-family:Arial; font-size:14px;"><b>OF</b></span>&nbsp;&nbsp;
	                <span style="font-family:Arial; font-size:14px;" tdata='PageCount'><b>#</b></span>
	            </td>
	          </tr>
	        </table>
	    </td>
	  </tr>	
	  <tr>
	  	<td style="border-bottom:2px solid black;" height="105x;">
	    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	 <tr>
	            <td  width="60" align="right" valign="top">
	                <span style="font-family:Arial; font-size:14px;"><b>FROM:</b></span>
	            </td>
	            <td width="292"  align="left">
	                &nbsp;            
	            </td>
	          </tr>
	          <tr>
	            <td align="left" valign="top">
	                &nbsp;
	            </td>
	            <td width="292"  align="left">
	                <div style="font-family:Arial; font-size:13px;"><b>
	                	<%=row.getString("ShipFromName") %>
	                </b></div>
	                <div style="font-family:Arial; font-size:13px;"><b>
	                	<%=row.getString("ShipFromAddress1") %>
	                </b></div>
	                <div style="font-family:Arial; font-size:13px;"><b>
	                	<%=row.getString("City") %>
	                </b></div>
	                <div style="font-family:Arial; font-size:13px;"><b>
	                	<%=row.getString("ShipFromAddress2") %>
	                </b></div>
	            </td>
	          </tr>
	         </table>
	    </td>
	  </tr>
	  <tr>
	  	<td style="border-bottom:2px solid black;" height="100x;">
	    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	 <tr>
	            <td  width="60" align="right" valign="top">
	                <span style="font-family:Arial; font-size:14px;"><b>TO:</b></span>
	            </td>
	            <td width="292"  align="left">
				&nbsp;
	            </td>
	          </tr>
	           <tr>
	            <td align="left" valign="top">
	                &nbsp;
	            </td>
	            <td width="292"  align="left">
	                <div style="font-family:Arial; font-size:13px;"><b>
	                	<%=row.getString("ShipToName") %>
	                </b></div>
	                <div style="font-family:Arial; font-size:13px;"><b>
	                	<%=row.getString("ShipToAddress1") %>
	                </b></div>
	                <div style="font-family:Arial; font-size:13px;"><b>
	                	<%=row.getString("ShipToAddress2") %>
	                </b></div>
	            </td>
	          </tr>
	        </table>
	    </td>
	  </tr>
	  <tr>
	  	<td valign="top" height="80px;">
	    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	        	<tr>
	            	<td width="180px" style="padding-top: 10px;">
	                	<div style="font-family:Arial; font-size:13px; margin-left:25px;white-space:nowrap;overflow:hidden;border:0px solid red;width:150px;"><b>
	                	SCAC&nbsp;:&nbsp;<%=row.getString("SCACCode") %>
	               		 </b></div>
	                     <div style="font-family:Arial; font-size:13px;margin-left:10px;white-space:nowrap;overflow:hidden;border:0px solid red;width:168px;"><b>
	                	B/L NO.:&nbsp;<%=row.get("OrderNo",0L) %>
	               		 </b></div>
	                     <div style="font-family:Arial; font-size:13px;margin-left:10px;border:0px solid red;width:167px;white-space:nowrap;overflow:hidden;"><b>
	                	P/L NO.:&nbsp;<%=row.getString("ReferenceNo") %>
	               		 </b></div>
	                     
	                </td>
	                <td valign="top" style="padding-top: 10px;">
	                	<div style="font-family:Arial; font-size:13px;"><b>
	                	&nbsp;
	               		 </b></div>
	                     <div style="font-family:Arial; font-size:13px;white-space:nowrap;overflow:hidden;border:0px solid red;width:177px;"><b>
	                	PRO NO.:&nbsp;<%=row.getString("ProNo") %>
	               		 </b></div>
	                     <div style="font-family:Arial; font-size:13px;padding-left:9px;white-space:nowrap;overflow:hidden;border:0px solid red;width:177px;"><b>
	                	 PO NO.:&nbsp;<%=row.getString("PONo") %>
	               		 </b></div>
	                </td>
	            </tr>
	            <tr>
	            	<td colspan="2"> 
	            		<div style="font-family:Arial; font-size:13px; margin-left:25px;border:0px solid red; width:330px;word-wrap:break-word; height:50px;overflow:hidden;"><b>
	                	NOTE&nbsp;:&nbsp;<%=row.getString("BOLNote") %>
	               		 </b></div>
	            	</td>
	            </tr>
	        </table>
	    </td>
	  </tr>
	  <tr>
	  	<td style="border-bottom:2px solid black;" height="40px;">
	     <div style="font-family:Arial; font-size:13px;margin-left:25px;"><b>
	    	TOTAL ORDER CARTON(S):<%=row.get("OrderedQty",0) %> CTNS
	     </b></div>
	    </td>
	  </tr>
	  </thead>
	  <%int Pallets = row.get("Pallets", 0); %>
	  <%for(int j = 0; j < Pallets; j ++){%>
		<%String code = "87"+ MoneyUtil.fillZeroByRequire(row.get("OrderNo", 0L), 7)+ MoneyUtil.fillZeroByRequire(j+1, 3); %>
	  <tr>
	  	<td style="padding-top: 20px;padding-left: 20px;">
	  		<div >
	  		  <img src="/barbecue/barcode?data=<%=code %>&height=100&width=1&type=code128" />
	  		</div>
	  		<div style="font-size:12px; font-family:Arial;padding-left:25px;"><b><%=code %></b></div>
	  		<span style="font-family:Arial; font-size:25px;"><b>
             	 PALLET:
            </b></span>
            <span style="font-family:Arial; font-size:25px;"><b>
             	 <%=j+1 %>
            </b></span>
            <span style="font-family:Arial; font-size:25px;"><b>
             	 OF
            </b></span>
            <span style="font-family:Arial; font-size:25px;"><b>
             	 <%=Pallets %>
            </b></span>
	  	</td>
	  </tr>
	  <%} %>
	  <%if(Pallets==0){ %>
	  		 <tr>
	  	<td style="padding-top: 20px;padding-left: 20px;">
	  		<div style="height:100px;">
	  		  &nbsp;
	  		</div>
	  		<div style="font-size:12px; font-family:Arial;padding-left:25px;"><b>&nbsp;</b></div>
	  		<span style="font-family:Arial; font-size:25px;"><b>
             	 PALLET:
            </b></span>
            <span style="font-family:Arial; font-size:25px;"><b>
             	 0
            </b></span>
            <span style="font-family:Arial; font-size:25px;"><b>
             	 OF
            </b></span>
            <span style="font-family:Arial; font-size:25px;"><b>
             	 0
            </b></span>
	  	</td>
	  </tr>
	  <%} %>
	</table>
</div> 	
<%} %>		
</body>
</html>
<script>

function supportAndroidprint(){
	//获取打印机名字列表
   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
   	 //判断是否有该名字的打印机
   	var printer = "<%=printName%>";
   	var printerExist = "false";
   	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){
		return androidIsPrint(containPrinter);
	}else{
		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消
			return androidIsPrint(containPrinter);
		}
	}
}


function androidIsPrint(containPrinter){
	var printHtml=$('div[name="shipping_lable"]');
	var flag = true ;
	for(var i=0;i<printHtml.length;i++){
		 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Load Label");
      	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
      	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","50mm",$(printHtml[i]).html());
         //visionariPrinter.PREVIEW();
        flag = flag && visionariPrinter.PRINT();
	}
	return flag ;
}
</script>