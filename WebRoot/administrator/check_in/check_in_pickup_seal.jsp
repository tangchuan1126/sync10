<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="java.util.*"%>
<%@ include file="../../include.jsp"%> 


<html>
  <head>
    <title>PickUp Seal</title>
    <!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
  </head>
  <%
  	long main_id  = StringUtil.getLong(request,"main_id");
    String cmd  = StringUtil.getString(request,"cmd");
    String seal  = StringUtil.getString(request,"out_seal");
//    String number  = StringUtil.getString(request,"number");
    long detail_id  = StringUtil.getLong(request,"detail_id");
    long door_id  = StringUtil.getLong(request,"door_id");
//    long status  = StringUtil.getLong(request,"status");
//    long doorId  = StringUtil.getLong(request,"doorId");
    long rel_type  = StringUtil.getLong(request,"rel_type");
    long isLive  = StringUtil.getLong(request,"isLive");
//    String note  = StringUtil.getString(request,"note");
    DBRow row = checkInMgrZwb.findGateCheckInById(main_id);
    DBRow[] loadBars = checkInMgrZwb.selectLoadBar();
    
  %>
  <script type="text/javascript">
  function closeWindow(){
		$.artDialog.close();
  }
  var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
  var submit = api.button([
  		{
			name: 'Submit',
			callback: function () {
				isInput();
				modSeal();
				return false ;
			},
			focus:true,
			focus:"default"
			
		}]
	);
  // 检查是否输入 Seal
  function isInput() {
	  var out_seal=$('#out_seal').val();
	  if(out_seal==''){
	      alert("Do you confirm no seal?")
		  submit();
	  }
  }
  function noSeal(){
	  $("#out_seal").val("NA");
  }
  function modSeal(){
	  $.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxModPickUpSealAction.action',
	  		dataType:'json',
	  		data:"seal="+$("#out_seal").val()+'&mainId='+<%=main_id%>+"&load_bar="+$("#load_bar").val()+'&detail_id='+<%=detail_id%>,
	  		success:function(data){
	  //				 parent.document.getElementById("seal_<%=main_id%>").innerHTML=$("#out_seal").val();
	  				 $.artDialog.opener.pickUpSealFlag(<%=main_id%>,<%=door_id%>,data.nofityflag,<%=rel_type%>,<%=isLive%>);
	  			     closeWindow();
	  			    
	  		 		 
	  		 	//	 parent.loaction.reload();

	  		}
	    });
	 
	 
  }
  
  </script>
  <body>
  <form name="mod_seal">
  <center>
    <table>
    	<tr>
    		<td>PickUp Seal</td>
    		<td>
    			<input  name="out_seal" id="out_seal" value="<%=row.get("out_seal","")%>"/>&nbsp;&nbsp;&nbsp;
    		</td>
    		<td>
    			<input type="button" class="long-button" value="No Seal" onclick="noSeal();">
    		</td>
    	</tr>
    </table>
  </center>
  </form>
  </body>
</html>
