<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">


<%
	long ps_id = StringUtil.getLong(request, "ps_id");
	String ctnr = StringUtil.getString(request, "ctnr");
	DBRow[] rows = checkInMgrZwb.getAllDoorOrSpot(ctnr,ps_id);
	
	
 %>
<html>
  <head>
    
    <title>check CTNR</title>
	<!--  基本样式和javascript -->
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<script language="javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	
	<!-- 遮罩 -->
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
    <!-- 引入ART -->
    <link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
	<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
	
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">
	var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	api.button([
		{
			name: 'Check Out All',
			callback: function () {
				checkoutAll(2);
				
				return false ;
			},
			focus:true,
			focus:"default"
		},
		{
			name: 'Cancel'
			
			
		}]
	);
	
	(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();
	</script>
  </head>
  
  <body onLoad="onLoadInitZebraTable()">
    <table width="100%" border="0" align="center" style="text-align:center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr >
    		<th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Entry ID</th>
    		<th width="10%" class="left-title" style="vertical-align:center;text-align:center;">CTNR</th>
    		<th width="10%" class="left-title" style="vertical-align:center;text-align:center;">LP</th>
    		<th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Spot</th>
    		<th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Door</th>
    		<th width="15%" class="left-title" style="vertical-align:center;text-align:center;">GateCheckIn</th>
    		<th width="15%" class="left-title" style="vertical-align:center;text-align:center;">TrailerStatus</th>
    		<th width="15%" class="left-title" style="vertical-align:center;text-align:center;">TractorStatus</th>
    		<th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Option</th>
    	</tr>
    	<%for(DBRow row:rows){ %>
    	<input type="hidden" value="<%=row.get("yc_id", "") %>" id="yc_id_<%=row.get("dlo_id", "")%>" />
    	<input type="hidden" value="<%=row.get("rel_type", "") %>" id="relType_<%=row.get("dlo_id", "")%>" />
    	<input type="hidden" value="<%=row.get("sd_id", "") %>" id="sd_id_<%=row.get("dlo_id", "")%>" />
    	<input type="hidden" value="<%=row.get("gate_container_no", "") %>" id="ctnr_<%=row.get("dlo_id", "")%>" />
    	<tr style="height:50px;" value="<%=row.get("status", "") %>" id="<%=row.get("dlo_id", "") %>">
    	
    		<td><%=row.get("dlo_id", "") %></td>
    		<td><%=row.get("gate_container_no", "") %></td>
    		<td><%=row.get("gate_liscense_plate", "") %></td>
    		<td><%
    			DBRow spot =(DBRow) row.getValue("spot");
    			if(!spot.get("yc_id", "").equals("")){%>
    				<%=spot.get("yc_no","") %>
    		 	<%}else{%>
    		 		Released
    			<%}%>
    		 </td>
    		<td><%
    			DBRow[] doors =(DBRow[]) row.getValue("door");
    			if(doors.length>0){
    				for(DBRow door:doors){
    			%>
    			<span>	<%=door.get("door_name", "") %></span>

    				
    			<%	}
    			}else{%>
    				Released
    			<%} %>
    		 </td>
    		<td><%String checkInTime = row.get("gate_check_in_time", "");
    		if(!checkInTime.equals("")){
    		
    			checkInTime = checkInTime.substring(0,16);
    			} %>
    			<%=checkInTime.equals("")?"&nbsp;":checkInTime %>
    			</td>
    		
    		<td><%
    		
    		
    		String status = new CheckInMainDocumentsStatusTypeKey().getContainerTypeKeyValue(row.get("status",0));
    		%>
    		<%=status %>
    		</td>
    		<td><%String tractor_status = new CheckInMainDocumentsStatusTypeKey().getContainerTypeKeyValue(row.get("tractor_status",0)); %>
    			<%=tractor_status %>
    		</td>
    		<td><input type="button" class="short-button" onclick="checkout(<%=row.get("dlo_id", "")%>,this)" value="check out" /></td>
    	</tr>
    	<%} %>
    </table>
  </body>
  <script type="text/javascript">
  	function checkout(dloId,evt){
  		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/checkoutCTNRAction.action';
  		var status = $("#"+dloId).attr("value");
  		var datas = {
  			'dlo_id':dloId,
  			'ctnr':$("#ctnr_"+dloId).val(),
  			'flag':1
  		};
  		
  		$.ajax({
  			url:uri,
  			data:datas,
  			dataType:'json',
  			type:'post',
  			beforeSend:function(){
  				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
  			},
  			success:function(data){
  				$.unblockUI();       //遮罩关闭
		 		$("#"+dloId).remove();
		 		var flag = $("input[type=button]").length;
		 		if(flag==0){
  					$.artDialog.close();
		 		}
  			},
  			error:function(){
  				$.unblockUI();       //遮罩关闭
  				alert("System error!");
  			}
  		});
  	}
  	function checkoutAll(flag){
  		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/checkoutCTNRAction.action';
  		
  		
  		var datas = {
  			'ps_id':<%=ps_id%>,
  			'ctnr':'<%=ctnr%>',
  			'flag':flag
  		};
  		
  		$.ajax({
  			url:uri,
  			data:datas,
  			dataType:'json',
  			type:'post',
  			beforeSend:function(){
  				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
  			},
  			success:function(data){
				$.unblockUI();       //遮罩关闭
		 		$.artDialog.close();
  			},
  			error:function(){
  				$.unblockUI();       //遮罩关闭
  				alert("System error!");
  			}
  		});
  	}
  </script>
</html>
