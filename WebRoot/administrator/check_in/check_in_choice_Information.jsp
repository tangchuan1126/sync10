<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.fr.base.core.json.JSONArray"%>
<%@page import="com.fr.base.core.json.JSONObject"%>
<%@ include file="../../include.jsp"%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<%
	String str=StringUtil.getString(request,"str");
    String number=StringUtil.getString(request,"number");
	JSONObject jsonObj = StringUtil.getJsonObject(str);
	long ps_id=StringUtil.getLong(request,"ps_id");
			 	
%>

<style>
.set{padding:2px;width:99%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;border: 2px solid blue;} 
</style>
</head>
<title></title>
<body>
	<fieldset class="set" style="border:2px solid #993300;">
	<legend>
		<span style="font-size:20px; font-weight: bold;"><%=number%></span>
	</legend>
		<table width="99%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" isNeed="true" isBottom="true" style="margin-top:5px;" >
			<thead>
				<tr>
					<th width="6%"  class="right-title"  style="text-align: center;">Choice</th>
				    <th width="8%"  class="right-title"  style="text-align: center;">Carrier</th>
				    <th width="16%" class="right-title"  style="text-align: center;">Appiontment</th>
				    <th width="8%"  class="right-title"  style="text-align: center;">Zone</th>
				    <th width="8%"  class="right-title"  style="text-align: center;">Door</th>
				</tr>
			</thead>
		    <tbody>
		    <%if(jsonObj != null){ %>
				<%JSONArray array =  StringUtil.getJsonArrayFromJson(jsonObj,"values");%>
				<%if(array != null && array.length() > 0 ){%>
					<%for(int i=0;i<array.length();i++){%>
					<%JSONObject jsonItem = StringUtil.getJsonObjectFromArray(array,i); %>
			    	<tr id="tr<%=i%>" ondblclick="doubleClick(<%=i%>)" onclick="trSingleClick(this)"  >
			    		<td align="center" height="25px;" style="border-bottom:1px dashed #CCC;">
			    			<input name="ra" type="radio" value="<%=i%>" zhi="<%=i%>" onclick="singleClick()" />
			    		</td>
			    		<td align="center" style="border-bottom:1px dashed #CCC;">
			    			<%if(jsonItem != null){ %>
			    				<span class="span_company">
			    				<%=StringUtil.getJsonString(jsonItem,"carrier")%>
			    				</span>&nbsp;
			    			<%}%>
			    		</td>
			    		<td align="center" style="border-bottom:1px dashed #CCC;">
			    			<%if(jsonItem != null){ %>
			    				<%if(!StringUtil.getJsonString(jsonItem,"time").equals("")){ %>
			    				    <span class="span_time"><%=StringUtil.getJsonString(jsonItem,"time").substring(0,10)%></span>
			    				<%}%>
			    				&nbsp;
			    			<%}%>
			    		</td>
			    		<td align="center" style="border-bottom:1px dashed #CCC;">
			    		    <%if(jsonItem != null){ %>
			    				<%if(!StringUtil.getJsonString(jsonItem,"area_id").equals("")){ 
			    				%>
			    				    <span class="span_area"><%=StringUtil.getJsonString(jsonItem,"area_name")%></span>
			    				    <span class="span_area_id" style="display:none;"><%=StringUtil.getJsonString(jsonItem,"area_id") %></span>
			    				<%}else{%>
			    					<span class="span_area_id" style="display:none;">0</span>
			    					<span class="span_area">N/A</span>
			    				<%} %>
 			    			<%}%>
			    		</td>
			    		<td align="center" style="border-bottom:1px dashed #CCC;">
			    		    <%if(jsonItem != null){ %>
			    				<%if(!StringUtil.getJsonString(jsonItem,"sd_id").equals("")){ 
			    				%>
			    				    <span class="span_door"><%=StringUtil.getJsonString(jsonItem,"doorid")%></span>
			    				    <span class="span_door_id" style="display:none;"><%=StringUtil.getJsonString(jsonItem,"sd_id") %></span>
			    				<%}else{%>
			    					<span class="span_door_id" style="display:none;">0</span>
			    					<span class="span_door">N/A</span>
			    				<%} %>
 			    			<%}%>
			    		</td>
			    	</tr>
			    	<%}%>
			    <%}%>	
		    <%}%>
		    </tbody>
		</table>
	</fieldset>	
</body>
</html>
<script>
var startNum=0;

function trSingleClick(tr){    //tr单击事件
	var ras=$('input[name="ra"]');
	for(var i=0;i<ras.length;i++){     //先去掉所有颜色
		var zhi=$(ras[i]).attr("zhi");
		$('#tr'+zhi).attr('bgcolor',"");
	}
	var ra=$('input[name="ra"]',$(tr));
	ra.attr("checked","checked");
	var zhi=$(ra).attr("zhi");
	$('#tr'+zhi).attr('bgcolor','#E6F3C5');
}

function singleClick(){      //radio单击事件
	var ra=$('input[name="ra"]:checked');
	var ras=$('input[name="ra"]');
	for(var i=0;i<ras.length;i++){     //先去掉所有颜色
		var zhi=$(ras[i]).attr("zhi");
		$('#tr'+zhi).attr('bgcolor',"");
	}
	var zhi=$(ra).attr("zhi");
	$('#tr'+zhi).attr('bgcolor','#E6F3C5');
}

function doubleClick(selectedIndex){      //双击事件
	initReturnData(selectedIndex)
}
//返回一个Json数组
function initReturnData(selectedIndex){
	var tr = $('#tr'+selectedIndex);
  	var span_time = $(".span_time",tr);
  	var span_area = $(".span_area",tr);
  	var span_company = $(".span_company",tr);
  	var span_area_id = $(".span_area_id",tr);
  	var span_door = $(".span_door",tr);
  	var span_door_id = $(".span_door_id",tr);
  	var time='';
  	var area='';
  	var company='';
  	var area_id=0;
  	if(span_time.html()!=null){
  		time=span_time.html().trim();
  	}
  	if(span_area_id.html()!=null ){
  		area_id=span_area_id.html().trim();
  	}
  	if(span_area.html()!=null){
  		area=span_area.html().trim();
  	}
  	if(span_company.html()!=null){
  		company=span_company.html().trim()
  	}
  	if(span_door.html()!=null){
  		span_door=span_door.html().trim()
  	}
  	if(span_door_id.html()!=null){
  		span_door_id=span_door_id.html().trim()
  	}
	var obj = {
			'span_time':time,
			'span_area':area,
			'span_company':company,
			'span_area_id':area_id,
			'span_door_id':span_door_id,
			'span_door':span_door
	}	
	$.artDialog.opener.huixian(obj);
	$.artDialog && $.artDialog.close();
 }
function returnCloseDialog(selectedIndex){
	initReturnData(selectedIndex);
}
//绑定键盘事件

$('body').on("keydown", function(event) {
	var key = event.which;
	if (key == 13) {                 //绑定回车
		var val= $('input[name="ra"]:checked').val();
		returnCloseDialog(val);
 	}
	if(key== 38){                    //绑定上
		startNum--;
		var ra=$('input[name="ra"]');
		var endNum=ra.length;
		if(startNum < 0){
			startNum=endNum-1;
		}
		for(var i=0;i<ra.length;i++){     //先去掉所有颜色
			var zhi=$(ra[i]).attr("zhi");
			$('#tr'+zhi).attr('bgcolor',"");
		}
		$(ra[startNum]).attr('checked',true);
		var zhi=$(ra[startNum]).attr("zhi");
		$('#tr'+zhi).attr('bgcolor','#E6F3C5');
	}
	if(key==40){                     //绑定了下
		startNum++;
		var ra=$('input[name="ra"]');
		var endNum=ra.length;
		if(startNum>=endNum){
			startNum=0;
		}
		for(var i=0;i<ra.length;i++){     //先去掉所有颜色
			var zhi=$(ra[i]).attr("zhi");
			$('#tr'+zhi).attr('bgcolor',"");
		}
		$(ra[startNum]).attr('checked',true);
		var zhi=$(ra[startNum]).attr("zhi");
		$('#tr'+zhi).attr('bgcolor','#E6F3C5');

		
	}
});

(function(){
	var ra=$('input[name="ra"]');
	if(ra.length>0){
		$(ra[0]).attr('checked',true);
		var zhi=$(ra[0]).attr("zhi");
		$('#tr'+zhi).attr('bgcolor','#E6F3C5');
	}
})();

</script>