<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.CheckInDoorStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="checkInDoorStatusTypeKey" class="com.cwc.app.key.CheckInDoorStatusTypeKey"/>
<jsp:useBean id="checkInLiveLoadOrDropOffKey" class="com.cwc.app.key.CheckInLiveLoadOrDropOffKey"/>
<jsp:useBean id="checkInMainDocumentsRelTypeKey" class="com.cwc.app.key.CheckInMainDocumentsRelTypeKey"/>
<jsp:useBean id="checkInTractorOrTrailerTypeKey" class="com.cwc.app.key.CheckInTractorOrTrailerTypeKey"/>

<%
	long id=StringUtil.getLong(request,"infoId");
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long ps_id = adminLoggerBean.getPs_id() ;
long adminId=adminLoggerBean.getAdid(); 
DBRow manRow=checkInMgrZwb.findGateCheckInById(id);

String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/check_in/gate_check_out.html?id="+id;
String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
String checkInFileUploadAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/checkin/checkInFileUploadAction.action";
DBRow[] files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(id,FileWithTypeKey.OCCUPANCY_MAIN);
 %>	
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
	<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
	
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	<!-- 在线阅读 -->
	<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script> 
	<!-- 打印 -->
	<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
	<script type="text/javascript" src="../js/print/m.js"></script>
	 <!-- 遮罩 -->
    <script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
    
	<style type="text/css">
td.topBorder{
	border-top: 1px solid silver;
}
.ui-datepicker{font-size: 0.9em;}
</style>
<script type="text/javascript">

(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

$(function(){
	$("#tabs").tabs({});
	
	$("input[name='checkboxDoor']").attr("checked",true); 
	$("input[name='checkboxPark']").attr("checked",true); 
	
	
});
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	$.artDialog.open(uri , {id:'file_up',title: 'Upload photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
				 //调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   	}});
}
//在线获取
function onlineScanner(target){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target="+target; 
	$.artDialog.open(uri , {title: 'Online Mutiple',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
var onlineNames = "";
//jquery file up 回调函数
function uploadFileCallBack(fileNames,_target){
	if(_target=="onlineScanner"){
		if($.trim(fileNames).length > 0 ){
			var array = fileNames.split(",");
			var lis = "";
			for(var index = 0 ,count = array.length ; index < count ; index++ ){
				var a =  createA(array[index]);
				if(a.indexOf("href") != -1){
				    lis += a;
				}
			}
			var td = $("#over_file_td");
			td.append(lis);
			onlineNames += fileNames+",";
			$("input[name='file_names']").val(onlineNames); 
		} 
	}else{
		    $("p.new").remove();
		    if($.trim(fileNames).length > 0 ){
				$("input[name='file_names']").val(fileNames); 
				var array = fileNames.split(",");
				var lis = "";
				for(var index = 0 ,count = array.length ; index < count ; index++ ){
					var a =  createA(array[index]) ;
					 
					if(a.indexOf("href") != -1){
					    lis += a;
					}
				}
				var td = $("#over_file_td");
				
				td.append(lis); 
			} 
	}		
		$("#submitBtn").focus();
}
function createA(fileName,target){
    var id = fileName.substring(0,fileName.length-4);
    var  a = "<p id='add"+id+"' style='color:#439B89' class='new' ><a id='"+id+"'  href='javascript:void(0)' onclick='showTempPictrueOnline(&quot;"+fileName+"&quot;)' style='color:#439B89' >"+fileName+"</a>&nbsp;&nbsp;<a  href='javascript:deleteA(&quot;"+id+"&quot;)' style='color:red' >×</a></p>";
    return a ;
}

function deleteA(id){
	$("#add"+id).remove();
	var file_names = $("input[name='file_names']").val(); 
	
	 var array = file_names.split(",");
	 var lastFile = "";
	 for(var index = 0 ; index < array.length ; index++ ){
			if(id==array[index].substring(0,array[index].length-4)){
				array[index]="";
				
			}
			lastFile += array[index];
			if(index!=array.length-1 && array[index]!=""){
				lastFile+=",";
			}
	 }
	 
	 $("input[name='file_names']").val(lastFile);
	
}
function closeWindow(){
	$.artDialog.close();
}
//图片在线显示  		 
function showPictrueOnline(fileWithType,fileWithId ,currentName){
    var obj = {
  		file_with_type:fileWithType,
  		file_with_id : fileWithId,
  		current_name : currentName ,
  		cmd:"multiFile",
  		table:'file',
  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/check_in"
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
}
//删除图片
function deleteFile(file_id){
	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:'file',file_id:file_id,folder:'check_in',pk:'file_id'},
		success:function(data){
			if(data && data.flag === "success"){
				$("#file_tr_"+file_id).remove();
			}else{
				showMessage("System error,please try later","error");
			}
			
		},
		error:function(){
			showMessage("System error,please try later","error");
		}
	});
}
function confirm(){
	    $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
		if(onlineNames!="" && onlineNames!=null){
			var onlineNames = $("input[name='file_names']").val(); 
			onlineNames = onlineNames.substring(0,onlineNames.length-1);
			$("input[name='file_names']").val(onlineNames);
		}
		
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/CheckInFileUploadAction.action',
			dataType:'json',
			data:'dlo_id='+<%=id%>+'&createMainId='+<%=adminId%>+'&'+$("#fileform").serialize(),
			success:function(data){	          
			   parent.location.reload();        
			   $.artDialog && $.artDialog.close();
			}
		});
	
}
function showTempPictrueOnline(_fileName){
	   var obj = {
			current_name:_fileName ,
	   		table:"temp",
	   		fileNames:$("input[name='file_names']").val(),
	   		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upl_imags_tmp"
		}
	   if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}
function onlineSingleScanner(_target){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_single_scanner.html?target="+_target; 
$.artDialog.open(uri , {title: 'Online Single',width:'300px',height:'170px', lock: true,opacity: 0.3,fixed: true});

}
function closeWindow(){
	$.artDialog.close();
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	<fieldset style="width:300px; width:96%; border:2px solid blue;" >
		<legend>
						<a  style="color:#f60;" target="_blank" >
	  						E<%=manRow.getString("dlo_id") %>
	  					</a>
	  					<a style="color:mediumseagreen;width:50px" target="_blank">
	  					<%if(!manRow.getString("gate_container_no").equals(""))
  						 	{
	  					%>
	  						<font color='blue'>|</font>
	  						<%=checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(CheckInTractorOrTrailerTypeKey.TRAILER)%>:
	  						
  						 	
  						<% 		
  								out.print(manRow.getString("gate_container_no"));
  						 	} 
  						 %> 
	  					</a>
	  					<a main_id="<%=manRow.get("dlo_id",0) %>" tractor_status="<%=manRow.get("status",0) %>" style="color:#808000;" target="_blank" >
	  					<%
	  						if(manRow.get("status",0)!=0 && manRow.get("status",0)!=11 && !manRow.getString("gate_container_no").equals("") ){
	  					%>
	  						<font color='blue'>|</font>
	  						
	  						<%if(manRow.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.BOTH){
	  						%>
	  						Delivery:<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(manRow.get("status",0))%>
	  						<%
	  						}else{
	  						%>
	  						<%=checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(manRow.get("rel_type",0)) %>:<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(manRow.get("status",0))%>
	  						<%
	  						}
	  						%>
	  					<%	
	  						}
	  					%>
		  						
		  					
	  					</a>
	  				</legend>
	  				<div style="padding-right: 20px;color:#06C;font-weight: bold;" align="right">
							<%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(manRow.get("isLive",0))%>
							
	  				</div>	
		</legend>
<%
	  					DBRow[] details = checkInMgrZwb.selectDetailsInfo(0,id);
	  					if(details != null && details.length > 0 ){ 
	  						flag:for(int i=0;i< details.length ;i++){ 
	  						  if(!details[i].getString("doorId").equals("") ){ 
			  						for(int a = 0;a<i;a++){
				      				  if(details[i].getString("doorId").equals(details[a].getString("doorId"))){
				      					  continue flag ;
				      				  }
						      		}
		  					       DBRow[] numbers = checkInMgrZwb.selectDetailsInfo(details[i].get("rl_id",01),id);
%>
	<fieldset style="border:2px solid #993300; width:90%; margin-top:10px;">
		<legend>
			<table border="0" cellspacing="0" cellpadding="0">
			  <tr>
				    <td style="font-size:16px;font-weight:bold;" >DOOR:</td>
				    <td width="40px;" style="font-size:16px;font-weight:bold;" id="<%=details[i].getString("rl_id") %>" doorSta = "<%=details[i].getString("occupancy_status") %>"  mainId="<%=details[i].getString("dlo_id") %>" >
			    	<%=details[i].getString("doorId") %> 
				    </td>
		      </tr>
		   </table>
	    </legend>
		      <%
	      			if(numbers.length > 0 ){
	      				for(int j=0;j< numbers.length ;j++){
			      			if(!numbers[j].getString("number").equals("")){
			  %>
		      <div style="height:25px;" align="left">  
			   		 <table border="0" cellspacing="0" cellpadding="0">
					    <tr>
						       <%if(numbers[j].get("number_type", 0l)==12 || numbers[j].get("number_type", 0l)==13 || numbers[j].get("number_type", 0l)==14){
							   %>
							   	<td style="color:red" align="right" width="65px;"><%=moduleKey.getModuleName(numbers[j].get("number_type", "")) %>:</td>
							   
							   <% 
							   } else{
							   %>
							   	<td style="color:blue" align="right" width="65px;"><%=moduleKey.getModuleName(numbers[j].get("number_type", "")) %>:</td>
							   	
							   <%
							   }
							   %>
						    <td width="110px;" >
						     	<%=numbers[j].getString("number") %>
						    </td>
						</tr>
					  </table>
			   </div>
			   <%}
	    		   }
	          }
			   
	  		}else{
				  DBRow[] gateNumber = checkInMgrZwb.findAllLoadByMainId(id);
					if(!gateNumber[0].getString("number").equals("")){
				  	  %>
				  	  <fieldset style="border:2px solid #993300; width:90%; margin-top:10px;">
  				      
  				      		  <div style="height:25px;" align="left">
	  							<table border="0" cellspacing="0" cellpadding="0">
								  <tr> 
								       <%
								       	if(gateNumber[0].get("number_type", 0l)==12 || gateNumber[0].get("number_type", 0l)==13 || gateNumber[0].get("number_type", 0l)==14){
									   %>
									   	<td style="color:red" align="right" width="65px;"><%=moduleKey.getModuleName(gateNumber[0].get("number_type", "")) %>:</td>
									   
									   <% 
									   } else{
									   %>
									   	<td style="color:blue" align="right" width="65px;"><%=moduleKey.getModuleName(gateNumber[0].get("number_type", "")) %>:</td>
									   	
									   <%
									   }
									   %>
								     <td width="110px;" >
								   	 <%=gateNumber[0].getString("number") %>
								     </td>
								 </tr>
								</table>
							  </div>
  				      <%
			  	   	 }
					
			    }
	      	
	      	%>
	      	</fieldset>
	      	<%
	  	}
	}
	  						  
%>
</fieldset>
</fieldset>

	<br>
		
	<form id="fileform" method="post" action="<%=checkInFileUploadAction %>">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		    <td colspan="2" align="left" valign="top">
			    <fieldset style="width:300px; width:96%; border:2px solid blue; height:100px; overflow:auto">
					<legend style="font-size:18px;font-weight:normal;color:#f60;">
							Photo
					</legend>
			    	<table>
			    		<tr>
			    			<td>
			    				<input type="button" id="online1" class="long-button" onclick="onlineScanner('onlineScanner');" value="Multiple Photos" />
			    			</td>
							<td>
	                            <input type="button" id="online" class="long-button" onclick="onlineSingleScanner('onlineScanner');" value="Single Photo" />
								<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="Upload Photo" />
							</td>
						</tr>
						<tr>
						</tr>
						<%
						 if(files != null && files.length > 0 ){
				      		for(DBRow file : files){
						%>
						<tr id="file_tr_<%=file.getString("file_id") %>">
							<td><%= DateUtil.showLocalparseDateTo24Hours(file.getString("upload_time") , ps_id) %></td>
							<td>
							 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		                 <%if(StringUtil.isPictureFile(file.getString("file_name"))){ %>
					             <p>
					 	            <a style="color:#439B89" id="filename" href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.OCCUPANCY_MAIN%>','<%=id%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
					             </p>
					     <%}else{ %>
									<a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder= check_in'><%=file.getString("file_name") %></a>
						 <%}%>
							</td>
							<td width="30px"></td>
							<td>
								<a  href="javascript:deleteFile('<%=file.get("file_id",0l) %>')" style='color:red' >×</a>
							</td>	
						 </tr>	 
						<%}}%>
						
						<tr>
							<input type="hidden" name="backurl" id="backurl" value="" />
			 				<input type="hidden" name="file_with_type" value="<%=FileWithTypeKey.OCCUPANCY_MAIN %>" />
			 				<input type="hidden" name="sn" id="sn" value="window_check_out"/>
						 	<input type="hidden" name="file_names" id="file_names"/>
						 	<input type="hidden" name="path" value="check_in"/>
						 	<input type="hidden" name="dlo_id" value="<%=id%>"/>
						 	<td></td>
						 	<td id="over_file_td"></td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>
	</form>	
	<table>
		 <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:97%;position:absolute;left;0;bottom:0;">
			 <input type="button"  name="Submit2" value="Check out" class="normal-green" onClick="confirm();" >
			 <input name="Submit2" type="button" class="normal-white" value="Cancel" onClick="closeWindow();">
	     </div>
	</table>
</body>
