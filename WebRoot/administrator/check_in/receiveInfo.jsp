<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="reasonKey" class="com.cwc.app.key.LoadConsolidateReasonKey"/> 


<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long ps_id = adminLoggerBean.getPs_id(); 
	long detailId = StringUtil.getLong(request, "dlo_detail_id");
	DBRow recepit = checkInMgrWfh.getReceivePalletInfoByDetailId(detailId);
	
%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<head>
	
	
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../js/accordion/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/accordion/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="../js/accordion/jquery-ui.theme.min.css" />
	<script src="../js/showMessage/showMessage.js" type="text/javascript"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
	div.lines{padding:0px;margin: :0px;}
	div.pallets table td{border:1px solid silver;}
	span.lineInfo{font-size: 14px;font-weight: bold;}
	.single{}
	.double{}
	.single:hover {background:#ddd; }
	.double:hover {background:#ddd;}
	td.header{width: 25%;text-align: center;font-size: 14px;font-weight: bold;background: black;color: white;line-height: 30px;height: 30px;}
	</style>
	<script type="text/javascript">
	$(function() {
		$("#receive").accordion({
			heightStyle: "content"
		});
		
		$(".linesInfo").click(function(){
			 $(this).next(".pallets").slideToggle("slow");
		})
		$(".pallets tr#show_sn").click(function(){
			var con_id = $(this).attr("value");
			var con_no = $(this).attr("con_no");
			$.ajax({
	 			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/ajaxFindSN.action',
	 			type:'post',
	 			dataType:'json',
	 			data:{con_id:con_id,pageSize:1,pageNo:1,ps_id:<%=ps_id%>},
	 			success:function(data){
	 				if(data!=null && data.length>0){
	 					var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/receive_sn_info.html?con_id='+con_id;
	 					 $.artDialog.open(url, {
	 						 title: "Pallet No. "+con_no,width:700,height:500, lock: true,opacity: 0.3,fixed: true, 
	 						    cancelVal: 'Close',
	 						    cancel: function(){
	 							}
	 					 });
	 				}else{
	 					showMessage("No scanning SN!","alert");
	 				}
	 			},
	 			error:function(){
	 				alert('System error');
	 			}
	 			
	 		});
		});
	});
	
	</script>
</head>
<body style="font-size:12px">
	<div style="font-size:20px ;text-align: center;border: 1px solid silver;padding: 5px;width:700px;">
		<B><%=moduleKey.getModuleName(recepit.get("number_type",0)) %>:<%=recepit.getString("number") %> </B>
	</div>
				<div id="receive" style="width:700px">
				<% 
		//		DBRow recepit =	data.get("datas",new DBRow[0]);
				if(recepit != null){
	//				for(DBRow recepit : datas){
						DBRow[] receipt_lines = (DBRow[])recepit.get("receipt_lines",new DBRow[]{});
							if(!StrUtil.isBlank(recepit.getString("receipt_no") )){
								
						%>
							<h3>Receipt No:<%=recepit.getString("receipt_no") %> &nbsp; &nbsp; &nbsp; 
								Total Pallets : <%=recepit.get("total_pallets",0l) %>&nbsp; &nbsp; &nbsp;
								Total Expected Qty : <%=recepit.get("expected_qty",0l)%>&nbsp; &nbsp; &nbsp;
								Total Received Qty : <%=recepit.get("received_qty",0l) %>&nbsp; &nbsp; &nbsp;
							</h3>
							<div class="lines" style="padding: 0px;padding: 10px;padding-top: 0px;">
						<%
							if(receipt_lines != null && receipt_lines.length > 0 ){
								for(DBRow line : receipt_lines){
									%>
									<div class="order" style="border:1px solid silver; margin-top:10px;background-color: white; padding: 4px;">
									<div class="linesInfo" style="cursor: pointer;">
										<div style="float: left;width:50% ; height: 70px;border-right:2px dashed  silver;">
											<span class="lineInfo">Item: <font color="#f60"><%=line.getString("item_id") %></font></span><br />
									 		<span class="lineInfo">Lot NO: <%=line.getString("lot_no") %> </span><br />
								            <span class="lineInfo">Is Need Scan SN: <font color="#f60"><%=line.get("is_check_sn",0)==1?"Yes":"No" %></font></span><br />
								            <span class="lineInfo">Status: <%=line.getString("status","")%></span>
									 		
									 	</div>
									 	<div style="float: left;line-height:18px;height: 70px;padding-left: 4px;">
											<span class="lineInfo">Pallets: <font color="#f60"><%=line.get("pallets",0l) %></font></span><br />
									 		<span class="lineInfo">Expected Qty: <%=line.get("expected_qty",0l) %></span><br />
								            <span class="lineInfo">Received Qty: <font color="#f60"><%=line.get("received_qty",0l) %> (G:<%=line.get("goods_total",0l) %>+D:<%=line.get("damage_total",0l) %>)</font></span><br />
								            <span class="lineInfo">Start Scan Time: <%=DateUtil.showLocalparseDateTo24Hours(line.getString("scan_start_time"),recepit.get("ps_id",0l)) %></span>
									 	</div>
									 	
									 	<div style="clear: both; "></div>
									 </div>
									 		<div class="pallets" style="margin: 4px;display: none;">
							 				 	<table  style="border-collapse :collapse;border:1px solid silver;width: 100%;">
							 				 		<thead>
							 				 			<td class="header" >Pallet Type</td>
							 				 			<td class="header" >Pallet No.</td>
							 				 			<td class="header" >Qty/Pallet</td>
							 				 			<td class="header" ></td>
							 				 		</thead>
								
									 		<% DBRow[] pallets =(DBRow[]) line.get("pallet",new DBRow[]{});
									 			if(pallets != null && pallets.length > 0){
									 			//	List<DBRow> hasConsolidate = new ArrayList<DBRow>();
									 				%>
									 				<% 
									 				for(int pindex = 0 , pcount = pallets.length ; pindex < pcount ; pindex++){
									 					DBRow pallet = pallets[pindex];
									 					String classType = pindex % 2 == 0 ? "single":"double"; 
									 					String palletNumber =  pallet.getString("plate_no");
									 			//		if(pallet.get("wms_consolidate_reason", 0) != 0 && !StringUtil.isNull(pallet.getString("wms_consolidate_pallet_number"))){
									 			//			hasConsolidate.add(pallet);
									 			//			continue ;
									 			//		}
									 					
									 			//		DBRow consolidateRow = loadPaperWorkZr.getConsolidatePalletType(palletNumber, hasConsolidate);
									 					%>
									 						 <tr id="show_sn" class="<%=classType%>" id="pallet_line" style="cursor: pointer;" con_no="<%=palletNumber%>" value="<%=pallet.get("con_id", 0L)%>">
									 						 	<td style="text-align: center;font-size: 14px">
									 						 		<%if(pallet.get("container_type", 0)==3){
									 						 			out.print("TLP");
									 						 		}else{
									 						 			out.print("CLP");
									 						 		}%>
									 						 	</td>
									 						 	<td style="text-align: center;font-size: 14px"> 
									 						 		<%
									 									if(pallet.get("detail_id",0l)!=detailId){
									 								%>
									 									<font color="red">!</font>
									 								
									 								<%
									 									}else{
									 										out.print("&nbsp;");
									 									}
									 								%>
									 								
									 								<%= palletNumber %> 
									 							</td>
									 							<%
									 							      if(pallet.get("goods_type",0)==2 || (pallet.get("goods_type",0)==0 && pallet.get("is_partial",0)==2 )){
									 							%>
									 								  <td style="text-align: center;font-size: 14px"> <%=pallet.get("normal_qty",0l)+pallet.get("damage_qty",0l) %></td>
									 								  
									 							<%    	  
									 							      }else{
									 							%>
									 								  <td style="text-align: center;font-size: 14px"> <%=pallet.getString("length") %>X<%=pallet.getString("width") %>X<%=pallet.getString("height") %></td>
									 							
									 							<%   	  
									 							      }
									 							
									 							%>
									 						 	<td style="font-size: 14px;text-align:center;">
									 						 	<div style="line-height: 20px;height: 20px;padding-left: 15px;">
									 						 		&nbsp;<span style="width:45px;display:inline-block;">G: <font color="#f60"><%=pallet.get("normal_qty",0l) %></font></span>
									 							    &nbsp;<span  style="width:40px;display:inline-block;">D: <font color="#f60"><%=pallet.get("damage_qty",0l) %></font></span>
									 							</div>
									 						 	</td>
									 						 	
									 						 	
									 						 </tr>
 									 					<% 	
									 				}
									 			}else{
									 				%>
									 				<tr>
									 					<td style="height: 30px;line-height: 20px;text-align: center;" colspan="4">No Records</td>
									 				</tr>
									 				<%
									 			}
									 			
									 		%>
								 				</table>
									 		</div>	
									</div>
							
									<% 
									}
								}
							}
						%>
								</div>
					<% 
			//		}		
		%>
		<%}%>
		</div>
		
	
</body>