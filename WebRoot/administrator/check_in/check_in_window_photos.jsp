<%@page import="com.cwc.app.key.FileWithCheckInClassKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<html>
<head>

<%
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adminId = adminLoggerBean.getAdid();
long entry_id = StringUtil.getLong(request, "entry_id");
long psId = adminLoggerBean.getPs_id();
DBRow row = checkInMgrZwb.findMainById(entry_id);
//String deleteFileAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/CommonFileDeleteAction.action";
String uploadFilesAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/checkin/UploadFilesAction.action";

DBRow[] files = null ;
DBRow file = null;

if(entry_id > 0l){
	files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(entry_id, FileWithTypeKey.OCCUPANCY_MAIN);
}
String base_path = ConfigBean.getStringValue("systenFolder")  + "_fileserv/file/";	
%>

<title>Check In Photos</title>
<style type="text/css">
	#imageList {
		border: 1px solid #aaaaaa;
		height: 385px;
		margin-top: 5px;
		white-space: nowrap;
		overflow-x: scroll;
		overflow-y: hidden;
		background-color: #e8e8e8;
	}
	
	#imageList ul {
		padding-left: 10px;
		padding-bottom: 10px;
	}
	
	#imageList ul li {
		display: inline-block;
		padding-left: 10px;
		padding-top: 0px;
		padding-bottom: 10px;
	}
	
	#imageList ul li:last-child {
		padding-right: 20px;
	}
	
	#imageList ul li > .image_div {
		text-align: center;
	}
	#imageList ul li > .image_div > img {
		width: 200px;
		height:300px;
		opacity: 1;
	}
	
	a.abs {
		display: block;
		overflow: hidden;
		text-align:center;
	}
		
	.delete_btn a{
		margin-top: 5px;
		float: right;
		text-decoration: none;
	}
	
	.upload{
		position: relative;
		border:1px solid #aaa;
		display:inline-block;
	    margin: 0px 0px 0px 0px;
	    padding: 0px 0px 0px 0px;
	    vertical-align: top;
		height: 60px;
		box-shadow: 1px 1px 3px #88a1b8, 1px -1px 3px #b6cfe7, -1px 1px 3px #b6cfe7, -1px -1px 3px #b6cfe7;
	} 
	.upload div{
		float: left;
	}
	.upload .multiple-button{
		background-image: url("../js/picture/images/multiple_photos_grey.png");
		background-position: 0px -17px;
		width:99px;
		height:100%;
		border:none;
	}
	.upload .multiple-button:HOVER{
		background-image: url("../js/picture/images/multiple_photos_blue.png");
	}
	.upload .single-button{
		background-image: url("../js/picture/images/single_photos_grey.png");
		background-position: 0px -17px;
		width:99px;
		height:100%;
		border-left:1px solid #469eff;
		border-right:1px solid #469eff;
		border-top:none;
		border-bottom:none;
	}
	.upload .single-button:HOVER{
		background-image: url("../js/picture/images/single_photos_blue.png");
	}
	.upload .upload-button{
		background-image: url("../js/picture/images/upload_photos_grey.png");
		background-position: 0px -17px;
		width:99px;
		height:100%;
		border:none;
	}
	.upload .upload-button:HOVER{
		background-image: url("../js/picture/images/upload_photos_blue.png");
	}
</style>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
</head>
<body>
   	<div id="upload" class="upload" style="margin:0px; padding:0px;">
		<div id="mutiple" class="multiple-button" onclick="onlineScanner('onlineScanner');" title="Multiple Photos" onkeypress="if(event.keyCode == 13) { onlineScanner('onlineScanner');return false; }" />
		<div id="single" class="single-button" onclick="onlineSingleScanner('onlineScanner');" title="Single Photo" onkeypress="if(event.keyCode == 13) { onlineSingleScanner('onlineScanner');return false; }" />
		<div id="uploadfile" class="upload-button" onclick="uploadFile('jquery_file_up');" title="Upload photo" onkeypress="if(event.keyCode == 13) { uploadFile('jquery_file_up');return false; }"/>
	</div>
	<div id="imageList">
		<ul>
		<% if(files.length > 0) {
			int length = files.length - 1;
			for(int i = length; i >= 0; i--) {
		%>
			<li id='file_li_<%=files[i].get("file_id", 0l) %>'>
				<div class="image_div">
					<img src="<%=base_path + files[i].getString("file_id") %>" onload="scaleImage(this, 300, 300)" onclick="showPictrueOnline('<%=FileWithTypeKey.OCCUPANCY_MAIN%>', '<%=entry_id%>', '<%=files[i].getString("file_name") %>');"/>
				</div>
				<div class="info">
					<a class="abs" href="javascript:void(0)"><%=files[i] == null ? "" : files[i].getString("file_name") %></a>
					<div class="delete_btn">
						<a href="javascript:deleteFile('<%=files[i].get("file_id", 0l) %>')">
							<span style="display: inline-block; background:url(../js/easyui/themes_visionari/icons/cancel.png) top left no-repeat; width: 16px; height:16px;  vertical-align: middle;"></span>
							<span style="vertical-align: middle;">Delete</span>
						</a>
					</div>
					<div style="clean: both"></div>
				</div>
			</li>
		<%		
			}
		}%>
		</ul>
	</div>
	<form id="fileform">
		<input type="hidden" name="backurl" id="backurl" value="" />
		<input type="hidden" name="file_with_type" value="<%=FileWithTypeKey.OCCUPANCY_MAIN %>" />
		<input type="hidden" name="sn" id="sn" value="window_check_in"/>
	 	<input type="hidden" name="file_names" id="file_names"/>
	 	<input type="hidden" name="is_in_file_serv" id="is_in_file_serv" value="1"/>
	 	<input type="hidden" name="path" value="check_in"/>
	 	<input type="hidden" name="dlo_id" value="<%=entry_id%>"/>
	 	<input type="hidden" name="creator" value="<%=adminId%>"/>	
	 	<input type="hidden" name="file_with_class" value="<%=FileWithCheckInClassKey.PhotoWindowCheckIn%>"/>	
	 	<input type="hidden" name="appointmentdate" id="appointmentdate" value="<%=!row.getString("appointment_time").equals("") ? DateUtil.showLocalTime(row.getString("appointment_time"), psId) : ""%>"/>
	</form>
	
	<script type="text/javascript">
		// 文件上传
		function uploadFile(_target){
		    var fileNames = $("input[name='file_names']").val();
		    var obj  = {
			     reg: "picture_office",
			     limitSize: 8,
			     target: _target
			 }
		    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file_serv/jquery_file_up.html?"; 
			uri += jQuery.param(obj);
			if(fileNames && fileNames.length > 0 ){
				uri += "&file_names=" + fileNames;
			}
			 $.artDialog.open(uri , {id:'file_up', title: 'Upload Photo', width:'770px', height:'530px', lock: true,opacity: 0.3, fixed: true,
	 			close: function(){
					// 调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
	   		 	}
			 });
		}	
		
		var onlineNames = "";
		//jquery file up 回调函数
		<%-- 
		function uploadFileCallBack(fileNames, target){

//		    $("p.new").remove();
			if($.trim(fileNames).length > 0 ){
				$("input[name='file_names']").val(fileNames); 
				var array = fileNames.split(",");
				for(var index = 0 ; index < array.length ; index++ ) {
					if(array[index].length>0){
						var id = array[index].substring(0, array[index].length - 4);
						if($("#add" + id).length > 0) {
							continue;
						}
						var a =  createA(array[index]) ;
						if($("#imageList ul").children().length > 0) {
							$(a).insertBefore($("#imageList ul li:first"));
						} else {
							$("#imageList ul").append(a);
						}
						
						$.ajax({
							url: '<%= uploadFilesAction%>',
							dataType: 'json',
							data: $("#fileform").serialize()
							,
							success: function (data) {
								
							}
						});	
						
					}
				}
				
			/*	tab.tabs({
					selected:3
				});
				*/
				loadPhotos();
			} 
		}	 
		 --%>
		function uploadFileCallBack(files, target){
			var fileIds = "";
			if(files.length > 0 ){
				for(var i = 0 ; i < files.length ; i++ ) {
					var id = files[i].file_id;
					if($("#add" + id).length > 0) {
						continue;
					}
					var a =  createA(files[i]) ;
					if($("#imageList ul").children().length > 0) {
						$(a).insertBefore($("#imageList ul li:first"));
					} else {
						$("#imageList ul").append(a);
					}
					fileIds += id+",";
				}
				if(fileIds.length>0){
					fileIds = fileIds.substring(0, fileIds.length-1);
					$("input[name='file_names']").val(fileIds); 
					$.ajax({
						url: '<%=uploadFilesAction%>',
						dataType: 'json',
						data: $("#fileform").serialize(),
						success: function (data) {
							
						}
					});
				}
				loadPhotos();
			} 
		}	
		
		function createA(file){
		    var  a = "<li>"
		    			+ "<div class='image_div'><img id='add" + file.file_id + "' src='<%=base_path%>" + file.file_id + "' onclick='showPictrueOnline(<%=FileWithTypeKey.OCCUPANCY_MAIN%>, <%=entry_id%>)'" + "></div>"
		    			+ "<div class='info'><a href='javascript:void(0)' class='abs'>" + file.original_file_name + "</a>"
		    				+ "<div class='delete_btn'>"
		    					+ "<a href='javascript:deleteFile(&quot;" + file.file_id + "&quot;)'>"
		    						+ "<span style='display:inline-block; background:url(../js/easyui/themes_visionari/icons/cancel.png) top left no-repeat; width: 16px; height:16px;  vertical-align: middle;'></span>"
									+ "<span style='vertical-align:middle;'>Delete</span>"
		    					+ "</a>"
		    				+ "</div>"
		    				+ "<div style='clean: both'></div>"
		    			+ "</div>"
		    			
		    		+ "</li>";
		    return a ;
		}
		
		// 图片在线显示  		 
		function showPictrueOnline(fileWithType,fileWithId ,currentName){
		    var obj = {
		  		file_with_type:fileWithType,
		  		file_with_id : fileWithId,
		  		current_name : currentName ,
		  		cmd:"multiFile",
		  		table:'file',
		  		base_path:'<%= base_path%>'
			}
			openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}	
		
		// 删除图片
		<%-- 
		function deleteFile(file_id) {
			//console.log('<%= deleteFileAction%>');
			$.ajax({
				url: '<%= deleteFileAction%>',
				dataType: 'json',
				data: {
					table_name: 'file',
					file_id: file_id,
					folder: 'check_in',
					pk: 'file_id'
				},
				success: function (data) {
					//console.log(data);
					if(data && data.flag === "success") {
						$("#file_li_" + file_id).remove();
					} else {
						showMessage("System error, please try later.", "error");
					}
					
				},
				error: function () {
					showMessage("System error,please try later", "error");
				}
			});
		}
		 --%>
		function deleteFile(file_id) {
			
			$.artDialog({
			    content: '<span style="font-size:14px;">Confirm delete this photo?</span>',
			    icon: 'question',
			    lock: true,	
			    width: 240,
			    height: 70,
			    title:'tips',
			    okVal: 'Yes',
			    ok: function () {
			    	$.ajax({
	    				url: '<%=base_path%>'+file_id,
	    				dataType: 'json',
	    				type:'delete',
	    				success: function (data) {
	    					if(data && data.length>0) {
	    						$("#file_li_" + file_id).remove();
	    					} else {
	    						showMessage("System error, please try later.", "error");
	    					}
	    				},
	    				error: function () {
	    					showMessage("System error,please try later", "error");
	    				}
	    			});
			    },
			    cancelVal: 'No',
			    cancel: function(){
			    	
				}
			});	
		}
		
		// 上传图片到将图片信息存到表里
		//function uploadFiles() {
			
			//tab.unbind("tabsselect");
			
		//	return true;
		//}	
		//在线获取
		function onlineScanner(target){
		    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file_serv/picture_online_scanner.html?target="+target; 
			$.artDialog.open(uri , {title: 'Online Mutiple',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
		}
		function onlineSingleScanner(_target){
		    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file_serv/picture_online_single_scanner.html?target="+_target; 
			$.artDialog.open(uri , {title: 'Online Single',width:'300px',height:'170px', lock: true,opacity: 0.3,fixed: true});
		}
		
		function scaleImage(o, w, h){
			var img = new Image();
			img.src = o.src;
			if(img.width >0 && img.height >0)
			{
				if(img.width/img.height >= w/h)
				{
					if(img.width > w)
					{
						$(o).width(w);
						$(o).height(img.height*w/img.width);
					}
					else
					{
						$(o).width(img.width);
						$(o).height(img.height);
					}
				}
				else
				{
					if(img.height > h)
					{
						$(o).height(h);
						$(o).width(parseInt(img.width / img.height * h));
					}
					else
					{
						$(o).width(img.width);
						$(o).height(img.height);
					}
				}
			}
		}
	</script>
</body>
</html>