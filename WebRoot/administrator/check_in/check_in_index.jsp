<%@page import="com.cwc.app.key.CheckInEntryPriorityKey"%>
<%@page import="com.cwc.app.key.CheckInOrderComeStatusKey"%>
<%@page import="com.cwc.app.key.WaitingTypeKey"%>
<%@page import="com.cwc.app.api.zr.LoadBarUseMgrZr"%>
<%@page import="com.cwc.app.key.OccupyTypeKey"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.OccupyStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey" %>
<%@page import="com.cwc.app.key.ModuleKey"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="OccupyStatusTypeKey" class="com.cwc.app.key.OccupyStatusTypeKey"/>
<jsp:useBean id="checkInTractorOrTrailerTypeKey" class="com.cwc.app.key.CheckInTractorOrTrailerTypeKey"/>
<jsp:useBean id="checkInLiveLoadOrDropOffKey" class="com.cwc.app.key.CheckInLiveLoadOrDropOffKey"/>
<jsp:useBean id="occupyTypeKey" class="com.cwc.app.key.OccupyTypeKey"/>
<jsp:useBean id="checkInMainDocumentsRelTypeKey" class="com.cwc.app.key.CheckInMainDocumentsRelTypeKey"/>
<jsp:useBean id="waitingTypeKey" class="com.cwc.app.key.WaitingTypeKey"/>
<jsp:useBean id="checkInOrderComeStatusKey" class="com.cwc.app.key.CheckInOrderComeStatusKey"/>
<jsp:useBean id="checkInEntryPriorityKey" class="com.cwc.app.key.CheckInEntryPriorityKey"/>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnlineServ.js"></script> 

<!-- 时间 -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 
 <link rel="stylesheet" type="text/css" href="../js/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="../js/easyui/themes/icon.css" />

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉框多选 -->
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.filter.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.filter.min.js"></script>	


<script src="<%=ConfigBean.getStringValue("systenFolder")%>/administrator/check_in/json/group.js" type="text/javascript"></script>
<style>
.ui-datepicker{font-size: 0.9em;}
.set{padding:2px;width:95%;word-break:break-all;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 0px;border: 2px solid green;} 
p{text-align:left;}
span.stateName{width:32%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:68%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
.zebraTable td { border-bottom: 0px solid #DDDDDD;padding-left: 10px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
	
}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 1px 4px;
	width: 420px; 
	height:43px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-18px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

/* 调整 refresh 位置 */
.adjustRefresh {
	background: url(../check_in/imgs/button_long_refresh_2.png) no-repeat 0 0;
	width: 130px;
	height: 42px;
}

/* legend_foot fieldset 右下角开口 */
.demoa {
	bottom: -8px;	
	display: inline-block;
	position: absolute;
	text-align: left;
	left: 27px;
	background: #FCFCFC;
	padding:0px 5px;
}
#containerType {
	bottom: -8px;	
	right: 16px;
	display: inline-block;
	text-align: right;
	position: absolute;
	background:#fff;
}
.purpose {
	top: -2px;	
	right: 30px;
	display: inline-block;
	text-align: right;
	position: absolute;
	background:#fcfcfc;
}
.loadBar{
	margin:5px 0px;
}
div.page-last:hover{
	background-position:-45px bottom;
	
}
div.page-last{
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -1px bottom;
    cursor: pointer;
    display: inline-block;
    height: 24px;
    line-height: 24px;
    text-align: center;
    width: 44px;
    margin-right:10px;
}
div.page-last-none{
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -91px bottom;
    display: inline-block;
    height: 24px;
    line-height: 24px;
    text-align: center;
    width: 44px;
    margin-right:10px;
}
div.page-frist:hover{
	background-position:-45px 47px;
	
}
div.page-frist{
	cursor:pointer;
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -1px 47px;
    color: #aaa;
    display: inline-block;
    height: 23px;
    line-height: 21px;
    width: 44px;
}
div.page-first-none{
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -91px 48px;
    display: inline-block;
    height: 24px;
    line-height: 24px;
    text-align: center;
    width: 44px;
}
div.page-next{
	cursor:pointer;
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -1px -24px;
    color: #aaa;
    cursor: pointer;
    display: inline-block;
    height: 22px;
    line-height: 21px;
    width: 24px;
}
div.page-next-none{
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -54px -24px;
    color: #aaa;
    display: inline-block;
    height: 22px;
    line-height: 21px;
    width: 24px;
}
div.page-next:hover{
	 background-position: -27px -24px;
} 
div.page-up{
	cursor:pointer;
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -1px top;
    color: #aaa;
    cursor: pointer;
    display: inline-block;
    height: 22px;
    line-height: 22px;
    width: 24px;
}
div.page-up-none{
    background-image: url("./imgs/page-first-last-btn.png");
     background-position: -53px top;
    color: #aaa;
    display: inline-block; 
    height: 22px;
    line-height: 22px;
    width: 24px;
}

div.page-up:hover{
	 background-position: -27px top;
	
}
div.page-no{
	background: none repeat scroll 0 0 white;
    border-radius: 5px;
    display: inline-block;
    height: 21px;
    line-height: 22px;
    padding: 0 7px;
    white-space:nowrap; 
    box-shadow: 1px 1px 1px 0 #aaa inset;
}
div.page-no:hover{
	background: #0076ff;
	color:#FFF;
}
</style>
<!-- 
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";

#menu {
	width: 150px;
	display: none;
	position: absolute;
	background: #ddd;
	border: solid #ccc 1px;
}

#menu ul {
	list-style: none;
	margin: 0px;
	padding: 0px;
}

#menu ul li a {
	margin: 0px;
	padding: 5px;
	text-decoration: none;
	display: inline-block;
	height: 20px;
	width: 100px;
	color: #333;
	font-size: 13px;
}

#menu ul li a:hover {
	background: #eee;
	border: solid #fff 1px;
	height: 18px;
}
</style>
 -->
<style type="text/css">
	body {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
	td {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
</style>
<%
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 		long psId = adminLoggerBean.getPs_id(); 
 		//long adgid=adminLoggerBean.getAdgid(); 
 		boolean  isadministrator = adminLoggerBean.isAdministrator();  //判断当前登录帐号是不是admin.如果是返回true

		//long mainStatus = StringUtil.getLong(request,"mainStatus");
		String mainStatus = StringUtil.getString(request,"mainStatus");
	//	System.out.println(mainStatus);
		//long tractorStatus = StringUtil.getLong(request,"tractorStatus");
		String tractorStatus = StringUtil.getString(request,"tractorStatus");
		String entryType = StringUtil.getString(request,"entryType");
		//long numberStatus = StringUtil.getLong(request,"numberStatus");
		String numberStatus = StringUtil.getString(request,"numberStatus");
		//long doorStatus = StringUtil.getLong(request,"doorStatus");
		String doorStatus = StringUtil.getString(request,"doorStatus");
		//System.out.println("mainStatus---"+mainStatus+"tractorStatus"+tractorStatus+"numberStatus---"+numberStatus+"doorStatuss---"+doorStatus);
		long loadBar = StringUtil.getLong(request,"loadBar");
		long priority = StringUtil.getLong(request,"priority");
		long ps_id = StringUtil.getLong(request,"ps_id");
		long main_id  = StringUtil.getLong(request,"main_id");
		String waitingType = StringUtil.getString(request, "waitingType");
		String comeStatus = StringUtil.getString(request, "comeStatus");
		String purposeStatus = StringUtil.getString(request, "purposeStatus");
		String customer_id = StringUtil.getString(request, "customer_id");
		
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(StringUtil.getInt(request,"p"));
		pc.setPageSize(5);
//		DBRow[] rows = checkInMgrZwb.selectAllMain(main_id,pc);
		if(ps_id==0){
			ps_id=psId;
		}
		String start_time=StringUtil.getString(request, "start_time");
		String end_time=StringUtil.getString(request, "end_time");	
		
		DBRow[] rows = checkInMgrZwb.filterCheckIn(entryType,start_time,end_time,mainStatus,tractorStatus,numberStatus,waitingType,comeStatus,purposeStatus,ps_id,loadBar,priority,pc,customer_id);
		String key = StringUtil.getString(request,"key");
		String cmd = StringUtil.getString(request,"cmd");
		int search_mode = StringUtil.getInt(request,"search_mode");
		String CLOSED = checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.CLOSE);
		String EXCEPTION = checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.EXCEPTION);
		String PARTIALLY = checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.PARTIALLY);
		String RELEASED = OccupyStatusTypeKey.getOccupyStatusValue(OccupyStatusTypeKey.RELEASED);
		if(cmd.equals("search"))
		{
			rows = checkInMgrZwb.searchCheckInByNumber(key,search_mode,pc,adminLoggerBean,request);
		}
		
		DBRow[] ps = checkInMgrZwb.findSelfStorage();
		long tabSelect=StringUtil.getLong(request, "tabSelect");
		DBRow[] customers = checkInMgrWfh.getAllCustomer();
		
		 
%>
<title>Check in</title>
<script type="text/javascript">
$(function(){
	if($("#main_id").val()==0){
		$("#main_id").val("");
	}
	     
 	$('#start_time').datetimepicker({
 		dateFormat: "mm/dd/yy"
 		
 	});    
 	$('#end_time').datetimepicker({
 		dateFormat: "mm/dd/yy"
 		
 	});    
	$("#ui-datepicker-div").css("display","none");
		
	
	 if(navigator.userAgent.indexOf('Firefox') >= 0){
		 $(".purpose").css("top","-15px");
	 }

});
function getLastOfDay(){
		var s = "";
		  var d = new Date();  
		  var c = ":";
		   s += d.getFullYear()+"-";
		   s += fixNumber(d.getMonth() + 1) + "-";
		   s += fixNumber(d.getDate()) + " ";   
		   s += fixNumber(23) + c + "59:59";
		return s;
	}
function getCurrentDay(_date){
		 
		  var s = "";
	  var d = _date || new Date();  
	  var c = ":";
	   s += d.getFullYear()+"-";
	   s += fixNumber(d.getMonth() + 1) + "-";
	   s += fixNumber(d.getDate()) + " ";   
	   s += fixNumber(0) + c + "00:00";
	return s;
}

function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
		
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
		   searchRightButton();
		}  
	}  
}
function search()
{
	var val = $("#search_key").val();
			
	if(val.trim()=="")
	{
		alert("Please enter key word for searching");
	}
	else
	{
		val = val.replace(/\'/g,'');
		var val_search = "\'"+val.toUpperCase()+"\'";
		$("#search_key").val(val_search);
		document.search_form.key.value = val_search;
		document.search_form.search_mode.value = 1;
		$("#easy_search").hide();
		$("#search_key").attr("disabled",true);
		document.search_form.submit();
	}
}

function searchRightButton()
{
	var val = $("#search_key").val();
			
	if (val=="")
	{
		alert("Please enter key word for searching");
	}
	else
	{
		val = val.replace(/\'/g,'');
		$("#search_key").val(val);
		document.search_form.key.value = val;
		document.search_form.search_mode.value = 2;
		document.search_form.submit();
	}
}

function filter(flag){
	if(flag!="refresh"){
		document.filter_form.entryType.value = $("#entryType").val();
		document.filter_form.mainStatus.value = $("#mainStatus").val();
		document.filter_form.tractorStatus.value = $("#tractorStatus").val();
		document.filter_form.numberStatus.value = $("#numberStatus").val();
		document.filter_form.ps_id.value = $("#ps").val();
		document.filter_form.start_time.value = $("#start_time").val();
		document.filter_form.end_time.value = $("#end_time").val();
		document.filter_form.loadBar.value = $("#loadBar").val();
		document.filter_form.priority.value = $("#priority").val();
		document.filter_form.waitingType.value = $("#waitingType").val();
		document.filter_form.comeStatus.value = $("#comeStatus").val();
		document.filter_form.purposeStatus.value = $("#purposeStatus").val();
		document.filter_form.customer_id.value = $("#customerId").val();
		document.filter_form.tabSelect.value = 1;
		
	}else{
		document.filter_form.tabSelect.value = 0;
	}
	
	
	document.filter_form.submit();
}
jQuery(function($){
	 
	addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInJSONAction.action",
			"merge_field","dlo_id");
});


function modOccupancyDetailsNumberStatus(main_id,detail_id,number_status,note,rl_id,rel_type,isLive,equipmentId,resources_type,resources_type_name,resources_id_name){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/DockCloseAction.action',
		data:{detail_id:detail_id,number_status:number_status,main_id:main_id,note:note,equipment_id:equipmentId,rl_id:rl_id,resources_type:resources_type},
		dataType:'json',
		type:'post',
		beforeSend:function(request){
		},
		success:function(data){
			if(data != null){
				if(data.not_close && data.not_close==1){
					
					$.artDialog({content:data.data,height:100, lock: true,opacity: 0.3,fixed: true,title:'tips'});
					return;
				}
				if(data.nofityflag==1){
					if(isLive==1){
						InYardOrLeaving(main_id,rl_id,rel_type,equipmentId,resources_type);
					}else if(isLive==2 || isLive==3){
						alert("Drop Off or Swap , Don't release door");
						if(rel_type==<%=CheckInMainDocumentsRelTypeKey.DELIVERY%>){
							$("a[main_id='"+main_id+"'][tractor_status!=5]").html('Delivery:<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.INYARD)%>');
						}else{
							$("a[main_id='"+main_id+"'][tractor_status!=5]").html('Pick Up:<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.INYARD)%>');
						}
					    
					    modOccupancyDetailsDoorStatus(main_id,rl_id,<%=CheckInMainDocumentsStatusTypeKey.INYARD %>,equipmentId,resources_type);
					}
				}else if(data.nofityflag==2){
					releaseDoor(main_id,rl_id,equipmentId,resources_type,rel_type,resources_type_name,resources_id_name);
				}
	/**			else if(data.nofityflag==4){
					pickUpSeal(main_id,rl_id,detail_id,rel_type,isLive,equipmentId);
					
				}*/
				$("#"+detail_id).remove();
				if(number_status==<%=CheckInChildDocumentsStatusTypeKey.CLOSE%>){
					$("span[attr='"+main_id+"_"+detail_id+"']").html('<%=CLOSED%>');
				}
				if(number_status==<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION%>){
					$("span[attr='"+main_id+"_"+detail_id+"']").html('<%=EXCEPTION%>');
				}
				if(number_status==<%=CheckInChildDocumentsStatusTypeKey.PARTIALLY%>){
					$("span[attr='"+main_id+"_"+detail_id+"']").html('<%=PARTIALLY%>');
				}
			}
			
		},
		error:function(serverresponse, status){
  				alert("System error!");
  		}
	});
		
}

function modOccupancyDetailsDoorStatus(main_id,door_id,is_leave,equipmentId,resources_type){
	
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/DockCloseReleaseDoorAction.action',
		data:{resources_id:door_id,main_id:main_id,is_leave:is_leave,equipment_id:equipmentId,resources_type:resources_type},
		dataType:'json',
		type:'post',
		beforeSend:function(request){
		},
		success:function(data){
			if(data.id>0){
				 if($("#"+main_id+"_"+door_id+" input[type='button']")){
					 $("#"+main_id+"_"+door_id+" input[type='button']").remove();
			     }
				 $("span[attr='doorsta_"+main_id+"_"+door_id+"']").html('<%=RELEASED%>');
			}
			document.location.reload();
			
		},
		error:function(){
			alert("System error"); 
		}
	});
}
 
function closeNum(main_id,detail_id,number_status,door_id,rel_type,isLive,equipmentId,resources_type,number_type,resources_type_name,resources_id_name){
	var param = { 
			main_id:main_id,
			detail_id:detail_id,
			number_status:number_status,
			door_id:door_id,
			rel_type:rel_type,
			isLive:isLive,
			equipmentId:equipmentId,
			resources_type:resources_type,
			title:'Do you want to close it?',
			number_type:number_type
	};
	param = $.param(param);
	var height = '300px';
	var width = '400px';
	if(rel_type==1||(number_type==<%=moduleKey.CHECK_IN_BOL%>||number_type==<%=moduleKey.CHECK_IN_CTN%>||number_type==<%=moduleKey.CHECK_IN_DELIVERY_ORTHERS%>)){
		height = '150px' 
		width = '250px'
	}
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_moreMenu.html?'+param;
	 $.artDialog.open(url, {
		 title: "Close",width:width,height:height, lock: true,opacity: 0.3,fixed: true, 
		 	okVal:'Yes',
		 	ok: function () {
		 		var pickUpSeal =  $.artDialog.data('pickUpSeal')==undefined?'':$.artDialog.data('pickUpSeal');
		 		var deliverySeal =  $.artDialog.data('deliverySeal')==undefined?'':$.artDialog.data('deliverySeal');
		 		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
		 		$.ajax({
		 			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxSaveSeal.action',
		 			type:'post',
		 			dataType:'json',
		 			data:{pick_up_seal:pickUpSeal,delivery_seal:deliverySeal,equipment_id:equipmentId},
		 			success:function(data){
		 				$.unblockUI();       //遮罩关闭
						modOccupancyDetailsNumberStatus(main_id,detail_id,number_status,'',door_id,rel_type,isLive,equipmentId,resources_type,resources_type_name,resources_id_name);
		 			},
		 			error:function(){
		 				alert('System error');
		 				$.unblockUI();       //遮罩关闭
		 			}
		 			
		 		});
		    },
		    cancelVal: 'No',
		    cancel: function(){
			}
	 });
	
	
} 
function exception(main_id,detail_id,number_status,door_id,rel_type,isLive,equipmentId,resources_type,number_type,resources_type_name,resources_id_name){
	var param = {
			main_id:main_id,
			detail_id:detail_id,
			number_status:number_status,
			door_id:door_id,
			rel_type:rel_type,
			isLive:isLive,
			equipmentId:equipmentId,
			resources_type:resources_type,
			title:'Do you want to submit Exception?',
			flag:1,
			number_type:number_type,
			more_type:2
	};
	param = $.param(param);
	var height = '120px';
	var width = '350px';

	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_moreMenu.html?'+param;
	 $.artDialog.open(url, {
		 title: "Exception",width:width,height:height, lock: true,opacity: 0.3,fixed: true,
		 	okVal:'Yes',
		 	ok: function () {
		    	var note = $.artDialog.data('note');
				modOccupancyDetailsNumberStatus(main_id,detail_id,number_status,note,door_id,rel_type,isLive,equipmentId,resources_type,resources_type_name,resources_id_name);
		    },
		    cancelVal: 'No',
		    cancel: function(){
		    	
			}
	 });
	 
	
	//	
}
function partially(main_id,detail_id,number_status,door_id,rel_type,isLive,equipmentId,resources_type,number_type,resources_type_name,resources_id_name){

	 $.artDialog({
		 	content:'<b>Do you want to submit Partially？</b>',
		 	title: "Partially",width:'265px', lock: true,opacity: 0.3,fixed: true,
			okVal:'Yes',
		 	ok: function () {
				modOccupancyDetailsNumberStatus(main_id,detail_id,number_status,'',door_id,rel_type,isLive,equipmentId,resources_type,resources_type_name,resources_id_name);
		    },
		    cancelVal: 'No',
		    cancel: function(){
		    	
			}
	 });	
	
}
function showMoreCloseTask(entry_id,show_type,equipment_id,dlo_id){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_show_task.html?entry_id='+entry_id+'&show_type='+show_type+'&equipment_id='+equipment_id+'&dlo_id='+dlo_id;
	 $.artDialog.open(url, {
		 title: "Closed Task",width:500,height:300, lock: true,opacity: 0.3,fixed: true, 
	 });
}
function releaseDoor(main_id,door_id,equipmentId,resources_type,rel_type,resources_type_name,resources_id_name){

	var para = $("#entry_"+main_id).find("#equipment_"+equipmentId).attr("para")
	var paras = para.split("|");
	resources_id_name = paras[3];
	resources_type_name = paras[1];
	if(resources_id_name!=''){
		resources_id_name = ': '+resources_id_name;
	}
	 $.artDialog({
		 content:'<b>Do you want to release the '+resources_type_name+resources_id_name+'</b>',
		 title: "Release Door",width:'300px', lock: true,opacity: 0.3,fixed: true,
		 	okVal:'Yes',
		 	ok: function () {
		 		modOccupancyDetailsDoorStatus(main_id,door_id,"",equipmentId,resources_type);
		    },
		    cancelVal: 'No',
		    cancel: function(){
		    	
			}
	 });	
}
function releaseSpace(main_id,door_id,equipmentId,resources_type,rel_type,resources_type_name,resources_id_name){
	
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/ajaxGetTaskCount.action',
		data:{main_id:main_id,equipment_id:equipmentId},
		dataType:'json',
		type:'post',
		success:function(data){
			if(data.count==0){
				releaseDoor(main_id,door_id,equipmentId,resources_type,rel_type,resources_type_name,resources_id_name);	
			}else{
				alert("There needs to close the tasks!");
			}
		},
		error:function(){
			alert("System error");
		}
	});
}



function InYardOrLeaving(main_id,door_id,rel_type,equipmentId,resources_type){

	$.artDialog({
	    content: 'Truck leave or stay',
	    icon: 'question',
	    lock:true,
	    width: 180,
	    height: 70,
	    title:'Reminder',
	    okVal: 'Stay',
	    ok: function () {
		    if(rel_type==<%=CheckInMainDocumentsRelTypeKey.DELIVERY%>){
				$("a[main_id='"+main_id+"'][tractor_status!=5]").html('Delivery:<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.INYARD)%>');
			}else{
				$("a[main_id='"+main_id+"'][tractor_status!=5]").html('Pick Up:<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.INYARD)%>');
			}
		    
		    modOccupancyDetailsDoorStatus(main_id,door_id,<%=CheckInMainDocumentsStatusTypeKey.INYARD %>,equipmentId,resources_type);
    	    $('#'+main_id+'_'+door_id).html("<input  type='button' class='short-button' value='Release' onclick='releaseDoor("+main_id+","+door_id+","+equipmentId+","+resources_type+","+rel_type+","+resouces_type_name+","+resources_id_name+")'/>");
			
	    },
	    cancelVal: 'Leave',
	    cancel: function(){
	    	if(rel_type==<%=CheckInMainDocumentsRelTypeKey.DELIVERY%>){
				$("a[main_id='"+main_id+"'][tractor_status!=5]").html('Delivery:<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.LEAVING)%>');
			}else{
				$("a[main_id='"+main_id+"'][tractor_status!=5]").html('Pick Up:<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.LEAVING)%>');
			}
	    	modOccupancyDetailsDoorStatus(main_id,door_id,<%=CheckInMainDocumentsStatusTypeKey.LEAVING%>,equipmentId,resources_type);
		}
	});
		

}
function pickUpSeal(main_id,door_id,detail_id,rel_type,isLive,equipmentId){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_pickup_seal.html?main_id='+main_id+'&door_id='+door_id+'&detail_id='+detail_id+'&rel_type='+rel_type+'&isLive='+isLive; 
	$.artDialog.open(uri , {title: "PickUp Seal",width:'400px',height:'100px', lock: true,opacity: 0.3,fixed: true});
}
function  pickUpSealFlag(main_id,door_id,notifyFlag,rel_type,isLive,equipmentId){
	if(notifyFlag==2){
		releaseDoor(main_id,door_id)
	}else if(notifyFlag==1){
		if(isLive==1){
			InYardOrLeaving(main_id,door_id,rel_type,equipmentId);
		}else if(isLive==2 || isLive==3){
			alert("Drop Off or Swap , Don't release door");
			if(rel_type==<%=CheckInMainDocumentsRelTypeKey.DELIVERY%>){
				$("a[main_id='"+main_id+"'][tractor_status!=5]").html('Delivery:<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.INYARD)%>');
			}else{
				$("a[main_id='"+main_id+"'][tractor_status!=5]").html('Pick Up:<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.INYARD)%>');
			}
		    
		    modOccupancyDetailsDoorStatus(main_id,door_id,<%=CheckInMainDocumentsStatusTypeKey.INYARD %>,equipmentId);
		}
	}
	
}
//document.onclick=function(){//左击清除弹出菜单   
//    var menu=document.getElementById("menu");   
//    menu.style.display="none";   
//} 

$(document).ready(function(){
	$("#storage_camera").bind("contextmenu",function(e){
		return false;
	})
});

function showCam(){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/camera_list.html';
	 $.artDialog.open(url, {title: "Webcam List",width:'400px',height:'300px', lock: false,opacity: 0.3,fixed: true});
}
function showPlates(id)
{
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInCheckEntryPlateAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:'entry_id='+id,
		cache:false,
		success: function(data){
			$.unblockUI();       //遮罩关闭
			if(data && data.total > 0)
			{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_view_container_plates.html?id='+id; 
				$.artDialog.open(uri , {title: "Plates",width:'850px',height:'540px', lock: true,opacity: 0.3,fixed: true});
			}
			else
			{
				var artMessage = artDialog({
					title:"message",
				    content: 'no plates received or shipped'
				});
				artMessage.show();
				setTimeout(function () {
					artMessage.close();
				}, 1500);
			}
		},
		error:function(){
			$.unblockUI();       //遮罩关闭
		}
	});
	
}
function showUsers(el){
	if($(el).find("#users").length>0){
		$(el).find("#user").css("display","none");
		$(el).find("#users").css("display","inline-block");
	}
}
function hiddenUsers(el){
	$(el).find("#user").css("display","inline-block");
	$(el).find("#users").css("display","none");
}

</script>
</head>
<body onLoad="onLoadInitZebraTable()">
<!-- <input id="adadasdasd" value="-1"><button id="sssssssssssssssssssss">打开</button>
 -->
<script type="text/javascript">
$("#adadasdasd").click(function(){    //绑定通知文本框
	 var id=this.id;

	 var ids=$('#'+id+'Id').val();
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/check_in/check_in_admin_user.html";
	 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:"1000053", //需要回显的UserId
			 not_check_user:"1000055",					//某些人不 会被选中的
			 proJsId:"5",						// -1全 部,1普通员工,5副主管,10主管,15主管+副主管
			 ps_id:'1000005',					//所属仓库
			 id:id,					
			 handle_method:'setSelectAdminUsers',
			 group :groupJson.Warehouse	
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: 'User List',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
});

</script>
	<div class="demo" style="width:100%">
	  <div id="tabs">
		<ul>
			<li><a href="#checkin_search">Common Tools</a></li>
			<li><a href="#checkin_filter">Advanced Search</a></li>
			<li><a href="#report">Report</a></li>
		</ul>
		<div id="checkin_search">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search_2.png" width="70" height="60" border="0"/></a></div>
						</div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td >
									<div  class="search_shadow_bg">
									 <input name="search_key" type="text" class="search_input" style="font-size:17px;font-family:Arial;color:#333333;height:30px;" id="search_key" onkeydown="if(event.keyCode==13)search()" value="<%=key %>"/>
									</div>
								</td>
								<td width="67" align="center">
									<input type="button" class="button_long_refresh adjustRefresh" onclick="filter('refresh');"/>
								</td>
								<td width="67" align="center">
								   <tst:authentication bindAction="com.cwc.app.api.zwb.CheckInMgrZwb.gateCheckInAdd">
								   <img width="148" border="0" height="45" src="../check_in/imgs/gateCheckIn.png" onclick="gateCheckIn()">
								   </tst:authentication>
								</td>
								<td width="3px"></td>
<!-- 								<td width="67" align="center"><img width="129" border="0" height="51" src="imgs/wait_check_in_bg.jpg" onclick="waiting()"></td> -->
								<td width="3px"></td>
							    <td width="67" align="center">
							    	<img width="148" border="0" height="45" src="../check_in/imgs/camera.png" onclick="showCam()">
							</tr>
						</table>
						<script>eso_button_even();</script>
				   </td>
	            </tr>
	          </table>
	      </div>
	      <div id="checkin_filter">
	      		<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left">
						<div style="margin-bottom: 2px;">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="252px;">
										Start Time :&nbsp;<input maxlength="100" value="<%=start_time %>" id="start_time" />
									</td>
									<td width="240px">
										End Time:&nbsp;<input maxlength="100" value="<%=end_time %>" id="end_time"  />&nbsp; &nbsp; &nbsp; 
									</td>
									<td  width="168px;">
										WHSE:&nbsp;<select id="ps" name="ps" <%=adminLoggerBean.isAdministrator() ?"":"disabled"%>>
												<option value="-1">All Storage</option>
												<%
													for(int j = 0;j<ps.length;j++)
													{
												%>
													<option value="<%=ps[j].get("ps_id",0l)%>"  <%=ps_id==ps[j].get("ps_id",0l)?"selected":"" %>><%=ps[j].getString("ps_name")%></option>
												<%
													}
												%>
											</select>&nbsp; &nbsp; 
									</td>
									<td width="150px">
									   Priority:
								    		<select id="priority">
												<option value="0">NA</option>
	                                            <option <%= priority==checkInEntryPriorityKey.LOW?"selected":""%> value="<%=checkInEntryPriorityKey.LOW%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.LOW)%></option>
	                                            <option <%= priority==checkInEntryPriorityKey.MIDDLE?"selected":""%> value="<%=checkInEntryPriorityKey.MIDDLE%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.MIDDLE)%></option>
	                                            <option <%= priority==checkInEntryPriorityKey.HIGH?"selected":""%> value="<%=checkInEntryPriorityKey.HIGH%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.HIGH)%></option>
								    		</select>
									</td>
									
									
								</tr>
								<tr height="5px;">
								</tr>
								<tr>
									
								</tr>
							</table>
						</div>
						<div style="float:left;margin-top: 5px;">
						<select id="entryType" style="width:150px" multiple="multiple" name="entryType">
							<option value="<%=CheckInMainDocumentsRelTypeKey.DELIVERY %>" <%=entryType.contains(CheckInMainDocumentsRelTypeKey.DELIVERY+"")?"selected":"" %>><%=checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.DELIVERY)%></option>
							<option value="<%=CheckInMainDocumentsRelTypeKey.PICK_UP %>" <%=entryType.contains(CheckInMainDocumentsRelTypeKey.PICK_UP+"")?"selected":"" %>><%=checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.PICK_UP)%></option>
							<option value="<%=CheckInMainDocumentsRelTypeKey.BOTH %>" <%=entryType.contains(CheckInMainDocumentsRelTypeKey.BOTH+"")?"selected":"" %>><%=checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.BOTH)%></option>
							<option value="<%=CheckInMainDocumentsRelTypeKey.NONE %>" <%=entryType.contains(CheckInMainDocumentsRelTypeKey.NONE+"")?"selected":"" %>><%=checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.NONE)%></option>	
    						<option value="<%=CheckInMainDocumentsRelTypeKey.VISITOR %>" <%=entryType.contains(CheckInMainDocumentsRelTypeKey.VISITOR+"")?"selected":"" %>><%=checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.VISITOR)%></option>							
							<option value="<%=CheckInMainDocumentsRelTypeKey.PATROL %>" <%=entryType.contains(CheckInMainDocumentsRelTypeKey.PATROL+"")?"selected":"" %>><%=checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.PATROL)%></option>	
							<option value="<%=CheckInMainDocumentsRelTypeKey.SMALL_PARCEL %>" <%=entryType.contains(CheckInMainDocumentsRelTypeKey.SMALL_PARCEL+"")?"selected":"" %>><%=checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.SMALL_PARCEL)%></option>							
													
						</select>
						<select id="tractorStatus" style="width:100px"  multiple="multiple"  name="tractorStatus">
							<option value="<%=CheckInMainDocumentsStatusTypeKey.UNPROCESS %>" <%=tractorStatus.contains(CheckInMainDocumentsStatusTypeKey.UNPROCESS+"")?"selected":"" %>><%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.UNPROCESS)%></option>
							<option value="<%=CheckInMainDocumentsStatusTypeKey.PROCESSING %>" <%=tractorStatus.contains(CheckInMainDocumentsStatusTypeKey.PROCESSING+"")?"selected":"" %>><%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.PROCESSING)%></option>
							<option value="<%=CheckInMainDocumentsStatusTypeKey.INYARD %>" <%=tractorStatus.contains(CheckInMainDocumentsStatusTypeKey.INYARD+"")?"selected":"" %>><%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.INYARD)%></option>							
							<option value="<%=CheckInMainDocumentsStatusTypeKey.LEAVING %>" <%=tractorStatus.contains(CheckInMainDocumentsStatusTypeKey.LEAVING+"")?"selected":"" %>><%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.LEAVING)%></option>						
							<option value="<%=CheckInMainDocumentsStatusTypeKey.LEFT %>" <%=tractorStatus.contains(CheckInMainDocumentsStatusTypeKey.LEFT+"")?"selected":"" %>><%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.LEFT)%></option>	

						</select>
						<select id="mainStatus" style="width:100px" multiple="multiple" name="mainStatus">
							<option value="<%=CheckInMainDocumentsStatusTypeKey.UNPROCESS %>" <%=mainStatus.contains(CheckInMainDocumentsStatusTypeKey.UNPROCESS+"")?"selected":"" %>><%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.UNPROCESS)%></option>
							<option value="<%=CheckInMainDocumentsStatusTypeKey.PROCESSING %>" <%=mainStatus.contains(CheckInMainDocumentsStatusTypeKey.PROCESSING+"")?"selected":"" %>><%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.PROCESSING)%></option>
							<option value="<%=CheckInMainDocumentsStatusTypeKey.INYARD %>" <%=mainStatus.contains(CheckInMainDocumentsStatusTypeKey.INYARD+"")?"selected":"" %>><%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.INYARD)%></option>							
							<option value="<%=CheckInMainDocumentsStatusTypeKey.LEAVING %>" <%=mainStatus.contains(CheckInMainDocumentsStatusTypeKey.LEAVING+"")?"selected":"" %>><%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.LEAVING)%></option>						
							<option value="<%=CheckInMainDocumentsStatusTypeKey.LEFT %>" <%=mainStatus.contains(CheckInMainDocumentsStatusTypeKey.LEFT+"")?"selected":"" %>><%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInMainDocumentsStatusTypeKey.LEFT)%></option>

						</select>
						<select id="numberStatus" style="width:100px" multiple="multiple"  name="numberStatus">
							<option value="<%=CheckInChildDocumentsStatusTypeKey.UNPROCESS %>" <%=numberStatus.contains(CheckInChildDocumentsStatusTypeKey.UNPROCESS+"")?"selected":"" %>><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.UNPROCESS)%></option>
							<option value="<%=CheckInChildDocumentsStatusTypeKey.PROCESSING %>" <%=numberStatus.contains(CheckInChildDocumentsStatusTypeKey.PROCESSING+"")?"selected":"" %>><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.PROCESSING)%></option>
							<option value="<%=CheckInChildDocumentsStatusTypeKey.CLOSE %>" <%=numberStatus.contains(CheckInChildDocumentsStatusTypeKey.CLOSE+"")?"selected":"" %>><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.CLOSE)%></option>
							<option value="<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION %>" <%=numberStatus.contains(CheckInChildDocumentsStatusTypeKey.EXCEPTION+"")?"selected":"" %>><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.EXCEPTION)%></option>
						</select>
						Customer ID:
						<select id="customerId" style="width:100px"   name="numberStatus">
							<option value="">ALL</option> 
							<%
								for(DBRow customer:customers){
									out.print("<option value=\""+customer.get("customer_id", "")+"\"");
									if(customer_id.contains(customer.get("customer_id", ""))){
										out.print("selected");
									}
									out.print(">"+customer.get("customer_id", "")+"</option>");
								}
							%>
						</select>
						<br><br>
						<select id="waitingType" style="width:180px" multiple="multiple"  name="waitingType">
							<option value="<%=WaitingTypeKey.NotWaiting %>" <%=waitingType.contains(WaitingTypeKey.NotWaiting+"")?"selected":"" %>><%=waitingTypeKey.getWaitingTypeKeyValue(WaitingTypeKey.NotWaiting)%></option>
							<option value="<%=WaitingTypeKey.EarlyWaiting %>" <%=waitingType.contains(WaitingTypeKey.EarlyWaiting+"")?"selected":"" %>><%=waitingTypeKey.getWaitingTypeKeyValue(WaitingTypeKey.EarlyWaiting)%></option>
							<option value="<%=WaitingTypeKey.LateWaiting%>" <%=waitingType.contains(WaitingTypeKey.LateWaiting+"")?"selected":"" %>><%=waitingTypeKey.getWaitingTypeKeyValue(WaitingTypeKey.LateWaiting)%></option>
							<option value="<%=WaitingTypeKey.WarehouseWaitng %>" <%=waitingType.contains(WaitingTypeKey.WarehouseWaitng+"")?"selected":"" %>><%=waitingTypeKey.getWaitingTypeKeyValue(WaitingTypeKey.WarehouseWaitng)%></option>
							<option value="<%=WaitingTypeKey.NoAppointment %>" <%=waitingType.contains(WaitingTypeKey.NoAppointment+"")?"selected":"" %>><%=waitingTypeKey.getWaitingTypeKeyValue(WaitingTypeKey.NoAppointment)%></option>
						</select>
						<select id="comeStatus" style="width:150px" multiple="multiple"  name="comeStatus">
							<option value="<%=CheckInOrderComeStatusKey.NoAppointment %>" <%=comeStatus.contains(CheckInOrderComeStatusKey.NoAppointment+"")?"selected":"" %>><%=checkInOrderComeStatusKey.geCheckInOrderComeStatusKeyValue(checkInOrderComeStatusKey.NoAppointment)%></option>
							<option value="<%=CheckInOrderComeStatusKey.OnTime %>" <%=comeStatus.contains(CheckInOrderComeStatusKey.OnTime+"")?"selected":"" %>><%=checkInOrderComeStatusKey.geCheckInOrderComeStatusKeyValue(checkInOrderComeStatusKey.OnTime)%></option>
							<option value="<%=CheckInOrderComeStatusKey.TooEarly %>" <%=comeStatus.contains(CheckInOrderComeStatusKey.TooEarly+"")?"selected":"" %>><%=checkInOrderComeStatusKey.geCheckInOrderComeStatusKeyValue(checkInOrderComeStatusKey.TooEarly)%></option>
							<option value="<%=CheckInOrderComeStatusKey.TooLate %>" <%=comeStatus.contains(CheckInOrderComeStatusKey.TooLate+"")?"selected":"" %>><%=checkInOrderComeStatusKey.geCheckInOrderComeStatusKeyValue(checkInOrderComeStatusKey.TooLate)%></option>
						</select>
						<select id="purposeStatus" style="width:150px" multiple="multiple"  name="purposeStatus">
							<option value="<%=checkInLiveLoadOrDropOffKey.LIVE %>" <%=purposeStatus.contains(checkInLiveLoadOrDropOffKey.LIVE+"")?"selected":"" %>><%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(checkInLiveLoadOrDropOffKey.LIVE)%></option>
							<option value="<%=checkInLiveLoadOrDropOffKey.DROP %>" <%=purposeStatus.contains(checkInLiveLoadOrDropOffKey.DROP+"")?"selected":"" %>><%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(checkInLiveLoadOrDropOffKey.DROP)%></option>
							<option value="<%=checkInLiveLoadOrDropOffKey.PICK_UP %>" <%=purposeStatus.contains(checkInLiveLoadOrDropOffKey.PICK_UP+"")?"selected":"" %>><%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(checkInLiveLoadOrDropOffKey.PICK_UP)%></option>
						</select>
						 </div>
						<script type="text/javascript">
							$("#entryType").multiselect({
						        noneSelectedText: "Entry Type",
						        checkAllText: "select all",
						        uncheckAllText: 'unselect all',
						        selectedList:1,
						        minWidth:150,
						        height:100
						    });
						 	$("#mainStatus").multiselect({
						        noneSelectedText: "Trailer Status",
						        checkAllText: "select all",
						        uncheckAllText: 'unselect all',
						        selectedList:1,
						        minWidth:113,
						        height:150
						    });
						    $("#tractorStatus").multiselect({
						        noneSelectedText: "Tractor Status",
						        checkAllText: "select all",
						        uncheckAllText: 'unselect all',
						        selectedList:1,
						        minWidth:113,
						        height:150
						    });
						    $("#numberStatus").multiselect({
						        noneSelectedText: "Order Status",
						        checkAllText: "select all",
						        uncheckAllText: 'unselect all',
						        selectedList:1,
						         minWidth:113,
						         height:120
						    });
						    $("#waitingType").multiselect({
						        noneSelectedText: "Waiting Type",
						        checkAllText: "select all",
						        uncheckAllText: 'unselect all',
						        selectedList:1,
						         minWidth:113,
						         height:120
						    });
						    $("#comeStatus").multiselect({
						        noneSelectedText: "Come Status",
						        checkAllText: "select all",
						        uncheckAllText: 'unselect all',
						        selectedList:1,
						         minWidth:113,
						         height:120
						    });
						    $("#purposeStatus").multiselect({
						        noneSelectedText: "Equipment Purpose",
						        checkAllText: "select all",
						        uncheckAllText: 'unselect all',
						        selectedList:1,
						         minWidth:113,
						         height:120
						    });
						</script>
						
					 
					    
					<div style="    display: inline-block;margin-left: 20px;padding-top: 47px;">
						<input type="button" class="button_long_search" value="Search" onclick="filter();"/>
						
						 <!-- <input type="button" class="long-button-export" value="Export" onclick="down()"/> -->
					</div> 
					</td>
					<td width="67" align="center">
					   <tst:authentication bindAction="com.cwc.app.api.zwb.CheckInMgrZwb.gateCheckInAdd">								 
						 <img width="148" border="0" height="45" src="../check_in/imgs/gateCheckIn.png" onclick="gateCheckIn()">
					   </tst:authentication>
				    </td>
				    <td width="3px"></td>
<!-- 				    <td width="67" align="center">						  -->
<!-- 						 <img width="129" border="0" height="51" src="imgs/wait_check_in_bg.jpg" onclick="waiting()"> -->
<!-- 				    </td> -->
				    
				</tr>
			</table>
	      </div>
       <div id="report">
       <div style="margin-left:30px;font-weight: bold;">Reports:</div>
       		<table style="margin-left:50px">
       			<tr>
       				<td width="400px" ><a style="color:blue;" href="../check_in/report/EQUIPMENT REPORT.xlsm">EQUIPMENT REPORT.xlsm</a></td>
       				<td width="300px"><a style="color:blue;" href="../check_in/report/GATE ACTIVTY REPORT.xlsm">GATE ACTIVITY REPORT.xlsm</a></td>
       				<td ><a style="color:blue;" href="../check_in/report/LOAD BAR REPORT.xlsm">LOAD BAR REPORT.xlsm</a></td>
       			</tr>
       				
       			<tr>
       				<td ><a style="color:blue;" href="../check_in/report/SCORE CARD OUTBOUND OR INBOUND REPORT.xlsm">SCORE CARD OUTBOUND OR INBOUND REPORT.xlsm</a></td>
       				<td ><a style="color:blue;" href="../check_in/report/TASK REPORT.xlsm">TASK REPORT.xlsm</a></td>
       				<td ><a style="color:blue;" href="../check_in/report/FORECAST REPORT.xlsm">FORECAST REPORT.xlsm</a></td>
       			</tr> 
       			<tr>
       				<td><a style="color:blue;" href="../check_in/report/PALLET TYPE REPORT.xlsm">PALLET TYPE REPORT.xlsm</a></td>
       				<td><a style="color:blue;" href="../check_in/report/CTNR IN YARD REPORT.xlsm">CTNR IN YARD REPORT.xlsm</a></td>
       				<td></td>
       			</tr>
       		</table>
       </div>
       </div>
       <script>
	       $("#tabs").tabs({
		   		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
		   });
	       $("#tabs").tabs("select",<%=tabSelect%>);
       </script>
    </div>
    <br/>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
    	    
    	    <th width="25%" class="left-title" style="vertical-align:center;text-align:center;">Timestamp</th>
	        <th width="22%" class="left-title" style="vertical-align:center;text-align:center;">Tractor</th>
	        <th width="33%" class="left-title" style="vertical-align:center;text-align:center;">Task</th>
	        <th  class="left-title" style="vertical-align: center;text-align: center;">Log</th>
	       
		</tr>
		<%if(rows != null && rows.length > 0){
      		for(DBRow row : rows){
      			
      			
      		//====查找车头车尾的数据  wfh ==== 
					long entry_id = row.get("dlo_id", 0L);
					long showTimePsId = row.get("ps_id", 0L);
					DBRow[] entry_tractor =  checkInMgrWfh.findTractorByEntryId(entry_id,1);//type == 1-车头 
					DBRow[] entry_trailer = checkInMgrWfh.findTractorByEntryId(entry_id,2);//type == 2-车尾
					//DBRow[] equipment = checkInMgrWfh.findTractorByEntryId(entry_id,0);
					if(entry_tractor==null || entry_tractor.length==0){
						entry_tractor = new DBRow[1];
						entry_tractor[0] = new DBRow();
					}
					//得到设备下task的数量 车头有任务显示在task列
					int count = checkInMgrWfh.findTaskCountByEquipmentId(entry_id,entry_tractor[0].get("equipment_id", 0));
					if(count>0){
						List list = new ArrayList();
						if(entry_trailer!=null&&entry_trailer.length>0){
							for(int i=0;i<entry_trailer.length;i++){
								list.add(i, entry_trailer[i]);
							}
						}
						list.add(list.size(), entry_tractor[0]);
						entry_trailer = (DBRow[])list.toArray(new DBRow[0]);
					}
					
				
					//处理 显示带走的货柜
				//查找这条entry带走的equipment（车尾
				DBRow[] checkOutEntry = checkInMgrWfh.findCheckOutIdByEntryId(entry_id);
				if(checkOutEntry!=null&&checkOutEntry.length>0){
					List list = new ArrayList();
					for(int i=0;i<entry_trailer.length;i++){
						list.add(i, entry_trailer[i]);
					}
					int size = list.size();
					int addSize = 0;
					for(int i=0;i<checkOutEntry.length;i++){
						long checkOutEntryId = checkOutEntry[i].get("check_out_entry_id", 0L);
						long checkInEntryId = checkOutEntry[i].get("check_in_entry_id", 0L);
						if(checkOutEntryId==checkInEntryId){
							continue;
						}
						checkOutEntry[i].add("isCheckOutTrailer", 1);
						list.add(size+addSize, checkOutEntry[i]);
						addSize+=1;
					}
					
					entry_trailer = (DBRow[])list.toArray(new DBRow[0]);
				}
      			
				//==== end ====
      	%>
      	<tr height="40px" id="entry_<%=entry_id%>">
      		<td align="center" valign="middle"> 
    		
 			<!-- 修改通知时间显示位置 -->
 			
      		<fieldset align="left" style="border:2px solid blue;width: 393px;border-radius: 5px;margin-top: 15px;margin-bottom: 5px;position: relative;padding:4px 0px;">
  					<legend style="font-size:11px;font-weight:bold;">
  					E<%=row.getString("dlo_id") %>
  					<%
      				int trailerRelType =0;
					if(entry_trailer!=null&&entry_trailer.length>0){
						trailerRelType = entry_trailer[0].get("rel_type", 0);
					}
      				if(StrUtil.isBlank(entry_tractor[0].get("equipment_number","")) && entry_trailer.length == 0 ){
 						out.println(" | Pre-CheckIn");	
 					}
      			
      			
      				%>
  					</legend>
  					<table cellspacing="0" cellpadding="0" border="0" id="containerType">
      				<tr>
      					<td style="padding:0px 5px;font-weight: bold;">
			      			<%=row.get("waiting", 0)>0?waitingTypeKey.getWaitingTypeKeyValue(row.get("waiting", 0)):"" %> 
      					</td>
      				</tr>
      				</table>
      				
      				<table cellspacing="0" cellpadding="0" border="0" class="purpose">
      				<tr>
      					<td style="padding:0px 5px;font-weight: bold;">
			      			<%
								DBRow wareshouse = checkInMgrWfh.findPSNameByPsId(row.get("ps_id", 0L));
			      				out.print(wareshouse.get("title", ""));
							%> 
      					</td>
      				</tr>
      				</table>
      				
  					<%	
		/**      			 String appointment_time = row.getString("appointment_time");
  						if(!StringUtil.isBlank(row.getString("appointment_time")))
		      			 	 appointment_time =  DateUtil.showLocalparseDateTo24Hours(appointment_time,psId);*/
  						
  						String gate_check_in_operate_time = row.getString("gate_check_in_time");
  						if(!StringUtil.isBlank(row.getString("gate_check_in_time")))
  							 gate_check_in_operate_time = DateUtil.showLocalparseDateTo24Hours(gate_check_in_operate_time,showTimePsId);

  						String window_check_in_operate_time = row.getString("window_check_in_time");
  						if(!StringUtil.isBlank(row.getString("window_check_in_time")))
  							 window_check_in_operate_time = DateUtil.showLocalparseDateTo24Hours(window_check_in_operate_time,showTimePsId);
  						
  						String warehouse_check_in_operate_time =row.getString("warehouse_check_in_time");
  						if(!StringUtil.isBlank(row.getString("warehouse_check_in_time")))
  							 warehouse_check_in_operate_time = DateUtil.showLocalparseDateTo24Hours(warehouse_check_in_operate_time,showTimePsId);
  						
  						String check_out_time = row.getString("check_out_time");
  						if(!StringUtil.isBlank(row.getString("check_out_time")))
  							 check_out_time = DateUtil.showLocalparseDateTo24Hours(check_out_time,showTimePsId);
  						
      				%>
      				
  					<%if(!gate_check_in_operate_time.equals("")){
  						DBRow[] gate = checkInMgrWfh.findScheduleByMainId(entry_id, 56);//查询gate的通知
  						List<DBRow> gateSchedule = new ArrayList<DBRow>();
  						for(DBRow g :gate){
  							gateSchedule.add(g);
  						}
  						DBRow[] noticedResult = gateSchedule.toArray(new DBRow[0]);
  						if(noticedResult!=null && noticedResult.length>0){
  					%>
  					<div style="text-align:left;clear:both;width:393px">
  					
  							<div  align="" style="padding-left:73px">
  								 <span style="font-size:10px;">Gate Check In:</span>
		  						 <span style="font-size:10px;font-weight:bold;">
		  						 	<%out.print(gate_check_in_operate_time);%> 
		  						 </span>
							</div>
							<table width="" border="0"cellspacing="0" cellpadding="0">
  							<%
	  						 	for(int i=0;i<noticedResult.length;i++){				 	
					 				if(i<2){
					 					int numberType = noticedResult[i].get("number_type", 0);
					 					DBRow[] users = (DBRow[])noticedResult[i].get("scheduleUser", new DBRow[0]);
					 	     %>
					 	     			<tr >
						 	     			<%if(numberType==moduleKey.CHECK_IN_LOAD||numberType==moduleKey.CHECK_IN_PONO||numberType==moduleKey.CHECK_IN_ORDER||numberType==moduleKey.CHECK_IN_PICKUP_ORTHERS|| numberType == moduleKey.SMALL_PARCEL){ %>
										   	<td style="color:blue;padding:0px;font-size:10px;" align="right" width="64px;"><%=moduleKey.getModuleName(noticedResult[i].get("number_type", "")) %>:</td>
										   	<%}else if(numberType==moduleKey.CHECK_IN_BOL||numberType==moduleKey.CHECK_IN_CTN||numberType==moduleKey.CHECK_IN_DELIVERY_ORTHERS){ %>
										   	<td style="color:red;padding:0px;font-size:10px;" align="right" width="64px;"><%=moduleKey.getModuleName(noticedResult[i].get("number_type", "")) %>:</td>
										   	<%}else{ %>
										  	<td align="right" style="padding:0px;" width="64px;"></td>
										  	<%} %>
					 	     				<td style="padding:0px;width:92px" height="14px">&nbsp;<a style="color:blue;font-size:10px;" onclick="detailNotices(<%=noticedResult[i].get("schedule_id", 0) %>)" href="javascript:void(0)"><%=!StrUtil.isBlank(noticedResult[i].get("number", ""))? noticedResult[i].get("number", ""):"No Task"%></a></td>
					 	     				<td>
					 	     					<%
												int noticesCount = users.length;
												noticesCount = noticesCount>1?1:noticesCount;
												for(int noticesIndex=0;noticesIndex<noticesCount;noticesIndex++){
												%>
													Notified &nbsp;&nbsp;<%=users[noticesIndex].get("schedule_execute_name", "") %>
													
												<%}%>
												<%=users.length>1?"...":"" %>
					 	     				</td>
					 	     			</tr> 		
						   	<%	}else{
							  			break;
									}
					 		   }
							 			%>
							</table>
							<%	
	  							if(noticedResult.length>2){ 
							%>
	 						 				<div style="text-align:right;clear:both;">
				  								<span><a href="javascript:void(0)" onclick='moreNotices(<%=row.get("dlo_id",0) %>,56)' style="color:blue">More...</a></span>
				  							</div>	
						 	<%
						 				}
							%>
						</div>
							<%
	  						 	}else{
	  			  			%>
	  			  				<div style="text-align:left;clear:both;padding-left:73px;">
	  					 			 <span style="font-size:10px;padding_left:15px;">Gate Check In:</span>
			  						 <span style="font-size:10px;font-weight:bold;">
			  						 	<%out.print(gate_check_in_operate_time);%> 
			  						 </span>
			  				    </div>
			  				    <%
			  				    if(noticedResult.length>2){ 
							%>
	 						 				<div style="text-align:right;clear:both;">
				  								<span><a href="javascript:void(0)" onclick='moreNotices(<%=row.get("dlo_id",0) %>,<%=noticedResult[0].get("associate_process",0) %>)' style="color:blue">More...</a></span>
				  							</div>	
						 	<%
						 				}
							%>
	  						<%	
	  					  			}
  					  			}
  							%> 
  					<%if(!window_check_in_operate_time.equals("")||window_check_in_operate_time.equals("")){ 
  					
  						DBRow[] noticedResult = checkInMgrWfh.findScheduleByMainId(entry_id, 52);//查询window的通知
  						if(noticedResult!=null && noticedResult.length>0){
  					%> 
  					<div style="border-top:1px solid #ddd;padding:0px;margin: 5px 0;"> </div>
  					<div style="text-align:left;clear:both;margin-top:0px">
  						
  							<div  align="" style="margin-left:56px">
  								<span style="font-size:10px;">Window Check In:</span>
								<span style="font-size:10px;font-weight:bold;">
									
		  						 	<%if(!"".equals(window_check_in_operate_time)){
		  						 		out.print(window_check_in_operate_time); 
		  						 	} 
		  						 	%> 
								</span>
							</div>
							<table width="" border="0" cellspacing="0" cellpadding="0" >
	  						 <%
	  						 	for(int i=0;i<noticedResult.length;i++){				 	
					 				if(i<2){
					 					int numberType = noticedResult[i].get("number_type", 0);
					 					DBRow[] users = (DBRow[])noticedResult[i].get("scheduleUser", new DBRow[0]);
								%>
						 	     		<tr >
						 	     			<%if(numberType==moduleKey.CHECK_IN_LOAD||numberType==moduleKey.CHECK_IN_PONO||numberType==moduleKey.CHECK_IN_ORDER||numberType==moduleKey.CHECK_IN_PICKUP_ORTHERS|| numberType == moduleKey.SMALL_PARCEL){ %>
										   	<td style="color:blue;padding:0px;font-size:10px;" align="right" width="64px;"><%=moduleKey.getModuleName(noticedResult[i].get("number_type", "")) %>:</td>
										   	<%}else if(numberType==moduleKey.CHECK_IN_BOL||numberType==moduleKey.CHECK_IN_CTN||numberType==moduleKey.CHECK_IN_DELIVERY_ORTHERS){ %>
										   	<td style="color:red;padding:0px;font-size:10px;" align="right" width="64px;"><%=moduleKey.getModuleName(noticedResult[i].get("number_type", "")) %>:</td>
										   	<%}else{ %>
										  	<td align="right" style="padding:0px;" width="64px;"></td>
										  	<%} %>
					 	     				<td style="padding:0px;width:92px" height="14px">&nbsp;<a style="color:blue;font-size:10px;" onclick="detailNotices(<%=noticedResult[i].get("schedule_id", 0) %>)" href="javascript:void(0)"><%=noticedResult[i].get("number", "") %></a></td>
					 	     				<td>
					 	     					<%
												int noticesCount = users.length;
												noticesCount = noticesCount>1?1:noticesCount;
												for(int noticesIndex=0;noticesIndex<noticesCount;noticesIndex++){
												%>
													Notified &nbsp;&nbsp;<%=users[noticesIndex].get("schedule_execute_name", "") %>
													
												<%}%>
												<%=users.length>1?"...":"" %>
					 	     				</td>
					 	     				<%-- <%
					 	     				if(users!=null&&users.length>0){ 
					 	     					if(!StringUtil.isBlank(users[0].get("schedule_finish_name",""))){
					 	     				%>
					 	     				<td style="color:#bbb;padding:0px;">Closed by</td>
					 	     				<td><%=users[0].get("schedule_finish_name","") %></td>
					 	     				<%}} %> --%>
					 	     			</tr>
						   	<%		}else{
						  				break;
								    }
					 		   }
							%>
	  						</table>
	  						<%	
	  						if(noticedResult.length>2){ 
							%>
					 				<div style="text-align:right;clear:both;">
	  									<span><a href="javascript:void(0)" onclick='moreNotices(<%=row.get("dlo_id",0) %>,52)' style="color:blue">More...</a></span>
		  							</div>	
						 	<%
						 	}
							%>
	  					
  					</div>
  						<%}else{ %>
  						<%if(!"".equals(window_check_in_operate_time)){%>
  							<div style="clear:both;padding-left:58px;text-align: left;font-size:10px;">
			  						<span >Window Check In:</span>
			  						<B >
		  						 		<% out.print(window_check_in_operate_time); %> 
		  						 	</B> 
		  					</div>
  						<% } 
						 	%>
  					<%}}
  						DBRow[] noticedResult = checkInMgrWfh.findTractorByEntryId(entry_id,0);//type == 0-车尾 and 车头
  						if(noticedResult!=null&&noticedResult.length>0){
  							boolean showFlag = false;
  							for(int i=0;i<noticedResult.length;i++){
  								warehouse_check_in_operate_time = noticedResult[i].get("check_in_warehouse_time", "");
									if(!StringUtil.isBlank(warehouse_check_in_operate_time))
	  								showFlag = true; 
	  						}
	  						if(showFlag){
	  						%>
								<div style="border-top:1px solid #ddd;padding:0px;margin: 5px 0;"> </div>
							<%
	  						}
  							for(int i=0;i<noticedResult.length;i++){
  								warehouse_check_in_operate_time = noticedResult[i].get("check_in_warehouse_time", "");
									if(!StringUtil.isBlank(warehouse_check_in_operate_time)){	//有warehouse的时间就显示出来
  					%>
			  					<div style="clear:both;padding-left:22px;font-size:10px;margin-top:6px;text-align: left;">
			  							<span  style="display:inline-block;width:43px;text-align:right;"><%=checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(noticedResult[i].get("equipment_type", 0)) %>: </span>
			  							<span  style="display:inline-block;width:88px"><%=noticedResult[i].get("equipment_number", "")%> </span>
			  							<span >WHSE Check In:</span>
			  							<span style="font-size:10px;font-weight:bold;">
			  								<%
												warehouse_check_in_operate_time = DateUtil.showLocalparseDateTo24Hours(warehouse_check_in_operate_time, noticedResult[i].get("ps_id", 0l));
			  									out.print(warehouse_check_in_operate_time);
											%>
			  							</span>
			  					</div>
  					<%		}}
  						}%>
  				<%
  					DBRow[] gateCheckOut = noticedResult;//type == 0-所有的设备
  					if(gateCheckOut!=null&&gateCheckOut.length>0){
  						boolean showFlag = false;
  						for(int i=0;i<gateCheckOut.length;i++){
  							String check_out_operate_time = gateCheckOut[i].get("check_out_time", "");  //分割线
  							if(!StringUtil.isBlank(check_out_operate_time))
  								showFlag = true;
  						}
  						if(showFlag){
  						%>
							<div style="border-top:1px solid #ddd;padding:0px;margin: 5px 0;"> </div>   
						<%
  						}
  						for(int i=0;i<gateCheckOut.length;i++){
  							String check_out_operate_time = gateCheckOut[i].get("check_out_time", "");
  							if(!StringUtil.isBlank(check_out_operate_time)){
				%>
						<div style="text-align:left;clear:both;padding-left:22px;font-size:10px;margin-top:6px">
							 <span  style="display:inline-block;width:43px;text-align:right;"><%=checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(gateCheckOut[i].get("equipment_type", 0)) %>: </span>
							 <span  style="display:inline-block;width:86px"><%=gateCheckOut[i].get("equipment_number", "")%> </span>
							 <span >Gate Check Out:</span>
							 <span style="font-size:10px;font-weight:bold;">
							 	<%
									check_out_operate_time = DateUtil.showLocalparseDateTo24Hours(check_out_operate_time, gateCheckOut[i].get("ps_id", 0l));
									out.print(check_out_operate_time);
								%>
							 </span>
						</div>
				<%}	}}%> 
				</fieldset>
 			<!-- appointment_time  显示 -->
      		
 			<%
      		DBRow[] tasks = checkInMgrWfh.findAllTaskByMainId(entry_id,showTimePsId);
      		if(tasks != null && tasks.length>0){ 
      		%>
      		<fieldset align="left" style="border:2px solid blue;width: 393px;border-radius: 5px;margin-top: 15px;margin-bottom: 5px;position: relative;padding: 3px 0 8px;">
      			<legend style="color: black;font-weight:bold;font-size:11px;">
      			Appointment
      			</legend>
      			<table cellspacing="0" cellpadding="0" border="0" class="purpose">
      				<tr>
      					<td style="padding:0px;font-weight: bold;">
			      			<%=row.get("priority", 0)>0?checkInEntryPriorityKey.getCheckInEntryPriorityKey(row.get("priority", 0)):"" %> 
      					</td>
      				</tr>
      			</table>
      			<table width="100%" cellspacing="0" cellpadding="0" border="0">
      			<%
   					int taskCount = tasks.length;
   					taskCount = taskCount>2?2:taskCount;
   					for(int i=0;i<taskCount;i++){
   						int numberType = tasks[i].get("number_type", 0);
       			%>
						<tr>
							<td width="64px" style="text-align:right;font-size:10px;padding-left:0px">
							<%if(numberType==moduleKey.CHECK_IN_LOAD||numberType==moduleKey.CHECK_IN_PONO||numberType==moduleKey.CHECK_IN_ORDER||numberType==moduleKey.CHECK_IN_PICKUP_ORTHERS|| numberType == moduleKey.SMALL_PARCEL){ %>
									<span style="color:blue" align="right" ><%=moduleKey.getModuleName(numberType) %>:</span>
							<%}else if(numberType==moduleKey.CHECK_IN_BOL||numberType==moduleKey.CHECK_IN_CTN||numberType==moduleKey.CHECK_IN_DELIVERY_ORTHERS){ %>
								   	<span style="color:red" align="right" ><%=moduleKey.getModuleName(numberType) %>:</span>
							<%} %>
							</td>
							<td width="89px" style="text-align:left;padding:0px;font-size:10px;">&nbsp;<%=tasks[i].get("number", "")%></td>
							<td style="text-align:left;font-size:10px;font-weight:bold;width: 100px;padding-left: 6px;"><%=tasks[i].get("appointment_time","") %></td>
							<td style="text-align:left;font-size:10px;font-weight:bold;padding:0px;"><%=checkInOrderComeStatusKey.geCheckInOrderComeStatusKeyValue(tasks[i].get("come_status",0)) %></td>
						</tr>      						
      			<%
      				}
      			%>
      			</table>
      			<%if(tasks!=null&&tasks.length>2){ %>
      				<div style="text-align:right;clear:both;">
						<span><a href="javascript:void(0)" onclick='moreTaskAppointment(<%=row.get("dlo_id",0) %>)' style="color:blue">More...</a></span>
					</div>
      			<%} %>
      		</fieldset>
      		<%   
      			}
      		%>		
 					
      		</td>  	  
      		
      		<td  align="center" valign="middle" style='word-break:break-all'>
      		
      		<%// =====   修改车头栏  有可能为多个
      		//	entry_tractor
      			for(int jj = 0;jj<entry_tractor.length;jj++){
      		
					if(!entry_tractor[jj].get("equipment_number","").equals(""))
					 	{
			%>
      		<fieldset align="left" class="set" style="width:230px;position: relative;margin-bottom: 10px;border-radius: 5px;margin-top:10px;" id="equipment_<%=entry_tractor[jj].get("equipment_id",0L) %>" para="<%=entry_tractor[jj].get("resources_type", 0)%>|<%=occupyTypeKey.getOccupyTypeKeyName(entry_tractor[jj].get("resources_type", 0)) %>|<%=entry_tractor[jj].get("resources_id", 0)%>|<%=entry_tractor[jj].get("resources_id_name", "")%>">
  					<legend style="font-size:11px;font-weight:bold;">
  					
  							<a style="" target="_blank" >
  						 		 Tractor: <%out.print(entry_tractor[jj].get("equipment_number",""));%> 
  						 	</a>
						<%
  						//	DBRow equipmentSpace = checkInMgrWfh.findEquipmentOccupancySpace(entry_tractor[jj].get("equipment_id",0));  ?改方法不再使用
  						if(entry_tractor[jj].get("resources_type", 0)>0){
  						%>
							<font color='green'>|</font>
							<!-- releaseDoor(main_id,door_id,equipmentId,resources_type,rel_type) -->
							<a style="color:black;" onclick="releaseSpace(<%=entry_id %>,<%=entry_tractor[jj].get("resources_id", 0) %>,<%=entry_tractor[jj].get("equipment_id", 0) %>,<%=entry_tractor[jj].get("resources_type", 0) %>,<%=entry_tractor[jj].get("rel_type", 0) %>,'<%=occupyTypeKey.getOccupyTypeKeyName(entry_tractor[jj].get("resources_type", 0))%>','<%=entry_tractor[jj].get("resources_id_name", "") %>')" title="Release <%=occupyTypeKey.getOccupyTypeKeyName(entry_tractor[jj].get("resources_type", 0)) %>" href="javascript:void(0)">
							<%=occupyTypeKey.getOccupyTypeKeyName(entry_tractor[jj].get("resources_type", 0))+":"%> 
							<%=entry_tractor[jj].get("resources_id_name", "") %>
							</a>
						<%} %>
  					</legend>
  					<%if(entry_trailer==null||entry_trailer.length==0){ %>
					<table style="color:black;font-weight:bold;" border="0" cellspacing="0" cellpadding="0" id="containerType" >
	  						<tr><td style="padding:0px 5px;">
		  					<%
		  						if(entry_tractor[jj].get("equipment_status",0)!=0 && !entry_tractor[jj].getString("equipment_number").equals("") ){
		  							if(entry_tractor[jj].get("rel_type",0)>0)
		  								out.print(checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(entry_tractor[jj].get("rel_type",0))+" :");
		  					%>
	  								<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(entry_tractor[jj].get("equipment_status",0))%>
	  						<%}   %>
	  						</td></tr>
	  					</table>
  					<%}else if(entry_tractor[jj].get("equipment_status",0)==checkInMainDocumentsStatusTypeKey.LEFT){ %>
  					
  					<table id="containerType" cellspacing="0" cellpadding="0" border="0" style="color:black;font-weight: bold;" >
  						<tr><td style="padding:0px 5px;">
	  					  <%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(checkInMainDocumentsStatusTypeKey.LEFT)%>
	  					  </td></tr>
		  			</table>
		  			<%}%>
  					<div style="text-align:left;clear:both;"> 						
  						<div style="width:100px;float:left" align="right">Driver License:</div>
  						<div>
  						<%
  						 	if(!row.getString("gate_driver_liscense").equals(""))
  						 	{
  						 		out.print(row.getString("gate_driver_liscense"));
  						 	} 
  						 %> 
  						</div>
  					</div>
  					
  					<div style="text-align:left;clear:both;"> 						
  						<div style="width:100px;float:left" align="right">Driver Name:</div>
  						<div>
  						<%
  						 	if(!row.getString("gate_driver_name").equals(""))
  						 	{
  						 		out.print(row.getString("gate_driver_name"));
  						 	} 
  						 %> 
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;">
  						<div style="width:100px;float:left" align="right">GPS:</div>
  						<div >
  						<%
  						 	if(!row.getString("imei").equals(""))
  						 	{
  						 		out.print(row.getString("imei"));
  						 	} 
  						 %> 
  						 <input type="hidden" name="initgpsId" id="initgpsId" value="<%=row.getString("id") %>" />
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;">
  						<div style="width: 100px;float:left" align="right">Carrier:</div>
  						<div>
  						<%
  						 	if(!row.getString("company_name").equals(""))
  						 	{
  						 		out.print(row.getString("company_name"));
  						 	} 
  						 %> 
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;">
  						<div style="width: 100px;float:left" align="right">MC/DOT:</div>
  						<div>
  						<%
  						 	if(!row.getString("mc_dot").equals(""))
  						 	{
  						 			out.print(row.getString("mc_dot"));
  						 	} 
  						 %> 
  						</div>
  					</div>
  					
  				</fieldset>
  				<%
					 	}
					}//多车头 end 
				%>
      		</td>
  			<!-- task  -->
  			
  			<td id="detail" align="center" valign="middle" style='word-break:break-all'>
 					<%
  						if(entry_trailer!=null){
  						for(int ii=0;ii<entry_trailer.length;ii++){  //当一个entry对应多个trailer时显示多个trailer fieldset
 					%>
	      		<fieldset align="left" style="width:396px; border:2px solid blue;position:relative;margin-bottom: 20px;border-radius: 5px;margin-top: 20px;" id="equipment_<%=entry_trailer[ii].get("equipment_id",0L) %>" para="<%=entry_trailer[ii].get("resources_type", 0)%>|<%=occupyTypeKey.getOccupyTypeKeyName(entry_trailer[ii].get("resources_type", 0)) %>|<%=entry_trailer[ii].get("resources_id", 0)%>|<%=entry_trailer[ii].get("resources_id_name", "")%>">
	  				<legend style="font-size:11px;font-weight:bold;">
	  					
	  					<a style="width:50px" target="_blank">
	  					<%if(!entry_trailer[ii].getString("equipment_number").equals(""))
  						 	{
	  					%>
	  						<%=checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(entry_trailer[ii].get("equipment_type",0))%>:
  						<% 		
  								out.print(entry_trailer[ii].getString("equipment_number"));
  						 	} 
  						 %> 
	  					</a>
  						<%
	  						//	DBRow equipmentSpace = checkInMgrWfh.findEquipmentOccupancySpace(entry_trailer[ii].get("equipment_id",0)); // ？不再使用
						if(entry_trailer[ii].get("resources_type", 0)!=0){
  						%>
						<font color='blue'>|</font>
						<a style="color:black;" onclick="releaseSpace(<%=entry_trailer[ii].get("check_in_entry_id", 0) %>,<%=entry_trailer[ii].get("resources_id", 0) %>,<%=entry_trailer[ii].get("equipment_id", 0) %>,<%=entry_trailer[ii].get("resources_type", 0) %>,<%=entry_trailer[ii].get("rel_type", 0) %>,'<%=occupyTypeKey.getOccupyTypeKeyName(entry_trailer[ii].get("resources_type", 0))%>','<%=entry_trailer[ii].get("resources_id_name", "") %>')" title="Release <%=occupyTypeKey.getOccupyTypeKeyName(entry_trailer[ii].get("resources_type", 0)) %>" href="javascript:void(0)">
							<%=occupyTypeKey.getOccupyTypeKeyName(entry_trailer[ii].get("resources_type", 0))+":"%>
							<%=entry_trailer[ii].get("resources_id_name", "") %>
  						</a>
  						<%}%>
	  					
	  				</legend>
	  					
	  					<table style="color:black;font-weight:bold;" border="0" cellspacing="0" cellpadding="0" id="containerType" >
	  						<tr><td style="padding:0px 5px;">
		  					<%
		  						if(entry_trailer[ii].get("equipment_status",0)!=0 && !entry_trailer[ii].getString("equipment_number").equals("") ){
		  							if(entry_trailer[ii].get("rel_type",0)>0)
		  								out.print(checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(entry_trailer[ii].get("rel_type",0))+" :");
		  					%>
	  								<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(entry_trailer[ii].get("equipment_status",0))%>
	  						<%}   %>
	  						</td></tr>
	  					</table>
		  				
	  				
	  				<%//pick up   customer  | delievery  title  设备下task的customerID与 title
	  						DBRow[] customerOrTitle =checkInMgrWfh.findCustomerOrTitleByEntryEquipment(entry_trailer[ii].get("check_in_entry_id", 0),entry_trailer[ii].get("equipment_id", 0),checkInMainDocumentsRelTypeKey.PICK_UP);
	  					if(entry_trailer[ii].get("rel_type", 0)==checkInMainDocumentsRelTypeKey.PICK_UP|| entry_trailer[ii].get("rel_type", 0)==checkInMainDocumentsRelTypeKey.BOTH){
	  				%>
	  					<%
	  						String customerID = "";
	  						for(DBRow customerRow:customerOrTitle){
	  							if(!StrUtil.isBlank(customerRow.get("customer_id", ""))){
	  								customerID +=customerRow.get("customer_id", "")+",";
	  							}
	  							
	  						}
	  						
	  						customerID = customerID.length()>0?customerID.substring(0, customerID.length()-1):customerID;
	  						if(!customerID.equals("")){
	  					%>
	  					<div style="padding-left: 20px;color:#06C;font-weight: bold;" align="left">
							CustomerID : <%=customerID %>
	  					</div>		
						<%
	  						}
	  					}
	  					if(entry_trailer[ii].get("rel_type", 0)==checkInMainDocumentsRelTypeKey.DELIVERY || entry_trailer[ii].get("rel_type", 0)==checkInMainDocumentsRelTypeKey.BOTH){
	  						String title = "";
		  					for(DBRow titleRow:customerOrTitle){
		  						if(!StrUtil.isBlank(titleRow.get("supplier_id", ""))){
		  							title +=titleRow.get("supplier_id", "")+",";
		  						}
	  						}
		  					title = title.length()>0?title.substring(0, title.length()-1):title;
		  					
	  						if(!title.equals("")){
	  					%>
	  					<div style="padding-left: 20px;color:#06C;font-weight: bold;" align="left">
	  						Title : <%=title %>
	  					</div>		
	  					<%
	  						}
	  					}
						%>

	  				
	  				<table  border="0" cellspacing="0" cellpadding="0" class="purpose">
	  							<tr><td style="color:black;font-weight: bold; padding:0px 5px;">
	  						<%if(entry_trailer[ii].get("isCheckOutTrailer", 0)==1){ %>
	  						<%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(checkInLiveLoadOrDropOffKey.PICK_UP)%>
	  						<%}else{ %>
							<%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(entry_trailer[ii].get("equipment_purpose", 0))%>
							<%} %>
							</td></tr>							
	  				</table>	
	  				
	  				<%
	  					
	  					//查找出来任务占用的资源
	  					DBRow[] occupanySpace = checkInMgrWfh.findOccupancySpaceByMainIdAndEquipmentId(entry_trailer[ii].get("check_in_entry_id", 0), entry_trailer[ii].get("equipment_id", 0));
 	  					if(occupanySpace!=null && occupanySpace.length>0){
	  						for(int _i=0;_i< occupanySpace.length ;_i++){
		  					  DBRow space = occupanySpace[_i];
	  						 /*  int srr_id = entry_trailer[ii].get("srr_id",0);
	  						  //设备单独占用的资源不显示在这里 所以在这里按照space表的ID过滤掉设备的资源
	  						  if(srr_id==space.get("srr_id",0)){
	  							  continue;
	  						  } */
 	  						  if(space!=null && space.get("resources_id",0)>0 ){
		  					    //因为只循环占用的资源 所以查询space表得到资源下的task
		  					    DBRow[] numbers = checkInMgrWfh.selectSpaceBySpaceAndEntryId(space.get("resources_id",0),entry_trailer[ii].get("check_in_entry_id", 0),space.get("resources_type",0),entry_trailer[ii].get("equipment_id",0));
	  				%>
		  				<fieldset align="left" style="border:2px solid #993300; width:95%; margin-top:10px;border-radius: 5px;">
		  					<legend>
			  					<table border="0" cellspacing="0" cellpadding="0">
			  			
								  <tr>
								   
								    <td style="font-size:11px;" >Assgin To &nbsp; <%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type",0))%>:</td>
								    <td width="40px;" style="font-size:11px;" id="<%=space.get("resources_id",0) %>" doorSta = "<%=space.get("resources_type",0) %>"  mainId="<%=entry_trailer[ii].get("check_in_entry_id", 0) %>" >
								     <%=space.get("resources_id_name", "")%> 
								    </td>   
								   
								    <td width="70px;">
								    	<%
								    	int occupy_status = space.get("occupy_status",0);
								    	occupy_status  = occupy_status == 0?space.get("occupancy_status",0):occupy_status;  //两个表取出的字段不一致  details  space
								    	if((space.get("resources_type",0)==1&&occupy_status==OccupyStatusTypeKey.RELEASED)||(space.get("resources_type",0)==2&&occupy_status==OccupyStatusTypeKey.RELEASED)){%>
				  							<span style="font-size:11px;">Released</span>
				  						<%			
				  						  }else{
				  								boolean numflag = false;
				  								boolean doorflag = false;
				  								if(numbers != null && numbers.length > 0 ){
				  						      		for(int j=0;j< numbers.length ;j++){
				  						      			if(numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE && numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION && numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.PARTIALLY ){
				  						      				numflag = true;
				  						      			}
				  						      			if(occupy_status==OccupyStatusTypeKey.OCUPIED){
				  						      				doorflag=true;
				  						      			}
				  						      		}	
				  								}
				  						%>
					  							<%	
					  							if(doorflag==false && occupy_status==OccupyStatusTypeKey.RESERVERED && space.get("resources_type", 0)==1){%>
							 		    		    <span attr="doorsta_<%=entry_trailer[ii].get("check_in_entry_id", 0) %>_<%=space.get("resoutces_id", 0) %>" style="font-size:11px;">Reserved</span>
							 		     		<%}else{%>
							 	   		 	 		<span attr="doorsta_<%=entry_trailer[ii].get("check_in_entry_id", 0) %>_<%=space.get("resoutces_id", 0) %>" style="font-size:11px;">Occupied</span>
							 	   		 		<%}%>
								 	 </td>	
					  							 <td id="<%=entry_trailer[ii].get("check_in_entry_id", 0) %>_<%=space.get("resoutces_id", 0) %>">
					  							 <%if(numflag==false){%>
					  							 	<input type="button" class="short-button" value="Release" 
					  							 		onclick="releaseDoor('<%=entry_trailer[ii].get("check_in_entry_id", 0)%>','<%=space.get("resources_id", 0)%>',<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=entry_trailer[ii].get("rel_type", 0)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')"/>
					  						     <%}  %>
					  						     </td>
									    <%}%>
								  </tr>
								</table>
		  					</legend>
		  							<%
					      			if(numbers != null && numbers.length > 0 ){
					      					for(int j=0;j< numbers.length ;j++){
							      			if(!numbers[j].getString("number").equals("")){
							      				int numberType = numbers[j].get("number_type", 0);
			  						%>
		  				   		     <div style="margin-top:5px;" align="left">  
		  				   		     <table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<%if(numberType==moduleKey.CHECK_IN_LOAD||numberType==moduleKey.CHECK_IN_PONO||numberType==moduleKey.CHECK_IN_ORDER||numberType==moduleKey.CHECK_IN_PICKUP_ORTHERS|| numberType == moduleKey.SMALL_PARCEL){ %>
										   	<td style="color:blue" align="right" width="55px;"><%=moduleKey.getModuleName(numbers[j].get("number_type", "")) %>:</td>
										   	<%}else if(numberType==moduleKey.CHECK_IN_BOL||numberType==moduleKey.CHECK_IN_CTN||numberType==moduleKey.CHECK_IN_DELIVERY_ORTHERS){ %>
										   	<td style="color:red" align="right" width="55px;"><%=moduleKey.getModuleName(numbers[j].get("number_type", "")) %>:</td>
										   	<%}else{ %>
										  	<td align="right" width="55px;"></td>
										  	<%} %>
										    <td width="90px;" style="padding:0px;" >
										     	&nbsp;<%=numbers[j].getString("number") %>
										    </td>
										    <td>
										    	   
					  						 	     <input id="<%=numbers[j].get("dlo_detail_id",0l)%>" type="button" style="color:#000" name="Submit"  onclick="showMenu(this,<%=row.getString("dlo_id")%>,'<%=numbers[j].get("dlo_detail_id",0l)%>')"  class="more-fun-short-button">
												    	<div id="menubutton_<%=row.getString("dlo_id")%>_<%=numbers[j].get("dlo_detail_id",0l)%>" class="easyui-menu" style='width:120px;display:none' >
												    	
						  						 	 <%if(numbers[j].get("number_type", 0l)==10 || numbers[j].get("number_type", 0l)==11 || numbers[j].get("number_type", 0l)==17 || numbers[j].get("number_type", 0l)==18){
										 			 %>
										 			 <%  if(numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE){%>
										  			   <div   onclick="closeNum(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=numbers[j].get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.CLOSE %>,<%=space.get("resources_id", 0)%>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=space.get("resources_type", 0)%>,<%=numberType%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')">
										  			   
						  						 	   Close
						  						 	   </div>
						  						 	   <%}if(numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION){ %>
						  						 	   <div   onclick="exception(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=numbers[j].get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION %>,<%=space.get("resources_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=space.get("resources_type", 0)%>,<%=numberType%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')" >
						  						 	  Exception
						  						 	   </div>
						  						 	   <%}if(numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.PARTIALLY){ %>
						  						 	   <div   onclick="partially(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=numbers[j].get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.PARTIALLY %>,<%=space.get("resources_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=space.get("resources_type", 0)%>,<%=numberType%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')" >
						  						 	  Partially
						  						 	   </div>
						  						 	   <%} %>
					  						 	     <%
													   }else{
														  if(numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE){ 
												     %>
												     	 <div  onclick="closeNum(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=numbers[j].get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.CLOSE %>,<%=space.get("resources_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=space.get("resources_type", 0)%>,<%=numberType%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')">
												     	 
						  						 	   Close 
						  						 	   </div>
						  						 	   <%}if(numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION){  %>
						  						 	   <div   onclick="exception(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=numbers[j].get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION %>,<%=space.get("resources_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=space.get("resources_type", 0)%>,<%=numberType%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')" >
						  						 	  Exception
						  						 	   </div>
												     <%
													   }}
													 %>
					  						 	    </div>
					  						 	    <%
									      				if(numbers[j].get("number_status",01)==CheckInChildDocumentsStatusTypeKey.CLOSE ){
									      		    %>
									      		     <!--    <span style="font-size:10px; color:#999"><%=CLOSED %></span> -->
											      		     <%if(numberType == ModuleKey.CHECK_IN_LOAD || numberType == ModuleKey.CHECK_IN_ORDER ||  numberType == ModuleKey.CHECK_IN_PONO){ %>
											      		      <a href="javascript:void(0)" onclick="showPalletsInfo('<%=numbers[j].get("dlo_detail_id", 0l) %>','<%=numbers[j].get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Load Info</a>
											      		   	<%}else if(numberType == ModuleKey.CHECK_IN_CTN || numberType == ModuleKey.CHECK_IN_BOL){ %>
										      		   	 	  <a href="javascript:void(0)" onclick="showPalletsInfo('<%=numbers[j].get("dlo_detail_id", 0l) %>','<%=numbers[j].get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive Info</a>

										      		   	    <%}else{ %>
											      		   	 	<span style="font-size:10px; color:#999"><%=CLOSED %></span>
											      		   	<%} %>
									      		    <%
									      				}else{
									      					if(numbers[j].get("number_status",01)==CheckInChildDocumentsStatusTypeKey.EXCEPTION && (numberType == ModuleKey.CHECK_IN_CTN || numberType == ModuleKey.CHECK_IN_BOL)){
									      			%>
									      					  <a href="javascript:void(0)" onclick="showPalletsInfo('<%=numbers[j].get("dlo_detail_id", 0l) %>','<%=numbers[j].get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive</a>
									      			
									      			<%
									      					}else{
									      		    %>
									      		     	   	 <span attr="<%=entry_trailer[ii].get("check_in_entry_id", 0) %>_<%=numbers[j].get("dlo_detail_id",0l) %>" style="font-size:10px; color:#999"><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(numbers[j].get("number_status",0)) %></span> 
									      		    <%
									      					}
									      				}
									 	   		 	%>
										    </td>
										</tr>
										<tr><td colspan="5" style="padding-left:5px">
										<!-- 显示warhouse通知的人 -->
										<%
											int detailId = numbers[j].get("dlo_detail_id",0);
											DBRow[] notices = checkInMgrWfh.findScheduleByDetailId(detailId, 53);
											if(notices!=null && notices.length>0){
												%>
												<table cellspacing="0" cellpadding="0" border="0">
												<tr>
													<td style="width:80px;padding:0px;color:#999">
														Assign Labor
													</td>
													<td width="125px" style="padding:0px">
														
														<%
														int noticesCount = notices.length;
														noticesCount = noticesCount>1?1:noticesCount;
														for(int noticesIndex=0;noticesIndex<noticesCount;noticesIndex++){
														%>
															<%=notices[noticesIndex].get("schedule_execute_name", "") %>
															
														<%}%>
														
												</td>
												<%String closeName =notices[0].getString("schedule_finish_name");  
												
												if(!StringUtil.isBlank(closeName)){
												%>
												<td style="color:#999;width:80px"><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(notices[0].get("number_status", 0)) %> by</td>
												<td><%=closeName %></td>
												<%} %>
											</tr>
											</table>
											</td></tr>
											<%} %>
									 </table>		  						 	
		  						 	 </div>
  									<%
  										}
							      	 } 
					      		  }
		  							%>
		  							</fieldset><!-- 资源的fieldset -->
		  							<%
	  						  }else{
	  							  DBRow[] gateNumber = checkInMgrZwb.findAllLoadByMainId(row.get("dlo_id",01));
	  								if(!gateNumber[0].getString("number").equals("")){
	  							  	  %>
	  							  	  <fieldset style="border:2px solid #993300; width:95%; margin-top:10px;border-radius: 5px;">
	  			  				      		  <div style=" margin: 5px 2px;" align="left">
	  				  						
	  											    <%if(gateNumber[0].get("number_type", 0l)==12 || gateNumber[0].get("number_type", 0l)==13 || gateNumber[0].get("number_type", 0l)==14){
												   %>
												   	<div style="color:red;display: inline-block;" align="right" width="65px;"><%=moduleKey.getModuleName(gateNumber[0].get("number_type", "")) %>:</div>
												   
												   <% 
												   } else{
												   %>
												   	<div style="color:blue;display: inline-block;" align="right" width="65px;"><%=moduleKey.getModuleName(gateNumber[0].get("number_type", "")) %>:</div>
												   	
												   <%
												   }
												   %>
	  											     <div width="110px;" style="display: inline-block;" >
	  											   	 <%=gateNumber[0].getString("number") %>
	  											     </div>
	  											 
	  										  </div>
		  							</fieldset>
					<%  
									}  
	  							}
						 }}//资源遍历END
						 //查询没有占用门或者停车位的task
					%>
					
					
					
					<%
	  				//查询关闭的task   task type:number close at space:no
	  				DBRow[] taskClosed = checkInMgrWfh.findTaskClosedByEntry(entry_trailer[ii].get("check_in_entry_id", 0),entry_trailer[ii].get("equipment_id",0));
					//分成两组 一组是 正常close  另一组是exception close
					List<DBRow> normalCloseTask = new ArrayList<DBRow>();
					List<DBRow> exceptionCloseTask = new ArrayList<DBRow>();
					for(DBRow taskRow:taskClosed){
						if(taskRow.get("number_status",01) == CheckInChildDocumentsStatusTypeKey.CLOSE){
							normalCloseTask.add(taskRow);
						}else{
							exceptionCloseTask.add(taskRow);
						}
					}
	  				if(taskClosed!=null&& taskClosed.length > 0){
	  				%>
		  		  <fieldset align="left" style="text-algin:left;border:2px solid #000; width:95%; margin-top:10px;border-radius: 5px;padding-top: 15px;">
	  			  <table  cellspacing="0" cellpadding="0" border="0" width="100%">
	  			  <!-- normal close -->
		  			<%	
		  			int task_count = normalCloseTask.size();
		  			if(task_count>2){
		  				task_count = 2;
		  			}
		  			for(int norIn=0;norIn<task_count;norIn++){ 
		  				DBRow taskRow = normalCloseTask.get(norIn);
		  			%>
	  			 		
	  			  	<tr style="height:25px;">
	  			  	
	  			  		<%if(taskRow.get("number_type", 0)==moduleKey.CHECK_IN_LOAD||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_PONO||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_ORDER||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_PICKUP_ORTHERS|| taskRow.get("number_type", 0) == moduleKey.SMALL_PARCEL){ %>
			   				<td style="text-align: right;color:blue" width="53px"><%=moduleKey.getModuleName(taskRow.get("number_type", 0)) %>:</td>
			   			<%}else if(taskRow.get("number_type", 0)==moduleKey.CHECK_IN_BOL||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_CTN||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_DELIVERY_ORTHERS){ %>
			   				<td style="text-align: right;color:red" width="53px"><%=moduleKey.getModuleName(taskRow.get("number_type", 0)) %>:</td>
			   			<%}else{ %>
			   				<td style="text-align: right;" width="53px"></td>
			   			<%} %>
			   		<td style="text-align:left; padding:0px; margin:0px;width: 90px;">
	  			  		&nbsp;<%=taskRow.get("number", "")%>
	  			  	</td>
	  			  	<td style="width:135px">
	  					 <%//显示单据的状态
	   		 			 if(taskRow.get("number_status",01)==CheckInChildDocumentsStatusTypeKey.CLOSE ){
	   		 			 	if(taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_LOAD || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_ORDER ||  taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_PONO || taskRow.get("number_type", 0) == ModuleKey.SMALL_PARCEL){ %>
     		    				<a href="javascript:void(0)" onclick="showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Load Info</a>
     						<%}else if(taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_CTN || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_BOL){ %>
	      		   	 			<a href="javascript:void(0)" onclick="showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive Info</a>
		      		   	    <%}else{ %>
	     		   				<span style="font-size:10px; color:#999"><%=CLOSED %></span>
	     		   			<%}} //显示状态 end %>
    		   		</td>
    		    	<td style="padding-left:0px;text-align:center">
						At <%if(taskRow.get("occupancy_type", 0)>0){
  			  				out.print(occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))+": "+taskRow.get("rl_id_name", "")); 
  			   				}
  			   	   		%>
					</td >
			   	</tr>
			   		
			   	<tr><td colspan="5" style="padding-left:5px">
			   		<!-- 显示warhouse通知的人 -->
					<%
						int detailId = taskRow.get("dlo_detail_id",0);
						DBRow[] notices = checkInMgrWfh.findScheduleByDetailId(detailId, 53);
						if(notices!=null && notices.length>0){
							%>
							<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td style="padding:0px;width:80px;height:14px;color:#999">
									Assign Labor
								</td>
								<td width="125px" style="padding:0px">
									
									<%
									int noticesCount = notices.length;
									noticesCount = noticesCount>1?1:noticesCount;
									for(int noticesIndex=0;noticesIndex<noticesCount;noticesIndex++){
									%>
										<%=notices[noticesIndex].get("schedule_execute_name", "") %>
										
									<%}%>
									
								</td>
							<%String closeName =notices[0].getString("schedule_finish_name");  
							
							if(taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.PROCESSING && taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.UNPROCESS){
							%>
							<td style="color:#999;padding:0px;width:80px" ><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(notices[0].get("number_status", 0)) %> by</td>
							<td style="padding:0px;"><%=closeName %></td>
							<%} %>
						</tr>
						</table>
						<%} %>
						</td></tr>
         			<%}if(normalCloseTask.size()>2){ %>
         				<tr><td colspan="5" style="padding-left:5px" align="right"><a href="javascript:void(0)" onclick="showMoreCloseTask(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,1,<%=entry_trailer[ii].get("equipment_id", 0L)%>,<%=row.get("dlo_id", 0L)%>)">More...</a></td></tr>
  			  <%}%> 
  			  </table>
  			  
  			  
  			  <!-- exception close -->
  			  <%	
  				task_count = exceptionCloseTask.size();
	  			if(task_count>2){
	  				task_count = 2;
	  			}
	  			if(task_count>0 && normalCloseTask.size()!=0){
	  			%> 
	  				<div style="border-top:1px solid #ddd;padding:0px;margin: 5px 0;"></div>
	  			<%
	  			}%>
  			  <table cellspacing="0" cellpadding="0" border="0" width="100%">
  			  <%
	  			for(int norIn=0;norIn<task_count;norIn++){
	  				DBRow taskRow = exceptionCloseTask.get(norIn);
  			  %>
	  			  	<tr style="height:25px;">
	  			  		<%if(taskRow.get("number_type", 0)==moduleKey.CHECK_IN_LOAD||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_PONO||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_ORDER||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_PICKUP_ORTHERS||taskRow.get("number_type", 0)==moduleKey.SMALL_PARCEL){ %>
			   				<td style="text-align: right;color:blue" width="53px"><%=moduleKey.getModuleName(taskRow.get("number_type", 0)) %>:</td>
			   			<%}else if(taskRow.get("number_type", 0)==moduleKey.CHECK_IN_BOL||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_CTN||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_DELIVERY_ORTHERS){ %>
			   				<td style="text-align: right;color:red" width="53px"><%=moduleKey.getModuleName(taskRow.get("number_type", 0)) %>:</td>
			   			<%}else{ %>
			   				<td style="text-align: right;" width="53px"></td>
			   			<%} %>
			   		<td style="text-align:left; padding:0px; margin:0px;width: 90px;">
	  			  		&nbsp;<%=taskRow.get("number", "")%>
	  			  	</td>
	  			  	<td style="width:135px">
	  					 <%//显示单据的状态
	    		 			 if(taskRow.get("number_status",01)==CheckInChildDocumentsStatusTypeKey.CLOSE ){
	    		 			 	if(taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_LOAD || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_ORDER ||  taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_PONO){ %>
	      		    				<a href="javascript:void(0)" onclick="showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Load Info</a>
	      						<%}else if(taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_CTN || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_BOL){ %>
			      		   	 	  <a href="javascript:void(0)" onclick="showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive Info</a>

			      		   	    <%}else{ %>
	      		   					<span style="font-size:10px; color:#999"><%=CLOSED %></span>
	      		   				<%} %>
	    		    	<%
	    					}else{
	    					//***********继续显示 more 按钮组
	    				%>
					 	     <input id="<%=taskRow.get("dlo_detail_id",0l)%>" type="button" style="color:#000" name="Submit"  onclick="showMenu(this,<%=row.getString("dlo_id")%>,'<%=taskRow.get("dlo_detail_id",0l)%>')"  class="more-fun-short-button">
					    	<div id="menubutton_<%=row.getString("dlo_id")%>_<%=taskRow.get("dlo_detail_id",0l)%>" class="easyui-menu" style='width:120px;display:none' >
					    	
						 	 <%if(taskRow.get("number_type", 0l)==10 || taskRow.get("number_type", 0l)==11 || taskRow.get("number_type", 0l)==17 || taskRow.get("number_type", 0l)==18){
			 			 %>
			 			  	<%if(taskRow.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE){%>
			  			   		<div   onclick="closeNum(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=taskRow.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.CLOSE %>,<%=taskRow.get("rl_id", 0)%>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,'<%=entry_trailer[ii].get("equipment_id", 0)%>',<%=taskRow.get("occupancy_type", 0)%>,<%=taskRow.get("number_type", 0l)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))%>','<%=taskRow.get("rl_id_name", "")%>')">
						 	  	 	Close
						 	   </div>
						 	   <%}if(taskRow.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION){ %>
						 	   <div   onclick="exception(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=taskRow.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION %>,<%=taskRow.get("rl_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,'<%=entry_trailer[ii].get("equipment_id", 0)%>',<%=taskRow.get("occupancy_type", 0)%>,<%=taskRow.get("number_type", 0l)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))%>','<%=taskRow.get("rl_id_name", "")%>')" >
						 	 	 	Exception
						 	   </div>
						 	   <%}if(taskRow.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.PARTIALLY){  %>
						 	   <div   onclick="partially(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=taskRow.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.PARTIALLY %>,<%=taskRow.get("rl_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,'<%=entry_trailer[ii].get("equipment_id", 0)%>',<%=taskRow.get("occupancy_type", 0)%>,<%=taskRow.get("number_type", 0l)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))%>','<%=taskRow.get("rl_id_name", "")%>')" >
						 	 		Partially
						 	   </div>
					 	     <%
						 	   }
						   } else{
					     %>
						     <%  if(taskRow.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE){%>
						     	 	<div  onclick="closeNum(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=taskRow.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.CLOSE %>,<%=taskRow.get("rl_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,'<%=entry_trailer[ii].get("equipment_id", 0)%>',<%=taskRow.get("occupancy_type", 0)%>,<%=taskRow.get("number_type", 0l)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))%>','<%=taskRow.get("rl_id_name", "")%>')">
							 	  		Close
							 	   	</div>
							 <%}if(taskRow.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION){ %>
							 	   <div   onclick="exception(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=taskRow.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION %>,<%=taskRow.get("rl_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,'<%=entry_trailer[ii].get("equipment_id", 0)%>',<%=taskRow.get("occupancy_type", 0)%>,<%=taskRow.get("number_type", 0l)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))%>','<%=taskRow.get("rl_id_name", "")%>')" >
							 	  		Exception 
							 	  		
							 	   </div>
					     <%}}%>
						 	</div>
    				  	<%
		      				if(taskRow.get("number_status",01)==CheckInChildDocumentsStatusTypeKey.CLOSE ){
		      		    %>
				      		     <%if(taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_LOAD || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_ORDER ||  taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_PONO){ %>
				      		      <a href="javascript:void(0)" onclick="showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Load Info</a>
				      			 <%}else if(taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_CTN || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_BOL){ %>
	      		   	 	  		  <a href="javascript:void(0)" onclick="showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive Info</a>

	      		   	    <%        }
		      				}else if(taskRow.get("number_status",01)==CheckInChildDocumentsStatusTypeKey.EXCEPTION && (taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_LOAD || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_ORDER || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_PONO || taskRow.get("number_type", 0) == ModuleKey.SMALL_PARCEL)){
		      			%>
		      					<a href="javascript:void(0)" onclick="showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666;font-size: 10px;margin-left: 5px;"  >Load Info</a>
		      			<%
		      				}
		      				else if(taskRow.get("number_status",01)==CheckInChildDocumentsStatusTypeKey.EXCEPTION && (taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_CTN || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_BOL)){
		      			%>
		      					  <a href="javascript:void(0)" onclick="showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive</a>
		      			
		      			<%
		      				}else if(taskRow.get("is_forget_task", 0)==1 && (taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.CLOSE && taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.EXCEPTION && taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.PARTIALLY)){ %>
								<span attr="<%=entry_trailer[ii].get("check_in_entry_id", 0) %>_<%=taskRow.get("dlo_detail_id",0l) %>" style="font-size:10px; color:#999">Forget</span>		      	
				      	<%  }else{ %>
    				    	 	<span attr="<%=entry_trailer[ii].get("check_in_entry_id", 0) %>_<%=taskRow.get("dlo_detail_id",0l) %>" style="font-size:10px; color:#999"><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(taskRow.get("number_status",0)) %></span>
    		   			<%  } 
    				  	   } //显示状态 end%>
    		    </td>
    		    <td style="padding-left:0px;text-align:center">
					At <%if(taskRow.get("occupancy_type", 0)>0){
	  			  				out.print(occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))+": "+taskRow.get("rl_id_name", "")); 
	  			   			}
	  			   	   %>
				</td >
		   		</tr>
		   		<tr><td colspan="5" style="padding-left:5px">
		   		<!-- 显示warhouse通知的人 -->
				<%
					int detailId = taskRow.get("dlo_detail_id",0);
					DBRow[] notices = checkInMgrWfh.findScheduleByDetailId(detailId, 53);
					if(notices!=null && notices.length>0){
						%>
						<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td style="padding:0px;width:80px;height:14px;color:#999">
								Assign Labor
							</td>
							<td width="125px" style="padding:0px">
								
								<%
								int noticesCount = notices.length;
								noticesCount = noticesCount>1?1:noticesCount;
								for(int noticesIndex=0;noticesIndex<noticesCount;noticesIndex++){
								%>
									<%=notices[noticesIndex].get("schedule_execute_name", "") %>
									
								<%}%>
								
							</td>
						<%String closeName =notices[0].getString("schedule_finish_name");  
						
						if(taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.PROCESSING && taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.UNPROCESS){
						%>
						<td style="color:#999;padding:0px;width:80px" ><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(notices[0].get("number_status", 0)) %> by</td>
						<td style="padding:0px;"><%=closeName %></td>
						<%} %>
					</tr>
					</table>
					<%} %>
					</td></tr>
					<%}if(exceptionCloseTask.size()>2){%>
         				<tr><td colspan="5" style="padding-left:5px" align="right"><a href="javascript:void(0)" onclick="showMoreCloseTask(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,2,<%=entry_trailer[ii].get("equipment_id", 0L)%>,<%=row.get("dlo_id", 0L)%>)">More...</a></td></tr>
  			  <%} %> 
			   	 </table>
  			  </fieldset>
  			  <%}//查询关闭的资源  end %>
  			  <!-- 显示没有占用资源的task -->
  			  <%
  			  	DBRow[] noSpaceTasks = checkInMgrWfh.findNoSpaceTaskByEntryId(entry_trailer[ii].get("check_in_entry_id", 0L), entry_trailer[ii].get("equipment_id", 0L));
  			  	if(noSpaceTasks.length>0){
  			  %>
  			 	<fieldset style="border: 2px solid #993300;border-radius: 5px;margin-top: 10px;width: 95%;">
  			 	<div align="left" style="margin-top:5px;">
  			 		<table border="0" cellspacing="0" cellpadding="0">
  			 		<%for(DBRow noSpaceTask:noSpaceTasks){
  			 			int numberType = noSpaceTask.get("number_type", 0);
  			 			DBRow space = new DBRow();
  			 			%>
						<tr>
							<%if(numberType==moduleKey.CHECK_IN_LOAD||numberType==moduleKey.CHECK_IN_PONO||numberType==moduleKey.CHECK_IN_ORDER||numberType==moduleKey.CHECK_IN_PICKUP_ORTHERS|| numberType == moduleKey.SMALL_PARCEL){ %>
						   	<td style="color:blue" align="right" width="55px;"><%=moduleKey.getModuleName(noSpaceTask.get("number_type", "")) %>:</td>
						   	<%}else if(numberType==moduleKey.CHECK_IN_BOL||numberType==moduleKey.CHECK_IN_CTN||numberType==moduleKey.CHECK_IN_DELIVERY_ORTHERS){ %>
						   	<td style="color:red" align="right" width="55px;"><%=moduleKey.getModuleName(noSpaceTask.get("number_type", "")) %>:</td>
						   	<%}else{ %>
						  	<td align="right" width="55px;"></td>
						  	<%} %>
						    <td width="90px;" style="padding:0px;" >
						     	&nbsp;<%=noSpaceTask.getString("number") %>
						    </td>
						    <td>
						    	   <%if(noSpaceTask.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE){%>
	  						 	     <input id="<%=noSpaceTask.get("dlo_detail_id",0l)%>" type="button" style="color:#000" name="Submit"  onclick="showMenu(this,<%=row.getString("dlo_id")%>,'<%=noSpaceTask.get("dlo_detail_id",0l)%>')"  class="more-fun-short-button">
								    	<div id="menubutton_<%=row.getString("dlo_id")%>_<%=noSpaceTask.get("dlo_detail_id",0l)%>" class="easyui-menu" style='width:120px;display:none' >
								    	
		  						 	 <%if(noSpaceTask.get("number_type", 0l)==10 || noSpaceTask.get("number_type", 0l)==11 || noSpaceTask.get("number_type", 0l)==17 || noSpaceTask.get("number_type", 0l)==18){
		  						 	%>
						 			 <%  if(noSpaceTask.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE){%>
						  			   <div   onclick="closeNum(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=noSpaceTask.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.CLOSE %>,<%=space.get("resources_id", 0)%>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=space.get("resources_type", 0)%>,<%=numberType%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')">
						  			   
		  						 	   Close
		  						 	   </div>
		  						 	   <%}if(noSpaceTask.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION && noSpaceTask.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE ){ %>
		  						 	   <div   onclick="exception(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=noSpaceTask.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION %>,<%=space.get("resources_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=space.get("resources_type", 0)%>,<%=numberType%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')" >
		  						 	  Exception
		  						 	   </div>
		  						 	   <%}if(noSpaceTask.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.PARTIALLY  && noSpaceTask.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE){ %>
		  						 	   <div   onclick="partially(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=noSpaceTask.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.PARTIALLY %>,<%=space.get("resources_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=space.get("resources_type", 0)%>,<%=numberType%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')" >
		  						 	  Partially
		  						 	   </div>
		  						 	   <%} %>
	  						 	     <%
									   }else{
										  if(noSpaceTask.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE){ 
								     %>
								     	 <div  onclick="closeNum(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=noSpaceTask.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.CLOSE %>,<%=space.get("resources_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=space.get("resources_type", 0)%>,<%=numberType%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')">
								     	 
		  						 	   Close 
		  						 	   </div>
		  						 	   <%}if(noSpaceTask.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION && noSpaceTask.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE ){  %>
		  						 	   <div   onclick="exception(<%=entry_trailer[ii].get("check_in_entry_id", 0L)%>,<%=noSpaceTask.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION %>,<%=space.get("resources_id", 0) %>,<%=entry_trailer[ii].get("rel_type",0l)%>,<%=entry_trailer[ii].get("equipment_purpose",0l)%>,<%=entry_trailer[ii].get("equipment_id", 0)%>,<%=space.get("resources_type", 0)%>,<%=numberType%>,'<%=occupyTypeKey.getOccupyTypeKeyName(space.get("resources_type", 0))%>','<%=space.get("resources_id_name", "")%>')" >
		  						 	  Exception
		  						 	   </div>
								     <%
									   }}
									 %>
	  						 	    </div>
	  						 	    <%
						    	   }
					      				if(noSpaceTask.get("number_status",01)==CheckInChildDocumentsStatusTypeKey.CLOSE ){
					      		    %>
					      		     <!--    <span style="font-size:10px; color:#999"><%=CLOSED %></span> -->
							      		     <%if(numberType == ModuleKey.CHECK_IN_LOAD || numberType == ModuleKey.CHECK_IN_ORDER ||  numberType == ModuleKey.CHECK_IN_PONO){ %>
							      		      <a href="javascript:void(0)" onclick="showPalletsInfo('<%=noSpaceTask.get("dlo_detail_id", 0l) %>','<%=noSpaceTask.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Load Info</a>
							      		   	<%}else if(numberType == ModuleKey.CHECK_IN_CTN || numberType == ModuleKey.CHECK_IN_BOL){ %>
						      		   	 	  <a href="javascript:void(0)" onclick="showPalletsInfo('<%=noSpaceTask.get("dlo_detail_id", 0l) %>','<%=noSpaceTask.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive Info</a>
	
						      		   	    <%}else{ %>
							      		   	 	<span style="font-size:10px; color:#999"><%=CLOSED %></span>
							      		   	<%} %>
					      		    <%
					      				}else{
					      					if(noSpaceTask.get("number_status",01)==CheckInChildDocumentsStatusTypeKey.EXCEPTION && (numberType == ModuleKey.CHECK_IN_CTN || numberType == ModuleKey.CHECK_IN_BOL)){
					      			%>
					      					  <a href="javascript:void(0)" onclick="showPalletsInfo('<%=noSpaceTask.get("dlo_detail_id", 0l) %>','<%=noSpaceTask.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive</a>
					      			
					      			<%
					      					}else{
					      		    %>
					      		     	   	 <span attr="<%=entry_trailer[ii].get("check_in_entry_id", 0) %>_<%=noSpaceTask.get("dlo_detail_id",0l) %>" style="font-size:10px; color:#999"><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(noSpaceTask.get("number_status",0)) %></span> 
					      		    <%
					      					}
					      				}
					 	   		 	%>
						    </td>
						</tr>
						<tr><td colspan="5" style="padding-left:5px">
						<!-- 显示warhouse通知的人 -->
						<%
							int detailId = noSpaceTask.get("dlo_detail_id",0);
							DBRow[] notices = checkInMgrWfh.findScheduleByDetailId(detailId, 53);
							if(notices!=null && notices.length>0){
								%>
								<table cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td style="width:80px;padding:0px;color:#999">
										Assign Labor
									</td>
									<td width="125px" style="padding:0px">
										
										<%
										int noticesCount = notices.length;
										noticesCount = noticesCount>1?1:noticesCount;
										for(int noticesIndex=0;noticesIndex<noticesCount;noticesIndex++){
										%>
											<%=notices[noticesIndex].get("schedule_execute_name", "") %>
											
										<%}%>
										
								</td>
								<%String closeName =notices[0].getString("schedule_finish_name");  
								
								if(!StringUtil.isBlank(closeName)){
								%>
								<td style="color:#999;width:80px"><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(notices[0].get("number_status", 0)) %> by</td>
								<td><%=closeName %></td>
								<%} %>
							</tr>
							</table>
							</td></tr>
							<%}
							}  %>
					 </table>
					 </div>
  			 	</fieldset>
  			 	<%} %>
  			 <!-- 显示seal loadBar Take CTNR -->
  			  <fieldset align="left" style="text-align:left;border: 0px solid #930;width:95%;border-radius: 5px;" >
  			  	<!-- seal -->
  			  	<table  width="" cellspacing="0" cellpadding="0" border="0"> 
  			  		<tr >
			  				<%if(entry_trailer[ii].get("rel_type",0)==checkInMainDocumentsRelTypeKey.PICK_UP||entry_trailer[ii].get("rel_type",0)==checkInMainDocumentsRelTypeKey.BOTH){ %>
			  					<td style="text-align:right;width:85px;padding-left: 55px;">PickUp Seal:</td>
			  					<td style="text-align:left;padding: 0;width:74px">&nbsp;<%=entry_trailer[ii].getString("seal_pick_up").equals("")?"NA":entry_trailer[ii].getString("seal_pick_up") %></td>
			  				<%}if(entry_trailer[ii].get("rel_type",0)==checkInMainDocumentsRelTypeKey.DELIVERY||entry_trailer[ii].get("rel_type",0)==checkInMainDocumentsRelTypeKey.BOTH){ %>
			  				<td style="text-align:right;width:85px;">Delivery Seal:</td>
			  				<td style="text-align:left;padding: 0;">&nbsp;<%=entry_trailer[ii].getString("seal_delivery").equals("")?"NA":entry_trailer[ii].getString("seal_delivery") %></td>
			  				<%} %>
  			  		</tr>
  			  	</table>
  			  	<!-- loadBar -->
  			  	<%
  			  		DBRow[] loadBarUse = loadBarUseMgrZr.androidGetLoadBarUse(entry_trailer[ii].get("equipment_id",0),entry_trailer[ii].get("check_in_entry_id", 0));
  			  		if(loadBarUse!=null && loadBarUse.length>0){
  			  			List<DBRow> list = new ArrayList<DBRow>();
  			  			for(DBRow lb:loadBarUse){
  			  				if(lb == null) 
  			  					continue;
  			  				list.add(lb);
  			  			}
  			  			loadBarUse = (DBRow[])list.toArray(new DBRow[0]);
  			  	%>
  			  	<table  width="" cellspacing="0" cellpadding="0" border="0">
		  				<%
		  				for(int i=0;i<loadBarUse.length;i+=2){ 
			  				
		  				%>
		  				<tr>
		  					<td style="color:#aaa;width:55px;padding:0px;">Load Bar</td>
		  					<td style="text-align:right;width:75px;pdding:0px"><%=loadBarUse[i].get("load_bar_name", "") %>:</td>
		  					<td style="padding:0;width: 40px;" >&nbsp;<%=loadBarUse[i].get("count", 0) %> </td>
		  					<%if(loadBarUse.length > i+1){  //判断取第二个是否超出数组长度 
		  						%>
			  				<td style="color:#aaa;width:55px;padding:0px;">Load Bar</td>
		  					<td style="text-align:right;width:75px;padding:0px">
			  						<%=loadBarUse[i+1].get("load_bar_name", "")%>:
			  					
		  					</td>
		  					
		  					<td style="padding:0;width: 40px;">
		  						
		  							&nbsp;<%=loadBarUse[i+1].get("count", 0) %> 
		  						
		  					</td>
		  					<%} %>
		  				</tr>
		  				<%} %>
  			 	</table>
  			 	<%} %>
  			 	
  			  </fieldset>
  			  </fieldset>
  			  <%}} %>
      		</td>
  			
  			
  			<!-- task end  -->
  			<td align="center">
  				<div style="width:205px">
  				<%DBRow[] checkInLog = checkInMgrZwb.findCheckInLog(row.get("dlo_id",0l)); %>
  				<%if(checkInLog.length>0 && checkInLog!=null){ %>
	  				<%for(int mm=0;mm<checkInLog.length;mm++){ %>
	  				    <%if(mm<=2){ %>
	  						<div align="left">
	  							<span style="font-weight: bold;font-size:10px"><%=checkInLog[mm].getString("employe_name") %></span>
	  							
	  							<a href="javascript:void(0)" style="color:green;font-size:10px" onclick="checkInLog('<%=row.get("dlo_id", 0L) %>','<%=showTimePsId%>')">
	  								<span><%=checkInLogTypeKey.getCheckInLogTypeKeyValue(checkInLog[mm].getString("log_type")) %></span>
	  							</a>
	  						</div>
	  						<div style="padding-left:20px;text-align:left;"><%=checkInLog[mm].getString("note") %></div>
	  						<div align="left">
	  						<% 
	  							String operator_time=checkInLog[mm].getString("operator_time");
	  							if(!StringUtil.isBlank(operator_time)){
	  								operator_time = DateUtil.showLocalparseDateTo24Hours(operator_time,showTimePsId);
	  							}
	  						%>
	  							<span style="color:#999"><%=operator_time%></span>
	  						</div>
	  					<%} %>
	  				<%} %>
	  				<%if(checkInLog.length>3){ %>
	  					<div align="right"><a href="javascript:void(0)" style="color:green" onclick="checkInLog('<%=row.get("dlo_id", 0L) %>','<%=showTimePsId%>')">More...</a></div>
	  				<%} %>
  				<%} %>
  				</div>
  			</td>
  			
			<%
				//遍历得到所有设备的状态 已判断是否可以点击按钮
				String _status = "" ;
				if(entry_trailer!=null){
					for(int i=0;i<entry_trailer.length;i++){
						_status += entry_trailer[i].get("equipment_status", 0);
							_status += ",";
					}
				}
				for(int i=0;i<entry_tractor.length;i++){
					_status += entry_tractor[i].get("equipment_status", 0);
					if(i!=entry_tractor.length-1){
						_status += ",";
					}
				}
			%>
    		
      	</tr>
		<tr class="split">
  			<td colspan="6" style="height:28px;line-height:24px;background-image:url('../imgs/linebg_24.jpg');text-align:right;padding-right:20px;">
	  			<input type="button" class="long-button"  value="PDF Download" onClick="openPdfDownload(<%=row.getString("dlo_id") %>)"/>	&nbsp;&nbsp;
	  		<!-- 	<input type="button" value="Plates" class="long-button" onclick="showPlates('<%=row.getString("dlo_id") %>');" />&nbsp;&nbsp; -->
	  			<input type="button" class="long-button-print"  value="  Entry Print" onClick="openPrint(<%=row.getString("dlo_id") %>)"/>	&nbsp;&nbsp;
  				<input type="button" class="long-button-print"  value="BOL Print" onClick="openBOLPrint(<%=row.getString("dlo_id") %>)"/>	&nbsp;&nbsp;
  				<input type="button" value="Photos" class="long-button" onclick="showPictrueOnline('<%=FileWithTypeKey.OCCUPANCY_MAIN%>','<%=row.getString("dlo_id") %>','');" />&nbsp;&nbsp;
   				<tst:authentication bindAction="com.cwc.app.api.zyj.CheckInMgrZyj.webCheckInWindowAddOrUpdateTask">
  					<input type="button" value="Window" class="long-button"  onclick="checkInWindowNew('<%=row.getString("dlo_id") %>','<%=row.getString("zone_id") %>','<%=_status %>')" />&nbsp;&nbsp; 
  					<!-- <input type="button" value="WindowCheckIn" class="long-button"  onclick="checkInWindow('<%=row.getString("dlo_id") %>','<%=row.getString("zone_id") %>','<%=_status %>','<%=row.getString("isLive")%>')" />&nbsp;&nbsp; --> 
  				</tst:authentication>
<!-- 		<tst:authentication bindAction="com.cwc.app.api.zwb.CheckInMgrZwb.addFiles">
	  				<input type="button" value="WindowCheckOut" class="long-button" onclick="windowCheckOut(<%=row.getString("dlo_id") %>,'<%=_status %>','<%=row.getString("isLive")%>')"  />&nbsp;&nbsp;
	  			</tst:authentication> -->  		
	  			<input type="button" value="Assign Task" class="long-button" onclick="warehouseCheckIn(<%=row.getString("dlo_id") %>,'<%=_status %>',<%=row.get("load_count",0l) %>,0)" />&nbsp;&nbsp;
	  			<tst:authentication bindAction="com.cwc.app.api.zwb.CheckInMgrZwb.gateCheckInAdd">	 
	       			<input name="Submit3" type="button" class="long-button" value="Gate Edit" onClick="modCheckIn(<%=row.getString("dlo_id") %>,'<%=_status %>', '<%=row.getString("window_check_in_time")%>', '<%=row.getString("warehouse_check_in_time")%>')"/>&nbsp;&nbsp;
	       		</tst:authentication>
	  			<tst:authentication bindAction="com.cwc.app.api.zwb.CheckInMgrZwb.checkOut">
	  				<input type="button" value="Gate Check Out" class="long-button" onclick="outfangfa('<%=row.getString("dlo_id") %>','<%=entry_tractor[0].getString("equipment_status") %>')" id="checkOut" />&nbsp;&nbsp;   	    
	  			</tst:authentication>
  			</td>
  	    </tr>
      	<% 		
      		}
      	}else{
      	%>
      		<tr style="background:#E6F3C5;">
   						<td colspan="10" style="text-align:center;height:120px;">No Data</td>
   		    </tr>
      	<% }%>
	</table>
	<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" action="check_in_index.html">
	    <input type="hidden" name="p"/>
	    <input type="hidden" name="entryType"  value="<%=entryType%>"/>
	    <input type="hidden" name="mainStatus"  value="<%=mainStatus%>"/>
	    <input type="hidden" name="tractorStatus"  value="<%=tractorStatus%>"/>
		<input type="hidden" name="numberStatus" value="<%=numberStatus%>"/>
		<input type="hidden" name="ps_id" value="<%=ps_id%>"/>
		<input type="hidden" name="main_id" value="<%=main_id%>"/>
		<input type="hidden" name="key" value="<%=key %>"/>
		<input type="hidden" name="cmd" value="<%=cmd %>"/>
		<input type="hidden" name="search_mode" value="<%=search_mode %>"/>
		<input type="hidden" name="start_time" value="<%=start_time %>"/>
		<input type="hidden" name="end_time" value="<%=end_time %>"/>
		<input type="hidden" name="tabSelect" value="<%=tabSelect %>" />
		<input type="hidden" name="loadBar" value="<%=loadBar %>" />
		<input type="hidden" name="priority" value="<%=priority %>" />
		<input type="hidden" name="waitingType" value="<%=waitingType %>" />
		<input type="hidden" name="comeStatus" value="<%=comeStatus %>" />
		<input type="hidden" name="purposeStatus" value="<%=purposeStatus %>" />
		<input type="hidden" name="customer_id" value="<%=customer_id %>" />
		
	  </form>
	  <tr>
	    <td height="32px" align="right" valign="middle" class="turn-page-table" style="border-bottom: 1px solid #999;border-top: 1px solid #999;padding:0;">
	        <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("Page：<span style='color:#0076FF;'>" + pc.getPageNo() + "</span>/" + pc.getPageCount() + " &nbsp;&nbsp;Total：<span style='color:#0076FF;'>" + pc.getAllCount() + " &nbsp;&nbsp;</span>");
				if(pc.isFirst()){
					out.println("<div onclick=\"javascript:go(" + 1 + ")\" class=\"page-frist\">&nbsp;</div>");					
				}else{
					out.println("<div class=\"page-first-none\">&nbsp;</div>");
				}
		//		out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
				if(pc.isFornt()){
					out.println("<div onclick=\"javascript:go(" + pre + ")\" class=\"page-up\">&nbsp;</div>");
				}else{
					out.println("<div class=\"page-up-none\">&nbsp;</div>");
				}
		//		out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
				//显示五个页码  -----
				if(pc.getPageNo()-2>0){
					out.println("<div onclick=\"javascript:go("+(pc.getPageNo()-2)+")\" class=\"page-no\" style=\"text-align: center;cursor:pointer;\">"+(pc.getPageNo()-2)+"</div>");
				}
				if(pc.getPageNo()-1>0){
					out.println("<div onclick=\"javascript:go("+(pc.getPageNo()-1)+")\" class=\"page-no\" style=\"text-align: center;cursor:pointer;\">"+(pc.getPageNo()-1)+"</div>");
				}
				//当前页
				out.println("<div class=\"page-no\" style=\"text-align: center; background:#007aff;color:#FFf\">"+(pc.getPageNo())+"</div>");
				
				if(pc.getPageNo()+1<=pc.getPageCount()){
					out.println("<div onclick=\"javascript:go("+(pc.getPageNo()+1)+")\" class=\"page-no\" style=\"text-align: center;cursor:pointer;\">"+(pc.getPageNo()+1)+"</div>");
				}
				if(pc.getPageNo()+2<=pc.getPageCount()){
					out.println("<div onclick=\"javascript:go("+(pc.getPageNo()+2)+")\" class=\"page-no\" style=\"text-align: center;cursor:pointer;\">"+(pc.getPageNo()+2)+"</div>");
				}
				if(pc.isNext()){
					out.println("<div onclick=\"javascript:go(" + next + ")\" class=\"page-next\" >&nbsp;</div>");
				}else{
					out.println("<div class=\"page-next-none\">&nbsp;</div>");
				}
		//		out.println(HtmlUtil.aStyleLink("gop",">","javascript:go(" + next + ")",null,pc.isNext()));
				if(pc.isLast()){
					out.println("<div onclick=\"javascript:go(" + pc.getPageCount() + ")\" class=\"page-last\" >&nbsp;</div>");
				}else{
					out.println("<div class=\"page-last-none\">&nbsp;</div>");
				}
		//		out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
			<div style="display: inline-block;margin:0;padding:0;background-image:url('./imgs/page-background.png');text-align: center;line-height: 31px;border-left: 1px solid #888;height: 30px; width: 112px;">
	      Go to 
	     	<div style="padding:0;margin:0;background: white;border-radius: 5px;display: inline-block; height: 20px;border:1px solid #097EFF;">
	     		
	     		<input onkeydown="if(event.keyCode==13)(go(document.getElementById('jump_p2').value))" style="text-align:center;color:#0477a2;border:0px;width:28px;border-radius: 5px; name="jump_p2" type="text" id="jump_p2" value="<%=pc.getPageNo()%>"/><input
	     		 name="Submit22" type="button" style="border-radius: 0px 2px 2px 0px;height:20px;width:28px;padding-top:0px;background-image:url('./imgs/page-btn-background.png');border:0px;width: 28px;background-position: -19px -10px;color: white;cursor:pointer;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
	      	</div>
	      </div>
	    </td>
	  </tr>
   </table>
  
	<form action="check_in_index.html" method="post" name="search_form">
		<input type="hidden" name="key" />
		<input type="hidden" name="cmd" value="search"/>
		<input type="hidden" name="search_mode"/>
	</form>
	<form action="check_in_index.html" method="post" name="filter_form">
		<input type="hidden" name="entryType"/>
		<input type="hidden" name="mainStatus"/>
		<input type="hidden" name="numberStatus" />
		<input type="hidden" name="tractorStatus" />
		<input type="hidden" name="ps_id"/>
		<input type="hidden" name="main_id"/>
		<input type="hidden" name="cmd" value="filter"/>
		<input type="hidden" name="tabSelect" value="1" />
		<input type="hidden" name="start_time"/>
		<input type="hidden" name="end_time"/>
		<input type="hidden" name="loadBar"/>
		<input type="hidden" name="priority"/>
		<input type="hidden" name="waitingType"/>
		<input type="hidden" name="comeStatus"/>
		<input type="hidden" name="purposeStatus"/>
		<input type="hidden" name="customer_id"/>
	</form>
	<form action="" name="download_form" id="download_form">
	</form>
	<!-- 右键菜单样式
	<div id="menu" style="width:auto;">
		<ul>
			<li id="showWebcam" onclick="showWebcam()" ><a href="#">View Webcam</a>
			</li>
			<li onclick ="openWebcamWind()"><a href="#">Set Webcam</a>
			</li>
		</ul>
	</div>
	 -->
</body>
</html>
<script>
function moreTaskAppointment(entry_id){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/getTaskAppointment.action',
		data:'entry_id='+entry_id,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    
		},
		success:function(data){
			
			var html = '';			
			html += '<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:90%;">'
  				+'<legend style="font-size:14px;font-weight:normal;color:#999999;">Appointment Time</legend>'
  				+'<table>';
			if(data!=null&&data.length>0){
				for(var i=0;i<data.length;i++){
					html+='<tr style="height: 20px;">';
					if(data[i].number_type==<%=moduleKey.CHECK_IN_LOAD%>||data[i].number_type==<%=moduleKey.CHECK_IN_PONO%>||data[i].number_type==<%=moduleKey.CHECK_IN_ORDER%>||data[i].number_type==<%=moduleKey.CHECK_IN_PICKUP_ORTHERS %>)
						html += '<td style="text-align:right;color:blue">'+data[i].number_type_name+':</td>';
					else if(data[i].number_type==<%=moduleKey.CHECK_IN_BOL%>||data[i].number_type==<%=moduleKey.CHECK_IN_CTN%>||data[i].number_type==<%=moduleKey.CHECK_IN_DELIVERY_ORTHERS%>)
						html += '<td style="text-align:right;color:red">'+data[i].number_type_name+':</td>';
					html += '<td width="100px">'+data[i].number+'</td>'
						+'<td>'+data[i].appointment_time+'</td>'
						+'<td style="padding-left:5px">'+data[i].come_status_name
						+'</td></tr>';
					}
			}
  			html += '</table>'
  				+'</fieldset>';
  				
  			$.artDialog({
			    content: html,
			    lock:true,
			    width: 450,
			    height: 250,
			    title:'Appointment Time',
			});
		},
		error:function(){
			alert("System error"); 
		}
	});
}
function showPalletsInfo(detail_id,number_type){
	if(number_type == '<%= moduleKey.CHECK_IN_LOAD%>' * 1){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/load.html?dlo_detail_id='+detail_id; 
		$.artDialog.open(uri , {title: "Load Pallet Info",width:740,height:500, lock: true,opacity: 0.3,fixed: true});
	}else if(number_type == '<%= moduleKey.CHECK_IN_CTN %>' * 1 || number_type == '<%= moduleKey.CHECK_IN_BOL %>' * 1){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/receiveInfo.html?dlo_detail_id='+detail_id; 
		$.artDialog.open(uri , {title: "Receive Pallet Info",width:740,height:500, lock: true,opacity: 0.3,fixed: true});
	 
	}else if(number_type == '<%= moduleKey.SMALL_PARCEL %>'){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/small_parcel_order.html?dlo_detail_id='+detail_id; 
		$.artDialog.open(uri , {title: "Load Pallet Info",width:740,height:500, lock: true,opacity: 0.3,fixed: true});
	}else{
 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/load_order.html?dlo_detail_id='+detail_id; 
		$.artDialog.open(uri , {title: "Load Pallet Info",width:740,height:500, lock: true,opacity: 0.3,fixed: true});
	}
}

function showMenu(evt,dlo_id,type){
	$("<scri"+"pt>"+"</scr"+"ipt>").attr({src:'../js/easyui/jquery.easyui.menu.js',type:'text/javascript',id:'load'}).appendTo($('head').remove('#loadScript'));
 	var x = $(evt).offset().left;
	var y = $(evt).offset().top;
	 	 
	$('#menubutton_'+dlo_id+'_'+type).menu('show', { 
		left: x, 
		top: y+20
	});
}
function checkInLog(dlo_id,ps_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/Check_in_log.html?dlo_id='+dlo_id+'&ps_id='+ps_id; 
	$.artDialog.open(uri , {title: "Check In Log",width:'900px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}
function gateCheckIn(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_check_in.html'; 
	$.artDialog.open(uri , {title: "Gate Check In",width:'950px',height:'90%', lock: true,opacity: 0.3,fixed: true});
}

function checkInWindow(main_id,zone_id,status,islive){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/ajaxFindEquipment.action',
		data:{entry_id:dlo_id},
		dataType:'json',
		type:'post',
		success:function(data){
			if(data!=null&&data.length>0){
				var flag = true;	
				for(var i=0;i<data.length;i++){
					var status = data[i].equipment_status;
					if(status!=5){  //5为设备离开 
						flag = false;
						break;
					}
				}
				if(flag){
					alert("Has checked out by gate!");
					return false;
				} 
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window.html?id='+main_id+'&zoneId='+zone_id; 
				$.artDialog.open(uri , {title: "Window Check In",width:'820px',height:'90%', lock: true,opacity: 0.3,fixed: true});
				
			}else{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window.html?id='+main_id+'&zoneId='+zone_id; 
				$.artDialog.open(uri , {title: "Window Check In",width:'820px',height:'90%', lock: true,opacity: 0.3,fixed: true});
			}
		},
		error:function(){
			alert("System error");
		}
	});
	<%-- status = status.split(",");
	var flag = true;
	for(var i=0;i<status.length;i++){
		if(status[i]!=5){
			flag = false;
			break;
		}
	}
	if(flag){
		alert("The documents have been check out!");
		return false;
	}
	
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window.html?id='+main_id+'&zoneId='+zone_id; 
	$.artDialog.open(uri , {title: "WindowCheckIn",width:'820px',height:'540px', lock: true,opacity: 0.3,fixed: true}); --%>
}

function warehouseCheckIn(id,status,load_count,rel_type){

	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/ajaxFindEquipment.action',
		data:{entry_id:id},
		dataType:'json',
		type:'post',
		success:function(data){
			if(data!=null&&data.length>0){
				var flag = true;	
				for(var i=0;i<data.length;i++){
					var status = data[i].equipment_status;
					if(status!=5){  //5为设备离开 
						flag = false;
						break;
					}
				}
				if(flag){
					alert("Has checked out by gate!");
					return false;
				}
				if(load_count==0 && rel_type==2){     //pick up  但是 没有load number
					alert("Please check in office");
					return false;
				}
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_warehouse.html?infoId='+id; 
				$.artDialog.open(uri , {title: "Assign Task",width:'50%',height:'80%', lock: true,opacity: 0.3,fixed: true});
				
			}else{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_warehouse.html?infoId='+id; 
				$.artDialog.open(uri , {title: "Assign Task",width:'50%',height:'80%', lock: true,opacity: 0.3,fixed: true});
			}
		},
		error:function(){
			alert("System error");
		}
	});
	
	<%-- 
	status = status.split(",");
	var flag = true;
	for(var i=0;i<status.length;i++){
		if(status[i]!=5){
			flag = false;
			break;
		}
	}
	if(flag){
		alert("The documents have been check out!");
		return false;
	}
	if(load_count==0 && rel_type==2){     //pick up  但是 没有load number
		alert("Please check in office");
		return false;
	}

	
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_warehouse.html?infoId='+id; 
	$.artDialog.open(uri , {title: "WarehouseCheckIn",width:'50%',height:'80%', lock: true,opacity: 0.3,fixed: true}); --%>
	
	
}



function selectDialog(uri,title1){
	$.artDialog.open(uri , {title: title1,width:'750px',height:'620px', lock: true,opacity: 0.3,fixed: true});
}

function modCheckIn( dlo_id, status, window_time, ware_time ){
	// 判断 window 是否发了 schedule
	$.ajax( {
  		type: 'post',
  		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCanOrNoGateEditAction.action',
  		dataType: 'json',
  		data: 'entry_id=' + dlo_id,
		error: function( serverresponse, status ) {
			showMessage( "System error", "alert" );
  		},
	 	success: function( data ) {
	 		if( data != null && data.result ) {
	 			if ( data.result == "no" ) {
	 				alert( "Has checked in by window!" );
	 			} else if ( data.result == "can" ) {
	 				gateEdit( dlo_id, status, window_time, ware_time );
	 			}
	 		}
	 	}
	} );

}

// 抽离出 gateEdit 的方法
function gateEdit( dlo_id, status, window_time, ware_time ) {
	// 只有车头离开就不让 checkcout 操作
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/ajaxFindEquipment.action',
		data:{entry_id:dlo_id},
		dataType:'json',
		type:'post',
		success:function(data){
			if(data!=null&&data.length>0){
				var flag = true;	
				for(var i=0;i<data.length;i++){
					var status = data[i].equipment_status;
					if(status!=5){  //5为设备离开 
						flag = false;
						break;
					}
				}
				if(flag){
					alert("Has checked out by gate!");
					return false;
				}
				if(ware_time != '')
				{
					alert("Has checked in  by warehouse!");
					return false;
				}
				if(window_time != '')
				{
					alert("Has checked in  by window!");
					return false;
				}
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_check_in.html?mainId='+dlo_id; 
				$.artDialog.open(uri , {title: "Edit",width:'950px',height:'80%', lock: true,opacity: 0.3,fixed: true});
				
			}else{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_check_in.html?mainId='+dlo_id; 
				$.artDialog.open(uri , {title: "Edit",width:'950px',height:'80%', lock: true,opacity: 0.3,fixed: true});
			}
		},
		error:function(){
			alert( "System error" );
		}
	});

	<%-- status = status.split(",");
	var flag = true;
	for(var i=0;i<status.length;i++){
		if(status[i]!=5){
			flag = false;
			break;
		}
	}
	if(flag){
		alert("The documents have been check out!");
		return false;
	}
	if(ware_time != '')
	{
		alert("The documents have been warehouse check in!");
		return false;
	}
	if(window_time != '')
	{
		alert("The documents have been window check in!");
		return false;
	}
	
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_check_in.html?mainId='+dlo_id; 
	$.artDialog.open(uri , {title: "Edit",width:'950px',height:'80%', lock: true,opacity: 0.3,fixed: true}); --%>
}
function openPdfDownload(dlo_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_pdf_download.html?infoId='+dlo_id; 
	$.artDialog.open(uri , {title: "Pdf Download",width:'700px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}
function openPrint(dlo_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>check_in/check_in_open_print.html?main_id='+dlo_id; 
	$.artDialog.open(uri , {title: "Print",width:'800px',height:'550px', lock: true,opacity: 0.3,fixed: true});
}
function openBOLPrint(dlo_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_bol_print.html?infoId='+dlo_id; 
	$.artDialog.open(uri , {title: "BOL Print",width:'700px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}
function moreNotices(dlo_id,associate_process){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/more_notices.html?dlo_id='+dlo_id+'&associate_process='+associate_process; 
	$.artDialog.open(uri , {title: "Notices",width:'1000px',height:'350px', lock: true,opacity: 0.3,fixed: true});
}
function detailNotices(schedule_sub_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/detail_notices.html?schedule_sub_id='+schedule_sub_id; 
	$.artDialog.open(uri , {title: "Notice",width:'1000px',height:'150px', lock: true,opacity: 0.3,fixed: true});
}

function outfangfa(infoId,tractor_status){
	
	//只有车头离开就不让checkcout操作
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/ajaxFindEquipment.action',
		data:{entry_id:infoId,equipment_type:1},
		dataType:'json',
		type:'post',
		success:function(data){
			if(data!=null&&data.length>0){
				var flag = true;	
				for(var i=0;i<data.length;i++){
					var status = data[i].equipment_status;
					if(status!=5){  //5为设备离开 
						flag = false;
						break;
					}
				}
				if(flag){
					alert("Has checked out by gate!");
					return false;
				}
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_check_out.html?infoId='+infoId; 
				$.artDialog.open(uri , {title: "Gate Check Out",width:'1050px',height:'568px', lock: true,opacity: 0.3,fixed: true});
				
			}else{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_check_out.html?infoId='+infoId; 
				$.artDialog.open(uri , {title: "Gate Check Out",width:'1050px',height:'568px', lock: true,opacity: 0.3,fixed: true});
			}
		},
		error:function(){
			alert("System error");
		}
	});
	
	<%-- if(tractor_status==5){
		alert("The documents have been check out!");
		return false;
	}
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_check_out.html?infoId='+infoId; 
	$.artDialog.open(uri , {title: "GateCheckOut",width:'740px',height:'480px', lock: true,opacity: 0.3,fixed: true}); --%>
  // alert("释放本单据所有门");
 
}


//图片在线显示  		 
function showPictrueOnline(fileWithType,fileWithId ,currentName){
    var obj = {
  		file_with_type:fileWithType,
  		file_with_id : fileWithId,
  		current_name : currentName ,
  		cmd:"multiFile",
  		table:'file',
  		//base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/check_in"
  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "_fileserv/file/"
	}
    //if(window.top && window.top.openPictureOnlineShow){
	//	window.top.openPictureOnlineShow(obj);
	//}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	//}
}
function waiting(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_waiting_all_list.html'; 
	$.artDialog.open(uri , {title: "Waiting List",width:'750px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}

function down(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var start_time='<%=start_time%>';
	var end_time='<%=end_time%>';
	var entryType = '<%=entryType.equals("")?0:entryType%>'
	var mainStatus='<%=mainStatus.equals("")?0:mainStatus%>';
	var tractorStatus='<%=tractorStatus.equals("")?0:tractorStatus%>';
	var numberStatus='<%=numberStatus.equals("")?0:numberStatus%>';
	var loadBar='<%=loadBar%>';
	var priority=<%=priority%>;
	var waitingType='<%=waitingType%>';
	var comeStatus='<%=comeStatus%>';
	var purposeStatus='<%=purposeStatus%>';
	var ps_id=<%=ps_id%>;
	var para='start_time='+start_time+'&end_time='+end_time+'&mainStatus='+mainStatus+'&numberStatus='
	+numberStatus+'&ps_id='+ps_id+'&tractorStatus='+tractorStatus+'&entryType='+entryType+'&loadBar='+loadBar
	+'&priority='+priority+'&waitingType='+waitingType+'&comeStatus='+comeStatus+'&purposeStatus='+purposeStatus;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/CheckInExportAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();       //遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}
				
		},
		error:function(){
			$.unblockUI();       //遮罩关闭
			alert("System error"); 
		}
	});
}

function checkInWindowNew(entry_id,zone_id,status,tractor_status){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/ajaxFindEquipment.action',
		data:{entry_id:entry_id},
		dataType:'json',
		type:'post',
		success:function(data){
			if(data!=null&&data.length>0){
				var flag = true;	
				for(var i=0;i<data.length;i++){
					var status = data[i].equipment_status;
					if(status!=5){  //5为设备离开 
						flag = false;
						break;
					}
				}
				if(flag){
					alert("Has checked out by gate!");
					return false;
				}
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_main.html?entry_id='+entry_id; 
				
				$.artDialog.open(uri,{title:"Window Check In E"+entry_id,width:'1000px',height:'540px',lock:true,opacity:0.3,fixed:true,close:function(){if(monitorCheckInWindowValue){location.reload();}}});
				
			}else{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_main.html?entry_id='+entry_id; 
				
				$.artDialog.open(uri,{title:"Window Check In E"+entry_id,width:'1000px',height:'540px',lock:true,opacity:0.3,fixed:true,close:function(){if(monitorCheckInWindowValue){location.reload();}}});
			}
		},
		error:function(){
			alert("System error");
		}
	});
	<%-- status = status.split(",");
	var flag = true;
	for(var i=0;i<status.length;i++){
		if(status[i]!=5){
			flag = false;
			break;
		}
	}
	if(flag){
		alert("The documents have been check out!");
		return false;
	}
	
	//TODO 后台对状态的处理
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_main.html?entry_id='+entry_id; 
	
	$.artDialog.open(uri,{title:"WindowCheckIn E"+entry_id,width:'1000px',height:'540px',lock:true,opacity:0.3,fixed:true,close:function(){if(monitorCheckInWindowValue){location.reload();}}}); --%>
}

var monitorCheckInWindowValue = false;

function monitorCheckInWindowChanged(){
	monitorCheckInWindowValue = true;
}
</script>
