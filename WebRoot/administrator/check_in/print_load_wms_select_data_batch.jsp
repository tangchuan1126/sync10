<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>

<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<html>
<head>

<%
String loadNo = StringUtil.getString(request, "loadNo");
long entry_id = StringUtil.getLong(request, "entry_id");
String door_name = StringUtil.getString(request, "door_name");
String type = StringUtil.getString(request, "type");
int number_type=StringUtil.getInt(request, "number_type");

DBRow[] rows = sqlServerMgr.findLoadCompanyIdCustomerIdByLoadNo(loadNo,0L, request);
if(0 == rows.length)
{
	rows = sqlServerMgr.findOrderCompanyIdCustomerIdByLoadNo(loadNo,0L, request);
}
  
%>
<title>select load</title>
<script type="text/javascript">
function selectLoad(CompanyID, loadNo, CustomerID, obj,number_type)
{
	$("input:radio", $(obj)).attr("checked", true);
	 //$.artDialog && $.artDialog.close();
	 //$.artDialog.opener.setParentCompanyLoadCustomer  && $.artDialog.opener.setParentCompanyLoadCustomer(CompanyID, loadNo, CustomerID,'<%=entry_id%>','<%=door_name%>','<%=type%>',number_type);
}
function changeCursor(obj)
{
	 $(obj).css("cursor","Pointer"); 
}
function submitLoadtsSel()
{

}
</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		<tr height="40px">
			<th width="5%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">&nbsp;</th>
			<th width="20%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">CompanyID</th>
	        <th width="35%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">LoadNo</th>
	        <th width="40%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">CustomerID</th>
		</tr>
		<%
		if(rows.length > 0)
		{
			for(int i = 0; i < rows.length; i ++)
			{
		%>	
		<tr height="35px" onclick="selectLoad('<%=rows[i].getString("CompanyID") %>','<%=rows[i].getString("LoadNo") %>','<%=rows[i].getString("CustomerID") %>', this,<%=number_type %>)" onmouseover="changeCursor(this)">
			<td>
				<input type="radio" name="radio<%=rows[i].getString("LoadNo") %>" <%=i==0?"checked='checked'":"" %>
				number="<%=rows[i].getString("LoadNo")%>"
				companyId="<%=rows[i].getString("CompanyID") %>"
				customerId="<%=rows[i].getString("CustomerID") %>"
				dataType="<%=type.toUpperCase()%>">
			</td>
			<td>
				<%=rows[i].getString("CompanyID") %>
			</td>
			<td>
				<%=rows[i].getString("LoadNo") %>
			</td>
			<td>
				<%=rows[i].getString("CustomerID") %>
			</td>
		</tr>		
		<%
			}
		}else{
		%>
			<tr>
				<td colspan="4" style="text-align:center;line-height:50px;height:50px;background:#E6F3C5;border:1px solid silver;">无数据</td>
			</tr>
		<%} %>
	</table>
</body>
</html>
