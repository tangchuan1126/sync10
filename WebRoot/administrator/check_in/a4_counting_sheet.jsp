<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<%@ include file="../../include.jsp"%>
<%
	long entry_id = StringUtil.getLong(request, "entry_id");
    if(entry_id == 0l){
    	entry_id = StringUtil.getLong(request, "entryId");
    }
	String jsonString = StringUtil.getString(request, "jsonString");
	JSONArray jsons = new JSONArray(jsonString);
	DBRow mainRow=checkInMgrZwb.findGateCheckInById(entry_id);
	int print = StringUtil.getInt(request, "isprint") ; 
	String printName=StringUtil.getString(request,"print_name");
	String containerNo=StringUtil.getString(request,"containerNo");
	String appointmentDate=StringUtil.getString(request,"appointmentDate");
	long adid  = StringUtil.getLong(request, "adid");
	long occupy_type = StringUtil.getLong(request, "resources_type");
%>
<html>
  <head>
    <title>tally_sheet</title>
    <%if(print <= 0){ %>
	<!--  基本样式和 javascript -->
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<!-- 引入  Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- jquery UI 加上 autocomplete -->
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<!-- 打印 -->
	<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
	<script type="text/javascript" src="../js/print/m.js"></script>
	<%} %>
	<script type="text/javascript">
		function print(){
		     var printHtml=$('div[name="printHtml"]');
		     for(var i=0;i<printHtml.length;i++){
		    	 var count = $('input[name="cLength"]',printHtml[i]).val();//分几页显示
// 		    	 var resultLength = $('input[name="resultLength"]',printHtml[i]).val();//总数据量
		    	 var a1=$('#a1',printHtml[i]);
		    	 var a2=$('#a2',printHtml[i]);
		    	 var a3=$('#a3',printHtml[i]);
		    	 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");			// 设置页码字体
		    	 visionariPrinter.SET_PRINT_PAGESIZE(1, 0, 0, "Letter");
		    	 
		    	 visionariPrinter.ADD_PRINT_HTM(0, 0, "100%", "100%", a1.html());
		    	 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头
		    	 
	    		 for(var i=0; i<count;i++){
		    		var id = "#c"+i;
		    		var cHtml = $(id,printHtml[i]);
					visionariPrinter.ADD_PRINT_TABLE(100,0,"100%","270mm",cHtml.html());
					visionariPrinter.NewPageA();//执行该函数之后所增加的内容会在前面内容的尾页之后新建一页输出	,前面无内容时，仍然从第一页开始。 
		    	 }
		    	 
		    	 visionariPrinter.ADD_PRINT_TABLE(100,0,"100%","270mm",a2.html());
		    	 
		    	 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",a3.html());
		    	 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1); 
		    	 
		    	 visionariPrinter.ADD_PRINT_TEXT("1.6cm",670,"100%","100%","PAGE:#/&"); 
		    	 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
		    	 
		    	 
			 
		    	 visionariPrinter.SET_PRINT_COPIES(1);
// 		    	 visionariPrinter.PREVIEW();
			     visionariPrinter.PRINT(); 
		     }		 
		}
	</script>
  </head>
  
  <body>
   <!-- US letter 标准 216mm X 279mm-->
   <%if(print != 1  ){%>
    <div style="width: 216mm;border: 1px #dddddd solid; background: #eeeeee; -webkit-border-radius: 7px; -moz-border-radius: 7px; margin: 0 auto; text-align: right;">
      <input class="short-short-button-print" type="button" onClick="print()" value="Print" />
   </div>
 	<%} %>
   <%
	   long ps_id = 0;
   
	   if(mainRow!=null){
			ps_id = mainRow.get("ps_id",0l);
	   }
// 	   if(adid == 0l){
// 	   	   AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
// 	       adid = adminLoggerBean.getAdid();
// 	       ps_id = adminLoggerBean.getPs_id();
// 	   }else{
// 	   	   ps_id = adminMgr.getDetailAdmin(adid).get("ps_id",0l);
// 	   }
	   
	   appointmentDate=DateUtil.showLocalparseDateTo24Hours(appointmentDate,ps_id);//将预约时间转换为仓库所在地时间，（格式：01/09/2015 10:00）
	   
	    for(int x=0;x<jsons.length();x++){
	    	JSONObject json=jsons.getJSONObject(x);
	    	String loadNo = "";
	    	String ctnr = "";
	    	String bol = "";
	    	
	    	String number=json.getString("number");
	    	String door_name = json.getString("door_name");
	    	String checkDataType=json.getString("checkDataType");
	    	int number_type = json.getInt("number_type");

	    	
	    	if(number_type == ModuleKey.CHECK_IN_CTN){
	    		ctnr=number;
	    	}
	    	if(number_type == ModuleKey.CHECK_IN_BOL){
	    		bol=number;
	    	}
	    	if(number_type == ModuleKey.CHECK_IN_LOAD){
	    		loadNo=number;			
	    	}
	    	
	    	String companyId = json.getString("companyId");
			String customerId = json.getString("customerId");
			DBRow[] result = checkInMgrZwb.getOrderItemRowOfCountingSheet(loadNo, companyId, customerId, adid, request);
// 			System.out.println(StringUtil.convertDBRowArrayToJsonString(result));
			int rows = 40;    // 多少行
   		  	int cols = 3;     // 多少列
   			//未超过一页数据
	        if(result.length <= 90){
	        	rows = 30;    // 多少行
	        }
   		  	
   		 	long orderNo = 0;
			double pageLength = ((result.length-90)/(double)(rows*cols))+1;
			double cLength = (result.length-90)/(double)(rows*cols);
// 			double rowsLength = result.length/(double)(rows*cols);
// 			System.out.println("rowsLength="+rowsLength+"   result="+result.length);
	%>
   <div style="border: 1px red solid; width: 210mm; margin: 0 auto;" name="printHtml">
   	<input type="hidden" name="cLength" value="<%=cLength%>">
<%--    	<input type="hidden" name="resultLength" value="<%=result.length%>"> --%>
     <div id="a1" style="width: 210mm";>
       <table width="100%" border="0" cellspacing="0" cellpadding="0" border-collapse="collapse">
      	<tr>
      		<td width="50%" height="25px" valign="middle" align="left" style="font-size: 12px; font-family: Verdana; font-weight: bold; text-decoration: underline;">LOAD:&nbsp;#<span><%=loadNo %></td>
      		<td width="50%" height="25px" valign="middle" align="right" style="font-size: 12px; font-family: Verdana; font-weight: bold; text-decoration: underline;padding-right:50px;">
      			<%if(OccupyTypeKey.SPOT==occupy_type){ %>
					SPOT:&nbsp;#<%=door_name != null ? door_name : "" %>
				<%}else{ %>
					DOCK:&nbsp;#<%=door_name != null ? door_name : "" %>
				<% }%>
      		</td>
      	</tr>
      	<tr>
      		<td width="50%" height="25px" valign="middle" align="left" style="font-size: 12px; font-family: Verdana; font-weight: bold; text-decoration: underline;">Trailer Number:&nbsp;#<%=containerNo%></td>
      		<td width="50%" height="25px" valign="middle" align="right" style="font-size: 12px; font-family: Verdana; font-weight: bold; text-decoration: underline;padding-right:50px;">Appointment Date:&nbsp;<%=appointmentDate%></td>
      	</tr>
      </table>
<!--       <div style="height: 5px; width: 210mm; clear: both;">&nbsp;</div> -->
        <div style="text-align: center;">
          <span style="font-size: 14px; font-family: Verdana; font-weight: bold">Carrier/Drivers Tally Sheet<br />
          Tally Sheet Count is Mandatory</span>
        </div>
      </div>
    <!--   a1 end   -->
    
     <!-- cm start 超过90个，一页纸显示不下 -->
    <%if(result.length>90){
    	for(int m=0; m<cLength; m++){ %>
	    	<div id="c<%=m%>" style="margin-top: 3px; width: 210mm;">
	    		<table id="tally_sheet_c<%=m%>" width="100%" border="0" cellspacing="0" cellpadding="0" border-collapse="collapse">
	    			<thead>
			           <tr>
			             <td  width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left:3px solid black;border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           RefNo
				         </td>
			             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black;border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           Plts
				         </td>  
			             <td width="9%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black; border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           Pcs
				         </td>  
			             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black; border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           Model
				         </td>  
			             <td  width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-top: 3px solid black;border-left: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           RefNo
				         </td>
			             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black;border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           Plts
				         </td>  
			             <td width="9%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black;border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           Pcss
				         </td> 
			             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black; border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           Model
				         </td> 	          
			             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 3px solid black; border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           RefNo
				         </td>
			             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black;border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           Plts
				         </td>  
			             <td width="9%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black;border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
				           Pcs
				         </td>  
			             <td width="9%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black; border-top: 3px solid black; border-bottom: 3px solid black; border-right: 3px solid black;">
				           Model
				         </td> 	         
				       </tr>   
			         </thead> 
	    			<%for(int i = 1; i < rows+1; i++ ) {%> 
	    			<tr>
	    				<%
							 for(int j = 0; j < cols; j++ ){
				 		 %>
				 		 <td width="8%" height="18px" valign="middle" align="center" style="padding-bottom: 3px; font-size: 12px; font-family: Verdana;border-bottom:1px solid black;border-left: 3px solid black;<% if(i == rows) {%>border-bottom: 3px solid black;	<%}%> ">
				             &nbsp; 	         
					       </td> 
				           <td  masterwidth="8%" height="18px" align="center" style="font-size: 12px; font-family: Verdana; border-bottom:1px solid black;border-left: 1px solid black;<% if(i == rows) {%>border-bottom: 3px solid black;	<%}%>">
				             &nbsp;     
				           </td>
				           <td  width="9%" height="18px" align="center" style="font-size: 12px; font-family: Verdana; border-bottom:1px solid black;border-left: 1px solid black;<% if(i == rows) {%>border-bottom:  3px solid black;<%}%>">
				             &nbsp;
					       </td>   
				           <td  <%if(j == 2) {%>width="9%"<% } else {%>width="8%" <%}%> height="18px" align="center" style="font-size: 12px; font-family: Verdana;border-bottom:1px solid black;border-left: 1px solid black;<% if (j == cols-1) {%>border-right: 3px solid black;<% } else {%>border-right: none;<%} %><% if(i == rows) {%>border-bottom:  3px solid black;	<%}%>">
				             &nbsp;     
				           </td>
				        <% }%>
	    			</tr>
	    			<% }%>
	    		</table>
	    	</div>
    <% }
    }%>
	<!--   cm end   -->
    
	<!--    尾页列表 a2 start -->
    <div id="a2" style="margin-top: 3px; width: 210mm;">
      <table id="tally_sheet_a2" width="100%" border="0" cellspacing="0" cellpadding="0" border-collapse="collapse">
	     <thead>
           <tr>
             <td  width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left:3px solid black;border-top:3px solid black; border-bottom: 3px solid black; border-right: none;">
	           RefNo
	         </td>
             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black;border-top:3px solid black; border-bottom:3px solid black; border-right: none;">
	           Plts
	         </td>  
             <td width="9%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black; border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
	           Pcs
	         </td>  
             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black; border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
	           Model
	         </td>  
             <td  width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-top: 3px solid black;border-left: 3px solid black; border-bottom: 3px solid black; border-right: none;">
	           RefNo
	         </td>
             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black;border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
	           Plts
	         </td>  
             <td width="9%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black;border-top:3px solid black; border-right: none; border-bottom: 3px solid black;">
	           Pcs
	         </td> 
             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black; border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
	           Model
	         </td> 	          
             <td  width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-top: 3px solid black; border-left: 3px solid black; border-bottom: 3px solid black; border-right: none;">
	          RefNo
	         </td>
             <td width="8%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black;border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
	           Plts
	         </td>  
             <td width="9%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black;border-top: 3px solid black; border-bottom: 3px solid black; border-right: none;">
	           Pcs
	         </td>  
             <td width="9%" height="19" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black; border-top: 3px solid black; border-bottom: 3px solid black; border-right: 3px solid black;">
	           Model
	         </td> 	         
	       </tr>   
         </thead> 
	     <%  
	     	 int a2_rows = 30;
   		     for(int a2_i = 0 ;a2_i < a2_rows; a2_i++ ) {
 	     %>
 	      
         <tr>
	         <%
				 for(int j = 0; j < cols; j++ ){
	 		 %>
	           <td width="8%" height="18px" valign="bottom" align="center" style="padding-bottom: 3px; font-size: 12px; font-family: Verdana; border-left: 3px solid black; border-bottom: 1px solid black;">&nbsp;</td>  
	 		   
	           <td  width="8%" height="18px" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black; border-bottom: 1px solid black; ">&nbsp;</td>
	           <td  width="9%" height="18px" align="center" style="font-size: 12px; font-family: Verdana; border-left: 1px solid black; border-bottom: 1px solid black;">&nbsp;</td>  
	           <td  <%if(j == 2) {%>width="9%"<%} else {%>width="8%" <%}%> height="18px" align="center" style="font-size: 12px; font-family: Verdana; border-bottom: 1px solid black; border-left: 1px solid black;<% if (j == cols-1) {%>border-right: 3px solid black;<% } else {%>border-right: none;<%} %>">
	             &nbsp;             
	           </td>	                 
	         <%
	            }                
	        %> 
        </tr> 
 		<%}%> 
        
         <tfoot>
           <tr>
             <td height="20" colspan="6" align="left" style="padding-left: 7px; font-size: 14px; font-family: Verdana; border: 3px solid black; border-right: none;">
             	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Pallets:&nbsp;&nbsp;&nbsp;&nbsp;<span id="totalPallets_<%=x%>" name="totalPallets" style="font-style: italic; font-weight: bold;"></span>
             </td>   
             <td height="20" colspan="6" align="left" style="padding-left: 7px; font-size: 14px; font-family: Verdana; border: 3px solid black;">
             	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Case:&nbsp;&nbsp;&nbsp;&nbsp;<span id="totalCase_<%=x%>" name="totalCase" style="font-style: italic; font-weight: bold;"></span>
             </td>               
           </tr>
           <tr>
             <td height="30" colspan="6" align="left" style="padding-left: 5px; font-size: 14px; font-family: Verdana; border:3px solid black; border-right: none; border-top: none;">
			   Confirmed Pallets:&nbsp;<input type="text" style="width: 200px; border-left: none; border-top:none; border-right: none; border-bottom: 1px solid #000;" value=""/>
             </td>   
             <td height="30" colspan="6" align="left" style="padding-left: 5px; font-size: 14px; font-family: Verdana; border:3px solid black; border-top:none;">
               Confirmed Case:&nbsp;<input type="text" style="width: 200px; border-left: none; border-top: none; border-right: none; border-bottom: 1px solid #000;" value=""/>
             </td>               
           </tr>
         </tfoot>          
	   </table>
    </div>
    <!--a2 end -->
    
    <div id="a3" style="margin-top: 5px; width: 210mm;">
      <div>
        <span style="font-size: 14px; font-family: Verdana; text-decoration: underline; font-weight: bold;">ATTENTION DRIVERS/PIECE COUNT REQUIRED:</span><br />
        <span style="font-size: 12px; font-family: Verdana;">This shipment is being tendered to you as a "Live Load" which required the carrier, driver<br /> and or their agents physical count pieces and pallets as the freight is being loaded<br /> onto the Carriers Equipment. Any piece count discrepancy must be brought to the shippers<br /> attention & Corrected prior to leaving the facility.</span>
      </div>
	<!--       <div style="height: 5px; width: 210mm; clear: both;">&nbsp;</div> -->
      <table width="100%" border="0" cellspacing="0" cellpadding="0" border-collapse="collapse">
      	<tr>
      		<td width="20%" height="40px" valign="middle" style="padding-left: 15px; font-size: 12px; font-family: Verdana; border: 1px solid black;">Driver's Signature</td>
      		<td width="30%" height="40px" valign="bottom" align="right" style="padding-right:10px; padding-bottom: 5px; border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;" >
      			<input type="text" style="width: 160px; height: 10px; border-left: none; border-top: none; border-right: none; border-bottom: 1px solid #000;" value="" /><img border='0' src='<%=ConfigBean.getStringValue("systenFolder")%>administrator/imgs/print/sign-up-here_1.jpg' />
      		</td>
      		<td width="20%" height="40px" valign="middle" align="center" style="padding-top: 5px; padding-left: 15px; font-size: 12px; font-family: Verdana; border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">Date</td>
      		<td width="30%" height="40px" valign="bottom" align="right"  style="padding-bottom: 5px;padding-right: 15px; border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;"><span style="font-size: 14px;font-style: italic; font-weight: bold;"><%=DateUtil.showLocalparseDateTo24Hours(DateUtil.NowStr(),ps_id) %></span></td>
      	</tr>
      	<tr>
      		<td width="20%" height="40px" valign="middle" style="padding-top: 5px; padding-left: 15px; font-size: 12px; font-family: Verdana;border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">Printed Name</td>
      		<td width="30%" height="40px" valign="bottom" align="right" style="padding-right:10px; padding-bottom: 5px; border-bottom: 1px solid black; border-right: 1px solid black;" >
      			<input type="text" style="width: 160px; height: 10px; border-left: none; border-top: none; border-right: none; border-bottom: 1px solid #000;" value="" /><img border='0' src='<%=ConfigBean.getStringValue("systenFolder")%>administrator/imgs/print/sign-up-here_1.jpg' />
      		</td>
      		<td width="20%" height="40px" valign="middle" align="center" style="padding-top: 5px; padding-left: 15px; font-size: 12px; font-family: Verdana; border-bottom: 1px solid black; border-right: 1px solid black;">Carrier Name</td>
      		<td width="30%" height="40px" valign="bottom" align="right" style="padding-right: 15px; border-bottom: 1px solid black; border-right: 1px solid black;"><span style="font-size: 14px;font-style: italic; font-weight: bold;"><%=null!=mainRow?mainRow.getString("company_name"):"" %></span></td>
      	</tr>
      </table>
	<!--       <div style="height: 1px; width: 210mm; clear: both;">&nbsp;</div> -->
      <div>
     	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
		   <tr>
		     <td align="left" valign="top" style="font-size: 12px; font-family:Arial;font-weight: bold;" width="400px;">DESTINATION:</td>
		     <td align="left">
		     	<div style="font-size: 15px; font-family:Arial;font-weight: bold;">All Pallets In Good Condition</div>
		     	<div style="height:2px;">&nbsp;</div>
			    <div style="font-size: 12px; font-family:Arial;font-weight: bold;"><span>Driver's Signature:__________________</span></div>
			    <div style="height:2px;">&nbsp;</div>
			    <div style="font-size: 12px; font-family:Arial;font-weight: bold;"><span style="margin-left:55px;">Quantity:__________________</span></div>
		     </td>
		   </tr>
		 </table>
      </div>
    </div>  
    
    <script type="text/javascript">
 	    <%
 	   		double palletCountSum = 0d;//total pallet
		    int caseCountSum = 0;//total case
		    int palletId = 1;
 	    	//为每页的表格赋值
 	    	for(int m=0;m<pageLength;m++){
 	    		int rowIndex = 1;  // 行下标
 		    	int colIndex = 0;  // 列下标
 		    	int index = 1;	   // 序号
 		    	
 		    	
 		    	String sheetId = "tally_sheet_c"+m;
 		    	 
 			    if(result.length <=90){
 			    	sheetId = "tally_sheet_a2";//内容填充a2div下的table
 			    }
 		    	
 			    long preOrder = 0;
 			   
 			    
 			    int startCount = m*rows*cols;//当前页信息在result中开始的位置
 			    int endCount = (m+1)*rows*cols;//当前页信息在result中结束的位置
 			    
 			    //多页，但是a2上面没有填充数据，跳出循环；或者没有数据时
 			    if(result.length==0 || result.length <=startCount){
		    		break;
		    	}
//  			   System.out.println((endCount-startCount));
 			    //判断是否是最后一页，若是最后一页，
 			    if( (result.length >startCount) && (result.length <= endCount) ){
 			    	endCount = result.length;//最后一页信息结束的位置设置为result长度
 			    	
 			    	//最后一页数据未超过90个，信息显示在签字列表中
 			    	if((endCount-startCount)<=90){
 			    		sheetId = "tally_sheet_a2";//取a2下的table
 			    		rows = 30;
 			    	}
 			    }
 			    
 			   for(int n=startCount; n<endCount; n++){
	      			double palletCount = result[n].get("palletCount", 0d);
	 		      	palletCountSum += palletCount;
	 		      	int orderLineCase = result[n].get("orderLineCase", 0);
	 		      	caseCountSum += orderLineCase;
	 		      	
	 		      	boolean isShow = false;//默认不是第一个item
	 		      	//long nowOrder = result[n].get("orderno", 0l);
	 		      	long nowOrder = result[n].get("orderno", 0l);
	 		      	String refNo = result[n].get("RefNo", "");
// 	 		      	System.out.println("nowOrder="+nowOrder+"     orderLineCase="+orderLineCase);
	 		      	
	 		      	orderNo = nowOrder;
	 		      	//判断是不是order中的第一个item
	 		      	if(preOrder!=nowOrder){
	 		      		preOrder = nowOrder;
	 		      		
	 		      		isShow = true;
	 		      	}
	 		      	
	 		      	//填满一列，换下一列
	 		      	if(index > rows) {
	 		      		colIndex = colIndex + 4;
	 		      		rowIndex = 1;
	 		      		index = 1;
	 		      	}
	 		      	
	 		      	int palletCountToInt = MoneyUtil.formatDoubleUpInt(palletCount);//得到接近的较大值
	 		%>	
	 		
	 		<%	
	 	 			//一个 OrderNo 开始时，order赋值；上一个order底部边框加粗显示。
	 		     if(isShow) {
 	    	%>
					// 赋值用
 	    			var th = document.getElementById('<%=sheetId%>').rows[<%=rowIndex%>];
					th.cells[<%=colIndex%>].innerHTML = <%=refNo%>;

					<% if(rowIndex!=1&&n!=0){%>
					// 一个 OrderNo 结束，底部边框加粗显示，以区别于其他 OrderNo
						var th = document.getElementById('<%=sheetId%>').rows[<%=rowIndex-1%>];
						th.cells[<%=colIndex%>].style.cssText   += "border-bottom: 3px solid black";
						th.cells[<%=colIndex+1%>].style.cssText += "border-bottom: 3px solid black";
						th.cells[<%=colIndex+2%>].style.cssText += "border-bottom: 3px solid black";
						th.cells[<%=colIndex+3%>].style.cssText += "border-bottom: 3px solid black";
		 			<%}%>
			 <%	}%>
			 
			// 最后一个元素，底部边框加粗显示
			 <% if(n==result.length-1){%>
				var th = document.getElementById('<%=sheetId%>').rows[<%=rowIndex%>];
				th.cells[<%=colIndex%>].style.cssText   += "border-bottom: 3px solid black";
				th.cells[<%=colIndex+1%>].style.cssText += "border-bottom: 3px solid black";
				th.cells[<%=colIndex+2%>].style.cssText += "border-bottom: 3px solid black";
				th.cells[<%=colIndex+3%>].style.cssText += "border-bottom: 3px solid black";
 			<%}%>	
			 
			 <%
	 		     //pallet值为0时，显示黑方框，并且显示orderNo,然后换下一行填值
		 		if (0 == palletCountToInt) {
		 	 %>
		 		th = document.getElementById('<%=sheetId%>').rows[<%=rowIndex%>];
    			
    			// 2015/6/10 改（gql） 不填充pallets信息
    			th.cells[<%=colIndex%>].innerHTML = <%=orderNo%>;//显示orderNo
    			th.cells[<%=colIndex+1%>].innerHTML = '&nbsp;';
<%--     				'<span style="font-style: italic; font-weight: bold;"><%=palletId %></span>&nbsp;' --%>
<%-- 					+'<%if(palletId < 10) {%>&nbsp;&nbsp;&nbsp;<%}else if(palletId < 100){%>&nbsp;&nbsp;<%}%>' --%>
// 					+'<input type="text" style="text-align: center; width: 14px; height: 14px; background: #000; border: 1px solid #000;" value="" />';
    		
    		<%
    			//然后换下一行填值
				rowIndex++;
				palletId++;
				index++;
			} else{
			%>
			
				//pallet值不为0，逐个显示pallet,显示白方框，
			<%
			    for (int palletNum = palletCountToInt; palletNum > 0; palletNum--) {
			    	//填满一列，换下一列 ； rows:行数，30
			    	if(index > rows) {
			      		colIndex = colIndex + 4;
			      		rowIndex = 1;
			      		index = 1;
			      	}
			%> 
					th = document.getElementById('<%=sheetId%>').rows[<%=rowIndex%>];
<%-- 					th.cells[<%=colIndex%>].innerHTML = <%=orderNo%>;//显示orderNo(一个order结束去掉边框加粗) --%>
//2015/6/10 改（gql） 不填充pallets信息
 			    	th.cells[<%=colIndex+1%>].innerHTML = '&nbsp;';
<%--  			    		'<span style="font-style: italic; font-weight: bold;"><%=palletId %></span>&nbsp;' --%>
<%--  			    										+'<%if(palletId < 10) {%>&nbsp;&nbsp;&nbsp;<%}else if(palletId < 100){%>&nbsp;&nbsp;<%}%>'  --%>
// 		    										+'<input type="text" style="text-align: center; width: 14px; height: 14px; background: #FFF; border: 1px solid #000;" value="" />';
			
			
			<%
					rowIndex++;
					palletId++;
					index++;
				}
			}
		    
	    }
 			   
	%>
		 	 
		var th = document.getElementById('<%=sheetId%>').rows[1];
		 	 
 <%}%>
	//2015/6/10 改（gql） 不填充total信息
 		<%-- var totalPallets = '<%=0 != MoneyUtil.formatDoubleUpInt(palletCountSum) ? MoneyUtil.formatDoubleUpInt(palletCountSum) : "" %>';
 		var totalCase = '<%=0 != caseCountSum ? caseCountSum : "" %>';
 		$("#totalPallets_"+<%=x%>).text(totalPallets);
 		$("#totalCase_"+<%=x%>).text(totalCase); --%>
    </script>
    <%} %>
  </body>
</html>
<script>
function supportAndroidprint(){
	//获取打印机名字列表
   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
   	 //判断是否有该名字的打印机
   	var printer = "<%=printName%>";
   	var printerExist = "false";
   	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){
		return androidIsPrint(containPrinter);
	}else{
		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消
			return androidIsPrint(containPrinter);
		}
	}
}

function androidIsPrint(containPrinter){
	 var printHtml=$('div[name="printHtml"]');
	 var flag = true ;
     for(var i=0;i<printHtml.length;i++){
    	 var count = $('input[name="cLength"]',printHtml[i]).val();//分几页显示
//     	 var resultLength = $('input[name="resultLength"]',printHtml[i]).val();//总数据量
    	 var a1=$('#a1',printHtml[i]);
    	 var a2=$('#a2',printHtml[i]);
    	 var a3=$('#a3',printHtml[i]);
    	 visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");			// 设置页码字体
    	 visionariPrinter.SET_PRINT_PAGESIZE(1, 0, 0, "Letter");
    	 
    	 visionariPrinter.ADD_PRINT_HTM(0, 0, "100%", "100%", a1.html());
    	 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头
    	 
		 for(var i=0; i<count;i++){
    		var id = "#c"+i;
    		var cHtml = $(id,printHtml[i]);
			visionariPrinter.ADD_PRINT_TABLE(100,0,"100%","270mm",cHtml.html());
			visionariPrinter.NewPageA();//执行该函数之后所增加的内容会在前面内容的尾页之后新建一页输出	,前面无内容时，仍然从第一页开始。 
    	 }
    	 
    	 visionariPrinter.ADD_PRINT_TABLE(100,0,"100%","270mm",a2.html());
    	 
    	 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",a3.html());
    	 visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1); 
    	 
    	 visionariPrinter.ADD_PRINT_TEXT("1.6cm",670,"100%","100%","PAGE:#/&"); 
    	 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码
	 
    	 visionariPrinter.SET_PRINT_COPIES(1);
    	 //visionariPrinter.PREVIEW();
	     flag = flag && visionariPrinter.PRINT(); 
     }		 
     return flag ;
}
</script>
