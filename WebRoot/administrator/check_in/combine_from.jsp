<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 


<html>
<head>

<title>Combine From</title>
<%
	boolean isPrint = StringUtil.getInt(request, "isprint") == 1;
%>
 <base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
 <%if(!isPrint){ %>
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
<%} %>
<script>
	function print_shipping_lable(){
		var printHtml=$('div[name="shipping_lable"]');
		for(var i=0;i<printHtml.length;i++){
			 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Load Label");
	      	 visionariPrinter.SET_PRINTER_INDEXA ("LabelPrinter");//指定打印机打印  
	      	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
	         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$(printHtml[i]).html());
	         visionariPrinter.PREVIEW();
	         //visionariPrinter.PRINT();
		}
	}
</script>
</head>
<body>
<div style="border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;">
	<input class="short-short-button-print" type="button" onclick="print_shipping_lable()" value="Print">
</div>
<br/>	
<%
	
	String printName=StringUtil.getString(request,"print_name");
// 	long orderNo=StringUtil.getLong(request, "orderNo");
	String orderNo=StringUtil.getString(request, "orderNo");
	String customerId=StringUtil.getString(request, "CustomerID");
	String companyId=StringUtil.getString(request, "CompanyID");
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long adid = adminLoggerBean.getAdid();
	
// 	DBRow[] rows = loadPaperWorkMgrZzq.findPalletCombinationFromByOrder(orderNo,companyId,customerId, adid, request);
	DBRow[] rows = loadPaperWorkMgrZzq.findPalletCombinationFromByOrder("136248","HONHAI","W12", adid, request);
	String pallet = (rows!=null&&rows.length>0)?rows[0].getString("plate_no"):"";

	/* String loadNo=StringUtil.getString(request, "load");
    String customerId=StringUtil.getString(request, "CustomerID");
    String companyId=StringUtil.getString(request, "CompanyID");
   
    long MasterBOLNo = StringUtil.getLong(request, "MasterBOLNo");
	long adid = StringUtil.getLong(request, "adid");
	if(adid == 0l){
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		 adid = adminLoggerBean.getAdid();
	}
	DBRow[] rows = sqlServerMgr.findOrderShipPalletByLoadNo(loadNo, MasterBOLNo, customerId, companyId, adid, null);
	for(int i = 0; i < rows.length; i ++)
	{
		DBRow row = rows[i]; */
%>
<div id="shipping_lable" name="shipping_lable" style="border:1px solid red; width:363px;margin:0 auto;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" >
		<thead>
		  <tr>
	          <td width="200px;" align="left" colspan="2">
	              <span style="font-family:Arial;margin-left:10px; font-size:15px;"><b>COMBINE &nbsp;FROM</b></span>
	          </td>
	          <td align="right" style="padding-right:10px;">
	            <span style="font-size: 8px;font-family:Arial;">PAGE:</span>
				<span tdata="pageNO" style="font-size: 8px;font-family:Arial;">#</span>
				<span style="font-size: 8px;font-family:Arial;">/</span>
				<span tdata="PageCount" style="font-size: 8px;font-family:Arial;">#</span>
	          </td>
		  </tr>	
		   <tr>
	          <td align="left" colspan="3" style="border-bottom:2px solid black;" height="30px;">
	              <span style="font-family:Arial;margin-left:10px; font-size:15px;"><b>PALLET&nbsp;:</b></span>
	              <span tdata="pageNO" style="font-size: 14px;font-family:Arial;"><%=pallet %></span>
	          </td>
		  </tr>	
		 </thead>
		 <%
		 for(int i = 0; i < rows.length; i ++)
			{
				DBRow row = rows[i];
				DBRow[] details = (DBRow[])rows[i].get("details");
		 %>
		  <tr>
		  	<td width="20%" align="center" style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;">
		  		<span style="font-family:Arial; font-size:14px;"><b>Order :</b></span>
		  	</td>
		  	<td width="20%" style="padding-top: 5px;padding-bottom: 5px;">
		  		<span style="font-size: 13px;font-weight: bold;font-family:Arial;">&nbsp;<%=row.get("order_no",0L) %></span>
		  	</td>
		  	<td style="padding-top: 5px;padding-left: 10px;padding-bottom: 5px;">
		  		<span style="font-size: 8px;font-weight: bold;font-family:Arial;">&nbsp;</span>
		  	</td>
		  </tr>
		  <%
		  	for(int j=0;j<details.length; j ++){
		  		if(j==details.length-1){
		 %>
		 <tr>
		  	<td style="border-bottom:2px solid black;">
		  		<span style="font-size: 8px;font-weight: bold;font-family:Arial;">&nbsp;</span>
		  	</td>
		  	<td style="border-bottom:2px solid black;">
		  		<span style="font-size: 8px;font-weight: bold;font-family:Arial;"><%=details[j].get("item_id",0L) %></span>
		  	</td>
		  	<td style="padding-left: 10px;border-bottom:2px solid black;">
		  		<span style="font-size: 8px;font-weight: bold;font-family:Arial;">QTY:&nbsp;&nbsp;<%=details[j].get("shipped_qty_changed",0L) %></span>
		  	</td>
		  </tr>		
		 <%			
		  		}else{
		 %>
		 <tr>
		  	<td>
		  		<span style="font-size: 8px;font-weight: bold;font-family:Arial;">&nbsp;</span>
		  	</td>
		  	<td>
		  		<span style="font-size: 8px;font-weight: bold;font-family:Arial;"><%=details[j].get("item_id",0L) %></span>
		  	</td>
		  	<td style="padding-left: 10px;">
		  		<span style="font-size: 8px;font-weight: bold;font-family:Arial;">QTY:&nbsp;&nbsp;<%=details[j].get("shipped_qty_changed",0L) %></span>
		  	</td>
		  </tr>
		 <%	
		  		}
		  	}
		  %>
		 <%	
		  	}
		  %>
	</table>
</div>
 	
</body>
</html>
<script>

function supportAndroidprint(){
	//获取打印机名字列表
   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
   	 //判断是否有该名字的打印机
   	var printer = "<%=printName%>";
   	var printerExist = "false";
   	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){
		return androidIsPrint(containPrinter);
	}else{
		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消
			return androidIsPrint(containPrinter);
		}
	}
}


function androidIsPrint(containPrinter){
	var printHtml=$('div[name="shipping_lable"]');
	var flag = true ;
 	for(var i=0;i<printHtml.length;i++){
		 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Load Label");
      	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
      	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","50mm",$(printHtml[i]).html());
         //visionariPrinter.PREVIEW();
        flag =  flag && visionariPrinter.PRINT();
	}
 	return flag ;
}
</script>