<%@page import="com.cwc.service.checkin.action.zwb.AjaxAddWahouseAction"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
 <%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="java.util.*"%>
<%@ include file="../../include.jsp"%> 
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>System User</title>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/thickbox.css";
</style>
<!-- 引入Art -->
<!-- <link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script> -->
<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>

<script src="<%=ConfigBean.getStringValue("systenFolder")%>/administrator/check_in/json/group.js" type="text/javascript"></script>


 	<style type="text/css">
 			 
			*{margin:0px;padding:0px;font-size:12px;}
			ul.myul{list-style-type:none;}
			ul.myul li{float:left;width:160px;border:0px solid red;line-height:25px;height:25px;border:0px solid silver;}
			table.mytable {border-collapse:collapse ;width:98%;margin:0px auto;}
			table.mytable td.left{width:100px;}
			table.mytable tr td{border-bottom:1px solid silver;}
			span.span_name{display:block;float:left;margin-left:3px;cursor:pointer;}
			span.span_checkBox{display:block;float:left;margin-top:4px;}
		</style>
		 
		<%
			int singlecheck =  StringUtil.getInt(request,"single_check"); //1表示的是单选
			String userids =  StringUtil.getString(request,"user_ids"); //表示的是要回显的checkbox
			String notCheckUser =   StringUtil.getString(request,"not_check_user"); // 表示某些User是不能被选中的
			
		    AdminLoginBean adminLoginBean  =  AjaxAddWahouseAction.convertObjectToAdminLoginBean(request.getSession().getAttribute(Config.adminSesion));
		    long param_ps_id = StringUtil.getLong(request,"ps_id");
			long ps_id = param_ps_id > 0 ? param_ps_id:  adminLoginBean.getPs_id();
			String inputId=StringUtil.getString(request,"id");			
			long proJsId = StringUtil.getLong(request,"proJsId");
			String handle_method = StringUtil.getString(request,"handle_method");
			String set_id_val_node	= StringUtil.getString(request, "set_id_val_node");
			String set_name_val_node = StringUtil.getString(request, "set_name_val_node");
			String groups = StringUtil.getString(request, "group");	//人员类型分组(角色)group.js
			long group = StringUtils.isNotEmpty(groups) ?Long.valueOf(groups):0L; //将人员分组(角色)转为Long型
		%>
		<%
			DBRow[] rows = transportMgrZr.getAllUserAndDept(proJsId , ps_id , group );
			//遍历出一个List 集合
			Map<String,List<DBRow>> mapUsers = new HashMap<String,List<DBRow>>();
			Map<String,String> mapAdgidNameAndValue = new HashMap<String,String>();
			List<String> adgidIds = new ArrayList<String>();
			if(rows != null && rows.length > 0){
				for(int index = 0 , count = rows.length ; index < count ;index++){
					DBRow temp = rows[index];
					String adgid = temp.getString("adgid");
					if(adgid.trim().length() > 0 ){
						List<DBRow> listTemp = mapUsers.get(adgid);
						if(listTemp == null || (listTemp != null && listTemp.size() < 1)){
							listTemp = new ArrayList<DBRow>();
						}
						listTemp.add(temp);
						mapUsers.put(adgid,listTemp);
						if(mapAdgidNameAndValue.get(adgid) == null){
							adgidIds.add(adgid);
							mapAdgidNameAndValue.put(adgid,temp.getString("name"));
						}
					}
				}
			}
		 	DBRow[] productCatelog =  checkInMgrZwb.findSelfStorage();
		 	
		%>
		<script type="text/javascript">
	 
		  var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
		  api.button([
			          { name: 'Confirm',
			            callback: function () {
			                   selecteUsers()
			                   return false ;
			                                  },
			            focus:true
			           },
			           {  name: 'Cancel',
			              callback: function () {
			                         cancel()
				                 return true ;
				                        		}
			           }]
		     );    
			var userIds = '<%= userids%>';
			var notCheckUser = '<%= notCheckUser%>';
			//console.log(userIds);
		 	jQuery(function($){
		 		
			 	$(".adgid_check_checked").live("click",function(){
					var _this = $(this);
					var target = _this.attr("target");
					if(_this.attr("checked")){
						
						/*	// checked 部门下的所有的人都选择.
							
							// 当checkbox为不可用时,不参与全选.      [disabled!='disabled'] 刘鹏远 2014-12-17
							// 之前find选择器为 input[type='checkbox']
							// 现在find选择器为 input[type='checkbox'][disabled!='disabled']
						*/
						$("#"+target).find("input[type='checkbox'][disabled!='disabled']").attr("checked","checked");
						
					}else{
						$("#"+target).find("input[type='checkbox'][disabled!='disabled']").attr("checked",false);
					}
				})
				$(".user_check_checked").live("click",function(){
					var _this = $(this);
					if('<%= singlecheck%>' * 1  == 1){
						otherUncheck(_this);
					}else{
					var parent = _this.parent().parent().parent();
					if($(parent).attr("id")){
			 			var adgid = $(parent).attr("id").split("ul_")[1];
					 	checkAllLiOnUlIsChecked(adgid);
					}
					}
					//TODO 检查当前用户是否也属于其他组
					var user_id = _this.attr("id");
					var _user = $("[name='"+user_id+"']");
					_user.not(_this).attr("checked",_this.attr("checked"));
					var parent2 = _user.not(_this).parent().parent().parent();
					if($(parent2).attr("id")){
			 			var adgid = $(parent2).attr("id").split("ul_")[1];
					 	checkAllLiOnUlIsChecked(adgid);
					}
					
				})
				//singlecheck 为1 那么表示的是单选
				showUserChecked(userIds);
				showNotUserchecked(notCheckUser);
				initPsIdAndProJsId();
				
				/*
					初始化时,加载group 下载拉框
					刘鹏远 2014-12-17
					=======ccccccccccccc 标记 ccccccccccccc=======
				*/
		<%-- 	$.getJSON("json/group.js",function(data ){
				var html = '';
				for(var o in data){
					html = html + '<option value="' + data[o] + '">' + o + '</option>';
				}
				var $group = $("#group");
				$group.html(html);
				$group.val("<%=group%>");	//加载完成后会默认选中
			}); --%>
			initGroupSelect();
			function initGroupSelect(){
				var html = '';
				for(var o in groupJson){
					html = html + '<option value="' + groupJson[o] + '">' + o + '</option>';
				}
				var $group = $("#group");
				$group.html(html);
				$group.val("<%=group%>");	//加载完成后会默认选中
			}
				
			/*
				初始化时,加载User 下载拉框
				刘鹏远 2014-12-17
				=======bbbbbbbbbbbbb 标记 bbbbbbbbbbbb=======
			*/
			$.getJSON("json/warehouse.json",function(data){
				var html = '';
				for(var o in data){
					html = html + '<option value="' +  data[o] + '">' + o + '</option>';
				}
				var $proJsId = $("#proJsId");
				$proJsId.html(html);
				$proJsId.val("<%=proJsId%>");//加载完成后会默认选中
			});
			
			/*
				当 group下载拉框 改变时,User也会对应的变化
				刘鹏远 2014-12-17
				=======aaaaaaaaaaa 标记 aaaaaaaaa=======
			*/
			$("#group").change(function(){
				var value = $(this).find("option:selected").html();
				$.getJSON("json/" +  value.toLowerCase() + ".json",function(data){
					var html = '';
					for(var o in data){
						html =  html + '<option value="' +  data[o] + '">' + o + '</option>';
					}
					$("#proJsId").html(html);
				});
				/* var ddd =  eval(dd);
				var html = '';
				for(var o in ddd){
					html = '<option value="' + o + '">' + ddd[o] + '</option>' + html;
				}
*/
				
			});
		})
			function showNotUserchecked(notCheckUser){
		 		if(notCheckUser.length > 0 ){
					var array = notCheckUser.split(",");
					for(var index = 0 , count = array.length ; index < count ; index++ ){
							var node = $("[name='user_"+array[index]+"']").attr("checked",false).attr("disabled","disabled");
							node.parent().parent().parent().each(function(){
								if(this.id != ''){
									var adgid = this.id.split("ul_")[1];
									checkAllLiOnUlIsChecked(adgid);
								}
							});							
					}
				}
			}
			function showUserChecked(userIds){
				if(userIds.length > 0 ){
					var array = userIds.split(",");
					for(var index = 0 , count = array.length ; index < count ; index++ ){
						var node = $("[name='user_"+array[index] +"']").attr("checked",true);
						node.parent().parent().parent().each(function(){
							if(this.id != ''){
								var adgid = this.id.split("ul_")[1];
								checkAllLiOnUlIsChecked(adgid);// 然后检查是不是这个部门下的所有的都选择了。选择了那么部门的应该选择上
							}
						});
					}
				}
			}
			function otherUncheck(_this){
		 		$(".user_check_checked").not(_this).attr("checked",false);
			}
			//注册部门选择 然后全选
			function checkAllLiOnUlIsChecked(adgid){
			 
				var ul = $("#ul_"+adgid);
			 
				var checks = $("input[type='checkbox']",ul);
				var flag = true ;
				for(var index = 0 , count = checks.length ; index < count ; index++ ){
					if(!$(checks.get(index)).attr("checked")){
						flag = false ;
						break ;
					}
				}
				var target = $("#"+ul.attr("target"));
				target.attr("checked",flag?"checked":false);
			 
			}
			function search(){
				var form =  $("#myform");
				form.submit();
			}
			function initPsIdAndProJsId(){
			 
			 	$("#proJsId option[value='<%= proJsId%>']").attr("selected",true);
			 	$("#ps_id option[value='<%= ps_id%>']").attr("selected",true)  
			}
			function selecteUsers(){
				//选中的UserId
				var checks = $(".user_check_checked");
				var ids = "" ;
				var names = "";
				checks.each(function(){
					var _this = $(this);
					var liParent = _this.parent().parent();
					if(_this.attr("checked")){
						var tempIds = _this.attr("id").replace ("user_","");
						if(checkids(ids,tempIds)){
						ids += ","+tempIds;
						names += ","+$(".span_name",liParent).html();}
					}
				});
				ids =  ids.length > 1 ? ids.substr(1):"";
				names = names.length > 1 ? names.substr(1):"";
                //alert("11111");           
				 $.artDialog && $.artDialog.close();
				// $.artDialog.opener.setParentUserShow  && $.artDialog.opener.setParentUserShow(ids,names ,'<%= handle_method%>','<%=set_id_val_node%>','<%=set_name_val_node%>');	
				
				$.artDialog.opener.setParentUserShow(ids,names,'<%=inputId%>'); 
			}
			function cancel(){ $.artDialog && $.artDialog.close();}
			function checkids(ids,id){
				if(ids == '')return true;
				if(id != ''){
					var arr = ids.split(",");
					for(var i = 0; i < arr.length; i++){
						if(id == arr[i])
							return false;
					}
				}else{
					return false;
				}
				return true;
			}
	 	/*	function changeState(_this){
				var node = $(_this);
				var checkbox = $("#"+node.attr("target"));
				if(checkbox && checkbox.length > 0 ){
					if( !checkbox.attr("checked")){
					    checkbox.attr("checked","true");
					}else{ checkbox.removeAttr("checked");}
					 
				}
			}*/
			function changeState(_this){
				var node = $(_this);
				var checkbox = $("#"+node.attr("target"));
				if(checkbox && checkbox.length > 0 ){
					if( !checkbox.attr("checked")){
					    if('<%= singlecheck%>' * 1  == 1){
					    	$(".user_check_checked").attr("checked",false);
						}
					    checkbox.attr("checked","true");
					}else{ checkbox.removeAttr("checked");}
					 
				} 
			}
		</script>
</head>
<body>
	<div style="line-height:30px;height:6%;padding-left:20px" >  
		<form id="myform">
			<input type="hidden" value="<%=singlecheck %>" name="single_check"/>
			<input type="hidden" value="<%=userids %>" name="user_ids"/>
			<input type="hidden" value="<%=notCheckUser %>" name="not_check_user"/>
			<input type="hidden" value="<%=handle_method %>" name="handle_method" />
			<input type="hidden" value="<%=inputId %>" name="id" />
			WareHouse:<select name="ps_id" id="ps_id">
	 			option value="-1" >ALL</option>
	 			<%
	 				if(productCatelog != null && productCatelog.length > 0){
	 					for(DBRow tempCatelog: productCatelog){
	 						%>
	 							<option value='<%=tempCatelog.get("ps_id",0) %>'><%=tempCatelog.getString("ps_name") %></option>
	 						<% 
	 					}
	 					
	 				}
	 			%>
	 		</select>
			
			Group:<select name="group" id="group">
						
					</select>
			User:<select name="proJsId" id="proJsId">
					 <option value="-1">ALL</option>
					 <option value="1">Worker</option>
					 <option value="5">Lead</option>
					 <option value="10">SuperVisor</option>
					 <option value="15">Lead+SuperVisor</option>
					 
				</select>
			 
			 		<input type="button" value="inquiry" class="button_long_search" onclick="search();"/>	
		</form>
		 
	</div>
	   <fieldset style="border:2px #cccccc solid;padding:15px;margin:5px;width:93%; height:85%; overflow:auto"> 
	   <table class="mytable" >
	 	<%
	 		if(adgidIds != null && adgidIds.size() > 0 ){
	 			
	 			for(int index = 0 ,count = adgidIds.size() ; index < count ; index++ ){
	 				if(adgidIds.get(index).equals("100025")){
	 					continue ;
	 				}
	 				%>
	 				<tr>
	 					<td class="left">
	 						<span class="span_checkBox" style="margin-top:-1px;">
	 							<input type="checkbox" class="adgid_check_checked" target="ul_<%=adgidIds.get(index) %>" id="adgid_<%= adgidIds.get(index)%>" <%=singlecheck == 1?"disabled":"" %>/>
	 						</span>&nbsp;
	 						<label class="span_name" for="adgid_<%= adgidIds.get(index)%>"><%= mapAdgidNameAndValue.get(adgidIds.get(index)) %></label>
<%--
=======1111111111 标记 1111111111=======
之前代码:
<span class="span_name"><%= mapAdgidNameAndValue.get(adgidIds.get(index)) %></span>

问题:没有任务操作,需要改为点文字相当于点击 checkbox

现改为:
<label class="span_name" for="adgid_<%= adgidIds.get(index)%>">&nbsp;<%= mapAdgidNameAndValue.get(adgidIds.get(index)) %></label>
刘鹏远 2014-12-17
--%>
	 					
	 					
	 					</td>
	 					<td>
	 					<%
	 						List<DBRow> tempListDBRow = mapUsers.get(adgidIds.get(index));
	 						if(tempListDBRow != null && tempListDBRow.size() > 0 ){
	 					%>
	 							<ul class="myul" id="ul_<%=adgidIds.get(index) %>" target="adgid_<%= adgidIds.get(index)%>">
	 								<%for(DBRow row : tempListDBRow) {%>
	 									<li><span class="span_checkBox"><input type="checkbox" class="user_check_checked" name="user_<%= row.get("adid",0l)%>" id="user_<%= row.get("adid",0l)%>"/></span>&nbsp;
	<label class="span_name" for="user_<%= row.get("adid",0l)%>"><%= row.getString("employe_name") %></label>
<%--
=======22222222222222 标记 222222222222=======

	之前为:	<span class="span_name" onclick="changeState(this);" target="user_<%= row.get("adid",0l)%>"><%= row.getString("employe_name") %></span>
	问题:当checkbox被禁用时,点击文字触发changeState(this)方法也会被选择
	
	现改为:	<label class="span_name" for="user_<%= row.get("adid",0l)%>">&nbsp;<%= row.getString("employe_name") %></label>
	刘鹏远 2014-12-17
--%>
	 									</li>
	 								<%} %>	
	 							</ul>
	 					</td>
	 				</tr>
	 				<%
	 					}
	 			}
	 		}
	 	%>
	 		<!--  
			<tr>
				<td class="left">
					<span class="span_checkBox" style="margin-top:-1px;"><input type="checkbox" /></span>
					<span class="span_name">总经理</span>
				</td>
				<td>
					<ul class="myul">
						<li><span class="span_checkBox"><input type="checkbox" /></span><span class="span_name">张三</span></li>
						<li><span class="span_checkBox"><input type="checkbox" /></span><span class="span_name">张三</span></li>
						<li><span class="span_checkBox"><input type="checkbox" /></span><span class="span_name">张三</span></li>
					</ul>
				</td>
			</tr>
		 -->
		 
			 
		 
		  </table>
		 </fieldset>	 
		<!--  <div style="text-align:center;margin-top:3px;" >	
		 	<input type="button" class="normal-green-long" value="Confirm" onclick="selecteUsers();"/>
		 		&nbsp;	&nbsp;
		 	<input type="button" class="normal-green-long" value="Cancel" onclick="cancel();">
		 </div> -->
</body>
</html>