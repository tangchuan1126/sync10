<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.CheckInDoorStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="checkInDoorStatusTypeKey" class="com.cwc.app.key.CheckInDoorStatusTypeKey"/>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long ps_id=adminLoggerBean.getPs_id();   
long adminId=adminLoggerBean.getAdid(); 
DBRow psRow=adminMgrZwb.findAdminPs(adminId);
String ps_name	=psRow.get("title","") ; 
int p =StringUtil.getInt(request,"p");
long entryId = StringUtil.getLong(request,"entryId");
String license_plate = StringUtil.getString(request,"license_plate");
String trailerNo = StringUtil.getString(request,"trailerNo");
String cmd = StringUtil.getString(request,"cmd");
long doorId = StringUtil.getLong(request,"doorId");
long dlo_id = StringUtil.getLong(request,"dlo_id");
long area_id = StringUtil.getLong(request,"area_id");
String doorName = StringUtil.getString(request,"doorName");
DBRow[] rows = null;
if(cmd.equals("filter")){
	rows = checkInMgrZwb.findMainMesByCondition(ps_id, entryId, license_plate, trailerNo);
}
%>	
<head>
	 
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<style type="text/css" media="all">
	@import "../js/thickbox/thickbox.css";
	</style>
	<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
	<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript">
jQuery(function($){
	 
	addAutoComplete($("#trailerNo"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInCTNRJSONAction.action?flag=1",
			"gate_container_no","gate_container_no");
	addAutoComplete($("#license_plate"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInLicensePlateJSONAction.action?flag=1",
			"gate_liscense_plate","gate_liscense_plate");
});
$(function(){
	$("input[type!='button']").keyup(function(){
		 this.value=this.value.toUpperCase();
		 this.value=trim(this.value);
		// alert(this.value);
		 
	 });
	$("#entryId").keyup(function(){
		if($("#entryId").val()!=""){
			$("#license_plate").val("");  
			$("#trailerNo").val("");
		}
		
	 });
	$("#license_plate").keyup(function(){
		if($("#license_plate").val()!=""){
			$("#entryId").val("");  
			$("#trailerNo").val("");
		}
		
    });
	$("#trailerNo").keyup(function(){
	   if($("#trailerNo").val()!=""){
		   $("#license_plate").val("");  
		   $("#entryId").val("");
	   }
	   
		
    });
	$("#entryId").focus();
	
});
function trim(str){
    return str.replace(/[ ]/g,"");  //去除字符算中的空格
}
</script>
</head>
<body>
	<fieldset style="border:2px #cccccc solid;padding:5px;margin:5px;width:95%;">
	  <legend style="font-size:14px;font-weight:normal;color:#999999;">
			<font style="color:red"><b>Dock[<%=doorName%>]</b></font>
	  </legend>
	  <table width="98%" border="0" cellspacing="5" cellpadding="2">
	   <tr height="20px">
		    <td align="right">Entry ID</td>
			<td>
			   <input  id="entryId" value="<%=entryId==0?"":entryId%>"/>
			</td>
		</tr>
	   <tr>
			<td align="right"> License Plate</td>
			<td>
			   <input  id="license_plate" value="<%=license_plate%>"/>
			   <input type="button" class="long-button" value="No Ocuppied" <%=dlo_id==0?"hidden":""%> onClick="updateDock(<%=dlo_id%>,'<%=doorId %>',2,1,<%=area_id%>);"/>
			</td>
		</tr>
		<tr>
			<td align="right"> Trailer/CTNR</td>
			<td>
				<input id="trailerNo"/ value="<%=trailerNo%>">
				<input type="button" class="button_long_refresh" value="Search" onclick="filter();"/>
			</td>
		</tr>
	  </table>
	</fieldset>
	<form name="filter_form" id="filter_form" >
		<input type="hidden" name="ps_id" id="ps_id" value="<%=ps_id%>"/>	
		<input type="hidden" name="entryId"  />
		<input type="hidden" name="license_plate"  />
		<input type="hidden" name="trailerNo"  />
		<input type="hidden" name="dlo_id"  />
		<input type="hidden" name="doorId"  />
		<input type="hidden" name="doorName"  />
		<input type="hidden" name="p"  />
		<input type="hidden" name="cmd" />
	</form>	
	
	<br/>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" id="content">
    	<tr> 
	        <th width="15%" class="left-title" style="vertical-align:center;text-align:center;">Door</th>
	        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Door Zone</th>
	        <th width="15%" class="left-title" style="vertical-align:center;text-align:center;">Tractor</th>
	        <th width="15%" class="left-title" style="vertical-align:center;text-align:center;">Trailer</th>
	        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">Time</th>
	        <th width="10%" class="left-title" style="vertical-align: center;text-align: center;">Option</th>
	    </tr>
	
	</table>
	
</body>
<script>

function filter(){
	$("#content tr:not(:first)").remove();
	if( $("#entryId").val()=="" && $("#license_plate").val()=="" && $("#trailerNo").val()==""){
		alert("Please enter at least one");
		return false;
	}else{
	
		var ps_id = $("#ps_id").val();
		var entryId = $("#entryId").val();
		var license_plate = $("#license_plate").val();
		var trailerNo = $("#trailerNo").val();
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindResourceByEquipmentAction.action',
	  		dataType:'json',
	//  		async:false,
	  		data:"ps_id="+ps_id+'&entryId='+entryId+"&license_plate="+license_plate+"&trailerNo="+trailerNo,
	  		success:function(data){
	  			var html="";
				if(data.length===0){
					if($("#entryId").val()==""){
						checkin();
					}else{
						alert("Not found EntryId");
					}
					
					return false;
				}else{
					
					for(var i=0;i<data.length;i++){
								var doorId = data[i].door_id==null?"-1":data[i].sd_id;
								var dock = data[i].dock==null?" ":data[i].dock;
								//console.log(data[i].yc_id==null?"":data[i].yc_id);
								html += '<tr id="doubt_id" height="50px">';
								html+='<td id="text"  align="center" valign="middle" style="word-break:break-all" >';
									html+='<font><b>'+dock+'</b></font>' ;
								html+='</td>';
								
								html+='<td id="text"  align="center" valign="middle" style="word-break:break-all" >';
								if(!(data[i].area_name==null)){
									html+=data[i].area_name;
								}else{
									html+="&nbsp;";
								}
								html+='</td>';
								
								html+='<td id="text"  align="center" valign="middle" style="word-break:break-all" >';
									if(!(data[i].dlo_id==null)){
										html+='<fieldset style="width:250px; border:2px solid green;" > <legend> <a style="color:#f60;font-size:14px;" target="_blank" >';
										html+='E'+data[i].dlo_id;
										
										html+='</a> </legend> <div style="text-align:left;clear:both;"> <div style="width:100px;float:left" align="right">License Plate:</div> <div>';
										if(!(data[i].gate_liscense_plate==null)){
											html+='<b>'+data[i].gate_liscense_plate+'</b>';
										}
										html+='</div> </div> <div style="text-align:left;clear:both;"> <div style="width:100px;float:left" align="right">Carrier:</div> <div>';
										if(!(data[i].company_name==null)){
											html+='<b>'+data[i].company_name+'</b>';
										}
										html+='</div> </div> </fieldset>';
									}else{
										html+="&nbsp;";
									}
								html+='</td>';
								
								html+='<td id="text"  align="center" valign="middle" style="word-break:break-all" >';
									if(!(data[i].dlo_id==null)){
										
										html+='<div style="text-align:left;clear:both;"> <div style="width:100px;float:left" align="right">Trailer/CTNR:</div> <div>';
										if(!(data[i].gate_container_no==null)){
											html+='<b>'+data[i].gate_container_no+'</b>';
										}
										html+='</div> </div>';
										
									}else{
										html+="&nbsp;";
									}
								html+='</td>';
								
								html+='<td id="text"  align="center" valign="middle" style="word-break:break-all" >';
									if(!(data[i].warehouse_check_in_operate_time==null)){
										html+='<fieldset style="border:2px solid green; width:270px;"> <legend> <a style="color:#f60;font-size:14px;" target="_blank" >Time</a> </legend> <div style="text-align:left;clear:both;"> <div style="width:120px;float:left" align="right">DockCheckInTime:</div> <div>';
										html+='<b>'+(data[i].warehouse_check_in_operate_time==null?"&nbsp;":data[i].warehouse_check_in_operate_time)+'</b>';
										html+='</div> </div> <div style="text-align:left;clear:both;"> <div style="width:120px;float:left" align="right">PatrolTime:</div> <div>';
										if(!(data[i].patrol_last_update_time==null)){
											html+='<b'+(data[i].patrol_last_update_time)+'</b>';
										}
										html+='</div> </div> </fieldset>';
									}else{
										html+="&nbsp;";
									}
									
			
								html+='</td><td> <input type="button"  name="Submit2" value="Update"  class="short-button" onClick="updateDock('+data[i].dlo_id+','+data[i].sd_id+',1,'+data.length+','+<%=area_id%>+');"> </td></tr>';
								
					}
				}
				
					
			    $("#content").append(html);
	  		}
	    });
	
	}
	
}
function checkin(){
	$.artDialog({
	    content: 'not fount,Do you want to checkIn ？',
	    icon: 'question',
	    lock:true,
	    width: 260,
	    height: 70,
	    title:'Reminder',
	    okVal: 'Yes',
	    ok: function () {
	    	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_createEntryId.html?doorId='+<%=doorId%>+'&doorName='+<%=doorName%>; 
			$.artDialog.open(uri , {title: "Create EntryID",width:'500px',height:'200px', lock: true,opacity: 0.3,fixed: true});
	    },
	    cancelVal: 'No',
	    cancel: function(){
	    	
		}
	});
}
function updateDock(searchdlo_id,searchDoorId,flag,len,area_id){
	
	var license_plate=$("#license_plate").val();
    var trailerNo=$("#trailerNo").val();
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxUpdateDockAction.action',
  		dataType:'json',
 // 		async:false,
  		data:"searchdlo_id="+searchdlo_id+'&searchDoorId='+searchDoorId+"&flag="+flag+"&dlo_id="+<%=dlo_id%>+"&doorId="+<%=doorId%>+"&adminId="+<%=adminId%>+'&area_id='+area_id,
  		success:function(data){
				if(len>1){
					checkData(license_plate,trailerNo,searchdlo_id);
				}else{
					parent.location.reload();
					$.artDialog.close();
					
				}
				
  			    	
  		}
    });
}
function closeWindow(){
	$.artDialog.close();
}
function checkData(license_plate,trailerNo,mainId){
	
	parent.document.check_data.license_plate.value=license_plate;
	parent.document.check_data.trailerNo.value=trailerNo;
	parent.document.check_data.mainId.value=mainId;
	parent.document.check_data.submit();
	$.artDialog.close();
}
function verifyData(license_plate,trailerNo,mainId){
	$.artDialog({
	    content: 'Do you want to closed other records ?',
	    icon: 'question',
	    lock:true,
	    width: 280,
	    height: 70,
	    title:'Reminder',
	    okVal: 'Yes',
	    ok: function () {
	    	checkData(license_plate,trailerNo,mainId);
			
	    },
	    cancelVal: 'No',
	    cancel: function(){
	    	parent.location.reload();
	    	$.artDialog.close();
		}
	});
}
</script>
