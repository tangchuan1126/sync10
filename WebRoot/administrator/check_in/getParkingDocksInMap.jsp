<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
	int psId = StringUtil.getInt(request,"storageId");
	DBRow storageKml = googleMapsMgrCc.getStorageKmlByPsId(psId);
	String inputId=StringUtil.getString(request,"inputId");
	String kmlItemType=StringUtil.getString(request,"type");
	String kmlName = "0";
	if(storageKml != null){
		kmlName = storageKml.getString("kml");
	}
	 String mapWidth = StringUtil.getString(request,"width");
	 String mapHeight = StringUtil.getString(request,"height");
	
%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<head>
	
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">
	
	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

	<style type="text/css" media="all">
	<%--
	@import "../js/thickbox/global.css";
	
	--%>
	@import "../js/thickbox/thickbox.css";
	</style>
	<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
	<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
	<script src="../js/thickbox/global.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
	<script src="../js/jgrowl/jquery.jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css" />
	<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" />
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- GoogleMap -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&sensor=false"></script>
	<script type="text/javascript" src="/Sync10-ui/bower_components/requirejs/require.js"></script>
	 <script type="text/javascript" src="/Sync10-ui/pages/gis/js/gis_main.js"></script>
	<script type="text/javascript" src="/Sync10-ui/pages/gis/js/GoogleMapsV3.js"></script>
	<script type="text/javascript" src="/Sync10-ui/pages/gis/js/jsmap.js"></script> 
	<!-- <script src="../js/maps/GoogleMapsV3.js"></script> 
	<script src="../js/maps/jsmap.js"></script>  -->
	<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
	</style>
	<script type="text/javascript">
	var currentPsId;
	$(function(){
		$("#tabs").tabs({});
		//初始化map
    		jsMapInit(false,'Check In');
		//jsMapInit(true);
		//加载kml
		currentPsId="<%=psId %>";
		getParkingDocksOccupancy("<%=psId %>");
		<%-- //var url = getKmlUrl('<%=kmlName %>');
		//loadKml(url,true);
		parkingDocksLayer = "<%=kmlItemType %>";
		getParkingDocksOccupancyAjax("<%=psId %>"); --%>
		//kml加载完成后将地图放大一级
		//mapZoomChange();
	});
	function mapZoomChange(){
		var url = getKmlUrl('<%=kmlName %>');
		var kml = jsmap.storageKml[url];
		if(kml && kml.status == "OK"){
			var zoom = jsmap.zoom;
			if(zoom < 21){
				jsmap.setZoom(zoom+1);
			}
		}else{
		    setTimeout(mapZoomChange,500);
		}
	}
	//调整地图视口
	function fitMapBounds(){
		if(jsmap.storageObjsBounds["<%=psId %>_door_parking"]){
			jsmap.fitBounds(jsmap.storageObjsBounds["<%=psId %>_door_parking"]);
			var zoom = jsmap.zoom;
			if(zoom < 21){
				jsmap.setZoom(zoom+1);
			}
		}else{
		    setTimeout(fitMapBounds,500);
		}
	}
	function getParkingDocksOccupancy(psId){
		if(jsmap.isFitBounds&&jsmap.isFitBounds["ps_id"]&&jsmap.isFitBounds["ps_id"]!=psId){
			//切换仓库
			if($.isEmptyObject(jsmap.storageBasePolyline[psId])){
				getStorageBaseDataAjax(psId,false);//获取地图base数组 并且有数据时绘制base
			}else{
				var bounds=jsmap.storageBasePolyline[psId].data.bounds;
				jsmap.googleMap.fitBounds(bounds);
			}
		}else{
			//未切换仓库
			if($.isEmptyObject(jsmap.storageBasePolyline[psId])){
				getStorageBaseDataAjax(psId,false);//第一次进来时执行
			}
		}
		if("<%=kmlItemType%>"=='docks'){
			jsmap.flag["drawDocks"]=true;
			drawDockYardByPsId(psId,false);
			startDocksParkingRefresh();
		}else{
			jsmap.flag["drawParking"]=true;
			drawDockYardByPsId(psId,false);
			startDocksParkingRefresh();
		}
		
		
	}
	function storageKmlClick(data){
		if(data && data.type=="<%=kmlItemType %>" && !data.available){
			$.artDialog.opener.getDoorOrParking(data.id,data.name,data.areaId);
			$.artDialog && $.artDialog.close();
		}
	}

	</script>
</head>
<body>
	<div id="jsmap"  style="width: <%=mapWidth %>px; height: <%=mapHeight %>px;"></div>
</body>