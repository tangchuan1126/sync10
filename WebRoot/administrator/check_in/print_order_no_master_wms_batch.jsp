<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="wmsSearchItemStatusKey" class="com.cwc.app.key.WmsSearchItemStatusKey"></jsp:useBean>
<html>
<head>

<title>print wms</title>
<%
long entryId = StringUtil.getLong(request, "entryId");
String loadNo=StringUtil.getString(request,"loadNo");
String window_check_in_time = StringUtil.getString(request,"window_check_in_time");
String DockID = StringUtil.getString(request, "DockID");
String company_name = StringUtil.getString(request, "company_name");
String gate_container_no = StringUtil.getString(request, "gate_container_no");
String seal = StringUtil.getString(request, "seal");
String CompanyID = StringUtil.getString(request, "CompanyID");
String CustomerID = StringUtil.getString(request, "CustomerID");
//System.out.println("orderloadNo:"+loadNo+","+entryId);
%>
<script>
/*	function printWms(){
		var printHtml=$('div[name="printWms"]');
		for(var i=0;i<printHtml.length;i++){
			 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Load Label");
	      	 visionariPrinter.SET_PRINTER_INDEXA ("LabelPrinter");//指定打印机打印  
	         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
	         visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			 visionariPrinter.ADD_PRINT_TABLE("0.4cm",7,"100%","90mm",$(printHtml[i]).html());
			 //visionariPrinter.SET_PRINT_STYLEA(0,"AngleOfPageInside",180);
			 visionariPrinter.SET_PRINT_COPIES(1);
			// visionariPrinter.ADD_PRINT_TEXT("0.5cm",350,"100%","100%",addStyleForText2());
			 //visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);
			 visionariPrinter.ADD_PRINT_HTM("14cm",160,"100%","100%",addStyleForText("CONTINUED..."));
			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
			 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","Last");
			 //visionariPrinter.PREVIEW();
			 visionariPrinter.PRINT(); 
		}
	}*/
	function addStyleForText(text)
	{
		return '<span style="font-size: 10;font-family:Verdana;">'+text+'</span>';
	}
</script>
</head>
<body>

	
	 <%
	 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	 long adid = adminLoggerBean.getAdid();
	    DBRow[] orderInfoRows = sqlServerMgr.findOrdersSomeInfoWmsByLoadNo(loadNo, adid, request);
	 	if(orderInfoRows.length > 0)
	 	{
		 	DBRow orderInfoRow = orderInfoRows[0];
					
	 %>
		 		<div id="printWms" name="printWms" style="border: 0px solid red; width:363px;margin:0 auto;">
		 			<table width="368px" border="0" align="left" cellpadding="0" cellspacing="0">
						<thead>
						  <tr style="width: 100%;">
						  	<td style="border-bottom:2px solid black;width: 100%;" colspan="5">
								<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;" ><%=new TDate().getFormateTime(DateUtil.NowStr(), "yyyy-MM-dd HH:mm:ss") %></span>&nbsp;
										</td>
										<td colspan="2">
											<span style="font-size: 10;font-weight: bold;font-family:Verdana;">ORDER  LOADING  TICKET</span>
										</td>
										<td>
											<span style="font-size: 8;font-family:Verdana;">PAGE:</span>
											<span style="font-size: 8;font-family:Verdana;" tdata='pageNO'>#</span>
											<span style="font-size: 8;font-family:Verdana;">/</span>
											<span style="font-size: 8;font-family:Verdana;" tdata='PageCount'>#</span>
										</td>
									</tr>
									
									<tr>
										<td>	
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">ENTRY ID&nbsp;:&nbsp;<%=entryId %></span>
										</td>
										<td colspan="3">
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">LOAD NO&nbsp;:&nbsp;<%=loadNo %></span>
										</td>
									 </tr>
									<tr>
										<td>
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">WAREHOUSE&nbsp;:&nbsp;<%=orderInfoRow.getString("CompanyID") %></span>
										</td>
										<td>
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;"><%=orderInfoRow.getString("CustomerID") %></span>
										</td>
										<td>
											&nbsp;
										</td>
									 </tr>
									 <tr>
										<td>
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">SHIP  TO&nbsp;:&nbsp;<%=orderInfoRow.getString("ShipToID") %></span>
										</td>
										<td>
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;"><%=orderInfoRow.getString("AccountID") %></span>
										</td>
									</tr>
									<tr>
										<td>
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">SHIP  DATE&nbsp;:&nbsp;<%=DateUtil.parseDateFormat(window_check_in_time, "yyyy-MM-dd") %></span>
										</td>
										<td>
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">DOCK&nbsp;:&nbsp;<%=DockID%></span>
										</td>
									 </tr>
									 <tr>
										<td>
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">CARRIER&nbsp;:&nbsp;<%=company_name %></span>
										</td>
										<td>
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">VEHICLENO&nbsp;:&nbsp;<%=gate_container_no %></span>
										</td>
										<td>
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">SEALS&nbsp;:&nbsp;<%=seal%></span>
										</td>
										<td colspan="2">
										
										</td>
									 </tr>
									 <tr>
										<td colspan="5">
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">PalletTypeID&nbsp;:&nbsp;<%=orderInfoRow.getString("PalletTypeID")%></span>
										</td>
									 </tr>
								 </table>
							</td>
						  </tr>
						  <tr style="width: 100%;">
							<td style="width: 15%;border-bottom: 1px solid black;" align="left"><span style="font-size: 8;font-weight: bold;font-family:Verdana;">LINE</span></td>
						     <td style="width: 28%;border-bottom: 1px solid black;" align="left"><span style="font-size: 8;font-weight: bold;font-family:Verdana;">ITEM ID</span></td>
						     <td style="width: 21%;border-bottom: 1px solid black;" align="left"><span style="font-size: 8;font-weight: bold;font-family:Verdana;">PICK LOCATION</span></td>
						     <td style="width: 18%;border-bottom: 1px solid black;" align="left"><span style="font-size: 8;font-weight: bold;font-family:Verdana;">STAGING</span></td>
						     <td style="width: 18%;border-bottom: 1px solid black;" align="right"><span style="font-size: 8;font-weight: bold;font-family:Verdana;">PLATE NO.</span></td>
						 </tr>
						</thead>
						  <%
					  		int palletTotal = 0;
					  		int itemTotal = 0;
					  		int caseTotal = 0;
					  		double loadPalletNum	= 0d;
					  		int count = 0;
					  		//遍历order
					  		for(int m = 0; m < orderInfoRows.length; m ++)
					  		{
						  		DBRow orderInfoOneRow = orderInfoRows[m];
						  		//pallet总数
						  		DBRow loadPalletNumRow  = sqlServerMgr.findPalletsByOrderNoCompanyId(orderInfoOneRow.getString("CompanyID"), orderInfoOneRow.get("OrderNo", 0L));
					  			loadPalletNum = loadPalletNumRow.get("palletNum", 0d);
					  			palletTotal += loadPalletNum;
						  	%>
						  	<tr style="width: 100%;">
							  	<td colspan="2" align="left" style="border-top: 5px;vertical-align: middle;border-top: 1px solid black;;" rowspan="2" valign="middle">
							  		&nbsp;<img src="/barbecue/barcode?data=<%="98"+ MoneyUtil.fillZeroByRequire(orderInfoOneRow.get("OrderNo", 0L), 7) %>&width=1&height=20&type=code39" />&nbsp;&nbsp;&nbsp;
							  	</td>
							  	<td colspan="3" align="left" style="border-top: 5px;vertical-align: middle;border-top: 1px solid black;" valign="bottom" height="25px">
							  		<span style="font-size: 8;font-weight: bold;font-family:Verdana;">PalletTypeID&nbsp;:&nbsp;<%=orderInfoOneRow.getString("PalletTypeID")%></span>
									&nbsp;<span style="font-size: 8;font-weight: bold;font-family:Verdana;">PONo:&nbsp;<%=orderInfoOneRow.getString("PONo")%></span>										  		
							  	</td>
							  </tr>
							  <tr style="width: 100%;">
							  	<td colspan="3" align="left" valign="top" height="25px">
							 		<span style="font-size: 8;font-weight: bold;font-family:Verdana;">ORDER NO&nbsp;:&nbsp;<%=orderInfoOneRow.get("OrderNo", 0L) %></span>
							  		&nbsp;<span style="font-size: 8;font-weight: bold;font-family:Verdana;">PLTS&nbsp;:&nbsp;<%=(int)loadPalletNum %></span>
							  		&nbsp;<span style="font-size: 8;font-weight: bold;font-family:Verdana;"><%=orderInfoOneRow.getString("PickingType") %></span>
							  	</td>
							  </tr>
						  	<%	
						  		DBRow[] orderItemRows = sqlServerMgr.findOrdersItemsWmsByOrderNo(orderInfoOneRow.get("OrderNo", 0L), orderInfoOneRow.getString("CompanyID"));
						  		for(int i = 0; i < orderItemRows.length; i ++)
						  		{
						  			count ++;
						  			DBRow orderItemRow = orderItemRows[i];
						  			itemTotal += orderItemRow.get("ShippedQty", 0d);
						  %>
						  <tr align="center" valign="middle" style="width: 100%;">
						    <td style="width: 15%;" align="left">
						    	<span style="font-size: 8;font-weight: bold;font-family:Verdana;"><%=StringUtil.parseIndexTo4Str(count) %></span>
						    </td>
						    <td style="width: 28%;" align="left">
						    	<span style="font-size: 8;font-weight: bold;font-family:Verdana;"><%=orderItemRow.getString("ItemID") %></span>
						    </td>
						    <td style="width: 21%;" align="left">
						    	<%
						    		DBRow locationRow = sqlServerMgr.findLocationWmsByPlateNo(orderItemRow.get("PlateNo", 0), orderInfoOneRow.getString("CompanyID"));
						    		if(null != locationRow)
						    		{
						    	%>
						    	<span style="font-size: 8;font-weight: bold;font-family:Verdana;"><%=locationRow.getString("LocationID") %></span>
						    	<%		
						    		}
						    		else
						    		{
								%>
						    		&nbsp;
						    	<%		
						    		}
						    	%>
						    </td>
						   <td style="width: 18%;" align="left">
						    	<span style="font-size: 8;font-weight: bold;font-family:Verdana;"><%=orderInfoOneRow.getString("StagingAreaID") %></span>
						    </td>
						    <td style="width: 18%;" align="right">
						    	<span style="font-size: 8;font-weight: bold;font-family:Verdana;"><%=orderItemRow.get("PlateNo", 0L) %></span>
						    </td>
						  </tr>
						  <tr>
							  <%
							  
							  DBRow qtyInfoRow 	= sqlServerMgr.findOrderCaseQtyByItemIdQty(orderItemRow.getString("ItemID"),orderInfoOneRow.getString("CompanyID"), orderItemRow.get("ShippedQty", 0d), orderInfoOneRow.getString("CustomerID"));
									int caseQty			= qtyInfoRow.get("case_qty", 0);
									int pieceQty		= qtyInfoRow.get("piece_qty", 0);
									int innerQty		= qtyInfoRow.get("inner_qty", 0);
									int caseSumQty		= qtyInfoRow.get("case_sum_qty", 0);
									String Unit			= qtyInfoRow.getString("unit");
									String caseQtyStr	= "";
									if(0 != caseQty)
									{
										caseQtyStr += "+" + (int)caseQty + "(Case)";
									}
									if(0 != pieceQty)
									{
										caseQtyStr += "+" + (int)pieceQty + "(Piece)";
									}
									if(0 != innerQty)
									{
										caseQtyStr += "+" + (int)innerQty + "(Inner)";
									}
									if(0 != pieceQty)
									{
										caseQtyStr += "=" + (int)caseSumQty;
									}
									if(caseQtyStr.length() > 0)
									{
										caseQtyStr = caseQtyStr.substring(1);
									}
									caseTotal += caseSumQty;
									boolean isLast = false;
									if((i+1) == orderItemRows.length)
									{
										isLast = true;
									}
									String perOrderItemLastStyle = "";
									if(!isLast)
									{
										perOrderItemLastStyle = "border-bottom:1px dotted black;";
									}
								%>
						  	<td style="width: 12%;<%=perOrderItemLastStyle %>" align="right" colspan="2">
						    	<span style="font-size: 8;font-weight: bold;font-family:Verdana;"><%=(int)orderItemRow.get("ShippedQty", 0d)+"("+Unit+")" %></span>
						    </td>
						    <td style="width: 15%;<%=perOrderItemLastStyle %>" align="right" colspan="3">
						    	<span style="font-size: 8;font-weight: bold;font-family:Verdana;">
									<%=caseQtyStr %>
								</span>
						    </td>
						  </tr>
						  <%
						  		}
					  		}
						   %>
						   <%if(orderInfoRows.length > 0){ %>
							<tr>
							 	<td colspan="5" style="border-top: 1px black solid;">
							 		<table style="width: 100%;">
									  <tr>
									 	<td width="77px;">
									 		<span style="font-size: 8;font-weight: bold;font-family:Verdana;">TOTAL PLTS:<%=palletTotal %></span>
										</td>
										<td align="right" width="77px;">
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">TOTAL QTY:&nbsp;<%=itemTotal%></span>
										</td>
										<td align="right"  width="206px;">
											<span style="font-size: 8;font-weight: bold;font-family:Verdana;">TOTAL CASE:&nbsp;<%=caseTotal%></span>
										</td>
								  	  </tr>
							 		</table>
							 	</td>
							 </tr>
						  	<%} %>
						   <tfoot>
						   	   
						   </tfoot>
						</table>
		 		</div>
	<%} %>
</body>
</html>
