<%@page import="com.cwc.app.key.PlateStatusInOrOutBoundKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Plate Level Inventary 换另外一种库存表现形式之前</title>
<%
long ps_id = StringUtil.getLong(request,"ps_id");
int p = StringUtil.getInt(request, "p");
PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(30);
String start_time=StringUtil.getString(request, "start_time");
String end_time=StringUtil.getString(request, "end_time");	
String key = StringUtil.getString(request,"key");
String cmd = StringUtil.getString(request,"cmd");
int search_mode = StringUtil.getInt(request,"search_mode");
//System.out.println(start_time+","+end_time);
int type = StringUtil.getInt(request, "type");
int status = StringUtil.getInt(request, "status");
long entry_id_in = StringUtil.getLong(request,"entry_id_in");
long entry_id_out = StringUtil.getLong(request,"entry_id_out");
long ic_id_in = StringUtil.getLong(request,"ic_id_in");
long ic_id_out = StringUtil.getLong(request,"ic_id_out");

DBRow[] rows = checkInMgrZyj.searchPlatesLevelInventary(type,start_time, end_time, ps_id,status,entry_id_in,entry_id_out,ic_id_in, ic_id_out, pc);
if(cmd.equals("search"))
{
	
}
TDate tdate = new TDate();
%>
<style type="text/css">
.fieldName{width: 110px;text-align: right;float: left;height: 20px;}
.fieldValue{text-align: left;height: 20px;}
.fieldValue1{text-align: right;height: 20px;}
.filedValueImp{color:#060;}
</style>
<script type="text/javascript">
function expandDetail(plate)
{
	$("div[id="+plate+"_plate]").toggle();
}
function searchByEntryOrder(entry_in, entry_out, ic_id_in, ic_id_out)
{
	document.searchByEntryOrder.entry_id_in.value = entry_in;
	document.searchByEntryOrder.entry_id_out.value = entry_out;
	document.searchByEntryOrder.ic_id_in.value = ic_id_in;
	document.searchByEntryOrder.ic_id_out.value = ic_id_out;
	//$("#searchByEntryOrder").submit();
	ajaxLoadDatasJsp();
}
$(function(){
	$(".search_plate_a")
		.css("cursor","pointer")
		.css("color", "#060")
		.css("text-decoration", "none")
		.attr("href", "javascript:void(0)")
		/*.click(function(){
			checkSearchByEntryOrder($(this));
		})*/
		;
	$("div[id$=_plate]").css("width", "300px");
});
function checkSearchByEntryOrder(obj)
{
	var key  = $(obj).attr("search_plate_key");
	var type = $(obj).attr("search_plate_type");
	if(type == "EI")
	{
		searchByEntryOrder(key, 0, 0, 0);
	}
	else if(type == "EO")
	{
		searchByEntryOrder(0, key, 0, 0);
	}
	else if(type == "I")
	{
		searchByEntryOrder(0, 0, key, 0);
	}
	else if(type == "O")
	{
		searchByEntryOrder(0,0,0,key);
	}
}
function ajaxLoadDatasJsp()
{
	var para = 'start_time=<%=start_time%>&end_time=<%=end_time%>&ps_id=<%=ps_id%>&type=<%=type%>&status=<%=status%>&p=<%=p%>';
	para += '&entry_id_in='+document.searchByEntryOrder.entry_id_in.value
		+ '&entry_id_out='+document.searchByEntryOrder.entry_id_out.value
		+ '&ic_id_in='+document.searchByEntryOrder.ic_id_in.value
		+ '&ic_id_out='+document.searchByEntryOrder.ic_id_out.value
		+ '&key='+document.searchByEntryOrder.key.value
		+ '&search_mode='+document.searchByEntryOrder.search_mode.value
		+ '&cmd='+document.searchByEntryOrder.cmd.value;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/plate_level_inventary_datas.html',
		type: 'post',
		data:para,
		dataType: 'html',
		async:false,
		success: function(html){
			$("#datas").html(html);
			//$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});
}

</script>
</head>
<body>
<form action="" method="post" id="searchByEntryOrder" name="searchByEntryOrder">
	<input type="hidden" name="entry_id_in" value="<%=entry_id_in%>"/>
	<input type="hidden" name="entry_id_out" value="<%=entry_id_out %>"/>
	<input type="hidden" name="ic_id_in" value="<%=ic_id_in %>"/>
	<input type="hidden" name="ic_id_out" value="<%=ic_id_out %>"/>
	<input type="hidden" name="p" value="<%=p%>"/>
	<input type="hidden" name="ps_id" value="<%=ps_id%>"/>
	<input type="hidden" name="key" value="<%=key %>"/>
	<input type="hidden" name="cmd" value="<%=cmd %>"/>
	<input type="hidden" name="search_mode" value="<%=search_mode %>"/>
	<input type="hidden" name="start_time" value="<%=start_time %>"/>
	<input type="hidden" name="end_time" value="<%=end_time %>"/>
	<input type="hidden" name="type" value="<%=type %>"/>
	<input type="hidden" name="status" value="<%=status %>"/>
</form>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
	        <th width="7%" class="left-title" style="vertical-align:center;text-align:center;">Plate No</th>
	        <th width="7%" class="left-title" style="vertical-align:center;text-align:center;">Status</th>
	        <th width="7%" class="left-title" style="vertical-align: center;text-align: center;">Box Qty</th>
	        <th width="7%" class="left-title" style="vertical-align:center;text-align:center;">Customer</th>
	        <th width="7%" class="left-title" style="vertical-align:center;text-align:center;">Staging Area</th>
	        <th width="7%" class="left-title" style="vertical-align:center;text-align:center;">Remain Time (Day)</th>
	        <th width="19%" class="left-title" style="vertical-align:center;text-align:center;">Delivery</th>
	        <th width="19%" class="left-title" style="vertical-align:center;text-align:center;">PickUp</th>
	        <th width="20%" class="left-title" style="vertical-align:center;text-align:center;">CTNR Info</th>
		</tr>
		
		<%if(rows != null && rows.length > 0){
      		for(DBRow row : rows){
      	%>
<%--       	 onclick="expandDetail('<%=row.getString("plate_no") %>')" --%>
      	<tr height="45px" style="cursor: pointer;" onclick="expandDetail('<%=row.getString("plate_no") %>')">
      		<td align="center" valign="middle" style='word-break:break-all;'>
				<div><span style="font-weight: bold;color: blue;"><%=row.getString("plate_no") %></span></div>
      		</td>
      		<td align="center" valign="middle" style='word-break:break-all;'>
				<div><%=row.getString("status") %></div>
      		</td>
      		<td align="center" valign="middle" style='word-break:break-all;'>
				<div><%=row.getString("box_number") %></div>
      		</td>
      		<td align="center" valign="middle" style='word-break:break-all;'>
				<div><%=row.getString("customer_id") %></div>
      		</td>
      		<td align="center" valign="middle" style='word-break:break-all;'>
				<div><%=row.getString("location_name") %></div>
      		</td>
      		<td align="center" valign="middle" style='word-break:break-all;'>
      			<% String endTime = StrUtil.isBlank(row.getString("finish_time_out"))?DateUtil.NowStr():row.getString("finish_time_out"); %>
				<%=checkInMgrZyj.floatRemain1(checkInMgrZyj.getDiffDateOrhhmmss(row.getString("finish_time_in"), endTime, "dd"))%>
      		</td>
  			<td align="left" valign="middle"> 
    			<div id='<%=row.getString("plate_no")%>_plate' style="display: block;">
    				<div>
      					<div class="fieldName">EntryID&nbsp;:&nbsp;</div>
      					<div class="fieldValue">
      							<span class="filedValueImp"><%=row.getString("dlo_id_in") %></span>
      							(I:<a class="search_plate_a" search_plate_key="<%=row.getString("dlo_id_in") %>" search_plate_type="EI">
		      						<%=row.get("entry_in_in",0) %>
		      					</a>,
		      					O:<a class="search_plate_a" search_plate_key="<%=row.getString("dlo_id_in") %>" search_plate_type="EO">
		      						<%=row.get("entry_in_out",0) %>
      							</a>)
      					</div>
      				</div>
      				<div>
      					<div class="fieldName">Delivery Time&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><span class="filedValueImp"><%=tdate.getFormateTime(row.getString("finish_time_in"), "yyyy-MM-dd HH:mm") %></span></div>
      				</div>
    			</div>
      			<div id='<%=row.getString("plate_no")%>_plate' style="display: none;">
      				<div>
      					<div class="fieldName">EntryID&nbsp;:&nbsp;</div>
      					<div class="fieldValue">
      							<span class="filedValueImp"><%=row.getString("dlo_id_in") %></span>
      							(I:<a class="search_plate_a" search_plate_key="<%=row.getString("dlo_id_in") %>" search_plate_type="EI">
		      						<%=row.get("entry_in_in",0) %>
		      					</a>,
		      					O:<a class="search_plate_a" search_plate_key="<%=row.getString("dlo_id_in") %>" search_plate_type="EO">
		      						<%=row.get("entry_in_out",0) %>
      							</a>)
      					</div>
      				</div>
      				<div>
      					<div class="fieldName">Delivery Time&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><span class="filedValueImp"><%=tdate.getFormateTime(row.getString("finish_time_in"), "yyyy-MM-dd HH:mm") %></span></div>
      				</div>
      				<div>
      					<div class="fieldName">LP&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("gate_liscense_plate_in") %></div>
      				</div>
      				<div>
      					<div class="fieldName">Driver Name&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("gate_driver_name_in") %></div>
      				</div>
      				<div>
      					<div class="fieldName">Driver License&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("gate_driver_liscense_in") %></div>
      				</div>
      				<div>
      					<div class="fieldName">Carrier&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("company_name_in") %></div>
      				</div>
      				<div>
      					<div class="fieldName">Storage&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("ps_title_in") %></div>
      				</div>
      				<div>
      					<div class="fieldName"></div>
      					<div class="fieldValue"></div>
      				</div>
      			</div>
      		</td>  
      		<td align="left" valign="middle">
      			<%if(!StrUtil.isBlank(row.getString("dlo_id_out"))){ %> 
    			<div id='<%=row.getString("plate_no")%>_plate' style="display: block;">
					<div>
      					<div class="fieldName">EntryID&nbsp;:&nbsp;</div>
      					<div class="fieldValue">
      						<span class="filedValueImp"><%=row.getString("dlo_id_out") %></span>
	      					(I:<a class="search_plate_a" search_plate_key="<%=row.getString("dlo_id_out") %>" search_plate_type="EI">
	      						<%=row.get("entry_out_in",0) %>
	      					</a>,
	      					O:<a class="search_plate_a" search_plate_key="<%=row.getString("dlo_id_out") %>" search_plate_type="EO">
	      						<%=row.get("entry_out_out",0) %>
	    					</a>)
      					</div>
      				</div>
      				<div>
      					<div class="fieldName">PickUp Time&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><span class="filedValueImp"><%=tdate.getFormateTime(row.getString("finish_time_out"), "yyyy-MM-dd HH:mm")%></span></div>
      				</div>
    			</div>
    			<div id='<%=row.getString("plate_no")%>_plate' style="display: none;">
    				
      				<div>
      					<div class="fieldName">EntryID&nbsp;:&nbsp;</div>
      					<div class="fieldValue">
      						<span class="filedValueImp"><%=row.getString("dlo_id_out") %></span>
	      					(I:<a class="search_plate_a" search_plate_key="<%=row.getString("dlo_id_out") %>" search_plate_type="EI">
	      						<%=row.get("entry_out_in",0) %>
	      					</a>,
	      					O:<a class="search_plate_a" search_plate_key="<%=row.getString("dlo_id_out") %>" search_plate_type="EO">
	      						<%=row.get("entry_out_out",0) %>
	    					</a>)
      					</div>
      				</div>
      				<div>
      					<div class="fieldName">PickUp Time&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><span class="filedValueImp"><%=tdate.getFormateTime(row.getString("finish_time_out"), "yyyy-MM-dd HH:mm")%></span></div>
      				</div>
      				<div>
      					<div class="fieldName">LP&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("gate_liscense_plate_out") %></div>
      				</div>
      				<div>
      					<div class="fieldName">Driver Name&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("gate_driver_name_out") %></div>
      				</div>
      				<div>
      					<div class="fieldName">Driver License&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("gate_driver_liscense_out") %></div>
      				</div>
      				<div>
      					<div class="fieldName">Carrier&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("company_name_out") %></div>
      				</div>
      			</div>
      			<%} %>
      		</td>  
    		<td align="left" valign="middle"> 
    			<div id='<%=row.getString("plate_no")%>_plate' style="display: block;">
	    			<div class="fieldName">CTNR&nbsp;:&nbsp;</div>
	      			<div class="fieldValue">
	      				<span class="filedValueImp"><%=row.getString("ctnr") %></span>
	      				(I:<a class="search_plate_a" search_plate_key="<%=row.getString("ic_id")%>" search_plate_type="I">
      						<%=row.get("ic_in_cn",0) %>
      					</a>)
	      			</div>
    			</div>
    			<div id='<%=row.getString("plate_no")%>_plate' style="display: block;">
	    			<div class="fieldName">PONo&nbsp;:&nbsp;</div>
	      			<div class="fieldValue">
	      				<span class="filedValueImp"><%=row.getString("pono") %></span>
	      				(O:<a class="search_plate_a" search_plate_key="<%=row.getString("ic_id")%>" search_plate_type="O">
      						<%=row.get("ic_out_cn",0) %>
      					</a>)
	      			</div>
    			</div>
    			<div id='<%=row.getString("plate_no")%>_plate' style="display: none;">
      				<div>
      					<div class="fieldName">CTNR&nbsp;:&nbsp;</div>
      					<div class="fieldValue">
      						<span class="filedValueImp"><%=row.getString("ctnr") %></span>
      						(I:<a class="search_plate_a" search_plate_key="<%=row.getString("ic_id")%>" search_plate_type="I">
      							<%=row.get("ic_in_cn",0) %>
      						</a>)
      					</div>
      				</div>
      				<div>
      					<div class="fieldName">PONo&nbsp;:&nbsp;</div>
      					<div class="fieldValue">
      						<span class="filedValueImp"><%=row.getString("pono") %></span>
      						(O:<a class="search_plate_a" search_plate_key="<%=row.getString("ic_id")%>" search_plate_type="O">
      							<%=row.get("ic_out_cn",0) %>
      					</a>)
      					</div>
      				</div>
      				<div>
      					<div class="fieldName">Customer&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("customer_id") %></div>
      				</div>
      				<div>
      					<div class="fieldName">BOL&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("bol") %></div>
      				</div>
      				<div>
      					<div class="fieldName">Expected Qty&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("qty") %></div>
      				</div>
      				<div>
      					<div class="fieldName">Receive Qty&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("receive_qty") %></div>
      				</div>
      				<div>
      					<div class="fieldName">ETD&nbsp;:&nbsp;</div>
      					<div class="fieldValue"><%=row.getString("ship_out_date") %></div>
      				</div>
      			</div>
      		</td>      	  
      	</tr>
      	
      	<% 		
      		}
      	}else{
      	%>
      		<tr style="background:#E6F3C5;">
   				<td colspan="9" style="text-align:center;height:120px;">No Data</td>
   		    </tr>
      	<% }%>
	</table>
	
	<br>
	<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <tr>
	    <td height="28" align="right" valign="middle" class="turn-page-table">
	        <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
	      Goto 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
	    </td>
	  </tr>
   </table>
</body>
</html>