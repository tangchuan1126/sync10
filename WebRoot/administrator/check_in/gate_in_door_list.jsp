<%@page import="com.cwc.app.key.ModuleKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder")%>';
</script>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- GoogleMap -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&sensor=false"></script>
<script src="../js/maps/GoogleMapsV3.js"></script> 
<script src="../js/maps/jsmap.js"></script> 
<!-- show message -->
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 
getParkingDocksOccupancy
AjaxFindDoorsByZoneIdAction		//已经修改
 
AjaxFindZoneReadyDoorAction
AjaxFindLocationByDoorIdAction  //没有关系 点击某一个门的时候执行的ajax请求(数据显示出来 右侧)
AjaxFindReadyDoorSeachAction   //已经修改-->
<%
	//可以选择的门的列表,  free + (当前associate_type + associate_id (查询的值)) 
	long associate_id = StringUtil.getLong(request, "associate_id");
	int associate_type = ModuleKey.CHECK_IN ;
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long adminId=adminLoggerBean.getAdid();
	long ps_id=adminLoggerBean.getPs_id();
//	DBRow psRow = adminMgrZwb.findAdminPs(adminId);

 	  DBRow[] rows = null ;//storageDoorMgrZr.getAvailableDoorsBy(psRow.get("ps_id",0l), associate_type, associate_id);
     String inputId=StringUtil.getString(request,"inputId");
     long inputValue=StringUtil.getLong(request,"inputValue");//doorId
     
//      out.print("inputValue="+inputValue);
     long zoneId=StringUtil.getLong(request,"zoneId");
     long usezoneId=StringUtil.getLong(request,"usezoneId");
     long cmd=StringUtil.getLong(request,"cmd");
//      DBRow[] useRow=checkInMgrZwb.selectUseDoor(psRow.get("ps_id",0l),usezoneId);     //被占用的门
     DBRow[] useRow=checkInMgrZwb.getUnavailableDoor(ps_id,usezoneId);     //被占用的门
     
     DBRow[] canUseRow=checkInMgrZwb.getVacancyDoor(ps_id,usezoneId,associate_id);     //可用的门以及该entry_id下的门
     if(cmd==1){
    	 canUseRow=checkInMgrZwb.getVacancyDoor(ps_id,usezoneId,0L);     //查询该区域下的可用门
     }
//      System.out.println("inputValue="+inputValue);
	
	DBRow storageKml = googleMapsMgrCc.getStorageKmlByPsId(Integer.parseInt(String.valueOf(ps_id)));
	String kmlName = "0";
	if(storageKml != null){
		kmlName = storageKml.getString("kml");
	}
 	//查询所有zone
	DBRow[] allZone =checkInMgrZwb.findAllZone(ps_id);
%>
<title>Check In</title>
</head>
<body onload="getLocation(<%=inputValue%>,<%=ps_id%>)">
	<div id="tabs">
		<ul>
			<li><a href="#av1">Available Door</a></li>	
			<li><a href="#av2">Occupied Door</a></li>
		</ul>
		<div id="av1">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="search_tb">
				<tr>
					<td width="380px" height="30px" bgcolor="#dddddd" style="padding-left:5px;border-right:1px solid #eeeeee;font-weight:bold">
			 			
			 			<input type="text" id="seachDoor" onkeyup="seach(1)" onkeypress="if(event.keyCode==13){seach(0);return false;}" />&nbsp;
			<!--			<input type="button" value="Find" class="button_long_search" onclick="seach(1)" />  --> 
			     		
			     		<select id="allzone" onchange="findDoorsByZoneId()" >
			     			<option value="0">Select Zone</option>
			     		  	<%
			     		  	if(allZone!=null && allZone.length>0){
			     		  		for(int i=0;i<allZone.length;i++){
			     		  	%>
			     		  	<option value="<%=allZone[i].get("area_id",0)%>"  <%=allZone[i].get("area_id",0)==zoneId?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
			     		  	<%			
			     		  		}
			     			}
			     		  	%>
			     		</select>
		     		</td>
		     		<td bgcolor="#dddddd" style="padding-left:5px;font-weight:bold" align="left">Staging</td>
		     		<td align="right" bgcolor="#dddddd" style="padding-left:5px;font-weight:bold;height:30px">
						<input class="long-button"  type="button" onclick="getDoorInMap()" value="Map" >&nbsp;
					</td>
		 		</tr>
		 	</table>
		 	
	    	<div align="left" style=" margin-top:2px; height:320px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">		
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						    <div style="background-color:#FFF;width:300px; border:2px #dddddd solid; height:280px; float:left; overflow:auto" >
				        		 <table width="100%" border="0" cellspacing="0" cellpadding="0" id="readyDoor">
						        	
					        	<% 
					        		for(int i=0;i<canUseRow.length;i++){
					        			 if(canUseRow[i].get("sd_id", 0l)==inputValue){
					        	%>
						        	<tr ondblclick="doubleClick(this)" onclick="singleClick(<%=canUseRow[i].get("sd_id", 0l) %>,<%=ps_id%>,this)" name="warehouse_tr" bgcolor="#E6F3C5">
										<td style="border-bottom:1px solid #dddddd;" width="20%" height="30px" align="center">
											<input name="doorId" type="radio" checked="checked" value="<%=canUseRow[i].get("sd_id", 0l) %>" text="<%=canUseRow[i].getString("doorid") %>"  />
										</td>
										<td style="border-bottom:1px solid #dddddd" > Door [<%=canUseRow[i].getString("doorid") %>]</td>
									</tr>
									<% }else{%>
						        	<tr ondblclick="doubleClick(this)" onclick="singleClick(<%=canUseRow[i].get("sd_id", 0l) %>,<%=ps_id %>,this)" name="warehouse_tr">
										<td style="border-bottom:1px solid #dddddd;" width="20%" height="30px" align="center">
											<input name="doorId" type="radio" value="<%=canUseRow[i].get("sd_id", 0l) %>" text="<%=canUseRow[i].getString("doorid") %>"  />
										 </td>
										<td style="border-bottom:1px solid #dddddd" > Door [<%=canUseRow[i].getString("doorid") %>]</td>
									</tr>
					        	<%} }%>
						        	
						        	
								 </table>
				      		</div>
					    </td>				   
					    <td>
					   	    <div style="background-color:#FFF;width:200px; border:2px #dddddd solid; height:280px; float:left; overflow:auto">
				        		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="conTab">
						  			
								</table>
				     		</div>
					    </td>
					</tr>
				</table>
				<div align="right">
					<input class="normal-green" type="button"  value="Confirm" onclick="fantian(2)" >
				</div>
			</div>         
	    </div>
	    
		<div id="av2">
			<form name="useDoor">
	    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		     		<tr>
		     			<td width="100%" height="25px" bgcolor="#dddddd" style="padding-left:5px;border-right:1px solid #eeeeee;font-weight:bold">Door
		     		 	<select id="useDoorZone" onchange="findUseDoorsByZoneId()" >
		     		  		<option value="0">Select Zone</option>
		     		  		<%
		     		  		if(allZone!=null && allZone.length>0){
		     		  			for(int i=0;i<allZone.length;i++){
		     		  		%>
		     		  		<option value="<%=allZone[i].get("area_id",0)%>"  <%=allZone[i].get("area_id",0)==usezoneId?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
		     		  		<%			
		     		  			}
		     		 		}
		     		  		%>
		     		  	</select>
		     		  	<input  type="hidden" name="zoneId" id="zoneId" value="<%=zoneId%>"/>
		     		  	<input  type="hidden" name="inputValue" id="inputValue" value="<%=inputValue%>"/>
		     		  	<input  type="hidden" name="usezoneId" id="usezoneId"/>
		     		  	<input  type="hidden" name="cmd" id="cmd"/>
		     			</td>	     
		     		</tr>
				</table>
			</form>
			<div align="left" style=" margin-top:2px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">		
			     <%for(int a=0;a<useRow.length;a++){ %>
		     	 <div style="background-color:#FFF;border-bottom:1px solid #dddddd;padding-left:8px;">
		     	 	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="useDoor">
					  <tr height="25px">
					    <td width="150px;"><span style="color:red">Door&nbsp;[<%=useRow[a].getString("doorId") %>]</span></td>
					    <td>&nbsp; <%=useRow[a].getString("detail") %></td>
					  </tr>
					</table>
		     	 </div>			
		     	 <%} %>
		     </div>
	    </div>  
	</div>  
</body>
</html>
<script>
$(document).ready(function(){
	
	jQuery(function($){
		
		$("#tabs").tabs({
			cache:true,
			spinner:'<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie:{
				expires: 30000
			},
			select:function(event,ui){
				if(ui.index==0){
					window.setTimeout(function(){$("#seachDoor").focus();},300);
				}
			}
		});
		var cmd=<%=cmd%>;
		if(cmd!=1){
			 $("#tabs").tabs("select",0);  //默认选中第一个
		}
// 		findDoorsByZoneId();
	});
	
	$('#seachDoor').focus();
});

function zong_double_lick(tr){
	fantianZone(2);
}

function zone_single_click(tr){
	
	var zone_tr=$('tr[name="zone_tr"]');
	for(var i=0;i<zone_tr.length;i++){     
		//先去掉所有颜色
		$(zone_tr[i]).attr('bgcolor',"");
	}
	var ra=$('input[name="zoneDoorId"]',tr);
	$(tr).attr('bgcolor','#E6F3C5');
	ra.attr("checked","checked");
}

function doubleClick(tr){              
	//双击
	var ra=$('input[name="doorId"]',tr);
	fantian(2);
}

function singleClick(door_id,ps_id,tr){ 	
	//单击
	var warehouse_tr=$('tr[name="warehouse_tr"]');
	for(var i=0;i<warehouse_tr.length;i++){     
		//先去掉所有颜色
		$(warehouse_tr[i]).attr('bgcolor',"");
	}
	$(tr).attr('bgcolor','#E6F3C5');
    var ra=$('input[name="doorId"]',tr);
    ra.attr("checked","checked");
    getLocation(door_id,ps_id);
}

function findDoorsByZoneId_1111(){
	$('#seachDoor').val('');//选择区域时，清空输入框
	
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindDoorsByZoneIdAction.action',
		data:'zoneId='+$("#allzone").val()+"&mainId=<%=associate_id%>&ps_id="+<%=ps_id%>,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    },
		success:function(data){
			
			var html='';
			var inputVal = <%=inputValue%>;
	  		for(var i=0;i<data.length;i++){
	  			var sd_id=data[i].sd_id;
				if(sd_id==inputVal){
					html+=
						'<tr ondblclick="doubleClick(this)" onclick="singleClick('+data[i].sd_id+',<%=ps_id %>,this)" name="warehouse_tr" bgcolor="#E6F3C5"> '+
				    		'<td style="border-bottom:1px solid #dddddd;" width="20%" height="30px" align="center">'+
				     			'<input name="doorId" type="radio" checked="checked" value="'+data[i].sd_id+'" text="'+data[i].doorid+'"  />'+
				    		'</td>'+
			        		'<td style="border-bottom:1px solid #dddddd" > Door ['+data[i].doorid+']</td>'+
			   			'</tr>';
				}else{
					html+='<tr ondblclick="doubleClick(this)" onclick="singleClick('+data[i].sd_id+',<%=ps_id%>,this)" name="warehouse_tr">'+
				    '<td style="border-bottom:1px solid #dddddd;" width="20%" height="30px" align="center">'+
				     	'<input name="doorId" type="radio" value="'+data[i].sd_id+'" text="'+data[i].doorid+'"  />'+
				    ' </td>'+
			        '<td style="border-bottom:1px solid #dddddd" > Door ['+data[i].doorid+']</td>'+
			   		'</tr>';
				}
	  		}

	  		$('#readyDoor').html(html);
		},
		error:function(){
		}
	});
}


function seach(flag){
	//console.log(flag);
	var seachDoor = $('#seachDoor').val();
	var zone_id=$("#allzone").val();
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindReadyDoorSeachAction.action',
  		dataType:'json',
  		data:"doorName="+seachDoor+"&ps_id="+<%=ps_id%>+"&mainId=<%=associate_id%>"+"&zone_id="+zone_id+'&flag='+flag,
  		success:function(data){
  			if(flag==1){
		  		var html='';
		  		for(var i=0;i<data.length;i++){
					html+=
						'<tr ondblclick="doubleClick(this)" onclick="singleClick('+data[i].sd_id+',<%=ps_id %>,this)" name="warehouse_tr">'+
							'<td style="border-bottom:1px solid #dddddd;" width="20%" height="30px" align="center">'+
								'<input name="doorId" type="radio" value="'+data[i].sd_id+'" text="'+data[i].doorid+'" onclick="getLocation('+data[i].sd_id+',<%=ps_id %>)" />'+
						    ' </td>'+
						    '<td style="border-bottom:1px solid #dddddd" > Door ['+data[i].doorid+']</td>'+
					   '</tr>';
		  		}

		  		$('#readyDoor').html(html);
		  		
  			}else{
	  	
  				if(seachDoor!=""){
  					if(data.length>0){
	  					$("#search_tb").append('<input name="doorId" type="hidden" id="doorId"  value="'+data[0].sd_id+'"/>');
	  						fantian(1,data[0].doorid);
	  				}else{
	  					showMessage("Door["+seachDoor+"] not found or occupied","alert");
	  					return false;
	  				}
  				}
  			}
  		}
    });
}

function searchZoneDoor(){
	var searchZoneDoor = $("#zoneDoor").val();
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindZoneReadyDoorAction.action',
		data:'zoneId='+<%=zoneId%>+'&doorName='+searchZoneDoor,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    
		},
		success:function(data){
			
  				if(searchZoneDoor!=""){
  					if(data.length>0){
	  						$("#search_tb").html('<input name="zoneDoorId" type="hidden" id="zoneDoorId"  value="'+data[0].sd_id+'"/>');
	  						fantianZone(1,searchZoneDoor);
	  				}else{
	  					showMessage("Door not found","alert");
	  					return false;
	  				} 
  				}
  			
		},
		error:function(){
			showMessage("System error","error");
		}
	});
}

function getLocation(door_id,ps_id){
	
	$('#conTab').html('');
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindLocationByDoorIdAction.action',
  		dataType:'json',
  		data:"door_id="+door_id+"&ps_id="+ps_id,
  		success:function(data){
	  		var html='';
  			for(var i=0;i<data.length;i++){
				html+='<tr>'+
					     '<td style="border-bottom:1px solid #dddddd;" width="20%" height="30px" align="center">&nbsp;</td>'+							     
					     '<td style="border-bottom:1px solid #dddddd">'+data[i].location_name+'</td>'+
					  '</tr>';
  			}	
  			$(html).appendTo('#conTab');
  		}
    });
}

function findZoneBydoorName(doorName){
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindReadyDoorSeachAction.action',
  		dataType:'json',
  		async:false,
  		data:"doorName="+doorName+"&ps_id="+<%=ps_id%>+"&mainId=<%=associate_id%>",
  		success:function(data){
  			if(data.length>0){
  				$("#allzone").val(data[0].area_id);
  			}
  			
  		}
    });
}

function fantian(flag,doorName){
	
	var door = "";
	var dname ="";
	
	if(flag==1){
		door=$("#doorId").val();
		dname = doorName;
	}else{
		door = $("input[name='doorId']:checked").val();
		dname = $("input[name='doorId']:checked").attr("text");
		
	}
	if(door==undefined){
		showMessage("Please choose door First","alert");
		return false;
	}
	findZoneBydoorName(dname);
	
	var zone_id=$("#allzone").val();
	
	$.artDialog.opener.getDoor(door,dname,'<%=inputId%>',zone_id);
	$.artDialog.close();
}

function fantianZone(flag,doorName){
	if(flag==1){
		var door=$("#zoneDoorId").val();
		var doorName = doorName;
	}else{
		var door = $("input[name='zoneDoorId']:checked").val();
		var doorName = $("input[name='zoneDoorId']:checked").attr("text");
	}
	if(door==undefined){
		showMessage("Please choose door First","alert");
		return false;
	}
	$.artDialog.opener.getDoor(door,doorName);
	$.artDialog.close();
}
function getDoorOrParking(door,doorName,zoneId){
	$.artDialog.opener.getDoor(door,doorName,'<%=inputId%>',zoneId);
	$.artDialog && $.artDialog.close();
}
function getParkingDocksOccupancyAjax(psId){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/getParkingDocksOccupancy.action',
		data:'psId='+psId,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    },
		success:function(data){
			if(data && data.flag == "true"){
				showParkingDocksOccupancy('<%=ps_id %>','<%=kmlName %>',data.objs);
			}
		},
		error:function(){
		}
	});
}
function getDoorInMap(){
	var width = window.parent.innerWidth;
	var height = window.parent.innerHeight;
	width = parseInt(width*0.9);
	height = parseInt(height*0.9);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/getParkingDocksInMap.html?storageId='+<%=ps_id%>+'&type=docks&width='+width+'&height='+height; 
	$.artDialog.open(uri , {title: "Map",width:width+'px',height:height+'px', lock: true,opacity: 0.3,fixed: true});
}

function findDoorsByZoneId(){
	$('#seachDoor').val('');//选择区域时，清空输入框
	$("#useDoorZone").val($("#allzone").val());
	findUseDoorsByZoneId()
}

function findUseDoorsByZoneId(){
	document.useDoor.usezoneId.value=$("#useDoorZone").val();
	document.useDoor.zoneId.value=$("#useDoorZone").val();
	document.useDoor.cmd.value='1';
	document.useDoor.submit();
}
</script>
