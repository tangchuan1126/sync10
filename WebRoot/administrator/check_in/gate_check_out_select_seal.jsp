<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%>

 <head>
	 <!--  基本样式和javascript -->
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
 	<title>Please select the seal</title>
 </head>
<style type="text/css">
	#warehouse {
	    padding:10px; width:140px; height:70px;
	    border: 5px solid #dedede;
	    -moz-border-radius: 15px;      				  /* Gecko browsers */
	    -webkit-border-radius: 15px;  				  /* Webkit browsers */
	    border-radius:15px 15px 15px 15px;            /* W3C syntax */
	}
	#gate {
	    padding:10px; width:140px; height:70px;
	    border: 5px solid #dedede;
	    -moz-border-radius: 15px;      				  /* Gecko browsers */
	    -webkit-border-radius: 15px;  				  /* Webkit browsers */
	    border-radius:15px 15px 15px 15px;            /* W3C syntax */
	}
</style>
<%
	String after_seal=StringUtil.getString(request, "after_seal");
	String now_seal=StringUtil.getString(request, "now_seal");
	String input_id=StringUtil.getString(request, "input_id");
	String equipment_id = StringUtil.getString(request, "equipment_id");
%>
<body>
	<div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
		    <td align="left" height="71px;">
			    <div id="warehouse" style="cursor:pointer;" onmouseover="bianse(this)" onclick="assignment(0)">
					<span><b>Warehouse Add Seal</b></span>
					<br/><br/>
		    		<span style="color:blue;" id="after"><%=after_seal %></span>
				</div>
		    </td>
		    <td width="20%" rowspan="2" align="center">
		    	<img alt="" src="imgs/arrow.gif" >
		    </td>
		    <td align="left" >
		    	<div id="gate" style="cursor:pointer;" onmouseover="bianse(this)" onclick="assignment(1)">
			   	     <span><b>Gate Check Seal</b></span>
			   	     <br/><br/>
			    	 <span style="color:blue;" id="now"><%=now_seal %></span>
		    	</div>
		    </td>
		  </tr>
		</table>
	</div>
</body>
<script>

	function bianse(div){
		$('#warehouse').css("border-color","");
		$('#gate').css("border-color","");
		$(div).css("border-color","#E6F3C5");
	}
	
	function assignment(num){
		var seal='';
		if(num==0){
			seal=$('#after').html();
		}else{
			seal=$('#now').html();
		}
		$.artDialog.opener.check(seal,'<%=input_id%>','<%=equipment_id%>');
		$.artDialog && $.artDialog.close();
	}
</script>