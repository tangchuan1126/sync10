<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>

<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<%
String type_number_door = StringUtil.getString(request, "type_number_door");
long entry_id = StringUtil.getLong(request, "entry_id");
String out_seal = StringUtil.getString(request, "out_seal");
String jsonString = StringUtil.getString(request, "jsonString");
JSONArray jsons = new JSONArray(jsonString);
%>
<title>select load</title>
<script type="text/javascript">
$(function(){
	$(function(){
		var jsons = [];
		jsons = eval('<%=jsons%>');
		for(var i=0;i<jsons.length;i++)
		{
			json = jsons[i];
			var checkDataType = json.checkDataType;
			var number = json.number;
			var door_name = json.door_name;
			var companyId = json.companyId;
			var customerId = json.customerId;
			var length = json.length;
			var out_seal = json.out_seal;
			var number_type=json.number_type;
			var load="";
			var ctnr="";
			var bol="";
			if(number_type==<%=ModuleKey.CHECK_IN_CTN%>){
				ctnr=number;
			}
			if(number_type==<%=ModuleKey.CHECK_IN_BOL%>){
				bol=number;
			}
			if(number_type==<%=ModuleKey.CHECK_IN_LOAD%>){
				load=number;
				loadNotOnlyLoadings(checkDataType,number,door_name,number_type);
			}
			else
			{
				loadNotOnlyReceipts(checkDataType,number,door_name,number_type);
			}
		}
	});
});
function loadNotOnlyLoadings(type, number, door_name,number_type)
{
	 var para = 'entry_id=<%=entry_id%>&type='+type+'&door_name='+door_name+'&loadNo='+number+'&number_type='+number_type;
	 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/print_load_wms_select_data_batch.html',
			dataType:'html',
			async:false,
			data:para,
			success:function(html){	
				$(html).appendTo($('#av1'));
			}
	});
}
function loadNotOnlyReceipts(type,number,door_name,number_type)
{
	var para = 'entry_id=<%=entry_id%>&type='+type+'&door_name='+door_name+'&number='+number+'&number_type='+number_type;
	 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/print_receipts_wms_select_data_batch.html',
			dataType:'html',
			async:false,
			data:para,
			success:function(html){	
				$(html).appendTo($('#av1'));
			}
	});
}
function submitLoadReceiptsSel()
{
	var checkedRadio = $("input:radio:checked", $("#av1"));
	var jsons=[];
	if(checkedRadio.length > 0)
	{
		for(var i=0;i<checkedRadio.length;i++)
		{
			var json={
					checkDataType:$(checkedRadio[i]).attr("dataType"),
					number:$(checkedRadio[i]).attr("number"),
					companyId:$(checkedRadio[i]).attr("companyId"),
					customerId:$(checkedRadio[i]).attr("customerId")
			};
			jsons.push(json);
		}
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.setParentCompanyIdCustomerIdBatch  && $.artDialog.opener.setParentCompanyIdCustomerIdBatch(jsons);
	}
	else
	{
		showMessage("No data to print","alert");
		delayClose();
	}
}
function delayClose()
{
	setTimeout("$.artDialog && $.artDialog.close();", 1500);	
}
</script>
</head>
<body>
	<div id="av1" style="height: 92%;">
	
	</div>
	<div style="height: 8%;vertical-align: middle;width: 98%;text-align: right;" class="win-bottom-line">
		<input class="normal-green-long" type="button" onclick="submitLoadReceiptsSel()" value="BatchPrint" >
	</div>
</body>
</html>
