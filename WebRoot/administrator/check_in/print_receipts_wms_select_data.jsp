<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>

<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<html>
<head>
<%
String type = StringUtil.getString(request, "type");
String number = StringUtil.getString(request, "number");
String ctnNo = "";
String bolNo = "";
if("BOL".equals(type.toUpperCase()))
{
	bolNo = number;
}
else if("CTNR".equals(type.toUpperCase()))
{
	ctnNo = number;
}

long entry_id = StringUtil.getLong(request, "entry_id");
String door_name = StringUtil.getString(request, "door_name");
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adid = adminLoggerBean.getAdid();
DBRow[] rows = sqlServerMgr.findReceiptsByBolNoOrContainerNo(bolNo, ctnNo, "", "", adid, request);
//System.out.println(type+","+number+","+entry_id+","+door_name);
//System.out.println("r:"+rows.length);
%>
<title>select load</title>
<script type="text/javascript">
function selectLoad(CompanyID, loadNo, CustomerID, bol, ctnr)
{
	 $.artDialog && $.artDialog.close();
	 $.artDialog.opener.setParentCompanyReceiptCustomer  && $.artDialog.opener.setParentCompanyReceiptCustomer(CompanyID, '<%=number%>', CustomerID,'<%=entry_id%>','<%=door_name%>','<%=type%>', bol, ctnr);
}
function changeCursor(obj)
{
	 $(obj).css("cursor","Pointer"); 
}
</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		<tr height="40px">
			<th width="25%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">CompanyID</th>
	        <th width="25%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">BOL</th>
	        <th width="25%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">CTNR</th>
	        <th width="25%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">CustomerID</th>
		</tr>
		<%
		if(rows.length > 0)
		{
			for(int i = 0; i < rows.length; i ++)
			{
		%>	
		<tr height="35px" onclick="selectLoad('<%=rows[i].getString("CompanyID") %>','<%=rows[i].getString("LoadNo") %>','<%=rows[i].getString("CustomerID") %>','<%=rows[i].getString("BOLNo") %>','<%=rows[i].getString("ContainerNo") %>')" onmouseover="changeCursor(this)">
			<td>
				<%=rows[i].getString("CompanyID") %>
			</td>
			<td>
				<%=rows[i].getString("BOLNo") %>
			</td>
			<td>
				<%=rows[i].getString("ContainerNo") %>
			</td>
			<td>
				<%=rows[i].getString("CustomerID") %>
			</td>
		</tr>		
		<%
			}
		}else{
		%>
			<tr>
				<td colspan="3" style="text-align:center;line-height:50px;height:50px;background:#E6F3C5;border:1px solid silver;">无数据</td>
			</tr>
		<%} %>
	</table>	
</body>
</html>
