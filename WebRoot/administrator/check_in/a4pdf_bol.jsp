<%@page import="com.cwc.app.key.RelativeFileKey"%>

<%@ page contentType="text/html;charset=UTF-8"%>

<%@page import="us.monoid.json.JSONObject"%>

<%@page import="us.monoid.json.JSONArray"%>

<%@ include file="../../include.jsp"%>

<jsp:useBean id="wmsItemUnitKey" class="com.cwc.app.key.WmsItemUnitKey"></jsp:useBean>

<%

  //String loadNo=StringUtil.getString(request, "load");

  long entry_id=StringUtil.getLong(request, "entry_id");

  long adid = StringUtil.getLong(request, "adid");

  String printName=StringUtil.getString(request,"print_name");

  String jsonString = StringUtil.getString(request, "jsonString");

  JSONArray jsons = new JSONArray(jsonString);

  

  DBRow[] mainRows = checkInMgrZwb.selectAllMain(entry_id, null);

  DBRow mainRow=mainRows.length > 0?mainRows[0]:null;

  //StringUtil.getString(request, "loadNo");

  String out_seal=StringUtil.getString(request, "out_seal");

  

  String container_no=StringUtil.getString(request, "container_no");   //传过来的货柜号

  DBRow tractorRow=checkInMgrZwb.findTractorByEntryId(entry_id);   //根据主单据id 查询 车头号

  

  boolean isPrint = StringUtil.getInt(request, "isprint") == 1; 

  String tempShipperKey = entry_id+"_"+RelativeFileKey.BiLLOfLoadingShipper ;     //签字的时候使用 - 张睿

  String tempCarrierKey = entry_id+"_"+RelativeFileKey.BillOfLoadingCarrier ;     //签字的时候使用 - 张睿

  

  //记录打印的标签，转pdf用

  /* String printed_master_bol_no="";

  String printed_number="";

  for(int m=0;m<jsons.length();m++){

      JSONObject json=jsons.getJSONObject(m);

      String number=json.getString("number");

      String master_bol_nos = json.getString("master_bol_nos");

      printed_number +=","+number;

      printed_master_bol_no += ","+master_bol_nos;

  }

 */

%>

<html>

  <head>

  <base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">

  <!--<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>-->

  <%if(!isPrint){ %>

  <!--  基本样式和javascript -->

  <script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

  <script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>

  <!-- table 斑马线 -->

  <script src="js/zebra/zebra.js" type="text/javascript"></script>

  <link href="js/zebra/zebra.css" rel="stylesheet" type="text/css" />

  <!-- 引入Art -->

  <link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />

  <script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>

  <script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>

  <script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>

  <!-- jquery UI 加上 autocomplete -->

  <script type="text/javascript" src="js/autocomplete/jquery.ui.core.js"></script>

  <script type="text/javascript" src="js/autocomplete/jquery.ui.widget.js"></script>

  <script type="text/javascript" src="js/autocomplete/jquery.ui.position.js"></script>

  <script type="text/javascript" src="js/autocomplete/jquery.ui.tabs.js"></script>

  <script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.js'></script>

  <script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.html.js'></script>

  <script type='text/javascript' src='js/autocomplete/autocomplete.js'></script>

  <link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.core.css" />

  <link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.theme.css" />

  <link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.tabs.css" />

  <link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.autocomplete.css" />

  <!-- 打印 -->

  <script type="text/javascript" src="js/print/LodopFuncs.js"></script>

  <script type="text/javascript" src="js/print/m.js"></script>

  <%} %>

  

  <title>a4print_bol</title>

<script type="text/javascript">

//zhangrui start

function getSignImage(parent){

   

  return {

    shipper_image: $(".shipper_image",parent) ,

    carrier_image: $(".carrier_image",parent)

  }

}

function printPreSignDisplayImage(obj){

  obj.shipper_image.css("display","none");

  obj.carrier_image.css("display","none");

}

function printAddSignImage(visionariPrinter , obj){  

  //alert("printAddSignImage");

  var shipper_image = obj.shipper_image ;

  var carrier_image = obj.carrier_image ;

  var flag = false;

  if(shipper_image && shipper_image.length > 0){

  //zhangrui

  //添加一个图片 shipper

    visionariPrinter.ADD_PRINT_IMAGE(840,20,150,80,"<img border='0' src='"+shipper_image.attr("src")+"' />");

    shipper_image.css("display","block");

    visionariPrinter.SET_PRINT_STYLEA(0,"Stretch",2);//按原图比例(不变形)缩放模式

    visionariPrinter.SET_PRINT_STYLEA(0,"TransColor","#FFFFFF");

    visionariPrinter.SET_PRINT_STYLEA(0,"PageIndex","First");          //只在第一页显示

    

  }


  if(carrier_image && carrier_image.length > 0 ){

  //添加一个图片 carrier

  visionariPrinter.ADD_PRINT_IMAGE(835,505,150,80,"<img border='0' src='"+carrier_image.attr("src")+"' />");

  carrier_image.css("display","block");

  visionariPrinter.SET_PRINT_STYLEA(0,"Stretch",2);//按原图比例(不变形)缩放模式

  visionariPrinter.SET_PRINT_STYLEA(0,"TransColor","#FFFFFF"); 

  visionariPrinter.SET_PRINT_STYLEA(0,"PageIndex","First");          //只在第一页显示

  

  }

  

  //添加一个图片 shipper 和  添加一个图片 carrier返回true

  if(shipper_image && shipper_image.length > 0 && carrier_image && carrier_image.length > 0){

    flag = true;

  }

  

  return  flag;

  

}

//zhangrui end



function print() {

  

  var printHtml=$('div[name="printHtml"]');

  var detail_id = "";//生成pdf，gql 2015/04/24

  for(var i=0;i<printHtml.length;i++) {

    var a1=$('#a1',printHtml[i]);

    var a2=$('#a2',printHtml[i]);

    var a3=$('#a3',printHtml[i]);

    var a4=$('#a4',printHtml[i]);

    detail_id = $('#detailId',printHtml[i]).val();//生成pdf，gql 2015/04/24

    

    //zhangrui

    var signobj = getSignImage(printHtml[i]);

    printPreSignDisplayImage(signobj);

    

    visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");      // 设置页码字体

    visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");

    visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",a1.html());

    visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);

  

    visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a2.html());

    visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  

   

    visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a3.html());

    visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1); 

    

    var orderItemLength =$('#orderItemLength',printHtml[i]).val()*1; 
  
  var strHtml = printHtml[i].innerHTML;
     var arr = strHtml.split("<!--page-break-start-->")||[];
     for(var j=1;j<arr.length;j++){
       var innerHtml = arr[j].replace("page-break-before:always;","");
       innerHtml.replace("width:195mm","100%");
       
       visionariPrinter.NewPageA();
       var reg1 =/<!--header\s+start-->([\s\S]*)?<!--header\s+end-->/;
       var reg2 =/<!--table\s+start-->([\s\S]*)?<!--table\s+end-->/ig;
       
       //header 
       var headerArr = innerHtml.match(reg1)||[];
       if(headerArr.length>0){
         visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",headerArr[0]);
       }
       
       //body
       var bodyArr = innerHtml.match(reg2)||[];
       if(bodyArr.length>0){
         var strBody = bodyArr[0];
         var tableArr = strBody.split("<!--table start-->");
         for(var k=1;k<tableArr.length;k++){
           var tableInnerArr = tableArr[k].split("<!--table end-->");
           visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",tableInnerArr[0]);
           visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
         }
       }
    }
      
    /**if(orderItemLength>6){

      visionariPrinter.NewPageA();

      visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",a4.html()); 

         //visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);

    }**/

    

    /**visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#top',printHtml[i]).html());

    visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头

    visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");          //第一页不显示

    //zhangrui

    

    

    //visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 

    visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码

    **/
  var isSign = printAddSignImage(visionariPrinter,signobj);
    visionariPrinter.SET_PRINT_COPIES(1);

//      visionariPrinter.PREVIEW();

      visionariPrinter.PRINT();



  }

  

  //web端打印时，根据entryId和detailId生成pdf使用 。gql 2015/04/24

  if(detail_id!=""){

//    console.log("detail_id="+detail_id);

    creatPdf('<%=entry_id%>', detail_id);

  }

  

  

  

}



//查询number对应的detail_id,web端打印时，根据entryId和detailId生成pdf使用 。gql 2015/04/24

function creatPdf(entry_id,detail_id){

  $.ajax({

    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/pdf/createPdfByPdfServerAction.action',

    async:false,

    dataType:'json',

    data:'entry_id='+entry_id+"&detail_id="+detail_id,

    success:function(data){

    },

    error:function(){}

  });

}





function androidSign(tempFile,tempKey,signType){

  Android.androidSign(tempFile,tempKey,signType);

 }

function androidGetImage(relativeFile , tempShipperKey , fileName){

    //alert("androidGetImage");

   var parent = document.getElementById(relativeFile+"_"+tempShipperKey);

   var src = '<%=ConfigBean.getStringValue("systenFolder")%>'+'_fileserv_signature/file/'+fileName;

   var str = "<img src='"+src+"' width='150px' height='70px' />" ;

   parent.innerHTML = str ;

 }

//发送到另一个服务器操作本地图片，转换pdf

function androidToPdf(){

  var message = true;

  var printAll = $('div[name="printHtml"]');

  for(var i=0;i<printAll.length;i++){

    if(message){

      message =downLoad(i);

    }else{

      downLoad(i);

    }

    

  }

  

  return message;

}

//以上两个方法是在android调用的



//将标签转为图片并上传到服务器



  

  //生成pdf图片，zwb 2015/05/04

  function createImg(){

    var printHtml=$('div[name="printHtml"]');

    for(var i=0;i<printHtml.length;i++) {

      var a1=$('#a1',printHtml[i]);

      var a2=$('#a2',printHtml[i]);

      var a3=$('#a3',printHtml[i]);

      var a4=$('#a4',printHtml[i]);

      //zhangrui

      var signobj = getSignImage(printHtml[i]);

      printPreSignDisplayImage(signobj);

      

      visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");      // 设置页码字体

      visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");

      visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",a1.html());

      visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);

    

      visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a2.html());

      visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  

     

      visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a3.html());

      visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1); 

      

      var orderItemLength =$('#orderItemLength',printHtml[i]).val()*1; 

      if(orderItemLength>6){

        visionariPrinter.NewPageA();

        visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",a4.html()); 

           //visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);

      }

      

      visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#top',printHtml[i]).html());

      visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头

      visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");          //第一页不显示

      //zhangrui

      var isSign = printAddSignImage(visionariPrinter,signobj);

      

      visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 

      visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码

      

      visionariPrinter.SET_PRINT_COPIES(1);

      visionariPrinter.SET_SAVE_MODE("SAVEAS_IMGFILE_EXENAME",".jpg");

      visionariPrinter.SAVE_TO_FILE("vvme.jpg");



    }

    

  }

</script>

</head>

<body>

 <%if(!isPrint){ %>

<div style="width:195mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;">

  <input class="short-short-button-print" type="button" onclick="print()" value="Print">

  <input type="button" value="Export"  onclick="createImg()" />

</div>

<%} %>

<%

long ps_id = 0;

if(mainRow!=null){

  ps_id = mainRow.get("ps_id",0l);

}

// if(adid == 0l){

//  AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 

//     adid = adminLoggerBean.getAdid();

//     ps_id = adminLoggerBean.getPs_id();

// }else{

//  ps_id = adminMgr.getDetailAdmin(adid).get("ps_id",0l);

// }

int loop_index =0;

for(int m=0;m<jsons.length();m++){

  JSONObject json=jsons.getJSONObject(m);

  

  String number=json.getString("number");

  String door_name = json.getString("door_name");

  String companyId = json.getString("companyId");

  String customerId = json.getString("customerId");

  String loadNo=number;

  String master_bol_nos = json.getString("master_bol_nos");

  String order_nos = json.getString("order_nos");

  String checkDataType=json.getString("checkDataType");

 

  DBRow equipRow = checkInMgrZwb.getEquipmentByTask(entry_id, number);//修改Gate In等 时间从设备表查询

  

  DBRow detailRow = printLabelMgrGql.getDetailByEntryId(entry_id, number);//查询number对应的detail_id,web端打印时，根据entryId和detailId生成pdf使用 。gql 2015/04/24

  

  String relativeValue =  entry_id+"_"+loadNo+"_"+master_bol_nos; //签字文件 关联的value

  

  DBRow[] loadOrderNos = sqlServerMgr.findMasterBolLinesByMasterBolStr(loadNo,master_bol_nos,companyId,customerId,adid, null);

//  if(0 == loadOrderNos.length){

//    loadOrderNos = sqlServerMgr.findOrdersNoAndCompanyByLoadNoOrderStr(loadNo,order_nos,companyId,customerId,adid, null);

//  }

  for(int i = 0; i < loadOrderNos.length; i ++) {

    long orderNo = loadOrderNos[i].get("OrderNo", 0L);

    String companyIdOr = loadOrderNos[i].getString("CompanyID");

    DBRow orderInfoRow = sqlServerMgr.findOrderSomeInfoByOrderNo(orderNo, companyIdOr,adid, request);

    DBRow orderPONoRow = sqlServerMgr.findOrderPONoInfoByOrderNo(orderNo, companyIdOr,adid, request);

    loop_index++;

  int page_num =1;

  int page_size=55;

  int line_number =0; 

  

    if(null!=orderInfoRow && null!=orderPONoRow)

    {
     orderPONoRow.add("OrderNo",orderNo);
     orderPONoRow= checkInMgrZwb.findOrderByBundle(companyIdOr,orderPONoRow); 
     String referenceNo = orderPONoRow.getString("ReferenceNo");
     if(referenceNo!=null && !"".equals(referenceNo)){
       orderInfoRow.add("ReferenceNo",referenceNo);
     }


     DBRow[] orderItemRows = sqlServerMgr.findOrderItemsInfoByOrderNo(orderNo, companyIdOr, adid, request);
     if(orderItemRows!=null && orderItemRows.length>0){
        for(int j=0;j<orderItemRows.length;j++){
           orderItemRows[j].add("OrderNo",orderNo);
        }
        //checkInMgrZwb.findOrderByBundle(companyId,orderPONoRow);
        orderItemRows = checkInMgrZwb.findOrderLineByBundle(companyIdOr,orderItemRows);
     }
     
   int total_page_num = 1+orderItemRows.length/page_size;

   if(orderItemRows.length % page_size>0 && orderItemRows.length>6){

    total_page_num = total_page_num+1;

   }

    System.out.println("orderItemRows.length:"+orderItemRows.length);

%>

    <!-- loop body start -->

    <div style="border:1px red solid; width:195mm; margin:0 auto; margin-top:3px;" name="printHtml">

      <input type="hidden" id="companyId" value="<%=companyId%>"/>

    <input type="hidden" id="customerId" value="<%=customerId%>"/>

    <input type="hidden" id="loadNo" value="<%=loadNo%>"/>

    <input type="hidden" id="dn" value="<%=orderInfoRow.getString("ReferenceNo") %>"/>

    <input type="hidden" id="detailId" value="<%=detailRow.get("dlo_detail_id",0l)%>"/>

  <div id="a1">

    <!-- 头 -->

    <!--<div style="border:1px red solid; width:195mm;  margin:0 auto; margin-top:3px;display:none;" id="top">

      <table cellpadding="0" cellspacing="0" border="0" width="100%">

        <tbody><tr>

           <td colspan="6" style="border-top: 1px solid black; border-left: 1px solid black;border-right: 1px solid black;border-bottom:1px solid black;" align="center">

             <table cellpadding="0" cellspacing="0" border="0" width="100%">

            <tbody><tr>

              <td style="font-size:12px; font-family:Arial;" align="center" height="39" width="17%"><b>Date:</b><span style="font-style: italic;"><%=DateUtil.showLocalparseDateTo24Hours(DateUtil.NowStr(),ps_id) %></span></td>

              <td align="center" width="72%">

                <div style="font-size:18px; font-family:Arial; font-weight: bold;" align="center">SUPPLEMENT TO THE BILL OF LADING</div>

                  <div style="font-size:14px; font-family:Arial;" align="">&nbsp;&nbsp;&nbsp;&nbsp;<b>BILL OF Lading Number:</b><span style="font-style: italic;"><%=orderNo %></span></div>

              </td>

              <td align="center" width="11%">&nbsp;</td>

            </tr>

          </tbody></table>

           </td>

          </tr>

         </tbody>

      </table>

    </div>-->

    

    

    <table cellpadding="0" cellspacing="0" border="0" width="100%">

    <tbody>

         <tr>

           <td style="border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black; font-size: 12px; font-family: Arial;" align="center" height="36" width="15%"><b>Date:</b><span style="font-style: italic;"><%=DateUtil.showLocalparseDateTo24Hours(DateUtil.NowStr(),ps_id) %></span></td>

           <td style="border-bottom: 1px solid black; border-top: 1px solid black; font-size: 18px; font-weight: bold; font-family: Arial;" align="center" width="74%">NON NEGOTIABLE BILL OF LADING</td>

           <td style="border-bottom: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;" align="center" width="11%">PAGE:1/<%=total_page_num%></td>

         </tr>

    </tbody>

  </table>

  <table cellpadding="0" cellspacing="0" border="0" width="100%">

    <tbody>

          <tr>

            <td style="border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; font-size: 12px; font-family: Arial; " align="left" height="48" width="55%">

              <div style="background-color:#000; color: #FFF; font-weight:bold;" align="center">SHIP FROM</div>

              <div style="padding-left: 2px;">&nbsp;<b>Name:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("ShipFromName") %></span></div>

              <div style="padding-left: 2px;">&nbsp;<b>Address:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("Address1")+" "+orderInfoRow.getString("Address2") %></span></div>

              <div style="padding-left: 2px;">&nbsp;<b>City/State/Zip:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("City")+","+orderInfoRow.getString("State")+" "+orderInfoRow.getString("ZipCode") %></span></div>

              <div style="padding-left: 2px;">&nbsp;<b>Phone:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("Phone") %></span></div>

              <div>

                <table cellpadding="0" cellspacing="0" border="0" width="100%">

            <tbody><tr>

              <td style="font-size: 12px; font-family: Arial;padding-left: 2px;">&nbsp;<b>SID#:</b><%=loadNo %></td>

              <td style="font-size: 12px; font-family: Arial;width:80px;margin-right:10px;"><b>FOB: </b><input style="text-align: center; width: 14px; height: 14px; background: #FFF; border: 2px solid #000;" type="text"></td>

            </tr>

          </tbody></table>

              </div>

              <div style="background-color:#000; color: #FFF;font-weight:bold; clear: left" align="center">SHIP TO</div>

            </td>

            <td style=" border-bottom: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;" align="left" valign="top" width="45%">

              <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Bill of Lading Number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderNo %></span></div>

              <div>&nbsp;</div>

          <div>&nbsp;</div>

          <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Reference NO:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("ReferenceNo") %></span></div>

              <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Load No:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=loadNo %></span></div>

              <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Appointment Date:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=new TDate().getFormateTime(orderInfoRow.getString("AppointmentDate"), "MM/dd/yyyy HH:mm") %></span></div>

            </td>

          </tr>

          <tr>

            <td style="border-bottom:1px solid black; border-right:1px solid black;border-left:1px solid black;font-size:14px;font-family:Arial;" align="left">

               <div style="padding-left: 2px; width:425px; border:red solid 0px;white-space:nowrap;overflow:hidden;">

              

              &nbsp;<b>Name:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("ShipToName") %></span>

              &nbsp;<b>Location&nbsp;#:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=StringUtil.fromStringGetNumber(orderInfoRow.getString("ShipToName")) %></span></div>

              

              <div style="padding-left: 2px;">&nbsp;<b>Address:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("ShipToAddress1")+" "+orderInfoRow.getString("ShipToAddress2") %></span></div>

              <div style="padding-left: 2px;">&nbsp;<b>City/State/Zip:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("ShipToCity")+","+orderInfoRow.getString("ShipToState")+" "+orderInfoRow.getString("ShipToZipCode")%></span></div>

              <div>

                <table cellpadding="0" cellspacing="0" border="0" width="100%">

            <tbody><tr>

              <td style="font-size: 12px;font-family: Arial;padding-left: 2px;">&nbsp;<b>CID#:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("ShipToID")+" "+orderInfoRow.getString("ShipToPhone") %> </span></td>

              <td style="font-size: 12px;font-family: Arial; width: 80px; margin-right: 10px;"><b> FOB: </b>

                <input style="text-align: center; width: 14px; height: 14px; background: #FFF; border: 2px solid #000;" type="text"></td>

            </tr>

          </tbody></table>

              </div>

              <div style="background-color:#000; color:#FFF;font-family:Arial; clear: left" align="center">THIRD PARTY FREIGHT CHARGES BILL TO:</div>

            </td>

            <%DBRow carrier = sqlServerMgr.findCarrierInfoByCarrierId(orderInfoRow.getString("CarrierID"), orderInfoRow.getString("CompanyID")); %>

            <td style="border-bottom:1px solid black;font-size:13px;font-family:Arial;border-right:1px solid black;" align="left" valign="top">

              <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">CARRIER NAME: </span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=carrier?carrier.getString("CarrierName"):"" %></span></div>

              <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Trailer Number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=container_no%></span></div>

              <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Tractor Number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=tractorRow?tractorRow.getString("tractor_num"):"" %></span></div>

              <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Driver License No.</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=null!=mainRow?mainRow.getString("gate_driver_liscense"):"" %></span></div>

              <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">Seal No.:</span>&nbsp;&nbsp;<span style="font-style: italic;">

                <%if(!StringUtil.isBlank(out_seal)){%>

                <%=out_seal %>

              <%}%>

              </span></div>

            </td>

          </tr>

          <tr>

            <td style="border-bottom:1px solid black;border-left:1px solid black; border-right:1px solid black;font-size:12px;font-family:Arial;" align="left" valign="top">

              <div style="padding-left: 2px;">&nbsp;<b>Name:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("BillToName") %></span></div>

              <div style="padding-left: 2px;">&nbsp;<b>Address:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("BillToAddress1")+" "+orderInfoRow.getString("BillToAddress2") %></span></div>

              <div style="padding-left: 2px;">&nbsp;<b>City/State/Zip:</b>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("BillToCity")+","+orderInfoRow.getString("BillToState")+" "+orderInfoRow.getString("BillToZipCode")%></span></div>

              <div style="border-bottom:1px solid black;"></div>

              <div style="font-weight: bold;padding-left: 2px;">SPECIAL INSTRUCTIONS:</div>

              <div style="font-style:italic;">

                <%if(!orderInfoRow.getString("BOLNote").equals("")){  %>

               <%String[] str=StringUtil.replaceEnter(orderInfoRow.getString("BOLNote")).split("<br>");  %>

               <%if(str.length>5){%>

                 <%=str[0]%><br><%=str[1]%><br><%=str[2]%><br><%=str[3]%><br><%=str[4]%>

                 <%}else{ %>

                   <%for(int p=0;p<str.length;p++){%>

                    <%=str[p] %><br>

                   <%} %>

                 <%} %>

              <%}%>

              </div>

            </td>

            <td style="border-bottom:1px solid black;border-right:1px solid black; font-size:12px;font-family:Arial;" align="left" valign="top">

              <div style="padding-left: 2px;">&nbsp;<span style="font-weight:bold">SCAC:</span>&nbsp;&nbsp;

          <span style="font-style: italic;">

            <%

              out.println(null!=carrier?carrier.getString("SCACCode"):"" );

            %>

            </span>

          </div>

              <div style="padding-left: 2px;">&nbsp;<span style="font-weight: bold">Pro number:</span>&nbsp;&nbsp;<span style="font-style: italic;"><%=orderInfoRow.getString("ProNo")%></span></div>

              <div style="border-bottom: 1px solid black" align="center">  

                <font style="FONT-SIZE: 18px; font-family: time Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman'; WIDTH: 100%; COLOR: #B4B4B4; LINE-HEIGHT: 150%;">

                  BAR CODE SPACE

                </font>

                <div style="padding-left: 2px;">&nbsp;</div>

                <div style="padding-left: 2px;">&nbsp;</div>

              </div>

              <div style="font-size: 11px;font-weight: bold;margin-left:5px;text-align:left;">Freight Charge Terms:<span style="font-style: italic;">(freight charges are prepaid unless marked otherwise)</span></div>

              <div style="padding-left:2px; border-bottom:1px solid black;font-weight:bold;padding-bottom:5px; ">

                <%String freightTerm = orderInfoRow.getString("FreightTerm"); %>

              Prepaid&nbsp;<%="Prepaid".equals(freightTerm)?"_X_":"____" %>

              Collect&nbsp;<%="Collect".equals(freightTerm)?"_X_":"____" %>

              3rd Party&nbsp;<%="Third Party".equals(freightTerm)?"_X_":"____" %>          

              </div>

              <div style="clear:both">

                <div style="float:left;margin-left:5px;">

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                  <input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000; margin-top:1px;" type="text">

                  <br><span style="font-size:12px;">(check box)</span>

                </div>

                <div style="float:right;margin-bottom:5px; margin-right:30px;">

                  Master Bill of Lading:with attached<br>underlying  Bills of lading

                </div>

              </div>

            </td>

          </tr>

    </tbody>

  </table>



    <!-- CUSTOMER ORDER INFORMATION -->

  

   <!-- CUSTOMER ORDER INFORMATION -->

    <table   width="100%" style="border-collapse:collapse; border:solid ; border-width:1px;border-color:#000;"  border="1">

        <tr>

          <td colspan="8" align="left" valign="top" style="font-size:12px;font-family:Arial;font-weight:bold;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;" >

            <div style="background-color:#000; color:#FFF;font-size:12px;font-family:Arial; font-weight:bold;" align="center">CUSTOMER ORDER INFORMATION</div>

          </td>

        </tr>

      <tr>

          <td style="font-size:12px;font-family:Arial;font-weight:bold;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="24%">CUSTOMER ORDER NUMBER</td>

          <td style="font-size:12px;font-family:Arial;font-weight:bold;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="12%">#PKGS</td>

          <td style="font-size:12px;font-family:Arial;font-weight:bold;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="12%">WEIGHT</td>

          <td colspan="2" style="font-size:12px;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF"><span style="font-weight:bold;">PALLET/SLIP</span><br/><span style="font-size: 9px;">(CIRCLE ONE)</span></td>

          <td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF">ADDITIONAL SHIPPER INFO</td>

        </tr>

        <tr>

          <td style="font-size:12px;font-family:Arial;font-style: italic;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="24%"><%=orderPONoRow.getString("PONo") %></td>

          <td style="font-size:12px;font-family:Arial;font-style: italic;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="12%"><%=orderPONoRow.get("orderLineCaseSum", 0) %></td>

          <td style="font-size:12px;font-family:Arial;font-style: italic;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="12%"><%=orderPONoRow.get("weightItemAndPallet", 0) %></td>

          <td style="font-size:12px;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="6%">Y</td>

          <td style="font-size:12px;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="6%">N</td>

          <td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF">&#160;</td>

        </tr>   

        <tr>

          <td style="font-size:12px;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="24%">&#160;</td>

          <td style="font-size:12px;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="12%">&#160;</td>

          <td style="font-size:12px;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="12%">&#160;</td>

          <td style="font-size:12px;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="6%">Y</td>

          <td style="font-size:12px;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" width="6%">N</td>

      <td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF">&#160;</td>

        </tr>                   

        <tr>

          <td style="font-weight:bold;font-size:12px;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF" height="15">GRAND TOTAL</td>

          <td style="font-size:12px;font-family:Arial;font-style: italic;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF"><%=orderPONoRow.get("orderLineCaseSum", 0) %></td>

          <td style="font-size:12px;font-family:Arial;font-style: italic;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" align="center" bgcolor="#FFFFFF"><%=orderPONoRow.get("weightItemAndPallet", 0) %></td>

          <td colspan="5" style="font-size:12px;font-family:Arial;border-bottom: 1px solid black; border-left: 1px solid black; border-top: 1px solid black;border-right: 1px solid black;" bgcolor="#999999">&#160;</td>

        </tr>

      </table>

    

    <!--<table cellpadding="0" cellspacing="1" bgcolor="#000000" border="0" width="100%">

        <tbody><tr>

          <td colspan="6" align="left" valign="top">

            <div style="background-color:#000; color:#FFF;font-size:12px;font-family:Arial; font-weight:bold;" align="center">CUSTOMER ORDER INFORMATION</div>

          </td>

        </tr>

        <tr>

          <td style="font-size:12px;font-family:Arial;font-weight:bold;" align="center" bgcolor="#FFFFFF" width="24%">CUSTOMER ORDER NUMBER</td>

          <td style="font-size:12px;font-family:Arial;font-weight:bold;" align="center" bgcolor="#FFFFFF" width="12%">#PKGS</td>

          <td style="font-size:12px;font-family:Arial;font-weight:bold;" align="center" bgcolor="#FFFFFF" width="12%">WEIGHT</td>

          <td colspan="2" style="font-size:12px;font-family:Arial;" align="center" bgcolor="#FFFFFF"><span style="font-weight:bold;">PALLET/SLIP</span><br><span style="font-size: 9px;">(CIRCLE ONE)</span></td>

          <td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;" align="center" bgcolor="#FFFFFF">ADDITIONAL SHIPPER INFO</td>

        </tr>

        <tr>

          <td style="font-size:12px;font-family:Arial;font-style: italic;" align="center" bgcolor="#FFFFFF" width="24%"><%=orderPONoRow.getString("PONo") %></td>

          <td style="font-size:12px;font-family:Arial;font-style: italic;" align="center" bgcolor="#FFFFFF" width="12%"><%=orderPONoRow.get("orderLineCaseSum", 0) %></td>

          <td style="font-size:12px;font-family:Arial;font-style: italic;" align="center" bgcolor="#FFFFFF" width="12%"><%=orderPONoRow.get("weightItemAndPallet", 0) %></td>

          <td style="font-size:12px;font-family:Arial;" align="center" bgcolor="#FFFFFF" width="6%">Y</td>

          <td style="font-size:12px;font-family:Arial;" align="center" bgcolor="#FFFFFF" width="6%">N</td>

          <td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;" align="center" bgcolor="#FFFFFF">&nbsp;</td>

        </tr>   

        <tr>

          <td style="font-size:12px;font-family:Arial;" align="center" bgcolor="#FFFFFF" width="24%">&nbsp;</td>

          <td style="font-size:12px;font-family:Arial;" align="center" bgcolor="#FFFFFF" width="12%">&nbsp;</td>

          <td style="font-size:12px;font-family:Arial;" align="center" bgcolor="#FFFFFF" width="12%">&nbsp;</td>

          <td style="font-size:12px;font-family:Arial;" align="center" bgcolor="#FFFFFF" width="6%">Y</td>

          <td style="font-size:12px;font-family:Arial;" align="center" bgcolor="#FFFFFF" width="6%">N</td>

      <td colspan="3" style="font-size:12px;font-weight:bold;font-family:Arial;" align="center" bgcolor="#FFFFFF">&nbsp;</td>

        </tr>                   

        <tr>

          <td style="font-weight:bold;font-size:12px;font-family:Arial;" align="center" bgcolor="#FFFFFF" height="15">GRAND TOTAL</td>

          <td style="font-size:12px;font-family:Arial;font-style: italic;" align="center" bgcolor="#FFFFFF"><%=orderPONoRow.get("orderLineCaseSum", 0) %></td>

          <td style="font-size:12px;font-family:Arial;font-style: italic;" align="center" bgcolor="#FFFFFF"><%=orderPONoRow.get("weightItemAndPallet", 0) %></td>

          <td colspan="5" style="font-size:12px;font-family:Arial;" bgcolor="#999999">&nbsp;</td>

        </tr>

      </tbody></table>-->

    </div>

    

    <div id="a2">

      <!-- CARRIER INFORMATION -->

      

       <table cellpadding="0" cellspacing="0" border="0" width="100%">

       <thead>

        <tr>

          <td colspan="9" style="color:#FFF;font-size:12px;font-family:Arial;" align="center" bgcolor="#000000" height="19" valign="top">

              CARRIER INFORMATION

          </td>

        </tr>     

        <tr>

          <td colspan="2" style="font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;" align="center">HANDLING <br>UNIT</td>

          <td colspan="2" style="font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;" align="center">PACKAGE</td>

          <td rowspan="2" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%"><span style="font-weight:bold"><span style="font-weight:bold">WEIGH</span>T</span></td>

          <td rowspan="2" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">H.M.<br>(X)</td>

          <td rowspan="2" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial; " align="center" width="38%"><span style="font-weight:bold">COMMODITY DESCRIPTION</span><br>

          <span style="font-family:Arial;font-size:7px; padding-left:5px;padding-right:5px;">Commodities requiring special or 

additional care or attention in handling or stowing must be so marked and

 packaged as to ensure safe transportation with ordinary care.See 

Section 2(e) of NMFC Item 360</span></td>

          <td colspan="2" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center">LTL ONLY</td>

        </tr>

        <tr>

          <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="7%">QTY</td>

          <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">TYPE</td>

          <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="7%">QTY</td>

          <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">TYPE</td>

          <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="9%">NMFC#</td>

          <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center" width="9%">CLASS</td>

        </tr>

       </thead>  <tbody>

        <%

        if(orderItemRows.length <6)

        {

          for(int j = 0; j < orderItemRows.length; j ++) {

              double palletCount = orderItemRows[j].get("palletCount", 0d);

              int orderLineCase = orderItemRows[j].get("orderLineCase", 0);

              int weight = orderItemRows[j].get("weightItemAndPallet", 0);

      %>

        <tr>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center"><%=MoneyUtil.formatDoubleDecimal(palletCount) %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">Plts</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderLineCase %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=0!=orderLineCase?"CTNS":"Piece" %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=MoneyUtil.formatDoubleDecimal(weight) %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center">

          <%="("+orderItemRows[j].get("OrderedQty", 0)+" "+wmsItemUnitKey.getWmsItemUnitKeyByKey(orderItemRows[j].getString("Unit"))+")"+orderItemRows[j].getString("CommodityDescription") %>&nbsp;

          </td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderItemRows[j].getString("NMFC") %>&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderItemRows[j].getString("FreightClass") %>&nbsp;</td>

        </tr>

        

        <%

       }

      }%>

      

      <%

        int blankCount = orderItemRows.length <5+1?(6-orderItemRows.length):5;

        for(int j = 0; j < (5-orderItemRows.length); j ++) {

      %>

        

        <tr>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

        </tr>

         <%} %>

      

      <!--GRAND TOTAL-->

      <%

        double palletCountSum = 0d;

        int caseCountSum = 0;

        int weightSum = 0;

        for(int j = 0; j < orderItemRows.length; j ++) {

            double palletCount = orderItemRows[j].get("palletCount", 0d);

            palletCountSum += palletCount;

            int orderLineCase = orderItemRows[j].get("orderLineCase", 0);

            caseCountSum += orderLineCase;

            int weight = orderItemRows[j].get("weightItemAndPallet", 0);

            weightSum += weight;

        }

      %>

        

        <tr>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center"><%=0!=palletCountSum?MoneyUtil.formatDoubleUpInt(palletCountSum):"" %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=caseCountSum %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=weightSum %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-weight: bold;" align="center">GRAND TOTAL</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">&nbsp;</td>

        </tr>

      </tbody>

   </table>

  </div>

    <!-- end -->

    <div id="a3">

   <table cellpadding="0" cellspacing="0" border="0" width="100%">

        <tbody><tr>

          <td style="border-bottom:1px solid black;border-top:3px solid black; border-right:1px solid black;border-left:1px solid black; font-size:10px;font-family:Arial;" align="left" height="20" valign="top" width="57%">

       <!--       <div style="margin-left:5px;border:1px solid red; width:180px; float: left">

          <span style="font-size:11px;font-family:Arial;font-weight:bold;"><b>CONSIGNEE SIGNATURE/DATE</b></span>

                </div>

                <div style="border:0px solid blue;width:200px;float:left;margin-right:1px;">

                  <div>Property described above is received in good order,except as noted.</div>

                  <div>&nbsp;</div>

                  <div>X________________________________</div>

                  <div>Consignee Signature/Print Name</div>

                </div>

        -->         

                <table>

                  <tr>

                    <td valign="top" width="200px;">

                      <span style="font-size:10px;font-family:Verdana;font-weight:bold;"><b>CONSIGNEE SIGNATURE/DATE</b></span>

                    </td>

                    <td>

                      <div style="font-size: 7px; font-family:Arial;font-style: italic;">

                        Property described above is received in good order,except as noted.

                      </div>

                      <div style="font-size: 5px; font-family:Arial;">&nbsp;</div>

                      <div style="font-size: 5px; font-family:Arial;">&nbsp;</div>

                      <div style="font-size: 5px; font-family:Arial;">&nbsp;</div>

                      <div style="font-size: 5px; font-family:Arial;">&nbsp;</div>

                      <div style="font-size: 5px; font-family:Arial;">&nbsp;</div>

                      <div style="font-size: 5px; font-family:Arial;">X_________________________________________________________________________</div>

                      <div style="font-size: 10px; font-family:Arial;">Consignee Signature/Print Name</div>

                    </td>

                  </tr>

                </table>

            </td>

          <td style="border-bottom:3px solid black;border-right:3px solid black;border-left:3px solid black; border-top:3px solid black;font-family:Arial; font-size:15px;" align="left" valign="top" width="45%">

            <div><span style="font-weight:bold; padding-left:2px; margin-bottom:10px;">COD Amount:</span> $___________________________</div>

            <div style="font-weight:bold;text-align:center;margin-top:2px;">Fee Terms:&nbsp;Collect:

            

              <input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;" type="text">

        &nbsp;Prepaid:

            

              <input style="text-align:center;width:12px;height:12px;background:#FFF;border:2px solid #000;" type="text">

            </div>

            <div style="font-weight:bold;text-align:center;margin-top:2px;"><label for="customer">Customer check acceptable:</label>       

              <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg">

            </div>

          </td>

        </tr>

        <tr>

          <td colspan="2" style="font-size:12px;font-family:Arial;border-bottom:1px solid black;border-left:1px solid black;border-right:1px solid black;font-weight:bold;" align="left" height="20" valign="top">

            <div style="margin-left:5px;margin-right:5px;">NOTE Liability Limitation for loss or damage in this shipment may be applicable. See 49 U.S.C. §14706(c)(1)(A)and (B). </div></td>

        </tr>        

        <tr>

          <td colspan="2">

              <table cellpadding="0" cellspacing="0" border="0" width="100%">

              <tbody><tr>

              <%

                  String tempRelativeFile = relativeValue+"_"+orderNo ;

              %>

                <td onclick="androidSign('<%=tempRelativeFile %>','<%=tempShipperKey %>','<%=RelativeFileKey.BiLLOfLoadingShipper %>')" style="border-bottom:1px solid black; border-right:1px solid black;border-left:1px solid black;font-size:12px;font-family:Arial;" height="180" valign="top" width="34%">

                  <div style="font-weight:bold; padding-left: 5px;margin-top:5px;">SHIPPER SIGNATURE/DATE</div>

                  <div style="font-size: 9px; padding-left: 5px;">This is to certify that the 

above named materials are properly classified,packaged,marked and 

labeled, and are in proper condition for transportation according to the 

applicable regulations of the DOT.</div>

                  <br>

                  <%--

                  <div>&nbsp;</div>

                  <div>&nbsp;</div>

                  <div style="padding-left: 5px;">X</div>

                  

                   --%>

                   <div id='<%=tempRelativeFile+"_"+tempShipperKey %>' style="padding-left: 5px; border-bottom:1px solid black;font-family:Arial;font-size:12px; height:70px;">

                      <%

                        //因为Bol的详细需要每一个都签字，所以在bol的时候relative_file 都添加上order

                        DBRow shipperImag = relativeFileMgrZr.queryReativeFile(tempRelativeFile, RelativeFileKey.BiLLOfLoadingShipper);

                          if(shipperImag != null){

                            String shipperUrl = shipperImag.getString("relative_file_path");

                            %>

                            <div style="float:left;">

                    <img class="shipper_image" src="<%=ConfigBean.getStringValue("systenFolder")+"_fileserv_signature/file/"+shipperUrl %>" width="150px" height="70px" />

                   

                            </div>

                            <% 

                          }

                      %>

                      

                      <%

                      if(shipperImag != null && !StringUtil.isNull(shipperImag.getString("create_time")) ){

                    %>

                       <div style="float:right;padding-top:50px;font-size:10px;" >&nbsp;&nbsp; 

                        <% 

                          String shipperImagTime = shipperImag.getString("create_time") ;
                          if(!shipperImagTime.equals("")){
                             out.print( DateUtil.showLocalparseDateToNoYear24Hours(shipperImagTime,ps_id));
                          }
                          

                        %>

                        &nbsp;&nbsp;

                      </div>

                      <% }%>

                      

                  </div>

                  

                  <div style="padding-left: 5px; font-family:Arial;font-size:12px;">

                    <div style="float:left;">Signature/ Print Name</div> 

                    <div style="float:right;margin-right: 50px;" >Date</div>

                  </div>

                </td>

                <td style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;font-size:12px;font-family:Arial;" valign="top" width="14%">

                  <div style="text-decoration:underline;font-family:Arial;font-size:12px;margin-top:5px;">Trailer Loader:</div>

                  <div style="margin-top:5px;margin-left:5px;text-align:left;font-size:12px;">

                    <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg">

                    By Shipper

                  </div>

                  <div style="margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;">

                    <p>

                      <!-- <input name="prepaid2" id="prepaid2" style="width:10px;height:10px;background:#FFF;border:2px solid #000;" type="text"> --> 

                      <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/kongcha.jpg">

                      By Driver                 </p>

                  </div>

                </td>

                <td style="border-bottom:1px solid black; border-right:1px solid black;text-align:center;font-size:12px;font-family:Arial;" valign="top" width="16%">

                  <div style="text-decoration:underline;font-family:Arial;font-size:12px;margin-top:5px;">Freight Counted:</div>

                  <div style="margin-top:5px;margin-left:5px;text-align:left;font-family:Arial;font-size:12px;">

                  <img alt="" src="<%=ConfigBean.getStringValue("systenFolder") %>administrator/imgs/print/cha.jpg">

                  By Driver/Pieces</div>

                  <div style="margin-left:5px;margin-top:12px;font-family:Arial;font-size:12px;">NO SHIPPER LOADER COUNT</div>

                </td>

                <td onclick="androidSign('<%=tempRelativeFile %>','<%=tempCarrierKey %>','<%=RelativeFileKey.BillOfLoadingCarrier %>')"  style="border-bottom:1px solid black;border-right:1px solid black;" valign="top" width="36%">

                  <div style="font-weight: bold;font-family:Arial;font-size:12px;margin-left:5px;margin-top:5px;">CARRIER SIGNATURE/PICKUP DATE</div>

                  <div style="font-size:7px;font-family:Arial;margin-left:5px;margin-right:5px;">Carrier 

acknowledges receipt of packages and required placards.Carrier certifies

 emergency response information was made available and/or carrier has 

the DOT emergency response guidebook or equivalent  documentation in the

 vehicle.</div>

                  <div style="font-style: italic;font-size:9px;font-weight: bold;margin:5px 5px 0px 5px;font-family:Arial;">Property described above is received in good order, except as noted.</div>

                  <%--

                  <div>&nbsp;</div>

                  <div>&nbsp;</div>

                  <div style="font-size:10px;font-family:Arial;margin-left:5px;">X&nbsp;</div>

                     --%><div id='<%=tempRelativeFile+"_"+tempCarrierKey %>' style="border-bottom:1px solid black;font-family:Arial;font-size:12px; height:70px;  ">

                      <%

                        DBRow carrierImag = relativeFileMgrZr.queryReativeFile(tempRelativeFile, RelativeFileKey.BillOfLoadingCarrier);

                          if(carrierImag != null){

                            String carrierUrl = carrierImag.getString("relative_file_path");

                            %>

                            <div style="float:left;">

                    

          <img class="carrier_image" src="<%=ConfigBean.getStringValue("systenFolder")+"_fileserv_signature/file/"+carrierUrl %>" width="150px" height="70px" />

                            </div>

                            <% 

                          }

                      %>

                      

                      <%

                      if(carrierImag != null && !StringUtil.isNull(carrierImag.getString("create_time")) ){

                      %>

                       <div style="float:right;padding-top:50px;font-size:10px;" >&nbsp;&nbsp;

                        <% 

                          String carrierImagTime = carrierImag.getString("create_time") ;
                          if(!carrierImagTime.equals("")){
                            out.print( DateUtil.showLocalparseDateToNoYear24Hours(carrierImagTime,ps_id));
                          }
                          

                        %>

                        &nbsp;&nbsp; 

                      </div>

                      <% }%>

                  </div>

                  <div style="font-size:12px;font-family:Arial;margin-left:5px;float:left">

                    <div style="float:left;">Signature/ Print Name</div>  

                    <div style="float:right;margin-left:70px">Date</div>

                  </div>

                  <div>&nbsp;</div>

                  <div style="font-weight: bold;font-family:Arial;font-size:12px;margin-left:5px;"> 

                      <table width="98%">

                      <tr>

                        <td width="50%" style="font-style: italic;font-size:10px;font-weight: normal;" align="left">Gate In:&nbsp;<%=DateUtil.showLocalparseDateToNoYear24Hours(equipRow.getString("check_in_time"),ps_id)%></td>

                        <td style="font-style: italic;font-size:10px;font-weight: normal;" align="left">

                        <%if(!equipRow.getString("check_in_window_time").equals("")){ %>

                          Window In:&nbsp;<%=DateUtil.showLocalparseDateToNoYear24Hours(equipRow.getString("check_in_window_time"),ps_id)%>

                      <%} %>

                        </td>

                      </tr>                     

                    </table>

                      

                     <% 

                       String timeIn = ""; //timeIn 的时间是DockCheckIn的时间

                       String timeOut = "";//timeOut 的时间是签字的时间create_time

                       if(equipRow != null){

                       timeIn = DateUtil.showLocalparseDateToNoYear24Hours(equipRow.getString("check_in_warehouse_time"),ps_id);

                       timeOut = DateUtil.showLocalparseDateToNoYear24Hours(equipRow.getString("check_out_warehouse_time"),ps_id);

                      }

                  %>

                   <table width="98%">

                    <tr>

                      <td width="50%" style="font-style: italic;font-size:10px;font-weight: normal;" align="left">Dock In:&nbsp;<%=timeIn %></td>

                      <td style="font-style: italic;font-size:10px;font-weight: normal;" align="left">Dock Close:&nbsp;<%=timeOut %></td>

                    </tr>

                  </table>

                  

                  </div>

                </td>

              </tr>

            </tbody></table>

          </td>

        </tr>

      </tbody>

</table>

    </div>

    

    

<%

 if(orderItemRows.length >=6){



%>   

    

     

        <%

       

          for(int j = 0; j < orderItemRows.length; j ++) {

              double palletCount = orderItemRows[j].get("palletCount", 0d);

              int orderLineCase = orderItemRows[j].get("orderLineCase", 0);

              int weight = orderItemRows[j].get("weightItemAndPallet", 0);

        if(line_number%page_size==0){

          page_num++;%>

         <div id="a4">
       <!--page-break-start-->
           <div style="border:1px red solid; width:195mm;  margin:0 auto; margin-top:3px;page-break-before:always;" id="top">
      <!--header start-->
            <table cellpadding="0" cellspacing="0" border="0" width="100%">

            <tbody><tr>

               <td colspan="6" style="border-top: 1px solid black; border-left: 1px solid black;border-right: 1px solid black;border-bottom:1px solid black;" align="center">

               <table cellpadding="0" cellspacing="0" border="0" width="100%">

              <tbody><tr>

                <td style="font-size:12px; font-family:Arial;" align="center" height="39" width="17%"><b>Date:</b><span style="font-style: italic;"><%=DateUtil.showLocalparseDateTo24Hours(DateUtil.NowStr(),ps_id) %></span></td>

                <td align="center" width="72%">

                <div style="font-size:18px; font-family:Arial; font-weight: bold;" align="center">SUPPLEMENT TO THE BILL OF LADING</div>

                  <div style="font-size:14px; font-family:Arial;" align="">&nbsp;&nbsp;&nbsp;&nbsp;<b>BILL OF Lading Number:</b><span style="font-style: italic;"><%=orderNo %></span></div>

                </td>

                <td style="border-bottom: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; font-size: 12px; font-family: Arial;" align="center" width="11%">PAGE:<%=page_num%>/<%=total_page_num%></td>
              </tr>

              </tbody></table>

               </td>

              </tr>

             </tbody>

            </table>
      <!--header end-->
          </div>

          
      <!--table start-->
           <table cellpadding="0" cellspacing="0" border="0" width="100%">

             <thead>

            <tr>

              <td colspan="9" style="color:#FFF;font-size:12px;font-family:Arial;" align="center" bgcolor="#000000" height="19" valign="top">

                CARRIER INFORMATION

              </td>

            </tr>     

            <tr>

              <td colspan="2" style="font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;" align="center">HANDLING <br>UNIT</td>

              <td colspan="2" style="font-weight:bold;font-size:12px;font-family:Arial;border-left:1px black solid;border-bottom:1px black solid;" align="center">PACKAGE</td>

              <td rowspan="2" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%"><span style="font-weight:bold"><span style="font-weight:bold">WEIGH</span>T</span></td>

              <td rowspan="2" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">H.M.<br>(X)</td>

              <td rowspan="2" style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial; " align="center" width="38%"><span style="font-weight:bold">COMMODITY DESCRIPTION</span><br>

              <span style="font-family:Arial;font-size:7px; padding-left:5px;padding-right:5px;">Commodities requiring special or 

        additional care or attention in handling or stowing must be so marked and

         packaged as to ensure safe transportation with ordinary care.See 

        Section 2(e) of NMFC Item 360</span></td>

              <td colspan="2" style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center">LTL ONLY</td>

            </tr>

            <tr>

              <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="7%">QTY</td>

              <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">TYPE</td>

              <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="7%">QTY</td>

              <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="8%">TYPE</td>

              <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" width="9%">NMFC#</td>

              <td style="font-weight:bold;border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center" width="9%">CLASS</td>

            </tr>

             </thead>  

        

         <%}%>

      

        <tbody><tr>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center"><%=MoneyUtil.formatDoubleDecimal(palletCount) %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">Plts</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderLineCase %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=0!=orderLineCase?"CTNS":"Piece" %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=MoneyUtil.formatDoubleDecimal(weight) %></td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center">&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center">

          <%="("+orderItemRows[j].get("OrderedQty", 0)+" "+wmsItemUnitKey.getWmsItemUnitKeyByKey(orderItemRows[j].getString("Unit"))+")"+orderItemRows[j].getString("CommodityDescription") %>&nbsp;

          </td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderItemRows[j].getString("NMFC") %>&nbsp;</td>

          <td style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=orderItemRows[j].getString("FreightClass") %>&nbsp;</td>

        </tr>

        

        <%

         if(j == orderItemRows.length-1){%>

        <tr>

        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center"><%=0!=palletCountSum?MoneyUtil.formatDoubleUpInt(palletCountSum):"" %></td>

        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">&nbsp;</td>

        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=caseCountSum %></td>

        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">&nbsp;</td>

        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-style: italic;" align="center"><%=weightSum %></td>

        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">&nbsp;</td>

        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;font-weight: bold;" align="center">GRAND TOTAL</td>

        <td style="border-left:1px black solid;border-bottom:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">&nbsp;</td>

        <td style="border-left:1px black solid;border-bottom:1px black solid;border-right:1px black solid;font-size:12px;font-family:Arial;" align="center" bgcolor="#999999">&nbsp;</td>

      </tr>

      <%}

    

    if(j == orderItemRows.length-1 || (line_number+1)%page_size==0){%>

          </tbody>

         </table>
    <!--table end-->
       </div>

     <%}

      line_number++; 

     }

      %>

     

 <%} %>   



    </div>

<!-- loop body end -->

    

    

  <%} %>

    <%} %>

   <%} %>

  </body>

</html>

<script>



function supportAndroidprint(){

  //获取打印机名字列表

    var printer_count =  visionariPrinter.GET_PRINTER_COUNT();

     //判断是否有该名字的打印机

    var printer = "<%=printName%>";

    var printerExist = "false";

    var containPrinter = printer;

  for(var i = 0;i<printer_count;i++){

    if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){

      printerExist = "true";

      containPrinter=visionariPrinter.GET_PRINTER_NAME(i);

      break;

    }

  }

  if(printerExist=="true"){

    return androidIsPrint(containPrinter);

  }else{

    var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面

    if(op!=-1){ //判断是否点了取消

      return androidIsPrint(containPrinter);

    }

  }

}



function androidIsPrint(containPrinter) {

  //alert("222222");
debugger;
  var printHtml=$('div[name="printHtml"]');

  var flag = true ;

  for(var i=0;i<printHtml.length;i++) {

    var a1=$('#a1',printHtml[i]);

    var a2=$('#a2',printHtml[i]);

    var a3=$('#a3',printHtml[i]);

    var a4=$('#a4',printHtml[i]);

    //zhangrui

    var signobj = getSignImage(printHtml[i]);

    printPreSignDisplayImage(signobj);

    

    visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");      // 设置页码字体

    visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");

    visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  

    visionariPrinter.ADD_PRINT_HTM(0,0,"100%","270mm",a1.html());

    visionariPrinter.SET_PRINT_STYLEA(0,"Top2Offset",55);

  

    visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a2.html());

    visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  

   

    visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",a3.html());

    visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);

    

    var orderItemLength =$('#orderItemLength',printHtml[i]).val()*1; 
  var strHtml = printHtml[i].innerHTML;
  var arr = strHtml.split("<!--page-break-start-->")||[];
   for(var j=1;j<arr.length;j++){
     var innerHtml = arr[j].replace("page-break-before:always;","");
     innerHtml.replace("width:195mm","100%");
     
     visionariPrinter.NewPageA();
     var reg1 =/<!--header\s+start-->([\s\S]*)?<!--header\s+end-->/;
     var reg2 =/<!--table\s+start-->([\s\S]*)?<!--table\s+end-->/ig;
     
     //header 
     var headerArr = innerHtml.match(reg1)||[];
     if(headerArr.length>0){
       visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",headerArr[0]);
     }
     
     //body
     var bodyArr = innerHtml.match(reg2)||[];
     if(bodyArr.length>0){
       var strBody = bodyArr[0];
       var tableArr = strBody.split("<!--table start-->");
       for(var k=1;k<tableArr.length;k++){
         var tableInnerArr = tableArr[k].split("<!--table end-->");
         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",tableInnerArr[0]);
          visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);  
       }
     }
   }
    /**if(orderItemLength>6){

      visionariPrinter.NewPageA();

      visionariPrinter.ADD_PRINT_TABLE("40",0,"100%","100%",a4.html()); 

         //visionariPrinter.SET_PRINT_STYLEA(0,"LinkedItem",-1);

    }

    

    visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#top',printHtml[i]).html());

    visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);                   //页头

    visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","First");          //第一页不显示

     

    var isSign = printAddSignImage(visionariPrinter,signobj);

    

   //visionariPrinter.ADD_PRINT_TEXT("0.6cm",670,"100%","100%","PAGE:#/&"); 

    visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);                   //页码

 **/  
  
    visionariPrinter.SET_PRINT_COPIES(1);

    //visionariPrinter.PREVIEW();
    var isSign = printAddSignImage(visionariPrinter,signobj);
        flag = flag &&  visionariPrinter.PRINT(); 

        

      //添加shipper 和carrier签字的图片的才转存pdf

      if(isSign){

       //downLoad(i);//记录打印的标签，转pdf用

      }

     

  }

  return flag ;

}



function downLoad(i) {

   var message="";

    var printAll = $('div[name="printHtml"]');



    var entryId = "<%=entry_id%>";

    var type="bol_"+(i+1);

    var companyId = $('#companyId', printAll[i]).val();

    var customerId = $('#customerId', printAll[i]).val();

    var loadNo = $('#loadNo', printAll[i]).val();

    var dn = $('#dn',printAll[i]).val();

    var html = $(printAll[i]).html();

    var reg1 = new RegExp("(<input (.*?)>)", "gmi");

    var reg2 = new RegExp("(<img (.*?)>)", "gmi");



    html = html.replace(/nbsp/gi, "#160");

    html = html.replace(/<br>/gi, "<br></br>");

    html = html.replace(reg1, "$1</input>");

    html = html.replace(reg2, "$1</img>");

    var template = '<html xmlns="http://www.w3.org/1999/xhtml">\
              <head>\
                <meta name="generator" content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />\
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\
                  <style type="text/css">\
                  @page:left { margin: 5mm }\
                  @page:right { margin: 5mm }\
          @page{ size: 210mm 297mm;}\
                </style>\
                <title>a4print</title>\
              </head>\
              <body>\
              <div style="border:1px #FFFFFF solid; width:195mm;">';

    template += html + "</div></body></html>";

    $.ajax({

        url : '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/Html2PdfUploadAction.action',

        async : false,

        data:'entryId='+entryId+'&type='+type+'&companyId='+companyId+'&customerId='+customerId+'&loadNo='+loadNo+'&dn='+dn+'&template='+encodeURIComponent(template), 

        dataType : 'text',

        type : 'post',

        success : function (data) {

            var result = $.parseJSON(data);

            if (result.message === "true") {

              message = true;

            }

        }

    });



    return message;



}



$(function(){

  //androidToPdf();

});

</script>

