<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
long psId= StringUtil.getLong(request,"ps_id");
String area_id= StringUtil.getString(request,"area_id");
String area_name =StringUtil.getString(request,"area_name");
DBRow[] zoneDocks=null;
if(area_id!=null && !area_id.equals("")){
	zoneDocks=googleMapsMgrCc.getAreaDoorByPsIdandArea(psId,Long.parseLong(area_id));
}else{
	DBRow[] areas = googleMapsMgrCc.getAreaPositionByName(psId, area_name);
	DBRow area = new DBRow();
	if(areas.length>0){
		area = areas[0];
		area_id=area.getString("area_id");
		zoneDocks=googleMapsMgrCc.getAreaDoorByPsIdandArea(psId,Long.parseLong(area_id));
	}
}
String dock_names="";
String dock_Ids="";
if(zoneDocks!=null && zoneDocks.length>0){
	for(int i=0;i<zoneDocks.length;i++){
		DBRow row=zoneDocks[i];
		if(!dock_names.equals("")){
			dock_names+=","+row.getString("doorId");
		}else{
			dock_names+=row.getString("doorId");
		}
		if(!dock_Ids.equals("")){
			dock_Ids+=","+row.getString("sd_id");
		}else{
			dock_Ids+=row.getString("sd_id");
		}
	}
}

%>
<html>
  <head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入提示信息 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<style type="text/css">
.text-overflow {
	display:block;
	word-break:keep-all;
	white-space:nowrap;
	overflow:hidden;
	text-overflow:ellipsis;
}
#menu_ {
	width: 150px;
	position: absolute;
	display:none;
	background: #ddd;
	border: solid #ccc 1px;
}

#menu_ ul {
	list-style: none;
	margin: 0px;
	padding: 0px;
}

#menu_ ul li a {
	margin: 0px;
	padding: 5px;
	text-decoration: none;
	display: inline-block;
	height: 20px;
	width: 120px;
	color: #333;
	font-size: 13px;
}

#menu_ ul li a:hover {
	background: #eee;
	border: solid #fff 1px;
	height: 18px;
}
</style>
  </head>
  <body>
  <div id="container" onclick="hideMenu()">
  	<div id="showZoneDocks" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;height: 95%;" tabindex='0'>
	    <table style="width: 100%;height: 95%;">
	    	<tr>
	    		<td>
					<div id="notExitDoor" style="background-color:#FFF; width: 100%; border:2px #dddddd solid; height:100%; overflow:auto" >
					<%
						if(zoneDocks != null && zoneDocks.length>0){
							for(int i=0; i<zoneDocks.length; i++){
								DBRow d = zoneDocks[i];
								%>
								<div title="<%=d.getString("doorId") %>" style="width: 110px; height: 25px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
									<div class="text-overflow" style="width: 100%;height:100%;position:relative; float: left; text-align: center;background-color: #CCFFFF;" onmouseover="this.style.backgroundColor='#2DF7AB'" onmouseout="this.style.backgroundColor='#CCFFFF'" oncontextmenu="showMenu(this,event)">
										<%=d.getString("doorId") %>
										<input type="hidden" id="sd_id" name="sdid" value="<%=d.getString("sd_id") %>">
										<div style="width: 12px; height: 12px; float: right;position:absolute;right:0px;top:0px; margin: 1px; cursor: pointer; background-image: url(http://localhost:8080/Sync10/administrator/imgs/maps/delete_grey.png);" onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/delete_blue.png&quot;)'" onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/delete_grey.png&quot;)'" onclick="removeTitle(this.parentNode)"></div>
									</div>
									<!-- 
									<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_grey.png'); margin: 1px; cursor: pointer;" 
												onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'" 
												onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'"
												onclick="confirm(this.parentNode)">
									</div>
									 -->
								</div>
								<%
							}
						}
					%>
					<div title="add dock" id="add_dock" style="width: 110px; height: 25px; border: solid; border-color: #eeeeee; border-width: 1px; margin: 5px; float: left; cursor: default;" >
									<div class="text-overflow" style="width: 100%;height:100%; float: left; text-align: center;background-color: #eeeeee;cursor: pointer;" onclick="selectDock()">
										<div id="add_bg_img" style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_blue.png'); margin: 1px; cursor: pointer;margin-top: 8px;margin-right: 50px;" 
												onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'" 
												onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'"
												">
										</div>
									</div>
					</div>
					</div>
				</td>
	    	</tr>
	    </table>
	</div>
	</div>
	<!-- 右键菜单样式 -->
		<!-- <div id="menu_" style="width:auto;">
			<ul>
				<li id="modifyZone" onclick="assignArea()" ><a
					href="#">assign the dock</a></li>
			</ul>
		</div> -->
	
  </body>
<script type="text/javascript">
var dName='<%=dock_names%>',olddock_Ids='<%=dock_Ids%>',dockName,dockid,eleNode;
//注册键盘事件
document.onkeydown = function(e) {
    //捕捉回车事件
    var ev = (typeof event!= 'undefined') ? window.event : e;
     if(ev.ctrlKey && ev.keyCode==83){
    	commitData();
    	return false;
    }else if(ev.ctrlKey&& ev.keyCode==65){
    	selectDock();
    	return false;
    }else if(ev.ctrlKey&& ev.keyCode==67){
    	closeWindow();
    	return false;
    }
}
$(document).ready(function(){
	$(document).find("#showZoneDocks").bind("contextmenu",function(e){
		return false;
	});
	var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	var updateDialog = null;
	var addDialog = null;
	api.button([
			{
				name: 'Submit',
				callback: function () {
					commitData();
					return false;
				},
				focus:true,
				focus:"default"
			},
			{
				name: 'Cancel'
				
				
			}] )
});
function closeWindow() {
	$.artDialog.close();
}
//移除除门
function removeTitle(ele){
	var info='Is it really will be removed '+ele.parentNode.title+' from <%=area_name%>';
	maskLayer(ele,info);
}
//添加门
function selectDock(){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_door.html?ps_id=<%=psId%>&dock='+dName+'&flag=addDock';
	$.artDialog.open(url, {title: "Select Dock ["+'<%=area_name%>'+"]",width:'825px',height:'435px', lock: true,opacity: 0.3});
}
/**
 * 回填
 */
 function backFillDoor(names,ids){
	if(dName){
		dName+=","+names;
	}
	 var names_=names.split(',');
		var ids_=ids.split(',');
		for(var i=0;i<names_.length;i++){
			var node="<div title='"+names_[i]+"' style='width: 110px; height: 25px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;' >"+
			"<div class='text-overflow' style='width: 100%;height:100%; float: left; text-align: center;background-color: #CCFFFF;position: relative;' onmouseover=\"this.style.backgroundColor='#2DF7AB'\" onmouseout=\"this.style.backgroundColor='#CCFFFF'\" >"+
			names_[i]+"<input type='hidden' name='sdid' class='adid' value='"+ids_[i]+"'>"+
			"<div style='width: 12px; height: 12px; float: right;position:absolute;right:0px;top:0px; margin: 1px; cursor: pointer; background-image: url(http://localhost:8080/Sync10/administrator/imgs/maps/delete_grey.png);' onmousemove=\"this.style.backgroundImage='url(&quot;../imgs/maps/delete_blue.png&quot;)'\" onmouseout=\"this.style.backgroundImage='url(&quot;../imgs/maps/delete_grey.png&quot;)'\" onclick=\"removeTitle(this.parentNode)\"></div>"+
			"</div> </div>";
			$("#add_dock").before(node);
		}
		$("#showZoneDocks").focus();
}
/**
 * 提交修改
 */
 function commitData(){
		var sdidNodes=document.getElementsByName("sdid");
		var sdids="";
		var contrastFlag=false;
		if(sdidNodes.length>0){
			for(var i=0;i<sdidNodes.length;i++){
				if($.inArray(sdidNodes[i].value,olddock_Ids.split(','))<0){
					contrastFlag=true;
				}else if(olddock_Ids.split(',').length>sdidNodes.length){
					contrastFlag=true;
				}
				if(sdids!=""){
					sdids+=","+sdidNodes[i].value;
				}else{
					sdids+=sdidNodes[i].value;
				}
			}
		}else{
			contrastFlag=true;
		}
		if(contrastFlag){
			$.ajax({
				data:'sdIds='+sdids+'&position_id=<%=area_id%>&olddock_Ids='+olddock_Ids,
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/zoneDockHaldeAction.action',
				dataType:'json',
				type:'post',
				beforeSend:function(request){
				 	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				success:function(data){
					if(data && data.flag=="true"){
						showMessage("Save succeed","succeed");
						 setTimeout('closeWindow()',1500);
						 $.artDialog.opener.reloadDock(<%=psId%>);
						
					}
				},
		   	 	error:function(){
		   			showMessage("System error","error");
		   	  	}
			});
		}else{
			showMessage("didn't any change","error");
			setTimeout('closeWindow()',1500);
		}
		
		
	}
//显示右键菜单
 function showMenu(ele,event){
 	eleNode=ele;
 	dockName=ele.childNodes[0].data.trim();
 	dockid=ele.childNodes[1].value;
 	var evt = event || window.event;
 	var container = document.getElementById('showZoneDocks'); 
 	var rightedge = container.clientWidth-evt.clientX;
     var bottomedge = container.clientHeight-evt.clientY;
     var menu = document.getElementById('menu_');
 	var display=$('#menu_').css('display');
 	if(display='inherit'){
 		$('#menu_').css('display','none');
 	}
 	display=$('#menu_').css('display');
 	if(display=='none'){
 		$('#menu_').css('display','inherit');
 		if (rightedge < menu.offsetWidth)
 			 menu.style.left = container.scrollLeft + evt.clientX - menu.offsetWidth + "px";
 		else
 			 menu.style.left = container.scrollLeft + evt.clientX + "px";
 		 if (bottomedge < menu.offsetHeight)
 	            menu.style.top = container.scrollTop + evt.clientY - menu.offsetHeight + "px";
         else
             menu.style.top = container.scrollTop + evt.clientY + "px";
 	}
 }
 /*隐藏菜单*/
 function hideMenu() {
 	$('#menu_').css('display','none');
 } 
 /**
 *更改门的分配区域
 */
 <%-- function assignArea(){
	 var psId='<%=psId%>';
		var type="-1"
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_area_single.html?ps_id='+psId+'&area_type='+type+'&dockName='+dockName+'&area_name=<%=area_name%>';
		$.artDialog.open(url, {title: "Select Area ["+'<%=area_name%>'+"]",width:'790px',height:'425px', lock: true,opacity: 0.3});
		hideMenu();
 } --%>
 /**
 *分配成功后，刷新dock
 */
 function reloadDock(el){
	 if(olddock_Ids.indexOf(dockid)==0){
		 olddock_Ids=olddock_Ids.substring(dockid.length+1,olddock_Ids.length);
	 }else{
		 olddock_Ids=olddock_Ids.substring(0,olddock_Ids.indexOf(dockid)-1)+olddock_Ids.substring((olddock_Ids.indexOf(dockid)+dockid.length),olddock_Ids.length);
	 }
	 var oldAreaId='<%=area_id%>';
	 $.ajax({
     		data:'position_id='+$(el).find("#area_id").val()+'&sdId='+dockid+'&oldAreaId='+oldAreaId+'&olddock_Ids='+olddock_Ids,
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/assignDockAreaAction.action',
			dataType:'json',
			type:'post',
     		success:function(data){
     			if(data && data.flag=="true"){
     				$(eleNode.parentNode).remove();
     			 	 $.artDialog.opener.reloadDock(<%=psId%>);
     				showMessage("Distribution of success","succeed");
     			}
     		},
     		error:function(){
     			showMessage("System error","error");
  		}
     	});
 }
 /**
  * 遮罩层
  */
  function maskLayer(el,info){
 	 $.artDialog({
 		 	title:'Notify',
 		 	lock: true,
 		 	opacity: 0.3,
 		 	icon:'question',
  		    content: "<span style='font-weight:bold;'>"+info+"?</span>",
 		    button: [
 		        {
 		            name: 'YES',
 		            callback: function () {
 		            	$(el.parentNode).remove();
   		            },
 		            focus: true
 		        },
 		        {
 		            name: 'NO'
 		        }
 		    ]
 		});
  }
</script>
</html>