<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 时间 -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 
 <link rel="stylesheet" type="text/css" href="../js/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="../js/easyui/themes/icon.css" />

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 
<style>
.ui-datepicker{font-size: 0.9em;}
.set{padding:2px;width:95%;word-break:break-all;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 0px;border: 2px solid green;} 
p{text-align:left;}
span.stateName{width:32%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:68%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
.zebraTable td { border-bottom: 0px solid #DDDDDD;padding-left: 10px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
	
}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:500px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>
<style type="text/css">
	body {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
	td {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
</style>
<%
		long ps_id = StringUtil.getLong(request,"ps_id");
		String start_time=StringUtil.getString(request, "start_time");
		String end_time=StringUtil.getString(request, "end_time");	
		String key = StringUtil.getString(request,"key");
		String cmd = StringUtil.getString(request,"cmd");
		int search_mode = StringUtil.getInt(request,"search_mode");
		int type = StringUtil.getInt(request, "type");
		int status = StringUtil.getInt(request, "status");
		DBRow[] ps = checkInMgrZwb.findSelfStorage();
		
%>
<title>Plate Level Inventary</title>
<script type="text/javascript">
$(function(){
	     
	$("#tabs").tabs({
   		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
   });
	
	$("#tabs_status").tabs({
		cache: false,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		load: function(event, ui) {onLoadInitZebraTable();}	,
		selected:0,
		show:function(event, ui)
			{
				go(1);
				//filterPlateInfos(0, 1);
			}
		});
 	$('#start_time').datetimepicker({
 		dateFormat: "yy-mm-dd"
 	});    
 	$('#end_time').datetimepicker({
 		dateFormat: "yy-mm-dd"
 	});    
	$("#ui-datepicker-div").css("display","none");
	$(function(){
		$(".chzn-select").chosen({no_results_text: "no this options:"});
	});
});

function loadPlateDatas(start_time, end_time, ps_id, type, status, page)
{
	var para = 'start_time='+start_time+'&end_time='+end_time+'&ps_id='+ps_id+'&type='+type+'&status='+status+'&p='+page;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/plate_level_inventary_datas.html',
		type: 'post',
		data:para,
		dataType: 'html',
		async:false,
		success: function(html){
			$("#datas").html(html);
			//$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
}
function searchPlateByKey(search_key, cmd, search_mode)
{
	var search_key_type = $("#key_type").val();
	var para = 'key='+search_key+'&cmd='+cmd+'&search_mode='+search_mode+'&search_key_type='+search_key_type;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/plate_level_inventary_datas.html',
		type: 'post',
		data:para,
		dataType: 'html',
		async:false,
		success: function(html){
			$("#datas").html(html);
			//$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
}
function filterPlateInfos(search, changedIndex)
{
	var start_time = $("#start_time").val();
	var end_time = $("#end_time").val();	
	var ps_id = $("#ps").val();
	var type = $("input[type=radio][name=type]:checked").val();
	var status = $('#tabs_status').tabs('option', 'selected');
	var page = 0;	
	if(1 != changedIndex)
	{
		page = document.dataForm.p.value;
	}
	//赋值
	document.dataForm.start_time.value = start_time;
	document.dataForm.end_time.value = end_time;
	document.dataForm.ps_id.value = ps_id;
	document.dataForm.type.value = type;
	document.dataForm.status.value = status;
	document.dataForm.p.value = page;
	document.dataForm.cmd.value = "";
	loadPlateDatas(start_time, end_time, ps_id, type, status, page);
	/*
	if(1==search && $("#tabs_status").tabs("select")!=0)
	{
		$("#tabs_status").tabs("select", 0);
	}
	*/
}
function go(page)
{
	document.dataForm.p.value = page;
	var cmd = document.dataForm.cmd.value;
	if(cmd == 'search')
	{
		searchPlateByKey(document.dataForm.key.value,"search" ,document.dataForm.search_key_type.value);
	}
	else
	{
		filterPlateInfos(1);
	}
}
function getLastOfDay(){
		var s = "";
		  var d = new Date();  
		  var c = ":";
		   s += d.getFullYear()+"-";
		   s += fixNumber(d.getMonth() + 1) + "-";
		   s += fixNumber(d.getDate()) + " ";   
		   s += fixNumber(23) + c + "59:59";
		return s;
	}
function getCurrentDay(_date){
		 
		  var s = "";
	  var d = _date || new Date();  
	  var c = ":";
	   s += d.getFullYear()+"-";
	   s += fixNumber(d.getMonth() + 1) + "-";
	   s += fixNumber(d.getDate()) + " ";   
	   s += fixNumber(0) + c + "00:00";
	return s;
}
function fixNumber(num){
	return num < 10 ? "0"+num : num;
}
function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
		
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
		   searchRightButton();
		}  
	}  
}
function search()
{
	var val = $("#search_key").val();
			
	if(val.trim()=="")
	{
		alert("Please enter key word for searching");
	}
	else
	{
		val = val.replace(/\'/g,'');
		var val_search = "\'"+val.toUpperCase()+"\'";
		$("#search_key").val(val_search);
		document.dataForm.cmd.value = "search";
		document.dataForm.key.value = val_search;
		document.dataForm.search_key_type.value = 1;
		searchPlateByKey(val_search,"search" ,1);
	}
}

function searchRightButton()
{
	var val = $("#search_key").val();
			
	if (val=="")
	{
		alert("Please enter key word for searching");
	}
	else
	{
		val = val.replace(/\'/g,'');
		$("#search_key").val(val);
		document.dataForm.cmd.value = "search";
		document.dataForm.key.value = val;
		document.dataForm.search_key_type.value = 2;
		searchPlateByKey(val,"search" ,2);
	}
}

function filter(flag){
	if(flag!="refresh"){
		document.filter_form.ps_id.value = $("#ps").val();
		document.filter_form.start_time.value = $("#start_time").val();
		document.filter_form.end_time.value = $("#end_time").val();
		document.filter_form.tabSelect.value = 1;
	}
	document.filter_form.submit();
}
jQuery(function($){
	if(1==$("#key_type").val())
	{
		addAutoComplete($("#search_key"),
				"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInSearchPlateInventaryJSONAction.action",
				"merge_field","plate_inventary_id");
	}else if(1==$("#key_type").val())
	{
		addAutoComplete($("#search_key"),
				"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/GetSearchCheckInJSONAction.action",
				"merge_field","dlo_id");
	}
	$("#key_type").change(function(){
		$("#search_key").val("");
		if(1==$(this).val())
		{
			addAutoComplete($("#search_key"),
					"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInSearchPlateInventaryJSONAction.action",
					"merge_field","plate_inventary_id");
		}
		else if(2==$(this).val())
		{
			addAutoComplete($("#search_key"),
					"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/GetSearchCheckInJSONAction.action",
					"merge_field","dlo_id");
		}
	});
});

</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<div class="demo" style="width:100%">
	  <div id="tabs">
		<ul>
			<li><a href="#checkin_filter">Advanced Search</a></li>
			<li><a href="#checkin_search">Common Tools</a></li>
		</ul>
	      <div id="checkin_filter">
	      		<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left">
						<div style="margin-bottom: 5px;">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="240px;">
										Start Time:&nbsp;<input maxlength="100" value="<%=start_time %>" id="start_time" />
									</td>
									<td width="240px;">
										End Time:&nbsp;<input maxlength="100" value="<%=end_time %>" id="end_time"  />
									</td>
									<td width="200px;">
										<input type="radio" value="1" name="type" id="type1" checked="checked"><label for="type1" style="cursor: pointer;">Delivery Time</label>
										<input type="radio" value="2" name="type" id="type2"><label for="type2" style="cursor: pointer;">PickUp Time</label>
									</td>
									<td width="280px">	
									<div class="side-by-side clearfix">
										<select id="ps" name="ps" class="chzn-select" data-placeholder="Choose a Ps..." tabindex="1"  style="width:250px">
											<option value="-1">All Storage</option>
											<%
												for(int j = 0;j<ps.length;j++)
												{
											%>
												<option value="<%=ps[j].get("ps_id",0l)%>"  <%=ps_id==ps[j].get("ps_id",0l)?"selected":"" %>><%=ps[j].getString("ps_name")%></option>
											<%
												}
											%>
										</select> 
									</div>
									</td>
									<td width="200px;" align="left">
										<input type="button" class="button_long_refresh" value="Search" onclick="filterPlateInfos(1, 1);"/>
									</td>
									<td></td>
								</tr>
							</table>
						</div>
						
					</td>
				</tr>
			</table>
	      </div>
	      <div id="checkin_search">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="140px">
	              	<div class="side-by-side clearfix">
					<select id="key_type" class="chzn-select" data-placeholder="Choose a KeyType..." tabindex="1"  style="width:120px">
	              		<option value=1>PlateNo</option>
	              		<option value=2>Entry</option>
	              	</select>
	              	</div>
	              </td>
	              <td width="700" style="padding-top:3px;" align="left">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td >
									<div  class="search_shadow_bg">
									 <input name="search_key" type="text" class="search_input" style="font-size:17px;font-family:Arial;color:#333333;height:30px;" id="search_key" onkeydown="if(event.keyCode==13)search()" value="<%=key %>"/>
<!-- 									 <input type="button" class="button_long_refresh" value="Refresh" onclick="filter('refresh');"/> -->
									</div>
								</td>
							</tr>
						</table>
						<script>eso_button_even();</script>
				   </td>
				   <td></td>
	            </tr>
	          </table>
	      </div>
       </div>
    </div>
    <br>
    <div class="demo" style="width:100%">
	  <div id="tabs_status">
		<ul>
			<li><a href="#all">All</a></li>
			<li><a href="#on_hand">OnHand</a></li>
			<li><a href="#shipped">Shipped</a></li>
		</ul>
		<div id="all" style="height:0px;"></div>
		<div id="on_hand" style="height:0px;"></div>
		<div id="shipped" style="height:0px;"></div>
	  </div>
	</div>
	<div id="datas"></div>
    <br/>
    
  	<form name="dataForm" action="">
	    <input type="hidden" name="p"/>
		<input type="hidden" name="ps_id" value="<%=ps_id%>"/>
		<input type="hidden" name="key" value="<%=key %>"/>
		<input type="hidden" name="cmd" value="<%=cmd %>"/>
		<input type="hidden" name="search_mode" value="<%=search_mode %>"/>
		<input type="hidden" name="start_time" value="<%=start_time %>"/>
		<input type="hidden" name="end_time" value="<%=end_time %>"/>
		<input type="hidden" name="type" value="<%=type %>"/>
		<input type="hidden" name="status" value="<%=status %>"/>
		<input type="hidden" name="search_key_type" value=""/>
	</form>
	<form action="" method="post" name="search_form">
		<input type="hidden" name="key" />
		<input type="hidden" name="cmd" value="search"/>
		<input type="hidden" name="search_mode"/>
	</form>
	<form action="" method="post" name="filter_form">
		<input type="hidden" name="ps_id"/>
		<input type="hidden" name="cmd" value="filter"/>
		<input type="hidden" name="start_time"/>
		<input type="hidden" name="end_time"/>
	</form>
	<!-- 右键菜单样式
	<div id="menu" style="width:auto;">
		<ul>
			<li id="showWebcam" onclick="showWebcam()" ><a href="#">View Webcam</a>
			</li>
			<li onclick ="openWebcamWind()"><a href="#">Set Webcam</a>
			</li>
		</ul>
	</div>
	 -->
</body>
</html>