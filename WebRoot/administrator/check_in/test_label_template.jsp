<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.LableTemplateTypeKey" %>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<script type="text/javascript" src="../js/assembleUtils.js"></script>
<%
	long pc_id=1000034;
	//long lable_template_type=LableTemplateTypeKey.PRODUCTLABEL;
	long lable_template_type=LableTemplateTypeKey.CONTAINERTLABEL;
	DBRow[] rows=customSeachMgrGql.getLableLableTemplateByType(pc_id, lable_template_type);
	String str=rows[0].get("lable_content", "");
%>
<title>test_label_template</title>
 <style>
	.tablecloth{
	background: #FFF;	
	padding:0px;
}
</style>
<script type="text/javascript">	
	//virtual data for test assembleUtil
	var data = {
			date: "14-08-14",
			containerTypeId: "2953",
			pid: "1000034",
			productName: "10260010031/E701I-A3",
			lot: "",
			customer: "",
			supplier: "",
			totalProducts: "8",
			totalPackages: "8",
			piece: "Product",
			Packages: "2X2X2",
			barCodePcid: "1000249",
			pcid: "1000249",
			}; 
	var datas = { 
			date: "14-08-14",
			id: "1000034",
			code: "E701I-A3",
			barCodePcid: "<img src='/barbecue/barcode?data=1000034&amp;width=1&amp;height=50&amp;type=code39'>"
			};
	$(function(){		
		//console.log(assembleTemplate(data, $('#hiddenOne').html()));
		//param data for container, datas for label
		$("#showContainer").html(assembleTemplate(data, $('#hiddenOne').html()));
	});
	
	function print(){
		 visionariPrinter.PRINT_INITA(0,0,"60mm","30mm","Product Label");
         visionariPrinter.SET_PRINTER_INDEXA ("tsc");//指定打印机打印  
		 visionariPrinter.SET_PRINT_PAGESIZE(1,"6cm","3cm","60X30");
         visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#biaoqian').html());
         visionariPrinter.SET_PRINT_COPIES(1);
		 visionariPrinter.PREVIEW();
		//visionariPrinter.PRINT();
	}	
</script>
</head>
<body>
<div style="width:210mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;">
	<input class="short-short-button-print" type="button" onclick="print()" value="Print">
</div>
<!-- <div style="float:left;position:relative;margin-left:130px;margin-top:160px;text-align:center;border: 1px solid red;" id="lableContent">
<%=rows[0].get("lable_content", "") %>

</div> -->
<!-- 填充内容 -->
<center><div id="showContainer" style="width:390px;margin-top:15px;">
</div>
</center>
<!-- 获取content -->
<div id="hiddenOne"  style="display:none;" >
	<%=rows[0].get("lable_content", "") %>
</div>
</body>
</html>

