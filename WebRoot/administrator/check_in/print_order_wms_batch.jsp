<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="wmsSearchItemStatusKey" class="com.cwc.app.key.WmsSearchItemStatusKey"></jsp:useBean>
<html>
<head>

<title>print wms</title>
<%
	long entryId = StringUtil.getLong(request, "entryId");
	//long entryId = 100732;
    DBRow mainRow=checkInMgrZwb.findGateCheckInById(entryId);
	
	String window_check_in_time = mainRow.get("window_check_in_time","");
	String DockID=StringUtil.getString(request, "door_name");
	String company_name = mainRow.getString("company_name");
	String gate_container_no = mainRow.getString("gate_container_no");
	String loadNo=StringUtil.getString(request,"load");
	String CompanyID = StringUtil.getString(request, "CompanyID");
	String CustomerID = StringUtil.getString(request, "CustomerID");
	String out_seal = StringUtil.getString(request, "out_seal");
	if(StringUtil.isBlank(out_seal))
	{
		out_seal = mainRow.getString("out_seal");
	}
	//masterCount大于0，打印Master Loading Ticket，等于0，打印Order Loading Ticket
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adid = adminLoggerBean.getAdid();
	int masterCount = sqlServerMgr.findMasterBolNoCountByLoadNo(loadNo, CompanyID, CustomerID, adid, request);
	//System.out.println("mloadNo:"+loadNo+","+entryId+","+CompanyID+","+CustomerID+","+masterCount);
%>
<script>
$(function(){
	var wmsUrl = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/';
	var para = 'loadNo=<%=loadNo%>&entryId=<%=entryId%>&window_check_in_time=<%=window_check_in_time%>&DockID=<%=DockID%>';
	para += '&company_name=<%=company_name%>&gate_container_no=<%=gate_container_no%>&seal=<%=out_seal%>&CompanyID=<%=CompanyID%>&CustomerID=<%=CustomerID%>';
	if('<%=masterCount%>'*1 != 0)
	{
		wmsUrl += 'print_order_master_wms.html';
	}
	else
	{
		wmsUrl += 'print_order_no_master_wms.html';
	}
	$.ajax({
		url: wmsUrl,
		type: 'post',
		dataType: 'html',
		data:para,
		async:false,
		success: function(html){
			$(html).appendTo($("#av"));
		}
	});	
});
	function printWms(){
		var printHtml=$('div[name="printWms"]');
		for(var i=0;i<printHtml.length;i++){
			 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Load Label");
	      	 visionariPrinter.SET_PRINTER_INDEXA ("LabelPrinter");//指定打印机打印  
	         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
	         visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			 visionariPrinter.ADD_PRINT_TABLE("0.4cm",7,"100%","100mm",$(printHtml[i]).html());
			 //visionariPrinter.SET_PRINT_STYLEA(0,"AngleOfPageInside",180);
			 visionariPrinter.SET_PRINT_COPIES(1);
			// visionariPrinter.ADD_PRINT_TEXT("0.5cm",350,"100%","100%",addStyleForText2());
			 //visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",2);
			 visionariPrinter.ADD_PRINT_HTM("14cm",160,"100%","100%",addStyleForText("CONTINUED..."));
			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
			 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","Last");
			visionariPrinter.PREVIEW();
			//visionariPrinter.PRINT(); 
		}
	}
	function addStyleForText(text)
	{
		return '<span style="font-size: 10;font-family:Verdana;">'+text+'</span>';
	}
</script>
</head>
<body>
	<div id="av"></div>
	
</body>
</html>
