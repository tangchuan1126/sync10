<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
	String systenFolder = ConfigBean.getStringValue("systenFolder");
    
	DBRow[] contextCmds = googleMapsMgrCc.getContextCommand();

	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	long adminId = adminLoggerBean.getAdid();
	long adgId = adminLoggerBean.getAdgid();
	DBRow[] allStorageKml = googleMapsMgrCc.getAllStorageKmlByAdmin(adminLoggerBean);
	//根据登录帐号判断是否为客户
	Boolean bl = adminMgrZwb.findAdminGroupByAdminId(adminId); //如果bl 是true 说明是客户登录
	//如果是用户登录 查询用户下的产品
	DBRow[] titles = null;
	long number = 0;
	if (bl) { 
		number = 1;
		titles = adminMgrZwb.getProprietaryByadminId(adminLoggerBean
		.getAdid());
	} else {
		number = 0;
		titles = mgrZwb.selectAllTitle();
	}
%>
<script type="text/javascript">
var systenFolder = "<%=systenFolder%>";
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GIS</title>
<!-- 基本css 和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />

<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript'
	src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript'
	src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>

<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- jstree -->
<script type="text/javascript" src="../js/tree/jquery.js"></script>
<script type="text/javascript" src="../js/tree/jquery.tree.js"></script>
<script type="text/javascript" src="../js/tree/jquery.tree.checkbox.js"></script>
<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>

<!-- GoogleMap -->
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&sensor=false"></script>
<script type="text/javascript" src="//www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load('visualization', '1', {packages: ['table','geochart']}); //load google chart
</script>
<script src="../js/maps/GoogleMapsV3.js"></script>
<script src="../js/maps/jsmap.js"></script>
<script src="../js/maps/storageRoad.js"></script>

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet"
	href="../js/fullcalendar/chosen.css" />
<script type="text/javascript"
	src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css"
	type="text/css" />

<script type="text/javascript"
	src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>
<script type="text/javascript"
	src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css"
	href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";

@import "../js/thickbox/thickbox.css";

#menu {
	width: 150px;
	display: none;
	position: absolute;
	background: #ddd;
	border: solid #ccc 1px;
}

#menu ul {
	list-style: none;
	margin: 0px;
	padding: 0px;
}

#menu ul li a {
	margin: 0px;
	padding: 5px;
	text-decoration: none;
	display: inline-block;
	height: 20px;
	width: 100px;
	color: #333;
	font-size: 13px;
}

#menu ul li a:hover {
	background: #eee;
	border: solid #fff 1px;
	height: 18px;
}
</style>

<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<style type="text/css">
html,body {
	width: 100%;
	height: 100% !important;
	margin: 0px;
	padding: 0px;
	overflow: hidden;
}

.gis_left {
	width: 12%;
	height: 100%;
	background-color: #EEEEEE;
	float: left;
}

.gis_main {
	width: 88%;
	height: 100%;
	background-color: #EEEEEE;
	float: left;
}

.map_tool {

	padding: 2px;
	border: none;
}

.map_tool_selected {
	/*
	float:right; 
	margin: 3px;  
	*/
	padding: 1px;
	border: solid;
	border-width: 1px;
	border-color: #EEEEEE;
	background-color: #EEEEEE;
}

.map_tool_mousedown {
	/*
	float:right; 
	margin: 3px;  
	*/
	border: solid;
	border-width: 1px;
	border-color: #EEEEEE;
	padding: 1px;
	background-color: #EEEEEE;
}

li {
	font-size: 12px;
}

.mouseover_kml {
	background-color: #FFFFFF;
}

.text-overflow {
	display: block;
	word-break: keep-all;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}

.table {
	table-layout: fixed; /* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */
}

.table td {
	word-break: keep-all;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}
.kml_layer{
	padding: 3px; 
	font-size: 12px; 
	background-color: #FFFFFF; 
	display: none; 
	position: absolute; 
	cursor: pointer;
	border-color: #DDDDDD;
	border-width: 2px;
	border-style: solid;
}
.gis_main_select{
	background:#fafdfe;
	width:100%;
	height:24px;
	line-height:28px;
	border:1px solid #9bc0dd;
	-moz-border-radius:2px;
	-webkit-border-radius:2px;
	border-radius:2px;
}
.gis_main_button{
	margin-left:15px; padding: 6px 8px; cursor: pointer; display: inline-block; 
	text-align: center; line-height: 1; *padding:4px 10px; *height:2em; letter-spacing:2px; font-family: Tahoma, Arial/9!important; 
	width:auto; overflow:visible; *width:1;   border-radius: 5px; 
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#FFFFFF', endColorstr='#DDDDDD'); 
	text-shadow: 0px 1px 1px rgba(255, 255, 255, 1);
	box-shadow: 0 1px 0 rgba(255, 255, 255, .7),  0 -1px 0 rgba(0, 0, 0, .09); -moz-transition:-moz-box-shadow linear .2s; 
	-webkit-transition: -webkit-box-shadow linear .2s; transition: box-shadow linear .2s;color: #FFF;
	border: solid 1px #1c6a9e;
	background: #2288cc; 
	}			
</style>
<script type="text/javascript" class="source">
var menus = []; //左侧菜单
var layer_,from,to;
var auth=false
if('<%=adgId%>'=='10000'){
	auth=true;
};
//定时任务的刷新时间
var time = {"truck":120000,"dock":60000};
$(function () {
	//初始化菜单权限
	initmenuAuth();
	//初始化左侧菜单div高度
	initMenuSize();
	//设置map div高度
	$("#jsmap").height($("#gis_main").height() - $("#map_tool").height());
	//初始化时间控件
	initTimeField();
	//初始化资产信息树
	initAssetTree();
	//初始化map
	jsMapInit();
	//初始化车辆定位
	getLastPosition();
	//初始化map tool
	initMapTool();
	//初始化图层div大小
	initStorageKmlLayerSie();
});
$(window).resize(function() {
	initMenuSize();
	resizeMap();
	setFloatWindowPosition();
	initStorageKmlLayerSie();
});
function initMenuSize(){
	var menuHeight = 0;
	for(var i=0; i<menus.length; i++){
		menuHeight += $("#"+menus[i]+"_top").height();
	}
	$("#gis_left").height($("body").height());
	//设置左侧菜单div高度
	for(var i=0; i<menus.length; i++){
		$("#"+menus[i]).height($("#gis_left").height() - menuHeight);
	}
}
function resizeMap(){
	var leftWidth = 0;
	leftWidth += $("#gis_left")[0].style.display!="none" ? $("#gis_left").width(): 0;
	leftWidth += $("#left_show_img")[0].style.display!="none" ? $("#left_show_img").width() : 0;
	$("#jsmap").height($("body").height()-$("#map_tool").height());
	$("#jsmap").width($("body").width()-leftWidth);
	$("#map_tool").width($("body").width()-leftWidth);
}
function initmenuAuth(){
	$("#storageKml_top").show();
	menus.push("storageKml");
	if(auth){
		 //"assetTree","geoFencing","geoLine","geoPoint","productStorage","productDemand","maintainLayer","stagingPlate"
		$("#assetTree_top").show();
		menus.push("assetTree");
		$("#geoFencing_top").show();
		menus.push("geoFencing");
		$("#geoLine_top").show();
		menus.push("geoLine");
		$("#geoPoint_top").show();
		menus.push("geoPoint");
		$("#productStorage_top").show();
		menus.push("productStorage");
		$("#productDemand_top").show();
		menus.push("productDemand");
		$("#maintainLayer_top").show();
		menus.push("maintainLayer");
	}
	if('<%=adgId%>'=='10000'||'<%=adgId%>'=='1000006'){
		$("#stagingPlate_top").show();
		menus.push("stagingPlate");
	
	}
	
}
function initMapTool(){
	//清除图层控件
	addMapControl("right_bottom","click",clearMap,"clearMap","clear1.png");
	//map tool控件
	addMapControl("top_right","mouseover",displayMapTool,"tool");
}
function initTimeField(){
	var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth()+1;
	var day = d.getDate();
	month = month > 9 ? month : "0"+month;
	day = day > 9 ? day : "0"+day;
	var date = year+"-"+month+"-"+day;
	$("#start_time").val(date);
	$("#end_time").val(date);
	$("#start_time").datetimepicker({
		dateFormat: "yy-mm-dd",
		timeFormat: "",
		showTime: false,
		showSecond: false,
		showMinute: false,
		showHour: false
	});
 	$("#end_time").datetimepicker({
 		dateFormat: "yy-mm-dd",
		timeFormat: "",
		showTime: false,
		showSecond: false,
		showMinute: false,
		showHour: false
	});
 	$("#ui-datepicker-div").css("display","none");
}
var assetIds = "";   //用于实时位置
function initAssetTree(){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/GetAllAssetAction.action',
		data:'',
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    },
		success:function(data){
			if(data && data.length>0){
				assetIds = "";
				var html = "<ul>\n"
				var lastGroup = -1;
				for(var i=0; i<data.length; i++){
					var groupId = data[i].group_id;
					var assetId = data[i].asset_id;
					var assetName = data[i].asset_name;
					var assetImei = data[i].asset_imei;
					var icon = data[i].icon_url;
					if(groupId != lastGroup){
						var groupName = data[i].group_name;
						if(lastGroup != -1){
							html += "</li>\n"
						}
						html += "<li id='group_"+groupId+"' rel='group'><a href='#'><ins>&nbsp;</ins>"+groupName+"</a>\n";
					}
					html += "<ul id='child_ul'>\n"+
							"<li id='asset_"+assetId+"' rel='asset' imei='"+assetImei+"'><a href='#'><ins>&nbsp;</ins> <img alt='pic' src='<%=systenFolder%>"+icon+"' >&nbsp;"+assetName+"</a></li>\n"+
							"</ul>\n";
					lastGroup = groupId;
					assetIds += assetId+",";
					$("#asset").data("asset_"+assetId,data[i]);
				}
				html += "</li>\n</ul>";
				assetIds = assetIds.substr(0,assetIds.length-1);
				$("#asset").html(html);
			}
			creatAssetTree();
		},
		error:function(){
		}
	});
}
function creatAssetTree(){
	$("#asset").tree({
		ui : {
			theme_name : "checkbox"
		},
		rules : {
			// only nodes of type root can be top level nodes
			valid_children : [ "group" ],
			multiple:true,	//支持多选
			drag_copy:false	//禁止拷贝
		},
		types : {
			// all node types inherit the "default" node type
			"default" : {
				deletable : false,
				renameable : false,
				draggable : false
			},
			"group" : {
				valid_children : [ "asset" ]
			}, 
			"asset" : {
				valid_children : "none",
				max_depth :0
			}
		},
		"callback" : {
			onselect : function(node,tree_obj){ //选择
				treeNodeSelect(node);
				startCarIsChecked();
            },
            onrgtclk : function(NODE,TREE_OBJ,EV){	//右键
				
            }
        },
		plugins : {
			checkbox : {}
		}
	});
	//assetTree节点默认全选
	$("[rel='group']").each(function(){jQuery.tree.plugins.checkbox.uncheck(this);});
	$("[rel='asset']").each(function(){assetNotDisplay[this.id]=true;});
	contextMenu();
}
var assetNotDisplay = {}; //不需要实时显示的车辆,true不显示,false显示
//树节点点击  
function treeNodeSelect(node){
	var nodeType = $(node).attr("rel");
	var itemId = $(node).attr("itemId");
	var t = $.tree.reference(node);
	var checked = t.get_node(node).children("a").hasClass("checked");  //点击之前的状态
	if(nodeType == "group"){
		$(node).find("[rel='asset']").each(function(){
			treeNodeSelect(this);
		});
	}
	if(nodeType == "asset"){
		if(checked==true){  //取消勾选
			if(jsmap.markerCurrent[node.id]){
				jsmap.hideMarker(jsmap.markerCurrent[node.id]);
				jsmap.labelCurrent[node.id].hide();
			}
			assetNotDisplay[node.id] = true;
		}else{  //勾选
			if(jsmap.markerCurrent[node.id]){
				jsmap.showMarker(jsmap.markerCurrent[node.id]);
			    jsmap.labelCurrent[node.id].show();
			}
			assetNotDisplay[node.id] = false;
		}
	}
}
//隐藏/显示左侧菜单
function showOrHideAssetTree(flat){
	if(flat==1){   //hide
		//$("#left_show_img").show();
		$("#gis_left").hide();
		//菜单显示控件
		addMapControl("top_left","click",showOrHideAssetTree,"showAssetTree","hide_light_right.png");
	}else{  //show
		//$("#left_show_img").hide();
		$("#gis_left").show();
		hideMapControl("showAssetTree");
	}
	initMenuSize();
	resizeMap();
}
//工具选择
function mapToolClick(el){
	var radioButton = ["move_map","measure_distance","measure_area"];
	var id = el.id;
	//设置工具
	setMapTool(id);
	//修改map_tool选中状态
	for(var i=0; i<radioButton.length; i++){
		if(radioButton[i] != id && $("radioButton[i]")){
			$("#"+radioButton[i]).attr("flag","0");
			$("#"+radioButton[i]).attr("class","map_tool");
		}
	}
}
function displayLegend(){
	showParkingDocksLegend();
}
//菜单选择
function selectMenu(menu){
	var dis = document.getElementById(menu).style.display;
	var isOpen = false;
	var index = 0;
	if(dis != "none"){
		isOpen = true;
	}
	for(var i=0; i<menus.length; i++){
		$("#"+menus[i]).hide();
		$("#"+menus[i]+"_img").attr({src:"../imgs/maps/Closed.png"});
		if(menus[i]==menu){
			index = i;
		}
	}
	if(isOpen && menus.length>1){
		index<menus.length-1 ? index++ : index--;
		//未点击过的围栏需要刷新
		var geoAlarm = "geoFencing,geoLine,geoPoint";
		if(geoAlarm.indexOf(menus[index]) > -1){
			initGeoAlarmLabel(menus[index]);
		}
	}
	$("#"+menus[index]).show();
	$("#"+menus[index]+"_img").attr({src:"../imgs/maps/Opened.png"});
	//关闭绘图
	cancelGeo();
}
//仓库列表样式
function storageKmlMouseout(){
	var selected = $("#storageKml").data("selected");
	$("#storageKml div[id!="+selected+"][kmlname]").css("backgroundColor","#EEEEEE");
}
function initStorageKmlLayerSie(){
	var layer = $("#storageKml div[eltype='layer']");
	layer.width($("#storageKml").width()-10);
}
//显示图层选项
function showStorageKmlLayer(el){
	//initStorageKmlLayerSie();
	$("#storageKml div[eltype='layer']").hide();
	var layer = $(el).find("div").last();
	//layer.width($("#storageKml").width()-10);
	layer.show();
}
function showWebcamLayer(){
	var webcamCheck = $("#kml_"+psId+"_layer input[id='webcam']").attr("checked");
}
//定位仓库
var storageObjInterval = null;
var currentPsId = null;//仓库id
var storageLostFocus = false;
//选择仓库
function selectKml(kmlName,psId,layer){
	layer_=layer;
	//clearMap();//清除需求分布查询之后图上的着色问题
	onSelectLayer();
	//提前加载大数据
	initStorageData(psId);
	//菜单样式
	$("#storageKml").data("selected",$("#kml_"+psId).attr("id"));
	$("#kml_"+psId).css("backgroundColor","#DDDDDD");
	storageKmlMouseout();
	
	var layers = "";
	var clearLayers = "";
	var fit = false;
	if(psId!=currentPsId || !layer || storageLostFocus){
		fit = true;
		layers += "base,";
		storageLostFocus = false;
	}
	currentPsId = psId;
	var url = getKmlUrl(kmlName);
	var areaCheck = $("#kml_"+psId+"_layer input[id='area']").attr("checked");
	var titleCheck = $("#kml_"+psId+"_layer input[id='title']").attr("checked");
	var zoneDockCheck = $("#kml_"+psId+"_layer input[id='zonedock']").attr("checked");
	var zonePersonCheck = $("#kml_"+psId+"_layer input[id='zonePerson']").attr("checked");
	var locationCheck = $("#kml_"+psId+"_layer input[id='location']").attr("checked");
	var stagingCheck = $("#kml_"+psId+"_layer input[id='staging']").attr("checked");
	var dockCheck = $("#kml_"+psId+"_layer input[id='docks']").attr("checked");
	var parkingCheck = $("#kml_"+psId+"_layer input[id='parking']").attr("checked");
	var webcamCheck = $("#kml_"+psId+"_layer input[id='webcam']").attr("checked");
	var printerCheck = $("#kml_"+psId+"_layer input[id='printer']").attr("checked");
	var roadCheck = $("#kml_"+psId+"_layer input[id='road']").attr("checked");
	var lightCheck = $("#kml_"+psId+"_layer input[id='light']").attr("checked");
	var div_nodes=$("#kml_"+psId).parent().siblings().find('div');
	if(!layer){
		for(var i=0;i<div_nodes.length;i++){
			if(!!(i%2)){
					$("#"+div_nodes[i].id).hide();
					var chockBoxNodes=$("#"+div_nodes[i].id).find('input');
					for(var j=0;j<chockBoxNodes.length;j++){
						if(j==0){
							$(chockBoxNodes[j]).attr('checked',true);
						}else{
							$(chockBoxNodes[j]).attr('checked',false);
						}
					}
			}	
		}
	}
	//zoneDock title和zone只能单选
	if(areaCheck && layer == "area"){
		titleCheck = false;
		$("#kml_"+psId+"_layer input[id='title']").attr("checked",false);
		zoneDockCheck = false;
		$("#kml_"+psId+"_layer input[id='zonedock']").attr("checked",false);
		zonePersonCheck=false;
		 $("#kml_"+psId+"_layer input[id='zonePerson']").attr("checked",false)
	}
	if(titleCheck && layer == "title"){
		areaCheck = false;
		$("#kml_"+psId+"_layer input[id='area']").attr("checked",false);
		zoneDockCheck = false;
		$("#kml_"+psId+"_layer input[id='zonedock']").attr("checked",false);
		zonePersonCheck=false;
		 $("#kml_"+psId+"_layer input[id='zonePerson']").attr("checked",false)
	}
	if(zoneDockCheck && layer == "zonedock"){
		areaCheck = false;
		$("#kml_"+psId+"_layer input[id='area']").attr("checked",false);
		titleCheck = false;
		$("#kml_"+psId+"_layer input[id='title']").attr("checked",false);
		zonePersonCheck=false;
		 $("#kml_"+psId+"_layer input[id='zonePerson']").attr("checked",false)
	}
	if(zonePersonCheck&&layer == "zonePerson"){
		areaCheck = false;
		$("#kml_"+psId+"_layer input[id='area']").attr("checked",false);
		titleCheck = false;
		$("#kml_"+psId+"_layer input[id='title']").attr("checked",false);
		zoneDockCheck = false;
		$("#kml_"+psId+"_layer input[id='zonedock']").attr("checked",false);
		
	}
	//zone-name
	if(areaCheck){
		layers += "area,";
	}else{
		if(!titleCheck && !zoneDockCheck&&!zonePersonCheck){
			clearLayers += "area,";
		}
	}
	//zone-title
	if(titleCheck){
		layers += "area,zone-title,";
	}
	//zone-dock
	if(zoneDockCheck){
		layers += "area,zone-dock,";
	}
	if(zonePersonCheck){
		layers += "area,";
	}
	//staging
	if(stagingCheck){
		layers += "staging,";
	}else{
		clearLayers += "staging,";
	}
	if(layers.length >0){
		loadStorageLayer(psId,layers.substring(0, layers.length-1));
	}
	if(clearLayers.length >0){
		clearStorageLayer(psId,clearLayers.substring(0, clearLayers.length-1));
	}
	//门和停车位
	var parkingDocksShow = "";
	var parkingDocksHide = [];
	if(dockCheck){
		parkingDocksShow += "docks,";
	}else{
		parkingDocksHide.push("docks");
	}
	if(parkingCheck){
		parkingDocksShow += "parking,";
	}else{
		parkingDocksHide.push("parking");
	}
	parkingDocksLayer = parkingDocksShow.substring(0, parkingDocksShow.length-1);
	
	if(storageObjInterval!=null){
		clearInterval(storageObjInterval);
	}
	
	if(dockCheck || parkingCheck){
		parkingDocksOccupancy();
		displayLegend();
		storageObjInterval = setInterval(parkingDocksOccupancy,time.dock);
	}else {
		removeFloatWindow();
	}
	if(parkingDocksHide.length>0){
		for(var i=0; i<parkingDocksHide.length; i++){
			clearStorageObj(parkingDocksHide[i]);
		}
	}
	//location
	url = getKmlUrl("loc_"+kmlName);
	if(locationCheck){
		loadKml(url,false);
	}else{
		unLoadKml(url);
	}
	//webcam
	if(webcamCheck){
		drawWebcam(psId);
	}else{
		jsmap.clearWebcam();
	}
	//printer
	if(printerCheck){
		drawPrinter(psId);
	}else{
		jsmap.clearPrinter();
	}
	//light
	if(lightCheck){
		DrawLightLayer(psId);
	}else{
		jsmap.clearLight('all');
	}
	//road
	if(roadCheck){
		loadRoadLayer(psId,{"main":true,"entery":true});
	}else{
		clearRoadLayer();
	}
	
	if(zoneDockCheck){
		jsmap.loadZoneDocks(psId);
	}else{
		jsmap.clearZoneDocks();
	}
	
	if(zonePersonCheck){
		jsmap.loadZonePerson(psId);
	}else{
		jsmap.clearZonePerson();
	}
	
}
//仓库位置点击事件
function storageKmlClick(data,e){
	var type =data.state;
	if(type==1){
		var event=null;
    	for(var v in e){
    		if(e[v] && e[v].type=="click"){
    			event = e[v];
    		}
    	}
    	if(event.ctrlKey){
			//prescribedRoute_(this,e);
		}else{
			storageMenuClick(data);
		}
	
	}else if(type==6 ){
	showWebcam(data.webcamData);	
	}
}


//仓库位置右击事件
function storageKmlRightClick(data,position,latlng){ 
	var type =data.state;
	if(!type){
		return false;
	}

	if(auth){
		//执行方法之前先隐藏右键菜单
		if(layer_=='zonedock'){
			$('#addDock').show();
			$("#addPerson").hide();
		}else if(layer_=='zonePerson'){
			$("#addPerson").show();
			$("#addDock").hide();
		}else{
			$("#addPerson").hide();
			$("#addDock").hide();
		}	
		if(type!=5){
		$("#modifyPosition").show();
		$("#modifyZone").hide();
		}else{
		$("#modifyPosition").hide();
		$("#modifyZone").show();
		}
		
		if(type==6){
			openWebcamWind(data.webcamData);
			var _type =data.state;
			var psId=data.psId;
			var id=data.webcamData.id;
			dragStorageLayerObject(_type,psId,id);
			return ;
		}
	    var position_name = (type == 1 ? data.name_full : data.name);
		if(type != 5){
			$("#modifyPosition").show();
		}
		if(type == 3){
			if(data.availableStatus == "0"){
				$("#stopUse").show();
				$("#startUse").hide();
			}else{
				$("#stopUse").hide();
				$("#startUse").show();
			}
		}else{
			$("#stopUse").hide();
			$("#startUse").hide();
		  }
	}
	//导航菜单
	//if(data.state == 1){
		var navData = $("#navFrom").data("nav");
		if(navData && navData.psId==currentPsId){
			if(navData.from){
				$("#navFrom").hide();
			}else{
				$("#navFrom").show();
			}
			if(navData.to){
				$("#navTo").hide();
			}else{
				$("#navTo").show();
			}
			if(navData.from && navData.to){
				$("#navFrom").show();
				$("#navTo").show();
			}
		}else{
			$("#navFrom").data("nav",{psId : currentPsId});
			$("#navFrom").show();
			$("#navTo").show();
		}
	//}else{
	//	$("#navFrom").hide();
	//	$("#navTo").hide();
	//}
	if(auth){
		if(type==5){
    $("#jsmap").data("curr_poly_data",data);
		}else{
    $("#jsmap").data("layers_data",data);
			
		}
	}
    $("#jsmap").data("latlng",latlng);
	var offset = $("#jsmap").offset();
	var posx =parseInt(position.x+offset.left);
	var posy =parseInt(position.y+offset.top);
    var menu=document.getElementById("menu");
	menu.style.display="block";//设置菜单可见   
    menu.style.top=posy+"px";//设置菜单位置为鼠标右击的位置   
    menu.style.left=posx+"px";
}

document.onclick=function(){//左击清除弹出菜单   
    var menu=document.getElementById("menu");   
    menu.style.display="none";   
} 
// 屏蔽鼠标右键菜单
$(document).ready(function(){
	$(document).find("#jsmap").bind("contextmenu",function(e){
		return false;
	});
	$(document).find("#menu").bind("contextmenu",function(e){
		return false;
	});
});
function openPinterWindow(data){
	var psId=data.ps_id;
	var p_id =data.p_id;
	var type =data.type;
	if(!auth){
		reSetObjectDraggable(7,psId,p_id);
		return ;
	}
	var name =data.name;
	var size =data.size;
	var p_type =data.p_type;
	var param="";
	var ip =data.ip;
	var port =data.port;
	var servers =data.servers;
	var servers_name=data.servers_name;
	if(p_type==0){
		param="&p_type="+p_type+"&ip="+ip+"&port="+port;
	}else if(p_type==1){
		param="&p_type="+p_type+"&servers="+servers;
	}
	var x=data.x;
	var y=data.y;
	var area_id=data.area_id || "";
	var area_name=data.area_name||"";
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/modify_printer.html?name='+name+'&size='+size+'&x='+x+'&y='+y+'&type='+type+'&psId='+psId+'&id='+p_id+'&pageType=1'+'&area_id='+area_id+'&area_name='+area_name+param+'&servers_name='+servers_name;
	$.artDialog.open(url, {title: "Set Printer ["+name+"]",width:'300px', lock: false,opacity: 0.3,fixed: true,id:"printer",cancel:false});
}

//修改Light
function modifyLight(data){
	var id =data.id;
	var status =data.status;
	var psId =data.ps_id;
	if(!auth){
		reSetObjectDraggable(status,psId,id);
		return ;
	}
	var name =data.name;
	var x =data.x;
	var y =data.y;
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/modify_light.html?ps_id='+psId+"&x="+x+'&y='+y+'&status='+status+'&name='+name+'&id='+id;
	$.artDialog.open(url, {title: "Modify Light ["+name.toUpperCase()+"]",width:'400px', lock: false,opacity: 0.3,id:"light",cancel:false});
}
function  openWebcamWind(data){
	var id =data.id;
	var ip=data.ip;
	var ps_id=data.ps_id;
	var port =data.port;
	var username =data.user;
	var password =data.password;
	var x=data.x;
	var y=data.y;
	var inner_radius=data.inner_radius;
	var outer_radius=data.outer_radius;
	var s_degree=data.s_degree;
	var e_degree=data.e_degree;
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/camparameter.html?ip='+ip+'&port='+port+'&username='+username+'&password='+password+'&x='+x+'&y='+y+'&inner_radius='+inner_radius+'&outer_radius='+outer_radius+'&s_degree='+s_degree+'&e_degree='+e_degree+'&ps_id='+ps_id+'&id='+id+'&pageType=1';
	$.artDialog.open(url, {title: "Set Webcam ["+port+"]",width:'475px', lock: false,opacity: 0.3,fixed: true,id:"webcam",cancel:false});
}
// 在菜单处回调查看商品信息
function storageMenuClick(data){
	
    var psId =data.psId;
    var position_all =data.name_full;
    var state =data.state;
  if(state==1){//点击的是location
	 var          title= $("#select_stock").data("pro_title")==-1?null:$("#select_stock").data("pro_title");
	 var        lot_num= $("#select_stock").data("pro_lot_num")==-1?null:$("#select_stock").data("pro_lot_num");
	 var           line= $("#select_stock").data("pro_line")==-1?0:$("#select_stock").data("pro_line"); 
	 var pro_category_1= $("#select_stock").data("pro_category_1");
	 var pro_category_2= $("#select_stock").data("pro_category_2"); 
	 var pro_category_3= $("#select_stock").data("pro_category_3"); 
	 var catalog_id=null;
		if(!"-1"==pro_category_1){
			catalog_id = pro_category_1;
		}
		if(!"-1"==pro_category_2){
			catalog_id = pro_category_2;
		}
		if(!"-1"==pro_category_3){
			catalog_id = pro_category_3;
		}
	 if(!catalog_id){
		 catalog_id="";
	 }
	 if(!title){
		 title="";
		 
	 }
	 if(!lot_num){
		 lot_num="";
	 }
	 if(!line){
		 line="";
	 }
	 
	 var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/dialog_product_info.html?ps_id='+psId+'&position_all='+position_all+'&title='+'&lot_num='+lot_num+'&line='+line+'&catalog_id='+catalog_id;
	 $.artDialog.open(url, {title: "Product Info",width:'1000px',height:'660px', lock: false,opacity: 0.3,fixed: true});
  }//else if (state==3){
	  
	//  showMessage("--查询parking docks ","error");
	  
  //}
  $("#menu").data("data","");
}

//显示监控画面
function showWebcam(data){
	var webCam =data;
	var port =data.port;
	if(webCam){
		var rtsp = getWebcamRtsp(webCam);
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/web_cam.html?rtsp='+rtsp;
		$.artDialog.open(url, {title: "Webcam Screen["+port+"]",width:'860px',height:'660px', lock: false,opacity: 0.3,fixed: true});
	}
}
//生成摄像头rtsp地址
function getWebcamRtsp(webCam){
	var rtsp = "rtsp://";
	if(webCam.user && webCam.user != "" && webCam.password && webCam.password!=""){
		rtsp += webCam.user + ":" + webCam.password + "@";
	}
	rtsp += webCam.ip + ":" + webCam.port + "/h264/ch1/main/av_stream";
	return rtsp;
}
//修改Area
function modifyZone(){
		var storagePosition = $("#jsmap").data("curr_poly_data");
		//var dialogId="area_info";
		var area = storagePosition;
		var key =storagePosition.key;
		if(storagePosition.state == 1){
			area = storagePosition.area;
		}
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/modify_area.html?ps_id='+area.psId+"&area_name="+area.name+'&key='+key;
		$.artDialog.open(url, {title: "Modify zone ["+area.name.toUpperCase()+"]",width:'400px', lock: false,opacity: 0.3,id:"area_info",cancel:false});
		$("#modifyZone").hide();
		//var layer=jsmap.storageProvenLayer[key];
		//layer.getVisible(false);
		//dragStorageLayer(layer,dialogId);
}

function modifyPosition(){
		var layerData =$("#jsmap").data("layers_data");
		var dialogId="layer_info";
		var type =layerData.state;
		var key =layerData.key;
		var title =null;
		if(type==1){
			title="Modify Location [";
	    }else if(type==2){
    		title="Modify Staging [";
		}else if(type==3){
			title="Modify Docks [";
		}else if(type==4){
			title="Modify Parking [";
		}
		title += layerData.name.toUpperCase()+"]";
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/modify_location.html?ps_id='+layerData.psId+"&positionName="+layerData.name+"&type="+type+'&key='+key;
		$.artDialog.open(url, {title: title,width:'400px', lock: false,opacity: 0.3,id:dialogId,cancel:false});
		$("#modifyLocation").hide();
		//var layer=jsmap.storageProvenLayer[key];
		//layer.getVisible(false);
		//dragStorageLayer(layer,dialogId);
	    
}
//停用门
function stopUse(){
	var data = $("#jsmap").data("layers_data");
	var posId = data.id;
	setStorageDoorStatus(posId,1);
}
//启用门
function startUse(){
	var data = $("#jsmap").data("layers_data");
	var posId = data.id;
	setStorageDoorStatus(posId,0);
}
function setStorageDoorStatus(sdId,status){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/setStorageDoorStatusAction.action',
		data:'sd_id='+sdId+"&status="+status,
		dataType:'json',
		type:'post',
		timeout: 5000,
		beforeSend:function(request){
	    },
		success:function(data){
			if(data && data.flag == "true"){
				//刷新图层
				parkingDocksOccupancy();
			}
		},
		error:function(){
		}
	});
}
/**
 *parking dock占用情况
 */
function parkingDocksOccupancy(){
	getParkingDocksOccupancyAjax(currentPsId);
}
function getParkingDocksOccupancyAjax(psId){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/getParkingDocksOccupancy.action',
		data:'psId='+psId,
		dataType:'json',
		type:'post',
		timeout: 5000,
		beforeSend:function(request){
	    },
		success:function(data){
			if(data && data.flag == "true"){
				showParkingDocksOccupancy(currentPsId,data.objs);
			}
		},
		error:function(){
		}
	});
}
//查询围栏 路线  关键点
var pageRaedy = true;  //页码初始化完成，解决初始化页码和初始化页面数据异步执行导致的页码错误
var initPage = false;
var initGeoFen = false;
var initGeoLine = false;
var initGeoPoint = false;

var geoPageSize = 25;
var linePageNo = 1;
var linePageCount = 0;
var lineDataCount = 0;

var fenPageNo = 1;
var fenPageCount = 0;
var fenDataCount = 0;

var pointPageNo = 1;
var pointPageCount = 0;
var pointDataCount = 0;

var currentGeoType = null;
function initGeoAlarmLabel(type){
	if(type){
		currentGeoType = type;
	}else{
		type = currentGeoType;
	}
	if(!initPage){
		pageRaedy = false;
		getGeoAlarmLabel("count",0,0);
		initPage = true;
	}
	//页码初始化未完成时等待
	if(!pageRaedy){
		setTimeout(initGeoAlarmLabel,100);
		return;
	}
	if(type=="geoFencing" && !initGeoFen){
		getGeoAlarmLabel("geoFencing",fenPageNo,geoPageSize);
		initGeoFen = true;
	}
	if(type=="geoLine" && !initGeoLine){
		getGeoAlarmLabel("geoLine",linePageNo,geoPageSize);
		initGeoLine = true;
	}
	if(type=="geoPoint" && !initGeoPoint){
		getGeoAlarmLabel("geoPoint",pointPageNo,geoPageSize);
		initGeoPoint = true;
	}
}
function showGeoAlarmLabel(type,pageTo){
	if(type=="geoFencing"){
		if(pageTo=="next"){
			fenPageNo<fenPageCount ? fenPageNo++ : "" ;
		}else if(pageTo=="prev"){
			fenPageNo>1 ? fenPageNo-- : "" ;
		}else if(pageTo=="first"){
			fenPageNo=1;
		}else if(pageTo=="last"){
			fenPageNo=fenPageCount;
		}else{
			fenPageNo=1;
		}
		getGeoAlarmLabel("geoFencing",fenPageNo,geoPageSize);
	}
	if(type=="geoLine"){
		if(pageTo=="next"){
			linePageNo<linePageCount ? linePageNo++ : "";
		}else if(pageTo=="prev"){
			linePageNo>1 ? linePageNo-- : "";
		}else if(pageTo=="first"){
			linePageNo=1;
		}else if(pageTo=="last"){
			linePageNo=linePageCount;
		}else{
			linePageNo=1;
		}
		getGeoAlarmLabel("geoLine",linePageNo,geoPageSize);
	}
	if(type=="geoPoint"){
		if(pageTo=="next"){
			pointPageNo<pointPageCount ? pointPageNo++ : "";
		}else if(pageTo=="prev"){
			pointPageNo>1 ? pointPageNo-- : "";
		}else if(pageTo=="first"){
			pointPageNo=1;
		}else if(pageTo=="last"){
			pointPageNo=pointPageCount;
		}else{
			pointPageNo=1;
		}
		getGeoAlarmLabel("geoPoint",pointPageNo,geoPageSize);
	}
}
function getGeoAlarmLabel(type,pageNo,pageSize,key){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/GetGeoAlarmLabelAction.action',
		data:'type='+type+"&pageNo="+pageNo+"&pageSize="+pageSize+(key ? "&key="+key : ""),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    },
		success:function(data){
			if(data && data.flag == "true"){
				creatGeoAlarmTable(data);
			}
		},
		error:function(){
		}
	});
}
function creatGeoAlarmTable(data){
	var type = data.type;
	var geo = data.geo;
	if(type=="count"){
		pointDataCount = 0;
		lineDataCount = 0;
		fenDataCount = 0;
		for(var i=0; i<geo.length; i++){
			var geotype = geo[i].geotype;
			if(geo[i].geotype == 1){
				pointDataCount += parseInt(geo[i].num);
			}else if(geo[i].geotype == 2){
				lineDataCount += parseInt(geo[i].num);
			}else if(geotype==3 || geotype==4 || geotype==5){
				fenDataCount += parseInt(geo[i].num);
			}
		}
		fenPageCount = Math.ceil(fenDataCount/geoPageSize);
		linePageCount = Math.ceil(lineDataCount/geoPageSize);
		pointPageCount = Math.ceil(lineDataCount/geoPageSize);
		if(fenPageNo > fenPageCount){
			fenPageNo = fenPageCount;
		}
		if(linePageNo > linePageCount){
			linePageNo = linePageCount;
		}
		if(pointPageNo > pointPageCount){
			pointPageNo = pointPageCount;
		}
		pageRaedy = true;
	}else{
		var html = "";
		for(var i=0; i<geo.length; i++){
			html += "<div class='text-overflow' title='["+ geo[i].type +"] "+ geo[i].def +"' id='geo_"+geo[i].id+"' style=\"padding: 3px; width: 100%; font-size: 12px; cursor: pointer; \" onmouseover=\"this.style.backgroundColor='#FFFFFF'\" onmouseout=\"this.style.backgroundColor='#EEEEEE'\">"
					+ "<input type='checkbox' onclick=\"selectGeoAlarmlabel(this.checked,'"+ type +"_list',"+geo[i].id+")\" style='vertical-align : middle'>&nbsp;"
					+ geo[i].name 
					+ "</div>"
					+ "<hr style='size: 1px; width: 100%; color: #FFFFFF' >"
			$("#"+type+"_list").data("geo_"+geo[i].id,geo[i]);
		}
		$("#"+type+"_list").html(html);
	}
	refreshPageCtrl();
}
function refreshPageCtrl(){
	if(fenPageNo == fenPageCount){
		$("#geoFencing_next").attr("src","../imgs/maps/page_next_dis.gif");
		$("#geoFencing_last").attr("src","../imgs/maps/page_last_dis.gif");
	}else{
		$("#geoFencing_next").attr("src","../imgs/maps/page_next.gif");
		$("#geoFencing_last").attr("src","../imgs/maps/page_last.gif");
	}
	if(fenPageNo == 1){
		$("#geoFencing_first").attr("src","../imgs/maps/page_first_dis.gif");
		$("#geoFencing_prev").attr("src","../imgs/maps/page_prev_dis.gif");
	}else{
		$("#geoFencing_first").attr("src","../imgs/maps/page_first.gif");
		$("#geoFencing_prev").attr("src","../imgs/maps/page_prev.gif");
	}
	
	if(linePageNo == linePageCount){
		$("#geoLine_next").attr("src","../imgs/maps/page_next_dis.gif");
		$("#geoLine_last").attr("src","../imgs/maps/page_last_dis.gif");
	}else{
		$("#geoLine_next").attr("src","../imgs/maps/page_next.gif");
		$("#geoLine_last").attr("src","../imgs/maps/page_last.gif");
	}
	if(linePageNo == 1){
		$("#geoLine_first").attr("src","../imgs/maps/page_first_dis.gif");
		$("#geoLine_prev").attr("src","../imgs/maps/page_prev_dis.gif");
	}else{
		$("#geoLine_first").attr("src","../imgs/maps/page_first.gif");
		$("#geoLine_prev").attr("src","../imgs/maps/page_prev.gif");
	}
	
	if(linePageNo == linePageCount){
		$("#geoPoint_next").attr("src","../imgs/maps/page_next_dis.gif");
		$("#geoPoint_last").attr("src","../imgs/maps/page_last_dis.gif");
	}else{
		$("#geoPoint_next").attr("src","../imgs/maps/page_next.gif");
		$("#geoPoint_last").attr("src","../imgs/maps/page_last.gif");
	}
	if(linePageNo == 1){
		$("#geoPoint_first").attr("src","../imgs/maps/page_first_dis.gif");
		$("#geoPoint_prev").attr("src","../imgs/maps/page_prev_dis.gif");
	}else{
		$("#geoPoint_first").attr("src","../imgs/maps/page_first.gif");
		$("#geoPoint_prev").attr("src","../imgs/maps/page_prev.gif");
	}
	$("#geoFencing_page").html(fenPageNo+"/"+fenPageCount);
	$("#geoFencing_page").prev().hide();
	$("#geoLine_page").html(linePageNo+"/"+linePageCount);
	$("#geoLine_page").prev().hide();
	$("#geoPoint_page").html(linePageNo+"/"+linePageCount);
	$("#geoPoint_page").prev().hide();
}
function selectPage(type,source,event){
	var span = $("#"+type+"_page");
	var pageNo = (type=="geoLine"?linePageNo:(type=="geoPoint"?pointPageNo:fenPageNo));
	var pageCount = (type=="geoLine"?linePageCount:(type=="geoPoint"?pointPageCount:fenPageCount));
	if(source=="click"){
		span.html("/"+pageCount);
		span.prev().show();
		span.prev().focus();
	}else{
		if(source=="keyup" && event.keyCode!=13){
			return;
		}
		var val = parseInt(span.prev().val());
		if(isNaN(val) || val<1 || val>pageCount || val==pageNo){
			span.prev().hide();
			span.html(pageNo+"/"+pageCount);
			return;
		}
		type=="geoLine" ? (linePageNo=val) : (type=="geoPoint" ? (pointPageNo=val) : (fenPageNo=val));
		getGeoAlarmLabel(type,val,geoPageSize);
	}
	
}
//显示/隐藏围栏、线路、关键点
function selectGeoAlarmlabel(checked,dataEle,id){
	var geo = $("#"+dataEle).data("geo_"+id);
	displayGeoFencing(geo,checked);
}
//搜索围栏、线路、关键点
function searchAlarmLabel(type){
	
}
function addGeofencing(type){
	var mapType = type;
	var pointEl = "";
	$("#fencing_points").val("");
	$("#line_points").val("");
	$("#point_points").val("");
	if(type == "geoFencing" || type == "polygon" || type == "circle" || type == "rect"){
		$("#fencing_name").val("");
		$("#fencing_def").val("");
		//$("#fencing_type").val("");
		mapType = $("#fencing_type").val();
		$("#addGeoFencing").show(500);
		pointEl = "#fencing_points";
	}else if(type == "geoLine"){
		$("#line_name").val("");
		$("#line_def").val("");
		mapType = $("#line_type").val();
		$("#addGeoLine").show(500);
		pointEl = "#line_points";
	}else if(type == "geoPoint"){
		$("#point_name").val("");
		$("#point_def").val("");
		mapType = $("#point_type").val();
		$("#addGeoPoint").show(500);
		pointEl = "#point_points";
	}
	//绘制并回填结果
	drawOnMap(mapType,function(points){
		$(pointEl).val(points);
	});
}
//删除围栏
function removeGeoFencing(type){
	var divs = $("#"+type+"_list div");
	if(divs && divs.length>0){
		var ids = "";
		for(var i=0; i<divs.length; i++){
			var checked = $(divs[i]).find("input").attr("checked");
			if(checked){
				var geo = $("#"+type+"_list").data($(divs[i]).attr("id"));
				ids += geo.id+",";
			}
		}
		if(ids == ""){
			alert("Please choose item first!");
			return;
		}
		ids = ids.substr(0,ids.length-1);
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/RemoveAlarmLabelAction.action',
			data:"ids="+ids+"&type="+type,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    	
		    },
			success:function(data){
				if(data && data.flag == "true"){
					clearGeoObj(); //清除图层
					
					initPage = false;
					initGeoFen = false;
					initGeoLine = false;
					initGeoPoint = false;
					initGeoAlarmLabel(data.geotype);
				}else{
					showMessage("Delete failed","alert");
				}
			},
			error:function(){
				showMessage("System error","error");
			}
		});
	}
}
//取消画图
function cancelGeo(){
	setMapTool("move_map");
	$("#addGeoFencing").hide(500);
	$("#addGeoLine").hide(500);
	$("#addGeoPoint").hide(500);
}
//保存围栏
function saveAlarmLabel(type){
	var form = (type=="line" ? $("#addGeoLineForm") : (type=="point" ? $("#addGeoPointForm") : $("#addGeoFencingForm")));
	var name = form.find("[name='name']").val();
	var points = form.find("[name='points']").val();
	if(!name){
		alert("Name can't be empty...");
		return ;
	}
	if(!points){
		alert("Please create Map first...");
		return ;
	}
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/SaveGeoAlarmLabelAction.action',
		data:form.serialize()+"&points="+points,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    	
	    },
		success:function(data){
			if(data && data.flag == "true"){
				cancelGeo();
				initPage = false;
				initGeoFen = false;
				initGeoLine = false;
				initGeoPoint = false;
				initGeoAlarmLabel(data.geotype);
				showMessage("Save successfully","succeed");
			}else{
				showMessage("Save failed","alert");
			}
		},
		error:function(){
			showMessage("System error","error");
		}
	});
}
//刷新围栏 
function refreshGeoFencing(type){
	linePageNo = 1;
	fenPageNo = 1;
	pointPageNo = 1;
	initPage = false;
	initGeoFen = false;
	initGeoLine = false;
	initGeoPoint = false;
	initGeoAlarmLabel(type);
}

//库存查询
var pro_url = {
		//产品线
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadProductLineAction.action',
		//一级分类
		url1:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadFirstProductCatalogAction.action',
		//二级分类
		url2:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadSecondProductCatalogAction.action',	
		//三级分类
		url3:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadThirdProductCatalogAction.action'
}
//选择title
function selectTitle(titleId,parent){
	if(titleId == "-1"){
		$("#"+parent+" #pro_lot_num_tr").hide();
		$("#"+parent+" #pro_lot_num").html("");
		$("#"+parent+" #pro_line_tr").hide();
		$("#"+parent+" #pro_line").html("");
		selectCategory(1,"-1",parent);
	}else{
		ajaxLotNumber(titleId);
		ajaxProductData(titleId,0,"#"+parent+" #pro_line",parent,pro_url.url);
	}
}
//加载批次
function ajaxLotNumber(titleId){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLotNumberByTitleAction.action',
		type:'post',
		dataType:'json',
		data:"titleId="+titleId,
		success:function(data){
			if(data){
				$("#pro_lot_num_tr").show();
				var html = "<option value='-1'>ALL</option>\n";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						html += "<option value="+data[i].id+">"+data[i].name+"</option>\n";
					}
				}
				$("#pro_lot_num").html(html);
			}
	    }
	});
}
//加载产品线或分类     id:TITLE或产品线或上级分类id   titleId：TITLE ID   eleId:HTML标签id
function ajaxProductData(id,titleId,eleId,parent,url){
	var _parent = parent;
	$.ajax({
		url:url,
		type:'post',
		dataType:'json',
		data:'id='+id+'&atomicBomb=<%=number%>&title_id='+titleId,
		success:function(data){
			if(data){
				$(eleId+"_tr").show();
				var html = "<option value='-1' selected='selected'>ALL</option>\n";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						html += "<option value="+data[i].id+">"+data[i].name+"</option>\n";
					}
				}else{
					selectCategory(1,"-1",_parent);
				}
				$(eleId).html(html);
			}
	    }
	});
}
//选择分类
function selectCategory(level,id,parent){
	if(id=="-1"){
		for(var i=level; i<=3; i++){
			$("#"+parent+" #pro_category_"+i+"_tr").hide();
			$("#"+parent+" #pro_category_"+i).html("");
		}
	}else{
		var url = pro_url["url"+level];
		var titleId = $("#pro_title").val();
		var eleId = "#"+parent+" #pro_category_"+level;
		ajaxProductData(id,titleId,eleId,parent,url);
	}
}
//库存位置查询
function searchStorageCatalog(){
	var psId = $("#pro_psid").val();
	var kmlname=$("#kml_"+psId).attr("kmlname");
	selectKml(kmlname,psId,"storageCatalog");
	$("#select_stock").data("pro_lot_num",$("#pro_lot_num").val());//保存批次	
	$("#select_stock").data("pro_title",$("#pro_title").val());//保存title
	$("#select_stock").data("pro_psid",$("#pro_psid").val());//保存仓库编号
	$("#select_stock").data("pro_line",$("#pro_line").val());//保存产品线
	$("#select_stock").data("pro_category_1",$("#pro_category_1").val());//商品类型1
	$("#select_stock").data("pro_category_2",$("#pro_category_2").val());//商品类型2
	$("#select_stock").data("pro_category_3",$("#pro_category_3").val());//商品类型3
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/QueryStorageCatalogAction.action',
		type:'post',
		dataType:'json',
		data:$("#productStorageFrom").serialize(),
		success:function(data){
			if(data){
				/* var datas= [
							{'position':'VALLEY1-B4B071','area_name':'1-B4','physical_quantity_count':'338','theoretical_quantity_count':'192'},
							{'position':'VALLEY1-B4B075','area_name':'1-B4','physical_quantity_count':'13','theoretical_quantity_count':'33'},
							{'position':'VALLEY1-C4C087','area_name':'1-C4','physical_quantity_count':'76','theoretical_quantity_count':'23'},
							{'position':'VALLEY1-C4C101','area_name':'1-C4','physical_quantity_count':'483','theoretical_quantity_count':'45'},
							{'position':'VALLEY1-C4C109','area_name':'1-C4','physical_quantity_count':'94','theoretical_quantity_count':'44'},
							{'position':'VALLEY1-C3C067','area_name':'1-C3','physical_quantity_count':'61','theoretical_quantity_count':'200'},
							{'position':'VALLEY1-C3C078','area_name':'1-C3','physical_quantity_count':'42','theoretical_quantity_count':'109'},
							{'position':'VALLEY1-C3C027','area_name':'1-C2','physical_quantity_count':'44','theoretical_quantity_count':'121'},
							{'position':'VALLEY1-C2C039','area_name':'1-C2','physical_quantity_count':'23','theoretical_quantity_count':'32'},
							{'position':'VALLEY1-C2C055','area_name':'1-C2','physical_quantity_count':'56','theoretical_quantity_count':'392'},
							{'position':'VALLEY1-C2C046','area_name':'1-C2','physical_quantity_count':'31','theoretical_quantity_count':'355'},
							{'position':'VALLEY1-B2B028','area_name':'1-B2','physical_quantity_count':'44','theoretical_quantity_count':'121'},
							{'position':'VALLEY1-B2B039','area_name':'1-B2','physical_quantity_count':'23','theoretical_quantity_count':'32'},
							{'position':'VALLEY1-B2B046','area_name':'1-B2','physical_quantity_count':'31','theoretical_quantity_count':'355'},
							{'position':'VALLEY1-B1B008','area_name':'1-B1','physical_quantity_count':'44','theoretical_quantity_count':'121'},
							{'position':'VALLEY1-B1B012','area_name':'1-B1','physical_quantity_count':'23','theoretical_quantity_count':'32'},
							{'position':'VALLEY1-B1B013','area_name':'1-B1','physical_quantity_count':'31','theoretical_quantity_count':'355'},
							];
				showStorageCatalogLocation(psId, datas); */
					showStorageCatalogLocation(psId, data);
			}
	    }
	});
}
//查询需求分布，国家
function searchProductDemandByCountry(){
	var condition = {
		title_id : $("#productDemand #pro_title").val(),
		product_line : $("#productDemand #pro_line").val(),
		pc_name : $("#productDemand #pro_name").val(),
		start_time : $("#productDemand #start_time").val().trim(),
		end_time : $("#productDemand #end_time").val().trim(),
		product_catalog : ""
	}
	if(condition.title_id == "-1"){
		condition.title_id = "";
	}
	for(var i=1; i<=3; i++){
		var category = $("#productDemand #pro_category_"+i).val();
		if(category && category != "-1"){
			condition.product_catalog = category;
		}
	}
	$("#productDemand").data("condition",condition);
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/GetB2BOrderOrProductCountGroupCountryAction.action',
		type:'post',
		dataType:'json',
		data:condition,
		success:function(data){
			if(data && data.length>0){
				var data_temp = [];
				for(var i=0; i<data.length; i++){
					var d = data[i];
					data_temp.push({
						regionId : d.deliver_ccid,
						count : d.product_count
					});
				}
				showBoundaryInMap(data_temp,searchProductDemandByProvince);
				storageLostFocus = true;
			}else{
				showMessage("No Data...","alert");
			}
	    },
	    error:function(){
	    	showMessage("System error...","error");
	    }
	});
}
//查询需求分布，省份
function searchProductDemandByProvince(country_id){
	var condition = $("#productDemand").data("condition");
	condition.ccid = country_id;
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/GetB2BOrderOrProductCountGroupProvinceAction.action',
		type:'post',
		dataType:'json',
		data:condition,
		success:function(data){
			if(data && data.length>0){
				var data_temp = [];
				for(var i=0; i<data.length; i++){
					var d = data[i];
					data_temp.push({
						regionId : d.deliver_pro_id,
						count : d.product_count
					});
				}
				showBoundaryInMap(data_temp);
			}else{
				showMessage("No Data...","alert");
			}
	    },
	    error:function(){
	    	showMessage("System error...","error");
	    }
	});
}
//线路规划
function routePlan(type){
	var poly = $("#jsmap").data("curr_poly_data");
	var latlng = $("#jsmap").data("latlng");
	addRouteMarker(latlng,type);
	var nav = $("#navFrom").data("nav");
	nav[type] = {"type":poly.state, "name":poly.name, "latlng":latlng};
	if(nav.from && nav.to){
		getRoutePathAjax();
	}
}
//获取导航线路
function getRoutePathAjax(){
	var nav = $("#navFrom").data("nav");
	var _data = "ps_id="+nav.psId;
	var fromLatlng = nav.from.latlng.lng()+" "+nav.from.latlng.lat();
	var toLatlng = nav.to.latlng.lng()+" "+nav.to.latlng.lat();
	_data += "&from_type="+nav.from.type+"&from_position="+nav.from.name+"&from_latlng="+fromLatlng;
	_data += "&to_type="+nav.to.type+"&to_position="+nav.to.name+"&to_latlng="+toLatlng;
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/maps/GetRoutePathAction.action',
		type:'post',
		dataType:'json',
		data : _data,
		success:function(data){
			//$("#navFrom").data("nav",null);
			if(data && data.length>0){
				showRoutePath(data);
			}else{
				showMessage("Roadless...","alert");
			}
	    },
	    error:function(){
	    	$("#navFrom").data("nav",null);
	    	clearRoutePath();
	    	showMessage("Routing fail.","error");
	    }
	});
}


function coordinateSystem(){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/maps/GetStorageCoordinateSysAction.action',
		type:'post',
		dataType:'json',
		data : {"ps_id":currentPsId},
		success:function(data){
			if(data && data.length>0){
				showCoordinateSystem(currentPsId,data);
			}else{
				showMessage("No coordinate system data.","alert");
			}
	    },
	    error:function(){
	    	showMessage("Get coordinate system fail.","error");
	    }
	});
}
/**
 * 改变图层勾选状态
 * layers: 图层id数组
 * select: 勾选(true)或取消勾选(false)
 * triger: 触发更改(true)或仅更改状态(false)
 * invertOther: 反向处理其它图层(true)或仅处理layers指定的图层(false)
 */
function layerSelectStatus(layers,select,triger,invertOther){
	var ls = $("#kml_"+currentPsId+"_layer input[type='checkbox']");
	var radio = ["area","title","zonedock","zonePerson"];  //单选处理
	var radio_id = null;
	for(var i=0; i<ls.length; i++){
		var l = $(ls[i]);
		var id = l.attr("id");
		if($.inArray(id,radio)>-1){
			if($.inArray(id,layers)>-1){
				//l.attr("checked",select);
				radio_id = id;
			}
		}else{
			if($.inArray(id,layers)>-1){
				l.attr("checked",select);
			}else{
				if(invertOther){
					l.attr("checked",!select);
				}
			}
		}
	}
	//单选处理
	if((select && invertOther) || radio_id){
		for(var i=0; i<radio.length; i++){
			$("#kml_"+currentPsId+"_layer input[type='checkbox'][id='"+radio[i]+"']").attr("checked",false);
		}
		if(radio_id){
			$("#kml_"+currentPsId+"_layer input[type='checkbox'][id='"+radio_id+"']").attr("checked",select);
		}
	}
	if(triger){
		var kml = $("#kml_"+currentPsId).attr("kmlname");
		selectKml(kml,currentPsId,"select_status");
	}
}
 /**
 *点击straging时显示straging上的货物信息
 */
 function showGoodsInfo(){
	 
 }
</script>
</head>
<body>
	<div id="page_main"
		style="height: 100%;width:100%;background-color: #EEEEEE;">
		<div style="float: left; display: none;" id="left_show_img">
			<img style="margin: 4px;" src="../imgs/maps/hide_light_right.png"
				alt="show" onclick="showOrHideAssetTree(2)"
				onmouseover="this.src='../imgs/maps/hide_deep_right.png'"
				onmouseout="this.src='../imgs/maps/hide_light_right.png'">
		</div>
		<div id='gis_left' class='gis_left'>
			<!-- 仓库信息 -->
			<div id="storageKml_top" onclick="selectMenu('storageKml')"
				style="background-color: #CCCCCC; width: 100%; float: left;display: none;">
				<div style="padding: 2px; float: left;">
					<table style="margin: 0px; border: none; cursor: default;">
						<tr>
							<td><img id="storageKml_img" src="../imgs/maps/Opened.png">
							</td>
							<td><h3>Storage</h3></td>
						</tr>
					</table>
				</div>
				<div style="padding: 4px; float: right;" id="left_hide_img">
					<img src="../imgs/maps/hide_light_left.png" alt="hide"
						onclick="showOrHideAssetTree(1)"
						onmouseover="this.src='../imgs/maps/hide_deep_left.png'"
						onmouseout="this.src='../imgs/maps/hide_light_left.png'">
				</div>
			</div>
			<div id="storageKml" style=" width:100%; overflow: auto;">
				<%
					if (allStorageKml != null) {
						for (int i = 0; i < allStorageKml.length; i++) {
							DBRow kml = allStorageKml[i];
				%>
				<div>
					<span style="float: right; cursor: pointer;" onclick="showStorageKmlLayer(this.parentNode)"><img alt="◆" src="../imgs/maps/layer.png"></span>
					<div id='kml_<%=kml.getString("ps_id")%>'
						kmlname='<%=kml.getString("kml")%>'
						style="padding: 3px; font-size: 12px; cursor: pointer;"
						onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>)"
						onmouseover="this.style.backgroundColor='#DDDDDD';"
						onmouseout="storageKmlMouseout()">
						<span><%=kml.getString("title")%></span>
					</div>
					<div id='kml_<%=kml.getString("ps_id")%>_layer' class='kml_layer' eltype="layer">
						<span style="float: right;" onclick="$(this.parentNode).hide();">
							<img alt="-" src="../imgs/maps/delete_grey.png" onmouseover="this.src='../imgs/maps/delete_blue.png'" onmouseout="this.src='../imgs/maps/delete_grey.png'">
						</span>
						<table style="width: 95%;">
							<tr>
								<td style="width: 50%;"><input type="checkbox" id="area"
									checked="checked" style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'area')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Zone-Name</span>
								</td>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox" id="title"
									style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'title')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Zone-Title</span>
								</td>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox"
									id="zonedock" style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'zonedock')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Zone-Resource</span>
								</td>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox"
									id="zonePerson" style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'zonePerson')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Zone-Person</span>
								</td>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox"
									id="location" style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'location')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Location</span>
								</td>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox" id="staging"
									style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'staging')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Staging</span>
								</td>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox" id="docks"
									style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'docks')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Dock</span>
								</td>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox" id="parking"
									style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'parking')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Parking</span>
								</td>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox" id="webcam"
									style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'webcam')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Webcam</span>
								</td>
							</tr>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox" id="printer"
									style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'printer')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Printer</span>
								</td>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox" id="road"
									style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'road')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Road</span>
								</td>
							</tr>
							<tr>
								<td style="width: 50%;"><input type="checkbox" id="light"
									style='vertical-align : middle'
									onclick="selectKml('<%=kml.getString("kml")%>',<%=kml.getString("ps_id")%>,'light')">&nbsp;&nbsp;
									<span style='vertical-align : middle; color: #333333'>Light</span>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<hr style="size: 1px; width: 100%; color: #FFFFFF">
				<%
					}
					}
				%>
			</div>
			<!-- 车辆信息 -->
			<div id="assetTree_top" onclick="selectMenu('assetTree')"
				style="background-color: #CCCCCC; width: 100%; float: left; display: none;">
				<hr style="size: 1px; width: 100%; color: #EEEEEE";>
				<div style="padding: 2px; float: left;">
					<table style="margin: 0px; border: none; cursor: default;">
						<tr>
							<td><img id="assetTree_img" src="../imgs/maps/Closed.png">
							</td>
							<td><h3>Truck</h3></td>
						</tr>
					</table>
				</div>
			</div>
			<div id='assetTree'
				style="display: none; width:100%; overflow: auto;">
				<table style="width:100%;" class="table">
					<div style="float: left; width: 100%;">
						<div class="map_tool" style="float: left; width: 25px;"
							onclick="addAsset()"
							onmousemove="this.className='map_tool_selected'"
							onmouseout="this.className='map_tool'"
							onmousedown="this.className='map_tool_mousedown'"
							onmouseup="this.className='map_tool_selected'">
							<span style="cursor: default; text-align: center;font-size:12px;">Add</span>
						</div>
						<div
							style="float: left; margin: 3px; color: grey; width: 2px;font-size:12px;">|</div>
						<div class="map_tool" style="float: left; width: 25px;"
							onclick="initAssetTree()"
							onmousemove="this.className='map_tool_selected'"
							onmouseout="this.className='map_tool'"
							onmousedown="this.className='map_tool_mousedown'"
							onmouseup="this.className='map_tool_selected'">
							<span style="cursor: default; text-align: center;font-size:12px;">Ref</span>
						</div>
					</div>
				</table>
				<div id='asset' style="width:100%; overflow: auto;"></div>
			</div>
			<!-- 围栏管理 -->
			<div id="geoFencing_top"
				onclick="selectMenu('geoFencing');initGeoAlarmLabel('geoFencing');"
				style="background-color: #CCCCCC; width: 100%; float: left;display: none">
				<hr style="size: 1px; width: 100%; color: #EEEEEE">
				<div style="padding: 2px; float: left;">
					<table style="margin: 0px; border: none; cursor: default;">
						<tr>
							<td><img id="geoFencing_img" src="../imgs/maps/Closed.png">
							</td>
							<td><h3>GeoFencing</h3></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="geoFencing"
				style="display: none; width:100%; overflow: auto;">
				<table style="width:100%;" class="table">
					<tr>
						<td>
							<!-- 
						<input type="text" id="geoFencing_search">&nbsp;&nbsp;
						<img src="../imgs/maps/search.png" alt="搜索" align="top" onclick="searchAlarmLabel('geoFencing')">
						<br/>
					 -->
							<div style="float: left; width: 100%;">
								<div class="map_tool" style="float: left; width: 25px;"
									onclick="addGeofencing('geoFencing')"
									onmousemove="this.className='map_tool_selected'"
									onmouseout="this.className='map_tool'"
									onmousedown="this.className='map_tool_mousedown'"
									onmouseup="this.className='map_tool_selected'">
									<span style="cursor: default; text-align: center;">Add</span>
								</div>
								<div style="float: left; margin: 3px; color: grey; width: 2px;">|</div>
								<div class="map_tool" style="float: left; width: 25px;"
									onclick="refreshGeoFencing('geoFencing')"
									onmousemove="this.className='map_tool_selected'"
									onmouseout="this.className='map_tool'"
									onmousedown="this.className='map_tool_mousedown'"
									onmouseup="this.className='map_tool_selected'">
									<span style="cursor: default; text-align: center;">Ref</span>
								</div>
								<div style="float: left; margin: 3px; color: grey; width: 2px;">|</div>
								<div class="map_tool" style="float: left; width: 25px;"
									onclick="removeGeoFencing('geoFencing')"
									onmousemove="this.className='map_tool_selected'"
									onmouseout="this.className='map_tool'"
									onmousedown="this.className='map_tool_mousedown'"
									onmouseup="this.className='map_tool_selected'">
									<span style="cursor: default; text-align: center;">Del</span>
								</div>
							</div>
							<div id="addGeoFencing"
								style="display: none; float: left; width: 100%; border: solid; border-color: #888888; border-width: 1px;background-color: #FFFFFF;">
								<form id="addGeoFencingForm">
									<table style="width: 100%">
										<tr>
											<td width="20%">Name</td>
											<td width="80%"><input type="text" id="fencing_name"
												name="name" style="width: 100%;" /></td>
										</tr>
										<tr>
											<td>Explain</td>
											<td><input type="text" id="fencing_def" name="def"
												style="width: 100%;" /></td>
										</tr>
										<tr>
											<td>Type</td>
											<td><select id="fencing_type" name="type"
												style="width: 100%;" onchange="addGeofencing(this.value)">
													<option value="polygon" selected="selected">Polygon</option>
													<option value="circle">Circle</option>
													<option value="rect">Rectangle</option>
											</select>
											</td>
										</tr>
										<tr>
											<td>Geometry</td>
											<td><input type="text" id="fencing_points" name="points"
												style="width: 100%;" disabled="disabled" /></td>
										</tr>
										<tr>
											<td colspan="2" align="right"><input type="button"
												value="Cancel" onclick="cancelGeo()"
												onmouseover="this.style.backgroundColor='#FFFFFF'"
												onmouseout="this.style.backgroundColor='#DDDDDD'" /> <input
												type="button" value="Save"
												onclick="saveAlarmLabel('fencing')"
												onmouseover="this.style.backgroundColor='#FFFFFF'"
												onmouseout="this.style.backgroundColor='#DDDDDD'" />
											</td>
										</tr>
									</table>
								</form>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div id="geoFencing_list" style=" width:100%;"></div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle"><img id="geoFencing_first"
							alt="first" src="../imgs/maps/page_first.gif"
							onclick="showGeoAlarmLabel('geoFencing','first')"
							style="vertical-align: bottom" /> <img id="geoFencing_prev"
							alt="prev" src="../imgs/maps/page_prev.gif"
							onclick="showGeoAlarmLabel('geoFencing','prev')"
							style="vertical-align: bottom" /> <input type="text"
							style="width: 30px; display: none;"
							onblur="selectPage('geoFencing','blur')"
							onkeyup="selectPage('geoFencing','keyup',event)"> <span
							id="geoFencing_page" onclick='selectPage("geoFencing","click")'>0/0</span>
							<img id="geoFencing_next" alt="next"
							src="../imgs/maps/page_next.gif"
							onclick="showGeoAlarmLabel('geoFencing','next')"
							style="vertical-align: bottom" /> <img id="geoFencing_last"
							alt="last" src="../imgs/maps/page_last.gif"
							onclick="showGeoAlarmLabel('geoFencing','last')"
							style="vertical-align: bottom" />
						</td>
					</tr>
				</table>
			</div>
			<!-- 路线管理 -->
			<div id="geoLine_top"
				onclick="selectMenu('geoLine');initGeoAlarmLabel('geoLine');"
				style="background-color: #CCCCCC; width: 100%; float: left;display: none;">
				<hr style="size: 1px; width: 100%; color: #EEEEEE">
				<div style="padding: 2px; float: left;">
					<table style="margin: 0px; border: none; cursor: default;">
						<tr>
							<td><img id="geoLine_img" src="../imgs/maps/Closed.png">
							</td>
							<td><h3>Route</h3></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="geoLine" style="display: none; width:100%; overflow: auto;">
				<table style="width:100%;" class="table">
					<tr>
						<td>
							<!-- 
						<input type="text" id="geoLine_search">&nbsp;&nbsp;
						<img src="../imgs/maps/search.png" alt="搜索" align="top" onclick="searchAlarmLabel('geoLine')">
						<br/>
					 -->
							<div style="float: left; width: 100%;">
								<div class="map_tool" style="float: left; width: 25px;"
									onclick="addGeofencing('geoLine')"
									onmousemove="this.className='map_tool_selected'"
									onmouseout="this.className='map_tool'"
									onmousedown="this.className='map_tool_mousedown'"
									onmouseup="this.className='map_tool_selected'">
									<span style="cursor: default; text-align: center;">Add</span>
								</div>
								<div style="float: left; margin: 3px; color: grey; width: 2px;">|</div>
								<div class="map_tool" style="float: left; width: 25px;"
									onclick="refreshGeoFencing('geoLine')"
									onmousemove="this.className='map_tool_selected'"
									onmouseout="this.className='map_tool'"
									onmousedown="this.className='map_tool_mousedown'"
									onmouseup="this.className='map_tool_selected'">
									<span style="cursor: default; text-align: center;">Ref</span>
								</div>
								<div style="float: left; margin: 3px; color: grey; width: 2px;">|</div>
								<div class="map_tool" style="float: left; width: 25px;"
									onclick="removeGeoFencing('geoLine')"
									onmousemove="this.className='map_tool_selected'"
									onmouseout="this.className='map_tool'"
									onmousedown="this.className='map_tool_mousedown'"
									onmouseup="this.className='map_tool_selected'">
									<span style="cursor: default; text-align: center;">Del</span>
								</div>
							</div>
							<div id="addGeoLine"
								style="display: none; float: left; width: 100%; border: solid; border-color: #888888; border-width: 1px;background-color: #FFFFFF;">
								<form id="addGeoLineForm">
									<table style="width: 100%">
										<tr>
											<td width="20%">Name</td>
											<td width="80%"><input type="text" id="line_name"
												name="name" style="width: 100%;" />
											</td>
										</tr>
										<tr>
											<td>Explain</td>
											<td><input type="text" id="line_def" name="def"
												style="width: 100%;" /></td>
										</tr>
										<tr>
											<td>Route</td>
											<td><input type="text" id="line_points" name="points"
												style="width: 100%;" disabled="disabled" /> <input
												type="hidden" id="line_type" name="type" value="line" />
											</td>
										</tr>
										<tr>
											<td colspan="2" align="right"><input type="button"
												value="Cancel" onclick="cancelGeo()"
												onmouseover="this.style.backgroundColor='#FFFFFF'"
												onmouseout="this.style.backgroundColor='#DDDDDD'" /> <input
												type="button" value="Save" onclick="saveAlarmLabel('line')"
												onmouseover="this.style.backgroundColor='#FFFFFF'"
												onmouseout="this.style.backgroundColor='#DDDDDD'" />
											</td>
										</tr>
									</table>
								</form>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div id="geoLine_list" style=" width:100%;"></div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle"><img id="geoLine_first"
							alt="first" src="../imgs/maps/page_first.gif"
							onclick="showGeoAlarmLabel('geoLine','first')"
							style="vertical-align: bottom" /> <img id="geoLine_prev"
							alt="prev" src="../imgs/maps/page_prev.gif"
							onclick="showGeoAlarmLabel('geoLine','prev')"
							style="vertical-align: bottom" /> <input type="text"
							style="width: 30px; display: none;"
							onblur="selectPage('geoLine','blur')"
							onkeyup="selectPage('geoLine','keyup',event)"> <span
							id="geoLine_page" onclick='selectPage("geoLine","click")'>0/0</span>
							<img id="geoLine_next" alt="next"
							src="../imgs/maps/page_next.gif"
							onclick="showGeoAlarmLabel('geoLine','next')"
							style="vertical-align: bottom" /> <img id="geoLine_last"
							alt="last" src="../imgs/maps/page_last.gif"
							onclick="showGeoAlarmLabel('geoLine','last')"
							style="vertical-align: bottom" />
						</td>
					</tr>
				</table>
			</div>
			<!-- 关键点管理 -->
			<div id="geoPoint_top"
				onclick="selectMenu('geoPoint');initGeoAlarmLabel('geoPoint');"
				style="background-color: #CCCCCC; width: 100%; float: left;display: none;">
				<hr style="size: 1px; width: 100%; color: #EEEEEE">
				<div style="padding: 2px; float: left;">
					<table style="margin: 0px; border: none; cursor: default;">
						<tr>
							<td><img id="geoPoint_img" src="../imgs/maps/Closed.png">
							</td>
							<td><h3>Key Point</h3></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="geoPoint" style="display: none; width:100%; overflow: auto;">
				<table style="width:100%;" class="table">
					<tr>
						<td>
							<!-- 
						<input type="text" id="geoPoint_search">&nbsp;&nbsp;
						<img src="../imgs/maps/search.png" alt="搜索" align="top" onclick="searchAlarmLabel('geoPoint')">
						<br/>
					 -->
							<div style="float: left; width: 100%;">
								<div class="map_tool" style="float: left; width: 25px;"
									onclick="addGeofencing('geoPoint')"
									onmousemove="this.className='map_tool_selected'"
									onmouseout="this.className='map_tool'"
									onmousedown="this.className='map_tool_mousedown'"
									onmouseup="this.className='map_tool_selected'">
									<span style="cursor: default; text-align: center;">Add</span>
								</div>
								<div style="float: left; margin: 3px; color: grey; width: 2px;">|</div>
								<div class="map_tool" style="float: left; width: 25px;"
									onclick="refreshGeoFencing('geoPoint')"
									onmousemove="this.className='map_tool_selected'"
									onmouseout="this.className='map_tool'"
									onmousedown="this.className='map_tool_mousedown'"
									onmouseup="this.className='map_tool_selected'">
									<span style="cursor: default; text-align: center;">Ref</span>
								</div>
								<div style="float: left; margin: 3px; color: grey; width: 2px;">|</div>
								<div class="map_tool" style="float: left; width: 25px;"
									onclick="removeGeoFencing('geoPoint')"
									onmousemove="this.className='map_tool_selected'"
									onmouseout="this.className='map_tool'"
									onmousedown="this.className='map_tool_mousedown'"
									onmouseup="this.className='map_tool_selected'">
									<span style="cursor: default; text-align: center;">Del</span>
								</div>
							</div>
							<div id="addGeoPoint"
								style="display: none; float: left; width: 100%; border: solid; border-color: #888888; border-width: 1px;background-color: #FFFFFF;">
								<form id="addGeoPointForm">
									<table style="width: 100%">
										<tr>
											<td width="20%">Name</td>
											<td width="80%"><input type="text" id="point_name"
												name="name" style="width: 100%;" />
											</td>
										</tr>
										<tr>
											<td>Explain</td>
											<td><input type="text" id="point_def" name="def"
												style="width: 100%;" /></td>
										</tr>
										<tr>
											<td>Point</td>
											<td><input type="text" id="point_points" name="points"
												style="width: 100%;" disabled="disabled" /> <input
												type="hidden" id="point_type" name="type" value="point" />
											</td>
										</tr>
										<tr>
											<td colspan="2" align="right"><input type="button"
												value="Cancel" onclick="cancelGeo()"
												onmouseover="this.style.backgroundColor='#FFFFFF'"
												onmouseout="this.style.backgroundColor='#DDDDDD'" /> <input
												type="button" value="Save" onclick="saveAlarmLabel('point')"
												onmouseover="this.style.backgroundColor='#FFFFFF'"
												onmouseout="this.style.backgroundColor='#DDDDDD'" />
											</td>
										</tr>
									</table>
								</form>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div id="geoPoint_list" style=" width:100%;"></div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle"><img id="geoPoint_first"
							alt="first" src="../imgs/maps/page_first.gif"
							onclick="showGeoAlarmLabel('geoPoint','first')"
							style="vertical-align: bottom" /> <img id="geoPoint_prev"
							alt="prev" src="../imgs/maps/page_prev.gif"
							onclick="showGeoAlarmLabel('geoPoint','prev')"
							style="vertical-align: bottom" /> <input type="text"
							style="width: 30px; display: none;"
							onblur="selectPage('geoPoint','blur')"
							onkeyup="selectPage('geoPoint','keyup',event)"> <span
							id="geoPoint_page" onclick='selectPage("geoPoint","click")'>0/0</span>
							<img id="geoPoint_next" alt="next"
							src="../imgs/maps/page_next.gif"
							onclick="showGeoAlarmLabel('geoPoint','next')"
							style="vertical-align: bottom" /> <img id="geoPoint_last"
							alt="last" src="../imgs/maps/page_last.gif"
							onclick="showGeoAlarmLabel('geoPoint','last')"
							style="vertical-align: bottom" />
						</td>
					</tr>
				</table>
			</div>
			<!-- 库存查询 -->
			<div id="productStorage_top" onclick="selectMenu('productStorage');"
				style="background-color: #CCCCCC; width: 100%; float: left;display: none;">
				<hr style="size: 1px; width: 100%; color: #EEEEEE">
				<div style="padding: 2px; float: left;">
					<table style="margin: 0px; border: none; cursor: default;">
						<tr>
							<td><img id="productStorage_img"
								src="../imgs/maps/Closed.png"></td>
							<td><h3>Inventory</h3></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="productStorage"
				style="display: none; width:100%; overflow: auto;">
				<div id="select_stock"
					style="border: solid; border-color: #888888; border-width: 1px;background-color: #FFFFFF;">
					<form id="productStorageFrom">
						<table width="100%" border="0" cellspacing="3px" cellpadding="0px">
							<tr width="100%">
								<td width="35%" style="text-align: right; ">Storage:</td>
								<td width="65%" style="border-bottom:1px #dddddd solid"><select
									name="pro_psid" id="pro_psid" style="width: 100%;" class="gis_main_select">
										<%
											if (allStorageKml != null && allStorageKml.length > 0) {
												for (int i = 0; i < allStorageKml.length; i++) {
													DBRow row = allStorageKml[i];
													out.println("<option value='" + row.getString("ps_id")
															+ "'>" + row.getString("title") + "</option>");
												}
											} else {
												out.println("<option value='0'><span style='color: gray;'>storage</span></option>");
											}
										%>
								</select>
								</td>
							</tr>
							<!-- 
	            	<tr>
	            		<td style="text-align: right;">库存状态:</td>
						<td>
							<select name="pro_status" id="pro_status" style="width: 100%;">
								<option value="-1">ALL</option>
								<option value="<%=ProductStatusKey.IN_STORE%>">有货</option>
								<option value="<%=ProductStatusKey.STORE_OUT%>">缺货</option>
								<option value="<%=ProductStatusKey.NO_STORE%>">无库存(库存为0)</option>
							</select>
						</td>
					</tr>
					-->
							<tr>
								<td style="text-align: right;">Item/Set:</td>
								<td><select name="pro_type" id="pro_type"
									style="width: 100%;" class="gis_main_select">
										<option value="-1" selected="selected">ALL</option>
										<option value="0">Loose</option>
										<option value="1">Set</option>
								</select>
								</td>
							</tr>
							<tr>
								<td style="text-align: right;">Title:</td>
								<td><select name="pro_title" id="pro_title"
									style="width: 100%;"
									onchange="selectTitle(this.value,'productStorage')" class="gis_main_select">
										<option value='-1' selected="selected">ALL</option>
										<%
											if (titles != null && titles.length > 0) {
												for (int i = 0; i < titles.length; i++) {
													DBRow row = titles[i];
													out.println("<option value='" + row.getString("title_id")
															+ "'>" + row.getString("title_name") + "</option>");
												}
											}
										%>
								</select>
								</td>
							</tr>
							<tr id="pro_lot_num_tr" style="display: none;">
								<td style="text-align: right;">Lot:</td>
								<td><select name="pro_lot_num" id="pro_lot_num"
									style="width: 100%;" class="gis_main_select">
										<option value="-1" selected="selected">ALL</option>
								</select>
								</td>
							</tr>
							<tr id="pro_line_tr" style="display: none;">
								<td style="text-align: right;">PDT Line:</td>
								<td><select name="pro_line" id="pro_line"
									style="width: 100%;" class="gis_main_select"
									onchange="selectCategory(1,this.value,'productStorage')">
										<option value='-1' selected='selected'>ALL</option>
								</select>
								</td>
							</tr>
							<tr id="pro_category_1_tr" style="display: none;">
								<td style="text-align: right;">1st Level:</td>
								<td><select name="pro_category_1" id="pro_category_1"
									style="width: 100%;" class="gis_main_select"
									onchange="selectCategory(2,this.value,'productStorage')">
										<option value='-1' selected='selected'>ALL</option>
								</select>
								</td>
							</tr>
							<tr id="pro_category_2_tr" style="display: none;">
								<td style="text-align: right;">2nd Level:</td>
								<td><select name="pro_category_2" id="pro_category_2"
									style="width: 100%;" class="gis_main_select"
									onchange="selectCategory(3,this.value,'productStorage')">
										<option value='-1' selected='selected'>ALL</option>
								</select>
								</td>
							</tr>
							<tr id="pro_category_3_tr" style="display: none;">
								<td style="text-align: right;">3rd Level:</td>
								<td><select name="pro_category_3" id="pro_category_3"
									style="width: 100%;" class="gis_main_select">
										<option value='-1' selected='selected'>ALL</option>
								</select>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="text-align: right;"><input
									type="button" value="Inquiry" class="gis_main_button" onclick="searchStorageCatalog()"
									onmouseover="onmouseoverButton(this)"
									onmouseout="onmouseoutButton(this)" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<!-- 运营分析 -->
			<div id="productDemand_top" onclick="selectMenu('productDemand');"
				style="background-color: #CCCCCC; width: 100%; float: left; display: none;">
				<hr style="size: 1px; width: 100%; color: #EEEEEE">
				<div style="padding: 2px; float: left;">
					<table style="margin: 0px; border: none; cursor: default;">
						<tr>
							<td><img id="productDemand_img"
								src="../imgs/maps/Closed.png"></td>
							<td><h3>Demand Analysis</h3></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="productDemand"
				style="display: none; width:100%; overflow: auto;">
				<div
					style="border: solid; border-color: #888888; border-width: 1px;background-color: #FFFFFF;">
					<form id="productDemandFrom">
						<table width="100%" border="0" cellspacing="3px" cellpadding="0px">
							<tr>
								<td width="35%" style="text-align: right;">Title:</td>
								<td width="65%"><select name="pro_title" id="pro_title"
									style="width: 100%;" class="gis_main_select"
									onchange="selectTitle(this.value,'productDemand')">
										<option value='-1' selected="selected">ALL</option>
										<%
											if (titles != null && titles.length > 0) {
												for (int i = 0; i < titles.length; i++) {
													DBRow row = titles[i];
													out.println("<option value='" + row.getString("title_id") + "'>" + row.getString("title_name") + "</option>");
												}
											}
										%>
								</select>
								</td>
							</tr>
							<tr id="pro_line_tr" style="display: none;">
								<td style="text-align: right;">PDT Line:</td>
								<td><select name="pro_line" id="pro_line"
									style="width: 100%;"
									onchange="selectCategory(1,this.value,'productDemand')">
										<option value='-1' selected='selected'>ALL</option>
								</select>
								</td>
							</tr>
							<tr id="pro_category_1_tr" style="display: none;">
								<td style="text-align: right;">1st Level:</td>
								<td><select name="pro_category_1" id="pro_category_1"
									style="width: 100%;"
									onchange="selectCategory(2,this.value,'productDemand')">
										<option value='-1' selected='selected'>ALL</option>
								</select>
								</td>
							</tr>
							<tr id="pro_category_2_tr" style="display: none;">
								<td style="text-align: right;">2nd Level:</td>
								<td><select name="pro_category_2" id="pro_category_2"
									style="width: 100%;"
									onchange="selectCategory(3,this.value,'productDemand')">
										<option value='-1' selected='selected'>ALL</option>
								</select>
								</td>
							</tr>
							<tr id="pro_category_3_tr" style="display: none;">
								<td style="text-align: right;">3rd Level:</td>
								<td><select name="pro_category_3" id="pro_category_3"
									style="width: 100%;">
										<option value='-1' selected='selected'>ALL</option>
								</select>
								</td>
							</tr>
							<tr>
								<td style="text-align: right;">PDT Name:</td>
								<td><input type="text" id="pro_name" style="width: 100%;">
								</td>
							</tr>
							<tr>
								<td style="text-align: right;">Begin:</td>
								<td><input type="text" id="start_time" style="width: 100%;">
								</td>
							</tr>
							<tr>
								<td style="text-align: right;">End:</td>
								<td><input type="text" id="end_time" style="width: 100%;">
								</td>
							</tr>
							<tr>
								<td colspan="2" style="text-align: right;"><input
									type="button" value="Inquiry" class="gis_main_button" 
									onclick="searchProductDemandByCountry()"
									onmouseover="onmouseoverButton(this)" onmouseout="onmouseoutButton(this)" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<div id="maintainLayer_top" onclick="selectMenu('maintainLayer');"
				style="background-color: #CCCCCC; width: 100%; float: left;display: none">
				<hr style="size: 1px; width: 100%; color: #EEEEEE">
				<div style="padding: 2px; float: left;">
					<table style="margin: 0px; border: none; cursor: default;">
						<tr>
							<td><img id="maintainLayer_img"
								src="../imgs/maps/Closed.png"></td>
							<td><h3>Layer Maintenance</h3></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="maintainLayer"	style="display: none; width:100%; overflow: auto; cursor: default;">
				<p id="create_layer" style="margin:0px;padding:5px;text-align:center;font-size:13; background:#eeeeee;border:solid 1px #DDDDDD;"
					onclick="showLayerDome('layer_demo')">Layer Element</p>
				<div id="layer_demo" style="margin:0px;padding:5px;text-align:center;background:#FFFFFF;border:solid 1px #DDDDDD;display: none; height:210px;">
					<div  id="image_01" >
						<img id="webcam_img" src="../imgs/maps/webcam.png" onmousedown="LayerMousedown(6,this,event)">
					</div>
					<div><b>Webcam</b></div>
					</br>
					<div  id="image_02" >
						<img id="webcam_img" src="../imgs/maps/printer_layer.png" onmousedown="LayerMousedown(7,this,event)">
					</div>
					<div><b>Printer</b></div>
					</br>
					<div  id="image_04" >
						<img id="webcam_img" src="../imgs/maps/light_off.png" onmousedown="LayerMousedown(9,this,event)">
					</div>
					<div><b>Light</b></div>
					</br>
					<div id="image_03" >
						<img id="rectangle_img" src="../imgs/maps/rectangle_img.png" onmousedown="LayerMousedown('x',this,event)">
					</div>
					<div><b>Rectangle</b></div>
				</div>
				<p id="create_printer_servers"
					style="margin:0px;padding:5px;text-align:center;font-size:13; background:#eeeeee;border:solid 1px #DDDDDD;"
					onclick="showLayerDome('printer_servers')">Printer Servers</p>
				<div id="printer_servers" style="display: none; width:100%; height:30px; overflow: auto;" align="center" >
					<input type="button" value="New Servers "  style="border: 0px; background-image: url(../imgs/maps/bk_servers.png);width: 100px;height: 30px;" onclick="openDialog()"/>
				</div>
				<!-- <p style="margin:0px;padding:5px;text-align:center;font-size:13; background:#eeeeee;border:solid 1px #DDDDDD;"
					onclick="showLayerDome('edit_road')">Road Manage</p>
				<div id="edit_road" style="display: none; width:100%; height:30px; overflow: auto;" align="center" >
					<input type="button" value="Edit Road"  style="border: 0px; background-image: url(../imgs/maps/bk_servers.png);width: 100px;height: 30px;" onclick="initEditRoad(currentPsId)"/>
				</div>
				 -->
			</div>
			<div id="stagingPlate_top" onclick="selectMenu('stagingPlate');"
				style="background-color: #CCCCCC; width: 100%; float: left;display: none">
				<hr style="size: 1px; width: 100%; color: #EEEEEE">
				<div style="padding: 2px; float: left;">
					<table style="margin: 0px; border: none; cursor: default;">
						<tr>
							<td><img id="stagingPlate_img"
								src="../imgs/maps/Closed.png"></td>
							<td><h3>Select Container</h3></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="stagingPlate" style="display: none; width:100%; overflow: auto;">
				<div style="border: solid; border-color: #888888; border-width: 1px;background-color: #FFFFFF;">
					<table width="100%" border="0" cellspacing="3px" cellpadding="0px">
						<tr>
							<td style="text-align: right;width: 30%;">CTNR:</td>
							<td>
								<select id="ic_id" class="gis_main_select">
									<option value="-1">ALL</option>
									<% 
									 DBRow [] containers=googleMapsMgrCc.selectContainer(3);
									if(containers.length>0){
										for(int i=0;i<containers.length;i++){
											DBRow container=containers[i];
											%>
											<option value="<%=container.get("ic_id") %>"><%=container.get("container_no")%></option>
											<%
										}
									}
									%>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="right">
								<input type="button" value="Inquiry" class="gis_main_button" onclick="queryPlate()"  onmouseover="onmouseoverButton(this)" onmouseout="onmouseoutButton(this)"/>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div id='gis_main' class='gis_main'>
			<div id="map_tool" style="width:99%; float:left; font-size: 12px;"></div>
			<div id='jsmap' style="width:100%; height:100%;"></div>
		</div>
		<div class="contextMenu" id="contextMenu">
			<ul>
				<%
					for (int i = 0; i < contextCmds.length; i++) {
				%>
				<li id="<%=contextCmds[i].getString("nameen")%>"><%=contextCmds[i].getString("name")%>
				</li>
				<%
					}
				%>
			</ul>
		</div>
		<div id="replayCtrl" style="display: none;">
			<table style="width: 200px; padding: 5px;" cellspacing="5px">
				<tr>
					<td style="width: 30%" align="right">Speed</td>
					<td style="width: 50%">
						<div
							style="height: 8px; width: 100%; background-color: #FFFFFF; border: 1px solid #999999; border-radius: 4px;"
							align="left" onmousemove="setReplaySpeed(event,false)"
							onclick="setReplaySpeed(event,true)"
							onmouseout="unSetReplaySpeed()">
							<div id="replay_speed"
								style="height: 100%; width: 10%; background-color: #999999;"></div>
						</div>
						<div id="replay_speed_value_div"
							style="position: absolute; display: none; font-size: 8px; background-color: #FFFFFF; border: 1px solid #000000; border-radius: 3px;">12</div>
					</td>
					<td id="replay_speed_value" style="width: 20%">5p/s</td>
				</tr>
				<tr>
					<td align="right">Progress</td>
					<td>
						<div
							style="height: 8px; width: 100%; background-color: #FFFFFF; border: 1px solid #999999; border-radius: 4px;"
							align="left">
							<div id="replay_process"
								style="height: 100%; width: 0%; background-color: #999999;"></div>
						</div>
					</td>
					<td id="replay_process_value">0%</td>
				</tr>
				<tr>
					<td colspan="3" align="center"><input type="button" id="play"
						onclick="replayCtrl('play')"
						style="display: none; width: 20px; height: 16px; background-repeat: no-repeat; background-position: center; background-image: url('../imgs/maps/play.png'); " />
						<input type="button" id="pause" onclick="replayCtrl('pause')"
						style="width: 20px; height: 16px; background-repeat: no-repeat; background-position: center; background-image: url('../imgs/maps/pause.png');" />
						<input type="button" id="stop" onclick="replayCtrl('stop')"
						style="width: 20px; height: 16px; background-repeat: no-repeat; background-position: center; background-image: url('../imgs/maps/stop.png');" />
					</td>
				</tr>
			</table>
		</div>
		<!-- 右键菜单样式 -->
		<div id="menu" style="width:auto;">
			<ul>
				<!-- <li id="showWebcam" onclick="showWebcam()" ><a href="#">View Webcam</a> 
				</li>-->
				<!-- <li id="setWebcam" onclick ="openWebcamWind()"><a href="#">Set Webcam</a> -->
				</li>
				<li id="modifyZone" onclick="modifyZone()" style="display: none;"><a
					href="#">Modify Zone</a></li>
				<li id="modifyPosition" onclick="modifyPosition()"
					style="display: none;"><a href="#">Modify Layer</a></li>
				<li id="stopUse" onclick="stopUse()" style="display: none;"><a
					href="#">Stop Use</a></li>
				<li id="startUse" onclick="startUse()" style="display: none;"><a
					href="#">Reuse</a></li>
				<li id="navFrom" onclick="routePlan('from')" style="display: none;"><a
					href="#">From here</a></li>
				<li id="navTo" onclick="routePlan('to')" style="display: none;"><a
					href="#">To here</a></li>
				<li id="addPerson" onclick="addPerson()" style="display: none;"><a
					href="#">Add Person</a></li>
				<li id="addDock" onclick="addDock()" style="display: none;"><a
					href="#">Add Dock</a></li>
			</ul>
		</div>
		<div id="map_tool_control" style="position: absolute; display: none; background-color: #FFFFFF; border: solid 1px #AAAAAA;"
				onmouseover="this.value=1" onmouseout="this.value=0;mapToolCtrlMouseout()">
			<div id="measure_area" class="map_tool" onclick="mapToolClick(this)"
				onmousemove="this.className='map_tool_selected'"
				onmouseout="if(this.getAttribute('flag')!='1'){this.className='map_tool'}"
				onmousedown="this.className='map_tool_mousedown';this.setAttribute('flag','1')"
				onmouseup="this.className='map_tool_selected'">
				<img alt="clear" src="../imgs/maps/biaochi.png" align="top">
				<span style="cursor: default;">Area</span>
			</div>
			<div id="measure_distance" class="map_tool"
				onclick="mapToolClick(this)"
				onmousemove="this.className='map_tool_selected'"
				onmouseout="if(this.getAttribute('flag')!='1'){this.className='map_tool'}"
				onmousedown="this.className='map_tool_mousedown';this.setAttribute('flag','1')"
				onmouseup="this.className='map_tool_selected'">
				<img alt="clear" src="../imgs/maps/biaochi.png" align="top">
				<span style="cursor: default;">Distance</span>
			</div>
			<div id="move_map" flag="1" class="map_tool_selected"
				onclick="mapToolClick(this)"
				onmousemove="this.className='map_tool_selected'"
				onmouseout="if(this.getAttribute('flag')!='1'){this.className='map_tool'}"
				onmousedown="this.className='map_tool_mousedown';this.setAttribute('flag','1')"
				onmouseup="this.className='map_tool_selected'">
				<img alt="clear" src="../imgs/maps/pan1.png" align="top">
				<span style="cursor: default;">Move Map</span>
			</div>
		<div id="display_legend" class="map_tool" onclick="openRefreshTimeWin()"
				onmousemove="this.className='map_tool_selected'"
				onmouseout="this.className='map_tool'"
				onmousedown="this.className='map_tool_mousedown'"
				onmouseup="this.className='map_tool_selected'">
				<img alt="clear" src="../imgs/maps/setting.png" align="top">
				<span style="cursor: default;">Set Refresh Time</span>
			</div>
		
			<div id="coordinate_system" class="map_tool" onclick="coordinateSystem(this)"
				onmousemove="this.className='map_tool_selected'"
				onmouseout="this.className='map_tool'"
				onmousedown="this.className='map_tool_mousedown'"
				onmouseup="this.className='map_tool_selected'">
				<img alt="clear" src="../imgs/maps/coordinate_sys.png" align="top">
				<span style="cursor: default;">Coordinate System</span>
			</div>
			
		</div>
</body>
<script type="text/javascript">
//显示地图工具
function displayMapTool(el){
	if(jsmap.mapControls["tool"]){
		var el = $(jsmap.mapControls["tool"].div);
		var top = (el.offset().top + el.height()+2).toFixed(0);
		var left = el.offset().left.toFixed(0);
		var tool = $("#map_tool_control");
		tool.offset({top : top, left : left});
		tool.show();
		tool.attr("value","1");
		//el.attr("onmouseout","mapToolCtrlMouseout()");
		el.bind("mouseout",mapToolCtrlMouseout);
	}
}
function mapToolCtrlMouseout(){
	
	var tool = $("#map_tool_control");
	tool.attr("value","0");
	
	setTimeout(function(){
		var t = $("#map_tool_control");
		if(t && t.attr("value")!="1"){
			t.hide();
		}
	},1000);
	
}
//设置回放速度
function setReplaySpeed(e,flag){
	var maxValue = 50;
	var ele = $("#floatWindowInfo #replay_speed");
	var len = e.clientX - ele.offset().left;
	var per = len/ele.parent().width();
	var value = parseInt((maxValue-1)*per+1);  //范围1-50
	
	var valShow = $("#floatWindowInfo #replay_speed_value_div");
	valShow.html(value);
	var top = ele.offset().top - valShow.height() - 3;
	var left = e.clientX - valShow.width()/2;
	valShow.show();
	valShow.offset({top: top, left: left});
	if(flag){ //鼠标点击时设置
		replaySpeed = value;
		$("#floatWindowInfo #replay_speed_value").html(value+"p/s");
		ele.css("width",parseInt(per*100)+"%");
	}
}
function unSetReplaySpeed(){
	$("#floatWindowInfo #replay_speed_value_div").hide();
}
function setReplayProcess(count,num){
	var per = num/count;
	var value = parseInt(per*100)+"%";
	$("#floatWindowInfo #replay_process").css("width",value);
	$("#floatWindowInfo #replay_process_value").html(value);
	if(per == 1){
		replayCtrl("stop");
	}
}
function replayCtrl(ctrl){
	if(ctrl == "play"){
		replayPause = false;
		historyPlay(false);
		$("#floatWindowInfo #play").hide();
		$("#floatWindowInfo #pause").show();
	}
	if(ctrl == "pause"){
		replayPause = true;
		$("#floatWindowInfo #pause").hide();
		$("#floatWindowInfo #play").show();
	}
	if(ctrl == "stop"){
		replayPause = true;
		$("#floatWindowInfo #pause").hide();
		$("#floatWindowInfo #play").show();
		stopReplay();
	}
}
function historyPlayAfterFitMapBounds(){
	replayPause = false;
	var force = false;
	if(currentPsId == null){
		force = true;
	}
	//先清除正在播放的轨迹
	clearHistory();
	var flag = historyPlay(true);
	fitHistoryBounds(force);
	return flag;
}

//gps实时位置刷新时间
var currentPositionInterval =null;
//setInterval(getLastPosition, time.truck);
//解析历史轨迹
function parseHistory(data){
	pps = [];
  	var list = [];
	for(var i=0; i<data.length; i++){
		var p = new JSMapPushpin(data[i].lat,data[i].lon,{},null);
		var icon = getHeadingMarkerIconURL2("green",data[i].rad);
		var markerImage = jsNewMarkerImege(icon, null, null, jsNewPoint(18, 18), null);
		p.icon = markerImage;
		pps.push(p);
		//new LabelOverlay(list[i],i)
	}
}
function getLastPosition(){
	if(assetIds == ""){
		return;
	}
    $.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/carsCommandsAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 6000,
		cache:false,
		data:{ids:assetIds,option:1},
		beforeSend:function(request){
			
		},
		error: function(e){
			//alert("定位失败,请稍后重新尝试");
		},
		success: function(data){
			if(data){
				//移除不需要显示的数据
				for(var i=0; i<data.length; i++){
					var id = "asset_" + data[i].aid;
					if(assetNotDisplay[id]){
						data.splice(i--,1);
					}
				}
				showCarsInMap(data);
			}
		}
	});
}
function contextMenu(){
  	 $('#child_ul li').contextMenu('contextMenu', 
     {
	  itemStyle: {
        backgroundColor : '#EEEEEE',
        border: 'none'
      },
      bindings: {
	      
	  	  'Location': function(t) {
    		$.tree.plugins.checkbox.check(t);
    	    treeNodeSelect(t);
		  	getCarMapLocation(t);
	      },
          'History': function(t) {
		      //$.tree.plugins.checkbox.check(t);
		      //treeNodeSelect(t);
	          var asset_id=(t.id).substring(6,(t.id).length);
	          var title = "Historical track ["+t.textContent.trim()+"]";
	   	 	  var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/query_history.html?asset_id='+asset_id;
			  $.artDialog.open(url, {title: title,width:'350px',height:'170px', lock: false,opacity: 0.3});
         },
         'GeoFence': function(t){
        	  var asset_id=(t.id).substring(6,(t.id).length);
	          var asset_name = t.textContent.trim();
	          var imei = $(t).attr("imei");
	          var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/asset_alarm_manage.html?type=geoFencing&asset_id='+asset_id+'&asset_name='+asset_name+'&imei='+imei+"&p=1,1";
			  $.artDialog.open(url, {title: "Geofencing Management",width:'800px',height:'480px', lock: false,opacity: 0.3});
	      },
         'KeyPoint': function(t){
        	  var asset_id=(t.id).substring(6,(t.id).length);
	          var asset_name = t.textContent.trim();
	          var imei = $(t).attr("imei");
	          var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/asset_alarm_manage.html?type=geoPoint&asset_id='+asset_id+'&asset_name='+asset_name+'&imei='+imei+"&p=1,1";
			  $.artDialog.open(url, {title: "Key Point Management",width:'800px',height:'480px', lock: false,opacity: 0.3});
	      },
         'Road': function(t){
        	  var asset_id=(t.id).substring(6,(t.id).length);
	          var asset_name = t.textContent.trim();
	          var imei = $(t).attr("imei");
	          var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/asset_alarm_manage.html?type=geoLine&asset_id='+asset_id+'&asset_name='+asset_name+'&imei='+imei+"&p=1,1";
			  $.artDialog.open(url, {title: "Route Management",width:'800px',height:'480px', lock: false,opacity: 0.3});
	      },
		 'LocFrequency': function(t){
			  var asset_id=(t.id).substring(6,(t.id).length);
			  var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/loc_frequency.html?asset_id='+asset_id;
		   	  $.artDialog.open(url, {title: "Set GPS Frequency",width:'320px',height:'175px', lock: false,opacity: 0.3,fixed: true});
		 }
     }
	  });
}
function addAsset(){
   var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/add_asset.html';
   $.artDialog.open(url, {title: "Add GPS",width:'400px',height:'280px', lock: false,opacity: 0.3,fixed: true});
}

function showLayerDome(id){
	  $("#"+id).slideToggle("slow");
}

var imgHeight="";
var imgWidth="";
var imgEventX="";
var imgEventY="";
var imgDiv="";
var imgDivWinW="";
var imgDivWinH="" ;
var imgDivWinX="";
var imgDivWinY="" ;
function LayerMousedown(type,el,event){
	if(type==6){
	$("#kml_"+currentPsId+"_layer input[id='webcam']").attr("checked","checked");
	 drawWebcam(currentPsId);
	}else if(type==7){
	$("#kml_"+currentPsId+"_layer input[id='printer']").attr("checked","checked");
	drawPrinter(currentPsId);
	}else if(type ==9){
	$("#kml_"+currentPsId+"_layer input[id='light']").attr("checked","checked");
	DrawLightLayer(currentPsId);	
	}
	var e = event || window.event; 
    e.preventDefault();
	e.stopPropagation();
	imgDiv=$(el).clone();
	imgDiv.css("z-index",100);
	imgDiv.removeAttr("onmousedown");
	imgDiv.attr({layertype : type});
	imgDiv.id="drag_img";
    imgHeight=$(el).height();
    imgWidth=$(el).width();
    imgDiv.css({position :'absolute'});
	imgEventX=e.clientX-imgWidth/2;
	imgEventY=e.clientY-imgHeight;
	imgDiv.offset({left:imgEventX,top:imgEventY});
	$("body").append(imgDiv);
	document.onmousemove=LayerMousemove;
	document.onmouseup = LayerMouseStop;
}
function LayerMousemove(e){
	imgEventX=e.clientX-imgWidth/2;
	imgEventY=e.clientY-imgHeight/2;
	imgDiv.offset({left:imgEventX,top:imgEventY});
}
function LayerMouseStop(e){
	if(!currentPsId&&$.isEmptyObject(currentPsId)){
		alert("Please choose storage first!");
		document.onmouseup=null;
		imgDiv.remove();
		return ;
	}
	var type = imgDiv.attr("layertype");
	document.onmousemove = null;
	document.onmouseup = null;
	imgEventX=e.clientX;
	imgEventY=e.clientY;
	var map = document.getElementById(MAP_ID);
	var mapWinX = map.offsetLeft;
	var mapWinY = map.offsetTop;
	var mapWinW = map.offsetWidth;
	var mapWinH = map.offsetHeight;
	imgDivWinW = imgDiv.width();
	imgDivWinH = imgDiv.height();
	var x = imgEventX - mapWinX;
	var y = imgEventY - mapWinY;
	imgDivWinX = imgDiv.position().left;
	imgDivWinY = imgDiv.position().top;
	var newWinX = imgDivWinX + e.clientX - imgEventX;
	var newWinY = imgDivWinY + e.clientY - imgEventY;
	if((mapWinX < newWinX && (mapWinX + mapWinW) >(newWinX + imgDivWinW))&&(mapWinY < newWinY && (mapWinY + mapWinH) >(newWinY + imgDivWinH))){ //禁止浮动框移出地图
		if(type==6){
			 y += imgHeight/2;
			var latlng=jsmap.fromContainerPixelToLatLng(x,y);
			var lat=latlng.lat();
			var lng=latlng.lng();
			createLayerDemo(type,currentPsId ,lat,lng);
			var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/camparameter.html?ps_id='+currentPsId+'&id=0&pageType=0&inner_radius=0&outer_radius=100&s_degree=0&e_degree=150'+'&lat='+lat+'&lng='+lng;
			$.artDialog.open(url, {title: "Set Webcam [demo]",width:'475px', lock: false,opacity: 0.3,fixed: true,id:"webcam",cancel:false});
		}
		if(type==7){	
			x -= parseInt(imgWidth/2);
			y += parseInt(imgHeight/2);
			var latlng=jsmap.fromContainerPixelToLatLng(x,y);
			var lat=latlng.lat();
			var lng=latlng.lng();
			createLayerDemo(type,currentPsId ,lat,lng);
			var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/modify_printer.html?psId='+currentPsId+'&id=0&pageType=0&lat='+lat+'&lng='+lng;
			$.artDialog.open(url, {title: "Set Printer [dome]",width:'320px', lock: false,opacity: 0.3,fixed: true,id:"printer",cancel:false});
		}
		if(type==9){	
			y += imgHeight/2;
			var latlng=jsmap.fromContainerPixelToLatLng(x,y);
			var lat=latlng.lat();
			var lng=latlng.lng();
			createLayerDemo(type,currentPsId ,lat,lng);
			var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/modify_light.html?ps_id='+currentPsId+'&id=0&pageType=0&lat='+lat+'&lng='+lng;
			$.artDialog.open(url, {title: "Set Light [dome]",width:'320px', lock: false,opacity: 0.3,fixed: true,id:"light",cancel:false});
		}
		if(type=="x"){
			var whs=$("#kml_"+currentPsId).find("span").text();
			var latlng=jsmap.fromContainerPixelToLatLng(x,y);
			var lat=latlng.lat();
			var lng=latlng.lng();
			createLayerDemo(type,currentPsId ,lat,lng);
			var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/modify_storage_layer.html?psId='+currentPsId+'&lat='+lat+'&lng='+lng+'&storage_name='+whs;
			$.artDialog.open(url, {title: "WHS:["+whs+"] Layer",width:'360px', lock: false,opacity: 0.3,fixed: true,id:"layer",cancel:false});
		}
  	}
	imgDiv.remove();
}

function openDialog(){
     var url='<%=ConfigBean.getStringValue("systenFolder")%>administrator/print/print_set_list.html?ps_id='+currentPsId;
     $.artDialog.open(url, {title: "Create Printer Servers ",width:'50%',height:'60%', lock: false,opacity: 0.3,fixed: true,id:"create_servers"});
}

function showZonedocks(psId,area_id ,area_name){
	 var url='<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/show_zone_docks.html?ps_id='+currentPsId+'&area_id='+area_id+'&area_name='+area_name;
     $.artDialog.open(url, {title: area_name+" Info",width:'420px',height:'300px', lock: true,opacity: 0.3,fixed: true,id:"zoneDocks"});
}
function showZonePerson(psId,area_id ,area_name){
	 var url='<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/show_zone_person.html?ps_id='+currentPsId+'&area_id='+area_id+'&area_name='+area_name;
     $.artDialog.open(url, {title: area_name+" Info",width:'420px',height:'300px', lock: true,opacity: 0.3,fixed: true,id:"zonePerson"});
}

function refreshWindow(){
	 window.location.reload();
}

function getAotuLocations(){
	var  obj =jsmap.storageDemoLayer;
	var layers="";
	for (var key in obj){
		if(key.indexOf("autoLocal")!=-1){
			if(!$.isEmptyObject(obj[key].data)){
			var name =obj[key].data.name;
			var psId =obj[key].data.psId;
			var poly=obj[key];
			var height=obj[key].data.height;
			var width=obj[key].data.width;
			var latlng=obj[key].data.point;
			var angle=obj[key].data.angle;
			var point =convertLatlngToCoordinateAjax(psId, latlng.lat(), latlng.lng());
			var x =point.x;
			var y =point.y;
			var local =psId+","+name+","+height+","+width+","+x+","+y+","+angle+";";
			layers+=local;
		    }
		}
	}
		return layers;
}
/**
 * 在area内添加人员
 */
 function addPerson(){
	var areaInfo= $("#jsmap").data("curr_poly_data");
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/show_zone_person.html?ps_id='+areaInfo.psId+"&area_name="+areaInfo.name;
	$.artDialog.open(url, {title: "Modify zone ["+areaInfo.name.toUpperCase()+"]",width:'420px',height:'320px', lock: true,opacity: 0.3,id:"area_info",cancel:false});
}
/**
 * 在area中添加门
 */
 function addDock(){
	 var areaInfo= $("#jsmap").data("curr_poly_data");
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/show_zone_docks.html?ps_id='+areaInfo.psId+"&area_name="+areaInfo.name;
		$.artDialog.open(url, {title: "Modify zone ["+areaInfo.name.toUpperCase()+"]",width:'420px',height:'320px', lock: true,opacity: 0.3,id:"area_info",cancel:false});
}
 /**
  * ctrl+单击鼠标指定路线
  */
  function prescribedRoute_(flag,e,data){
 	if(!from || !to){
 		var latLng=jsmap.overlay.getProjection().fromContainerPixelToLatLng(jsNewPoint(e.clientX-flag.offsetLeft,e.clientY-flag.offsetTop));
 		if(storageKmlRightClick){
 			var position = jsmap.overlay.getProjection().fromLatLngToContainerPixel(latLng);
 			if(e.ctrlKey){
 				$("#jsmap").data("latlng",latLng);
 				storageKmlRightClick(data.data, position, latLng);
 				if(!from ){
 					from='from';
 					routePlan('from');
 				}else if(!to){
 					routePlan('to');
 					to='to';
 				}
 				
 			}
 		}
 	}
 }
 function openRefreshTimeWin(){
	 var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/set_refresh_time.html?truck_time='+time.truck+'&dock_time='+time.dock;
	 $.artDialog.open(url, {title: "Set Refresh Time ",width:'420px', lock: true,opacity: 0.3,id:"set_time"}); 
 }
 function  setRefreshTime(parm){
	 this.time.truck=parm.truck;
	 this.time.dock=parm.dock;
	 if(storageObjInterval!=null){
		 clearInterval(storageObjInterval);
		 parkingDocksOccupancy();
		 storageObjInterval= setInterval(parkingDocksOccupancy,time.dock);
	 }
	 startCarIsChecked();
 }
 
 function startCarIsChecked(){
	 var readGps = false;
		for(var key in assetNotDisplay){
			if(!assetNotDisplay[key]){
				readGps = true;
				break;
			}
		}
		if(currentPositionInterval!=null){
			clearInterval(currentPositionInterval);
		}
		if(readGps){
			getLastPosition();
			currentPositionInterval =setInterval(getLastPosition, time.truck);
		}
 }
 function queryPlate(){
	 
	 if(!currentPsId){
		 showMessage("Choose storage please!","alert"); 
		return ;
	 }
	 initStagingState(currentPsId);
	 layerSelectStatus(['staging'],true,true,false);
	var filter_id= $("#ic_id").val();
	   $.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/showContainerCountsAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 6000,
			cache:false,
			data:{"ic_id":filter_id,"ps_id":currentPsId},
			beforeSend:function(request){
				
			},
			success: function(data){
				if(data&&data.flag=="true"){
				showStagingPlate(data.rows,filter_id); 	
				}else if(data&&data.flag=="nodata"){
				showMessage("no data","alert");	
				}
			},
			error: function(e){
				//alert("定位失败,请稍后重新尝试");
				showMessage("System error","error");	
			}
		});
	 
	 
 }
 
 function openContainerWin(data){
		var staging_id=data.staging_id;
		var ic_id=data.filter_id;
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/showStagingPlate.html?staging_id='+staging_id+'&ic_id='+ic_id;
		$.artDialog.open(url, {title: "["+data.name+"]",width:'460px', lock: true,opacity: 0.3,fixed: true,id:"stagingPlate"})
 }
 function onmouseoverButton(flag_el){
	 flag_el.style.background='#E6E6FA';
	 flag_el.style.border='solid 1px #C5C1AA';
 }
 function onmouseoutButton(flag_el){
	 flag_el.style.background='#2288cc';
	 flag_el.style.border='solid 1px #1c6a9e';
 }
</script>
</html>
