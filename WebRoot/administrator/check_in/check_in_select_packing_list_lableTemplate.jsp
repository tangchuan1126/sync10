<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@page import="com.cwc.app.key.ModuleKey" %>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/>
<%@ include file="../../include.jsp"%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>

<%
	
	String entry_id = StringUtil.getString(request, "entryId");
	
	String jsonString = StringUtil.getString(request, "jsonString");
	JSONArray jsons = new JSONArray(jsonString);
	
	long adid = 0L;
	if(adid == 0l){
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	    adid = adminLoggerBean.getAdid();
	}
	
   
%>
<html>
  <head>
    <title>selectLableTemplate</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">

  <style type="text/css">
  	.lableTemplate{
  		margin-top:2px;
  		height:424px;
  		border:2px #dddddd solid;
  		background:#eeeeee;
  		padding:5px;
  		-webkit-border-radius:7px;
  		-moz-border-radius:7px;
  		overflow-y:auto;
  	}
  	.td_line{border-bottom: 1px solid #dddddd; font-family:Verdana;}
  </style>
 <script type="text/javascript">
  	function selectAllPackingList(){
  		var $lables = $("input[name='packing_list']");
  		if($("#packing_list_all").attr("checked")){
  			$lables.each(function(){
  				$(this).attr("checked",true);
  			});
  		}else{
  			$lables.each(function(){
  				$(this).attr("checked",false);
  			});
  		}
  	}
  	
  	function printBataPackList(){
  		var ses=$("input[name='packing_list']:checked");
  		if(ses.length==0){
  			alert("Please select a label");
  			return false;
  		}
  		var str = [];
  		for(var i=0;i<ses.length;i++){
  			var number=$(ses[i]).attr('num');
  			var companyId=$(ses[i]).attr('companyId');
  			var customerId=$(ses[i]).attr('customerId');
  			var accountId=$(ses[i]).attr('accountId');
  			
  			var json = {
				order_no:number,
				companyId:companyId,
				customerId:customerId,
				accountId:accountId
  			};
  			str.push(json);
  		}
  		var json = JSON.stringify(str);
  		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>check_in/a4print_packing_list.html',
			dataType:'html',
			async:false,
			data:'jsonString='+json+'&isprint=1',
			success:function(html){	
				$('#hiddenPackingList').html(html);
				printPackingList();
			}
		});
  	}
  	
  	
  	function printSinglePackList(order_no,companyId,customerId,accountId){
  		var str = [];
			var json = {
				order_no:order_no,
				companyId:companyId,
				customerId:customerId,
				accountId:accountId
			};
			str.push(json);
			var json = JSON.stringify(str);
			var url='<%=ConfigBean.getStringValue("systenFolder")%>check_in/a4print_packing_list.html?jsonString='+json;
			$.artDialog.open(url, {title: "Webcam List",width:'900px',height:'600px', lock: false,opacity: 0.3,fixed: true});	
  	}
</script>
</head>
<body>
    <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;" align="right">
    	<input class="long-long-button" type="button" value="Combined Print" onclick="printBataPackList()"/>
    </div>
    <div class="lableTemplate">
   		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td width="30%" class="td_line">&nbsp;</td>
		    <td style="border-bottom: 1px solid #dddddd; font-family:Verdana;border-left: 1px solid #dddddd">
		    	<input onclick="selectAllPackingList()" type="checkbox" id="packing_list_all" name="packing_list_all" />
		    </td>
		    <td class="td_line">&nbsp;</td>
		    <td class="td_line">&nbsp;</td>
		  </tr>
		</table>
		<%  for(int i=0;i<jsons.length();i++){%>
		<%  JSONObject json=jsons.getJSONObject(i);
				String number=json.getString("number");
		    	String checkDataType=json.getString("checkDataType");
		    	String companyId=json.getString("companyId");
		    	String customerId=json.getString("customerId");
		    	String doorName=json.getString("door_name");
		    	int number_type=json.getInt("number_type");
		    	String order_no=json.getString("order");
		    	if((checkDataType.toUpperCase()).equals("DELIVERY")){
		    		continue;
		    	}
		    	DBRow[] orders=null;
		    	if(ModuleKey.CHECK_IN_LOAD == number_type){
		    		orders = sqlServerMgr.findMasterBolLinesByMasterBolStr(number,"",companyId, customerId,adid, null);//customerId
// 					if(0 == orders.length){
// 						orders = sqlServerMgr.findOrdersNoAndCompanyByLoadNoOrderStr(number,"",companyId, customerId,adid, null);//customerId
// 					}
		    	}else{
		    	
		    		orders = new DBRow[1];
					DBRow row = new DBRow();
					if(ModuleKey.CHECK_IN_ORDER == number_type){
						row.add("OrderNo", number);
					}else if(ModuleKey.CHECK_IN_PONO == number_type){
						row.add("OrderNo", order_no);
					}
					row.add("CompanyID", companyId);
					row.add("customerId", customerId);
					orders[0] = row;
		    	}
		   		for(int a = 0; a < orders.length; a ++){ 
		%>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#FFF;">	
			  <tr>
			    <td width="30%" align="center" valign="middle" style="font-size: 18px; font-weight:bold;border-bottom: 1px solid black;">
			    	<div align="left" style="font-size:12px; color:#666;padding-left:20px;">Order:</div>
			    	<div align="left" style="padding-left:20px;"><%=orders[a].get("OrderNo", 0L) %></div>
			    	<div align="left" style="font-size:12px; color:#666;padding-left:20px;"><%=orders[a].getString("CompanyID")%></div>
			    </td>
			    <td style="border-left: 1px solid #dddddd;border-bottom: 1px solid black;">				
			    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td align="left" valign="middle" width="50px;" style="border-bottom:1px solid #dddddd">
					    	<input type="checkbox"  num="<%=orders[a].get("OrderNo", 0L) %>"  name="packing_list" companyId="<%=orders[a].getString("CompanyID")%>" customerId="<%=orders[a].getString("customerId")%>" accountId="<%=orders[a].getString("AccountID")%>"  onclick="changeAllBtn()" />
					    </td>
					    <td width="400px;" style="border-bottom:1px solid #dddddd">
						    <div style="font-size:30px;font-weight:bold;">
						    	<a href="javascript:void(0)"  style="text-decoration:none;outline:none" onclick="printSinglePackList(<%=orders[a].get("OrderNo", 0L) %>,'<%=orders[a].getString("CompanyID")%>','<%=orders[a].getString("customerId")%>','<%=orders[a].getString("AccountID")%>')" >Packing List</a>
						    </div>
						    <div style="font-size:12px; color:#666">&nbsp;</div>
					    </td>
					    <td style="border-left:1px solid #dddddd;border-bottom:0px solid #dddddd" align="left">
					    	<table>
								<tr><td style="font-size:12px;border-bottom:0px solid #dddddd;color:#666">&nbsp;</td></tr>
								<tr><td style="font-size:12px;border-bottom:0px solid #dddddd;color:#666">&nbsp;</td></tr>
								<tr><td style="font-size:12px;color:#666">&nbsp;</td></tr>
							</table>
					    </td>
					  </tr>
					</table>
			    </td>			
			  </tr>
		  	  <%} %>
		  <%} %>
		</table>
    </div>	
    <div id="hiddenPackingList" style="display:none">
    
    </div>
</body>
</html>






