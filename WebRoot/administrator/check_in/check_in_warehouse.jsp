<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.cwc.app.key.OccupyTypeKey"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey" />
<jsp:useBean id="statusKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" />
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!--数据处理  -->
<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<!-- 在线阅读 -->
<script type="text/javascript"
	src="../js/office_file_online/officeFileOnline.js"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript'
	src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript'
	src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.autocomplete.css" />

<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes/icon.css" />
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet" />
<script src="<%=ConfigBean.getStringValue("systenFolder")%>/administrator/check_in/json/group.js" type="text/javascript"></script>

<%
	long infoId=StringUtil.getLong(request,"infoId");
    long selectid=StringUtil.getLong(request,"selected");
	 DBRow row=checkInMgrZwb.findMainById(infoId);
	 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	 long ps_id = adminLoggerBean.getPs_id() ;
	
	DBRow[] equipments = checkInMgrZr.getEntryTaskEquipment(infoId);
// 	System.out.println(StringUtil.convertDBRowsToJsonString(equipments));
%>

<style type="text/css">
html, body {
	margin: 0px;
	padding: 0px;
}

body, td, th, pre, code, select, option, input, textarea {
	font-family: "Trebuchet MS", Sans-serif;
	font-size: 10pt;
}

.demo {
	float: left;
	margin: 0px;
	border: 0px solid gray;
	font-family: Verdana;
	font-size: 12px;
	background: white;
	width: 80%;
}
.taskinfo{
   padding:2 0 0 0;
   margin: 0 0 0 10;
   height: 25px;
   line-height: 20px;
   font-size:13px;
}
</style>
<title>Check in</title>
</head>
<body style="padding:0px">
	<div id="tab" style="margin-top: 0px; background: #eeeeee; padding: 0px; -webkit-border-radius: 7px; -moz-border-radius: 7px; border: 0px solid #dddddd">
		<!--选项卡  -->
		<div id="tabs" style="border: 0 solid #aaaaaa; padding: 0;">
			<ul >
				<%if(equipments!=null&&equipments.length>0){ 
	    		    for(int i=0;i<equipments.length;i++){%>
						<li ><a style="font-size:12px;font-weight:bold" href="#<%=equipments[i].get("equipment_id")%>"><%=equipments[i].get("equipment_type_value") %> : <%=equipments[i].get("equipment_number") %></a></li>
				<% }
	    		 }%>
			</ul>
           <fieldset  style="border:0px #cccccc solid;padding:0px;margin:0px;width:100%; overflow: auto;height:90%">
			<div style="background-color: white; border: 0px solid #dddddd;width: 100%" >
				<%if(equipments!=null&&equipments.length>0){ 
	    		             for(int p=0;p<equipments.length;p++){
							//改为直接调用 androidWareHouseData获取所有的信息
							  DBRow doorRows = checkInMgrZr.assginTaskDetaillByEntryAndEquipment (infoId, equipments[p].get("equipment_id",Long.MAX_VALUE),true) ;
// 							  System.out.println(StringUtil.convertDBRowsToJsonString(doorRows));
							 //得到tree树
						     DBRow[] tree_rows=(DBRow[])doorRows.get("tree"); 
// 						     System.out.println(StringUtil.convertDBRowsToJsonString(tree_rows));
						     //开关
						     Boolean istree=tree_rows==null?false:tree_rows.length>0; 
// 						    //得到schedule
// 						     DBRow[] schedule_rows=(DBRow[])doorRows.get("schedules"); 
// 						     System.out.println(StringUtil.convertDBRowArrayToJsonString(schedule_rows));
// 						     //开关
// 						     Boolean isschedule=schedule_rows==null?false:schedule_rows.length>0; 
							 %>
				<!--tree-->			 
				<div class="demo" id="<%=equipments[p].get("equipment_id")%>"  style="padding:0px;width:100%">			 
						<!--door  -->
						   <%if(istree){ %>
							<div id="<%="tree"+equipments[p].get("equipment_id")%>" "style="margin-right:0px;width:100%;float:left">
									<%for(int i=0;i<tree_rows.length;i++){ 
									 int resource_type=tree_rows[i].get("resources_type",0);
									 String type="NULL";
									 String value=tree_rows[i].get("resources_type_value","NULL");
									 if(OccupyTypeKey.DOOR==resource_type){
										 type="DOOR";
									 }else if(OccupyTypeKey.SPOT==resource_type){
										 type="SPOT";
									 }
									 //判断一个door 或者 spot 下面所有任务是否全部Closed(有点浪费)
									 DBRow[] lotRow=(DBRow[])((DBRow)tree_rows[i]).get("load_list");
									 boolean isAllClosed=true;
									 for(int a=0;a<lotRow.length;a++){
										 if(!lotRow[a].get("id","").equals("")){
											 String status=statusKey.getContainerTypeKeyValue(lotRow[a].getString("number_status")); 
											 if(!status.trim().equals(statusKey.getContainerTypeKeyValue(3))){//3表示Closed
												 isAllClosed=false;break;
											 }
										 }
									 }
									 
									 %>
									 <div style="padding-top:10px;border-bottom:1px solid #cccccc;padding-left:15px" class="door">
									  <!--door or spot  -->
									    <%if(!isAllClosed){ %>
									    <input type="checkbox" class="checkall" target="<%="userName"+equipments[p].get("equipment_id")+i+i%>" />
									    <%} else{%>
									     <input type="checkbox" style="visibility: hidden" />
									    <%} %>
									    <span   style="font-weight: bold;font-size:16px; color: green;text-decoration:none"><%=type %>:<%=value %></span>
									  <!--tasks  -->
									  <div >
									    <ul style="list-style-type: none;margin: 0px;padding-left: 15">
											<%if(lotRow!=null && lotRow.length>0){ 
											  for(int a=0;a<lotRow.length;a++){
										      if(!lotRow[a].get("id","").equals("")){
										    	  
										    	  %>
											<li style="line-height: 4em;">
											    <div style="padding:0px;margin:0px;width:100%;clear: both">
												   <div style="line-height: 25px;width: 45%;float: left;height:40px;padding:15 0 0 25;">
                                                           <!--加个判断如果是closed 就不显示复选框  -->
                                                           <% 
                                                           String status=statusKey.getContainerTypeKeyValue(lotRow[a].getString("number_status"));
                                                           if(!status.trim().equals(statusKey.getContainerTypeKeyValue(3))){ %>
                                                          <input type="checkbox" class="<%="userName"+equipments[p].get("equipment_id")+i+i%>" id="<%=lotRow[a].getString("dlo_detail_id")%>" isupdate="<%=lotRow[a].getString("execute_user")==""?"0":"1" %>" equipment_id="<%=lotRow[a].getString("equipment_id")%>" />
                                                           <%} else{%>
									                       <input type="checkbox" style="visibility: hidden" />
									                              <%} %>
                                                          <span style="font-size:14px;"><%=moduleKey.getModuleName(lotRow[a].get("number_type",0))%>:<%=lotRow[a].get("id","")%></span>
                                                   </div>
                                                   <!--content  -->
                                                    <div style="float:left;width:1%;font-size: 25px;opacity:0.1;">[</div>
                                                    <div style="margin: 0;padding: 0;width: 49%;float:left;height:50px;">
                                                     <div class="taskinfo"><%=lotRow[a].get("execute_user") %></div>
                                                     <div class="taskinfo" style="color:#94A25E"><%=status %></div>
                                                    </div>
												</div>				
											</li>
											<%}}}%>
										</ul>
									   </div>
									   <!--button   -->
									   <% if(!isAllClosed){%>
									   <div style="margin:70 0 5 0;padding-left:10px;text-align: left" >
									      <span style="font-weight: bold;">Operator:</span><input class="operators" type="text" size="16" title="" onclick="tongzhi('<%="userName"+equipments[p].get("equipment_id")+i%>')" id="<%="userName"+equipments[p].get("equipment_id")+i%>"/>
									                            <input type="hidden" value="" id="ids" /> 
									                            <input class="short-button" type="button" onclick="addTz('<%="userName"+equipments[p].get("equipment_id")+i%>')" value="Assign">
									   </div>
									   <%}else{ %>
									     <div style="margin:70 0 5 0;padding-left:10px;text-align: left" >
									     </div>
									   <%} %>
									  </div> 	
									<%}%>
								</div>
								<%}%>
				        </div>
				<%}} %>
			 </div>
			</fieldset>
		</div>
	</div>


</body>
</html>
 <script type="text/javascript" class="source">
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '120px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1500,
	 showOverlay: true
	};
	//加载选项卡
    $("#tabs").tabs({
          spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
          cache:false
          /* event:"mouseover",
           fx:{
        	  opacity:0.5,
        	  height:"slideUp"
          } */
     });
    $("#tabs").tabs("select",<%=selectid%>);
   /*  $(".checkall").live("click",function(){
		var _this = $(this);
		var target = _this.attr("target");
		if(_this.attr("checked")=="checked"){
			$("."+target).attr("checked","checked");
		}else{
			$("."+target).attr("checked",false);
		}
	}); */
    $("input[type='checkbox']").live("click",function(evet){
    	var _this = $(this);
    	if(_this.attr("target")){
    		var target = _this.attr("target");
    		if(_this.attr("checked")=="checked"){
    			$("."+target).attr("checked","checked");
    		}else{
    			$("."+target).attr("checked",false);
    		}
    	}else{   //子选项
    		var checkboxs=$("."+_this.attr("class"));
    	    var flag=true;
    	    for(var index = 0 , count = checkboxs.length ; index < count ; index++ ){
				if(!$(checkboxs.get(index)).attr("checked")){
					flag = false ;
					break ;
				}
			};
			var target=$("input[target='"+_this.attr("class")+"']");
    	    target.attr("checked",flag?"checked":false);
    	}
    });
	$(".door").mouseover(function(){
		$(this).css("background","#eeeeee");
	}).mouseleave(function(){
		$(this).css("background","white");
	});
})();
function myFormSubmit(){   
		var eqids=$(".demo");
		var selected=$("#tabs").tabs("option","selected");
		var oldurl=location.href;
		try{
			if(oldurl.indexOf("&selected=", 0)==-1){
				oldurl=oldurl+"&selected="+selected;
			}else{
				var temp=oldurl.substring(0,oldurl.indexOf("&selected=",0));
				oldurl=temp+"&selected="+selected;
			}
		}catch(e){
			oldurl=location.href;
		}
		location.replace(oldurl);
}

function tongzhi(inputid){
		 var ids=$('#ids').val();
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/check_in/check_in_admin_user.html"; 
		 var option = {
				 single_check:1, 					// 1表示的 单选
				 not_check_user:"",					//某些人不 会被选中的
				 proJsId:"0",						// -1全 部,1普通员工,5副主管,10主管
				 ps_id:'0',					        //所属仓库
				 user_ids:ids,                      //需要回显的UserId
				 id:inputid,
				 handle_method:'setSelectAdminUsers',
				 group :groupJson.Warehouse	
		 };
		 uri  = uri+"?"+jQuery.param(option);
		 $.artDialog.open(uri , {title: 'User List',width:'50%',height:'80%', lock: true,opacity: 0.3,fixed: true});
}

function setParentUserShow(ids,names,inputId){ //通知回显
		$('#'+inputId).val(names);
		$('#'+inputId).attr("title",names);
		$('#ids').val(ids);		
}



function addTz(searchid){      //添加通知
	    var eqids=$(".demo");
	    var selected=$("#tabs").tabs("option","selected");
	    var eqid=-1;
	    if(eqids!=null&&selected!=null&&eqids.length>0){
	    	eqid=eqids[selected].id;
	    };
		var userNameVal=$('#'+searchid).val();
	                           
		if(userNameVal==''){
			showMessage("Please choose operator","alert");
 			$.unblockUI();       //遮罩关闭
			return false;
		}
        // 查找本door or spot 下选择的任务
        var _searchid=searchid;
        var num=searchid.substr(searchid.length-1);
        var searchid=searchid+num;
        var selector="."+searchid+":checked";
        var checkedNode =$(selector);
		//alert(checkedNode.length);
		if(checkedNode.length==0){
			showMessage("Please choose order","alert");
			$.unblockUI();       //遮罩关闭
			return false;
		}
		var ids=$('#ids').val();
		var names=$('#'+_searchid).val();
		var infoId=<%=infoId%>;
		//debugger;
		var values=[];
		for(var i=0;i<checkedNode.length;i++){
			var value={};
			value.detail_id=checkedNode[i].id;
			value.isupdate=$(checkedNode[i]).attr("isupdate");
			values.push(value);
		};
		var equipment_id= checkedNode.length>0&&$(checkedNode[0]).attr("equipment_id");
		var requestdata={
				entry_id:infoId,
				equipment_id:equipment_id,
				execute_user_ids:ids,
				values:values,
		};
		if(requestdata){
			 //调通知
			 $.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCheckInWarehouseNotice.action',
				async:false,
				dataType:'json',
				data:'para='+jQuery.fn.toJSON(requestdata), 
				beforeSend:function(request){
     				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
   			},
				success:function(data){	 
					 $.unblockUI();
				},
				error:function(){
				    $.unblockUI();
				}
			});
		}
		myFormSubmit();
}


	
</script> 
