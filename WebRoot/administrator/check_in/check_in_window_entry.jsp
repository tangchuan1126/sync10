<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<jsp:useBean id="checkInEntryPriorityKey" class="com.cwc.app.key.CheckInEntryPriorityKey"/>
<html>
<head>

<%
	long entry_id = StringUtil.getLong(request, "entry_id");
	DBRow mainRow = checkInMgrZwb.selectMainByEntryId(entry_id);
	int priority = mainRow.get("priority", 0);
	long mc_dot_status = mainRow.get("mc_dot_status", 0L);
	DBRow[] files = null ;
	DBRow file = null;
	long tempItem = 0;
	
	if(entry_id > 0l){
		files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(entry_id, FileWithTypeKey.OCCUPANCY_MAIN);
		if(files.length > 0) {
			// 取第一张图片信息
			file = files[0];
		}
	}
	
	// 获取上传文件所在的路径
	String base_path = ConfigBean.getStringValue("systenFolder")  + "_fileserv/file/";
%>
<title>Check In Entry</title>
<!-- 	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" /> -->
<!-- 	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" /> -->
<!-- 	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" /> -->
<!-- 	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" /> -->
<style type="text/css">
	table#entry_info { width: 900px; height: 430px;margin: 10px auto; }
	#entry_info tr { height:30px; }
	#entry_info tr td:nth-child(1) { width: 150px; text-align: right; }
	#entry_info tr td:nth-child(2) { width: 440px; text-align: left; }
	#entry_info tr td input { width: 230px; text-transform:uppercase;}
	#entry_info tr:nth-child(1) td:nth-child(3) {
		width: 420px;
		height: 420px;
		text-align: center;
		vertical-align:top;
	}
	#entry_info tr:nth-child(10) td:nth-child(1) input { 
		width: 90px;
		margin-right: 20px; 
		text-transform:none;
	}
	span.temp {
		margin-left: 5px;
		font-style: italic;
		font-weight: bold;
		color: #FF6666;
	}
	
	#use {
		margin-left: 10px;
	}
	
	#dialog-choose {
		display: none;
		font-size: 1.1em;
	}
	
	#dialog-confirm {
		display: none;
		font-size: 1.1em;
	}
	
	/* 重设 buttons 样式*/
	.buttons_padding {
		padding-top: 0.2em !important;
		padding-bottom: 0.2em !important;
	}
	
	#check_in {
		margin-left: 270px;
	}
	
	.change_position {
		background-position: center right;
	}
	
	/* 插入物 */
	span.insertion {
		display: block;
		text-align: center;
		font-weight: bold;
	}
	
	#buttons {
/* 		border: 2px #dddddd solid; */
/* 		background:#eeeeee; */
		
	}
	
	
</style>
<!-- 	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script> -->
<!-- 	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script> -->
<!-- 	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script> -->
<!-- 	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script> -->
<!-- 	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script> -->
<!-- 	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script> -->
<!-- 	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script> -->
</head>
<body>
<table id="entry_info">
	<tr>
		<td>
			Entry ID :
		</td>
		<td>
			<%=entry_id %>
		</td>
		<td rowspan="10">
			<img alt="" src='<%=file == null ? "" : base_path + file.getString("file_id") %>' style="width: 370px; height: 280px;"
			onload="scaleImage(this, 370, 370)"  onclick="showPictrueOnline('<%=FileWithTypeKey.OCCUPANCY_MAIN%>', '<%=entry_id%>', '<%=file == null ? "" : file.getString("file_name") %>');">
			<%  if(file == null) 
				{
			%>
				<span class="insertion">Please upload driver photo.</span>
			<%
				}
			%>
		</td>
	</tr>
	<tr>
		<td>
			MC/DOT :
		</td>
		<td>
			 <input value="<%=mainRow.getString("mc_dot") %>" id="mc_dot" name="mc_dot" onBlur="autoMC();" onkeypress="autoMCEnter(this, event);" />
		</td>
	</tr>
	<tr>
		<td>
			Carrier :
		</td>
		<td>
			<input value="<%=mainRow.getString("company_name") %>" id="company_name" name="company_name"  onBlur="autoCM();" onkeypress="autoCMEnter(this, event)" /> *
		</td>
	</tr>
	<tr>
		<td>
			GPS :
		</td>
		<td>
			<input value="<%=mainRow.getString("imei") %>" id="gps_tracker" name="gps_tracker" />
			<input type="hidden" value="<%=mainRow.getString("gps_tracker") %>" id="gps_tracker_id" name="gps_tracker_id" />
			<input type="hidden" value="0" id="mark" name="mark" />
		</td>
	</tr>
	<tr>
		<td>
			Driver License :
		</td>
		<td>
			<input value="<%=mainRow.getString("gate_driver_liscense") %>" id="driver_liscense" name="driver_liscense" onBlur="autoDriverName(this);" /> *
		</td>
	</tr>
	<tr>
		<td>
			Driver Name :
		</td>
		<td>
			<input  id="driver_name" name="driver_name" value="<%=mainRow.getString("gate_driver_name") %>" /> *
		</td>
	</tr>
	<tr>
		<td>
			Priority :
		</td>
		<td>
			<select id="priority">
				<option value="0" >NA</option>
				<option <%= priority==checkInEntryPriorityKey.LOW?"selected":""%> value="<%=checkInEntryPriorityKey.LOW%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.LOW)%></option>';
    	  		<option <%= priority==checkInEntryPriorityKey.MIDDLE?"selected":""%> value="<%=checkInEntryPriorityKey.MIDDLE%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.MIDDLE)%></option>';
    	   		<option <%= priority==checkInEntryPriorityKey.HIGH?"selected":""%> value="<%=checkInEntryPriorityKey.HIGH%>"><%=checkInEntryPriorityKey.getCheckInEntryPriorityKey(checkInEntryPriorityKey.HIGH)%></option>';
    		</select>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="3">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td id="buttons" colspan="3">
			<!-- <input class="normal-green change_position" id="check_in" type="button" onclick="submit();" value="Check In"> -->
			<input id="closeNotice" class="normal-green-long" type="button" onclick="closeNotice();" value="Close Notice" >
			<input id="nextStep" class="normal-green" type="button" onclick="tabSelect(1)" value="Next Step">
		</td>
	</tr>
</table>

<input type="hidden" name="dataval" id="dataval" />

<div id="dialog-choose" title="Update by mc_dot or carrier?">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Mc_dot  or carrie both have a record, you want to update by mc_dot  or carrier?</p>
</div>

<div id="dialog-confirm" title="Confirm?">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you Sure?</p>
</div>


<script type="text/javascript">

var updateDialog = null;
// 判断getUpdateDialog 中是否进行了操作，如果没有，直接提交表单中的数据，如果有，按getUpdateDialog对应的操作执行
var isOperation = false;

$(function() {
	addAutoComplete($("#company_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInDBCarrierJSONAction.action",
			"carrier","carrier");
	// minLength：设置触发 autoComplete 最小长度
	$("#company_name").autocomplete({minLength: 3});
	addAutoComplete($("#mc_dot"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInDBMcdotJSONAction.action",
			"mc_dot","mc_dot");	
	$("#mc_dot").autocomplete({minLength: 3});
	
	completeSelect($("#company_name"), autoCM);

	completeSelect($("#mc_dot"), autoMC);
	clickAutoComplete($("#mc_dot"));
});

function completeSelect(jqObj, func) {
	jqObj.autocomplete({ 

		select: function (event, ui) {
			
			var arg = ui.item.value;
			func(arg);

		}

	});	
}

//当点击文本框时触发事件
//$("#Both input[id='ctnr']")
function clickAutoComplete(jqObj) {
	jqObj.bind('click',function() {
		var val = $.trim($(this).val());
		if(val != ""){
			jqObj.autocomplete( "search" );
		}
	});

}
// 输入 carrier 后，如果存在 mc/dot，则为其赋值
function autoCM(r) {
	var carrier_page;
	
	
	if(r !== undefined && typeof r !== "object") {
		carrier_page = r;
	} else {
		carrier_page = $.trim($("#company_name").val());
	}
	var mc_dot_page = $.trim($("#mc_dot").val());
	if(carrier_page != undefined && carrier_page != '') {
		if(mc_dot_page != undefined) {
			$.ajax({
		  		type : 'post',
		  		url : '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindMcDotByCompanyNameAction.action',
		  		dataType : 'json',
		  		data : "carrierName=" + carrier_page,
		  		success : function (data) {
					if(data != null && data != undefined && data != '') {
						if(data.mc_dot != undefined) {
		 					$("#mc_dot").val(data.mc_dot);
						}
	 				}
		  		}
		    });
		}
	}
}
//提供回车事件支持
function autoCMEnter(field, event) {
	var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	
	if(keyCode == 13) {
		var i;
		var arr = $('input');
		for (i = 0; i < arr.length; i++)
			if (field == arr[i]) 
				break;
		i = (i + 1) % arr.length;
		arr[i].focus();
	}
	

}

// 输入 carrier 后，如果存在 mc/dot，则为其赋值
function autoMC(r) {
	var mc_dot_page;
	if(r !== undefined && typeof r !== "object") {
		mc_dot_page = r;
	} else {
		mc_dot_page = $.trim($("#mc_dot").val());
	}
	var carrier_page = $.trim($("#company_name").val());
	if(mc_dot_page != undefined && mc_dot_page != '') {
		if(carrier_page != undefined) {
			$.ajax({
		  		type : 'post',
		  		url : '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindCompanyNameByMcDotAction.action',
		  		dataType : 'json',
		  		data : "mc_dot=" + mc_dot_page,
		  		success : function (data) {
					if(data != null && data != undefined && data != '') {
						if(data.carrier != undefined) {
		 					$("#company_name").val(data.carrier.toUpperCase());
						}
	 				}
		  		}
		    });
		}
	}
}

//提供回车事件支持
function autoMCEnter(field, event) {
	var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	
	if(keyCode == 13) {
		var i;
		var arr = $('input');
		for (i = 0; i < arr.length; i++)
			if (field == arr[i]) 
				break;
		i = (i + 1) % arr.length;
		arr[i].focus();
	}
	

}
// 输入 licenseNo 后，如果存在 driver name，则为其赋值
function autoDriverName(r) {
	var licenseNo = $.trim(r.value).toUpperCase();
	AjaxFindNameByLicenseAction(licenseNo);

}

// 根据传入的 licenseNo，如果符合条件自动填写对应的 driver name
function AjaxFindNameByLicenseAction(licenseNo){
	if(licenseNo != undefined && licenseNo != '') {
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindNameByLicenseAction.action',
	  		dataType:'json',
	  		data:"licenseNo=" + licenseNo,
	  		success:function(data){
				if(data != null && data != '' && data != undefined){
					if (data.gate_driver_name != undefined) {
						var name = data.gate_driver_name.toUpperCase();
						$("#driver_name").val(name);
					}
 				}
	  		}
	    });
	}
}

// 验证司机信息
function checkDriverInfo() {
	// 获取页面数据
	var main_id = <%=entry_id %>;
	var mc_dot_page = $.trim($("#mc_dot").val());
	var carrier_page = $.trim($("#company_name").val()).toUpperCase();
	var gps = $.trim($("#gps_tracker_id").val());
	var gps_name = $.trim($("#gps_tracker").val());
	var driver_liscense = $.trim($("#driver_liscense").val());
	var driver_name = $.trim($("#driver_name").val());
	var priority = $("#priority").val();
	//console.log(gps+","+gps_name);
	// 验证
	if(carrier_page == "") {
		alert("Please enter Carrier");
		$("#company_name").focus();
		
		return false;
	}
	
	if(driver_liscense == "") {
		alert("Please enter Driver License");
		$("#driver_liscense").focus();
		
		return false;
	}
	if(driver_name == "") {
		alert("Please enter Driver Name");
		$("#driver_name").focus();
		
		return false;
	}	
	
	
	
	var dataval_match = "mc_dot=" + mc_dot_page + "&carrierName=" + carrier_page + "&main_id=" + main_id;
	
	
	var urlstring = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/GateCheckInUpdateAction.action';
	var dataval = "" + "main_id=" + main_id + "&gps=" + gps +"&gps_name="+gps_name+ "&driver_liscense=" + driver_liscense
	+ "&driver_name=" + driver_name + "&priority=" + priority+"&mark="+$("#mark").val();
	
	// 将 dataval 存放到 #dataval 元素的 info 属性中 
	$("#dataval").attr("info", dataval);
	// 如果 mc_dot 不为空，则做  mc_dot 和 carrier 之间的验证
	if(mc_dot_page !== undefined && mc_dot_page != "") {
		$.ajax({
	  		type : 'post',
	  		url : '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxMatchNameAndMcdotAction.action',
	  		dataType : 'json',
	  		data : dataval_match,
	  		success : function (data) {
	  
				var result = data.result;
				if(result == "complete_matching") {
					
					dataval += "&mc_dot=" + mc_dot_page + "&carrierName=" + carrier_page;
					autoajax(urlstring, dataval);				
				} else if(result == "not_exactly_match") {
					
					var sys_carrier = data.rows_mc_dot[0].carrier;
					var sys_mc_dot = data.rows_carrier[0].mc_dot;
				    $( "#dialog-choose" ).dialog({
				        resizable: false,
				        width: 640,
				        height:180,
				        modal: true,
				        buttons: {
				        	"Update by mc_dot": function() {
					 			dataval += "&mc_dot=" + mc_dot_page + "&carrierName=" + sys_carrier;
								autoajax(urlstring, dataval);					 			
								$( this ).dialog( "close" );
				          	},
				          	"Update by carrier": function() {
								dataval += "&mc_dot=" + sys_mc_dot + "&carrierName=" + carrier_page;
								autoajax(urlstring, dataval);		
								$( this ).dialog( "close" );
				          	},
				          	Cancel: function() {
				            	$( this ).dialog( "close" );
				          }
				        }
					});				
				} else if(result == "updateByMcdot") {
					var sys_carrier = data.carrier;
					
					getUpdateDialog("carrier", sys_carrier, urlstring, dataval);
				} else if(result == "updateByCarrier") {
					var sys_mc_dot = data.mc_dot;
					getUpdateDialog("mc_dot", sys_mc_dot, urlstring, dataval);
				} else if(result == "not_match_at_all") {
					getUpdateDialog("", "", urlstring, dataval, "not_match_at_all");	
				} 
	  		},		
			error: function (serverresponse, status) {
				alert("System error!");
			}
		});		
	  // 否则不做
	} else {
		dataval += dataval_match;
		autoajax(urlstring, dataval);
	}
	
	return true;
}

function submit() {
	// 如果是以点击按钮进行提交，给提交按钮添加属性 submit
	$("#check_in").attr("submit", "submit");
	checkDriverInfo();
}

// 更新数据对话框
// special：特殊情况下的 dialog
function getUpdateDialog(updateItem, sys, urlstring, dataval, special) {
	var mc_dot_page = $.trim($("#mc_dot").val());
	var carrier_page = $.trim($("#company_name").val().toUpperCase());
	var content = "";
	var page = "";
	
	if(updateItem == "mc_dot") {
		page = mc_dot_page;
	} else if(updateItem == "carrier") {
		page = carrier_page;
	}
	
	if(special == "not_match_at_all") {
		content = "<div class='check'><input id='checkIn1' name='checkIn' type='checkbox' onclick='check(this)' insert='insert' value='1' /><label id='checkIn1_label' for='checkIn1'>Carrier and Mc_dot does not exist, Using and add this record!</label></div>"
	} else {
		content = "<div class='check'><input id='checkIn1' name='checkIn' type='checkbox' onclick='check(this, &quot;" +  updateItem + "&quot;, &quot;" + sys + "&quot;)' value='1' /><label id='checkIn1_label' for='checkIn1'>Update " + updateItem + ": <strong>" + page + "</strong></label></div>"
				+ "<div class='check'><input id='checkIn2' name='checkIn' type='checkbox' onclick='check(this, &quot;" +  updateItem + "&quot;, &quot;" + sys + "&quot;)' value='2' /><label id='checkIn2_label' for='checkIn2'>System " + updateItem + ": <strong>" + sys + "</strong></label></div>";	
	}
	
	updateDialog = $.artDialog({
		id: page,
		width :  '500px',
		height :  '25%',
	    title : 'confirm',
	    lock : true,
	    opacity : 0.3,
	    fixed : true,
	    content : content,
	    close: function(event, ui) {
	    		if(!isOperation) {
					dataval += "&mc_dot=" + mc_dot_page + "&carrierName=" + carrier_page;
					$.ajax({
						url: urlstring,
						data: dataval,
						dataType: 'json',
						type: 'post',
						async: false,
						success: function (data) {
							var result = data.result; 
							if (result == "success") {
								
								if($("#check_in").attr("submit")) {
									$("#tabs").tabs("select", 0);
									loadDriverInfo();
								}				
								showMessage("Success!","alert");
							} else {
								
							}
						},
						error: function() {
							alert("System error!");
						}
					});
	    		}

	    }
	});
	
}

		
function checkOperation(obj, updateItem, sys) {
 	var operator = $(":checkbox[name=checkIn]:checked").val() * 1;
	var carrier_page = $.trim($("#company_name").val()).toUpperCase();
	var mc_dot_page = $.trim($("#mc_dot").val()).toUpperCase();
	var urlstring = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/GateCheckInUpdateAction.action';
	var dataval = $("#dataval").attr("info");
	if (operator == 1) {
 		var insert = $(obj).attr("insert");
 		if(insert) {
			$.ajax({
		  		type : 'post',
		  		url : '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxAddCarrierInfoAction.action',
		  		dataType : 'json',
		  		data : "mc_dot=" + mc_dot_page + "&carrierName=" + carrier_page,
		  		async: false,
		  		success : function (data) {
		  			isOperation = true;
	  				var result = data.result;
	  				if(result == "success") {
	  					dataval += "&mc_dot=" + mc_dot_page + "&carrierName=" + carrier_page;
						autoajax(urlstring, dataval);	
	  				} else {
	  					alert("System error!");
	  					
	  				}
		  		} 		
			 });			
 		} else {
 			if(updateItem == "carrier") {
	 			$.ajax({
		    	    url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxModCarrierAction.action',
		    	    dataType : 'json',
		    	    data : "mc_dot=" + mc_dot_page + "&carrierName=" + carrier_page,
		    	    async: false,
		    	    success: function (data) {
		    	    	isOperation = true;
	    	    		var result = data.result;
	    	    		if(result == "success") {
	    	    			dataval += "&mc_dot=" + mc_dot_page + "&carrierName=" + carrier_page;
							autoajax(urlstring, dataval);	
	    	    		}
		    	    },
		    	    cache: false
		    	});	 				
 			} else if(updateItem == "mc_dot") {
	 			$.ajax({
		    	    url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxModMcDotAction.action',
		    	    dataType : 'json',
		    	    data : "mc_dot=" + mc_dot_page + "&carrierName=" + carrier_page,
		    	    async: false,
		    	    success: function (data) {
		    	    	isOperation = true;
	    	    		var result = data.result;
	    	    		if(result == "success") {
	    	    			dataval += "&mc_dot=" + mc_dot_page + "&carrierName=" + carrier_page;
							autoajax(urlstring, dataval);	
	    	    		}
		    	    },
		    	    cache: false
		    	}); 				
 			}
 		}
	} else if(operator == 2) {
		if(updateItem == "carrier") {
			isOperation = true;
	    	dataval += "&mc_dot=" + mc_dot_page + "&carrierName=" + sys;
			autoajax(urlstring, dataval);	
		} else if(updateItem == "mc_dot") {
			isOperation = true;
   			dataval += "&mc_dot=" + sys + "&carrierName=" + carrier_page;
			autoajax(urlstring, dataval); 				
		}		
	}
 	
	updateDialog && updateDialog.close();
}


// 复选框实现单选功能
function check(obj, updateItem, sys) {
	var checks = document.getElementsByName("checkIn");
    if(obj.checked) {
        for(var i=0; i<checks.length; i++) {
            checks[i].checked = false;
        }
        obj.checked = true;  
        checkOperation(obj, updateItem, sys);
    }
    else {
        for(var i=0; i<checks.length; i++) {
            checks[i].checked = false;
        }
    }
}

// 提交方法
function autoajax(url, data) {
	$.ajax({
		url: url,
		data: data,
		dataType: 'json',
		type: 'post',
		async: false,
		success: function (data) {
			var result = data.result; 
			if (result == "success") {
				
				if($("#check_in").attr("submit")) {
					$("#tabs").tabs("select", 0);
					loadDriverInfo();
				}				
				showMessage("Success!","alert");
			} else {
				
			}
			updateDialog && updateDialog.close();
		},
		error: function() {
			alert("System error!");
		}
	});
}


// 图片处理
function scaleImage(o, w, h){
	var img = new Image();
	img.src = o.src;
	if(img.width >0 && img.height >0)
	{
		if(img.width/img.height >= w/h)
		{
			if(img.width > w)
			{
				$(o).width(w);
				$(o).height(img.height*w/img.width);
			}
			else
			{
				$(o).width(img.width);
				$(o).height(img.height);
			}
		}
		else
		{
			if(img.height > h)
			{
				$(o).height(h);
				$(o).width(parseInt(img.width / img.height * h));
			}
			else
			{
				$(o).width(img.width);
				$(o).height(img.height);
			}
		}
	}
}
//图片在线显示  		 
function showPictrueOnline(fileWithType,fileWithId ,currentName){
    var obj = {
  		file_with_type:fileWithType,
  		file_with_id : fileWithId,
  		current_name : currentName ,
  		cmd:"multiFile",
  		table:'file',
  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "_fileserv/file/"
	}
	openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
}
$("#gps_tracker").blur(function(){
	 if( $("#gps_tracker").val()!=""){
		 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindAssetByGPSNumberAction.action',
			data:'gps_tracker='+$("#gps_tracker").val(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				if(!data.imei){
					findGPSTip();
					$("#mark").val(1);
				}else{
					$("#mark").val(0);
					$("#gps_tracker_id").val(data.id);
				}
				
			},
			error:function(){
				showMessage("System error!","error"); 
			}
		 });
	 }
});
function findGPSTip(){
	$.artDialog({
	    content: 'GPS not found!do you want to add a new one?',
	    icon: 'question',
	    lock:true,
	    opacity: 0.3,
	    width: 230,
	    height: 70,
	    title:'Reminder',
	    okVal: 'Yes',
	    ok: function () {
	    	
	    },
	    cancelVal: 'No',
	    cancel: function(){
	    	$("#gps_tracker").val("");
	    	$("#gps_tracker").focus();
		}
	});
}
function closeNoticeOper(){
	var entryId = <%=entry_id%>;
	$.ajax( {
  		type: 'post',
  		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCloseNoticeByEidAction.action',
  		dataType: 'json',
  		data: 'entry_id=' + entryId,
		error: function( serverresponse, status ) {
  			showMessage( "System error", "alert" );
  		},
	 	success: function( data ) {
	 		if( data != null && data.result ) {
	 			if ( data.result == "success" ) {
	 				showMessage( "Close Window Schedules Success!", "alert" );
	 			} else if ( data.result == "allClose" ) {
	 				showMessage( "All the window schedule had closed!", "alert" );
	 			}
	 			
	 			setTimeout( function() {
	 				window.parent.location.reload();
	 			}, 1000 ); 
	 		}
	 	}
	} );
}
//关闭 window schedule
function closeNotice() {
	$.artDialog({
	    content: '<span style="font-size:14px;">Are you sure close the window schedules?</span>',
	    icon: 'warning',
	    width: 350,
	    height: 70,
	    title:'',
	    okVal: 'Confirm',
	    ok: function () {
	    	closeNoticeOper();
	    },
	    cancelVal: 'Cancel',
	    cancel: function(){
	    	
		}
	});	
}
</script>
</body>
</html>

