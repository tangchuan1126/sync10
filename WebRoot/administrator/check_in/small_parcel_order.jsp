<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="reasonKey" class="com.cwc.app.key.LoadConsolidateReasonKey"/> 


<%
	long detailId = StringUtil.getLong(request, "dlo_detail_id");
	DBRow data = checkInMgrWfh.findLoadSmallParcelOrderByDetailId(detailId); // loadPaperWorkZr.getPalletInfoByDetailId(detailId);
	int is_order = data.get("is_order", 0);
	DBRow[] datas =(DBRow[]) data.get("data", new DBRow[0]);
%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<head>
	
	
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../js/accordion/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/accordion/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="../js/accordion/jquery-ui.theme.min.css" />
	<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
	div.orders{padding:0px;margin: :0px;}
	div.pallets table td{border:1px solid silver;}
	span.order_info{font-size: 14px;font-weight: bold;}
	.single{}
	.double{}
	.tn_line_info{
	   height: 32px;
	   line-height: 29px;
	   padding: 3px;
	   border-top: 1px dashed #aaa;
	}
	.tn_info{
		display: inline-block;
   		font-weight: bold;
    	margin-right: 20px;
	}
	td.header{width: 25%;text-align: center;font-size: 14px;font-weight: bold;background: black;color: white;line-height: 25px;height: 25px;}
	</style>
	<script type="text/javascript">
	$(function() {
		$("#load").accordion({
			heightStyle: "content"
		});
		
		$(".orderInfo").click(function(){
			 $(this).next(".pallets").slideToggle("slow");
		})
	});
	
	</script>
</head>
<body style="font-size:12px">
	<div style="font-size:20px ;text-align: center;border: 1px solid silver;padding: 5px;">
		<B><%=moduleKey.getModuleName(data.get("number_type",0)) %>:<%=data.getString("number") %> </B>
	</div>
				<div id="load">
				<% 
						if(datas.length > 0 ){
					%>
							<h3><%= is_order==1?"Orders: "+datas.length:"&nbsp;"%> </h3>
							<div class="orders" style="padding: 0px;padding: 10px;padding-top: 0px;">
						<%
							if(is_order == 1){
								for(DBRow order : datas){
									DBRow[] pallets = (DBRow[]) order.get("pallets", new DBRow[0]);
									%>
									<div class="order" style="border:1px solid silver; margin-top:10px;background-color: white; padding: 4px;">
									<div class="orderInfo" style="cursor: pointer;">
										<div style="float: left;width:60% ; height: 54px;border-right:2px dashed  silver;">
											<span class="order_info">Order Number: <font color="#f60"><%=order.getString("order_numbers") %></font></span><br />
									 		<span class="order_info">Reference No: <%=order.getString("reference_no") %> </span><br />
											<span class="order_info">Customer ID:  <%=order.getString("customer_id") %> </span><br />
									 	</div>
									 	<div style="float: left;height: 54px;">
											&nbsp;&nbsp;&nbsp;<span class="order_info">TN's:  <%=pallets.length%> </span><br>
											&nbsp;&nbsp;&nbsp;<span class="order_info">Carrier ID:  <%=order.get("carrier_id", "")%> </span><br>
											&nbsp;&nbsp;&nbsp;<span class="order_info">Shipped Date:  <%=order.get("shipped_date", "")%> </span>
									 	</div>
									 	<div style="clear: both; "></div>
									 </div>
									 		<div class="pallets" style="margin: 4px;">
									 			
							 				 	<table  style="border-collapse :collapse;border:1px solid silver;width: 100%;">
							 				 		<thead>
							 				 			<td class="header" >Scan Tracking No.</td>
							 				 			<td class="header" >Scan Time</td>
							 				 			<td class="header" >Scan User</td>
							 				 		</thead>
								
									 		<% 
									 			if(pallets.length > 0 ){
									 				for(int pindex = 0 , pcount = pallets.length ; pindex < pcount ; pindex++){
									 					DBRow pallet = pallets[pindex];
									 		//			if(pallet.get("is_scanned", 0)==1){
									 		//				continue;
									 		//			}
									 					String classType = pindex % 2 == 0 ? "single":"double"; 
									 					String palletNumber =  pallet.getString("wms_pallet_number");
									 					if(pallet.get("wms_scan_number_type", 0) == 3){
									 						continue ;
									 					}
									 					%>
									 						 <tr class="<%=classType  %>">
									 						 	<td style="text-align: center;font-size: 14px"> <%= palletNumber %> </td>
									 						 	<td style="text-align: center;font-size: 14px"> <%=pallet.getString("scan_time") %></td>
									 						 	<td style="text-align: center;font-size: 14px"> <%=pallet.getString("scan_name") %></td>
									 						 </tr>
 									 					<% 	
									 				}
									 				 
									 			}else{
									 				%>
									 				<tr>
									 					<td style="height: 30px;line-height: 20px;text-align: center;" colspan="3">No Records</td>
									 				</tr>
									 				<%
									 			}
									 			
									 		%>
								 				</table>
									 		</div>	
									</div>
							
									<% 
								}
							}else{%>
								<table border="1" borderColor=”#111111″ cellSpacing=”0″ cellPadding=”2″ style="border-collapse :collapse;border:1px solid silver;width: 100%;margin-top: 10px;">
		 				 		<thead>
		 				 			<td class="header" >Scan Tracking No.</td>
		 				 			<td class="header" >Scan Time</td>
		 				 			<td class="header" >Scan User</td>
		 				 		</thead>
				 		<% 
				 			
				 			if(datas.length > 0 ){
				 				for(int pindex = 0 , pcount = datas.length ; pindex < pcount ; pindex++){
				 					DBRow pallet = datas[pindex];
				 					String classType = pindex % 2 == 0 ? "single":"double"; 
				 					String palletNumber =  pallet.getString("wms_pallet_number");
				 					if(pallet.get("wms_scan_number_type", 0) != 3){
				 						continue ;
				 					}
				 					%>
				 						 <tr class="<%=classType  %>">
				 						 	<td style="text-align: center;font-size: 14px"> <%= palletNumber %> </td>
				 						 	<td style="text-align: center;font-size: 14px"> <%=pallet.getString("scan_time") %></td>
				 						 	<td style="text-align: center;font-size: 14px"> <%=pallet.getString("scan_name") %></td>
				 						 </tr>
					 					<% 	
				 				}
				 				 
				 			}else{
				 				%>
				 				<tr>
				 					<td style="height: 30px;line-height: 20px;text-align: center;" colspan="3">No Records</td>
				 				</tr>
				 			<%
				 			}
				 			
				 			%>
			 				</table>
			 			<%
						}
						}
						%>
								</div>
					
		</div>
		
	
</body>