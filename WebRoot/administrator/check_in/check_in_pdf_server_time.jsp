<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@ include file="../../include.jsp"%>
<html>
<head>
<%
// 	String	nowDate = DateUtil.getStrCurrYear()+"-"+(Integer.parseInt(DateUtil.getStrCurrMonth())+1)+"-"+DateUtil.getStrCurrDay();
%>

<!--  基本样式和javascript -->
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
 
<!-- <script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
table 斑马线
<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css" />
引入Art
<link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>

遮罩
<script type="text/javascript" src="js/blockui/jquery.blockUI.233.js"></script> -->

<script type="text/javascript" src="js/showMessage/showMessage.js"></script>

<!-- <script type='text/javascript' src='../dwr/engine.js'></script> -->
<!-- 时间控件 -->
<link type="text/css" href="comm.css" rel="stylesheet" />
<script type="text/javascript" src="js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
 
<script type="text/javascript" src="js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
 
<link type="text/css" href="js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<link type="text/css" href="js/fullcalendar/jquery.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="js/fullcalendar/jquery.multiselect.filter.css" rel="stylesheet" />
<script type="text/javascript" src="js/fullcalendar/jquery.multiselect.min.js"></script>	 
<script type="text/javascript" src="js/fullcalendar/jquery.multiselect.filter.min.js"></script>	
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
<script type="text/javascript">
// dwr.engine._errorHandler = function(message, ex) {};  
</script>
<%
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 long psId = StringUtil.getLong(request, "ps_id");//当前账号所属仓库id
 if(psId==0l){
	psId=adminLoggerBean.getPs_id();
 }
 DBRow[] ps = checkInMgrZwb.findSelfStorage();//查询仓库
%>
<title>Creat PDF By Time</title>
</head>
<!-- <body onLoad="dwr.engine.setActiveReverseAjax(true);"> -->
<body>
	<div style="width:98%;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:center;">
		<h1>PDF SERVER</h1>
		
	</div>
	<div style="height: 40px;">
		<div style="float: left;font-weight: bold;font-size: 15px;padding-top: 10px;">
			&nbsp;&nbsp;&nbsp;1、MasterBol &nbsp;&nbsp;&nbsp;2、Bol&nbsp;&nbsp;&nbsp; 3、Counting Sheet
		</div>
		<div align="center" style="font-size: 15px;padding-top: 10px;">
		    Warehouse:&nbsp;<select id="ps" name="ps" <%=adminLoggerBean.isAdministrator() ?"":"disabled"%> class="w186">
								<option value="-1">All Storage</option>
								<%
									for(int j = 0;j<ps.length;j++)
									{
								%>
									<option value="<%=ps[j].get("ps_id",0l)%>"  <%=psId==ps[j].get("ps_id",0l)?"selected":"" %>><%=ps[j].getString("ps_name")%></option>
								<%
									}
								%>
							</select>&nbsp; &nbsp;&nbsp; 
			StartTime:&nbsp;<input type="text" value="" id="startTime" size="20"/>&nbsp;&nbsp;&nbsp;
			EndTime:&nbsp;<input type="text" value="" id="endTime"size="20"/>&nbsp;&nbsp;&nbsp;
			<input id="CreatPdfByTime" type="button" name="creatPdf" value="CreatPdfByTime" onclick="CreatPDFByTime()"/>
		</div>
	</div>
	<div style="width:98%;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:center;">
		<div style="height:60px; border:0px solid red; margin:10px;background-color:#FFF">
			<!-- 正在生成pdf 的 div -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="creatingPdf">
			 
			</table>
			<!-- 结束 -->
		</div>
	</div>
	<div style="width:98%;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:center;">
		<div style="height:400px; border:0px solid red; margin:10px;background-color:#FFF;overflow:scroll">
			<!-- 处理完pdf 的 记录 -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="endPdf">
			
			</table>
			<!-- 结束 -->
		</div>
	</div>
	<div id="printHtml" style="display: none;"></div>
</body>
</html>
<script>

jQuery(function($){
// 	$("#startTime").date_input();
// 	$("#endTime").date_input();
	
	
	$("#startTime , #endTime").datetimepicker({
		showSecond: false,
		showMinute: true,
		changeMonth: true,
		changeYear: true,
		timeFormat: 'HH:mm',
		dateFormat: 'mm/dd/yy'
});
})
//手动按时间段生成pdf
function CreatPDFByTime(){
	
	var startTime=$('#startTime').val();
	var endTime=$('#endTime').val();
	var ps=$("#ps").val();
	if(startTime&&startTime!=""&&endTime&&endTime!=""&&ps&&ps!=""){
		//根据参数获取masterbol和bol签的url路径
		 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/getPrintUrlByTime.action',
			async:false,
			dataType:'json',
			data:'startTime='+startTime+'&endTime='+endTime+'&ps_id='+ps,
			success:function(result){
				if(result&&result.flag=="success"&&result.data){
					for(n=0;n<result.data.length;n++){
						var count = n;
						var data = result.data[n];
						var entry_id = data.entry_id;
						var detail_id = data.detail_id;
						//判断是否成功
						var master_flag = true;
						var bol_flag = true;
						var counting_flag = true;
						var ftp_flag = true;
						var ftp_flag = true;
						var load_no = "";
						var sign = false;//masterBol 默认不签字
						var bol_data=false;
						
					//根据参数获取masterbol和bol签的url路径
					 $.ajax({
						url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/getPrintUrlByParam.action',
									async:false,
									dataType:'json',
									data:'entry_id='+entry_id+'&detail_id='+detail_id+"&by_time=yes",
									success:function(data){
										if(data&&data.flag=="success"){
											load_no = data.number;
//					 						if(load_no!=""){
												var trId = entry_id+"_"+load_no+"_Creating";
												var creating_pdf = '<tr id="'+trId+'"><td width="40%" align="center" valign="middle" height="60px;">'
																+'<span style="font-size: 22px; font-weight:bold;" >Entry ID:'+entry_id+'</span>'
																+'</td>'
																+'<td width="30%" align="center" valign="middle">'
																+'	<span style="font-size: 22px;font-weight:bold;" >Load:'+load_no+'</span>'
																+'</td>'
																+'<td align="center" valign="middle">'
																+'	<span style="font-size: 24px;font-weight:bold; color:green" ><img src="check_in/imgs/create_pdf.gif" style="margin-left: 20px;margin-top:5px; float: left;"> </span>'
																+'</td>'
																+'</tr>';
												$("#creatingPdf").html(creating_pdf);
//					 						}
											//将masterbol的签上传pdf
											if(data.masterbolurl){
												for(i=0;i<data.masterbolurl.length;i++){
													var printUrl = data.masterbolurl[i].printurl;
//					 								console.log(printUrl);
													$.ajax({
														url:printUrl,
														async:false,
														dataType:'html',
														data:'',
														success:function(html){
									 						$('#printHtml').html(html);
									 						//masterBol是否都上传成功
									 						if(master_flag){
									 							master_flag=androidToPdf();
									 						}else{
									 							androidToPdf();
									 						}
									 						sign = true;
														},
														error:function(){
															showMessage("系统错误","error");
														}
													});
												}
											}

											//将bol的签上传pdf
											if(data.bolurl){
												for(j=0;j<data.bolurl.length;j++){
													var printUrl = data.bolurl[j].printurl;
													$.ajax({
														url:printUrl,
														async:false,
														dataType:'html',
														data:'',
														success:function(html){
									 						$('#printHtml').html(html);
									 						//Bol是否都上传成功
									 						if(bol_flag){
									 							bol_flag=androidToPdf();
//					 				 							console.log(bol_flag);
									 						}else{
									 							androidToPdf();
									 						}
									 						bol_data=true;
//					 				 						console.log(bol_flag);
														},
														error:function(){
															showMessage("系统错误","error");
														}
													});
												}
											}
										}
										//追加counting sheet图片
										if(data&&(data.masterbolurl||data.bolurl)){
											$.ajax({
												url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/addCountingSheetToPdf.action',
												async:false,
												dataType:'json',
												data:'entry_id='+entry_id+'&detail_id='+detail_id,
												success:function(data){
//					 								var result=$.parseJSON(data);
													//Bol是否都上传成功
							 						if(data.message=="false"){
							 							counting_flag=false;
							 						}
												}
											});
										}
										
									},
									error:function(e){
										showMessage("系统错误","error");
									}
								});
								
							if(load_no!=""){
								 var master="OK";
								 var bol="OK";
								 var counting="OK";
								 var master_color = "green";
								 var bol_color = "green";
								 var c_color = "green";
								if(!master_flag){
									master="Error";
									master_color = "red";
								}else if(!sign){
									master="No Signature";
								}
								
								if(!bol_flag){
									bol="Error";
									bol_color = "red";
								}
								if(!bol_data){
									bol="No Data";
								}
								if(!counting_flag){
									counting="Error";
									c_color = "red";
								}
								
								var trId = entry_id+"_"+load_no+"_End";count
								var end_pdf = '<tr id="'+trId+'">'
												+'<td valign="middle">'
												+'	<span style="font-size: 12px; font-weight:bold;" >'+count+'</span>'
												+'</td>'
												+'<td align="center" valign="middle">'
												+'	<span style="font-size: 12px; font-weight:bold;" >Entry ID:'+entry_id+'</span>'
												+'</td>'
												+'<td width="30%" align="center" valign="middle">'
												+'	<span style="font-size: 12px; font-weight:bold;" >Load:'+load_no+'</span>'
												+'</td>'
												+'<td align="center" valign="middle">'
												+'	<span style="font-size: 12px;font-weight:bold; color:red" >End</span>'
												+'</td>'
												
												+'<td align="center" valign="middle">'
												+'	<span style="font-size: 12px;font-weight:bold; color:'+master_color+'" >'+master+'</span>'
												+'</td>'
												+'<td align="center" valign="middle">'
												+'	<span style="font-size: 12px;font-weight:bold; color:'+bol_color+'" >'+bol+'</span>'
												+'</td>'
												+'<td align="center" valign="middle">'
												+'	<span style="font-size: 12px;font-weight:bold; color:'+c_color+'" >'+counting+'</span>'
												+'</td>'
												+'</tr>';
//					 			$("#endPdf").remove("#"+trId);
								$("#endPdf").append(end_pdf);
							}
							
							$("#creatingPdf").html("");
						
									
					}
				}
							
			},
			error:function(e){
				showMessage("系统错误","error");
			}
		});
				
			
	}
			
}



</script>
