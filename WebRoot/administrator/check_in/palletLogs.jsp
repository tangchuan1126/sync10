<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="palletInventoryKey" class="com.cwc.app.key.PalletInventoryKey"/>
<%
	long ps_id = StringUtil.getLong(request,"ps_id");
	String palletType = StringUtil.getString(request,"palletType");
	String startTime=StringUtil.getString(request, "start_time");
	String endTime=StringUtil.getString(request, "end_time");
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(10);
	
	DBRow[] ps = checkInMgrZwb.findSelfStorage();	
	
	DBRow[] data = checkInMgrWfh.findPalletLog(startTime,endTime,palletType,ps_id,pc);
%>
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- 时间 -->
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
	<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	
	<!-- 带搜索功能的下拉框、 -->
	<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
	<script src="../b2b_order/chosen.jquery.min.js" type="text/javascript"></script> 
		
</head>
<script type="text/javascript">
$(function(){
	     
 	$('#start_time').datetimepicker({
 		dateFormat: "yy-mm-dd"
 		
 	});    
 	$('#end_time').datetimepicker({
 		dateFormat: "yy-mm-dd"
 		
 	});    
	$("#ui-datepicker-div").css("display","none");
});

function searchPalletLog(){
	var ps_id = $("#whse").val();
	var palletType = $("#palletType").val();
	var start_time = $("#start_time").val();
	var end_time = $("#end_time").val();
	document.dataForm.ps_id.value = ps_id;
	document.dataForm.palletType.value = palletType;
	document.dataForm.start_time.value = start_time;
	document.dataForm.end_time.value = end_time;
	document.dataForm.submit();
}
</script>
<body onLoad="onLoadInitZebraTable()">
<div class="demo" style="width:100%">
	  <div id="tabs">
		<ul>
			<li><a href="#checkin_filter">Advanced Search</a></li>
		</ul>
		<div id="checkin_filter">
			<table style=" margin-left:50px;margin-bottom:19px;margin-top:19px;">
				<tr>
					<td style="font-size: 14px;">Storage Location:</td>
					<td>
					
					
					<select id="whse" name="WHSE" style="width:150px" class="chzn-select"  >
							<option value="0" >All Storage</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("ps_id",0l)%>"  <%=ps_id==ps[j].get("ps_id",0l)?"selected":"" %>> <%=ps[j].getString("ps_name")%></option>
							<%
								}
							%>
						</select>
					</td>
					<td style="font-size: 14px;width:100px" align="right">Pallet Type :</td>
					<td width="170px;">
					<select id="palletType" style="width:150px" class="chzn-select"  >
						<option value="0" >All Type</option>
						<option value="<%=palletInventoryKey.Pallet1 %>" <%=palletType.equals(""+palletInventoryKey.Pallet1)?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet1) %></option>
						<option value="<%=palletInventoryKey.Pallet2 %>" <%=palletType.equals(""+palletInventoryKey.Pallet2)?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet2) %></option>
						<option value="<%=palletInventoryKey.Pallet3 %>" <%=palletType.equals(""+palletInventoryKey.Pallet3)?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet3) %></option>
						<option value="<%=palletInventoryKey.Pallet4 %>" <%=palletType.equals(""+palletInventoryKey.Pallet4)?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet4) %></option>
						<option value="<%=palletInventoryKey.Pallet5 %>" <%=palletType.equals(""+palletInventoryKey.Pallet5)?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet5) %></option>
						<option value="<%=palletInventoryKey.Pallet6 %>" <%=palletType.equals(""+palletInventoryKey.Pallet6)?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet6) %></option>
					</select>
					</td>
					<td width="260px;" style="margin-left:20px;">
						Start Time :&nbsp;<input maxlength="100" value="<%=startTime %>" id="start_time" />
					</td>
					<td style="margin-left:20px;">
						End Time:&nbsp;<input maxlength="100" value="<%=endTime %>" id="end_time"  /> 
					</td>
					
					<td style="width:200px" align="right"><input type="button" onclick="searchPalletLog();"  class="long-button" value="Search" /></td>
				</tr>
			</table>
		</div>
       </div>
       <script>
       $("#tabs").tabs({
	   		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
	   });
	   	$("#palletType").chosen({allow_single_deselect:true,disable_search:true,no_results_text: "没有符合条件的结果!"});
	   	$("#whse").chosen({allow_single_deselect:true,disable_search:true,no_results_text: "没有符合条件的结果!"});
       </script>
       
    </div>



	  <!-- table 斑马线  -->
	  <table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"  isNeed="false" class="zebraTable mytable" isNeed="false" style="margin-left:3px;margin-top:20px;">
			  <tr> 
		        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Operator</th>
		        <th width="20%" style="vertical-align: center;text-align: center;" class="right-title">Operation Time</th>
			  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Pallet Type</th>
			  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Title</th>
			  	<th width="15%" style="vertical-align: center;text-align: center;" class="right-title">WHSE</th>
		        <th width="15%" style="vertical-align: center;text-align: center;" class="right-title">Operate</th>
		        <th width="20%" style="vertical-align: center;text-align: center;" class="right-title">Data</th>
		      </tr>
		      <%for(int i=0;i<data.length;i++){ %>
		      
	        <tr style="text-align: center;height:60px;">
	        	<td><%=data[i].get("employe_name", "") %></td>
	        	<td><%=data[i].get("post_date", "").substring(0,data[i].get("post_date","").lastIndexOf(".")) %></td>
	        	<td><%=palletInventoryKey.getPalletInventoryName(data[i].get("pallet_type", 0)) %></td>
	        	<td><%=data[i].get("title_name","") %></td>
	        	<td>
	        		<fieldset style="width:230px"><legend style="color:#777"><span style="font-size: 15px"><B><%=data[i].get("title", "") %></B></span> warehouse</legend></fieldset>
	        	</td>
	        	<td><%=data[i].get("operation", "") %></td>
	        	<td style="text-align:left;">
	        		<div style="border-bottom:1px dashed black;color:green;width:200px;padding-left:30px;"><div style="color:#000;width:100px;text-align: right;display: inline-block;"><b>PalletCount:</b></div>&nbsp;<%=data[i].get("quantity", 0F) %></div>
	        		<div style="color:red;width:200px;padding-left:30px;"><div style="color:#000;width:100px;text-align: right;display: inline-block;" ><b>DamagenCount:</b></div>&nbsp;<%=data[i].get("damaged_quantity", 0F) %></div>
	        	</td>
	        </tr> 
	        <%} %>	  
	 </table>	
	 <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" action="pallet_inventory_logs.html">
	    <input type="hidden" name="p"/>

		<input type="hidden" name="palletType" value="<%=palletType %>"/>
		<input type="hidden" name="ps_id" value="<%=ps_id %>"/>
		<input type="hidden" name="start_time" value="<%=startTime %>"/>
		<input type="hidden" name="end_time" value="<%=endTime %>"/>
	  </form>
	  <tr>
	    <td height="28" align="right" valign="middle" class="turn-page-table">
	        <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
	      Goto 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
	    </td>
	  </tr>
   </table>
   
   
</body>
