<%@page import="com.cwc.app.key.EquipmentTypeKey"%>
<%@page import="com.cwc.app.key.YesOrNotKey"%>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.OccupyTypeKey"%>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%
long entry_id=StringUtil.getLong(request,"entry_id");
DBRow row=checkInMgrZwb.findMainById(entry_id);
DBRow[] liveLoads = new DBRow[0];
DBRow[] lps = checkInMgrZyj.checkInWindowFindEquipmentByEntryInIdpurpose(0, entry_id,EquipmentTypeKey.Tractor);
DBRow[] liveCtnrs = checkInMgrZyj.checkInWindowFindEquipmentByEntryInIdpurpose(CheckInLiveLoadOrDropOffKey.LIVE, entry_id, EquipmentTypeKey.Container);
liveLoads = new DBRow[lps.length + liveCtnrs.length];
System.arraycopy(lps, 0, liveLoads, 0, lps.length);
System.arraycopy(liveCtnrs, 0, liveLoads, lps.length, liveCtnrs.length);
DBRow[] dropOffs  = checkInMgrZyj.checkInWindowFindEquipmentByEntryInIdpurpose(CheckInLiveLoadOrDropOffKey.DROP, entry_id, 0);
DBRow[] pickUps   = checkInMgrZyj.checkInWindowFindEquipmentByEntryOutIdpurpose(CheckInLiveLoadOrDropOffKey.PICK_UP, entry_id, 0);
DBRow[] tmss = checkInMgrZyj.checkInWindowFindEquipmentByEntryInIdpurpose(CheckInLiveLoadOrDropOffKey.TMS, entry_id, 0);
List<DBRow> tmsss = new ArrayList<DBRow>();
for(DBRow tms:tmss){
	if(tms.get("equipment_type", 0) == CheckInTractorOrTrailerTypeKey.TRAILER){
		tmsss.add(tms);
	}
}
tmss = tmsss.toArray(new DBRow[0]);
OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
List occupyTypeKeys = occupyTypeKey.getOccupyTypeKeys();
%>
<title>Check In CTNR</title>
<!-- <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" /> -->
<!-- <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" /> -->
<!-- <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" /> -->
<!-- <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" /> -->
<!-- <script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script> -->
<!-- <script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script> -->
<!-- <script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script> -->
<!-- <script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script> -->
<!-- <script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script> -->
<!-- <script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script> -->
<!-- <script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script> -->
<style type="text/css">

.lp_ctnr_cls{
	width:100%;
}

.lp_ctnr_cls tr td:nth-child(1){
	width:50px;
	text-align: right;
}

.lp_ctnr_cls tr td:nth-child(2){
	width:150px;
	text-align: left;
}

.lp_ctnr_cls tr td:nth-child(3){
	width:95px;
	text-align: left;
}

.lp_ctnr_cls tr td:nth-child(4){
	width:150px;
	text-align: left;
}

.lp_ctnr_cls tr td:nth-child(5){
	width:180px;
	text-align: left;
}

.lp_ctnr_cls tr td:nth-child(6){
	width:180px;
	text-align: left;
}

.lp_ctnr_cls tr td input{
	width:150px;
}

img{
	cursor:pointer;
}

.wrap-search-input{
	position: relative;
	display: inline-block;
}

.icon-search-input{
	background: url(imgs/search.png) no-repeat center center;
	position: absolute;
	cursor: pointer;
	top: 0px;
	right: 0px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.wrap-search-input input:focus+.icon-search-input{
	border-color:#66afe9;
	outline:0;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)
}

.wrap-search-input input:focus+.icon-na-input{
	border-color:#66afe9;
	outline:0;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)
}

.icon-na-input{
	background: url(../imgs/na_14x14.png) no-repeat center center;
	position: absolute;
	cursor: pointer;
	top: 0px;
	right: 0px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.align-width-span{
	
	display:-moz-inline-box;
	display:inline-block;
	width:18px;
	text-align: center;
}
</style>
<script type="text/javascript">
var liveLoads = eval('<%=StringUtil.convertDBRowArrayToJsonString(liveLoads)%>');
var dropOffs = eval('<%=StringUtil.convertDBRowArrayToJsonString(dropOffs)%>');
var pickUps = eval('<%=StringUtil.convertDBRowArrayToJsonString(pickUps)%>');
var tmss = eval('<%=StringUtil.convertDBRowArrayToJsonString(tmss) %>');
var EXISTLIVELOAD = false;
$(document).ready(function(){
	liveLoadRender();
	dropOffRender();
	pickUpRender();
	tmsRender();
});

(function(){
	$.blockUI.defaults = {
		css: { 
			padding:'8px',
		  	margin:0,
		  	width:'170px', 
		  	top:'45%', 
		  	left:'40%', 
		  	textAlign:'center', 
		  	color:'#000', 
		  	border:'3px solid #999999',
		  	backgroundColor:'#ffffff',
		  	'-webkit-border-radius':'10px',
		  	'-moz-border-radius':'10px',
		  	'-moz-box-shadow':'0 1px 12px rgba(0,0,0,0.5)',
		  	'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 	},
	 	overlayCSS:{ 
	  		backgroundColor:'#000', 
	  		opacity:        '0.6' 
	 	},
	 	baseZ:99999, 
	 	centerX:true,
	 	centerY:true, 
	 	fadeOut:1000,
	 	showOverlay:true
	};
})();

$.fn.searchInput = function(events) {

    return this.each(function() {

    	if(this.nodeName !== 'INPUT'){
    		return;
    	}
    	var _self = $(this);
    	if(!_self.parent().hasClass('wrap-search-input')){
    		var height = _self.outerHeight();

    		_self.wrap('<div class="wrap-search-input" title="SEARCH"></div>');

    		var span = $('<span class="icon-search-input"></span>').width(height).height(height)
    					.insertAfter(_self.css('padding-right', height + 'px'));
    	}else{
    		
    		var span = _self.next('.icon-search-input');
    	}
    	
    	for(var e in events){
    		$.fn.on.call(span, e, events[e]);
    	}
    	_self.on('focus', function(){
    		
    		span.css('border-color', '#66afe9');
    	}).on('blur', function(){
    		
    		span.css('border-color', 'none');
    	});
    });
}

$.fn.naInput = function(events) {

    return this.each(function() {

    	if(this.nodeName !== 'INPUT'){
    		return;
    	}
    	var _self = $(this);
    	if(!_self.parent().hasClass('wrap-search-input')){
    		var height = _self.show().outerHeight();

    		_self.wrap('<div class="wrap-search-input"></div>');

    		var span = $('<span class="icon-na-input"></span>').width(height).height(height)
    					.insertAfter(_self.css('padding-right', height + 'px'));
    	}else{
    		var span = _self.next('.icon-na-input');
    	}
    	for(var e in events){
    		$.fn.on.call(span, e, events[e]);
    	}
    	_self.on('focus', function(){
    		
    		span.css('border-color', '#66afe9');
    	}).on('blur', function(){
    		
    		span.css('border-color', 'none');
    	});
    });
}

/**
 * 渲染整个Live Load
 * 
 */

 
function liveLoadRender(){

	if(liveLoads && liveLoads.length > 0){
	
		for(var i = 0; i < liveLoads.length; i ++){
		
			var liveLoad = liveLoads[i];
			var goType = '<%=CheckInLiveLoadOrDropOffKey.LIVE%>';
			
			if(liveLoad.equipment_type == '<%=CheckInTractorOrTrailerTypeKey.TRACTOR%>'){
			
				//展示
				$("#lpViewPage").append("<div id=live_load_view_"+liveLoad.equipment_id+"></div>");
				var viewTemplate = $("#lpViewTemplate").clone(true);
				$("#live_load_view_"+liveLoad.equipment_id).append(
					
					$(liveLoadViewRender(viewTemplate,liveLoad,goType)).html()
				);
				
				//编辑
				$("#lpEditPage").append("<div id=live_load_edit_"+liveLoad.equipment_id+"></div>");
				var editTemplate = $("#lpEditTemplate").clone(true);
				$("#live_load_edit_"+liveLoad.equipment_id).append(
					
					$(liveLoadEditRender(editTemplate,liveLoad,goType)).html()
				);
				
				<%-- if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
					$("span[name=occupy_type_name]", $("#liveLoadViewTemplate")).text("Door :");
					$("span[name=occupy_name]", $("#liveLoadViewTemplate")).text(liveLoad.doorid);
					
					$("span[name=occupy_type_name]", $("#liveLoadEditTemplate")).text("Door :");
					$("span[name=occupy_name]", $("#liveLoadEditTemplate")).text(liveLoad.doorid);
					
				}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
					
					$("span[name=occupy_type_name]", $("#liveLoadViewTemplate")).text("Spot :");
					$("span[name=occupy_name]", $("#liveLoadViewTemplate")).text(liveLoad.yc_no);
					$("span[name=occupy_type_name]", $("#liveLoadEditTemplate")).text("Spot :");
					$("span[name=occupy_name]", $("#liveLoadEditTemplate")).text(liveLoad.yc_no);
				}
				
				$("input[name=occupy_type]", $("#liveLoadEditTemplate")).val(liveLoad.resources_type);
				$("input[name=occupy_id]", $("#liveLoadEditTemplate")).val(liveLoad.resources_id); --%>
				
			}else if(liveLoad.equipment_type == '<%=CheckInTractorOrTrailerTypeKey.TRAILER%>'){
				
				EXISTLIVELOAD = true;
				
				//展示
				$("#liveLoadViewPage").append("<div id=live_load_view_"+liveLoad.equipment_id+"></div>");
				var viewTemplate = $("#liveLoadViewTemplate").clone(true);
				$("#live_load_view_"+liveLoad.equipment_id).append(
						
					$(liveLoadViewRender(viewTemplate,liveLoad,goType)).html()
				);
				
				//编辑
				$("#liveLoadEditPage").append("<div id=live_load_edit_"+liveLoad.equipment_id+"></div>");
				var editTemplate = $("#liveLoadEditTemplate").clone(true);
				$("#live_load_edit_"+liveLoad.equipment_id).append(
					
					$(liveLoadEditRender(editTemplate,liveLoad,goType)).html()
				);
			}
		}
		
		if(!EXISTLIVELOAD){
			
			$("#live_load_set").css("border","0px solid #999");
		} 
		
	}else{
		
		$("#live_load_set").css("border","0px solid #999");
	}
}

//渲染展示状态
function liveLoadViewRender(liveLoadCtnr,liveLoad,goType){
	
	var obj = $(liveLoadCtnr);
	$("span[name=equipment_number]", obj).text(liveLoad.equipment_number);
	
	if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		$("span[name=occupy_type_name]", obj).text("Door :");
		$("span[name=occupy_name]", obj).text(liveLoad.doorid);
		
	}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("span[name=occupy_type_name]", obj).text("Spot :");
		$("span[name=occupy_name]", obj).text(liveLoad.yc_no);
	}
	
	$("span[name=seal_delivery]", obj).text(liveLoad.seal_delivery);
	$("span[name=seal_pick_up]", obj).text(liveLoad.seal_pick_up);
	
	$("img[name=deleteLiveLoad]", obj).attr("equipment_id", liveLoad.equipment_id);
	$("img[name=deleteLiveLoad]", obj).attr("equipment_status", liveLoad.equipment_status);
	$("img[name=editLiveLoad]", obj).attr("equipment_id", liveLoad.equipment_id);
	$("img[name=editLiveLoad]", obj).attr("go_type", goType);
	$("img[name=editLiveLoad]", obj).attr("equipment_status", liveLoad.equipment_status);
	
	$("img[name=swapLiveLoad]", obj).attr("data-id", liveLoad.equipment_id);
	$("img[name=swapLiveLoad]", obj).attr("data-type", liveLoad.equipment_purpose);
	$("img[name=swapLiveLoad]", obj).attr("data-name", liveLoad.equipment_number);
	
	return obj;
}

//渲染编辑状态
function liveLoadEditRender(liveLoadCtnr,liveLoad,goType){
	
	var obj = $(liveLoadCtnr);
	
	$("input[name=equipment_number]", obj).attr("value",liveLoad.equipment_number);
	$("input[name=equipment_id]", obj).attr("value",liveLoad.equipment_id);
	$("input[name=equipment_purpose]", obj).attr("value",liveLoad.equipment_purpose);
	
	if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		
		$("input[name=occupy_name]", obj).attr("value",liveLoad.doorid);
	}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("input[name=occupy_name]", obj).attr("value",liveLoad.yc_no);
	}
	
	$("select[name=occupy_type] option[value='"+liveLoad.resources_type+"']",obj).attr("selected","selected");
	
	$("input[name=occupy_id]", obj).attr("value",liveLoad.resources_id);
	$("input[name=occupy_id]", obj).attr("data-id",liveLoad.resources_id);
	
	<%-- if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		$("span[name=occupy_type_name]", obj).text("Door :");
		$("span[name=occupy_name]", obj).text(liveLoad.doorid);
		
	}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("span[name=occupy_type_name]", obj).text("Spot :");
		$("span[name=occupy_name]", obj).text(liveLoad.yc_no);
	}
	
	$("input[name=occupy_type]", obj).attr("value",liveLoad.resources_type);
	$("input[name=occupy_id]", obj).attr("value",liveLoad.resources_id); --%>
	
	if(liveLoad.seal_delivery == 'NA'){
		
		$("input[name=seal_delivery]", obj).attr("value","");
	}else{
		
		$("input[name=seal_delivery]", obj).attr("value",liveLoad.seal_delivery);
	}

	if(liveLoad.seal_pick_up == 'NA'){
		
		$("input[name=seal_pick_up]", obj).attr("value","");
	}else{
		
		$("input[name=seal_pick_up]", obj).attr("value",liveLoad.seal_pick_up);
	}
	
	$("img[name=updateLiveLoad]", obj).attr("equipment_id", liveLoad.equipment_id);
	$("img[name=cancelLiveLoad]", obj).attr("equipment_id", liveLoad.equipment_id);
	
	return obj;
}

//取消转换
function liveLoadCancel(target){
	
	if(!EXISTLIVELOAD){
		
		$("#live_load_set").css("border","0px solid #999");
	}
	
	var id = $(target).attr("equipment_id")
	//换
	var view = $("#live_load_view_"+id).html();
	var edit = $("#live_load_edit_"+id).html();
	
	$("#live_load_view_"+id).html(edit);
	$("#live_load_edit_"+id).html(view);
}

//编辑转换
function liveLoadSwap(target){
	
	if(autoCommit()){
		
		var status = $(target).attr("equipment_status");
		
		if(status != <%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%> && status != <%=CheckInMainDocumentsStatusTypeKey.PROCESSING%>){
			
			showMessage("All Task Closed or Equipment Left.","alert");
			return false;
		}
		
		var id = $(target).attr("equipment_id");
		//换
		var view = $("#live_load_view_"+id).html();
		//重置
		var occupy_id = $("#live_load_edit_"+id+" input[name='occupy_id']");
		occupy_id.val(occupy_id.attr("data-id"));
		
		var edit = $("#live_load_edit_"+id).html();
		
		$("#live_load_view_"+id).html(edit);
		$("#live_load_edit_"+id).html(view);
		
		//绑定na
		fillNaInput("lpViewPage");
		fillNaInput("liveLoadViewPage");
	}
}

function fillNaInput(id){
	
	$("#"+id+" input[name='seal_delivery']").naInput({
  		"click":function(event){
  			
  			$(this).prev().val("NA");
  		}
 	});
    
    $("#"+id+" input[name='seal_pick_up']").naInput({
  		"click":function(event){
  			
  			$(this).prev().val("NA");
  		}
 	});
}

//添加
function liveLoadAdd(target){
	
	if(autoCommit()){
		
		$("#live_load_set").css("border","2px solid #999");
		
		var obj = $("#liveLoadEditTemplate").clone(true);
		
		$("img[name=updateLiveLoad]",obj).attr("equipment_id", '0');
		$("img[name=cancelLiveLoad]",obj).attr("equipment_id", '0');
		$("input[name=equipment_purpose]",obj).attr("value",'1');
		
		$("#live_load_view_0").html(obj.html());
		$("#live_load_edit_0").html('');
		
		fillNaInput("live_load_view_0");
	}
}

//保存
function liveLoadUpdate(target, is_check_success){
	
	var id = $(target).attr("equipment_id");
	var obj = $("#live_load_view_"+id);
	
	var equipment_number = $("input[name=equipment_number]",obj).attr("value");
	
	if(equipment_number == ''){
		
		showMessage("Enter CTNR.","alert");
		return false;
	}
	
	var equipment_purpose =  $("input[name=equipment_purpose]",obj).attr("value");
	
	var occupy_id = $("input[name=occupy_id]",obj).attr("value");
	
	/*if(occupy_id == ''){
		
		showMessage("Select Door / Spot.","alert");
		return false;
	}*/
	
	var occupy_type = $("select[name=occupy_type]",obj).attr("value");
	var seal_delivery = $("input[name=seal_delivery]",obj).attr("value");
	var seal_pick_up = $("input[name=seal_pick_up]",obj).attr("value");
	
	var seal_pick_up = $("input[name=seal_pick_up]",obj).attr("value");
	var equipment_type = $("input[name=equip_type]",obj).attr("value");
	
	var para = 
		"equipment_id="+id
		+"&check_in_entry_id=<%=entry_id%>"
		+"&equipment_number="+equipment_number
		+"&occupy_type="+occupy_type
		+"&occupy_id="+occupy_id
		+"&seal_delivery="+seal_delivery
		+"&seal_pick_up="+seal_pick_up
		+"&equipment_purpose="+equipment_purpose
		+"&equipment_type="+equipment_type;
	
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowUpdateEquipmentAction.action',
		type:'post',
		dataType:'json',
		timeout:60000,
		cache:false,
		data:para+"&is_check_success="+is_check_success,
		async:false,
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		error:function(serverresponse, status){
			$.unblockUI();
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		},
		success: function(data){
			$.unblockUI();
			var flag = data.flag;
			var equipment_this_entry_len = data.equipment_this_entry_len;//本entry已经提交的设备，此值大于0，清空本条添加的数据
			var equipment_not_entry_len  = data.equipment_not_entry_len;//非本entry已经提交的设备，此值大于，弹出框列表数据，问其是否要使其他的left并添加本设备名的设备
			//console.log(equipment_this_entry_len+","+equipment_not_entry_len+","+flag);
			if(flag && flag == '<%=YesOrNotKey.NO%>')
			{
				if(equipment_not_entry_len > 0)
				{
					var equipment_not_entry = data.equipment_not_entry;
					var html_msg = '<table width="500px">';
					html_msg += '<tr width="500px"><td colspan="5">Make sure the under equipment left and add a new one ?</td></tr>';
					html_msg += '<tr height="10px"><td colspan="5"></td></tr>';
					for(var i=0; i<equipment_not_entry_len; i++)
					{
						html_msg += '<tr width="500px">'
										+'<td width="50px" align="left">'+equipment_not_entry[i].equipment_type_value+" :</td>"
										+'<td width="120px" align="left"> '+equipment_not_entry[i].equipment_number+"</td>"
										+'<td width="50px" align="left"> at '+equipment_not_entry[i].resources_type_value+" :</td>"
										+'<td width="80px" align="left"> '+equipment_not_entry[i].resources_name+"</td>"
										+'<td width="140px" align="left"> come with : E'+equipment_not_entry[i].entry_id+"</td>"
									+"</tr>";
					}
					html_msg += "</table>";
					$.artDialog({
					    content: $(html_msg).html(),
					    icon: 'warning',
					    width: 550,
					    height: 70,
					    title:'',
					    opacity: 0.1,
					    lock: true,
					    fixed:true,
					    okVal: 'Confirm',
					    ok: function () {
							liveLoadUpdate(target, '<%=YesOrNotKey.YES%>');
					    },
					    cancelVal: 'Cancel',
					    cancel:function(){
					    	$("input[name=equipment_number]",obj).val("");
					    }
					});	  
				}
				else if(equipment_this_entry_len > 0)
				{
					showMessage(equipment_number+" exist.","alert");
					$("input[name=equipment_number]",obj).val("");
				}
			}
			else
			{
				$.artDialog.opener.monitorCheckInWindowChanged();
				$.unblockUI();
				loadEquipments();
			}
		}
	});
	return true;
}

//删除
function liveLoadDelete(obj, is_check_success){
	
	if(autoCommit()){
		
		var status = $(obj).attr("equipment_status");
		
		if(status != <%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%> && status != <%=CheckInMainDocumentsStatusTypeKey.PROCESSING%>){
			
			showMessage("All Task Closed or Equipment Left.","alert");
			return false;
		}
		
		var id = $(obj).attr("equipment_id");
		var dialog_content = '';
		var icon_style = 'question';
		if(is_check_success && is_check_success=='<%=YesOrNotKey.YES%>'){
			
			dialog_content = 'You will delete all tasks in this equipment, ';
			icon_style = 'warning';
		}
		
		$.artDialog({
		    content: dialog_content+'do you want to delete?',
		    icon: icon_style,
		    width: 200,
		    height: 70,
		    lock: true,
		    fixed:true,
		    opacity: 0.1,
		    okVal: 'Confirm',
		    ok: function () {
		    	$.ajax({
		    		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowDeleteEquipmentByIdAction.action',
		    		type: 'post',
		    		dataType: 'json',
		    		timeout: 60000,
		    		cache:false,
		    		data:"id="+id+"&is_check_success="+is_check_success,
		    		async:false,
		    		beforeSend:function(request){
		    			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    		},
		    		error:function(serverresponse, status){
	    	  			
		    			$.unblockUI();
	    	  			showMessage(errorMessage(serverresponse.responseText),"alert");
	    	  		},
		    		success: function(data){
		    			
		    			$.artDialog.opener.monitorCheckInWindowChanged();
		    			$.unblockUI();
		    			if(data.flag && data.flag == '<%=YesOrNotKey.NO%>')
	    				{
		    				liveLoadDelete(obj, '<%=YesOrNotKey.YES%>');
	    				}
		    			else
	    				{
			    			loadEquipments();
	    				}
		    		}
		    	});
		    },
		    cancelVal: 'Cancel'
		});	  
	}
}

/**
 * 渲染整个tms
 * 
 */

 
function tmsRender(){

	if(tmss && tmss.length > 0){
	
		for(var i = 0; i < tmss.length; i ++){
		
			var tms = tmss[i];
			var goType = '<%=CheckInLiveLoadOrDropOffKey.TMS%>';
			
			if(tms.equipment_type == '<%=CheckInTractorOrTrailerTypeKey.TRACTOR%>'){
			
				//展示
				$("#lpViewPage").append("<div id=tms_view_"+tms.equipment_id+"></div>");
				var viewTemplate = $("#lpViewTemplate").clone(true);
				$("#tms_view_"+tms.equipment_id).append(
					
					$(tmsViewRender(viewTemplate,tms,goType)).html()
				);
				
				//编辑
				$("#lpEditPage").append("<div id=tms_edit_"+tms.equipment_id+"></div>");
				var editTemplate = $("#lpEditTemplate").clone(true);
				$("#tms_edit_"+tms.equipment_id).append(
					
					$(tmsEditRender(editTemplate,tms,goType)).html()
				);
				
				<%-- if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
					$("span[name=occupy_type_name]", $("#liveLoadViewTemplate")).text("Door :");
					$("span[name=occupy_name]", $("#liveLoadViewTemplate")).text(liveLoad.doorid);
					
					$("span[name=occupy_type_name]", $("#liveLoadEditTemplate")).text("Door :");
					$("span[name=occupy_name]", $("#liveLoadEditTemplate")).text(liveLoad.doorid);
					
				}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
					
					$("span[name=occupy_type_name]", $("#liveLoadViewTemplate")).text("Spot :");
					$("span[name=occupy_name]", $("#liveLoadViewTemplate")).text(liveLoad.yc_no);
					$("span[name=occupy_type_name]", $("#liveLoadEditTemplate")).text("Spot :");
					$("span[name=occupy_name]", $("#liveLoadEditTemplate")).text(liveLoad.yc_no);
				}
				
				$("input[name=occupy_type]", $("#liveLoadEditTemplate")).val(liveLoad.resources_type);
				$("input[name=occupy_id]", $("#liveLoadEditTemplate")).val(liveLoad.resources_id); --%>
				
			}else if(tms.equipment_type == '<%=CheckInTractorOrTrailerTypeKey.TRAILER%>'){
				
				EXISTLIVELOAD = true;
				
				//展示
				$("#tmsViewPage").append("<div id=tms_view_"+tms.equipment_id+"></div>");
				var viewTemplate = $("#tmsViewTemplate").clone(true);
				$("#tms_view_"+tms.equipment_id).append(
						
					$(tmsViewRender(viewTemplate,tms,goType)).html()
				);
				
				//编辑
				$("#tmsEditPage").append("<div id=tms_edit_"+tms.equipment_id+"></div>");
				var editTemplate = $("#tmsEditTemplate").clone(true);
				$("#tms_edit_"+tms.equipment_id).append(
					
					$(tmsEditRender(editTemplate,tms,goType)).html()
				);
			}
		}
		
		if(!EXISTLIVELOAD){
			
			$("#tms_set").css("border","0px solid #999");
		} 
		
	}else{
		
		$("#tms_set").css("border","0px solid #999");
	}
}

//渲染展示状态
function tmsViewRender(tmsCtnr,tms,goType){
	
	var obj = $(tmsCtnr);
	$("span[name=equipment_number]", obj).text(tms.equipment_number);
	
	if(tms.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		$("span[name=occupy_type_name]", obj).text("Door :");
		$("span[name=occupy_name]", obj).text(tms.doorid);
		
	}else if(tms.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("span[name=occupy_type_name]", obj).text("Spot :");
		$("span[name=occupy_name]", obj).text(tms.yc_no);
	}
	
	$("span[name=seal_delivery]", obj).text(tms.seal_delivery);
	$("span[name=seal_pick_up]", obj).text(tms.seal_pick_up);
	
	$("img[name=deleteTms]", obj).attr("equipment_id", tms.equipment_id);
	$("img[name=deleteTms]", obj).attr("equipment_status", tms.equipment_status);
	$("img[name=editTms]", obj).attr("equipment_id", tms.equipment_id);
	$("img[name=editTms]", obj).attr("go_type", goType);
	$("img[name=editTms]", obj).attr("equipment_status", tms.equipment_status);
	
	$("img[name=swapTms]", obj).attr("data-id", tms.equipment_id);
	$("img[name=swapTms]", obj).attr("data-type", tms.equipment_purpose);
	$("img[name=swapTms]", obj).attr("data-name", tms.equipment_number);
	
	return obj;
}

//渲染编辑状态
function tmsEditRender(tmsCtnr,tms,goType){
	
	var obj = $(tmsCtnr);
	
	$("input[name=equipment_number]", obj).attr("value",tms.equipment_number);
	$("input[name=equipment_id]", obj).attr("value",tms.equipment_id);
	$("input[name=equipment_purpose]", obj).attr("value",tms.equipment_purpose);
	
	if(tms.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		
		$("input[name=occupy_name]", obj).attr("value",tms.doorid);
	}else if(tms.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("input[name=occupy_name]", obj).attr("value",tms.yc_no);
	}
	
	$("select[name=occupy_type] option[value='"+tms.resources_type+"']",obj).attr("selected","selected");
	
	$("input[name=occupy_id]", obj).attr("value",tms.resources_id);
	$("input[name=occupy_id]", obj).attr("data-id",tms.resources_id);
	
	<%-- if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		$("span[name=occupy_type_name]", obj).text("Door :");
		$("span[name=occupy_name]", obj).text(liveLoad.doorid);
		
	}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("span[name=occupy_type_name]", obj).text("Spot :");
		$("span[name=occupy_name]", obj).text(liveLoad.yc_no);
	}
	
	$("input[name=occupy_type]", obj).attr("value",liveLoad.resources_type);
	$("input[name=occupy_id]", obj).attr("value",liveLoad.resources_id); --%>
	
	if(tms.seal_delivery == 'NA'){
		
		$("input[name=seal_delivery]", obj).attr("value","");
	}else{
		
		$("input[name=seal_delivery]", obj).attr("value",tms.seal_delivery);
	}

	if(tms.seal_pick_up == 'NA'){
		
		$("input[name=seal_pick_up]", obj).attr("value","");
	}else{
		
		$("input[name=seal_pick_up]", obj).attr("value",tms.seal_pick_up);
	}
	
	$("img[name=updateTms]", obj).attr("equipment_id", tms.equipment_id);
	$("img[name=cancelTms]", obj).attr("equipment_id", tms.equipment_id);
	
	return obj;
}

//取消转换Tms
function tmsCancel(target){
	
	if(!EXISTLIVELOAD){
		
		$("#tms_set").css("border","0px solid #999");
	}
	
	var id = $(target).attr("equipment_id")
	//换
	var view = $("#tms_view_"+id).html();
	var edit = $("#tms_edit_"+id).html();
	
	$("#tms_view_"+id).html(edit);
	$("#tms_edit_"+id).html(view);
}

//编辑转换
function tmsSwap(target){
	
	if(autoCommit()){
		var status = $(target).attr("equipment_status");
		if(status != <%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%> && status != <%=CheckInMainDocumentsStatusTypeKey.PROCESSING%>){
			
			showMessage("All Task Closed or Equipment Left.","alert");
			return false;
		}
		
		var id = $(target).attr("equipment_id");
		//换
		var view = $("#tms_view_"+id).html();
		//重置
		var occupy_id = $("#tms_edit_"+id+" input[name='occupy_id']");
		occupy_id.val(occupy_id.attr("data-id"));
		
		var edit = $("#tms_edit_"+id).html();
		
		$("#tms_view_"+id).html(edit);
		$("#tms_edit_"+id).html(view);
		
		//绑定na
		fillNaInput("lpViewPage");
		fillNaInput("liveLoadViewPage");
	}
}

function fillNaInput(id){
	
	$("#"+id+" input[name='seal_delivery']").naInput({
  		"click":function(event){
  			
  			$(this).prev().val("NA");
  		}
 	});
    
    $("#"+id+" input[name='seal_pick_up']").naInput({
  		"click":function(event){
  			
  			$(this).prev().val("NA");
  		}
 	});
}

//添加
function tmsAdd(target){
	
	if(autoCommit()){
		
		$("#tms_set").css("border","2px solid #999");
		
		var obj = $("#tmsEditTemplate").clone(true);
		
		$("img[name=updateTms]",obj).attr("equipment_id", '0');
		$("img[name=cancelTms]",obj).attr("equipment_id", '0');
		$("input[name=equipment_purpose]",obj).attr("value",'1');
		
		$("#tms_view_0").html(obj.html());
		$("#tms_edit_0").html('');
		
		fillNaInput("tms_view_0");
	}
}

//保存
function tmsUpdate(target, is_check_success){
	
	var id = $(target).attr("equipment_id");
	var obj = $("#tms_view_"+id);
	
	var equipment_number = $("input[name=equipment_number]",obj).attr("value");
	
	if(equipment_number == ''){
		
		showMessage("Enter CTNR.","alert");
		return false;
	}
	
	var equipment_purpose =  $("input[name=equipment_purpose]",obj).attr("value");
	
	var occupy_id = $("input[name=occupy_id]",obj).attr("value");
	
	/*if(occupy_id == ''){
		
		showMessage("Select Door / Spot.","alert");
		return false;
	}*/
	
	var occupy_type = $("select[name=occupy_type]",obj).attr("value");
	var seal_delivery = $("input[name=seal_delivery]",obj).attr("value");
	var seal_pick_up = $("input[name=seal_pick_up]",obj).attr("value");
	
	var seal_pick_up = $("input[name=seal_pick_up]",obj).attr("value");
	var equipment_type = $("input[name=equip_type]",obj).attr("value");
	
	var para = 
		"equipment_id="+id
		+"&check_in_entry_id=<%=entry_id%>"
		+"&equipment_number="+equipment_number
		+"&occupy_type="+occupy_type
		+"&occupy_id="+occupy_id
		+"&seal_delivery="+seal_delivery
		+"&seal_pick_up="+seal_pick_up
		+"&equipment_purpose="+equipment_purpose
		+"&equipment_type="+equipment_type;
	
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowUpdateEquipmentAction.action',
		type:'post',
		dataType:'json',
		timeout:60000,
		cache:false,
		data:para+"&is_check_success="+is_check_success,
		async:false,
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		error:function(serverresponse, status){
			$.unblockUI();
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		},
		success: function(data){
			$.unblockUI();
			var flag = data.flag;
			var equipment_this_entry_len = data.equipment_this_entry_len;//本entry已经提交的设备，此值大于0，清空本条添加的数据
			var equipment_not_entry_len  = data.equipment_not_entry_len;//非本entry已经提交的设备，此值大于，弹出框列表数据，问其是否要使其他的left并添加本设备名的设备
			//console.log(equipment_this_entry_len+","+equipment_not_entry_len+","+flag);
			if(flag && flag == '<%=YesOrNotKey.NO%>')
			{
				if(equipment_not_entry_len > 0)
				{
					var equipment_not_entry = data.equipment_not_entry;
					var html_msg = '<table width="500px">';
					html_msg += '<tr width="500px"><td colspan="5">Make sure the under equipment left and add a new one ?</td></tr>';
					html_msg += '<tr height="10px"><td colspan="5"></td></tr>';
					for(var i=0; i<equipment_not_entry_len; i++)
					{
						html_msg += '<tr width="500px">'
										+'<td width="50px" align="left">'+equipment_not_entry[i].equipment_type_value+" :</td>"
										+'<td width="120px" align="left"> '+equipment_not_entry[i].equipment_number+"</td>"
										+'<td width="50px" align="left"> at '+equipment_not_entry[i].resources_type_value+" :</td>"
										+'<td width="80px" align="left"> '+equipment_not_entry[i].resources_name+"</td>"
										+'<td width="140px" align="left"> come with : E'+equipment_not_entry[i].entry_id+"</td>"
									+"</tr>";
					}
					html_msg += "</table>";
					$.artDialog({
					    content: $(html_msg).html(),
					    icon: 'warning',
					    width: 550,
					    height: 70,
					    title:'',
					    opacity: 0.1,
					    lock: true,
					    fixed:true,
					    okVal: 'Confirm',
					    ok: function () {
							liveLoadUpdate(target, '<%=YesOrNotKey.YES%>');
					    },
					    cancelVal: 'Cancel',
					    cancel:function(){
					    	$("input[name=equipment_number]",obj).val("");
					    }
					});	  
				}
				else if(equipment_this_entry_len > 0)
				{
					showMessage(equipment_number+" exist.","alert");
					$("input[name=equipment_number]",obj).val("");
				}
			}
			else
			{
				$.artDialog.opener.monitorCheckInWindowChanged();
				$.unblockUI();
				loadEquipments();
			}
		}
	});
	return true;
}

//删除
function tmsDelete(obj, is_check_success){
	
	if(autoCommit()){
		
		var status = $(obj).attr("equipment_status");
		
		if(status != <%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%> && status != <%=CheckInMainDocumentsStatusTypeKey.PROCESSING%>){
			
			showMessage("All Task Closed or Equipment Left.","alert");
			return false;
		}
		
		var id = $(obj).attr("equipment_id");
		var dialog_content = '';
		var icon_style = 'question';
		if(is_check_success && is_check_success=='<%=YesOrNotKey.YES%>'){
			
			dialog_content = 'You will delete all tasks in this equipment, ';
			icon_style = 'warning';
		}
		
		$.artDialog({
		    content: dialog_content+'do you want to delete?',
		    icon: icon_style,
		    width: 200,
		    height: 70,
		    lock: true,
		    fixed:true,
		    opacity: 0.1,
		    okVal: 'Confirm',
		    ok: function () {
		    	$.ajax({
		    		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowDeleteEquipmentByIdAction.action',
		    		type: 'post',
		    		dataType: 'json',
		    		timeout: 60000,
		    		cache:false,
		    		data:"id="+id+"&is_check_success="+is_check_success,
		    		async:false,
		    		beforeSend:function(request){
		    			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    		},
		    		error:function(serverresponse, status){
	    	  			
		    			$.unblockUI();
	    	  			showMessage(errorMessage(serverresponse.responseText),"alert");
	    	  		},
		    		success: function(data){
		    			
		    			$.artDialog.opener.monitorCheckInWindowChanged();
		    			$.unblockUI();
		    			if(data.flag && data.flag == '<%=YesOrNotKey.NO%>')
	    				{
		    				tmsDelete(obj, '<%=YesOrNotKey.YES%>');
	    				}
		    			else
	    				{
			    			loadEquipments();
	    				}
		    		}
		    	});
		    },
		    cancelVal: 'Cancel'
		});	  
	}
}

/**
 * 渲染整个drop off 
 * 
 */
function dropOffRender(){
	
	if(dropOffs && dropOffs.length > 0){
		
		for(var i = 0; i < dropOffs.length; i ++){
			
			var dropOff = dropOffs[i];
			var goType = '<%=CheckInLiveLoadOrDropOffKey.DROP%>';
			
			//展示
			$("#dropOffViewPage").append("<div id=drop_off_view_"+dropOff.equipment_id+"></div>");
			var viewTemplate = $("#dropOffViewTemplate").clone(true);
			$("#drop_off_view_"+dropOff.equipment_id).append(
					
				$(dropOffViewRender(viewTemplate,dropOff,goType)).html()
			);
			
			//编辑
			$("#dropOffEditPage").append("<div id=drop_off_edit_"+dropOff.equipment_id+"></div>");
			var editTemplate = $("#dropOffEditTemplate").clone(true);
			$("#drop_off_edit_"+dropOff.equipment_id).append(
				
				$(dropOffEditRender(editTemplate,dropOff,goType)).html()
			);
		}
	}else{
		
		$("#drop_off_set").css("border","0px solid #999");
	}
}

//渲染展示状态
function dropOffViewRender(liveLoadCtnr,liveLoad,goType){
	
	var obj = $(liveLoadCtnr);
	$("span[name=equipment_number]", obj).text(liveLoad.equipment_number);
	
	if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		$("span[name=occupy_type_name]", obj).text("Door :");
		$("span[name=occupy_name]", obj).text(liveLoad.doorid);
		
	}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("span[name=occupy_type_name]", obj).text("Spot :");
		$("span[name=occupy_name]", obj).text(liveLoad.yc_no);
	}
	
	$("span[name=seal_delivery]", obj).text(liveLoad.seal_delivery);
	$("span[name=seal_pick_up]", obj).text(liveLoad.seal_pick_up);
	
	$("img[name=deleteDropOff]", obj).attr("equipment_id", liveLoad.equipment_id);
	$("img[name=deleteDropOff]", obj).attr("equipment_status", liveLoad.equipment_status);
	$("img[name=editDropOff]", obj).attr("equipment_id", liveLoad.equipment_id);
	$("img[name=editDropOff]", obj).attr("go_type", goType);
	$("img[name=editDropOff]", obj).attr("equipment_status", liveLoad.equipment_status);
	
	$("img[name=swapLiveLoad]", obj).attr("data-id", liveLoad.equipment_id);
	$("img[name=swapLiveLoad]", obj).attr("data-type", liveLoad.equipment_purpose);
	$("img[name=swapLiveLoad]", obj).attr("data-name", liveLoad.equipment_number);
	
	return obj;
}

//渲染编辑状态
function dropOffEditRender(liveLoadCtnr,liveLoad,goType){
	
	var obj = $(liveLoadCtnr);
	
	$("input[name=equipment_number]", obj).attr("value",liveLoad.equipment_number);
	$("input[name=equipment_id]", obj).attr("value",liveLoad.equipment_id);
	$("input[name=equipment_purpose]", obj).attr("value",liveLoad.equipment_purpose);
	
	if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		
		$("input[name=occupy_name]", obj).attr("value",liveLoad.doorid);
	}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("input[name=occupy_name]", obj).attr("value",liveLoad.yc_no);
	}
	
	$("select[name=occupy_type] option[value='"+liveLoad.resources_type+"']",obj).attr("selected","selected");
	
	$("input[name=occupy_id]", obj).attr("value",liveLoad.resources_id);
	$("input[name=occupy_id]", obj).attr("data-id",liveLoad.resources_id);
	
	if(liveLoad.seal_delivery == 'NA'){
		
		$("input[name=seal_delivery]", obj).attr("value","");
	}else{
		$("input[name=seal_delivery]", obj).attr("value",liveLoad.seal_delivery);
	}
	
	if(liveLoad.seal_pick_up == 'NA'){
		
		$("input[name=seal_pick_up]", obj).attr("value","");
	}else{
		$("input[name=seal_pick_up]", obj).attr("value",liveLoad.seal_pick_up);
	}
	
	$("img[name=updateDropOff]", obj).attr("equipment_id", liveLoad.equipment_id);
	$("img[name=cancelDropOff]", obj).attr("equipment_id", liveLoad.equipment_id);
	
	return obj;
}

//取消转换
function dropOffCancel(target){
	
	if(!dropOffs){
		
		$("#drop_off_set").css("border","0px solid #999");
	}
	
	var id = $(target).attr("equipment_id")
	
	//换
	var view = $("#drop_off_view_"+id).html();
	var edit = $("#drop_off_edit_"+id).html();
	
	$("#drop_off_view_"+id).html(edit);
	$("#drop_off_edit_"+id).html(view);
}

//编辑转换
function dropOffSwap(target){
	
	if(autoCommit()){
		
		var status = $(target).attr("equipment_status");
		
		if(status != <%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%> && status != <%=CheckInMainDocumentsStatusTypeKey.PROCESSING%>){
			
			showMessage("All Task Closed or Equipment Left.","alert");
			return false;
		}
		
		var id = $(target).attr("equipment_id")
		
		//换
		var view = $("#drop_off_view_"+id).html();
		
		//重置
		var occupy_id = $("#drop_off_edit_"+id+" input[name='occupy_id']");
		occupy_id.val(occupy_id.attr("data-id"));
		
		var edit = $("#drop_off_edit_"+id).html();
		
		$("#drop_off_view_"+id).html(edit);
		$("#drop_off_edit_"+id).html(view);
		
		fillNaInput("dropOffViewPage");
	}
}

//添加
function dropOffAdd(target){
	
	if(autoCommit()){
		
		$("#drop_off_set").css("border","2px solid #999");
		
		var obj = $("#dropOffEditTemplate").clone(true);
		
		$("img[name=updateDropOff]",obj).attr("equipment_id", '0');
		$("img[name=cancelDropOff]",obj).attr("equipment_id", '0');
		$("input[name=equipment_purpose]",obj).attr("value",'2');
		
		$("#drop_off_view_0").html(obj.html());
		$("#drop_off_edit_0").html('');
		
		fillNaInput("drop_off_view_0");
	}
}

//保存
function dropOffUpdate(target, is_check_success){
	
	var id = $(target).attr("equipment_id");
	var obj = $("#drop_off_view_"+id);
	
	var equipment_number = $("input[name=equipment_number]",obj).attr("value");
	var equipment_type = $("input[name=equip_type]",obj).attr("value");
	var occupy_id = $("input[name=occupy_id]",obj).attr("value");
	
	if(equipment_number == ''){
		
		showMessage("Enter CTNR.","alert");
		return false;
	}
	
	/*if(occupy_id == ''){
		
		showMessage("Select Door / Spot.","alert");
		return false;
	}*/
	
	var equipment_purpose =  $("input[name=equipment_purpose]",obj).attr("value");
	var occupy_type = $("select[name=occupy_type]",obj).attr("value");
	
	var seal_delivery = $("input[name=seal_delivery]",obj).attr("value");
	var seal_pick_up = $("input[name=seal_pick_up]",obj).attr("value");
	
	var para = 
		"equipment_type="+equipment_type
		+"&check_in_entry_id=<%=entry_id%>"
		+"&equipment_number="+equipment_number
		+"&equipment_purpose="+equipment_purpose
		+"&seal_delivery="+seal_delivery
		+"&seal_pick_up="+seal_pick_up
		+"&occupy_type="+occupy_type
		+"&occupy_id="+occupy_id
		+"&equipment_id="+id;
		
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowUpdateEquipmentAction.action',
		type:'post',
		dataType:'json',
		timeout:60000,
		cache:false,
		data:para+"&is_check_success="+is_check_success,
		async:false,
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		error:function(serverresponse, status){
			$.unblockUI();
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		},
		success:function(data){
			$.unblockUI();
			var flag = data.flag;
			var equipment_this_entry_len = data.equipment_this_entry_len;//本entry已经提交的设备，此值大于0，清空本条添加的数据
			var equipment_not_entry_len  = data.equipment_not_entry_len;//非本entry已经提交的设备，此值大于，弹出框列表数据，问其是否要使其他的left并添加本设备名的设备
			//console.log(equipment_this_entry_len+","+equipment_not_entry_len+","+flag);
			if(flag && flag == '<%=YesOrNotKey.NO%>')
			{
				if(equipment_not_entry_len > 0)
				{
					var equipment_not_entry = data.equipment_not_entry;
					var html_msg = '<table width="500px">';
					html_msg += '<tr width="500px"><td colspan="5">Make sure the under equipment left and add a new one ?</td></tr>';
					html_msg += '<tr height="10px"><td colspan="5"></td></tr>';
					for(var i=0; i<equipment_not_entry_len; i++)
					{
						html_msg += '<tr width="500px">'
										+'<td width="50px" align="left">'+equipment_not_entry[i].equipment_type_value+" :</td>"
										+'<td width="120px" align="left"> '+equipment_not_entry[i].equipment_number+"</td>"
										+'<td width="50px" align="left"> at '+equipment_not_entry[i].resources_type_value+" :</td>"
										+'<td width="80px" align="left"> '+equipment_not_entry[i].resources_name+"</td>"
										+'<td width="140px" align="left"> come with : E'+equipment_not_entry[i].entry_id+"</td>"
									+"</tr>";
					}
					html_msg += "</table>";
					$.artDialog({
					    content: $(html_msg).html(),
					    icon: 'warning',
					    width: 550,
					    height: 70,
					    title:'',
					    opacity: 0.1,
					    lock: true,
					    fixed:true,
					    okVal: 'Confirm',
					    ok: function () {
					    	dropOffUpdate(target, '<%=YesOrNotKey.YES%>');
					    },
					    cancelVal: 'Cancel',
					    cancel:function(){
					    	$("input[name=equipment_number]",obj).val("");
					    }
					});	  
				}
				else if(equipment_this_entry_len > 0)
				{
					showMessage(equipment_number+" exist.","alert");
					$("input[name=equipment_number]",obj).val("");
				}
			}
			else
			{
				$.artDialog.opener.monitorCheckInWindowChanged();
				$.unblockUI();
				loadEquipments();
			}
		}
	});
	
	return true;
}

//删除
function dropOffDelete(obj, is_check_success){
	
	if(autoCommit()){
		
		var status = $(obj).attr("equipment_status");
		
		if(status != <%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%> && status != <%=CheckInMainDocumentsStatusTypeKey.PROCESSING%>){
			
			showMessage("All Task Closed or Equipment Left.","alert");
			return false;
		}
		
		var id = $(obj).attr("equipment_id");
		var dialog_content = '';
		var icon_style = 'question';
		if(is_check_success && is_check_success=='<%=YesOrNotKey.YES%>'){
			
			dialog_content = 'You will delete all tasks in this equipment, ';
			icon_style = 'warning';
		}
		$.artDialog({
		    content: dialog_content+'do you want to delete?',
		    icon: icon_style,
		    width: 200,
		    height: 70,
		    title:'',
		    opacity: 0.1,
		    lock: true,
		    fixed:true,
		    okVal: 'Confirm',
		    ok: function () {
		    	$.ajax({
		    		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowDeleteEquipmentByIdAction.action',
		    		type: 'post',
		    		dataType: 'json',
		    		timeout: 60000,
		    		cache:false,
		    		data:"id="+id+"&is_check_success="+is_check_success,
		    		async:false,
		    		beforeSend:function(request){
		    			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    		},
					error:function(serverresponse, status){
	    	  			
						$.unblockUI();
	    	  			showMessage(errorMessage(serverresponse.responseText),"alert");
	    	  		},
		    		success: function(data){
		    			
		    			$.artDialog.opener.monitorCheckInWindowChanged();
		    			
		    			$.unblockUI();
		    			if(data.flag && data.flag == '<%=YesOrNotKey.NO%>')
	    				{
		    				dropOffDelete(obj, '<%=YesOrNotKey.YES%>');
	    				}
		    			else
	    				{
			    			loadEquipments();
	    				}
		    		}
		    	});
		    },
		    cancelVal: 'Cancel',
		});	  
	}
}

/**
 * 渲染整个Pick Up
 * 
 */
function pickUpRender(){
	
	if(pickUps && pickUps.length > 0){
		
		for(var i = 0; i < pickUps.length; i ++){
			
			var pickUp = pickUps[i];
			var goType = '<%=CheckInLiveLoadOrDropOffKey.PICK_UP%>';
			
			//展示
			$("#pickUpViewPage").append("<div id=pick_up_view_"+pickUp.equipment_id+"></div>");
			var viewTemplate = $("#pickUpViewTemplate").clone(true);
			$("#pick_up_view_"+pickUp.equipment_id).append(
					
				$(pickUpViewRender(viewTemplate,pickUp,goType)).html()
			);
			
			//编辑
			$("#pickUpEditPage").append("<div id=pick_up_edit_"+pickUp.equipment_id+"></div>");
			var editTemplate = $("#pickUpEditTemplate").clone(true);
			$("#pick_up_edit_"+pickUp.equipment_id).append(
				
				$(pickUpEditRender(editTemplate,pickUp,goType)).html()
			);
		}
		
	}else{
	
		$("#pick_up_set").css("border","0px solid #999");
	}
}

//渲染展示状态
function pickUpViewRender(liveLoadCtnr,liveLoad,goType){
	
	var obj = $(liveLoadCtnr);
	
	$("span[name=equipment_number]", obj).text(liveLoad.equipment_number);
	
	if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		
		$("span[name=occupy_type]", obj).text("Door :");
		$("span[name=occupy_name]", obj).text(liveLoad.doorid);
		
	}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("span[name=occupy_type]", obj).text("Spot :");
		$("span[name=occupy_name]", obj).text(liveLoad.yc_no);
	}

	$("span[name=entry_in_id]",  obj).text(liveLoad.check_in_entry_id);
	
	$("span[name=seal_delivery]", obj).text(liveLoad.seal_delivery);
	$("span[name=seal_pick_up]", obj).text(liveLoad.seal_pick_up);
	
	$("img[name=deletePickUp]",obj).attr("equipment_id", liveLoad.equipment_id);
	$("img[name=deletePickUp]",obj).attr("equipment_status", liveLoad.equipment_status);
	$("img[name=editPickUp]",obj).attr("equipment_id", liveLoad.equipment_id);
	$("img[name=editPickUp]",obj).attr("equipment_status", liveLoad.equipment_status);
	
	return obj;
}

//渲染编辑状态
function pickUpEditRender(liveLoadCtnr,liveLoad,goType){
	
	var obj = $(liveLoadCtnr);
	
	$("input[name=equipment_number]", obj).attr("value",liveLoad.equipment_number);
	
	$("input[name=equipment_id]", obj).attr("value",liveLoad.equipment_id);
	$("input[name=before_equipment_id]", obj).attr("value",liveLoad.equipment_id);
	$("input[name=equipment_purpose]", obj).attr("value",liveLoad.equipment_purpose);
	
	if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		
		$("span[name=occupy_type]", obj).text("Door :");
		$("span[name=occupy_name]", obj).text(liveLoad.doorid);
		
	}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("span[name=occupy_type]", obj).text("Spot :");
		$("span[name=occupy_name]", obj).text(liveLoad.yc_no);
	}
	
	$("input[name=occupy_type]", obj).attr("value",liveLoad.resources_type);
	$("span[name=entry_in_id]",  obj).text(liveLoad.check_in_entry_id);
	
	$("span[name=seal_delivery]", obj).text(liveLoad.seal_delivery);
	$("span[name=seal_pick_up]", obj).text(liveLoad.seal_pick_up);
	
	$("img[name=updatePickUp]",obj).attr("equipment_id", liveLoad.equipment_id);
	$("img[name=cancelPickUp]",obj).attr("equipment_id", liveLoad.equipment_id);
	
	return obj;
}

//添加
function pickUpAdd(target){
	if(autoCommit()){
		
		$("#pick_up_set").css("border","2px solid #999");
		
		var obj = $("#pickUpEditTemplate").clone(true);
		
		var tmp = $("#pickUpViewTemplate span[name='entry_in_id']").parents("td").html();
		
		$("img[name=updatePickUp]",obj).attr("equipment_id", '0');
		$("img[name=cancelPickUp]",obj).attr("equipment_id", '0');
		$("input[name=equipment_purpose]",obj).attr("value",'4');
		
		$("span[name='entry_in_id']",obj).parents("td").html(tmp);
		
		$("#pick_up_view_0").html(obj.html());
		$("#pick_up_edit_0").html('');
		addAutoComplete($("#pick_up_set input[name=equipment_number]"),
				
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInCTNRJSONAction.action?flag=1",
				"equipment_number","equipment_number");
		$("#pick_up_set input[name=equipment_number]").autocomplete({minLength: 3});
		clickAutoComplete( $( "#pick_up_set input[name=equipment_number]" ) );	
		//查询按钮
	    $("#pickAllPage input[name='equipment_number']").searchInput({
	  		"click":function(event){
	  			
	  			getPickUp($(this).prev());
	  		}
	 	});
	}
}

function pickUpKeyPress(event){
	
	if(event.keyCode==13){
		
		getPickUp($(event.target));
	}
}

function pickUpOnBlur(target){
	
	getPickUp($(target));
}
function selectCommon(jqObj) {
	$(jqObj).autocomplete({ 
		select: function (event, ui) {
			var arg = ui.item.value;
			$(jqObj).val(arg);
			getPickUp($(jqObj));
		}
	});	

}
//当点击文本框时触发事件
//$("#Both input[id='ctnr']")
function clickAutoComplete(jqObj) {
	jqObj.bind('click',function() {
		var val = $.trim($(this).val());
		if(val != ""){
			jqObj.autocomplete( "search" );
		}
	});

}
//清空其他值
function clearPickUp(target,event){
	
	if(event.keyCode!=13){
		
		var obj = $(target).parents("tr");
		
		$("input[name=equipment_id]", obj).val('');
		$("span[name=occupy_type]", obj).html('');
		$("span[name=occupy_name]", obj).html('');
		$("span[name=seal_delivery]", obj).html('');
		$("span[name=seal_pick_up]", obj).html('');
		
		var tmp = $("#pickUpViewTemplate span[name='entry_in_id']").parents("td").html();
		$("span[name=entry_in_id]", obj).parents("td").html(tmp);
	}
}

//取消转换
function pickUpCancel(target){
	
	if(!pickUps){
		
		$("#pick_up_set").css("border","0px solid #999");
	}
	
	var id = $(target).attr("equipment_id")
	
	//换
	var view = $("#pick_up_view_"+id).html();
	var edit = $("#pick_up_edit_"+id).html();
	
	$("#pick_up_view_"+id).html(edit);
	$("#pick_up_edit_"+id).html(view);
}

//pick up 保存
function pickUpUpdate(target, is_check_success){
	
	var id = $(target).attr("equipment_id");
	var obj = $("#pick_up_view_"+id);
	
	var equipment_number = $("input[name=equipment_number]",obj).attr("value");
	var equipment_id = $("input[name=equipment_id]",obj).attr("value");
	
	if(equipment_number == '' || equipment_id == ''){
		
		showMessage("Enter CTNR.","alert");
		return false;
	}
	
	var equipment_type = $("input[name=equip_type]",obj).attr("value");
	var equipment_purpose =  $("input[name=equipment_purpose]",obj).attr("value");
	var before_equipment_id = $("input[name=before_equipment_id]", obj).attr("value");
	var occupy_id = $("input[name=occupy_id]", obj).attr("value");
	var occupy_type = $("input[name=occupy_type]", obj).attr("value");
	
	var para = 
		"check_in_entry_id=<%=entry_id%>"
		+"&equipment_id="+equipment_id
		+"&equipment_number="+equipment_number
		+"&equipment_type="+equipment_type
		+"&equipment_purpose="+equipment_purpose
		+"&before_equipment_id="+before_equipment_id
		+"&occupy_type="+occupy_type
		+"&occupy_id="+occupy_id;
		
			
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowUpdateEquipmentAction.action',
		type:'post',
		dataType:'json',
		timeout:60000,
		cache:false,
		data:para+"&is_check_success="+is_check_success,
		async:false,
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		error:function(serverresponse, status){
  			
			$.unblockUI();
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		},
		success: function(data){
			
			$.unblockUI();
			var flag = data.flag;
			var equipment_this_entry_len = data.equipment_this_entry_len;//本entry已经提交的设备，此值大于0，清空本条添加的数据
			var equipment_not_entry_len  = data.equipment_not_entry_len;//非本entry已经提交的设备，此值大于，弹出框列表数据，问其是否要使其他的left并添加本设备名的设备
			//console.log(equipment_this_entry_len+","+equipment_not_entry_len+","+flag);
			if(flag && flag == '<%=YesOrNotKey.NO%>')
			{
				if(equipment_not_entry_len > 0)
				{
					var equipment_not_entry = data.equipment_not_entry;
					var html_msg = '<table width="500px">';
					html_msg += '<tr width="500px"><td colspan="5">Make sure the under equipment left and add a new one ?</td></tr>';
					html_msg += '<tr height="10px"><td colspan="5"></td></tr>';
					for(var i=0; i<equipment_not_entry_len; i++)
					{
						html_msg += '<tr width="500px">'
										+'<td width="50px" align="left">'+equipment_not_entry[i].equipment_type_value+" :</td>"
										+'<td width="120px" align="left"> '+equipment_not_entry[i].equipment_number+"</td>"
										+'<td width="50px" align="left"> at '+equipment_not_entry[i].resources_type_value+" :</td>"
										+'<td width="80px" align="left"> '+equipment_not_entry[i].resources_name+"</td>"
										+'<td width="140px" align="left"> come with : E'+equipment_not_entry[i].entry_id+"</td>"
									+"</tr>";
					}
					html_msg += "</table>";
					$.artDialog({
					    content: $(html_msg).html(),
					    icon: 'warning',
					    width: 550,
					    height: 70,
					    title:'',
					    opacity: 0.1,
					    lock: true,
					    fixed:true,
					    okVal: 'Confirm',
					    ok: function () {
					    	pickUpUpdate(target, '<%=YesOrNotKey.YES%>');
					    },
					    cancelVal: 'Cancel',
					    cancel:function(){
					    	$("input[name=equipment_number]",obj).val("");
					    }
					});	  
				}
				else if(equipment_this_entry_len > 0)
				{
					showMessage(equipment_number+" exist.","alert");
					$("input[name=equipment_number]",obj).val("");
				}
			}
			else
			{
				$.artDialog.opener.monitorCheckInWindowChanged();
				$.unblockUI();
				loadEquipments();
			}
		}
	});
	
	return true;
}

//编辑转换
function pickUpSwap(target){
	
	if(autoCommit()){
		
		var status = $(target).attr("equipment_status");
		
		if(status != <%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%> && status != <%=CheckInMainDocumentsStatusTypeKey.PROCESSING%>){
			
			showMessage("All Task Closed or Equipment Left.","alert");
			return false;
		}
		
		var id = $(target).attr("equipment_id");
		
		//换
		var view = $("#pick_up_view_"+id).html();
		var edit = $("#pick_up_edit_"+id).html();
		
		$("#pick_up_view_"+id).html(edit);
		$("#pick_up_edit_"+id).html(view);
		
		//查询按钮
	    $("#pickAllPage input[name='equipment_number']").searchInput({
	  		"click":function(event){
	  			
	  			getPickUp($(this).prev());
	  		}
	 	});
	}
}

//删除
function pickUpDelete(obj, is_check_success){
	
	if(autoCommit()){
		
		var status = $(obj).attr("equipment_status");
		
		if(status != <%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%> && status != <%=CheckInMainDocumentsStatusTypeKey.PROCESSING%>){
			
			showMessage("All Task Closed or Equipment Left.","alert");
			return false;
		}
		
		var id = $(obj).attr("equipment_id");
		
		var dialog_content = '';
		var icon_style = 'question';
		if(is_check_success && is_check_success=='<%=YesOrNotKey.YES%>')
		{
			dialog_content = 'You will delete all tasks in this equipment, ';
			icon_style = 'warning';
		}
		$.artDialog({
		    content: dialog_content+'do you want to delete?',
		    icon: icon_style,
		    width: 200,
		    height: 70,
		    title:'',
		    opacity: 0.1,
		    lock: true,
		    fixed:true,
		    okVal: 'Confirm',
		    ok: function () {
		    	$.ajax({
		    		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowDeleteEquipmentByIdAction.action',
		    		type: 'post',
		    		dataType: 'json',
		    		timeout: 60000,
		    		cache:false,
		    		data:"id="+id+"&is_check_success="+is_check_success,
		    		async:false,
		    		beforeSend:function(request){
		    			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    		},
		    		error:function(serverresponse, status){
		      			
		    			$.unblockUI();
		      			showMessage(errorMessage(serverresponse.responseText),"alert");
		      		},
		    		success: function(data){
		    			
		    			$.artDialog.opener.monitorCheckInWindowChanged();
		    			$.unblockUI();
		    			if(data.flag && data.flag == '<%=YesOrNotKey.NO%>')
	    				{
		    				pickUpDelete(obj, '<%=YesOrNotKey.YES%>');
	    				}
		    			else
	    				{
			    			loadEquipments();
	    				}
		    		}
		    	});
		    },
		    cancelVal: 'Cancel',
		});	  
	}
}

function checkEquipments(){
	
	var returnVal = false;
	
	var update1 = $("#dropOffViewPage img[name='updateDropOff']");
	var update2 = $("#drop_off_view_0 img[name='updateDropOff']");
	var update3 = $("#pickUpViewPage img[name='updatePickUp']");
	var update4 = $("#pick_up_view_0 img[name='updatePickUp']");
	var update5 = $("#liveLoadViewPage img[name='updateLiveLoad']");
	var update6 = $("#live_load_view_0 img[name='updateLiveLoad']");
	var update7 = $("#lpViewPage img[name='updateLiveLoad']");
	
	if(update1.length > 0){
		
		returnVal = dropOffUpdate(update1[0]);
		
	}else if(update2.length > 0){
		
		returnVal = dropOffUpdate(update2[0]);
		
	}else if(update3.length > 0){
		
		returnVal = pickUpUpdate(update3[0]);
		
	}else if(update4.length > 0){
		
		returnVal = pickUpUpdate(update4[0]);
		
	}else if(update5.length > 0){
		
		returnVal = liveLoadUpdate(update5[0]);
		//update5.trigger("click");
	}else if(update6.length > 0){
		
		returnVal = liveLoadUpdate(update6[0]);
		//update6.trigger("click");
	}else if(update7.length > 0){
		
		returnVal = liveLoadUpdate(update7[0]);
		//update7.trigger("click");
	}else{
		
		returnVal = true;
	}
	
	return returnVal;
}

//自动提交
function autoCommit(){
	
	var returnVal = false;
	
	var update1 = $("#dropOffViewPage img[name='updateDropOff']");
	var update2 = $("#drop_off_view_0 img[name='updateDropOff']");
	var update3 = $("#pickUpViewPage img[name='updatePickUp']");
	var update4 = $("#pick_up_view_0 img[name='updatePickUp']");
	var update5 = $("#liveLoadViewPage img[name='updateLiveLoad']");
	var update6 = $("#live_load_view_0 img[name='updateLiveLoad']");
	var update7 = $("#lpViewPage img[name='updateLiveLoad']");
	
	if(update1.length > 0){
		
		dropOffUpdate(update1[0]);
		
	}else if(update2.length > 0){
		
		dropOffUpdate(update2[0]);
		
	}else if(update3.length > 0){
		
		pickUpUpdate(update3[0]);
		
	}else if(update4.length > 0){
		
		pickUpUpdate(update4[0]);
	}else if(update5.length > 0){
		
		liveLoadUpdate(update5[0]);
		//update5.trigger("click");
	}else if(update6.length > 0){
		
		liveLoadUpdate(update6[0]);
		//update6.trigger("click");
	}else if(update7.length > 0){
		
		liveLoadUpdate(update7[0]);
		//update7.trigger("click");
	}else{
		
		returnVal = true;
	}
	
	return returnVal;
}

var PICKUPINFO;
//查询pick up
function getPickUp(target){
	
	PICKUPINFO = target;
	
	if(target.val() == ''){
		
		return false;
	}
	var equipmentId = $("input[name=equipment_id]", $(target).parent().parent()).val();
	//console.log("equipmentID:"+equipmentId);
	if(equipmentId == '')
		{
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowInYardDropEquipsByNumberAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:"number="+target.val()+"&entry_id=<%=entry_id%>",
				async:false,
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				error:function(serverresponse, status){
		  			
					$.unblockUI();
		  			showMessage(errorMessage(serverresponse.responseText),"alert");
		  		},
				success: function(data){
					
					$.unblockUI();
					if(data && data.length > 0){
						
						//if(data.length == 1){
							setPickUp(data[0]);
						//}else{
							
		<%-- 					var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_window_select_ctnr.html?number='+target.val(); --%>
						//	$.artDialog.open(uri , {title: "CTNRS:"+target.val(),width:'800px',height:'350px', lock: true,opacity: 0.3,fixed: true});
						//}
					}else{
						
						showMessage("CTNR not found","alert");
					}
				}
			});
		}
	
}

var DEFINELEMENT;
//选择
function selectDoorOrSpot(obj){
	
	DEFINELEMENT = obj;
	
	var occupyType = $("select[name=occupy_type]",$(obj).parents("tr")).val();
	var id = $("input[name=occupy_id]",$(DEFINELEMENT).parents("tr")).val();
	
	if(occupyType==<%=OccupyTypeKey.SPOT%>){
		
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/get_yard_no.html?storageId=<%=row.get("ps_id", 0L)%>'
			+'&yc_id='+id+'&inputId=occupy_name&mainId=<%=entry_id %>';
				
		$.artDialog.open(uri , {title: "Spot",width:'600px',height:'520px', lock: true,opacity: 0.3,fixed: true});
		
	}else if(occupyType==<%=OccupyTypeKey.DOOR%>){
		
	  	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_in_door_list.html?'
	  		+'inputValue='+id+'&inputId=occupy_name&associate_id=<%=entry_id %>';
	  	
	  	$.artDialog.open(uri,{title:"Door and Location",width:'700px',height:'470px',lock:true,opacity:0.3,fixed:true});
	}
}

//停车位回填
function setYardNo(yardId,yardNo,inputId,zone_id){
	
	var obj = $(DEFINELEMENT).parents("tr");
	
	$("input[name=occupy_name]",obj).val(yardNo);
	$("input[name=occupy_id]",obj).val(yardId);
}

//回填门
function getDoor(door,doorName,inputId,zone_id){   
	
	
	var obj = $(DEFINELEMENT).parents("tr");
	
	$("input[name=occupy_name]",obj).val(doorName);
	$("input[name=occupy_id]",obj).val(door);
}

//回填pickup
function setPickUp(liveLoad){
	
	var obj = PICKUPINFO.parents("tr");
	
	$("input[name=equipment_id]",obj).val(liveLoad.equipment_id);
	
	if(liveLoad.resources_type == '<%=OccupyTypeKey.DOOR%>'){
		
		$("span[name=occupy_type]",obj).text("Door :");
		$("span[name=occupy_name]",obj).text(liveLoad.doorid);
		
	}else if(liveLoad.resources_type == '<%=OccupyTypeKey.SPOT%>'){
		
		$("span[name=occupy_type]",obj).text("Spot :");
		$("span[name=occupy_name]",obj).text(liveLoad.yc_no);
	}
	$("input[name=occupy_id]",obj).val(liveLoad.resources_id);
	$("input[name=occupy_type]",obj).val(liveLoad.resources_type);
	
	var tmp = $("#pickUpEditTemplate span[name='entry_in_id']").parents("td").html();
	$("span[name=entry_in_id]",obj).parents("td").html(tmp);
	
	$("span[name=entry_in_id]",obj).text(liveLoad.check_in_entry_id);
	if(liveLoad.seal_delivery) {
		$("span[name=seal_delivery]",obj).text(liveLoad.seal_delivery.toUpperCase());
	}
	
	if(liveLoad.seal_pick_up) {
		$("span[name=seal_pick_up]",obj).text(liveLoad.seal_pick_up.toUpperCase());
	}

	
}

//输入转换成大写
function changeUperCase(obj, event){
	
	$(obj).val($(obj).val().toUpperCase());
}

function findEquipmentById(equip_id, equipType, goType){
	
	var para = 'id='+equip_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowFindEquipmentReOccupyByIdAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:para,
		async:false,
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		error:function(serverresponse, status){
  			
			$.unblockUI();
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		},
		success: function(data){
			
			$.unblockUI();
			editLpOrCtnrOne(data, equipType, goType);
		}
	});
}

function clearOccupy(target){
	
	$(target).parents("tr").find("input[name='occupy_name']").val('');
	$(target).parents("tr").find("input[name='occupy_id']").val('');
}

function swapLiveDropCtnr(target){
	
	if(autoCommit()){
		
		var equipment_id = $(target).data("id");
		var equipment_purpose = $(target).data("type");
		var equipment_number = $(target).data("name");
		
		var type1 = '';
		var type2 = '';
		
		if(equipment_purpose == <%=CheckInLiveLoadOrDropOffKey.LIVE%>){
			
			equipment_purpose = '<%=CheckInLiveLoadOrDropOffKey.TMS%>';
			type1 = 'LiveLoad';
			type2 = 'TMS';
		}else if(equipment_purpose==<%=CheckInLiveLoadOrDropOffKey.TMS%>){
			
			equipment_purpose = '<%=CheckInLiveLoadOrDropOffKey.DROP%>';
			type1 = 'TMS';
			type2 = 'DropOff';
		}else if(equipment_purpose==<%=CheckInLiveLoadOrDropOffKey.DROP%>){
			equipment_purpose = '<%=CheckInLiveLoadOrDropOffKey.LIVE%>';
			type1 = 'DropOff';
			type2 = 'LiveLoad';
		}else{
			return false;
		}
		
		var para = 
			"equipment_id="+equipment_id
			+"&equipment_purpose="+equipment_purpose;
		
		var contentHtml = '<table><tr><td>Change [ '+equipment_number+' ] '+"</td></tr>"
						+'<tr><td>'+type1+' --> '+type2+' ?'+"</td></tr></table>";
		//var content = 'Change [ '+equipment_number+' ] '+type1+' --> '+type2+' ?';
		
		$.artDialog({
		    content: $(contentHtml).html(),//content
		    icon: "question",
		    width: 280,
		    height: 70,
		    opacity: 0.1,
		    lock: true,
		    fixed:true,
		    okVal: 'YES',
		    ok: function () {
		    	$.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/CheckInWindowEquipmentChangeTypeAction.action',
					type:'post',
					dataType:'json',
					timeout:60000,
					cache:false,
					data:para,
					async:false,
					beforeSend:function(request){
						$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
					},
					error:function(serverresponse, status){
						$.unblockUI();
			  			showMessage(errorMessage(serverresponse.responseText),"alert");
			  		},
					success: function(data){
						$.unblockUI();
						loadEquipments();
					}
				});	
		    },
		    cancel:true,
		    cancelVal: 'NO'
		});	
	}
}
</script>
<style type="text/css">
.value_font{font-size: 13px;font-weight: bold;}
</style>
</head>
<body>
<div style="width:958px;height:390px; border:0px solid red;overflow:auto;" id="equipments">
<fieldset style="border:0px solid #999;width:900px; margin-bottom: 10px;">
<div>
	<div id="lpViewPage">
				
	</div>
	<div id="lpEditPage" style="display: none;">
				
	</div>
</div>
</fieldset>


<!-- Live Load 模板 ------------------------------------------------------------>
<fieldset style="border:2px solid #999;width:900px; margin-bottom: 10px;" id="live_load_set">
	<legend align="left">
		<span style="font-size:14px;font-weight:normal;display:block;width:200px;position: relative;height:17px;">
			<span style="display: inline-block;left: 0px;position: absolute">
				Live Load
			</span>
			<span style="display: inline-block;right: 0px;position: absolute;">
				Trailer / CTNR <img src="../imgs/add.png" alt="Add" title="ADD" name="addLiveLoad" onclick="liveLoadAdd(this)">
			</span>
		</span>
		<span id="live_load_legend"></span>
	</legend>
	<div id="liveLoadAllPage">
		
		<div id="liveLoadViewPage">
			
		</div>
		
		<div id="liveLoadAddPage">
			<div id="live_load_view_0">
			
			</div>
			<div id="live_load_edit_0" style="display: none;">
			
			</div>
		</div>
		
		<div id="liveLoadEditPage" style="display: none;">
			
		</div>
	</div>
	<div style="padding-left:9px;margin-top:3px;">
		
	</div>
</fieldset>

<div id="liveLoadViewTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td style="">
				<span class="value_font">CTNR :</span>
			</td>
			<td>
				<span name="equipment_number" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_type_name" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_name" class="value_font"></span>
			</td>
			<td>
				Delivery Seal : <span name="seal_delivery" class="value_font"></span>
			</td>
			<td>
				Pick Up Seal  : <span name="seal_pick_up" class="value_font"></span>
			</td>
			<td>
				<span class="align-width-span">
					<img src="../imgs/application_edit.png" title="EDIT" name="editLiveLoad" onClick="liveLoadSwap(this);">
				</span>
				
				<span class="align-width-span">
					<img src="../imgs/del_append.gif" title="DELETE" name="deleteLiveLoad" onClick="liveLoadDelete(this)" >
				</span>
				
				<span class="align-width-span">
					<img src="../imgs/transform.png" title="DropOff" name="swapLiveLoad" onClick="swapLiveDropCtnr(this)" >
				</span>
			</td>
		</tr>
	</table>
</div>

<div id="liveLoadEditTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td>
				CTNR :
			</td>
			<td>
				<input name="equipment_number" onkeyup="changeUperCase(this, event)">
				<input type="hidden" name="equipment_id">
				<input type="hidden" name="equipment_purpose">
				<input type="hidden" name="equip_type" value="<%=CheckInTractorOrTrailerTypeKey.TRAILER%>">
			</td>
			
			<td>
				<select style="width:90px;" name="occupy_type" onChange="clearOccupy(this);">
					<%for(int i = 0; i < occupyTypeKeys.size(); i ++){ %>
						<option value="<%=occupyTypeKeys.get(i)%>">
							<%=occupyTypeKey.getOccupyTypeKeyName(String.valueOf(occupyTypeKeys.get(i))) %>
						</option>
					<%}%>
				</select>
			</td>
			<td>
				<input name="occupy_name" readonly="true" onclick="selectDoorOrSpot(this)"/>
				<input type="hidden" name="occupy_id" >
			</td>
			
			<!-- <td>
				<span name="occupy_type_name"></span>
				<input type="hidden" name="occupy_type">
			</td>
			<td>
				<span name="occupy_name"></span>
				<input type="hidden" name="occupy_id">
			</td> -->
			
			<td>
				<input name="seal_delivery" onkeyup="changeUperCase(this, event)"/>
			</td>
			<td>
				<input name="seal_pick_up" onkeyup="changeUperCase(this, event)"/>
			</td>
			<td>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_ok.gif" alt="Update" title="UPDATE" name="updateLiveLoad" onClick="liveLoadUpdate(this);">
				</span>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_error.gif" alt="Cancel" title="CANCEL" name="cancelLiveLoad" onClick="liveLoadCancel(this);">
				</span>
			</td>
		</tr>
	</table>
</div>

<div id="lpViewTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td style="">
				<span class="value_font">LP :</span>
			</td>
			<td>
				<span name="equipment_number" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_type_name" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_name" class="value_font"></span>
			</td>
			<td>
				Delivery Seal : <span name="seal_delivery" class="value_font"></span>
			</td>
			<td>
				Pick Up Seal  : <span name="seal_pick_up" class="value_font"></span>
			</td>
			<td>
				<span class="align-width-span">
				<img src="../imgs/application_edit.png" alt="Edit" title="EDIT" name="editLiveLoad" onClick="liveLoadSwap(this);">
				</span>
			</td>
		</tr>
	</table>
</div>

<div id="lpEditTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td>
				<spa class="value_font">LP :</spa>
			</td>
			<td>
				<input name="equipment_number" onkeyup="changeUperCase(this, event)">
				
				<input type="hidden" name="equipment_id">
				<input type="hidden" name="equipment_purpose">
				<input type="hidden" name="equip_type" value="<%=CheckInTractorOrTrailerTypeKey.TRACTOR%>">
			</td>
			<td>
				<select style="width:90px;" name="occupy_type" onChange="clearOccupy(this);">
					<%for(int i = 0; i < occupyTypeKeys.size(); i ++){ %>
						<option value="<%=occupyTypeKeys.get(i)%>">
							<%=occupyTypeKey.getOccupyTypeKeyName(String.valueOf(occupyTypeKeys.get(i))) %>
						</option>
					<%}%>
				</select>
			</td>
			<td>
				<input name="occupy_name" readonly="true" onclick="selectDoorOrSpot(this)"/>
				<input type="hidden" name="occupy_id" >
			</td>
			
			<!-- <td>
				<span name="occupy_type_name"></span>
				<input type="hidden" name="occupy_type">
			</td>
			<td>
				<span name="occupy_name"></span>
				<input type="hidden" name="occupy_id">
			</td> -->
			<td>
				<input name="seal_delivery" onkeyup="changeUperCase(this, event)"/>
			</td>
			<td>
				<input name="seal_pick_up" onkeyup="changeUperCase(this, event)"/>
			</td>
			<td>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_ok.gif" alt="Update" title="UPDATE" name="updateLiveLoad" onClick="liveLoadUpdate(this);">
				</span>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_error.gif" alt="Cancel" title="CANCEL" name="cancelLiveLoad" onClick="liveLoadCancel(this);">
				</span>
			</td>
		</tr>
	</table>
</div>

<!-- TMS 模板 ------------------------------------------------------------>
<fieldset style="border:2px solid #999;width:900px; margin-bottom: 10px;" id="tms_set">
	<legend align="left">
		<span style="font-size:14px;font-weight:normal;display:block;width:200px;position: relative;height:17px;">
			<span style="display: inline-block;left: 0px;position: absolute">
				TMS
			</span>
			<span style="display: inline-block;right: 0px;position: absolute;">
				Trailer / CTNR <img src="../imgs/add.png" alt="Add" title="ADD" name="addTms" onclick="tmsAdd(this)">
			</span>
		</span>
		<span id="tms_legend"></span>
	</legend>
	<div id="tmsAllPage">
		
		<div id="tmsViewPage">
			
		</div>
		
		<div id="tmsAddPage">
			<div id="tms_view_0">
			
			</div>
			<div id="tms_edit_0" style="display: none;">
			
			</div>
		</div>
		
		<div id="tmsEditPage" style="display: none;">
			
		</div>
	</div>
	<div style="padding-left:9px;margin-top:3px;">
		
	</div>
</fieldset>

<div id="tmsViewTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td style="">
				<span class="value_font">CTNR :</span>
			</td>
			<td>
				<span name="equipment_number" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_type_name" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_name" class="value_font"></span>
			</td>
			<td>
				Delivery Seal : <span name="seal_delivery" class="value_font"></span>
			</td>
			<td>
				Pick Up Seal  : <span name="seal_pick_up" class="value_font"></span>
			</td>
			<td>
				<span class="align-width-span">
					<img src="../imgs/application_edit.png" title="EDIT" name="editTms" onClick="tmsSwap(this);">
				</span>
				
				<span class="align-width-span">
					<img src="../imgs/del_append.gif" title="DELETE" name="deleteTms" onClick="tmsDelete(this)" >
				</span>
				
				<span class="align-width-span">
					<img src="../imgs/transform.png" title="DropOff" name="swapTms" onClick="swapLiveDropCtnr(this)" >
				</span>
			</td>
		</tr>
	</table>
</div>

<div id="tmsEditTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td>
				CTNR :
			</td>
			<td>
				<input name="equipment_number" onkeyup="changeUperCase(this, event)">
				<input type="hidden" name="equipment_id">
				<input type="hidden" name="equipment_purpose">
				<input type="hidden" name="equip_type" value="<%=CheckInTractorOrTrailerTypeKey.TRAILER%>">
			</td>
			
			<td>
				<select style="width:90px;" name="occupy_type" onChange="clearOccupy(this);">
					<%for(int i = 0; i < occupyTypeKeys.size(); i ++){ %>
						<option value="<%=occupyTypeKeys.get(i)%>">
							<%=occupyTypeKey.getOccupyTypeKeyName(String.valueOf(occupyTypeKeys.get(i))) %>
						</option>
					<%}%>
				</select>
			</td>
			<td>
				<input name="occupy_name" readonly="true" onclick="selectDoorOrSpot(this)"/>
				<input type="hidden" name="occupy_id" >
			</td>
			
			<!-- <td>
				<span name="occupy_type_name"></span>
				<input type="hidden" name="occupy_type">
			</td>
			<td>
				<span name="occupy_name"></span>
				<input type="hidden" name="occupy_id">
			</td> -->
			
			<td>
				<input name="seal_delivery" onkeyup="changeUperCase(this, event)"/>
			</td>
			<td>
				<input name="seal_pick_up" onkeyup="changeUperCase(this, event)"/>
			</td>
			<td>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_ok.gif" alt="Update" title="UPDATE" name="updateTms" onClick="tmsUpdate(this);">
				</span>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_error.gif" alt="Cancel" title="CANCEL" name="cancelTms" onClick="tmsCancel(this);">
				</span>
			</td>
		</tr>
	</table>
</div>

<div id="lpViewTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td style="">
				<span class="value_font">LP :</span>
			</td>
			<td>
				<span name="equipment_number" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_type_name" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_name" class="value_font"></span>
			</td>
			<td>
				Delivery Seal : <span name="seal_delivery" class="value_font"></span>
			</td>
			<td>
				Pick Up Seal  : <span name="seal_pick_up" class="value_font"></span>
			</td>
			<td>
				<span class="align-width-span">
				<img src="../imgs/application_edit.png" alt="Edit" title="EDIT" name="editTms" onClick="tmsSwap(this);">
				</span>
			</td>
		</tr>
	</table>
</div>

<div id="lpEditTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td>
				<spa class="value_font">LP :</spa>
			</td>
			<td>
				<input name="equipment_number" onkeyup="changeUperCase(this, event)">
				
				<input type="hidden" name="equipment_id">
				<input type="hidden" name="equipment_purpose">
				<input type="hidden" name="equip_type" value="<%=CheckInTractorOrTrailerTypeKey.TRACTOR%>">
			</td>
			<td>
				<select style="width:90px;" name="occupy_type" onChange="clearOccupy(this);">
					<%for(int i = 0; i < occupyTypeKeys.size(); i ++){ %>
						<option value="<%=occupyTypeKeys.get(i)%>">
							<%=occupyTypeKey.getOccupyTypeKeyName(String.valueOf(occupyTypeKeys.get(i))) %>
						</option>
					<%}%>
				</select>
			</td>
			<td>
				<input name="occupy_name" readonly="true" onclick="selectDoorOrSpot(this)"/>
				<input type="hidden" name="occupy_id" >
			</td>
			
			<!-- <td>
				<span name="occupy_type_name"></span>
				<input type="hidden" name="occupy_type">
			</td>
			<td>
				<span name="occupy_name"></span>
				<input type="hidden" name="occupy_id">
			</td> -->
			<td>
				<input name="seal_delivery" onkeyup="changeUperCase(this, event)"/>
			</td>
			<td>
				<input name="seal_pick_up" onkeyup="changeUperCase(this, event)"/>
			</td>
			<td>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_ok.gif" alt="Update" title="UPDATE" name="updateTms" onClick="tmsUpdate(this);">
				</span>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_error.gif" alt="Cancel" title="CANCEL" name="cancelTms" onClick="tmsCancel(this);">
				</span>
			</td>
		</tr>
	</table>
</div>

<!-- Drop Off 模板 ------------------------------------------------------------>
<fieldset style="border:2px solid #999;width:900px; margin-bottom: 10px;" id="drop_off_set">
	<legend align="left">
	
		<span style="font-size:14px;font-weight:normal;display:block;width:200px;position: relative;height:17px;">
			<span style="display: inline-block;left: 0px;position: absolute">
				Drop Off
			</span>
			<span style="display: inline-block;right: 0px;position: absolute;">
				Trailer / CTNR <img src="../imgs/add.png" alt="Add" title="ADD" name="addDropOff" onclick="dropOffAdd(this)">
			</span>
		</span>
		<span id="drop_off_legend"></span>
	</legend>
	<div>
		<div id="dropOffViewPage">
			
		</div>
		
		<div id="dropOffAddPage">
			<div id="drop_off_view_0">
			
			</div>
			<div id="drop_off_edit_0" style="display: none;">
			
			</div>
		</div>
		
		<div id="dropOffEditPage" style="display: none;">
			
		</div>
	</div>
	<div style="padding-left:9px;margin-top:3px;">
		
	</div>
</fieldset>

<div id="dropOffViewTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td style="">
				<span class="value_font">CTNR :</span>
			</td>
			<td>
				<span name="equipment_number" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_type_name" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_name" class="value_font"></span>
			</td>
			<td>
				Delivery Seal : <span name="seal_delivery" class="value_font"></span>
			</td>
			<td>
				Pick Up Seal  : <span name="seal_pick_up" class="value_font"></span>
			</td>
			<td>
				<span class="align-width-span">
					<img src="../imgs/application_edit.png" alt="Edit" title="EDIT" name="editDropOff" onClick="dropOffSwap(this);">
				</span>
				<span class="align-width-span">
					<img src="../imgs/del_append.gif" alt="Delete" title="DELETE" name="deleteDropOff" onClick="dropOffDelete(this)" >
				</span>
				<span class="align-width-span">
					<img src="../imgs/transform.png" title="LiveLoad" name="swapLiveLoad" onClick="swapLiveDropCtnr(this)" >
				</span>
			</td>
		</tr>
	</table>
</div>

<div id="dropOffEditTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td>
				<span class="value_font">CTNR :</span>
			</td>
			<td>
				<input name="equipment_number" onkeyup="changeUperCase(this, event)">
				
				<input type="hidden" name="equipment_id">
				<input type="hidden" name="equip_type" value="<%=CheckInTractorOrTrailerTypeKey.TRAILER%>">
				<input type="hidden" name="equipment_purpose">
			</td>
			<td>
				<select style="width:90px;" name="occupy_type" onChange="clearOccupy(this);">
					<%for(int i = 0; i < occupyTypeKeys.size(); i ++){ %>
						<option value="<%=occupyTypeKeys.get(i)%>">
							<%=occupyTypeKey.getOccupyTypeKeyName(String.valueOf(occupyTypeKeys.get(i))) %>
						</option>
					<%}%>
				</select>
			</td>
			<td>
				<input name="occupy_name" readonly="true" onclick="selectDoorOrSpot(this)"/>
				<input type="hidden" name="occupy_id" >
			</td>
			<td>
				<input name="seal_delivery" onkeyup="changeUperCase(this, event)"/>
			</td>
			<td>
				<input name="seal_pick_up" onkeyup="changeUperCase(this, event)"/>
			</td>
			<td>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_ok.gif" alt="Update" title="UPDATE" name="updateDropOff" onClick="dropOffUpdate(this);">
				</span>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_error.gif" alt="Cancel" title="CANCEL" name="cancelDropOff" onClick="dropOffCancel(this);">
				</span>
			</td>
		</tr>
	</table>
</div>

<!-- Pick Up 模板 ------------------------------------------------------------>
<fieldset style="border:2px solid #999;width:900px; margin-bottom: 20px;" id="pick_up_set">
	<legend align="left">
		<span style="font-size:14px;font-weight:normal;display:block;width:200px;position: relative;height:17px;">
			<span style="display: inline-block;left: 0px;position: absolute">
				Pick Up
			</span>
			<span style="display: inline-block;right: 0px;position: absolute;">
				Trailer / CTNR <img src="../imgs/add.png" alt="Add" title="ADD" name="addPickUp" onclick="pickUpAdd(this)">
			</span>
		</span>
		<span id="pick_up_legend"></span>
	</legend>
	<div id="pickAllPage">
		<div id="pickUpViewPage">
			
		</div>
		
		<div id="pickUpAddPage">
			<div id="pick_up_view_0">
			
			</div>
			<div id="pick_up_edit_0" style="display: none;">
			
			</div>
		</div>
		
		<div id="pickUpEditPage" style="display: none;">
			
		</div>
	</div>
	<div style="padding-left:9px;margin-top:3px;">
		
	</div>
</fieldset>

<div id="pickUpViewTemplate" style="display: none;">
	<table class="lp_ctnr_cls">
		<tr>
			<td>
				<span class="value_font">CTNR :</span>
			</td>
			<td>
				<span name="equipment_number" class="value_font"></span>
			</td>
			<td>
				<span name="occupy_type" class="value_font"></span>
				<span name="occupy_name" class="value_font"></span>
			</td>
			<td>
				<a href="javascript:void(0)" onclick="pickUpCtnrEntry(this)">
					Entry In : <span name="entry_in_id" class="value_font"></span>
				</a>
			</td>
			<td>
				Delivery Seal : <span name="seal_delivery" class="value_font"></span>
			</td>
			<td>
				Pick Up Seal  : <span name="seal_pick_up" class="value_font"></span>
			</td>
			<td>
				<span class="align-width-span">
				<img src="../imgs/application_edit.png" alt="Edit" title="EDIT" name="editPickUp" onClick="pickUpSwap(this);">
				</span>
				<span class="align-width-span">
				<img src="../imgs/del_append.gif" alt="Delete" title="DELETE" name="deletePickUp" onClick="pickUpDelete(this)" >
				</span>
			</td>
		</tr>
	</table>
</div>

<div id="pickUpEditTemplate" style="display: none;">
	
	<table class="lp_ctnr_cls">
		<tr>
			<td>
				<span class="value_font">CTNR :</span>
			</td>
			<td>
			<!--  -->
				<input name="equipment_number" onfocus="selectCommon(this)"  onkeypress="pickUpKeyPress(event);" onkeyup="changeUperCase(this,event);clearPickUp(this,event);">
				<input type="hidden" name="equipment_id">
				<input type="hidden" name="before_equipment_id">
				
				<input type="hidden" name="equip_type" value="<%=CheckInTractorOrTrailerTypeKey.TRAILER%>">
				<input type="hidden" name="equipment_purpose">
			</td>
			<td>
				<span name="occupy_type"></span>
				
				<span name="occupy_name"></span>
				<input type="hidden" name="occupy_id" >
				<input type="hidden" name="occupy_type" >
			</td>
			<td>
				<a href="javascript:void(0)" onclick="pickUpCtnrEntry(this)">
					Entry In : <span name="entry_in_id" class="value_font"></span>
				</a>
			</td>
			<td>
				Delivery Seal : <span name="seal_delivery" class="value_font"></span>
			</td>
			<td>
				Pick Up Seal : <span name="seal_pick_up" class="value_font"></span>
			</td>
			<td>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_ok.gif" alt="Update" title="UPDATE" name="updatePickUp" onClick="pickUpUpdate(this);">
				</span>
				<span class="align-width-span">
				<img src="../imgs/standard_msg_error.gif" alt="Cancel" title="CANCEL" name="cancelPickUp" onClick="pickUpCancel(this);">
				</span>
			</td>
		</tr>
	</table>
</div>
</div>
<div align="right" style="padding:5px;margin-top:2px;">
	<input id="previousStep" class="normal-green" type="button" onclick="tabSelect(0);" value="Previous">
	<input id="nextStep" class="normal-green" type="button" onclick="tabSelect(2);" value="Next Step">
</div>
<!-- <div id="error_msg"></div> -->
</body>
<script type="text/javascript">
function pickUpCtnrEntry(obj)
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_index.html?cmd=search';
	var entryId = $("span[name=entry_in_id]", $(obj)).text();
	if(entryId && entryId != '' && entryId != '0')
	{
		url += "&key="+entryId;
		window.open(url);
	}
}
</script>
</html>

