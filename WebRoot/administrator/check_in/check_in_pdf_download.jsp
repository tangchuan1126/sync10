<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<%@page import="com.cwc.app.key.YesOrNotKey"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 提示信息 -->
<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<%
	 long entryId=StringUtil.getLong(request,"infoId");
	String serverName = request.getServerName();
// System.out.print("http://"+serverName+"/Sync10/_fileserv_print/file/");
//     AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
    DBRow[] rows = checkInMgrZwb.getFileIdByEntryId(entryId,request);
//     System.out.println(StringUtil.convertDBRowArrayToJsonString(rows));
 %>

<script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<title>Check in pdf download</title>
</head>
<body>
	<fieldset style="border:2px #cccccc solid;margin:10px;width:600px;">
		<legend style="font-size:17px;font-weight:normal;color:#f60;">
				E<%=entryId%>
		</legend>
	
<% 
	if(rows!=null&&rows.length>0){
%>
<%
	for(DBRow row : rows){
		DBRow[] details = (DBRow[])row.get("details");
		String loadNo = row.get("number","");
		String dnFlag = row.get("dnFlag","");
		String signFlag = row.get("signFlag","");
		String counting = row.get("counting","");
		String customerId = row.get("customer_id","");
		int numberType = row.get("number_type",0);
		long detailId = row.get("dlo_detail_id",0L);
%>
	<fieldset style="border:2px #cccccc solid;margin:10px;width:94%;">	
			<legend style="font-size:17px;font-weight:normal;color:#f60;">
				<div style="float: left;margin-top: 5px;">
					<span style="font-size: 14px;font-weight: bold;color:blue;font-family:Arial;">&nbsp;<%=moduleKey.getModuleName(numberType) %>:&nbsp;&nbsp;<%=loadNo %></span>
					<span style="font-size: 12px;font-weight: bold;font-family:Arial;">&nbsp;(<%=customerId %>)</span>
					&nbsp;				
				</div>
				<%if("fail".equals(dnFlag)){ %>
						<img src="imgs/warning.png" style="margin-left: 20px;margin-top:5px; float: left;">&nbsp;
				<% }%>
				<%if("fail".equals(signFlag)){ %>
					<img src="imgs/sign.png" style="margin-left: 20px;margin-top:5px; float: left;">&nbsp;
				<% }%>
				<%if("fail".equals(counting)){ %>
					<img src="imgs/camera_pdf.png" style="margin-left: 20px;margin-top:5px; float: left;">&nbsp;
				<% }%>	
				
				<%if("success".equals(signFlag)){ %>
					<img src="imgs/create_1.png" style="margin-left: 20px;margin-top:5px; float: left;" onclick="createPDF('<%=entryId%>','<%=detailId%>','<%=customerId %>')"  onmouseover="mouseOverCreate(this);" onmouseout="mouseOutCreate(this);">&nbsp;
				<% }%>	
				
				
				
			</legend>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
		<% if(details!=null&&details.length>0){
			for(DBRow r : details){
				String fileId = r.get("file_id","");
				String dn = r.get("dn","");
				int ftpFlag = r.get("flag",0);
				
				if(!"".equals(fileId)){
			%>
	  			<tr>
		  			<td width="35%">
			  			<span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;padding-left:55px;">DN:&nbsp;<%=dn %></span>
		  			</td>
					<td width="45%">
						<img src="imgs/pdf.png" style="margin-left: 50px;margin-bottom:5px;margin-top:5px; float: left;" width="25px;" height="25px;">&nbsp;
						<div style="float: left;margin-top: 10px;">&nbsp;&nbsp;<%=dn+".pdf"%></div>
				    </td>
				
				    <td width="20%" style="text-align: center;">
				    	<% 
				    		if(ftpFlag==YesOrNotKey.NO){
				    	%>
<!-- 				    		<img src="imgs/up_1.png" style="margin-left: 5px;" onmouseover="mouseOverUp(this);" onmouseout="mouseOutUp(this);"> -->
				    	<%		
				    		}
				    	%>
				    	
				    	<a href='http://<%=serverName%>/Sync10/_fileserv_print/download/<%=fileId%>.pdf'  style="color:blue" >
			       			<img src="imgs/down_load_1.png" style="margin-left: 5px;" onmouseover="mouseOver(this);" onmouseout="mouseOut(this);">
<!-- 			       			<img src="imgs/download2.png" style="margin-left: 5px;" onmouseover="mouseOver(this);" onmouseout="mouseOut(this);"> -->
			       		</a>
				    </td>
				</tr>
  			<%		
  				}else{
  			%>
  				<tr>
		  			<td colspan="3" style="height: 30px;">
				  		<span style="font-size: 12px;color:red;font-weight: bold;font-family:Arial;padding-left:55px;">DN:&nbsp;<%=dn %></span>
			  		</td>
			  	</tr>
	  		<%		
  				}
  			 }
		}
%>
	</table>
	</fieldset>	
<%}
}else{
%>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
		 <tr>
	      <td style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;" colspan="3">NO PDF</td>
	    </tr>
    </table>
<%}%>
</fieldset>
	<div id="printHtml" style="display: none;"></div>
</body>
</html>
<script type="text/javascript" class="source">
	function mouseOverUp(obj){
		$(obj).attr("src","imgs/up_2.png");
	}
	function mouseOutUp(obj){
		$(obj).attr("src","imgs/up_1.png");
	}
	function mouseOver(obj){
		$(obj).attr("src","imgs/down_load_2.png");
	}
	function mouseOut(obj){
		$(obj).attr("src","imgs/down_load_1.png");
	}
	function mouseOverCreate(obj){
		$(obj).attr("src","http://<%=serverName%>/Sync10/administrator/check_in/imgs/create_2.png");
	}
	function mouseOutCreate(obj){
		$(obj).attr("src","http://<%=serverName%>/Sync10/administrator/check_in/imgs/create_1.png");
	}


	function getUrlParam(searchStr,name) {
        var reg = new RegExp("(^|&)?" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = searchStr.match(reg);  //匹配目标参数
        if (r != null) return unescape(r[2]); return null; //返回参数值
    }

	
	/*把entry和detail_id下的masterBol、bol上传为pdf*/
	function createPDF(entry_id,detail_id,customer_id){
		
		$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/DeleteExistPrintedLabelAction.action',
			async:false,
			dataType:'json',
			data:'entry_id='+entry_id+'&detail_id='+detail_id+'&customer_id='+customer_id+'&manual=true',
			beforeSend:function(request){
		     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});//添加遮罩
			},
			success:function(data){
				location.replace(location.href);
				$.unblockUI();//关闭遮罩
			},
			error:function(e){
				$.unblockUI();//关闭遮罩
				showMessage("系统错误","error");
			}

		});
		//根据参数获取masterbol和bol签的url路径
		/**$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/getPrintUrlByParam.action',
			async:false,
			dataType:'json',
			data:'entry_id='+entry_id+'&detail_id='+detail_id+'&customer_id='+customer_id,
			beforeSend:function(request){
		     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});//添加遮罩
			},
			success:function(data){
				//将masterbol的签上传pdf
				if(data&&data.masterbolurl){
					for(i=0;i<data.masterbolurl.length;i++){
						var printUrl = data.masterbolurl[i].printurl;
 						$.ajax({
							url:printUrl,
							async:false,
							dataType:'html',
							data:'',
							success:function(html){
		 						$('#printHtml').html(html);
		 						androidToPdf();
							},
							error:function(){$.unblockUI();showMessage("系统错误","error");}
						});
					}
				}
				
				//将bol的签上传pdf
				if(data&&data.bolurl){
					for(j=0;j<data.bolurl.length;j++){
						var printUrl = data.bolurl[j].printurl;
						var uploadUrl='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/Html2PdfUploadAction.action';
// 						console.log(printUrl);
						$.ajax({
							url:printUrl,
							async:false,
							dataType:'html',
							data:'',
							success:function(html){
		 						$('#printHtml').html(html);
		 						androidToPdf();
							},
							error:function(){$.unblockUI();showMessage("系统错误","error");}
						});
					}
				}
				if(data&&((data.masterbolurl&&data.bolurl.length>0)||(data.bolurl&&data.masterbolurl.length>0))){
					$.ajax({
						url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/addCountingSheetToPdf.action',
						//url:'',
						async:false,
						dataType:'json',
						data:'entry_id='+entry_id+'&detail_id='+detail_id+'&customer_id='+customer_id,
						success:function(data){}
					});
				}
				
				location.replace(location.href);
				$.unblockUI();//关闭遮罩
			},
			error:function(e){
				$.unblockUI();//关闭遮罩
				showMessage("系统错误","error");
			}
		});**/
			
	}
	
</script>
