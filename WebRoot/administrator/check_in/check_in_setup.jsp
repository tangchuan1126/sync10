<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Check In SetUp</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 添加遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 根据分辨率弹出框 -->
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<%
DBRow[] rows = checkInMgrZr.getCheckInLoadBarSetup();
%>
<script>
 $.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '34%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

//文件上传
 function uploadFile(_target){
     var fileNames = $("input[name='file_names']").val();
     var obj  = {
	     reg:"xls",
	     target:_target,
	     limitSize:2,
	     limitNum:1
	 }
     var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
 	uri += jQuery.param(obj);
 	 if(fileNames && fileNames.length > 0 ){
 		uri += "&file_names=" + fileNames;
 	}
 	 $.artDialog.open(uri , {id:'file_up',title: 'Upload Excel',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
     		 close:function(){
 					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
 					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
    		 }});
 }

//jquery file up 回调函数
 function uploadFileCallBack(fileNames,_target){
	//console.log("fileNames="+fileNames+"     _target="+_target);
	if($.trim(fileNames).length > 0)
	{
		if(_target=="load_bar_set"){
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/check_in/check_in_setup_show.html?fileNames="+fileNames;
			$.artDialog.open(uri,{title: "检测上传Load Bar关系信息",width:'850px',height:'530px', lock: true,opacity: 0.3,fixed: true});
		}else{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/check_in/check_in_setup_bol_show.html?fileNames="+fileNames;
			$.artDialog.open(uri,{title: "检测上传Bol关系信息",width:'850px',height:'530px', lock: true,opacity: 0.3,fixed: true});
		}
		
	}
 }
 
 //刷新界面
 function refreshWindow(){
	window.location.reload();
}
 	
 	
</script>
</head>
<body onload="onLoadInitZebraTable()">
	<div id="tabs">
		<ul>
			<li><a href="#load_bar_set">Load Bar Set</a></li>
			<li><a href="#bol_set"> Bol Set</a></li>
		</ul>
	    <div id="load_bar_set" style="text-align: right;">
	      	<input type="button" id="uploadfile" class="long-long-button-yellow" onclick="uploadFile('load_bar_set');" value="导入Load Bar数据" onkeypress="if(event.keyCode==13){uploadFile('jquery_file_up');return false;}"/>
	   		<br/>
	   		<br/>
	   		<table class="zebraTable" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
				<tr> 
					<th width="15%" class="right-title" style="vertical-align: center;text-align: center;">Company(Facility)</th>
				 	<th width="20%" class="right-title" style="vertical-align: center;text-align: center;">Customer</th>
				 	<th width="15%" class="right-title"  style="vertical-align: center;text-align: center;">Supplier(Title)</th>
					<th width="15%" class="right-title"  style="vertical-align: center;text-align: center;">Account (ship to)</th>
					<th width="15%" class="right-title"  style="vertical-align: center;text-align: center;">Freight Term</th>
					<th width="20%" class="right-title"  style="vertical-align: center;text-align: center;">LOAD BAR</th>
				</tr>
				<%
				if(rows!=null){
					for(int i = 0; i < rows.length; i ++){
					%>
				  	<tr align="center" valign="middle">
						<td height="40" align="center">&nbsp;<%=rows[i].getString("company_id")%></td>
						<td height="40" align="center">&nbsp;<%=rows[i].getString("customer_id")%></td>
						<td height="40" align="center">&nbsp;<%=rows[i].getString("title_name")%></td>
						<td height="40" align="center">&nbsp;<%=rows[i].getString("ship_to_name")%></td>
						<td height="40" align="center">&nbsp;<%=rows[i].getString("freight_term")%></td>
						<td height="40" align="center">&nbsp;<%=rows[i].getString("load_bar_name")%></td>
				  	</tr>
			  		<%}
				}%>
  			</table>
	    </div>
	    <div id="bol_set" style="text-align: right;">
	      	<input type="button" id="uploadfile" class="long-long-button-yellow" onclick="uploadFile('bol_set');" value="导入Bol数据" onkeypress="if(event.keyCode==13){uploadFile('jquery_file_up');return false;}"/>
			<br/>
	   		<br/>
			<table class="zebraTable" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
				<tr> 
					<th width="15%" class="right-title"  style="vertical-align: center;text-align: center;">Company(Facility)</th>
				 	<th width="15%" class="right-title" style="vertical-align: center;text-align: center;">Customer</th>
				 	<th width="15%" class="right-title"  style="vertical-align: center;text-align: center;">Supplier(Title)</th>
					<th width="15%" class="right-title"  style="vertical-align: center;text-align: center;">Account (ship to)</th>
					<th width="15%" class="right-title"  style="vertical-align: center;text-align: center;">Freight Term</th>
					<th width="10%" class="right-title"  style="vertical-align: center;text-align: center;">MBOL</th>
					<th width="15%" class="right-title"  style="vertical-align: center;text-align: center;">BOL</th>
				</tr>
				<%
				DBRow[] bolRows = checkInMgrZr.getCheckInBolSetup();
				if(bolRows!=null){
					for(int i = 0; i < bolRows.length; i ++){
					%>
				  	<tr align="center" valign="middle">
						<td height="40" align="center" >&nbsp;<%=bolRows[i].getString("company_id")%></td>
						<td height="40" align="center" >&nbsp;<%=bolRows[i].getString("customer_id")%></td>
						<td height="40" align="center" >&nbsp;<%=bolRows[i].getString("title_name")%></td>
						<td height="40" align="center" >&nbsp;<%=bolRows[i].getString("ship_to_name")%></td>
						<td height="40" align="center" >&nbsp;<%=bolRows[i].getString("freight_term")%></td>
						<td height="40" align="center" >&nbsp;<%=bolRows[i].get("master_bol",0l)==1?"NO":"YES"%></td>
						<td height="40" align="center" >&nbsp;<%=bolRows[i].get("bol",0l)==1?"NO":"YES"%></td>
					  </tr>
			  		<%}
				}%>
  			</table>
		
		</div>
     </div>
     <input type="hidden" name="file_names" id="file_names"/>
</body>

 <script>
		$("#tabs").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
			load: function(event, ui) {onLoadInitZebraTable();}
		});
</script>
</html>

