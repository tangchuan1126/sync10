<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long adminId=adminLoggerBean.getAdid();
	//System.out.println(adminId);
	DBRow[] titleRow=checkInMgrZwb.findTitleByAdminId(adminId);
	
	String load=StringUtil.getString(request, "load");
	long entry_id=StringUtil.getLong(request, "entry_id");
%>
<title>Title List</title>
<script type="text/javascript">
	function openLableTemplate(title_id){
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_load_lable_template.html?load=<%=load%>&entry_id=<%=entry_id%>&title_id='+title_id;
		 $.artDialog.open(url, {title: "Lable Template",width:'800px',height:'600px', lock: false,opacity: 0.3,fixed: true});
	}
</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<table width="98%" align="center" border="0" cellspacing="0" cellpadding="0">		
		<tr>
			<td>
				<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0"  class="zebraTable" >
					<tr> 
				        <th width="15%" class="left-title" style="vertical-align:center;text-align:center;">Name</th>
				        <th width="" class="left-title" style="vertical-align: center;text-align: center;">Logo</th>				       
					</tr>
					<%for(int i=0;i<titleRow.length;i++){ %>
					<tr onclick="openLableTemplate(<%=titleRow[i].get("title_id",0l) %>)">
						<td width="220" nowrap="nowrap" align="center" valign="middle" >
							<%=titleRow[i].get("title_name","") %>
						</td>
						<td align="center" valign="middle" style="border-left:2px #eeeeee solid;border-bottom:2px #eeeeee solid;padding-top:5px;padding-bottom:5px;">
							<img src="imgs/vvme_logo.jpg">
						</td>
					</tr>
					<%} %>
				</table>
			</td>
		</tr>		
	</table>
</body>
</html>

