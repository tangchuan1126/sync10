<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<html>
	<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<head>
<%
	long id=StringUtil.getLong(request,"id");
	long zoneId=StringUtil.getLong(request,"zoneId");
	DBRow row=checkInMgrZwb.findMainById(id);
	//long zoneId=row.get("zone_id",0l);
%>

<title>Check In</title>
</head>
<body>
	<div style="height:430px; border:0px solid red;overflow:auto">
	    <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:770px;">
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td>
			   		 Entry ID:&nbsp;<span id="mId"><%=id %></span>
			   		 	<%if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.PICK_UP){%>
			   		 	      <span id="entryType" style="color:blue;font-weight: bold;">PICK UP</span>
			   		 	<%}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.DELIVERY){%>
			   		 	 	  <span id="entryType" style="color:blue;font-weight: bold;">DELIVERY</span>
			   		 	<%}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.BOTH){ %>
			   		 		  <span id="entryType" style="color:blue;font-weight: bold;">BOTH</span>
			   		 	<%}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.NONE){ %>
			   		 		 <span id="entryType" style="color:blue;font-weight: bold;">NONE</span>
			   		 	<%} %>
			   		 	<select id="relType">		   
							  <option value="1" <%=row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.DELIVERY?"selected":""%> >Delivey</option>
							  <option value="2" <%=row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.PICK_UP?"selected":""%>>PICK UP</option>
							  <option value="3" <%=row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.BOTH?"selected":""%>>Both</option>
							  <option value="4" <%=row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.NONE?"selected":""%>>None</option>
						</select>
						<input class="short-short-button-mod" type="button" onclick="saveRelType()" value="save" name="">
			    </td>
			    <td align="right">			    	
			    	<input type="button" class="long-button-add" value="Add" onclick="add()" />
			    </td>
			  </tr>
			</table>	
	    </div>
	    <br/>
	    <div id="Container" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:770px;">
		  <%DBRow[] winCheckIn = checkInMgrZwb.findOccupancyDetails(id);%>
		  <%for(int i=0;i<winCheckIn.length;i++){%>
		  <%
		  		String number=""; 
		        String ctn_checked="";
			    String bol_checked="";
			    String others_checked="";
			    
			    String pick_up_checked="";
			    String delivery_checked="";
			    String detail_id=winCheckIn[i].get("dlo_detail_id","");
			    long rl_id=0;
			    String door_name="";
			    
		        if(!winCheckIn[i].get("load_number","").equals("")){
		        	number=winCheckIn[i].get("load_number","");
		        	pick_up_checked="checked";
		        }else if(!winCheckIn[i].get("ctn_number","").equals("")){
		        	number=winCheckIn[i].get("ctn_number","");
		        	ctn_checked="selected";
					delivery_checked="checked";
		        }else if(!winCheckIn[i].get("bol_number","").equals("")){
		        	number=winCheckIn[i].get("bol_number","");
		        	bol_checked="selected";
					delivery_checked="checked";
		        }else if(!winCheckIn[i].get("others_number","").equals("")){
		        	number=winCheckIn[i].get("others_number","");
		        	others_checked="selected";
		        	delivery_checked="checked";
		        }
		        if(winCheckIn[i].get("rl_id",0l)!=0){
		        	rl_id=winCheckIn[i].get("rl_id",0l);
		        	door_name=winCheckIn[i].getString("doorId");
		        }
		  %>
		  		<%if(winCheckIn[i].get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.PICK_UP){%>
		   		 	<div id="div<%=i%>">
					   <table width="100%" border="0" cellspacing="0" cellpadding="0" >
							<tr>					 							
							   <td width="100px" rowspan="2" align="right" height="50px">
							  	   <input id="zone<%=i%>" value="<%=zoneId%>" type="hidden" />
							  	   <input id="detailid<%=i%>" value="<%=detail_id%>" type="hidden" />
								   <span name="ckName" >Pick Up</span>
							   	   <input type="radio" name="ra<%=i%>" id="ra" value="0" <%=pick_up_checked%> /></br>
							   	   <span name="ctnName" align="center" >Delivey</span>
						   		   <input type="radio" name="ra<%=i%>" id="ra" value="1" <%=delivery_checked%> />
							   </td>			   
							   <td width="100px" align="center">
							   		<span name="title">LOAD:</span>
							   </td>
							   <td width="200px" align="left" >
							  		<input type="text" name="number" id="<%=i%>"  size="25" value="<%=number%>" />			
							   </td>
							   <td width="270px" align="right" >
							   		Operator:<input type="text" id="tongzhi<%=i%>" name="tongzhi" size="22" />
							   		<input type="hidden" id="tongzhi<%=i%>Id" />
							   </td>	
							   <td rowspan="2" align="center"><input type="button" value="Del" class="short-short-button-del" name="detButton" /></td>		
							</tr>
							<tr>									
								<td align="center">DOOR:</td>
								<td><input type="text" name="door" id="door<%=i%>" size="25" in="<%=i%>"  value="<%=door_name%>" text="<%=rl_id%>" /></td>
								<td align="right">Email:<input id="youjian<%=i%>" type="checkbox" checked="checked">&nbsp;Message:<input id="duanxin<%=i%>" type="checkbox" checked="checked">&nbsp;Browser:<input id="yemian<%=i%>" type="checkbox" checked="checked"></td>								
							</tr>
						 </table>
						 <div>Note：<textarea name="textfield" id="beizhu<%=i%>" cols="93" rows="1"></textarea></div>
						 <hr size="1"/>
				    </div>
	   		 	<%}else if(winCheckIn[i].get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.DELIVERY){%>
	   		 	     <div id="div<%=i%>">
					   <table width="100%" border="0" cellspacing="0" cellpadding="0" >
							<tr>
							   <td width="8%"  rowspan="2" height="50px">Entry ID:</td>
							   <td width="10%" rowspan="2">[<span style="color:red" id="main<%=i%>" ><%=winCheckIn[i].get("dlo_id",0l) %></span>]</td>
							   <td width="1%" rowspan="2">
								   <span name="ckName" style="display:none">Pick Up</span>
							   	   <input type="radio" name="ra<%=i%>" id="ra" value="0" style="display:none"/><br/>
							   	   <span name="ctnName" style="display:none">Delivery</span>
						   		   <input type="radio" name="ra<%=i%>" id="ra" value="1" checked="checked" style="display:none"/>
							   </td>			   
							   <td width="7%" align="right">
							   		<span name="title">
									   	 <select id="choice<%=i%>">
											 <option value="0" <%=ctn_checked %> >CTNR</option>
											 <option value="1" <%=others_checked %> >OTHER</option>
											 <option value="2" <%=bol_checked %> >BOL</option>
										 </select>
							   		</span>
							   </td>
							   <td width="33%" align="center" >
							  		<input type="text" name="number" id="<%=i%>" size="30" value="<%=number%>" disabled="disabled" />			
							   </td>
							   <td width="30%" >
							   		Operator:<input type="text" id="tongzhi<%=i%>" name="tongzhi" size="20"  onclick="tongzhi(this)"/>
							   		<input type="hidden" id="tongzhi<%=i%>Id" />
							   </td>	
							   <td rowspan="2" ><input type="button" value="Del" class="short-short-button-del" name="detButton" onclick="det(<%=i%>)" /></td>	
							</tr>
							<tr>									
								<td width="3%"  align="right" >DOOR:</td>
								<td width="14%" align="center" ><input type="text" name="door" id="door<%=i%>" size="30" in="<%=i%>" value="<%=door_name %>" text="<%=rl_id %>" onclick="openTheDoor(this)" /></td>
								<td>Email:<input id="youjian<%=i%>" type="checkbox" checked="checked">&nbsp;Message:<input id="duanxin<%=i%>" type="checkbox" checked="checked">&nbsp;Browser:<input id="yemian<%=i%>" type="checkbox" checked="checked"></td>									
							</tr>
						 </table>
						 <div>Note：<textarea name="textfield" id="beizhu<%=i%>" cols="93" rows="1"></textarea></div>
						 <hr size="1"/>
					 </div>
	   		 	<%}else if(winCheckIn[i].get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.BOTH){ %>
	   		 		 <div id="div<%=i%>">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" >
								<tr>
								   <td width="8%"  rowspan="2" height="50px">Entry ID:</td>
								   <td width="10%" rowspan="2">[<span style="color:red" id="main<%=i%>" ><%=winCheckIn[i].get("dlo_id",0l) %></span>]</td>
								   <td width="10%" rowspan="2">
									   <span name="ckName">Pick Up</span>
								   	   <input type="radio" name="ra<%=i%>" id="ra" value="0" <%=pick_up_checked %> /></br>
								   	   <span name="ctnName">Delivey</span>
							   		   <input type="radio" name="ra<%=i%>" id="ra" value="1" <%=delivery_checked %> />
								   </td>			   
								   <td width="7%" align="right">
								   		<%if(!delivery_checked.equals("")){ %>
									   		<span name="title">
											   <select id="choice<%=i%>">
												   <option value="0" <%=ctn_checked%> >CTNR</option>
												   <option value="1" <%=others_checked %> >OTHER</option>
												   <option value="2" <%=bol_checked%> >BOL</option>
											   </select>
										   </span>
								   		<%}else{ %>
								   			<span name="title">LOAD:</span>
								   		<%} %>
								   </td>
								   <td width="24%" align="center" >
								  		<input type="text" name="number" id="<%=i%>" size="20" value="<%=number%>" disabled="disabled" />			
								   </td>
								   <td width="30%" >
								   		Operator:<input type="text" id="tongzhi<%=i%>" name="tongzhi" size="20" onclick="tongzhi(this)" />
								   		<input type="hidden" id="tongzhi<%=i%>Id" />
								   </td>	
								   <td rowspan="2" ><input type="button" value="Del" class="short-short-button-del" name="detButton" onclick="det(<%=i%>)" /></td>
								</tr>
								<tr>									
									<td width="3%"  align="right" >DOOR:</td>
									<td width="14%" align="center" ><input type="text" name="door" id="door<%=i%>" size="20" in="<%=i%>" value="<%=door_name %>" text="<%=rl_id %>" onclick="openTheDoor(this)" /></td>
									<td>Email:<input id="youjian<%=i%>" type="checkbox" checked="checked">&nbsp;Message:<input id="duanxin<%=i%>" type="checkbox" checked="checked">&nbsp;Browser:<input id="yemian<%=i%>" type="checkbox" checked="checked"></td>									
								</tr>
							 </table>
							 <div>Note：<textarea name="textfield" id="beizhu<%=i%>" cols="93" rows="1"></textarea></div>
							 <hr size="1"/>
						</div>
	   		 	<%}else if(winCheckIn[i].get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.NONE){ %>
	   		 		<div id="div<%=i%>">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" >
								<tr>
								   <td width="8%"  rowspan="2" height="50px">Entry ID:</td>
								   <td width="10%" rowspan="2">[<span style="color:red" id="main<%=i%>" ><%=winCheckIn[i].get("dlo_id",0l) %></span>]</td>	
								   <td width="10%" rowspan="2">
									   <span name="ckName">Pick Up</span>
								   	   <input type="radio" name="ra<%=i%>" id="ra" value="0"  <%=pick_up_checked %> /></br>
								   	   <span name="ctnName">Delivey</span>
							   		   <input type="radio" name="ra<%=i%>" id="ra" value="1"  <%=delivery_checked %> />
								   </td>			   
								   <td width="7%" align="right">
								   		<span name="title">LOAD:</span>
								   </td>
								   <td width="24%" align="center" >
								  		<input type="text" name="number" id="<%=i%>" size="20" value="<%=number%>" disabled="disabled" />			
								   </td>
								   <td width="30%" >
								   		Operator:<input type="text" id="tongzhi<%=i%>" name="tongzhi" size="20" onclick="tongzhi(this)" />
								   		<input type="hidden" id="tongzhi<%=i%>Id" />
								   </td>
								   <td rowspan="2" ><input type="button" value="Del" class="short-short-button-del" name="detButton" onclick="det(<%=i%>)" /></td>		
								</tr>
								<tr>									
									<td width="3%"  align="right" >DOOR:</td>
									<td width="14%" align="center" ><input type="text" name="door" id="door<%=i%>" size="20" in="<%=i%>" value="<%=door_name %>" text="<%=rl_id %>"  onclick="openTheDoor(this)"/></td>
									<td>Email:<input id="youjian<%=i%>" type="checkbox" checked="checked">&nbsp;Message:<input id="duanxin<%=i%>" type="checkbox" checked="checked">&nbsp;Browser:<input id="yemian<%=i%>" type="checkbox" checked="checked"></td>								
								</tr>
							 </table>
							 <div>Note：<textarea name="textfield" id="beizhu<%=i%>" cols="93" rows="1"></textarea></div>
							 <hr size="1"/>
					 </div>
	   		 	<%} %>
		  <%}%>
	    </div>
	    
    </div>
    <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:770px;">
    	<input class="normal-green" type="button" onclick="tijiao()" value="Confirm" >
    </div>
</body>
</html>
<script>
var p=<%=winCheckIn.length%>+1;

function add(){
	p++;
	var main_id=$('#mId').html() ;
	if(main_id==""){
		alert("请输入主单据号");
		return false;
	}
	var con=$('#Container');
	var entryType=$('#entryType').html();
	//debugger;
	//alert(entryType);
	var html='';
	if(entryType=='PICK UP'){
		html='<div id="av">'+
				   '<table width="100%" border="0" cellspacing="0" cellpadding="0" >'+
						'<tr>'+
						   '<td width="8%"  rowspan="2" height="50px">Entry ID:</td>'+
						   '<td width="10%" rowspan="2">[<span style="color:red" id="main'+p+'" >'+main_id+'</span>]</td>'+	
						   '<td width="1%" rowspan="2">'+
							   '<span name="ckName" style="display:none">Pick Up</span>'+
						   	   '<input type="radio" name="ra'+p+'" id="ra" value="0" checked="checked" style="display:none"/></br>'+
						   	   '<span name="ctnName" style="display:none">Delivey</span>'+
					   		   '<input type="radio" name="ra'+p+'" id="ra" value="1" style="display:none" />'+
						   '</td>'+			   
						   '<td width="7%" align="right">'+
						   		'<span name="title">LOAD:</span>'+
						   '</td>'+
						   '<td width="33%" align="center" >'+
						  		'<input type="text" name="number" id="'+p+'" size="30" value="" />'+			
						   '</td>'+
						   '<td width="30%" >'+
						   		'Operator:<input type="text" id="tongzhi'+p+'" name="tongzhi" size="20" />'+
						   		'<input type="hidden" id="tongzhi'+p+'Id" />'+
						   '</td>'+	
						   '<td rowspan="2" ><input type="button" value="Del" class="short-short-button-del" name="detButton" /></td>'+		
						'</tr>'+
						'<tr>'+										
							'<td width="3%"  align="right" >DOOR:</td>'+
							'<td width="14%" align="center" ><input type="text" name="door" id="door'+p+'" size="30" in="'+p+'" value="" /></td>'+
							'<td>Email:<input id="youjian'+p+'" type="checkbox" checked="checked">&nbsp;Message:<input id="duanxin'+p+'" type="checkbox" checked="checked">&nbsp;Browser:<input id="yemian'+p+'" type="checkbox" checked="checked"></td>'+									
						'</tr>'+
					 '</table>'+
					 '<div>Note：<textarea name="textfield" id="beizhu'+p+'" cols="93" rows="1"></textarea></div>'+
					 '<hr size="1"/>'+
			 '</div>';
	}else if(entryType=='DELIVERY'){
		html='<div id="av">'+
				'<table width="100%" border="0" cellspacing="0" cellpadding="0" >'+
						'<tr>'+
						   '<td width="8%"  rowspan="2" height="50px">Entry ID:</td>'+
						   '<td width="10%" rowspan="2">[<span style="color:red" id="main'+p+'" >'+main_id+'</span>]</td>'+	
						   '<td width="1%" rowspan="2">'+
							   '<span name="ckName" style="display:none">Pick Up</span>'+
						   	   '<input type="radio" name="ra'+p+'" id="ra" value="0" style="display:none"/></br>'+
						   	   '<span name="ctnName" style="display:none">Delivey</span>'+
					   		   '<input type="radio" name="ra'+p+'" id="ra" value="1" checked="checked" style="display:none"/>'+
						   '</td>'+			   
						   '<td width="7%" align="right">'+
						   		'<span name="title">'+
								   	 '<select id="choice'+p+'">'+
										 '<option value="0"  >CTNR</option>'+
										 '<option value="1">OTHER</option>'+
										 '<option value="2"  >BOL</option>'+
									 '</select>'+
						   		'</span>'+
						   '</td>'+
						   '<td width="33%" align="center" >'+
						  		'<input type="text" name="number" id="'+p+'" size="30" value="" />'+			
						   '</td>'+
						   '<td width="30%" >'+
						   		'Operator:<input type="text" id="tongzhi'+p+'" name="tongzhi" size="20" />'+
						   		'<input type="hidden" id="tongzhi'+p+'Id" />'+
						   '</td>'+	
						   '<td rowspan="2" ><input type="button" value="Del" class="short-short-button-del" name="detButton" /></td>'+		
						'</tr>'+
						'<tr>'+										
							'<td width="3%"  align="right" >DOOR:</td>'+
							'<td width="14%" align="center" ><input type="text" name="door" id="door'+p+'" size="30" in="'+p+'" /></td>'+
							'<td>Email:<input id="youjian'+p+'" type="checkbox" checked="checked">&nbsp;Message:<input id="duanxin'+p+'" type="checkbox" checked="checked">&nbsp;Browser:<input id="yemian'+p+'" type="checkbox" checked="checked"></td>'+									
						'</tr>'+
					 '</table>'+
					 '<div>Note：<textarea name="textfield" id="beizhu'+p+'" cols="93" rows="1"></textarea></div>'+
					 '<hr size="1"/>'+
			 '</div>';
	}else if(entryType=='BOTH'){
		html='<div id="av">'+
				'<table width="100%" border="0" cellspacing="0" cellpadding="0" >'+
						'<tr>'+
						   '<td width="8%"  rowspan="2" height="50px">Entry ID:</td>'+
						   '<td width="10%" rowspan="2">[<span style="color:red" id="main'+p+'" >'+main_id+'</span>]</td>'+	
						   '<td width="10%" rowspan="2">'+
							   '<span name="ckName">Pick Up</span>'+
						   	   '<input type="radio" name="ra'+p+'" id="ra" value="0" /></br>'+
						   	   '<span name="ctnName">Delivey</span>'+
					   		   '<input type="radio" name="ra'+p+'" id="ra" value="1"  />'+
						   '</td>'+			   
						   '<td width="7%" align="right">'+
						   		'<span name="title">LOAD:</span>'+
						   '</td>'+
						   '<td width="24%" align="center" >'+
						  		'<input type="text" name="number" id="'+p+'" size="20" value="" />'+			
						   '</td>'+
						   '<td width="30%" >'+
						   		'Operator:<input type="text" id="tongzhi'+p+'" name="tongzhi" size="20" />'+
						   		'<input type="hidden" id="tongzhi'+p+'Id" />'+
						   '</td>'+	
						   '<td rowspan="2" ><input type="button" value="Del" class="short-short-button-del" name="detButton" /></td>'+		
						'</tr>'+
						'<tr>'+										
							'<td width="3%"  align="right" >DOOR:</td>'+
							'<td width="14%" align="center" ><input type="text" name="door" id="door'+p+'" size="20" in="'+p+'" /></td>'+
							'<td>Email:<input id="youjian'+p+'" type="checkbox" checked="checked">&nbsp;Message:<input id="duanxin'+p+'" type="checkbox" checked="checked">&nbsp;Browser:<input id="yemian'+p+'" type="checkbox" checked="checked"></td>'+									
						'</tr>'+
					 '</table>'+
					 '<div>Note：<textarea name="textfield" id="beizhu'+p+'" cols="93" rows="1"></textarea></div>'+
					 '<hr size="1"/>'+
				'</div>';
	}else if(entryType=='NONE'){
		html='<div id="av">'+
				'<table width="100%" border="0" cellspacing="0" cellpadding="0" >'+
						'<tr>'+
						   '<td width="8%"  rowspan="2" height="50px">Entry ID:</td>'+
						   '<td width="10%" rowspan="2">[<span style="color:red" id="main'+p+'" >'+main_id+'</span>]</td>'+	
						   '<td width="10%" rowspan="2">'+
							   '<span name="ckName">Pick Up</span>'+
						   	   '<input type="radio" name="ra'+p+'" id="ra" value="0"  /></br>'+
						   	   '<span name="ctnName">Delivey</span>'+
					   		   '<input type="radio" name="ra'+p+'" id="ra" value="1"  />'+
						   '</td>'+			   
						   '<td width="7%" align="right">'+
						   		'<span name="title">LOAD:</span>'+
						   '</td>'+
						   '<td width="24%" align="center" >'+
						  		'<input type="text" name="number" id="'+p+'" size="20" value="" />'+			
						   '</td>'+
						   '<td width="30%" >'+
						   		'Operator:<input type="text" id="tongzhi'+p+'" name="tongzhi" size="20" />'+
						   		'<input type="hidden" id="tongzhi'+p+'Id" />'+
						   '</td>'+	
						   '<td rowspan="2" ><input type="button" value="Del" class="short-short-button-del" name="detButton" /></td>'+		
						'</tr>'+
						'<tr>'+										
							'<td width="3%"  align="right" >DOOR:</td>'+
							'<td width="14%" align="center" ><input type="text" name="door" id="door'+p+'" size="20" in="'+p+'" /></td>'+
							'<td>Email:<input id="youjian'+p+'" type="checkbox" checked="checked">&nbsp;Message:<input id="duanxin'+p+'" type="checkbox" checked="checked">&nbsp;Browser:<input id="yemian'+p+'" type="checkbox" checked="checked"></td>'+									
						'</tr>'+
					 '</table>'+
					 '<div>Note：<textarea name="textfield" id="beizhu'+p+'" cols="93" rows="1"></textarea></div>'+
					 '<hr size="1"/>'+
			 '</div>';
	}
    
	var $container=$(html).appendTo(con);                    //获得容器

	
	
    var $det_button=$("input[name='detButton']",$container); //获得删除按钮
    //绑定删除事件
    $det_button.click(function(){
    	$container.remove('#av');
	});

    var $door=$("input[name='door']",$container);       //获得门的文本框
    var $number=$("input[name='number']",$container);
    
    
    var $tongzhi=$("input[name='tongzhi']",$container); //获得通知文本框

    var $ra=$("input[name='ra"+p+"']",$container); 
    $ra.click(function(){
		if(this.value==0){
			$("span[name='title']",$container).html('LOAD:');
		}else{
			var html='<select id="choice'+p+'">'+
						  '<option value="0">CTNR</option>'+
						  '<option value="1">OTHER</option>'+
						  '<option value="2">BOL</option>'+
					 '</select>';
			$("span[name='title']",$container).html(html);
		}
    });

	$tongzhi.click(function(){    //绑定通知文本框
		 var id=this.id;
		 var ids=$('#'+id+'Id').val();
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/check_in/check_in_admin_user.html"; 
		 var option = {
				 single_check:0, 					// 1表示的 单选
				 user_ids:ids, //需要回显的UserId
				 not_check_user:"",					//某些人不 会被选中的
				 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
				 ps_id:'0',	
				 id:id,					//所属仓库
				 handle_method:'setSelectAdminUsers'
		 };
		 uri  = uri+"?"+jQuery.param(option);
		 $.artDialog.open(uri , {title: 'User List',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	});

    
    $door.click(function(){                       //绑定门和位置
    	var id=$(this).attr("in");
	    var ra=$("input[name='ra"+id+"']:checked",$container).val();
	    	
    	var $num=$("input[name='number']",$container); 
    	
    	if($num.val()==""){
			alert("Please enter LOAD# and CTN#");
			return false;
    	}
    	var numVal=$num.val();
    	var numName="";
    	if(ra==0){
    		numName="LOAD#";
    	}else{
		    numName="CTN#"
    	}
    	var inputId=this.id;
    	var mainId=$('#main'+id,$container).html();
    	var zoneId=<%=zoneId%>;
    	var inputValue=$(this).attr("text");
    	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/door_list.html?zoneId='+zoneId+'&mainId='+mainId+'&numVal='+numVal+'&inputId='+inputId+'&inputValue='+inputValue+'&numName='+numName;
		$.artDialog.open(uri , {title: "Door and Location",width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	});
}

function det(num){
	$('#div'+num).remove();
}

function tongzhi(input){
	 var id=input.id;
	 var ids=$('#'+id+'Id').val();
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/check_in/check_in_admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:ids, //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',	
			 id:id,					//所属仓库
			 handle_method:'setSelectAdminUsers'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: 'User List',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}

function setParentUserShow(ids,names,inputId){ //通知回显
	$("#"+inputId).val(names);
	$("#"+inputId+"Id").val(ids);			
}

function getDoor(door,doorName,inputId){    //门回显
	$('#'+inputId).val(doorName);
	$('#'+inputId).attr("text",door);
}

function openTheDoor(input){
	var numVal='';
	var numName='';
	var inputId=input.id;
	var mainId=<%=id%>;
	var zoneId=<%=zoneId%>;
	var inputValue=$(input).attr("text");
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/door_list.html?type=docks&zoneId='+zoneId+'&mainId='+mainId+'&numVal='+numVal+'&inputId='+inputId+'&inputValue='+inputValue+'&numName='+numName;
	$.artDialog.open(uri , {title: "Door and Location",width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
}

function tijiao(){                                  //提交方法
	var $num=$("input[name='number']"); 
	var ctn=$("input[name='ctn']");
	if($num.length==0){
		alert("Please add Load#");
		return false;
	}
	
	var data={};
	data.items=[]; 
	var prints=[]; 
	for(var i=0;i<$num.length;i++){
		var id=$($num[i]).attr("id");
		var doorId=$('#door'+id).attr("text");
		var mainId=$('#main'+id).html();
		var tongzhiId=$('#tongzhi'+id+'Id').val();
		var ra=$("input[name='ra"+id+"']:checked").val();
		var beizhu=$('#beizhu'+id).val();
		var youjian='';
		var duanxin='';
		var yemian='';			
		if($('#youjian'+id).attr('checked')=='checked'){
			youjian=1;
		}else{
			youjian=0;
		}
		if($('#duanxin'+id).attr('checked')=='checked'){
			duanxin=1;
		}else{
			duanxin=0;
		}
		if($('#yemian'+id).attr('checked')=='checked'){
			yemian=1;
		}else{
			yemian=0;
		}	
		var loadId='';
		var ctnId='';
		var otherId='';
		var bolId='';
		if(ra==0){	
			loadId=$($num[i]).val();
		}else{
			var choice= $('#choice'+id).val();     //选择值
			if(choice==0){
				ctnId=$($num[i]).val();	
			}else if(choice==1){
				otherId=$($num[i]).val();
			}else{
				bolId=$($num[i]).val();
			}
			
		}

		if(loadId=="" && ctn==""){
			alert("Please enter LOAD# and CTN#");
			return false;
		}
		if(doorId==undefined){
			alert("Please choose door First");
			return false;
		}
	
		data.mainId=mainId;
		var num=0;
		for(var a=0;a<prints.length;a++){
			if(prints[a]==doorId){
				num=1;
			}
		}
		if(num==0){
			prints.push(doorId);
		}
		var op={};
		op.otherId=otherId;
		op.bolId=bolId;
		op.loadId=loadId;
		op.doorId=doorId;
		op.ctnId=ctnId;
		op.tongzhi=tongzhiId;
		op.youjian=youjian;
		op.duanxin=duanxin;
		op.yemian=yemian;
		op.beizhu=beizhu;
		data.items.push(op);	
	}
	//alert(jQuery.fn.toJSON(data));
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxAddOccupancyDetailsAction.action',
  		dataType:'json',
  		data:'str='+jQuery.fn.toJSON(data),
	 	success:function(data){
  			if(data.flag=="success"){
  			    parent.location.reload(); 
  			    $.artDialog.close(); 
  			}
  		}
    });

}
</script>