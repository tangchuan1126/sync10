<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="palletInventoryKey" class="com.cwc.app.key.PalletInventoryKey"/>
<%
	int palletId = StringUtil.getInt(request,"pallet_id");
	DBRow pallet = checkInMgrZwb.getPalletDetails(palletId);
	DBRow[] titles = (DBRow[])pallet.get("titles", new DBRow[0]);
%>
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- 时间 -->
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
	<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
	<!-- jquery UI  -->
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
 

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>


	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">


	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

</head>
<script type="text/javascript">
$(function(){
var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	api.button([
		{
			name: 'Close'
		}]
	);
})

</script>
<body onLoad="onLoadInitZebraTable()">
	
		<div>
			PalletType: <span style="font-size:14px;font-weight:bold;margin-right:20px;"><%=palletInventoryKey.getPalletInventoryName(pallet.get("pallet_type", 0))%></span> 
			PalletTotaily:<span style="font-size:14px;font-weight:bold;margin-right:20px;color:green;"><%=pallet.get("pallet_totaily", 0F) %></span> 
			DamagedTotaily:<span style="font-size:14px;font-weight:bold;color:red"><%=pallet.get("damaged_totaily", 0F) %></span>
		</div>
	  <!-- table 斑马线  -->
	  <table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"  isNeed="false" class="zebraTable mytable" isNeed="false" style="margin-left:3px;margin-top:20px;">
			  <tr> 
		        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Title</th>
		        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Pallet Count</th>
			  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Damaged Count</th>
			  	<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">Creat Date</th>
			  	<th width="15%" style="vertical-align: center;text-align: center;" class="right-title">Storage Location</th>
		      </tr>
		      <%for(int i=0;i<titles.length;i++){ %>
		      
	        <tr style="text-align: center;height:60px;">
	        	<td><%=titles[i].get("title_name", "") %></td>
	        	<td><%=titles[i].get("pallet_count", 0F) %></td>
	        	<td><%=titles[i].get("damaged_count", 0F) %></td>
	        	<td><%=titles[i].get("post_date","").substring(0,titles[i].get("post_date","").lastIndexOf(".")) %></td>
	        	<td><%=pallet.get("title","") %></td>
	        </tr> 
	        <%} %>	  
	 </table>	
</body>
