<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="palletInventoryKey" class="com.cwc.app.key.PalletInventoryKey"/>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long psId=adminLoggerBean.getPs_id(); 
	//long adgid=adminLoggerBean.getAdgid(); 
	boolean  isadministrator = adminLoggerBean.isAdministrator();  //判断当前登录帐号是不是admin.如果是返回true
	int palletId = StringUtil.getInt(request, "pallet_id");
	int titleId = StringUtil.getInt(request, "title_id");
	DBRow pallet = checkInMgrWfh.findPalletInventoryById(palletId);
	pallet = pallet==null?new DBRow():pallet;
	DBRow[] title = checkInMgrWfh.findAllTitle(pallet.get("pallet_type", ""), pallet.get("ps_id", 0L));
	long ps_id = StringUtil.getLong(request, "ps_id");
	DBRow[] ps = checkInMgrZwb.findSelfStorage();	
	int flage = 1; //0添加  1修改
	if(palletId==0){
		flage = 0;
	}
%>
<script type="text/javascript">
	
</script>
<head>
	
	
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	
	 <!-- 遮罩 -->
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
	<!-- 下拉框多选 -->
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
	<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
	
	<!-- 带搜索功能的下拉框、 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../b2b_order/chosen.jquery.min.js" type="text/javascript"></script> 

	<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
	div.orders{padding:0px;margin: :0px;}
	div.pallets table td{border:1px solid silver;}
	span.order_info{font-size: 14px;font-weight: bold;}
	.single{}
	.double{}
	td.header{width: 33%;text-align: center;font-size: 14px;font-weight: bold;background: black;color: white;line-height: 25px;height: 25px;}
	.firstTD{
		text-align: right;
	}
	</style>
	<script type="text/javascript">
	$(function(){
		
		$.blockUI.defaults = {
		 css: { 
		  padding:        '8px',
		  margin:         0,
		  width:          '170px', 
		  top:            '45%', 
		  left:           '40%', 
		  textAlign:      'center', 
		  color:          '#000', 
		  border:         '3px solid #999999',
		  backgroundColor:'#ffffff',
		  '-webkit-border-radius': '10px',
		  '-moz-border-radius':    '10px',
		  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		 },
		 //设置遮罩层的样式
		 overlayCSS:  { 
		  backgroundColor:'#000', 
		  opacity:        '0.6' 
		 },
		 
		 baseZ: 99999, 
		 centerX: true,
		 centerY: true, 
		 fadeOut:  1000,
		 showOverlay: true
		};
		
	var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	api.button([
		{
			name: 'Confirm',
			callback: function () {
				confirm('<%=flage%>');
				
				return false ;
			},
			focus:true,
			focus:"default"
		},
		{
			name: 'Cancel'
		}]
	);
	});
	function confirm(flage){
		var title = $("#title").val();
		//console.log(title)
		var palletCount = $("#palletCount").val();
		var damagedCount = $("#damagedCount").val();
		if(palletCount>=damagedCount){
			var data = {
					palletId:$("#palletId").val(),
					titleId:title,
					palletType:$("#palletType").val(),
					palletCount:palletCount,
					damagedCount:damagedCount,
					ps_id:$("#Storage").val(),
					flag:flage
			};
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/saveOrEditPallet.action',
				dataType:'json',
				data:data,
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
			    },
				success:function(data){
					 parent.location.reload(); 
				},
				error:function(){ 
					$.unblockUI();
					alert("System error,please try later");
				}
			});
		}else{
			alert("Damage the number is not greater than the pallet number !");
		}
		
		
	}
	
	</script>
</head>
<body style="font-size:12px">
		
		<fieldset style="border-top:2px solid #993300;width:96%;border-left:0px solid #fff;border-right:0px solid #fff;border-bottom:0px solid #fff;"> <legend style="font-size:16px;"><B>PalletInventory</B></legend>
		<table>
			<tr>
				<td class="firstTD">Pallet Type:</td>
				<td>
					<input type="hidden" id="palletId"  value="<%=palletId %>"/> 
					
					
					
					<select id="palletType" class="chzn-select"  style="width:200px; " <%=palletId!=0?"disabled":"" %>>
						
						<option value="<%=palletInventoryKey.Pallet1 %>" <%=pallet.get("pallet_type", 0)==palletInventoryKey.Pallet1?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet1) %></option>
						<option value="<%=palletInventoryKey.Pallet2 %>" <%=pallet.get("pallet_type", 0)==palletInventoryKey.Pallet2?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet2) %></option>
						<option value="<%=palletInventoryKey.Pallet3 %>" <%=pallet.get("pallet_type", 0)==palletInventoryKey.Pallet3?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet3) %></option>
						<option value="<%=palletInventoryKey.Pallet4 %>" <%=pallet.get("pallet_type", 0)==palletInventoryKey.Pallet4?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet4) %></option>
						<option value="<%=palletInventoryKey.Pallet5 %>" <%=pallet.get("pallet_type", 0)==palletInventoryKey.Pallet5?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet5) %></option>
						<option value="<%=palletInventoryKey.Pallet6 %>" <%=pallet.get("pallet_type", 0)==palletInventoryKey.Pallet6?"selected":"" %>><%=palletInventoryKey.getPalletInventoryName(palletInventoryKey.Pallet6) %></option>
					
						
					</select>
					</td>
			</tr>
			
			<tr>
				<td class="firstTD">Storage:</td>
				<td>
				<%
					String str = "";
					if(isadministrator){
						str = "disabled";
						ps_id = psId;
					}
						
				%>
					<select id="Storage" class="chzn-select"  style="width:200px; " name="Storage" <%=palletId!=0?"disabled":"" %>>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("ps_id",0l)%>"  <%=ps_id==ps[j].get("ps_id",0l)?"selected":"" %>><%=ps[j].getString("ps_name")%></option>
							<%
								}
							%>
						</select>
				</td>
			</tr>
			<tr>
				<td class="firstTD">Title:</td>
				<td style="font-size:12px;">
					<select id="title" style="width:200px; " onchange="selectTitleCount()" class="chzn-select"  >
						<%for(int j= 0;j<title.length;j++){ %>
							<option value="<%=title[j].get("title_id", 0)%>" <%=j==0&&flage==1?"selected":"" %>><%=title[j].get("title_name", "") %></option>
							
						<%} %>
						</select>
				</td>
			</tr>
			<tr>
				<td class="firstTD">Pallet Count:</td>
				<td><input type="text" style="width:200px; " id="palletCount" value=""/> </td>
			</tr>
			<tr>
				<td class="firstTD">Damaged Count:</td>
				<td>
				<input type="text" style="width:200px; " id="damagedCount" value=""/>
				</td>
			</tr>
		</table>
	
	</fieldset>
	<script type="text/javascript">
	$("#title").chosen({no_results_text: "没有该选项:"});
	$("#palletType").chosen({allow_single_deselect:true,disable_search:true,no_results_text: "没有符合条件的结果!"});
   	$("#Storage").chosen({allow_single_deselect:true,disable_search:true,no_results_text: "没有符合条件的结果!"});
	function selectTitleCount(){
		 var flag = <%=flage %>;
		 if(flag===1){
			 var title = $("#title").val();
			var data = {
				titleId:title,
				palletType:$("#palletType").val(),
				ps_id:$("#Storage").val()
			};
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/findPalletCountByIdTypeTitle.action',
				dataType:'json',
				data:data,
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
			    },
				success:function(data){

					 $("#palletCount").val(data.PALLET_COUNT);
					 $("#damagedCount").val(data.DAMAGED_COUNT);
					$.unblockUI();
				},
				error:function(){
					$.unblockUI();
					alert("System error,please try later");
				}
			});
		 }
	}
		
		$(function (){
			selectTitleCount();
		});
	</script>
	
	
	
</body>