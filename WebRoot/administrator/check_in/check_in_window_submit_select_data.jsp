<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 

<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<html>
<head>
<%
  String numberStr=StringUtil.getString(request, "number");
  String[] numbers=numberStr.split(",");
%>
<title>Select Data</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
</head>
<body>
	<div style="height:430px; overflow:auto;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td style="border:1px solid #BBB; background-color:#E5E5E5; height:35px; font-weight:bold;" align="center">Search No</td>
	    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >Type</td>
	    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >CompanID</td>
	    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >CustomerID</td>
	    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >OrderNo</td>
	    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >OpNo</td>
	  </tr>
	  <%for(int i=0;i<numbers.length;i++){ %>
	  <tr style="background-color:#f9f9f9" name="seData" id="<%=i%>">
	  	<td style="border-bottom:2px solid #BBB;border-right:1px solid #BBB;" align="center">
	  		<span style="color:blue" valign="middle" id="number<%=i%>"><%=numbers[i]%></span>
	  	</td>
	  	<td colspan="6" style="border-bottom:1px solid #BBB;border-left:1px solid #BBB;">
	  		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	  			<%DBRow[] rows= sqlServerMgr.checkOrderInfoByOrderNo(numbers[i],0,request); %>
	  			<%if(rows.length>0){ %>
				<%for(int a=0;a<rows.length;a++){ %>
				<tr height="35px" onmouseover="changeColor(this)" id="<%=i%><%=a%>" onmouseout="changeColorB(this)" onclick="choice(this)" >
					<td align="left" width="90px;">
						<input name="radio<%=i%>" num="<%=i%><%=a%>" type="radio" id="radio" value="<%=rows[a].get("order_type",0) %>" />
						<%=moduleKey.getModuleName(rows[a].get("order_type",0)) %>
					</td>
					<td align="center" width="90px;">
						<span id="company<%=i%>"><%=rows[a].getString("CompanyID") %></span>&nbsp;
					</td>
					<td align="center" width="90px;">
						<span id="customer<%=i%>"><%=rows[a].getString("CustomerID") %></span>&nbsp;
					</td>
					<td align="center" width="90px;">
						<span id="order<%=i%>"><%=rows[a].getString("OrderNo") %></span>&nbsp;
					</td>
					<td align="center" width="90px;">
						<span id="po<%=i%>"><%=rows[a].getString("PONo") %></span>&nbsp;
						<span style="display:none" id="accountId<%=i%>"><%=rows[a].getString("AccountID") %></span>&nbsp;
						<span style="display:none" id="staging<%=i%>"><%=rows[a].getString("StagingAreaID") %></span>&nbsp;
					</td>
				</tr>
				<%} %>	
				<%}else{ %>
					<tr><td colspan="5" height="35px;" align="center" style="background-color:#E6F3C5">no data</td></tr>
				<%} %>	
			</table>
	  	</td>
	  </tr>
	  <%} %>
	</table>
	</div>
	<div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;width:98%;margin-top:2px;">
		<input class="normal-green" type="button" value="Submit" onclick="createData()">
	</div>
</body>
</html>
<script>
	function createData(){
		var selectData=[];
		var tr=$("tr[name='seData']");
		for(var i=0;i<tr.length;i++){
			var op={};
			var id=$(tr[i]).attr('id');
			var number=$('#number'+id).html();
			var ra=$("input[name='radio"+id+"']:checked");
			var seNum=$(ra).attr("num");
			var container=$('#'+seNum);
			var number_type=$(ra).val();
 			var company_id=$('#company'+id,container).html();
			var customer_id=$('#customer'+id,container).html();
			var order_no=$('#order'+id,container).html();
			var po_no=$('#po'+id,container).html();
			var accountId=$('#accountId'+id,container).html();
			var staging=$('#staging'+id,container).html();
			if(number_type==null){
				number_type='';
			}
			if(company_id==null){
				company_id='';
			}
			if(customer_id==null){
				customer_id='';
			}
			if(order_no==null){
				order_no='';
			}
			if(po_no==null){
				po_no='';
			}
			if(accountId==null){
				accountId='';
			}
			if(staging==null){
				staging='';
			}
			op.number=number;
			op.number_type=number_type;
			op.company_id=company_id;
			op.customer_id=customer_id;
			op.order_no=order_no;
			op.po_no=po_no;
			op.accountId=accountId;
			op.staging=staging;
			selectData.push(op);
		}
		 //alert(jQuery.fn.toJSON(selectData));
		 $.artDialog.opener.tijiao  && $.artDialog.opener.tijiao(selectData);
		 $.artDialog && $.artDialog.close();
	}
	function changeColor(tr){
		$(tr).css("background-color","#E6F3C5");
	}
	function changeColorB(tr){
		$(tr).css("background-color","#f9f9f9");
	}
	function choice(tr){
		var ra=$('#radio',tr);
		$(ra).attr('checked',true);
	}
</script>
