<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="wmsSearchItemStatusKey" class="com.cwc.app.key.WmsSearchItemStatusKey"></jsp:useBean>
<html>
<head>


<title>print wms</title>
<%
	long entryId = StringUtil.getLong(request, "entryId");
	//long entryId = 100732;
    DBRow mainRow=checkInMgrZwb.findGateCheckInById(entryId);
	
	String CompanyID = StringUtil.getString(request, "CompanyID");
	String CustomerID = StringUtil.getString(request, "CustomerID");
	String DockID=StringUtil.getString(request, "door_name");
	String loadNo=StringUtil.getString(request,"number");
	String out_seal = StringUtil.getString(request, "out_seal");
	String window_check_in_time = mainRow.get("window_check_in_time","");
	String company_name = mainRow.getString("company_name");
	String gate_container_no = mainRow.getString("gate_container_no");
	int number_type=StringUtil.getInt(request, "number_type");
	String order_no=StringUtil.getString(request, "order_no");
	long resources_type = StringUtil.getLong(request, "resources_type");
		
	if(StringUtil.isBlank(out_seal))
	{
		out_seal = mainRow.getString("out_seal");
	}
	//masterCount大于0，打印Master Loading Ticket，等于0，打印Order Loading Ticket
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adid = adminLoggerBean.getAdid();
	int masterCount = sqlServerMgr.findMasterBolNoCountByLoadNo(loadNo, CompanyID, CustomerID, adid, request);
	//System.out.println("mloadNo:"+loadNo+","+entryId+","+out_seal);
%>
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">

 <script>
$(function(){
	
	
	if(<%=number_type%>==<%=ModuleKey.CHECK_IN_LOAD%>){
		var wmsUrl = '<%=ConfigBean.getStringValue("systenFolder")%>check_in/';
		var para = 'loadNo=<%=loadNo%>&entryId=<%=entryId%>&window_check_in_time=<%=window_check_in_time%>&DockID=<%=DockID%>';
		para += '&company_name=<%=company_name%>&gate_container_no=<%=gate_container_no%>&seal=<%=out_seal%>&CompanyID=<%=CompanyID%>&CustomerID=<%=CustomerID%>&resources_type=<%=resources_type%>';
		if('<%=masterCount%>'*1 != 0){
			wmsUrl += 'print_order_master_wms.html';
		}else{
			wmsUrl += 'print_order_no_master_wms.html';
		}
	}else{
		var para='';
		if(<%=number_type%>==<%=ModuleKey.CHECK_IN_ORDER%>){
			var para = 'order_no=<%=loadNo%>&entryId=<%=entryId%>&window_check_in_time=<%=window_check_in_time%>&DockID=<%=DockID%>';
		}else if(<%=number_type%>==<%=ModuleKey.CHECK_IN_PONO%>){
			var para = 'order_no=<%=order_no%>&entryId=<%=entryId%>&window_check_in_time=<%=window_check_in_time%>&DockID=<%=DockID%>';
		}
		var wmsUrl = '<%=ConfigBean.getStringValue("systenFolder")%>check_in/';
		
		para += '&company_name=<%=company_name%>&gate_container_no=<%=gate_container_no%>&seal=<%=out_seal%>&CompanyID=<%=CompanyID%>&CustomerID=<%=CustomerID%>&resources_type=<%=resources_type%>';
		
		wmsUrl += 'print_order_no_master_wms_order.html';
		
	}
	
 	$.ajax({
		url: wmsUrl,
		type: 'post',
		dataType: 'html',
		data:para,
		async:false,
		success: function(html){
			$(html).appendTo($("#av"));
		}
	});	
});
	function printWms(){
		var printHtml=$('div[name="printWms"]');
		for(var i=0;i<printHtml.length;i++){
			 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Load Label");
	      	 visionariPrinter.SET_PRINTER_INDEXA ("LabelPrinter");//指定打印机打印  
	      	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
	         visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			 visionariPrinter.ADD_PRINT_TABLE("0.4cm",7,"100%","100mm",$(printHtml[i]).html());
			 visionariPrinter.SET_PRINT_COPIES(1);
			 visionariPrinter.ADD_PRINT_HTM("14cm",160,"100%","100%",addStyleForText("CONTINUED..."));
			 visionariPrinter.SET_PRINT_STYLEA(0,"ItemType",1);
			 visionariPrinter.SET_PRINT_STYLEA(0,"PageUnIndex","Last");
			 //visionariPrinter.PREVIEW();
			 visionariPrinter.PRINT(); 
		}
	}
	function addStyleForText(text)
	{
		return '<span style="font-size: 10;font-family:Verdana;">'+text+'</span>';
	}
</script>
</head>
<body>


<div id="av"></div>
	
</body>
</html>
