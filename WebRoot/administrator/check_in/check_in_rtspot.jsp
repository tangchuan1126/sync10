<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.OccupyStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="checkInDoorStatusTypeKey" class="com.cwc.app.key.OccupyStatusTypeKey"/>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		long psId=adminLoggerBean.getPs_id();  
		long adminId=adminLoggerBean.getAdid(); 
		PageCtrl pc = new PageCtrl();
		int p =StringUtil.getInt(request,"p");
		pc.setPageNo(p);
		pc.setPageSize(10);
		long ps_id = StringUtil.getLong(request,"ps_id");
		long spot_area = StringUtil.getLong(request,"spot_area");
		long spotStatus = StringUtil.getLong(request,"spotStatus");
		String start_time = StringUtil.getString(request,"start_time");
		String end_time = StringUtil.getString(request,"end_time");
		long dlo_id = StringUtil.getLong(request,"dlo_id");
		String cmd = StringUtil.getString(request,"cmd");
		long spotId = StringUtil.getLong(request,"spotId");
		long searchSpotId = StringUtil.getLong(request,"searchSpotId");
		int flag=StringUtil.getInt(request,"flag");
		String license_plate = StringUtil.getString(request,"license_plate");
		String trailerNo = StringUtil.getString(request,"trailerNo");
		long mainId = StringUtil.getLong(request,"mainId");
		int doubt = StringUtil.getInt(request, "doubt");
		if(ps_id==0){
	ps_id=psId;
		}
	//	if(spotStatus==0){
	//		spotStatus=2;
	//	}
//		if(cmd.equals("check")){
	//           int doubt_id = StringUtil.getInt(request, "doubt_id");
	//           checkInMgrZwb.verifyData(adminId,ps_id,mainId,license_plate,trailerNo,1,doubt_id);
	//	}
		//DBRow[] rows = checkInMgrZwb.rtOccupiedSpot(ps_id,spot_area,spotStatus,pc);
		DBRow[] ps = checkInMgrZwb.findSelfStorage();
		DBRow[] spots = checkInMgrZwb.findSpotArea(ps_id);
		boolean isDone = true;
		for(int i = 0;i<spots.length;i++){
	if(spots[i].get("patrol_time", "").equals("")){
		isDone = false;
	}
		}
%>
<title>Check in</title>
<script type="text/javascript">


function loadSpotBySpotType(area_id){
$("#ui-tabs-1,#ui-tabs-2,#ui-tabs-3,#ui-tabs-4").html("");
	 location.href = 'check_in_rtspot.html?p=1&spot_area='+area_id+'&ps_id=<%=ps_id %>&spotStatus=<%=spotStatus%>';
	      
}
</script>
<style type="text/css">
	.hover:hover .one{
		font-size:20px;
		color:red;
	
		display:block;
	}
</style>
</head>
<body >
	<div class="demo" style="width:98%">
	  <div id="tabs">
		<ul>
			<li><a href="#checkin_filter">Advanced Search</a></li>
		</ul>
	      <div id="checkin_filter">
	      		<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left">
						<div style="float:left;">
						<select id="ps" name="ps">
							<option value="-1">All Storage</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("ps_id",0l)%>" <%=ps_id==ps[j].get("ps_id",0l)?"selected":"" %>><%=ps[j].getString("ps_name")%></option>
							<%
								}
							%>
						</select>
		     		  	<select id="spotStatus" name="spotStatus">
							<option value="-1">Spot Status</option>
							<option value="2" <%=spotStatus==2?"selected":"" %> >Occupied</option>
							<option value="<%=OccupyStatusTypeKey.RELEASED %>" <%=spotStatus==OccupyStatusTypeKey.RELEASED?"selected":"" %>>No Occupied</option>
						</select>
						<input type="button" class="button_long_search" value="Search" onclick="filter();"/>
					</td>
			<!--      <td width="67" align="center">							 
						<input type="button" class="button_long_refresh" value="提交审核" onclick="commit();"/>
					</td>-->	
				</tr>
			</table>
	      </div>
       </div>
       <script>
	       $("#tabs").tabs({
		   	    cache: true,
		   		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		   		cookie: { expires: 30000 } ,
		   		show:function(event,ui)
		   			 {
		   			 	
		   			 }
		   });
       </script>
    </div>
    <br/>
    
    
    <div class="demo" style="width:100%">
	  <div id="spotType">
		<ul>
					
			<li><a class="hover"   href="#0"><%=isDone==true?"<font color='green'>":"<font color='red'>"%>ALL</font></a>
				
			
			<span class="one"; style="display:none">is ok</span>
			</li>
		<%for(int i=0;i<spots.length;i++){ %>
			<li><a  href="#<%=spots[i].get("area_id", 0l)%>"><%=!spots[i].get("patrol_time","").equals("")?"<font color='green'>"+spots[i].get("area_name","")+"</font>":"<font color='red'>"+spots[i].get("area_name","")+"</font>"%></a></li>
		<%} %>
		</ul>
		<div id="0"></div>
		<%
   		  		
   		  	for(int i=0;i<spots.length;i++){
  		%>
		<div id="<%=spots[i].get("area_id",01)%>"></div>
		<%
  		  			}
		%>
		
       </div>
    </div>
    
    
    
     


	      		
    
	      		
	      		
       <script>
       $("#tabs").tabs({
		   	    cache: true,
		   		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		   		cookie: { expires: 30000 } ,
		   		show:function(event,ui)
		   		{
		   			
		   		},
		   		
		   });
       		
	       $("#spotType").tabs({
		   	    cache: true,
		   		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		   		cookie: { expires: 30000 } ,
		   		show:function(event,ui)
		   		{
		   			showCheckInSpotZone(ui.panel.id);
		   		},
		   		
		   		
		   });
		   function showCheckInSpotZone(spot_area,p){
				$.ajax({
					url:'check_in_rtspot_ALL.html',
					data:"spot_area="+spot_area+"&p="+(p?p:0)+"&license_plate=<%=license_plate%>&trailerNo=<%=trailerNo%>&mainId=<%=mainId%>&cmd=<%=cmd%>&doubt=<%=doubt%>&spotStatus=<%=spotStatus%>&ps_id=<%=ps_id%>",
					dataType:'html',
					type:'post',
					beforeSend:function(request){
				//	  $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				    },
					success:function(html){
				//    	$.unblockUI();
					   $("#"+spot_area).html(html);
					   
					},
					error:function(){
					}
				});
			};	
       </script>
    </div>
	<form action="check_in_rtspot.html" method="post" name="filter_form">
		<input type="hidden" name="ps_id"/>
		<input type="hidden" name="spotStatus" />
	</form>
</body>
</html>
<script>

function createEntryId(spotId,spotName){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_createEntryId.html?spotId='+spotId+'&spotName='+spotName; 
	$.artDialog.open(uri , {title: "Create EntryID",width:'500px',height:'200px', lock: true,opacity: 0.3,fixed: true});
}

function filter(){
	
	document.filter_form.ps_id.value = $("#ps").val();
	document.filter_form.spotStatus.value = $("#spotStatus").val();
	document.filter_form.submit();
}


</script>