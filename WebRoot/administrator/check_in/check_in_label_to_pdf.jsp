<%@page import="com.cwc.app.key.YesOrNotKey" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@ include file="../../include.jsp"%>
<html>
<head>

<%
	DBRow[] rows = printLabelMgrGql.queryPrintTaskBy(YesOrNotKey.NO);

%>

 <!--  基本样式和javascript -->
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
 
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="js/blockui/jquery.blockUI.233.js"></script>

<script>
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();
</script>
<title>Create PDF</title>

</head>
<body onload="">
<div style="width:210mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:left;">
	<h1><%=rows.length %></h1>
</div>
<br/>
<div style="width:210mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;">
	<input class="short-short-button-print" type="button" onclick="toPdf()" value="ToPDF">
</div>

<div id="printHtml"></div>
</body>
</html>
<script>
 function toPdf(){
     //alert("有多少可能上传的:"+<%=rows.length%>);
	 // 	 $.blockUI({message: '<img src="js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
// 	var printUrlAll = $('div[name="printUrlAll"]');
// 	for(var i=0;i<printUrlAll.length;i++){
// 		var printUrl = $(printUrlAll[i]).text();
// 		 console.log(printUrl);
	 <%if(rows!=null){
		 for(DBRow row:rows){
		 	String printUrl = row.get("url","");
		 	long print_task_id = row.get("print_task_id",0L);
	 %>
	 //调用签中的上传方法
		 $.ajax({
				url:'<%=printUrl%>',
				dataType:'html',
				data:'',
				success:function(html){	
	// 				$.unblockUI();       //遮罩关闭
					iscoun=false;
					$('#printHtml').html(html);
					androidToPdf();
				}
			});
		 
		 //修改print_task表中的conver_pdf为yes（已转）
		 $.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/file_up/updatePrintTaskStatus.action',
					data:'printTaskId='+'<%=print_task_id%>' ,
					dataType:'json',
					type:'post',
					success:function(data){
					}
				});	 
		 
	 <%
	 	}
	 }
	 %>
 }

</script>

