<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLogTypeKey" %>
<jsp:useBean id="checkInLogTypeKey" class="com.cwc.app.key.CheckInLogTypeKey"/>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInTractorOrTrailerTypeKey" %>
<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="checkInMainDocumentsRelTypeKey" class="com.cwc.app.key.CheckInMainDocumentsRelTypeKey"/>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/>
<jsp:useBean id="checkInLiveLoadOrDropOffKey" class="com.cwc.app.key.CheckInLiveLoadOrDropOffKey"/>
<jsp:useBean id="checkInTractorOrTrailerTypeKey" class="com.cwc.app.key.CheckInTractorOrTrailerTypeKey"/>

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		long psId=adminLoggerBean.getPs_id();  
		long adminId=adminLoggerBean.getAdid();
		DBRow psRow=adminMgrZwb.findAdminPs(adminId);
		String ps_name	=psRow.get("title","") ;  
	/**	PageCtrl pc = new PageCtrl();
		int p =StringUtil.getInt(request,"p");
		pc.setPageNo(p);
		pc.setPageSize(10);*/
		long ps_id = StringUtil.getLong(request,"ps_id");
		long pa_id = StringUtil.getLong(request,"pa_id");
		long flag = StringUtil.getLong(request,"flag");
		String backurl = StringUtil.getString(request,"backurl");
		DBRow[] rows = checkInMgrXj.findCheckInPatrolApproveDetails(ps_id,pa_id, null);
%>
<title>Difference Spot</title>
<script type="text/javascript">
$(function(){
	$("#note").blur(function(){
		$("input[id='note']").val($("#note").val());
	});
});

</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <tr>
	    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; Patrol » Verify</td>
	  </tr>
	</table>
	<br>
	<form name="form1" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCheckInPatrolVerifyDataAction.action" onSubmit="return checkForm(this)">
	<input type="hidden" name="pa_id" value="<%=pa_id%>">
	<input type="hidden" name="backurl" value="<%=backurl%>">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
 	  	    <th width="3%" class="left-title" style="vertical-align: center;text-align: center;"><input type="checkbox" onclick="allCheck(this.checked)" checked="checked"/></th>  
	        <th width="25%" class="left-title" style="vertical-align:center;text-align:center;">Tractor / Trailer </th>
	        <th width="25%" class="left-title" style="vertical-align:center;text-align:center;">Time</th>
	        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">Reason</th>
	        <th width="10%" class="left-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
		</tr>
		<%if(rows != null && rows.length > 0){
      		for(int i=0;i<rows.length;i++){
      	%>
      	<tr height="50px" value="<%=rows[i].getString("pad_id")%>">
 	      	<td  align="center" valign="middle" style='word-break:break-all' value="<%=rows[i].getString("pad_id")%>">
 	      	<%
			if (rows[i].get("approve_status",0)==1)
			{
			%>
      	    	<input name="pad_ids" type="checkbox" value="<%=rows[i].getString("pad_id")%>" checked="checked">
      	    <%
			}
			else
			{
			%>
			&nbsp;
			<%
			}
			%>	
      	    </td>
      	    <td  id="text" align="center" valign="middle" style='word-break:break-all'>
      	     <fieldset style="width:90%; border:2px solid green;" >
	  				<legend>
	  					<span style="color:mediumseagreen;font-size:14px">
	  					<%
	  						 	if(!rows[i].getString("check_in_entry_id").equals(""))
	  						 	{
	  						 		out.print("<font color='#f60'>E"+rows[i].getString("check_in_entry_id")+"</font>");
	  						 	}
	  					 %> 
	  					
	  					 </span>
	  				</legend>
	  		  <table>
  			   		<div style="text-align:left;clear:both;"> 						
  						<div style="width:150px;float:left" align="right"> <font style="font-weight:bold;"><%=checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(rows[i].getString("equipment_type")) %> :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(!rows[i].getString("equipment_number").equals(""))
	  						 	{
	  						 		out.print("<font>"+rows[i].getString("equipment_number")+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;"> 						
  						<div style="width:150px;float:left" align="right"> <font style="font-weight:bold;">Type :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(rows[i].get("equipment_purpose",0)>0)
	  						 	{
	  						 		out.print("<font>"+checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(rows[i].get("equipment_purpose",0))+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;"> 						
  						<div style="width:150px;float:left" align="right"> <font style="font-weight:bold;">Status :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(rows[i].get("rel_type", 0)>0)
	  						 	{
	  						 		out.print("<font>"+checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(rows[i].get("rel_type", 0))+":"+checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(rows[i].get("equipment_status", 0))+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
		  	  </table>
		  	  </fieldset>
      	    </td>
      	    <td id="text" align="center" valign="middle" style='word-break:break-all'>
      	    <fieldset style="width:90%; border:2px solid blue;" >
	  				<legend>
	  				    <a style="color:#f60;font-size:14px;" target="_blank" >
				      		Time
	  					</a>
	  				</legend>
  			<table>
  			   		<div style="text-align:left;clear:both;"> 						
  						<div style="width:200px;float:left" align="right"> <font style="font-weight:bold;">GateCheckInTime :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(!rows[i].getString("check_in_time").equals(""))
	  						 	{
	  						 		out.print("<font>"+DateUtil.showLocalparseDateTo24Hours(rows[i].getString("check_in_time"),ps_id)+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;"> 						
  						<div style="width:200px;float:left" align="right"> <font style="font-weight:bold;">WindowCheckInTime :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(!rows[i].getString("check_in_window_time").equals(""))
	  						 	{
	  						 		out.print("<font>"+DateUtil.showLocalparseDateTo24Hours(rows[i].getString("check_in_window_time"),ps_id)+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
  					<div style="text-align:left;clear:both;"> 						
  						<div style="width:200px;float:left" align="right"> <font style="font-weight:bold;">WarehouseCheckInTime :</font>&nbsp;&nbsp;</div>
  						<div>
  						<%
	  						 	if(!rows[i].getString("check_in_warehouse_time").equals(""))
	  						 	{
	  						 		out.print("<font>"+DateUtil.showLocalparseDateTo24Hours(rows[i].getString("check_in_warehouse_time"),ps_id)+"</font>");
	  						 	}
	  					 %> 
  						</div>
  					</div>
		  	  </table>
	  		</fieldset>
      	    </td>
      	    <td  align="center" valign="middle" style='word-break:break-all'>
  			<%
			if (rows[i].get("approve_status",0)==2)
			{
			%>
					<span style="color:#990000;font-weight:normal"><%=rows[i].getString("note")%></span>
			<%
			}
			else
			{
			%>
      	     		<input type="text" name="note_<%=rows[i].getString("pad_id")%>" id="note" style="width:180px;height:25px;font-size:14px;border:#cccccc solid 1px;color:#FF0000" value="<%=rows[i].getString("note")%>"> 
			<%
			}
			%>
      	    </td>
      	    <td>
      	    	<% if(rows[i].get("approve_status",0)==2){
      	    	%>
      	    	    <img src='../imgs/standard_msg_ok.gif' width='16' height='16' align='absmiddle'> <font color="green"><strong>Approved</strong></font>
      	    	
      	    	<%
      	    	}else{
      	    	%>
      	    		<input onclick="checkOut(this,<%=rows[i].getString("pad_id")%>,<%=i%>)" <%=rows[i].get("approve_status",0)==2?"hidden":"" %> type="button" class="short-button" value="check out">&nbsp;
      	    		
      	    	<%
      	    	}
      	    	%>
      	    	
      	    </td>
      	</tr>
		
      	<% 		
      		}
      		
      	}else{
      	%>
      		<tr style="background:#E6F3C5;">
   						<td colspan="10" style="text-align:center;height:120px;">No Data</td>
   		    </tr>
      	<% }%>
	</table>
   <br/>
  <table width="100%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td align="center" valign="middle" bgcolor="#eeeeee">
	  <input name="Submit" type="submit" class="long-button-redtext" value="check out all" <%=flag==1?"hidden":"" %>> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input name="Submit2" type="button" class="long-button" value="Back" onClick="history.back();">
    </td>
  </tr>
</table>
</form>
</body>
</html>
<script>
function checkOut(evt,pad_id,last){
	var note = $(evt).parent().parent().find("#note").val();
	if(note===""){
		alert("Please enter reason");
		return false;
	}else{
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxCheckInPatrolVerifyDataSingleAction.action',
			data:{
				pad_id:pad_id,
				pa_id:$("input[name=pa_id]").val(),
				note:note
			},
			dataType:'json',
			type:'post',
			success:function(data){
				if(data.flag=="success"){
					location.reload();
				}
				
			},
			error:function(){
				alert("System Error!");
			}
		});
	}
	
}
function checkForm(theForm)
{
	var reason = theForm.note;
	var pad_ids = theForm.pad_ids;
	
	var haveEmpty = false;
	var oneSelected = false;
	
	//先判断是否数组
	if(typeof pad_ids.length == "undefined")
	{
			if (pad_ids.checked)
			{
				oneSelected = true;
			}
			
			if (reason.value=="")
			{
				reason.style.background = "#EDF36D";
				haveEmpty = true;
			}
	}
	else
	{
		for (i=0; i<pad_ids.length; i++)
		{
			if (pad_ids[i].checked)
			{
				oneSelected = true;
			}
			
			if (pad_ids[i].checked&&reason[i].value=="")
			{
				reason[i].style.background = "#EDF36D";
				haveEmpty = true;
			}
		}
	}
	
	if (!oneSelected)
	{
		alert("choice at least one");
		return(false);
	}
	
	if (haveEmpty)
	{
		alert("Please enter reason");
		return(false);
	}

	return(true);
}

function allCheck(all_or_no)
{
	if(all_or_no)
	{
		$("[name$='pad_ids']").attr("checked",true);
	}
	else
	{
		$("[name$='pad_ids']").attr("checked",false);
	}
		
}
</script>