<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.Enumeration"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="checkInMainDocumentsRelTypeKey" class="com.cwc.app.key.CheckInMainDocumentsRelTypeKey"/> 

<%
	long entry_id = StringUtil.getLong(request, "main_id");
	//main_id,detail_id,number_status,"",door_id,rel_type,isLive,equipmentId
	long detail_id = StringUtil.getLong(request, "detail_id");
	int  number_status = StringUtil.getInt(request, "number_status");
	
	int door_id = StringUtil.getInt(request, "door_id");
	int rel_type = StringUtil.getInt(request, "rel_type");
	int isLive = StringUtil.getInt(request, "isLive");
	int equipmentId = StringUtil.getInt(request, "equipmentId");
	int resourcesId = StringUtil.getInt(request, "resources_id");
	int number_type = StringUtil.getInt(request, "number_type");
	String title = StringUtil.getString(request, "title");
	int flag = StringUtil.getInt(request, "flag");
	int moreType = StringUtil.getInt(request, "more_type");
	
	DBRow[] loadBars=checkInMgrZwb.selectLoadBar();
	String loadBarSelect = "";
	for(DBRow row:loadBars){
		loadBarSelect += "<option value="+row.get("load_bar_id", 0)+">"+row.get("load_bar_name", "")+"</option>";
	}
	DBRow seal = checkInMgrWfh.findEquipmentByEquipmentId(equipmentId);
	
%>
<html>
  <head>
    <title>tally_sheet</title>
    
	<!--  基本样式和 javascript -->
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	
	<!-- 引入  Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	function saveLoadBar(main_id,equipment_id){
		var loadBarId = $(".loadBar").find("#loadBarName :selected").val();
		var loadBarName = $(".loadBar").find("#loadBarName :selected").text();
		var loadBarCount = $(".loadBar").find("#loadBarCount").val();
		if(loadBarCount==''){
			alert("Please Input Count!");
			return;
		}
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/saveLoadBar.action',
			data:{'load_bar_id':loadBarId,'main_id':main_id,'equipment_id':equipment_id,count:loadBarCount},
			dataType:'json',
			type:'post',
			beforeSend:function(request){
				
			},
			success:function(data){
					var count = $("#"+loadBarId).val();
					if(count * 1>0){
						loadBarCount= (count*1) + (loadBarCount*1);
						$("#"+loadBarId).val(loadBarCount);
					}else{ 
						var html = "<span><b style=\"width:68px;display: inline-block;text-align: right;\">"+loadBarName+"</b>:&nbsp;&nbsp;<input disabled='disabled' type='number' min='1' style='width: 100px;' id='"+loadBarId+"' value='"+loadBarCount+"' />"
						+"&nbsp;<input id='edit_"+loadBarId+"' style='margin-left:10px;' onclick='editLoadBar("+loadBarId+")' type='button' class='theme-button-edit' value='Edit'/>"
						+"<input id='save_"+loadBarId+"' style='margin-left:10px;display:none;' onclick='updateLoadBarCount("+main_id+","+equipment_id+","+loadBarId+")' type='button' class='theme-button-edit' value='Save'/>"
						+"</span><br/>";
						$("#showLoadBar").append(html);
					}
					$(".loadBar").find("#loadBarCount").val("");
			},
			error:function(){
				alert("System error"); 
				
			}
		});
	}
	function updateLoadBarCount(entry_id,equipment_id,loadBarId){
		var loadBarCount = $("#"+loadBarId).val()*1;
		if(loadBarCount<1 || loadBarCount==0){
			alert("Please enter the correct number");
		}
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/updateLoadBar.action',
			data:{'load_bar_id':loadBarId,'main_id':entry_id,'equipment_id':equipment_id,count:loadBarCount},
			dataType:'json',
			type:'post',
			beforeSend:function(request){
				
			},
			success:function(data){
				$("#"+loadBarId).attr("disabled","disabled");
				$("#edit_"+loadBarId).css("display","");
				$("#save_"+loadBarId).css("display","none");
			},
			error:function(){
				alert("System error"); 
				
			}
		});
	}
	function saveNote(){
		$.artDialog.data('note', $("#exception_note").val());
	}
	function editLoadBar(load_bar_id){
		$("#"+load_bar_id).removeAttr("disabled");
		$("#edit_"+load_bar_id).css("display","none");
		$("#save_"+load_bar_id).css("display","");
		
	}
	function changeUperCase(obj, event){
		
		$(obj).val($(obj).val().toUpperCase());
	}
	function toNA(obj){
		$($(obj).parent().children("input")[0]).val("NA");
		var id=$($(obj).parent().children("input")[0]).attr("id");
		if(id==1)
			$.artDialog.data('deliverySeal', $($(obj).parent().children("input")[0]).val());
		if(id==2)
			$.artDialog.data('pickUpSeal', $($(obj).parent().children("input")[0]).val());
	}
	
	function deliverySeal(obj){
		$.artDialog.data('deliverySeal', $(obj).val());
	}
	function pickupSeal(obj){
		$.artDialog.data('pickUpSeal', $(obj).val());
	}
	
</script>
<style type="text/css">
		.wrap-search-input{
	position: relative;
	display: inline-block;
}

.icon-search-input{
	background: url(imgs/search.png) no-repeat center center;
	position: absolute;
	cursor: pointer;
	top: 0px;
	right: 0px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.wrap-search-input input:focus+.icon-search-input{
	border-color:#66afe9;
	outline:0;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)
}

.wrap-search-input input:focus+.icon-na-input{
	border-color:#66afe9;
	outline:0;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)
}

.icon-na-input{
	background: url(../imgs/na_14x14.png) no-repeat center center;
	position: absolute;
	cursor: pointer;
	top: 0px;
	right: 0px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.align-width-span{
	
	display:-moz-inline-box;
	display:inline-block;
	width:18px;
	text-align: center;
}
	</style>
  </head>
  
  <body>
  	<div style="margin-left:30px;" id="test">
  		<B><%=title %></B>
  		<%if(flag==1){ %>
  			<div style="margin-top:30px;">
  			Note:&nbsp;<textarea style="width:250px" id="exception_note" type="textarea" onchange="saveNote()" ></textarea>
  			</div>
  		<%} %>
  	</div> 
  	<%if(moreType!=2){ %>
		<%if(rel_type!=checkInMainDocumentsRelTypeKey.DELIVERY && (number_type!=moduleKey.CHECK_IN_BOL&&number_type!=moduleKey.CHECK_IN_CTN&&number_type!=moduleKey.CHECK_IN_DELIVERY_ORTHERS)){ %>
  		 <fieldset style=" border: 2px solid ;border-radius: 5px;width:94%;margin-top:30px;">
  		 		<legend>Load Bar</legend>
			    	 <div class="loadBar">
			    	   <span>LoadBar: </span>
			    	   <select id="loadBarName"> <%=loadBarSelect%> </select>
			    	  <span style="margin-left:5px">Count: </span><input min="1" onkeypress=" if(event.keyCode==13) saveLoadBar('<%=entry_id %>','<%=equipmentId %>')" style="width:70px" id="loadBarCount" type="number" />
			    	  &nbsp;<input onclick="saveLoadBar('<%=entry_id %>','<%=equipmentId %>')" type="button" class="theme-button-ok" value="AddLoadBar" />
			    	  </div>
			    	  <br/>
			    	 
			    	  <div id="showLoadBar">
						<%
							DBRow[] loadBar = loadBarUseMgrZr.androidGetLoadBarUse(equipmentId, entry_id);
							if(loadBar!=null&&loadBar.length>0){
								for(DBRow row: loadBar){
									if(row==null)
										continue;
						%>
							<span style="display: inline-block;margin-top: 5px;">
							<b style="width:68px;display: inline-block;text-align: right;"><%=row.get("load_bar_name", " ") %></b>:&nbsp;
							<input disabled='disabled'  style='width: 100px;' onkeypress=" if(event.keyCode==13) updateLoadBarCount('<%=entry_id %>','<%=equipmentId %>','<%=row.get("load_bar_id", 0) %>')" type="number" min="1" id="<%=row.get("load_bar_id",0) %>" value="<%=row.get("count", 0) %>"/>
							<input id="edit_<%=row.get("load_bar_id", 0) %>" style="margin-left:10px;" onclick="editLoadBar('<%=row.get("load_bar_id", 0) %>')" type="button" class="theme-button-edit" value="Edit"/>
							<input id="save_<%=row.get("load_bar_id", 0) %>" style="margin-left:10px;display:none" onclick="updateLoadBarCount('<%=entry_id %>','<%=equipmentId %>','<%=row.get("load_bar_id", 0) %>')" type="button" class="theme-button-edit" value="Save"/>
							</span>
							<br>
						<%}} %>
					</div>
					
			    	  </div> 
			 </fieldset>
			
		 <%}%>
		 <%if(rel_type==checkInMainDocumentsRelTypeKey.DELIVERY || rel_type==checkInMainDocumentsRelTypeKey.BOTH){ %>
			 	<div class="wrap-search-input" style="margin-top:20px;"><span>Delivery Seal: </span><input onblur="deliverySeal(this)" onkeyup="changeUperCase(this, event)" name="seal_delivery" value="<%=seal!=null?seal.get("seal_delivery", ""):"" %>"   style="padding-right: 22px;"><span onclick="toNA(this)" class="icon-na-input" style="width: 22px; height: 22px; border-color: rgb(102, 175, 233);"></span></div>
		 <%} if(rel_type==checkInMainDocumentsRelTypeKey.PICK_UP || rel_type==checkInMainDocumentsRelTypeKey.BOTH){  %>		
		 		<div  class="wrap-search-input" style="margin-top:20px;"><span style="width: 73px;display: inline-block;padding-left:3px">PickUp Seal: </span><input onblur="pickupSeal(this)" onkeyup="changeUperCase(this, event)" name="seal_pickup" value="<%=seal!=null?seal.get("seal_pick_up", ""):"" %>" style="padding-right: 22px;"><span onclick="toNA(this)" class="icon-na-input" style="width: 22px; height: 22px; border-color: rgb(102, 175, 233);"></span></div>
		<%}} %>
  </body>
</html>