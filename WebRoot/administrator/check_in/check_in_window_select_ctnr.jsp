<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<jsp:useBean id="checkInMainDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey"></jsp:useBean>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<jsp:useBean id="occupyTypeKey" class="com.cwc.app.key.OccupyTypeKey"></jsp:useBean>
<%@page import="com.cwc.app.key.ModuleKey" %>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"></jsp:useBean>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"></jsp:useBean>

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
 
 <link rel="stylesheet" type="text/css" href="../js/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="../js/easyui/themes/icon.css" />

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<%
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
String number = StringUtil.getString(request, "number");
DBRow[] equipments = checkInMgrZyj.findInYardDropEquipmentsByNumber(number, adminLoggerBean.getPs_id());	
%>

<title>Check In Select Equipments</title>
<script type="text/javascript">

function setEquipment(equipment_id){
	
	var equipment = $("#"+equipment_id).text();
	
	$.artDialog.opener.setPickUp($.parseJSON(equipment));
	
	$.artDialog && $.artDialog.close();
}
</script>
<style type="text/css">
table tr td{padding: 10px;font-size: 14px;text-align: center;}
</style>
</head>
<body onLoad="onLoadInitZebraTable()">
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	<tr>
		<th class="left-title" style="vertical-align:center;text-align:center;">Equipment Number</th>
		<th class="left-title" style="vertical-align:center;text-align:center;">Occupy Resources</th>
		<th class="left-title" style="vertical-align:center;text-align:center;">Entry In</th>
		<th class="left-title" style="vertical-align:center;text-align:center;">Status</th>
		<th class="left-title" style="vertical-align:center;text-align:center;">Seal</th>
		<th class="left-title" style="vertical-align:center;text-align:center;">Tasks</th>
	</tr>
	<%for(int i = 0; i < equipments.length; i ++){ %>
		<tr onclick="setEquipment('<%=equipments[i].get("equipment_id", 0L)%>')">
			<td>
				<%=equipments[i].getString("equipment_number") %>&nbsp;
				<span style="display: none;" id="<%=equipments[i].get("equipment_id", 0L)%>"><%=StringUtil.convertDBRowsToJsonString(equipments[i])%></span>
			</td>
			<td>
				<%DBRow[] occupys = (DBRow[])equipments[i].get("resources", new DBRow[0]); %>
				<%for(int j = 0; j < occupys.length; j ++){ %>
					<%=occupyTypeKey.getOccupyTypeKeyName(occupys[j].get("resources_type", 0))  %> : 
					<%=occupys[j].getString("resources_name") %>
					<%=(j == occupys.length-1)?"":"<br>" %>
				<%} %>
			</td>
			<td>
				E<%=equipments[i].get("check_in_entry_id", 0L) %>
			</td>
			<td>
				<%=checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(equipments[i].get("equipment_status", 0)) %>
			</td>
			<td align="left" style="text-align: left;">
				Delivery:<%=StrUtil.isBlank(equipments[i].getString("seal_delivery"))?"NA":equipments[i].getString("seal_delivery") %><br>
				Pick Up :<%=StrUtil.isBlank(equipments[i].getString("seal_pick_up"))?"NA":equipments[i].getString("seal_pick_up") %>
			</td>
			<td align="left">
				<%DBRow[] tasks = checkInMgrZyj.findTasksByEquipmentId(equipments[i].get("equipment_id", 0L)); %>
				<%if(tasks.length > 0){ %>
				<%for(int j=0; j < tasks.length; j ++){ %>
					<%=moduleKey.getModuleName(tasks[j].get("number_type", 0)) %>:
					<%=tasks[j].getString("number") %>&nbsp;<%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(tasks[j].get("number_status", 0))%><br>
				<%} %>
				<%}else{out.println("&nbsp;");} %>
			</td>
		</tr>
	<%} %>
</table>
</body>
</html>