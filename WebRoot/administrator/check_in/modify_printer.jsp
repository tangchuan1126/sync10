<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*"%>
<%@page import="com.cwc.util.*,com.cwc.app.util.*"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.util.StringUtil"%>
<%
	String p_id = StringUtil.getString(request, "id");
	String type = StringUtil.getString(request, "type");
	String size = StringUtil.getString(request, "size");
	String psId = StringUtil.getString(request, "psId");
	String name = StringUtil.getString(request, "name","");
	String servers_name=StringUtil.getString(request, "servers_name");
	String ip = StringUtil.getString(request, "ip","");
	String port = StringUtil.getString(request, "port","");
	String servers = StringUtil.getString(request, "servers","");
	String p_type = StringUtil.getString(request, "p_type");
	String x = StringUtil.getString(request, "x");
	String y =StringUtil.getString(request, "y");
	String lat =StringUtil.getString(request,"lat","");
	String lng =StringUtil.getString(request,"lng","");
	String pageType =StringUtil.getString(request, "pageType","1");
	String area_name =StringUtil.getString(request, "area_name","");
	String area_id =StringUtil.getString(request, "area_id");
	DBRow[] allStorageKml = googleMapsMgrCc.getAllStorageKml();
	DBRow[] printerServers = androidPrintMgrZr.getAllAndroidPrinterServerByPsId(Long.parseLong(psId));
%>
<head>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript"
	src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript"
	src="../js/scrollto/jquery.scrollTo-min.js"></script>


<script type="text/javascript"
	src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css"
	type="text/css" />

<link rel="stylesheet"
	href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css"
	type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script src="../js/maps/verify.js" type="text/javascript"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<style type="text/css">
td.topBorder {
	border-top: 1px solid silver;
}
</style>
<style type="text/css">
.ui-datepicker {
	font-size: 0.9em;
}
</style>
<script type="text/javascript">
$(function () { 
	//添加button
	var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	var updateDialog = null;
	var addDialog = null;
	api.button([
			{
				name: 'Delete',
				callback: function () {
					doDelete('<%=name%>','<%=p_id%>','<%=psId%>');
					return false;
				},
			},
			{
				name: 'Submit',
				callback: function () {
					commitData();
					return false;
				},
				focus:true,
				focus:"default"
			},
			{
				name: 'Cancel',
				callback:function (){
					closeWindow('<%=p_id%>','cancel');
					return false;
				},
			}] 
		);
	if("<%=lat%>"!="" && "<%=lng%>"!=""){
		convertCoordinateAjax(<%=psId%>,"<%=lat%>","<%=lng%>");
	}
	//initServersSelect();
	$("select[name='type']").val(<%=type%>);
	setOption();
	$("select[name='size']").val(<%=size%>);
	if("<%=psId%>"!=""){
		$("#storage").val("<%=psId%>");
		}else{
		$("#storage option:first").prop("selected", 'selected');
		}
	if("<%=servers%>"!=""){
		$("#servers_id").val("<%=servers%>");
		$("#servers_name").val("<%=servers_name%>");
		}/* else{
		$("#servers option:first").prop("selected", 'selected');
		} */
	if("<%=p_type%>"=="0"){
		$("input:radio[value='0']").attr('checked','true');
		$("#_servers").hide();
		$("#_ip").show();
		$("#_port").show();
	}else{
		$("input:radio[value='1']").attr('checked','true');
		$("#_servers").show();
		$("#_ip").hide();
		$("#_port").hide();
	};
	
	
});

function onSelect(){
	var p_type=$("input[name='radioType']:checked").val();
	if(p_type=="0"){
		$("#_servers").hide();
		$("#_ip").show();
		$("#_port").show();
	}else if(p_type=="1"){
		$("#_servers").show();
		$("#_ip").hide();
		$("#_port").hide();
	}
	
}
function setOption(){
    var val = $("select[name='type']").val();
	var lable="<option value='0'>60*30</option>"+
		"<option value='1'>80*35</option>"+
		"<option value='2'>80*40</option>"+
		"<option value='3'>100*50</option>"+
		"<option value='4'>120*152</option>";
	var letter="<option value='5'>216*275</option>"+
		"<option value='6'>210*297</option>";
	if(val ==0){
		$("select[name='size']").html(lable);
	}else if(val==1){
		$("select[name='size']").html(letter);
	}
}
function convertCoordinateAjax(psId ,lat,lng){
	$.ajax({
		url:<%=ConfigBean.getStringValue("systenFolder")%>+'action/administrator/gis/convertCoordinate.action',
		data:'psId='+psId+'&lat='+lat+'&lng='+lng,
		dataType:'json',
		type:'post',
		async:true, 
		beforeSend:function(request){
	    },
		success:function(data){
	    	if(data&&data.flag=="true"){
	        var x =data.x;
	        var y =data.y;
	    	$("#x").val(x);
	    	$("#y").val(y);
	    	}
		},
		error:function(){
		}
	});
	}
function closeWindow(printerId,flag) {
	var pageType='<%=pageType%>';
	
	if(pageType=="0"){
	$.artDialog.opener.jsmap.clearDomePrinter('<%=psId%>')
	}
    $.artDialog.opener.reLoadPrinter('<%=psId%>',printerId,flag);
	$.artDialog.opener.reSetObjectDraggable(7,'<%=psId%>','<%=p_id%>');
	$.artDialog.close();
}
function updateData(){
   	 var ischanged =0;
   	 if($("#x").val()!="<%=x%>" || $("#y").val()!="<%=y%>"){
   		 ischanged=1;
   	 }
   	 var p_type =$("input[name='radioType']:checked").val();
   	 var service =$("#servers_id").val();
   	var p_type=$("input:radio[name='radioType']:checked").val();
   	 var servers_name =$("#servers_name").val();
   	 var flag1=false,flag2=false;
   	 var type=$("select[name='type']").val();
   	 var ip=$("#ip").val();
   	 var port=$("#port").val();
   	 var x=$("#x").val();
   	 var y=$("#y").val();
   	 var size=$("select[name='size']").val();
   	 var area_id=$("#area_id").val();
   	 var oldservers='<%=servers%>';
   	 var oldip='<%=ip%>';
   	 var oldport='<%=port%>';
   	 var oldName='<%=name%>';
   	if($("#name").val()==''){
   		verifyName();
   	}
   	
   	if(p_type==0){
 		 if(ip==''){
 			verifyIP();
 		 }
 		 if(port==''){
 			verifyPort();
 			
 		 }
 		 if(verifyMgs['verifyName']==0 || verifyMgs['verifyName']==2  ){
 			 $("#name").focus();
 			 return;
 		 }
 		 if(verifyMgs['verifyIP']==0 || verifyMgs['verifyIP']==2){
 			 $("#ip").focus();
 			 return;
 		 }
 		 if(verifyMgs['verifyPort']==0 ||
  				verifyMgs['verifyPort']==2){
 			 $("#port").focus();
 			 return;
 		 }
 		if("<%=p_type%>"!=p_type ||"<%=area_id%>"!=area_id ||
 			   	 "<%=size%>"!=size|| "<%=y%>"!=y|| "<%=x%>"!=x ||
 			   	 "<%=type%>"!=type || oldip!=ip
 			   	 || oldport!=port){
 			   	 	flag1= true;
 			  }
 	 }else{
 		if(service==""){
 			$('#VServersNameError').show();
 			$("#VServersNameRight").hide();
 			$("#VServersNameError").attr('title','Server cannot be empty!');
 			verifyMgs['VServersName']=0;
 	   	 }else{
 	   		$('#VServersNameError').hide();
 			$("#VServersNameRight").show();
 			verifyMgs['VServersName']=1;
 	   	 }
 		if(verifyMgs['verifyName']==0 || verifyMgs['verifyName']==2  ){
			 $("#name").focus();
			 return;
		 }
 		if(verifyMgs['VServersName']==0){
 			 return;
 		}
 		if("<%=p_type%>"!=p_type ||"<%=area_id%>"!=area_id ||
 			   	 "<%=size%>"!=size|| "<%=y%>"!=y|| "<%=x%>"!=x ||
 			   	 "<%=type%>"!=type || oldservers!=service ){
 			   	 	flag1= true;
 			  }
 	 }
   	
	 
   	 
   	if(verifyMgs[verifyX]==0 || verifyMgs[verifyX]==2){
		$("#x").focus();
		return;
	}
	if(verifyMgs[verifyY]==0 || verifyMgs[verifyY]==2){
		$("#y").focus();
		return;
	}
   	 if(oldName!=$("#name").val() ){
   	 	flag2=true;
   	 } 	
   	 
  	
   	 if(!(flag1|| flag2)){
   	 	showMessage(" didn't any change propertys","error");
   	 }else{
   		 var printer_data={};
   		 $.each($("#printer").serializeArray(),function(i,field){
   			 if(field.name=='radioType'){
   				printer_data['p_type']=field.value;
   			 }else if(field.name=='serversName'){
   				printer_data['servers_name']=field.value;
   			 }else if(field.name=='zone'){
   				printer_data['area_name']=field.value;
   			 }else{
   				printer_data[field.name]=field.value;
   			 }
   		 });
   		
   		 
		 $.ajax({
		     url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/modifyPrinter.action',
			type:'post',
			dataType:'json',
			data:$("#printer").serialize()+'&ischanged='+ischanged+'&pageType=<%=pageType%>&psId='+$("#storage").val()+'&p_type='+p_type+'&servers_name='+servers_name,
			async:false, 
			beforeSend:function(request){
			 	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
				$.unblockUI();
				if(data && data.flag=="true"){
					showMessage("Save succeed","succeed");
					printer_data['physical_area']=data.id;
					printer_data['latlng']=data.latlng;
					
					if(data.id){
						delete printer_data.id;
						printer_data.p_id=data.id;
					}
					
					 var printer_data1=$.artDialog.opener.jsmap.storageBounds[printer_data.ps_id+'_printer'];
					 if(!printer_data1 ){
							$.artDialog.opener.jsmap.storageBounds[printer_data.ps_id+'_printer']=[printer_data];
						}else{
							var isExist=false;
							for(var i=0;i<printer_data1.length;i++){
								if(printer_data1[i].p_id==printer_data.p_id){
									printer_data1[i]=printer_data;
									isExist=true;
								}
							}
							if(!isExist){
								printer_data1[printer_data1.length]=printer_data;
							}
						}  
					 closeWindow(printer_data.p_id,'add_or_update');
				}else if(data && data.flag=="exist"){
					showMessage("printerName is exist","error");
				}else if(data&& data.flag=="authError"){
					showMessage("Insufficient permissions!","error");
				}else{
					showMessage("Save failed!","error");
				}
			},
	   	 	error:function(){
	   	 	$.unblockUI();
	   		showMessage("System error","error");
	   	  }
	         });
      }
   
}

	
function commitData(){
	 if($("#servers_id").val()!="<%=servers%>" ||$("#name").val()!="<%=name%>"||$("#size").val()!="<%=size%>"||$("#x").val()!="<%=x%>" || $("#y").val()!="<%=y%>" ||$("select[name =\"type\"]").val()!="<%=type%>"||$("#zone").val()!="<%=area_name%>"||$("#storage").val()!="<%=psId%>"){
		updateData(); 
	  }else{
      closeWindow();  
	  }
		
}
	document.onkeydown=function(evt){
	  var evt=evt?evt:(window.event?window.event:null);//兼容IE和FF
	  if (evt.keyCode==13){
		  commitData();
	   }
	}
	
function selectZone(){
		var psId=$("#ps_id").val();
		var name=$("#name").val()?$("#name").val():"";
		var type="-1"
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_area_single.html?ps_id='+psId+'&area_type='+type;
		$.artDialog.open(url, {title: "Select Area ["+name+"]",width:'790px',height:'425px', lock: true,opacity: 0.3});
	}
	
function backFillArea(names,ids,eleId){
	$("#zone").val(names);
	$("#area_id").val(ids);
}
function doDelete(printerName,p_id,ps_id){
	 $.artDialog({
		 	title:'Notify',
		 	lock: true,
		 	opacity: 0.3,
		 	icon:'question',
		    content: "<span style='font-weight:bold;'>Delete ["+printerName+"] ?</span>",
		    button: [
		        {
		            name: 'YES',
		            callback: function () {
		            	deletePrinter(p_id,ps_id);
		            },
		            focus: true
		        },
		        {
		            name: 'NO'
		        }
		    ]
		});
	
	
}
function deletePrinter(p_id,ps_id){
	 $.ajax({
	     url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/StorageLocationDeleteAction.action',
		type:'post',
		dataType:'json',
		data:{"location_id":p_id,"ps_id":ps_id,"type":7},
		async:false, 
		beforeSend:function(request){
	     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success: function(data){
			if(data && data.flag=="true"){
				showMessage("Delete succeed ","succeed");
			}else{
				showMessage("Delete fail ","error");
			}
		 $.unblockUI();
   		 closeWindow(p_id);
		},
   	 	error:function(){
   		showMessage("System error","error");
   		$.unblockUI();
   	  }
         });
}
var verifyMgs={};
function verifyName(){
	var oldName='<%=name%>';
	var vMsg="";
	if($("#name").val()==''){
		vMsg="name is empty!";
	}
	if(oldName!=''&& ($("#name").val()=='<%=name%>')){
		$("#VNameRight").show();
		 $("#VNameError").hide();
		 verifyMgs['verifyName']=1;
	}else{
		verifyMgs['verifyName']=verify('name','VNameRight','VNameError','','<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/verifyName.action','ps_id=<%=psId%>&type=7&name='+$('#name').val(),vMsg);
	}
	
}
function verifyPort(){
	var vMsg="";
	if($('#port').val()==''){
		vMsg="Port is empty!";
	}else{
		vMsg="Data format error";
	}
		var regInt = new RegExp("^([1-9][0-9]*)$");
		verifyMgs['verifyPort']=verify('port','VPortRight','VPortError',regInt,'','',vMsg);
}
function verifyIP(){
	var vMsg="";
	if($('#ip').val()==''){
		vMsg="IP is empty!";
	}else{
		vMsg="Data format error";
	}
		var regIp = new RegExp("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
		verifyMgs['verifyIP']=verify('ip','VIPRight','VIPError',regIp,'','',vMsg);
}
function verifyX(){
	  var vMsg="";
	  if($("#x").val()==''){
		  vMsg="x is Empty!";
	}else{
		vMsg="Data format error";
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs[verifyX]=verify('x','x_con','x_del',reg,'','',vMsg);
}
function verifyY(){
	  var vMsg="";
	  if($("#y").val()==''){
		  vMsg="y is Empty!";
	}else{
		vMsg="Data format error";
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs[verifyY]=verify('y','y_con','y_del',reg,'','',vMsg);
}
/**
 * 选择printer server
 */
 function add_printerServers(){
	 var url='<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/printers_list.html?ps_id=<%=psId%>';
     $.artDialog.open(url, {title: "Choose Printer Servers ",width:'50%',height:'60%', lock: false,opacity: 0.3,fixed: true,id:"create_servers"})
}
/**
 * 回填printer server
 */
 function backFillPrinterServer(s_id,s_name){
	$("#servers_name").val(s_name);
	$("#servers_id").val(s_id);
}
</script>
</head>
<body>
	<form id="printer">
		<table width="100%" height="100%" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td align="center" valign="top" colspan="2">
						<table>
							<tr style="display:none">
							<th width="35%"  >Storage:</th>
							<td width="65%"  >
							<select id="storage" style="width: 100%">
									<%
								if (allStorageKml != null) {
									for (int i = 0; i < allStorageKml.length; i++) {
										DBRow kml = allStorageKml[i];
							         %>
									<option value="<%=kml.getString("ps_id")%>"><%=kml.getString("title")%></option>
									<%
			                         }
								}  
			                         %>
							</select>
							</td>
							</tr>
							<tr>
								<td align="right" >Name：</td>
								<td align="left" style="position: relative;" ><input type="text"
									style="width: 85%;" name="name" id="name" onchange="this.value=this.value.trim(),verifyName()"
									value="<%=name%>" /><span id="vNameMsgInfo" style="position: absolute;top: 5px;height: 16px;width:16px;">
									<img  title="Name is exist" src="../imgs/maps/delete_red.jpg" id="VNameError" style="display: none;">
									<img  src="../imgs/maps/confirm.jpg" id="VNameRight" style="display: none;">
									</span>
									
								</td>
								
							</tr>
							<tr>
								<td align="left" colspan="2">
								<input type="radio"	name="radioType" value="0"  onclick="onSelect()"/>IP-Printer	
								<input type="radio"  name="radioType" value="1" onclick="onSelect()"/>Servers-Printer
								</td>
								
							</tr>
							<tr style="display:none" id="_servers" >
								<td align="right" >Servers：</td>
								<td align="left" style="position: relative;">
								<%-- <select name ="servers" id="servers_Select" style="width: 85%" on>
								<option value ="-1">Choose servers...</option>
								
								<% if(printerServers.length>0){
										for(int i=0;i<printerServers.length;i++){
											DBRow server=printerServers[i];
									%>
									<option value ="<%=server.getString("printer_server_id") %>"><%=server.getString("printer_server_name") %></option>
									<%	}
								}%> 
								</select>--%>
								<input type="text" name="serversName" id="servers_name" value="Choose Servers!" onfocus="add_printerServers()" style="width: 85%;" >
								<span id="vNameMsgInfo" style="position: absolute;top: 5px;height: 16px;width:16px;">
									<img  title="Name is exist" src="../imgs/maps/delete_red.jpg" id="VServersNameError" style="display: none;">
									<img  src="../imgs/maps/confirm.jpg" id="VServersNameRight" style="display: none;">
									</span>
								<input type="hidden" name="servers" id="servers_id">
								</td>
							</tr>
							<tr style="display:none" id="_ip">
								<td align="right" >IP ：</td>
								<td align="left" style="position: relative;" ><input type="text"
									style="width: 85%;" name="ip" id="ip"
									value="<%=ip%>" onchange="this.value=this.value.trim(),verifyIP()"/><span id="VIpMsg" style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img  title="IP is incorrect, please re-enter" src="../imgs/maps/delete_red.jpg" id="VIPError" style="display: none;">
									<img  src="../imgs/maps/confirm.jpg" id="VIPRight" style="display: none;">
									</span>
								</td>
								
							</tr>
							<tr style="display:none" id ="_port">
								<td align="right" >Port：</td>
								<td align="left" style="position: relative;" ><input type="text"
									style="width: 85%;" name="port" id="port"
									value="<%=port%>" onchange="this.value=this.value.trim(),verifyPort()"/><span id="VPortMsg" style="position: absolute;top: 5px;height: 16px;width:16px;">
									<img  title="Port is incorrect, please re-enter" src="../imgs/maps/delete_red.jpg" id="VPortError" style="display: none;">
									<img  src="../imgs/maps/confirm.jpg" id="VPortRight" style="display: none;">
									</span>
								</td>
								
							</tr>
							<tr>
								<td align="right" >X Position：</td>
								<td align="left" style="position: relative;"><input type="text"
									style="width: 85%;" name="x" id="x"
									value="<%=x%>" onchange="verifyX()"/>
									<span style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img id="x_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             		<img id="x_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
									</span>
								</td>
								
							</tr>
							<tr>
								<td align="right" >Y Position：</td>
								<td align="left" style="position: relative;"><input type="text"
									style="width: 85%;" name="y" id="y"
									value="<%=y%>" onchange="verifyY()" />
									<span style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img id="y_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             		<img id="y_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
									</span>
								</td>
								
							</tr>
							<tr>
								<td align="right" >Type：</td>
								<td align="left" >
								<select name="type" style="width: 85%" onchange="setOption()">
								<option value="0">Label </option>
								<option value="1">Letter/A4</option>
								</select>
								</td>
							</tr>
							<tr>
								<td align="right" >Size：</td>
								<td align="left" >
								<select name="size"  style="width: 85%">
								<option value='0'>60*30</option>
								<option value='1'>80*35</option>
								<option value='2'>80*40</option>
								<option value='3'>100*50</option>
								<option value='4'>120*152</option>
								</select>
								</td>
							</tr>
							<tr>
								<td align="right" >Physical Area：</td>
								<td align="left" ><input type="text"
									style="width: 85%;" name="zone" id="zone"
									value="<%=area_name %>" onfocus="selectZone()" />
									<input name="area_id" id="area_id" type="hidden" value="<%=area_id%>" />
								</td>
							</tr>
						</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center"><span
					style="color: red;" id="errInfo">&nbsp;</span></td>
			</tr>
			<input id="p_id" name="p_id" type="hidden" value="<%=p_id%>" />
			<input id ="ps_id" name="ps_id" type="hidden" value="<%=psId%>" />
	
		</table>
	</form>
</body>
