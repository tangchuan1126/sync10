<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>

<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder")%>';
</script>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<%
	String type_number_door = StringUtil.getString(request, "type_number_door");
	long entry_id = StringUtil.getLong(request, "entry_id");
	String out_seal = StringUtil.getString(request, "out_seal");
	String jsonString = StringUtil.getString(request, "jsonString");
	JSONArray jsons = new JSONArray(jsonString);
	
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long adminId=adminLoggerBean.getAdid();
	DBRow[] titleRow=checkInMgrZwb.findTitleByAdminId(adminId);
%>

<title>batch print</title>
<script type="text/javascript">
$(function(){
	var jsons = [];
	jsons = eval('<%=jsons%>');
	for(var i=0;i<jsons.length;i++)
	{
		json = jsons[i];
		var checkDataType = json.checkDataType;
		var number = json.number;
		var door_name = json.door_name;
		var companyId = json.companyId;
		var customerId = json.customerId;
		var length = json.length;
		var out_seal = json.out_seal;
		var number_type=json.number_type;
		var order=json.order;
		var receipt_no = json.receipt_no;
		console.log("rece:"+receipt_no);
		var load="";
		if(checkDataType.toUpperCase()=="PICKUP"){
			loadingWms(number,door_name,companyId,customerId,'<%=out_seal%>',number_type,order);
		}else{
			var ctnr="";
			var bol="";
			if(number_type==<%=ModuleKey.CHECK_IN_CTN%>){
				ctnr=number;
			}
			if(number_type==<%=ModuleKey.CHECK_IN_BOL%>){
				bol=number;
			}
			loadReceiptWms(receipt_no, ctnr,bol,door_name,companyId,customerId,'<%=out_seal%>');
		}
	}
});

</script>
</head>
<body onLoad="onLoadInitZebraTable()">

<div id="tabs" >
	<ul>
		<li><a href="#bill_lading" onclick="loadingLableTemplate()">BILL OF LADING</a></li>
		<li><a href="#ticket" >Loading/Receipts Ticket</a></li>	 
		<li><a href="#counting_sheet" onclick="loadingSelctCountingSheet()">Counting Sheet</a></li>
	</ul>
	<div id="bill_lading" >
		<div id="lableTemplatelist"></div>
	</div>
    <div id="ticket" align="center" style="overflow:auto">
     <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
       		<input type="button" class="short-short-button-print"  value="Print"  onclick="printLoadsAndReceipts()"/>
       </div>
       <table style="width: 100%">
			<tr>
				<td>
					<div align="center" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width: 430px;">
						LOADING TICKET
				    </div>
				</td>
				<td>
					<div align="center" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width: 430px;">
						RECEIPTS TICKET
				    </div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="av1" align="center" style="overflow:auto;width: 430px;">
				    	<div id="loadQian" name="loadQian" style="display:block; border:0px solid red;" align="center"  >
				            
				    	</div>
				    </div>
				</td>
				<td valign="top">
					<div id="av2" align="center" style="overflow:auto;width: 430px;">
				    	<div id="receiptWms" name="receiptWms" style=" display:block; border:0px solid red;" align="center"  >
				            
				    	</div>
				    </div>
				</td>
			</tr>
		</table>
    </div>
    <div id="counting_sheet">
    	<div id="counting_sheet_list"></div>
    </div>
</div>

	
<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
 $("#tabs").tabs("select",0);
 
 (function(){
		$.blockUI.defaults = {
		 css: { 
		  padding:        '8px',
		  margin:         0,
		  width:          '170px', 
		  top:            '45%', 
		  left:           '40%', 
		  textAlign:      'center', 
		  color:          '#000', 
		  border:         '3px solid #999999',
		  backgroundColor:'#ffffff',
		  '-webkit-border-radius': '10px',
		  '-moz-border-radius':    '10px',
		  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		 },
		 //设置遮罩层的样式
		 overlayCSS:  { 
		  backgroundColor:'#000', 
		  opacity:        '0.6' 
		 },
		 
		 baseZ: 99999, 
		 centerX: true,
		 centerY: true, 
		 fadeOut:  1000,
		 showOverlay: true
		};
	})();
 
 function loadingLableTemplate(){
	$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_select_lableTemplate.html',
			dataType:'html',
			async:'false',
			data:'jsonString=<%=jsonString%>&out_seal=<%=out_seal%>&entryId=<%=entry_id%>',
			success:function(html){	
				$('#lableTemplatelist').html(html);
			}
	});
 }
 
 function loadingSelctCountingSheet(){
		$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_select_cunting_sheet_lableTemplate.html',
				dataType:'html',
				async:'false',
				data:'jsonString=<%=jsonString%>&out_seal=<%=out_seal%>&entryId=<%=entry_id%>',
				success:function(html){	
					$('#counting_sheet_list').html(html);
				}
		});
	 }
</script>
</body>
</html>

<script>
	 function loadingWms(number,door_name,CompanyID,CustomerID,out_seal,number_type,order){
			 //alert(load+","+door_name+","+CompanyID+","+CustomerID+","+out_seal);
			 var para = 'entryId=<%=entry_id%>&number='+number+'&door_name='+door_name
			 +'&CompanyID='+CompanyID+'&CustomerID='+CustomerID+'&out_seal=<%=out_seal%>&number_type='+number_type+'&order_no='+order;
			
			 $.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/print_order_wms.html',
					dataType:'html',
					async:false,
					data:para,
					success:function(html){	
						$(html).appendTo($('#loadQian'));
					}
			});
	 }
	 function loadReceiptWms(receipt_no,ctnr,bol,door_name,CompanyID,CustomerID,out_seal){
			 var para = 'entryId=<%=entry_id%>&ctnr='+ctnr+'&bol='+bol+'&door_name='+door_name+'&CompanyID='+CompanyID
					+'&CustomerID='+CustomerID+'&out_seal=<%=out_seal%>&receipt_no='+receipt_no;
			 $.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>check_in/print_receipts_wms_by_bol_container.html',
					dataType:'html',
					async:false,
					data:para,
					success:function(html){	
						$(html).appendTo($('#receiptWms'));
					}
			});
	 }
    function printLoadsAndReceipts()
    {
    	if($("#loadQian:has(div)").length > 0)
    	{
    		printWms();
    	}
		
    	if($("#receiptWms:has(div)").length > 0)
    	{
    		printReceiptWms();
    	}
    }
    
    
    function openLableTemplate(title_id){
 		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_load_lable_template.html?jsonString=<%=jsonString%>&entry_id=<%=entry_id%>&out_seal=<%=out_seal%>&title_id='+title_id;
 		 $.artDialog.open(url, {title: "Lable Template",width:'800px',height:'600px', lock: false,opacity: 0.3,fixed: true});
 	}
     
</script>
 
