<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<%
       long num=StringUtil.getLong(request,"number");
       long zone_id=StringUtil.getLong(request,"zone_id");
       long wait_id=StringUtil.getLong(request,"wait_id");
       long entry_id=StringUtil.getLong(request,"entry_id");
       String order_id=StringUtil.getString(request,"order_id");
       String order_type=StringUtil.getString(request,"order_type");
       DBRow[] rows=checkInMgrZwb.selectAllCheckInWait(zone_id);
       DBRow row=checkInMgrZwb.findWaitById(wait_id);    
%>

<title>Check In</title>
</head>
<body onLoad="onLoadInitZebraTable()">
<div id="tabs">
	<ul>	 
		<li><a href="#av1">Current Waiting List</a></li>	
		<li><a href="#av2">All Waiting Door</a></li>	
	</ul>
	<div id="av1">
		<div align="left" style="margin-top:2px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">	
			<span >Please submit waiting reason!</span>
		</div>	
		<div align="right" style="margin-top:2px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">	
    		<div align="left">
    			 <input type="radio" name="ra" value="0" checked="checked"  />
    			<%=order_type %>:&nbsp;<%=order_id %> 
    		</div>
    		<div align="left">
	    		<input type="radio" name="ra" value="1"  />
	    		ZONE:&nbsp;<%=zone_id==0?"N/A":zone_id%>
	    	</div>
	    	<br/>
	    	<div align="left">
	    		<textarea name="" cols="84" rows="3" id="yuanyin"></textarea>
	    	</div>
	    	<br/>
    		<input type="button" value="ADD WAITING" class="long-long-button" onclick="lvd()" />&nbsp;&nbsp;
    	</div>   
		<br/>
	    <div id="waitno" align="left" style="margin-top:2px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">	
    		<%if(row!=null){%>
	    		<div style="border-bottom:1px solid #69C">WAITING NUMBER:&nbsp;<span style="font-size:14px; color:blue"><%=row.get("waiting_id",0l) %></span></div>
	    		<div>ENTRYID:<span style="font-size:14px; color:blue" ><%=entry_id%></span></div>
	    		<%if(row.get("zone_id",0l)!=0){ %>
	    		<div>WAITING ZONE:<span style="font-size:14px; color:blue" ><%=zone_id%></span></div>
	    		<%} %>
	    		<%if(row.get("bill_id",0l)!=0){ %>
	    		<div>WAITING BILL:<span style="font-size:12px; color:blue" ><%=row.get("bill_id",0l) %></span></div>
	    		<div>BILL TYPE:<span style="font-size:12px; color:blue" ><%=row.getString("bill_type") %></span></div>
	    		<%} %>
	    		<div>REASON:<span style="font-size:12px; color:blue" ><%=row.getString("reason") %></span></div>
    	    <%}%>
    	</div>    
    </div> 
    <div id="av2">
    	 <div align="left" style="margin-top:2px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">	
    		<form id="mySeachForm" method="post" action="">
	    		Please enter ZONE:&nbsp;<input type="text" name="zone_id" />&nbsp;&nbsp;
	    		<input type="hidden" value="1" name="number" />
	    		<input type="button" value="Inquiry" class="button_long_search" onclick="seach()" />
    		</form>
    	 </div>
    	 <br/>
    	 <div>
   	 	    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		    	<tr> 
			        <th width="10%" class="right-title" style="vertical-align: center;text-align: center;">Number</th>
			        <th width="20%" class="left-title" style="vertical-align: center;text-align: center;">ZONE</th>
			        <th width="24%" class="left-title" style="vertical-align:center;text-align:center;">Entry ID</th>
			        <th width="21%" class="left-title" style="vertical-align:center;text-align:center;">BILL ID</th>
			        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">TYPE</th>
			        <th width="21%" class="left-title" style="vertical-align: center;text-align: center;">Status</th>
				</tr>
				<%for(int i=0;i<rows.length;i++){ %>
				<tr>
					<td align="center"><%=rows[i].get("waiting_id",0l) %>&nbsp;</td>
					<td align="center"><%=rows[i].get("zone_id",0l) %>&nbsp;</td>
					<td align="center"><%=rows[i].get("entry_id",0l) %>&nbsp;</td>
					<td align="center"><%=rows[i].getString("bill_id") %>&nbsp;</td>
					<td align="center"><%=rows[i].getString("bill_type") %>&nbsp;</td>
					<td align="center">
					<%if(rows[i].get("wait_status",0l)==1){ %>
						WAITING
					<%}else{ %>
						NONE
					<%} %>
					</td>
				</tr>
				<%} %>
			</table>
    	 </div>
    </div>  
</div> 
<form id="myfrom" >
	<input type="hidden" name="zone_id"  id="zone_id"  value="<%=zone_id%>"  />
	<input type="hidden" name="entry_id" id="entry_id" value="<%=entry_id%>" />
	
	<input type="hidden" name="order_id" id="order_id" value="<%=order_id%>" />
	<input type="hidden" name="order_type" id="order_type" value="<%=order_type%>" />
	
	
	
	<input type="hidden" name="wait_id"  id="wait_id"  />
</form>
<script type="text/javascript">
 $("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	cookie: { expires: 30000 } ,
 });
 $("#tabs").tabs("select",<%=num%>);
</script> 
</body>
</html>
<script>
	function seach(){
		$('#mySeachForm').submit();
	}

	function lvd(){
		var ra=$("input[name='ra']:checked").val();
		var beizhu=$('#yuanyin').val();		
		var zone_id=0;
		var order_id='';
		var order_type='<%=order_type%>';
		if(ra==0){
			order_id='<%=order_id%>';
		}else{
			zone_id=<%=zone_id%>;
		}

	$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxAddWaitDoorAction.action',
			data:'zone_id=<%=zone_id%>&entry_id=<%=entry_id%>&order_id='+order_id+'&beizhu='+beizhu+'&order_type='+order_type,
			dataType:'json',
			type:'post',
			success:function(data){
				//alert(data.id);
				$('#wait_id').val(data.id);
                $('#myfrom').submit();
			}
		});	


	}
</script>
