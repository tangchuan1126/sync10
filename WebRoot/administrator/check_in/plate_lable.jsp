<%@page import="java.util.Date"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.util.DateUtil" %>

 
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 

<%
	long entry_id = StringUtil.getLong(request, "entry_id");
	String container_no = StringUtil.getString(request, "container_no");
 	int start = StringUtil.getInt(request, "start");
	int count = StringUtil.getInt(request, "count");
	String printName = StringUtil.getString(request, "print_name");
	String gateCheckInTime = StringUtil.getString(request, "gate_check_in_time");
    String relative_number = StringUtil.getString(request, "relative_number");
	boolean isprint = StringUtil.getInt(request, "isprint") == 1;
	//112062001 - 112062002
%>

<html>
<head>
 <!--  基本样式和javascript -->
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
<!--  基本样式和javascript -->
<%if(!isprint){ %>
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
<%} %>




<title>Check In</title>
</head>
<body>

	<div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<input type="button" class="short-short-button-print"  value="Print"  onclick="printGate()"/>
	</div>
	<br/>
	<div align="center">
		<%
			for(int index = 1   ;index <= count ; index++ ){
				String plateNo = entry_id + ""+StringUtil.fixContianerPlateNumber(index+start);
		%>
		<div id="avg" name="avg" style="width:368px; display:block; border:1px solid red;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial; font-size:12px;">
			    <tr>
			        <td width="34%" rowspan="2" align="center" valign="middle" style="border-bottom:2px solid black;border-right:2px solid black;">
			        	<span style="font-size:20px;"><b>Entry ID</b></span>
			        </td>
			        <td width="43%" height="39" align="center" style="border-bottom:2px solid black;" >
			        	<%=  DateUtil.NowStr() %>
			        &nbsp;</td>
			        <td width="23%" rowspan="7" style="border-left:2px solid black;" align="center" >
			        	<img alt="" src="check_in/imgs/shuma.jpg" id="showImg">
			        </td>
		        </tr>
			    <tr>
			      <td height="33" align="center" valign="middle" style="border-bottom:2px solid black;"><%=entry_id %>&nbsp;</td>
		        </tr>
			    <tr>
			      <td height="40" align="center" valign="middle" style="border-bottom:2px solid black;border-right:2px solid black;">
			      	<b>Container No</b>
			      </td>
			      <td align="center" valign="middle" style="border-bottom:2px solid black;"><%=container_no %>&nbsp;</td>
		        </tr>
                 <tr>
			      <td height="40" align="center" valign="middle" style="border-bottom:2px solid black;border-right:2px solid black;">
			      	<b>Gate Check In Time</b>
			      </td>
			      <td align="center" valign="middle" style="border-bottom:2px solid black;"><%= (gateCheckInTime != null && gateCheckInTime.length() >=19) ? gateCheckInTime.substring(0,19) : gateCheckInTime  %>&nbsp;</td>
		        </tr>
                 <tr>
			      <td height="40" align="center" valign="middle" style="border-bottom:2px solid black;border-right:2px solid black;">
			      	<b>Relative</b>
			      </td>
			      <td align="center" valign="middle" style="border-bottom:2px solid black;"><%= relative_number %>&nbsp;</td>
		        </tr>
			    <tr>
			      <td height="40" align="center" valign="middle" style="border-bottom:2px solid black;border-right:2px solid black;">
			      	<b>Plate</b>
			      </td>
			      <td align="center" valign="middle" style="border-bottom:2px solid black;"><%=plateNo %>&nbsp;</td>
		        </tr>
			    <tr>
			      <td height="260" colspan="2" align="center" valign="middle" style="border-bottom:2px solid black;">&nbsp;
						&nbsp;
			      </td>
                </tr>
			    <tr>
			    	<td colspan="2" valign="middle" height="70" align="center">
			    		<img class="bottom_barcode" src="/barbecue/barcode?data=<%=plateNo %>&width=1&height=40&type=Code39" /> 
			    	</td>
                    <td>&nbsp;</td>
			    </tr>	 		      		      		 			      			     
			</table>
		</div>
		<%} %>
	</div>
<!-- 	<div id="xuanzhuan" > -->
<!-- 		<div style="writing-mode:tb-rl;border:1px solid blue;height:185px; width:70px;"> -->
<!-- 			<img class="bottom_barcode" src="/barbecue/barcode?data=123123123&width=1&height=40&type=Code39" style="margin-top:50;margin-left:100  " />  -->
<!-- 		</div> -->
<!-- 	</div> -->
</body>
</html>
<script>
     function printGate(){
    	 	//$('#showImg').html('');
    	 	$('#showImg').css("display","none");
    	   //获取打印机名字列表
    		var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
    	   //判断是否有该名字的打印机
    		var printer = "LabelPrinter";
    		var printerExist = "false";
    		var containPrinter = printer;
    		for(var i = 0;i<printer_count;i++){
    			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
    				printerExist = "true";
    				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
    				break;
    			}
    		}
    	 
    		if(printerExist=="true"){
   			     var printHtml=$('div[name="avg"]');
   			     for(var i=0;i<printHtml.length;i++){
		    	    	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Label");
    	              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
    	              	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
    	  				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","99%",$(printHtml[i]).html());
    	  			
    	  				 visionariPrinter.ADD_PRINT_BARCODE("10mm","83mm","15mm", "100mm", "Code39", "<%=plateNo %>");
    	  				 visionariPrinter.SET_PRINT_STYLEA(0,"ShowBarText",0);
    	  				 visionariPrinter.SET_PRINT_STYLEA(0,"Angle","90");
    	  				$('#showImg').css("display","block");
    	  			     visionariPrinter.SET_PRINT_COPIES(1);
    	  				 //visionariPrinter.PREVIEW();
     	  			     visionariPrinter.PRINT(); 
    	  			
   			     }
     		}else{
    			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
    			if(op!=-1){ //判断是否点了取消
    				 var printHtml=$('div[name="avg"]');
    			     for(var i=0;i<printHtml.length;i++){
    			    	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Label");
    	              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
    	              	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
    	  				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","99%",$(printHtml[i]).html());
    	  			
    	  				 visionariPrinter.ADD_PRINT_BARCODE("10mm","83mm","15mm", "100mm", "Code39", "<%=plateNo %>");
    	  				 visionariPrinter.SET_PRINT_STYLEA(0,"ShowBarText",0);
    	  				 visionariPrinter.SET_PRINT_STYLEA(0,"Angle","90");
    	  				
    	  			     visionariPrinter.SET_PRINT_COPIES(1);
    	  				 //visionariPrinter.PREVIEW();
 	    	  			 visionariPrinter.PRINT();    
    	  				
    			     }
     			}	
    		}
     }
     
     function supportAndroidprint(){
     		//获取打印机名字列表
    	   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
    	   	 //判断是否有该名字的打印机
    	   	var printer = "<%=printName%>";
    	   	var printerExist = "false";
    	   	var containPrinter = printer;
    		for(var i = 0;i<printer_count;i++){
    			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
    				printerExist = "true";
    				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
    				break;
    			}
    		}
    		if(printerExist=="true"){
    			return androidIsPrint(containPrinter);
    		}else{
    			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
    			if(op!=-1){ //判断是否点了取消
    				return androidIsPrint(containPrinter);
    			}
    		}
    	}
     
     
     function androidIsPrint(containPrinter){
    	 var printHtml=$('div[name="avg"]');
    	 var flag = true ;
		     for(var i=0;i<printHtml.length;i++){
		    	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Label");
              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
              	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
  				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","99%",$(printHtml[i]).html());
  			
  				 visionariPrinter.ADD_PRINT_BARCODE("10mm","83mm","15mm", "100mm", "Code39", "<%=plateNo %>");
  				 visionariPrinter.SET_PRINT_STYLEA(0,"ShowBarText",0);
  				 visionariPrinter.SET_PRINT_STYLEA(0,"Angle","90");
  				
  			     visionariPrinter.SET_PRINT_COPIES(1);
  				 //visionariPrinter.PREVIEW();
 	  			 //visionariPrinter.PRINT(); 
	  			 flag = flag && visionariPrinter.PRINT();
		     }
		     return flag ;
     }
</script>
