<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.List"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.text.SimpleDateFormat"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<html>
<head>
<!--  基本样式和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<script src="../js/fullcalendar/colorpicker.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/css/colorpicker.css" rel="stylesheet" />
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
body {font-size:12px; font-family:Verdana, Geneva, sans-serif}
ul.myul{list-style-type:none;}
ul.myul li{float:left;width:300px;border:2px #dddddd solid;background:#eeeeee;line-height:25px;height:25px;margin-left: 5px;margin-bottom: 4px;}
span.title{font-weight: bold;}  <!--#f60-->
</style>
<%
	long id = StringUtil.getLong(request,"id");
	DBRow entry = checkInMgrZwb.findMainById(id);
	int entryInQty = checkInMgrZyj.findPlateCountByEntryIdIn(id);
	int entryOutQty = checkInMgrZyj.findPlateCountByEntryIdOut(id);
	DBRow[] rows = checkInMgrZyj.findPlateLevelProductInventaryByEntryHandle2(id);
%>
<script type="text/javascript">
</script>
<title>Check In Container Plates</title>
</head>
<body onLoad="onLoadInitZebraTable()">
	<div style="height:50px; border:0px solid red;overflow:auto">
	    <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:810px;">
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td height="25px;">
			    	<span class="title">EntryID : </span><%=id %>
			    </td>
			    <td align="left">			    	
			    	<span class="title">Entry In Qty : </span><%=entryInQty %>
			    </td>
			    <td align="left">			    	
			    	<span class="title">Entry Out Qty : </span><%=entryOutQty %>
			    </td>
			  </tr>
			</table>
	    </div>
    </div>
    <div id="tabs">
		<ul>
			<%for(int i = 0; i < rows.length; i ++){%>
			<%String number = moduleKey.getModuleName(rows[i].get("number_type", ""))+" : "+rows[i].getString("number"); %>
				<%LinkedHashMap<String, List<DBRow>> receiveStagPlates = (LinkedHashMap<String, List<DBRow>>)rows[i].get("staging_plates_in", new LinkedHashMap<String, List<DBRow>>()); %>
				<%LinkedHashMap<String, List<DBRow>> shippedStagPlates = (LinkedHashMap<String, List<DBRow>>)rows[i].get("staging_plates_out", new LinkedHashMap<String, List<DBRow>>()); %>
				<%if(receiveStagPlates.size() > 0 || shippedStagPlates.size() > 0){ %>
					<li><a href='<%="#"+rows[i].get("number_type", "")+"_"+rows[i].getString("number")%>'><span style="color: green;font-weight: bold;" title='<%=number%>'><%=number%></span></a></li>
				<%} %>
			<%} %>
		</ul>
		<%for(int i = 0; i < rows.length; i ++){%>
		<%String number = moduleKey.getModuleName(rows[i].get("number_type", ""))+" : "+rows[i].getString("number"); %>
			<div id='<%=rows[i].get("number_type", "")+"_"+rows[i].getString("number")%>'>
			<%LinkedHashMap<String, List<DBRow>> receiveStagPlates = (LinkedHashMap<String, List<DBRow>>)rows[i].get("staging_plates_in", new LinkedHashMap<String, List<DBRow>>()); %>
			<%LinkedHashMap<String, List<DBRow>> shippedStagPlates = (LinkedHashMap<String, List<DBRow>>)rows[i].get("staging_plates_out", new LinkedHashMap<String, List<DBRow>>()); %>
			<%if(receiveStagPlates.size() > 0 || shippedStagPlates.size() > 0){ %>
				<table style="width: 800px;border:2px #dddddd solid;background:#eeeeee;margin-left: 5px;">
					<tr height="35px">
						<td width="28%">
							<span class="title">PONo:</span><%=rows[i].getString("po_no") %>
						</td>
						<td width="25%">
							<span class="title">CustomerID:</span><%=rows[i].getString("customer_id") %>
						</td>
						<td width="47%">
							<span class="title">Appointment Time:</span>
							<%=!StrUtil.isBlank(rows[i].getString("appointment_time"))?DateUtil.FormatDatetime("yyyy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("appointment_time"))):"" %>
						</td>
					</tr>
					<tr height="35px">
						<td width="28%">
							<span class="title">Expect Box Qty:</span><%=rows[i].getString("qty") %>
						</td>
						<td width="25%">
							<span class="title">Receive Qty:</span>
							<%=checkInMgrZyj.findPlateLevelProductInventaryByEntryDetailIn(rows[i].get("dlo_detail_id", 0L)).length %>
						</td>
						<td width="47%">
							<span class="title">Shipped Qty:</span>
							<%=checkInMgrZyj.findPlateLevelProductInventaryByEntryDetailOut(rows[i].get("dlo_detail_id", 0L)).length %>
						</td>
					</tr>
				</table>
				<div style="height:360px;overflow: auto;margin-top: 5px;">
				
				<%if(receiveStagPlates.size() > 0){ %>
				<%Set<String> receiveStagins = receiveStagPlates.keySet();%>
				<%for (Iterator iteratorR = receiveStagins.iterator(); iteratorR.hasNext();) { %>
				<%String stagingId = (String) iteratorR.next(); %>
				<fieldset style="border:1.5px #dddddd dashed;padding:7px;width:90%;word-break:break-all;margin-top:10px;margin-bottom:10px;margin-left:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px;">
					<legend><span style="font-weight: bold;">StagingArea:</span><span style="font-size: 18px; color: blue;">
						<%=null==storageDoorLocationMgrZYZ.getDetailLoadUnloadLocation(Long.parseLong(stagingId))?"":storageDoorLocationMgrZYZ.getDetailLoadUnloadLocation(Long.parseLong(stagingId)).getString("location_name")%>
					</span></legend>
					<%List<DBRow> plates = (List<DBRow>)receiveStagPlates.get(stagingId); %>
					<%if(plates != null && plates.size() > 0){ %>
						<table style="width:95%;" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
						<%for(DBRow plate : plates) {%>
							<tr style="width:90%;height: 25px;">
								<td style="width: 20%;text-align: left;"><%=plate.getString("plate_no") %></td>
								<td style="width: 20%;text-align: left;"><%=plate.get("box_count", 0F) %></td>
								<td style="width: 30%;text-align: left;"><%=!StrUtil.isBlank(plate.getString("shipped_time"))?DateUtil.FormatDatetime("yyyy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(plate.getString("shipped_time"))):"&nbsp;" %></td>
								<td style="width: 30%;text-align: left;"><%=0==plate.get("entry_out", 0L)?"&nbsp;":"E"+plate.get("entry_out", 0L)+"&nbsp;&nbsp;&nbsp;<span style='color:red'>OUT</span>"%></td>
							</tr>
						<%} %>	
						</table>
					<%} %>
				</fieldset>
				<%} %>
				<%} %>
				
				<%if(shippedStagPlates.size() > 0){ %>
				<%Set<String> shippedStagins = shippedStagPlates.keySet();%>
				<%for (Iterator iteratorS = shippedStagins.iterator(); iteratorS.hasNext();) { %>
				<%String stagingId = (String) iteratorS.next(); %>
				<fieldset style="border:1.5px #dddddd dashed;padding:7px;width:90%;word-break:break-all;margin-top:10px;margin-bottom:10px;margin-left:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px;">
					<legend><span style="font-weight: bold;">StagingArea:</span><span style="font-size: 18px; color: blue;">
						<%=null==storageDoorLocationMgrZYZ.getDetailLoadUnloadLocation(Long.parseLong(stagingId))?"":storageDoorLocationMgrZYZ.getDetailLoadUnloadLocation(Long.parseLong(stagingId)).getString("location_name")%>
					</span></legend>
					<%List<DBRow> plates = (List<DBRow>)shippedStagPlates.get(stagingId); %>
					<%if(plates != null && plates.size() > 0){ %>
						<table style="width:95%;" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
						<%for(DBRow plate : plates) {%>
							<tr style="width:90%;height: 25px;">
								<td style="width: 20%;text-align: left;"><%=plate.getString("plate_no") %></td>
								<td style="width: 20%;text-align: left;"><%=plate.get("box_count", 0F) %></td>
								<td style="width: 30%;text-align: left;"><%=!StrUtil.isBlank(plate.getString("shipped_time"))?DateUtil.FormatDatetime("yyyy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(plate.getString("shipped_time"))):"&nbsp;" %></td>
								<td style="width: 30%;text-align: left;"><%=0==plate.get("entry_out", 0L)?"&nbsp;":"E"+plate.get("entry_out", 0L)+"&nbsp;&nbsp;&nbsp;<span style='color:red'>OUT</span>"%></td>
							</tr>
						<%} %>	
						</table>
					<%} %>
				</fieldset>
				<%} %>
				<%} %>
				</div>
			<%} %>
			</div>
		<%} %>
	</div>
    
<!--     	<div style="text-align:center;height:60px;background:#eeeeee;border:1px solid silver;width: 823px;text-align: center;padding-top:19px;">No Data</div> -->
   
</body>
<script type="text/javascript">
$("#tabs").tabs({
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
});
</script>
</html>