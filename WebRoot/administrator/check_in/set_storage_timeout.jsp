<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
	long ps_id =StringUtil.getLong(request, "ps_id");
	String title =StringUtil.getString(request, "title");
	DBRow[] datas =googleMapsMgrCc.getStorageTimeOutBypsId(ps_id);
	
%>
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">

	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>

	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>


	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
	
	<!-- stateBox 信息提示框 -->
	<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
	.div_1{
		width: 400px;
		height: aotu;
		margin: 0 30px;
	}
	.div_2{
		width: 96%;
		height:35px;
		line-height:35px;
		margin: 5px 5px;
	}
	.div_3{
		width: 60%;
		height: 34px;
		margin:3px ;
		float: left;
	}
	.input{
		width: 80%;
		height: 34px;
		font-size: 14px;
	}
	.div_4{
		width: 35%;
		height: 34px;
		margin:3px ;
		float: left;
		background: #444;
		color: white;
		text-align: center;
		font-size: 14px;
	}
</style>
<style type="text/css">
	.ui-datepicker{font-size: 0.9em;}
</style>
<script type="text/javascript">
$(function(){
	//添加button
		var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
		api.button([
				{
					name: 'Submit',
					callback: function () {
						commitData();
						return false;
					},
					focus:true,
					focus:"default"
				},
				{
					name: 'Cancel',
					callback:function (){
						closeWindow();
						return false;
					},
				}] 
			);
  });
  
  function closeWindow(){
	  $.artDialog.close();
  }
  
  function commitData(){
	  
	 var timeout_one = $("#timeout_one").val();
	 var timeout_two = $("#timeout_two").val();
	 var timeout_three = $("#timeout_three").val();
	   
	 if(timeout_one * 1 <= 0  ){
		 showMessage("Data Error","error");
		 return ;
	 }
	 if(timeout_two * 1 <= 0){
		 showMessage("Data Error","error");
		 return ;
	 }
	 if(timeout_three * 1 <= 0){
		 showMessage("Data Error","error");
		 return ;
	 }
	  $.ajax({
			url:<%=ConfigBean.getStringValue("systenFolder")%>+'action/administrator/gis/storageTimeOut.action',
			data:$("#timeout").serialize(),
			dataType:'json',
			type:'post',
			async:true, 
			beforeSend:function(request){
		    },
			success:function(data){
		    	if(data&&data.id!=0){
		    		showMessage("Successful operation!","alert");
		    		closeWindow();
		    	}
			},
			error:function(){
				showMessage("System Error!","error");
			}
		});
	  
  }
  
  function verify(el){
	    var value =el.value;
  		var vMsg="Data format error";
  		var reg = /^-?\d+(\.\d+)?$/; 
  		var bl =reg.test(value);
  	  	if(bl && value*1 > 0){
  	  	$(el).nextAll("#con").show();
  	  $(el).nextAll("#del").hide();
  	  	}else{
  	  	$(el).nextAll("#del").attr("titile",vMsg);
  	 	$(el).nextAll("#con").hide();
  	  	$(el).nextAll("#del").show();	
  	  	}
  }
</script>
</head>
<body >
	<form id="timeout">
	
	<div style="width: 100%;height: 30px;line-height: 30px;font-size:16px; text-align: center;background: #E5E5E5; color: black;">&nbsp&nbspStorage&nbsp&nbsp:&nbsp&nbsp<%=title%> &nbsp&nbsp<span style="font-size: 9px;"> unit(min)</span> </div>
	 <input type="hidden" id ="ps_id" name="ps_id" value="<%=ps_id %>">
			 <%
			 String flag ="";
		   		if(datas.length>0){
			   	DBRow row =datas[0];
			   	flag="update";
			 %>
 		 <div class="div_1">
			  <input type="hidden" id ="flag" name="flag" value="<%=flag%>">
			  <div class="div_2">
			   		<div  class="div_4">Report One :</div>
			   		<div class="div_3">
			    	 <input type="text" class="input" id ="timeout_one" name="timeout_one" value="<%=row.getString("timeout_one") %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="this.value=this.value.replace(/\D/g,'')">
			    	  <img id="con" src="../imgs/maps/confirm.jpg" style="display: none">
		              <img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
			 	    </div>
			  </div>
			  <div class="div_2">
			   		<div class="div_4">Report Two :</div>
				    <div class="div_3">
				    	<input type="text"  class="input" id ="timeout_two" name="timeout_two" value="<%=row.getString("timeout_two") %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="this.value=this.value.replace(/\D/g,'')">
				 	 	<img id="con" src="../imgs/maps/confirm.jpg" style="display: none">
						<img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
				 	</div>
			  </div>
			  <div class="div_2">
				   	<div class="div_4"> Report Three :</div>
				    <div class="div_3">
				     <input type="text" class="input" id ="timeout_three" name="timeout_three" value="<%=row.getString("timeout_three") %>"onchange="this.value=this.value.trim(),verify(this)"  onkeyup="this.value=this.value.replace(/\D/g,'')">
				  	 <img id="con" src="../imgs/maps/confirm.jpg" style="display: none">
					 <img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
				  	</div>
			  </div>
		 <div>
			  <% 
			  }else {
				 flag="add";
			  %>
			  
			  <input type="hidden" id ="flag" name="flag" value="<%=flag%>">
			  <div class="div_1">
				  <div class="div_2">
				       <div class="div_4">Report One :</div>
				       <div class="div_3">
			    		<input type="text" class="input" id ="timeout_one" name="timeout_one" value="" onfocus="this.value=''" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="this.value=this.value.replace(/\D/g,'')">
			  		   </div>
			  		    <img id="con" src="../imgs/maps/confirm.jpg" style="display: none">
					    <img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
				  </div>
				  <div class="div_2">
				       <div class="div_4">Report Two :</div>
				       <div class="div_3">
			   			<input type="text"  class="input" id ="timeout_two" name="timeout_two"  value="" onfocus="this.value=''" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="this.value=this.value.replace(/\D/g,'')">
			   			 <img id="con" src="../imgs/maps/confirm.jpg" style="display: none">
					     <img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
			  		   </div>
				  </div>
				  <div class="div_2">
					   <div class="div_4">Report Three :</div>
					   <div class="div_3">
				    	 	<input type="text" class="input" id ="timeout_three" name="timeout_three" value="" onfocus="this.value=''" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="this.value=this.value.replace(/\D/g,'')">
 							<img id="con" src="../imgs/maps/confirm.jpg" style="display: none">
							<img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">				    	 	
				 	   </div>
				  </div>
			 <div>
			  
			  
			
			  <%
			  }
			 %>
		 <div>
	</form>
</body>
