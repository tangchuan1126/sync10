<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.ModuleKey"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/>
<%@page import="com.fr.base.core.json.JSONArray"%>
<%@page import="com.fr.base.core.json.JSONObject"%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<%
	String numberStr=StringUtil.getString(request, "number");
	String[] numbers=numberStr.split(",");
	int id = StringUtil.getInt(request, "id");
	String datas = StringUtil.getString(request, "datas");
 	 
	JSONObject jsonObj =   StringUtil.getJsonObject(datas);

	
%>
<title>Select</title>
<script type="text/javascript">
function selectLoad(CompanyID, number, CustomerID, AccountID,PONo, OrderNo,number_type,StagingAreaID,title,freightTerm){
	 $.artDialog.opener.setParentNumber  && $.artDialog.opener.setParentNumber(CompanyID, number, CustomerID, AccountID, PONo, OrderNo,<%=id%>,number_type,StagingAreaID,title,freightTerm);
	 $.artDialog && $.artDialog.close();

}
function changeCursor(obj){
	 $(obj).css("cursor","Pointer"); 
}

</script>
</head>
<body>
	
	<div id="tabs">
	<ul>
		<li><a href="#av1">Search : <%=numberStr %></a></li>
		<%if(jsonObj!=null && StringUtil.getJsonArrayFromJson(jsonObj,"loads") != null && StringUtil.getJsonArrayFromJson(jsonObj,"loads").length() > 0 ){%>
		<li><a href="#av2">You may search LOAD#</a></li>		 
		<%} %>
	</ul>
	<div id="av1">
			<div style="height:430px; overflow:auto;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5; height:35px; font-weight:bold;" align="center">Search No</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >Type</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >CompanyID</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >CustomerID</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >ORDER</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >PO</td>
				  </tr>
				  <%for(int i=0;i<numbers.length;i++){ %>
				  <tr style="background-color:#f9f9f9" name="seData" id="<%=i%>">
				  	<td style="border-bottom:2px solid #BBB;border-right:1px solid #BBB;" align="center">
				  		<span style="color:blue" valign="middle" id="number<%=i%>"><%=numbers[i]%></span>
				  	</td>
				  	<td colspan="6" style="border-bottom:1px solid #BBB;border-left:1px solid #BBB;">
				  		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
				  			<%if(jsonObj != null){ %>
							<%JSONArray array =  StringUtil.getJsonArrayFromJson(jsonObj,"orders");%>
							<%if(array != null && array.length() > 0 ){%>
							<%for(int a=0;a<array.length();a++){%>
							<%//for(int a=0;a<rows.length;a++){ %>
							<%JSONObject jsonItem = StringUtil.getJsonObjectFromArray(array,a); %>
							<tr height="35px" onmouseover="changeColor(this)" id="<%=a%>" num="<%=i%>" onmouseout="changeColorB(this)" onclick="choice(this)" >
								<td align="center" width="90px;">
									<input type="hidden" id="number_type<%=a%>" value="<%=StringUtil.getJsonString(jsonItem,"order_type")%>"/>
									<%//=moduleKey.getModuleName(rows[a].get("order_type",0)) %>
									<%if(jsonItem != null){ %>
						    				<span id="title<%=a%>">
						    				<%=moduleKey.getModuleName(StringUtil.getJsonString(jsonItem,"order_type"))%>
						    				</span>
						    		<%}%>
								</td>
								<td align="center" width="90px;">
									<%if(jsonItem != null){ %>
						    				<span id="company<%=a%>">
						    				<%=StringUtil.getJsonString(jsonItem,"companyid")%>
						    				</span>
						    		<%}%>
								</td>
								<td align="center" width="90px;">
									<%if(jsonItem != null){ %>
						    				<span id="customer<%=a%>">
						    				<%=StringUtil.getJsonString(jsonItem,"customerid")%>
						    				</span>
						    		<%}%>
								</td>
								<td align="center" width="90px;">
									<%if(jsonItem != null){ %>
						    				<span  id="order<%=a%>">
						    				<%=StringUtil.getJsonString(jsonItem,"orderno")%>
						    				</span>
						    		<%}%>
								</td>
								<td align="center" width="90px;">
									<%if(jsonItem != null){ %>
						    				<span id="po<%=a%>">
						    				<%=StringUtil.getJsonString(jsonItem,"pono")%>
						    				</span>
						    		<%}%>
							
									<span style="display:none" id="accountId<%=a%>"><%=StringUtil.getJsonString(jsonItem,"accountid")%></span>
									<span style="display:none" id="staging<%=a%>"><%=StringUtil.getJsonString(jsonItem,"stagingareaid")%></span>
									<span style="display:none" id="freightTerm<%=a%>"><%=StringUtil.getJsonString(jsonItem,"freightterm")%></span>
								</td>
								
							</tr>
						 <%		} 
							
								}
							}
						 %>	
						</table>
				  	</td>
				  </tr>
				  <%
				  	} 
				  %>
				</table>
				</div>	
	</div>
	<div id="av2">
               <div style="height:430px; overflow:auto;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5; height:35px; font-weight:bold;" align="center">Search No</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >Type</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >CompanyID</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="100px" >CustomerID</td>
				    <td style="border:1px solid #BBB; background-color:#E5E5E5;font-weight:bold;" align="center" width="200px" >LOAD</td>
				  </tr>
				  <%for(int i=0;i<numbers.length;i++){ %>
				  <tr style="background-color:#f9f9f9" name="seData" id="<%=i%>">
				  	<td style="border-bottom:2px solid #BBB;border-right:1px solid #BBB;" align="center">
				  		<span style="color:blue" valign="middle" id="number<%=i%>_load"><%=numbers[i]%></span>
				  	</td>
				  	<td colspan="6" style="border-bottom:1px solid #BBB;border-left:1px solid #BBB;">
				  		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
				  			<%if(jsonObj != null){ %>
							<%JSONArray array =  StringUtil.getJsonArrayFromJson(jsonObj,"loads");%>
							<%if(array != null && array.length() > 0 ){%>
							<%for(int a=0;a<array.length();a++){%>
							<%//for(int a=0;a<rows.length;a++){ %>
							<%JSONObject jsonItem = StringUtil.getJsonObjectFromArray(array,a); %>
							<tr height="35px" onmouseover="changeColor(this)" id="<%=a%>" num="<%=i%>" onmouseout="changeColorB(this)" onclick="choice_load(this)" >
								<td align="center" width="90px;">
									<input type="hidden" id="number_type<%=a%>_load" value="<%=StringUtil.getJsonString(jsonItem,"order_type")%>"/>
									<%//=moduleKey.getModuleName(rows[a].get("order_type",0)) %>
									<%if(jsonItem != null){ %>
						    				<span id="title<%=a%>_load">
						    				<%=moduleKey.getModuleName(StringUtil.getJsonString(jsonItem,"order_type"))%>
						    				</span>
						    		<%}%>
								</td>
								<td align="center" width="90px;">
									<%if(jsonItem != null){ %>
						    				<span id="company<%=a%>_load">
						    				<%=StringUtil.getJsonString(jsonItem,"companyid")%>
						    				</span>
						    		<%}%>
								</td>
								<td align="center" width="90px;">
									<%if(jsonItem != null){ %>
						    				<span id="customer<%=a%>_load">
						    				<%=StringUtil.getJsonString(jsonItem,"customerid")%>
						    				</span>
						    		<%}%>
								</td>
								<td align="center" width="200px;">
									<span style="display:none" id="staging<%=a%>_load"><%=StringUtil.getJsonString(jsonItem,"stagingareaid")%></span>
									<span style="display:none" id="accountId<%=a%>_load"><%=StringUtil.getJsonString(jsonItem,"accountid")%></span>
									<span style="display:none" id="freightTerm<%=a%>_load"><%=StringUtil.getJsonString(jsonItem,"freightterm")%></span>
									<%if(jsonItem != null){ %>
						    				<span  id="load<%=a%>_load">
						    				<%=StringUtil.getJsonString(jsonItem,"number")%>
						    				</span>
						    		<%}%>
						    		
									
								</td>
							</tr>
						 <%		} 
							
								}
							}
						 %>	
						</table>
				  	</td>
				  </tr>
				  <%
				  	} 
				  %>
				</table>
				</div>	
    </div>
</div>
<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});

 $("#tabs").tabs("select",0);
</script>
	
</body>
</html>
<script>
	function changeColor(tr){
		$(tr).css("background-color","#E6F3C5");
	}
	function changeColorB(tr){
		$(tr).css("background-color","#f9f9f9");
	}
	function choice(tr){
		var id=$(tr).attr('id');
		var num=$(tr).attr('num');
		var number=$('#number'+num).html();
	    var number_type=$('#number_type'+id).val().trim();
		var CompanyID=$('#company'+id).html().trim();
		var CustomerID=$('#customer'+id).html().trim();
		var PONo=$('#po'+id).html().trim();
		var OrderNo=$('#order'+id).html().trim();
		var AccountID=$('#accountId'+id).html().trim();
		var StagingAreaID=$('#staging'+id).html().trim();
		var title=$('#title'+id).html().trim();
		var freightTerm=$('#freightTerm'+id).html().trim();
		selectLoad(CompanyID, number, CustomerID, AccountID,PONo, OrderNo,number_type,StagingAreaID,title,freightTerm)
	}
	function choice_load(tr){
		var id=$(tr).attr('id');
		var num=$(tr).attr('num');
		var number=$('#load'+id+'_load').html().trim();
	    var number_type=$('#number_type'+id+'_load').val().trim();
		var CompanyID=$('#company'+id+'_load').html().trim();
		var CustomerID=$('#customer'+id+'_load').html().trim();
		//var load=$('#load'+id+'_load').html().trim();
		var AccountID=$('#accountId'+id+'_load').html().trim();
		var StagingAreaID=$('#staging'+id+'_load').html().trim();
		var title=$('#title'+id+'_load').html().trim();
		var freightTerm=$('#freightTerm'+id+'_load').html().trim();
		var PONo='';
		var OrderNo='';
		selectLoad(CompanyID, number, CustomerID, AccountID,PONo, OrderNo,number_type,StagingAreaID,title,freightTerm)
	}
</script>
