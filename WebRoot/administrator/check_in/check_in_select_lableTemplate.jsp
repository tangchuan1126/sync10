<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey" />
<%@ include file="../../include.jsp"%>
<script type="text/javascript">
var systenFolder = '<%=ConfigBean.getStringValue("systenFolder") %>';
</script>

<%
	
	String entry_id = StringUtil.getString(request, "entryId");
	String out_seal = StringUtil.getString(request, "out_seal");
	String container_no=StringUtil.getString(request, "container_no");
	String jsonString = StringUtil.getString(request, "jsonString");
	JSONArray jsons = new JSONArray(jsonString);
	
	boolean vizioFlag = true;
	String hidLoader ="";
	String hidFreight ="";
	if(jsons!=null&&jsons.length()>0){
 		JSONObject json=jsons.getJSONObject(0);
 		String loadNo=json.getString("number");
 		DBRow detailRow = printLabelMgrGql.findLabelTempDetail(entry_id, loadNo);
 		if(detailRow!=null){
 			hidLoader = detailRow.getString("trailer_loader");
 			hidFreight = detailRow.getString("freight_counted");
 		}
	}
%>

<html>
<head>
<title>selectLableTemplate</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<style type="text/css">
.lableTemplate {
	margin-top: 2px;
	height: 424px;
	border: 2px #dddddd solid;
	background: #eeeeee;
	padding: 5px;
	-webkit-border-radius: 7px;
	-moz-border-radius: 7px;
	overflow-y: auto;
}

.td_line {
	border-bottom: 1px solid #dddddd;
	font-family: Verdana;
}
</style>
<script type="text/javascript">
  
  	function changeAll(){
  		var $lables = $("input[name='lable']");
  		if($("#changeAll").attr("checked")){
  			$lables.each(function(){
  				$(this).attr("checked",true);
  			});
  		}else{
  			$lables.each(function(){
  				$(this).attr("checked",false);
  			});
  		}
  	}
  	function changeAllBtn(){
  		$("#changeAll").attr("checked",false);
  	}
  	
  	 //记录打印的标签，转pdf用
  	function printedLabel(number, entry_id, master_bol_no, url,type){
  			$.ajax({
  			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxAddPrintedlabelAction.action',
  			data:'number='+number+'&master_bol_no='+master_bol_no+'&url='+url+'&type='+type,
  			dataType:'json',
  			type:'post',
  			success:function(data){
  			}
  		});	
  	}
  	
  	 //联合打印
  	function print_list(){
  		var ses=$("input[name='lable']:checked");
  		if(ses.length==0){
  			alert("Please select a label");
  			return false;
  		}
  		for(var i=0;i<ses.length;i++){
  			var path=$(ses[i]).val();
  			var number=$(ses[i]).attr('num');
  			var doorName=$(ses[i]).attr('doorName');
  			var companyId=$(ses[i]).attr('companyId');
  			var customerId=$(ses[i]).attr('customerId');
  			var master_bol_nos = $(ses[i]).attr('master_bol_nos');
  				//$("#load_"+number).attr("master_bol_nos");
  			var order_nos = $(ses[i]).attr('order_nos');
  				//$("#load_"+number).attr("order_nos");
  				
  			editLabelTempDetail(number);	
  			
  			var str = [];
  			var json = {
  				checkDataType:'LOAD',
  				number:number,
  				door_name:doorName,
  				companyId:companyId,
  				customerId:customerId,
  				master_bol_nos:master_bol_nos,
  				order_nos:order_nos,
  				checkLen:1
  			};
  			str.push(json);
  			var json = JSON.stringify(str);
  			var url_path='<%=ConfigBean.getStringValue("systenFolder")%>'+path+'?jsonString='+json+'&entry_id=<%=entry_id%>'+'&isprint=1&out_seal=<%=out_seal%>&container_no=<%=container_no%>';
  			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>'+path,
				dataType:'html',
				async:false,
				data:'jsonString='+json+'&entry_id=<%=entry_id%>'+'&isprint=1&out_seal=<%=out_seal%>',
				success:function(html){
					$('#printListMoBan').html(html);
// 					$('#url_path').val(url_path);
					print();
				}
			});
  		}

  	}
  	//修改loader和freight的值，只有customerId不是VIZO和VZB的签用
  	function editLabelTempDetail(number)
  	{
  		var vizioFlag='<%=vizioFlag%>';
  		if(vizioFlag=="true"){
  			var loader = $("input[name='loader']:checked").val();
  	  		var freight = $("input[name='freight']:checked").val();
  	  		var hidLoader = $("#hidLoader").val();
  	  		var hidFreight = $("#hidFreight").val();
  	  		if(!loader){
  	  			loader="";
  	  		}
  	  		if(!freight){
  	  			freight="";
  	  		}
//  	   		console.log("loader="+loader+"  hidLoader="+hidLoader+"  freight="+freight+"  hidFreight="+hidFreight);
  	  		if(loader!=hidLoader||freight!=hidFreight){
  	  			$.ajax({
  	  	  			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/editLabelTempDetailAction.action',
  	  	  			data:'entry_id=<%=entry_id%>&load_no='+number+'&trailer_loader='+loader+'&freight_counted='+freight,
  	  	  			dataType:'json',
  	  	  			type:'post',
  	  	  			success:function(data){
  	  	  			}
  	  	  		});	
  	  		}
  		}
  	}
  	 
  	 
  	function printSingle(evt,number,checkDataType,companyId,customerId,doorName,master_bol_nos,order_nos,number_type){
  		editLabelTempDetail(number);
  		var path = $(evt).attr("test");
  		var str = [];
		var json = {
			checkDataType:checkDataType,
			number:number,
			door_name:doorName,
			companyId:companyId,
			customerId:customerId,
			master_bol_nos:master_bol_nos,
			order_nos:order_nos,
			checkLen:1
		};
		if(typeof(number_type)!="undefined"){
			json["number_type"] = number_type
		}
		str.push(json);
		var json = JSON.stringify(str);
  		if(path==""){
  			alert("Not Found！");
  		}else{
			var url = '<%=ConfigBean.getStringValue("systenFolder")%>'+path+'?jsonString='+json+'&entry_id=<%=entry_id%>&out_seal=<%=out_seal%>&container_no=<%=container_no%>';
			$.artDialog.open(url, {title: "Webcam List",width:'900px',height:'600px', lock: false,opacity: 0.3,fixed: true});	
  		}
  	}
  	
  	function printSingleOrder(evt,number,checkDataType,companyId,customerId,doorName,master_bol_nos,order_no,number_type){
  		editLabelTempDetail(number);
  		var path = $(evt).attr("test");
  		var str = [];
		var json = {
			checkDataType:checkDataType,
			number:number,
			door_name:doorName,
			companyId:companyId,
			customerId:customerId,
			master_bol_nos:master_bol_nos,
			order_no:order_no,
			number_type:number_type,
			checkLen:1
		};
		str.push(json);
		var json = JSON.stringify(str);
  		if(path==""){
  			alert("Not Found！");
  		}else{
			var url = '<%=ConfigBean.getStringValue("systenFolder")%>'+path+'?jsonString='+json+'&entry_id=<%=entry_id%>&out_seal=<%=out_seal%>&container_no=<%=container_no%>';
			$.artDialog.open(url, {title: "Webcam List",width:'900px',height:'600px', lock: false,opacity: 0.3,fixed: true});	
  		}
  	}
  	
</script>
</head>

<body>
	<div
		style="border: 2px #dddddd solid; background: #eeeeee; padding: 5px; -webkit-border-radius: 7px; -moz-border-radius: 7px;"
		align="right">
		<input class="long-long-button" type="button" value="Combined Print"
			onclick="print_list()" />
	</div>

	<div id="loaderFreight"
		style="border: 2px #dddddd solid; background: #eeeeee; padding: 5px; margin-top: 5px; display: none;">
		<input type="hidden" id="hidLoader" name="hidLoader"
			value="<%=hidLoader%>"> <input type="hidden" id="hidFreight"
			name="hidFreight" value="<%=hidFreight%>">
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			style="font-family: Arial; font-size: 13px; font-weight: bold;">
			<tr>
				<td width="35%"><span style="font-size: 15px;">Trailer
						Loader:&nbsp;&nbsp;</span> <input type="radio" id="loader1" name="loader"
					value="1" <%="1".equals(hidLoader)?"checked='checked'":"" %> />By
					Shipper&nbsp; <input type="radio" id="loader2" name="loader"
					value="2" <%="2".equals(hidLoader)?"checked='checked'":"" %> />By
					Driver&nbsp;</td>
				<td width="65%"
					style="border-left: 2px solid #dddddd; padding-left: 10px;"><span
					style="font-size: 15px;">Freight Counted:&nbsp;&nbsp;</span> <input
					type="radio" id="freight1" name="freight" value="1"
					<%="1".equals(hidFreight)?"checked='checked'":"" %> />By
					Shipper&nbsp; <input type="radio" id="freight2" name="freight"
					value="2" <%="2".equals(hidFreight)?"checked='checked'":"" %> />By
					Driver/pallets said to contain&nbsp; <input type="radio"
					id="freight3" name="freight" value="3"
					<%="3".equals(hidFreight)?"checked='checked'":"" %> />By
					Driver/Pieces&nbsp;</td>
			</tr>
		</table>
	</div>

	<div class="lableTemplate">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="30%" class="td_line">&nbsp;</td>
				<td
					style="border-bottom: 1px solid #dddddd; font-family: Verdana; border-left: 1px solid #dddddd">
					<input onclick="changeAll()" type="checkbox" id="changeAll" />
				</td>
				<td class="td_line">&nbsp;</td>
				<td class="td_line">&nbsp;</td>
			</tr>
		</table>
		<%  for(int i=0;i<jsons.length();i++){%>
		<%  JSONObject json=jsons.getJSONObject(i);
			        //String load_number='';
			        boolean canPrint = true;
			        String shipToName="";
					String number=json.getString("number");
			    	String checkDataType=json.getString("checkDataType");
			    	String companyId=json.getString("companyId");
			    	String customerId=json.getString("customerId");
			    	String doorName=json.getString("door_name");
			    	int number_type=json.getInt("number_type");
			    	String order_no=json.getString("order");
			    	if((checkDataType.toUpperCase()).equals("CTNR")){
			    		continue;
			    	}
			    	
				if((checkDataType.toUpperCase()).equals("PICKUP")){ 
					if(number_type==ModuleKey.CHECK_IN_LOAD){
			%>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			style="background-color: #FFF;">
			<% 
	    			DBRow[] rows = sqlServerMgr.findBillOfLadingTemplateByLoad(number, companyId, "",0,request);//customerId
	    	 	    rows = checkInMgrZwb.findLableByName(rows,number_type);
	    	 	   for(int a=0;a<rows.length;a++){ 
 					  companyId=rows[a].getString("CompanyID");
 					  shipToName =rows[a].getString("ShipToName");
 					  //判断ShipToName包含制定缩写就不可打印
 					  if(shipToName.toUpperCase().indexOf("KOHL") != -1){
 					  	canPrint = false;
 					  }
				      customerId=rows[a].getString("CustomerID");
				      String master_bol_nos = rows[a].getString("master_bol_nos");
				      int master_bol_nos_len = rows[a].get("master_bol_nos_len", 0);
				      String order_nos = rows[a].getString("order_nos");
				      int order_no_len = rows[a].get("order_no_len", 0);
				      
				      
				      //判断是不是显示Trailer Loader 和 Freight Counted的勾选框
				      if(customerId.equals("VIZIO") || customerId.equals("VZB") 
				 				||customerId.equals("VIZIO3") ||customerId.equals("VZO") ||customerId.equals("VIZIO2")){
				    	  vizioFlag=false;
				 		}
    	 	 %>
			<input type="hidden" id="load_<%=number %>"
				master_bol_nos="<%=master_bol_nos%>" order_nos="<%=order_nos%>">
			<tr>
				<td width="30%" align="center" valign="middle"
					style="font-size: 18px; font-weight: bold; border-bottom: 1px solid black;">
					<div align="left" style="padding-left: 20px; font-size: 22px;"><%=rows[a].get("customerId", "") %></div>
					<div align="left"
						style="font-size: 12px; color: #666; padding-left: 20px;">LOAD:</div>
					<div align="left" style="padding-left: 20px;"><%=number %></div>
					<div align="left"
						style="font-size: 12px; color: #666; padding-left: 20px;">
						<%if(master_bol_nos_len > 0){ %>
						MasterBolNo:
						<%}else{ %>
						OrderNo:
						<%} %>
					</div>
					<div align="left" style="padding-left: 20px;">
						<%if(master_bol_nos_len > 0){ %>
						<%=master_bol_nos %>
						<%}else{ %>
						<%=order_nos %>
						<%} %>
					</div>
				</td>
				<td
					style="border-left: 1px solid #dddddd; border-bottom: 1px solid black;">
					<%DBRow masterBol =(DBRow) rows[a].get("MasterBOLFormatBAK",new DBRow());
				    	       // System.out.println("路径:"+masterBol.get("template_path", ""));
								if(!masterBol.get("template_path", "").equals("")){
						%>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" valign="middle" width="50px;"
								style="border-bottom: 1px solid #dddddd"><input
								type="checkbox" value="<%=masterBol.get("template_path", "") %>"
								num="<%=number %>" name="lable" companyId="<%=companyId %>"
								customerId="<%=customerId %>" doorName="<%=doorName %>"
								onclick="changeAllBtn()" master_bol_nos="<%=master_bol_nos %>"
								order_nos="<%=order_nos %>" /></td>
							<td width="400px;" style="border-bottom: 1px solid #dddddd">
								<div style="font-size: 30px; font-weight: bold;">
									<a href="javascript:void(0)"
										style="text-decoration: none; outline: none"
										<%
										if(!canPrint){
										%>
										onclick="alert('This ship account can not print BOL!')"
										<%}
										else{ 
										%>
										onclick="printSingle(this,'<%=number %>','<%=checkDataType %>','<%=companyId %>','<%= customerId%>','<%=doorName%>','<%=master_bol_nos %>','<%=order_nos %>',<%=number_type%>)"
										<% 
										}
										%>
										test="<%=masterBol.get("template_path", "") %>">MasterBOL</a>
								</div>
								<div style="font-size: 12px; color: #666"><%=rows[a].get("MasterBOLFormat", "error") %></div>
							</td>
							<td
								style="border-left: 1px solid #dddddd; border-bottom: 1px solid #dddddd"
								align="left">
								<table>
									<tr>
										<td
											style="font-size: 12px; border-bottom: 1px solid #dddddd; color: #666">page:<%=masterBol.get("paper", "") %></td>
									</tr>
									<tr>
										<td
											style="font-size: 12px; border-bottom: 1px solid #dddddd; color: #666">width:<%=masterBol.get("print_range_width", "") %></td>
									</tr>
									<tr>
										<td style="font-size: 12px; color: #666">height:<%=masterBol.get("print_range_height", "") %></td>
									</tr>
								</table>
							</td>
						</tr>
					</table> <%}DBRow Bol =(DBRow) rows[a].get("BOLFormatBAK",new DBRow());
						   //System.out.println("路径:"+Bol.get("template_path", ""));
								if(!Bol.get("template_path", "").equals("")){
						%>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" valign="middle" width="50px;"><input
								type="checkbox" value="<%=Bol.get("template_path", "")  %>"
								num="<%=number %>" name="lable" companyId="<%=companyId %>"
								customerId="<%=customerId %>" doorName="<%=doorName %>"
								name="lable" onclick="changeAllBtn()"
								master_bol_nos="<%=master_bol_nos %>"
								order_nos="<%=order_nos %>" /></td>
							<td width="400px;">
								<div style="font-size: 30px; font-weight: bold;">
									<a href="javascript:void(0)"
										style="text-decoration: none; outline: none"
										<%
										if(!canPrint){
										%>
										onclick="alert('This ship account can not print BOL!')"
										<%}
										else{ 
										%>
										onclick="printSingle(this,'<%=number %>','<%=checkDataType %>','<%=companyId %>','<%= customerId%>','<%=doorName%>','<%=master_bol_nos %>','<%=order_nos %>',<%=number_type%>)"
										<% 
										}
										%>
										test="<%=Bol.get("template_path", "") %>">BOL</a>
								</div>
								<div style="font-size: 12px; color: #666"><%=rows[a].get("BOLFormat", "error")%></div>
							</td>
							<td style="border-left: 1px solid #dddddd" align="left">
								<table>
									<tr>
										<td
											style="font-size: 12px; border-bottom: 1px solid #dddddd; color: #666">page:<%=Bol.get("paper", "") %></td>
									</tr>
									<tr>
										<td
											style="font-size: 12px; border-bottom: 1px solid #dddddd; color: #666">width:<%=Bol.get("print_range_width", "") %></td>
									</tr>
									<tr>
										<td style="font-size: 12px; color: #666">height:<%=Bol.get("print_range_height", "") %></td>
									</tr>
								</table>
							</td>
						</tr>
					</table> <%} %>
				</td>
			</tr>
			<%} %>
		</table>
		<%}else{ %>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			style="background-color: #FFF;">
			<%     		  
	    		    //customerId="VIZIO";
	    		    //companyId="W12";
	    		    DBRow[] rows=null;
	    		    if(number_type==ModuleKey.CHECK_IN_ORDER){
	    		    	 rows = sqlServerMgr.findBillOfLadingTemplateByOrderNo(Long.parseLong(number), companyId, customerId,0,request);
	    		    }else if(number_type==ModuleKey.CHECK_IN_PONO && !"".equals(order_no)){
	    		    	rows = sqlServerMgr.findBillOfLadingTemplateByOrderNo(Long.parseLong(order_no), companyId, customerId,0,request);
	    		    }
	    			if(rows!=null){
				 	rows = checkInMgrZwb.findLableByName(rows,number_type);
	    	 	   for(int a=0;a<rows.length;a++){ 
 					  companyId=rows[a].getString("CompanyID");
				      customerId=rows[a].getString("CustomerID");
				      shipToName =rows[a].getString("ShipToName");
				      //判断ShipToName包含制定缩写就不可打印
				      if(shipToName.toUpperCase().indexOf("KOHL") != -1){
 					  	canPrint = false;
 					  }
				      String master_bol_nos = rows[a].getString("master_bol_nos");
				      int master_bol_nos_len = rows[a].get("master_bol_nos_len", 0);
				      String order_nos = rows[a].getString("order_nos");
				      int order_no_len = rows[a].get("order_no_len", 0);
				      
				      //判断是不是显示Trailer Loader 和 Freight Counted的勾选框
				      if(customerId.equals("VIZIO") || customerId.equals("VZB") 
				 				||customerId.equals("VIZIO3") ||customerId.equals("VZO") ||customerId.equals("VIZIO2")){
				    	  vizioFlag=false;
				 		}
    	 	 %>
			<input type="hidden" id="load_<%=number %>"
				master_bol_nos="<%=master_bol_nos%>" order_nos="<%=order_nos%>">
			<tr>
				<td width="30%" align="center" valign="middle"
					style="font-size: 18px; font-weight: bold; border-bottom: 1px solid black;">
					<div align="left" style="padding-left: 20px; font-size: 22px;"><%=rows[a].get("customerId", "") %></div>
					<div align="left"
						style="font-size: 12px; color: #666; padding-left: 20px;"><%=moduleKey.getModuleName(number_type) %>:
					</div>
					<div align="left" style="padding-left: 20px;"><%=number %></div>
				</td>
				<td
					style="border-left: 1px solid #dddddd; border-bottom: 1px solid black;">
					<%DBRow Bol =(DBRow) rows[a].get("BOLFormatBAK",new DBRow());
						   //System.out.println("路径:"+Bol.get("template_path", ""));
								if(!Bol.get("template_path", "").equals("")){
						%>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" valign="middle" width="50px;"><input
								type="checkbox" value="<%=Bol.get("template_path", "")  %>"
								num="<%=number %>" name="lable" companyId="<%=companyId %>"
								customerId="<%=customerId %>" doorName="<%=doorName %>"
								name="lable" onclick="changeAllBtn()"
								master_bol_nos="<%=master_bol_nos %>"
								order_nos="<%=order_nos %>" /></td>
							<td width="400px;">
								<div style="font-size: 30px; font-weight: bold;">
									<a href="javascript:void(0)"
										style="text-decoration: none; outline: none"
										<%
										if(!canPrint){
										%>
										onclick="alert('This ship account can not print BOL!')"
										<%}
										else{ 
										%>
										onclick="printSingle(this,'<%=number %>','<%=checkDataType %>','<%=companyId %>','<%= customerId%>','<%=doorName%>','<%=master_bol_nos %>','<%=order_nos %>',<%=number_type%>)"
										<% 
										}
										%>
										test="<%=Bol.get("template_path", "") %>">BOL</a>
								</div>
								<div style="font-size: 12px; color: #666"><%=rows[a].get("BOLFormat", "error")%></div>
							</td>
							<td style="border-left: 1px solid #dddddd" align="left">
								<table>
									<tr>
										<td
											style="font-size: 12px; border-bottom: 1px solid #dddddd; color: #666">page:<%=Bol.get("paper", "") %></td>
									</tr>
									<tr>
										<td
											style="font-size: 12px; border-bottom: 1px solid #dddddd; color: #666">width:<%=Bol.get("print_range_width", "") %></td>
									</tr>
									<tr>
										<td style="font-size: 12px; color: #666">height:<%=Bol.get("print_range_height", "") %></td>
									</tr>
								</table>
							</td>
						</tr>
					</table> <%} %> <%} %>
				</td>
			</tr>
			<%} %>
		</table>
		<%} %>
		<%} %>
		<%} %>
	</div>
	<div id="printListMoBan" style="display: none"></div>

	<script type="text/javascript">
	 //判断是不是显示Trailer Loader 和 Freight Counted的勾选框
		function showLoader(){
		  var vizioFlag='<%=vizioFlag%>';
		  if(vizioFlag=="true"){
			  $("#loaderFreight").css("display","block");
		  }
	  }
	</script>
</body>
</html>






