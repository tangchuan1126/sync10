<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<%
	long info_id=StringUtil.getLong(request,"infoId");
	String in_seal=StringUtil.getString(request, "in_seal");
	String out_seal=StringUtil.getString(request,"out_seal");
	DBRow mainRow=checkInMgrZwb.findGateCheckInById(info_id);
	String checkInTime=mainRow.getString("window_check_in_time");
	long num=1;
	if(checkInTime.equals("")){
		num=2;
	}
	long load_count=mainRow.get("load_count",0l);

%>


<title>Choice Single Or Multiple</title>
</head>
<body>
<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px; " align="center">
	    <table width="90%" height="90%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td align="center" valign="middle" width="50%"><img src="imgs/single_load.jpg" onclick="singleClick(<%=load_count%>)"></td>
		    <td align="center" valign="middle"><img src="imgs/multiple_load.jpg" onclick="multipleClick(<%=num%>)"></td>
		  </tr>
		</table>
	</div>	
</body>
</html>
<script type="text/javascript" class="source">
  function singleClick(load_count){
	var info_id=<%=info_id%>;
	var in_seal='<%=in_seal%>';
	var out_seal='<%=out_seal%>';
	if(load_count<=1){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_warehouse.html?infoId='+info_id+'&in_seal='+in_seal+'&out_seal='+out_seal; 
//		$.artDialog.open(uri , {title: "WarehouseCheckIn",width:'750px',height:'620px', lock: true,opacity: 0.3,fixed: true});
		$.artDialog.opener.selectDialog(uri,"WarehouseCheckIn");
		$.artDialog && $.artDialog.close();
	}else{
		 showMessage("Multiple load Please check in office","alert");
	}
  }
  function multipleClick(num){
	  var info_id=<%=info_id%>;
	  var in_seal='<%=in_seal%>';
	  var out_seal='<%=out_seal%>';
	  if(num==1){
		  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_warehouse.html?infoId='+info_id+'&in_seal='+in_seal+'&out_seal='+out_seal; 
//		  $.artDialog.open(uri , {title: "WarehouseCheckIn",width:'750px',height:'620px', lock: true,opacity: 0.3,fixed: true});
	  	  $.artDialog.opener.selectDialog(uri,"WarehouseCheckIn");
		  $.artDialog && $.artDialog.close();
	  }else{
		  showMessage("Please check in office","alert");
	  }
  }

</script>
