<%@page import="com.cwc.app.key.ModuleKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>


<%@ include file="../../include.jsp"%>

<%
	String printDirect="";
	boolean isPrint = StringUtil.getInt(request, "isprint") == 1; 
	String jsonString = StringUtil.getString(request, "jsonString");
	JSONArray jsons = new JSONArray(jsonString);
	
	String printName=StringUtil.getString(request, "print_name");
	long adid = StringUtil.getLong(request, "adid");
	long ps_id = 0;
	if(adid == 0l){
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	    adid = adminLoggerBean.getAdid();
	    ps_id = adminLoggerBean.getPs_id();
	}else{
		ps_id = adminMgr.getDetailAdmin(adid).get("ps_id",0l);
	}

	String logoUrl ="";
	String width = "";
	String height ="";
	

%>
<html>
<head>
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
<%if(!isPrint){ %>
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>

<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
<%} %>
<title>Packing List</title>


</head>
<body>
<%if(!isPrint){ %>
	<div style="float:left;width:210mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;">
		<input class="short-short-button-print" type="button" onclick="printPackingList()" value="Print">
	</div>
<%} %>
	<%
	for(int i = 0; i < jsons.length(); i ++){
		JSONObject json=jsons.getJSONObject(i);
		long order=Long.parseLong(json.getString("order_no"));
		String companyId=json.getString("companyId");
		String customerId=json.getString("customerId");
		String accountId=json.getString("accountId");
		
		DBRow orderInfo = sqlServerMgr.findOrderSomeInfosByOrderNo(order,companyId, customerId, adid, request);
		if(orderInfo == null){
			continue; 
		}
		orderInfo.put("print_date",DateUtil.showLocalparseDateTo24Hours(DateUtil.NowStr(),ps_id));
		
		DBRow[] orderItems = sqlServerMgr.findOrderItemsSomeInfoGroupByOrderNo(order, companyId, customerId, adid, request);
		 float volumeTotal = 0F;
	     int unitTotal = 0;
	     int caseTotal = 0 ;
		for(DBRow orderC : orderItems ){
			  volumeTotal += orderC.get("volume1", 0F); 
			  unitTotal += orderC.get("ShippedQty", 0);  
			  caseTotal += orderC.get("orderLineCase", 0);  
		}
		orderInfo.put("volumeTotal",volumeTotal);
		orderInfo.put("unitTotal",unitTotal);
		orderInfo.put("caseTotal",caseTotal);

		DBRow printTemplate = checkInMgrWfh.getPacklistPrintTemplate(accountId,"");
		/**String strTemplateHeader =printTemplate.getString("template_header");
		String strTemplateOrderInfo =printTemplate.getString("template_order_info");
		String strTemplateItemHeader =printTemplate.getString("template_item_header");
		String strTemplateItemData =printTemplate.getString("template_item_data");
		String headerInfo = StringUtil.MergerTemplate(strTemplateHeader,orderInfo);
		String orderData = StringUtil.MergerTemplate(strTemplateOrderInfo,orderInfo);**/
		//String strTemplate = printTemplate.getString("full_template").replaceAll("\\r\\n" , "");
		String strTemplate = printTemplate.getString("full_template");
		printDirect = printTemplate.getString("print_direct","1");
		logoUrl =printTemplate.getString("image_url");
		width = printTemplate.getString("width","0");
		height =printTemplate.getString("height","0");
		String strHtml = StringUtil.MergerTemplate(strTemplate,orderInfo);
		
		Pattern p1 = Pattern.compile("<!--\\s?Item\\s+Start\\d?\\s?-->");
		Matcher m1 = p1.matcher(strHtml);
		Pattern p2 = Pattern.compile("<!--\\s?Item\\s+End\\d?\\s?-->");
		Matcher m2 = p2.matcher(strHtml);
		String strItemTemplate = "";
		
		
		int index =0;
		while(m1.find() && m2.find() ) {
			String strStart = m1.group().toString();
			String strEnd = m2.group().toString();
			
			int startIndex = strHtml.indexOf(strStart);
			int endIndex = strHtml.indexOf(strEnd);
			
			strItemTemplate = strHtml.substring(startIndex,endIndex+ strEnd.length());
			
			//System.out.println("strItemTemplate:"+strItemTemplate);
			String orderItemData = "";
			for(int j = 0; j < orderItems.length; j ++){
				String itemDate = StringUtil.MergerTemplate(strItemTemplate,orderItems[j]);
				orderItemData+= itemDate;
				
			}
			//System.out.println("orderItemData:"+orderItemData);
			strHtml = strHtml.replace(strItemTemplate, orderItemData);
			
	    }
		
		
	%>
	<%=strHtml%>
	
 
	<%} %>
	
</body>
</html>
<script type="text/javascript">
function operation(){
	document.getElementById("volumeTotals").innerHTML  = document.getElementById("volumeTotal").value;
	document.getElementById("unitTotals").innerHTML  = document.getElementById("unitTotal").value;
	document.getElementById("caseTotals").innerHTML  = document.getElementById("caseTotal").value;
}
</script>
<script>
function supportAndroidprint(){
	//获取打印机名字列表
   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
   	 //判断是否有该名字的打印机
   	var printer = "<%=printName%>";
   	var printerExist = "false";
   	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){
		return androidIsPrint(containPrinter);
	}else{
		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消
			return androidIsPrint(containPrinter);
		}
	}
}

function androidIsPrint(containPrinter){
	var printAll = $('div[name="printPackingListHtml"]');
	var flag = true ;
	for(var i=0;i<printAll.length;i++){
		visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
		visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"Letter");
		visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
		visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$(printAll[i]).html());	
		visionariPrinter.SET_PRINT_COPIES(1);
		visionariPrinter.PREVIEW();
	 	//flag = flag && visionariPrinter.PRINT();
	}
	
	return flag ;
}

$(function(){
  var logo_url = '<%=logoUrl%>';
  var logo_width = '<%=width%>';
  var logo_height = '<%=height%>';
  var strLogo = "<img id=\"area_img\"   border=\"0\" src=\""+logo_url+"\" "; 
  if(parseInt(logo_width)>0){
  	 strLogo+=" width=\""+logo_width+"\" ";
  }

  if(parseInt(logo_height)>0){
  	 strLogo+=" height=\""+logo_height+"\" ";
  }
  strLogo+=">";
  $("#area_logo").css("display","block");
  $("#area_logo").html(strLogo);
  
  



});

function printPackingList() {
	var printAll = $('div[name="printPackingListHtml"]');
	for(var i=0;i<printAll.length;i++){
		visionariPrinter.SET_PRINT_STYLE("FontName", "Verdana");
		visionariPrinter.SET_PRINT_PAGESIZE(<%=printDirect%>,0,0,"Letter");
		visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$(printAll[i]).html());	
		visionariPrinter.SET_PRINT_COPIES(1);
		//visionariPrinter.PREVIEW();
	 	visionariPrinter.PRINT();
	}
	
}


</script>
