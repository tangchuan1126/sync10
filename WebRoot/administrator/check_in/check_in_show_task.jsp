<%@page import="com.cwc.app.floor.api.wfh.FloorCheckInMgrWfh"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey" %>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="checkInChildDocumentsStatusTypeKey" class="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"/>
<jsp:useBean id="occupyTypeKey" class="com.cwc.app.key.OccupyTypeKey"/>
<%
	
	long entry_id = StringUtil.getLong(request, "entry_id");
	long dlo_id = StringUtil.getLong(request, "dlo_id");
	int equipment_id = StringUtil.getInt(request, "equipment_id");
	int showType = StringUtil.getInt(request, "show_type");
	DBRow[] taskClosed = checkInMgrWfh.findTaskClosedByEntry(entry_id,equipment_id);
	DBRow entry_trailer = checkInMgrWfh.findEquipmentByEquipmentId(equipment_id);
	List<DBRow> tasks = new ArrayList<DBRow>();
	for(DBRow task : taskClosed){
		if(showType==1){
			if(task.get("number_status",01) == CheckInChildDocumentsStatusTypeKey.CLOSE){
				tasks.add(task);
			}
		}else{
			if(task.get("number_status",01) != CheckInChildDocumentsStatusTypeKey.CLOSE){
				tasks.add(task);
			}
		}
	}
%>
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<!-- table 斑马线 -->

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">

	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<script type="text/javascript">
		function showMenu(evt,dlo_id,type){
			$("<scri"+"pt>"+"</scr"+"ipt>").attr({src:'../js/easyui/jquery.easyui.menu.js',type:'text/javascript',id:'load'}).appendTo($('head').remove('#loadScript'));
		 	var x = $(evt).offset().left;
			var y = $(evt).offset().top;
			 	 
			$('#menubutton_'+dlo_id+'_'+type).menu('show', { 
				left: x, 
				top: y+20
			});
		}
		function closeNum(main_id,detail_id,number_status,door_id,rel_type,isLive,equipmentId,resources_type,number_type,resources_type_name,resources_id_name){
			$.artDialog.opener.closeNum(main_id,detail_id,number_status,door_id,rel_type,isLive,equipmentId,resources_type,number_type,resources_type_name,resources_id_name);
			$("input[name='menubutton_"+main_id+"_"+detail_id+"']").remove();
			$("span[attr='"+main_id+"_"+detail_id+"']").html('Closed');
		}
		function exception(main_id,detail_id,number_status,door_id,rel_type,isLive,equipmentId,resources_type,number_type,resources_type_name,resources_id_name){
			$.artDialog.opener.exception(main_id,detail_id,number_status,door_id,rel_type,isLive,equipmentId,resources_type,number_type,resources_type_name,resources_id_name)
			$("span[attr='"+main_id+"_"+detail_id+"']").html('Exception');
		}
		function partially(main_id,detail_id,number_status,door_id,rel_type,isLive,equipmentId,resources_type,number_type,resources_type_name,resources_id_name){
			$.artDialog.opener.partially(main_id,detail_id,number_status,door_id,rel_type,isLive,equipmentId,resources_type,number_type,resources_type_name,resources_id_name)
			$("span[attr='"+main_id+"_"+detail_id+"']").html('Partially');
		}
	</script>
</head>
<body onLoad="onLoadInitZebraTable()" style="margin-top:0px">
<div style="font-weight: bold;font-size:16px;margin:0px;position: fixed;width:100%;height:20px;background:#fff;padding-left: 20px;padding-top:5px">Etnry ID: <%=dlo_id %></div>
<div style="height:20px"></div>
 <fieldset align="left" style="text-algin:left;border:2px solid #000; width:95%; margin-top:10px;border-radius: 5px;padding-top: 15px;">
	 <table cellspacing="0" cellpadding="0" border="0" width="100%">
  			  <%
	  			for(DBRow taskRow:tasks){
  			  %>
	  			  	<tr style="height:25px;">
	  			  		<%if(taskRow.get("number_type", 0)==moduleKey.CHECK_IN_LOAD||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_PONO||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_ORDER||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_PICKUP_ORTHERS){ %>
			   				<td style="text-align: right;color:blue" width="53px"><%=moduleKey.getModuleName(taskRow.get("number_type", 0)) %>:</td>
			   			<%}else if(taskRow.get("number_type", 0)==moduleKey.CHECK_IN_BOL||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_CTN||taskRow.get("number_type", 0)==moduleKey.CHECK_IN_DELIVERY_ORTHERS){ %>
			   				<td style="text-align: right;color:red" width="53px"><%=moduleKey.getModuleName(taskRow.get("number_type", 0)) %>:</td>
			   			<%}else{ %>
			   				<td style="text-align: right;" width="53px"></td>
			   			<%} %>
			   		<td style="text-align:left; padding:0px; margin:0px;width: 120px;">
	  			  		&nbsp;<%=taskRow.get("number", "")%>
	  			  	</td>
	  			  	<td style="width:135px">
	  					 <%//显示单据的状态
	    		 			 if(taskRow.get("number_status",01)==CheckInChildDocumentsStatusTypeKey.CLOSE ){
	    		 			 	if(taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_LOAD || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_ORDER ||  taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_PONO){ %>
	      		    				<a href="javascript:void(0)" onclick="$.artDialog.opener.showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Load Info</a>
	      						<%}else if(taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_CTN || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_BOL){ %>
			      		   	 	  <a href="javascript:void(0)" onclick="$.artDialog.opener.showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive Info</a>

			      		   	    <%}else{ %>
	      		   					<span style="font-size:10px; color:#999">Closed</span>
	      		   				<%} %>
	    		    	<%
	    					}else{
	    					//***********继续显示 more 按钮组
	    				%>
					 	     <input id="<%=taskRow.get("dlo_detail_id",0l)%>" type="button" style="color:#000" name="menubutton_<%=dlo_id%>_<%=taskRow.get("dlo_detail_id",0l)%>"  onclick="showMenu(this,<%=dlo_id%>,'<%=taskRow.get("dlo_detail_id",0l)%>')"  class="more-fun-short-button">
					    	<div id="menubutton_<%=dlo_id%>_<%=taskRow.get("dlo_detail_id",0l)%>" class="easyui-menu" style='width:120px;display:none' >
					    	
						 	 <%if(taskRow.get("number_type", 0l)==10 || taskRow.get("number_type", 0l)==11 || taskRow.get("number_type", 0l)==17 || taskRow.get("number_type", 0l)==18){
			 			 %>
			 			  	<%if(taskRow.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE){%>
			  			   		<div   onclick="closeNum(<%=entry_trailer.get("check_in_entry_id", 0L)%>,<%=taskRow.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.CLOSE %>,<%=taskRow.get("rl_id", 0)%>,<%=entry_trailer.get("rel_type",0l)%>,<%=entry_trailer.get("equipment_purpose",0l)%>,'<%=entry_trailer.get("equipment_id", 0)%>',<%=taskRow.get("occupancy_type", 0)%>,<%=taskRow.get("number_type", 0l)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))%>','<%=taskRow.get("rl_id_name", "")%>')">
						 	  	 	Close
						 	   </div>
						 	   <%}if(taskRow.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION){ %>
						 	   <div   onclick="exception(<%=entry_trailer.get("check_in_entry_id", 0L)%>,<%=taskRow.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION %>,<%=taskRow.get("rl_id", 0) %>,<%=entry_trailer.get("rel_type",0l)%>,<%=entry_trailer.get("equipment_purpose",0l)%>,'<%=entry_trailer.get("equipment_id", 0)%>',<%=taskRow.get("occupancy_type", 0)%>,<%=taskRow.get("number_type", 0l)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))%>','<%=taskRow.get("rl_id_name", "")%>')" >
						 	 	 	Exception
						 	   </div>
						 	   <%}if(taskRow.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.PARTIALLY){  %>
						 	   <div   onclick="partially(<%=entry_trailer.get("check_in_entry_id", 0L)%>,<%=taskRow.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.PARTIALLY %>,<%=taskRow.get("rl_id", 0) %>,<%=entry_trailer.get("rel_type",0l)%>,<%=entry_trailer.get("equipment_purpose",0l)%>,'<%=entry_trailer.get("equipment_id", 0)%>',<%=taskRow.get("occupancy_type", 0)%>,<%=taskRow.get("number_type", 0l)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))%>','<%=taskRow.get("rl_id_name", "")%>')" >
						 	 		Partially
						 	   </div>
					 	     <%
						 	   }
						   } else{
					     %>
						     <%  if(taskRow.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE){%>
						     	 	<div  onclick="closeNum(<%=entry_trailer.get("check_in_entry_id", 0L)%>,<%=taskRow.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.CLOSE %>,<%=taskRow.get("rl_id", 0) %>,<%=entry_trailer.get("rel_type",0l)%>,<%=entry_trailer.get("equipment_purpose",0l)%>,'<%=entry_trailer.get("equipment_id", 0)%>',<%=taskRow.get("occupancy_type", 0)%>,<%=taskRow.get("number_type", 0l)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))%>','<%=taskRow.get("rl_id_name", "")%>')">
							 	  		Close
							 	   	</div>
							 <%}if(taskRow.get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION){ %>
							 	   <div   onclick="exception(<%=entry_trailer.get("check_in_entry_id", 0L)%>,<%=taskRow.get("dlo_detail_id",0l) %>,<%=CheckInChildDocumentsStatusTypeKey.EXCEPTION %>,<%=taskRow.get("rl_id", 0) %>,<%=entry_trailer.get("rel_type",0l)%>,<%=entry_trailer.get("equipment_purpose",0l)%>,'<%=entry_trailer.get("equipment_id", 0)%>',<%=taskRow.get("occupancy_type", 0)%>,<%=taskRow.get("number_type", 0l)%>,'<%=occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))%>','<%=taskRow.get("rl_id_name", "")%>')" >
							 	  		Exception 
							 	  		
							 	   </div>
					     <%}}%>
						 	</div>
    				  	<%
		      				if(taskRow.get("number_status",01)==CheckInChildDocumentsStatusTypeKey.CLOSE ){
		      		    %>
				      		     <%if(taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_LOAD || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_ORDER ||  taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_PONO){ %>
				      		      <a href="javascript:void(0)" onclick="$.artDialog.opener.showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Load Info</a>
				      			 <%}else if(taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_CTN || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_BOL){ %>
	      		   	 	  		  <a href="javascript:void(0)" onclick="$.artDialog.opener.showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive Info</a>

	      		   	    <%        }
		      				}
		      				else if(taskRow.get("number_status",01)==CheckInChildDocumentsStatusTypeKey.EXCEPTION && (taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_CTN || taskRow.get("number_type", 0) == ModuleKey.CHECK_IN_BOL)){
		      			%>
		      					  <a href="javascript:void(0)" onclick="$.artDialog.opener.showPalletsInfo('<%=taskRow.get("dlo_detail_id", 0l) %>','<%=taskRow.get("number_type", 0) %>')"  style="font-weight: bold;color:#666"  >Receive</a>
		      			
		      			<%
		      				}else if(taskRow.get("is_forget_task", 0)==1 && (taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.CLOSE && taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.EXCEPTION && taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.PARTIALLY)){ %>
								<span attr="<%=entry_trailer.get("check_in_entry_id", 0) %>_<%=taskRow.get("dlo_detail_id",0l) %>" style="font-size:10px; color:#999">Forget</span>		      	
				      	<%  }else{ %>
    				    	 	<span attr="<%=entry_trailer.get("check_in_entry_id", 0) %>_<%=taskRow.get("dlo_detail_id",0l) %>" style="font-size:10px; color:#999"><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(taskRow.get("number_status",0)) %></span>
    		   			<%  } 
    				  	   } //显示状态 end%>
    		    </td>
    		    <td style="padding-left:0px;text-align:center">
					At <%if(taskRow.get("occupancy_type", 0)>0){
	  			  				out.print(occupyTypeKey.getOccupyTypeKeyName(taskRow.get("occupancy_type", 0))+": "+taskRow.get("rl_id_name", "")); 
	  			   			}
	  			   	   %>
				</td >
		   		</tr>
		   		<tr><td colspan="5" style="padding-left:5px">
		   		<!-- 显示warhouse通知的人 -->
				<%
					int detailId = taskRow.get("dlo_detail_id",0);
					DBRow[] notices = checkInMgrWfh.findScheduleByDetailId(detailId, 53);
					if(notices!=null && notices.length>0){
						%>
						<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td style="padding:0px;width:80px;height:14px;color:#999">
								Assign Labor
							</td>
							<td width="125px" style="padding:0px">
								
								<%
								int noticesCount = notices.length;
								noticesCount = noticesCount>1?1:noticesCount;
								for(int noticesIndex=0;noticesIndex<noticesCount;noticesIndex++){
								%>
									<%=notices[noticesIndex].get("schedule_execute_name", "") %>
									
								<%}%>
								
							</td>
						<%String closeName =notices[0].getString("schedule_finish_name");  
						
						if(taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.PROCESSING && taskRow.get("number_status",0)!=checkInChildDocumentsStatusTypeKey.UNPROCESS){
						%>
						<td style="color:#999;padding:0px;width:80px" ><%=checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(notices[0].get("number_status", 0)) %> by</td>
						<td style="padding:0px;"><%=closeName %></td>
						<%} %>
					</tr>
					</table>
					<%} %>
					</td></tr>
					<%}%>
			   	 </table>
			   	 </fieldset>
</body>
</html>