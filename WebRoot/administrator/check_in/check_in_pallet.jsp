<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="palletInventoryKey" class="com.cwc.app.key.PalletInventoryKey"/>
<html>
<head>
<!--  基本样式和javascript -->
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>



<!-- 下拉框多选 -->
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
	<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>



<!-- 带搜索功能的下拉框、 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../b2b_order/chosen.jquery.min.js" type="text/javascript"></script> 


<script type="text/javascript" src="../js/select.js"></script>

<style>

.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
	
}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:500px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
body {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
	td {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
</style>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long psId=adminLoggerBean.getPs_id(); 
	//long adgid=adminLoggerBean.getAdgid(); 
	boolean  isadministrator = adminLoggerBean.isAdministrator();  //判断当前登录帐号是不是admin.如果是返回true
	String key = StringUtil.getString(request,"key");
	String cmd = StringUtil.getString(request,"cmd");
	int search_mode = StringUtil.getInt(request,"search_mode");
	long ps_id = StringUtil.getLong(request,"ps_id");
	int invenStatus = StringUtil.getInt(request,"invenStatus");
	int tabSelect=StringUtil.getInt(request, "tabSelect");
	int titleId=StringUtil.getInt(request, "title_id");
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(5);
	
	DBRow[] ps = checkInMgrZwb.findSelfStorage();		
	DBRow[] row = checkInMgrWfh.getAllPallet(ps_id,invenStatus,titleId,pc);	
	DBRow[] title = checkInMgrWfh.findAllTitle("", 0L);
	if(cmd.equals("search")){
//		row = checkInMgrZwb.findPalletByType(ps_id,incenStatus);	
	}
%>
<title>Check in</title>
<script type="text/javascript">

function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
		
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
		   searchRightButton();
		}  
	}  
}
function search()
{
	var val = $("#search_key").val();
			
	if(val.trim()=="")
	{
		alert("Please enter key word for searching");
	}
	else
	{
		val = val.replace(/\'/g,'');
		var val_search = "\'"+val.toUpperCase()+"\'";
		$("#search_key").val(val_search);
		
		document.search_form.key.value = val_search;
		document.search_form.search_mode.value = 1;
		document.search_form.submit();
	}
}
function filter(flag){
	if(flag!="refresh"){
		document.filter_form.ps_id.value = $("#whse").val();
		document.filter_form.tabSelect.value = 1;
		
	}else{
		document.filter_form.tabSelect.value = 0;
	}
	
	
	document.filter_form.submit();
}
function searchInventory(){
	var ps_id = $("#whse").val();
	var invenStatus = $("#invenStatus").val();
	var titleId = $("#title").val();
	document.dataForm.ps_id.value = ps_id;
	document.dataForm.invenStatus.value = invenStatus;
	document.dataForm.tabSelect.value = 1;
	document.dataForm.title_id.value = titleId;
	document.dataForm.submit();
	
}
function addOrEditPallet(pa_id,ps_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/saveOrEditPallet.html?pallet_id='+pa_id+'&ps_id='+ps_id; 
	$.artDialog.open(uri , {title: "AddOrUpdatePalletInventory",width:'400px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}
function showTitle(palletId){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/showPalletTitles.html?pallet_id='+palletId; 
	$.artDialog.open(uri , {title: "AddOrUpdatePalletInventory",width:'600px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}

</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<div class="demo" style="width:100%">
	  <div id="tabs">
		<ul>
			
			<li><a href="#checkin_filter">Advanced Search</a></li>
		</ul>
		
		<div id="checkin_filter">
			<table style=" margin-left:100px;margin-bottom:19px;margin-top:19px;">
				<tr>
					<td style="font-size: 14px;">WHSE:</td>
					<td>
					<%
						String str = "";
					if(adgid!=10000L){
						str = "disabled";
						ps_id = psId;
					}
						
					%>
				
					<select id="whse" name="WHSE" style="width:150px" class="chzn-select" <%=str %>>
							<option value="0" >All Storage</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("ps_id",0l)%>"  <%=ps_id==ps[j].get("ps_id",0l)?"selected":"" %>> <%=ps[j].getString("ps_name")%></option>
							<%
								}
							%>
						</select>
					</td>
					<td style="font-size: 14px;width:200px" align="right">Inventory Status:</td>
					<td><select id="invenStatus"  style="width:150px" class="chzn-select"  name="invenStatus">
							<option value="0" >All Inventory</option>
							<option value="1" <%=invenStatus==1?"selected":"" %>>Keep Inventory</option>
							<option value="2" <%=invenStatus==2?"selected":"" %>>No Inventory</option>
							
						</select></td>
					<td style="font-size: 14px;width:100px" align="right">Title:</td>
					<td style="font-size:12px;">
					
						<select  name="title_id" id="title" class="chzn-select" data-placeholder="Choose a Title..." style="width:200px;" tabindex="1" >
							<option value="0">All Title</option>
							<%for(int j= 0;j<title.length;j++){ %>
							<option value="<%=title[j].get("title_id", 0)%>" <%=titleId==title[j].get("title_id", 0)?"selected":"" %> ><%=title[j].get("title_name", "") %></option>
							<%} %>
						</select>
		   	 
						
						</td>
					<td style="width:200px" align="right"><input type="button" onclick="searchInventory();"  class="long-button" value="Search" /></td>
				</tr>
			</table>
		</div>
       </div>
       <script>
     
       $("#tabs").tabs({
	   		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
	   });
       	$("#tabs").tabs("select",<%=tabSelect%>);
    	$("#title").chosen({no_results_text:"没有该选项:"});
       	$("#invenStatus").chosen({allow_single_deselect:true,disable_search:true,no_results_text: "没有符合条件的结果!"});
       	$("#whse").chosen({allow_single_deselect:true,disable_search:true,no_results_text: "没有符合条件的结果!"});
       </script>
       
    </div>
    <div align="right" style="height:31px;">  <input style="margin-right: 27px;margin-top: 5px;" type="button" class="theme-button-add" onclick="addOrEditPallet(0,0)" value="Add"></div>
    <table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"  isNeed="false" class="zebraTable mytable" isNeed="false" style="margin-left:3px;">
    	<tr> 
	        <th width="20%" class="left-title" style="vertical-align:center;text-align:center;">Pallet Type</th>
	        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Pallet Totality</th>
	        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">Damaged Totality</th>
	        <th width="35%" class="left-title" style="vertical-align:center;text-align:center;">Storage Location</th>
	        <th  class="left-title" style="vertical-align: center;text-align: center;">Options</th>
		</tr>
		<%if(row.length>0){ 
			for(int i=0;i<row.length;i++){
		%>
		
		<tr style="text-align: center">
			<td style="padding:25px 15px 25px 15px;"><%=palletInventoryKey.getPalletInventoryName(row[i].get("pallet_type", 0)) %></td>
			<td style="padding:25px 15px 25px 15px;"><%=row[i].get("pallet_count", "") %></td>
			<td style="padding:25px 15px 25px 15px;"><%=row[i].get("damaged_count", "") %></td>
			<td style="padding:25px 15px 25px 15px;" align="center">
			
			<fieldset style="width:400px"><legend style="color:#777"><span style="font-size: 15px"><B><%=row[i].get("title", "") %></B></span> warehouse</legend>
			
			<%
				DBRow[] titles = (DBRow[]) row[i].get("titles",new DBRow[0]);
				int length = titles.length;
				if(length>3){
					length = 3;
				}
				for(int j = 0;j<length;j++){
			%>
			<div style="border-bottom: 1px dashed #bbb;margin:0px;padding:0px;">
			<table  border="0" cellpadding="0" cellspacing="0" style = "margin:0px;padding:0px;width:400px;background:#fff">
				<tr>
					<td rowspan="2" style="text-align:center;border-right: 1px solid #bbb;" width="150px"><%=titles[j].get("title_name", "") %></td>
                    <td align="right">Pallet Count:</td>
					<td style="color:green;padding-left:0px">&nbsp;<%=titles[j].get("pallet_count", 0F) %></td>
				</tr>
				<tr>
                	<td align="right" width="100px">Damaged Count:</td>
					<td style="color:red;padding-left:0px">&nbsp;<%=titles[j].get("damaged_count", 0F) %></td>
				</tr>
			</table>
			</div>
			
			
			
			<%} %>
			<%if(titles.length>3){ %>
			<div align="right" style=""><a style="color:#f60" onclick="showTitle('<%=row[i].get("pallet_id", 0) %>')" href="javascript:void(0)">More...</a></div>
			<%} %>
			</fieldset>
			
			</td>
			<td><input type="button" onclick="addOrEditPallet('<%=row[i].get("pallet_id", 0)  %>','<%=row[i].get("ps_id", 0L) %>')" class="short-short-button-mod" value="Edit"></td>
		
		</tr>
		<%}} %>
	</table>
	<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" action="check_in_pallet.html">
	    <input type="hidden" name="p"/>

		<input type="hidden" name="title_id" value="<%=titleId %>" />
		<input type="hidden" name="tabSelect" value="<%=tabSelect %>" />
		<input type="hidden" name="invenStatus" value="<%=invenStatus %>"/>
		<input type="hidden" name="ps_id" value="<%=ps_id %>"/>
		<input type="hidden" name="key" value="<%=key %>"/>
		<input type="hidden" name="cmd" value="<%=cmd %>"/>
		<input type="hidden" name="search_mode" value="<%=search_mode %>"/>

	  </form>
	  <tr>
	    <td height="28" align="right" valign="middle" class="turn-page-table">
	        <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
	      Goto 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
	    </td>
	  </tr>
   </table>
  
	
	<form action="check_in_pallet.html" method="post" name="search_form">
		<input type="hidden" name="key" />
		<input type="hidden" name="cmd" value="search"/>
		<input type="hidden" name="search_mode"/>
	</form>
		<form action="check_in_pallet.html" method="post" name="filter_form">
		
		<input type="hidden" name="ps_id"/>
		<input type="hidden" name="cmd" value="filter"/>
		<input type="hidden" name="tabSelect" value="1" />
	</form>
</body>
</html>

