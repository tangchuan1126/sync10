<%@page import="com.cwc.app.key.RelativeFileKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="us.monoid.json.JSONObject"%>
<%@page import="us.monoid.json.JSONArray"%>
<%@ include file="../../include.jsp"%>
<%
	String palletIds=StringUtil.getString(request, "palletIds");
	String date=StringUtil.getString(request, "date");
	DBRow[] rows=checkInMgrZwb.queryPalletsByLineAndId(palletIds);//
			
	long adid = StringUtil.getLong(request, "adid");
    String printName=StringUtil.getString(request,"print_name");
	boolean isPrint = StringUtil.getInt(request, "isprint") == 1;
	
%>

<html>
<head>
 <!--  基本样式和javascript -->
<base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
 
<%if(!isPrint){ %>
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>

<%} %>
<title>TLP</title>
<script type="text/javascript">

function androidSign(tempFile,tempKey,signType){
	Android.androidSign(tempFile,tempKey,signType);
 }
 //以上两个方法是在android调用的
 //zhangrui start

function print(){
	var printAll = $('div[name="printAll"]');
	for(var i=0;i<printAll.length;i++){
			 
		 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.2cm","15.2cm","102X152");
// 		 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
         visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$(printAll[i]).html());
         visionariPrinter.SET_PRINT_COPIES(1);
 
         //visionariPrinter.PREVIEW();
         visionariPrinter.PRINT();

			
	}
			
		/* 
			//获取打印机名字列表
	        var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
	         //判断是否有该名字的打印机
	        var printer = "LabelPrinter";
	        var printerExist = "false";
	        var containPrinter = printer;
	        for(var i = 0;i<printer_count;i++){
	          if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){            
	            printerExist = "true";
	            containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
	            break;
	          }
	        } */
	}
</script>
</head>
<body>
<%if(!isPrint){ %>
<div style="width:102mm;border:1px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;margin:0 auto;text-align:right;">
	<input class="short-short-button-print" type="button" onclick="print()" value="Print">
</div>
<%} %>
<br>
<!-- 第一个 -->
<div align="center">
<%
for(int i = 0; i < rows.length; i++){
	DBRow row = rows[i];
	Boolean is_clp = row.get("container_type", 3)==1;
%>
	<div id="printAll" name="printAll" style="width:368px; display:block; border:1px solid red">
		<table style="font-size: 13px;font-family: Arial;text-align:left;vertical-align:center;width:100%;empty-cells:show;border-collapse: collapse; ">
		  <tr style="height:43px;text-align:center;">
		    <td rowspan="2" style="font-size: 100px;border-right:2px solid black;border-bottom:2px solid black;"><center><b>
		   		<% if(is_clp){ %>
		   			C	
		   		<% }else if(row.get("goods_type", 0)==2){%>
			   		D
		   		<% }else{ %>
		   			T
		   		<% }%>
		    </b></center></td>
		    <td colspan="2" style="border-right: 2px solid black;border-bottom: 2px solid black;border-left: 2px solid black;width:20%;"><h2 style="margin-top:16px;">Date</h2></td>
		    <td style="border-bottom: 2px solid black;border-left: 2px solid black;width:40%"><h2 style="margin-top:16px;font-weight:bold;"><%=date %>&nbsp;</h2></td>
		  </tr> 
  		  <tr> 
		    <td colspan="3" style="height:63px;border-bottom: 2px solid black;width:60%;"> 
		    	<div style="vertical-align:middle; line-height:30px;overflow:hidden;">
		    		<label style="position: absolute;">Receipt NO.</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img style="margin-left: 72px;" src="/barbecue/barcode?data=<%=row.get("receipt_no",0l) %>&width=1&height=25&type=code128"/>
	    		</div>
	    		
	    		<div style="font-size: 28px;font-weight:bold;">
		    		<center><%=row.getString("receipt_no") %>&nbsp;</center>
    			</div>
		    </td>
		  </tr> 
		  <tr style="height:43px">
		    <td colspan="2" style="border-right: 2px solid black;border-bottom: 2px solid black;">  PCID<br>
		    <center><div style="font-size: 20px;font-weight:bold;"><%=row.get("pc_id", 0L) %>&nbsp;</div></center>
		    </td>
		    <td colspan="2" style="width:56%;border-bottom: 2px solid black;">  Item ID<br> <div style="font-size: 20px;font-weight:bold;"><center><%=row.getString("item_id") %>&nbsp;</center></div></td>
		  </tr>
		  <tr style="height:43px"> 
		    <td colspan="4" style="border-bottom: 2px solid black;">  LOT<br><div style="font-size: 20px;font-weight:bold;"><center><%=row.getString("lot_no") %>&nbsp;</center></div></td>
		  </tr>
		  <tr style="height:43px">
		    <td colspan="2" style="border-right: 2px solid black;border-bottom: 2px solid black;">  Customer<br><div style="font-size: 20px;font-weight:bold;"><center><%=row.getString("customer_id") %>&nbsp;</center></div></td>
		    <td colspan="2" style="border-bottom: 2px solid black;">  Supplier<br><div style="font-size: 20px;font-weight:bold;"><center><%=row.getString("supplier") %>&nbsp;</center></div></td>
		  </tr>
		  <tr style="height:43px">
		    <td colspan="2" style="border-right: 2px solid black;border-bottom: 2px solid black;">  Total Product QTY<br><div style="font-size: 20px;font-weight:bold;"><center>
		    	<% if(row.get("goods_type", 0)==2){ %>
		   			<%=(row.get("qty", 0)!=0?row.get("qty", 0):"")%>
		   		<% }else if(row.get("qty", 0)==0){%>
			   		<%=(row.get("length", 0)*row.get("width", 0)*row.get("height", 0)!=0?row.get("length", 0)*row.get("width", 0)*row.get("height", 0):"")%>
		   		<% }else{%>
			   		<%=(row.get("qty", 0)!=0?row.get("qty", 0):"")%>
		   		<% }%>
		    	&nbsp;</center></div></td>
		    <td colspan="2" style="border-bottom: 2px solid black;">  Pallet Configuration<br><div style="font-size: 20px;font-weight:bold;"><center>
		    	<% if(row.get("goods_type", 0)==2){ %>
		    		<%=(row.get("qty", 0)!=0?row.get("qty", 0):"")%>
		   		<% }else if( row.get("config_type", 0) == 2 || row.get("is_partial", 0)==1){%>
			   		<%=row.get("length", 0)*row.get("width", 0)*row.get("height", 0)%>
		   		<% }else if(row.get("is_partial", 0)==2){%>
			   		<%=(row.get("qty", 0)!=0?row.get("qty", 0):"")%>
		   		<% }else {%>
			   		<%=row.getString("length")%>&nbsp;X&nbsp;<%=row.getString("width")%>&nbsp;X&nbsp;<%=row.getString("height") %>
		   		<% }%>	
		    	&nbsp;</center></div></td>
		  </tr> 
		  <tr style="height:43px"> 
		    <td colspan="4" style="border-bottom: 2px solid black;">Staging Location<div style="font-size: 30px;font-weight:bold;"><center><%=row.getString("location") %>&nbsp;</center></div></td>
		  </tr>
		  <% if(!is_clp){ %>  
		  <tr style="height:43px">
		    <td colspan="2" style="border-right: 2px solid black;border-bottom: 2px solid black; position: relative; "> <span style="position: absolute; top: 3px;">Entry ID</span><br><div style="font-size: 20px;font-weight:bold; vertical-align: middle; "><center><%=row.getString("check_in_entry_id") %>&nbsp;</center></div></td>
		    <td colspan="2" style="border-bottom: 2px solid black; position: relative;"> <span style="position: absolute; top: 3px;">CTNR/Trailer</span><br><div style="font-size: 20px;font-weight:bold; vertical-align: middle;"><center><%=row.getString("equipment_number") %>&nbsp;</center></div></td>
		  </tr> 
		  <% }else{ %> 
		  <tr style="height:43px"> 
		    <td colspan="4" style="border-bottom: 2px solid black;">  Ship To<br><div style="font-size: 20px;font-weight:bold;"><center><%=row.get("ship_to_name", "") %>&nbsp;</center></div></td>
		  </tr>	
		  <% } %> 
		  <tr style="height:110px;text-align:center">
		    <td colspan="4"  >
			    <img src="/barbecue/barcode?data=94<%=StringUtil.formatNumber("#000000000000", row.get("container",0l)) %>&width=2&height=50&type=code128"/><span style="display:none"></span><br>
			    <div style="font-size: 30px;font-weight:bold;"><%=row.get("container",0l) %>&nbsp;</div>
		    </td> 
		  </tr>  
		</table>
	
	</div>
	<br>
<%} %>
</div>
</body>
</html>
<script>
function supportAndroidprint(){
	//获取打印机名字列表
   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
   	 //判断是否有该名字的打印机
   	var printer = "<%=printName%>";
   	var printerExist = "false";
   	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){
		return androidIsPrint(containPrinter);
	}else{
		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消
			return androidIsPrint(containPrinter);
		}
	}
}

function androidIsPrint(containPrinter){
	//alert(containPrinter);
	var printAll = $('div[name="printAll"]');
	var flag = true ;

	for(var i=0;i<printAll.length;i++){
		visionariPrinter.SET_PRINT_PAGESIZE(1,"10.2cm","15.2cm","102X152");
		 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
		    
        visionariPrinter.ADD_PRINT_TABLE(0,0,"100%","100%",$(printAll[i]).html());
        visionariPrinter.SET_PRINT_COPIES(1);
 		//visionariPrinter.PREVIEW();
 		flag = flag && visionariPrinter.PRINT();
 	 
	}
	return flag ;
}

</script>

