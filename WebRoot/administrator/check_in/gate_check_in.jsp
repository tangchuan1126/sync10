<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.CheckInChildDocumentsStatusTypeKey"%>
<%@page import="com.cwc.app.key.FileWithCheckInClassKey"%>
<%@page import="com.cwc.app.key.SpaceRelationTypeKey"%>
<%@page import="com.cwc.app.key.OccupyTypeKey"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.CheckInLiveLoadOrDropOffKey" %>
<%@page import="com.cwc.app.key.GateCheckLoadingTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsRelTypeKey" %>
<%@page import="com.cwc.app.key.CheckInMainDocumentsStatusTypeKey" %>
<%@page import="com.cwc.app.key.OrderSystemTypeKey" %>

<jsp:useBean id="gateCheckLoadingTypeKey" class="com.cwc.app.key.GateCheckLoadingTypeKey"/> 
<jsp:useBean id="checkInLiveLoadOrDropOffKey" class="com.cwc.app.key.CheckInLiveLoadOrDropOffKey"/> 
<jsp:useBean id="orderSystemTypeKey" class="com.cwc.app.key.OrderSystemTypeKey"/> 
<jsp:useBean id="checkInTractorOrTrailerTypeKey" class="com.cwc.app.key.CheckInTractorOrTrailerTypeKey"/> 
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/> 
<jsp:useBean id="fileWithCheckInClassKey" class="com.cwc.app.key.FileWithCheckInClassKey"/> 
<%
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 long ps_id=adminLoggerBean.getPs_id();   
 long adminId=adminLoggerBean.getAdid(); 
// DBRow psRow=adminMgrZwb.findAdminPs(adminId);
// String ps_name	=psRow.get("title","") ;  
 DBRow[] spots = checkInMgrZwb.findSpotArea(ps_id);
//查询所有zone
 DBRow[] allZone =checkInMgrZwb.findAllZone(ps_id);
 long mainId = StringUtil.getLong(request,"mainId");
 DBRow tractorRow=null;
 DBRow trailerRow=null;
 DBRow[] detailRow = null;
 int trailerrel_type=0;
 int trailerisLive=0;
 int tractorrel_type=0;
 int tractorisLive=0;
 DBRow doorOrSpot=null; 
 DBRow pickUpCtnrRow=null;
 DBRow resourceRow = null;
 if(mainId>0){
	 doorOrSpot = checkInMgrZwb.getResourcesByModuleId(mainId);//查询所占资源
	 tractorRow = checkInMgrZwb.selectTractorByEntryId(mainId);//车头信息    					 
	 trailerRow = checkInMgrZwb.selectTrailerByEntryId(mainId);//车尾信息 
	 if(trailerRow!=null){
		 trailerrel_type = trailerRow.get("rel_type",0);
		 trailerisLive = trailerRow.get("equipment_purpose",0);
	 }
	 if(tractorRow!=null){
		 tractorrel_type = tractorRow.get("rel_type",0);
		 tractorisLive = tractorRow.get("equipment_purpose",0);
	 }
	
	 detailRow = checkInMgrZwb.findloadingByInfoId(mainId);    //子单据数据
	 
	 if(detailRow!=null&&detailRow.length>0){
		 resourceRow = checkInMgrZwb.getSpaceResorceRelation(SpaceRelationTypeKey.Task, detailRow[0].get("dlo_detail_id", 0l));//根据子单据id从资源表查询task占用的设备
	 }
	 pickUpCtnrRow = checkInMgrZwb.selectPickUpCTNRByEntryId(mainId);
//	 if(detailRow.length==0){
//		 detailRow = checkInMgrZwb.selectLocationByEntryId(mainId);
//	 }

	 
 }
 
 String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
 String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
 String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
 String checkInFileUploadAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/checkin/checkInFileUploadAction.action";
 DBRow[] files = null ;
 if(mainId > 0l){
 	 files = checkInMgrZwb.getAllFileByFileWithIdAndFileWithType(mainId,FileWithTypeKey.OCCUPANCY_MAIN);
 }
 %>	
<head>
	 
	
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	<style type="text/css" media="all">
	@import "../js/thickbox/thickbox.css";
	</style>

	<link rel="stylesheet" type="text/css" href="../js/artDialog4.1.7/skins/aero.css" />
	<script src="../js/artDialog4.1.7/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/artDialog4.1.7/plugins/iframeTools.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
	<!-- 在线阅读 -->
	<script type="text/javascript" src="../js/office_file_online/officeFileOnlineServ.js"></script> 
	<!-- 打印 -->
	<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
	<script type="text/javascript" src="../js/print/m.js"></script>
	
	<!-- 图片上传 -->
	<script type="text/javascript" src="../js/picture/picture.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/picture/picture.css" />
	
	<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
	<script src="../js/showMessage/showMessage.js" type="text/javascript"></script>
	<style type="text/css">
		.check {
			width:100%;
			height:30px;
			font-family: Verdana;
			font-size: 14px;
		}
		
		.wrap-search-input{
			position: relative;
			display: inline-block;
		}

		.icon-search-input{
			background: url(imgs/search.png) no-repeat center center;
			position: absolute;
			cursor: pointer;
			top: 0px;
			right: 0px;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
		}
		
		.wrap-search-input input:focus+.icon-search-input{
			border-color:#66afe9;
			outline:0;
			-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6);
			box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)
		}
	</style>

<script type="text/javascript">
var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
var updateDialog = null;
var addDialog = null;
api.button([
		{
			name: 'Submit',
			callback: function () {
				searchTrailerNo(<%=mainId%>);
			//	addGateCheckInMes(<%=mainId%>);
				return false ;
			},
			focus:true,
			focus:"default"
		},
		{
			name: 'Cancel'
			
			
		}]
	);
function cancel(){$.artDialog && $.artDialog.close();}
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();
jQuery(function($){
	 
	addAutoComplete($("#company_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInDBCarrierJSONAction.action",
			"carrier","carrier");
	// minLength：设置触发 autoComplete 最小长度
	$("#company_name").autocomplete({minLength: 3});
	addAutoComplete($("#mc_dot"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInDBMcdotJSONAction.action",
			"mc_dot","mc_dot");	
	$("#mc_dot").autocomplete({minLength: 3});
	
	completeSelect($("#company_name"), autoCM);

	completeSelect($("#mc_dot"), autoMC);

// 	addAutoComplete($("#liscense_plate"),
<%-- 			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInLicensePlateJSONAction.action?flag=2", --%>
// 			"gate_liscense_plate","gate_liscense_plate");
	
	addAutoComplete($("#Both input[id='ctnr']"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInCTNRJSONAction.action?flag=1",
			"equipment_number","equipment_number");
	
	addAutoComplete($("#pickup_ctnr input[id='ctnr']"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInCTNRJSONAction.action?flag=1",
			"equipment_number","equipment_number");
	
	addAutoComplete($("#dpickup_ctnr input[id='ctnr']"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInCTNRJSONAction.action?flag=1",
			"equipment_number","equipment_number");
	//为mcdot绑定
	clickAutoComplete($("#mc_dot"));
	//为carrier绑定
	clickAutoComplete($("#company_name"));
	//为PickingUp Ctnr的 Ctnr/trailer 文本框绑定点击后触发事件
	clickAutoComplete($("#Both input[id='ctnr']"));
});
function completeSelect(jqObj, func) {
	jqObj.autocomplete({ 
		select: function (event, ui) {
			var arg = ui.item.value;
			func(arg);
		}
	});	

}
//当点击文本框时触发事件
//$("#Both input[id='ctnr']")
function clickAutoComplete(jqObj) {
	jqObj.bind('click',function() {
		var val = $.trim($(this).val());
		if(val != ""){
			jqObj.autocomplete( "search" );
		}
	});

}
$(function(){
	var trailerrel_type = <%=trailerrel_type%>;
	var trailerisLive = <%=trailerisLive%>;
	var tractorrel_type = <%=tractorrel_type%>;
	var tractorisLive = <%=tractorisLive%>;
	$("#tabs").tabs({
		select: function(event,ui){
			if(<%=mainId%>==0 ){
				$("table[name='showMes'] input").val(""); 
				$("select[id='spotArea']").val("");
				$("select[id='allzone']").val("");
				$("input[name='live']").attr("checked",false);
				$("td[id='door_td']").removeAttr("style");
				$("input[id='door']").removeAttr("disabled");
				$("select[id='allzone']").removeAttr("disabled");
				$("input[id='yc_no']").removeAttr("disabled");
				$("select[id='spotArea']").removeAttr("disabled");
				$("#pickup_ctnr").hide();
				$("#dpickup_ctnr").hide();
				$("#single_load").attr("checked",true);
				$("#single_tr").show();
				$("#multiple_tr").hide();
				$("#pickupCTNR legend").html("PickUp CTNR");
				$("#deliveryCTNR legend").html("Delivery CTNR");

			}	
		
			if(ui.index==0 && $("#search_number_delivery").val()==""){
				window.setTimeout(function(){$("#search_number_delivery").focus();},300);
		//		var ctnr = $("#gate_container_no").val();
		//		$("#search_number_delivery").val(ctnr);
			}
			if(ui.index==2){
				$("#Both select[id='spotArea']").attr("disabled","disabled");
				$("#Both input[id='door']").attr("disabled","disabled");
				$("#Both select[id='allzone']").attr("disabled","disabled");
				$("#Both input[id='yc_no']").attr("disabled","disabled");
	//			if($("#gate_container_no").val() !=''){
	//				alert("Please select Swap CTNR");
	//				$("#gate_container_no").val('');
	//			}
			}
		}});
	
		$("#gate_container_no").keyup(function(){
//			var ctnr = $("#gate_container_no").val();
//			$("#search_number_delivery").val(ctnr);
		//	if($("#tabs").tabs("option","selected")==0){
		//		$("input[name='live']").attr("checked",false);
		//		$("#pickup_ctnr").hide();
		//		$("#dpickup_ctnr").hide();
		//	}
			$("#repeat").val(0);
			
		});
		if(<%=mainId%> > 0){
			if(trailerrel_type == <%=CheckInMainDocumentsRelTypeKey.DELIVERY %>){
				 $("#tabs").tabs("select",0);  
				 $("#initNum").val($("#Delivery input[name='search_number']").val());
				 if(trailerisLive == <%=checkInLiveLoadOrDropOffKey.DROP %> &&　tractorisLive　== <%=checkInLiveLoadOrDropOffKey.PICK_UP %> ){
					 $("#dpickup_ctnr").show();
				 }
				 findZoneByTitleName('<%=detailRow!=null && detailRow.length>0?detailRow[0].getString("supplier_id"):""%>');
				 
			} 
			if(trailerrel_type==<%=CheckInMainDocumentsRelTypeKey.PICK_UP %> || tractorrel_type==<%=CheckInMainDocumentsRelTypeKey.PICK_UP %> ){
				 $("#tabs").tabs("select",1);  
				 $("#initNum").val($("#PickUp input[name='search_number']").val());
				 if(trailerisLive == <%=CheckInLiveLoadOrDropOffKey.DROP %> &&　tractorisLive　== <%=CheckInLiveLoadOrDropOffKey.PICK_UP %> ){
					 $("#pickup_ctnr").show();
				 }
				 
			} 
			if(trailerisLive == <%=CheckInLiveLoadOrDropOffKey.DROP %> || ( trailerisLive == <%=CheckInLiveLoadOrDropOffKey.DROP %> &&　tractorisLive　== <%=CheckInLiveLoadOrDropOffKey.PICK_UP %> ) ){
				$("#pickupCTNR legend").html("Drop Off CTNR");
				$("#deliveryCTNR legend").html("Drop Off CTNR");
			}
			
			if(trailerisLive !=<%=CheckInLiveLoadOrDropOffKey.DROP %> && tractorisLive　== <%=CheckInLiveLoadOrDropOffKey.PICK_UP %>){
				 $("#tabs").tabs("select",2);  
			}
			if(tractorrel_type == <%=CheckInMainDocumentsRelTypeKey.SMALL_PARCEL %>){
				 $("#tabs").tabs("select",3);  
			}
			if(tractorrel_type == <%=CheckInMainDocumentsRelTypeKey.NONE %>){
				 $("#tabs").tabs("select",4);  
			}
			if(tractorrel_type == <%=CheckInMainDocumentsRelTypeKey.VISITOR %>){
				 $("#tabs").tabs("select",5);  
			}
			$("#doorId").val($("#checkInDoorId").val());
		}
		
	 $("#gps_tracker").blur(function(){
		 if( $("#gps_tracker").val()!=""){
			 $.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindAssetByGPSNumberAction.action',
				data:'gps_tracker='+$("#gps_tracker").val(),
				dataType:'json',
				type:'post',
				beforeSend:function(request){
			    
				},
				success:function(data){
					if(data.length==0){
						findGPSTip();
						$("#mark").val(1);
					}else{
						$("#mark").val(0);
						$("#gpsId").val(data.id);
					}
					
				},
				error:function(){
					showMessage("System error!","error"); 
				}
			 });
		 }
	 });
	 $("input[type!='button']").attr("style","text-transform:uppercase");
	 $("input[id!='company_name'][id!='mc_dot'][id!='driver_name']").keyup(function(){
		 this.value=trim(this.value);
		 
	 });
	 $("input[name='search_number']").keyup(function(){
		 $("input[name='live']").attr("checked",false);
		 $("#repeat").val(0);
		 $("#pickup_ctnr").hide();
		 $("#dpickup_ctnr").hide();
	//	 var ctnr = $("#search_number_delivery").val();
	//	 $("#gate_container_no").val(ctnr);

	 });
	 loadCount();

});

// 去左右空格
strim = String.prototype.trim; 
rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g 
 
trim = strim && !strim.call("\uFEFF\xA0") ? 
        function( text ) { 
            return text == null ? 
                "" : 
                strim.call( text ); 
        } : 
        function( text ) { 
            return text == null ? 
                "" : 
                ( text + "" ).replace( rtrim, "" ); 
        } 

function getYcNo(){
	var yc="";
	if($("#tabs").tabs("option","selected")==0){
		yc=$("#Delivery input[id='yc_no']").val();
		
	}
	else if($("#tabs").tabs("option","selected")==1){
		yc=$("#PickUp input[id='yc_no']").val();
	}	
	else if($("#tabs").tabs("option","selected")==2){
		yc=$("#Both input[id='yc_no']").val();
	}
	else{
		yc=$("#None input[id='yc_no']").val();
	}		
	return yc;
}
function getDoorName(){
	var doorName="";
	if($("#tabs").tabs("option","selected")==0){
		doorName=$("#Delivery input[id='door']").val();
		
	}
	else if($("#tabs").tabs("option","selected")==1){
		doorName=$("#PickUp input[id='door']").val();
	}	
	else if($("#tabs").tabs("option","selected")==2){
		doorName=$("#Both input[id='door']").val();
	}	
	else{
		doorName=$("#None input[id='door']").val();
	}	
	return doorName;
}
function searhYc(flag){
	var yc=getYcNo();
	var ps_id=<%=ps_id%>;
	if(yc!="" ){
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindStorageYardSearchAction.action',
	  		dataType:'json',
	  		data:"yc="+yc+'&ps_id='+ps_id+"&flag="+flag+"&main_id=<%=mainId%>",
	  		success:function(data){
	  			
  					if(data.length==0 ){	  					
	  					showMessage("Spot not found or occupied!","alert");
	  					$("input[id='yc_no']").val("");
  						$("input[id='yc_no']").focus();
	  					return false;
	  				}else{
	  					$("#yc_id").val(data.yc_id);
	  					$("input[id='door']").val("");
	  					$('#checkInDoorId').val("");
	  				}
	  	
	  		}
	    });
	}

}
//zhangrui
function doorBlur(){
	var doorName = getDoorName();
		 if( doorName!=""){
			 $.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindReadyDoorSeachAction.action',
				data:'doorName='+doorName+'&ps_id='+<%=ps_id%>,
				dataType:'json',
				type:'post',
				beforeSend:function(request){
			    
				},
				success:function(data){
					if(data.length==0 && <%=mainId%> ==0 ){	
						showMessage("Door not found or occupied!","alert");
						$("input[id='door']").val("");
						$("input[id='door']").focus();
			//			alert("Door is occupied");
			//			$("input[id='door']").val("");
			//			$("input[id='door']").focus();
						
					}else{
						$("#checkInDoorId").val(data[0].sd_id);
						$("#zone_id").val(data[0].area_id);
					}
					
				},
				error:function(){
					showMessage("System error!","error"); 
				}
			 });
		 }
}
function findGPSTip(){
	$.artDialog({
	    content: 'GPS not found!do you want to add a new one?',
	    icon: 'question',
	    lock:true,
	    opacity: 0.3,
	    width: 230,
	    height: 70,
	    title:'Reminder',
	    okVal: 'Yes',
	    ok: function () {
	    	$("#mutiple").focus();
	    },
	    cancelVal: 'No',
	    cancel: function(){
	    	$("#gps_tracker").val("");
	    	$("#gps_tracker").focus();
		}
	});
}
function isNULL(value){
	if(value && value.length > 0 ){
		var fixValue = value.toUpperCase();
 		if(fixValue == "N/A" || fixValue =="NA"){
			return true ;
 	 	}else{
			return false ;
 	 	}
    }
 	return true ;
}
function search(flag,ele){
	
		$("#showMessage_delivery").html(""); //清掉提示的信息
		$("#showMessage_pickUp").html(""); //清掉提示的信息
		$("select[id='allzone']").val("");
		$("input[id='door']").val("");
		$("select[id='spotArea']").val("");
		$("input[id='yc_no']").val("");
		$("#zone_id").val(""); 
//		if($("input[name='live']:checked").val()!=1){
			$("input[id='door']").removeAttr("disabled");
			$("select[id='allzone']").removeAttr("disabled");
			$("input[id='yc_no']").removeAttr("disabled");
			$("select[id='spotArea']").removeAttr("disabled");
//		}
			$("#checkInDoorId").val('');
			$("#yc_id").val('');
		
		
		var search = "";
		var number="";
		var number_type="";
		var islive="";
		if(flag==1){
			
			number=$('#search_number_delivery').val();
			search = $("#delivery_search").serialize();
			number_type=2;
			if($("#Delivery input[name='live']:checked").val()==3){
				$("#dpickup_ctnr").show();
				$("#dpickup_ctnr input[id='door']").attr("disabled","disabled");
				$("#dpickup_ctnr select[id='allzone']").attr("disabled","disabled");
				$("#dpickup_ctnr input[id='yc_no']").attr("disabled","disabled");
				$("#dpickup_ctnr select[id='spotArea']").attr("disabled","disabled");
			}else{
				$("#pickup_ctnr").hide();
				$("#dpickup_ctnr").hide();
			}
			islive = $("#Delivery input[name='live']:checked").val();
			
		}else if(flag==2){
			number=$('#search_number_pickup').val();
			search = $("#pickup_search").serialize();
			number_type=1;
			if($("#PickUp input[name='live']:checked").val()==3){
				$("#pickup_ctnr").show();
				$("#pickup_ctnr input[id='door']").attr("disabled","disabled");
				$("#pickup_ctnr select[id='allzone']").attr("disabled","disabled");
				$("#pickup_ctnr input[id='yc_no']").attr("disabled","disabled");
				$("#pickup_ctnr select[id='spotArea']").attr("disabled","disabled");
			}else{
				$("#pickup_ctnr").hide();
				$("#dpickup_ctnr").hide();
			}
			if($("input[name='load_count']:checked").attr("id")!="single_load"){
				$("input[id='door']").attr("disabled","disabled");
				$("select[id='allzone']").attr("disabled","disabled");
			}
			
			islive = $("#PickUp input[name='live']:checked").val();
		}
		if(islive==2 || islive==3){
			if($("#gate_container_no").val()==""){
		
				showMessage("Please enter CTNR!","alert");
				$("#gate_container_no").focus();
			//	return false;
			}
			$("#pickupCTNR legend").html("Drop Off CTNR");
			$("#deliveryCTNR legend").html("Drop Off CTNR");
			
		}
		if(islive==1){
			$("#pickupCTNR legend").html("PickUp CTNR");
			$("#deliveryCTNR legend").html("Delivery CTNR");
		}
		
		var doorflag = true;
		if(number!=""){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/getLoadingAction.action',
				data:search+'&ps_id='+<%=ps_id%>+'&number_type='+number_type+'&mainId='+<%=mainId%>,
				dataType:'json',
				type:'post',
				beforeSend:function(request){
			    
				},
				success:function(data){
					$.unblockUI();       //遮罩关闭
					if(data.num==0){
						var order_type=0;
						showMessage("Record not found!","alert");
						//$("input[id='yc_no']").focus();
						$("table[name='showMes'] input").not("#yc_no").val("");  
						if($("input[id='door']").val()==""){
							$("#checkInDoorId").val("");
						}
						if(flag==1){
							order_type=<%=ModuleKey.CHECK_IN_DELIVERY_ORTHERS%>;
							$('#Delivery #title').html("Delivery :");
						}
						else if(flag==2){
							order_type=<%=ModuleKey.CHECK_IN_PICKUP_ORTHERS%>;
							$('#PickUp #title').html("Pick Up :");
						}
						$("#number_order_status").val(data.status);
						$("#order_system_type").val(data.system_type);
						$("#number_type").val(order_type);
						$("#repeat").val(0);
						$("#company_id").val("");
						$("#customer_id").val("");
						$("#account_id").val("");
						$("#customer").val("");
						$("#supplier_id").val("");
						$("#staging_area_id").val("");
						$("#order_no").val("");
						$("#po_no").val("");
						$("#freight_term").val("");
						
					}
/**					if(data.flag==1 && $("#initNum").val()!=number){
						showMessage("This is duplicate record!","alert");
						$("#repeat").val(1);
						return false;
					}*/
			        if(data.num==1){
						$("table[name='showMes'] input").not("#yc_no").val("");  	
			        	if(data.door_name>0 && islive==1 && doorflag){
							doorflag = false;
							$("select[id='allzone']").val(data.area_id);
							$("#zone_id").val(data.area_id);
							$("input[id='door']").val(data.door_name);
							$("#checkInDoorId").val(data.door_id);
							$("input[id='door']").attr("disabled","disabled");
							$("select[id='allzone']").attr("disabled","disabled");
							$("input[id='yc_no']").attr("disabled","disabled");
							$("select[id='spotArea']").attr("disabled","disabled");
							if(data.is_all_picked==2){
								showMessage("The goods not ready yet");

							}
							
						}
						
						if(data.appointmentdate){
							var apppointTime = data.appointmentdate;
							$("input[id='appointment']").val(apppointTime.substr(0,16));
							$("#appointment_time").val(apppointTime);
							var differ =dateDiff("<%=DateUtil.showLocalTime(DateUtil.NowStr(),ps_id)%>",apppointTime)/60; 
						//	alert('<%=DateUtil.showLocalTime(DateUtil.NowStr(),ps_id)%>');
						//	alert(differ);
							if(differ>=120 && islive==1 && data.system_type==<%=OrderSystemTypeKey.WMS%> ){
								doorflag = true;
						//		showMessage("Driver more than an hour late , Please Window CheckIn","alert");
								//修改到页面显示
								if(flag==2){
									$("#showMessage_pickUp").html("Driver late , Please Window CheckIn");
								}else if(flag==1){
									$("#showMessage_delivery").html("Driver late , Please Window CheckIn");
								}
								$("select[id='allzone']").val("");
								$("#zone_id").val("");
								$("input[id='door']").val("");
								$("#checkInDoorId").val("");
						//		$("input[id='door']").attr("disabled","disabled");
						//		$("select[id='allzone']").attr("disabled","disabled");
								
							}
							if(differ <= -120 && islive==1 && data.system_type==<%=OrderSystemTypeKey.WMS%>){
								doorflag = true;
							//	showMessage("Driver more than two hours earlier , Please Window CheckIn","alert");
								//修改到页面显示
								if(flag==2){
									$("#showMessage_pickUp").html("Driver earlier , Please Window CheckIn"); 
								}else if(flag==1){
									$("#showMessage_delivery").html("Driver earlier , Please Window CheckIn");
								}
								
								$("select[id='allzone']").val("");
								$("#zone_id").val("");
								$("input[id='door']").val("");
								$("#checkInDoorId").val("");
						//		$("input[id='door']").attr("disabled","disabled");
						//		$("select[id='allzone']").attr("disabled","disabled");
								
							}
						}else{
							$("input[id='appointment']").val("");
						}
						if($("#company_name").val()==""){
							$("#company_name").val(isNULL(data.carrierid)?"":data.carrierid);
						}
						
						if(flag==2){
							if(data.system_type==<%=OrderSystemTypeKey.WMS%>){
								$("#staging").val(data.stagingareaid);
								$("#staging_zone").val(data.area_name);
							}else{
								$("input[id='account_id']").val(data.accountid);
								//autoDoor();
							}
						}
						if(flag==1 || flag==3 ){
							if(data.system_type==<%=OrderSystemTypeKey.WMS%>){
								findZoneByTitleName(data.supplierid);
								
							}else{
								$("input[id='title']").val(data.customer_id);
								//autoDoor();
								
							}
							
						}
						//console.log(data);
						$("#receipt_no").val(data.receiptno);
						$("#number_order_status").val(data.status);
						$("#order_system_type").val(data.system_type);
						$("#company_id").val(data.companyid);
						$("#customer_id").val(data.customerid);
						$("#customer").val(data.customerid);
						$("#account_id").val(data.accountid);
						$("#staging_area_id").val(data.stagingareaid);
						$("#number_type").val(data.order_type);
						$("#add_checkin_number").val(data.number);
						if(data.system_type==<%=OrderSystemTypeKey.WMS%>){
							$("#supplier_id").val(data.supplierid);
						}else{
							$("#supplier_id").val(data.customerid);
							$("#ic_id").val(data.ic_id);
						}
						$("#freight_term").val(data.freightterm);
						
		//				alert(data.pono);
						var title=""
						if(data.order_type==<%=ModuleKey.CHECK_IN_PONO%>){
							$("#order_no").val(data.orderno);
							title="PO";
						}
						if(data.order_type==<%=ModuleKey.CHECK_IN_ORDER%>){
							$("#po_no").val(data.pono);
							title="ORDER";
						}
						if(data.order_type==<%=ModuleKey.CHECK_IN_LOAD%>){
							title="LOAD";
						}
						
						if(data.order_type==<%=ModuleKey.CHECK_IN_CTN%>){
							title="CTNR";
						}
						if(data.order_type==<%=ModuleKey.CHECK_IN_BOL%>){
							title="BOL";
						}
						if(flag==1){
							$('#Delivery #title').html(title+":");
						//	$('#search_number_delivery').val(data.number);
						}
						else if(flag==2){
							$('#PickUp #title').html(title+":");
							$('#search_number_pickup').val(data.number);
						}
						$("#repeat").val(0);
						
						
						
					}
			        if(data.num==-1){
						$("#muti_num").val(-1);
						showMessage("Found Multiple Data,please Check In Office","alert");
						$("#company_id").val("");
						$("#customer_id").val("");
						$("#account_id").val("");
						$("#supplier_id").val("");
						$("#staging_area_id").val("");
						$("#number_type").val("");
						$("#order_no").val("");
						$("#po_no").val("");
						$("#freight_term").val("");
						$("input[name='search_number']").val("");
			        }else{
			        	$("#muti_num").val(0);
			        }
			        clear();
				    autoDoorORSpot(flag,doorflag,ele);
					
			       
			       //console.log(flag+doorflag);
			      
					
				},
				error:function(){
					$.unblockUI();       //遮罩关闭
					showMessage("System error","error");
				}
				
			});
			
		}else{
			//
			$("#search_number_delivery").focus();
			$("#search_number_pickup").focus();
			autoDoorORSpot(flag,doorflag,ele);
		}
		

		
		
}
function autoDoor(){
	if($("#tabs").tabs("option","selected")==0){
		var val = $("#Delivery td#door_td input#door").val();
		if(val != ""){
			return;
		}
		var index = $("#Delivery #allzone").data("autoDoorTimeoutIndex");
		if(index == undefined){
			index = 1;
		}else{
			index = index+1;
		}
		var op = $("#Delivery #allzone option");
		for (var i=index; i<op.length; i++){
			$(op[i]).attr("selected","selected");
			findDoorsByZoneId(1);
			setTimeout(autoDoor,1000);
			$("#Delivery #allzone").data("autoDoorTimeoutIndex",i);
			return;
		}
		
	}
	else if($("#tabs").tabs("option","selected")==1){
		var val = $("#PickUp td#door_td input#door").val();
		if(val != ""){
			return;
		}
		var index = $("#PickUp #allzone").data("autoDoorTimeoutIndex");
		if(index == undefined){
			index = 1;
		}else{
			index = index+1;
		}
		var op = $("#PickUp #allzone option");
		for (var i=index; i<op.length; i++){
			$(op[i]).attr("selected","selected");
			findDoorsByZoneId(1);
			setTimeout(autoDoor,1000);
			$("#PickUp #allzone").data("autoDoorTimeoutIndex",i);
			return;
		}
	}	
	
	
}

function autoDoorORSpot(flag,doorflag,ele){ //flag 选中的是第几页  doorflag 是否为迟到and  live选中1（liveLoad）
			//loadSingleMultiple();
			if(doorflag){
				// 判断选择的是否是 swap ctnr
				var selectRadio = $(ele).val();
				if(selectRadio === undefined || selectRadio != 3) {
					$("input[id='yc_no']").removeAttr("disabled");
					$("select[id='spotArea']").removeAttr("disabled");
					$("input[id='door']").removeAttr("disabled");
					$("select[id='allzone']").removeAttr("disabled");
				}

				if(flag==1){
					if($("#Delivery input[name='live']:checked").val()==3|$("#Delivery input[name='live']:checked").val()==2||doorflag){
					/////////*********************************start
						$.ajax({
							url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxGetAreaAction.action',
							data:{
								ps_id:<%=ps_id%>,
								DropOffKey:$("#Delivery input[name='live']:checked").val(),
								type:flag
							},
							dataType:'json',
							type:'post',
							success:function(data){
								if(data.length>0){
									$("#Delivery #spotArea option").each(function(){
										$(this).each(function(){
											if(!($(this).val()==''))
												$(this).remove();
										});
									});
									var option = "";
									$(data).each(function(){
										option += "<option "+(this.selected)+" value='"+this.area_id+"' >"+this.area_name+"</option> ";
									});
									$("#Delivery #deliveryCTNR #spotArea").each(function(){
										$(this).append(option);
									});
									findSpotByZoneId(3);//使用精确查找
								}else{
									getYardNo();  //如果查询出来的区域都没有停车位 则直接打开选择space的窗口
								}
							},
							error:function(){
								showMessage("System error!","error"); 
							}
						});
						//////***************************************end
					}
				}else if(flag==2){
					if($("#PickUp input[name='live']:checked").val()==3||$("#PickUp input[name='live']:checked").val()==2||doorflag){
					/////////*********************************start
						$.ajax({
							url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxGetAreaAction.action',
							data:{
								ps_id:<%=ps_id%>,
								DropOffKey:$("#PickUp input[name='live']:checked").val(),
								type:flag
							},
							dataType:'json',
							type:'post',
							success:function(data){
								if(data.length>0){
									$("#PickUp #spotArea option").each(function(){
										$(this).each(function(){
											if(!($(this).val()==''))
												$(this).remove();
										});
									});
									var option = "";
									$(data).each(function(){
										option += "<option "+(this.selected)+" value='"+this.area_id+"' >"+this.area_name+"</option> ";
									});
									
									$("#PickUp #pickupCTNR #spotArea").each(function(){
										$(this).append(option);
									});
									findSpotByZoneId(3);//使用精确查找
								}else{
									getYardNo();	////如果查询出来的区域都没有停车位 则直接打开选择space的窗口
								}
							},
							error:function(){
								showMessage("System error!","error"); 
							}
						});
					//////***************************************end
				}
			}
		}
}
function dateDiff(date1, date2){
    var type1 = typeof date1, type2 = typeof date2;
    if(type1 == 'string')
    date1 = stringToTime(date1);
    else if(date1.getTime)
    date1 = date1.getTime();
    if(type2 == 'string')
    date2 = stringToTime(date2);
    else if(date2.getTime)
    date2 = date2.getTime();
    return (date1 - date2) / 1000;//结果是秒
}

//字符串转成Time(dateDiff)所需方法
function stringToTime(string){
    var f = string.split(' ', 2);
    var d = (f[0] ? f[0] : '').split('-', 3);
    var t = (f[1] ? f[1] : '').split(':', 3);
    return (new Date(
    parseInt(d[0], 10) || null,
    (parseInt(d[1], 10) || 1)-1,
    parseInt(d[2], 10) || null,
    parseInt(t[0], 10) || null,
    parseInt(t[1], 10) || null,
    parseInt(t[2], 10) || null
    )).getTime();

} 
function clear(){
	 $("#pickup_ctnr input").val("");
     $("#pickup_ctnr select").val("");
     $("#dpickup_ctnr input").val("");
     $("#dpickup_ctnr select").val("");
}
function findZoneByTitleName(title_name){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindZoneByTitleNameAction.action',
		data:'title_name='+title_name+'&ps_id='+<%=ps_id%>,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    
		},
		success:function(data){
			
			var zone="";
			for(var i=0;i<data.length;i++){
				//alert(data[i].area_name);
				if(i==0){
					zone=data[i].area_name+",";
				}
				
				if(i>0 && data[i].area_name!=data[i-1].area_name){
					zone +=data[i].area_name+",";
				}
			
			}
			zone=zone.substr(0,zone.length-1);
			$("input[id='title']").val(title_name);
			$("input[id='deliveryZone']").val(zone);
		},
		error:function(){
			showMessage("System error!","error"); 
		}
	 });
}

function getYardNo(){
	var yc_id = $("#yc_id").val();
	var spotZoneId=$("#spotZoneId").val();
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/get_yard_no.html?storageId=<%=ps_id%>'+'&yc_id='+yc_id+'&mainId='+<%=mainId%>+'&spotZoneId='+spotZoneId+'&inputId=-1'; 
	$.artDialog.open(uri , {title: "Spot",width:'600px',height:'520px', lock: true,opacity: 0.3,fixed: true});
	
}
//停车位回填
function setYardNo(yardId,yardNo,inputId,zone_id){
//	$("input[id='yc_no']").focus();
	$("input[id='yc_no']").val(yardNo);
	$("#yc_id").val(yardId);
	$("input[id='door']").val("");
	$("#checkInDoorId").val("");
	$("select[id='allzone']").val("");
	$("select[id='spotArea']").val(zone_id);
	$("#spotZoneId").val(zone_id);
	$("#spotArea").focus();
	$("select[id='spotArea']").focus();
	clear();
	
	
}


function addGateCheckInMes(main_id){
	<%-- 
	if($("#over_file_td p").attr("id")==null && <%=mainId%>==0){
		showMessage("Photo is required!","alert");
		$("#mutiple").focus();
		return  false;
	}
	if($("#over_file_td p").attr("id")==null && $("#file_tb a").length==0 && <%=mainId%> > 0){
		showMessage("Photo is required!","alert");
		$("#mutiple").focus();
		return  false;
	}
	 --%>
	 //改用新的文件上传控件，直接上传至文件服务器
	 if(!$("#show").showPicture("hasNewFile") && <%=mainId%>==0){
			showMessage("Photo is required!","alert");
			$("#mutiple").focus();
			//return  false;
		}
	if(!$("#show").showPicture("hasFile") && <%=mainId%> > 0){
		showMessage("Photo is required!","alert");
		$("#mutiple").focus();
		//return  false;
	}
	if($("#loadcount").val()=="" && $("input[name='load_count']:checked").attr("id")=="multiple_load"){
		showMessage("Please enter NO. Of LOADS!","alert");
		return  false; 
	}
	if($("input[name='live']:checked").length==0 && $("#tabs").tabs("option","selected")==0 && $("#tabs").tabs("option","selected")==1){

		showMessage("Please choice Live Load/Drop Off/Swap CTNR/PickUp CTNR!","alert");
		return false;
	}
	
	var yc_no="";
	var door ="";
	if($("#tabs").tabs("option","selected")==0){
		yc_no=$("#Delivery input[id='yc_no']").val();
		door=$("#Delivery input[id='door']").val();
	}
	else if($("#tabs").tabs("option","selected")==1){
		yc_no=$("#PickUp input[id='yc_no']").val();
		door=$("#PickUp input[id='door']").val();
	}else if($("#tabs").tabs("option","selected")==3){
		yc_no=$("#small_parcel input[id='yc_no']").val();
		door=$("#small_parcel input[id='door']").val();
	}
	
	if(yc_no=="" && door=="" && $("#tabs").tabs("option","selected")!=4 && $("#tabs").tabs("option","selected")!=2 && $("#tabs").tabs("option","selected")!=5){

		showMessage("Please enter Spot or Door!","alert");
		$("input[id='yc_no']").focus();
		return  false;
	}
	if($("input[name='live']:checked").val()==2 || $("input[name='live']:checked").val()==3){
		if($("#gate_container_no").val()==""){
			showMessage("Please enter CTNR!","alert");
			$("#gate_container_no").focus();
			return false;
		}
		
	}
	
	
	if($("#repeat").val()==1 ){
		showMessage("This is duplicate record!","alert");
		if($("#tabs").tabs("option","selected")==0){
			window.setTimeout(function(){$("#search_number_delivery").focus();},300);
		}else if($("#tabs").tabs("option","selected")==1){
			window.setTimeout(function(){$("#search_number_pickup").focus();},300);
		}
		return false;
	}else{
		$("input[id='door']").removeAttr("disabled");
		$("select[id='allzone']").removeAttr("disabled");
		if($("#gps_tracker").val()==""){
			$("#gpsId").val("");
		}
		var numberType="";
		var add_checkin_number="";
		var rel_type="";
		var mainStatusKey="";
		var islive="";
//		var doorId="";
//		var yc_id="";
		var ctnr="";
		var load_count=""
		var search_ctnr="";
		if($("#tabs").tabs("option","selected")==1){
			search_ctnr=$('#ctnr',$('#PickUp')).val();
		}else if($("#tabs").tabs("option","selected")==2){   
			search_ctnr=$('#ctnr',$('#Both')).val();
		}else if($("#tabs").tabs("option","selected")==0){
			search_ctnr=$('#ctnr',$('#Delivery')).val();
			
		}
		if($("#tabs").tabs("option","selected")==0){
			islive = $("#Delivery input[name='live']:checked").val();
			add_checkin_number = $("#delivery_search input[name='search_number']").val();
			rel_type=<%=CheckInMainDocumentsRelTypeKey.DELIVERY%>;
			mainStatuskey=<%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%>;
			ctnr=$("#Delivery input[id='ctnr']").val();
	//		numberType  = <%=GateCheckLoadingTypeKey.CTNR%>;

		}
		else if($("#tabs").tabs("option","selected")==1){
			islive=$("#PickUp input[name='live']:checked").val();
			add_checkin_number = $("#pickup_search input[name='search_number']").val();
			rel_type=<%=CheckInMainDocumentsRelTypeKey.PICK_UP%>;
			mainStatuskey=<%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%>;
			ctnr=$("#PickUp input[id='ctnr']").val();
	//		numberType  = <%=GateCheckLoadingTypeKey.LOAD%>;
			if($("input[name='load_count']:checked").attr("id")=="multiple_load"){
				load_count=$("#loadcount").val();
			}else if(add_checkin_number!="" && $("input[name='load_count']:checked").attr("id")=="single_load"){
				load_count=1;
				if($("#muti_num").val()==-1){
					load_count=0;
				}
			}
		}	
		else if($("#tabs").tabs("option","selected")==2){
			ctnr=$("#Both input[id='ctnr']").val();
			$("#yc_id").val("");
			islive=4;
			rel_type = <%=CheckInMainDocumentsRelTypeKey.CTNR %> ;
			mainStatuskey=<%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%>;
		}	
		else if($("#tabs").tabs("option","selected")==3){
		//	add_checkin_number = $("#small_parcel_no").val();
			islive =<%=CheckInLiveLoadOrDropOffKey.LIVE %>;
			rel_type=<%=CheckInMainDocumentsRelTypeKey.SMALL_PARCEL%>;
			mainStatuskey=<%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%>;
		}
		else if($("#tabs").tabs("option","selected")==4){
	//		islive=$("#None input[name='live']:checked").val();
			islive =<%=CheckInLiveLoadOrDropOffKey.LIVE %>;
			rel_type=<%=CheckInMainDocumentsRelTypeKey.NONE%>;
			mainStatuskey=<%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%>;
		}
		else{
			islive =$("#None input[name='live']:checked").val();
			rel_type=<%=CheckInMainDocumentsRelTypeKey.VISITOR %>;
			mainStatuskey=<%=CheckInMainDocumentsStatusTypeKey.UNPROCESS%>;
		}
		if(islive==2 || islive==1){
			$("input[id='ctnr']").val("");
		}
		
		$("#live").val(islive);
		if($("#muti_num").val()!=-1){
			$("#add_checkin_number").val(add_checkin_number);
		}
		$("#type").val(numberType);
		$("#rel_type").val(rel_type);
		$("#status").val(mainStatuskey);
		/* 
		if(onlineNames!="" && onlineNames!=null){
			var onlineNames = $("input[name='file_names']").val(); 
			onlineNames = onlineNames.substring(0,onlineNames.length-1);
			$("input[name='file_names']").val(onlineNames);
		}
		 */
		$("input[name='file_names']").val($("#show").showPicture("getUploadPicture"));
		var carrier_page = trim($("#company_name").val()).toUpperCase();
		var mc_dot_page = trim($("#mc_dot").val());
		var urlstring = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/gateCheckInAddAction.action';
		var dataval = "" + 'main_id=' + main_id + '&search_ctnr=' + ctnr + '&number_type=' + $("#number_type").val() + '&load_count=' + load_count + '&search_ctnr='+search_ctnr+'&';
 		if(carrier_page != undefined && carrier_page != '') {
 			if(mc_dot_page != undefined && mc_dot_page != '') {
 				$.ajax({
 			  		type : 'post',
 			  		url : '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindCompanyNameByMcDotAction.action', 
 			  		dataType : 'json',
 			  		data : "mc_dot=" + mc_dot_page + "&carrierName=" + carrier_page,
 			  		success : function(data) {
 						if(data != undefined && data != '') {
 							debugger;
 							var result = data.result;
 							var sys_carrier = '';
 							if(data.carrier == '' || data.carrier == undefined) {
 								sys_carrier = 'NA';
 							} else {
 								sys_carrier = data.carrier.toUpperCase();
 							}					
 							// carrier 没有其对应 mc_dot，提示用户是否添加一条新纪录
 							if(result == "new") {
 								getDialog("new", 1, carrier_page, sys_carrier, urlstring, dataval);
							} else if(result == "success") {	// mcdot 有对应的 companyName，且只有一条记录，且记录中的 companyName 不等于输入的 companyName;
 								if(sys_carrier != carrier_page) {
 									getDialog("update", 1, carrier_page, sys_carrier, urlstring, dataval);
 								} else {
 									dataval += $("#add_form").serialize();
									autoajax(urlstring, dataval);	
 								}
 							} else { // 获取的记录数大于 1 条，暂时未做处理
								
 							}
 						}
 			  		},
 			  		error:function(serverresponse, status){
 	    	  			$.unblockUI();
 	    	  			showMessage(errorMessage(serverresponse.responseText),"alert");
 	    	  		}
 			    });	 
				
 				$.ajax({
 			  		type : 'post',
 			  		url : '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindMcDotByCompanyNameAction.action', 
 			  		dataType : 'json',
 			  		data : "carrierName=" + carrier_page + "&mc_dot=" + mc_dot_page,
 			  		success : function(data) {
 						if(data != undefined && data != '') {
 							var result = data.result;
 							var sysmc_dot = data.mc_dot;
 								if(sysmc_dot == '' || sysmc_dot == undefined) {
 									sysmc_dot = 'NA';
 								} 					
 							// carrier 没有其对应 mc_dot，提示用户是否添加一条新纪录
 							if(result == "success") {	// carrier 有对应的 mc_dot，且只有一条记录，且记录中的 mc_dot 不等于输入的 mc_dot;
 								if(sysmc_dot != mc_dot_page) {
 									getDialog("update", 2, mc_dot_page, sysmc_dot, urlstring, dataval);
 								}
 							} else { // 获取的记录数大于 1 条，暂时未做处理
								
 							}
 						}
 			  		}
 			    });			       
 			} else {
				dataval += $("#add_form").serialize();
				autoajax(urlstring, dataval);
 			}
 		} 		
	

	}
}
function errorMessage(val)
{
	var error1 = val.split("[");
	var error2 = error1[1].split("]");	
	
	return error2[0];
}
// 点击 checkbox 后的操作，oper 进行哪种操作（1 为 add，2 为 update），flag 表示更新的是 carrier 还是 mcdot，1 为 carrier，2为 mcdot
function checkOperation(oper, flag, urlstring, dataval) {
	var system = trim($("#autoSystem").val()).toUpperCase();
 	var operator = $(":checkbox[name=checkIn]:checked").val() * 1;
	var carrier_page = trim($("#company_name").val()).toUpperCase();
	var mc_dot_page = trim($("#mc_dot").val());
 	if (operator == 1) {
 		// 如果是新增操作
 		if (oper == 1) {
			$.ajax({
		  		type : 'post',
		  		url : '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxAddCarrierInfoAction.action',
	  		dataType : 'json',
	  		data : "carrierName=" + carrier_page + "&mc_dot=" + mc_dot_page,
	  		success : function(data) {
	  			if(data != undefined && data != '') {
	  				var result = data.result;
	  				if(result == "success") {
	  					dataval += $("#add_form").serialize();
						autoajax(urlstring, dataval);					  				
	  				} else {
	  					art.dialog.tips('Failure!');
	  				}
	  			}
	  		}
		 });			
		} else if (oper == 2) { // 如果是更新操作
	 		if(flag == 1) {
	 			$.ajax({
		    	    url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxModCarrierAction.action?carrierName=' + carrier_page + '&mc_dot=' +  mc_dot_page,
		    	    dataType : 'json',
		    	    success: function (data) {
		    	    	if(data != undefined && data != '') {
		    	    		var result = data.result;
		    	    		if(result == "success") {
		    	    			$('#company_name').val(carrier_page);
		    	    			dataval += $("#add_form").serialize();
								autoajax(urlstring, dataval);	
		    	    		}
		    	    	}
		    	    },
		    	    cache: false
		    	}); 	
	 		} else if(flag == 2) {
	 			$.ajax({
		    	    url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxModMcDotAction.action?carrierName=' + carrier_page + '&mc_dot=' +  mc_dot_page,
			    	    dataType : 'json',
			    	    success: function (data) {
			    	    	if(data != undefined && data != '') {
			    	    		var result = data.result;
			    	    		if(result == "success") {
			    	    			$('#mc_dot').val(mc_dot_page);
			    	    			dataval += $("#add_form").serialize();
									autoajax(urlstring, dataval);		 
			    	    		} 
			    	    	}
			    	    },
			    	    cache: false
			    	});
		 		}			
			}

	} else if(operator == 2) {
		if (oper == 1) {
			dataval += $("#add_form").serialize();
			autoajax(urlstring, dataval);	 
		} else if (oper == 2) {
	 		if(flag == 1) {
	 			$('#company_name').val(carrier_page);
	 			dataval += $("#add_form").serialize();
				autoajax(urlstring, dataval);
	 		} else if(flag == 2) {
	 			$('#mc_dot').val(mc_dot_page);
	 			dataval += $("#add_form").serialize();
				autoajax(urlstring, dataval);
	 		}		
		}
	} else if(operator == 3) {
		if(oper == 1) {
			addDialog.close();
		} else if (oper == 2) {
			if(flag == 1) {
				$('#company_name').val(system);
				dataval += $("#add_form").serialize();
				autoajax(urlstring, dataval);	
			} else if(flag == 2) {
				$('#mc_dot').val(system);
				dataval += $("#add_form").serialize();
				autoajax(urlstring, dataval);		
			}		
		}

	}
	updateDialog && updateDialog.close();
}
// 复选框实现单选功能
function check(oper, flag, obj) {
	var checks = document.getElementsByName("checkIn");
	var urlstring = $(obj).attr("urlstring");
	var dataval = $(obj).attr("dataval");
    if(obj.checked) {
        for(var i=0; i<checks.length;i++) {
            checks[i].checked = false;
        }
        obj.checked = true;  
        checkOperation(oper, flag, urlstring, dataval);
    }
    else {
        for(var i=0;i<checks.length;i++) {
            checks[i].checked = false;
        }
    }
}

// ajax 提交表单数据

function autoajax(url,data){
	$.ajax({
		url:url,
		data:data+'&checkOutFlag='+checkOutFlag,
		dataType:'json',
		type:'post',
		async:false,
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   // 遮罩打开
			 api.button({
	                name: 'Submit',
	                disabled: true
	            });
		},
		success:function(data){
			api.button({
	                name: 'Submit',
	                disabled: false
	            });
			var entry_id=data.entry_id;
			if(data.flag==53){
		
				 showMessage("Upload file failed , please again upload!","alert");
				$.unblockUI(); 
				$.artDialog.list['carrier'].close();
				$("#over_file_td *").remove();
				return false;
			}
			$.unblockUI();
			
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>check_in/check_in_open_print.html',
				dataType:'html',
				async:'false',
				data:'main_id='+entry_id,
				success:function(html){						
					var printHtml=$('div[name="avg"]',html);
					dayin(printHtml);
					if(<%=mainId%> > 0){
						parent.location.reload();
					}else{
						window.parent.location.href ='<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/check_in_index.html';
					}
				    $.artDialog && $.artDialog.close();
				}
			});
			//$.unblockUI(); //遮罩关闭
			//parent.location.reload();        
		    //$.artDialog && $.artDialog.close();
		},
		error:function(serverresponse, status){
  			$.unblockUI();
  			api.button({
                name: 'Submit',
                disabled: false
            });
  			showMessage(errorMessage(serverresponse.responseText),"alert");
  		}
		
		
		});

}

//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    //var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file_serv/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	//if(fileNames && fileNames.length > 0 ){
	//	uri += "&file_names=" + fileNames;
	//}
	 $.artDialog.open(uri , {id:'file_up',title: 'Upload Photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   		 }});
}
//在线获取
function onlineScanner(target){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file_serv/picture_online_scanner.html?target="+target; 
	$.artDialog.open(uri , {title: 'Online Multiple',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function onlineSingleScanner(_target){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file_serv/picture_online_single_scanner.html?target="+_target; 
	$.artDialog.open(uri , {title: 'Online Single',width:'300px',height:'170px', lock: true,opacity: 0.3,fixed: true});
}
//摄像头 
function onlineScannerSingle(target){
 	var  strActionPage = '<%=ConfigBean.getStringValue("systenFolder")%>'+"action/file/FileOnLineScannerUploadAction.action";
	acquireImage(strActionPage,target);
}
var onlineNames = "";
//jquery file up 回调函数
function uploadFileCallBack(fileNames,_target){
	$("#"+_target).showPicture("appendPicture",fileNames);
}
function createA(fileName){
	 
     var id = fileName.substring(0,fileName.length-4);
    var  a = "<p id='add"+id+"' style='color:#439B89' class='new' ><a  href='javascript:void(0)' onclick='showTempPictrueOnline(&quot;"+fileName+"&quot;)' style='color:#439B89' >"+fileName+"</a>&nbsp;&nbsp;<a  href='javascript:deleteA(&quot;"+id+"&quot;)' style='color:red' >×</a></p>";
    return a ;
}

function deleteA(id){
	$("#add"+id).remove();
	var file_names = $("input[name='file_names']").val(); 
	
	 var array = file_names.split(",");
	 var lastFile = "";
	 for(var index = 0 ; index < array.length ; index++ ){
			if(id==array[index].substring(0,array[index].length-4)){
				array[index]="";
				
			}
			lastFile += array[index];
			if(index!=array.length-1 && array[index]!=""){
				lastFile+=",";
			}
	 }
	 
	 $("input[name='file_names']").val(lastFile);
}
//图片在线显示  		 
function showPictrueOnline(fileWithType,fileWithId ,currentName){
    var obj = {
  		file_with_type:fileWithType,
  		file_with_id : fileWithId,
  		current_name : currentName ,
  		cmd:"multiFile",
  		table:'file',
  		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/check_in"
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
}
//删除图片
function deleteFile(file_id){
	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:'file',file_id:file_id,folder:'check_in',pk:'file_id'},
		success:function(data){
			if(data && data.flag === "success"){
				$("#file_tr_"+file_id).remove();
			}else{
				showMessage("System error,please try later","error");
			}
			
		},
		error:function(){
			showMessage("System error,please try later","error");
		}
	});
}
function closeWindow(){
	$.artDialog.close();
}
function doorEnter(event){
	 if(event.keyCode==13){
		 door();
		 return false;
	}
}
function door(){
	var doorName=getDoorName();
	if( doorName!=""){
		 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindReadyDoorSeachAction.action',
			data:'doorName='+doorName+'&ps_id='+<%=ps_id%>,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				if(data.length==0 && <%=mainId%> ==0){
					$("input[id='door']").val("");
				
					 showMessage("Door not found or occupied!","alert");
					$("input[id='door']").focus();
		//		}else if(data.occupancy_status==1 || data.occupancy_status==2){
		//			$("input[id='door']").val("");
		//			alert("Door is occupied");
		//			$("input[id='door']").focus();
				}else{
					$("#checkInDoorId").val(data[0].sd_id);
					$("#zone_id").val(data[0].area_id);
					openDoor();
				}
				
			},
			error:function(){
				showMessage("System error!","error");
			}
		 });
	 }else{
		 openDoor();
	 }
	
}
function yardEnter(event){
	 if(event.keyCode==13){
		 yard(0);
		 return false;
	}
}
function yard(flag){
	var yc=getYcNo();
	var ps_id=<%=ps_id%>;
	if(yc!=""){
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindStorageYardSearchAction.action',
	  		dataType:'json',
	  		data:"yc="+yc+'&ps_id='+ps_id+"&flag="+flag+"&mainId="+<%=mainId%>,
	  		success:function(data){
	  			
	  					if(data.length>0 && <%=mainId%> ==0){
	  						if(data[0].yc_status==2){
	  							$("input[id='yc_no']").val("");
	  							showMessage("Spot is occupied!","alert");
		  						$("input[id='yc_no']").focus();
		  						return false;
	  						}else{
	  							$("#yc_id").val(data[0].yc_id);
	  							getYardNo();
	  						}
		  						
		  				}else if(data.length==0 ){
		  					$("input[id='yc_no']").val("");
		
		  					showMessage("Spot not found!","alert");
		  					$("input[id='yc_no']").focus();
		  					return false;
		  				}
	  	
	  		}
	    });
	}else{
		getYardNo();
	}
}


function findSpotOrDoorByCtnr(){
	var ctnr="";
	if($("#tabs").tabs("option","selected")==0){
		ctnr=$("#Delivery input[id='ctnr']").val()
	}else if($("#tabs").tabs("option","selected")==1){
		ctnr=$("#PickUp input[id='ctnr']").val()
	}else{
		ctnr=$("#Both input[id='ctnr']").val()
	}
	$("#ctnr").val();
	var entryId=$("#entryId").val();
	if(ctnr=="" && <%=mainId%>==0){
		showMessage("please enter Trailer/CTNR!","alert");
		return false;
	}else if (ctnr!="" || entryId!=""){
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindSpotOrDoorByCtnrAction.action',
	  		dataType:'json',
	  		data:"ctnr="+ctnr+"&entryId="+entryId+'&ps_id='+<%=ps_id%>,
	  		success:function(data){
	  			if(!$.isEmptyObject(data)){
	  				if($("#tabs").tabs("option","selected")==0){
	  					$("#dpickup_ctnr input[id='yc_no']").val(data[0].yc_no);
		  				$("#dpickup_ctnr select[id='spotArea']").val(data[0].spot_area_id);
		  				$("#dpickup_ctnr input[id='door']").val(data[0].doorid);
		  				$("#dpickup_ctnr select[id='allzone']").val(data[0].door_area_id);
	  					$("#dpickup_ctnr input[id='door']").attr("disabled","disabled");
						$("#dpickup_ctnr select[id='allzone']").attr("disabled","disabled");
						$("#dpickup_ctnr input[id='yc_no']").attr("disabled","disabled");
						$("#dpickup_ctnr select[id='spotArea']").attr("disabled","disabled");
	  				}else if($("#tabs").tabs("option","selected")==1){
	  					$("#pickup_ctnr input[id='yc_no']").val(data[0].yc_no);
		  				$("#pickup_ctnr select[id='spotArea']").val(data[0].spot_area_id);
		  				$("#pickup_ctnr input[id='door']").val(data[0].doorid);
		  				$("#pickup_ctnr select[id='allzone']").val(data[0].door_area_id);
	  					$("#pickup_ctnr input[id='door']").attr("disabled","disabled");
						$("#pickup_ctnr select[id='allzone']").attr("disabled","disabled");
						$("#pickup_ctnr input[id='yc_no']").attr("disabled","disabled");
						$("#pickup_ctnr select[id='spotArea']").attr("disabled","disabled");
	  				}else{
	  					$("#Both input[id='yc_no']").val(data[0].yc_no);
		  				$("#Both select[id='spotArea']").val(data[0].spot_area_id);
		  				$("#Both input[id='door']").val(data[0].doorid);
		  				$("#Both select[id='allzone']").val(data[0].door_area_id);
	  					$("#Both input[id='door']").attr("disabled","disabled");
						$("#Both select[id='allzone']").attr("disabled","disabled");
						$("#Both input[id='yc_no']").attr("disabled","disabled");
						$("#Both select[id='spotArea']").attr("disabled","disabled");
	  				}
  					
	  					$.artDialog({
		  				    content:"<tr><td align='right' width='110'><b>Release Code</b>:</td><td> "+data[0].check_in_entry_id+"</td></tr></br>"+
		  				    "<tr><td align='right' width='110'><b>License Plate</b>:</td><td> "+data[0].gate_liscense_plate+"</td></tr></br>"+
		  				    "<tr><td align='right' width='110'><b>Driver License</b>:</td><td> "+data[0].gate_driver_liscense+"</td></tr>",
		  				    lock:true,
		  				    width: 300,
		  				    height: 80,
		  				    title:'Reminder',
		  				    okVal: 'close',
		  				    ok: function () {
		  				    },
		  				    
		  				});
	  				
	  			}else{
	  			//clear();
	  				showMessage("not found!","error");
	  				$("#ctnr,#pickup_ctnr,#Both input[id='yc_no']").val("");
	  				$("#ctnr,#pickup_ctnr,#Both select[id='spotArea']").val("");
	  				$("#ctnr,#pickup_ctnr,#Both input[id='door']").val("");
	  				$("#ctnr,#pickup_ctnr,#Both select[id='allzone']").val("");
	  			}
	  					
	  	
	  		}
	    });
	}
}
function loadSingleMultiple(){
/////////*********************************start
	$("table[name='showMes'] input").val("");  
	$("input[name=live]").removeAttr("checked");
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxGetAreaAction.action',
		data:{
			ps_id:<%=ps_id%>,
			DropOffKey:0,
			type:0
		},
		dataType:'json',
		type:'post',
		success:function(data){
			$("#PickUp #spotArea option").each(function(){
				$(this).each(function(){
					if(!($(this).val()==''))
						$(this).remove();
				});
			});
			var option = "";
			$(data).each(function(){
				option += "<option "+(this.selected)+" value='"+this.area_id+"' >"+this.area_name+"</option> ";
			});
			
			$("#PickUp #pickupCTNR #spotArea").each(function(){
				$(this).append(option);
			});
			
			findSpotByZoneId(3);
		},
		error:function(){
			showMessage("System error!","error"); 
		}
	});
//////***************************************end
loadCount();
}
function loadCount(){
	if($("#tabs").tabs("option","selected")==1){
		if($("input[name='load_count']:checked").attr("id")=="single_load"){
			$("#single_tr").show();
			$("#multiple_tr").hide();
			$("input[id='door']").removeAttr("disabled");
			$("select[id='allzone']").removeAttr("disabled");
			if(<%=mainId%>==0){
				$("input[id='door']").val("");
				$("select[id='allzone']").val("");
			}
			$("#loadcount").val("");
		}else{
			$("#single_tr").hide();
			$("#multiple_tr").show();
			$("input[id='door']").attr("disabled","disabled");
			$("select[id='allzone']").attr("disabled","disabled");
			$("input[id='door']").val("");
			$("select[id='allzone']").val("");
			$("#search_number_pickup").val("");
		}
	}
	
}

// 当前输入值和数据库对应数据有差异时的弹出层
function getDialog(dialog, flag, inputVal, system, urlstring, dataval) {
	debugger;
	var text = '';
	if(flag == 1) {
		text = 'Carrier';
		
	} else if(flag == 2) {
 		text = 'MC/DOT';
	};
	
	$("#autoSystem").val(system);
	if(dialog == "update") {
		updateDialog = $.artDialog({
			id:'carrier',
			width :  '700px',
			height :  '40%',
		    id : 'modify',
		    title : 'confirm',
		    lock : true,
		    opacity : 0.3,
		    fixed : true,
		    content : "<div class='check'><input id='checkIn1' name='checkIn' dataval='"+dataval+"' urlstring='"+urlstring+"' type='checkbox' onclick='check(" + 2 + ", " + flag + ", this)" + "' value='1' /><label id='checkIn1_label' for='checkIn1'>Using and updated " + text + ": <strong>"+ inputVal +"</strong></label></div>"
		    		+ "<div class='check'><input id='checkIn2' name='checkIn' dataval='"+dataval+"' urlstring='"+urlstring+"' type='checkbox' onclick='check(" + 2 + ", " + flag + ", this)" + "' value='2' /><label id='checkIn2_label' for='checkIn2'>Using but not update " + text + ": <strong>"+ inputVal +"</strong></label></div>"
			  		+ "<div class='check'><input id='checkIn3' name='checkIn' dataval='"+dataval+"' urlstring='"+urlstring+"' type='checkbox' onclick='check(" + 2 + ", " + flag + ", this)" + "' value='3' /><label id='checkIn3_label'  for='checkIn3'>System " + text + ": <strong>"+ system + "</strong></label></div>"
		});
		updateDialog.position(100, 180);
	} else if(dialog == "new") {
		addDialog = $.artDialog({
			id:'carrier',
		    title: 'AddRecord',
		    content:  "<div class='check'><input id='checkInNew1' name='checkIn' dataval='"+dataval+"' urlstring='"+urlstring+"' type='checkbox' onclick='check(" + 1 + ", " + flag + ", this)" + "' value='1' /><label id='checkInNew1_label' for='checkInNew1'>Carrier and Mc_dot does not exist, add this record!</label></div>"
		    		+ "<div class='check'><input id='checkInNew2' name='checkIn' dataval='"+dataval+"' urlstring='"+urlstring+"' type='checkbox' onclick='check(" + 1 + ", " + flag + ", this)" + "' value='2' /><label id='checkInNew2_label' for='checkInNew2'>For the temporary use, but don't add this record!</label></div>"
		    		+ "<div class='check'><input id='checkInNew3' name='checkIn' dataval='"+dataval+"' urlstring='"+urlstring+"' type='checkbox' onclick='check(" + 1 + ", " + flag + ", this)" + "' value='3' /><label id='checkInNew3_label'  for='checkInNew3'>Cancel</strong></label></div>",
		    width : '40em',
		    height : '40%',
	    	lock : true,
		    opacity : 0.3,
		    fixed : true,
		});
	}
}

// 输入 carrier 后，如果存在 mc/dot，则为其赋值
function autoCM(r) {
	var carrier_page;
	if(r !== undefined && typeof r !== "object") {
		carrier_page = r;
	} else {
		carrier_page = trim($("#company_name").val());
	}
	var mc_dot_page = trim($("#mc_dot").val());
	
	if(carrier_page != undefined && carrier_page != '') {
		if(mc_dot_page != undefined) {
			$.ajax({
		  		type : 'post',
		  		url : '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindMcDotByCompanyNameAction.action',
		  		dataType : 'json',
		  		data : "carrierName=" + carrier_page,
		  		success : function(data) {
		  			//$("#mc_dot").val('');
					if(data != null && data != undefined && data != '') {
						if(data.mc_dot != undefined) {
		 					$("#mc_dot").val(data.mc_dot);
						}
	 				}
		  		}
		    });
		}
	}
}

//提供回车事件支持
function autoCMEnter(field, event) {
	var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	
	if(keyCode == 13) {
		var i;
		var arr = $('input');
		for (i = 0; i < arr.length; i++)
			if (field == arr[i]) 
				break;
		i = (i + 1) % arr.length;
		arr[i].focus();
	}
	

}

// 输入 carrier 后，如果存在 mc/dot，则为其赋值
function autoMC(r) {
	var carrier_page = trim($("#company_name").val());

	var mc_dot_page;
	if(r !== undefined && typeof r !== "object") {
		mc_dot_page = r;
	} else {
		mc_dot_page = trim($("#mc_dot").val());
	}

	if(mc_dot_page != undefined && mc_dot_page != '') {
		if(carrier_page != undefined) {
			$.ajax({
		  		type : 'post',
		  		url : '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindCompanyNameByMcDotAction.action',
		  		dataType : 'json',
		  		data : "mc_dot=" + mc_dot_page,
		  		success : function(data) {
		  			//$("#company_name").val('');
					if(data != null && data != undefined && data != '') {
						if(data.carrier != undefined) {
		 					$("#company_name").val(data.carrier.toUpperCase());
						}
	 				}
		  		}
		    });
		}
	}
}

//提供回车事件支持
function autoMCEnter(field, event) {
	var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	
	if(keyCode == 13) {
		var i;
		var arr = $('input');
		for (i = 0; i < arr.length; i++)
			if (field == arr[i]) 
				break;
		i = (i + 1) % arr.length;
		arr[i].focus();
	}
	

}

// 输入 licenseNo 后，如果存在 driver name，则为其赋值
function autoDriverName(r) {
	var licenseNo = trim(r.value).toUpperCase();
	AjaxFindNameByLicenseAction(licenseNo);

}

// 根据传入的 licenseNo，如果符合条件自动填写对应的 driver name
function AjaxFindNameByLicenseAction(licenseNo){
	if(licenseNo != undefined && licenseNo != '') {
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindNameByLicenseAction.action',
	  		dataType:'json',
	  		data:"licenseNo=" + licenseNo,
	  		success:function(data){
				if(data != null && data != '' && data != undefined){
					if (data.gate_driver_name != undefined) {
						var name = data.gate_driver_name.toUpperCase();
						$("#driver_name").val(name);
					}
 				}
	  		}
	    });
	}
}
// 页面执行完后加载的函数
function afterLoad() {
	$("#company_name").width("200px");
	$("#mc_dot").width("200px");
	$("#driver_liscense").width("200px");
	$("#driver_name").width("200px");
	$("#gps_tracker").width("200px");
	$("#in_seal").width("200px");
	$("#liscense_plate").width("200px");
	$("#gate_container_no").width("200px");	
	
	loadPhotosFromFileserv();
}
//得到字母对应的数字
function changechar(str)
{
   
    if ((str=="a")||(str=="A"))
    return 10;
    else if ((str=="b")||(str=="B"))
    	return 12;
    else if ((str=="c")||(str=="C"))
    	return 13;
    else if ((str=="d")||(str=="D"))
    	return 14;
    else if ((str=="e")||(str=="E"))
    	return 15;
    else if ((str=="f")||(str=="F"))
    	return 16;
    else if ((str=="g")||(str=="G"))
    	return 17;
    else if ((str=="h")||(str=="H"))
    	return 18;
    else if ((str=="i")||(str=="I"))
    	return 19;
    else if ((str=="j")||(str=="J"))
    	return 20;
    else if ((str=="k")||(str=="K"))
    	return 21;
    else if ((str=="l")||(str=="L"))
    	return 23;
    else if ((str=="m")||(str=="M"))
    	return 24;
    else if ((str=="n")||(str=="N"))
    	return 25;
    else if ((str=="o")||(str=="O"))
    	return 26;
    else if ((str=="p")||(str=="P"))
    	return 27;
    else if ((str=="q")||(str=="Q"))
    	return 28;
    else if ((str=="r")||(str=="R"))
    	return 29;
    else if ((str=="s")||(str=="S"))
    	return 30;
    else if ((str=="t")||(str=="T"))
    	return 31;
    else if ((str=="u")||(str=="U"))
    	return 32;
    else if ((str=="v")||(str=="V"))
    	return 34;
    else if ((str=="w")||(str=="W"))
    	return 35;
    else if ((str=="x")||(str=="X"))
    	return 36;
    else if ((str=="y")||(str=="Y"))
    	return 37;
    else if ((str=="z")||(str=="Z"))
    	return 38; 
    else 
    	return -1000;
}
//验证
function getCtnr(ctnr){
	var exp=/[A-Za-z]{4}\d{7}$/g;
	var num = new Array(10)
    for (i=0;i<11;i++)
    {
        num[i]=0;
    }
    if (exp.test(ctnr))
    {
	    //??????ͷ
	    num[0]=changechar(ctnr.substr(0,1));
	    num[1]=changechar(ctnr.substr(1,1));
	    num[2]=changechar(ctnr.substr(2,1));
	    num[3]=changechar(ctnr.substr(3,1));
	
	    //???к?
	    num[4]=ctnr.substr(4,1);
	    num[5]=ctnr.substr(5,1);
	    num[6]=ctnr.substr(6,1);
	    num[7]=ctnr.substr(7,1);
	    num[8]=ctnr.substr(8,1);
	    num[9]=ctnr.substr(9,1);
	    //У??????
	    num[10]=ctnr.substr(10,1);
		var sum=num[0]+num[1]*2+num[2]*4+num[3]*8+num[4]*16+num[5]*32+num[6]*64+num[7]*128+num[8]*256+num[9]*512;
		var result=sum%11;
		result=result%10
		//document.write("??װ??????Ϊ??" + test + "<br><br>")
		if (result!= num[10]){
			return "CTNR input is wrong,The last of the CTNR is <span style=\"color:red;font-weight: bold;\">"+result+"</span>";
		}else{
			return "success";
		}
	}
}
//查询忘记checkout的设备
var checkOutFlag = 0;
function searchTrailerNo(dlo_id,verifyCtnrFlag){
	
	var gate_container_no = $("#gate_container_no").val();
	var liscense_plate = $("#liscense_plate").val();
	var ps_id = <%=ps_id%>;
	if($("#liscense_plate").val()==""){
		showMessage("Please enter License Plate!","alert");
		$("#liscense_plate").focus();
		return  false;
	}
	if($("#company_name").val()==""){
		showMessage("Please enter Carrier!","alert");
		$("#company_name").focus();
		return  false;
	}
	
	if($("#driver_liscense").val()==""){
		showMessage("Please enter Driver License!","alert");
		$("#driver_liscense").focus();
		return  false;
	}
	if($("#driver_name").val()==""){
		showMessage("Please enter Driver Name!","alert");
		$("#driver_name").focus();
		return  false;
	}
	<%-- 
	if($("#over_file_td p").attr("id")==null && <%=mainId%>==0){
		showMessage("Photo is required!","alert");
		$("#mutiple").focus();
		return  false;
	}
	if($("#over_file_td p").attr("id")==null && $("#file_tb a").length==0 && <%=mainId%> > 0){
		showMessage("Photo is required!","alert");
		$("#mutiple").focus();
		return  false;
	}
	 --%>
	//改用新的文件上传控件，直接上传至文件服务器
	 if(!$("#show").showPicture("hasNewFile") && <%=mainId%>==0){
			showMessage("Photo is required!","alert");
			$("#mutiple").focus();
			//return  false;
		}
	if(!$("#show").showPicture("hasFile") && <%=mainId%> > 0){
		showMessage("Photo is required!","alert");
		$("#mutiple").focus();
		//return  false;
	}
	if($("#loadcount").val()=="" && $("input[name='load_count']:checked").attr("id")=="multiple_load"){
		showMessage("Please enter NO. Of LOADS!","alert");
		return  false; 
	}
	if($("input[name='live']:checked").length==0 && $("#tabs").tabs("option","selected")==0 && $("#tabs").tabs("option","selected")==1){

		showMessage("Please choice Live Load/Drop Off/Swap CTNR/PickUp CTNR!","alert");
		return false;
	}
	
	
	var yc_no="";
	var door ="";
	if($("#tabs").tabs("option","selected")==0){
		yc_no=$("#Delivery input[id='yc_no']").val();
		door=$("#Delivery input[id='door']").val();
	}
	else if($("#tabs").tabs("option","selected")==1){
		yc_no=$("#PickUp input[id='yc_no']").val();
		door=$("#PickUp input[id='door']").val();
	}else if($("#tabs").tabs("option","selected")==3){
		yc_no=$("#small_parcel input[id='yc_no']").val();
		door=$("#small_parcel input[id='door']").val();
	}
	
	if(yc_no=="" && door=="" && $("#tabs").tabs("option","selected")!=4 && $("#tabs").tabs("option","selected")!=2 && $("#tabs").tabs("option","selected")!=5){

		showMessage("Please enter Spot or Door!","alert");
		$("input[id='yc_no']").focus();
		return  false;
	}
	if($("input[name='live']:checked").val()==2 || $("input[name='live']:checked").val()==3){
		if($("#gate_container_no").val()==""){
			showMessage("Please enter CTNR!","alert");
			$("#gate_container_no").focus();
			return false;
		}
		
	}
	if($("#repeat").val()==1 ){
		showMessage("This is duplicate record!","alert");
		if($("#tabs").tabs("option","selected")==0){
			window.setTimeout(function(){$("#search_number_delivery").focus();},300);
		}else if($("#tabs").tabs("option","selected")==1){
			window.setTimeout(function(){$("#search_number_pickup").focus();},300);
		}
		return false;
	}
	
	//验证海运集装箱
	var exp=/[A-Za-z]{4}\d{7}$/g;
	if(exp.test(gate_container_no) && verifyCtnrFlag!=1){
		var result = getCtnr(gate_container_no);
		if(result!='success'){
			var html = result+"<br/><br/>Do you want to continue?";
			var showInfo = $.artDialog({content:html,icon:"question",title: 'Info', lock: true,opacity: 0.3,fixed: true,
					button:[
						{
							name:"Gate Check In",
							callback:function(){
								searchTrailerNo(dlo_id,1);
							},
							focus:true
						},{
							name:"Cancel",
							callback:function(){
								showInfo.close();
							}
						}
					]
				});
			return false;
		}
	}
	if((gate_container_no!=''||liscense_plate!='')&&(gate_container_no!='<%=trailerRow!=null ? trailerRow.getString("equipment_number"):"" %>' || liscense_plate!='<%=tractorRow!=null ? tractorRow.getString("equipment_number"):"" %>')){
		//
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/checkOutEquipmentAction.action',
			data:{dlo_id:dlo_id,liscense_plate:liscense_plate,gate_container_no:gate_container_no,ps_id:ps_id},
			dataType:'json',
			type:'post',
			success:function(data){
				if(data!=null && data.length>0){
					
					var html_msg = '<table width="500px">';
					html_msg += '<tr width="500px"><td colspan="5">Make sure the under equipment left and add a new one ?</td></tr>';
					html_msg += '<tr height="10px"><td colspan="5"></td></tr>';
					for(var i=0; i<data.length; i++)
					{
						var resources_name = data[i].resources_type_name!=undefined?'at '+data[i].resources_type_name:'';
						var resources_id = data[i].resources_id!=undefined?data[i].resources_id:' ';
						resources_name = resources_name!=''?resources_name+': ':''; 
						html_msg += '<tr width="500px">'
										+'<td width="55px" align="left">'+data[i].equipment_type_name+" :</td>"
										+'<td width="120px" align="left"> '+data[i].equipment_number+"</td>"
										+'<td width="57px" align="left"> '+resources_name+"</td>"
										+'<td width="80px" align="left"> '+resources_id+"</td>"
										+'<td width="140px" align="left"> come with : E'+data[i].check_in_entry_id+"</td>"
									+"</tr>";
					}
					html_msg += "</table>";
					$.artDialog({
					    content: $(html_msg).html(),
					    icon: 'warning',
					    width: 550,
					    height: 70,
					    title:'',
					    okVal: 'Yes',
					    lock: true,
					    opacity: 0.3,
					    fixed: true,
					    ok: function () {
					    	checkOutFlag = 1;
					    	$(this).artDialog("close");
					    	addGateCheckInMes(<%=mainId%>);
					    },
					    cancelVal: 'No',
					    cancel:function(){
					    	checkOutFlag = 0;
					    	$(this).artDialog("close");
					    	addGateCheckInMes(<%=mainId%>);
					    }
					});	  
				}else{
					    	addGateCheckInMes(<%=mainId%>);
				}
			},
			error:function(){
				alert("System error");
			}
			
		});
	}else{
		addGateCheckInMes(<%=mainId%>);
	}
}
function selectCommon(jqObj) {
	$(jqObj).autocomplete({ 
		select: function (event, ui) {
			var arg = ui.item.value;
			$(jqObj).val(arg);
			findSpotOrDoorByCtnr();
		}
	});	
}
function checkDropWithParking(evt){
	var ctnr = $("#gate_container_no").val();
	if(ctnr == ""){
		showMessage("Please enter  CTNR!","alert");
		$(evt).attr("checked",false);
		$("#gate_container_no").focus();
		return ;
	}
	var checked = $(evt).attr("checked");
	if(checked){
		$("#add_form #live").val($(evt).val()); 
	}
	else{
		$("#add_form #live").val(""); 
	}
}
function loadPhotosFromFileserv(){
	$("#show").showPicture({
		base_path:'<%=ConfigBean.getStringValue("systenFolder")%>',
		limitSize:8,
		file_with_id:'<%=mainId%>',
		file_with_type:'<%=FileWithTypeKey.OCCUPANCY_MAIN %>'
	});
}
</script>
</head>
<body onload="afterLoad()" style="font-family:Verdana, Geneva, sans-serif;">
	<input type="hidden" id="autoSystem" />
    <form name="add_form" id="add_form" >
	 <table width="100%" height="30%" border="0" cellpadding="0" cellspacing="0">
	 <tr>
	    <td colspan="2" align="left" valign="top">	
			<fieldset style="border:2px #cccccc solid;padding:5px;margin:5px;width:95%;">
			<legend style="font-size:14px;font-weight:normal;color:#999999;">
		Gate Check In
			</legend>			
	
	  <table width="98%" border="0" cellspacing="5" cellpadding="2">
	   <tr>
			<td width="143px" align="right"><img width="60px" height="22px" src="./imgs/car_head1.jpg">  LP</td>
			<td width="285px;">
			   <input name="liscense_plate" id="liscense_plate" value='<%=tractorRow!=null ? tractorRow.getString("equipment_number"):"" %>' tabindex="1"/> *
			</td>
			<td align="right" style="width: 93px;">
			<img width="40px" height="40px" src="./imgs/container_stereoscopic_2.png"><div style="float:right;padding-top:17px"> CTNR</div></td>
			<td width="295px;">
			<!-- 	<input name="gate_container_no"    id="gate_container_no" value='<%=trailerRow!=null ? trailerRow.getString("equipment_number"):"" %>'/> -->
				
				<input name="gate_container_no"  id="gate_container_no" value='<%=trailerRow!=null ? trailerRow.getString("equipment_number"):"" %>' tabindex="2"/>
					
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<fieldset style="border:2px #cccccc solid;padding:5px;margin:5px 5px 5px 90px;width:700px;">
					<legend style="font-size:14px;font-weight:normal;color:#999999;">
						Driver Info
					</legend>	
					<table width="700px" style="padding-left:0px">
						<tbody><tr>
							<td align="right">MC/DOT</td>
							<td>
							   <input value="<%=tractorRow!=null ? tractorRow.getString("mc_dot"):"" %>" id="mc_dot" name="mc_dot" onBlur="autoMC();" onkeypress="autoMCEnter(this, event)"  tabindex="3"/>
							</td>
							<td align="right"> Driver License</td>
							<td>
							   <input value="<%=tractorRow!=null ? tractorRow.getString("gate_driver_liscense"):"" %>" id="driver_liscense" name="driver_liscense" onBlur="autoDriverName(this);"  tabindex="4"/> *
							</td>
						</tr>
						<tr></tr>
						<tr height="20px">
						<td align="right"> Carrier</td>
							<td width="295px"  >
							   <input value="<%=tractorRow!=null ? tractorRow.getString("company_name"):"" %>" id="company_name" name="company_name" onBlur="autoCM();" onkeypress="autoCMEnter(this, event)"  tabindex="5"/> *
							</td>
						    
							<td align="right"> Driver Name</td>
							<td>
								<input  id="driver_name" name="driver_name" value="<%=tractorRow!=null ? tractorRow.getString("gate_driver_name"):"" %>" tabindex="6"/> *
						    </td>
						</tr>
						<tr></tr>
						<tr height="20px">
							<td align="right"> GPS</td>
							<td>
								<input value="<%=tractorRow!=null ? tractorRow.getString("imei"):"" %>" id="gps_tracker" name="gps_tracker"  tabindex="-1"/>
						    </td>
						    <td align="right"> Delivery Seal</td>
							<td>
								<input value="<%=trailerRow!=null ? trailerRow.getString("seal_delivery"):"" %>" id="in_seal" name="in_seal" tabindex="7"/>
						    </td>
						</tr>
					</tbody></table>
				</fieldset>
			</td>
		</tr>
		<tr></tr>
		
		<tr>
			<input type="hidden" name="add_ps_id" id="add_ps_id" value="<%=ps_id%>"/>	
			<input type="hidden" name="status" id="status" />
			<input type="hidden" name="type" id="type" />
			<input type="hidden" name="rel_type" id="rel_type" />
			<input type="hidden" name="add_checkin_number" id="add_checkin_number" />
			<input type="hidden" name="mark" id="mark" />
			<input type="hidden" name="gpsId" id="gpsId" />
			<input type="hidden" name="appointment_time" id="appointment_time" value="<%=trailerRow!=null&& !trailerRow.getString("appointment_time").equals("") ?DateUtil.showLocalTime(trailerRow.getString("appointment_time"), ps_id):"" %>"/>
			<input type="hidden" name="live" id="live" value=""/>
			<input type="hidden" name="yc_id" id="yc_id" value="<%= (resourceRow!=null && resourceRow.get("resources_type",1)==OccupyTypeKey.SPOT)?resourceRow.get("resources_id",0l):""%>"/>
			<input type="hidden" name="checkInDoorId"  id="checkInDoorId" value="<%= (resourceRow!=null && resourceRow.get("resources_type",1)==OccupyTypeKey.DOOR)?resourceRow.get("resources_id",0l):""%>"/>
			<input type="hidden" name="zone_id" id="zone_id" />
			<input type="hidden" name="spotZoneId" id="spotZoneId" />
			<input type="hidden" name="repeat" id="repeat" />
			<input type="hidden" name="initNum" id="initNum" />
			<input type="hidden" name="number_type" id="number_type" value="<%=detailRow!=null && detailRow.length>0?detailRow[0].get("number_type",0):""%>"/>
			<input type="hidden" name="company_id" id="company_id" value="<%=detailRow!=null && detailRow.length>0?detailRow[0].get("company_id",""):""%>"/>
			<input type="hidden" name="customer_id" id="customer_id" value="<%=detailRow!=null && detailRow.length>0?detailRow[0].get("customer_id",""):""%>"/>
			<input type="hidden" name="account_id" id="account_id" value="<%=detailRow!=null && detailRow.length>0?detailRow[0].get("account_id",""):""%>"/>
			<input type="hidden" name="supplier_id" id="supplier_id" value="<%=detailRow!=null && detailRow.length>0?detailRow[0].get("supplier_id",""):""%>"/>
			<input type="hidden" name="staging_area_id" id="staging_area_id" value="<%=detailRow!=null && detailRow.length>0?detailRow[0].get("staging_area_id",""):""%>"/>
			<input type="hidden" name="ic_id" id="ic_id" value="<%=detailRow!=null && detailRow.length>0?detailRow[0].get("ic_id",0l):""%>"/>
			<input type="hidden" name="muti_num" id="muti_num" />
			<input type="hidden" name="po_no" id="po_no" value="<%=detailRow!=null && detailRow.length>0?detailRow[0].get("po_no",""):""%>"/>
			<input type="hidden" name="order_no" id="order_no" value="<%=detailRow!=null && detailRow.length>0?detailRow[0].get("order_no",""):""%>"/>
			<input type="hidden" name="freight_term" id="freight_term" value="<%=detailRow!=null && detailRow.length>0?detailRow[0].get("freight_term",""):""%>"/>
			<input type="hidden" name="number_order_status" id="number_order_status" />
			<input type="hidden" name="order_system_type" id="order_system_type" />	
			<input type="hidden" name="receipt_no" id="receipt_no" value='<%=detailRow!=null && detailRow.length>0?detailRow[0].get("receipt_no",""):""%>'/>			
		</tr>
		
	  </table>
</fieldset>
  </td>
	</tr>
	<tr>
	    <td colspan="2" align="left" valign="top">
		    <fieldset style="border:2px #cccccc solid;padding:15px;margin:5px;width:93%; height:120px; overflow:auto">
				<legend style="font-size:14px;font-weight:normal;color:#999999;">
						Photo
				</legend>
				<div id="show"></div>
		    	<table id="file_tb">
		    	
					<tr>
						<input type="hidden" name="backurl" id="backurl" value="" />
		 				<input type="hidden" name="file_with_type" value="<%=FileWithTypeKey.OCCUPANCY_MAIN %>" />
		 				<input type="hidden" name="sn" id="sn" value="gate_check_in"/>
					 	<!-- <input type="hidden" name="file_names" id="file_names"/> 新上传控件有处理，不需要这个 -->
					 	<input type="hidden" name="path" value="check_in"/>
					 	<input type="hidden" name="dlo_id" value="<%=mainId%>"/>
					 	<input type="hidden" name="file_with_class" value="<%=FileWithCheckInClassKey.PhotoGateCheckIn%>"/>
					 	
					 	<td></td>
					 	<td id="over_file_td"></td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
</table>
<br/>
</form>	
	<table width="98%" height="20%">
	  <tr width="100%" height="20%" valign="top">
		<td width="100%" height="20%" valign="top">
			<div class="demo" align="center" style="height: 100%">
	  		 	<div id="tabs" style="width: 98%;height: 100%">
					<ul>
						<li><a href="#Delivery"><b>Delivery</b></a></li>
						<li><a href="#PickUp"><b>Pick Up</b></a></li>
						<li><a href="#Both"><b>Picking Up CTNR</b></a></li>
						<li><a href="#small_parcel"><b>Small Parcel</b></a></li>
						<li><a href="#unknown"><b>Unknown</b></a></li>
						<li><a href="#None"><b>Visitor / Parking </b></a></li>
					</ul>
				<div id="Delivery">
				<%
							boolean flag = detailRow!=null && detailRow.length>0 ;
							DBRow mainRow = null;
							if(trailerRow!=null){
								mainRow = trailerRow ;
							}else{
								mainRow = tractorRow ;
							}
				%>
				  <form name="delivery_search" id="delivery_search">
					 <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0">
			            <tr>
			            	<td width="350px" >
			            	<%
			            			if(flag &&  mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY){
			            	%>
			            	<span  id="title"><%=moduleKey.getModuleName(detailRow[0].get("number_type",0)) %></span>
			            	<%
			            			}else{
            				%>
            				<span  id="title">CTNR/BOL</span>
			            	<%			
			            			}
			            	%>
								
								<input name="search_number" tabindex="11" id="search_number_delivery"  value="<%= flag && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY ? detailRow[0].getString("number"):"" %>" />
								<span style="color:red" id="showMessage_delivery"></span>
			                 </td>
			            </tr>
			            <tr>
			                <td>
				            	<input type="radio"   tabindex="12" id="live" name="live" onclick="search('1')" 
					                 	<%=mainRow !=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY && mainRow.get("equipment_purpose", 0) ==CheckInLiveLoadOrDropOffKey.LIVE ?"checked":""%>
								    	value="<%=CheckInLiveLoadOrDropOffKey.LIVE%>"/> 
								    <b>	<%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(CheckInLiveLoadOrDropOffKey.LIVE) %></b>
								<input type="radio" id="drop" name="live"  tabindex="12"  onclick="search('1')" 
								    	<%=trailerRow!=null && tractorRow!=null && trailerrel_type==CheckInMainDocumentsRelTypeKey.DELIVERY && trailerisLive ==CheckInLiveLoadOrDropOffKey.DROP && tractorisLive == CheckInLiveLoadOrDropOffKey.LIVE ?"checked":""%>
								    	value="<%=CheckInLiveLoadOrDropOffKey.DROP%>"/> 
								    <b>	<%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(CheckInLiveLoadOrDropOffKey.DROP) %></b>
								<input type="radio" id="live"    tabindex="12" name="live" onclick="search('1',this)" 
					                 	<%=trailerRow!=null && tractorRow!=null && trailerrel_type==CheckInMainDocumentsRelTypeKey.DELIVERY && trailerisLive ==CheckInLiveLoadOrDropOffKey.DROP && tractorisLive == CheckInLiveLoadOrDropOffKey.PICK_UP ?"checked":""%>
								    	value="<%=CheckInLiveLoadOrDropOffKey.SWAP %>"/> 
								    <b>	<%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(CheckInLiveLoadOrDropOffKey.SWAP) %></b>
							 </td>
			            </tr>
			          </table>
			          <fieldset id="deliveryCTNR" style="border:2px #cccccc solid;padding:5px;margin:5px;width:90%;">
			          	<legend style="font-size:14px;font-weight:normal;color:#999999;">
					Delivery CTNR
						</legend>		
				          <table width="98%" border="0" cellspacing="5" cellpadding="2" name="showMessageMes">
				            <tr height="20px">
								<td align="right" width="80px"> Spot</td>
								<td>
									<input name="yc_no" id="yc_no"  tabindex="13"  onkeypress="yardEnter(event)" onfocus="getYardNo()" value="<%=mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY && doorOrSpot!=null && doorOrSpot.get("resources_type",0)==OccupyTypeKey.SPOT  ?doorOrSpot.getString("resource_name"):""%>"/>
									<select  id="spotArea" tabindex="14" onchange="findSpotByZoneId(3)" >
									  <option value="">Spot Area</option>
						     		  <%
						     		  		if(spots!=null && spots.length>0){
						     		  			for(int i=0;i<spots.length;i++){
						     		  %>
						     		  <option value="<%=spots[i].get("area_id",0)%>" <%=mainRow!=null &&mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY  && doorOrSpot!=null && doorOrSpot.get("area_id",0)==spots[i].get("area_id",0)?"selected":""%>> <%=spots[i].get("area_name","")%></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
								
							    <td align="right" id="door_td"> Door</td>
								<td id="door_td">
									<input type="text"  id="door"   onkeypress="doorEnter(event)" tabindex="15" onfocus="openDoor()" value="<%=mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY && doorOrSpot!=null && doorOrSpot.get("resources_type",0)==OccupyTypeKey.DOOR ?doorOrSpot.getString("resource_name"):""%>"/>
									<select  id="allzone" onchange="findDoorsByZoneId(1)"  tabindex="15">
						     		  <option value="0">Zone</option>
						     		  <%
						     		  		if(allZone!=null && allZone.length>0){
						     		  			for(int i=0;i<allZone.length;i++){
						     		  %>
						     		  <option value="<%=allZone[i].get("area_id",0)%>" <%= mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY && doorOrSpot!=null && doorOrSpot.get("area_id",0)==allZone[i].get("area_id",0)?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							</tr>
							<tr>
								<td align="right"> Title</td>
								<td>
									<input  id="title" disabled="disabled" value="<%= flag && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY ? detailRow[0].getString("supplier_id"):"" %>"/>
								</td>
								<td align="right"> Appointment</td>
								<td>
									<input  id="appointment" disabled="disabled" value="<%=flag && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY && !detailRow[0].getString("appointment_date").equals("") ?DateUtil.showLocalTime(detailRow[0].getString("appointment_date"), ps_id):""%>"/>
								</td>
							</tr>
							<tr>
								<td align="right"> Zone</td>
								<td>
									<input  id="deliveryZone" disabled="disabled" />
								</td>
							</tr>
					     </table>
			          </fieldset>
			          <fieldset id="dpickup_ctnr" style="border:2px #cccccc solid;padding:5px;margin:5px;width:90%;display:none" >
			              <legend style="font-size:14px;font-weight:normal;color:#999999;">
					Picking Up CTNR
						</legend>
				          <table width="98%" border="0" cellspacing="5" cellpadding="2" name="showMes">
				             <tr>
				                <td align="right" width="80px"> Trailer/CTNR</td>
			            		<td>
			                 		<div class="wrap-search-input">
				                 	   	<input name="ctnr" id="ctnr"  onfocus="selectCommon(this)" value="<%=mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY && pickUpCtnrRow != null ? pickUpCtnrRow.getString("equipment_number"):""%>" onkeypress="if(event.keyCode==13){findSpotOrDoorByCtnr();return false;}"/>
				                 		<span class="icon-search-input" style="width: 24px; height: 24px;" onclick ="findSpotOrDoorByCtnr();"></span>
									</div>
			                 	</td>
			           <!--  <td>
	  		                  ReleaseCode:<input name="entryId" id="entryId" value="<%=mainRow!=null && !mainRow.getString("dlo_id").equals("") ? mainRow.getString("dlo_id"):""%>" onkeypress="if(event.keyCode==13){findSpotOrDoorByCtnr();return false;}"/>
			                 </td>-->
			            	</tr>
				            <tr>
								<td align="right" width="80px"> Spot</td>
								<td>
									<input name="yc_no" id="yc_no"   onkeypress="yardEnter(event)" onfocus="getYardNo()" value="<%=mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY  && pickUpCtnrRow != null ?pickUpCtnrRow.getString("spot_name"):""%>" disabled="disabled"/>
							   		<select  id="spotArea" onchange="findSpotByZoneId(3)" disabled="disabled">
									  <option value="">Spot Area</option>
						     		  <%
						     		  		if(spots!=null && spots.length>0){
						     		  			for(int i=0;i<spots.length;i++){
						     		  %>
						     		  <option value="<%=spots[i].get("area_id",0)%>" <%= mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY  && pickUpCtnrRow != null && pickUpCtnrRow.get("spot_area_id",0)==spots[i].get("area_id",0)?"selected":""%>> <%=spots[i].get("area_name","")%></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							    <td align="right" id="door_td"> Door</td>
								<td id="door_td">
									<input type="text"  id="door" tabindex="22"  onkeypress="doorEnter(event)" onfocus="openDoor()" value="<%=mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY && pickUpCtnrRow != null ?pickUpCtnrRow.getString("door_name"):""%>" disabled="disabled"/>
							    	<select  id="allzone" onchange="findDoorsByZoneId(1)" disabled="disabled">
						     		  <option value="0" tabindex="23" >Zone</option>
						     		  <%
						     		  		if(allZone!=null && allZone.length>0){
						     		  			for(int i=0;i<allZone.length;i++){
						     		  %>
						     		  <option value="<%=allZone[i].get("area_id",0)%>" <%= flag && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.DELIVERY && pickUpCtnrRow != null && pickUpCtnrRow.get("door_area_id",0)==allZone[i].get("area_id",0)?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							</tr>
					     </table>
			          </fieldset>
			       </form>
			       
				</div>
				<div id="PickUp">
					<form name="pickup_search" id="pickup_search">
					 <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0">
					    <tr height="25px;">
			            	<td>
			            		<input type="radio"  id="single_load" tabindex="16" name="load_count" <%=mainRow!=null && mainRow.get("load_count", 01)<=1 ?"checked":"" %>  onclick="loadSingleMultiple();"/> 
							    <b>Single</b>
							    <input type="radio"  id="multiple_load" tabindex="16" name="load_count" <%=mainRow!=null && mainRow.get("load_count", 01)>1 ?"checked":"" %>  onclick="loadSingleMultiple();"  /> 
							    <b>Multiple</b>
			            	</td>
			            </tr>
			            <tr id="single_tr" height="25px;">
			            	<td width="350px" >
								<%
			            			if(flag && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP){
				            	%>
				            	<span  id="title"><%=moduleKey.getModuleName(detailRow[0].get("number_type",0)) %></span>
				            	<%
				            			}else{
	            				%>
	            				<span  id="title">LOAD/PO/ORDER</span>
				            	<%			
				            			}
				            	%>
								<input name="search_number" id="search_number_pickup"  tabindex="17" search_number_pickupvalue="<%= flag && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP ?detailRow[0].getString("number"):""%>"/>
								<span style="color:red" id="showMessage_pickUp"></span>
			                 </td>
			            </tr>
			            <tr id="multiple_tr" style="display:none;height:25px;">
			            	 <td width="350px">
								NO. Of LOADS
								<input name="loadcount" id="loadcount" tabindex="18" value="<%=mainRow!=null && mainRow.get("load_count", 01)>1 ?mainRow.get("load_count", 01):"" %>"/>
								<span style="color:red" id="showMessage_pickUp"></span>
			                 </td>
			            </tr>
			            <tr height="25px;">
			            	<td>
			            		<input type="radio"  tabindex="19" id="live" name="live" onclick="search('2')"  
			            		<%=mainRow !=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP && mainRow.get("equipment_purpose", 0) ==CheckInLiveLoadOrDropOffKey.LIVE ?"checked":""%>
							    	value="<%=CheckInLiveLoadOrDropOffKey.LIVE%>"/> 
							    <b>	<%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(CheckInLiveLoadOrDropOffKey.LIVE) %></b>
							    <input type="radio" tabindex="19" id="drop" name="live"    onclick="search('2')"  
							    	<%=trailerRow!=null && tractorRow!=null && trailerrel_type==CheckInMainDocumentsRelTypeKey.PICK_UP && trailerisLive ==CheckInLiveLoadOrDropOffKey.DROP && tractorisLive == CheckInLiveLoadOrDropOffKey.LIVE ?"checked":""%>			
							    	value="<%=CheckInLiveLoadOrDropOffKey.DROP %>"/> 
							    <b>	<%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(CheckInLiveLoadOrDropOffKey.DROP) %></b>
							    <input type="radio" tabindex="19" id="drop" name="live"    onclick="search('2',this)" 
				                 	<%=trailerRow!=null && tractorRow!=null && trailerrel_type==CheckInMainDocumentsRelTypeKey.PICK_UP && trailerisLive ==CheckInLiveLoadOrDropOffKey.DROP && tractorisLive == CheckInLiveLoadOrDropOffKey.PICK_UP ?"checked":""%>
							    	value="<%=CheckInLiveLoadOrDropOffKey.SWAP%>" /> 
							    <b>	<%=checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(CheckInLiveLoadOrDropOffKey.SWAP) %></b>
			            	</td>
			            </tr>
			          </table>
			           <fieldset id="pickupCTNR" style="border:2px #cccccc solid;padding:5px;margin:5px;width:90%;">
			           <legend style="font-size:14px;font-weight:normal;color:#999999;">
					Pick Up CTNR
						</legend>
				          <table width="98%" border="0" cellspacing="5" cellpadding="2" name="showMes">
				            <tr height="20px">
								<td align="right" width="90px"> Spot</td>
								<td>
									<input name="yc_no" id="yc_no" tabindex="20"  onkeypress="yardEnter(event)" onfocus="getYardNo()" value="<%=mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP && doorOrSpot!=null && doorOrSpot.get("resources_type",0)==OccupyTypeKey.SPOT  ?doorOrSpot.getString("resource_name"):""%>"/>
							    	<select  id="spotArea" tabindex="21" onchange="findSpotByZoneId(3)" >
									  <option value="">Spot Area</option>
						     		  <%
						     		  		if(spots!=null && spots.length>0){
						     		  			for(int i=0;i<spots.length;i++){
						     		  %>
						     		  <option value="<%=spots[i].get("area_id",0)%>" <%= mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP && doorOrSpot!=null && doorOrSpot.get("area_id",0)==spots[i].get("area_id",0)?"selected":""%>> <%=spots[i].get("area_name","")%></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							    <td align="right" id="door_td"> Door</td>
								<td id="door_td">
									<input type="text"  id="door"  tabindex="22" onkeypress="doorEnter(event)" onfocus="openDoor()" value="<%=mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP  && doorOrSpot!=null && doorOrSpot.get("resources_type",0)==OccupyTypeKey.DOOR  ?doorOrSpot.getString("resource_name"):""%>"/>
							    	<select  id="allzone" tabindex="23" onchange="findDoorsByZoneId(1)" >
						     		  <option value="0">Zone</option>
						     		  <%
						     		  		if(allZone!=null && allZone.length>0){
						     		  			for(int i=0;i<allZone.length;i++){
						     		  %>
						     		  <option value="<%=allZone[i].get("area_id",0)%>" <%= mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP && doorOrSpot!=null && doorOrSpot.get("area_id",0)==allZone[i].get("area_id",0)?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							</tr>
							<tr>
								<td align="right"> Staging/Loc.</td>
								<td>
									<input  id="staging" disabled="disabled" value="<%= flag && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP? detailRow[0].getString("staging_area_id"):"" %>"/>
								</td>
								<td align="right"> Appointment</td>
								<td>
									<input  id="appointment" disabled="disabled" value="<%=flag && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP && !detailRow[0].getString("appointment_date").equals("")?DateUtil.showLocalTime(detailRow[0].getString("appointment_date"), ps_id):""%>"/>
								</td>
							</tr>
							<tr>
								<td align="right"> Zone</td>
								<td>
									<input  id="staging_zone" disabled="disabled" />
								</td>
								<td align="right"> Customer</td>
								<td>
									<input  id="customer" disabled="disabled" value="<%= flag && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP? detailRow[0].getString("customer_id"):"" %>"/>
								</td>
							</tr>
					     </table>
			          </fieldset>
			          <fieldset id="pickup_ctnr" style="border:2px #cccccc solid;padding:5px;margin:5px;width:90%;display:none" >
			             <legend style="font-size:14px;font-weight:normal;color:#999999;">
					Picking Up CTNR
						</legend>
				          <table width="98%" border="0" cellspacing="5" cellpadding="2" name="showMes">
				             <tr>
				                <td align="right" width="120px"> Trailer/CTNR</td>
			            		<td>
			            			<div class="wrap-search-input">
				                 	   	<input name="ctnr" id="ctnr"  tabindex="24" onfocus="selectCommon(this)" value="<%=mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP && pickUpCtnrRow != null ?pickUpCtnrRow.getString("equipment_number"):""%>" onkeypress="if(event.keyCode==13){findSpotOrDoorByCtnr();return false;}"/>
				                 		<span class="icon-search-input" style="width: 24px; height: 24px;" onclick ="findSpotOrDoorByCtnr();"></span>
									</div>
			                 	</td>
			           <!--  <td>
	  		                  ReleaseCode:<input name="entryId" id="entryId" value="<%=mainRow!=null && !mainRow.getString("dlo_id").equals("") ? mainRow.getString("dlo_id"):""%>" onkeypress="if(event.keyCode==13){findSpotOrDoorByCtnr();return false;}"/>
			                 </td>-->
			            	</tr>
				            <tr>
								<td align="right" width="120px"> Spot</td>
								<td>
									<input name="yc_no" id="yc_no"  tabindex="25"  onkeypress="yardEnter(event)" onfocus="getYardNo()" value="<%=mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP  && pickUpCtnrRow != null ?pickUpCtnrRow.getString("spot_name"):""%>" disabled="disabled"/>
							   		<select  id="spotArea" tabindex="26" onchange="findSpotByZoneId(3)" disabled="disabled">
									  <option value="">Spot Area</option>
						     		  <%
						     		  		if(spots!=null && spots.length>0){
						     		  			for(int i=0;i<spots.length;i++){
						     		  %>
						     		  <option value="<%=spots[i].get("area_id",0)%>" <%= mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP   && pickUpCtnrRow != null && pickUpCtnrRow.get("spot_area_id",0)==spots[i].get("area_id",0)?"selected":""%>> <%=spots[i].get("area_name","")%></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							    <td align="right" id="door_td"> Door</td>
								<td id="door_td">
									<input type="text"  id="door" tabindex="27"  onkeypress="doorEnter(event)" onfocus="openDoor()" value="<%=mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP  && pickUpCtnrRow != null ?pickUpCtnrRow.getString("door_name"):""%>" disabled="disabled"/>
							    	<select  id="allzone" tabindex="28" onchange="findDoorsByZoneId(1)" disabled="disabled">
						     		  <option value="0">Zone</option>
						     		  <%
						     		  		if(allZone!=null && allZone.length>0){
						     		  			for(int i=0;i<allZone.length;i++){
						     		  %>
						     		  <option value="<%=allZone[i].get("area_id",0)%>" <%= flag && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.PICK_UP  && pickUpCtnrRow != null && pickUpCtnrRow.get("door_area_id",0)==allZone[i].get("area_id",0)?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							</tr>
					     </table>
			          </fieldset>
			       </form>
				</div>
				<div id="Both">
				  <form name="both_search" id="both_search">
			           <fieldset style="border:2px #cccccc solid;padding:5px;margin:5px;width:90%;">
			             <legend style="font-size:14px;font-weight:normal;color:#999999;">
					Picking Up CTNR
						</legend>
				          <table width="98%" border="0" cellspacing="5" cellpadding="2" name="showMes">
				             <tr>
				                <td align="right" width="80px"> Trailer/CTNR</td>
			            		<td>
			            			<div class="wrap-search-input">
				                 	   	<input name="ctnr" id="ctnr"  tabindex="24" onfocus="selectCommon(this)" value="<%=mainRow!=null && pickUpCtnrRow != null ? pickUpCtnrRow.getString("equipment_number"):""%>" onkeypress="if(event.keyCode==13){findSpotOrDoorByCtnr();return false;}"/>
				                 		<span class="icon-search-input" style="width: 24px; height: 24px;" onclick ="findSpotOrDoorByCtnr();"></span>
									</div>
			                 	</td>
			           <!--  <td>
	  		                  ReleaseCode:<input name="entryId" id="entryId" value="<%=mainRow!=null && !mainRow.getString("dlo_id").equals("") ? mainRow.getString("dlo_id"):""%>" onkeypress="if(event.keyCode==13){findSpotOrDoorByCtnr();return false;}"/>
			                 </td>-->
			            	</tr>
				            <tr>
								<td align="right" width="80px"> Spot</td>
								<td>
									<input name="yc_no" id="yc_no"  tabindex="25" onkeypress="yardEnter(event)" onfocus="getYardNo()" value="<%=mainRow!=null  &&  pickUpCtnrRow != null ? pickUpCtnrRow.getString("spot_name"):""%>"/>
							   		<select  id="spotArea" tabindex="26" onchange="findSpotByZoneId(3)" >
									  <option value="">Spot Area</option>
						     		  <%
						     		  		if(spots!=null && spots.length>0){
						     		  			for(int i=0;i<spots.length;i++){
						     		  %>
						     		  <option value="<%=spots[i].get("area_id",0)%>" <%=mainRow!=null &&   pickUpCtnrRow != null && pickUpCtnrRow.get("spot_area_id",0)==spots[i].get("area_id",0)?"selected":""%>> <%=spots[i].get("area_name","")%></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							    <td align="right" id="door_td"> Door</td>
								<td id="door_td">
									<input type="text"  id="door"  tabindex="27" onkeypress="doorEnter(event)" onfocus="openDoor()" value="<%=mainRow!=null && mainRow.get("equipment_purpose", 0)==CheckInLiveLoadOrDropOffKey.PICK_UP &&  pickUpCtnrRow != null ? pickUpCtnrRow.getString("door_name"):""%>"/>
							    	<select  id="allzone" tabindex="28" onchange="findDoorsByZoneId(1)" >
						     		  <option value="0">Zone</option>
						     		  <%
						     		  		if(allZone!=null && allZone.length>0){
						     		  			for(int i=0;i<allZone.length;i++){
						     		  %>
						     		  <option value="<%=allZone[i].get("area_id",0)%>" <%=mainRow!=null && mainRow.get("equipment_purpose", 0)==CheckInLiveLoadOrDropOffKey.PICK_UP &&  pickUpCtnrRow != null && pickUpCtnrRow.get("door_area_id",0)==allZone[i].get("area_id",0)?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							</tr>
					     </table>
			          </fieldset>
			       </form>
				</div>
				
				<div id="small_parcel">
				
					<div style="margin-left:49px;" align="left">
					<%if(mainId > 0){ %>
						Small Parcel: <input id="small_parcel_no" disabled="disabled" value="<%=mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.SMALL_PARCEL && detailRow!=null && detailRow.length>0?detailRow[0].get("number",""):""%>"/>
					<%} %>
					</div>
					<fieldset style="border:2px #cccccc solid;padding:5px;margin:5px;width:90%;">
				          <table width="98%" border="0" cellspacing="5" cellpadding="2" name="showMes">
				            <tr height="20px">
								<td align="right" width="80px"> Spot</td>
								<td>
									<input name="yc_no" id="yc_no"   tabindex="29" onkeypress="yardEnter(event)" onfocus="getYardNo()" value="<%=mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.SMALL_PARCEL && doorOrSpot!=null && doorOrSpot.get("resources_type",0)==OccupyTypeKey.SPOT  ?doorOrSpot.getString("resource_name"):""%>"/>
									<select  id="spotArea" tabindex="30" onchange="findSpotByZoneId(3)" >
									  <option value="">Spot Area</option>
						     		  <%
						     		  		if(spots!=null && spots.length>0){
						     		  			for(int i=0;i<spots.length;i++){
						     		  %>
						     		  <option value="<%=spots[i].get("area_id",0)%>" <%= mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.SMALL_PARCEL && doorOrSpot!=null && doorOrSpot.get("area_id",0)==spots[i].get("area_id",0)?"selected":""%>> <%=spots[i].get("area_name","")%></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							    <td align="right" id="door_td"> Door</td>
								<td id="door_td">
									<input type="text"  id="door" tabindex="31"  onkeypress="doorEnter(event)" onfocus="openDoor()" value="<%=mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.SMALL_PARCEL && doorOrSpot!=null && doorOrSpot.get("resources_type",0)==OccupyTypeKey.DOOR  ?doorOrSpot.getString("resource_name"):""%>"/>
									<select  id="allzone" tabindex="32" onchange="findDoorsByZoneId(1)" >
						     		  <option value="0">Zone</option>
						     		  <%
						     		  		if(allZone!=null && allZone.length>0){
						     		  			for(int i=0;i<allZone.length;i++){
						     		  %>
						     		  <option value="<%=allZone[i].get("area_id",0)%>" <%= mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.SMALL_PARCEL && doorOrSpot!=null && doorOrSpot.get("area_id",0)==allZone[i].get("area_id",0)?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>
							</tr>
					     </table>
			          </fieldset>
				</div>
				<div id="unknown">
					<fieldset style="border:2px #cccccc solid;padding:5px;margin:5px;width:90%;">
				          <table width="98%" border="0" cellspacing="5" cellpadding="2" name="showMes">
				            <tr height="20px">
								<td align="right" width="80px"> Spot</td>
								<td>
									<input name="yc_no" id="yc_no" tabindex="33"  onkeypress="yardEnter(event)" onfocus="getYardNo()" value="<%=mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.NONE && doorOrSpot!=null && doorOrSpot.get("resources_type",0)==OccupyTypeKey.SPOT  ?doorOrSpot.getString("resource_name"):""%>"/>
									<select  id="spotArea" tabindex="34" onchange="findSpotByZoneId(3)" >
									  <option value="">Spot Area</option>
						     		  <%
						     		  		if(spots!=null && spots.length>0){
						     		  			for(int i=0;i<spots.length;i++){
						     		  %>
						     		  <option value="<%=spots[i].get("area_id",0)%>" <%= mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.NONE && doorOrSpot!=null && doorOrSpot.get("area_id",0)==spots[i].get("area_id",0)?"selected":""%>> <%=spots[i].get("area_name","")%></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							    <td align="right" id="door_td"> Door</td>
								<td id="door_td">
									<input type="text"  id="door" tabindex="35"  onkeypress="doorEnter(event)" onfocus="openDoor()" value="<%=mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.NONE && doorOrSpot!=null && doorOrSpot.get("resources_type",0)==OccupyTypeKey.DOOR  ?doorOrSpot.getString("resource_name"):""%>"/>
									<select  id="allzone" tabindex="36" onchange="findDoorsByZoneId(1)" >
						     		  <option value="0">Zone</option>
						     		  <%
						     		  		if(allZone!=null && allZone.length>0){
						     		  			for(int i=0;i<allZone.length;i++){
						     		  %>
						     		  <option value="<%=allZone[i].get("area_id",0)%>" <%= mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.NONE && doorOrSpot!=null && doorOrSpot.get("area_id",0)==allZone[i].get("area_id",0)?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>
							</tr>
					     </table>
			          </fieldset>
				</div>
				<div id="None">
					
					<fieldset style="border:2px #cccccc solid;padding:5px;margin:5px;width:90%;">
					 <div align="left" style="margin-left:79px"> 
					 <input value="<%=checkInLiveLoadOrDropOffKey.DROP %>"
					 	<%=mainRow !=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.VISITOR && mainRow.get("equipment_purpose", 0) ==CheckInLiveLoadOrDropOffKey.DROP ?"checked":""%>
					 	 name="live" id="drop" type="radio" onclick="checkDropWithParking(this)">
				     <span style="font-weight: bold;">Drop Off</span>
				     <input value="<%=checkInLiveLoadOrDropOffKey.TMS %>"
					 	<%=mainRow !=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.VISITOR && mainRow.get("equipment_purpose", 0) ==CheckInLiveLoadOrDropOffKey.TMS ?"checked":""%>
					 	 name="live" id="tms" type="radio" >
				     <span style="font-weight: bold;">TMS</span></div>
				          <table width="98%" border="0" cellspacing="5" cellpadding="2" name="showMes">
				            <tr height="30px">
								<td align="right" width="80px"> Spot</td>
								<td>
									<input name="yc_no" id="yc_no" tabindex="37"  onkeypress="yardEnter(event)" onfocus="openDoor()" value="<%=mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.VISITOR && doorOrSpot!=null && doorOrSpot.get("resources_type",0)==OccupyTypeKey.SPOT  ?doorOrSpot.getString("resource_name"):""%>"/>
									<select  tabindex="38" id="spotArea" onchange="findSpotByZoneId(3)" >
									  <option value="">Spot Area</option>
						     		  <%
						     		  		if(spots!=null && spots.length>0){
						     		  			for(int i=0;i<spots.length;i++){
						     		  %>
						     		  <option value="<%=spots[i].get("area_id",0)%>" <%= mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.VISITOR && doorOrSpot!=null && doorOrSpot.get("area_id",0)==spots[i].get("area_id",0)?"selected":""%>> <%=spots[i].get("area_name","")%></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>	
							    <td align="right" id="door_td"> Door</td>
								<td id="door_td">
									<input type="text" tabindex="39" id="door"   onkeypress="doorEnter(event)" onfocus="openDoor()" value="<%=mainRow != null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.VISITOR && doorOrSpot!=null && doorOrSpot.get("resources_type",0)==OccupyTypeKey.DOOR  ?doorOrSpot.getString("resource_name"):""%>"/>
									<select tabindex="40" id="allzone" onchange="findDoorsByZoneId(1)" >
						     		  <option value="0">Zone</option>
						     		  <%
						     		  		if(allZone!=null && allZone.length>0){
						     		  			for(int i=0;i<allZone.length;i++){
						     		  %>
						     		  <option value="<%=allZone[i].get("area_id",0)%>" <%= mainRow!=null && mainRow.get("rel_type", 0)==CheckInMainDocumentsRelTypeKey.VISITOR && doorOrSpot!=null && doorOrSpot.get("area_id",0)==allZone[i].get("area_id",0)?"selected":""%>> <%=allZone[i].get("area_name","") %></option>
						     		  <%			
						     		  		    }
						     		  		}
						     		  %>
		     		  				</select>
							    </td>
							</tr>
					     </table>
			          </fieldset>
				</div>
			</div>
		</div>
	  </td>
	 </tr>
	</table>
	<br/>
<!--   <table>
		 <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:97%;">
				 <input type="button"  name="Submit2" value="Submit" id="submitBtn" class="normal-green" onClick="addGateCheckInMes(<%=mainRow!=null?mainRow.get("dlo_id", 0):0%>);" onkeypress="if(event.keyCode==13){addGateCheckInMes();return false;}">
	     		 <input name="Submit2" type="button" class="normal-white" value="Cancel" onClick="closeWindow();" onkeypress="if(event.keyCode==13){closeWindow();return false;}">
	     </div>
	</table> --> 
<!-- 打印的容器 -->
<div id="av" style="width:368px; display: none"></div>	
</body>
<script>
function checkCTNR(evt){
	var ctnr = $("#gate_container_no").val();
	var ps_id = <%=ps_id%>;
	
	if(ctnr!=""){
	var uri = "<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/AjaxFindSpotByCtnNoAction.action";
	$.ajax({
		url:uri,
		data:{'container_no':ctnr,'ps_id':ps_id},
		dataType:'json',
		type:'post',
		success:function(data){
			
			if(data.dlo_id!=""&&data.dlo_id!=null){
				
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_checkCTNR.html?ctnr='+ctnr+'&ps_id='+ps_id; 
				$.artDialog.open(uri , {title: "Check CTNR",width:'880px',height:'450px', lock: true,opacity: 0.3,fixed: true});
			}
		},
		error:function(){
			showMessage("System error!","error");
		}
	});
	}
}
function dayin(printHtml){
	//获取打印机名字列表
	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
   //判断是否有该名字的打印机
	var printer = "LabelPrinter";
	var printerExist = "false";
	var containPrinter = printer;
	for(var i = 0;i<printer_count;i++){
		if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
			printerExist = "true";
			containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
			break;
		}
	}
	if(printerExist=="true"){		
	     for(var i=0;i<printHtml.length;i++){
    	    	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Label");
  	              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
  	                 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
  	  				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$(printHtml[i]).html());
  	  			     visionariPrinter.SET_PRINT_COPIES(1);
  	  				 //visionariPrinter.PREVIEW();
  	  			     visionariPrinter.PRINT();        		
	     }  
	}else{
		var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
		if(op!=-1){ //判断是否点了取消		
		     for(var i=0;i<printHtml.length;i++){
	    	    	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Product Label");
	              	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
	              	 visionariPrinter.SET_PRINT_PAGESIZE(1,"10.20cm","15.20cm","102X152");
	  				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$(printHtml[i]).html());
	  			     visionariPrinter.SET_PRINT_COPIES(1);
	  				 //visionariPrinter.PREVIEW();
	  			    visionariPrinter.PRINT();        		
		     }
		}	
	}
}

function findSpotByZoneId(flag){
	$("input[id='yc_no']").val("");
	var spotArea="";
	if($("#tabs").tabs("option","selected")==0){
		spotArea=$("#Delivery select[id='spotArea']").val();
		
	}
	else if($("#tabs").tabs("option","selected")==1){
		spotArea=$("#PickUp select[id='spotArea']").val();
	}	
	else if($("#tabs").tabs("option","selected")==2){
		spotArea=$("#Both select[id='spotArea']").val();
	}
	else if($("#tabs").tabs("option","selected")==3){
		spotArea=$("#small_parcel select[id='spotArea']").val();
	}
	else{
		spotArea=$("#None select[id='spotArea']").val();
	}
	var ps_id=$('#add_ps_id').val();
	if(spotArea!=""){
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindStorageYardSearchAction.action',
	  		dataType:'json',
	  		data:"spotArea="+spotArea+'&ps_id='+ps_id+"&flag="+flag+"&mainId="+<%=mainId%>,
	  		success:function(data){
	  			if(data.length>0){
	  				$("input[id='yc_no']").val(data[0].yc_no);
	  				$("#yc_id").val(data[0].yc_id);
	  				$("input[id='door']").val("");
	  				$("#checkInDoorId").val("");
	  				$("select[id='allzone']").val("");
	  				$("#spotZoneId").val(data[0].area_id);
	  				clear();
	  			}else if(data.length==0 && flag==3){
	  				findSpotByZoneId(4);
	  			}else if(data.length==0 && flag==4){
	  				$("input[id='yc_no']").val("");
	  				$("#yc_id").val("");
	  				getYardNo();
	  				$("select[id='spotArea']").val("");
	  			}
	  		}
	    });

	}
	
}
function findDoorsByZoneId(flag){
	$("input[id='door']").val("");
	$("#zone_id").val("");
	var zoneId="";
	if($("#tabs").tabs("option","selected")==0){
		zoneId=$("#Delivery select[id='allzone']").val();
		
	}
	else if($("#tabs").tabs("option","selected")==1){
		zoneId=$("#PickUp select[id='allzone']").val();
	}	
	else if($("#tabs").tabs("option","selected")==2){
		zoneId=$("#Both select[id='allzone']").val();
	}
	else if($("#tabs").tabs("option","selected")==3){
		zoneId=$("#small_parcel select[id='allzone']").val();
	}
	else if($("#tabs").tabs("option","selected")==4){
		zoneId=$("#unknown select[id='allzone']").val();
	}
	else{
		zoneId=$("#None select[id='allzone']").val();
	}
	if(zoneId>0){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindDoorsByZoneIdAction.action',
			data:'zoneId='+zoneId+"&mainId="+0+"&ps_id="+<%=ps_id%>+"&flag="+flag,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    },
			success:function(data){
				if(data.length>0){
	  				$("input[id='door']").val(data[0].doorid);
	  				$("#checkInDoorId").val(data[0].sd_id);
	  				$("input[id='yc_no']").val("");
	  				$("#yc_id").val("");
	  				$("select[id='spotArea']").val("");
	  				$("#zone_id").val(zoneId);
	  				clear();
	  			}else if(data.length==0 && flag==1){
	  				findDoorsByZoneId(2);
	  				$("#zone_id").val(zoneId);
	  			}else if(data.length==0 && flag==2){
	  				$("input[id='door']").val("");
	  				$("#checkInDoorId").val("");
	  				$("#zone_id").val("");
	  				openDoor();
	  			}
				
			},
			error:function(){
			}
		});
	}
}
function dianji(ra){
	if(ra.value==0){
        $("input[id='yc_no']").css("display","inline");
        $("input[id='door']").css("display","none");
        $("input[id='door']").val("");
        $("#checkInDoorId").val("");
	}else{
		$("input[id='yc_no']").css("display","none");
	    $("input[id='door']").css("display","inline");
	    $("input[id='yc_no']").val("");
	    $("#yc_id").val("");
	}
}

function openDoor(){
 
	var doorId='';
	var zoneId=$("#zone_id").val();
	var door = $("input[id='door']").val();
	if(door && door!=''){
		doorId=$("#checkInDoorId").val();
	}
// 	console.log("door="+door+"     doorId="+doorId);
// 	alert("doorId : " + doorId + "zoneId : " + zoneId);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/gate_in_door_list.html?zoneId='+zoneId+'&inputValue='+doorId+'&associate_id='+<%=mainId%>+'&inputId=-1'; 
// 	console.log(uri);
	$.artDialog.open(uri , {title: "Door and Location",width:'700px',height:'470px', lock: true,opacity: 0.3,fixed: true});
}

function getDoor(door,doorName,inputId,zone_id){
	$("input[id='door']").val(doorName);
	$("#checkInDoorId").val(door);
	$("input[id='yc_no']").val("");
	$("#yc_id").val("");
	$("select[id='spotArea']").val("");
	$("#zone_id").val(zone_id);
	$("select[id='allzone']").val(zone_id);
	clear();
}


function showTempPictrueOnline(_fileName){
 	   var obj = {
			current_name:_fileName ,
	   		table:"temp",
	   		fileNames:$("input[name='file_names']").val(),
	   		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upl_imags_tmp"
		}
 	   if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}
function getStorageYardInMap(){
	var width = window.parent.innerWidth;
	var height = window.parent.innerHeight;
	width = parseInt(width*0.9);
	height = parseInt(height*0.9);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/getParkingDocksInMap.html?storageId='+<%=ps_id%>+'&type=parking&width='+width+'&height='+height; 
	$.artDialog.open(uri , {title: "Map",width:width+'px',height:height+'px', lock: true,opacity: 0.3,fixed: true});
}
function getDoorInMap(){
	var width = window.parent.$("body").width();
	var height = window.parent.$("body").height();
	width = parseInt(width*0.9);
	height = parseInt(height*0.9);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/check_in/getParkingDocksInMap.html?storageId='+<%=ps_id%>+'&type=docks&width='+width+'&height='+height; 
	$.artDialog.open(uri , {title: "Map",width:width+'px',height:height+'px', lock: true,opacity: 0.3,fixed: true});
}
 	$('#liscense_plate').focus();



$(function(e){
	$("#mutiple").attr("tabindex",8);
	$("#single").attr("tabindex",9);
	$("#upload").attr("tabindex",10);
});
</script>
