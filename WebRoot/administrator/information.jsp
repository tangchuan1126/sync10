<%@page import="com.cwc.app.api.zr.AndroidPrintMgr"%>
<%@page import="com.cwc.app.key.PrintTaskKey"%>
<%@page contentType="text/html;charset=utf-8"%>
<%@page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.SystemTrackingKey"%>
<%@page import="com.cwc.app.key.TracingOrderKey"%>
<%@page import="com.cwc.app.key.PurchaseLogTypeKey"%>
<%@page import="com.cwc.app.key.InvoiceKey"%>
<%@page import="com.cwc.app.key.DrawbackKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.QualityInspectionKey"%>
<%@page import="com.cwc.app.key.PurchaseProductModelKey"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@include file="../include.jsp"%>
<%
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
long adid = adminLoggerBean.getAdid(); 
DBRow userInfo = adminMgr.getDetailAdmin(adid);
String emp_name = userInfo.getString("employe_name");
 
if(null == emp_name || emp_name.length() <= 0){
 
	emp_name = adminLoggerBean.getAccount();
}
%>
<HTML>
<HEAD>
<TITLE>TOP</TITLE>

<!-- mCustomScrollbar -->
<link href="/Sync10-ui/lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
<!-- Bootstrap Core CSS -->
<link href="/Sync10-ui/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="/Sync10-ui/pages/Examples/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
<!-- Timeline CSS -->
<link href="/Sync10-ui/pages/Examples/css/plugins/timeline.css" rel="stylesheet">
<!-- Morris Charts CSS -->
<link href="/Sync10-ui/pages/Examples/css/plugins/morris.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="/Sync10-ui/bower_components/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- 自定义 CSS -->
<link href="/Sync10-ui/pages/Examples/css/VisiStyle.css" rel="stylesheet">
    
<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>


<!-- link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script-->


<!-- 打印 -->
<script type="text/javascript" src="js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="js/print/m.js"></script>
<script src="js/printTask/printTask.js" type="text/javascript"></script>
<script type='text/javascript' src='../dwr/engine.js'></script>
<script type='text/javascript' src='../dwr/interface/sysNotifyMgrZr.js'></script>	
<script src="js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<script type="text/javascript">
dwr.engine._errorHandler = function(message, ex) {};  
//删除dropdown-toggle里的ul，就拿当前页面替换information.html -注意 .141 ip查找一下
//其他都一样
//============================set top menu===========================================
/*
$(function(){

//2015-05-13 19:00

$(".dropdown-toggle").click(function(){

	  var elthis=$(this);
	  var active=$(".active");
	  if(active.length > 0){
     active.removeClass('active')
	  }
	  elthis.addClass('active');
	  var menuclass=elthis.data('toggle');
    window.top.top_dropdown_menu(elthis,menuclass);

}).mouseleave(function() {
	
	var btnthis=$(this);
	var toggle=btnthis.data('toggle');
	 window.top.top_hover(btnthis,toggle);

});


})

*/

//============================end top menu===========================================

</script>
<script>

var t=null;
var monitorTraceOrdersTimer = null;
function ajaxLoadOrderTasksCount()
{
	var para = "1=1";

	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/GetTaskCountJSON.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request){
		},
		
		error: function(){
			//$("#orderTasksCount").html("(?)");
		},
		
		success: function(data){
			if (data.pubc+data.pric>0)
			{
				//$("#tasksCount").html("("+data.pubc+"|"+data.pric+")");
				$("#alert_img").attr({ src: "imgs/bell_plus_activite.gif" });
				
			}
			else
			{
				$("#tasksCount").html("");
				$("#alert_img").attr({ src: "imgs/bell_plus.png" });
			}
			
		}
	});
}

function ajaxLoadWaybillCount()
{
	var msg= "";
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/traceWayBillStockoutJSON.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		async:false,
		beforeSend:function(request){
		},
		error: function(){
			
		},
		success: function(data)
		{
			msg +="缺货:&nbsp;&nbsp;"
			for(var i = 0;i<data.length;i++)
			{
				msg+="<a href=\"waybill/waybill_index.html?cmd=trace&trace_type=<%=SystemTrackingKey.Stockout%>&ps_id="+data[i].psid+"\" target='main'>"+data[i].title+":"+data[i].stockcount+"</a>"
				msg+="&nbsp;&nbsp;"
			}
			msg +="<strong>|</strong>";
		}		
	});
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/traceWayBillOverdueCountJSON.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		async:false,
		beforeSend:function(request){
		},
		error: function(){
			
		},
		success: function(data)
		{
			msg +="&nbsp;&nbsp;超期未发货:&nbsp;&nbsp;"
			for(var i = 0;i<data.length;i++)
			{
				msg+="<a href=\"waybill/waybill_index.html?cmd=trace&trace_type=<%=SystemTrackingKey.Overdue%>&ps_id="+data[i].psid+"\" target='main'>"+data[i].title+":"+data[i].overduecount+"</a>"
				msg+="&nbsp;&nbsp;"
			}
		}		
	});
	
	return msg;
}


function monitorTraceOrders(type)
{
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getMonitorTraceCountJSonAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		
		beforeSend:function(request){
		},
		
		error: function(){
			
		},
		
		success: function(data)
		{
			var lackingordercount = data.lackingordercount*1;
			
			var verifycostordercount = data.verifycostordercount*1;
			var printdoubtordercount = data.printdoubtordercount*1;
			
			var disputerefundcount = data.disputerefundcount*1;
			var disputewarrantycount = data.disputewarrantycount*1;
			var otherdisputecount = data.otherdisputecount*1;
			
			//进单组
			var waitRecordCount = data.waitrecordordercount*1;
			var outBoundingCount = data.outboundingcount*1;
			var inOrderCount = (waitRecordCount*1+outBoundingCount*1);
			
			//订单组
			var doubtordercount = data.doubtordercount*1;
			var doubtaddressordercount = data.doubtaddressordercount*1;
			var doubtpayordercount = data.doubtpayordercount*1;
			var trackOrderCount = doubtordercount+doubtaddressordercount+doubtpayordercount;
			
			//快递组
			var deliveryExceptionCount = data.deliveryexceptioncount*1;
			var customsExceptionCount = data.customsexceptioncount*1;
			var notyetDeliveryedCount = data.notyetdeliveryedcount*1;
			var shippingCount = deliveryExceptionCount+customsExceptionCount+notyetDeliveryedCount;
			
			//发货组
			var stockoutWaybillCount = data.stockoutwaybillcount*1;
			var overdueWaybillCount = data.overduewaybillcount*1;
			var waybillCount = stockoutWaybillCount+overdueWaybillCount;

			//问答
			var questionCount=data.questioncount;
			
			//疑问商品
			var needAnswerDoubtGoodsCount = data.need_answer_doubt;
			var msg = "";
			
			if(type == <%=SystemTrackingKey.ALL%>)
			{
				msg = "<a href = \"javascript:monitorTraceOrders(<%=SystemTrackingKey.InOrder%>)\">进单("+inOrderCount+")</a>&nbsp;&nbsp;";
				msg +="<strong>|</strong>&nbsp;&nbsp;<a href = \"javascript:monitorTraceOrders(<%=SystemTrackingKey.NeedTrackingOrder%>)\">订单("+trackOrderCount+")</a>&nbsp;&nbsp;";
				msg +="<strong>|</strong>&nbsp;&nbsp;<a href = \"javascript:monitorTraceOrders(<%=SystemTrackingKey.NeedTrackingShipping%>)\">快递("+shippingCount+")</a>&nbsp;&nbsp;";
				msg +="<strong>|</strong>&nbsp;&nbsp;<a href = \"javascript:monitorTraceOrders(<%=SystemTrackingKey.NeedWayBill%>)\">发货("+waybillCount+")</a>&nbsp;&nbsp;";
				msg += "<strong>|</strong>&nbsp;&nbsp;<a href='javascript:purchaseNeedFollowUpCount(2,-1,-1)'>采购单("+data.purchase_need_follow_up_count+")</a>&nbsp;&nbsp;";
				msg += "<strong>|</strong>&nbsp;&nbsp;<a href='javascript:trackDeliveryCount()'>工厂交("+data.track_delivery_count+")</a>&nbsp;&nbsp;";
				msg += "<strong>|</strong>&nbsp;&nbsp;<a href='javascript:trackSendCount()'>仓库发("+data.track_send_count+")</a>&nbsp;&nbsp;";
				msg += "<strong>|</strong>&nbsp;&nbsp;<a href='javascript:trackReciveCount()'>仓库收("+data.track_recive_count+")</a>&nbsp;&nbsp;";
				msg += "<strong>|</strong>&nbsp;&nbsp;<a href='javascript:trackOceanShippingCount()'>海运("+data.ocean_shipping_count+")</a>&nbsp;&nbsp;";
				msg += "<strong>|</strong>&nbsp;&nbsp;<a href='javascript:trackProductFileCount()'>基础("+data.need_upload_product_total+")</a>&nbsp;&nbsp;";
				msg += "<strong>|</strong>&nbsp;&nbsp;<a href='javascript:getAllQuestionCatalog()'>问答("+questionCount+")</a>&nbsp;&nbsp;";
				msg += "<strong>|</strong>&nbsp;&nbsp;<a href='doubt_goods/doubt_goods_index.html?is_answerd=1' target='main'>商品识别("+needAnswerDoubtGoodsCount+")</a>"
				msg += "<strong>|</strong>&nbsp;&nbsp;<a href='javascript:getServicerAndStroagerNeedHandleCount(2,1)'>退货单("+data.servicer_and_stroager+")</a>&nbsp;&nbsp;";
			}
			else if(type==<%=SystemTrackingKey.InOrder%>)
			{
				msg = "<a href=\"order/ct_order_auto_reflush.html?handle=<%=HandleKey.WAIT4_RECORD%>\" target='main'>待抄单("+waitRecordCount+")</a><strong>|</strong><a href=\"order/ct_order_auto_reflush.html?handle=<%=HandleKey.OUTBOUNDING%>\" target='main'>出库中("+outBoundingCount+")</a>";
			}
			else if(type==<%=SystemTrackingKey.NeedTrackingOrder%>)
			{
				msg = "<a href=\"order/ct_order_auto_reflush.html?cmd=trace&trace_type=<%=SystemTrackingKey.DoubtOrder%>\" target='main'>疑问商品("+doubtordercount+")</a><strong>|</strong><a href=\"order/ct_order_auto_reflush.html?cmd=trace&trace_type=<%=SystemTrackingKey.DoubtAddress%>\" target='main'>疑问地址("+doubtaddressordercount+")</a><strong>|</strong><a href=\"order/ct_order_auto_reflush.html?cmd=trace&trace_type=<%=SystemTrackingKey.DoubtPay%>\" target='main'>疑问付款("+doubtpayordercount+")</a>";
				
			}
			else if(type==<%=SystemTrackingKey.NeedTrackingShipping%>)
			{
				msg = "<a href=\"order/ct_order_auto_reflush.html?cmd=trace&trace_type=<%=SystemTrackingKey.DeliveryException%>\" target='main'>派送例外("+deliveryExceptionCount+")</a><strong>|</strong><a href=\"order/ct_order_auto_reflush.html?cmd=trace&trace_type=<%=SystemTrackingKey.CustomsException%>\" target='main'>清关例外("+customsExceptionCount+")</a><strong>|</strong><a href=\"order/ct_order_auto_reflush.html?cmd=trace&trace_type=<%=SystemTrackingKey.NotYetDeliveryed%>\" target='main'>超期运输("+notyetDeliveryedCount+")</a>";
				
			}
			else if(type==<%=SystemTrackingKey.NeedWayBill%>)
			{
				msg = ajaxLoadWaybillCount();
			}
			
			<%
				if(adminLoggerBean.getAdgid()!=100025)
				{
			%>
				$("#track_td").html(msg);
			<%	
				}
			%>
			
			
		}		
		});
		
		clearTimeout(monitorTraceOrdersTimer);
		monitorTraceOrdersTimer = setTimeout("monitorTraceOrders("+type+")",1000*60*10);
}

function getAllQuestionCatalog(){
	$.ajax({
		type:'post',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxQuestionCountAction.action',
		dataType:'json',
		success:function(json){
			var msg = "";
			for(var i=0;i<json.length;i++){
                msg += '<a href="javascript:getQuestionCountByProductLineId('+json[i].productlineid+')">'+json[i].productlinename+'('+json[i].questioncount+')</a>&nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;';
			}
			$("#track_td").html(msg);
		}
	});   
}

function getQuestionCountByProductLineId(productLineId){
	document.question_form.productLine.value = productLineId;
	document.question_form.genjin.value="genjin";
	document.question_form.submit(); 
}

function transportTrackCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'first'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg += "<a  href='javascript:trackDeliveryCount()'>工厂交("+data.track_delivery_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
				mesg += "<a  href='javascript:trackSendCount()'>仓库发("+data.track_send_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
				mesg += "<a  href='javascript:trackReciveCount()'>仓库收("+data.track_recive_count+")</a>&nbsp&nbsp;&nbsp;&nbsp;";
				mesg += "<a href='javascript:trackOceanShippingCount()'>海运("+data.ocean_shipping_count+")";
				$("#track_td").html(mesg);
			}		
		});
	}
	function trackOceanShippingCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'ocean_shipping'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>海运:</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:needTrackOceanShipping(\""+data[i].cmd+"\")'>"+data[i].track_title+"("+data[i].track_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				});
				$("#track_td").html(mesg);
			}		
		});
	}
	function trackProductFileCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetProductTraceCountByFileAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:trackProductFileCountByProductLine()'>图片("+data.need_upload_product_total+")</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				$("#track_td").html(mesg);
			}		
		});
	}
	function trackProductFileCountByProductLine()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetProductTraceCountByFileAndProductLineAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				$.each(data,function(i)
				{
					mesg += "<a href='javascript:searchProductNeedUploadFileByProductLine(\""+data[i].product_line_id+"\")'>"+data[i].product_line_name+"("+data[i].need_upload_pc_count+")</a>&nbsp;&nbsp;";
				});
				$("#track_td").html(mesg);
			}		
		});
	}

	function searchProductNeedUploadFileByProductLine(productLineId)
	{
		$("#pro_line_id").val(productLineId);
		document.productNeedTrackByProductLine.cmd.value = "track_product_file";
		$("#productNeedTrackByProductLine").submit();
	}

	function trackDeliveryCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'ready_delivery'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:needTrackDeliveryProductLine("+data[i].product_line_id+",\""+data[i].product_line_name+"\")'>"+data[i].product_line_name+"("+data[i].track_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				});
				$("#track_td").html(mesg);
			}		
		});
	}
	
	function trackSendCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'send_store_transport'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>仓库:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:trackSendCountByPsid("+data[i].ps_id+",\""+data[i].title+"\")'>"+data[i].title+"("+data[i].need_track_send_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				});
				$("#track_td").html(mesg);
			}		
		});
	}
	
	function trackSendCountByPsid(ps_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:
			{
				type:'send_store_transport_ps',
				ps_id:ps_id
			},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:trackSendCount()'>"+title+"</a>:&nbsp;&nbsp;&nbsp;&nbsp;";
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"ready_send_ps\",\""+title+"\")'>备货中("+data.need_track_ready_transprot_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"packing_send_ps\",\""+title+"\")'>装箱中("+data.need_track_packing_transport_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"tag_send_ps\",\""+title+"\")'>内部标签("+data.need_track_tag_transport_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"tag_send_ps\",\""+title+"\")'>第三方标签("+data.need_track_third_tag_transport_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"product_file_send_ps\",\""+title+"\")'>实物图片("+data.need_track_product_file_transport_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"quality_inspection_send_ps\",\""+title+"\")'>质检报告("+data.need_track_quality_inspection_transport_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;"
				$("#track_td").html(mesg);
			}		
		});
	}
	
	function trackReciveCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'recive_store_transport'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>仓库:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:trackReciveCountByPsid("+data[i].ps_id+",\""+data[i].title+"\")'>"+data[i].title+"("+data[i].need_track_recive_count+")</a>&nbsp;&nbsp;"
				});
				$("#track_td").html(mesg);
			}		
		});
	}
	
	function trackReciveCountByPsid(ps_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:
			{
				type:'recive_store_transport_ps',
				ps_id:ps_id
			},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg += "<a  href='javascript:trackReciveCount()'>"+title+"</a>:&nbsp;&nbsp;";
				mesg += "<a  href='javascript:needTrackReceiveTransport("+data.ps_id+",\"intransit_receive_ps\",\""+title+"\")'>运输中("+data.need_track_intransit_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackReceiveTransport("+data.ps_id+",\"alreadyRecive_receive_ps\",\""+title+"\")'>已收货("+data.need_track_alreadyrecive_transport_count+")</a>&nbsp;&nbsp;"
				$("#track_td").html(mesg);
			}		
		});
	}
	
	function needTrackDeliveryProductLine(product_line_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/deliveryTrackByProductLine.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{product_line_id:product_line_id},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:trackDeliveryCount()'>"+title+":</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a href='javascript:needTrackDelivery("+product_line_id+",\""+title+"\",\""+data[i].cmd+"\")'>"+data[i].track_tilte+"("+data[i].track_count+")</a>&nbsp;&nbsp;"
				});
				$("#track_td").html(mesg);
			}		
		});
	}
	
	function needTrackDelivery(product_line_id,product_line_title,cmd)
	{
		document.track_form.product_line_id.value = product_line_id;
		document.track_form.cmd.value = cmd;
		document.track_form.product_line_title.value = product_line_title
		document.track_form.submit();
	}
	
	function needTrackOceanShipping(type)
	{
		document.track_form.cmd.value = type;
		document.track_form.submit();
	}
	
	function needTrackSendTransport(ps_id,type,title)
	{
		document.track_form.send_psid.value = ps_id;
		document.track_form.cmd.value = type;
		document.track_form.store_title.value = title;
		document.track_form.submit();
	}
	
	function needTrackReceiveTransport(ps_id,type,title)
	{
		document.track_form.receive_psid.value = ps_id;
		document.track_form.cmd.value = type;
		document.track_form.store_title.value = title;
		document.track_form.submit();
	}
	
	function purchaseNeedFollowUpCount(cmdType,productLineId,productLineName){
		$("#track_td").html("");
		var msg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/GetPurchaseNeedFollowUpCountAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{cmdType:cmdType,productLineId:productLineId},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				if(1 == cmdType){
					msg += "采购单<a href='javascript:purchaseNeedFollowUpCount(2,-1,-1)'>("+data.purchase_need_follow_up_count+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
				}
				else if(2 == cmdType)
				{
					msg += "<a href='javascript:purchaseNeedFollowUpCount(1,-1,-1)'>产品线:</a>&nbsp;&nbsp;&nbsp;&nbsp;";
					$.each(data, function(i){
						msg += "<a  href='javascript:purchaseNeedFollowUpCount(3,"+data[i].product_line_id+",\""+data[i].product_line_name+"\")'>"+data[i].product_line_name+"("+data[i].purchase_need_follow_count+")</a>&nbsp&nbsp;&nbsp;&nbsp;";
					})
				}
				else if(3 == cmdType)
				{
					$("#purchaseProductLineName").val(productLineName);
					msg += "<a href='javascript:purchaseNeedFollowUpCount(2,-1,-1)'>"+productLineName+"</a>:&nbsp;&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.INVOICE%>,<%=InvoiceKey.INVOICING %>)'>发票("+data.purchase_need_invoice+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.DRAWBACK%>,<%=DrawbackKey.DRAWBACKING %>)'>退税("+data.purchase_need_drawback+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.NEED_TAG%>,<%=TransportTagKey.TAG %>)'>内部标签("+data.purchase_need_tag+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.THIRD_TAG%>,<%=TransportTagKey.TAG %>)'>第三方标签("+data.purchase_need_tag_third+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.QUALITY%>,<%=QualityInspectionKey.NEED_QUALITY %>)'>质检要求("+data.purchase_need_quality+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.PRODUCT_MODEL%>,<%=PurchaseProductModelKey.NEED_PRODUCT_MODEL %>)'>商品范例("+data.purchase_need_product_model+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.PRICE%>,<%=PurchaseKey.OPEN %>)'>需确认价格("+data.purchase_need_affirm_price+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.PRICE%>,<%=PurchaseKey.AFFIRMTRANSFER %>)'>需跟进交货("+data.purchase_affirm_price+")</a>&nbsp;&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.PRICE%>,<%=PurchaseKey.APPROVEING %>)'>到货完成审核中("+data.purchase_arrival_examine+")</a>";
				}
				$("#track_td").html(msg);
			}		
		});
	};
	function purchaseFollowUpProcessSearch(productLineId, processKey, activity){
		$("#cmd").val("purchaseFollowUpProcessSearch");
		$("#purchaseProductLineId").val(productLineId);
		$("#purchaseFollowUpProcessKey").val(processKey);
		$("#purchaseFollowUpActivity").val(activity);
		$("#purchaseFollowUpSearch").submit();
	};
 	function getServicerAndStroagerNeedHandleCount(cmdType,servicerOrStroager)
 	{
 		$("#track_td").html("");
		var msg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/return_order/StatisticsReturnOrderAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{cmdType:cmdType,servicerOrStroager:servicerOrStroager},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				if(1 == cmdType){
					msg += "<a href='javascript:getServicerAndStroagerNeedHandleCount(2,1)'>退货单:("+data.servicer_and_stroager+")</a>";
				}
				else if(2 == cmdType)
				{
					msg += "<a href='javascript:getServicerAndStroagerNeedHandleCount(1,0)'>退货单("+data.servicer_and_stroager+")</a>&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:getServicerAndStroagerNeedHandleCount(3,2)'>客服("+data.servicer_need_handle_count+")</a>&nbsp;&nbsp;&nbsp;";
					msg += "<a href='javascript:getServicerAndStroagerNeedHandleCount(3,3)'>仓库("+data.storager_need_handle_count+")</a>";
				}
				else if(3 == cmdType)
				{
					if(2 == servicerOrStroager)
					{
						msg += "<a href='javascript:getServicerAndStroagerNeedHandleCount(2,2)'>客服:("+data.servicer_need_handle_count+")</a>&nbsp;&nbsp;&nbsp;";
						msg += "<a href='javascript:returnOrderNeedHandleSearch(\"need_recog_count\")'>需要识别商品("+data.need_recog_count+")</a>&nbsp;&nbsp;&nbsp;";
						msg += "<a href='javascript:returnOrderNeedHandleSearch(\"need_register_finish\")'>商品登记完成("+data.need_register_finish+")</a>&nbsp;&nbsp;&nbsp;";
						msg += "<a href='javascript:returnOrderNeedHandleSearch(\"need_relation_other_order\")'>需关联订单、运单或服务单("+data.need_relation_other_order+")</a>";
					}
					else if(3 == servicerOrStroager)
					{
						msg += "<a href='javascript:getServicerAndStroagerNeedHandleCount(2,2)'>仓库:("+data.storager_need_handle_count+")</a>&nbsp;&nbsp;&nbsp;";
						msg += "<a href='javascript:returnOrderNeedHandleSearch(\"need_gather_pic_count\")'>需要采集图片("+data.need_gather_pic_count+")</a>&nbsp;&nbsp;&nbsp;";
						msg += "<a href='javascript:returnOrderNeedHandleSearch(\"need_register_count\")'>需要登记商品("+data.need_register_count+")</a>";
					}
				}
				$("#track_td").html(msg);
			}		
		});
 	}
 	function returnOrderNeedHandleSearch(need_handle_type){
 		document.returnOrderNeedHandleSearchForm.cmd.value = need_handle_type;
		$("#returnOrderNeedHandleSearchForm").submit();
	};
 //实现消息的准确推送
sysNotifyMgrZr.setSysNofifySessionFlag('<%=adid%>');
  
</script>

<SCRIPT language=JavaScript>
<!--


function openDia(){
	window.top.openBillBy();
}
 function alertValue(){
		window.top.showSystemNotifyContent("你好,<span style='color:#f60;'>系统管理员</span>&nbsp;修改了你的 <br /><a href='javascript:void(0)' onclick='window.top.sheduleAClick(517220,1)' >1212ces测试111111132323</a>任务");
 }
 
  
//-->
</SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<META content="MSHTML 6.00.2900.2873" name=GENERATOR>
<!-- <style type="text/css">

/* *{margin:0px;padding:0px;font-size:12px;} */
a{text-decoration:none;}
td {
	font-size: 12px;
}
.tpl {
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: c8c8c8;
	border-right-color: #c8c8c8;
	border-bottom-color: #c8c8c8;
	border-left-color: #c8c8c8;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #f0f0f0;

	-moz-user-select: none; 
}



a.blue:link {
font-family: "Geneva", "Arial", "Helvetica", "sans-serif";
	color: #666666;
	text-decoration: none;
}
a.blue:visited {
font-family: "Geneva", "Arial", "Helvetica", "sans-serif";
	color: 666666;
	text-decoration: none;
}
a.blue:hover {
font-family: "Geneva", "Arial", "Helvetica", "sans-serif";
	color: #FF0000;
	text-decoration: none;

}
a.blue:active {
font-family: "Geneva", "Arial", "Helvetica", "sans-serif";
	color: 666666;
	text-decoration: none;

}

a.pre:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/pre.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.pre:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/pre.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.pre:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/pre_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.pre:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/pre.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}


a.ne:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/ne.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.ne:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/ne.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.ne:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/ne_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.ne:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/ne.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}



a.flush:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/flush.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.flush:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/flush.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.flush:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/flush_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.flush:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/flush.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}


a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}



a.help-center:link {
font-size: 12px;
color: #000000;
text-decoration: none;

}

a.help-center:visited {
font-size: 12px;
color: #000000;
text-decoration: none;

}

a.help-center:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;

}
a.help-center:active {
font-size: 12px;
color: #000000;
text-decoration: none;
}
ul.ul_opera{list-style-type:none;border:0px solid red;height:100%;}
ul.ul_opera li{float:left;padding-left:5px;padding-right:5px;cursor:pointer;}

</style>
<style type="text/css">a {blr:expression(this.onFocus=this.blur())}</style>
 --> 


<style type="text/css">
  /* html,body{overflow: hidden;padding: 0px}   
  #Compond_navbar{padding: 0px}
  body .sidebar{position:static;width:100%;display: block;margin:0px;}
  body .nav>li>a:hover,body .side-menubox .a_select{font-weight: bold;}
  

  .navbar-brand{margin-top: 3px;color:#5E5E5E !important;padding-top: 12px;padding-left: 10px;} 

  .navbar-static-top{background-color: #F8F8F8;}
  .nav>li>a.dropdown-toggle{color:#5E5E5E !important;}
 
 .nav>li>a:hover i{color:#5E5E5E}  

 .navbar {min-height:45px;}
 。navbar-top-links{  height: 45px;
  line-height: 1em;}  */

  html,body{overflow: hidden;padding: 0px;height: 100%}   
  #Compond_navbar{padding: 0px}
  body .sidebar{position:static;width:100%;display: block;margin:0px;}
  
 
.navbar-header{background:url('/Sync10/administrator/imgs/new_logo.png') no-repeat; background-position: 23% 48%;float:left;height:34px;width:34px;background-size: 100% 100%;margin: 6px 0 0 10px;}
.bootomstyle{margin-bottom: 0;border-bottom: solid 1px #D6D6D6;height: 45px;min-height: 45px;overflow:hidden;}
 
 
 
 /*2015-06-10 12：14 new */
.navbar-right{list-style: none;margin-right: 12px;height: 50px;overflow: hidden;vertical-align: middle;margin-bottom: 0px}
.Navigation_btn{line-height: 50px}
.Navigation_btn a,.Navigation_btn a.active,.Navigation_btn a:hover{width:150px;margin-right: 6px;vertical-align: middle;text-decoration: none;}
.Navigation_btn a:hover{color:#5E5E5E}
 /*2015-06-10 12：14 new */
 
 

</style>


 
</HEAD>
<BODY  oncontextmenu="window.event.returnValue=false"   onselectstart="event.returnValue=false"   ondragstart="window.event.returnValue=false"   onsource="event.returnValue=false"  onLoad="dwr.engine.setActiveReverseAjax(true);">
<div style="display:none;">

<form action="customerservice_qa/question_classify.html" target="main" method="post" name="question_form">
     <input type="hidden" name="productLine" />
     <input type="hidden" name="genjin" />
</form>
<form action="transport/transport_order_index.html" target="main" method="post" name="track_form">
	<input type="hidden" name="send_psid"/>
	<input type="hidden" name="receive_psid"/>
	<input type="hidden" name="product_line_id"/>
	<input type="hidden" name="cmd"/>
	<input type="hidden" name="store_title"/>
	<input type="hidden" name="product_line_title"/>
</form>
<form name="purchaseFollowUpSearch" id="purchaseFollowUpSearch" method="post" target="main" action="purchase/purchase.html">
	<input type="hidden" name="cmd" id="cmd"/>
	<input type="hidden" name="purchaseProductLineId" id="purchaseProductLineId"/>
	<input type="hidden" name="purchaseFollowUpProcessKey" id="purchaseFollowUpProcessKey"/>
	<input type="hidden" name="purchaseFollowUpActivity" id="purchaseFollowUpActivity"/>
	<input type="hidden" name="purchaseProductLineName" id="purchaseProductLineName"/>
</form>
<form name="returnOrderNeedHandleSearchForm" id="returnOrderNeedHandleSearchForm" method="post" target="main" action='<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/return_product_list.html'>
	<input type="hidden" name="cmd"/>
</form>
<form name="productNeedTrackByProductLine" id="productNeedTrackByProductLine" method="post" target="main" action="product/ct_product.html">
	<input type="hidden" name="cmd" id="cmd"/>
	<input type="hidden" name="pro_line_id" id="pro_line_id"/>
</form>
</div>

<div style="background:url('imgs/forum_r1_c1.gif') repeat-x;height:36px;line-height:36px; display:none;">
	<div style="height:100%;width:100px;background:url('imgs/system_logo.gif') no-repeat; background-position: 23% 48%;float:left; ">
		<!-- <a href="javascript:monitorTraceOrders(<%=SystemTrackingKey.ALL%>)"><img src="imgs/system_logo.gif" border="0"></a> 现在注释，Sync上没有地方用这个了 -->
	</div>
	<div style="float:right;border:0px solid green;">
		 <ul class="ul_opera">
		 <!--
 	      		<li style="border:0px solid silver;">
	      			<a style="display:block;width:16px;background:url('imgs/print_task.png') no-repeat; text-align:center; background-position: 23% 48%; " href="javascript:void(0);" onclick="openTask();">
	      			 	<span style="color:white;" id="print_task_count" >0</span>
 	      			</a>
	      		</li>
	      		<li class="split">|</li>
	      	   -->
	      	<li>
	      		<%DBRow[] warehouses = adminLoggerBean.getWarehouse();
	      		if(warehouses.length > 0)
	      		{
	      			out.print(warehouses[0].getString("warename"));
	      		}
	      		%>
<!-- 	      		<img src="imgs/issue.png" width="16" height="16" align="absmiddle"><span onclick="openDia();" style="cursor:pointer;"/> WayBill</span> -->
	      	</li>
	      	<li class="split">|</li>
	      	<!-- 
	      	<li>
	      		 <img src="imgs/bell_plus.png" width="16" height="16" align="absmiddle" id="alert_img"> <a href="task/new_task_list.html" target="main"  onFocus="this.blur()" style="text-decoration: none;color:#000000;">任务<span id="tasksCount" ></span></a>
	      	</li>
	      	
	      	<li class="split">|</li>
	      	 -->
	      	<li>
	      		 <a href="help_center/index.html" target="_blank" class="help-center" onFocus="this.blur()">Help</a> 
	      	</li>
	      	<li class="split">|</li>
	      	<li>
	      		<span style="font-size:12px;padding-right:4px;"><a href="schedule_management/show_schedule.html" target="main" style="text-decoration: none;color:#000000;"><%=emp_name%></a></span>
	      		<a class="logout" href="javascript:window.top.logout();" onFocus="this.blur()"></a>
	      	</li>
      </ul>
	</div>
	<div style="clear:both;"></div>	


 
  </div>
  
  <nav class="navbar navbar-default navbar-static-top bootomstyle" role="navigation">
	 <div class="navbar-header"></div>
      <a class="navbar-brand">Linc</a>
       <!--2015-06-10 12：14-->
	  <ul class="navbar-right">
	    <li class="Navigation_btn">
	     <a class="bell" href="schedule_management/show_schedule.html" target="main">
	            <i class="fa fa-bell fa-fw"></i>
	            <span>TASK</span>
	        </a>         
          <a class="user" href="schedule_management/show_schedule.html" target="main">
	            <i class="fa fa-user fa-fw"></i>
	            <span><%=emp_name%></span>
	        </a>
	         <%if(warehouses.length > 0){%>
         <a class="gear" href="/Sync10-ui/pages/gis/gis_main.html" target="main">
	            <i class="fa fa-gear fa-fw"></i>
	            <span><%=warehouses[0].getString("warename") %></span>
	        </a>   
	         <%}%>
	                
           <a class="out" href="javascript:window.top.logout();" onfocus="this.blur()">
	            <i class="fa fa-sign-out fa-fw"></i>
	            <span>Logout</span>
	        </a>
	    </li>
	  </ul>
	  <!--2015-06-10 12：14-->	  
  </nav>	  
  <script type="text/javascript" data-main="./leftMenu" src="/Sync10-ui/bower_components/requirejs/require.js"></script>
  
  <div id="androidPrint" style="display:none"></div>
  <input type="hidden" id="lodopIsShow" value="1" />
  <input type="hidden" id="lodopInstallationUrl" value="<%=ConfigBean.getStringValue("systenFolder")%>"/>
  
  <script type="text/javascript">
	//============================set top menu===========================================
	
	  $(function(){
	
	    $(".dropdown-toggle").click(function(){
	
	    	var elthis=$(this);
	    	var menuclass=elthis.data('toggle');
	    	
	    	var userList = '<li><a href="schedule_management/show_schedule.html" target="main"><i class="fa fa-user fa-fw"></i>&nbsp;<%=emp_name%></a></li>'+
				      <%if(warehouses.length > 0){%>	
				  		   '<li><a href="/Sync10-ui/pages/gis/gis_main.html" target="main"><i class="fa fa-gear fa-fw"></i>&nbsp;<%=warehouses[0].getString("warename") %></a></li>'+
				  	  <%}%>
				            
				           '<li class="divider"></li>'+
				           '<li><a href="javascript:window.logout();" onFocus="this.blur()"><i class="fa fa-sign-out fa-fw"></i>&nbsp;Logout</a></li>';
			
			/* var bellList = '<li><a href="help_center/index.html" target="_blank" onFocus="this.blur()"><i class="fa fa-question fa-fw"></i> Help</a></li>';	 */           
	    	var bellList = '<li><a href="schedule_management/show_schedule.html" target="main"><i class="fa fa-tasks fa-fw"></i> TASK</a></li>';
			
			var dropdowns = new Array();
			dropdowns["user"] = userList;
			dropdowns["bell"] = bellList;
			
	        window.top.top_dropdown_menu(elthis,menuclass, dropdowns[menuclass]);
	
	    });
	
	  })

  	//============================end top menu===========================================
  </script>
  
  </BODY></HTML>
