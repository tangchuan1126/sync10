<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.ProductStorageTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
long parentid = StringUtil.getLong(request,"parentid");
DBRow catalog = catalogMgr.getDetailProductStorageCatalogById(parentid);
if ( catalog==null )
{
	catalog = new DBRow();
	//catalog.add("title","根");
}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add Storage</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" /> -->
<link href="../comm.css" rel="stylesheet" type="text/css">
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 
<style type="text/css">
	div.cssDivschedule{
			width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
			background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
			background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
	
</style>
<script>

$(function(){
	$(".chzn-select").chosen({no_results_text: "no this options:"});
});
function addStorage(){
	if(volidate()){
		var waitTimeSet = setTimeout('showMessage("Load......","error");', 20000);
		$(".cssDivschedule").css("display","block");
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_catalog/AddStorageCatalog.action',
				data:$("#add_form").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					clearTimeout(waitTimeSet);
					showMessage("Success.","success");
					setTimeout("windowClose()", 1000);
				},
				error:function(){
					$(".cssDivschedule").css("display","none");
					 	
				}
			})	
		}
};
function volidate(){
	if("" == $("#title").val()){
		showMessage("Input Address Name", "alert");
		return false;
	}
	if("" == $("#ccid_hidden").val() || 0 == $("#ccid_hidden").val()){
		showMessage("Select Nation", "alert");
		return false;
	}
	return true;
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function removeAfterProIdInput(){$("#address_state_input").remove();}
function setPro_id(){
	removeAfterProIdInput();
	var node = $("#ccid_hidden");
	var value = node.val();
 	$("#pro_id").empty();
	 	  $.ajax({
				url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+value,
				dataType:'json',
				type:'post',
				success:function(data){
					if("" != data){
						var provinces = eval(data);
						$("#pro_id").attr("disabled",false);
						for(var i = 0; i < data.length; i ++){
							$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
						}
						$("#pro_id").append("<option value=-1>Input</option>");
					}else{
						$("#pro_id").attr("disabled",false);
						$("#pro_id").append("<option value=0>Select......</option>");
						$("#pro_id").append("<option value=-1>Input</option>");
					}
					$("#pro_id").trigger("liszt:updated");
		            $("#pro_id").chosen();
				},
				error:function(){
				}
		  })
 };
 function handleProInput(){
 	var value = $("#pro_id").val();
 	if(-1 == value){
 		$("#addBillProSpan").append("<input type='text' name='address_state_input' id='address_state_input' class='input-line'/>");
 		$("#addBillProSpan").css("display", "");
 	}else{
 		removeAfterProIdInput();
 	}
 };
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body>
<div class="cssDivschedule" style=""></div>
<br/>
<div style="border:0;height: 380px;">
	<form name="add_form" id="add_form" method="post" action="" >
		<input type="hidden" name="parentid" value="<%=parentid%>">
		<table width="98%" border="0" cellspacing="5" cellpadding="2">
		  <tr>
		    <td align="right" valign="middle" class="STYLE1 STYLE2" >Storage Name&nbsp;:&nbsp;</td>
		    <td align="left" valign="middle" ><input name="title" type="text" class="input-line" id="title" ></td>
		  </tr>
		  <tr>
				<td align="right" valign="middle" class="STYLE3" >Province&nbsp;:&nbsp;</td>
				<td> 
					<select disabled="disabled" id="pro_id" name="pro_id" onchange="handleProInput()" style="margin-right:5px;width:300px;" class="chzn-select" data-placeholder="Select Province..."></select>
				</td>
			</tr>
		<tr><td></td><td><span id="addBillProSpan" style="display: none;"></span><td></tr>
		  <tr>
			<td align="right" valign="middle" class="STYLE3" >Nation&nbsp;:&nbsp;</td>
			<td>
					 
		<!-- 国家省份信息   国家，省份，城市，街道，门牌号，邮编-->
			<%
				DBRow countrycode[] = orderMgr.getAllCountryCode();
				String selectBg="#ffffff";
				String preLetter="";
			%>
	      	 <select  id="ccid_hidden"  name="ccid" onChange="removeAfterProIdInput();setPro_id();" class="chzn-select" data-placeholder="Select Nation..." style="width:300px;">
		 	 	<option value="0">Select...</option>
				  <%
				  for (int i=0; i<countrycode.length; i++){
				  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
						if (selectBg.equals("#eeeeee")){
							selectBg = "#ffffff";
						}else{
							selectBg = "#eeeeee";
						}
					}  	
					preLetter = countrycode[i].getString("c_country").substring(0,1);
				  %>
		  		  <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
				  <%
				  }
				%>
	   			 </select>
				</td>
			</tr>
			<tr>
				<td align="right" valign="middle" class="STYLE3" >StorageType&nbsp;:&nbsp;</td>
				<td>
					<select name="storage_type" style="margin-right:5px;width:300px;" class="chzn-select" data-placeholder="Select Storage Type...">
						<%
							StorageTypeKey storageTypes	= new StorageTypeKey();
							List storageTypeList				= storageTypes.getStorageTypeKeys();
							for(int i = 0; i < storageTypeList.size(); i ++){
						%>			
								<option value="<%=storageTypeList.get(i) %>"><%=storageTypes.getStorageTypeKeyNameEn(storageTypeList.get(i)+"") %></option>
						<%			
							}
						%>
					</select>
				</td>
			</tr>
		</table>
	</form>	
</div>

<table style="width: 100%;">
  <tr style="width: 100%;" align="right">
    <td style="width: 100%">
	  <input type="button" name="Submit2" value="Submit" class="normal-green" onClick="addStorage();">
	</td>
  </tr>
</table> 
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>
