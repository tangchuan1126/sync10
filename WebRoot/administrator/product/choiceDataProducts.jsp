<%@page import="com.cwc.app.key.ProductFileTypeKey"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.util.StringUtil"%>
<%@page import="com.cwc.db.DBRow"%>
<%@page import="com.cwc.exception.JsonException"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.beans.jqgrid.FilterBean"%>
<%@page import="net.sf.json.JsonConfig"%>
<%@page import="com.cwc.app.beans.jqgrid.RuleBean"%>
<%@page import="com.cwc.json.JsonUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.PriceUOMKey"%>
<jsp:useBean id="priceUOMKey" class="com.cwc.app.key.PriceUOMKey"></jsp:useBean>


<%@ include file="../../include.jsp"%> 
<%! private static int oldpages = -1; %>
<%
	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	
	String pcid = StringUtil.getString(request,"pcid");
	String cmd = StringUtil.getString(request,"cmd");
	String key = StringUtil.getString(request,"key");
	int union_flag = StringUtil.getInt(request, "union_flag");
	String pro_line_id = StringUtil.getString(request,"pro_line_id");
	int product_file_types = StringUtil.getInt(request, "product_file_types");
	int product_upload_status = StringUtil.getInt(request, "product_upload_status");
	String title_id = StringUtil.getString(request, "title_id");
	long set_pid = StringUtil.getLong(request, "set_pid");
	int equal_or_not = StringUtil.getInt(request, "equal_or_not");
	
	//System.out.println("1set_pid:"+set_pid+",equal_or_not:"+equal_or_not);
//	System.out.println("pcid:"+pcid+",cmd:"+cmd+",key:"+key+",union_flag："+union_flag);	
//	System.out.println(",pro_line_id:"+pro_line_id+",product_file_types:"+product_file_types+",product_upload_status:"+product_upload_status+",title_id:"+title_id);
	
	
	boolean search = Boolean.parseBoolean(StringUtil.getString(request,"_search"));
	String sidx = StringUtil.getString(request,"sidx");//排序使用的字段
	String sord = StringUtil.getString(request,"sord");
	String filter = StringUtil.getString(request,"filters");
	FilterBean filterBean = null;
	if(search)
	{
		Map<String, Object> map = new HashMap<String, Object>();  //String-->Property ; Object-->Clss 
		map.put("rules", RuleBean.class);
		JsonConfig configjson = JsonUtils.getJsonConfig();
		configjson.setClassMap(map);
		filterBean = (FilterBean) JsonUtils.toBean(JSONObject.fromObject(filter),FilterBean.class,configjson); 
	}
			
	String c = StringUtil.getString(request,"rows");
	int pages = StringUtil.getInt(request,"page",1);
	PageCtrl pc = new PageCtrl();
	pc.setPageSize(Integer.parseInt(c));
	pc.setPageNo(pages);
	if(oldpages != pages||pages==1)
	{
		DBRow rows[];
		if (cmd.equals("filter"))
		{
			
	
			rows = proprietaryMgrZyj.filterProductByPcLineCategoryTitleId(true, pcid, pro_line_id, union_flag, product_file_types, product_upload_status,title_id, 0L,equal_or_not,set_pid, pc, request, -1);
			
		}
		else if (cmd.equals("search"))
		{
			//rows = productMgr.getSearchProducts4CT(key,pc);
			//rows =  productMgrZJ.getDetailProductLikeSearch(key,pc);//productMgr.getSearchProducts4CT(key,pcid,pc);
			//System.out.println(pcid);
			rows = proprietaryMgrZyj.findChoiceDetailProductLikeSearch(true, key, title_id, 0L,union_flag,equal_or_not,set_pid, pc,-1, request);
		}
		else if(cmd.equals("track_product_file"))
		{
			//rows = productMgrZyj.getProductInfosProductLineProductCodeByLineId(pro_line_id, pc);
			rows = proprietaryMgrZyj.findProductInfosProductLineProductCodeByLineId(true, pro_line_id, title_id, 0L, pc, request);
		}
		else
		{
			//rows = productMgr.getAllProducts(pc);
			rows = proprietaryMgrZyj.findCohiceProductsByTitleAdid(true, title_id, 0L, pc, request);
		//	System.out.println("2"+new JsonObject(rows).toString());
		}
		
		for(int i = 0;i<rows.length;i++)
		{
			if(rows[i].get("union_flag",0)==0)
			{
				rows[i].add("unionimg","<img src='../imgs/product.png'/>");
			}
			else
			{
				rows[i].add("unionimg","<a href='javascript:void(0)' title='View Suit Relationship'><img src='../imgs/union_product.png' border='0'/></a>");
				
			}
		  	StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
		  	String product_line_name = "";
		  	DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
		  	if(null != catalog)
		  	{
		  		/*
		  		long product_line_id = catalog.get("product_line_id", 0L);
		  		DBRow productLine = productLineMgrTJH.getProductLineById(product_line_id);
		  		if(null != productLine)
		  		{
		  			product_line_name = productLine.getString("name");
		  			catalogText.append("<img src='img/folderopen.gif'/> <a class='nine4' href='javascript:void(0)'>"+product_line_name+"</a><br/>");
			  		String s = "<img src='img/joinbottom.gif'/>";
			  		catalogText.append(s);
		  		}*/
		  		DBRow allFather[] = tree.getAllFather(rows[i].get("catalog_id",0l));
			  	for (int jj=0; jj<allFather.length-1; jj++)
			  	{
			  		catalogText.append("<img src='img/folderopen.gif'/>"+allFather[jj].getString("title")+"<br/>");
			  		String s = (jj==0) ? "<img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>"  : "<img  src='img/empty.gif'/><img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>";
			  		catalogText.append(s);
			  	}
			  	
			  	if (catalog!=null)
			  	{
			  		catalogText.append("<img  src='img/page.gif'/>"+catalog.getString("title"));
			  	}  
		  	}
		  	else
		  	{
		  		DBRow productLine = productLineMgrTJH.getProductLineById(rows[i].get("catalog_id", 0L));
		  		if(null != productLine)
		  		{
		  			catalogText.append("<img src='img/folderopen.gif'/>"+productLine.getString("name"));
		  		}
		  	}
		  	

			rows[i].add("catalog_text",catalogText.toString());
			String alive_text;
		  	if (rows[i].get("alive",0)==1)
			{
		      alive_text = "<input name='Submit43' type='button' class='short-button-cancel' value=' unavail' onClick=\"swichProductAlive('"+rows[i].getString("p_name")+"',"+rows[i].getString("pc_id")+","+rows[i].getString("alive")+")\"/>&nbsp;"+
		      				"<input type='button' class='short-button-convert' value='Label' onClick=\"make_tab("+rows[i].getString("pc_id")+")\"  />&nbsp;"+
		      				"<input type='button' class='short-button-convert' value='Label' onClick=\"manageCodes(this);\" />";
			}
			else
			{
		       alive_text = "<input name='Submit43' type='button' class='short-button-ok' value='avail' onClick=\"swichProductAlive('"+rows[i].getString("p_name")+"',"+rows[i].getString("pc_id")+","+rows[i].getString("alive")+")\"/>&nbsp;"+
		       				"<input type='button' class='short-button-convert' value='Label' onClick=\"make_tab("+rows[i].getString("pc_id")+")\"  />&nbsp;"+
		       				"<input type='button' class='short-button-convert' value='Label' onClick=\"manageCodes(this);\" />";
			}
			rows[i].add("alive_text",alive_text);
			rows[i].remove("p_img");
			

			
			StringBuffer html =  new StringBuffer();
			html.append("<ul class='myul'>");
			// 显示出来商品文件的个数 和 商品标签的个数

	  		ArrayList<String> selectedList= new ProductFileTypeKey().getProductFileTypeValue();
		 	Map<Integer,DBRow> productFileMap = transportMgrZr.getProductFileAndTagFile(rows[i].get("pc_id",0l),rows[i].get("pc_id",0l),FileWithTypeKey.PRODUCT_SELF_FILE);
		 	for(int indexOfList = 0 , countOfList = selectedList.size() ; indexOfList < countOfList ;indexOfList++ ){
		 		DBRow tempCount = productFileMap.get(indexOfList+1);
		 		int tempCountNumber = 0 ;
	 			tempCountNumber = null != tempCount?tempCount.get("count",0):0;
	 			html.append("<li style='width:70px;'>");
 				html.append(selectedList.get(indexOfList).trim());
 				html.append(":");
 				html.append(tempCountNumber);
 				html.append("</li>");

		 	}
		 	html.append("</ul>");
		 	rows[i].add("img",html.toString());
			
		 	String amozonCode = rows[i].getString("p_code2");
		 	String upcCode = rows[i].getString("upc");
		 	
			DBRow[] productCodes = productCodeMgrZJ.getUseProductCodes(rows[i].get("pc_id",0l));
			String pcodes = "";
			for(int j=0;j<productCodes.length;j++)
			{
				pcodes += productCodes[j].getString("p_code");
				
				if(j<productCodes.length-1)
				{
					pcodes +="\n";
				}
			}
			//rows[i].add("p_codes",pcodes);
			String p_codes_str = "";//"<input name='Submit43' type='button' class='short-short-button' value='条码' onclick='addProductCode("+rows[i].get("pc_id",0l)+")' title='"+pcodes+"'/>";
			if(!StringUtil.isBlank(amozonCode))
			{
				p_codes_str += "Amazon:"+amozonCode+"<br/>";
			}
			if(!StringUtil.isBlank(upcCode))
			{
				p_codes_str += "UPC:"+upcCode;
			}
			rows[i].add("p_codes",p_codes_str);
			rows[i].add("length_uom_name", lengthUOMKey.getLengthUOMKey(rows[i].get("length_uom", 0)));
			rows[i].add("weight_uom_name", weightUOMKey.getWeightUOMKey(rows[i].get("weight_uom", 0)));
			rows[i].add("price_uom_name", priceUOMKey.getMoneyUOMKey(rows[i].get("price_uom", 0)));
		}		
		
		DBRow data = new DBRow();
		data.add("page",pages);
		data.add("total",pc.getPageCount());
		
		data.add("rows",rows);
		
		oldpages = pages;
		data.add("records",pc.getAllCount());
		//System.out.println(new JsonObject(data).toString());
		out.println(new JsonObject(data).toString());
	}
%>
