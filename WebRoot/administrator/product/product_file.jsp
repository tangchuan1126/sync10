<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long pc_id = StringUtil.getLong(request,"pc_id");

DBRow product = productMgr.getDetailProductByPcid(pc_id);

DBRow[] productFiles = productMgrZJ.getProductFileByPcid(pc_id, true, true);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>商品图片</title>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form action="product_upload_show.html" name="upload_form" enctype="multipart/form-data" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
     <tr>
        <td align="left" valign="top"><br/>
            <table width="95%" align="center" cellpadding="0" cellspacing="0" >
				<tr>
					<td align="center" style="font-family:'黑体';border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;">
					<font size="25"><%=product.getString("p_name")%></font>
					</td>
				</tr>
			</table>
			<table width="95%" align="center" cellpadding="0" cellspacing="0" >
			<%
				for(int i = 0;i<productFiles.length;i++)
				{
			%>
				<tr>
					<td align="center"><font size="16"><strong><%=productFiles[i].getString("file_name")%></strong></font></td>
				</tr>
				<tr>
					<td align="center"><img src="../../upload/product/<%=productFiles[i].getString("file_name")%>"/></td>
				</tr>
			<%
				}
			%>
				<tr>
					
				</tr>
			</table>
		</td>
	</tr>
            
  </table>
</form>
</body>
</html>
