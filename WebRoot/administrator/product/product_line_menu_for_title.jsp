<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="../../include.jsp"%>
<%
	String product_line_id = StringUtil.getString(request,"product_line_id");
	String title_id = StringUtil.getString(request, "title_id");
	//System.out.println("getPcLineTitle:"+title_id);
%>
<ul id="productLinemenu" class="mcdropdown_menu">
	<li rel="0">所有产品线</li>
	<%
 		DBRow p1[] = proprietaryMgrZyj.findProductLinesByTitleId(true, 0L, 0L+"", title_id, 0, 0, null, request);
 			
 			//productLineMgrTJH.getAllProductLine();
		for (int i=0; i<p1.length; i++)
		{
			out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
			out.println("</li>");
		}
 	%>
</ul>
<input type="text" name="productLine" id="productLine" value="" />
<input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
<script type="text/javascript">
$("#productLine").mcDropdown("#productLinemenu",{
	allowParentSelect:true,
	  select: 
		function (id,name)
		{
			$("#filter_productLine").val(id);
			ajaxLoadCatalogMenuPage(id,'<%=title_id%>',0);
		}
});
$("#productLine").mcDropdown("#productLinemenu").setValue(<%=product_line_id%>);
</script> 