<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.ProductStoreOperationKey"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@ include file="../../include.jsp"%> 
<%
	String type = StringUtil.getString(request,"type");
	long ps_id = StringUtil.getLong(request,"ps_id");
	long pc_id = StringUtil.getLong(request,"pc_id");
	
	int[] purchase_status = {PurchaseKey.OPEN,PurchaseKey.AFFIRMPRICE,PurchaseKey.AFFIRMTRANSFER,PurchaseKey.AFFIRMTRANSFER,PurchaseKey.DELIVERYING};
	int[] delivery_status = {DeliveryOrderKey.READY,DeliveryOrderKey.INTRANSIT,DeliveryOrderKey.APPROVEING};
	int[] transport_status = {TransportOrderKey.PACKING,TransportOrderKey.INTRANSIT,TransportOrderKey.APPROVEING};

	DBRow[] rows = null;
	
	if(type.equals("purchaseCount"))
	{
		rows = productStorageMgrZJ.getPurchaseCount(ps_id,pc_id,purchase_status,false);
	}
	else
	{
		rows = productStorageMgrZJ.getDeliveryingCount(ps_id,pc_id,delivery_status,transport_status,false);
	}
	
	
%>
<html>
  <head>
    <title>采购及在途数</title>
    <link href="../comm.css" rel="stylesheet" type="text/css"/>
    <script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
  </head>
  
  <body>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr>
    		<th class="left-title " style="vertical-align: center;text-align: center;">相关单</th>
    		<th class="left-title " style="vertical-align: center;text-align: center;">商品名称</th>
    		<th class="left-title " style="vertical-align: center;text-align: center;">数量</th>
    		<th class="left-title " style="vertical-align: center;text-align: center;">预计到达时间</th>
    	</tr>
    	<%
    		for(int i=0;i<rows.length;i++)
    		{
    	%>
    		<tr>
    			<td><%=rows[i].getString("type")+rows[i].getString("bill_id")%></td>
    			<td><%=rows[i].getString("p_name")%></td>
    			<td><%=rows[i].getString("count")%></td>
    			<td><%=rows[i].getString("eta")%>&nbsp;</td>
    		</tr>
    	<%
    		}
    	%>
    </table>
  </body>
</html>
