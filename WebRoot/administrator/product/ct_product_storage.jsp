<%@page import="com.cwc.app.api.zj.SQLServerMgrZJ"%>
<%@page contentType="text/html;charset=UTF-8"%>
<%@page import="org.directwebremoting.json.types.JsonArray"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@page import="javax.mail.Transport"%>
<%@include file="../../include.jsp"%> 
<%@page import="com.cwc.app.beans.AdminLoginBean"%>
<%@taglib uri="/turboshop-tag" prefix="tst" %>
<html>
<head>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>		
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>

<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">


<!--  自动填充 -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long adminId=adminLoggerBean.getAdid();
	//根据登录帐号判断是否为客户
	Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
	//如果是用户登录 查询用户下的产品
	DBRow[] titles;
	DBRow[] p1;
	long number=0;
	if(bl){
		number=1;
		titles=adminMgrZwb.getProprietaryByadminId(adminLoggerBean.getAdid());
	}else{
		number=0;
		titles=mgrZwb.selectAllTitle();
	}
	DBRow[] lotNumberRow=storageCatalogMgrZyj.findProductLotNumberIdAndNameByTitlePcIdAdmin(true,0,"",0,null,request);  //批次
	//DBRow[] treeRows = catalogMgr.getProductStorageCatalogTree();   //仓库
	DBRow[] treeRows =storageCatalogMgrZyj.getProductStorageCatalogByType(StorageTypeKey.SELF, null);
%>
<script type="text/javascript">
function loadData(){
	var titles = <%=new JsonObject(titles).toString()%> ;
	var lotNumber=<%=new JsonObject(lotNumberRow).toString()%> ;
	var treeRows=<%=new JsonObject(treeRows).toString()%> ;
	var item=[];
	var cangku=[];
	for(var i=0;i<titles.length;i++){
		var op={};       
		op.id=titles[i].title_id;                                                                                                                                                                                                                                                                                                                                                                                                      
		op.name=titles[i].title_name;
		item.push(op);
	}
	for(var a=0;a<treeRows.length;a++){
		var op={};       
		op.id=treeRows[a].id;                                                                                                                                                                                                                                                                                                                                                                                                      
		op.name=treeRows[a].title;
		cangku.push(op);
	}
	var data=[{
				key:'TITLE：',
				type:'title',
				son_key:'产品线：',
				level:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLotNumberByTitleAction.action',
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadProductLineAction.action',
				
				son_key2:'一级分类：',
				url2:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadFirstProductCatalogAction.action',
				
				son_key3:'二级分类：',
				url3:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadSecondProductCatalogAction.action',	
						
				son_key4:'三级分类：',
				url4:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadThirdProductCatalogAction.action',

				array:item
			 },
			 {key:'仓库：',type:'cangku',array:cangku},
			 {key:'库存状态：',select:'true',type:'kucun',array:[{id:'<%=ProductStatusKey.IN_STORE%>',name:'有货'},{id:'<%=ProductStatusKey.STORE_OUT%>',name:'缺货'},{id:'<%=ProductStatusKey.NO_STORE%>',name:'无库存（库存为0）'}]},
			 {key:'商品类型：',select:'true',type:'leixing',array:[{id:'0',name:'散件'},{id:'1',name:'套装'}]},
			// {key:'批次：',type:'pici',array:lotNumber}
			 
			
	 		 ];
	initializtion(data);  //初始化
}

var title_id='';
var line_id='';
var catalog_id='';
var cangku='';
var lot_number='';
var kucun_type='';
var shangpin_type='';

//点击查询条件
function custom_seach(){
   var array=getSeachValue();
    title_id='';
    line_id='';
    catalog_id='';
    cangku='';
    lot_number='';
    kucun_type='';
    shangpin_type='';
   for(var i=0;i<array.length;i++){
	   if(array[i].type=='title'){
		   title_id=array[i].val;
	   }
	   if(array[i].type=='title_son'){
		   line_id=array[i].val;
	   }
	   if(array[i].type=='title_son_son'){
		   catalog_id=array[i].val;
	   }else if(array[i].type=='title_son_son_son'){
		   catalog_id=array[i].val;
	   }else if(array[i].type=='title_son_son_son_son'){
		   catalog_id=array[i].val;
	   }
	   if(array[i].type=='cangku'){
		   cangku=array[i].val;
	   }
	   if(array[i].type=='kucun'){
		   kucun_type=array[i].val;
	   }
	   if(array[i].type=='leixing'){
		   shangpin_type=array[i].val;
	   }
	   if(array[i].type=='pici'){
		   lot_number=array[i].val;
	   }
   }
   $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
 //  alert("title:"+title_id);
 //  alert("产品线:"+line_id);
 //  alert("产品分类:"+catalog_id);
 //  alert("仓库类型:"+cangku);
 //  alert("批次:"+lot_number);
 //  alert("商品类型:"+shangpin_type);
 //  alert("库存状态:"+kucun_type);
   var para='lot_number_id='+lot_number+'&cmd=cid&title_id='+title_id+'&type='+kucun_type+'&pcid='+catalog_id+'&union_flag='+shangpin_type+'&type='+kucun_type+'&pro_line_id='+line_id+'&cid='+cangku;
   $.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/ct_product_storage_show_list.html',
		type: 'post',
		dataType: 'html',
		data:para,
		async:false,
		success: function(html){
			$.unblockUI();   //遮罩关闭
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
			
		}
	});	
}

function go(number){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var para='lot_number_id='+lot_number+'&cmd=cid&title_id='+title_id+'&type='+kucun_type+'&pcid='+catalog_id+'&union_flag='+shangpin_type+'&type='+kucun_type+'&pro_line_id='+line_id+'&p='+number;
	   $.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/ct_product_storage_show_list.html',
			type: 'post',
			dataType: 'html',
			data:para,
			async:false,
			success: function(html){
				$.unblockUI();   //遮罩关闭
				$("#showList").html(html);
				onLoadInitZebraTable(); //重新调用斑马线样式
				
			}
		});	
}

function searchByName(){
	
	if ($("#filter_name").val()==""){
		alert("请填写关键词");
		return false;
	}
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var name=$("#filter_name").val();
//	 title_id='';
//	 line_id='';
//	 catalog_id='';
//	 cangku='';
//	 lot_number='';
//	 kucun_type='';
//	 shangpin_type='';
	 var para='lot_number_id='+lot_number+'&title_id='+title_id+'&type='+kucun_type+'&pcid='+catalog_id+'&union_flag='+shangpin_type+'&type='+kucun_type+'&pro_line_id='+line_id+'&name='+name+'&cmd=name';
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/ct_product_storage_show_list.html',
		type: 'post',
		dataType: 'html',
		data:para,
		async:false,
		success: function(html){
			$.unblockUI();   //遮罩关闭
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
			
		}
	});	
}
</script>
<title>存货与备货</title>
</head>

<body onload="loadData()">
<br/>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title">
   	   <img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   库存管理
    </td>
  </tr>
</table>
<br/>
<div id="tabs">
	<ul>
		<li><a href="#av1">过滤条件</a></li>
		<li><a href="#av2">商品搜索</a></li>		 
	</ul>
	<div id="av1">
		<div id="av"></div>
		<input type="hidden" id="atomicBomb" value="<%=number%>"/>
		<input type="hidden" id="title" value="0" />
	</div>
	<div id="av2">
        <input name="filter_name" type="text" id="filter_name" value="" style="width:320px;" > &nbsp;&nbsp;&nbsp;
        <input name="Submit3" type="button" class="button_long_search" value="搜索" onClick="searchByName()">
    </div>
</div>
<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,  
		cookie: { expires: 30000 } ,
	});
</script>
<br/>
<div align="right">
 	<input name="Submit2" type="button" class="long-button-ok" onClick="unionSplitAdvice()" value="联合建议">
	<input name="Submit2322" type="button" class="short-button-yellow" onClick="reCheckLackingOrders()" value="重算缺货" >
	<input type="button" class="long-button-green" onClick="demandAndPlanAnalysis()" value="计算备货与需求"/>
	<input name="button2" type="button" value="成本下载" onClick="downloadStorageCost()" class="long-button-export"/>
	<input name="button2" type="button" value="成本上传" onClick="uploadCostStorage()" class="long-button-upload"/>
	<input name="button2" type="button" value="下载" onClick="downloadProductStorage()" class="long-button-export"/>
	<input name="button2" type="button" value="上传" onClick="uploadProductStorage()" class="long-button-upload"/>
</div> 
<br/>
<div id="showList"></div>
</body>
</html>
<script>
(function(){	
	
	$.blockUI.defaults = {
			 css: { 
			  padding:        '8px',
			  margin:         0,
			  width:          '170px', 
			  top:            '45%', 
			  left:           '40%', 
			  textAlign:      'center', 
			  color:          '#000', 
			  border:         '3px solid #999999',
			  backgroundColor:'#ffffff',
			  '-webkit-border-radius': '10px',
			  '-moz-border-radius':    '10px',
			  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			 },
			 //设置遮罩层的样式
			 overlayCSS:  { 
			  backgroundColor:'#000', 
			  opacity:        '0.6' 
			 },
			 
			 baseZ: 99999, 
			 centerX: true,
			 centerY: true, 
			 fadeOut:  1000,
			 showOverlay: true
			};
})();

function reCheckLackingOrders(){
	$.blockUI.defaults = {
		css: { 
			padding:        '10px',
			margin:         0,
			width:          '200px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #aaa',
			backgroundColor:'#fff'
		},
		// 设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.8' 
		},
    centerX: true,
    centerY: true, 
		fadeOut:  2000
	};		
	$.prompt(
	"<div id='title'>重算缺货</div>确定重新计算缺货订单库存？<br />",
	{
   		  loaded:
				function (){	
				},
		  callback: 
				function (v,m,f){
					if (v=="y"){
						var ps_id = $("#filter_cid").val();
							$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">计算中，请稍后......</span>'});
							$.ajax({
								url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/ReCheckLackingOrders.action',
								type: 'post',
								dataType: 'html',
								timeout: 60000,
								cache:false,
								data:{ps_id:ps_id},
								beforeSend:function(request){
									
								},
								error: function(){
									alert("网络错误，请重试");
								},
								success: function(msg){
									if (msg=="ok"){
										$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">计算完成！</span>' });
										$.unblockUI();
										//alert("计算完成！");
										
									}else{
										alert("计算出错！");
									}
								}
							});		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}
function demandAndPlanAnalysis(){
	$.blockUI.defaults = {
		css: { 
			padding:        '10px',
			margin:         0,
			width:          '200px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #aaa',
			backgroundColor:'#fff'
		},
		// 设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.8' 
		},
    centerX: true,
    centerY: true, 
		fadeOut:  2000
	};
	$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">计算中，请稍后......</span>'});
		
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/orderProcess/demandAndPlanAnalysis.action',
			type: 'post',
			dataType: 'html',
			timeout: 300000,
			cache:false,
			data:"",
			beforeSend:function(request){
				
			},
			error: function(){
				alert("网络错误，请重试");
				$.unblockUI();
			},	
			success: function(msg){
				if (msg=="ok"){
					$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">计算完成！</span>' });
					$.unblockUI();
				}else{
					alert("计算出错！");
				}
			}
		});	
}
function downloadStorageCost(){
	var ps_id = $("#filter_cid").val();
	var catalog_id = $("#filter_pcid").val();
	var product_line = $("#filter_productLine").val();
	var para = "ps_id="+ps_id+"&catalog_id="+catalog_id+"&product_line="+product_line;
	if(ps_id!=0){
	$.blockUI.defaults = {
			css: { 
				padding:        '10px',
				margin:         0,
				width:          '200px', 
				top:            '45%', 
				left:           '40%', 
				textAlign:      'center', 
				color:          '#000', 
				border:         '3px solid #aaa',
				backgroundColor:'#fff'
			},
			// 设置遮罩层的样式
			overlayCSS:  { 
				backgroundColor:'#000', 
				opacity:        '0.8' 
			},
	    centerX: true,
	    centerY: true, 
			fadeOut:  2000
		};
		$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">生成文件中......</span>'});
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_storage/downloadStorageCostAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 600000,
			cache:false,
			data:para,
			beforeSend:function(request){
			},
			error: function(e){
				alert(e);
				alert("提交失败，请重试！");
			},
			success: function(date){
				if(date["canexport"]=="true"){
					$.unblockUI();
					document.download_form.action=date["fileurl"];
					document.download_form.submit();
				}else{
					alert("无法下载！");
				}
			}
		});
	}else{
		alert("请选择一个仓库再下载！")
	}
}
function uploadCostStorage(){
	tb_show('库存金额与运费','storage_cost_upload_excel.html?TB_iframe=true&height=500&width=800',false);
}
function downloadProductStorage(){
	var ps_id = $("#filter_cid").val();
	var catalog_id = $("#filter_pcid").val();
	var product_line = $("#filter_productLine").val();
	var para = "ps_id="+ps_id+"&catalog_id="+catalog_id+"&product_line="+product_line;
	$.blockUI.defaults = {
			css: { 
				padding:        '10px',
				margin:         0,
				width:          '200px', 
				top:            '45%', 
				left:           '40%', 
				textAlign:      'center', 
				color:          '#000', 
				border:         '3px solid #aaa',
				backgroundColor:'#fff'
			},
			// 设置遮罩层的样式
			overlayCSS:  { 
				backgroundColor:'#000', 
				opacity:        '0.8' 
			},
	    centerX: true,
	    centerY: true, 
			fadeOut:  2000
		};
		$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">生成文件中......</span>'});
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_storage/downloadProductStorage.action',
			type: 'post',
			dataType: 'json',
			timeout: 600000,
			cache:false,
			data:para,
			beforeSend:function(request){
			},
			error: function(e){
				alert(e);
				alert("提交失败，请重试！");
			},
			success: function(date){
				if(date["canexport"]=="true"){
					$.unblockUI();
					document.download_form.action=date["fileurl"];
					document.download_form.submit();
				}else{
					alert("无法下载！");
				}
			}
		});
}
function uploadProductStorage(){
	tb_show('拆散套装','storage_alert_upload_excel.html?TB_iframe=true&height=500&width=800',false);
}
</script>

