<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long storage_id = StringUtil.getLong(request,"storage_id");
String storage_name = StringUtil.getString(request,"storage_name");
long pid = StringUtil.getLong(request,"pid");
DBRow detail = productMgr.getDetailProductByPcid(pid);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加商品</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<script>
function addProduct1()
{
	var f = document.add_product_form;

	if (typeof $("input#preDamaged:checked").val()== "undefined")
	{
		alert("请选择退件检测情况");
	}
	else if (!isNum(f.proOid.value))
	{
		alert("请正确填写订单号");
	}
	else if (!isNum(f.proQuantity.value))
	{
		alert("请正确填写退件数量");
	}
    else
	{										
		parent.document.add_return_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addReturnProduct.action";
		parent.document.add_return_product_form.quantity.value = f.proQuantity.value;
		parent.document.add_return_product_form.pid.value = <%=storage_id%>;
		parent.document.add_return_product_form.oid.value = f.proOid.value;
		parent.document.add_return_product_form.damaged.value = $("input#preDamaged:checked").val();
		parent.document.add_return_product_form.submit();
	}
}

function addProduct()
{
	var f = document.add_product_form;

	if (f.proOid.value!=""&&!isNum(f.proOid.value))
	{
		alert("请正确填写订单号");
	}
	else if (!isNum(f.proQuantity.value))
	{
		alert("请正确填写退件数量");
	}
    else
    {
    	document.return_product.quantity.value = f.proQuantity.value;
		document.return_product.pid.value = <%=storage_id%>;
		document.return_product.oid.value = f.proOid.value;
		document.return_product.submit();
    }
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

<form action="return_product.html" name="return_product">
	<input type="hidden" name="pid" id="pid"/>
	<input type="hidden" name="oid" id="oid">
	<input type="hidden" name="quantity" id="quantity"/>
</form>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_product_form" method="post" action="" >

<table width="98%" border="0" cellspacing="5" cellpadding="2">

  <tr>
    <td colspan="2" align="left" valign="middle" class="text-line" style="border-bottom:1px solid #999999"><table width="100%" border="0" cellpadding="5" cellspacing="0" >
      <tr>
        <td width="96%" style="color:#666666;font-size:30px;font-weight:bold;font-family:Arial, 宋体"><span style="color:#990000"><%=storage_name%> - </span><%=detail.getString("p_name")%></td>
      </tr>
      <tr>
        <td style="color:#666666;font-size:13px;font-family:宋体">收到退件......</td>
      </tr>
    </table></td>
    </tr>
  
  <tr>
    <td colspan="2" align="left" valign="middle" class="text-line" >&nbsp;</td>
    </tr>
 <%-- 
  <tr>
    <td width="13%" align="left" valign="middle" class="STYLE1 STYLE2" >退件检测</td>
    <td width="87%" align="left" valign="middle" >
      <input type="radio" name="preDamaged" id="preDamaged" value="0">
   
      完好
      <input type="radio" name="preDamaged" id="preDamaged"  value="1">
      功能残损
	  <input type="radio" name="preDamaged" id="preDamaged"  value="2">
      包装残损
	  </td>
  </tr>
  --%>
  <tr>
    <td align="left" valign="middle" class="STYLE1 STYLE2" >相关订单号</td>
    <td align="left" valign="middle" ><label>
      <input type="text" name="proOid"  id="proOid" style="width:150px;">
    </label></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE1 STYLE2" >退件数量</td>
    <td align="left" valign="middle" ><input type="text" name="proQuantity" id="proQuantity" style="width:50px;"></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE1 STYLE2" >&nbsp;</td>
    <td align="left" valign="middle" >&nbsp;</td>
  </tr>
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="下一步" class="normal-green" onClick="addProduct();">
	 <input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close()">
	</td>
  </tr>
</table> 
</body>
</html>
