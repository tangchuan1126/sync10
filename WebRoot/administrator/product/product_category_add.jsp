<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
long parentid = StringUtil.getLong(request, "parentid");
String parentName = StringUtil.getString(request, "parentName", "");
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adminId = adminLoggerBean.getAdid();
Boolean bl = adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录

DBRow[] titles;
DBRow parentCategoryProductLine;
long selectedProductLine = 0;

if(bl){
     titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true, 0L, "", null, request);  //客户登录
}else{
	 titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(false, 0L, "", null, request);	
}
DBRow [] productLines = proprietaryMgrZyj.findProductLinesByTitleId(true, 0L, "", null, 0, 0, null, request);

//查询此分类下是否有商品
DBRow[] product = proprietaryMgrZyj.findProductByCategory(parentid == 0 ? -1 : parentid);

boolean existPro = false;

if(product != null && product.length > 0){
	
	existPro = true;
}

if(!"".equals(parentName)){
	
	parentCategoryProductLine = proprietaryMgrZyj.findProprietaryByCategoryId(parentid);
	
	if(parentCategoryProductLine!=null){
		
		selectedProductLine = parentCategoryProductLine.get("id",0L);
	}
}
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Product Category</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/simple.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<style type="text/css">
div.cssDivschedule{

	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
}

</style>

<script type="text/javascript">
$(function(){
	$(".cssDivschedule").css("display","none");
});

function SecondaryConfirm(){
	
	if(volidate()){
		
		if(<%=existPro%>){
			
			$.artDialog({
				
			    content: 'Products in category <%= parentName %> will be moved into category '+$("#title").val(),
			    icon: 'question',
			    width: 260,
			    height: 100,
			    opacity: 0.1,
			    lock: true,
			    fixed:true,
			    title:'Add Product Category',
			    okVal: 'Confirm',
			    ok: function () {
			    	
			    	submitData();
			    },
			    cancelVal: 'Cancel',
			    cancel: true
			});
			
		} else {
			
			submitData();
		}
	}
}

function submitData(){
	
	var title = $("#title").val();
		
	$(".cssDivschedule").css("display","block");
	
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProductCatalog.action',
		data:$("#subForm").serialize(),
		dataType:'json',
		type:'post',
		success:function(data){
			if(data && data.flag == "true"){
				showMessage("'"+title+"' added successfully","success");
				setTimeout("windowClose()", 1000);
			}else{
				$(".cssDivschedule").css("display","none");
				showMessage("Product Category '"+title+"' or Product Line '"+title+"' already exists","alert");
			}
		},
		error:function(){
			$(".cssDivschedule").css("display","none");
			showMessage("System error","error");
		}
	})
}
	
function volidate(){
	
	if(''==$("#title").val()) {
		
		showMessage("Please input product category name","alert");
		return false;
	}
	
	return true;
}

function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};

$(function(){
	$(".chzn-select").chosen({no_results_text: "no this option:"});
});
</script>

</head>
<body style="margin:0px;">
<div class="cssDivschedule" style="">
	
</div>
	<div style="height:100%;padding:5px;width:90%;">
		<form action="" id="subForm">
		<input type="hidden" name="parentid" value='<%=parentid %>'/>
			<table cellpadding="2" style="line-height:25px;width:90%;">
				<tr>
					<td width="30%" align="right">Parent Category:</td>
					<td><%="".equals(parentName)?"/":parentName %></td>
				</tr>
				<tr>
					<td width="30%" align="right">Category Name:</td>
					<td>
						<input name='title' type='text' id='title' style='width:300px;'>
					</td>
				</tr>
				
				<%
				if(parentName==null || "".equals(parentName))
				{
					if(0 != productLines.length)
					{
				%>
				<tr>
					<td width="30%" align="right">Select Product Line:</td>
					<td>
						<div class="side-by-side clearfix">
							<select name="productLineTitle" id="productLineTitle" class="chzn-select" data-placeholder="Choose a Product Line..." tabindex="1"  style="width:310px">
								<option value="">Choose a Product Line...</option>
							<%
								
								for(int i = 0; i < productLines.length; i ++)
								{
									DBRow pl = productLines[i];
							%>
								<option value='<%=pl.get("id", 0L) %>' <%if(selectedProductLine>0 && selectedProductLine==pl.get("id", 0L)) {%> selected="selected"<% }%>><%=pl.getString("name") %></option>
							<%		
								}
							%>
							</select>
						</div>
					</td>
				</tr>
				<% }
				}
				%>
			</table>
		</form>
		</div>
<div style="vertical-align:bottom;position:absolute;bottom:0px;width:100%" >
	<table align="right" width="100%">
		<tr align="right">
			<td class="win-bottom-line"><a class="buttons primary big" value="Submit" onclick="SecondaryConfirm();">Submit</a></td>
		</tr>
	</table>
</div>
</body>
</html>