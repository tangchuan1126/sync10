<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long cid = StringUtil.getLong(request,"cid");
int type = StringUtil.getInt(request,"type");
String name = StringUtil.getString(request,"name");
String cmd = StringUtil.getString(request,"cmd");

int export_flag = StringUtil.getInt(request,"export_flag");

DBRow rows[];

PageCtrl pc = new PageCtrl();
pc.setPageNo(1);
pc.setPageSize(25);
if ( cmd.equals("cid") )
{
	if ( export_flag==1 )
	{
		rows = productMgr.getAllProductStorageByPcid(cid,type,null); 
	}
	else
	{
		rows = productMgr.getAllProductStorageByPcid(cid,type,pc); 
	}

}
else if ( cmd.equals("name") )
{
	if ( export_flag==1 )
	{
		rows = productMgr.getProductStorageByName(name,null);
	}
	else
	{
		rows = productMgr.getProductStorageByName(name,pc);
	}
	

}
else
{
	if ( export_flag==1 )
	{
		rows = productMgr.getProductStorages(type,null);
	}
	else
	{
		rows = productMgr.getProductStorages(type,pc);
	}
	

}

DBRow detailCatalog = catalogMgr.getDetailProductStorageCatalogById(cid);
String lineBg;
DBRow proGroup[];
DBRow fathercatalog[];

//导出报表文件
if ( export_flag==1 )
{
	String pre_filename = "";
	
		if (detailCatalog==null)
		{
			pre_filename = "所有商品";
		}
		else
		{
			int k=0;
			fathercatalog = catalogMgr.getAllFatherCatalog(cid);
			for (; k<fathercatalog.length-1; k++)
			{
				pre_filename += fathercatalog[k].getString("title")+"-";
			}
			pre_filename += fathercatalog[k].getString("title");
		}
		
		if (type==0)
		{
			pre_filename += "下所有商品";
		}
		else
		{
			pre_filename += "下缺货商品";
		}
		
		pre_filename = java.net.URLEncoder.encode(pre_filename, "UTF-8"); 

		
	String d = DateUtil.NowStr();
	String filename = StringUtil.replaceString(d, "-", "");
	filename = StringUtil.replaceString(filename, " ", "");
	filename = StringUtil.replaceString(filename, ":", "");

	response.setContentType("text/plain;charset=gb2312"); 
	response.setHeader("Content-Disposition", "attachment; filename=\"" + pre_filename+"_"+filename + ".csv\"");
	
	out.write("所属类别,名称,存放位置,当前库存,单位,单价,条码\r\n");
	
	int i=0;
	for ( ; i<rows.length-1; i++ )
	{
		int j=0;
		fathercatalog = catalogMgr.getAllFatherCatalog(rows[i].get("cid",0l));
		for (; j<fathercatalog.length-1; j++)
		{
			out.write(fathercatalog[j].getString("title")+"-");
		}
		out.write(rows[i].getString("title")+",");
		
		out.write(rows[i].getString("p_name")+",");  
		out.write(rows[i].getString("store_station")+",");  
		out.write(rows[i].get("store_count",0f)+",");  
		out.write(rows[i].getString("per")+",");  
		out.write(rows[i].getString("price")+",");  
		out.write("*"+rows[i].getString("p_code")+"*\r\n");  
	}

		int j=0;
		fathercatalog = catalogMgr.getAllFatherCatalog(rows[i].get("cid",0l));
		for (; j<fathercatalog.length-1; j++)
		{
			out.write(fathercatalog[j].getString("title")+"-");
		}
		out.write(rows[i].getString("title")+",");
		
		out.write(rows[i].getString("name")+",");  
		out.write(rows[i].getString("store_station")+",");  
		out.write(rows[i].get("store_count",0f)+",");  
		out.write(rows[i].getString("per")+",");  
		out.write(rows[i].getString("price")+",");  
		out.write("*"+rows[i].getString("code")+"*");  

	return;
}

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>报表</title>
<link href="../../style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script>
function printInit()
{
	xprint.printing.header = "" //页眉 
	xprint.printing.footer = "" //页脚
	xprint.printing.portrait = true //控制横打还是竖打，true为竖打，false为横打
	xprint.printing.leftMargin = 0 //左边距 
	xprint.printing.topMargin = 0 //上边距 
	xprint.printing.rightMargin = 0 //右边距 
	xprint.printing.bottomMargin = 0 //下边距 
}

function printReport()
{
	printInit();

	xprint.printing.copies = 1
	xprint.printing.portrait = true //控制横打还是竖打，true为竖打，false为横打
	xprint.printing.Print(true) //直接打印，true:弹出选择打印机窗口,false:直接打印

}
</script>

<style>
.form
{
	padding:0px;
	margin:0px;
}
</style>
<style>
.PageNext{page-break-after: always;}<!--控制分页-->
</style>
<style media=print>  
.Noprint{display:none;}<!--用本样式在打印时隐藏非打印项目-->  
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<object id="xprint" style="display:none"
  classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814"
  codebase="http://www.1you.com/paypal/scriptx.cab#Version=6,1,431,2">
</object>
<table width="700" height="38" border="0" align="center" cellpadding="0" cellspacing="0" class="Noprint">
  <tr>
    <td width="1414"><label>
      <input type="button" name="Submit" value="打印报表" onClick="printReport()">
    </label></td>
  </tr>
</table>



<%
for (int kk=1; kk<=pc.getPageCount(); kk++)
{
	pc.setPageNo(kk);

	if ( cmd.equals("cid") )
	{
		rows = productMgr.getAllProductStorageByPcid(cid,type,pc);
	
	}
	else if ( cmd.equals("name") )
	{
		rows = productMgr.getProductStorageByName(name,pc);
	
	}
	else
	{
		rows = productMgr.getProductStorages(type,pc);
	
	}
%>

<div class="<%=kk!=pc.getPageCount()?"PageNext":""%>">
<table width="700" height="55" border="0" align="center" cellpadding="0" cellspacing="0" >
  <tr>
    <td width="323">报表：
		<%
		if (detailCatalog==null)
		{
			out.println("所有商品");
		}
		else
		{
			int k=0;
			fathercatalog = catalogMgr.getAllFatherCatalog(cid);
			for (; k<fathercatalog.length-1; k++)
			{
				out.println(fathercatalog[k].getString("title")+"-");
			}
			out.println(fathercatalog[k].getString("title"));
		}
		
		if (type==0)
		{
			out.println("下所有商品");
		}
		else
		{
			out.println("下缺货商品");
		}
		%>
	</td>
    <td width="165">日期：<%=DateUtil.NowStr()%></td>
    <td width="166" align="right" valign="middle">

	页数：<%=pc.getPageNo()%>/<%=pc.getPageCount()%></td>
  </tr>
</table>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="thetable">
  <form name="listForm" method="post">

	  <thead>
    <tr> 
	<td width="162" align="center" valign="middle"><strong>所属类别</strong></td>
        <td valign="middle"><strong>名称</strong></td>
        
        <td width="70" align="center" valign="middle"><strong>存放位置</strong></td>
        <td width="60" align="center" valign="middle"><strong>当前库存</strong></td>
        <td width="40" align="center" valign="middle"><strong>单位</strong></td>
        <td width="40" align="center" valign="middle"><strong>单价</strong></td>
        <td width="160" align="center" valign="middle"><strong>条码</strong></td>
        </tr>
	</thead>
    <%


for ( int i=0; i<rows.length; i++ )
{
	if ( i%2==0 )
	{
		lineBg = "background:#ffffff";
	}
	else
	{
		lineBg = "background:#eeeeee";
	}
	
	if ( rows[i].get("store_count",0f)<rows[i].get("store_count_alert",0f) )
	{
		lineBg = "background:#000000;color:#FFFFFF";
	}
%>
    <tr  > 
      <td height="40" align="center" valign="middle" style="<%=lineBg%>"   > 
	  
	  	<%
		int j=0;
		fathercatalog = catalogMgr.getAllFatherCatalog(rows[i].get("cid",0l));
		for (; j<fathercatalog.length-1; j++)
		{
			out.println(fathercatalog[j].getString("title")+"-");
		}
		//out.println(fathercatalog[j].getString("title"));
		%>
	  
      <%=rows[i].getString("title")%>        </td>
      <td width="158" valign="middle" style='word-break:break-all;' style="<%=lineBg%>" >
        
        <%=rows[i].getString("p_name")%>  </td>

      <td align="center" valign="middle" style="<%=lineBg%>" > <%=rows[i].getString("store_station")%>     </td>
	     <td align="center" valign="middle" style='word-break:break-all;' style="<%=lineBg%>" ><%=rows[i].getString("store_count")%>	</td>
      <td align="center" valign="middle" style="<%=lineBg%>" > <%=rows[i].getString("per")%>  </td>
      <td align="center" valign="middle" style="<%=lineBg%>" > <%=rows[i].getString("price")%>    </td>  
   
      <td align="center" valign="middle" style="font-family:'c39hrp48dmtt';font-size:36px;" style="<%=lineBg%>" >*<%=rows[i].getString("p_code")%>*</td>
    </tr>
    <%
}
%>
  </form>
</table>
</div>
<%
}
%>




</body>
</html>
