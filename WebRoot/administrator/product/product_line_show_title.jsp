<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long id = StringUtil.getLong(request,"id");
String name = StringUtil.getString(request,"name");

DBRow[] lineTitles = proprietaryMgrZyj.findProprietaryByAdidAndProductLineId(0L, id, 0L, null, request);

%>
<html>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Product Line Show Title</title>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css">

.panel table{width:100%;}

.panel table tr:nth-child(2n){
	
	background:#eeeeee;
}

.panel {
	
	margin-bottom: 20px;
	background-color: #fff;
	border: 1px #CFCFCF solid;
	-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
	box-shadow: 0 1px 1px rgba(0,0,0,.05);
	margin: 7px;
}

.panel .panelTitle{
	
	background: #f1f1f1;
	height: 40px;
	line-height: 40px;
	padding:0 10px;
	border-bottom: 1px #CFCFCF solid;
}

.panel .panelTitle .title-left{
	
	float: left;
}

.panel .panelTitle .title-right{
	float: right;
}

.clear{
	clear: both;
}

.right-title{

	height:40px;
	background-color:#fff;
}
.table {
  width: 100%;
  max-width: 100%;
}

.table>thead>tr>th {
  vertical-align: bottom;
  border-bottom: 2px solid #ddd;
  font-size: 14px;
}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
  padding: 8px;
  line-height: 1.42857143;
  vertical-align: top;
  border-top: 1px solid #ddd;
}

th {
  text-align: left;
}

table {
  border-spacing: 0;
  border-collapse: collapse;
}

table {
  background-color: transparent;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	
	<div class="panel">
		<div class="panelTitle">
			<div class="title-left">
				Product Line : <span style="  font-weight: bold;"><%=name %></span>
			</div>
		</div>
		
		<table class="table">
			<thead>
				<tr>
					<th>
						Title
					</th>
					<th>
						Customer
					</th>
				</tr>
			</thead>
			<tbody>
			
			<%
			if(lineTitles != null && lineTitles.length > 0 ){
				for(DBRow oneResult : lineTitles){%>
				<tr>
					<td>
						<%=oneResult.getString("title_name")%>
					</td>
					<td>
						<%=oneResult.getString("customer_id")%>
					</td>
				</tr>
				<%} 
			} else {%>
				<tr>
					<td colspan="100%" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">
						No Data.
					</td>
				</tr>
			<%} %>
			</tbody>
		</table>
	</div>
</body>
</html>