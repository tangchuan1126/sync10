<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long storage_id = StringUtil.getLong(request,"storage_id");
long pid = StringUtil.getLong(request,"pid");
float store_count = StringUtil.getFloat(request,"store_count");
float damage_count = StringUtil.getFloat(request,"damage_count");
String storage_name = StringUtil.getString(request,"storage_name");

DBRow detail = productMgr.getDetailProductByPcid(pid);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加商品</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script>
function addProduct()
{
	var f = document.add_product_form;
	
	if (!isNum(f.quantity.value))
	{
		alert("请正确填写数量");
	}
	else if (<%=store_count%><f.quantity.value)
	{
		alert("残损件数不能比现有库存多");
	}
    else
	{										
		parent.document.add_damaged_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addDamageProduct.action";
		parent.document.add_damaged_product_form.quantity.value = f.quantity.value;
		parent.document.add_damaged_product_form.pid.value = <%=storage_id%>;
		parent.document.add_damaged_product_form.submit();
	}
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_product_form" method="post" action="" >

<table width="98%" border="0" cellspacing="5" cellpadding="2">

  <tr>
    <td colspan="2" align="left" valign="middle" class="text-line" style="border-bottom:1px solid #999999"><table width="100%" border="0" cellpadding="5" cellspacing="0" >
      <tr>
        <td width="96%" style="color:#666666;font-size:30px;font-weight:bold;font-family:Arial, 宋体"><span style="color:#990000"><%=storage_name%> - </span><%=detail.getString("p_name")%></td>
      </tr>
      <tr>
        <td style="color:#666666;font-size:13px;font-family:宋体">新增残损......</td>
      </tr>
    </table></td>
    </tr>
  
  <tr>
    <td colspan="2" align="left" valign="middle" class="text-line" >&nbsp;</td>
    </tr>
  <tr>
    <td width="7%" align="left" valign="middle" class="STYLE1 STYLE2" >数量</td>
    <td width="93%" align="left" valign="middle" ><input name="quantity" type="text" class="input-line" id="quantity" style="width:140px;" ></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="middle" class="STYLE1 STYLE2" ><table width="312" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="left" valign="middle" style="color:#999999">当前库存：<%=store_count%>&nbsp;&nbsp;&nbsp;当前残损：<%=damage_count%></td>
        </tr>
    </table></td>
    </tr>
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="增加" class="normal-green" onClick="addProduct();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
	</td>
  </tr>
</table> 
</body>
</html>
