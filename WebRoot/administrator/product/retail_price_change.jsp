<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="retailPriceKey" class="com.cwc.app.key.RetailPriceKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

DBRow retailPrices[] = quoteMgr.getAllRetailPrice(pc);

%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>调价记录</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />


<style type="text/css">
<!-- 


-->
</style>

<script>
function closeTBWin()
{
	tb_remove();
}

</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">


<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 客户与报价应用 »   调价记录</td>
  </tr>
</table>


<br>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
<thead>
<tr>
  <th width="24%" align="left"  class="right-title"  style="text-align:left;" >调价原因</th>
  <th width="16%" align="left"  class="right-title"  style="text-align:center;" >发货仓库-销售地区</th>
  <th width="14%" align="left"  class="right-title"  style="text-align:center;" >申请人</th>
    <th width="14%"  class="left-title" style="text-align: center;">申请日期</th>
    <th width="16%"  class="left-title" style="text-align: center;">状态</th>
    <th width="16%"   class="right-title" style="text-align: center;">&nbsp;</th>
</tr>
</thead>
<%
for (int i=0; i<retailPrices.length; i++)
{
%>
  <tr>
    <td align="left" valign="middle"  ><%=retailPrices[i].getString("reasons")%></td>
    <td align="center" valign="middle"  >
	<%
	String ware_house = catalogMgr.getDetailProductStorageCatalogById(retailPrices[i].get("ps_id",0l)).getString("title");
	String country_area = productMgr.getDetailCountryAreaByCaId(retailPrices[i].get("ca_id",0l)).getString("name");
	out.println(ware_house+" -> "+country_area);
	%>
	</td>
    <td height="84" align="center" valign="middle"  ><%=retailPrices[i].getString("ask")%></td>
    <td height="84" align="center" valign="middle" ><%=retailPrices[i].getString("ask_date")%></td>
    <td height="84" align="center" valign="middle" style="line-height:20px;">
	<%=retailPrices[i].getString("approve")%><br>
	<%=retailPriceKey.getRetailPriceById(retailPrices[i].get("handle_status",0))%>
	<%
	if (retailPrices[i].get("handle_status",0)==com.cwc.app.key.RetailPriceKey.REJECT)
	{
		out.println("<br>("+retailPrices[i].getString("reject_reasons")+")");
	}
	%>
	</td>
    <td align="center" valign="middle"  >
      <input name="Submit" type="button" class="short-button" value="查看详细" onClick="window.location='retail_price_change_products.html?rp_id=<%=retailPrices[i].getString("rp_id")%>'">   </td>
  </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
    <input type="hidden" name="p">

	
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
</body>

</html>