<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ include file="../../../include.jsp"%>
<%
String[] copies = StringUtil.getString(request,"copies").split(",");
String[] pc_id = StringUtil.getString(request,"pc_id").split(",");


float mm_pix = 3.78f;
float print_range_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"))*mm_pix;//99
float print_range_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"))*mm_pix;//48.8f
float print_range_left = 0*mm_pix;
float print_range_top = 0*mm_pix;

String printer = StringUtil.getString(request,"printer");
String paper = StringUtil.getString(request,"paper");

float pnameheight;
float borderWidth = 2;

int printsumCount = 0;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>打印商品条码</title>

<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../../js/popmenu/common.js"></script>
		<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		
<script>

function printInit()
{
	xprint.printing.header = "" //页眉 
	xprint.printing.footer = "" //页脚
	xprint.printing.portrait = true //控制横打还是竖打，true为竖打，false为横打
	xprint.printing.leftMargin = 1.5 //左边距 
	xprint.printing.topMargin = 1 //上边距 
	xprint.printing.rightMargin = 0 //右边距  
	xprint.printing.bottomMargin = 0 //下边距 
}

function doPrint(pc_id)
{
	beforePrint(pc_id); 

	xprint.printing.Print(false); //直接打印，true:弹出选择打印机窗口,false:直接打印 
	
	afterPrint(pc_id);
}

function beforePrint(pc_id)
{
	document.getElementById("container_"+pc_id).style.border = "0px";
	//document.getElementById("container2").style.border = "0px";



}

function afterPrint(pc_id)
{

	document.getElementById("container_"+pc_id).style.border = "1px #000000 solid";
	//document.getElementById("container2").style.border = "1px #000000 solid";

}

function pos(obj,x,y)
{
	obj.style.position="absolute";

	obj.style.left=x*<%=mm_pix%>;
	obj.style.top=y*<%=mm_pix%>;

}
//---------------------------------------

function printBarCode()
{
		//document.getElementById("container").style.display = "none";
		var pcids = "<%=StringUtil.getString(request,"pc_id")%>";
		var pcid = pcids.split(",");
		var copies = "<%=StringUtil.getString(request,"copies")%>";
		var copie = copies.split(",");
		xprint.printing.printer="<%=printer%>"//LabelPrinter
		xprint.printing.paperSize = "<%=paper%>"; //纸张100X50
	
		for(q=0;q<pcid.length;q++)
		{
			pos(document.getElementById("container_"+pcid[q]),0,0);
			pos(document.getElementById("printcopies_"+pcid[q]),100,0);
			document.getElementById("container_"+pcid[q]).className="container";
			document.getElementById("printcopies_"+pcid[q]).style.display="none";
			xprint.printing.copies = copie[q];
			doPrint(pcid[q]);
			document.getElementById("container_"+pcid[q]).style.display = "";
			
			if(xprint.printing.WaitForSpoolingComplete())
			{
				document.getElementById("container_"+pcid[q]).style.display = "none";
				document.getElementById("printcopies_"+pcid[q]).style.display = "none";
				if(q==pcid.length-1)
				{
					window.close();
				}
			}
		}
		
}

var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}
  
</script>

<style>

.container
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height%>px;

	position:absolute;
	left:<%=print_range_left%>px;
	top:<%=print_range_top%>px;
	clip:rect(0,<%=print_range_width+2%>,<%=print_range_height%>,0);
	font-size:12px;
	font-weight:normal;
	border:1px red solid;
	font-family:Verdana;
}

#container2
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height%>px;

	position:absolute;
	left:<%=print_range_left%>px;
	top:<%=300f*3.78f%>px;
	clip:rect(0,<%=print_range_width%>,<%=print_range_height%>,0);
	font-size:12px;
	font-weight:normal;
	border:"1px #000000 solid";
	font-family:Verdana;
}

.num
{
	font-family:Arial;
}
.STYLE1 {color: #000000;font-weight:bold;font-size:18px;}

.STYLE2 {color: #000000;font-weight:bold;font-size:16px;}

td
{
	font-size:10px;
}

.print-code
{
	font-family: C39HrP48DmTt;font-size:40px;
}
</style>



<style media=print>  
.Noprint{display:none;}<!--用本样式在打印时隐藏非打印项目-->

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="printInit();" >
<object id="xprint" style="display:none"
  classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814"
  codebase="http://www.1you.com/paypal/scriptx.cab#Version=6,1,431,2">
</object>
<div id="keep_alive" style="display:none"></div>


<% 
	for(int c=0;c<pc_id.length;c++)
	{
		DBRow product = productMgr.getDetailProductByPcid(Long.parseLong(pc_id[c]));
		String[] pname = product.getString("p_name").split("/");
		String[] barcode = product.getString("p_code").split("/");
		
		if(pname.length>2)
		{
			pnameheight = 0.3f;
		}
		else
		{
			pnameheight = 0.6f;
		}
%>
	<div id="container_<%=product.get("pc_id",0l) %>" align="center" class="container Noprint">
<!------------------------------------------------->
<table width="<%=print_range_width%>px" height="<%=print_range_height-5%>" border="0" bordercolor="#000000" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" valign="middle">
		<table width="<%=print_range_width-borderWidth*2%>px" height="<%=(print_range_height-3)*pnameheight%>px" style="border-left:<%=borderWidth%>px #000000 solid;border-top:2px #000000 solid;border-right:<%=borderWidth%>px #000000 solid;" cellpadding="0" cellspacing="0">
			<tr>
				<%
					if(pname.length>1)
					{
				%>
					<td class="STYLE1" width="<%=pname[0].length()/(pname[0]+pname[1]).length()%>%" align="center" valign="middle">
					<%=pname[0]%>
					</td>
					<td class="STYLE1" width="<%=pname[1].length()/(pname[0]+pname[1]).length()%>%" style="border-left:1px #000000 solid;" align="center" valign="middle">
					<%=pname[1]%>
					</td>
				<%
					}
					else
					{
				%>
						<td class="STYLE1" width="100%" align="center" valign="middle" style="border-left:2px #000000 solid;border-top:2px #000000 solid;border-right:2px #000000 solid;">
						<%=product.getString("p_name")%>
						</td>
				<%
					}
				%>
			</tr>
		</table>
		</td>
		</tr>
		<% 
			if(pname.length>2)
			{
		%>
		<tr><td align="center" valign="middle">
			<table width="<%=print_range_width-borderWidth*2%>px" height="<%=(print_range_height-3)*pnameheight%>px" style="border-left:<%=borderWidth%>px #000000 solid;border-right:<%=borderWidth%>px #000000 solid;" cellpadding="0" cellspacing="0">
				<tr>
					<%
						
						float tr2char = product.getString("p_name").substring((pname[0]+pname[1]).length()+1).length();
						for(int j = 2;j<pname.length;j++)
						{
							if(j<pname.length-2||j==2)
							{
					%>
								<td class="STYLE2" width="<%=pname[j].length()/tr2char%>%" align="center" valign="middle" style="border-top:1px #000000 solid;">
									<%=pname[j]%>
								</td>
					<%	
							}
							else
							{
					%>
							<td class="STYLE2" width="<%=pname[j].length()/tr2char%>%" align="center" valign="middle" style="border-top:1px #000000 solid;border-left:1px #000000 solid;">
									<%=pname[j]%>
								</td>
					<%
							}
						}
					%>
					
				</tr>
			</table>
		</td></tr>
		<%
			}
		%>
			<tr><td align="center" valign="middle">
			<table width="<%=print_range_width-borderWidth*2%>px" height="<%=(print_range_height-3)*0.4%>px" style="border-top:<%=borderWidth%>px #000000 solid;" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" style="border-top:1px #000000 solid;" valign="middle">
						<span class="print-code">*<%=product.getString("p_code")%>*</span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!------------------------------------------------->
</div>
<div align="center" id="printcopies_<%=product.get("pc_id",0l)%>" class="Noprint" style="font-weight:inherit;font-size:<%=print_range_height/3%>px;height:<%=print_range_height%>px;">
	&nbsp;&nbsp;X&nbsp;&nbsp;<%=copies[c]%>
</div>
<%printsumCount += Integer.parseInt(copies[c]);%>
<script type="text/javascript">
	pos(document.getElementById("container_<%=product.get("pc_id",0l)%>"),0,<%=40*c+15*(c+1)%>);
	pos(document.getElementById("printcopies_<%=product.get("pc_id",0l)%>"),100,<%=40*c+15*(c+1)%>)
</script>
	
<%
	}
%>
<table id="title" class="Noprint" style="height: 40px;" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr>
    <td style="background: none repeat scroll 0% 0% rgb(255, 255, 204); padding: 10px; font-size: 15px; font-weight: bold; border-bottom: 1px solid rgb(153, 153, 153); width: 50%;" align="left">
		<input type="button" value="打印条码" onClick="printBarCode()"/>&nbsp;&nbsp;打印总量<%=printsumCount%>
	</td>
    <td style="background: none repeat scroll 0% 0% rgb(255, 255, 204); padding: 10px; font-size: 13px; font-weight: bold; border-bottom: 1px solid rgb(153, 153, 153); width: 50%;" align="right">
	 <span style="font-family: Arial,Helvetica,sans-serif;"></span>
	&nbsp;&nbsp;
	</td>
  </tr>
</tbody></table>
<script type="text/javascript">
	pos(document.getElementById("title"),0,0);
</script>
</body>
</html>
