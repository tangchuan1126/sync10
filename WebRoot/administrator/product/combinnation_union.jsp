<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long psid = StringUtil.getLong(request,"psid");
long s_pid = StringUtil.getLong(request,"s_pid");
long s_pc_id = StringUtil.getLong(request,"s_pc_id");
long cid = StringUtil.getLong(request,"cid");

String storage_name = StringUtil.getString(request,"storage_name");
DBRow detailPro = productMgr.getDetailProductByPcid(s_pc_id);
int combinationCount = productMgr.getPrepareUnionCount(psid,s_pc_id);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script>

$().ready(function() {

	function log(event, data, formatted) {

		
	}
		
});

function checkForm()
{	
	var theForm = document.add_union_form;
	
	if( theForm.combination_quantity.value=="" )
	{
		alert("需要拼装数不能为空");
	}
	else if( !isNum(theForm.combination_quantity.value)||theForm.combination_quantity.value><%=combinationCount%>||theForm.combination_quantity.value<=0)
	{
		alert("需要拼装数超出允许范围");
	}
	else
	{
		parent.combinationProduct(<%=psid%>,<%=s_pc_id%>,theForm.combination_quantity.value)
	}
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_union_form" method="post" action="" onSubmit="return false">

<table width="98%" border="0" cellspacing="3" cellpadding="2">

  <tr>
    <td colspan="2" align="left" valign="middle" style="padding:10px;">
	<span style="font-weight:bold;color:#666666;font-size:20px;font-family:Arial, Helvetica, sans-serif;"><span style="color:#990000"><%=storage_name%> - </span><%=detailPro.getString("p_name")%></span>	</td>
  </tr>
  <tr>
    <td width="66%" height="106" align="left" valign="top" style="padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="3">
	<%
	DBRow inSetProducts[] = productMgr.getProductsInSetBySetPid(s_pc_id);
	for (int i=0; i<inSetProducts.length; i++)
	{
		//预计算下是否缺货，缺多少
		float sub_quantity = inSetProducts[i].get("quantity",0f);
		DBRow detailDetailProductProductStorage = productMgr.getDetailProductProductStorageByPcid(cid,inSetProducts[i].get("pc_id",0l));
		float store_count = detailDetailProductProductStorage.get("store_count",0f);

		String preFix = "";
		if (store_count-sub_quantity<0)
		{
			preFix = "<span style='color:#CC0000;font-weight:bold'>[缺] " + (-(store_count-sub_quantity)) + " " + detailDetailProductProductStorage.getString("unit_name")+"</span>";
		}
		else
		{
		}
	%>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif">├  <%=inSetProducts[i].getString("p_name")%> x <span style="color:#CC0000"><%=sub_quantity%></span> 
		&nbsp; <%=preFix%>		</td>
      </tr>
	<%
	}
	%>
    </table></td>
    <td width="34%" align="left" valign="top" style="padding:10px;">
	
	<table width="217" border="0" cellpadding="1" cellspacing="0" bgcolor="#cccccc">
      <tr>
        <td>

			<table width="217" border="0" cellpadding="5" cellspacing="0" bgcolor="#f8f8f8">
		<%
	if (combinationCount>0)
	{
	%>
      <tr>
        <td width="545" style="font-size:12px;">可预拼装 <span style="color:#990000;font-weight:bold"><%=combinationCount%></span> <%=detailPro.getString("unit_name")%></td>
        </tr>
      <tr>
        <td  style="font-size:12px;">需要拼装 
         <input type="text" name="combination_quantity" id="combination_quantity" style="width:40px;"> <%=detailPro.getString("unit_name")%>		 </td>
        </tr>
	<%
	}
	else
	{
	%>
			      <tr>
        <td  style="font-size:12px;color:#CC0000;background:#FFFFE1;">散件库存不足，不能预拼装！	 </td>
        </tr>
	<%
	}
	%>
    </table>		</td>
      </tr>
    </table></td>
  </tr>
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="66%" align="left" class="win-bottom-line" style="color:#999999;padding-left:10px;font-size:12px;">
	<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle">
	拼装后，散件库存减少，套装库存相应增加。</td>
    <td width="34%" align="right" class="win-bottom-line">
	<%
	if ( combinationCount>0 )
	{
	%>
      <input type="button" name="Submit2" value="拼装" class="normal-green" onClick="checkForm();">
<%
}
%>
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();"> 
	</td>
  </tr>
</table> 
</body>
</html>
