<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String permitFile = "csv";
String filePath = "/administrator/product/storage_location_file/";

TUpload upload = new TUpload();
			
upload.useOrgName();
upload.setRootFolder(filePath);
upload.setPermitFile(permitFile);

int flag = upload.upload(request);
String msg;
boolean haveError = false;

if (flag==2)
{
	msg = "只能上传:"+permitFile;

	out.println("<script>");
	out.println("alert('"+msg+"')");
	out.println("history.back();");
	out.println("</script>");
}
else if (flag==1)
{
	msg = "上传出错";

	out.println("<script>");
	out.println("alert('"+msg+"')");
	out.println("history.back();");
	out.println("</script>");
}
else
{
	msg = upload.getFileName();
	
	String csvRow[] = productMgr.getCsv(filePath+msg);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>提交日志</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style>
.form
{
	padding:0px;
	margin:0px;
}

</style>
<script>
function inp()
{
	document.product_form.submit();
	parent.closeWinRefrech();
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
			<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td width="63%" style="font-size:14px;font-weight:bold">当前文件:<span style="color:#FF0000"><%=msg%></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 记录数：<span style="color:#FF0000"><%=csvRow.length%></span></td>
				<td width="37%" align="right" valign="middle" style="font-size:14px;font-weight:bold">&nbsp;</td>
			  </tr>
			</table>
			<table width="90%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;">
			<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addStorageLocation.action" method="post" name="product_form">
			<input type="hidden" name="csv_name" value="<%=filePath+msg%>">
			<input type="hidden" name="backurl" value="<%=upload.getRequestRow().getString("backurl")%>">
			  <tr >
				<td width="47%" height="30"  class="left-title " style="vertical-align: center;text-align: left;">商品条码</td>
				<td width="14%" align="center"  class="left-title " style="vertical-align: center;text-align: left;">数 量</td>
				<td width="39%" align="center"  class="left-title " style="vertical-align: center;text-align: left;">存放位置</td>
			  </tr>
			<%
			String bgcolor="";
			String fontcolor  = "";
			
			for (int i=0; i<csvRow.length; i++)
			{
				if (csvRow[i].trim().equals("")||csvRow[i].trim().split(",").length!=3)
				{
					continue;
				}
				
				if ((i+1)%2==0)
				{
					bgcolor = "bgcolor='#f8f8f8'";
					fontcolor = "color:#000000";
				}
				else
				{
					bgcolor = "bgcolor='#ffffff'";
					fontcolor = "color:#000000";
				}
			
				String cols[] = csvRow[i].split(",");
			
			%>
			  <tr <%=bgcolor%>>
				<td width="47%" height="30" style="font-size:13px;<%=fontcolor%>;border-bottom: 1px solid #dddddd;padding-left:10px;"><%=cols[0]%></td>
				<td width="14%" align="center" style="font-size:13px;<%=fontcolor%>;border-bottom: 1px solid #dddddd;padding-left:10px;"><%=cols[1]%></td>
				<td width="39%" align="center" style="font-size:13px;<%=fontcolor%>;border-bottom: 1px solid #dddddd;padding-left:10px;"><%=cols[2]%></td>
			  </tr>
			
			<%
			}
			
			}
			%>
			</form>
</table></td></tr>
<tr align="right" valign="bottom">
	<td valign="middle" class="win-bottom-line" style="padding-right:48px;">
	<%
	if (haveError)
	{
	%>
      <input name="Submit" type="button" class="normal-white" onClick="history.back()" value="  返 回  " >
    <%
	}
	else
	{
	%>
	<input name="Submit" type="button" class="normal-green" id="in" onClick="inp()" value="  确认提交  ">
	<%
	}
	%>	</td>
</tr></table>
</body>
</html>
