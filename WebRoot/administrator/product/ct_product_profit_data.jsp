<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.util.StringUtil,com.cwc.app.api.OrderMgr"%>
<%@page import="com.cwc.db.DBRow"%>
<%@page import="com.cwc.exception.JsonException"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.beans.jqgrid.FilterBean"%>
<%@page import="net.sf.json.JsonConfig"%>
<%@page import="com.cwc.app.beans.jqgrid.RuleBean"%>
<%@page import="com.cwc.json.JsonUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 

<%
//获取数据参数
long pcid = StringUtil.getLong(request,"pcid");
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
int union_flag = StringUtil.getInt(request,"union_flag");
long pro_line_id = StringUtil.getLong(request,"pro_line_id");
//grid参数
boolean search = Boolean.parseBoolean(StringUtil.getString(request,"_search"));//是否使用了搜索功能
String sidx = StringUtil.getString(request,"sidx");//排序使用的字段
String sord = StringUtil.getString(request,"sord");//排序顺序
String filter = StringUtil.getString(request,"filters");//搜索条件
int c = StringUtil.getInt(request,"rows");//每页显示多少数据
int pages = StringUtil.getInt(request,"page",1);//当前请求的是第几页

FilterBean filterBean = null;//搜索条件bean
	
if(search)//将搜索条件字符串转换成搜索条件bean，为方便后来搜索
{
	Map<String, Object> map = new HashMap<String, Object>();  //String-->Property ; Object-->Clss 
	map.put("rules", RuleBean.class);
	JsonConfig configjson = JsonUtils.getJsonConfig();
	configjson.setClassMap(map);
	filterBean = (FilterBean) JsonUtils.toBean(JSONObject.fromObject(filter),FilterBean.class,configjson); 
}

Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));

PageCtrl pc = new PageCtrl();
pc.setPageSize(c);
pc.setPageNo(pages);

DBRow products[];
if (cmd.equals("filter"))
{
	products = productMgrZJ.filterProduct(pcid,pro_line_id,union_flag,0,0,pc);
}
else if (cmd.equals("search"))
{
	products = productMgr.getSearchProducts4CT(key,pcid,pc);
}
else
{
	products = new DBRow[0];	
}
//对需要特殊显示的单元格内容进行预处理
for (int i=0; i<products.length; i++)
{
	StringBuffer sb = new StringBuffer();
	
	DBRow allFather[] = tree.getAllFather(products[i].get("catalog_id",0l));
	for (int jj=0; jj<allFather.length-1; jj++)
	{
	  	sb.append("<a href=\"javascript:filter2("+allFather[jj].getString("id")+",0,'')\">"+allFather[jj].getString("title")+"</a><br>");
	}
	
	DBRow catalog = catalogMgr.getDetailProductCatalogById(products[i].get("catalog_id",0l));
	if (catalog!=null)
	{
	 	sb.append("<a href=\"javascript:filter2("+catalog.getString("id")+",0,'')\">"+catalog.getString("title")+"</a>");
	}
	//追加商品分类
	products[i].add("catalog","<div style='text-align: left;padding-left:10px;'>"+sb.toString()+"</div>");
	//追加国家毛利率
	
	DBRow countryProfit[] = quoteMgr.getProductCountryProfitByPid(products[i].get("pc_id",0l),null);
	if ( countryProfit.length>0)
	{
		/**
		StringBuffer countryProfitSB = new StringBuffer();
		countryProfitSB.append("<div style='text-align: left;padding-left:10px;height:100px; '>");
		for (int jj=0; jj<countryProfit.length; jj++)
		{
			countryProfitSB.append(countryProfit[jj].getString("c_country"));
			countryProfitSB.append(":");
			countryProfitSB.append("&nbsp;&nbsp;<span style='color:blue;'>");
			countryProfitSB.append(countryProfit[jj].getString("profit"));
			countryProfitSB.append("</span>");
			countryProfitSB.append("<br>");
		}
		countryProfitSB.append("</div>");
		**/
		
		products[i].add("country_profit","<input type='button' name='Submit' value='查看' onclick=\"tb_show('销售地区毛利','detail_area_profit.html?sellCost="+products[i].get("unit_price",0d)+"&pid="+products[i].get("pc_id",0l)+"&TB_iframe=true&height=500&width=850',false)\"/>");
	}
	else
	{
		products[i].add("country_profit","无");
	}
	
	//把价格处理下
	String sellCost = "销售成本："+orderMgr.getOrderMergeCost(OrderMgr.ORDER_SOURCE_EBAY,orderMgr.getProductMergeCost(products[i].get("unit_price",0d)), 0);
	String purchaseCost = "采购成本："+orderMgr.getProductMergeCost(products[i].get("unit_price",0d));
	
	products[i].add("unit_price",purchaseCost+"<br>"+sellCost);
	products[i].add("weight",products[i].get("weight",0f)+"/"+products[i].getString("unit_name"));
}



DBRow data = new DBRow();
data.add("page",pages);//page，当前是第几页
data.add("total",pc.getPageCount());//total，总共页数
data.add("rows",products);//rows，返回数据
data.add("records",pc.getAllCount());//records，总记录数

out.println(new JsonObject(data).toString());
%>