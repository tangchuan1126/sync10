<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%
	DBRow[] importAllRows = null;
 	DBRow[] rows = null;
 	String fileName = StringUtil.getString(request,"fileNames");
 	try
 	{
 		HashMap<String,DBRow[]> error = proprietaryMgrZyj.uploadProductTitleExcelShow(fileName, "show", request);
 		importAllRows = error.get("allExcelProductTitles");
 		rows = error.get("errorProductTitles");
 	}
 	catch(FileTypeException e)
 	{
 		out.print("<script>alert('只可上传.xls文件');window.history.go(-1)</script>");
 	}
 	
 	boolean submit = true;
 	String msg = "";
 	
 %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>检测商品或title</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/poshytip/jquery-1.4.min.js"></script>


<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>			
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>

$.blockUI.defaults = {
	css: { 
		padding:        '10px',
		margin:         0,
		width:          '200px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #aaa',
		backgroundColor:'#fff'
	},
	
	// 设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.8' 
	},

centerX: true,
centerY: true, 

	fadeOut:  2000
};


</script>
<script type="text/javascript">
	
	function ajaxSaveProductTitles()
	{
		if(<%=submit%>)
		{
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/SaveImportProductsTitlesAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 6000000,
				cache:false,
				data:$("#saveProductsTitlesForm").serialize(),
				
				beforeSend:function(request){
				},
				
				error: function(e){
					showMessage("提交失败，请重试！","alert");
				},
				
				success: function(data){
					if (data && data["flag"]=='true')
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">保存成功！</span>' });
						$.unblockUI();
						$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
					}
					else
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">保存失败！</span>' });
						$.unblockUI();
						window.location.reload();
					}
				}
			});
		}
		else
		{
			alert("<%=msg%>");
		}
	};
	function closeWin()
	{
		$.artDialog.close();
	}
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="onLoadInitZebraTable()" >
<table width="100%" cellpadding="0" cellspacing="0" height="100%">
  <tr>
  	<td valign="top" width="98%" align="center">
	<div id="tabs" style="width:95%; height:100%" align="center">
		<ul>
			 <% 
			if(rows != null && rows.length > 0)
			{	
				submit = false;
			%>
				<li><a href="#error_product_title">上传的商品title信息错误提示</a></li>
			<%
			} 
			%>
			<li><a href="#all_product_title">上传的所有商品title信息</a></li>
		</ul>
		 <% 
			if(rows != null && rows.length > 0)
			{	
				submit = false;
		%>
		<div id="error_product_title" align="center">
			<table width="100%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;" class="zebraTable">
					<tr>
						<th width="30%"  class="left-title " style="vertical-align: center;text-align: center;">商品名</th>
						<th width="35%" class="right-title "style="vertical-align: center;text-align: center;">title</th>
						<th width="35%"  class="left-title " style="vertical-align: center;text-align: center;">错误原因</th>
					</tr>
				<%
					for(int i = 0; i < rows.length; i ++)
					{
				%>
					  <tr align="center" valign="middle">
						<td height="40" align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;">
						  <%=rows[i].getString("productName")%>&nbsp;
						</td>
						<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;">
							<%=rows[i].getString("productTitle") %>&nbsp;
						</td>
						<td style="border-bottom: 1px solid #dddddd;padding-left:10px;" align="left">
							<% 
								out.print("<font color='red'>上传excel内的数据第"+rows[i].getString("productTitleRowNumber")+"行</font><br/>");
								out.println("<font color='red'>"+rows[i].getString("errorMessage")+"</font>");
							%>
						</td>
					  </tr>
				  <%
						}
				  %>
		  </table>
		</div>
		<%
			}
		%>
	<form action="" id="saveProductsTitlesForm" method="post">
		<div id="all_product_title" align="center">
		<input type="hidden" name="tempfilename" id="tempfilename" value="<%=fileName%>"/>
			<table width="100%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;" class="zebraTable">
				<% 
				if(importAllRows != null && importAllRows.length > 0)
				{	
					if(rows == null || 0 == rows.length)
					{
						submit = true;
					}
					else
					{
						submit = false;
					}
				%>
					<tr>
						<th width="35%"  class="left-title " style="vertical-align: center;text-align: center;">商品名</th>
						<th width="65%" class="right-title "style="vertical-align: center;text-align: center;">title</th>
					</tr>
				<%
					for(int i = 0; i < importAllRows.length; i ++)
					{
				%>
					  <tr align="center" valign="middle">
						<td height="40" align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;">
						  <%=importAllRows[i].getString("productName")%>&nbsp;
						  <input type="hidden" name="importAllProductTitles" 
						  value='<%=importAllRows[i].getString("productName")+"_"+importAllRows[i].getString("productTitle")+"_"+importAllRows[i].getString("productTitleRowNumber") %>'/>
						</td>
						<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;">
							<%=importAllRows[i].getString("productTitle") %>&nbsp;
						</td>
					  </tr>
				  <%
						}
					}
					else
					{
						submit = false;
				  %>
						<tr align="center" valign="middle">
							<th height="100%" align="center" style="padding-left:10px;font-size:xx-large;background-color: white;">商品title信息为空</th>
						</tr>
				  <%
				  	}
				  %>
		  </table>
		 
		</div>
	</form>
	</div>
	</td>
  </tr>
  <script>
	$("#tabs").tabs({	
		cookie: { expires: 30000 } 
	});
</script>
 <tr>
 	<td valign="bottom">
 		<table width="100%" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="left" valign="middle" class="win-bottom-line">
					<%
						if(!msg.equals(""))
						{
					%>
						<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#000000"><%=msg%></span>
					<%
						}
					%>			  &nbsp;</td>
 				<td align="right" valign="middle" class="win-bottom-line"> 
			      <%  
			      	if(submit)
			      	{
			      %>
			      	<input type="button" name="Submit2" value="确定" class="normal-green" onClick="ajaxSaveProductTitles()"/>
			      <%
			      	}
			      %>
				  <input type="button" name="Submit3" value="取消" class="normal-white" onClick="closeWin();"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
</table>

</body>
</html>



