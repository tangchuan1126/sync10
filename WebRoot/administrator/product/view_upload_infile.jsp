<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String permitFile = "csv";
String filePath = "/administrator/product/infile/";

TUpload upload = new TUpload();
			
upload.useOrgName();
upload.setRootFolder(filePath);
upload.setPermitFile(permitFile);

int flag = upload.upload(request);
String msg;
boolean haveError = false;

if (flag==2)
{
	msg = "只能上传:"+permitFile;

	out.println("<script>");
	out.println("alert('"+msg+"')");
	out.println("history.back();");
	out.println("</script>");
}
else if (flag==1)
{
	msg = "上传出错";

	out.println("<script>");
	out.println("alert('"+msg+"')");
	out.println("history.back();");
	out.println("</script>");
}
else
{
	msg = upload.getFileName();
	
	String csvRow[] = productMgr.getCsv(filePath+msg);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>提交日志</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style>
.form
{
	padding:0px;
	margin:0px;
}

</style>
<script>
function inp()
{
	document.getElementById("in").disabled=true;
	document.product_form.submit();
	
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
			<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td width="50%" style="font-size:14px;font-weight:bold">当前文件:<span style="color:#FF0000"><%=msg%></span></td>
				<td width="50%" align="right" valign="middle" style="font-size:14px;font-weight:bold">入库仓库：<%=upload.getRequestRow().getString("title")%></td>
			  </tr>
			</table>
			<table width="90%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px">
			<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addInProduct.action" method="post" name="product_form">
			<input type="hidden" name="csv_name" value="<%=filePath+msg%>">
			<input type="hidden" name="cid" value="<%=upload.getRequestRow().getString("cid")%>">
			<input type="hidden" name="backurl" value="<%=upload.getRequestRow().getString("backurl")%>">
			  <tr >
				<td width="47%" height="30"  class="left-title " style="vertical-align: center;text-align: left;">商 品</td>
				<td width="14%" align="center"  class="left-title " style="vertical-align: center;text-align: left;">数 量</td>
			  </tr>
			<%
			boolean isInCatalog;
			
			String bgcolor="";
			String fontcolor  = "";
			
			for (int i=1; i<csvRow.length; i++)
			{
				isInCatalog = productMgr.isIncatalog(csvRow[i].split(",")[0].trim(),Long.parseLong(upload.getRequestRow().getString("cid")) );
				
				if ((i+1)%2==0)
				{
					bgcolor = "bgcolor='#f8f8f8'";
					fontcolor = "color:#000000";
				}
				else
				{
					bgcolor = "bgcolor='#ffffff'";
					fontcolor = "color:#000000";
				}
				
				if (!isInCatalog)
				{
					bgcolor = "bgcolor='#FF0000'";
					haveError = true;
					fontcolor = "color:#ffffff";
				}
			%>
			  <tr <%=bgcolor%>>
				<td width="50%" height="30" style="font-size:13px;<%=fontcolor%>;border-bottom: 1px solid #dddddd;padding-left:10px;"><%=csvRow[i].split(",")[0]%></td>
				<td width="50%" style="font-size:13px;<%=fontcolor%>;border-bottom: 1px solid #dddddd;padding-left:10px;"><%=csvRow[i].split(",")[1]%></td>
			  </tr>
			
			<%
			}
			
			}
			%>
			</form>
			</table>
		</td></tr>
		<tr>
			<td width="100%" valign="middle" class="win-bottom-line" align="right" style="padding-right:48px;">
			<%
			if (haveError)
			{
			%>
			 
			  <input name="Submit" type="button" class="normal-white" onClick="history.back()" value="  返 回  " >
			<%
			}
			else
			{
			%>
			 <span style="font-size:14px;font-weight:bold">入库仓库：<%=upload.getRequestRow().getString("title")%></span>
			<input name="Submit" type="button" class="normal-green" id="in" onClick="inp()" value="  确认提交  ">
			<%
			}
			%>
		</td>
		</tr></table>
</body>
</html>
