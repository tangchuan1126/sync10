<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));

long pcid = StringUtil.getLong(request,"pcid");
String pro_line_id = StringUtil.getString(request,"pro_line_id");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />

<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-ui-1.8.14.custom.min.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.src.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<style type="text/css">
td.categorymenu_td div{margin-top:-3px;}
</style>
<script language="javascript">
<!--
$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#search_key").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	$("#search_key").keydown(function(event){
		if (event.keyCode==13)
		{
			search();
		}
	});

});

function cleanSearchKey(divId)
{
	$("#"+divId+"search_key").val("");
}

function checkForm(theForm)
{
	if (theForm.catalog_id.value==0)
	{
		alert("请选择商品分类");
		return(false);
	}
	else if (theForm.p_name.value=="")
	{
		alert("请填写商品名称");
		return(false);
	}
	else if (theForm.p_code.value=="")
	{
		alert("请填写条码");
		return(false);
	}
	else
	{
		return(true);
	}
}

var cmd = "";

function filter()
{
	cmd = "filter";
	
	var pcid = $("#filter_pcid").val();	
	var union_flag = $("#union_flag").val();	
	var key = $("#search_key").val();	
	var pro_line_id = $("#filter_productLine").val();
	
	filter2(pcid,pro_line_id,union_flag,key);
}

function filter2(pcid,pro_line_id,union_flag,key)
{
	jQuery("#union").jqGrid('setGridParam',{postData:{cmd:cmd,pcid:pcid,pro_line_id:pro_line_id,union_flag:union_flag,key:key},page:1}).trigger('reloadGrid');
}

function search()
{
	cmd = "search";
	
	if ($("#search_key").val()=="")
	{
		alert("请填写关键字");
		return(false);
	}
	else
	{
		var pcid = $("#filter_pcid").val();	
		var key = $("#search_key").val();	
		
		
		jQuery("#union").jqGrid('setGridParam',{postData:{cmd:cmd,pcid:pcid,key:key},page:1}).trigger('reloadGrid');
	}
}

function exportData()
{
	tb_show('导出商品价格数据','export_product_profit.html?cmd='+cmd+'&pcid='+$("#filter_pcid").val()+'&union_flag='+$("#union_flag").val()+'&key='+$("#search_key").val()+'&pro_line_id='+$("#filter_productLine").val()+'&TB_iframe=true&height=300&width=600',false);
}

function closeWin()
{
	tb_remove();
}

function importData()
{
	tb_show('导入商品价格数据','import_product_profit.html?TB_iframe=true&height=400&width=900',false);
}

	function retailChange()
	{
		var para = "pc_id="+$("#filter_pcid").val()+"&pro_line_id="+$("#filter_productLine").val();
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/quote/nowQuoteChangeGrossProfit.action',
				type: 'post',
				dataType: 'json',
				//timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){
					$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">重算进行中...</span>' });
				},
				
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				
				success: function(data)
				{
					if(data["close"]=="true")
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">重算成功！</span>' });
						$.unblockUI();
					}
					else
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">重算失败！</span>' });
					}
				}
			});
	}
	
	function ajaxLoadCatalogMenuPage(id)
	{
			var para = "id="+id;
			$.ajax({
				url: 'product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}

//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:center;
		padding:5px;
		line-height:17px;
	}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  >
<form name="export_form" id="export_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/ExportProfitQuote.action" >
<input type="hidden" name="pcid" />
<input type="hidden" name="cmd" />
<input type="hidden" name="key" />
<input type="hidden" name="union_flag" />

<input type="hidden" name="ps_id" >
<input type="hidden" name="ca_id" >
</form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td align="left" class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle"/>&nbsp;&nbsp; 客户与报价应用 »   商品价格管理</td>
  </tr>
</table>
<br>



<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="65%">
	
	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
	
	<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
    		<ul id="productLinemenu" class="mcdropdown_menu">
             <li rel="0">所有产品线</li>
             <%
				  DBRow p1[] = productLineMgrTJH.getAllProductLine();
				  for (int i=0; i<p1.length; i++)
				  {
						out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
						out.println("</li>");
				  }
			 %>
			 </ul>
			 <input type="text" name="productLine" id="productLine" value="" />
			 <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"  />
</td>
    <td width="434" height="29" align="left" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">
	<input type="hidden" name="union_flag" value="0">

            <input name="Submit2" type="button" class="button_long_refresh" value="过滤" onClick="filter()">&nbsp;&nbsp;&nbsp;&nbsp;
            <input name="import/export2" type="button" class="long-long-button-yellow" value="导入商品价格数据" onclick="importData()"/>
            &nbsp;&nbsp;&nbsp;&nbsp;<input name="import/export" type="button" class="long-long-button-stat" value="导出数据到EXCEL" onClick="exportData()">
    </td>
  </tr>
  <tr>
  	<td id="categorymenu_td" class ="categorymenu_td"   width="423" align="left" valign="bottom" bgcolor="#eeeeee" style="padding-left:10px;"> 
           <ul id="categorymenu" class="mcdropdown_menu">
			  <li rel="0">所有分类</li>
			  <%
			  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
			  for (int i=0; i<c1.length; i++)
			  {
					out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
		
					  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
					  if (c2.length>0)
					  {
					  		out.println("<ul>");	
					  }
					  for (int ii=0; ii<c2.length; ii++)
					  {
							out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
							
								DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
								  if (c3.length>0)
								  {
										out.println("<ul>");	
								  }
									for (int iii=0; iii<c3.length; iii++)
									{
											out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
											out.println("</li>");
									}
								  if (c3.length>0)
								  {
										out.println("</ul>");	
								  }
								  
							out.println("</li>");				
					  }
					  if (c2.length>0)
					  {
					  		out.println("</ul>");	
					  }
					  
					out.println("</li>");
			  }
			  %>
		</ul>
	  	<input type="text" name="category" id="category" value="" />
	  	<input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />
	</td>
	<script type="text/javascript">
			$("#category").mcDropdown("#categorymenu",{
					  allowParentSelect:true,
					  select: 
					  
							function (id,name)
							{
								$("#filter_pcid").val(id);
							}
			
			});
			
			$("#productLine").mcDropdown("#productLinemenu",{
					allowParentSelect:true,
					  select: 
					  		
							function (id,name)
							{
								$("#filter_productLine").val(id);
								if(id!=0)
								{
									cleanSearchKey("");
									ajaxLoadCatalogMenuPage(id);
								}
							}
			
			});
			<%
			if (pcid>0)
			{
			%>
			$("#category").mcDropdown("#categorymenu").setValue(<%=pcid%>);
			<%
			}
			else
			{
			%>
			$("#category").mcDropdown("#categorymenu").setValue(0);
			<%
			}
			%> 
			
			<%
			if (!pro_line_id.equals("0") && !pro_line_id.equals(""))
			{
			%>
			$("#productLine").mcDropdown("#productLinemenu").setValue('<%=pro_line_id%>');
			<%
			}
			else
			{
			%>
			$("#productLine").mcDropdown("#productLinemenu").setValue(0);
			<%
			}
			%>
			</script>
    <td height="29" align="left" bgcolor="#eeeeee">
	<input onclick="cleanProductLine('')" type="text" name="search_key" id="search_key" value="" style="width:200px;"/>
           &nbsp;&nbsp;&nbsp;
<input name="Submit4" type="button" class="button_long_search" value="搜索" onClick="search()"/> &nbsp;&nbsp;
<input type="button" class="long-button-ok" value="重算" onclick="retailChange()"/>
</td>
  </tr>
</table>
	
	</div>
	
	
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
  <tr>
    <td align="left">&nbsp;
          
     </td>
  </tr>
</table>

<div align="left">

	<table id="union"></table>
	<div id="pageunion"></div> 
	
	<script type="text/javascript">
	function cleanProductLine(divId)
	{
		$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
		$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
	}
			var	lastsel;
			   $("#union").jqGrid({
					sortable:true,
					url:"ct_product_profit_data.html",
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.95),
					height:500,
					autowidth: false,
					shrinkToFit:true,
					caption: "商品信息",
					jsonReader:{
				   			id:'pc_id',
	                        repeatitems : false
	                	},
				   	rownumbers:true,
				   	colNames:['pc_id','商品名称','所属分类','重量(Kg)','单价','销售地区毛利'], 
				   	colModel:[ 
				   		{name:'pc_id',index:'pc_id',hidden:true,sortable:false},
				   		{name:'p_name',index:'p_name',sortable:false},
						{name:'catalog',index:'catalog',align:'center',sortable:false},
						{name:'weight',index:'weight',align:'center',sortable:false},
				   		{name:'unit_price',index:'unit_price',align:'center',sortable:false},
						{name:'country_profit',index:'country_profit',align:'center',sortable:false},
				   		], 
				   	rowNum:50,//-1显示全部
				   	mtype: "GET",
				   	gridview:true,
				   	pager:'#pageunion',
				   	sortname: 'pc_id', 
				   	viewrecords: true, 
				   	sortorder: "asc"
				   	}); 		   	
			   	jQuery("#union").jqGrid('navGrid',"#pageunion",{edit:false,add:false,del:false,search:false,refresh:true});
	</script>
</div>

</body>
</html>
