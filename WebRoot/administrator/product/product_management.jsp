<%@page import="com.cwc.json.JsonObject,net.sf.json.JSONArray"%>
<%@page import="com.cwc.app.key.GlobalKey"%>
<%@page import="com.cwc.app.key.MoreLessOrEqualKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="javax.mail.Transport"%>
<%@page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.floor.api.zyj.service.TitleService,com.cwc.app.floor.api.zyj.model.Title,java.util.List" %>
<%@include file="../../include.jsp"%> 
<%@taglib uri="/turboshop-tag" prefix="tst" %>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long adminId=adminLoggerBean.getAdid();

	//根据登录帐号判断是否为Customer
	Boolean bl=DBRowUtils.existDeptAndPostInLoginBean(GlobalKey.CLIENT_ADGID, adminLoggerBean);//如果bl 是true 说明是客户登录
	//如果bl 是true 说明是客户登录
	//如果是用户登录 查询用户下的产品
	List<Title> titles;
	DBRow[] p1;
	long number=0;
	TitleService titleService =  (TitleService)MvcUtil.getBeanFromContainer("titleService");
	if(bl){
		number=1;
		//titles=adminMgrZwb.getProprietaryByadminId(adminLoggerBean.getAdid());
		
		titles = titleService.getTitlesByCustomerId((int)adminLoggerBean.getCorporationId());
	}else{
		number=0;
		titles=titleService.getAll();
	}
%>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="/Sync10-ui/bower_components/jqueryui/themes/smoothness/jquery-ui.min.css">

<script type="text/javascript" src="/Sync10-ui/bower_components/jquery/dist/jquery.min.js"></script>

<script src="/Sync10-ui/bower_components/jqueryui/jquery-ui.min.js"></script>

<!-- <script type="text/javascript" src="/Sync10/administrator/js/autocomplete/autocomplete.js"></script> -->

<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />



<script type="text/javascript" src="/Sync10-ui/bower_components/jquery.browser/dist/jquery.browser.min.js"></script>

<!-- <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script> -->


<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript" ></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/simple.css" />          

<script src="/Sync10-ui/lib/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/art/plugins/iframeTools.source.js" type="text/javascript"></script>

<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jquery UI 加上 autocomplete -->
<!-- <script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>



 <script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script> -->


<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>


<!-- <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />  -->
 <!-- 遮罩 -->
 
 
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>
<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<script type="text/javascript" src="../js/jstree/dist/jstree.min.js"></script>
<link type="text/css" rel="stylesheet" href="../js/jstree/dist/themes/default/style.min.css" />

<title>Configuration</title>
<style>
.ui-tabs .ui-tabs-nav li.ui-tabs-active {
  margin-bottom: -2px;
  padding-bottom: 2px;
}

.ui-tabs .ui-tabs-nav li a{outline: none;}

.content{padding:10px;}
.aui_titleBar{  border: 1px #DDDDDD solid;}
.aui_header{height: 40px;line-height: 40px;}
.aui_title{
  height: 40px;
  width: 100%;
  font-size: 1.2em !important;
  margin-left: 20px;
  font-weight: 700;
  text-indent: 0;
}

 <!-- 高级搜索的样式-->
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;	
	border:1px #bdbdbd solid;	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-19px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

.ui-widget-content{
	border:1px #CFCFCF solid;
	border-radius: 0px;
  	-webkit-border-radius: 0px;
	-moz-border-radius:0;
}
.ui-widget-header{
	background:#f1f1f1 !important;
	border-radius: 0;
	border-top:0;
	border-left:0;
	border-right:0;
	border-bottom: 1px #CFCFCF solid;
}
.ui-tabs .ui-tabs-nav li a{
	padding: .5em 1em 13px !important;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
	  height: 40px;
	  margin-top: -4px !important;
	background:#fff;
}
.ui-tabs .ui-tabs-nav{
	padding :0 .2em 0;
}
.ui-tabs{
	padding:0;
}


.breadnav {
            padding:0 10px; height:25px;margin-bottom: 18px;
        }
        .breadcrumb{
          font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
          font-size: 14px;
          line-height: 1.42857143;
        }
        .breadcrumb {
        	margin-top:0;
          padding: 8px 15px;
          margin-bottom: 20px;
          list-style: none;
          background-color: #f5f5f5;
          border-radius: 4px;
        }

        .breadcrumb > li {
          display: inline-block;
        }

        .breadcrumb > .active {
          color: #777;
        }

        .breadcrumb > li + li:before {
            padding: 0 5px;
            color: #ccc;
            content: "/\00a0";
        }
        .breadcrumb a{
		      color: #337ab7;
                text-decoration: none;	
		}
</style>
<script type="text/javascript">

$(document).ready(function(){
	
	$("div[name='lpTree']").jstree();
});

function loadData(){
	var titles = <%=JSONArray.fromObject(titles).toString()%> ;
	var item=[];
	for(var i=0;i<titles.length;i++){
		var op={};       
		op.id=titles[i].id;                                                                                                                                                                                                                                                                                                                                                                                                      
		op.name=titles[i].name;
		item.push(op);
	}
	var data=[{
		key:'Title: ',
		type:'title',
		son_key:'Product Line: ',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadProductLineAction.action',
		
		son_key2:'Parent Category: ',
		url2:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadFirstProductCatalogAction.action',
		
		son_key3:'Sub Category: ',
		url3:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadSecondProductCatalogAction.action',	
				
		son_key4:'Sub-Sub Category: ',
		url4:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadThirdProductCatalogAction.action',

		array:item
	 },
	 {key:'Product Type: ',select:'true',type:'leixing',array:[{id:'0',name:'ITEM'},{id:'1',name:'UNIT'}]},	 
	// {key:'图片类型：',select:'true',type:'picType',array:[{id:'1',name:'商品包装'},{id:'2',name:'商品本身 '},{id:'3',name:'商品底帖 '},{id:'4',name:'商品称重  '},{id:'5',name:'商品标签 '}]},
// 	 {key:'Photos Status: ',select:'true',type:'picStuts',array:[{id:'1',name:'Not Complete'},{id:'2',name:'Complete'}]},
	 //{key:'Is Active: ',select:'true',type:'isActive',array:[{id:'-1',name:'All'},{id:'1',name:'Active'},{id:'0',name:'Inactive'}]},
	 {key:'Is Active: ',select:'true',type:'isActive',array:[{id:'1',name:'Active'},{id:'0',name:'Inactive'}]},
 	 ];
	initializtion(data);  //初始化
}

function down(){
	
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	
	var array = getSeachValue();
	title_id = "";
	pro_line_id = "";
	pcid = "";
	union_flag = -1;
	
	active = 1;
	
	for(var i=0;i<array.length;i++){
		
		if(array[i].type=='title'){
			title_id=array[i].val;
		}
		
		if(array[i].type=='title_son'){
			pro_line_id=array[i].val;
		}
		
		if(array[i].type=='title_son_son'){
			pcid=array[i].val;
			
		}else if(array[i].type=='title_son_son_son'){
			
			pcid=array[i].val;
		}else if(array[i].type=='title_son_son_son_son'){
			pcid=array[i].val;
		}
		if(array[i].type=='leixing'){
			union_flag=array[i].val;
		}
		
		if(array[i].type=='isActive'){
			active=array[i].val;
		}
	}
	
	params = {lineId:pro_line_id,catalogId:pcid,productType:union_flag,titleId:title_id,cmd:cmd,key:key, active:actie};
	
	var str = jQuery.param(params);
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxDownProductAndTitleAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:str,
		cache:false,
		success: function(date){
			
			$.unblockUI();
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}	
		}
	});
}

	function importProductTitles(_target)
	{
	    var fileNames = $("#file_names").val();
	    var obj  = {
		     reg:"xls",
		     limitSize:2,
		     limitNum:1
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: 'Upload Files',width:'770px',height:'530px', lock: true,opacity: 0.1,fixed: true});
	}
	
	function importInitData()
	{
	    var fileNames = $("#file_names").val();
	    var obj  = {
		     reg:"xlsm",
		     limitSize:2,
		     limitNum:1,
		     target:"initData"
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames.length > 0 )
		 {
			uri += "&file_names=" + fileNames;
		 }
		 $.artDialog.open(uri , {id:'file_up',title: 'Upload Files',width:'770px',height:'530px', lock: true,opacity: 0.1,fixed: true});
	}
	
	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,target)
	{
		if($.trim(fileNames).length > 0 )
		{
			if(target=="")
			{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/check_product_title_show.html?fileNames="+fileNames;
				$.artDialog.open(uri,{title: "检测上传用户title信息",width:'850px',height:'530px', lock: true,opacity: 0.1,fixed: true});
			}
			else
			{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/initData_show.html?file_name="+fileNames;
				$.artDialog.open(uri,{title: "Basic Data Initialize",width:'1200px',height:'700px', lock: true,opacity: 0.1,fixed: true});
			}
		}
	}
	function showProductDetails(pcid, pc_name)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_details.html?pc_id="+pcid;
		$.artDialog.open(uri,{title: pc_name,width:'850px',height:'530px', lock: true,opacity: 0.1,fixed: true});
	}
	function deleteBasicDatas()
	{
		var d = $.artDialog({
			lock: true,opacity: 0.1,fixed: true,
		    title: 'Delete Basic Data',
		    content: 'Delete all data except the titles ?',
		    button:[
					{
						name: 'Sure',
						callback: function () {
							ajaxDeleteBasicDatas(d);
							return false ;
						},
						focus:true
						
					},
					{
						name: 'Cancel'
					}]
		});
		d.show();
	}
	//删除
	function ajaxDeleteBasicDatas(d){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/DeleteAllBasicDatasExceptTitlesAction.action',
			type:'post',
			dataType:'json',
			beforeSend:function(){
				 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
		    success:function(data){
			    $.unblockUI();
			    d.close();
			    if(data && data.flag == "true"){
			    	refreshWindow();
			    }
		    },
			error:function(){
		    	$.unblockUI();
				showMessage("System error","error");
		    }
		});
	}
// 	function test(){
<%-- 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/lable_template/lable_template_ui_show.html"; --%>
// 		$.artDialog.open(uri,{title: 'Test',width:'850px',height:'530px', lock: true,opacity: 0.3,fixed: true});
// 	}
	
	
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onload="loadData()">

<!-- 
<div class="breadnav">
<ol class="breadcrumb">
            <li><a href="#">Setup</a></li>
            <li class="active">Configuration</li>
        </ol>
</div> -->


<div class="content">
<!-- <input type="button" value="点击" onclick="test()"/> -->
<form action="" name="download_form" id="download_form"> </form>
	
	<div id="tabs">
		<ul>
			<li><a href="#av1">Advanced Search</a></li>
			<li><a href="#av2">Common Tools</a></li>
		</ul>
		<div id="av1">
			<div id="av"></div>
			<input type="hidden" id="atomicBomb" value=""/>
			<input type="hidden" id="title" />
		</div>
		<div id="av2">
		    <!-- 名字搜索 -->
	        <div id="easy_search_father">
				<div id="easy_search" style="margin-top:-2px;">
					<a href="javascript:query()"><img id="eso_search" src="../imgs/easy_search_2.png" width="70" height="60" border="0"/></a>
				</div>
		    </div>
	           <table width="400" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="418">
						<div  class="search_shadow_bg">
						 <input name="seachInput" id="seachInput" type="text" class="search_input" style="height: 30px;font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)query()"/>
						</div>
					</td>
				</tr>
			  </table>	
	    </div>
	</div>
	<script type="text/javascript">  
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	</script>
	
	<div id="result"></div>
	<!-- 隐藏值 -->
	<input type="hidden" id="hiddenProductLineId" value=""/>
	<!-- 分页用隐藏值 -->
	<input type="hidden" id="lineId" />
	<input type="hidden" id="catalogId" />
	<input type="hidden" id="productType" />
	<input type="hidden" id="cmd" />
	<!-- 刷新画面用隐藏值 -->
	<input type="hidden" id="hiddenPageName" value=""/>
	<input type="hidden" id="hiddenPcId" value=""/>
	<input type="hidden" id="hiddenPcName" value=""/>
</div>
</body>
</html>
<script>
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.1' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

jQuery(function($){
    $("#tabs").tabs();  //默认选中第一个
    addAutoComplete($("#seachInput"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
});

(function(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html?active=1&titleId='+title_id,
		type: 'post',
		dataType: 'html',
		async:false,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
})();

function go(p){
	var pro_line_id=$('#lineId').val();
	var pcid=$('#catalogId').val();
	var cmd=$('#cmd').val();
	var union_flag=$('#productType').val();
	var params = {p:p,lineId:pro_line_id,catalogId:pcid,productType:union_flag,titleId:title_id,cmd:cmd};
	var str = jQuery.param(params);
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html',
		type: 'post',
		dataType: 'html',
		async:false,
		data:str,
		success: function(html){
			$("#result").html(html);
            window.scroll(0,0);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
}
var title_id='';
var pro_line_id='';
var pcid='';
var union_flag=0;
var key='';
var cmd='';

//根据条件查询商品
function custom_seach(){
	//遮罩打开
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   
	
	var array = getSeachValue();
	title_id = "";
	pro_line_id = "";
	pcid = "";
	union_flag = -1;
	key = "";
	cmd = "filter";
	
	active = 1;
	
	for(var i=0;i<array.length;i++){
		
		if(array[i].type=='title'){
			title_id=array[i].val;
		}
		
		if(array[i].type=='title_son'){
			pro_line_id=array[i].val;
		}
		
		if(array[i].type=='title_son_son'){
			pcid=array[i].val;
			
		}else if(array[i].type=='title_son_son_son'){
			
			pcid=array[i].val;
		}else if(array[i].type=='title_son_son_son_son'){
			pcid=array[i].val;
		}
		if(array[i].type=='leixing'){
			union_flag=array[i].val;
		}
		
		if(array[i].type=='isActive'){
			active=array[i].val;
		}
	}
	
	params = {lineId:pro_line_id,catalogId:pcid,productType:union_flag,titleId:title_id,cmd:cmd, active:active};

	$('#lineId').val(pro_line_id);
	$('#catalogId').val(pcid);
	$('#cmd').val('seach');
	$('#productType').val(union_flag);
	
	var str = jQuery.param(params);
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html',
		type: 'post',
		dataType: 'html',
		async:false,
		data:str,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
}

//高级查询控件事件
function query(){
	key=$('#seachInput').val();  //获取文本框值
	
	cmd='name';
	
	var str='cmd='+cmd+'&seachValue='+key+'&titleId='+title_id;
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html',
		type: 'post',
		dataType: 'html',
		async:false,
		data:str,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});
}

// 商品的CLP信息的导出
function exportProductClp(){
	
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	
	var para='cmd='+cmd+'&productType='+union_flag+'&lineId='+pro_line_id+'&catalogId'+pcid+'&titleId='+title_id+'&seachValue='+key;
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/box/AjaxDownCLPAndTitleAndShipToAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();//遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}else{
				showMessage("System error","error");
			}
		},
		error:function(){
			$.unblockUI();//遮罩关闭
			showMessage("System error","error");
		}
	});
}

//title 点击更多时触发
function morePcTitles(pc_id, p_name){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_add_title.html?pc_id="+pc_id;
	$.artDialog.open(url , {title: '['+p_name+'] Title',width:'600px',height:'450px', lock: true,opacity: 0.1,fixed: true,close:function(){loadTitlesByPcId(pc_id);}});
}

function refreshWindow(){
	if($("#hiddenPageName").val() == "product_management_show_list"){
		loadClpsByPcId($("#hiddenPcId").val(),$("#hiddenPcName").val());
	} else {
		window.location.reload();
	}
	$("#hiddenPageName").val("");
	$("#hiddenPcId").val("");
	$("#hiddenPcName").val("");
}

// 局部刷新Title区域
function loadTitlesByPcId(pc_id) {
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '}); //遮罩打开
	var para = "pc_id="+pc_id;
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/FindProprietaryByPcIdAction.action',
		type:'post',
		dataType:'json',
		data:para,
		async:false,
		success:function(data){
			$.unblockUI();//遮罩关闭
			$("#myulPcTitles_"+pc_id).html("");
			var html = '';
			if(data && data.length > 0)
			{
				var len = data.length > 5 ? 5: data.length;
				for(var i = 0; i < len; i ++)
				{
					html += '<li style="margin-left:1px; height:20px">';
					html += data[i].title_name;
					html += '</li>';
				}
				if(data.length >4)
				{
					$("#titleMore_"+pc_id).html("&nbsp;|&nbsp;More&nbsp;["+data.length+"]");
					$("#titleMore_"+pc_id).css("display","");
				}
				else
				{
					$("#titleMore_"+pc_id).css("display","none");
				}
			}
			if('' == html)
			{
				html += '<li style="height: 35px;"></li>';
			}
			$("#myulPcTitles_"+pc_id).html(html);
		},
		error:function(){
			$.unblockUI();//遮罩关闭
			showMessage("System error","error");
		}
	});
}

//局部刷新CLP Type区域
function loadClpsByPcId(pc_id,pc_name,customerId){
	if(!customerId){
		customerId = 0;
	}
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '}); //遮罩打开
	var para = "pc_id="+pc_id;
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/container/ClpGetByPcidAction.action?active=1&customerId=' + customerId,
		type:'post',
		dataType:'json',
		data:para,
		async:false,
		success:function(data) {
			$.unblockUI();//遮罩关闭
			$("#clpType_"+pc_id).html("");
			var html = '';
			if(data && data.length > 0) {
				var len = data.length > 2 ? 2: data.length;
				for(var i = 0; i < len; i ++) {
					html += '<fieldset style="border:2px #930 solid;padding:0 7px 7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px;">';
					html += '<legend style="color:green;font-weight:bold;text-decoration:underline;cursor:pointer;" onclick="openLpType(&quot;'+data[i].lpt_id+'&quot;,&quot;'+pc_name+'&quot;);">';
					html += data[i].lp_name;
					html += '</legend>';
					html += '<table style="width:100%;height:100%">';
					html += '<tr>';
					html += '<td style="background:none;border-bottom:none;width:30%;">';
					html += '<div>';
					html += '<div class="box left width30"><span>Inner Type:</span></div>';
					if(data[i].inner_pc_or_lp > 0){
						if(data[i].inner_clp.inner_pc_or_lp == 0 || data[i].inner_clp.length == 1){
							html += '<div name="lp" class="box right width60">';
							html += '<a href="javascript:void(0)" style="color:green;" onclick="openLpType(&quot;'+data[i].inner_pc_or_lp+'&quot;,&quot;'+pc_name+'&quot;);">';
							html += '<span style="color:#0066FF">';
							if(data[i].inner_clp){
								if(data[i].inner_clp.length == 1){
									html += data[i].inner_clp[0].lp_name;
								}else{
									
									html += data[i].inner_clp.lp_name;
								}
							}
							html += '</span></a></div>';
						}else{
							html += '<div name="lpTree" class="box right width60">';
							
							for(var j=0; j<data[i].inner_clp.length; j++){
								html += '<ul> <li>'
								html += '<a href="javascript:void(0)" style="color:green;" onclick="openLpType(&quot;'+data[i].inner_clp[j].lpt_id+'&quot;,&quot;'+pc_name+'&quot;);">';
								if(data[i].inner_clp[j]){
									html += data[i].inner_clp[j].lp_name;
								}
								html += '</a>';
							}
							
							for(var j=0; j<data[i].inner_clp.length; j++){
								html += '</li> </ul>';
							}
							
							html += '</div>';
						}
					}else{
						html += '<div class="box right width30"><span style="color:#0066FF">Product</span></div>';
					}
					html += '<div class="clear"></div>';
					html += '</div>';
					html += '<div>';
					html += '<div class="box left width30"><span>Product Qty:</span></div>';
					html += '<div class="box right width30"><span style="color:#0066FF">';
					html += data[i].inner_total_pc;
					html += '</span></div>';
					html += '<div class="clear"></div>';
					html += '</div></td>';
					html += '<td style="background:none;border-bottom:none;padding-bottom:20px;width:70%;padding-top:8px;">';
					//记录Title-Customer记录的条数
					var count = 0;
					if(data[i].clp_titles && data[i].clp_titles.length > 0){
						html += '<table style="width:100%;border-collapse: collapse;" class="innerTable">';
						html += '<tr>';
						html += '<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;" >Title</td>';
						html += '<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;" >Customer</td>';
						html += '<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;" >ShipTo</td>';
						html += '<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;" >Operation</td>';
						html += '</tr>';
						var titleLen = data[i].clp_titles.length > 2 ? 2: data[i].clp_titles.length;
						
						for(var j = 0; j < titleLen; j ++) {
							if(customerId > 0 && data[i].clp_customers[j].clp_customer.id > 0 && data[i].clp_customers[j].clp_customer.id != customerId){
								//continue;
							}
							count++;
							html += '<tr>';
							html += '<td width="25%" style="vertical-align:middle;text-align:center;padding-left: 5px;">';
							html += data[i].clp_titles[j].clp_title.title_name;
							html += '</td>';
							html += '<td width="25%" style="vertical-align:middle;text-align:center;padding-left: 5px;">';
							
							
							html += data[i].clp_customers[j].clp_customer.name;
							html += '</td>';							
							html += '<td width="25%" style="vertical-align:middle;text-align:center;padding-left: 5px;">';
							html += data[i].clp_shiptos[j].clp_shipto.ship_to_name;
							html += '</td>';
							html += '<td width="25%" style="vertical-align:middle;text-align:center;padding-left: 5px;">';
							
							
							html += '<a href="javascript:void(0)" style="color: green;" onclick="openClpTitleShipTo(&quot;'+data[i].clp_titles[j].clp_title.title_id+'&quot;,&quot;'+data[i].clp_shiptos[j].clp_shipto.ship_to_id+'&quot;,&quot;'+data[i].clp_titles[j].clp_title.title_name+'&quot;,&quot;'+data[i].clp_shiptos[j].clp_shipto.ship_to_name+'&quot;,&quot;'+pc_id+'&quot;,&quot;'+pc_name+'&quot;,' + data[i].clp_customers[j].clp_customer.id + ',&quot;' + data[i].clp_customers[j].clp_customer.name +  '&quot;);">';
							html += '<span style="color:#0066FF">Configuration</span>';
							html += '</a>';
							html += '</td>';
							html += '</tr>';
						}
						html += '</table>';
					}
					if(data[i].clp_titles && data[i].clp_titles.length > 0){
						html += '<div style="margin-top:10px;">';
					}else{
						html += '<div style="margin-top:40px;">';
					}
					
					html += '<a href="javascript:void(0)" style="color: green" onclick="openTitleAndShipTo(&quot;'+data[i].lpt_id+'&quot;,&quot;'+pc_id+'&quot;,&quot;'+pc_name+'&quot;);">';
					if(data[i].clp_titles && data[i].clp_titles.length > 2) {
						html += 'Title/Customer/ShipTo <span id="linkedTitleShipToMore_'+data[i].lpt_id+'">';
						html += '&nbsp;|&nbsp;More&nbsp;['+data[i].clp_titles.length+']';
						html += '</span>';
					}else{
						html += 'Title/Customer/ShipTo <span id="linkedTitleShipToMore_'+data[i].lpt_id+'"></span>';
					}
					html += '</a></div>';
					html += '</td>';
					html += '</tr>';
					html += '</table>';
					html += '</fieldset>';
				}
			} else {
				html += '<div></div>';
			}
			html += '<div style="position:absolute;height:20px; bottom: 5px;left: 15px; white-space:nowrap;">';  
			html += '<a href="javascript:void(0)" class="btn" onclick="openClpList(&quot;'+pc_id+'&quot;,&quot;'+pc_name+'&quot;);">';
			if(data.length > 2) {
				html += 'Manage CLP Type <span id="clpMore_'+pc_id+'">';
				html += '&nbsp;|&nbsp;More&nbsp;['+data.length+']';
				html += '</span>';
			}else{
				html += 'Manage CLP Type <span id="clpMore_'+pc_id+'"></span>';
			}
			html += '</a>';
			html += '<a href="javascript:void(0)" class="btn" style="margin-left: 5px;font-size:20px;vertical-align: bottom;padding:0;" onclick="addClp(&quot;'+pc_id+'&quot;,&quot;'+pc_name+'&quot;);">';
			html += '&nbsp;&nbsp;+&nbsp;&nbsp;';
			html += '</a>';
			html += '</div>';
			
			$("#clpType_"+pc_id).html(html);
			
			$("div[name='lpTree']").jstree();
		},
		error:function(){
			$.unblockUI();//遮罩关闭
			showMessage("System error","error");
		}
	});
}


function openClpList(pc_id,product_name){
	//alert($("#customerId").val());
	$("#hiddenPageName").val("product_management_show_list");
	$("#hiddenPcId").val(pc_id);
	$("#hiddenPcName").val(product_name);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_and_ps_info_list.html?pc_id="+pc_id+"&active=1"; 
	$.artDialog.open(uri , {title:"["+ product_name +"]"+ " CLP Type",width:'1105px',height:'480px', lock: true,opacity: 0.1,fixed: true,close:function(){loadClpsByPcId(pc_id,product_name,$("#customerId").val());}});
}

function openLpType(clpId,product_name){
	
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_show.html?productName="+product_name+'&innerlpId='+clpId; 
	$.artDialog.open(uri , {title:'Show ['+ product_name + '] CLP Type',width:'650px',height:'460px', lock: true,opacity: 0.1,fixed: true});
}

function addClp(pc_id,product_name){
	$("#hiddenPageName").val("product_management_show_list");
	$("#hiddenPcId").val(pc_id);
	$("#hiddenPcName").val(product_name);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_box_sku_ps_type_add.html?pc_id="+pc_id; 
	
	
	$.artDialog.open(uri , {title:"["+ product_name +"] Add CLP Type",width:'650px',height:'480px', lock: true,opacity: 0.1,fixed: true,close:function(){loadClpsByPcId(pc_id,product_name);}});
}

function openClpTitleShipTo(title_id,ship_to_id,titleName,shipToName,pc_id,product_name,customer_id,customerName){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_type_title_ship_to_list.html?title_id="+title_id+"&ship_to_id="+ship_to_id+"&titleName="+titleName+"&shipToName="+shipToName+"&pc_id="+pc_id+"&productName="+product_name+"&active=1" + "&customer_id=" + customer_id + "&customerName=" + customerName; 
	 $.artDialog.open(uri , {title:'['+product_name+'] CLP Type Title/Customer/ShipTo Configuration',width:'800px',height:'450px', lock: true,opacity: 0.1,fixed: true, resize:false});
}

function openTitleAndShipTo(lp_type_id,pc_id,product_name){
	$("#hiddenPageName").val("product_management_show_list");
	$("#hiddenPcId").val(pc_id);
	$("#hiddenPcName").val(product_name);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/title_shipto_list.html?lp_type_id="+lp_type_id+"&pc_id="+pc_id;
	$.artDialog.open(uri , {title:"Title/Customer/Ship To",width:'730px',height:'480px', lock: true,opacity: 0.1,fixed: true,close:function(){loadClpsByPcId(pc_id,product_name,$("#customerId").val());}});
}

function addBoxPsIdIndex(pc_id , product_name){
  	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_ps_index.html?pc_id="+pc_id; 
	 $.artDialog.open(uri , {title:"["+ product_name +"]"+ " 盒子到ShipTo顺序",width:'830px',height:'480px', lock: true,opacity: 0.1,fixed: true});
}

</script>
