<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long waybill_id = StringUtil.getLong(request,"waybill_id");
DBRow[] lackingProducts = productMgr.outStockProductForWaybill(waybill_id);

long psid = StringUtil.getLong(request,"psid");
long s_pid = StringUtil.getLong(request,"s_pid");
long s_pc_id = StringUtil.getLong(request,"s_pc_id");
long cid = StringUtil.getLong(request,"cid");
String pcids = StringUtil.getString(request,"pcids");
String store_type = StringUtil.getString(request,"store_type");

DBRow detailPro = productMgr.getDetailProductByPcid(s_pc_id);

DBRow detailDetailSetProductProductStorage = null; 
int combinationCount = 0;
if(store_type.equals("store"))
{
	detailDetailSetProductProductStorage = productMgr.getDetailProductProductStorageByPcid(cid,s_pc_id);
	combinationCount = (int)detailDetailSetProductProductStorage.get("store_count",0f);
}
else if(store_type.equals("back_up_store"))
{
	detailDetailSetProductProductStorage = backUpStorageMgrZJ.getDetailBackUpStore(s_pc_id,cid);
	combinationCount = (int)detailDetailSetProductProductStorage.get("back_up_store_count",0f);
}




%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<script>

$().ready(function() {

	function log(event, data, formatted) {

		
	}
		
});

function checkForm()
{	
	var theForm = document.add_union_form;
	
	if( theForm.split_quantity.value=="" )
	{
		alert("需要拆散数不能为空");
	}
	else if( !isNum(theForm.split_quantity.value)||theForm.split_quantity.value><%=combinationCount%>||theForm.split_quantity.value<=0)
	{
		alert("需要拆散数超出允许范围");
	}
	else
	{
		parent.splitProduct(<%=psid%>,<%=s_pc_id%>,theForm.split_quantity.value)
	}
}

function checkUseQuantity(_this,union_quantity)
{
	var node = $(_this);
	var value = node.val();
	var fixValue = value.replace(/[^0-9]/g,'');
	fixValue = fixValue*1;
	if(fixValue<union_quantity)
	{
		node.val(fixValue);
	}
	else
	{
		node.val(union_quantity);
	}
	
}

function loadSelectUseProduct()
{
	var use_product_html = "<table border='0' cellpadding='2' cellspacing='0' width='100%'>";
	
	var pcids = '<%=pcids%>';
	var pcid = pcids.split(",");
	
	for(var i=0;i<pcid.length-1;i++)
	{
		$("[value='"+pcid[i]+"']").attr("checked","checked");	
		
		var pc_id = pcid[i]; 
		var p_name = $("#p_name_"+pc_id).val();
		var union_quantity = $("#union_quantity_"+pc_id).val();
				
		use_product_html += "<tr><td align='left'>"+p_name+"</td>"
							+"<td align='right'><input onkeyup='checkUseQuantity(this,"+union_quantity+")' style='width:40px;' name='use_quantity_"+pc_id+"' value='"+union_quantity+"'/>"
							+"<input type='hidden' name='use_pc_id' value='"+pc_id+"'/></td></tr>"
	}
	
	use_product_html +="</table>";
	$("#use_product").html(use_product_html)
}

function selectUseProduct()
{
	var use_product_html = "<table border='0' cellpadding='2' cellspacing='0' width='100%'>";
	$("[name='pc_id']").each(function(){
			if(this.checked)
			{
				var pc_id = this.value; 
				var p_name = $("#p_name_"+pc_id).val();
				var union_quantity = $("#union_quantity_"+pc_id).val();
				
				use_product_html += "<tr><td align='left'>"+p_name+"</td>"
									+"<td align='right'><input onkeyup='checkUseQuantity(this,"+union_quantity+")' style='width:40px;' name='use_quantity_"+pc_id+"' value='"+union_quantity+"'/>"
									+"<input type='hidden' name='use_pc_id' value='"+pc_id+"'/></td></tr>"
			}	
		})
	use_product_html +="</table>";
	$("#use_product").html(use_product_html)
	
}

function useFormSubmit()
{
	document.use_form.submit();
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}((\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="loadSelectUseProduct()">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  	<td colspan="2" align="left" valign="top">
  		<font style="font-size:14px;">缺货列表:</font>
  				<%
    				for(int i = 0;i<lackingProducts.length;i++)
    				{
    			%>
    				<font style="font-size:14;font-weight:bolder;color:red;"><%=lackingProducts[i].getString("p_name")%></font>
    				<font style="font-size:14;font-weight:bolder;">X</font>
    				<font style="font-size:14;font-weight:bolder;color:red;font-style:italic;"><%=lackingProducts[i].get("lackingCount",0f)%></font>
    				<font style="padding:5px;"><strong>|</strong></font>
    			<%
    				}
    			%>
  	</td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top">
    	<table width="98%" height="100%" border="0" cellspacing="0" cellpadding="0">
      		<tr>
        		<td>
						<table width="98%" border="0" height="100%" cellspacing="0" cellpadding="0">
						  <tr>
						    <td align="left" valign="middle" style="padding:10px;">
								<span style="font-weight:bold;color:#666666;font-size:20px;font-family:Arial, Helvetica, sans-serif;"><span style="color:#990000">-</span><%=detailPro.getString("p_name")%></span>
							</td>
							<td style="padding-left10px;border-left-color:black;border-left-style: solid;border-left-width:1px;">
								<span style="font-weight:bold;color:#666666;font-size:20px;font-family:Arial, Helvetica, sans-serif;"><span style="color:#990000">-</span>需要的商品</span>
							</td>
						  </tr>
						  <tr>
						    <td width="50%" align="left" valign="top" style="padding:10px;">
							    <table width="100%" border="0" cellspacing="0" cellpadding="3">
								<%
								DBRow inSetProducts[] = productMgr.getProductsInSetBySetPid(s_pc_id);
								for (int i=0; i<inSetProducts.length; i++)
								{
									float sub_quantity = inSetProducts[i].get("quantity",0f);
								%>
							      <tr>
							        <td style="font-family:Arial, Helvetica, sans-serif">
							        	<input type="checkbox" name="pc_id" value="<%=inSetProducts[i].get("pc_id",0l)%>" onclick="selectUseProduct()"/><%=inSetProducts[i].getString("p_name")%> x <span style="color:#CC0000"><%=sub_quantity%></span>
							        	<input type="hidden" id="p_name_<%=inSetProducts[i].get("pc_id",0l)%>" value="<%=inSetProducts[i].getString("p_name")%>"/>
							        	<input type="hidden" id="union_quantity_<%=inSetProducts[i].get("pc_id",0l)%>" value="<%=sub_quantity%>"/>
									</td>
							      </tr>
								<%
								}
								%>
							    </table>
							</td>
						    <td  width="50%" align="left" valign="top" style="padding-left:10px;border-left-color:black;border-left-style: solid;border-left-width:1px;">
								<form name="use_form" action="<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/product/splitProductForWaybill.action">
									<input type="hidden" name="split_union_pc_id" value="<%=s_pc_id%>"/>
									<input type="hidden" name="ps_id" value="<%=psid%>"/>
									<input type="hidden" name="waybill_id" value="<%=waybill_id%>"/>
									<input type="hidden" name="store_type" value="<%=store_type%>">
									<div id="use_product">
										&nbsp;
									</div>
								</form>
						    </td>
						  </tr>
						</table>
				 </td>
			</tr>
		</table>	
	</td>
  </tr>
  <tr>
  	<td width="68%" align="left" class="win-bottom-line" style="color:#999999;padding-left:10px;font-size:12px;">
		<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle">
		选择你需要的散件与数量，每次只能拆散一个套装。
	</td>
	    <td width="32%" align="right" class="win-bottom-line">
		<%
		if ( combinationCount>0 )
		{
		%>
	      <input type="button" name="Submit2" value="拆散" class="normal-green" onClick="useFormSubmit()">
		<%
		}
		%>
		  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close();">
	</td>
  </tr>
</table>
</body>
</html>
