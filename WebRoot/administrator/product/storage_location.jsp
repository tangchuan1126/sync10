<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../include.jsp"%> 
<%
 	long cid = StringUtil.getLong(request,"cid");
 long psid = StringUtil.getLong(request,"psid");
 long filter_psid = StringUtil.getLong(request,"filter_psid");
 String machine_id = StringUtil.getString(request,"machine_id");

 String cmd = StringUtil.getString(request,"cmd");
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

 if (psid==0)
 {
 	psid = adminLoggerBean.getPs_id();
 }
 if (filter_psid==0)
 {
 	filter_psid = adminLoggerBean.getPs_id();
 }

 PageCtrl pc = new PageCtrl();
 pc.setPageNo(StringUtil.getInt(request,"p"));
 pc.setPageSize(30);

 DBRow storageLocations[] ;

 if (cmd.equals("filter"))
 {
 	storageLocations = productMgr.getStorageLocationByPsIdMachineID(filter_psid,machine_id, pc);
 }
 else
 {
 	storageLocations = productMgr.getAllStorageLocation(pc);
 }
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
function upload()
{
	tb_show('上传定位信息','upload_storage_location.html?title=上传定位信息&backurl=<%=StringUtil.getCurrentURL(request)%>&TB_iframe=true&height=500&width=1000',false);
}

function includeChinese(str) 
{
	var rxp=/^[\u4e00-\u9fa5]+$/g;

	return rxp.test(str);
}

function checkFileName(name)
{
	var a = name.split("\\");
	var aa = a[a.length-1].split(".");
	
	return(includeChinese(aa[0]));
}

function checkForm(theForm)
{
	if (theForm.psid.value==0)
	{
		alert("请选择对比仓库");
		return(false);
	}
	else
	{
		theForm.storage_name.value = $("#psid").getSelectedText();
		return(true);
	}
}

function getStorageLocationScanner(ps_id)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageLocationScannerJSON.action",
				{ps_id:ps_id},
				function callback(data)
				{ 
					$("#machine_id").clearAll();
					$("#machine_id").addOption("全部扫描机","");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#machine_id").addOption(data[i].machine_id,data[i].machine_id);
						});
					}
					
					var machine_id = "<%=machine_id%>";
					if (machine_id!="")
					{
						$("#machine_id").setSelectedValue(machine_id);
					}
			
				}
		);
}

function Invers(theForm){
	temp = theForm.elements.length ;
	for (i=0; i < temp; i++){
	if(theForm.elements[i].checked == 1){theForm.elements[i].checked = 0;}
	else {theForm.elements[i].checked = 1}
}
}

function switchCheckboxAction()
{
	Invers(document.data_rows_form);
}

function batchDelStorageLocation()
{
	var ok = true;
	if (typeof(document.data_rows_form.switchCheckboxSlid.length) == 'undefined')
	{
		if (document.data_rows_form.switchCheckboxSlid.checked==false)
		{
			alert("请选择需要删除的数据");
			ok = false;
		}
	}
	else
	{
		var selectOne = false;
		for (i=0; i<document.data_rows_form.switchCheckboxSlid.length; i++)
		{
			if (document.data_rows_form.switchCheckboxSlid[i].checked==true)
			{
				selectOne = true;
			}
		}
		if (!selectOne)
		{
			alert("请选择需要删除的数据");
			ok = false;
		}
	}
	
	if (ok)
	{
		document.data_rows_form.submit();
	}
}

function closeWin()
{
	tb_remove();
}

function closeWinRefrech()
{
	window.location.reload();
	tb_remove();
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}t.
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();getStorageLocationScanner(document.filter_form.filter_psid.value)">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 » 商品定位   </td>
  </tr>
</table>
<br>
<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="66%" height="30" style="border-bottom:1px #dddddd solid; padding-left:10px">
	<form action="storage_location_difference.html" method="post"  name="compare_form" onSubmit="return checkForm(this)">
	<input type="hidden" name="storage_name">
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>">
	
	<strong>仓库</strong>
	&nbsp;&nbsp;<select name="psid" id="psid">
		<option value="0">选择仓库...</option>
          <%
String qx;
DBRow storageTreeRows[] = catalogMgr.getProductStorageCatalogTree();
for ( int i=0; i<storageTreeRows.length; i++ )
{
	if ( storageTreeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
          <option value="<%=storageTreeRows[i].getString("id")%>" <%=storageTreeRows[i].get("id",0l)==psid?"selected":""%>> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageTreeRows[i].get("level",0))%>
          <%=qx%>
          <%=storageTreeRows[i].getString("title")%>          </option>
          <%
}
%>
        </select>
		
		
	<strong>&nbsp;&nbsp;商品分类</strong>
     &nbsp;&nbsp;
<select name="cid" id="cid">
      <option value="0">全部分类</option>
      <%
 qx="";
Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
DBRow treeRows[] = catalogMgr.getProductCatalogTree();

for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
      <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==cid?"selected":""%>> <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%> <%=qx%> <%=treeRows[i].getString("title")%> </option>
      <%
}
%>
    </select>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input name="Submit" type="submit" class="long-button" value="对比库存">
	  </form>
   </td>
  </tr>
  <tr>
  		<td width="34%" style="font-weight:bold;font-size:12px; padding-top:5px; padding-left:10px">
	<form action="storage_location.html" method="post"  name="filter_form" onSubmit="return checkForm(this)">
	<input type="hidden" name="cmd" value="filter">
	  仓库&nbsp;&nbsp; 
	  <select name="filter_psid" id="filter_psid" onChange="getStorageLocationScanner(this.value)">
		<option value="0">选择仓库...</option>
          <%
for ( int i=0; i<storageTreeRows.length; i++ )
{
	if ( storageTreeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
          <option value="<%=storageTreeRows[i].getString("id")%>" <%=storageTreeRows[i].get("id",0l)==filter_psid?"selected":""%>> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageTreeRows[i].get("level",0))%>
          <%=qx%>
          <%=storageTreeRows[i].getString("title")%>          </option>
          <%
}
%>
        </select>
	  
	   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;扫描机&nbsp;&nbsp;&nbsp;
	   <select name="machine_id" id="machine_id">
	   </select>&nbsp;&nbsp;
	   
	   <label>
	   <input name="Submit2" type="submit" class="long-button-export" value="过滤">
	   </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   <input name="Submit23" type="button" class="long-button-upload" onClick="upload()" value="  上传定位信息">
	</form>
	
	</td>
  </tr>
</table>
</div>


<br>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="30">
      <input name="Submit3" type="button" class="long-button-ok" value="批量删除" onClick="batchDelStorageLocation()">    </td>
  </tr>
</table>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/batchDelStorageLocation.action" method="post"  name="data_rows_form">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th class="left-title" style="vertical-align: center;text-align: center;"><input type="checkbox" name="switchCheckbox" id="switchCheckbox" value="checkbox" onClick="switchCheckboxAction()"></th>
        <th class="left-title" style="vertical-align: center;text-align: center;">扫描机</th>
        <th class="left-title" style="vertical-align: center;text-align: center;">仓库</th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;">商品条码</th>
        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title" >实际库存</th>
        <th width="12%" style="vertical-align: center;text-align: center;" class="right-title" >系统库存</th>
        <th width="26%" style="vertical-align: center;text-align: center;" class="right-title">存放位置</th>
    </tr>

<%
for (int i=0; i<storageLocations.length; i++)
{
%>

    <tr > 
      <td   width="4%" height="60" align="center" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><input type="checkbox" id="switchCheckboxSlid" name="switchCheckboxSlid" value="<%=storageLocations[i].get("sl_id",0l)%>"></td>
      <td   width="8%" align="center" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%=storageLocations[i].getString("machine_id")%></td>
      <td   width="9%" align="center" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%=storageLocations[i].getString("title")%></td>
      <td   width="31%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=storageLocations[i].getString("barcode")%></td>
      <td   align="center" valign="middle" >
	  <%
	  if (storageLocations[i].getString("quantity").equals(""))
	  {
	  	out.println("<font color=red>无</font>");
	  }
	  else
	  {
	  	out.println(storageLocations[i].getString("quantity"));
	  }
	  %>
	  
	  &nbsp;</td>
      <td   align="center" valign="middle" ><%=storageLocations[i].getString("merge_count")%>&nbsp;</td>
      <td   align="center" valign="middle" >
	  <%
	  DBRow locations[] = productMgr.getStorageLocationByBarcodeMachineID(filter_psid,storageLocations[i].get("pc_id",0l),storageLocations[i].getString("machine_id"),null);
	  for (int j=0; j<locations.length; j++)
	  {
	  %>
			<div style="padding:3px;color:#CC0000;font-weight:bold;font-size:13px;"><%=locations[j].getString("position")%> : <spab style="color:#0000FF"><%=locations[j].get("quantity",0f)%></span></div>

	  <%
	  }
	  %>
&nbsp;      </td>
    </tr>
<%
}
%>
</table>
</form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="cid" value="<%=cid%>">
<input type="hidden" name="psid" value="<%=psid%>">
<input type="hidden" name="machine_id" value="<%=machine_id%>">
<input type="hidden" name="filter_psid" value="<%=filter_psid%>">

  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
</body>
</html>
