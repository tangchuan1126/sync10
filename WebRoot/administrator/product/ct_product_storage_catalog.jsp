<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.StorageTypeKey" %>
<%
long expandID = StringUtil.getLong(request,"expandID");
 
StorageTypeKey storageTypeKey = new StorageTypeKey();
String ExportStorageAddressAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/product/ExportStorageAddressAction.action";

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Storage Address</title> 
<link href="../comm.css" rel="stylesheet" type="text/css">
<!-- <script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script> -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />		
<script type="text/javascript" src="../js/select.js"></script>
		
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.filter.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.filter.min.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 细滚动条 -->
<link rel="stylesheet" href="/Sync10-ui/lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css" />
<script type="text/javascript" src="/Sync10-ui/lib/mCustomScrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '230px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
</script>

<style type="text/css">
	.set {
	    border: 2px solid #999999;
	    font-weight: normal;
	    line-height: 18px;
	    margin-bottom: 10px;
	    margin-top: 10px;
	    padding: 7px;
	    width: 250px;
	    word-break: break-all;
	}
	body{
	    height:100%;	
	}

</style>
<script language="javascript">
<!--
$(function(){
	 $("#tabs").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
			show:function(event,ui)
			 {
				var storageType = $("#tabs a[test_index="+ui.index+"]").attr("test_class");
				showStorageCatalogTag(storageType);
			 }, 
			select:function(event, ui)
				{
					 showStorageCatalogTag(ui.index+1);
				}
		});
});

function showStorageCatalogTag(storageTypes,p){
	//alert(p);
	$.ajax({
		url:'ct_product_storage_catalog_tag_list.html',
		data:"storage_type="+storageTypes+"&p="+(p?p:0),
		dataType:'html',
		type:'post',
		beforeSend:function(request){
		  $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(html){
	    	$.unblockUI();
		   $("#storageCatalogDetail").html(html);
		   
		},
		error:function(){
		}
	});
};
function refreshWindow(){
	window.location.reload();	
};

function promptCheckAddCatalog(v,m,f)
{
	if (v=="y")
	{
		 if(f.proTextTitle == "")
		 {
				alert("请选填写新建分类名称");
				return false;
		 }
		 
		 return true;
	}

}

function modCatalog(id)
{
	tb_show('修改仓库分类',"mod_product_storage_catalog.html?id="+id+"&TB_iframe=true&height=500&width=850",false);
}

function del()
{
	if (confirm("该操作会把该货架及其子货架和货架下的所有商品一起删除，确认吗？"))
	{
		return(true);
	}
	else
	{
		return(false);
	}
}

//-->
</script>
<script>

function addCatalog(parentid) 
{
	var uri	= '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/add_product_storage_catalog.html?parentid="+parentid;
	$.artDialog.open(uri,{title:'Add Storage', height:450, width:600, lock:true,opacity:0.3,fixed:true});
	//tb_show('增加仓库子分类',"add_product_storage_catalog.html?parentid="+parentid+"&TB_iframe=true&height=500&width=850",false);
}
function modCountryProvince(storageId){

	var uri	= '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/storage_catalog/ct_storage_catalog_update_basic.html?storageId="+storageId;
	$.artDialog.open(uri,{title:'Mod Storage Basic Info', height:450, width:600, lock:true,opacity:0.3,fixed:true});
}
function modStorageDetailSend(storageId){
	var uri	= '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/storage_catalog/ct_storage_catalog_update_detail_send.html?storageId="+storageId;
	$.artDialog.open(uri,{title:'Mod Send Address', height:540, width:600, lock:true,opacity:0.3,fixed:true});
	
};
function modStorageDetailDeliver(storageId){
	var uri	= '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/storage_catalog/ct_storage_catalog_update_detail_deliver.html?storageId="+storageId;
	$.artDialog.open(uri,{title:'Mod Receive Address', height:540, width:600, lock:true,opacity:0.3,fixed:true});
	
};
function storageAreaAdjust(ps_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/storage_location/storage_location_area_list.html?ps_id="+ps_id;
	$.artDialog.open(uri,{title:'仓库区域调整', height:530, width:980, lock:true,opacity:0.3,fixed:true});
}
function storageAreaImport(ps_id){
	var width = window.innerWidth;
	var height = window.innerHeight;
	width = parseInt(width*0.9);
	height = parseInt(height*0.9);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/storage_location/add_location_area_xml_import.html?psid="+ps_id;
	//var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/storage_location/storageLocationList.html?psid="+ps_id;
	$.artDialog.open(uri,{title:'Map File', height:height+'px', width:width+'px', lock:true,opacity:0.3,fixed:true});
}
function setCountry(name,id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/ct_storage_country.html?ps_name="+name+"&ps_id="+id;
	$.artDialog.open(uri,{title:'Set Nation', height:500, width:400, lock:true,opacity:0.3,fixed:true});
}
function setProvince(name,id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/ct_storage_province.html?ps_name="+name+"&ps_id="+id;
	$.artDialog.open(uri,{title:'Set Area', height:500, width:400, lock:true,opacity:0.3,fixed:true});
}
</script>
<script language="JavaScript1.2">

function delC(id)
{
	if (confirm("确认操作吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/delProductStorageCatalog.action";
		document.del_form.id.value = id;
		document.del_form.submit();
	}
}

function markDevFlag(id)
{
	document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/catalog/MarkDevFlagAction.action";
	document.del_form.id.value = id;
	document.del_form.submit();
}

function unMarkDevFlag(id)
{
	document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/catalog/UnMarkDevFlagAction.action";
	document.del_form.id.value = id;
	document.del_form.submit();
}

function markReturnFlag(id,address,contact,phone)
{
	if (address==""||contact==""||phone=="")
	{
		alert("请先完整填写仓库地址信息");
		return(false);
	}
	else
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/catalog/MarkReturnFlagAction.action";
		document.del_form.id.value = id;
		document.del_form.submit();
		return(true);
	}
}

function unMarkReturnFlag(id,address,contact,phone)
{
	document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/catalog/UnMarkReturnFlagAction.action";
	document.del_form.id.value = id;
	document.del_form.submit();
	return(true);
}

function markSampleFlag(id)
{
	document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/catalog/MarkSampleFlagAction.action";
	document.del_form.id.value = id;
	document.del_form.submit();
}

function unMarkSampleFlag(id)
{
	document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/catalog/UnMarkSampleFlagAction.action";
	document.del_form.id.value = id;
	document.del_form.submit();
}

//修改仓库所属国家
function modCountry(name,id,inccid)
{

<%
StringBuffer countryCodeSB = new StringBuffer("");
DBRow countrycode[] = orderMgr.getAllCountryCode();

String selectBg="#ffffff";
String preLetter="";
countryCodeSB.append(" <select name='ccid' id='ccid' style='font-size:12px;'>");
countryCodeSB.append("<option value='0'>选择国家...</option>");
for (int countryCodeI=0; countryCodeI<countrycode.length; countryCodeI++)
{
  	if (!preLetter.equals(countrycode[countryCodeI].getString("c_country").substring(0,1)))
	{
		if (selectBg.equals("#eeeeee"))
		{
			selectBg = "#ffffff";
		}
		else
		{
			selectBg = "#eeeeee";
		}
	}  	
		
	preLetter = countrycode[countryCodeI].getString("c_country").substring(0,1);
	
	countryCodeSB.append("<option style='background:"+selectBg+"' value='"+countrycode[countryCodeI].getString("ccid")+"'>"+countrycode[countryCodeI].getString("c_country")+"</option>");
}
	countryCodeSB.append("</select>");
%>

	$.prompt(
	
	"<div id='title'>修改仓库所属国家</div><br /> "+name+" warehouse <br> <%=countryCodeSB.toString()%>",

	{
	      submit: promptCheckCountry,
   		  loaded:
		  
				function ()
				{
					$("#ccid").setSelectedValue(inccid);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/catalog/ModProductStorageCatalog.action";
						document.listForm.ccid.value = f.ccid;
						document.listForm.id.value = id;
						document.listForm.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function promptCheckCountry(v,m,f)
{
	if (v=="y")
	{
		  //an = m.children('#orderNoteWinText');
		  if(f.ccid == 0)
		  {
			   alert("请选择国家");
			   
				return false;
		  }
		  return true;
	}
}
//仓库地址导出
function exportStorageAddress(storage_type){
	
	$.ajax({
		url:'<%=ExportStorageAddressAction%>',
		type:'post',
		dataType:'json',
		timeout: 60000,
		cache:false,
		data:"storageType="+storage_type,
		
		beforeSend:function(request){
			$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">Export......</div>'});
	   },
		error:function(){
			showMessage("System Error.","error");
	   },
	   success:function(msg){
		   
			if(msg && msg.flag == "true"){
				window.location="../../"+msg["fileurl"];
				$.unblockUI();
			}else{
				showMessage("No Data.","alert");
				$.unblockUI();
	   }
	  }
	});
}
//地址导入
function importStorageAddress(_target,storageType){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    $("#storageType").val(storageType);
    var obj  = {
	     reg:"xls",
	     limitSize:2,
	     target:_target,
	     limitNum:1
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: 'Upload',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames){
	var storageType = $("#storageType").val();
	if($.trim(fileNames).length > 0 ){
		
		$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">Check Info<br/>Load......</span>'});
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/check_storage_address_show.html?fileNames="+fileNames+"&storageType="+storageType;
		$.artDialog.open(uri,{title: "Check Address Info",width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true});
		$.unblockUI();
	}
	
}

function setTimeOut(psId,title){
	var ps_id=psId?psId:0;
	var _title=title?title:"";
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/set_storage_timeout.html?ps_id='+ps_id+'&title='+_title;
	$.artDialog.open(url, {title: "["+_title+"] Time Out Set",width:'80%',height:'40%', lock: true,opacity: 0.3,fixed: true,id:"timeout"})
	
}
</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()" style="height:100%;">
<div id="product_storage_catalog_contain">
<div style="border-bottom:1px solid #CFCFCF; height:25px;padding-top: 15px;margin-left: 10px;margin-bottom: 3px;">
	<img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">
	<span style="font-size: 13px;font-weight: bold"> Setup » Product Storage Catalog</span>
</div>
<form method="post" name="del_form">
<input type="hidden" name="id" >
</form>

<form method="post" name="mod_form">
<input type="hidden" name="id" >
<input type="hidden" name="title">
<input type="hidden" name="address">
<input type="hidden" name="deliver_address">
<input type="hidden" name="contact">
<input type="hidden" name="phone">
<input type="hidden" name="ccid">
</form>

<form method="post" name="add_form">
<input type="hidden" name="parentid">
<input type="hidden" name="title">
<input type="hidden" name="address">
<input type="hidden" name="contact">
<input type="hidden" name="phone">
<input type="hidden" name="ccid">
</form>

<form method="post" name="listForm">
<input type="hidden" name="id" >
<input type="hidden" name="ccid">
</form>
 
 <input type="hidden" name="storageType" id="storageType">
&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-long-button-add" onClick="addCatalog(0)" value="Add Storage"><br/><br/>
<div id="tabs">
	<ul>
		<%
			ArrayList selectedKeyList = storageTypeKey.getStorageTypeKeys();
			for(int i = 0; i < selectedKeyList.size(); i ++)
			{
		%>
		<li><a href="#storageCatalogDetail" test_index="<%=i%>" test_class="<%=selectedKeyList.get(i) %>"><%=storageTypeKey.getStorageTypeKeyNameEn((String)selectedKeyList.get(i)) %><span> </span></a></li>
<%-- 		<li><a href="#storageCatalogDetail"><%=storageTypeKey.getStorageTypeKeyNameEn(StorageTypeKey.TRANSFOR) %><span> </span></a></li> --%>
<%-- 		<li><a href="#storageCatalogDetail"><%=storageTypeKey.getStorageTypeKeyNameEn(StorageTypeKey.SUPPLIER) %><span> </span></a></li> --%>
<%-- 		<li><a href="#storageCatalogDetail"><%=storageTypeKey.getStorageTypeKeyNameEn(StorageTypeKey.THIRD) %><span> </span></a></li> --%>
<%-- 		<li><a href="#storageCatalogDetail"><%=storageTypeKey.getStorageTypeKeyNameEn(StorageTypeKey.CUSTOMER) %><span> </span></a></li> --%>
<%-- 		<li><a href="#storageCatalogDetail"><%=storageTypeKey.getStorageTypeKeyNameEn(StorageTypeKey.REPAIR) %><span> </span></a></li> --%>
<%-- 		<li><a href="#storageCatalogDetail"><%=storageTypeKey.getStorageTypeKeyNameEn(StorageTypeKey.RETAILER) %><span> </span></a></li> --%>
		<%	} %>
	</ul>
	<div id="storageCatalogDetail"></div>
</div>
<br>
<br>
<br> 
</div>
<script type="text/javascript">
$(function(){
	setTimeout(function(){
		var height_=$("#product_storage_catalog_contain").parents().eq(0)[0].clientHeight;
		$("#product_storage_catalog_contain").height(height_).mCustomScrollbar();
	},500);
		
});
</script>

<style type="text/css">
	#product_storage_catalog_contain .mCSB_inside .mCSB_container{margin-right:0px;}
 	#product_storage_catalog_contain .mCSB_scrollTools{right:-6px}
</style>
</body>
</html>
