<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	long adminId = adminLoggerBean.getAdid();
 	
	long productCatalogId = StringUtil.getLong(request,"productCatalogId");
	String productCatalogName = StringUtil.getString(request,"productCatalogName");
   
	DBRow[] catalogTitles = proprietaryMgrZyj.getCustomerAndTitleByCategory(0L, productCatalogId, 0L, null, request);
 %>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<title>Manage Product Category Linked Title</title>
<style type="text/css">
.panel table{width:100%;}

.panel table tbody tr:nth-child(2n+1){
	
	background:#eeeeee;
}

.panel {
	
	margin-bottom: 20px;
	background-color: #fff;
	border: 1px #CFCFCF solid;
	-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
	box-shadow: 0 1px 1px rgba(0,0,0,.05);
	margin: 7px;
}

.panel .panelTitle{
	
	background: #f1f1f1;
	height: 40px;
	line-height: 40px;
	padding:0 10px;
	border-bottom: 1px #CFCFCF solid;
}

.panel .panelTitle .title-left{
	
	float: left;
}

.panel .panelTitle .title-right{
	float: right;
}

.clear{
	clear: both;
}

.right-title{

	height:40px;
	background-color:#fff;
}

.table {
  width: 100%;
  max-width: 100%;
}

.table>thead>tr>th {
  vertical-align: bottom;
  border-bottom: 2px solid #ddd;
  font-size: 14px;
}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
  padding: 8px;
  line-height: 1.42857143;
  vertical-align: top;
  border-top: 1px solid #ddd;
}

th {
  text-align: left;
}

table {
  border-spacing: 0;
  border-collapse: collapse;
}

table {
  background-color: transparent;
}
</style>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	
	<div class="panel">
		<div class="panelTitle">
			<div class="title-left">
				Product Category : <span style="  font-weight: bold;"><%=productCatalogName %></span>
			</div>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th>
						Title
					</th>
					<th>
						Customer
					</th>
				</tr>
			</thead>
			<tbody>
			
			<%
			if(catalogTitles != null && catalogTitles.length > 0 ){
				for(DBRow oneResult:catalogTitles){%>
				<tr>
					<td>
						<%=oneResult.getString("title_name")%>
					</td>
					<td>
						<%=oneResult.getString("customer_id")%>
					</td>
				</tr>
				<%} 
			} else {%>
				<tr>
					<td colspan="100%" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">
						No Data.
					</td>
				</tr>
			<%} %>
			</tbody>
		</table>
	</div>
</body>
</html>