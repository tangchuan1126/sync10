<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.PriceUOMKey,java.util.*,com.cwc.app.floor.api.zyj.service.AdminService,com.cwc.app.floor.api.zyj.service.ProductCustomerSerivce,com.cwc.app.floor.api.zyj.model.*"%>
<jsp:useBean id="priceUOMKey" class="com.cwc.app.key.PriceUOMKey"></jsp:useBean>
<jsp:useBean id="yesOrNotKey" class="com.cwc.app.key.YesOrNotKey"></jsp:useBean>
<%
long pc_id = StringUtil.getLong(request,"pc_id");
String numeric_error_message = "Enter numeric e.g (1.16, 2.1, 8)";
String positive_integer_error = "Enter numeric e.g (1, 2, 8)";


HttpSession sess = request.getSession(true);
Map<String,Object> loginUser = (Map<String,Object>) sess.getAttribute("adminSesion");
int corporationType = (Integer)loginUser.get("corporationType");
int customerId = 0;
//存放登录用户类型，表示用户是admin,是Customer,还是Title或者其它
String userType = "";
if(corporationType == 1){
	customerId = (Integer)loginUser.get("corporationId");
	userType = "Customer";
}else if(Boolean.TRUE.equals(loginUser.get("administrator"))){
	userType = "Admin";
}

ProductCustomerSerivce  productCustomerService = (ProductCustomerSerivce)MvcUtil.getBeanFromContainer("productCustomerSerivce");
List<Title> titles = new ArrayList<Title>();
if(customerId > 0){
	titles = productCustomerService.getTitlesByCustomerId(customerId);
}

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add Product</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/Sync10-ui/bower_components/jqueryui/themes/smoothness/jquery-ui.min.css">

<script type="text/javascript" src="/Sync10-ui/bower_components/jquery/dist/jquery.min.js"></script>

<script src="/Sync10-ui/bower_components/jqueryui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/Sync10-ui/bower_components/jquery.browser/dist/jquery.browser.min.js"></script>
<script src="/Sync10-ui/lib/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<style type="text/css">
	.aui_titleBar{  border: 1px #DDDDDD solid;}
	.aui_header{height: 40px;line-height: 40px;}
	.aui_title{
	  height: 40px;
	  width: 100%;
	  font-size: 1.2em !important;
	  margin-left: 20px;
	  font-weight: 700;
	  text-indent: 0;
	}

	td.categorymenu_td div{margin-top:-3px;}
	
	.ui-widget-content{
		border:1px #CFCFCF solid;
		border-radius: 0px;
	  	-webkit-border-radius: 0px;
		-moz-border-radius:0;
	}
	.ui-widget-header{
		background:#f1f1f1 !important;
		border-radius: 0;
		border-top:0;
		border-left:0;
		border-right:0;
		border-bottom: 1px #CFCFCF solid;
	}
	.ui-tabs .ui-tabs-nav li a{
		padding: .5em 1em 13px !important;
		outline: none !important;
	}
	.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
		height: 40px;
		margin-top: -3px !important;
		background:#fff;
	}
	.ui-tabs .ui-tabs-nav{
		padding :0 .2em 0;
	}
	.ui-tabs{
		padding:0;
	}
	
	.ui-jqgrid{border:0;}
	.ui-jqgrid-titlebar.ui-widget-header{
		background:#fff !important;
		height:35px;
		line-height:30px;
	}
	
	.panel {
		margin-top:23px;
		  margin-bottom: 20px;
		  background-color: #fff;
		  border: 1px #CFCFCF solid;
		  -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
		  box-shadow: 0 1px 1px rgba(0,0,0,.05);
		  
		}
		.panel .panelTitle{
			  background: #f1f1f1;
			  height: 40px;
			  line-height: 40px;
			  padding:0 15px;
		}
		.panel .panelTitle .title-left{
			    float: left;
		}
		.panel .panelTitle .title-right{
			  float: right;
		}
		.clear{
			  clear: both;
		}
		.right-title{
			height:40px;
			background-color:#fff;
		}
.badge{
    display: inline-block;    
    min-width: 10px;    
    padding: 3px 7px;    font-size: 12px;    font-weight: 700;    line-height: 1;color: #fff;    text-align: center;    white-space: nowrap;    vertical-align: baseline;    
    background-color: #8785F4;    
    border-radius: 15px;} .badge:hover{background-color: #4B48C2;}
		
</style>

<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script>

$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.1' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

var numeric_error_message = "Enter numeric e.g (1.16, 2.1, 8)";
var positive_integer_error = "Enter numeric e.g (1, 2, 8)";
var isMain = false;
var valid = true;
var main_code_add = '';
var userType = "<%=userType %>";

var regexp = new RegExp("^[A-Za-z0-9$+/-]+$");

$(function() {
    $( "#tabs" ).tabs({ disabled: [ 1, 2 ] });
    $(function(){
 		$(".chzn-select").chosen({no_results_text: "no this options:"});
 	});
  });
 
function show_error_message(error_box_id,message){
	$("#"+error_box_id).html(message);
	$("#"+error_box_id).removeClass("error").addClass("error_show");
}  
function hide_all_error_messages(){
	var form_data= $("#add_product_form").serializeArray();
   	for (var input in form_data){
        var error_element=$("#"+form_data[input]['name']+"_error");
        error_element.removeClass("error_show").addClass("error");
    }
}
function show_hint(id){
	$("#"+id+"_error").removeClass("error_show").addClass("error");
	$("#"+id+"_hint").removeClass("hint").addClass("hint_show");
}
function hide_hint(id){
	$("#"+id+"_hint").removeClass("hint_show").addClass("hint");
}
function setUp(form_id){
	var form_data= $(":input[type='text']");
	form_data.each(function(){
		if($("#"+this.name+"_error").length) $("#"+this.name+"_error").removeClass("error_show").addClass("error");
        if($("#"+this.name+"_hint").length){
        	$("#"+this.name+"_hint").removeClass("hint_show").addClass("hint");
        	$("#"+this.name).blur(function() {hide_hint(this.name);validateField(this.name);});
        	$("#"+this.name).focus(function() { show_hint(this.name);});
        	
        }
       
	});
	$('#lsn').on('blur', function(){
		checkSN(this);
	});

}
function validateField(id){

	no_error = true;
 	switch(id){
		case "p_name":
			p_name = $("#"+id);
			if (p_name.val() == ""){
				show_error_message(id+"_error","Please enter product name");no_error =  false;
			}else {
				if(p_name.val()!=$("#hidden_p_name").val()){
					no_error = check_pname_availability();
				}
			}
			break;
		case "p_code":
			p_code = $("#"+id);
			if (p_code.val() == ""){
				show_error_message(id+"_error","Please enter main code");no_error =  false;
			}else {
				if(!regexp.test(p_code.val())){
					show_error_message(id+"_error","Main code only support numbers, letters and $ - + /"); no_error = false;
				}
				/*else if(p_code.val()!=$("#hidden_p_code").val()){
					no_error = check_pcode_availability();
				}*/
			}
			break;
	/*	case "unit_name":
			if($("#"+id).val()==""){
				show_error_message("unit_name_error","please enter unit name");no_error =  false;
			}
			break;
			*/
		case "length":
			length = $("#"+id);
			
			if (!length.val()){
				show_error_message(id+"_error","Please enter length");no_error =  false;
			}else if(!isNumeric(length.val())){
				show_error_message(id+"_error",numeric_error_message);no_error =  false;
			}else if(window.parseFloat(length.val()) <= 0){
				show_error_message(id+"_error","Length must be greater than 0");no_error =  false;
			}else{
				$("#" + id).val(formatNumber(length.val(),3));
			}
			break;
		case "height":
			height = $("#"+id);
			if (!height.val()){
				show_error_message(id+"_error","Please enter height");no_error =  false;
			}else if(!isNumeric(height.val())){
				show_error_message(id+"_error",numeric_error_message);no_error =  false;
			}else if(window.parseFloat(height.val()) <= 0){
				show_error_message(id+"_error","Height must be greater than 0");no_error =  false;
			}else{
				$("#" + id).val(formatNumber(height.val(),3));
			}
			break;
		case "width":
			width = $("#"+id);
			if (!width.val()){
				show_error_message(id+"_error","Please enter width");no_error =  false;
			}else if(!isNumeric(width.val())){
				show_error_message(id+"_error",numeric_error_message);no_error =  false;
			}else if(window.parseFloat(width.val()) <= 0){
				show_error_message(id+"_error","Width must be greater than 0");no_error =  false;
			}else{
				$("#" + id).val(formatNumber(width.val(),3));
			}
			break;
		case "weight":
			weight = $("#"+id);
			if (!weight.val()){
				show_error_message(id+"_error","Please enter weight");no_error =  false;
			}else if(!isNumeric(weight.val())){
				show_error_message(id+"_error",numeric_error_message);no_error =  false;
			}else if(window.parseFloat(weight.val()) <= 0){
				show_error_message(id+"_error","Weight must be greater than 0");no_error =  false;
			}else{
				$("#" + id).val(formatNumber(weight.val(),3));
			}
			break;
		case "unit_price":
			unit_price = $("#"+id);
			if (!unit_price.val()){
				show_error_message(id+"_error","Please enter price");no_error =  false;
			}else if(!isNumeric(unit_price.val())){
				show_error_message(id+"_error",numeric_error_message);no_error =  false;
			}else if(window.parseFloat(unit_price.val()) < 0){
				show_error_message(id+"_error","Unit price must be greater or equal to 0");no_error =  false;
			}else{
				$("#" + id).val(formatNumber(unit_price.val(),2));
			}
			break;
		//case "gross_profit":
		case "freight_class":
			if($("#"+id).val()){
				if(!checkPositiveInteger($("#"+id).val())){
					show_error_message(id+"_error",positive_integer_error);no_error =  false;
				}
			}
			break;
		
		case "lsn":
			lsn = $("#"+id);
			if($("#product_key").val() == 1){
				
				if(!lsn.val()){
					show_error_message(id+"_error","Please enter SN length");no_error =  false;
				}else if(!checkPositiveInteger(lsn.val())){
					show_error_message(id+"_error",positive_integer_error);no_error =  false;
				}else if(window.parseFloat(lsn.val()) <= 0){
					show_error_message(id+"_error","SN length must be greater than 0");no_error =  false;
				}
			}
			
			break;
			
		default:
			
	}

	return no_error;

}

/**
 * 对数字字符串进行格式化,小数点后最多保留length位有效数字
 *
 * @param number  数字
 * @param length 小数点后保留位数
 */
function formatNumber(number_str,length){
	var count = 0;
	if(number_str.indexOf(".") >= 0){
		count = number_str.substring( number_str.indexOf(".") + 1 ).length;
	}
	
	if(count > length){
		var temp = new Number(parseFloat(number_str));
		return temp.toFixed(length) + "";		
	}else{
		return number_str;
	}
}

function validate_form(){
	form_data= $(":input[type='text']");
	var data = $(".data_error");
	var error_flag = true;
	
	form_data.each(function(){
		if(!validateField(this.id)){
			error_flag = false;
		}
	});

	data.each(function(){
		var $this = $(this);
		if($("#product_key").val() == 1){
			if(!$this.val()){
				$this.parent().next().find('.error').html("Please enter SN length");
				$this.parent().next().find('.error').removeClass("error").addClass("error_show");
				error_flag = false;
			}else if(!isNumeric($this.val())){
				$this.parent().next().find('.error').html(numeric_error_message);
				$this.parent().next().find('.error').removeClass("error").addClass("error_show");
				error_flag = false;
			}else if(window.parseFloat($this.val()) <= 0){
				$this.parent().next().find('.error').html("SN length must be greater than 0");
				$this.parent().next().find('.error').removeClass("error").addClass("error_show");
				error_flag = false;
			}else{
				//$this.parent().next().find('.error_show').removeClass("error_show").addClass("error");
				if(error_flag)
				error_flag = true;
			}
		}else{
			$this.parent().next().find('.error_show').removeClass("error_show").addClass("error");
			//error_flag = true;
		}
	});

	if(userType == "Customer"){
		if(parseInt($.trim($("#title_id").val())) == 0){
			$("#title_error").addClass("error_show").text("Please select title");
			error_flag = false;
		}else{
			$("#title_error").removeClass("error_show").empty();
		}
	}
	return error_flag;
}

function isNumeric( obj ) {
    return !$.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
}
function checkPositiveInteger(objVal)  
{  
     var re = /^[1-9]+[0-9]*]*$/;
     if (re.test(objVal))  
     {  
        return true;  
     }  
     return false;
}  
function addProduct()
{
		var f = document.add_product_form;
		/*f.action = "<=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProduct.action";*/
		f.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addNewProductAction.action";
		f.submit();		 
}
function check_pcode_availability(){
	var f = document.add_product_form;
	pcode = f.p_code.value;
	var para ="p_code="+pcode;
	no_error = true;
	$.ajax({
				url:  '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetProductByCodeJSONAction.action',
				type: 'post',
				timeout: 60000,
				cache:false,
				dataType: 'json',
				data:para,
				async:false,
				
				beforeSend:function(request){
				},
				
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				},
				
				success: function(data){
					if(data.p_code!=null){
						show_error_message("p_code_error","code already exists");
						no_error = false;
					}
				}
			});
	return no_error;	

}
function check_pname_availability(){
	var f = document.add_product_form;
	pname = f.p_name.value;
	var para ="p_name="+pname;
	no_error = true;
	$.ajax({
				url:  '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetProductByNameJSONAction.action',
				type: 'post',
				timeout: 60000,
				cache:false,
				dataType: 'json',
				data:para,
				async:false,
				
				beforeSend:function(request){
				},
				
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				},
				
				success: function(data){
					if(data.total_products>0){
						show_error_message("p_name_error","product already exists");
						no_error = false;
					}
					//return data;
				}
			});
	return no_error;	

}
function save_product(){
		var f = document.add_product_form;
		var lsnArr = "";
		if($("#product_key").val() == 1){
			if(f.lsn.length == undefined){
				lsnArr = f.lsn.value;
			}else{
				for(var i = 0 ; i< f.lsn.length ; i ++){
					lsnArr += f.lsn[i].value + ",";
				}
				lsnArr = lsnArr.substring(0 , lsnArr.length -1);
			}
		}else{
			var data = $(".data_error");
			data.each(function(){
				var $this = $(this);
				$this.val("");
			});
		}
		if(validate_form() && valid && checkIsExist()){
			var titleId = 0;
			if(userType == "Customer"){
				titleId = $("#title_id").val();
			}
			var para ="p_name="+f.p_name.value+"&p_code="+f.p_code.value+"&length="+f.length.value+"&height="+f.height.value+"&width="+f.width.value
					+"&weight="+f.weight.value+"&unit_price="+f.unit_price.value+"&lsn="+lsnArr + "&customerId=" + $("#customerId").val() + "&titleId=" + titleId
					+"&length_uom="+f.length_uom.value+"&weight_uom="+f.weight_uom.value+"&price_uom="+f.price_uom.value+"&nmfc_code="+f.nmfc_code.value+"&freight_class="+f.freight_class.value;
					//+"&gross_profit="+f.gross_profit.value+"&volume="+f.volume.value
					//+"&unit_name="+f.unit_name.value
			$.ajax({
					url:  '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addNewProductAction.action',
					type: 'post',
					timeout: 60000,
					cache:false,
					dataType: 'json',
					data:para,
					async:false,
					
					beforeSend:function(request){
					},
					
					error: function(jqXHR, textStatus, errorThrown) {
						showMessage("Update Error.","error");
						console.log(errorThrown);
					},
					
					success: function(data)
					{
						if(data != null && data.status == "success")
						{
							//var mainwindow=window.top.window.frames["iframe"].frames["main"];
							
							var mainwindow=window.parent.window;
							
							if(window.top.window.frames["iframe"])                        
							 	mainwindow=window.top.window.frames["iframe"].frames["main"];
							
							enableTabs();
							getCategoryAndCodePage(data.pc_id);
							
							$("#pc_id").val(data.pc_id);
							$("#addButton").css('display','none');
							$("#saveButton").css('display','inline');
							$("#hidden_p_name").val(f.p_name.value);
							$("#hidden_p_code").val(f.p_code.value);
							showMessage("Product added successfully.","succeed");
							
							//window.top.addRow(data.pc_id,data);
							mainwindow.addRow(data.pc_id,data);
							
							
							$("#codes_frame").attr('src',"/Sync10-ui/pages/product/index.html?pc_id="+data.pc_id);
							$("#tabs").tabs("load", 1);
						}
					}
				});
		
		
		}
		
}

function update_product(){
	
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '}); //遮罩打开
	var f = document.add_product_form;
	
	var lsnArr = "";
	if($("#product_key").val() == 1){
		if(f.lsn.length == undefined){
			lsnArr = f.lsn.value;
		}else{
			for(var i = 0 ; i< f.lsn.length ; i ++){
				lsnArr += f.lsn[i].value + ",";
			}
			lsnArr = lsnArr.substring(0 , lsnArr.length -1);
		}
	}else{
		var data = $(".data_error");
		data.each(function(){
			var $this = $(this);
			$this.val("");
		});
	}
	
	if(validate_form() && valid && checkIsExist()){
		
		var para ="p_name="+f.p_name.value+"&p_code="+f.p_code.value+"&length="+f.length.value+"&height="+f.height.value+"&width="+f.width.value
					+"&weight="+f.weight.value+"&unit_price="+f.unit_price.value+"&lsn="+lsnArr+"&pc_id="+f.pc_id.value
					+"&length_uom="+f.length_uom.value+"&weight_uom="+f.weight_uom.value+"&price_uom="+f.price_uom.value+"&nmfc_code="+f.nmfc_code.value+"&freight_class="+f.freight_class.value;
		$.ajax({
				url:  '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProduct.action',
				type: 'post',
				timeout: 60000,
				cache:false,
				dataType: 'html',
				data:para,
				async:false,
				error: function(jqXHR, textStatus, errorThrown) {
					showMessage("Error while updating product","error");
					$.unblockUI();//遮罩关闭
					console.log(textStatus, errorThrown);
				},
				success: function(data)
				{
					$.unblockUI();//遮罩关闭
					
					var mainwindow=window.parent.window;
					
					if(window.top.window.frames["iframe"])                        
					 	mainwindow=window.top.window.frames["iframe"].frames["main"];
					
					mainwindow.search();
					
					showMessage("Update Success.","succeed");
					$("#codes_frame").attr('src',"/Sync10-ui/pages/product/index.html?pc_id="+f.pc_id.value);
					$("#tabs").tabs("load", 1);
				}
			});
	}else{
		$.unblockUI();//遮罩关闭
	}
}

function enableTabs(){
	$("#tabs").tabs("enable", 1);
	$("#tabs").tabs("enable", 2);
}
function nextTab(){
	$("#tabs").tabs('option', 'selected', 1);
	$("#tabs").tabs("disable", 0);
	
}
function getCategoryAndCodePage(pc_id){
	$("#codes_frame").attr('src',"/Sync10-ui/pages/product/index.html?pc_id="+pc_id);
	$("#tabs").tabs("load", 1);
	$("#category_frame").attr('src',"mod_product_catalog.html?pc_id="+pc_id+"&iframe=true");
	$("#tabs").tabs("load", 2);
}
function reloadMainCode()
{
	if(isMain && main_code_add != '')
	{
		$("#p_code").val(main_code_add);
		$("#hidden_p_code").val(main_code_add);
		isMain = false;
	}
}

// yuanxinyu > sn length > modify > one to more

function delLsnInput(obj){
	var $self = $(obj),
		$parent = $self.parent().parent();
	
	if($self.next('.add_lsn_input').css('display') === "none"){
		$self.parent().parent().remove();
	}else{
		$(".add_lsn_input").hide();
		
		$parent.prev().find(".add_lsn_input").show();
		$parent.remove();
	}
	
}

function addLsnInput(obj){
	var html = "<tr class='lsn_tr'>";
		html +="<td class='STYLE3' width='120px'></td>";
		html +="<td align='left' valign='middle'>";
		html +="<input class='data_error' name='lsn' type='text' style='width:100px;height:30px;margin-left: 2px;' /> ";
		html +="<input type='button' class='del_lsn_input'> ";
		html +="<input type='button' class='add_lsn_input'>";
		html +="</td>";
		html +="<td>";
		html +="<span class='error'></span> ";
		html +="<span class='hint'>e.g (4, 8, 12)</span>";
		html +="</td>";
		html +="</tr>";

		var $h = $(html);
	
	$(obj).parent().parent().after($h);
	$(obj).hide();
	
	$h.on('blur', '.data_error', function(){
		var $this = $(this);
		$h.find('.hint_show').removeClass("hint_show").addClass("hint");
		
		if(!$this.val()){
			$h.find('.error').html("Please enter SN length");
			$h.find('.error').removeClass("error").addClass("error_show");
			valid = false;
		}else if(!isNumeric($this.val())){
			$h.find('.error').html(numeric_error_message);
			$h.find('.error').removeClass("error").addClass("error_show");
			valid = false;
		}else if(window.parseFloat($this.val()) <= 0){
			$h.find('.error').html("SN length must be greater than 0");
			$h.find('.error').removeClass("error").addClass("error_show");
			valid = false;
		}else{
			$h.find('.error_show').removeClass("error_show").addClass("error");
			valid = true;
		}
		
		valid = checkSN(this) && valid;
		
	}).on('focus', '.data_error', function(){
		$h.find('.error_show').removeClass("error_show").addClass("error");
		$h.find('.hint').removeClass("hint").addClass("hint_show");
	}).on('click', '.add_lsn_input', function(){
		addLsnInput(this);
	}).on('click', '.del_lsn_input', function(){
		delLsnInput(this);
	});
}

function checkIsExist(){
	var retuExi = true;
	var array = new Array();
	var data = $(".data_error");
	
	var dup = true;

	data.sort(function(a,b){
		
		if($("#product_key").val() == 1){
			 if(a.value == b.value){
				$(a).parent().next().find('.error').html("SN length already exists");
				$(a).parent().next().find('.error').removeClass("error").addClass("error_show");
				$(b).parent().next().find('.error').html("SN length already exists");
				$(b).parent().next().find('.error').removeClass("error").addClass("error_show");
				dup = false;
			 }
		}else{
			$(a).parent().next().find('.error').removeClass("error_show").addClass("error");
			$(b).parent().next().find('.error').removeClass("error_show").addClass("error");
			dup = true;
		}
	});
	
	return dup;
}

function checkSN(obj){
	var data = $(".data_error");
	var resVal = true;
	data.each(function(i,elem){
		var currVal = $(obj).val();
		var _thisVal = $(this).val();
		var _isExist = $(obj).parent().next();
		var _alIsExist = $(this).parent().next();
		if($("#product_key").val() == 1){
			
			if(obj != elem){
				if( currVal && _thisVal && currVal == _thisVal){
					_alIsExist.find('.error_show').removeClass("error_show").addClass("error");
					_isExist.find('.error').html("SN length already exists");
					_isExist.find('.error').removeClass("error").addClass("error_show");
					resVal = false;
				}else{
					_alIsExist.find('.error_show').removeClass("error_show").addClass("error");
				}
			}
		}else{
			_alIsExist.find('.error_show').removeClass("error_show").addClass("error");
			_isExist.find('.error').removeClass("error_show").addClass("error");	
			resVal = true;
		}
		
	});
	
	return resVal;
}

function hw_change(){
	var value = $("#length_uom").find("option:selected").attr("unit");
	$(".hw_style").html("");
	$(".hw_style").html(value);
}




function product_key_change(){
	var pk = $("#product_key").val();
	if(pk == 2){
		$(".lsn_tr").css("display","none");
	}else{
		$(".lsn_tr").css("display","");
	}
}

function changeColorBlack(){

	if($("#unit_price").val()!="")
	{
 	 	 $("#unit_price").val("");
 	 	 $("#unit_price").attr("class", "");
 	}
}

function changeColorGray()
{
	 
	 if($("#unit_price").val().trim()=="")	  
	 {
		  $("#unit_price").val("0");
		  $("#unit_price").attr("class","searchbarGray");
	 }
}
$(function(){
	changeColorBlack();
	changeColorGray();
});
</script>
<style type="text/css">
<!--
.input-line
{
	width:180px;
	font-size:12px;

}
.searchbarGray{
        color:#c4c4c4;
   }
.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
.error{display: none;}  
.hint{display:none;}    
.error_show{color: red;margin-left:10px;padding:4px;padding-left:22px;font-size:11px;border:red 1px solid;
-webkit-border-radius: 4px;-moz-border-radius: 4px;border-radius:4px;
background-attachment: fixed;
	background: url(img/error_validation.png);
	background-repeat: no-repeat;
	background-position: 2% ;
	background-color:#FFF5F5;}
.hint_show{color: #4D4D4D;margin-left:10px;padding:4px;padding-left:22px;font-size:11px;border:#c2c8c8 1px solid;
-webkit-border-radius:3px;-moz-border-radius: 3px;border-radius:3px;
background-attachment: fixed;
	background: url(img/hint.png);
	background-repeat: no-repeat;
	background-position: 2% ;
	background-color:#f9f9f9;
	}
#del_lsn_input, .del_lsn_input{background: url(img/del.png) center; width:16px; height:14px;border-style:none;cursor:pointer;}
#add_lsn_input, .add_lsn_input{background: url(img/add.png) center; width:16px; height:14px;border-style:none;cursor:pointer;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

<input type="hidden" id="customerId" value="<%=customerId %>" />
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
			<div id="tabs" style="margin-top:5px;">
				<ul>
					<li><a href="#tab_basic" id="basic_info_tab_link" onclick="reloadMainCode()">Basic Info</a></li>
					<li><a href="#tab_codes" id="codes_tab_link">Product Codes</a></li>
					<li><a href="#tab_category" id="category_tab_link">Product Category</a></li>
				</ul>
				<div id="tab_basic"> <!-- basic tab starts -->
					<form name="add_product_form" method="post" action="" id="add_product_form">
			
					 <!-- <table width="100%" border="0" cellspacing="5" cellpadding="2" style="border-collapse: collapse;">
						  
					 </table> -->
					 <table width="100%" border="0" cellspacing="5" cellpadding="2" style="border-collapse: collapse;">
						 <tr>
							    <td align="right" width="140px" valign="middle" class="STYLE3" ><label for="p_name">Product Name : </label></td>
							    <td align="left" valign="middle" width="180px">
							    	<input name="p_name" type="text" class="input-line" id="p_name" style="text-transform: uppercase;height:30px;margin-left: 2px;"/>
							    	<input name="hidden_p_name" type="hidden" class="input-line" id="hidden_p_name" style="text-transform: uppercase;"/>
							   		<input type="hidden" name="pc_id" id="pc_id" value=""/>
							   	</td>
							   	<td align="left" valign="middle">
							   		<span id="p_name_error" class="error"></span>
							   		<span id="p_name_hint" class="hint">e.g (E320I-B2/14572)</span>
							   	</td>
							  </tr>
							  <tr>
							    <td align="right" width="127px" valign="middle" class="STYLE3" ><label for="p_code">Main Code : </label></td>
							    <td align="left" valign="middle" >
							    	<input name="p_code" type="text" class="input-line" id="p_code" style="text-transform: uppercase;height:30px;  margin-left: 2px;"/>
							    	<input name="hidden_p_code" type="hidden" class="input-line" id="hidden_p_code" style="text-transform: uppercase;"/>
							    	
							    </td>
							    <td>
							    	<span id="p_code_error" class="error"></span> 
							    	<span id="p_code_hint" class="hint">e.g (102534CK)</span>
							    </td>
							  </tr>
							  <tr>
							    <td align="right" width="127px" valign="middle" class="STYLE3" ><label for="nmfc_code">NMFC Code : </label></td>
							    <td align="left" valign="middle" >
							    	<input name="nmfc_code" type="text" class="input-line" id="nmfc_code" style="text-transform: uppercase;height:30px;  margin-left: 2px;"/>
							    </td>
							    <td>
							    	<span id="nmfc_code_error" class="error"></span> 
							    	<span id="nmfc_code_hint" class="hint">e.g (102534CK)</span>
							    </td>
							  </tr>
						   <tr>
							    <td align="right" valign="middle" class="STYLE3" width="127px"><label for="width">Freight Class : </label></td>
							    <td align="left" valign="middle" >
							    	<input name="freight_class" type="text" id="freight_class"  style="width:100px;height:30px;margin-left: 2px;" />&nbsp;
							    	
							    </td>
							    <td>
							    	<span id="freight_class_error" class="error"></span> 
							    	<span id="freight_class_hint" class="hint"><%=positive_integer_error %></span>
							    </td>
						  </tr>
						  <tr>
						    <td align="right" valign="middle" class="STYLE3" width="127px"><label for="length">Length : </label></td>
						    <td align="left" valign="middle" style="width:180;position: relative;">
						    	<input name="length" type="text" id="length"  style="width:100px;height:30px;margin-left: 2px;" />
						    	<%ArrayList lengthUomKeys = lengthUOMKey.getLengthUOMKeys(); %>
								<div class="side-by-side clearfix" style="position: absolute; left: 110px; top: 4px;">
						 	     <select id="length_uom" name="length_uom" onchange="hw_change();" class="chzn-select" data-placeholder="Choose a UOM..." tabindex="1"  style="width:74px;height:50px;">
						 	     	<%for(int a=0;a<lengthUomKeys.size();a++){ %>
								    <option value="<%=Integer.parseInt(String.valueOf(lengthUomKeys.get(a))) %>" unit="<%=lengthUOMKey.getLengthUOMKey(Integer.parseInt(String.valueOf(lengthUomKeys.get(a))))%>"><%=lengthUOMKey.getLengthUOMKey(Integer.parseInt(String.valueOf(lengthUomKeys.get(a))))%></option>
								    <%}%>
								 </select>
								 </div>
						    </td>
						    <td>
						    	<span id="length_error" class="error"></span> 
						    	<span id="length_hint" class="hint"><%=numeric_error_message %></span>
						    </td>
						  </tr>
						  <tr>
						    <td align="right" valign="middle" class="STYLE3" width="127px"><label for="width">Width : </label></td>
						    <td align="left" valign="middle" >
						    	<input name="width" type="text" id="width"  style="width:100px;height:30px;margin-left: 2px;" />&nbsp;&nbsp;&nbsp;
						    	<span class="hw_style"></span>
						    </td>
						    <td>
						    	<span id="width_error" class="error"></span> 
						    	<span id="width_hint" class="hint"><%=numeric_error_message %></span>
						    </td>
						  </tr>
						  <tr>
						    <td align="right" valign="middle" class="STYLE3" width="127px"><label for="height">Height : </label></td>
						    <td align="left" valign="middle" >
						    	<input name="height" type="text" id="height"  style="width:100px;height:30px;margin-left: 2px;" />&nbsp;&nbsp;&nbsp;
						    	<span class="hw_style"></span>
						    </td>
						    <td>
						    	<span id="height_error" class="error"></span> 
						    	<span id="height_hint" class="hint"><%=numeric_error_message %></span>
						    </td>
						  </tr>
						  <tr>
						    <td align="right" valign="middle" class="STYLE3" width="127px"><label for="weight">Weight : </label></td>
						    <td align="left" valign="middle" style="width:180;position: relative;">
						    	<input name="weight" type="text" id="weight"  style="width:100px;height:30px;margin-left: 2px;" />
						    	<%ArrayList weightUomKeys = weightUOMKey.getWeightUOMKeys(); %>
								<div class="side-by-side clearfix" style="position: absolute; left: 110px; top: 4px;">
						 	     <select id="weight_uom" name="weight_uom" class="chzn-select" data-placeholder="Choose a UOM..." tabindex="1"  style="width:74px;height:30px;">
						 	     	<%for(int a=0;a<weightUomKeys.size();a++){ %>
								    <option value="<%=Integer.parseInt(String.valueOf(weightUomKeys.get(a))) %>" ><%=weightUOMKey.getWeightUOMKey(Integer.parseInt(String.valueOf(weightUomKeys.get(a))))%></option>
								    <%}%>
								 </select>
								 </div>
						    </td>
						    <td>
						    	<span id="weight_error" class="error"></span> 
						    	<span id="weight_hint" class="hint"><%=numeric_error_message %></span>
						    </td>
						  </tr>
						  <tr>
						    <td align="right" valign="middle" class="STYLE3" width="127px"><label for="unit_price">Price : </label></td>
						    <td align="left" valign="middle" style="width:180;position: relative;">
						    	<input name="unit_price" type="text" id="unit_price"  onblur="changeColorGray()" onfocus="changeColorBlack()" value="0"  style="width:100px;height:30px;margin-left: 2px;"/>&nbsp;
						    	<%ArrayList priceUomKeys = priceUOMKey.getMoneyUOMKeys(); %>
								<div class="side-by-side clearfix" style="position: absolute; left: 110px; top: 4px;">
						 	     <select id="price_uom" name="price_uom" class="chzn-select" data-placeholder="Choose a UOM..." tabindex="1"  style="width:74px;height:30px;">
						 	     	<%for(int a=0;a<priceUomKeys.size();a++){ %>
								    <option value="<%=Integer.parseInt(String.valueOf(priceUomKeys.get(a))) %>" ><%=priceUOMKey.getMoneyUOMKey(Integer.parseInt(String.valueOf(priceUomKeys.get(a))))%></option>
								    <%}%>
								 </select>
								 </div>
						    </td>
						    <td>
						    	<span id="unit_price_error" class="error"></span> 
						    	<span id="unit_price_hint" class="hint"><%=numeric_error_message %></span>
						    </td>
						  </tr>
						  <tr>
						    <td align="right" valign="middle" class="STYLE3" width="127px"><label for="serial_product">Serialized Product : </label></td>
						    <td align="left" valign="middle" style="width:100;">
						    	<%ArrayList yesOrNotKeys = yesOrNotKey.getYesOrNotKeys(); %>
								<div class="side-by-side clearfix" style="margin-left: 2px;">
						 	     <select id="product_key" name="prodcut_key" class="chzn-select" data-placeholder="Choose a key..." tabindex="1"  style="width:100px;height:30px;" onchange="product_key_change();">
						 	     	<%for(int a=0;a<yesOrNotKeys.size();a++){ %>
								    <option value="<%=Integer.parseInt(String.valueOf(yesOrNotKeys.get(a))) %>" <%if(Integer.parseInt(String.valueOf(yesOrNotKeys.get(a))) == 2) {%>selected <%} %>><%=yesOrNotKey.getYesOrNotKeyName(Integer.parseInt(String.valueOf(yesOrNotKeys.get(a))))%></option>
								    <%}%>
								 </select>
								 </div>
						    </td>
						    <td>
						    	<span id="unit_price_error" class="error"></span> 
						    	<span id="unit_price_hint" class="hint"><%=numeric_error_message %></span>
						    </td>
						  </tr>
						  <tr class='lsn_tr'>
						    <td align="left" style="text-align: right;" valign="middle" class="STYLE3" width="127px"><label for="lsn">SN Length : </label></td>
						    <td align="left" valign="middle">
						    	<input class="data_error" name="lsn" type="text" id="lsn"  style="width:100px;height:30px;margin-left: 2px;" />
						    	<input type="button" class="del_lsn_input" onClick="delLsnInput(this);" style="display:none;">
						    	<input type="button" class="add_lsn_input" onClick="addLsnInput(this);">
						    </td>
						    <td>
						    	<span id="lsn_error" class="error"></span> 
						    	<span id="lsn_hint" class="hint">e.g (4, 8, 12)</span>
						    </td>
						  </tr>
						  <% if(titles != null && titles.size() > 0){ %>
							  <tr class='lsn_tr'>
							    <td align="left" valign="middle" class="STYLE3" width="125px"><label for="lsn">Title : </label></td>
							    <td align="left" valign="middle">
								<select id="title_id" name="title_id"  class="chzn-select" data-placeholder="Choose a Title..." tabindex="1"  style="width:200px">
									<option value="0">Title...</option>
									<%
									 for(Title title : titles){ 
									%>	
								  	 	<option  value='<%=title.getId() %>'><%=title.getName() %></option>
								  	<%} %>
								</select>							    	
							    </td>
							    <td>
							    	<span id="title_error"></span> 
							    </td>
							  </tr>						  
						  <% } %>
						  <tr >
							    <td align="left" valign="middle"  >&nbsp;</td>
							   <td align="left" valign="middle" >&nbsp;</td>
							    <td  align="right" valign="middle" style="padding-top:20px;" >
								    <div style="">
									 	<a id="addButton" name="Submit2" value="Add" class="buttons big primary" onClick="save_product();">Add</a>
										<a id="saveButton" name="Submit2" value="Save" class="buttons big primary" onClick="update_product();" style="display:none;">Save</a>
									</div>  
								</td>
						  </tr>
					</table>
					</form>	
			
				</div><!-- basic tab end -->
				<div id="tab_codes">
					<iframe id="codes_frame" src="" width="100%" height="425px;" style="border:0px;"></iframe>
				</div>	
				<div id="tab_category">
					<iframe id="category_frame" src="" width="100%" height="425px;" style="border:0px;"></iframe>
				</div>
			</div>
		</td>
      </tr>
    </table>
    </td>
  </tr>
  <script>
  $(document).ready(function() {
  		setUp("add_product_form");
  		hw_change();
  		product_key_change();
  });
  
  </script>
</table> 
</body>

</html>
