<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="retailPriceKey" class="com.cwc.app.key.RetailPriceKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
long rp_id = StringUtil.getLong(request,"rp_id");

DBRow retailPriceProducts[] = quoteMgr.getRetailPriceItemsByRpId(rp_id,null);
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>调价记录</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<link href="../comm.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />


<style type="text/css">
<!-- 


-->
</style>

<script>
function closeTBWin()
{
	tb_remove();
}

</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">


<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 客户与报价应用 »   调价记录 »   商品列表</td>
  </tr>
</table>


<br>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
<thead>
<tr>
  <th width="28%" align="left"  class="right-title"  style="text-align:left;" >商品名称</th>
  <th width="16%" align="left"  class="right-title"  style="text-align:center;" >成本</th>
  <th width="17%" align="left"  class="right-title"  style="text-align:center;" >运费</th>
  <th width="17%" align="left"  class="right-title"  style="text-align:center;" >旧价格</th>
    <th width="22%"  class="left-title" style="text-align: center;">新价格</th>
    </tr>
</thead>
<%
for (int i=0; i<retailPriceProducts.length; i++)
{
%>
  <tr>
    <td align="left" valign="middle"  ><%=retailPriceProducts[i].getString("name")%></td>
    <td align="center" valign="middle"  >￥<%=retailPriceProducts[i].getString("price")%></td>
    <td align="center" valign="middle"  >￥<%=retailPriceProducts[i].getString("shipping_fee")%></td>
    <td height="84" align="center" valign="middle"  >$<%=retailPriceProducts[i].getString("old_retail_price")%></td>
    <td height="84" align="center" valign="middle" >$<%=retailPriceProducts[i].getString("new_retail_price")%></td>
  </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><label>
      <input name="Submit" type="button" class="long-button" value="返回" onClick="history.back();">
    </label></td>
  </tr>
</table>
</body>

</html>