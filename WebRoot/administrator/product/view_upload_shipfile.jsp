<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String permitFile = "csv";
String filePath = "/administrator/product/shipfile/";

TUpload upload = new TUpload();
			
upload.useOrgName();
upload.setRootFolder(filePath);
upload.setPermitFile(permitFile);

int flag = upload.upload(request);
String msg;
boolean haveError = false;

if (flag==2)
{
	msg = "只能上传:"+permitFile;

	out.println("<script>");
	out.println("alert('"+msg+"')");
	out.println("history.back();");
	out.println("</script>");
}
else if (flag==1)
{
	msg = "上传出错";

	out.println("<script>");
	out.println("alert('"+msg+"')");
	out.println("history.back();");
	out.println("</script>");
}
else
{
	msg = upload.getFileName();
	
	String csvRow[] = productMgr.getCsv(filePath+msg);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>提交日志</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style>
.form
{
	padding:0px;
	margin:0px;
}

</style>
<script>
function inp()
{
	document.getElementById("in").disabled=true;
	document.product_form.submit();
	parent.closeWinRefrech();
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td>
				<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
				  <tr>
					<td width="50%" style="font-size:14px;font-weight:bold">当前文件:<span style="color:#FF0000"><%=msg%></span></td>
					<td width="50%" align="right" valign="middle" style="font-size:14px;font-weight:bold">发货仓库：<%=upload.getRequestRow().getString("title")%></td>
				  </tr>
				</table>
				<table width="90%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px">
				<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addShipProduct.action" method="post" name="product_form">
				<input type="hidden" name="csv_name" value="<%=filePath+msg%>">
				<input type="hidden" name="cid" value="<%=upload.getRequestRow().getString("cid")%>">
				<input type="hidden" name="backurl" value="<%=upload.getRequestRow().getString("backurl")%>">
				  <tr >
					<td width="19%" height="30" class="left-title " style="vertical-align: center;text-align: left;">单 号</td>
					<td width="21%" class="left-title " style="vertical-align: center;text-align: left;">运单号</td>
					<td width="19%" class="left-title " style="vertical-align: center;text-align: left;">计费重量</td>
					<td width="21%" class="left-title " style="vertical-align: center;text-align: left;">配货人</td>
				  </tr>
				<%
				boolean isInCatalog;
				
				int rowLen = 4;
				
				String bgcolor="";
				String fontcolor  = "";
				String tmpSplit[] = null;
				
				for (int i=1; i<csvRow.length; i++)
				{
					if (csvRow[i].trim().equals(""))
					{
						continue;
					}
					
					tmpSplit = csvRow[i].split("\\,",rowLen);
					
					if ( !isValidateRowData(tmpSplit,rowLen) )
					{
						out.println("<strong><font size=3 color=red>数据格式不正确("+i+")</font></strong>");
						return;
					}
				
					if ((i+1)%2==0)
					{
						bgcolor = "bgcolor='#f8f8f8'";
					    fontcolor = "color:#000000";
					}
					else
					{
						bgcolor="";
						fontcolor = "color:#000000";
					}
					
					if ( orderMgr.getDetailPOrderByOid( Long.parseLong(tmpSplit[0]) )==null )
					{
						bgcolor = "bgcolor='#FEB3A3'";
						haveError = true;
					}
				
				%>
				  <tr <%=bgcolor%>>
					<td width="19%" height="30" style="font-size:13px;<%=fontcolor%>;border-bottom: 1px solid #dddddd;padding-left:10px;"><%=tmpSplit[0]%></td>
					<td width="21%" style="font-size:13px;<%=fontcolor%>;border-bottom: 1px solid #dddddd;padding-left:10px;"><%=tmpSplit[1]%></td>
					<td width="19%" style="font-size:13px;<%=fontcolor%>;border-bottom: 1px solid #dddddd;padding-left:10px;"><%=tmpSplit[2]%></td>
					<td width="21%" style="font-size:13px;<%=fontcolor%>;border-bottom: 1px solid #dddddd;padding-left:10px;"><%=tmpSplit[3]%></td>
				  </tr>
				
				<%
				}
				
				}
				%>
				</form>
				</table>
		</td>
  </tr>
  <tr>
    <td align="right" valign="middle" class="win-bottom-line" style="padding-right:48px;">
	<%
	if (haveError)
	{
	%>
     
      <input name="Submit" type="button" class="normal-white" onClick="history.back()" value=" 返 回  " >
    <%
	}
	else
	{
	%>
	 <span style="font-size:14px;font-weight:bold">发货仓库：<%=upload.getRequestRow().getString("title")%></span>
	<input name="Submit" type="button" class="normal-green" id="in" onClick="inp()" value="  确认提交  ">
	<%
	}
	%>
	</td>
  </tr>
</table>
</body>
</html>
<%!
public boolean isValidateRowData(String dataSplit[],int len)
{
	if (dataSplit.length!=len)
	{
		return(false);
	}

	for (int i=0; i<dataSplit.length; i++)
	{
		if (dataSplit[i].trim().equals(""))
		{
			return(false);
		}
	}
	
	return(true);
	
}
%>
