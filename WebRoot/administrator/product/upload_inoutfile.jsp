<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long purchase_id=StringUtil.getLong(request,"purchase_id");
	String title = StringUtil.getString(request,"title");
	String backurl = StringUtil.getString(request,"backurl");
	String filetype = StringUtil.getString(request,"filetype");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script> 
function uploadPurchaseIncoming()
{
	if(document.upload_form.cid.value==0 )
	{
		alert("请选择仓库");
	}
	else if($("#file").val() == "")
	{
		alert("请选择文件上传");
		
	}
	else
	{	
		var filename=$("#file").val().split(".");
		//filename[filename.length-1]=="csv"
		if(document.upload_form.file.value.indexOf("<%=filetype%>")==-1)
		{
			var message;
			if("<%=filetype%>"=="in")
			{
				message = "请选择入库日志文件上传";
			}
			else if("<%=filetype%>"=="ship_")
			{
				message = "请选择发货日志文件上传";
			}
			alert(message);
		}
		else
		{
			if("<%=filetype%>"=="in")
			{
				document.upload_form.action = "view_upload_infile.html";
			}
			else if("<%=filetype%>"=="ship_")
			{
				document.upload_form.action = "view_upload_shipfile.html";
			}
			var selectIndex = document.upload_form.cid.selectedIndex;
			document.upload_form.machine_id.value = $("#file").val();
			document.upload_form.title.value=document.upload_form.cid.options[selectIndex].text;
			document.upload_form.submit();
			
		}	
	}
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE2 {
	font-size: medium;
	font-weight: bold;
	color: #666666;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
			<form action="view_upload_storage_location.html" name="upload_form" enctype="multipart/form-data" method="post">
			<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td colspan="2" align="left" valign="top">
			<br>
            <br>
            <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;">
						<%=title %>
						</td>
					  </tr>
				  </table>
				  <br>
<br>

					<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" >
						
						 <tr>
							<td width="13%" align="left" valign="top" nowrap ><span class="STYLE2"><%=title %></span></td>
							<td width="87%" align="left" valign="top" >
							  <label>
								<input name="file" type="file" id="file">
								<input name="backurl" id="backurl" type="hidden" value="<%=backurl%>">
								<input name="title" id="title" type="hidden">
							  </label>
						   </td>
						</tr>
						<tr>
							<td><span class="STYLE2">选择仓库</span></td>
							<td>
									<select name="cid" id="cid">
										<option value="0">选择仓库...</option>
										  <%
								DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
								String qx;
								
								for ( int i=0; i<treeRows.length; i++ )
								{
									if ( treeRows[i].get("parentid",0) != 0 )
									 {
										qx = "├ ";
									 }
									 else
									 {
										qx = "";
									 }
									 
									 if (treeRows[i].get("level",0)>1)
									 {
										continue;
									 }
								%>
										  <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==StringUtil.getLong(request,"cid")?"selected":""%>> 
										  <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										  <%=qx%>
										  <%=treeRows[i].getString("title")%>          </option>
										  <%
								}
								%>
								</select>
							</td>
						</tr>
				  </table>
				</td>
			  </tr>
			 
			  <tr valign="bottom">
				<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
				  <input type="button" name="Submit2" value="确定" class="normal-green" onClick="uploadPurchaseIncoming();">
			
			    <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">				</td>
			  </tr>
</table> 
<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id %>"/>
<input type="hidden" name="machine_id" id="machine_id"/>
</form>
</body>
</html>
