<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="inOutStoreKey" class="com.cwc.app.key.InOutStoreKey"/>
<%
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

String cmd = StringUtil.getString(request,"cmd");
long cid = StringUtil.getLong(request,"cid");
int log_type = StringUtil.getInt(request,"log_type",-1);

String input_st_date = StringUtil.getString(request,"input_st_date");
String input_en_date = StringUtil.getString(request,"input_en_date");

TDate tDate = new TDate();

if (input_st_date.equals("")&&input_en_date.equals(""))
{
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}

long st_datemark = tDate.getDateTime(input_st_date);
long en_datemark = tDate.getDateTime(input_en_date);

DBRow in_products[];

if (cmd.equals("filter")&&cid>0&&log_type==-1)
{
	in_products = productMgr.getOutProductsByStEnPcCid( cid, st_datemark, en_datemark,pc);
}
else if (cmd.equals("filter")&&log_type>-1)
{
	in_products = productMgr.getOutProductsByStEnPcCidLogType( cid,log_type, st_datemark, en_datemark,pc);
}
else if (cmd.equals("amount"))
{
	in_products = productMgr.getAmountOutProductsByStEnPcCid( cid, st_datemark, en_datemark,pc);
}
else
{
	in_products = productMgr.getOutProductsByStEnPc(st_datemark,en_datemark, pc);
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.search_form.input_st_date.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.search_form.input_en_date.value = date;
}

function onMOBg(row,cl,index)
{
	row.style.background=cl;
}

function filter()
{
	document.search_form.cmd.value="filter";
	document.search_form.submit();
}

function amount()
{
	if ( document.search_form.cid.value==0 )
	{
		alert("请选择仓库");
	}
	else
	{
		document.search_form.cmd.value="amount";
		document.search_form.submit();
	}
}

function upload()
{
	if ( document.search_form.cid.value==0 )
	{
		alert("请选择仓库");
	}
	else if (document.upload_form.file.value=="")
	{
		alert("请选择上传的日志");
	}
	else if (document.upload_form.file.value.indexOf("out_")==-1)
	{
		alert("请选择出库日志文件上传");
	}
	else
	{
		var selectIndex = document.search_form.cid.selectedIndex;
	
		document.upload_form.cid.value=document.search_form.cid.value;
		document.upload_form.title.value=document.search_form.cid.options[selectIndex].text;
		document.upload_form.submit();
	}
}

//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"   onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   出库日志</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
<td>
<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
   
      <td width="49%" height="30">
	   <form action="out_product.html" method="get" name="search_form">
		<input type="hidden" name="cmd">

			开始&nbsp;
              <input name="input_st_date" type="text" id="input_st_date" size="9" value="<%=input_st_date%>" readonly> 
              <script language="JavaScript" type="text/javascript" >
    <!--
    	stfooCalendar = new dynCalendar('stfooCalendar', 'stcalendarCallback', '../js/images/');
    //-->
    </script> 
             &nbsp;&nbsp;结束 &nbsp;
              <input name="input_en_date" type="text" id="input_en_date" size="9" value="<%=input_en_date%>"  readonly>
			   <script language="JavaScript" type="text/javascript">
    <!--
    	enfooCalendar = new dynCalendar('enfooCalendar', 'encalendarCallback', '../js/images/');
    //-->
	</script>
                &nbsp;
	    <select name="cid" id="cid">
		<option value="0">所有仓库</option>>
          <%
DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
String qx;

for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
	 
	 if (treeRows[i].get("level",0)>1)
	 {
	 	continue;
	 }
%>
          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==StringUtil.getLong(request,"cid")?"selected":""%>> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>          </option>
          <%
}
%>
        </select> 
               &nbsp; &nbsp;
			   
			
			   <select name="log_type">
			     <option value="-1">所有类型</option>
				 <option value="<%=inOutStoreKey.OUT_STORE_UPLOAD%>"  <%=log_type==inOutStoreKey.OUT_STORE_UPLOAD?"selected":""%>><%=inOutStoreKey.getVal(String.valueOf(inOutStoreKey.OUT_STORE_UPLOAD))%></option>
				 <option value="<%=inOutStoreKey.OUT_STORE_CONVERT_PRODUCT%>"  <%=log_type==inOutStoreKey.OUT_STORE_CONVERT_PRODUCT?"selected":""%>><%=inOutStoreKey.getVal(String.valueOf(inOutStoreKey.OUT_STORE_CONVERT_PRODUCT))%></option>
				 <option value="<%=inOutStoreKey.OUT_STORE_DAMAGE%>" <%=log_type==inOutStoreKey.OUT_STORE_DAMAGE?"selected":""%>><%=inOutStoreKey.getVal(String.valueOf(inOutStoreKey.OUT_STORE_DAMAGE))%></option>
		       </select>
			  
	     &nbsp; &nbsp;
			   
	          <label>
	          <input name="Submit" type="button" class="button_long_refresh" onClick="filter()" value=" 过 滤 ">
			   &nbsp;
              <input name="Submit2" type="button" class="long-button-stat" onClick="amount()" value=" 汇 总 ">
      </label>
       </form>	  </td>
  </tr>
</table>
</div></td></tr></table>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="40" align="center" valign="middle" style="font-size:18px;font-weight:bold;color:#990000">出库日志</td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form name="listForm" method="post">


    <tr> 
        <th  style="vertical-align: center;text-align: left;" class="left-title" >商品名称</th>
        <th  style="vertical-align: center;text-align: left;" class="right-title">条码</th>
        <th width="16%"  style="vertical-align: center;text-align: center;" class="right-title">仓库</th>
        <th width="10%"  style="vertical-align: center;text-align: center;" class="right-title">数量</th>
        <th width="10%" class="right-title"  style="vertical-align: center;text-align: center;">出库日期</th>
        <th width="10%" class="right-title"  style="vertical-align: center;text-align: center;">类型</th>
        <th width="10%"  style="vertical-align: center;text-align: center;" class="right-title">操作人</th>
        </tr>

    <%
String typeColor = "";
for ( int i=0; i<in_products.length; i++ )
{
	if (in_products[i].get("log_type",0)==inOutStoreKey.OUT_STORE_DAMAGE)
	{
		typeColor = "#FF0000";
	}
	else if (in_products[i].get("log_type",0)==inOutStoreKey.OUT_STORE_CONVERT_PRODUCT)
	{
		typeColor = "#0000FF";
	}
	else
	{
		typeColor = "#000000";
	}
%>
    <tr > 
      <td  width="22%" height="46" valign="middle" style='word-break:break-all;' ><%=in_products[i].getString("p_name")%>&nbsp;</td>
      <td  width="22%" valign="middle" style='word-break:break-all;' ><%=in_products[i].getString("name")%></td>
      <td  align="center" valign="middle" ><span style="word-break:break-all;"><%=in_products[i].getString("title")%></span></td>
      <td  align="center" valign="middle" ><span style="word-break:break-all;"><%=in_products[i].getString("quantity")%></span></td>
      <td  align="center" valign="middle" style='word-break:break-all;'><%=in_products[i].getString("post_date")%></td>
      <td  align="center" valign="middle" style='word-break:break-all;color:<%=typeColor%>'><%=inOutStoreKey.getVal(in_products[i].getString("log_type"))%></td>
      <td align="center" valign="middle" ><span style="word-break:break-all;"><%=in_products[i].getString("account")%></span></td>
    </tr>
    <%
}
%>
</form>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
          <input type="hidden" name="p">
		  <input type="hidden" name="input_st_date" value="<%=input_st_date%>">
		<input type="hidden" name="input_en_date" value="<%=input_en_date%>">
		<input type="hidden" name="cid" value="<%=cid%>">
		<input type="hidden" name="cmd" value="<%=cmd%>">
		<input type="hidden" name="log_type" value="<%=log_type%>">
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
</body>
</html>
