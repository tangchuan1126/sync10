<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="org.apache.xmlbeans.impl.common.SystemCache"%>
<%@ include file="../../include.jsp"%> 

<%
	long oid = StringUtil.getLong(request,"oid");
	
	long pid = StringUtil.getLong(request,"pid");
	
	DBRow productStore = productMgr.getDetailProductStorageByPid(pid);
	
	DBRow product = productMgr.getDetailProductByPcid(productStore.get("pc_id",0l));
	
	int quantity = StringUtil.getInt(request,"quantity");
	
	int type = StringUtil.getInt(request,"type");
	
	String union_flag;
	DBRow[] productUnions = null; 
	if(type==0)
	{
		union_flag = "noral";
		productUnions = productMgr.getProductUnionsBySetPid(product.get("pc_id",0l)); 
	}
	else
	{
		union_flag = "union";
		productUnions = new DBRow[1];
		
		product.add("quantity",1);
		product.add("pid",product.get("pc_id",0l));
		productUnions[0] = product;
	}
	
	
%>
<html>
  <head>
    <title></title>
  </head>
  
  <body>
  	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/returnProduct.action" name="return_product_form_<%=union_flag%>" id="return_product_form_<%=union_flag%>">
  		<input type="hidden" id="return_union_flag_<%=union_flag%>" name="return_union_flag"/>
  		<input type="hidden" id="ps_id" name="ps_id" value="<%=productStore.get("cid",0l)%>"/>
  		<input type="hidden" id="oid" name="oid" value="<%=oid%>"/>
	    <table width="100%">
	    	<tr>
	    		<td>
	    			<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	  					<legend style="font-size:15px;font-weight:normal;color:#999999;">商品信息</legend>
						  <table width="97%" border="0" align="center" cellpadding="3" cellspacing="1">
						      <tr>
							      <td height="20" bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">商品名</td>
							      <td align="center" bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">数量</td>
							      <td align="center" bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">单位</td>
							      <td bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">完好</td>
							      <td bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">功能残损</td>
							      <td bgcolor="#eeeeee" style="font-family:Arial;font-weight:bold">外观残损</td>
						      </tr>
						      <%
						      	for(int i=0;i<productUnions.length;i++)
						      	{
						      %>
						      	<tr>
							      	<td width="41%" height="25" style="font-family:Arial;font-weight:bold"><%=productUnions[i].getString("p_name")%></td>
							      	<td width="9%" align="center" style="font-family:Arial;font-weight:bold">
							      		<%=productUnions[i].get("quantity",0f)*quantity%>
							      		<input type="hidden" id="<%=union_flag%>_pc_id" name="<%=union_flag%>_pc_id" value="<%=productUnions[i].get("pid",0l)%>"/>
							      		<input type="hidden" id="<%=productUnions[i].get("pid",0l)%>" value="<%=productUnions[i].get("quantity",0f)*quantity%>"/>
							      	</td>
							      	<td width="9%" align="center" style="font-family:Arial;font-weight:bold"><%=productUnions[i].getString("unit_name")%></td>
							      	<td width="15%" style="font-family:Arial;font-weight:bold"><input id="<%=productUnions[i].get("pid",0l)+"_nor"%>" name="<%=productUnions[i].get("pid",0l)+"_nor"%>" type="text" style="width:50px;" value="0"></td>
							      	<td width="13%" style="font-family:Arial;font-weight:bold"><input id="<%=productUnions[i].get("pid",0l)+"_damage"%>" name="<%=productUnions[i].get("pid",0l)+"_damage"%>" type="text" style="width:50px;" value="0"></td>
							      	<td width="13%" style="font-family:Arial;font-weight:bold"><input id="<%=productUnions[i].get("pid",0l)+"_package_damage"%>" name="<%=productUnions[i].get("pid",0l)+"_package_damage"%>" type="text" style="width:50px;" value="0"></td>
							    </tr>
						      <%
						      	}	
						      %>
						  </table>
			    	</fieldset>
	    		</td>
	    	</tr>
	    </table>
    </form>
    <script type="text/javascript">
    	function addReturn(union)
		{
			var pc_ids = $("[name='"+union+"_pc_id']");
			
			var submit = true;
			
			for (var i=0; i<pc_ids.length; i++)
			{
				//检查每一行的值是否合格
				var pc_id = pc_ids[i].value;
				var all = parseFloat($("#"+pc_id).val());
				
				var nor = parseFloat($("#"+pc_id+"_nor").val());
				var damage = parseFloat($("#"+pc_id+"_damage").val());
				var package_damage = parseFloat($("#"+pc_id+"_package_damage").val());
				
				var sum_all = nor+damage+package_damage;
				if(all !=sum_all)
				{
					submit = false;
					break;
				}
			}
			
			if(submit)
			{
				
				$("#return_union_flag_"+union).val(union);
				
				$("#return_product_form_"+union).submit();
			}
			else
			{
				alert("退件数量与总数不匹配！");
			}
		}
    </script>
  </body>
</html>
