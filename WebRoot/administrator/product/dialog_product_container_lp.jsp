<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="containerTypeKey" class="com.cwc.app.key.ContainerTypeKey"></jsp:useBean>
<%
	long ps_id = StringUtil.getLong(request,"ps_id");
	int container_type = StringUtil.getInt(request,"containType");
    long productId=StringUtil.getLong(request,"productId");       
    String productName=StringUtil.getString(request,"productName");         
	
	String lotNumberId=StringUtil.getString(request,"lot_number_id");
	long titleId=StringUtil.getLong(request,"titleId");
	String titleName=StringUtil.getString(request,"title_name");
	String locationName=StringUtil.getString(request,"locationName");
	
	DBRow[] rows = productStoreMgrZJ.getProductStoreCountGroupByContainer(ps_id,0,container_type,titleId,productId,lotNumberId);
%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<title>库存位置</title>
<script type="text/javascript">


</script>
</head>
<body>
   <fieldset style="border:1px red solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
		 <legend style="font-size:15px;font-weight:bold;"> 
			<%=productName %>
		 </legend>
		  <div align="left" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		 	<span style="font-size:12px; font-weight: bold; color:#999">容器类型:</span>
            <span style="font-size:12px; font-weight: bold; color:#00F">[<%=containerTypeKey.getContainerTypeKeyValue(container_type)%>]</span><br/>
            <hr size="1px;" color="#dddddd">
     	 </div>
              <%
              	for(int i=0;i<rows.length;i++)
              	{
              %>
	     	 <fieldset style="border:1px green solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
			 	<legend style="font-size:15px;font-weight:bold; color: green"> 	
			 			<% DBRow lpRow=containerMgrZYZ.getDetailContainer(rows[i].get("con_id",0l));%>	
			 			
			 			<span style="font-size:13px; font-weight: bold;">所在托盘编码:</span>
	        			<span style="font-size:13px; font-weight: bold;"><%=lpRow.getString("container")%></span>  
	        			<span style="font-size:13px; font-weight: bold;margin-left: 3px;">[<%=containerTypeKey.getContainerTypeKeyValue(container_type)%>]</span> 		 	
				 		 
			 	</legend>
			 	<div style="padding-left: 100px">
			       	  <span style="font-size:13px; font-weight: bold;">可用数量:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=rows[i].get("available",0) %></span>
			          <span style="font-size:13px; font-weight: bold; margin-left: 20px">物理数量:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=rows[i].get("physical",0) %></span>
			          
			             <%
			             	boolean hasInnerContainer = productStoreMgrZJ.hasInnerContainer(ps_id,rows[i].get("con_id",0l));
			 				if(hasInnerContainer)
			 				{
			 					String lpTypeName = containerTypeKey.getContainerTypeKeyValue(rows[i].get("container_type",0));
	 					%>
			 			<a href="javascript:void(0)" onclick="openlp('<%=rows[i].get("con_id",0l) %>','<%=lpRow.getString("container")%>','<%=lpTypeName %>',<%=rows[i].get("physical_count",0d) %>)" style="text-decoration:none; color:red;margin-left: 20px;" >
			 				<span style="font-size:13px; font-weight: bold;">查看内部容器</span>
			 			</a>
			 			<%
			 				}
			 			%>
			    </div>
			 </fieldset>
			 <%} %>
   </fieldset>
</body>
</html>
<script type="text/javascript">
function openlp(sub_con_id, sub_con_name,lpTypeName,physical){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + 
	"administrator/product/product_storage_sub_container_product_count.html?ps_id=<%=ps_id%>&pc_id=<%=productId%>&title_id=<%=titleId%>&p_name=<%=productName%>"
	+"&title_name=<%=titleName%>&lot_name=<%=lotNumberId%>&location_name=<%=locationName%>"
	+"&sub_con_id="+sub_con_id+"&sub_con_name="+sub_con_name+"&physical="+physical;
	$.artDialog.open(url , {title: lpTypeName+'&nbsp;[&nbsp;'+sub_con_name+'&nbsp;]',width:'800px',height:'650px', lock: true,opacity: 0.3,fixed: true});
}
</script>
