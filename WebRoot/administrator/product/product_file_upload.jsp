<%@page import="com.cwc.app.key.ProductFileTypeKey"%>
<%@page import="com.cwc.app.key.YesOrNotKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@ include file="../../include.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Upload Product Photos</title>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/art/skins/simple.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- 在线图片预览 -->
	 <script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	
	<link type="text/css" href="../js/picture_online_show/css/prettyPhoto.css" rel="stylesheet" />
	<style type="text/css" media="all">
		@import "../js/thickbox/thickbox.css";
	</style>
	<script type="text/javascript"	src="../js/picture_online_show/jquery.opacityrollover.js"></script>
	<script type="text/javascript"	src="../js/picture_online_show/jquery.prettyPhoto.js"></script>
	<script type="text/javascript"	src="../js/picture_online_show/jquery.smoothZoom.min.js"></script>

	<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="../buttons.css"  />
	
	<style type="text/css">
	.aui_titleBar{  border: 1px #DDDDDD solid;}
	.aui_header{height: 40px;line-height: 40px;}
	.aui_title{
	  height: 40px;
	  width: 100%;
	  font-size: 1.2em !important;
	  margin-left: 20px;
	  font-weight: 700;
	  text-indent: 0;
	}

	td.categorymenu_td div{margin-top:-3px;}
	
	.ui-widget-content{
		border:1px #CFCFCF solid;
		border-radius: 0px;
	  	-webkit-border-radius: 0px;
		-moz-border-radius:0;
	}
	.ui-widget-header{
		background:#f1f1f1 !important;
		border-radius: 0;
		border-top:0;
		border-left:0;
		border-right:0;
		border-bottom: 1px #CFCFCF solid;
	}
	.ui-tabs .ui-tabs-nav li a{
		padding: .5em 1em 13px !important;
	}
	.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
		  height: 40px;
		  margin-top: -4px !important;
		background:#fff;
	}
	.ui-tabs .ui-tabs-nav{
		padding :0 .2em 0;
	}
	.ui-tabs{
		padding:0;
	}
	
	.ui-jqgrid{border:0;}
	.ui-jqgrid-titlebar.ui-widget-header{
		background:#fff !important;
		height:35px;
		line-height:30px;
	}
	
	 
</style>

<style type="text/css">

   		 

.image_div img{width:230px;border:#888 1px solid;padding:2px;}


.desc_table{
padding-bottom:10px;}
div.content {
	margin: 0px auto;
	padding:0px 10px 10px 0px;
}

div.content {
	background-image: url("../js/picture_online_show/images/bg.jpg");
	background-repeat: repeat;
	height: 100%;
}

div.content ul.ul_float {
	width: 245px;
	float: left;
	margin: 0px;
	padding: 0px;
	margin-left: 10px;
}

div.content ul.ul_float li {
	list-style-type: none;
	display: block;
	padding: 5px;
	background: white;
	box-shadow: 0 1px 3px #BBBBBB;
	margin-top: 10px;
}

div.content ul.ul_float li.active {
	box-shadow: 0 1px 3px #f60;
}

div.image_div {
	width: 235px;
	margin: 0px auto;
	height:240px;
	text-align: center;
}

div.image_div img {
	width: 230px;
}

div.image_div:hover {
	cursor: pointer;
}

a.abs {
	color: #222222;
	overflow: hidden;
	padding: 0 0 6px;
	vertical-align: baseline;
	padding: 0 0 6px;
	display: block;
	font-size: 12px;
	line-height: 16px;
	text-decoration: none;
}

div.info {
	padding: 12px 6px 5px;
	height:50px;
}

.smooth_zoom_preloader {
	background-image: url("../js/picture_online_show/images/preloader.gif");
}

.smooth_zoom_icons {
	background-image: url("../js/picture_online_show/images/icons.png");
}

span.size_info {
	color: #f60;
}

.pp_description {
	line-height: 25px;
}
div.delete_btn{
float:right;}
.delete_btn a{
text-decoration:none;}

.table-pro{margin:10px;}
.table-pro .title{font-weight:700;} 
</style>
  	
  	<%
  	  		String updateTransportAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportProductFileCompleteAction.action";
  	  			String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
  	  	  		String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
  	  	  		String pc_id = StringUtil.getString(request,"pc_id");
  	  	  		String first_pc_name = StringUtil.getString(request, "theFirstName");
  	  	  		int can_operate = StringUtil.getInt(request, "can_operate");
  	  	  		DBRow[] productRows = transportMgrZr.getProductByPcIds(pc_id);
  	  	  		//读取配置文件中的transport_product_file
  	  	  		String value = systemConfig.getStringConfigValue("transport_product_file");
  	  	  		String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
  	  	  		String file_with_class = StringUtil.getString(request,"file_with_class");
  	  	  		int is_unable_to_provide = StringUtil.getInt(request,"is_unable_to_provide");//是否是无法提供照片
  	  	  		String[] arraySelected = value.split("\n");
  	  	  		//将arraySelected组成一个List
  	  	  		/*
  	  	  		ArrayList<String> selectedList= new ArrayList<String>();
  	  	  		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
  	  	  				String tempStr = arraySelected[index];
  	  	  				if(tempStr.indexOf("=") != -1){
  	  	  					String[] tempArray = tempStr.split("=");
  	  	  					String tempHtml = tempArray[1];
  	  	  					selectedList.add(tempHtml);
  	  	  				}
  	  	  		}*/
  	  	  		ProductFileTypeKey productFileTypeKey = new ProductFileTypeKey();
  	  	  		ArrayList<String> selectedKeyList= productFileTypeKey.getProductFileTypesKeys();
  	  	  		ArrayList<String> selectedValueList= productFileTypeKey.getProductFileTypeValue();
  	  	  		// 获取所有的关联图片 然后在页面上分类
  	  	  		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
  	  	  		DBRow[] imagesRows = productMgrZyj.getAllProductFileByPcId(pc_id,FileWithTypeKey.PRODUCT_SELF_FILE,pc_id,is_unable_to_provide);
  	  	  		 
  	  			if(imagesRows != null && imagesRows.length > 0 ){
  	  		for(int index = 0 , count = imagesRows.length ; index < count ; index++ ){
  	  			DBRow tempRow = imagesRows[index];
  	  			String product_file_type = tempRow.getString("product_file_type");
  	  			List<DBRow> tempListRow = imageMap.get(product_file_type);
  	  			if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1)){
  	  				tempListRow = new ArrayList<DBRow>();
  	  			}
  	  			tempListRow.add(tempRow);
  	  			imageMap.put(product_file_type,tempListRow);
  	  			
  	  		}
  	  			}
  	  	  		String backurl =  ConfigBean.getStringValue("systenFolder") +"administrator/product/product_file_upload.html?pc_id="+pc_id+"&theFirstName="+first_pc_name ;
  	  	  		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
  	  	  		DBRow adminRow = adminMgrGZY.getDetailAdmin(adminLoggerBean.getAdid());
  	  	%>
  	<%
  	/************************** picture online show starts *****************************/
  	
  	//根据传入的参数去查询对应的文件
	//只显示一张图片
	//显示多个图片()
	//如果是有多个图片的时候那么点击的那个。就应该让他选中
	String cmd = "multiFile";
	String table = "product_file";
	String uploadPath = systemConfig.getStringConfigValue("file_path_product");
	String basepath = ConfigBean.getStringValue("systenFolder") + "upload/"+uploadPath;
	String currentName = StringUtil.getString(request,"current_name");
	long currentFileId = 0l ;
 	DBRow[] rows = null;
 	//apply_image表中查询出的图片的所有信息
 	List rowApply=null;
 	//判断当前点击的是哪个图片
 	String currentFileIdApply="";
	
	if(cmd.equals("show_only")){
		rows = new DBRow[1];
		String imgPath = StringUtil.getString(request,"image_path");
		String imgName = StringUtil.getString(request,"image_name");
		DBRow temp = new DBRow();
		temp.add("image_path",imgPath);
		temp.add("img_name",imgName);
		rows[0] = (temp);		
	}
	if(table.equals("file")){
		long fileWithId = StringUtil.getLong(request,"file_with_id");
		int fileWithType = StringUtil.getInt(request,"file_with_type");
		int fileWithClass = StringUtil.getInt(request,"file_with_class");
		//过滤掉不是图片的文件
	if(cmd.equals("multiFile")){
	
	rows = 	fileMgrZr.getFilesByFileWithIdAndFileWithType(fileWithId,fileWithType);
		}else if(cmd.equals("sortMultiFile"))
		{
	rows = fileMgrZr.getFilesByFileWithIdAndFileWithTypeAndFileWithClass(fileWithId,fileWithType,fileWithClass);
		}
		List<DBRow> rowList = new ArrayList<DBRow>();
		if(rows != null  && rows.length > 0 ){
	for(DBRow fileRow :  rows){
		
		if(StringUtil.isPictureFile(fileRow.getString("file_name"))){
			if(currentName.equals(fileRow.getString("file_name"))){
		 		 currentFileId = fileRow.get("file_id",0l);
		 	 }
			rowList.add(fileRow);
		}	 
		
	}
		}
		rows = rowList.toArray(new DBRow[rowList.size()]);
	}
	//添加临时图片的显示
 	//table == temp 
	//basepath == ''
	
	if(table.equals("temp")){
	
		String fileNames = StringUtil.getString(request,"fileNames");
 		if(!StringUtil.isNull(fileNames)){
 			String[] namesArray = fileNames.split(",");
 		    rows = new DBRow[namesArray.length];
 		    for(int index = 0 , count = namesArray.length ; index < count ; index++ ){
 		    	DBRow row = new DBRow();
 		    	row.add("file_name",namesArray[index]);
 		    	rows[index] = row;
 		    }
 		}
	}
	if(table.equals("product_file")){
	
		long fileWithId = StringUtil.getLong(request,"file_with_id");
		int fileWithType = StringUtil.getInt(request,"file_with_type");
		int product_file_type=StringUtil.getInt(request,"product_file_type");
 		String pcId = StringUtil.getString(request,"pc_Id");
		//PageCtrl pc = new PageCtrl();
		//pc.setPageNo(1);
		//pc.setPageSize(100);
		//过滤掉不是图片的文件
		rows = 	fileMgrZr.getProductFileByFileTypeAndWithIdAndPcId(fileWithId,fileWithType,product_file_type,pcId);
		List<DBRow> rowList = new ArrayList<DBRow>();
		if(rows != null  && rows.length > 0 ){
	for(DBRow fileRow :  rows){
		if(StringUtil.isPictureFile(fileRow.getString("file_name"))){
			if(currentName.equals(fileRow.getString("file_name"))){
		 		 currentFileId = fileRow.get("pf_id",0l);
		 	 }
			rowList.add(fileRow);
		}	 
	}
		}
		rows = rowList.toArray(new DBRow[rowList.size()]);
	}	
	//apply_images表中的图片在线显示
	if(table.equals("apply_images")){
	
		String associationId = StringUtil.getString(request,"association_id");
		String associationType=StringUtil.getString(request,"association_type");
		rowApply = applyMoneyMgrLL.getImageList(associationId,associationType);
		List  row=new ArrayList();
		if(rowApply!=null &&rowApply.size()>0){
	for(int i=0;i<rowApply.size();i++){
		HashMap imageMap2 = (HashMap)rowApply.get(i);
		if(StringUtil.isPictureFile(imageMap2.get("path").toString())){
			if(currentName.equals(imageMap2.get("path").toString())){
				currentFileIdApply = imageMap2.get("id").toString();
		 	 }
		row.add(rowApply.get(i));}
	}
	rowApply=row;	
		}		
	}	
	
  	/************************** picture online show ends *****************************/
  	 %>
  	<script type="text/javascript">
  		/************************** picture online show starts *****************************/
  		
  		
  		function imageLoad(_this) {
			var node = $(_this);
			var image = new Image();
			image.src = node.attr("src");
			image.onload = function() {
				var imageHeight = image.height;
				var imageWidth = image.width;
	
				var changeHeight = imageHeight * (230 / imageWidth);
				node.attr("height", changeHeight + "px");
				
				
				
				
				
				var liNode = (node.parent().parent().parent());
				var spanSize = $(".size_info", liNode).html(
						imageWidth + "x" + imageHeight);
				
				
				
			};
		}
		/************************** picture online show ends *****************************/
  		function submitForm(){
  	  		var file = $("#file");
  	  		if(file.val().length < 1){
  	  	  		showMessage("Please Input Photos","alert");
				return ;
  	  	  	}
			var myform = $("#myform");
			myform.submit();
  	  	}
  	  	function fileWithClassTypeChange(){
		 	var file_with_class = $("#file_with_class").val();
		 	//$("#file_with_class").val("");
			var index1 = $("#tabs-pic a[test_class="+file_with_class+"]").attr("test_index");
		
		   	$("#tabs-pic").tabs("select" , Number(index1));
		   	 
		   	
			//$("#tabs").tabs( "select" , $(ui.tab).attr("test"));
    	 }
   	 jQuery(function($){
    
	   	  $("#tabs-pic").tabs({
	   			cache: true,
	   			cookie: { expires: 30000 } ,
	  		 	select: function(event, ui){
	  		 		
	  		 		var file_class = $(ui.tab).attr("test_class");
	  		 		var file_html = $(ui.tab).html();
				 	
				 	//$("#file_with_class").val(file_class);
				 	$("#photoType").html($(ui.tab).html());
				 	$("#file_with_class").val(file_class); 
				 	
// 				 	console.log("class:"+$("#file_with_class").val());
				 	
// 				 	$("#file_with_class option[value='"+file_class+"']").attr("selected",true);
				}
	   	 });
<%-- 	   	if('<%= file_with_class%>'.length > 0){ --%>
<%-- 			 $("#tabs").tabs( "select" , '<%= file_with_class%>' * 1-1 ); --%>
<%-- 			 $("#file_with_class option[value='"+'<%= file_with_class%>'+"']").attr("selected",true); --%>
// 		}
// 	   	 fileWithClassTypeChange();
   	  })
  	function deleteFile(file_id){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'product_file',file_id:file_id,folder:'product',pk:'pf_id'},
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}else{
					showMessage("System Error,Please Try Again Later","error");
				}
			},
			error:function(){
				showMessage("System Error,Please Try Again Later","error");
			}
		})
	}
   	function downLoad(fileName , tableName , folder){
   		 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ '<%= systemConfig.getStringConfigValue("file_path_product")%>');
   }
  	//文件上传
    //做成文件上传后,然后页面刷新提交数据如果是有添加文件
      function uploadFile(_target){
          var targetNode = $("#"+_target);
          var fileNames = $("input[name='file_names']").val();
          var obj  = {
      	     reg:"picture_office",
      	     limitSize:2,
      	     target:_target
      	 }
          var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
      	uri += jQuery.param(obj);
      	 if(fileNames && fileNames.length > 0 ){
      		uri += "&file_names=" + fileNames;
      	}
      	 $.artDialog.open(uri , {id:'file_up',title: 'Upload File',width:'920px',height:'530px', lock: true,opacity: 0.3,fixed: true,
          		 close:function(){
  					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
  					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
         		 }});
      }
    //jquery file up 回调函数
      function uploadFileCallBack(fileNames,target){
	  $("input[name='file_names']").val(fileNames);
          if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
              $("input[name='file_names']").val(fileNames);
              var myform = $("#myform");
              var file_with_class = $("#file_with_class").val();
  			$("#backurl").val("<%= backurl%>" + "&file_with_class="+file_with_class);
              myform.submit();
      	}
      }
      function onlineScanner(){
    	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
    	 $.artDialog.open(uri , {title: 'Capture Online',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
      }
      function unableToProvidePhotos(uril,product_name)
  	  {
  	  	var product_names = "";
  	  	if(product_name.length > 0)
  	  	{
			var product_name_arr = product_name.split(",");
			if(product_name_arr.length > 0)
			{
				for(var i = 0; i < product_name_arr.length; i ++)
				{
					product_names += product_name_arr[i];
					if(1 == i % 2)
					{
						product_names += "\n";
					}
					else
					{
						product_names += ",";
					}
				}
			}
  	  	}
  	  	if(product_names.indexOf(","))
  		{
  	  		product_names = product_names.substring(0, product_names.length-1);
  		}
  	  	var html = "<table><tr><td>Can\'t provide the Photos of following goods?</td></tr><tr><td>"+product_names+"</td></tr></table>"
	  	  $.artDialog({
			    content: $(html).html(),
			    icon: 'warning',
			    width: 450,
			    height: 70,
			    title:'',
			    opacity: 0.1,
			    lock: true,
			    fixed:true,
			    okVal: 'Confirm',
			    ok: function () {
			    	console.log(12);
			    	$("input[name='file_names']").val('unable_to_provide_photo.jpg');
		            var file_with_class = $("#file_with_class").val();
					$("#backurl").val("<%= backurl%>" + "&file_with_class="+file_with_class);
					$("#is_unable_to_provide").val(1);
					$("#myform").submit();
			    },
			    cancelVal: 'Cancel',
			    cancel:function(){
			    	
			    }
			});	  
  	  }
      
  	</script>
  	<style type="text/css">
  		.zebraTable td {line-height:25px;height:25px;}
		.right-title{line-height:20px;height:20px;}
		ul.ul_p_name{list-style-type:none;margin-left:-44px;}
		ul.ul_p_name li{margin-top:1px;margin-left:3px;line-height:25px;height:25px;padding-left:5px;padding-right:5px;float:left;border:1px solid silver;}
  		ul.fileUL{list-style-type:none;clear:both;}
		ul.fileUL li{line-height:20px;height:20px;padding-left:3px;padding-right:3px;margin-left:2px;float:left;margin-top:1px;border:1px solid silver;}
  	</style>
  </head>
  
  <body >
   	<!-- 支持多个图片同时上传 ,下面显示图片(用轮播器显示)-->
   	<form id="myform"   method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/product/ProductPictureUploadAction.action" %>'>
   		<input type="hidden" name="pc_id" value="<%=pc_id %>" />
   		 <input type="hidden" name="backurl" id="backurl" value=""/>
   		 <input type="hidden" name="first_pc_name" value='<%=first_pc_name %>'/>
   		 <input type="hidden" name="is_unable_to_provide" id="is_unable_to_provide"/>
   		 <%String product_names = ""; %>
   		 <%if(can_operate != YesOrNotKey.NO ){%>
   		<table class="table-pro">
   		<tr>
   			<td class="title" style="width:75px;text-align:right;">
   				Product&nbsp;:&nbsp;
   			</td>
   			<td colspan="2" style="text-align:left;">
   				 <%
   					
   				 	if(productRows != null && productRows.length > 0)
   				 	{
   				 	
   				 			for(DBRow tempRow : productRows)
   				 			{
	   				 		String product_name = tempRow.getString("p_name");
	   				 		product_names += product_name + ",";
   				 		 %>
   				 		  <%=product_name%>
   				 		 <% 
   				 			}
  						 %>
  						
  						 <% 
  						 if(product_names.length() > 0)
  						 {
  							product_names = product_names.substring(0,product_names.length()-1);
  						 }
   				 	}
   				 %>
   			</td>
   		</tr>
   		<tr><td colspan="3">&nbsp;</td></tr>
   			<tr>
   				<td class="title" style="text-align:right;">Photo Type&nbsp;:&nbsp;</td>
   				<td style="width:200px;"> 
   					<label id="photoType">SKU</label>
   					<input type="hidden" id="file_with_class" name="file_with_class" value="<%= selectedKeyList.get(0) %>"></input> 
<%--    					<select id="file_with_class" name="file_with_class" onchange="fileWithClassTypeChange();" style="width: 80px;height: 28px;">--%>
<%-- 						<%		for(int index = 0 , count = selectedKeyList.size() ; index < count ; index++ ){ --%>
<%-- 								String tempStr = selectedKeyList.get(index);--%>
<%-- 								String tempValue = productFileTypeKey.getProductFileTypesKeyValue(tempStr);--%>
<%-- 							%>		 --%>
<%-- 								<option value="<%=tempStr %>" <%=(0==index)?"selected='selected'":""%>><%=tempValue %></option> --%>
<%--
<%-- 							<%	}  --%>
<%-- 							%> --%> 
<%--   					</select> --%>
   				</td>
   				<td style="margin-left:30px;">
		 			<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PRODUCT_SELF_FILE %>" />
		 			<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_product")%>" />
   					<input type="hidden" name="file_names" />
   					<a class="buttons primary" onclick="uploadFile('jquery_file_up');" value="Upload Photos" ><i class="icon-upload "></i>&nbsp;Upload Photos</a><hidden></hidden>
   				    <a class="buttons" onclick="onlineScanner();" value="Capture Online" ><i class="icon-camera"></i>&nbsp;Capture Online</a>
   				</td>
   				 
   			</tr>
   		</table>
   		</form>	
   		<%} %>
   		 <!-- 显示出所有的商品图片 tabs-->
   		 <div id="tabs-pic" style="margin-top:15px;">
		   		 	<ul>
		   		 		<%
					 		if(selectedKeyList != null && selectedKeyList.size() > 0){
					 			for(int index = 0; index < selectedKeyList.size() ; index++ ){ 
					 			%>
					 				<li><a href="#transport_product_<%=selectedKeyList.get(index) %>" test_index="<%=index%>" test_class="<%=selectedKeyList.get(index) %>"> <%=productFileTypeKey.getProductFileTypesKeyValue(selectedKeyList.get(index)) %></a></li>
					 			<% 	
					 			} 
					 		}
				 		%>
		   		 	</ul>
   		 		<%
   		 		for(int index = 0 , count = selectedKeyList.size() ; index < count ; index++ ){
   		 				List<DBRow> tempListRows = imageMap.get(selectedKeyList.get(index));
   		 		%>
   		 			 <div id="transport_product_<%=selectedKeyList.get(index)%>">
   		 			 	<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="desc_table">
	   		 			 	<tr>
<%-- 			   		 			 <% --%>
<!-- // 			   		 			 	String product_photo_description = systemConfig.getStringConfigValue("product_photo_description"); -->
<!-- // 			   		 			 	if(!"".equals(product_photo_description)) -->
<!-- // 			   		 			 	{ -->
<!-- // 			   		 			 		String[] product_photo_description_arr = product_photo_description.split("\n"); -->
<!-- // 			   		 			 		if(index < product_photo_description_arr.length) -->
<!-- // 			   		 			 		{ -->
<%-- 			   		 			 %> --%>
<%-- 			   	   		 			 	<td colspan="2">Note:<%=product_photo_description_arr[index] %></td> --%>
<%-- 			   	   		 		 <%				 --%>
<!-- // 			   		 			 		} -->
<!-- // 			   		 			 	} -->
<%-- 			   		 			 %> --%>
								<td colspan="2">Note:<%=productFileTypeKey.getProductFileDescription(selectedKeyList.get(index))[0] %></td>
			   		 			 <td align="right">
			   		 			 <%if(can_operate != YesOrNotKey.NO ){%>
			   		 			 	<a class='buttons' value='Unable To Provide Now' onclick='unableToProvidePhotos("<%=adminRow.getString("file_path") %>", "<%=product_names %>")' >Unable To Provide Now</a>
			   		 			<%} %>
			   		 			 </td>
	   		 			 	</tr>
  		 			 		
   		 				</table>
   		 				<table cellpadding="0" cellspacing="0" border="0" width="100%">
   		 				<tr >
   		 			 					
   		 			 		
   		 			 	<%
   		 			 		if(tempListRows != null && tempListRows.size() > 0){
   		 			 		%>
   		 			 		<td>
   		 			 		<div class="content">
   		 			 		<%
   		 			 			 for(DBRow row : tempListRows){
   		 			 			%>
   		 			 				
   		 			 				 		
													
													<ul class="ul_float">
													<% 
														DBRow rowTemp = row;
								  				 	 	String title = rowTemp.getString("file_name");
									  				 	 String picturePath = basepath+"/"+ rowTemp.getString("file_name");
									  				 	 String pName = rowTemp.getString("p_name");
										 			 	 String productFileType = rowTemp.getString("product_file_type");
										 			 	 String certificateFileType = rowTemp.getString("file_with_class");
										 			 	 boolean isActiveLi ;
										 			 	 //判断是file表中的图片还是product_file表中的图片
										 			 	 if(rowTemp.get("file_id",0l)!=0){
										 			 		isActiveLi = (currentFileId == rowTemp.get("file_id",0l) );
										 			 	 }else{
										 			 		 isActiveLi = (currentFileId == rowTemp.get("pf_id",0l) );
										 			 	 }
										 			 	 //让
									 	 			 	 if(currentName.equalsIgnoreCase(title)){
										 			 		isActiveLi = true ;
										 			 	 }else{
										 			 		isActiveLi = false ;
										 			 	 }
									   	 			 	String tempHtml="";
										 			 	Map<Integer,String> map = null ; 
										 				if(rowTemp.get("file_with_type",0l)== FileWithTypeKey.product_file || rowTemp.get("file_with_type",0l)== FileWithTypeKey.PURCHASE_PRODUCT_FILE || rowTemp.get("file_with_type",0l)== FileWithTypeKey.PRODUCT_SELF_FILE){
										 					
										 					map = StringUtil.getConfigMap(systemConfig.getStringConfigValue("transport_product_file"));
										 					tempHtml = map.get(Integer.parseInt(productFileType));
									 	 					
										 				}else if(rowTemp.get("file_with_type",0l)== FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE || rowTemp.get("file_with_type",0l)== FileWithTypeKey.PRODUCT_TAG_FILE){
										 					map = StringUtil.getConfigMap(systemConfig.getStringConfigValue("transport_tag_types"));
										 					tempHtml = map.get(Integer.parseInt(productFileType));
										 					
										 				}else if(rowTemp.get("file_with_type",0l)== FileWithTypeKey.transport_certificate){
										 					map = StringUtil.getConfigMap(systemConfig.getStringConfigValue("transport_certificate"));
										 					tempHtml = map.get(Integer.parseInt(certificateFileType));
										 				}
														%>
														<li class='<%=isActiveLi?"active":""%>'>
															<%
																if(pName.equals("")&&pName.length()<=0){
																    	if(!tempHtml.equals("") && tempHtml.length()>0){
																			%>
																			<div class="image_div">
																				<a class="image_a" href='<%=picturePath%>'
																					rel="prettyPhoto[gallery2]"
																					title='FileType：<%=tempHtml%> , FileName：<%=title%>'>
																					<img
																					src='<%=picturePath%>' width="230px" />
																				</a>
																			</div>
																			<div class="info">
																				<a class="abs" href="javascript:void(0)">FileType：<%=tempHtml%> <br />
																					<!-- FileName：<%=title%>  -->
																					</a> <span class="size_info"></span>
																			</div> 
																			<%}else{%>
																				<div class="image_div">
																					<a class="image_a" href='<%=picturePath%>'
																						rel="prettyPhoto[gallery2]" title='<%=title%>'> <img
																						src='<%=picturePath%>' width="230px" />
																					</a>
																				</div>
																				<div class="info">
																					<a class="abs" href="javascript:void(0)"><%=title%></a> <span
																						class="size_info"></span>
																				</div> <%
																		 	}
													 			 }else{
													 %>
																	<div class="image_div">
																		<a class="image_a" href='<%=picturePath%>'
																			rel="prettyPhoto[gallery2]"
																			title='ProductName：<%=pName%> , FileType：<%=tempHtml%> , FileName：<%=title%>'>
																			<img src='<%=picturePath%>'
																			width="230px" /> </a>
																	</div>
																	<div class="info">
																		
																			<a class="abs" href="javascript:void(0)">
																				<!-- FileName：<%=title%> -->
																				</a> 
																		<div style="float:left;"><span class="size_info"></span></div>
																		<div class="delete_btn">
																			<%if(can_operate != YesOrNotKey.NO ){%>
									   		 			 					
									   		 			 						<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">
									   		 			 							<img src="../js/easyui/themes_visionari/icons/cancel.png" valign="bottom"/>
									   		 			 							Delete</a>  
									   		 			 				 	<%} %>
									   		 			 				 	
																		</div>
																		<div style="clear:both"></div>
																	</div> 
																	
												<%
													 	}
													 	
													 %>
														</li>
														
													</ul>
											
													
													
   		 			 			<% 
   		 			 		 	}
   		 			 		 	%>
   		 			 		 	<div style="clear:both;"></div>
												</div>
												</td>
								<%				
		   		 					}else{
		   		 			 			%>
		 		 			 			
		 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">No Photos</td>
		 								
		   		 			 			<%
		   		 			 		}
   		 			 	%>
   		 			 			
   		 			 		</tr>
   		 			 	</table>
   		 			 		
							<script type="text/javascript">
								jQuery(function($) {
									var onMouseOutOpacity = 0.67;
									$('.image_div').opacityrollover({
										mouseOutOpacity : 1.0,
										mouseOverOpacity : onMouseOutOpacity,
										fadeSpeed : 'fast'
									});
						
									var canvasWidth = 806;
									var canvasHeight = 447;
									if ($(window).width() * 1 < 1000) {
										canvasWidth = 700;//600
										canvasHeight = 367;//347
									}
						
									$("a.image_a")
											.prettyPhoto(
													{
														default_width : canvasWidth,
														default_height : canvasHeight,
														slideshow : false, /* false OR interval time in ms */
														autoplay_slideshow : false, /* true/false */
														opacity : 0.50, /* opacity of background black */
														theme : 'facebook', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
														modal : false, /* If set to true, only the close button will close the window */
														overlay_gallery : false,
														changepicturecallback : setZoom,
														callback : closeZoom,
														social_tools : false,
														image_markup : '<div name="122" style="width:'+canvasWidth+'px; height:'+canvasHeight+'px;" ><div id="fullResImage"  ><img id="rotateImg"  src="{path}" /></div></div>',
														fixed_size : true,
														keyboard_shortcuts: false,
						
														/******************************************
														Enable Responsive settings below if needed.                                                                 
														Max width and height values are optional.
														 ******************************************/
														responsive : false,
														responsive_maintain_ratio : true,
														max_WIDTH : '',
														max_HEIGHT : ''
													});
									//让点取的选中
						
									if ($("li.active").length > 0) {
										$("html,body").animate({
											scrollTop : $("li.active").offset().top
										}, 1000);
									}
								})
								function setZoom() {
									var imgObj = new Image();
								    imgObj.src = $("#rotateImg").attr('src');
								    var sizeImg = imgObj.width>imgObj.height?imgObj.width:imgObj.height;
								    $('#fullResImage').css({
								    "width":sizeImg+"px",
								    "height":sizeImg+"px",
								    });
								    if(imgObj.width<imgObj.height){				//高图需要进行横向居中。正好相等则什么也不做 
								    	 $("#rotateImg").css({
											"position" : "absolute",
											"left" : "25%"
										});
								    }else if(imgObj.width>imgObj.height){		//宽图需要进行纵向居中
								    	var topsize = Math.floor((1-imgObj.height/imgObj.width)/2*100);//计算应距离上边距百分之多少，并给过宽的图片加行top属性
									   $("#rotateImg").css({
											"position" : "absolute",
											"top" :""+topsize+"%",
										});
									}
									$('#fullResImage').smoothZoom('destroy').smoothZoom();
						
								}
						
								function closeZoom() {
									indexZ = 0;
									$('#fullResImage').smoothZoom('destroy');
								}
								//图片旋转
						
								var indexZ = 0;
								function turnLeft() {
									if (indexZ == 360 | indexZ == -360)
										indexZ = 0;
									$("#rotateImg").css({
										"-moz-transform" : "rotate(" + (indexZ -= 90) + "deg)",
										"-webkit-transform" : "rotate(" + (indexZ) + "deg)"
									});
						
								} 
								function turnRight() {
									if (indexZ == 360 | indexZ == -360)
										indexZ = 0;
									$("#rotateImg").css({
										"-moz-transform" : "rotate(" + (indexZ += 90) + "deg)"
									});
									$("#rotateImg").css({
										"-webkit-transform" : "rotate(" + (indexZ) + "deg)"
									});
								}
							</script>
   		 			 	
   		 			
   		 			 </div>
   		 			<% 
   		 		}
   		 	%>
   		 	
   		 </div>

   	
  </body>
  <script type="text/javascript">
//显示在线图片
    //图片在线显示  		 
 function showPictrueOnline(fileWithType,fileWithId ,currentName,productFileType,pcId,uploadPath){
   var obj = {
   		file_with_type:fileWithType,
   		file_with_id : fileWithId,
   		current_name : currentName ,
   		product_file_type:productFileType,
   		pc_Id : pcId,
   		cmd:"multiFile",
   		table:'product_file',
   		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+uploadPath
	}
   if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
}		
  </script>
  <script type="text/javascript">
//stateBox 信息提示框
  function showMessage(_content,_state){
  	var o =  {
  		state:_state || "succeed" ,
  		content:_content,
  		corner: true
  	 };
   
  	 var  _self = $("body"),
  	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
  	_self.append(_stateBox);	
  	 
  	if(o.corner){
  		_stateBox.addClass("ui-corner-all");
  	}
  	if(o.state === "succeed"){
  		_stateBox.addClass("ui-stateBox-succeed");
  		setTimeout(removeBox,1500);
  	}else if(o.state === "alert"){
  		_stateBox.addClass("ui-stateBox-alert");
  		setTimeout(removeBox,2000);
  	}else if(o.state === "error"){
  		_stateBox.addClass("ui-stateBox-error");
  		setTimeout(removeBox,2800);
  	}
  	_stateBox.fadeIn("fast");
  	function removeBox(){
  		_stateBox.fadeOut("fast").remove();
   }
  }


  function scaleImage(o, w, h){
		var img = new Image();
		img.src = o.src;
	//	console.log(w);
		if(img.width >0 && img.height >0)
		{
			if(img.width/img.height >= w/h)
			{
				if(img.width > w)
				{
					$(o).width(w);
					$(o).height(img.height*w/img.width);
					/*
					var width = w+"px";
					var height = parseInt(img.height*w/img.width)+"px";
					$(o).css({width:width, height:height});
					*/
				}
				else
				{
					$(o).width(img.width);
					$(o).height(img.height);
					/*
					var width = img.width+"px";
					var height = img.height+"px";
					$(o).css({width:width, height:height});
					*/
				}
			}
			else
			{
				if(img.height > h)
				{
					$(o).height(h);
					$(o).width(parseInt(img.width / img.height * h));
					/*
					var width = parseInt(img.width / img.height * h)+"px";
					var height = h+"px";
					$(o).css({width:width, height:height});
					*/
				}
				else
				{
					$(o).width(img.width);
					$(o).height(img.height);
					/*
					var width = img.width+"px";
					var height = img.height+"px";
					$(o).css({width:width, height:height});
					*/
				}
			}
		}
	}
  function changeImg(o, w, h)
  {
	  var img = new Image();
		img.src = o.src;
		var width = 0;
		var height = 0;
	//	console.log(w);
		if(img.width >0 && img.height >0)
		{
			if(img.width/img.height >= w/h)
			{
				if(img.width > w)
				{
					
					width = w;
					height = parseInt(img.height*w/img.width);
				}
				else
				{
					width = img.width;
					height = img.height;
				}
			}
			else
			{
				if(img.height > h)
				{
					width = parseInt(img.width / img.height * h);
					height = h;
				}
				else
				{
					width = img.width;
					height = img.height;
				}
			}
		}
		return {w:width,h:height};
  }
  
  function changeImg2(o, w, h)
  {
	  var img = new Image();
		img.src = o.src;
		var width = 0;
		var height = 0;
	//	console.log(w);
	  //alert(img.width);
		if(img.width >0 && img.height >0)
		{
			if(img.width/img.height >= w/h)
			{
				if(img.width > w)
				{
					
					width = w;
					height = parseInt(img.height*w/img.width);
				}
				else
				{
					width = img.width;
					height = img.height;
				}
			}
			else
			{
				if(img.height > h)
				{
					width = parseInt(img.width / img.height * h);
					height = h;
				}
				else
				{
					width = img.width;
					height = img.height;
				}
			}
		}
		//return {w:width,h:height};
		$(o).css({width:width + "px",height:height + "px"});
  }
  
  function reimageauto(){
	  
	 var img = $(".image_div img");
	  for(var i =0;i < img.length;i++){
		 
		  var itmeimg=img.eq(i);
		  var cssImg = changeImg(img[i],230,230);
		  if(window.parseInt(cssImg.w) > 0 && window.parseInt(cssImg.h) >0){
			  itmeimg.css({width:cssImg.w+"px",height:cssImg.h+"px"});	
		  }
		  
		  
	  }
	  
  }
  
  $(function(){
	  
	  var $tabs = $('#tabs-pic').tabs();
	
	  var active_atext = $tabs.find(".ui-state-active a").text();
	  var active_class = $tabs.find(".ui-state-active a").attr("test_class");
 	  $("#file_with_class").val(active_class);
 	  $("#photoType").html(active_atext);
 	 
 	 $(".image_div img").bind("load",function(){
 		 changeImg2(this,230,230);
 		 $(this).show();
 	 })
  });

  
  </script>
</html>
