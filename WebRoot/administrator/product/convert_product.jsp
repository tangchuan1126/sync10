<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long s_pid = StringUtil.getLong(request,"s_pid");
long s_pc_id = StringUtil.getLong(request,"s_pc_id");
String storage_name = StringUtil.getString(request,"storage_name");
DBRow detailPro = productMgr.getDetailProductByPcid(s_pc_id);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script>

$().ready(function() {

	function log(event, data, formatted) {

		
	}
	
	/**
	function formatItem(row) {
		return(row[0]);
	}
	
	function formatResult(row) {
		return row[0].replace(/(<.+?>)/gi, '');
	}
	**/

	$("#p_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 500,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];   
			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name+" ["+data[i].unit_name+"]["+data[i].p_code+"]",
    				value:data[i].p_name+" ["+data[i].unit_name+"]["+data[i].p_code+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			//var result = row.split("-");

			return row;
		}
	});
	
});

function checkForm()
{	
	var theForm = document.add_union_form;
	
	if(theForm.p_name.value=="")
	{
		alert("商品名不能为空");
	}
	else if(theForm.quantity.value=="")
	{
		alert("数量不能为空");
		
	}
	else if(!isNum(theForm.quantity.value))
	{
		alert("请正确填写数量");
	}
	else
	{
		var msg = "确定把 [<%=detailPro.getString("p_name")%>] 转换成\n["+theForm.p_name.value+"] X "+theForm.quantity.value+" ？";
	
		if ( confirm(msg) )
		{
			parent.convertProduct(<%=s_pid%>,<%=s_pc_id%>,theForm.p_name.value,theForm.quantity.value);
		}

	}
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
</script>

<style type="text/css">
<!--
.STYLE1 {font-weight: bold;font-size:15px;}
.STYLE3 {color: #999999}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_union_form" method="post" action="" >
 <input type="hidden" name="s_pid" value="<%=s_pid%>">
  <input type="hidden" name="s_pc_id" value="<%=s_pc_id%>">
<table width="98%" border="0" cellspacing="3" cellpadding="2">

  <tr>
    <td align="left" valign="middle" style="padding:20px;">
	<span style="font-weight:bold;color:#666666;font-size:30px;font-family:Arial, Helvetica, sans-serif;"><span style="color:#990000"><%=storage_name%> - </span><%=detailPro.getString("p_name")%></span> <br>
<br>
	<span style="font-size:15px;color:#666666">转换成 >>></span><br>

<input type="text" id="p_name" name="p_name"  style="color:666666;width:300px;font-size:16px;height:25px;font-weight:bold;font-family:Arial, Helvetica, sans-serif"/> 
      <span style="color:#666666;font-size:20px;font-weight:bold;font-family:Arial, Helvetica, sans-serif">x</span> <input type="text" id="quantity" name="quantity"  style="color:#0000FF;width:40px;font-size:16px;height:28px;font-weight:bold"/>	</td>
  </tr>
  <tr>
    <td align="left" valign="middle" style="padding:20px;">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle" style="padding:20px;color:#666666"><img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> 填写需要转换的<strong>商品名称</strong>和<strong>数量</strong>；填写商品名称关键字，可以获得系统提示；</td>
  </tr>
</table>
</form>
		
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="right" class="win-bottom-line">
      <input type="button" name="Submit2" value="转换" class="normal-green" onClick="checkForm();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
    </td>
  </tr>
</table> 
</body>
</html>
