<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />		
		<script type="text/javascript" src="../js/select.js"></script>


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
function closeTBWin()
{
	tb_remove();
}


function del(name,ca_id)
{
	if ( confirm("确定删除地区“"+name+"”？") )
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/DelCountryArea.action";
		document.del_form.ca_id.value = ca_id;		
		document.del_form.submit();	
	}

}


function addCountryArea()
{
	$.prompt(
	
	"<div id='title'>增加分区</div><br />分区名称<br><input name='proName' type='text' id='proName' style='width:250px;'><br>代表国家<br><select name='ccid' id='ccid' onchange='getStorageProvinceByCcid(this.value)'></select><br>代表地区<br><select name='proProId' id='proProId'></select><br><br>备注<br><textarea name='proNote' id='proNote' style='width:250px;height:50px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.proName == "")
						  {
							   alert("请填写分区名称");
							   return false;
						  }
						  else if (f.ccid==0)
						  {
  							   alert("请选择代表国家");
							   return false;
						  }
						  else if (f.proProId==0)
						  {
  							   alert("请选择代表地区");
							   return false;
						  }
						  return true;
					}
				}
		  ,
		loaded:
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/GetAllCountrysJSON.action",
						function callback(data)
						{ 
							$("#ccid").clearAll();
							$("#ccid").addOption("请选择......","0");
												
							if (data!="")
							{
								$.each(data,function(i){
								$("#ccid").addOption(data[i].c_country,data[i].ccid);
								});
							}
						}
					);
				}
		,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AddCountryArea.action";
						document.add_form.name.value = f.proName;		
						document.add_form.note.value = f.proNote;		
						document.add_form.sig_country.value = f.ccid;		
						document.add_form.sig_state.value = f.proProId;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 增加: "y", 取消: "n" }
	});
}

function modCountryArea(ca_id)
{
	$.prompt(
	
	"<div id='title'>修改分区</div><br />分区名称<br><input name='proName' type='text' id='proName' style='width:250px;'><br>代表国家<br><select name='ccid' id='ccid'  onchange='getStorageProvinceByCcid(this.value)'></select><br>代表地区<br><select name='proProId' id='proProId'></select><br><br>备注<br><textarea name='proNote' id='proNote' style='width:250px;height:50px;'></textarea>",
	
	{ 
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if(f.proName == "")
						  {
							   alert("请填写分区名称");
							   return false;
						  }
						  else if (f.ccid==0)
						  {
  							   alert("请选择代表国家");
							   return false;
						  }
						  else if (f.proProId==0)
						  {
  							   alert("请选择代表地区");
							   return false;
						  }
						  return true;
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{				
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetDetailountryAreaJSON.action",
							{ca_id:ca_id},//{name:"test",age:20},
							function callback(data)
							{
								$("#proName").setSelectedValue(data.name);
								$("#proNote").setSelectedValue(data.note);
								
								$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/GetAllCountrysJSON.action",
									function callback(countrys)
									{ 
										$("#ccid").clearAll();
										$("#ccid").addOption("请选择......","0");
															
										if (countrys!="")
										{
											$.each(countrys,function(i){
											$("#ccid").addOption(countrys[i].c_country,countrys[i].ccid);
											});
										}
										
										$("#ccid").setSelectedValue(data.sig_country);
									}
								);
					
								$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
										{ccid:data.sig_country},
										function callback(state_data)
										{ 
											$("#proProId").clearAll();
											
											if (state_data!="")
											{
												$.each(state_data,function(i){
													$("#proProId").addOption(state_data[i].pro_name,state_data[i].pro_id);
												});
											}
											
											if (data.sig_state>0)
											{
												$("#proProId").setSelectedValue(data.sig_state);
											}
										}
								);
							}
					);
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/ModCountryArea.action";
						document.mod_form.name.value = f.proName;		
						document.mod_form.note.value = f.proNote;
						document.mod_form.ca_id.value = ca_id;	
						document.mod_form.sig_country.value = f.ccid;			
						document.mod_form.sig_state.value = f.proProId;				
						document.mod_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 修改: "y", 取消: "n" }
	});
}

//国家地区切换
function getStorageProvinceByCcid(ccid)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#proProId").clearAll();
					$("#proProId").addOption("请选择......","");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#proProId").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
				}
		);
}
//-->
</script>

<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}

-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 基础数据管理 »   国家分区管理</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="ca_id" >
</form>

<form method="post" name="mod_form">
<input type="hidden" name="ca_id" >
<input type="hidden" name="name">
<input type="hidden" name="note">
<input type="hidden" name="sig_country">
<input type="hidden" name="sig_state">
</form>

<form method="post" name="add_form">
<input type="hidden" name="name">
<input type="hidden" name="note">
<input type="hidden" name="sig_country">
<input type="hidden" name="sig_state">
</form>

<table width="99%" height="30" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left"><input name="Submit" type="button" class="long-button-add" onClick="addCountryArea()" value="增加分区"></td>
  </tr>
</table>
<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0"   class="zebraTable">
			
			
				  <tr> 
					<th  class="left-title"  style="vertical-align: center;text-align: left;color:#999999">分区名称</th>
					<th  style="vertical-align: center;text-align: left;color:#999999" class="left-title">备注</th>
					<th  style="vertical-align: center;text-align: left;color:#999999" class="left-title">包含国家</th> 
					<th width="22%"  style="vertical-align: center;text-align: center;color:#999999" class="right-title">&nbsp;</th>
				  </tr>
			
	
				<%
			DBRow countryAreas[] = productMgr.getAllCountryAreas(null);
			
			for ( int i=0; i<countryAreas.length; i++ )
			{
				
			
			%>
				<tr > 
				  <td   width="17%" height="60" valign="middle"><%=countryAreas[i].getString("name")%></td>
				  <td   width="26%" height="60" valign="middle"><br>
<strong>代表国家：</strong> <%=orderMgr.getDetailCountryCodeByCcid(countryAreas[i].get("sig_country",0l)).getString("c_country")%>
<br><br>
<%=countryAreas[i].getString("note")%></td>
				  <td   width="35%" height="60"  style="padding-top:5px;padding-bottom:10px;"><table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td height="40" align="left"><input name="Submit3" type="button" class="short-short-button" value="设置" onClick="tb_show('设置分区国家','set_country_area.html?ps_name=<%=countryAreas[i].getString("name")%>&ca_id=<%=countryAreas[i].get("ca_id",0l)%>&TB_iframe=true&height=500&width=400',false)"></td>
					</tr>
					<tr>
					  <td width="64%" height="30">
				<%
				DBRow countrys[] = productMgr.getCountryAreaMappingByCaId(countryAreas[i].get("ca_id",0l));
				for (int j=0; j<countrys.length; j++)
				{
				%>
					<div style="width:100px;height:18px;float:left;margin-top:5px;overflow:hidden;padding-right:10px;">
					  <%=countrys[j].getString("c_country")%>					  </div>
			   <%
			   }
			   %>					  </td>
					</tr>
			
				  </table></td>
				  <td   align="center" valign="middle">
				    <input name="Submit2" type="button" class="short-short-button-mod" onClick="modCountryArea(<%=countryAreas[i].getString("ca_id")%>)" value="修改">
				  &nbsp;&nbsp;&nbsp;&nbsp; 
				  <input name="Submit2s" type="button" class="short-short-button-del" onClick="del('<%=countryAreas[i].getString("name")%>',<%=countryAreas[i].getString("ca_id")%>)" value="删除">				  </td>
				</tr>
				<%	
			}
			%>
</table>
	  
<br>
<br>
</body>
</html>
