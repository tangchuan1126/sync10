<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

String cmd = StringUtil.getString(request,"cmd");
long cid = StringUtil.getLong(request,"cid");
long scid = StringUtil.getLong(request,"scid");

String input_st_date = StringUtil.getString(request,"input_st_date");
String input_en_date = StringUtil.getString(request,"input_en_date");
String name = StringUtil.getString(request,"name");

TDate tDate = new TDate();

if (input_st_date.equals("")&&input_en_date.equals(""))
{
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}

long st_datemark = tDate.getDateTime(input_st_date);
long en_datemark = tDate.getDateTime(input_en_date);

DBRow in_products[];

if (cmd.equals("filter"))
{
	if (cid==0&&scid==0)
	{
		in_products = productMgr.getShipProductsByStEnPc(st_datemark,en_datemark, pc);
	}
	else
	{
		in_products = productMgr.getShipProductsByStEnPcCidScid(cid,scid,st_datemark, en_datemark,pc);
	}
}
else if (cmd.equals("amount"))
{
		in_products = productMgr.getAmountShipProductsByStEnPcCid(cid, st_datemark, en_datemark,pc);
}
else if (cmd.equals("search_name"))
{
	in_products = productMgr.getShipProductsFromSearchName(name,pc);
}
else
{
	in_products = productMgr.getShipProductsByStEnPc(st_datemark,en_datemark, pc);
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.search_form.input_st_date.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.search_form.input_en_date.value = date;
}

function onMOBg(row,cl,index)
{
	row.style.background=cl;
}

function filter()
{
	document.search_form.cmd.value="filter";
	document.search_form.submit();
}

function amount()
{
	if ( document.search_form.cid.value==0 )
	{
		alert("请选择仓库");
	}
	else
	{
		document.search_form.cmd.value="amount";
		document.search_form.submit();
	}
}

function upload()
{
	tb_show('订单发货','upload_inoutfile.html?title=订单发货&filetype=ship_&backurl=<%=StringUtil.getCurrentURL(request)%>&TB_iframe=true&height=500&width=1000',false);
}

function closeWin()
{
	tb_remove();
}

function closeWinRefrech()
{
	window.location.reload();
	tb_remove();
}

function checkForm(theForm)
{
	if (theForm.name.value=="")
	{
		alert("请填写内部单号或者运单号");
		return(false);
	}
	else
	{
		return(true);
	}
}

	function ajaxGetShipCompany(ps_id,id)
   	{
   		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getExpressCompanysByPsIdJSON.action',
			dataType:'json',
			data:{ps_id:ps_id},
			success:function(data)
			{
				$("#"+id).clearAll();
				$("#"+id).addOption("全部快递","0");
				
				if (data!="")
				{
					$.each(data,function(i)
					{
						$("#"+id).addOption(data[i].name,data[i].sc_id);
					});
				}
		 	}
		})
   	}
   	
   	function downloadSendWayBillOrder()
	{
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/sendWaybillDownLoad.action',
					type: 'post',
					dataType: 'json',
					cache:false,
					data:
					{
						cid:$("#cid").val(),
						sc_id:$("#scid").val(),
						st:$("#input_st_date").val(),
						en:$("#input_en_date").val()
					},
					
					beforeSend:function(request){
						 
					},
					
					error: function(e){
						 
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						 
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
		
	}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
.fix{text-align:left;width:95%;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<form name="download_form"></form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   发货日志</td>
  </tr>
</table>
<br>
<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px">
<table width="105%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
   
      <td width="80%" height="31" style="border-bottom:1px #dddddd solid; padding-left:10px">
	   <form action="ship_product.html" method="post" name="search_form">
		<input type="hidden" name="cmd">

			开始 
              <input name="input_st_date" type="text" id="input_st_date" size="9" value="<%=input_st_date%>" readonly> 
              <script language="JavaScript" type="text/javascript" >
    <!--
    	stfooCalendar = new dynCalendar('stfooCalendar', 'stcalendarCallback', '../js/images/');
    //-->
    </script> 
               &nbsp;结束 
              <input name="input_en_date" type="text" id="input_en_date" size="9" value="<%=input_en_date%>"  readonly>
			   <script language="JavaScript" type="text/javascript">
    <!--
    	enfooCalendar = new dynCalendar('enfooCalendar', 'encalendarCallback', '../js/images/');
    //-->
	</script>
                &nbsp;
	    <select name="cid" id="cid" onchange="ajaxGetShipCompany(this.value,'scid')">
		<option value="0">选择仓库...</option>>
          <%
DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();
String qx;

for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
	 
	 if (treeRows[i].get("level",0)>1)
	 {
	 	continue;
	 }
%>
          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==StringUtil.getLong(request,"cid")?"selected":""%>> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>          </option>
          <%
}
%>
        </select> 
		&nbsp; &nbsp;
		<select name="scid" id="scid">
		<option value="0">选择快递...</option>>
          <%
DBRow companys[] = expressMgr.getAllExpressCompany(null);

for ( int i=0; i<companys.length; i++ )
{
%>
          <option value="<%=companys[i].getString("sc_id")%>" <%=companys[i].get("sc_id",0l)==scid?"selected":""%>> 
          <%=companys[i].getString("name")%>          </option>
          <%
}
%>
        </select> 
               &nbsp; &nbsp;
	          <input name="Submit" type="button" class="button_long_refresh" onClick="filter()" value=" 过 滤 ">
    </form>	  </td>
	<td width="20%" align="center">
		<input name="Submit2" type="button" class="long-button-stat" onClick="amount()" value=" 汇 总 ">
	</td>
  </tr>
  <tr>
  		<td width="80%" style="padding-top:5px; padding-left:10px">
		  <form name="form1" method="post" action="" onSubmit="return checkForm(this)">
	<input type="hidden" name="cmd" value="search_name">
      内部单号/运单号

      <input name="name" type="text" id="name" value="<%=name.equals("")?"":name%>">
      <input name="Submit3" type="submit" class="button_long_search" value="  搜索  ">
      &nbsp;&nbsp;
	  <input type="button" class="long-button-next" onclick="downloadSendWayBillOrder()" value="下载">

    </form>		  </td><td align="center"><input name="Submit23" type="button" class="long-button-upload" onClick="upload()" value=" 上传日志 "></td>
  </tr>
  </table></td>
  </tr>
</table>
</div>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="40" align="center" valign="middle"  style="font-size:18px;font-weight:bold;color:#990000">发货日志</td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form name="listForm" method="post">

    <tr> 
        <th  style="vertical-align: center;text-align: left;"  class="left-title">内部单号</th>
        <th  style="vertical-align: center;text-align: left;"  class="right-title">运单号</th>
        <th  style="vertical-align: center;text-align: center;"  class="right-title">发货仓库</th>
        <th width="11%"  style="vertical-align: center;text-align: center;"  class="right-title">重量</th>
        <th width="7%"  style="vertical-align: center;text-align: center;"  class="right-title">快递</th>
        <th width="11%"  style="vertical-align: center;text-align: center;"  class="right-title">发票信息</th>
        <th width="5%"  style="vertical-align: center;text-align: center;"  class="right-title">目的地</th>
        <th width="7%"  style="vertical-align: center;text-align: center;" class="right-title">发货日期</th>
        <th width="5%"  style="vertical-align: center;text-align: center;"  class="right-title">配货人</th>
        <th width="10%"  style="vertical-align: center;text-align: center;"  class="right-title">操作员</th>
        </tr>

    <%


for ( int i=0; i<in_products.length; i++ )
{
	DBRow waybill = wayBillMgrZJ.getDetailInfoWayBillById(in_products[i].get("name",0l));
	long ps_id = 0;
	if(waybill!=null)
	{
		ps_id = waybill.get("ps_id",0l);
	}
%>
    <tr > 
      <td   width="10%" height="46" valign="middle" style='word-break:break-all;' ><%=in_products[i].getString("name")%></td>
      <td   width="12%" valign="middle" style='word-break:break-all;' ><%=in_products[i].getString("delivery_code")%></td>
      <td align="center" valign="middle" >
      	<fieldset style="" class="set fix">
		  		<legend><span style="" class="title">
		  			<%
		  				DBRow storeCatalog = catalogMgr.getDetailProductStorageCatalogById(ps_id);
		  				out.print(storeCatalog.getString("title"));
		  			%>仓库</span></legend>		 
  	 			<%
  	 				DBRow[] wayBillItems = wayBillMgrZJ.getWayBillOrderItems(in_products[i].get("name",0l));
  	 				if(null != wayBillItems && wayBillItems.length > 0){
  	 					for(DBRow row : wayBillItems){
  	 			%>
  	 				<p>
  	 					<span class="alert-text" style="font-weight:normal;"><%= row.getString("p_name") %></span> &nbsp;x&nbsp;<span style="color:blue;" class="quantity"><%= row.getString("quantity") %></span>
  	 				
  	 				</p>
  	 			<% 
  	 					}
  	 				}
  	 			%>
  	 	</fieldset>
      </td>
      <td   align="center" valign="middle" style='word-break:break-all;'>
      	<%=in_products[i].getString("product_weight")%>
      	<p>
  			<span class="weightName">运单重量:</span>
  			<span class="weightValue"><%= waybill.getString("all_weight")%>&nbsp;kg</span>
  		</p>
  		<p>
  			<span class="weightName">打印重量:</span>
  			<span class="weightValue"><%= waybill.get("print_weight",0.0f)%>&nbsp;kg</span>
  		</p>
  		<p>
  			<span class="weightName">发货重量:</span>
  			<span class="weightValue"><%= waybill.get("delivery_weight",0.0f)%>&nbsp;kg</span>
  		</p>
  		<p>
	  		<span class="weightName">运输重量:</span>
	  		<span class="weightValue">
	  		<%
  				if(waybill.get("tracking_weight",0.0f)>0)
  				{
  			%>
	  		<%=waybill.get("tracking_weight",0.0f)%>&nbsp;kg
	  		<%
			  	}
			%>
	  		</span>
	  	</p>
      </td>
      <td   align="center" valign="middle" style='word-break:break-all;'><%=expressMgr.getDetailCompany(in_products[i].get("sc_id",0l)).getString("name")%></td>
      <td   align="center" valign="middle" style='word-break:break-all;'>
      	<p>
      		材质:<%=waybill.getString("material")%><strong>|</strong><%=waybill.getString("material_chinese")%>
      	</p>
      	<p>
      		用途:<%=waybill.getString("inv_dog")%><strong>|</strong><%=waybill.getString("inv_dog_chinese")%>
      	</p>
      	<p>
      		描述:<%=waybill.getString("inv_rfe")%><strong>|</strong><%=waybill.getString("inv_rfe_chinese")%>
      	</p>
      </td>
      <td   align="center" valign="middle" style='word-break:break-all;'><%=in_products[i].getString("deliveryto")%></td>
      <td   align="center" valign="middle" style='word-break:break-all;'><%=DateUtil.StrtoYYMMDD(in_products[i].getString("post_date"))%></td>
      <td   align="center" valign="middle" style='word-break:break-all;'><%=in_products[i].getString("delivery_operator")%></td>
      <td   align="center" valign="middle" ><span style="word-break:break-all;"><%=in_products[i].getString("account")%></span></td>
    </tr>
    <%
}
%>
</form> 
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p">
		  <input type="hidden" name="input_st_date" value="<%=input_st_date%>">
		<input type="hidden" name="input_en_date" value="<%=input_en_date%>">
		<input type="hidden" name="cid" value="<%=cid%>">
		<input type="hidden" name="scid" value="<%=scid%>">
		<input type="hidden" name="cmd" value="<%=cmd%>">
		<input type="hidden" name="name" value="<%=name%>">
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
</body>
</html>
