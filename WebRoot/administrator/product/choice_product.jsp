<%@page import="com.cwc.app.key.PriceUOMKey"%>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="org.directwebremoting.json.types.JsonArray"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<%
 String pcid = StringUtil.getString(request,"pcid");
 String cmd = StringUtil.getString(request,"cmd","NONE");
 String key = StringUtil.getString(request,"key");
 String pro_line_id = StringUtil.getString(request,"pro_line_id");
 int product_file_types = StringUtil.getInt(request, "product_file_types");
 int product_upload_status = StringUtil.getInt(request, "product_upload_status");
 int union_flag = StringUtil.getInt(request,"union_flag");
 String title_id = StringUtil.getString(request, "title_id");

 boolean edit = true;

 Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));

 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 long adminId=adminLoggerBean.getAdid();
 //根据登录帐号判断是否为客户
 Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
 //如果是用户登录 查询用户下的产品
 DBRow[] titles;
 long number=0;
 if(bl){
 	number=1;
 	titles=adminMgrZwb.getProprietaryByadminId(adminLoggerBean.getAdid());
 }else{
 	number=0;
 	
 	
 	titles=mgrZwb.selectAllTitle();
 }
LengthUOMKey lengthUOMKey = new LengthUOMKey();
WeightUOMKey weightUOMKey = new WeightUOMKey();
PriceUOMKey priceUOMKey = new PriceUOMKey();
String lengthUoms = "";
ArrayList lengthUomKeys = lengthUOMKey.getLengthUOMKeys();
for(int a=0;a<lengthUomKeys.size();a++){
	int keys = Integer.parseInt(String.valueOf(lengthUomKeys.get(a)));
	String value = lengthUOMKey.getLengthUOMKey(keys);
	lengthUoms += ";"+keys+":"+value;
}
lengthUoms = lengthUoms.substring(1);
String weightUoms = "";
ArrayList weightUomKeys = weightUOMKey.getWeightUOMKeys();
for(int a=0;a<weightUomKeys.size();a++){
	int keys = Integer.parseInt(String.valueOf(weightUomKeys.get(a)));
	String value = weightUOMKey.getWeightUOMKey(keys);
	weightUoms += ";"+keys+":"+value;
}
weightUoms = weightUoms.substring(1);
String priceUoms = "";
ArrayList priceUomKeys = priceUOMKey.getMoneyUOMKeys();
for(int a=0;a<priceUomKeys.size();a++){
	int keys = Integer.parseInt(String.valueOf(priceUomKeys.get(a)));
	String value = priceUOMKey.getMoneyUOMKey(keys);
	priceUoms += ";"+keys+":"+value;
}
priceUoms = priceUoms.substring(1);
 %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Product Basic Info</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<%-- ***********************  --%>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-ui-1.8.14.custom.min.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<%-- Frank ****************** --%>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<%-- ***********************  --%>

<link type="text/css" href="../js/jqGrid-4.1.1/js/jquery-ui-1.8.14.custom.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />


<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="/Sync10-ui/lib/jqgridtitleheigth.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<style type="text/css">
	td.categorymenu_td div{margin-top:-3px;}
</style>
<script language="javascript">

var pcid = '<%=pcid%>';
var cmd = '<%=cmd%>';
var union_flag = '<%=union_flag%>';
var key = '<%=key%>';
var pro_line_id = '<%=pro_line_id%>';
var product_file_types = '<%=product_file_types%>';
var product_upload_status = '<%=product_upload_status%>';
var title_id = '<%=title_id%>';

var lastJqgridPcid;
$().ready(function() {

	function log(event, data, formatted) {
		
	}

	<%-- Frank ****************** --%>
	addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
	<%-- ***********************  --%>
	
	$("#search_key").keydown(function(event){
		if (event.keyCode==13)
		{
			search();
		}
	});

});

function filter()
{

	pcid = $("#filter_pcid").val();
	cmd = "filter";
	union_flag = $("input#union_flag:checked").val();
	key = '';
	pro_line_id = $("#filter_productLine").val();
	product_file_types = $("#product_file_types").val();
	product_upload_status = $("#product_upload_status").val();
	title_id = $("#title_id").val();

	jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:'filter',pcid:pcid,union_flag:union_flag,pro_line_id:pro_line_id,product_file_types:product_file_types,product_upload_status:product_upload_status,title_id:title_id},page:1}).trigger('reloadGrid');
	jQuery("#union").jqGrid('setGridParam',{url:"subDataProducts.html?id="+0,page:1});
	jQuery("#union").jqGrid('setCaption'," Suit Relationship: ").trigger('reloadGrid');
		
}

function afilter(filter_pcid)
{
	jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:'filter',pcid:filter_pcid,union_flag:0},page:1}).trigger('reloadGrid');
	$("#category").mcDropdown("#categorymenu").setValue(filter_pcid);
	$("input[id=union_flag][value='<%=union_flag%>']").attr('checked',true); 
	
}

<%
StringBuffer countryCodeSB = new StringBuffer("");
DBRow treeRows[] = catalogMgr.getProductCatalogTree();
String qx;

countryCodeSB.append("0:选择商品分类;");
for (int i=0; i<treeRows.length; i++)
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
	 
	countryCodeSB.append(treeRows[i].getString("id")+":"+Tree.makeSpace("&nbsp&nbsp&nbsp",treeRows[i].get("level",0))+qx+treeRows[i].getString("title"));
	if(i<treeRows.length-1)
	{
		countryCodeSB.append(";");
	}
	
}
%>


function closeWin()
{
	tb_remove();
}

function modCatalogCloseWin(pc_id)
{
	//tb_remove();
	refProduct($("#gridtest"),pc_id);
	
}

function openCatalogMenu()
{
	$("#category").mcDropdown("#categorymenu").openMenu();
}

function showProductImg(img)
{
	if(img != "")
	{
		tb_show('',img+'?TB_iframe=true&height=350&width=700',false);
	}
	else
	{
		alert("该商品暂时没有图片");
	}
}


function autoComplete(obj)
	{
	<%-- Frank ****************** --%>
		addAutoComplete(obj,
				"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
				"merge_info",
				"p_name");

				
	<%-- ***********************  --%>	

			 		  
			
		
	}
	function beforeShowAdd(formid)
	{
		$("#tr_unit_name").remove();
		$("#tr_p_code").remove();
		$("#tr_weight").remove();
		$("#tr_unit_price").remove();
		$("#tr_volume").remove();
		$("#tr_gross_profit").remove();
		jQuery("#union").jqGrid('setGridParam',{editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditUnionJqgrid.action?set_pid='+union_id,});
	}
	
	function beforeShowAddProduct(formid)
	{
		$("#tr_alive_text").remove();
	}
	
	function beforeShowEdit(formid)
	{
		
	}
	
	function beforeShowDel(formid)
	{
		jQuery("#union").jqGrid('setGridParam',{editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditUnionJqgrid.action?set_pid='+union_id,});
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	
	function refProductUnion(obj,rowid,p_name,iRow,iCol)
	{
		var para ="p_name="+p_name;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductGridJSONByPname.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				async:false,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					//console.log(data);
					obj.setRowData(rowid,data);
					
					jqgrideditcell(obj,iRow,iCol);
				}
			});
	}
	
	
	function refProduct(obj,rowid,iRow,iCol)
	{
		
		
		var para ="pc_id="+rowid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductGridJSONById.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				async:false,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					
				

					//console.log(JSON.stringify(data));
					obj.setRowData(rowid,data);
					//alliCol=iCol;
					jqgrideditcell(obj,iRow,iCol);
		                 
				}
			});
	}
	
	var alliCol,Timeouteditcell="";
	function jqgrideditcell(obj,iRow,iCol){		

		 /*
		
			 if(typeof Timeouteditcell!="undefined" && Timeouteditcell!=""){
                clearTimeout(Timeouteditcell);
               }
                Timeouteditcell =setTimeout(function(){
                  obj.jqGrid('editCell',iRow,alliCol,true);
              },1000);
            */
			
		
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}
	
	var icolclick=true;
	
	function refushUnion(obj,pid,iRow,iCol)
	{
		var para ="pid="+pid;
		
	
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getUnionProductDetailsByPid.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(){
						alert("提交失败，请重试！");
					},
					
					success: function(data)
					{
						
						
						icolclick=false;
						for(i=0;i<data.length;i++)
						{
							obj.setRowData(data[i].set_pid,data[i]);
						}
						
						/*
						 if(typeof Timeouteditcell!="undefined" && Timeouteditcell!=""){
			                  clearTimeout(Timeouteditcell);
			                }
			                  Timeouteditcell =setTimeout(function(){			                	 
			                   obj.jqGrid('editCell',iRow,iCol,true);
			                   
			                   icolclick=true;
			               },300);
			                  */
					}
				});
	}
	
	function afterComplete()
	{
		refProduct($("#gridtest"),union_id);
	}
	
	function search()
	{
		if ($("#search_key").val()=="")
		{
			alert("请填写关键字");
			pcid = '';
			cmd = '';
			union_flag = '';
			key = '';
			pro_line_id = '';
			product_file_types = '';
			product_upload_status = '';
			title_id = '';
			//return(false);
		}
		else
		{

			pcid = $("#filter_pcid").val();
			cmd = "search";
			union_flag = $("input#union_flag:checked").val();
			key = $("#search_key").val();
			pro_line_id = '';
			product_file_types = '';
			product_upload_status = '';
			title_id = $("#title_id").val();
			
			//jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:'search',pcid:filter_pcid,key:key,title_id:title_id}}).trigger('reloadGrid');
			jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:cmd,pcid:pcid,key:key,title_id:title_id}}).trigger('reloadGrid');
			jQuery("#union").jqGrid('setGridParam',{url:"subDataProducts.html?id="+0,page:1});
			jQuery("#union").jqGrid('setCaption'," Suit Relationship: ").trigger('reloadGrid');
		}
	}
	
	
	function swichProductAlive(name,pc_id,curAlive)
	{
		var msg;
		
		if (curAlive==1)
		{
			msg = "Are you sure to forbidden "+name+" to use?";
		}
		else
		{
			msg = "Are you sure to allow "+name+" to use?";
		}
	
		if (confirm(msg))
		{
			var para ="pc_id="+pc_id+"&curAlive="+curAlive;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/swichProductAlive.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert(e);
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					if(data.result=="true")
					{
						refProduct(jQuery("#gridtest"),pc_id);
					}
					
				}
			});
		}
	}

	function ajaxLoadProductLineMenuPage(title_id, pc_line_id,divId)
	{
			var para = "title_id="+title_id+"&product_line_id="+pc_line_id+"&divId="+divId;
			$.ajax({
				url: 'product_line_menu_for_title.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#productLinemenu_td").html(html);
				}
			});
	}
	
	function ajaxLoadCatalogMenuPage(id,title_id,divId)
	{
			var para = "id="+id+"&title_id="+title_id;
			$.ajax({
				url: 'product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
	
	function showImg(pc_id)
	{
		window.open("product_file.html?pc_id="+pc_id);
	}

	 function make_tab(productId)
	 {
		var purchase_id="";
		var factory_type="";
		var supplierSid="";
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/lable_template_ui_show.html?purchase_id='+purchase_id+'&pc_id='+productId+'&factory_type='+factory_type+'&supplierSid='+supplierSid; 
		$.artDialog.open(uri , {title: '标签列表',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	 }
	 function manage_pro_title(pc_id,p_name)
	 {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_list_by_product_related_for_select.html?pc_id="+pc_id;
		$.artDialog.open(url , {title: '管理'+p_name+'的TITLE',width:'700px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	 }
	 function changeTitle()
	 {
		ajaxLoadProductLineMenuPage($("#title_id").val(), "");
	 }
	 function importProductTitles(_target)
	 {
		    var fileNames = $("#file_names").val();
		    var obj  = {
			     reg:"xls",
			     limitSize:2,
			     limitNum:1
			 }
		    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
			uri += jQuery.param(obj);
			 if(fileNames.length > 0 ){
				uri += "&file_names=" + fileNames;
			}
			 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true});
		}
		//jquery file up 回调函数
		function uploadFileCallBack(fileNames)
		{
			if($.trim(fileNames).length > 0 )
			{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/check_product_title_show.html?fileNames="+fileNames;
				$.artDialog.open(uri,{title: "检测上传用户title信息",width:'850px',height:'530px', lock: true,opacity: 0.3,fixed: true});
			}
		}
	 function refreshWindow(){
			window.location.reload();
		};

	function loadData(){
		var titles = <%=new JsonObject(titles).toString()%> ;
		var item=[];
		for(var i=0;i<titles.length;i++){
			var op={};       
			op.id=titles[i].title_id;                                                                                                                                                                                                                                                                                                                                                                                                      
			op.name=titles[i].title_name;
			item.push(op);
		}
		
		
		var data=[{
			key:'TITLE：',
			type:'title',
			son_key:'Product Line：',
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadProductLineAction.action',
			
			son_key2:'Parent Category：',
			url2:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadFirstProductCatalogAction.action',
			
			son_key3:'Sub Category：',
			url3:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadSecondProductCatalogAction.action',	
					
			son_key4:'Sub-Sub Category：',
			url4:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadThirdProductCatalogAction.action',

			array:item
		 },
		 {key:'Product Type：',select:'true',type:'leixing',array:[{id:'0',name:'ITEM'},{id:'1',name:'UNIT'}]},	 
		// {key:'图片类型：',select:'true',type:'picType',array:[{id:'1',name:'商品包装'},{id:'2',name:'商品本身 '},{id:'3',name:'商品底帖 '},{id:'4',name:'商品称重  '},{id:'5',name:'商品标签 '}]},
		 {key:'Photos Status：',select:'true',type:'picStuts',array:[{id:'1',name:'Not Complete'},{id:'2',name:'Complete'}]},
	 	 ];
		initializtion(data);  //初始化
		
		
		//document.getElementById("liexing").style.display="none";
		$('#leixing').hide();
	}	
	
	//点击查询条件
	function custom_seach(){
		var array=getSeachValue();
		var title_id=0;
		var pro_line_id=0;
		var product_file_types=0;
		var product_upload_status=0;
		var union_flag=0;
		var pcid=0;
		for(var i=0;i<array.length;i++){
			 if(array[i].type=='title'){
				   title_id=array[i].val;
			 }
			 if(array[i].type=='title_son'){
				 pro_line_id=array[i].val;
			 }
			 if(array[i].type=='title_son_son'){
				 pcid=array[i].val;
			 }else if(array[i].type=='title_son_son_son'){
				 pcid=array[i].val;
			 }else if(array[i].type=='title_son_son_son_son'){
				 pcid=array[i].val;
			 }
			 if(array[i].type=='picType'){
				 product_file_types=array[i].val;
			 }
			 if(array[i].type=='picStuts'){
				 product_upload_status=array[i].val;
			 }
			 if(array[i].type=='leixing'){
				 union_flag=array[i].val;
			 }
		}

		key = '';
		jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:'filter',pcid:pcid,union_flag:union_flag,pro_line_id:pro_line_id,product_file_types:product_file_types,product_upload_status:product_upload_status,title_id:title_id},page:1}).trigger('reloadGrid');
		jQuery("#union").jqGrid('setGridParam',{url:"subDataProducts.html?id="+0,page:1});
		jQuery("#union").jqGrid('setCaption'," Suit Relationship: ").trigger('reloadGrid');	
	}
	function eso_button_even()
	{
		document.getElementById("eso_search").oncontextmenu=function(event) 
		{  
			if (document.all) window.event.returnValue = false;// for IE  
			else event.preventDefault();  
		};  
			
		document.getElementById("eso_search").onmouseup=function(oEvent) 
		{  
			if (!oEvent) oEvent=window.event;  
			if (oEvent.button==2) 
			{  
				search();
			}  
		};
	}
	$(function(){ 		
 		tabletitleauto.settitle({domid:"#gbox_gridtest",height:35,fontsize:14});
 	});
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		width: 100%;
		vertical-align:middle;
		padding:3px;
	}
<!-- 高级搜索的样式-->
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;	
	border:1px #bdbdbd solid;	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
ul.myul{list-style-type:none;}
ul.myul li{float:left;width:150px;border:0px solid red;line-height:25px;height:25px;border:0px solid silver;}

/*
.ui-jqgrid .ui-jqgrid-hbox {
    float: left;
    height: 45px;
    padding-right: 20px;
    vertical-align: middle;
}
.ui-jqgrid .ui-jqgrid-htable th div {
    height: 45px;
    overflow: hidden;
    position: relative;
    vertical-align: middle;
     
}
*/

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="loadData()">
	<div id="tabs">
		<ul>
			<li><a href="#av1">Advanced Search</a></li>
			<li><a href="#av2">Common Tools</a></li>		 
		</ul>
		<div id="av1">
			<!-- 条件搜索 -->
			<div id="av"></div>
			<input type="hidden" id="atomicBomb" value=""/>
			<input type="hidden" id="title" />
		</div>
		<div id="av2">
			<div id="easy_search_father">
				<div id="easy_search" style="margin-top:-2px;">
					<a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a>
				</div>
		    </div>
	           <table width="400" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="418">
						<div  class="search_shadow_bg">
						 <input name="search_key" id="search_key" value=""  type="text" class="search_input" style="font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)search()"/>
						</div>
					</td>
				</tr>
			  </table>	
			  <script>eso_button_even();</script>
	    </div>
	</div>
	<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,  
		cookie: { expires: 30000 } ,
	});
/*	
	$(document).ready(function(){
		$("#leixing").hide();
		});	
*/

	</script>


	<div style="clear:both"></div>
</div>


<table id="gridtest"></table>
<script type="text/javascript">


function cleanSearchKey(divId)
{
	$("#"+divId+"search_product_name").val("");
}
function addProductsPicture()
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_file_upload.html?";
	var addUri =  getSelectedIdAndNames();
 	if($.trim(addUri).length < 1 ){
		showMessage("请先选择商品!","alert");
		return ;
	}else{uri += addUri;}
 	var reg		= new RegExp("&","g"); //创建正则RegExp对象 
	var pc_ids	= addUri.substr(1).replace(reg,"=");  
	var pcIds	= pc_ids.split("=")[1];
	$.artDialog.open(uri , {title: "商品图片上传",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,
		close:function()
		{
			if(pcIds.length > 0)
			{
				var pcIdArr = pcIds.split(",");
				if(pcIdArr.length > 0)
				{
					for(var i = 0; i < pcIdArr.length; i ++)
					{
						modCatalogCloseWin(pcIdArr[i]);
					}
				}
			}
		}
	});
}
function getSelectedIdAndNames(){
	s = jQuery("#gridtest").jqGrid('getGridParam','selarrrow')+"";
	var theFirstName = "";
 	var array = s.split(",");
	var strIds = "";
 	for(var index = 0 , count = array.length ; index < count ; index++ ){
 	   if(0 == index)
 	   {
 		  theFirstName = $("#gridtest").getCell(array[0],"p_name");
 	   }
	   var number = array[index];
 	   var thisid= $("#gridtest").getCell(number,"pc_id");
 	   strIds += (","+thisid);
	}
	if(strIds.length > 1 ){
	    strIds = strIds.substr(1);
	}
	if(strIds+"" === "false"){return "";}
	
	return  "&pc_id="+strIds+"&theFirstName="+theFirstName;
}
function addProductPicture(pc_id,name)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_file_upload.html?pc_id="+pc_id+"&theFirstName="+name;
	$.artDialog.open(uri , {title: "Upload Photos | "+name+"",width:'875px',height:'500px', lock: true,opacity: 0.3,close:function(){modCatalogCloseWin(pc_id);},fixed: true});
}
function addProductFunc()
{
		alert("11");
}
</script>
<div id="pager2"></div>
<br/>
<!-- 
<table id="union"></table>
 -->
<div id="pageunion"></div>  
	
	<script type="text/javascript">
	

	
			var	lastsel;
		var initSring=function(elem){

                var obj=$(elem);
                 var valsring =obj.val();
                 if(valsring.indexOf("(") > -1)
                 obj.val(valsring.split(/\(/)[0]);
               return false;
     		};
			var SringTemplate = {editoptions: { dataInit: initSring }};

		//gaowenzhen,2015.2.4===set	
	    var lastColIndex,lastRowIndex,__iRow,list2this;
	    //===end
		$(window).resize(function(){
			$("#gridtest").jqGrid('setGridWidth', $('#tabs').innerWidth());
		});
	    var _width = $('#tabs').innerWidth();
		 $("#gridtest").jqGrid({
					//scroll:1,
					sortable:true,
					url:'choiceDataProducts.html',//dataDeliveryOrderDetails.html
					postData:{pcid:pcid,cmd:cmd,pro_line_id:'<%=pro_line_id%>'},
					datatype: "json",
					width:800 > _width ? 800 : _width,
					height:260,
					autowidth: false,
					shrinkToFit:true,
					jsonReader:{
				   			id:'pc_id',
	                        repeatitems : false,
	                        subgrid:{repeatitems : false}
	                	},
	                colNames:['pc_id','catalog_id','','Product Name','Main Code','Codes','<em>Product Line</em><em>Category</em>','Unit','Length','Width','Height','Weight','Price','Volume','<em>Length</em><em>UOM</em>','<em>Weight</em><em>UOM</em>','<em>Price</em><em>UOM</em>','LengthUOMKey','WeightUOMKey','PriceUOMKey','Images'],  
				   	colModel:[ 
				   		{name:'pc_id',index:'pc_id',hidden:true,sortable:false},
				   		{name:'catalog_id',index:'catalog_id',hidden:true,sortable:false},
				   		{name:'unionimg',index:'unionimg',sortable:false,width:20},
				   		{name:'p_name',index:'p_name',align:'left',editrules:{required:true},width:200},
				   		{name:'p_code',index:'p_code',align:'left',editrules:{required:true},width:110},
				   		{name:'p_codes',index:'p_codes',align:'left',width:130},
				   		{name:'catalog_text',index:'catalog_text',sortable:false,align:'left',edittype:'button',editoptions:{value:'Mod Category',class:'button_long_refresh'},width:160},
				   		{name:'unit_name',hidden:true,index:'unit_name',sortable:false,align:'center',width:60,editrules:{required:true},width:35}, 
				   		{name:'length',index:'length',sortable:false,hidden:false,align:'center',formatter:'number',editrules:{minValue:0,required:true,number:true},formatoptions:{decimalPlaces:1,thousandsSeparator: ","},width:45},
				   		{name:'width',index:'width',sortable:false,hidden:false,align:'center',formatter:'number',editrules:{minValue:0,required:true,number:true},formatoptions:{decimalPlaces:1,thousandsSeparator: ","},width:45},
				   		{name:'heigth',index:'heigth',sortable:false,hidden:false,align:'center',formatter:'number',editrules:{minValue:0,required:true,number:true},formatoptions:{decimalPlaces:1,thousandsSeparator: ","},width:45},
				   		{name:'weight',index:'weight',sortable:false,hidden:false,align:'center',formatter:'number',editrules:{required:true,number:true},formatoptions:{decimalPlaces:3,thousandsSeparator: ","},width:50},
				   		{name:'unit_price',index:'unit_price',hidden:true,formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","},editrules:{required:true,number:true},sortable:false,align:'center',width:50},
				   		{name:'volume',index:'volume',hidden:true,align:'center',editrules:{number:true,required:true},formatter:'number',editrules:{required:true,number:true},formatoptions:{decimalPlaces:1,thousandsSeparator: ","},width:50},
				   		{name:'length_uom_name',index:'length_uom_name',editable:true,edittype:'select',align:'center',editrules:{required:true},width:45,hidden:true},
				   		{name:'weight_uom_name',index:'weight_uom_name',editable:true,edittype:'select',editable:false,align:'center',editrules:{required:true},width:45,hidden:true},
				   		{name:'price_uom_name',index:'price_uom_name',editable:true,edittype:'select',editable:false,align:'center',editrules:{required:true},width:45,hidden:true},
				   		{name:'length_uom',index:'length_uom',editable:true,hidden:true,editable:false,align:'center',editrules:{required:true},width:50},
				   		{name:'weight_uom',index:'weight_uom',editable:true,hidden:true,editable:false,align:'center',editrules:{required:true},width:50},
				   		{name:'price_uom',index:'price_uom',editable:true,hidden:true,editable:false,align:'center',editrules:{required:true},width:50},
				   		{name:'img',index:'img',sortable:false,align:'left',width:125,hidden:true}
				   		], 
				   	rowNum:50,//-1显示全部
				   	mtype: "GET",
					rownumbers:true,
				   	pager:'#pager2',
				   	sortname: 'pc_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	//cellEdit: true, 
				   	cellsubmit: 'remote',
				   	//multiselect: true,
				   	caption: "Product Basic Info",
				   	shrinkToFit:false,
				 onCellSelect: function (rowid,iCol,cellName,e){
					// console.log(rowid+"==="+"===="+cellName+"----"+e);
					 // isT= e.currentTarget.innerText
				// console.log(e);
					
				   	//gaowenzhen,2015.2.4===
                	list2this=this;
                    var $this = $(this);
                    $this.jqGrid('setGridParam', {cellEdit: true});
                    $this.jqGrid('editCell', __iRow, iCol, true);
                    $this.jqGrid('setGridParam', {cellEdit: false});
                    
                    var row = $this.jqGrid('getRowData', rowid);
                    parent.$('#p_name').val(row.p_name);
                   //window.top.$('#p_name').val(row.p_name);
                   $.artDialog.close();

                    //lastRowIndex = __iRow;
                   
                   // lastColIndex = iCol;
            
                    
                },
                beforeSelectRow: function (rowid, e) {
                //gaowenzhen,2015.2.4===
                	list2this=this;
                    var $this = $(this),
                        $td = $(e.target).closest('td'),
                        $tr = $td.closest('tr'),
                        iRow = $tr[0].rowIndex,
                        iCol = $.jgrid.getCellIndex($td);	
                        __iRow=iRow;
                   
                    if (typeof lastRowIndex !== "undefined" && typeof lastColIndex !== "undefined" &&
                            (iRow !== lastRowIndex || iCol !== lastColIndex)) {
                    
                   ClearEditor(this,iRow,iCol);

                    }
                    

                    return true;
                },
                afterEditCell: function (rowid, cellName, cellValue, iRow) {
                //gaowenzhen,2015.2.4===
                	list2this=this;
                    var cellDOM = this.rows[iRow], oldKeydown,
                        $cellInput = $('input, select, textarea', cellDOM),
                        events = $cellInput.data('events'),
                        $this = $(this);
                    if (events && events.keydown && events.keydown.length) {
                        oldKeydown = events.keydown[0].handler;
                        $cellInput.unbind('keydown', oldKeydown);
                        $cellInput.bind('keydown', function (e) {
                            $this.jqGrid('setGridParam', {cellEdit: true});
                            oldKeydown.call(this, e);
                            $this.jqGrid('setGridParam', {cellEdit: false});
                        });
                    }
                },
			  onSelectCell: function(id,name){

				   					 	if(id == null) 
				   					 	{ 
				   					 		ids=0; 
				   					 		if(jQuery("#union").jqGrid('getGridParam','records')>0 )
				   					 		{
				   					 			jQuery("#union").jqGrid('setGridParam',{url:"subDataProducts.html?id="+id,page:1});
				   					 			jQuery("#union").jqGrid('setCaption'," Suit Relationship: ").trigger('reloadGrid');
				   					 		} 
				   					 	} 
				   					 	else 
				   					 	{ 
				   					 		var p_name = jQuery("#gridtest").jqGrid('getCell',id,'p_name');
				   					 		var catalog_id = jQuery("#gridtest").jqGrid('getCell',id,'catalog_id');
				   					 		
				   					 		
				   					 		jQuery("#union").jqGrid('setGridParam',{url:"subDataProducts.html?id="+id,page:1});
				   					 		jQuery("#union").jqGrid('setCaption',p_name+" Suit Relationship").trigger('reloadGrid');
				   					 	} 
				   						 formatUom(id);
				   					 },
				   					formatCell:function(id,name,val,iRow,iCol)
									{
				   						formatUom(id);
									},
				   	beforeEditCell:function(id,name,val,iRow,iCol)
				   				  {
				   					if(id == null) 
				   					 	{ 
				   					 		lastJqgridPcid = id;
				   					 		ids=0; 
				   					 		if(jQuery("#union").jqGrid('getGridParam','records')>0 )
				   					 		{
				   					 			jQuery("#union").jqGrid('setGridParam',{url:"subDataProducts.html?id="+id,page:1});
				   					 			jQuery("#union").jqGrid('setCaption'," Suit Relationship: ").trigger('reloadGrid');
				   					 		} 
				   					 	} 
				   					 	else 
				   					 	{ 
				   					 		var p_name = jQuery("#gridtest").jqGrid('getCell',id,'p_name');
				   					 		jQuery("#union").jqGrid('setGridParam',{url:"subDataProducts.html?id="+id,page:1});
				   					 		jQuery("#union").jqGrid('setCaption',p_name+" Suit Relationship").trigger('reloadGrid');
				   					 	}
				   					
				   				  },
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				   				  {	
                  //gaowenzhen,2015.2.4===set
				   list2this=this;
                   var $this = $(this);
                	setTimeout(function(){

                	//===end
				   						   		
				   				
				   				  	if(name=="catalog_text"||name=="p_code"||name=="length"||name=="width"||name=="heigth"||name=="unit_name"||name=="weight"||name=="unit_price")
				   				  	{
				   				  	  
				   				  		refProduct(jQuery("#gridtest"),rowid,iRow,iCol);
				   				  	}
				   				  	if(name=="weight"||name=="unit_price")
					   				{
				   				  	
					   				  		refushUnion(jQuery("#gridtest"),rowid,iRow,iCol);//这里的rowid已经是配件的ID了
					   				}
				   				 if(name=='length_uom_name'||name=='weight_uom_name' || name=='price_uom_name')
							   	  	{
				   						refProduct(jQuery("#gridtest"),rowid,iRow,iCol);
				   						formatUom(rowid);
									}


             //gaowenzhen,2015.2.4===set
				   		
               if(typeof lastRowIndex!="undefined" && typeof lastColIndex!="undefined"){
                	 
                	 ClearEditor(this,iRow,iCol);
                    }

                	}, 120);

                	//===end
				        
				   				  	
				   		 },
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditProductJqgrid.action',
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditProductJqgrid.action',
				   	errorCell:function(serverresponse, status)
				   				{
				   					alert(errorMessage(serverresponse.responseText));
				   				}
				   	}); 
		
//gaowenzhen,2015.2.4===set

//撤销编辑

function ClearEditor(giridobj,iRow,iCol){
 
  var $this=$(giridobj);
    if (typeof lastRowIndex !== "undefined" && typeof lastColIndex !== "undefined" ){
                    	
                        $this.jqGrid('setGridParam', {cellEdit: true});
                        $this.jqGrid('restoreCell', lastRowIndex, lastColIndex, true);
                        $this.jqGrid('setGridParam', {cellEdit: false});
                        $(giridobj.rows[lastRowIndex].cells[lastColIndex]).removeClass("ui-state-highlight");
                    }


}


//关闭开启的单元格
 $(document).click(function(e){

 

e.preventDefault();
//gets the element where the click event happened
var clickedElement = e.target;

if(!clickedElement.offsetParent){


 if(typeof lastRowIndex!="undefined" && typeof lastColIndex!="undefined" && list2this){


 ClearEditor(list2this,lastRowIndex,lastColIndex);


 }

 return false;

}

  

});


//===end



                 
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:false,del:false,search:false,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:true,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{errorTextFormat:errorFormat},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
			   	//jQuery("#gridtest").jqGrid('gridResize',{minWidth:350,maxWidth:parseInt(document.body.scrollWidth*0.95),minHeight:80, maxHeight:350});
			   	
			  
			   	
<%-- 			$("#gridtest").jqGrid('setColProp','length_uom',{editable:true,editType:"select",editoptions:{value:getBLPSelect(product_id,<%=b2BOrder.get("receive_psid",0l)%>)}}); --%>
			   	

			   	
			   	
			   	var union_id;//增加、删除套装配件时记录当前主商品ID
			   	$("#union").jqGrid({
					sortable:true,
					url:"subDataProducts.html",
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.95),
					height:150,
					autowidth: false,
					shrinkToFit:true,
					caption: " Suit Relationship",
					jsonReader:{
				   			id:'pid',
	                        repeatitems : false,
	                	},
				   	

				   	rowNum:100,//-1显示全部
				   	pgtext:false,
				   	pgbuttons:false,
				   	mtype: "GET",
				   	gridview:true,
					rownumbers:true,
				   	pager:'#pageunion',
				   	sortname: 'pc_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditUnionJqgrid.action',
				   	loadComplete:function(data)
				   	{
				   		union_id = data.union_id;
				   	},
				   	afterEditCell:function(id,name,val,iRow,iCol)
				   				  { 
				   					if(name=='p_name') 
				   					{
				   						autoComplete(jQuery("#"+iRow+"_p_name","#union"));
				   					}
				   				  }, 
				   	beforeSubmitCell:function(rowid)
				   				{
				   					var set_pid = jQuery("#union").getCell(rowid,'set_pid');
				   					var pid = rowid;
				   					
				   					jQuery("#union").jqGrid('setGridParam',{cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditUnionJqgrid.action?id='+rowid+'&set_pid='+set_pid,});
				   				},
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				   				  {
				   					/*	if(name=="weight"||name=="unit_price")
					   				  	{
					   				  		refushUnion(jQuery("#gridtest"),rowid);
					   				  	}
					   				  	else*/ if(name=="quantity"||name=="p_name" ||name=="weight"||name=="unit_price")
					   				  	{
					   				  		var set_pid = jQuery("#union").jqGrid('getCell',rowid,'set_pid');
					   				  	//	if(name=="p_name")
					   				  	//	{
					   				  			var p_name = jQuery("#union").jqGrid('getCell',rowid,'p_name')
					   				  			refProductUnion(jQuery("#union"),rowid,p_name,iRow,iCol);
					   				  	//	}
					   				  		refProduct(jQuery("#gridtest"),set_pid,iRow,iCol);
					   				  	}
					   				  	//refProduct(jQuery("#gridtest"),rowid);
				   				  },
				   	errorCell:function(serverresponse, status)
				   			  {
				   				alert(errorMessage(serverresponse.responseText));
				   			  }
				   	
				   	}); 		   	
			   	jQuery("#union").jqGrid('navGrid',"#pageunion",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},
						{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},
						{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat,afterComplete:afterComplete, width:350}
						,{beforeShowForm:beforeShowDel,afterComplete:afterComplete},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	//jQuery("#union").jqGrid('navGrid',"#pageunion",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat,afterComplete:afterComplete},{beforeShowForm:beforeShowDel,afterComplete:afterComplete},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#union").jqGrid('navButtonAdd','#pageunion',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#union").jqGrid('columnChooser');}});
			    jQuery("#union").jqGrid('gridResize',{minWidth:350,maxWidth:parseInt(document.body.scrollWidth*0.95),minHeight:80, maxHeight:350, width:600});
			    
			    
			    
				
			    
			    
			    
			    
			   	</script>

<form action="" name="download_form" id="download_form">
</form>
</body>
</html>
<script>
function formatUom(id)
{
	var product_id = jQuery("#gridtest").jqGrid('getCell',id,'pc_id');

	$("#gridtest").jqGrid('setColProp','length_uom_name',{editable:true,editType:"select",editoptions:{value:"<%=lengthUoms%>"}});
	$("#gridtest").jqGrid('setColProp','weight_uom_name',{editable:true,editType:"select",editoptions:{value:"<%=weightUoms%>"}});
	$("#gridtest").jqGrid('setColProp','price_uom_name',{editable:true,editType:"select",editoptions:{value:"<%=priceUoms%>"}});
}
function getAllLengthUom()
{
	return "<%=lengthUoms%>";
}
function getAllWeightUom()
{
	return "<%=weightUoms%>";
}
function getAllPriceUom()
{
	return "<%=priceUoms%>";
}
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

function down(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var para='pcid='+pcid+'&cmd='+cmd+'&key='+key+'&union_flag='+union_flag+'&pro_line_id='+pro_line_id
			+'&product_file_types='+product_file_types+'&product_upload_status='+product_upload_status+'&title_id='+title_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxDownProductAndTitleAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();       //遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}	
		}
	});
}

function customRefresh(){   //无条件时刷新页面
	location.reload();
}
</script>
