<%@page import="com.cwc.app.key.LengthUOMKey"%>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<%@page import="com.cwc.app.key.YesOrNotKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@ include file="../../include.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Product Details</title>
<%--	<link href="../comm.css" rel="stylesheet" type="text/css">--%>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	
<%--	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>--%>
<%--	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>--%>
<%--	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>--%>
	<!-- accordion -->
	<link href="../js/accordion/jquery-ui.css" rel="stylesheet">
	<script src="../js/accordion/jquery-ui.js"></script>
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
  	<%
  		long pc_id = StringUtil.getLong(request, "pc_id");
  		DBRow productInfo = productMgrZyj.findProductInfosByPcid(pc_id);
  		DBRow[] productSns = proprietaryMgrZyj.getProductSnByPcId(pc_id);
  		String sn_size = "";
  		if(productSns != null && productSns.length > 0){
  			for(DBRow productSn : productSns){
  				sn_size += productSn.getString("sn_size")+",";
  			}
  			sn_size = sn_size.substring(0, sn_size.length()-1);
  		}else{
  			sn_size = "";
  		}
  	%>
  	<script type="text/javascript">
	$(function() {
		$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">Loading......</div>'});
		 $( "#accordion" ).accordion({
			 heightStyle: "content"
			 });
			var para = "pc_id=<%=pc_id%>&theFirstName=<%=productInfo.getString("p_name") %>&can_operate=<%=YesOrNotKey.NO%>";
			 $.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_file_upload.html',
					dataType:'html',
 					data:para,
					success:function(html){	
						$.unblockUI();
						$('#product_images').html($(html));
					},
					error:function(){
						$.unblockUI();
						showMessage("System error, try it again...","error");
					}
			});
	});
	
	//product/product_file_upload.html?pc_id=1051327&theFirstName=M551D-A2/10248260045
  	</script>
  	<style type="text/css">
	.outterTr{height: 30px;}
  	.outterLeftTd{width:25%;text-align: right;padding-right: 10px;border-bottom: 1px silver dotted;vertical-align: middle;}
  	.outterRightTd{width:75%;text-align: left;padding-left: 10px;border-bottom: 1px silver dotted;vertical-align: middle;}
	.fieldName{font-size: 14px;}
	.fieldValue{font-size: 14px;font-weight: bold;}
	.ui-accordion-icons{line-height:35px;}
  	</style>
  </head>
  
  <body style="padding:10px;">
  
  <div id="accordion">
	<h3>Basic</h3>
	<div>
		<table style="width:98%;border-collapse: collapse;" cellpadding="1" cellspacing="0">
			<tr class="outterTr">
				<td class="outterLeftTd">
					<span class="fieldName">Product Name&nbsp;:</span>
				</td>
				<td class="outterRightTd">
					<span class="fieldValue"><%=productInfo.getString("p_name") %></span>
				</td>
			</tr>
			<tr class="outterTr">
				<td class="outterLeftTd">
					<span class="fieldName">Main Code&nbsp;:</span>
				</td>
				<td class="outterRightTd">
					<span class="fieldValue"><%=productInfo.getString("p_code") %></span>
				</td>
			</tr>
<!-- 			<tr class="outterTr"> -->
<!-- 				<td class="outterLeftTd"> -->
<!-- 					<span class="fieldName">Amozon Code&nbsp;:</span> -->
<!-- 				</td> -->
<!-- 				<td class="outterRightTd"> -->
<%-- 					<span class="fieldValue"><%=productInfo.getString("p_code2") %></span> --%>
<!-- 				</td> -->
<!-- 			</tr> -->
			<tr class="outterTr">
				<td class="outterLeftTd">
					<span class="fieldName">UPC Code:</span>
				</td>
				<td class="outterRightTd">
					<span class="fieldValue"><%=productInfo.getString("upc") %></span>
				</td>
			</tr>
						<%
							Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
					 		StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
						  	String product_line_name = "";
						  	DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(productInfo.getString("catalog_id")));
						  	if(null != catalog)
						  	{
						  		long product_line_id = catalog.get("product_line_id", 0L);
						  		DBRow productLine = productLineMgrTJH.getProductLineById(product_line_id);
						  		if(null != productLine)
						  		{
						  			product_line_name = productLine.getString("name");
						  			catalogText.append("<img src='img/folderopen.gif'/>"+product_line_name+"<br/>");
							  		String s = "<img src='img/joinbottom.gif'/>";
							  		catalogText.append(s);
						  		}
						  	}
						  	DBRow allFather[] = tree.getAllFather(productInfo.get("catalog_id",0l));
						  	for (int jj=0; jj<allFather.length-1; jj++)
						  	{
						  		catalogText.append("<img src='img/folderopen.gif'/> "+allFather[jj].getString("title")+"<br/>");
						  		String s = (jj==0) ? "<img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>"  : "<img  src='img/empty.gif'/><img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>";
						  		catalogText.append(s);
						  	}
						  	if (catalog!=null)
						  	{
						  		catalogText.append("<img  src='img/page.gif'/>"+catalog.getString("title")+"");
						  		
						  	}
							%>
			<tr class="outterTr">
				<td class="outterLeftTd" valign="top">
					<span class="fieldName">Product Line&nbsp;/&nbsp;Category&nbsp;:</span>
				</td>
				<td class="outterRightTd">
					<span class="fieldValue"><%=catalogText %></span>
				</td>
			</tr>
			<tr class="outterTr">
				<td class="outterLeftTd" valign="top">
					<span class="fieldName">Unit&nbsp;:</span>
				</td>
				<td class="outterRightTd">
					<span class="fieldValue"><%=productInfo.getString("unit_name") %></span>
				</td>
			</tr>
			<tr class="outterTr">
				<td class="outterLeftTd">
					<span class="fieldName">Dimension&nbsp;:</span>
				</td>
				<td class="outterRightTd">
					<span class="fieldValue">
						<%LengthUOMKey lengthUomKey = new LengthUOMKey(); %>
						<%=productInfo.get("length", 0f)%>&nbsp;*&nbsp;<%=productInfo.get("width", 0f)%>&nbsp;*&nbsp;<%=productInfo.get("heigth", 0f)%>&nbsp;(<%=lengthUomKey.getLengthUOMKey(productInfo.get("length_uom", 0)) %>)
					</span>
				</td>
			</tr>
			<tr class="outterTr">
				<td class="outterLeftTd" valign="top">
					<span class="fieldName">Weight&nbsp;:</span>
				</td>
				<td class="outterRightTd">
					<span class="fieldValue"><%=productInfo.get("weight", 0d) %>&nbsp;(<%=new WeightUOMKey().getWeightUOMKey(productInfo.get("weight_uom", 0)) %>)</span>
				</td>
			</tr>
			<tr class="outterTr">
				<td class="outterLeftTd" valign="top">
					<span class="fieldName">Volume&nbsp;:</span>
				</td>
				<td class="outterRightTd">
					<span class="fieldValue"><%=productInfo.get("volume", 0d) %></span>
				</td>
			</tr>
			<%if(!"".equals(sn_size)){ %>
				<tr class="outterTr">
					<td class="outterLeftTd" valign="top">
						<span class="fieldName">SN Length&nbsp;:</span>
					</td>
					<td class="outterRightTd">
						<span class="fieldValue"><%=sn_size %></span>
					</td>
				</tr>
			<%} %>
			<tr class="outterTr">
				<td class="outterLeftTd" valign="top">
					<span class="fieldName">Is Active&nbsp;:</span>
				</td>
				<td class="outterRightTd">
				<%YesOrNotKey yesOrNotKey = new YesOrNotKey(); %>
					<span class="fieldValue"><%=1==productInfo.get("alive", 0)?yesOrNotKey.getYesOrNotKeyName(YesOrNotKey.YES):yesOrNotKey.getYesOrNotKeyName(YesOrNotKey.NO) %></span>
				</td>
			</tr>
		</table>
	</div>
	<h3>Images</h3>
	<div>
		<div id="product_images">
		
		</div>
	</div>
</div>
  </body>
</html>
