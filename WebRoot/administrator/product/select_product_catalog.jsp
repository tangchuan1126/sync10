<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long catalog_id = StringUtil.getLong(request,"catalog_id");
long currentLevel = StringUtil.getLong(request,"level");
long currentHeight = StringUtil.getLong(request,"height");
DBRow catalog = catalogMgr.getDetailProductCatalogById(catalog_id);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Move Category</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
		<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

	
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script type="text/javascript">
	function moveToTheCatalog(move_to_catalog)
	{
		$("#move_to").val(move_to_catalog);
		if(move_to_catalog!=0){
			height = getCategoryHeight(move_to_catalog);
			lvl = getCategoryLevel(move_to_catalog); 
			cur_height = <%=currentHeight%>+1;
			result = cur_height+lvl;
			//alert(cur_height+" level: "+lvl);
			if(result > 3){
				showMessage("Cannot move to this category. Only 3 levels of categories allowed","alert");
				$("#move_to").val("");
			}
		}
	}
	
	function ajaxMoveCatalog()
	{
		if($("#move_to").val()==""){
			showMessage("Please select a category","alert");
		}else{
			parent.document.move_catalog_form.move_to.value = $("#move_to").val();
			parent.document.move_catalog_form.catalog_id.value = $("#catalog_id").val();
			parent.document.move_catalog_form.submit();
		}
		
	}
	var arr_of_category = new Array();
	var height_of_category = new Array();
	var level_of_category = new Array();
	
	function poplulateCategoryLevel(id,parent_id){

		arr_of_category[id]= parent_id;
		
	}
	function evaluateCategory(id,parent_id){
		return (notExceedingcatagoryLevelLimit(id) && notInSelectedCategoryTree(id,parent_id));
	}
	function notExceedingcatagoryLevelLimit(id){
		height = getCategoryHeight(id);
		lvl = getCategoryLevel(id); 
		cur_height = <%=currentHeight%>+1;
		result = cur_height+lvl;
		if(result > 3){
			return false;
		}else{
			return true;
		}
	}
	function notInSelectedCategoryTree(id,parent_id){
		key = id;
		catalog_id = <%=catalog_id%>;
		if(id==catalog_id){return false;}
		while(arr_of_category[key]){
			if(arr_of_category[key]==catalog_id){
				return false;
			}
			key = arr_of_category[key];
		}
		return true;
	}
	function checkCategoryLevel(id,parent_id){
		level = 1;
		arr_of_category[id]= parent_id;
		key = id;
		while(arr_of_category[key]){
			if(height_of_category[arr_of_category[key]]){
				height_of_category[arr_of_category[key]] = height_of_category[arr_of_category[key]] < getCategoryHeight(key)+1 ? getCategoryHeight(key)+1 : height_of_category[arr_of_category[key]] ;
			}else{
				height_of_category[arr_of_category[key]] = getCategoryHeight(key)+1;
			}
			key=arr_of_category[key];
			level++;
		}
		level_of_category[id]=level;
		return level;
	}
	function getCategoryHeight(id){
		if(height_of_category[id]){
			return height_of_category[id];
		}
		return 0;
	}
	function getCategoryLevel(id){
		if(level_of_category[id]){
			return level_of_category[id];
		}
		return 1;
	}
</script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
</head>

<table width="100%" cellpadding="0" cellspacing="0" height="100%">
  <tr>
  	<td valign="top">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
      <tr> 
        <th colspan="2"  class="left-title">The Category to be Moved：<span style="color:#069;"><%=catalog.getString("title")%></span></th>
      </tr>
    <tr> 
      <td height="60" colspan="2">
      		<script type="text/javascript">
				d = new dTree('d');
				d.add(0,-1,'Product Category Level 1</td></tr></table>','javascript:moveToTheCatalog(0)');
				<%
					DBRow [] parentCategory = catalogMgr.getAllProductCatalog();
					for(int i=0;i<parentCategory.length;i++)
					{
						
				%>
					poplulateCategoryLevel(<%=parentCategory[i].getString("id")%>,<%=parentCategory[i].getString("parentid")%>);
				<%
					}
					
					for(int i=0;i<parentCategory.length;i++)
					{
						
				%>	
					checkCategoryLevel(<%=parentCategory[i].getString("id")%>,<%=parentCategory[i].getString("parentid")%>);
					if(evaluateCategory(<%=parentCategory[i].getString("id")%>,<%=parentCategory[i].getString("parentid")%>)){
						
						d.add(<%=parentCategory[i].getString("id")%>,<%=parentCategory[i].getString("parentid")%>,'<%=parentCategory[i].getString("title")%></td></tr></table>','javascript:moveToTheCatalog(<%=parentCategory[i].getString("id")%>)');
					}
					
				<%
					}
				%>
				d.closeAll();
				document.write(d);
			</script>
      </td>
    </tr>
</table>
	</td>
  </tr>
 <tr>
 	<td valign="bottom" style="border-top-width:0px;">
 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="left" valign="middle" class="win-bottom-line">
					&nbsp;
				</td>
 				<td align="right" valign="middle" class="win-bottom-line"> 
			      <input type="button" name="Submit2" value="OK" class="normal-green" onClick="ajaxMoveCatalog()"/>
				  <input type="button" name="Submit2" value="Cancel" class="normal-white" onClick="$.artDialog.close();"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
</table>
<input type="hidden" name="move_to" id="move_to"/>
<input type="hidden" name="catalog_id" id="catalog_id" value="<%=catalog_id%>"/>	
</body>
</html>
