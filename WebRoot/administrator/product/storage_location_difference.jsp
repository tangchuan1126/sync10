<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../include.jsp"%> 
<%
 	long cid = StringUtil.getLong(request,"cid");
 long psid = StringUtil.getLong(request,"psid");
 String storage_name = StringUtil.getString(request,"storage_name");
 String backurl = StringUtil.getString(request,"backurl");

 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

 if (psid==0)
 {
 	psid = adminLoggerBean.getPs_id();
 }

 PageCtrl pc = new PageCtrl();
 pc.setPageNo(StringUtil.getInt(request,"p"));
 pc.setPageSize(30);

 DBRow storageLocations[] ;
 storageLocations = productMgr.getIncorrectStorageByCidPsid( cid, psid, pc);
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
function upload()
{
	if (document.upload_form.file.value=="")
	{
		alert("请选择上传的商品定位信息");
	}
	else if ( checkFileName(document.upload_form.file.value) )
	{
		alert("文件名不能包含中文");
		
	}
	else
	{
		document.upload_form.submit();
	}
}

function includeChinese(str) 
{
	var rxp=/^[\u4e00-\u9fa5]+$/g;

	return rxp.test(str);
}

function checkFileName(name)
{
	var a = name.split("\\");
	var aa = a[a.length-1].split(".");
	
	return(includeChinese(aa[0]));
}

function checkForm(theForm)
{
	if (theForm.psid.value==0)
	{
		alert("请选择对比仓库");
		return(false);
	}
	else
	{
		return(true);
	}
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/commmitStorageLocalDifference.action" method="post"  name="difference_form" >
	<input type="hidden" name="cid" value="<%=cid%>">
	<input type="hidden" name="psid" value="<%=psid%>">
	<input type="hidden" name="backurl" value="<%=backurl%>">
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 » 商品库存差异</td>
  </tr>
</table>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle" style="font-size:15px;font-weight:bold"><%=storage_name%> Warehouse 库存差异</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th style="vertical-align: center;text-align: center;" class="left-title">仓库</th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;">商品条码</th>
        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title" >实际库存</th>
        <th width="12%" style="vertical-align: center;text-align: center;" class="right-title" >系统库存</th>
        <th width="26%" style="vertical-align: center;text-align: center;" class="right-title">存放位置</th>
    </tr>

<%
for (int i=0; i<storageLocations.length; i++)
{
%>

    <tr > 
      <td   width="15%" height="60" align="center" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%=storageLocations[i].getString("title")%></td>
      <td   width="37%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=storageLocations[i].getString("barcode")%></td>
      <td   align="center" valign="middle" >
	  <%
	  if (storageLocations[i].getString("quantity").equals(""))
	  {
	  	out.println("<font color=red>无</font>");
	  }
	  else
	  {
	  	out.println(storageLocations[i].getString("quantity"));
	  }
	  %>
	  
	  &nbsp;</td>
      <td   align="center" valign="middle" ><%=storageLocations[i].getString("merge_count")%>&nbsp;</td>
      <td   align="center" valign="middle" >
	  <%
	  DBRow locations[] = productMgr.getStorageLocationByBarcode(psid,storageLocations[i].get("pc_id",0l),null);
	  for (int j=0; j<locations.length; j++)
	  {
	  %>
			<div style="padding:3px;color:#CC0000;font-weight:bold;font-size:13px;"><%=locations[j].getString("position")%> : <spab style="color:#0000FF"><%=locations[j].get("quantity",0f)%></span></div>

	  <%
	  }
	  %>
&nbsp;      </td>
    </tr>

<%
}
%>

<%
if (storageLocations.length==0)
{
%>
    <tr >
      <td height="60" colspan="5" align="center" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >没有差异库存</td>
    </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p">
<input type="hidden" name="storage_name" value="<%=storage_name%>">
<input type="hidden" name="cid" value="<%=cid%>">
<input type="hidden" name="psid" value="<%=psid%>">
  </form>
        <tr> 
          
    <td width="50%" height="28" align="right" valign="middle">
      <input name="Submit" type="submit" class="long-button" value="提交记录" onClick="document.difference_form.submit();">
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <input name="Submit2" type="button" class="long-button" value="返回" onClick="history.back();">    </td>
        <td width="50%" align="right" valign="middle">
		 <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">   
		</td>
        </tr>
</table> 
<br>
</body>
</html>
