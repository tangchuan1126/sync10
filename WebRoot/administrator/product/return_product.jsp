<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long oid = StringUtil.getLong(request,"oid");
	
	long pid = StringUtil.getLong(request,"pid");
	
	DBRow productStore = productMgr.getDetailProductStorageByPid(pid);
	
	DBRow product = productMgr.getDetailProductByPcid(productStore.get("pc_id",0l));
	
	int quantity = StringUtil.getInt(request,"quantity");
%>
<html>
  <head>
    <title>退货</title>
    <link href="../comm.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
    
    <script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css"/>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
	<script type="text/javascript">
		function click(union)
		{
			$("#add_button").attr("onclick","addReturn('"+union+"')");
		}
	</script>
  </head>
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
   	<table width="100%" height="100%" cellpadding="0" cellspacing="0">
   		<tr>
   			<td>
   				<fieldset style="width:96%;border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<legend style="font-size:15px;font-weight:normal;color:#999999;">主商品信息</legend>
						<table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
						  <tr>
						    <td width="69%" style="font-family:Arial;font-weight:bold"><%=product.getString("p_name")%></td>
						    <td width="31%" style="font-family:Arial;font-weight:bold"><%=quantity%> <%=product.getString("unit_name")%> </td>
						  </tr>
						</table>
			 	</fieldset>	
   			</td>
   		</tr>
   		<tr>
   			<td valign="top">
   				<br/>
	   			<div class="demo">
	   				<div id="tabs">
		  	            <ul>
		  	              	<li><a onclick="click('union')" href="return_product_tright.html?pid=<%=pid%>&type=1&quantity=<%=quantity%>&oid=<%=oid%>">原样退货<span> </span></a></li>
							<%
								if(product.get("union_flag",0)==1)
								{
							%>
							<li><a onclick="click('noral')" href="return_product_tright.html?pid=<%=pid%>&type=0&quantity=<%=quantity%>&oid=<%=oid%>">散件退货<span> </span></a></li>
							<%
								}
							%>
							
					    </ul>
				  	</div>
			    </div>
			    
			    <script>
					$("#tabs").tabs({
						cache: false,
						spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
						
						load: function(event, ui) 
						{
							if(ui.index==0)
							{
								click("union");
							}
							else if(ui.index==1)
							{
								click("noral");
							}
						}	
					});
				</script>
   			</td>
   		</tr>
   		<tr>
			<td align="right" valign="bottom">				
				<table width="100%">
					<tr>
						<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
						  	<input id="add_button" type="button" name="Submit2" value="增加" class="normal-green" onClick="addReturn('union')">
							<input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close()">
						</td>
					</tr>
				</table>
			</td>
		</tr>
   	</table>
   	
   	<script type="text/javascript">
		
		function closeRef()
		{
			parent.window.location.reload();
			$.artDialog.close();
		}
	</script>
  </body>
</html>
