<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long pcid = StringUtil.getLong(request,"pcid");
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
int union_flag = StringUtil.getInt(request,"union_flag");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);


DBRow rows[];

if (cmd.equals("filter"))
{
	rows = productMgr.getProductByPcid(pcid,union_flag,pc);
}
else if (cmd.equals("search"))
{
	//rows = productMgr.getSearchProducts4CT(key,pc);
	rows = productMgr.getSearchProducts4CT(key,pcid,pc);
	//System.out.println(pcid);
}
else
{
	rows = productMgr.getAllProducts(pc);	
}

Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

		
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript">
<!--
$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#search_key").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	$("#search_key").keydown(function(event){
		if (event.keyCode==13)
		{
			search();
		}
	});

});

function checkForm(theForm)
{
	if (theForm.catalog_id.value==0)
	{
		alert("请选择商品分类");
		return(false);
	}
	else if (theForm.p_name.value=="")
	{
		alert("请填写商品名称");
		return(false);
	}
	else if (theForm.p_code.value=="")
	{
		alert("请填写条码");
		return(false);
	}
	else
	{
		return(true);
	}
}

function filter()
{
		document.filter_search_form.cmd.value = "filter";		
		document.filter_search_form.pcid.value = $("#filter_pcid").val();	
		document.filter_search_form.union_flag.value = $("input#union_flag:checked").val();		
		document.filter_search_form.submit();
}

function search()
{
	if ($("#search_key").val()=="")
	{
		alert("请填写关键字");
		return(false);
	}
	else
	{
		document.filter_search_form.cmd.value = "search";		
		document.filter_search_form.key.value = $("#search_key").val();		
		document.filter_search_form.pcid.value = $("#filter_pcid").val();	
		document.filter_search_form.submit();
	}
}

function modProductCode(pcid)
{

<%
StringBuffer countryCodeSB = new StringBuffer("");
DBRow treeRows[] = catalogMgr.getProductCatalogTree();
String qx;

countryCodeSB.append("<select name='proTextCatalogId' id='proTextCatalogId' >");
countryCodeSB.append("<option  value='0'>选择商品分类</option>");
for (int i=0; i<treeRows.length; i++)
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
	 
	countryCodeSB.append("<option  value='"+treeRows[i].getString("id")+"'>"+Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))+qx+treeRows[i].getString("title")+"</option>");
}
	countryCodeSB.append("</select>");
%>


	$.prompt(
	
	"<div id='title'>修改商品</div><br />商品分类<br><%=countryCodeSB.toString()%><br>商品名称<br><input name='proTextProductName' type='text' id='proTextProductName' style='width:300px;'><br>商品条码<br><input name='proTextCodeName' type='text' id='proTextCodeName' style='width:300px;'><br>单位<br><input name='proTextUnitName' type='text' id='proTextUnitName' style='width:100px;'><br>单价<br><input name='proTextUnitPrice' type='text' id='proTextUnitPrice' style='width:100px;'><br>毛利率<br><input name='proTextGrossProfit' type='text' id='proTextGrossProfit' disabled='disabled' style='width:100px;'>格式：0.00<br>重量<br><input name='proTextWeight' type='text' id='proTextWeight' style='width:100px;'>Kg<br>体积<br><input name='proTextVolume' type='text' id='proTextVolume' style='width:100px;'>m³",
	
	{
	      submit: 
		  		function (v,m,f)
				{
					if (v=="y")
					{
						 if(f.proTextCatalogId == 0)
						 {
								alert("请选择商品分类");
								return false;
						 }
						 else if (f.proTextProductName == 0)
						 {
								alert("请填写商品名称");
								return false;
						 }
						 else if (f.proTextCodeName == 0)
						 {
								alert("请填写商品条码");
								return false;
						 }
						 else if (f.proTextUnitName == 0)
						 {
								alert("请填写单位");
								return false;
						 }
						 else if (f.proTextUnitPrice == 0)
						 {
								alert("请填写单价");
								return false;
						 }
						 else if (f.proTextGrossProfit == 0)
						 {
								alert("请填写毛利率");
								return false;
						 }
						 else if (f.proTextWeight == 0)
						 {
								alert("请填写重量");
								return false;
						 }
						 else if(f.proTextVolume == 0)
						 {
						 		alert("请填写体积");
								return false;
						 }
						 						 
						 return true;
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductJSON.action",
							{pcid:pcid},//{name:"test",age:20},
							function callback(data)
							{							
								$("#proTextCatalogId").setSelectedValue(data.catalog_id);
								$("#proTextProductName").val(data.p_name);
								$("#proTextCodeName").val(data.p_code);
								
								$("#proTextUnitName").val(data.unit_name);
								$("#proTextUnitPrice").val(data.unit_price);
								$("#proTextGrossProfit").val(data.gross_profit);
								$("#proTextWeight").val(data.weight);
								$("#proTextVolume").val(data.volume);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProduct.action";
						document.mod_form.pc_id.value = pcid;
						document.mod_form.catalog_id.value = f.proTextCatalogId;		
						document.mod_form.p_name.value = f.proTextProductName;
						document.mod_form.p_code.value = f.proTextCodeName;
						
						document.mod_form.unit_name.value = f.proTextUnitName;
						document.mod_form.unit_price.value = f.proTextUnitPrice;
						document.mod_form.gross_profit.value = f.proTextGrossProfit;
						document.mod_form.weight.value = f.proTextWeight;
						document.mod_form.volume.value = f.proTextVolume;
						
						document.mod_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function addProductCode()
{

	$.prompt(
	
	"<div id='title'>增加商品</div><br />商品分类<br><%=countryCodeSB.toString()%><br>商品名称<br><input name='proTextProductName' type='text' id='proTextProductName' style='width:300px;'><br>商品条码<br><input name='proTextCodeName' type='text' id='proTextCodeName' style='width:300px;'><br>单位<br><input name='proTextUnitName' type='text' id='proTextUnitName' style='width:100px;'><br>单价<br><input name='proTextUnitPrice' type='text' id='proTextUnitPrice' style='width:100px;'><br>毛利率<br><input name='proTextGrossProfit' type='text' id='proTextGrossProfit' style='width:100px;'>格式：0.00<br>重量<br><input name='proTextWeight' type='text' id='proTextWeight' style='width:100px;'>Kg<br>体积<br><input name='proTextVolume' type='text' id='proTextVolume' style='width:100px;'>m³",
	
	{
	      submit: 
		  		function (v,m,f)
				{
					if (v=="y")
					{
						 if(f.proTextCatalogId == 0)
						 {
								alert("请选择商品分类");
								return false;
						 }
						 else if (f.proTextProductName == 0)
						 {
								alert("请填写商品名称");
								return false;
						 }
						 else if (f.proTextCodeName == 0)
						 {
								alert("请填写商品条码");
								return false;
						 }
						 else if (f.proTextUnitName == 0)
						 {
								alert("请填写单位");
								return false;
						 }
						 else if (f.proTextUnitPrice == 0)
						 {
								alert("请填写单价");
								return false;
						 }
						 else if (f.proTextGrossProfit == 0)
						 {
								alert("请填写毛利率");
								return false;
						 }
						 else if (f.proTextWeight == 0)
						 {
								alert("请填写重量");
								return false;
						 }
						 else if(f.proTextVolume == 0)
						 {
						 		alert("请填写体积");
								return false;
						 }	 
						 return true;
					}
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProduct.action";
						document.mod_form.catalog_id.value = f.proTextCatalogId;		
						document.mod_form.p_name.value = f.proTextProductName;
						document.mod_form.p_code.value = f.proTextCodeName;
						
						document.mod_form.unit_name.value = f.proTextUnitName;
						document.mod_form.unit_price.value = f.proTextUnitPrice;
						document.mod_form.gross_profit.value = f.proTextGrossProfit;
						document.mod_form.weight.value = f.proTextWeight;
						document.mod_form.volume.value = f.proTextVolume;
											
						document.mod_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function del(name,pcid)
{
	if (confirm("确定删除 "+name+"？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/delProduct.action";
		document.del_form.pcid.value = pcid;
		document.del_form.submit();	
	}
}

function swichProductAlive(name,pc_id,curAlive)
{
	var msg;
	
	if (curAlive==1)
	{
		msg = "确定禁止 "+name+" 抄单？";
	}
	else
	{
		msg = "确定允许 "+name+" 抄单？";
	}

	if (confirm(msg))
	{
		document.switch_alive_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/swichProductAlive.action";
		document.switch_alive_form.pc_id.value = pc_id;
		document.switch_alive_form.curAlive.value = curAlive;
		document.switch_alive_form.submit();	
	}
}



function openAddUnionWin(set_pid)
{	
	tb_show('创建组合套装','add_union.html?set_pid='+set_pid+'&TB_iframe=true&height=300&width=650',false);
}

function openModUnionWin(set_pid,pid)
{	
	tb_show('修改组合套装','mod_union.html?set_pid='+set_pid+'&pid='+pid+'&TB_iframe=true&height=300&width=650',false);
}



function AddUnion(p_name,set_pid,quantity)
{
	document.add_union_form.set_pid.value = set_pid;
	document.add_union_form.quantity.value = quantity;
	document.add_union_form.p_name.value = p_name;
	document.add_union_form.submit();
}

function ModUnion(p_name,set_pid,pid,quantity)
{
	document.mod_union_form.pid.value = pid;
	document.mod_union_form.set_pid.value = set_pid;
	document.mod_union_form.quantity.value = quantity;
	document.mod_union_form.p_name.value = p_name;
	document.mod_union_form.submit();
}

function delProductUnion(set_pid,pid,name)
{
	if ( confirm("确认删除组合中的 "+name+" ？") )
	{
		document.del_union_form.set_pid.value = set_pid;
		document.del_union_form.pid.value = pid;
		document.del_union_form.submit();
	}
}

function switchCheckboxAction()
{
	$('input[name=switchCheckboxPid]').inverse();
}

function getAllChecked()
{
	return($('#switchCheckboxPid').checkbox());
}

function batchMod()
{
	var pids = getAllChecked();
	
	if ( pids=="" )
	{
		alert("最少选择一个商品");
	}
	else
	{
		tb_show('批量修改商品','batch_mod_products.html?pids='+pids+'&height=500&width=800',false);
	}
	
}

function addProduct()
{
	var id = $("#filter_pcid").val();
	if (id==0)
	{
		alert("请选择一个商品分类");
		openCatalogMenu();
	}
	else
	{
		tb_show('增加商品','add_product.html?catalog_id='+id+'&TB_iframe=true&height=350&width=700',false);
	}
}

function closeWin()
{
	tb_remove();
}

function openCatalogMenu()
{
	$("#category").mcDropdown("#categorymenu").openMenu();
}

function showProductImg(img)
{
	if(img != "")
	{
		tb_show('',img+'?TB_iframe=true&height=350&width=700',false);
	}
	else
	{
		alert("该商品暂时没有图片");
	}
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 商品管理 »   商品管理</td>
  </tr>
</table>
<br>
 <form name="add_union_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProductUnion.action" >
 <input type="hidden" name="set_pid">
 <input type="hidden" name="quantity">
 <input type="hidden" name="p_name">
 </form>
 
 <form name="mod_union_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductUnion.action" >
<input type="hidden" name="set_pid">
<input type="hidden" name="pid">
<input type="hidden" name="quantity">
<input type="hidden" name="p_name">
</form>

<form name="del_union_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/delProductUnion.action" >
<input type="hidden" name="set_pid">
<input type="hidden" name="pid">
</form>
 
 
<form name="del_form" method="post">
<input type="hidden" name="pcid">
</form>

<form name="mod_form" method="post">
<input type="hidden" name="pc_id">
<input type="hidden" name="catalog_id">
<input type="hidden" name="p_name" >
<input type="hidden" name="p_code">

<input type="hidden" name="unit_name">
<input type="hidden" name="unit_price">
<input type="hidden" name="gross_profit">
<input type="hidden" name="weight">
<input type="hidden" name="volume">
</form>


<form name="filter_search_form" method="get"  action="ct_product.html">
<input type="hidden" name="cmd" >
<input type="hidden" name="pcid" >
<input type="hidden" name="key" >
<input type="hidden" name="union_flag" >

</form>

<form name="switch_alive_form" method="post">
<input type="hidden" name="pc_id">
<input type="hidden" name="curAlive">
</form>

<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="65%">
	
	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
	
	<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td width="423" rowspan="2" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
	<div style="padding-bottom:5px;color:#666666;font-size:12px;">
	可以选择一个商品分类：
	</div>

	<ul id="categorymenu" class="mcdropdown_menu">
	  <li rel="0">所有分类</li>
	  <%
	  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
	  for (int i=0; i<c1.length; i++)
	  {
			out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));

			  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
			  if (c2.length>0)
			  {
			  		out.println("<ul>");	
			  }
			  for (int ii=0; ii<c2.length; ii++)
			  {
					out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
					
						DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
						  if (c3.length>0)
						  {
								out.println("<ul>");	
						  }
							for (int iii=0; iii<c3.length; iii++)
							{
									out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
									out.println("</li>");
							}
						  if (c3.length>0)
						  {
								out.println("</ul>");	
						  }
						  
					out.println("</li>");				
			  }
			  if (c2.length>0)
			  {
			  		out.println("</ul>");	
			  }
			  
			out.println("</li>");
	  }
	  %>
</ul>
	  <input type="text" name="category" id="category" value="" />
	  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />
	  

<script>
$("#category").mcDropdown("#categorymenu",{
		allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}

});
<%
if (pcid>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=pcid%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 


</script>	</td>
    <td width="434" height="29" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid"><input name="union_flag" type="radio" id="union_flag" value="0" <%=union_flag==0?"checked":""%>>
      全部
	  
	   <input type="radio" name="union_flag" id="union_flag" value="1" <%=union_flag==1?"checked":""%>>
	   拼
	   装
      
	  
	        &nbsp;&nbsp;
            <input name="Submit2" type="button" class="button_long_refresh" value="过滤" onClick="filter()">&nbsp;&nbsp;&nbsp;&nbsp;<input name="import/export" type="button" class="long-button" value="商品导入/导出" onClick="window.location.href='ct_product_import_or_export.html'"></td>
  </tr>
  <tr>
    <td height="29" bgcolor="#eeeeee">
	<input type="text" name="search_key" id="search_key" value="<%=key%>" style="width:200px;" >
           &nbsp;&nbsp;&nbsp;
<input name="Submit4" type="button" class="button_long_search" value="搜索" onClick="search()"> &nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" value="增加商品" onClick="addProduct()">	</td>
  </tr>
</table>
	
	</div>
	
	
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
  <tr>
    <td><input name="Submit" type="button" class="long-button-mod" value="批量修改" onClick="batchMod()"></td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form name="listForm" method="post">

	
    <tr> 
        <th width="36" align="center" valign="middle"  class="left-title"><input type="checkbox" name="switchCheckbox" id="switchCheckbox" value="checkbox" onClick="switchCheckboxAction()"></th>
        <th width="297"  class="left-title">名称</th>
        <th width="203"  class="right-title" style="vertical-align: center;text-align: center;">条码</th>
        <th width="176" align="left"  class="right-title" style="vertical-align: center;text-align: left;">类别</th>
        <th width="139" align="center"  class="right-title" style="vertical-align: center;text-align: center;">单位</th>
        <th width="122"  class="right-title" style="vertical-align: center;text-align: center;">重量(Kg)</th>
        <th width="142"  class="right-title" style="vertical-align: center;text-align: center;">单价</th>
        <th width="134"  class="right-title" style="vertical-align: center;text-align: center;">体积重</th>
        <th width="165"  class="right-title" style="vertical-align: center;text-align: center;">毛利率%</th>
        <th width="165"  class="right-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
    </tr>

    <%
for ( int i=0; i<rows.length; i++ )
{

%>
    <tr  > 
      <td height="60" align="center" valign="middle"   ><input type="checkbox" id="switchCheckboxPid" name="switchCheckboxPid" value="<%=rows[i].get("pc_id",0l)%>">      </td>
      <td height="80" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >

		
	   <%
	  if (rows[i].get("union_flag",0)==1)
	  {
	  %>
            <fieldset style="border:1px #cccccc solid;padding:7px;width:90%;background:#FFFFFF;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:13px;font-weight:bold;color:#000000;">
<%
	  if (rows[i].get("alive",0)==0)
	  {
			out.println("<img src='../imgs/standard_msg_error.gif' width='16' height='16' align='absmiddle'>");
		}
		%>

<%=rows[i].getString("p_name")%> <a href='javascript:openAddUnionWin(<%=rows[i].get("pc_id",0l)%>);'><img src='../imgs/addp.gif' border='0' align='absmiddle'></a></legend>
<%
DBRow inSetProducts[] = productMgr.getProductsInSetBySetPid(rows[i].get("pc_id",0l));
for (int isp_i=0; isp_i<inSetProducts.length; isp_i++)
{
%>
 <table width="100%" border="0" cellspacing="0" cellpadding="3">
   <tr>
     <th width="74%" style="font-size:12px;font-weight:normal;vertical-align: center;text-align: left;height:30px;border-bottom:1px #eeeeee solid;padding-left:5px;"><%=inSetProducts[isp_i].getString("p_name")%> x <span style='color:#990000'><%=inSetProducts[isp_i].getString("quantity")%> <%=inSetProducts[isp_i].getString("unit_name")%></span></th>
     <th width="26%" style="vertical-align: center;text-align: center;border-bottom:1px #eeeeee solid;padding-left:5px;" >
	 <a href="javascript:openModUnionWin(<%=inSetProducts[isp_i].getString("set_pid")%>,<%=inSetProducts[isp_i].getString("pid")%>);"><img src="../imgs/modp.gif" width="16" height="16" border="0" align="absmiddle"></a>
	 &nbsp;
	 <a href="javascript:delProductUnion(<%=inSetProducts[isp_i].getString("set_pid")%>,<%=inSetProducts[isp_i].getString("pid")%>,'<%=inSetProducts[isp_i].getString("p_name")%>');"><img src="../imgs/delp.gif" width="16" height="16" border="0" align="absmiddle"></a>	 </th>
   </tr>
 </table>
<%
}
%>
            </fieldset>

	  <%
	  }
	  else
	  {
	  	  if (rows[i].get("alive",0)==0)
	  {
			out.println("<img src='../imgs/standard_msg_error.gif' width='16' height='16' align='absmiddle'>");
		}
	  	//不能在套装下的商品添加商品
		if (productMgr.getProductUnionsByPid(rows[i].get("pc_id",0l)).length>0)
		{
		  	out.println("<span style='font-size:14px;'>"+rows[i].getString("p_name")+"</span>");
		}
		else
		{
		  	out.println("<span style='font-size:14px;'>"+rows[i].getString("p_name")+"</span> <a href='javascript:openAddUnionWin("+rows[i].get("pc_id",0l)+");'><img src='../imgs/addp.gif' border='0'  align='absmiddle'></a>");
		}
	  }
	  %>	  </td>
      <td align="center" valign="middle" style='word-break:break-all;' >      	
      	<%=rows[i].getString("p_code")%>
      	</td>
      <td align="left" valign="middle" style="line-height:20px;">
	  <span style="color:#999999">
	  <%
	  DBRow allFather[] = tree.getAllFather(rows[i].get("catalog_id",0l));
	  for (int jj=0; jj<allFather.length-1; jj++)
	  {
	  	out.println("<a class='nine4' href='?cmd=filter&pcid="+allFather[jj].getString("id")+"&key=&union_flag=0'>"+allFather[jj].getString("title")+"</a><br>");
	
	  }
	  %>
	  </span>
	  
	  <%
	  DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
	  if (catalog!=null)
	  {
	  	out.println("<a href='?cmd=filter&pcid="+rows[i].getString("catalog_id")+"&key=&union_flag=0'>"+catalog.getString("title")+"</a>");
	  }
	  %>	  </td>
      <td align="center" valign="middle"  ><%=rows[i].getString("unit_name")%></td>
      <td align="center" valign="middle"   ><%=rows[i].get("weight",0f)%></td>
      <td align="center" valign="middle"   ><%=rows[i].get("unit_price",0d)%></td>
      <td align="center" valign="middle"  ><%=rows[i].get("volume",0f)%></td>
      <td align="center" valign="middle"  ><%=MoneyUtil.round(MoneyUtil.mul(rows[i].get("gross_profit",0d),100d),2)%>%</td>
      <td align="center" valign="middle" nowrap="nowrap"><%
	  if (rows[i].get("alive",0)==1)
	  {
	  %>
        <input name="Submit43" type="button" class="short-short-button-cancel" value="禁抄" onClick="swichProductAlive('<%=rows[i].getString("p_name")%>',<%=rows[i].getString("pc_id")%>,<%=rows[i].getString("alive")%>)">
        <%
		}
		else
		{
		%>
        <input name="Submit43" type="button" class="short-short-button-ok" value="可抄" onClick="swichProductAlive('<%=rows[i].getString("p_name")%>',<%=rows[i].getString("pc_id")%>,<%=rows[i].getString("alive")%>)">
        <%
		}		
		%>&nbsp;&nbsp;
		<input type="button" class="short-short-button" onClick="showProductImg('<%=rows[i].getString("p_img")%>')" value="图片"> 
        <br>
        <br>
        <input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="modProductCode(<%=rows[i].getString("pc_id")%>)">
&nbsp;&nbsp;
<input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="del('<%=rows[i].getString("p_name")%>',<%=rows[i].getString("pc_id")%>)"></td>
    </tr>
    <%
}
%>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="ct_product.html">
    <input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="pcid" value="<%=pcid%>">
<input type="hidden" name="key" value="<%=key%>">
<input type="hidden" name="union_flag" value="<%=union_flag%>">

  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
