<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%
	int storage_type = StringUtil.getInt(request,"storage_type");
	int p = StringUtil.getInt(request,"p");
 
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(p);
	pc.setPageSize(20);
	DBRow[] rows;
	rows = storageCatalogMgrZyj.getProductStorageCatalogByType(storage_type,pc);
	
%>
<html>
 
<head>
<title>仓库类型详细列表</title>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}

a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body>
 <form name="dataForm" method="post" id="dataForm">
　<input type="hidden" name="p" id="p">
 </form>
 <script type="text/javascript">
	function go(num){
		$("#p").val(num);
		showStorageCatalogTag('<%= storage_type%>',num);}
</script>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
	<tr>
		<td colspan="7" style="text-align:right;">
		<input name="import/import" type="button" class="long-button-upload" value="Import" onClick="importStorageAddress('jquery_file_up',<%=storage_type %>);">&nbsp;
	　　<input name="import/export" type="button" class="long-button-export" value="Export" onClick="exportStorageAddress(<%=storage_type %>);">&nbsp;
		<div id="jquery_file_up">
			              			<input type="hidden" id="file_names" name="file_names" value=""/>
			            	</div>
		</td>
	</tr>
			      <tr> 
			        <th width="16%" class="left-title"  style="vertical-align: center;text-align: center;" align="center">StorageInfo</th>
			        <th width="21%" style="vertical-align: center;text-align: center;" class="left-title" align="center">ReceiveAddress</th>
			        <th width="21%" style="vertical-align: center;text-align: center;" class="left-title" align="center">SendAddress</th>
			        <th width="26%" style="vertical-align: center;text-align: center;" class="left-title" align="center">PriorityShipping</th> 
			        <th width="9%" style="vertical-align: center;text-align: center;" class="right-title" align="center">StorageFeatures</th>
<!-- 			        <th width="4%" style="vertical-align: center;text-align: center;" class="right-title" align="center">ProductQty</th> -->
			        <th width="5%" style="vertical-align: center;text-align: center;" class="right-title" align="center">Operation</th>
			      </tr>
  	 <%
			    	if(null != rows && rows.length > 0){
			    		for ( int i=0; i<rows.length; i++ ){
 				%>
			    <tr > 
			      <td   width="13%" height="60" valign="middle">
			      		<%=rows[i].getString("id")%><br/>
				      	<%
							out.println("<span style='font-size:14px;font-weight:bold'>"+rows[i].getString("title")+"</span>");
						%><br/>
			      		<%=  orderMgr.getDetailCountryCodeByCcid(rows[i].get("native",0l)).getString("c_country")%><br/>
						<%
							if(0 != rows[i].get("pro_id",0) && -1 != rows[i].get("pro_id",0)){
						%>	
								<%=storageCatalogMgrZyj.getProviceInfoById(rows[i].get("pro_id",0)).getString("pro_name")  %>
						<%		
							}else{
						%>	
								<%=rows[i].getString("pro_input")  %>
						<%	
							}
						%>
					<br/>
					<a href="javascript:modCountryProvince(<%=rows[i].get("id",0l)%>)"><img src="../imgs/application_edit.png" width="14" height="14" border="0"></a>
<!-- 				  	<br/> -->
<%-- 				  	<input type="button" class="long-button-mod" name="submit12" value="  AdjustArea" onclick="storageAreaAdjust(<%=rows[i].get("id",0l)%>);" > --%>
				  	<br/>
				  	<input type="button" class="long-button-mod" name="submit13" value="Map File" onclick="storageAreaImport(<%=rows[i].get("id",0l)%>);" >
				  </td>
			      <td   width="21%" height="60" valign="middle" style="line-height:20px;">
			      	<fieldset class="set" style="border-color:green;">
			      		<legend>
					      	<%
								out.println("<span style='font-size:14px;font-weight:bold'>"+rows[i].getString("title")+"</span>");
							%>	  
						</legend>
						<%=rows[i].getString("deliver_house_number")  %><br/>
						<%=rows[i].getString("deliver_street")  %><br/>
						<%=rows[i].getString("deliver_city")  %><br/>
						<%=rows[i].getString("deliver_zip_code")  %><br/>
						<%=rows[i].getString("deliver_contact")  %><br/>
						<%=rows[i].getString("deliver_phone")  %><br/>
						<%
							if(-1 != rows[i].get("deliver_pro_id",0) && 0 != rows[i].get("deliver_pro_id",0)){
								if(null == storageCatalogMgrZyj.getProviceInfoById(rows[i].get("deliver_pro_id",0))){
						%>
									<br/>
						<%						
								}else{
						%>
								<%=storageCatalogMgrZyj.getProviceInfoById(rows[i].get("deliver_pro_id",0)).getString("pro_name")  %><br/>	
						<%
								}
							}else{
						%>		
							<%=rows[i].getString("deliver_pro_input")  %><br/>
						<%		
							}
						%>
						<%
							if(0 == rows[i].get("deliver_nation",0l) || null == orderMgr.getDetailCountryCodeByCcid(rows[i].get("deliver_nation",0l))){
						%>		
							<br/>	
						<%		
							}else{
						%>		
								<%=orderMgr.getDetailCountryCodeByCcid(rows[i].get("deliver_nation",0l)).getString("c_country")%><br/>
						<%		
							}
						%>
			      	</fieldset>
				   	<a href="javascript:modStorageDetailDeliver(<%=rows[i].get("id",0l)%>)"><img src="../imgs/application_edit.png" width="14" height="14" border="0"></a>
				  </td>
				   <td   width="21%" height="60" valign="middle" style="line-height:20px;">
				   	 <fieldset class="set"  style="border-color:green;">
			      		<legend>
							<%
								out.println("<span style='font-size:14px;font-weight:bold'>"+rows[i].getString("title")+"</span>");
							%>
						</legend>
						<%=rows[i].getString("send_house_number")  %><br/>
						<%=rows[i].getString("send_street")  %><br/>
						<%=rows[i].getString("send_city")  %><br/>
						<%=rows[i].getString("send_zip_code")  %><br/>
						<%=rows[i].getString("send_contact")  %><br/>
						<%=rows[i].getString("send_phone")  %><br/>
						<%		
							if(-1 != rows[i].get("send_pro_id",0) && 0 != rows[i].get("send_pro_id",0)){
								if(null == storageCatalogMgrZyj.getProviceInfoById(rows[i].get("send_pro_id",0))){
						%>			
								<br/>		
						<%
								}else{
						%>		
								<%=storageCatalogMgrZyj.getProviceInfoById(rows[i].get("send_pro_id",0)).getString("pro_name")  %><br/>
						<%	
								}
							}else{
						%>		
							<%=rows[i].getString("send_pro_input")  %><br/>
						<%		
							}
						%>
						<%
							if(0 == rows[i].get("send_nation",0l)){
						%>
								<br/>
						<%		
							}else{
								if(null == orderMgr.getDetailCountryCodeByCcid(rows[i].get("send_nation",0l))){
						%>
								<br/>
						<%			
								}else{
						%>	
								<%=orderMgr.getDetailCountryCodeByCcid(rows[i].get("send_nation",0l)).getString("c_country")%><br/>
						<%
								}
							}
						%>
					 </fieldset>
					 <a href="javascript:modStorageDetailSend(<%=rows[i].get("id",0l)%>)"><img src="../imgs/application_edit.png" width="14" height="14" border="0"></a>
					 
				  </td>
			      <td width="17%" height="60" align="center" valign="middle"><table width="96%" border="0" cellspacing="0" cellpadding="0">
			          	<table>
							<tr>
								<td align="right">International:</td>
								<td><%DBRow countrysSelf[] = productMgr.getStorageCountryBySid(rows[i].get("id",0l)); %>
									<strong><%=countrysSelf.length%></strong>
								</td>
								<td>Nation</td>
								<td><input name="Submit3" type="button" class="short-short-button" value="SetUp" onClick="setCountry('<%=rows[i].getString("title")%>',<%=rows[i].get("id",0l)%>);"></td>
							</tr>
							<tr>
								<td align="right">internal:</td>
								<td><%DBRow provincesSelf[] = productMgr.getStorageProvinceBySid(rows[i].get("id",0l));%>
									<strong><%=provincesSelf.length%></strong>
								</td>
								<td>Area</td>
								<td><input name="Submit3" type="button" class="short-short-button" value="SetUp" onClick="setProvince('<%=rows[i].getString("title")%>',<%=rows[i].get("id",0l)%>);"></td>
							</tr>
						</table>
			      </td>
			      <td   width="12%" align="center" valign="middle" style="padding-top:10px;padding-bottom:10px;"><table width="72%" border="0" cellspacing="0" cellpadding="0">
			        <tr>
			          <td width="28%"><input type="checkbox" name="dev<%=rows[i].getString("id")%>" id="dev<%=rows[i].getString("id")%>" value="<%=rows[i].getString("id")%>" <%=rows[i].get("dev_flag",0)==1?"checked":""%> onClick="<%=rows[i].get("dev_flag",0)==1?"unMarkDevFlag":"markDevFlag"%>(this.value)"></td>
			          <td width="72%" height="25" style="<%=rows[i].get("dev_flag",0)==1?"font-weight:bold;color:#990000;":"font-weight:normal"%>"><label for="dev<%=rows[i].getString("id")%>">Send</label></td>
			        </tr>
			        <tr>
			          <td><input type="checkbox" name="return<%=rows[i].getString("id")%>" id="return<%=rows[i].getString("id")%>" value="<%=rows[i].getString("id")%>" <%=rows[i].get("return_flag",0)==1?"checked":""%> onClick="return <%=rows[i].get("return_flag",0)==1?"unMarkReturnFlag":"markReturnFlag"%>(this.value,'<%=StringUtil.replaceString(StringUtil.replaceString(rows[i].getString("deliver_street")+rows[i].getString("deliver_house_number"), "\r", ""),"\n","")%>','<%=rows[i].getString("deliver_contact")%>','<%=rows[i].getString("deliver_phone")%>')"></td>
			          <td height="25" style="<%=rows[i].get("return_flag",0)==1?"font-weight:bold;color:#990000;":"font-weight:normal"%>"><label for="return<%=rows[i].getString("id")%>">Refund</label></td>
			        </tr>
			        <tr>
			          <td><input type="checkbox" name="sample<%=rows[i].getString("id")%>" id="sample<%=rows[i].getString("id")%>" value="<%=rows[i].getString("id")%>" <%=rows[i].get("sample_flag",0)==1?"checked":""%>  onClick="<%=rows[i].get("sample_flag",0)==1?"unMarkSampleFlag":"markSampleFlag"%>(this.value)"></td>
			          <td height="25" style="<%=rows[i].get("sample_flag",0)==1?"font-weight:bold;color:#990000;":"font-weight:normal"%>"><label for="sample<%=rows[i].getString("id")%>">Sample</label></td>
			        </tr>
			      </table></td>
<%-- 			      <td   align="center" valign="middle"><%=productMgr.getNewProductByCid(rows[i].get("id",0l),null).length%></td> --%>
			      <td   align="center" valign="middle">
			      <input name="timeOut" type="button" class="short-short-button" onclick="setTimeOut('<%=rows[i].getString("id")%>','<%=rows[i].getString("title")%>')" value="Time">
			      <input name="Submit2s" type="button" class="short-short-button-del" onClick="delC(<%=rows[i].getString("id")%>)" value="Del">
			      </td>
			    </tr>
			    <%	
			    	}
			    	}else{
			    		%>
			    		<tr>
			    			<td colspan="7" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;font-weight: bold;">No Data...</td>
			    		</tr>
			    		<%
			    	}
				%>
			</table>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <tr>
	    <td height="28" align="right" valign="middle" class="turn-page-table">
	<%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","Privous","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
	      Goto
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"/>
	        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
	    </td>
	  </tr>
</table>
</body>
</html>



