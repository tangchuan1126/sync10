<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.FileSelectTypeKey"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<%
	String search_key		= StringUtil.getString(request,"search_key");
	long file_with_id		= StringUtil.getLong(request, "file_with_id");
	int file_with_type		= StringUtil.getInt(request, "file_with_type");
	String pc_id			= StringUtil.getString(request, "pc_id");
	String pc_id_single		= StringUtil.getString(request, "pc_id_single");
	int receipt_type_select	= StringUtil.getInt(request, "receipt_type_select");
	receipt_type_select		= 0 == receipt_type_select?FileSelectTypeKey.PRODUCT:receipt_type_select;
	DBRow[] rows			= new DBRow[0];
	int is_unable_provide	= 2;
	if(FileSelectTypeKey.PRODUCT != receipt_type_select)
	{
		int file_with_types = 0;
		if(FileSelectTypeKey.PURCHASE == receipt_type_select)
		{
			file_with_types = FileWithTypeKey.PURCHASE_PRODUCT_FILE;
		}
		else
		{
			file_with_types = FileWithTypeKey.product_file;
		}
		if(!"".equals(pc_id_single))
		{
			rows = productMgrZyj.getAllProductFileByPcId(pc_id_single,file_with_types,search_key, is_unable_provide);
		}
		else
		{
			rows = productMgrZyj.getAllProductFileByPcId(pc_id,file_with_types,search_key, is_unable_provide);
		}
	}
	else
	{
		if(!"".equals(pc_id_single))
		{
			rows = productMgrZyj.getAllProductFileByPcId(pc_id_single,FileWithTypeKey.PRODUCT_SELF_FILE,pc_id_single, is_unable_provide);
		}
		else
		{
			rows = productMgrZyj.getAllProductFileByPcId(pc_id,FileWithTypeKey.PRODUCT_SELF_FILE,pc_id, is_unable_provide);
		}
	}
	DBRow[] productRows = transportMgrZr.getProductByPcIds(pc_id);
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>商品文件列表</title>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<script src="../js/fullcalendar/colorpicker.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/css/colorpicker.css" rel="stylesheet" />
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript">
$(function(){
	if('<%=receipt_type_select%>' == '<%=FileSelectTypeKey.PURCHASE%>')
	{
		$("#lucene_search_div").attr("style","");
		addAutoComplete($("#searchKey"),
		 		"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/purchase/GetPurchaseJSONAction.action",
		 			"merge_field","purchase_id");
	}
	else if('<%=receipt_type_select%>' == '<%=FileSelectTypeKey.TRANSPORT%>')
	{
		$("#lucene_search_div").attr("style","");
		addAutoComplete($("#searchKey"),
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/transport/GetSearchTransportJSONAction.action",
				"merge_field","transport_id");
	}
	else if('<%=receipt_type_select%>' == '<%=FileSelectTypeKey.PRODUCT%>')
	{
		$("#lucene_search_div").attr("style","display:none");
	}
	
	$("input:checkbox[id=all_select]").click(function(){
		if($(this).attr("checked"))
		{
			$("input:checkbox[name^=select_product_names]").attr("checked",true);
			var pc_id = "";
			$("input:checkbox[name^=select_product_names][checked]").each(
				function(i)
				{
					pc_id += "," + $(this).val();
				}
			);
			if(pc_id.trim().length > 0)
			{
				pc_id = pc_id.substr(1);
				$("#pc_id_single").val(pc_id);
				searchReceiptType();
			}
		}
		else
		{
			$("input:checkbox[name^=select_product_names]").attr("checked",false);
		}
	});
	$("input:checkbox[name^=select_product_names]").click(function(){
		if(!$(this).attr("checked") && $("input:checkbox[id=all_select]").attr("checked"))
		{
			$("input:checkbox[id=all_select]").attr("checked",false);
		}
		var isAll = true;
		var pc_id = "";
		$("input:checkbox[name^=select_product_names]").each(
			function(i)
			{
				if($(this).attr("checked"))
				{
					pc_id += "," + $(this).val();
				}
				else
				{
					isAll = false;
				}
			}
		);
		if(pc_id.trim().length > 0)
		{
			pc_id = pc_id.substr(1);
			$("#pc_id_single").val(pc_id);
			searchReceiptType();
		}
		if(isAll && !$("input:checkbox[id=all_select]").attr("checked"))
		{
			$("input:checkbox[id=all_select]").attr("checked",true);
		}
	});
	var pc_id_single = '<%=pc_id_single%>';
	if('' == pc_id_single)
	{
		pc_id_single = '<%=pc_id %>'
	}
	if('' != pc_id_single)
	{
		var pc_single_arr = pc_id_single.split(",");
		if(pc_single_arr.length > 0)
		{
			for(var i = 0; i < pc_single_arr.length; i ++)
			{
				$("input:checkbox[name^=select_product_names_"+pc_single_arr[i]+"]").attr('checked',true);
			}
		}
		if($("input:checkbox[name^=select_product_names]").length == pc_single_arr.length)
		{
			$("input:checkbox[id=all_select]").attr("checked",true);
		}
	}	
})
function changeReceiptType()
{
	$("#searchKey").val("");
	var selectVal = $("#receiptTypeSelect").val();
	if(selectVal*1 == '<%=FileSelectTypeKey.PURCHASE %>')
	{
		$("#lucene_search_div").attr("style","");
		addAutoComplete($("#searchKey"),
		 		"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/purchase/GetPurchaseJSONAction.action",
		 			"merge_field","purchase_id");
	}
	else if(selectVal*1 == '<%=FileSelectTypeKey.TRANSPORT %>')
	{
		$("#lucene_search_div").attr("style","");
		addAutoComplete($("#searchKey"),
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/transport/GetSearchTransportJSONAction.action",
				"merge_field","transport_id");
	}
	else if(selectVal*1 == '<%=FileSelectTypeKey.PRODUCT %>')
	{
		$("#lucene_search_div").attr("style","display:none");
	}
}
var scaleImage = function(o, w, h){
	var img = new Image();
	img.src = o.src;
	if(img.width >0 && img.height >0)
	{
		if(img.width/img.height >= w/h)
		{
			if(img.width > w)
			{
				$(o).width(w);
				$(o).height(img.height*w/img.width);
			}
			else
			{
				$(o).width(img.width);
				$(o).height(img.height);
			}
		}
		else
		{
			if(img.height > h)
			{
				$(o).height(h);
				$(o).width = (img.width * h / img.height);
			}
			else
			{
				$(o).width(img.width);
				$(o).height(img.height);
			}
		}
	}
} 
function delProductFile(_this)
{
	var parentNode = $(_this).parent().parent();
	parentNode.remove();
}
function topCheckIsChecked(){
	if($("#topCheckbox").attr("checked"))
	{
		$("input:checkbox[name=productCheckbox]").attr("checked", true);
	}
	else
	{
		$("input:checkbox[name=productCheckbox]").attr("checked", false);
	}
}

function savePurchaseProductAndFile()
{
	var fileIds = '';
	var checkeds = $("input:checkbox[name=productCheckbox][checked=checked]");
	checkeds.each(function(){
		var _this = $(this);
		fileIds += "," + _this.val();
	})
	fileIds = fileIds.length > 1 ? fileIds.substr(1):"";
	$("#file_ids").val(fileIds);
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/PurchaseProductModelSaveAction.action',
		data:$("#saveProductFileForm").serialize(),
		dataType:'json',
		type:'post',
		success:function(data){
			if(data && data.flag == "true"){
				showMessage("添加成功","success");
				setTimeout("windowClose()", 1000);
			}else{
				showMessage("添加失败","error");
			}
		},
		error:function(){
			showMessage("系统错误","error");
		}
	})	
}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function searchReceiptType()
{
	var val = $("#searchKey").val();
	var selectVal = $("#receiptTypeSelect").val();
	if(val.trim()=="" && selectVal*1 != '<%=FileSelectTypeKey.PRODUCT %>')
	{
		alert("请输入要查询的关键字");
	}
	else
	{
		val = val.replace(/\'/g,'');
		var val_search = "\'"+val+"\'";
		$("#searchKey").val(val_search);
		document.searchProductFileForm.search_key.value = val_search;
		document.searchProductFileForm.search_mode.value = 1;
		document.searchProductFileForm.receipt_type_select.value = $("#receiptTypeSelect").val();
		document.searchProductFileForm.submit();
	}
}

function searchReceiptTypeRightButton()
{
	var val = $("#searchKey").val();
	
	var selectVal = $("#receiptTypeSelect").val();
	if(val.trim()=="" && selectVal*1 != '<%=FileSelectTypeKey.PRODUCT %>')
	{
		alert("你好像忘记填写关键词了？");
	}
	else
	{
		val = val.replace(/\'/g,'');
		$("#search_key").val(val);
		document.searchProductFileForm.search_key.value = val;
		document.searchProductFileForm.search_mode.value = 2;
		document.searchProductFileForm.receipt_type_select.value = $("#receiptTypeSelect").val();
 		document.searchProductFileForm.submit();
	}
}

function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) {  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  

	document.getElementById("eso_search").onmouseup=function(oEvent) {  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
         	searchReceiptTypeRightButton();
    	}  
	}  
}
</script>
<style type="text/css">
ul.ul_p_name{list-style-type:none;margin-left:-1px;}
ul.ul_p_name li{margin-top:1px;margin-left:3px;line-height:25px;height:25px;padding-left:5px;padding-right:5px;float:left;border:1px solid silver;}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:2px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
table.zebraTable tr td{
		line-height:30px;
		height:30px;
	}
</style>
</head>
<body onload= "onLoadInitZebraTable()">
<form id="saveProductFileForm" action='' method="post">
	<input type="hidden" name="file_ids" id="file_ids">
	<input type="hidden" name="file_with_id" value='<%=file_with_id %>'/>
	<input type="hidden" name="file_with_type" value='<%=file_with_type %>'/>
	<input type="hidden" name="pc_id" value='<%=pc_id %>'/>
</form>
<form action="" method="post" id="searchProductFileForm" name="searchProductFileForm">
	<input type="hidden" name="pc_id" value='<%=pc_id %>'/>
	<input type="hidden" name="pc_id_single" id="pc_id_single"/>
	<input type="hidden" name="file_with_id" value='<%=file_with_id %>'/>
	<input type="hidden" name="file_with_type" value='<%=file_with_type %>'/>
	<input type="hidden" name="receipt_type_select" id="receipt_type_select" value='<%=receipt_type_select %>'/>
	<input type="hidden" name="search_key"/>
	<input type="hidden" name="cmd" value="search"/>
	<input type="hidden" name="search_mode"/>
</form>
<fieldset style="width:98%;border:2px #cccccc solid;-webkit-border-radius:2px;-moz-border-radius:2px;margin-bottom: 10px; margin-top: 15px;text-align: center;">
	<legend style="font-size:15px;font-weight:normal;color:#999999;">选择文件</legend>
	<table style="width:95%; margin-top: 6px;margin-bottom: 6px; margin-left: 7px; text-align: left;">
		<tr style="margin-bottom: 4px;">
			<td width="8%">
				单据类型:
			</td>
			<td align="left">
				<select id="receiptTypeSelect" onchange="changeReceiptType()">
					<%
						FileSelectTypeKey fileSelectTypeKey = new FileSelectTypeKey();
						ArrayList<String> keyList			= fileSelectTypeKey.getFileSelectTypeKeys();
						for(int i = 0; i < keyList.size(); i ++)
						{
							if(keyList.get(i).equals(String.valueOf(receipt_type_select)))
							{
					%>
							<option value='<%=keyList.get(i) %>' selected="selected"><%=fileSelectTypeKey.getFileSelectTypeKeyById(keyList.get(i)) %></option>
					<%				
							}
							else
							{
					%>
							<option value='<%=keyList.get(i) %>'><%=fileSelectTypeKey.getFileSelectTypeKeyById(keyList.get(i)) %></option>
					<%		
							}
						}
					%>
				</select>
			</td>
			<td>&nbsp;</td>
			<td style="padding-top:3px;" align="left">
				<div id="lucene_search_div">
				<div id="easy_search_father">
				<div id="easy_search"><a href="javascript:searchReceiptType()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
				</div>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<div  class="search_shadow_bg">
								<input type="text" name="searchKey" id="searchKey" value="<%=search_key%>" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" onkeydown="if(event.keyCode==13)searchReceiptType()"/>
							</div>
						</td>
					</tr>
				</table>
				<script>eso_button_even();</script>
				</div>
			</td>
			<td align="right">
				<input type="button" class="short-short-button" value="提交" onclick="savePurchaseProductAndFile()"/>
			</td>
		</tr>
		<tr style="margin-bottom: 8px;">
			<td width="8%">
				<span">关联商品:</span>
			</td>
			<td width="8%">
				全选&nbsp;&nbsp;<input type="checkbox" id="all_select"/>
			</td>
			<td style="text-align:left;vertical-align: center;" colspan="3"  width="84%">
				 <%
				 	if(productRows != null && productRows.length > 0){
				 %>
					<ul class="ul_p_name" style="float:left;">	
				 	<% 
				 		for(DBRow tempRow : productRows){
				 	%>
				 		 	<li><%=tempRow.getString("p_name") %>
				 		 		<input type="checkbox" name='select_product_names_<%=tempRow.get("pc_id",0L) %>' value='<%=tempRow.get("pc_id",0L) %>'/>
				 		 	</li>
				 	<% 
				 		}
					%>
					</ul>
				 <% 
				 	}
				 %>
			</td>
		</tr>
	</table>
	<table width="95%" border="0" align="center" cellpadding="1" cellspacing="0"  class="zebraTable" style="margin-left:20px;margin-top:5px;">
	  <tr> 
	  	<th width="5%" style="vertical-align: center;text-align: center;" class="right-title">
			<input type="checkbox" id="topCheckbox" onclick="topCheckIsChecked()"/>
		</th>
	  	<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">文件</th>
	  	<th width="38%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
        <th width="20%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
        <th width="17%" style="vertical-align: center;text-align: center;" class="right-title">文件类型</th>
       	<th width="5%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
      </tr>
      	<%
	      	if(rows.length > 0)
	  		{
	      		for(int i = 0; i < rows.length; i ++)
	      		{
	      	%>
	   			<tr>
	   				<td>
	   					<input type="checkbox" name="productCheckbox" value='<%=rows[i].get("pf_id",0L) %>'/>
	   				</td>
	   				<td>
	   					<%
	   						if(rows[i].getString("file_name").endsWith(".gif") 
								||rows[i].getString("file_name").endsWith(".jpg")
								||rows[i].getString("file_name").endsWith(".jpeg") 
								||rows[i].getString("file_name").endsWith(".png"))
	   						{
	   					%>
	   						<img style="margin-top: 10px; margin-bottom: 10px;" src='../../upload/product/<%=rows[i].getString("file_name") %>' onload="scaleImage(this,80,80)">
	   					<%	
	   						}
	   					%>
					</td>
	   				<td>
	   					<a href='<%= downLoadFileAction%>?file_name=<%=rows[i].getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_product")%>'><%=rows[i].getString("file_name") %></a>
					</td>
	   				<td>
	   				<%
	   					DBRow[] productRow = transportMgrZr.getProductByPcIds(rows[i].getString("pc_id"));
	   					if(productRow.length > 0)
	   					{
	   						DBRow product = productRow[0];
	   				%>
	   					<%=product.getString("p_name")%>
	   				<%		
	   					}
	   				%>
					</td>
					<td>
						<%
							String value = systemConfig.getStringConfigValue("transport_product_file");
							String[] arraySelected = value.split("\n");
							for(int m = 0; m < arraySelected.length; m ++)
							{
								String[] types = arraySelected[m].split("=");
								if(rows[i].get("product_file_type",0) == Integer.parseInt(types[0]))
								{
						%>
								<%=types[1] %>
						<%			
									break;
								}
							}
						%>
					</td>
	   				<td>
	    	  			<input type="button" class="short-short-button-del" onclick="delProductFile(this)" value="删除">
	   				</td>
	   			</tr>
	     <%
	      		}
	  		}
	      	else
	      	{
      	%>
			<tr>
				<td colspan="6" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
			</tr>
		<% 		
	      	}
     	%>
  		 </table>
</fieldset>
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
</script>
  </body>
   
</html>
