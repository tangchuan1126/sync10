<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../include.jsp"%> 
<%
 	String cmd = StringUtil.getString(request,"cmd");
 String sort_approve_date = StringUtil.getString(request,"sort_approve_date","true");
 String sort_post_date = StringUtil.getString(request,"sort_post_date","true");
 int p = StringUtil.getInt(request,"p");

 long psid = StringUtil.getLong(request,"psid");
 int approve_status = StringUtil.getInt(request,"approve_status");

 PageCtrl pc = new PageCtrl();
 pc.setPageNo(p);
 pc.setPageSize(30);

 DBRow storageApprove[];
 DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

 if (cmd.equals(""))
 {
 	psid = adminLoggerBean.getPs_id();
 }

 if (cmd.equals("search"))
 {
 	storageApprove = productMgr.getStorageApprovesByPsidStatus(psid,approve_status,pc);
 }
 else if (cmd.equals("sortby_post_date"))
 {
 	storageApprove = productMgr.getAllStorageApprovesSortByPostDate(psid,approve_status,Boolean.valueOf(sort_post_date),pc);
 	sort_post_date = String.valueOf(!Boolean.valueOf(sort_post_date));
 }
 else if (cmd.equals("sortby_approve_date"))
 {
 	storageApprove = productMgr.getAllStorageApprovesSortByApproveDate(psid,approve_status,Boolean.valueOf(sort_approve_date),pc);
 	sort_approve_date = String.valueOf(!Boolean.valueOf(sort_approve_date));
 }
 else
 {
 	storageApprove = productMgr.getAllStorageApproves(pc);
 }
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--

//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 » 库存差异审核</td>
  </tr>
</table>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr>
    <td>
	<form action="" method="get">
	<input type="hidden" name="cmd" value="search">
	
	仓库 
	<select name="psid" id="psid">
<option value="0">所有仓库</option>
          <%
String qx;

for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==psid?"selected":""%>> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>          </option>
          <%
}
%>
      </select>
	  
	  &nbsp;&nbsp;状态 
	
	<select name="approve_status" id="approve_status">
	  <option value="0" selected>全部</option>
	  <option value="1" <%=approve_status==1?"selected":""%>>无需审批</option>
	   <option value="2" <%=approve_status==2?"selected":""%>>审核中</option>
	    <option value="3" <%=approve_status==3?"selected":""%>>已审核</option>
	  </select>
	&nbsp;&nbsp;
	<input name="Submit2" type="submit" class="long-button-ok" value="过滤">
	
	</form>
	    </td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th style="vertical-align: center;text-align: center;" class="left-title">仓库</th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;"><a href="?cmd=sortby_post_date&sort_post_date=<%=sort_post_date%>&approve_status=<%=approve_status%>&psid=<%=psid%>&p=<%=p%>">提交日期</a>
		<%
		if (cmd.equals("sortby_post_date")&&sort_post_date.equals("true"))
		{
		%>
			<img src="../imgs/arrow_up.png" width="16" height="16" align="absmiddle">
		<%
		}
		else if (cmd.equals("sortby_post_date")&&sort_post_date.equals("false"))
		{
		%>
			<img src="../imgs/arrow_down.png" width="16" height="16" align="absmiddle">
		<%
		}
		%>
		 </th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;">提交人</th>
        <th width="11%" style="vertical-align: center;text-align: center;" class="right-title" >差异商品数/已审核</th>
        <th width="11%" class="right-title" style="vertical-align: center;text-align: center;" >状态</th>
        <th width="8%" class="right-title" style="vertical-align: center;text-align: center;" >审核人</th>
        <th width="14%" class="right-title" style="vertical-align: center;text-align: center;" ><a href="?cmd=sortby_approve_date&sort_approve_date=<%=sort_approve_date%>&approve_status=<%=approve_status%>&psid=<%=psid%>&p=<%=p%>">审核日期</a>
				<%
		if (cmd.equals("sortby_approve_date")&&sort_approve_date.equals("true"))
		{
		%>
			<img src="../imgs/arrow_up.png" width="16" height="16" align="absmiddle">
		<%
		}
		else if (cmd.equals("sortby_approve_date")&&sort_approve_date.equals("false"))
		{
		%>
			<img src="../imgs/arrow_down.png" width="16" height="16" align="absmiddle">
		<%
		}
		%>
		</th>
        <th width="14%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
    </tr>

<%
for (int i=0; i<storageApprove.length; i++)
{
%>

    <tr > 
      <td   width="15%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold'><%=catalogMgr.getDetailProductStorageCatalogById(storageApprove[i].get("ps_id",0l)).getString("title")%></td>
      <td   width="17%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=storageApprove[i].getString("post_date")%></td>
      <td   width="10%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=storageApprove[i].getString("commit_account")%></td>
      <td   align="center" valign="middle" style="font-size:15px;"><%=storageApprove[i].getString("difference_count")%>/<span style="color:#006600;font-weight:normal"><%=storageApprove[i].getString("difference_approve_count")%></span></td>
      <td   align="center" valign="middle" >
	  <%
	  if (storageApprove[i].get("difference_count",0)==0)
	  {
	  	out.println("<font color=black><strong>无需审批</strong></font>");
	  }
	  else if ( storageApprove[i].get("approve_status",0)==0 )
	  {
	  	out.println("<img src='../imgs/standard_msg_error.gif' width='16' height='16' align='absmiddle'> <font color=red><strong>审核中</strong></font>");
	  }
	  else
	  {
	  	out.println("<img src='../imgs/standard_msg_ok.gif' width='16' height='16' align='absmiddle'> <font color=green><strong>已审核</strong></font>");
	  }
	  %>
	  </td>
      <td   align="center" valign="middle" ><%=storageApprove[i].getString("approve_account")%>&nbsp;</td>
      <td   align="center" valign="middle" >
	  <%
	  if (storageApprove[i].getString("approve_date").trim().equals("1999-01-01 00:00:00.0"))
	  {
	  	out.println("&nbsp;");
	  }
	  else
	  {
	  	out.println(storageApprove[i].getString("approve_date"));
	  }
	  %></td>
      <td   align="center" valign="middle" >
	  <%
	  if (storageApprove[i].get("difference_count",0)==0)
	  {
	  	out.println("&nbsp;");
	  }
	  else if ( storageApprove[i].get("approve_status",0)==0 )
	  {
	  %>
	   <input name="Submit" type="button" class="short-button" value="审核" onClick="location='storage_approve_detail.html?sa_id=<%=storageApprove[i].getString("sa_id")%>&ps_id=<%=storageApprove[i].getString("ps_id")%>&backurl=<%=StringUtil.getCurrentURL(request)%>'">
	  <%
	  }
	  else
	  {
	  %>
	  		 <input name="Submit" type="button" class="short-button" value="详细" onClick="location='storage_approve_detail.html?sa_id=<%=storageApprove[i].getString("sa_id")%>&ps_id=<%=storageApprove[i].getString("ps_id")%>&approve_status=<%=storageApprove[i].get("approve_status",0)%>'">
	  <%
	  }
	  %>
      </td>
    </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p">
		<input type="hidden" name="psid" value="<%=psid%>">
		<input type="hidden" name="approve_status" value="<%=approve_status%>">

  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
</body>
</html>
