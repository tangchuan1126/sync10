<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.UploadEditFileKey"%>
<%@ include file="../../include.jsp"%> 
<%
String cmd = StringUtil.getString(request,"cmd");
String sort_filed = StringUtil.getString(request,"sort_filed");
String sort_type = StringUtil.getString(request,"sort_type","asc");
int p = StringUtil.getInt(request,"p");

int approve_status = StringUtil.getInt(request,"approve_status");
UploadEditFileKey uploadEditFileKey = new UploadEditFileKey();

PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(30);

DBRow importApprove[] = productMgrZJ.filterImportProductApprove(0,0,sort_filed,sort_type,pc);
if(sort_type.equals("asc"))
{
	sort_type = "desc";
}
else
{
	sort_type = "asc";
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--

//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 » 库存差异审核</td>
  </tr>
</table>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr>
    <td>
	<form action="" method="get">
	<input type="hidden" name="cmd" value="search">
	<select name="approve_status" id="approve_status">
	  <option value="0" selected>审核状态</option>
	  <option value="1" <%=approve_status==1?"selected":""%>>未审核</option>
	   <option value="2" <%=approve_status==2?"selected":""%>>同意</option>
	   <option value="2" <%=approve_status==2?"selected":""%>>不同意</option>
	  </select>
	&nbsp;&nbsp;
	<input name="Submit2" type="submit" class="long-button-ok" value="过滤">
	
	</form>
	    </td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr>
    	<th align="center" class="right-title" style="vertical-align: center;text-align: center;">类型</th> 
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;"><a href="?sort_filed=import_date&sort_type=<%=sort_type%>&approve_status=<%=approve_status%>&p=<%=p%>">提交日期</a>
		<%
		if (sort_filed.equals("import_date")&&sort_type.equals("desc"))
		{
		%>
			<img src="../imgs/arrow_up.png" width="16" height="16" align="absmiddle">
		<%
		}
		else if (sort_filed.equals("import_date")&&sort_type.equals("asc"))
		{
		%>
			<img src="../imgs/arrow_down.png" width="16" height="16" align="absmiddle">
		<%
		}
		%>
		 </th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;">提交人</th>
        <th width="11%" class="right-title" style="vertical-align: center;text-align: center;" >状态</th>
        <th width="8%" class="right-title" style="vertical-align: center;text-align: center;" >审核人</th>
        <th width="14%" class="right-title" style="vertical-align: center;text-align: center;" ><a href="?sort_filed=approve_date&sort_type=<%=sort_type%>&approve_status=<%=approve_status%>&p=<%=p%>">审核日期</a>
		<%
		if(sort_filed.equals("approve_date")&&sort_type.equals("desc"))
		{
		%>
			<img src="../imgs/arrow_up.png" width="16" height="16" align="absmiddle">
		<%
		}
		else if(sort_filed.equals("approve_date")&&sort_type.equals("asc"))
		{
		%>
			<img src="../imgs/arrow_down.png" width="16" height="16" align="absmiddle">
		<%
		}
		%>
		</th>
        <th width="14%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
    </tr>

<%
for (int i=0; i<importApprove.length; i++)
{
%>

    <tr>
      <td height="40" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=uploadEditFileKey.getUploadEditFileById(importApprove[i].getString("upload_type"))%></td> 
      <td height="40"   width="17%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=importApprove[i].getString("import_date")%></td>
      <td   width="10%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=adminMgr.getDetailAdmin(importApprove[i].get("import_adid",0l)).getString("employe_name")%></td>
      <td   align="center" valign="middle" >
	  <%
	  if(importApprove[i].get("approve_status",0)==1)
	  {
	  	out.println("<strong>审核中</strong>");
	  }
	  else if(importApprove[i].get("approve_status",0)==2)
	  {
	  	out.println("<img src='../imgs/standard_msg_ok.gif' width='16' height='16' align='absmiddle'> <font color=green><strong>同意</strong></font>");
	  }
	  else
	  {
	  	out.println("<img src='../imgs/standard_msg_error.gif' width='16' height='16' align='absmiddle'> <font color=red><strong>不同意</strong></font>");
	  }
	  %>
	  </td>
      <td   align="center" valign="middle">&nbsp;</td>
      <td   align="center" valign="middle">
	  <%
	  if (importApprove[i].getString("approve_date").trim().equals(""))
	  {
	  	out.println("&nbsp;");
	  }
	  else
	  {
	  	out.println(importApprove[i].getString("approve_date"));
	  }
	  %></td>
      <td   align="center" valign="middle" >
	  <%
	  if ( importApprove[i].get("approve_status",0)==1)
	  {
	  %>
	   <input name="Submit" type="button" class="short-button" value="审核" onClick="location='import_product_approve_detail.html?ipa_id=<%=importApprove[i].getString("ipa_id")%>&backurl=<%=StringUtil.getCurrentURL(request)%>&upload_type=<%=importApprove[i].get("upload_type",0)%>&approve_status=<%=importApprove[i].get("approve_status",0)%>'">
	  <%
	  }
	  else
	  {
	  %>
	  		 <input name="Submit" type="button" class="short-button" value="详细" onClick="location='import_product_approve_detail.html?ipa_id=<%=importApprove[i].getString("ipa_id")%>&backurl=<%=StringUtil.getCurrentURL(request)%>&upload_type=<%=importApprove[i].get("upload_type",0)%>&approve_status=<%=importApprove[i].get("approve_status",0)%>'">
	  <%
	  }
	  %>
      </td>
    </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p">
		<input type="hidden" name="approve_status" value="<%=approve_status%>">

  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
</body>
</html>
