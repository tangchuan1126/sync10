<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.ProductStoreOperationKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@page import="com.cwc.app.key.ProductStoreCountTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
//仓库颜色
HashMap storageColor = new HashMap();
storageColor.put("LA","<font color='#0000FF'><b>洛杉矶仓库</b></font>");
storageColor.put("SZ","<font color='#FF9900'><b>深圳仓库</b></font>");
storageColor.put("BJ","<font color='#333333'><b>北京仓库</b></font>");
storageColor.put("GZ","<font color='#FF6600'><b>广州仓库</b></font>");
storageColor.put("海运中转库","<font color='#FF6600'><b>海运中转库</b></font>");


PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

String cmd = StringUtil.getString(request,"cmd");
long oid = StringUtil.getLong(request,"oid");
String p_name = StringUtil.getString(request,"p_name");
long account = StringUtil.getLong(request,"account");

String input_st_date = StringUtil.getString(request,"input_st_date");
String input_en_date = StringUtil.getString(request,"input_en_date");

long product_line_id = StringUtil.getLong(request,"filter_productLine");
long catalog_id = StringUtil.getLong(request,"filter_pcid");
int bill_type = ProductStoreBillKey.RETURN_ORDER;
int operotion = StringUtil.getInt(request,"operotion");
int quantity_type = StringUtil.getInt(request,"quantity_type");
long ps_id = StringUtil.getLong(request,"ps_id");
int onGroup = StringUtil.getInt(request,"onGroup");
 
TDate tDate = new TDate();

if (input_en_date.equals(""))
{
	input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}

tDate.addDay(-30);
if (input_st_date.equals(""))
{
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}


DBRow[] storeLogs = productMgr.getSearchReturnProductStoreSysLogsNew(input_st_date,input_en_date,product_line_id,catalog_id,p_name,oid,bill_type,quantity_type,ps_id,account,onGroup,pc);

DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();

ProductStoreOperationKey productOperationKey = new ProductStoreOperationKey();
ProductStoreBillKey productStoreBillKey = new ProductStoreBillKey();
ProductStoreCountTypeKey productStoreCountTypeKey = new ProductStoreCountTypeKey();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<style type="text/css">
	td.categorymenu_td div{margin-top:-3px;}
</style>
<script language="javascript">
$().ready(function() {
addAutoComplete($("#p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"p_name");

				//常用过滤选项卡中的商品回车查询
				$("#p_name").keydown(function(event){
					if (event.keyCode==13)
					{
						filter();
					}
				});
				
			});
<!--
function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.search_form.input_st_date.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.search_form.input_en_date.value = date;
}


function filter()
{
	document.search_form.submit();
}

function exportReturnLog()
{
	var datapara = $("#search_form").serialize()
	$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/exportReturnLogs.action',
					type: 'post',
					dataType: 'json',
					cache:false,
					data:datapara,
					
					beforeSend:function(request){
						$(".cssDivschedule").css("display","block");
					},
					
					error: function(e){
						$(".cssDivschedule").fadeOut("fast");
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						$(".cssDivschedule").fadeOut("fast");
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
}


	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
	div.innerDiv{margin:0 auto; margin-top:200px;text-align:center;border:1px solid silver;width:400px;height:30px;background:white;line-height:30px;font-size:14px;}
</style>
 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<form action="" name="download_form"></form>

<div class="cssDivschedule" style="display:none;">
	<div class="innerDiv">请求数据中...</div>
</div>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   库存日志</td>
  </tr>
</table>
<br>
<div class="demo">
  <div align="left" id="tabs">
	<ul>
  	    <li><a href="#filter">常用过滤</a></li>
	  	<li><a href="#search">单号搜索</a></li>
	</ul>
	<div id="filter" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			<form action="return_store_sys_logs.html" method="post" name="search_form" id="search_form">
			   		<table width="100%" border="0">
			   			<tr>
			   				<td width="13%">
			   					开始&nbsp;&nbsp;<input name="input_st_date" type="text" id="input_st_date" size="9" value="<%=input_st_date%>" readonly="readonly"> 
		    			     <script language="JavaScript" type="text/javascript" >
		    					stfooCalendar = new dynCalendar('stfooCalendar', 'stcalendarCallback', '../js/images/');
		    				 </script>
			   				</td>
			   				<td align="left" width="416">
			   					<ul id="productLinemenu" class="mcdropdown_menu">
					             <li rel="0">所有产品线</li>
					             <%
									  DBRow p1[] = productLineMgrTJH.getAllProductLine();
									  for (int i=0; i<p1.length; i++)
									  {
											out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
											 
											out.println("</li>");
									  }
								  %>
					           </ul>
					           <input type="text" name="productLine" id="productLine" value="" />
					           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
			   				</td>
			   				<td>
			   					<input name="onGroup" type="radio" value="0" <%=onGroup==0?"checked=\"checked\"":""%>/>列表 <input type="radio" name="onGroup" <%=onGroup==1?"checked=\"checked\"":""%> value="1">汇总
						        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						        <input name="Submit" type="button" class="button_long_refresh" onClick="filter()" value="过滤">
			   				</td>
			   			</tr>
			   			<tr>
			   				<td>
			   					结束&nbsp;&nbsp;<input name="input_en_date" type="text" id="input_en_date" size="9" value="<%=input_en_date%>" readonly="readonly">
							   <script language="JavaScript" type="text/javascript">
				    				enfooCalendar = new dynCalendar('enfooCalendar', 'encalendarCallback', '../js/images/');
								</script>
							</td>
			   				<td id="categorymenu_td" class="categorymenu_td" width="423" align="left" valign="bottom" style="padding-top:10px;">
			   					<ul id="categorymenu" class="mcdropdown_menu">
								  <li rel="0">所有分类</li>
								  <%
									  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
									  for (int i=0; i<c1.length; i++)
									  {
											out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
								
											  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
											  if (c2.length>0)
											  {
											  		out.println("<ul>");	
											  }
											  for (int ii=0; ii<c2.length; ii++)
											  {
													out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
													
														DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
														  if (c3.length>0)
														  {
																out.println("<ul>");	
														  }
															for (int iii=0; iii<c3.length; iii++)
															{
																	out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																	out.println("</li>");
															}
														  if (c3.length>0)
														  {
																out.println("</ul>");	
														  }
														  
													out.println("</li>");				
											  }
											  if (c2.length>0)
											  {
											  		out.println("</ul>");	
											  }
											  
											out.println("</li>");
									  }
									  %>
								</ul>
						  <input type="text" name="category" id="category" value="" />
						  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"/>
			   				</td>
			   				<td>
			   					 <select name="ps_id" id="ps_id">
									<option value="0">所有仓库</option>
										<%
										String qx;
										
										for ( int i=0; i<treeRows.length; i++ )
										{
											if ( treeRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
										%>
										          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==ps_id?"selected":""%>> 
										          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										          <%=qx%>
										          <%=treeRows[i].getString("title")%>          </option>
										          <%
										}
										%>
						        </select>
			   				</td>
			   				
			   					
			   			</tr>
			   			<tr>
			   				<td>
			   					<select name="account" id="account">
					  				<option value="">全部管理员</option>
								<%
									DBRow admins[] = adminMgr.getAllAdmin(null);
									
									for ( int i=0; i<admins.length; i++ )
									{
								%>
									<option value="<%=admins[i].get("adid",0l)%>" <%=account == admins[i].get("adid",0l)?"selected":""%>> 
										<%=admins[i].getString("employe_name")%>       
									</option>
								<%
									}
									%>
		         				</select>
		         			</td>
			   				<td style="">	   					
		               			商品名 <input style="width: 375px;" name="p_name" type="text" id="p_name" value="<%=p_name%>">
				           	</td>
				           <td>
				           		<select id="quantity_type" name="quantity_type">
				           			<option value="0" <%=quantity_type==0?"selected=\"selected\"":"" %>>全部</option>
				           			<option value="<%=ProductStoreCountTypeKey.STORECOUNT%>" <%=quantity_type==ProductStoreCountTypeKey.STORECOUNT?"selected=\"selected\"":"" %>>正常商品</option>
				           			<option value="<%=ProductStoreCountTypeKey.DAMAGED%>" <%=quantity_type==ProductStoreCountTypeKey.DAMAGED?"selected=\"selected\"":"" %>>残损商品</option>
				           		</select>
				           		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						        <input name="Submit" type="button" class="long-button-export" onClick="exportReturnLog()" value="下载">
				           </td>
			   			</tr>
			   		</table>
				<input type="hidden" name="cmd" value="search">
		    </form>
		</div>
		<div id="search">
			<form action="return_store_sys_logs.html" method="post">
				<input name="cmd" value="search" type="hidden"/>
				<table width="100%">
					<tr>
						<td nowrap="nowrap">
					       退货单<input name="oid" type="text" id="oid" style="width:70px;" value="<%=oid%>">号
					         &nbsp;&nbsp;&nbsp;<input name="Submit" type="submit" class="button_long_search" value="查询">
					    </td>
					</tr>
				</table>
			</form>
		</div>
   </div>
</div>
				 <script>
					$("#tabs").tabs({
						cache: true,
						spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
						cookie: {expires: 30000},
						load: function(event, ui) {onLoadInitZebraTable();}	
					});
				</script>	



<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="8%" style="vertical-align: center;text-align: center;" class="left-title">仓库</th>
        <th align="left" class="right-title" style="vertical-align: center;text-align: left;">商品</th>
        <th width="13%" style="vertical-align: center;text-align: center;" class="right-title" >操作</th>
        <th width="6%" style="vertical-align: center;text-align: center;" class="right-title">正常/残损</th>
        <th width="6%" style="vertical-align: center;text-align: center;" class="right-title">消费库存</th>
        <th width="8%" style="vertical-align: center;text-align: center;" class="right-title">操作人</th>
        <th width="8%" style="vertical-align: center;text-align: center;" class="right-title">关联单据类型</th>
        <th width="7%" style="vertical-align: center;text-align: center;" class="right-title">关联单号</th>
        <th width="14%" style="vertical-align: center;text-align: center;" class="right-title">时间</th>
    </tr>

    <%


for ( int i=0; i<storeLogs.length; i++ )
{

%>
    <tr > 
      <td   width="6%" height="60" align="center" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%=storageColor.get(storeLogs[i].getString("title"))%></td>
      <td   width="20%" align="left" valign="middle" style='word-break:break-all;' ><%=storeLogs[i].getString("p_name")%></td>
      <td   align="center" valign="middle" >
      	<%
      		if(onGroup==0)
      		{
      	%>
      		<%=productOperationKey.getProductStoreOperationId(storeLogs[i].getString("operation")).equals("")?storeLogs[i].getString("operation"):productOperationKey.getProductStoreOperationId(storeLogs[i].getString("operation"))%>
      	<%
      		}
      	%>
      	&nbsp;
      </td>
      <td  align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
      	<%=productStoreCountTypeKey.getProductStoreCountTypeKeyId(storeLogs[i].getString("quantity_type"))%>
      </td>
      <td  align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
      	<%=storeLogs[i].get("quantity",0f)>=0?storeLogs[i].get("quantity",0f):"<span style='color:red;'>"+storeLogs[i].get("quantity",0f)+"</span>"%></td>
      <td  align="center" valign="middle" style='word-break:break-all;'>
      	<% 
      		if(onGroup==0)
      		{
	      		DBRow admin = adminMgrLL.getAdminById(storeLogs[i].getString("adid"));
	      		if(admin==null)
	      		{
	      			admin = new DBRow();
	      		}
	      		
	      		out.print(admin.getString("employe_name"));
	      	}
      	%>&nbsp;
      </td>
      <td  align="center" valign="middle" style='word-break:break-all;'>
      	<%
      		if(onGroup==0)
      		{
      	%>
      	<%=productStoreBillKey.getProductStoreBillId(storeLogs[i].getString("bill_type"))%>
      	<%
      		}
      	%>&nbsp;
      </td>
      <td   align="center" valign="middle" style='word-break:break-all;'>
	  <%
	  	if(onGroup==0)
	  	{
	  		if (storeLogs[i].get("oid",0l)>0)
			{
			 	out.println("<a href='../order/ct_order_auto_reflush.html?val="+storeLogs[i].getString("oid")+"&cmd=search&search_field=3' target='_blank'>"+storeLogs[i].getString("oid")+"</a>");
			}
			else
			{
			  	out.println(storeLogs[i].get("oid",0l));
			}
	  	}
	  %>&nbsp;
	  </td>
      <td align="center" valign="middle">
      	<% 
      		if(onGroup==0)
	  		{
      	%>
      	<%=storeLogs[i].getString("post_date")%>
      	<%
      		}
      	%>&nbsp;
      </td>
    </tr>
    <%
}
%>
</form>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p">
		  
		  <input type="hidden" name="oid" value="<%=oid%>">
		  <input type="hidden" name="p_name" value="<%=p_name%>">
		  <input type="hidden" name="account" value="<%=account%>">
		  <input type="hidden" name="input_st_date" value="<%=input_st_date%>">
		  <input type="hidden" name="input_en_date" value="<%=input_en_date%>">
		  <input type="hidden" name="cmd" value="<%=cmd%>">
		  
		  <input type="hidden" name="filter_productLine" value="<%=product_line_id%>"/>
		  <input type="hidden" name="filter_pcid" value="<%=catalog_id%>"/>
		  <input type="hidden" name="bill_type" value="<%=bill_type%>"/>
		  <input type="hidden" name="operotion" value="<%=operotion %>"/>
		  <input type="hidden" name="quantity_type" value="<%=quantity_type%>"/>
		  <input type="hidden" name="ps_id" value="<%=ps_id%>"/>
		  <input type="hidden" name="onGroup" value="<%=onGroup%>"/>
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<script type="text/javascript">
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}
});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
					if(id==<%=product_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
				}
});
<%
if (catalog_id>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=catalog_id%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 

<%
if (product_line_id!=0)
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue('<%=product_line_id%>');
<%
}
else
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue(0);
<%
}
%>

function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"filter_name").val("");
}
</script>
</body>
</html>
