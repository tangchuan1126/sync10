<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="javax.mail.Transport"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%
	String cmd = StringUtil.getString(request,"cmd");

String p_name = StringUtil.getString(request,"p_name");

long catalog_id = StringUtil.getLong(request,"catalog_id");
long product_line_id = StringUtil.getLong(request,"product_line_id");
long ps_id = StringUtil.getLong(request,"ps_id");

int greater_less = StringUtil.getInt(request,"greater_less",2);
String st = StringUtil.getString(request,"st");
String en = StringUtil.getString(request,"en");
int sales_rate = StringUtil.getInt(request,"sales_rate",10);

AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);


if (cmd.equals(""))
{
	ps_id = adminLoggerBean.getPs_id();
}

	String p = StringUtil.getString(request,"p","0");
	
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
							
	String input_st_date,input_en_date;
	if ( st.equals("") )
	{	
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		st = input_st_date;
	}
	else
	{	
		input_st_date = st;
	}
							
	if ( en.equals("") )
	{	
		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		en = input_en_date;
	}
	else
	{	
		input_en_date = en;
	}

DBRow rows[] = productStoreLogsDetailMgrZJ.salesRate(input_st_date,input_en_date,product_line_id,catalog_id,p_name,ps_id,sales_rate,greater_less,pc);
DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();

Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>积压商品管理</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
		
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">


<!--  自动填充 -->

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<script language="javascript">
<!--
$().ready(function() {

	function log(event, data, formatted) {
		
	}

	addAutoComplete($("#filter_name"),
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
		"merge_info",
		"p_name");
	
	$("#filter_name").keydown(function(event){
		if (event.keyCode==13)
		{
			searchByName();
		}
	});
	
});


function onMOBg(row,cl,index)
{
	if ( !document.listForm.pid_batch[index].checked )
	{
		row.style.background=cl;
	}
}

function searchByCid()
{
	document.search_form.cmd.value = "filter";
	document.search_form.ps_id.value = $("#filter_cid").val();	
	document.search_form.catalog_id.value = $("#filter_pcid").val();
	document.search_form.product_line_id.value = $("#filter_productLine").val();
  	document.search_form.greater_less.value = $("#greater_less").val();
	document.search_form.sales_rate.value = $("#sales_rate").val();
	document.search_form.st.value = $("#st").val();
	document.search_form.en.value = $("#en").val();
	document.search_form.submit();
}

function filterByCid(catalog_id,ps_id)
{
	document.search_form.cmd.value = "filter";
	document.search_form.ps_id.value = ps_id;
	document.search_form.catalog_id.value = catalog_id;
	
  	document.search_form.greater_less.value = <%=greater_less%>;
	document.search_form.sales_rate.value = <%=sales_rate%>;
	document.search_form.st.value = '<%=input_st_date%>';
	document.search_form.en.value = '<%=input_en_date%>';
	document.search_form.submit();
}

function searchByName()
{
	if ($("#filter_name").val()=="")
	{
		alert("请填写关键词");
	}
	else
	{
		document.search_form.cmd.value = "search";
		document.search_form.p_name.value = $("#filter_name").val();	
		document.search_form.ps_id.value = $("#filter_cid").val();
		document.search_form.greater_less.value = $("#greater_less").val();
		document.search_form.sales_rate.value = $("#sales_rate").val();
		document.search_form.st.value = $("#st").val();
		document.search_form.en.value = $("#en").val();
		document.search_form.submit();
	}
}


function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 




function closeWin()
{
	tb_remove();
}

function closeWinRefresh()
{
	window.location.reload();
	tb_remove();
}

	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
	
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>

<form action="" method="get" name="search_form">		
  <input type="hidden" name="cmd"> 
  <input type="hidden" name="ps_id"> 
  <input type="hidden" name="catalog_id"> 
  <input type="hidden" name="p_name">
  <input type="hidden" name="product_line_id"/>
  <input type="hidden" name="greater_less"/>
  <input type="hidden" name="sales_rate"/>
  <input type="hidden" name="st"/>
  <input type="hidden" name="en"/>
</form>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
          <td height="33" >
		  <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
	 
		  <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
               <td nowrap="nowrap" style="font-weight:bold;">
               		开始:<input name="st" type="text" id="st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
               </td>
              <td width="416" style="padding-left:10px;">
	             <ul id="productLinemenu" class="mcdropdown_menu">
	             <li rel="0">所有产品线</li>
	             <%
					  DBRow p1[] = productLineMgrTJH.getAllProductLine();
					  for (int i=0; i<p1.length; i++)
					  {
							out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
							 
							out.println("</li>");
					  }
				  %>
	           </ul>
	           <input type="text" name="productLine" id="productLine" value="" />
	           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
			</td>
              <td width="357" height="30"  style="border-bottom:1px #dddddd solid">
              <em><select name="filter_cid" id="filter_cid"> 
				<option value="0">所有仓库</option> 
				<% 
				String qx; 
				 
				for ( int i=0; i<treeRows.length; i++ ) 
				{ 
					if ( treeRows[i].get("parentid",0) != 0 ) 
					 { 
					 	qx = "├ "; 
					 } 
					 else 
					 { 
					 	qx = ""; 
					 } 
				%> 
				          <option value='<%=treeRows[i].getString("id")%>' <%=treeRows[i].get("id",0l)==ps_id?"selected":""%>>  
				          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%> 
				          <%=qx%> 
				          <%=treeRows[i].getString("title")%>          </option> 
				          <% 
				} 
				%> 
        </select>&nbsp;&nbsp; 
		出库率 
			<select name="greater_less" id="greater_less"> 
				<option value="1" <%=1==greater_less?"selected=\"selected\"":""%>>大于</option> 
				<option value="2" <%=2==greater_less?"selected=\"selected\"":""%>>小于</option> 
			</select> 
			<input type="text" size="3" name="sales_rate" id="sales_rate" value="<%=sales_rate%>">% 
		&nbsp;&nbsp; 
        <input type="button" name="Submit4" class="button_long_refresh" onclick="searchByCid()" value="过滤"></em>			  
        </td>
 		</tr>
            <tr>
            <td nowrap="nowrap" style="padding-top:10px;font-weight:bold;">
               	结束:<input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;"/>
            </td>
			<td id="categorymenu_td" width="423" align="left" valign="bottom" bgcolor="#eeeeee" style="padding-left:10px;padding-top:10px;"> 
			           <ul id="categorymenu" class="mcdropdown_menu">
						  <li rel="0">所有分类</li>
						  <%
							  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
							  for (int i=0; i<c1.length; i++)
							  {
									out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
						
									  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
									  if (c2.length>0)
									  {
									  		out.println("<ul>");	
									  }
									  for (int ii=0; ii<c2.length; ii++)
									  {
											out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
											
												DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
												  if (c3.length>0)
												  {
														out.println("<ul>");	
												  }
													for (int iii=0; iii<c3.length; iii++)
													{
															out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
															out.println("</li>");
													}
												  if (c3.length>0)
												  {
														out.println("</ul>");	
												  }
												  
											out.println("</li>");				
									  }
									  if (c2.length>0)
									  {
									  		out.println("</ul>");	
									  }
									  
									out.println("</li>");
							  }
							  %>
						</ul>
				  <input type="text" name="category" id="category" value="" />
				  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"/>
			</td>
              <td height="30">
              	<input name="filter_name" type="text" id="filter_name" value="<%=p_name%>" style="width:200px;" onClick="cleanProductLine('')"> &nbsp;&nbsp;
        <input name="Submit3" type="button" class="button_long_search" value="搜索" onClick="searchByName()">			  </td>
            </tr>
          </table>

			</div>
		  </td>
  </tr>
</table>
	</td>
  </tr>
</table>
<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" >
  <form name="listForm" method="post">
    <tr> 
        <th width="325" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
        <th width="258" align="left" class="right-title"  style="vertical-align: center;text-align: center;">商品分类</th>
        <th width="258" align="left" class="right-title"  style="vertical-align: center;text-align: center;">出库总数</th>
        <th width="258" align="left" class="right-title"  style="vertical-align: center;text-align: center;">入库总数</th>
        <th width="325"  style="vertical-align: center;text-align: center;" class="right-title">出库率</th>
    </tr>

    <%
String delPCID = "";
DBRow proGroup[];
for ( int i=0; i<rows.length; i++ )
{
delPCID += rows[i].getString("pc_id")+",";
%>
    <tr > 
      <td height="60" valign="middle" style='font-size:14px;'>
      	<%=rows[i].getString("p_name")%>
      </td>
      <td height="60" align="left" valign="middle"   style='line-height:20px;' >
	   <span style="color:#999999">
	  <%
	  DBRow allCatalogFather[] = catalogTree.getAllFather(rows[i].get("catalog_id",0l));
	  for (int jj=0; jj<allCatalogFather.length-1; jj++)
	  {
	  	out.println("<a class='nine4' href='javascript:filterByCid("+allCatalogFather[jj].getString("id")+","+ps_id+")'>"+allCatalogFather[jj].getString("title")+"</a><br>");

	  }
	  %>
	  </span>
	  
	  <%
	  DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
	  if (catalog!=null)
	  {
	  	out.println("<a href='javascript:filterByCid("+rows[i].getString("catalog_id")+","+ps_id+")'>"+catalog.getString("title")+"</a>");
	  }
	  %>	  </td>
      <td align="center" valign="middle"   style='word-break:break-all;color:red;font-weight:bold'>
      	<%=rows[i].get("sum_used_quantity",0f)%>
	  </td>
	  <td align="center" valign="middle"   style='word-break:break-all;color:red;font-weight:bold'>
	  	<%=rows[i].get("sum_quantity",0f)%>
	  </td>
	  <td align="center" valign="middle"   style='word-break:break-all;color:red;font-weight:bold'>
	  	<%=MoneyUtil.round(rows[i].get("sales_rate",0d),2)%>%
	  </td>
    </tr>
<%
}
%>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
     <input type="hidden" name="p" >
	 <input type="hidden" name="ps_id" value="<%=ps_id%>">
	 <input type="hidden" name="cmd" value="<%=cmd%>">
	 <input type="hidden" name="p_name" value="<%=p_name%>">
	 <input type="hidden" name="catalog_id" value="<%=catalog_id%>">
	 <input type="hidden" name="product_line_id" value="<%=product_line_id%>"/>
	 <input type="hidden" name="greater_less" value="<%=greater_less%>"/>
	 <input type="hidden" name="sales_rate" value="<%=sales_rate%>"/>
	 <input type="hidden" name="st" value="<%=st%>"/>
	 <input type="hidden" name="en" value="<%=en%>"/>
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle">
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<script type="text/javascript">

$("#st").date_input();
$("#en").date_input();
			
function ref()
{
	window.location.reload();
}
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}
});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
					if(id==<%=product_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
				}
});
<%
if (catalog_id>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=catalog_id%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 

<%
if (product_line_id!=0)
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue('<%=product_line_id%>');
<%
}
else
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue(0);
<%
}
%>

function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"filter_name").val("");
}
</script>
</body>
</html>
