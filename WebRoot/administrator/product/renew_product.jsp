<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long psid = StringUtil.getLong(request,"psid");
long s_pid = StringUtil.getLong(request,"s_pid");
long s_pc_id = StringUtil.getLong(request,"s_pc_id");
String storage_name = StringUtil.getString(request,"storage_name");
DBRow detailPro = productMgr.getDetailProductByPcid(s_pc_id);
DBRow detailProStorage = productMgr.getDetailProductStorageByPid(s_pid);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script>

$().ready(function() {

	function log(event, data, formatted) {

		
	}
	

	
});

function checkForm()
{	
	var theForm = document.add_union_form;
	
	if( typeof(theForm.damaged_count_m)!="undefined" && theForm.damaged_count_m.value=="" )
	{
		alert("功能残损翻新数不能为空");
	}
	else if( typeof(theForm.damaged_count_m)!="undefined" && !isNum(theForm.damaged_count_m.value) )
	{
		alert("功能残损翻新数不正确");
	}
	else if( typeof(theForm.damaged_count_m)!="undefined" && (theForm.damaged_count_m.value<=0||theForm.damaged_count_m.value><%=detailProStorage.get("damaged_count",0f)%>) )
	{
		alert("功能残损翻新数超过允许范围");
	}
	else if( typeof(theForm.damaged_package_count_m)!="undefined" && theForm.damaged_package_count_m.value=="" )
	{
		alert("外观残损翻新数不能为空");	
	}
	else if( typeof(theForm.damaged_package_count_m)!="undefined" && !isNum(theForm.damaged_package_count_m.value) )
	{
		alert("外观残损翻新数不正确");
	}
	else if( typeof(theForm.damaged_package_count_m)!="undefined" && (theForm.damaged_package_count_m.value<=0||theForm.damaged_package_count_m.value><%=detailProStorage.get("damaged_package_count",0f)%>) )
	{
		alert("外观残损翻新数超过允许范围");
	}
	else
	{
		var damaged_count_m = 0;
		var damaged_package_count_m = 0;
		
		if ( typeof(theForm.damaged_count_m)!="undefined" )
		{
			damaged_count_m = theForm.damaged_count_m.value;
		}

		if ( typeof(theForm.damaged_package_count_m)!="undefined" )
		{
			damaged_package_count_m = theForm.damaged_package_count_m.value;
		}
		
		parent.renewProduct(<%=psid%>,<%=s_pid%>,<%=s_pc_id%>,damaged_count_m,damaged_package_count_m);
	}
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_union_form" method="post" action="" >

<table width="98%" border="0" cellspacing="3" cellpadding="2">

  <tr>
    <td align="left" valign="middle" style="padding:20px;">
	<span style="font-weight:bold;color:#666666;font-size:30px;font-family:Arial, Helvetica, sans-serif;"><span style="color:#990000"><%=storage_name%> - </span><%=detailPro.getString("p_name")%></span>	</td>
  </tr>
  <tr>
    <td align="left" valign="middle" style="padding:20px;">
	  	<%
	if ( detailProStorage.get("damaged_count",0f)>0||detailProStorage.get("damaged_package_count",0f)>0 )
	{
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="5">

	
	
	<%
	if ( detailProStorage.get("damaged_count",0f)>0 )
	{
	%>
      <tr>
        <td width="345" style="font-size:15px;">功能残损：<span style="color:#990000;font-weight:bold"><%=detailProStorage.get("damaged_count",0f)%></span> <%=detailPro.getString("unit_name")%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td width="968" style="font-size:15px;">翻新 
     
          <input type="text" name="damaged_count_m" id="damaged_count_m" style="width:40px;"> <%=detailPro.getString("unit_name")%> </td>
      </tr>
	<%
	}
	%>
	
	<%
	if ( detailProStorage.get("damaged_package_count",0f)>0 )
	{
	%>
      <tr>
        <td style="font-size:15px;">外观残损：<span style="color:#990000;font-weight:bold"><%=detailProStorage.get("damaged_package_count",0f)%></span> <%=detailPro.getString("unit_name")%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td style="font-size:15px;">翻新 
		<input type="text" name="damaged_package_count_m" id="damaged_package_count_m" style="width:40px;"> <%=detailPro.getString("unit_name")%>
		</td>
      </tr>
	<%
	}
	%>
    </table>
	<%
	}
	else
	{
		out.println("没有残损件！");
	}	
	%>
	</td>
  </tr>
</table>
</form>
		
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="right" class="win-bottom-line">
		  	<%
	if ( detailProStorage.get("damaged_count",0f)>0||detailProStorage.get("damaged_package_count",0f)>0 )
	{
	%>
      <input type="button" name="Submit2" value="翻新" class="normal-green" onClick="checkForm();">
<%
}
%>
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
    </td>
  </tr>
</table> 
</body>
</html>
