<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%
	long pc_id = StringUtil.getLong(request,"pc_id");
	long ps_id = StringUtil.getLong(request,"ps_id");
	float count = StringUtil.getFloat(request,"lacking_count");
	count = Math.abs(count);
	DBRow[] lackingProducts = productMgrZJ.getLackingProduct(pc_id,ps_id,count);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
 <title>拆货建议</title>
 <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 
 <link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

 <link href="../comm.css" rel="stylesheet" type="text/css">
 <script type="text/javascript">
 	function adviceProduct()
 	{
 		var pcids = "";
		
 		$("[id='check_lacking']").each(function(){
 			if(this.checked)
 			{
 				pcids += this.value+",";
 			}
			
		});
		
 		$.ajax({
			url: 'advice_product.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{pcids:pcids,ps_id:<%=ps_id%>},
			
			beforeSend:function(request){
				
			},
			
			error: function(){
				
			},
			
			success: function(html)
			{
				$("#advice_product").html(html);
			}
		});
 	}
 	
 	function openSplit(storage_name,s_pid,s_pc_id,cid)
	{	
		$.artDialog.open("split_union.html?psid="+cid+"&s_pid="+s_pid+"&s_pc_id="+s_pc_id+"&storage_name="+storage_name+"&cid="+cid, {title: "拆散套装",width:'700px',height:'300px',fixed:true, lock: true,opacity: 0.3});
	}
	
 </script> 
</head>

  <body>
    <table height="100%" width="100%">
    	<tr>
    		<td width="35%" valign="top" height="100%" style="font-size:14px;border-right-width:1px;border-right-style:solid;">
    			<table>
    			<%
    				for(int i = 0;i<lackingProducts.length;i++)
    				{
    			%>
    				<tr>
    					<td valign="middle" nowrap="nowrap">
    						<input onclick="adviceProduct()" id="check_lacking" type="checkbox" checked="checked" value="<%=lackingProducts[i].get("pc_id",0l)%>"/><%=lackingProducts[i].getString("p_name")%>
    					</td>
    					<td align="right" nowrap="nowrap">
    						缺货个数:<%=lackingProducts[i].get("lackingCount",0f)%>
    					</td>
    				</tr>
    			<%
    				}
    			%>
    			</table>
    		</td>
    		<td>
    			<div id="advice_product">
    			</div>
    		</td>
    	</tr>
    </table>
    <script type="text/javascript">
  		adviceProduct();
  	</script>
  </body>
</html>
