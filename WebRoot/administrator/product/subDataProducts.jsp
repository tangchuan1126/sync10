<%@page import="com.cwc.app.key.CodeTypeKey"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.util.StringUtil"%>
<%@page import="com.cwc.db.DBRow"%>
<%@page import="com.cwc.exception.JsonException"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.beans.jqgrid.FilterBean"%>
<%@page import="net.sf.json.JsonConfig"%>
<%@page import="com.cwc.app.beans.jqgrid.RuleBean"%>
<%@page import="com.cwc.json.JsonUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.PriceUOMKey"%>
<jsp:useBean id="priceUOMKey" class="com.cwc.app.key.PriceUOMKey"></jsp:useBean>
<%@ include file="../../include.jsp"%> 
<%	    
	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	
	long pc_id = StringUtil.getLong(request,"id");
	DBRow inSetProducts[] = productMgr.getProductsInSetBySetPid(pc_id);
	
	for(int i = 0;i<inSetProducts.length;i++)
	{
		DBRow[] mainCodes = productCodeMgrZJ.getProductCodeByTypeAndCodePcid(CodeTypeKey.Main, "", inSetProducts[i].get("pc_id", 0L));
		String mainCode = "";
		if(mainCodes.length > 0)
		{
			mainCode = mainCodes[0].getString("p_code");
		}
		StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
		  
	  	DBRow allFather[] = tree.getAllFather(inSetProducts[i].get("catalog_id",0l));
	  	for (int jj=0; jj<allFather.length-1; jj++)
	  	{
	  		catalogText.append("<img src='img/folderopen.gif'/>"+allFather[jj].getString("title")+"<br/>");
	  		String s = (jj==0) ? "<img src='img/joinbottom.gif'/>"  : "<img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>";
	  		catalogText.append(s);
	  	}
	  	
	  	DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(inSetProducts[i].getString("catalog_id")));
	  	if (catalog!=null)
	  	{
	  		catalogText.append("<img  src='img/page.gif'/>"+catalog.getString("title"));
	  	}

		inSetProducts[i].add("catalog_text",catalogText.toString());
		inSetProducts[i].remove("p_img");
		/*
		inSetProducts[i].add("length", (int)inSetProducts[i].get("length",0d)+"("+inSetProducts[i].getString("length_uom")+")");
		inSetProducts[i].add("width", (int)inSetProducts[i].get("width",0d)+"("+inSetProducts[i].getString("length_uom")+")");
		inSetProducts[i].add("heigth", (int)inSetProducts[i].get("heigth",0d)+"("+inSetProducts[i].getString("length_uom")+")");
		inSetProducts[i].add("weight", (int)inSetProducts[i].get("weight",0d)+"("+inSetProducts[i].getString("weight_uom")+")");
		inSetProducts[i].add("unit_price", (int)inSetProducts[i].get("unit_price",0d)+"("+inSetProducts[i].getString("price_uom")+")");
		*/
		inSetProducts[i].add("p_code", mainCode);
		inSetProducts[i].add("gross_profit",(inSetProducts[i].get("gross_profit",0d)*100));
		inSetProducts[i].add("length_uom_name", lengthUOMKey.getLengthUOMKey(inSetProducts[i].get("length_uom", 0)));
		inSetProducts[i].add("weight_uom_name", weightUOMKey.getWeightUOMKey(inSetProducts[i].get("weight_uom", 0)));
	}
	
	DBRow data = new DBRow();
	data.add("page",1);
	data.add("total",1);
	data.add("records",inSetProducts.length);
	data.add("rows",inSetProducts);
	
	DBRow userData = new DBRow();
	userData.add("set_id",pc_id);
	data.add("union_id",pc_id);
	out.println(new JsonObject(data).toString());
%>