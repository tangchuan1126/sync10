<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
long expandID = StringUtil.getLong(request,"expandID");
String title_id = StringUtil.getString(request, "title_id");
DBRow[] titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true, 0L, "", null, request);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Product Category</title>

<style type="text/css" media="all">

	@import "../js/thickbox/global.css";
	@import "../js/thickbox/thickbox.css";
	
	.heightLight a.node, .heightLight{
		color: #ED3D14;
		font-weight: bolder;
	}
	.aui_titleBar{  border: 1px #DDDDDD solid;}
	.aui_header{height: 40px;line-height: 40px;}
	.aui_title{
	  height: 40px;
	  width: 100%;
	  font-size: 1.2em !important;
	  margin-left: 20px;
	  font-weight: 700;
	  text-indent: 0;
	}
	.aui_close{
		color:#fff !important;
		text-decoration:none !important;
	}
	
	.breadnav {
            padding:0 30px; height:25px;margin-bottom: 25px;
        }
        .breadcrumb{
          font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
          font-size: 14px;
          line-height: 1.42857143;
        }
        .breadcrumb {
          margin-top:0;
	  padding: 8px 15px;
          margin-bottom: 20px;
          list-style: none;
          background-color: #fff;
          border-radius: 4px;
        }

        .breadcrumb > li {
          display: inline-block;
        }

        .breadcrumb > .active {
          color: #777;
        }

        .breadcrumb > li + li:before {
            padding: 0 5px;
            color: #ccc;
            content: "/\00a0";
        }
        .breadcrumb a{
		      color: #337ab7;
                text-decoration: none;	
		}
</style>

<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />


<link rel="stylesheet" type="text/css" href="../comm.css" />
<link rel="stylesheet" type="text/css" href="../js/popmenu/menu.css" />
<link rel="stylesheet" type="text/css" href="../dtree.css" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/simple.css" />
<link rel="stylesheet" type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" />
<link rel="stylesheet" type="text/css" href="../js/fullcalendar/chosen.css" />
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<link rel="stylesheet" type="text/css" href="../js/fullcalendar/stateBox.css"  />

<script type="text/javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
<script type="text/javascript" src="../js/art/plugins/jquery.artDialog.source.js" ></script>
<script type="text/javascript" src="../js/art/plugins/iframeTools.source.js"></script>
<script type="text/javascript" src="../js/art/plugins/iframeTools.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/thickbox/thickbox.js"></script>
<script type="text/javascript" src="../js/thickbox/global.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type="text/javascript" src="../js/fullcalendar/chosen.jquery.js"></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<script type="text/javascript" src="../js/zebra/zebra.js" ></script>

<link rel="stylesheet" type="text/css" href="../buttons.css"  />


<style type="text/css" media="all">
	.aui_titleBar{  border: 1px #DDDDDD solid;}
	.aui_header{height: 40px;line-height: 40px;}
	.aui_title{
	  height: 40px;
	  width: 100%;
	  font-size: 1.2em !important;
	  margin-left: 20px;
	  font-weight: 700;
	  text-indent: 0;
	}
	.aui_close{
		color:#fff !important;
		text-decoration:none !important;
	}
</style>


<script language="JavaScript1.2">

document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>");

(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000',
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

function addProductCatalogPage(parentid, parentName){
	
	if(!parentName){parentName="";}
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_category_add.html?parentid="+parentid+"&parentName="+parentName;
	$.artDialog.open(url , {title: 'Add Product Category',width:'600px',height:'380px', lock: true,opacity: 0.3,fixed: true});
}

function addProductCatalog(parentid){
	
	$.prompt(
	
	"<div id='title'>Add Product Category</div><br />Parent Category：<input name='proTextParentTitle' type='text' id='proTextParentTitle' style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br>Add New<br><input name='proTextTitle' type='text' id='proTextTitle' style='width:300px;'>",
	{
	      submit: promptCheckAddProductCatalog,
   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductCatalogJSON.action",
							{parentid:parentid},//{name:"test",age:20},
							function callback(data)
							{
								$("#proTextParentTitle").setSelectedValue(data.title);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
					
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProductCatalog.action";
						document.add_form.parentid.value = parentid;
						document.add_form.title.value = f.proTextTitle;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { Submit: "y", Cancel: "n" }
	});
}

function promptCheckAddProductCatalog(v,m,f){
	
	if (v=="y"){
		
		 if(f.proTextTitle == ""){
			 
				alert("请选填写新建分类名称");
				return false;
		 }
		 
		 return true;
	}
}

function updateProductCategory(id){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/update_product_category.html?id="+id;
	$.artDialog.open(url , {title: 'Rename Product Category',width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}

function modProductCatalog(id){
	
	$.prompt(
	
	"<div id='title'>Mod Category</div><br />Category Name<br><input name='proTextTitle' type='text' id='proTextTitle' style='width:300px;'>",
	
	{

   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductCatalogJSON.action",
							{parentid:id},//{name:"test",age:20},
							function callback(data)
							{
								$("#proTextTitle").setSelectedValue(data.title);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductCatalog.action";
						document.mod_form.id.value = id;
						document.mod_form.title.value = f.proTextTitle;		
						document.mod_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { Submit: "y", Cancel: "n" }
	});
}

function delC(id,title){
	$.artDialog({
	    content: 'Delete '+title+' ?',
	    icon: 'question',
	    width: 260,
	    height: 100,
	    opacity: 0.1,
	    lock: true,
	    fixed:true,
	    title:'Delete Product Category',
	    okVal: 'Confirm',
	    ok: function () {
	    	$("#productCategoryId").val(id);
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/delProductCatalog.action',
				data:$("#del_form").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "true"){
						showMessage("Product Category '"+title+"' Delete Successfully","success");
						setTimeout("refreshWindow()", 1000);
					}else{
						showMessage("Product Category '"+title+"' Can't Delete, It's Have Product","alert");
					}
				},
				error:function(){
					showMessage("System error","error");
				}
			})
	    },
	    cancelVal: 'Cancel',
	    cancel: function(){
	    	
		}
	});
}

function transferToProductLine(productCatalogId,productCatalogName){
	
	$.artDialog({
	    content: 'Transfer To Product Line By '+productCatalogName+' ?',
	    icon: 'question',
	    width: 270,
	    height: 100,
	    lock: true,
        opacity:0.3,
	    title:'Transfer To Product Line',
	    okVal: 'Confirm',
	    ok: function () {
	    	var tdId="#productQty"+productCatalogId;
			var productQty = $(tdId).text();
			if(productQty > 0){
				showMessage("Can not Transfer To Product Line! Current Product Catalog has Product","alert");
			}else {
				$("#productCategoryIdByTransfer").val(productCatalogId);
				$("#productCategoryNameByTransfer").val(productCatalogName);
				$.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/transferToProductLine.action',
					data:$("#transferToProductLine_form").serialize(),
					dataType:'json',
					type:'post',
					success:function(data){
						if(data && data.flag == "true"){
							showMessage("'"+productCatalogName+"' Transfer To Product Line Successfully","success");
							setTimeout("refreshWindow()", 1000);
						}else{
							showMessage("Product Line '"+productCatalogName+"' already exists","alert");
						}
					},
					error:function(){
						showMessage("System error","error");
					}
				})
			}
	    },
	    cancelVal: 'Cancel',
	    cancel: function(){
	    	
		}
	});
}

function moveCatalog(pc_id)
{
	lvl = getCategoryLevel(pc_id);
	//tb_show('移动商品分类','select_product_catalog.html?catalog_id='+pc_id+'&TB_iframe=true&height=500&width=800',false);
	height = getCategoryHeight(pc_id);
	if(height>1){
		showMessage("Cannot move this category as it already has 3 levels.","alert");
	}else{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/select_product_catalog.html?catalog_id="+pc_id+"&level="+lvl+"&height="+height;
		$.artDialog.open(url , {title: 'Move Product Category',width:'800px',height:'500px', lock: true,opacity: 0.3,fixed: true,});
	}
	
}
function changeTitle()
{
	$("#changeTitleForm").submit();
}
function refreshWindow()
{
	window.location.reload();
}
$(function(){
	$(".chzn-select").chosen({no_results_text: "no this option:"});
});

var arr_of_category = new Array();
var height_of_category = new Array();
var level_of_category = new Array();
function checkCategoryLevel(id,parent_id){
	level = 1;
	arr_of_category[id]= parent_id;
	key = id;
	while(arr_of_category[key]){
		
		if(height_of_category[arr_of_category[key]]){
			height_of_category[arr_of_category[key]] = height_of_category[arr_of_category[key]] < getCategoryHeight(key)+1 ? getCategoryHeight(key)+1 : height_of_category[arr_of_category[key]] ;
		}else{
			height_of_category[arr_of_category[key]] = getCategoryHeight(key)+1;
		}
		key=arr_of_category[key];
		level++;
	}
	level_of_category[id]=level;
	return level;
}
function getCategoryHeight(id){
	if(height_of_category[id]){
		return height_of_category[id];
	}
	return 0;
}
function getCategoryLevel(id){
	if(level_of_category[id]){
		return level_of_category[id];
	}
	return 0;
}

function downloadTemplate(){
	
	var url = '../../administrator/product/Category.xlsm';
	
	document.downloadTemplateForm.action = url;
    document.downloadTemplateForm.submit();
}
</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}

/*
* ESO 搜索
*/
.eso_search_parent {
	position: absolute;
	width: 0px;
	height: 0px;
	z-index: 1;
}

.eso_search_icon {
	position: absolute;
	left: 372px;
	top: 5px;
	width: 55px;
	height: 30px;
	z-index: 1;
	visibility: visible;
}

.eso_search_icon img {
	width: 26px;
	height: 26px;
	border: 0;
}

.eso_search_input {
	background: url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width: 408px;
	height: 36px;
	padding-top: 3px;
	padding-left: 3px;
	margin-bottom: 5px;
}

.eso_search_input input {
	background: url(../imgs/search_bg.jpg) repeat 0 0;
	width: 400px;
	height: 30px;
	font-weight: bold;
	border: 1px #bdbdbd solid;
	font-size: 17px;
	font-family: Arial;
	color: #333333;
}

.onMouseHover tr:hover {
	background: #E6F3C5;
	border-bottom: 1px #c5c5c5 dotted;
}

.onMouseHover .zebraTable {
	border-collapse: collapse;
}

.buttons-group {
  display: none;
  list-style: none;
  padding: 0;
  margin: 0;
  zoom: 1;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background: #f0f0f0;">
<form action="" name="downloadTemplateForm" id="downloadTemplateForm"></form>

	<!-- <div style="border-bottom:1px solid #CFCFCF; height:25px;padding-top: 15px;margin-left: 30px;">
		<img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">
		<span style="font-size: 13px;font-weight: bold"> Setup » Product Category</span>
	</div> 
	<div class="breadnav">
		<ol class="breadcrumb">
		            <li><a href="#">Setup</a></li>
		            <li class="active">Product Category</li>
		        </ol>
		</div>-->
	
	<form action="" id="del_form">
		<input type="hidden" name="id" id="productCategoryId">
	</form>

	<form action="" id="transferToProductLine_form">
		<input type="hidden" name="productCategoryIdByTransfer"
			id="productCategoryIdByTransfer"> <input type="hidden"
			name="productCategoryNameByTransfer"
			id="productCategoryNameByTransfer">
	</form>

	<form method="post" name="mod_form">
		<input type="hidden" name="id"> <input type="hidden"
			name="title">
	</form>

	<form method="post" name="add_form">
		<input type="hidden" name="parentid"> <input type="hidden"
			name="title">
	</form>
	<br/>
	<div style="margin-left: 30px; margin-right: 30px; padding: 10px; box-shadow: 0 0 5px #939393; background: #fff; margin-bottom: 8px; height: 40px;">

		<div>
			<div class="eso_search_parent">
				<div class="eso_search_icon">
					<a> <img style="cursor: pointer;" id="eso_search"
						src="../imgs/query.png">
					</a>
				</div>
			</div>
			
			<div style="position: absolute; width: 0px; height: 0px; z-index: 1;">
				<a style="position: relative; left: 425px; top:2px;" name="Submit" class="buttons  primary big" onClick="addProductCatalogPage(0)">
					<i class="icon-plus"></i>&nbsp;Add Category</a>
					
				<a style="position: relative; left: 571px; top:-27px;" name="Submit" class="buttons" onClick="downloadTemplate()"><i class="icon-download-alt"></i>&nbsp;Download Template</a>
			</div>

			<div class="eso_search_input">
				<input id="search_key" name="search_key" type="text" class="ui-autocomplete-input" autocomplete="off" role="textbox"
					aria-autocomplete="list" aria-haspopup="true">
			</div>
			
		</div>
		
	</div>

	<div style="margin-top: 22px; margin-left: 30px; margin-right: 30px; padding: 10px; box-shadow: 0 0 5px #939393; background: #fff; margin-bottom: 8px;">
		<form name="listForm" method="post">
			<table style="width: 100%;" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
				
				<input type="hidden" name="id">
				<input type="hidden" name="parentid">
				<input type="hidden" name="imp_color">
				<tr>
					<th width="80%" colspan="2" style="height: 34px;" class="left-title">Product Category Name</th>
					<th width="10%" style="height: 34px; vertical-align: center; text-align: center;" class="left-title">Product Line Name</th>
					<th width="10%" style="height: 34px; vertical-align: center; text-align: center;" class="right-title">Product Quantity</th>
				</tr>
				<tr>
					<td height="60" colspan="4" class="onMouseHover">
					<script type="text/javascript">
					d = new dTree('d');
					d.add(0,-1,'Product Category Name</td><td align="center" valign="middle" width="15%">&nbsp;</td><td align="left" valign="middle" width="45%"><span id="hiddendTreeRoot"></span></td></tr></table>'
					,'','');
					<%
						DBRow [] parentCategory = proprietaryMgrZyj.findParentProductCatagorysByTitleId(true, 0L, "",title_id,null, request);
						for(int i=0;i<parentCategory.length;i++){	
					%>
						catlevel = checkCategoryLevel(<%=parentCategory[i].getString("id")%>,<%=parentCategory[i].getString("parentid")%>);
						if(catlevel==1){
							d.add(<%=parentCategory[i].getString("id")%>
							,<%=parentCategory[i].getString("parentid")%>
							,'<%=parentCategory[i].getString("title")%></td>'
								+'<td align="left" style="width: 55%;"><div class="buttons-group"><input name="Submit" type="button" class="buttons" onClick="addProductCatalogPage(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')" value="Add"><input name="Submit32" type="button" class="buttons" onClick="delC(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')" value="Del">&nbsp;&nbsp;<input name="Submit3" type="button" class="buttons" onClick="updateProductCategory(<%=parentCategory[i].getString("id")%>)" value="Rename">&nbsp;&nbsp;<input class="buttons" type="button" value="Move" onClick="moveCatalog(<%=parentCategory[i].getString("id")%>)" />&nbsp;&nbsp;<input class="buttons" name="Submit32" type="button" value="Transfer To Product Line" onClick="transferToProductLine(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/>&nbsp;&nbsp; &nbsp;&nbsp;<input type="button" value="Link Product Line" class="buttons" onclick="update_product_line(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/><input type="button" value="Title/Customer" class="buttons" onclick="addTitle(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/></div></td>'
								+'<td align="center" valign="middle" width="10%"><%= parentCategory[i].getString("product_line_name")%></td>'
								+'<td align="center" valign="middle" width="10%" id="productQty<%=parentCategory[i].get("id",0l)%>"><%=productMgrZyj.getProductsByCategoryid(parentCategory[i].get("id",0l),null)%></td></tr></table>'
							,''
							,'<%=parentCategory[i].getString("title")%>');
						}else if(catlevel==2){
							d.add(<%=parentCategory[i].getString("id")%>
							,<%=parentCategory[i].getString("parentid")%>
							,'<%=parentCategory[i].getString("title")%></td>'
							+'<td align="left"  style="width: 55%;"><div class="buttons-group"><input name="Submit" type="button" class="buttons" onClick="addProductCatalogPage(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')" value="Add"><input name="Submit32" type="button" class="buttons" onClick="delC(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')" value="Del">&nbsp;&nbsp;<input name="Submit3" type="button" class="buttons" onClick="updateProductCategory(<%=parentCategory[i].getString("id")%>)" value="Rename">&nbsp;&nbsp;<input class="buttons" type="button" value="Move" onClick="moveCatalog(<%=parentCategory[i].getString("id")%>)" />&nbsp;&nbsp;<input class="buttons" name="Submit32" type="button" value="Transfer To Product Line" onClick="transferToProductLine(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/>&nbsp;&nbsp; <input type="button" value="Title/Customer" class="buttons" onclick="addTitle(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/></div></td>'
							+'<td align="center" valign="middle" width="10%">&nbsp;</td>'
							+'<td align="center" valign="middle" width="10%" id="productQty<%=parentCategory[i].get("id",0l)%>"><%=productMgrZyj.getProductsByCategoryid(parentCategory[i].get("id",0l),null)%></td></tr></table>'
							,''
							,'<%=parentCategory[i].getString("title")%>');
						}else{
							d.add(<%=parentCategory[i].getString("id")%>
							,<%=parentCategory[i].getString("parentid")%>
							,'<%=parentCategory[i].getString("title")%></td>'
							+'<td align="left"  style="width:55%;"><div class="buttons-group"><input name="Submit32" type="button" class="buttons" onClick="delC(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')" value="Del">&nbsp;&nbsp;<input name="Submit3" type="button" class="buttons" onClick="updateProductCategory(<%=parentCategory[i].getString("id")%>)" value="Rename">&nbsp;&nbsp;<input class="buttons" type="button" value="Move" onClick="moveCatalog(<%=parentCategory[i].getString("id")%>)" />&nbsp;&nbsp;<input class="buttons" name="Submit32" type="button" value="Transfer To Product Line" onClick="transferToProductLine(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/><input type="button" value="Title/Customer" class="buttons" onclick="addTitle(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/></div></td>'
							+'<td align="center" valign="middle" width="10%">&nbsp;</td>'
							+'<td align="center" valign="middle" width="10%" id="productQty<%=parentCategory[i].get("id",0l)%>"><%=productMgrZyj.getProductsByCategoryid(
						parentCategory[i].get("id", 0l), null)%></td></tr></table>'
							,''
							,'<%=parentCategory[i].getString("title")%>');
						}
					<%}%>
					document.write(d);
					
					//隐藏跟节点
					$("#hiddendTreeRoot").parent().parent().parent().parent().hide();
					
					$("#search_key").on("change",function(evt){
						
						var txt = $(evt.target).val().trim().toLowerCase();
						
						var array = d.aNodes;
						
						d.closeAll();
						
						for(var i in array){
							
							var name = array[i].title.toLowerCase();
							
							$("#id"+array[i]._ai).parent().removeClass("heightLight");
							
							if(txt!=='' && name.indexOf(txt)>=0){
								
								$("#id"+array[i]._ai).parent().addClass("heightLight");
								
								d.openTo(array[i].id,false);
							}
						}
					});
					
					$("#eso_search").on("click",function(evt){
						
						var txt = $("#search_key").val().trim().toLowerCase();
						
						var array = d.aNodes;
						
						d.closeAll();
						
						for(var i in array){
							
							var name = array[i].title.toLowerCase();
							
							$("#id"+array[i]._ai).parent().removeClass("heightLight");
							
							if(txt!=='' && name.indexOf(txt)>=0){
								
								$("#id"+array[i]._ai).parent().addClass("heightLight");
								
								d.openTo(array[i].id,false);
							}
						}
					});
					
				</script>
				
				<% if(parentCategory.length == 0){ %>
					
					<div style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Data.</div>
				<% }%>
				
				</td>
				</tr>
			</table>
		</form>
	</div>
<br>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/moveProductCatalog.action" name="move_catalog_form">
	<input type="hidden" name="move_to" id="move_to" /> 
	<input type="hidden" name="catalog_id" id="catalog_id" />
</form>
<form action="" name="download_form" id="download_form"></form>
<script>

$(document).ready(function() {
	
	$('.onMouseHover .dTreeNode').bind('mouseover', function(evt) {
		
		$(evt.target).parents(".dTreeNode").find(".buttons-group").show();
	});
	
	$('.onMouseHover .dTreeNode').bind('mouseout', function(evt) {
		
		$(evt.target).parents(".dTreeNode").find(".buttons-group").hide();
	});
});
	
function update_product_line(id,name){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/category_link_product_line.html?category_id="+id+"&category_name="+name; 
	 $.artDialog.open(uri , {title: 'Link Product Line',width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}
function addTitle(id,name){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_catalog_linked_title.html?productCatalogId="+id+"&productCatalogName="+name;
	 $.artDialog.open(uri , {title: 'Title / Customer',width:'600px',height:'450px', lock: true,opacity: 0.3,fixed: true});
}

function showTitle(id,name){
	
	 var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_line_show_title.html?id="+id+"&name="+name;
	
	//console.log(url);
	$.artDialog.open(url,{title:'Show Title',width:'600px',height:'450px',lock:true,opacity:0.3,fixed:true});
}
function down(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	var title_id=$('#title_id').val();
	var para='title_id='+title_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxDownCatalogAndTitleAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();       //遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}
		}
	});
}

function upload(_target){
	var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"xls",
	     limitSize:2,
	     target:_target,
	     limitNum:1
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: 'Upload',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames){
	if($.trim(fileNames).length > 0 ){
	   var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/dialog_product_catalog_title_upload.html?fileName='
					+ fileNames;
			$.artDialog.open(uri, {
				title : '上传产品分类TITLE关系',
				width : '600px',
				height : '300px',
				lock : true,
				opacity : 0.3,
				fixed : true
			});
		}
	}
</script>
</body>
</html>
