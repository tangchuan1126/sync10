<%@page import="java.util.Map"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.DifferentKey"%>
<%@ include file="../../include.jsp"%> 
<%
	Map<String,DBRow[]> errorResult = initDataMgrZJ.initData(request);
	DBRow[] errorProduct = errorResult.get("errorProduct");
	DBRow[]	errorTitle = errorResult.get("errorTitle");
	DBRow[] errorSKUTitle = errorResult.get("errorSKUTitle");
	DBRow[] errorCategoryTitle = errorResult.get("errorCategoryTitle");
	DBRow[] errorContainerType = errorResult.get("errorContainerType");
	DBRow[] errorBLP = errorResult.get("errorBLP");
	DBRow[] errorCLP = errorResult.get("errorCLP");
	DBRow[] errorShipTo = errorResult.get("errorShipTo");
	DBRow[] errorShipToAddress = errorResult.get("errorShipToAddress");
	DBRow errorFile = errorResult.get("errorFile")[0];
	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script language="javascript">
$(function(){
	 $("#initData").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
		});	
 })
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp;Setup Init</td>
  </tr>
</table>
<br>
	<div id="initData">
		 <ul>
			 <li><a href="#errorSKU">Error SKU</a></li>
			 <li><a href="#errorTitle">Error TITLE</a></li>
			 <li><a href="#errorSKUTitle">Error SKU&TITLE</a></li>
			 <li><a href="#errorCagegoryTitle">Error CATEGORY&TITLE</a></li>
			 <li><a href="#errorContainerType">Error CONTAINERTYPE</a></li>
			 <li><a href="#errorBLP">Error BLP</a></li>
			 <li><a href="#errorCLP">Error CLP</a></li>
			 <li><a href="#errorShipTo">Error ShipTo</a></li>
			 <li><a href="#errorShipToAddress">Error ShipToAddress</a></li>
		 </ul>
		 <div id="errorSKU">
		 	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" style="font-size:8px">
		 		<tr>
		 			<td nowrap="nowrap">Line</td>
		 			<td nowrap="nowrap">Reason</td>
		 			<td nowrap="nowrap">LOT</td>
		 			<td nowrap="nowrap">SKU</td>
		 			<td nowrap="nowrap">Key Code</td>
		 			<td nowrap="nowrap">CATEGORY</td>
		 			<td nowrap="nowrap">UPC</td>
		 			<td nowrap="nowrap">UOM</td>
		 			<td nowrap="nowrap">Piece Depth<br/>(Inches)</td>
		 			<td nowrap="nowrap">Piece Height<br/>(Inches)</td>
		 			<td nowrap="nowrap">Piece Width<br/>(Inches)</td>
		 			<td nowrap="nowrap">Weight per unit<br/>(LBS)</td>
		 			<td nowrap="nowrap">Unit Cost</td>
		 			<td nowrap="nowrap">Class</td>
		 			<td nowrap="nowrap">NMFC Code</td>
		 		</tr>
		 		<%
		 			for(int i = 0;i<errorProduct.length;i++)
		 			{
		 		%>
		 		<tr>
		 			<td nowrap="nowrap"><%=errorProduct[i].get("line",0)+1%></td>
		 			<td><font color="red"><%=errorProduct[i].getString("reason")%></font></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("lot")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("sku")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("key_code")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("category")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("upc")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("uom")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("length")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("height")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("width")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("weight")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("unit_price")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("freight_class")%></td>
		 			<td nowrap="nowrap"><%=errorProduct[i].getString("nmfc_code")%></td>
		 		</tr>
		 		<%
		 			}
		 		%>
		 	</table>
		 </div>
		 <div id="errorTitle">
		 	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 		<tr>
		 			<td>Line</td>
		 			<td>Reason</td>
		 			<td>TITLE</td>
		 		</tr>
		 		<%
		 			for(int i = 0;i<errorTitle.length;i++)
		 			{
		 		%>
		 		<tr>
		 			<td><%=errorTitle[i].get("line",0)+1%></td>
		 			<td><font color="red"><%=errorTitle[i].getString("reason")%></font></td>
		 			<td><%=errorTitle[i].getString("title_name")%></td>
		 		</tr>
		 		<%
		 			}
		 		%>
		 	</table>
		 </div>
		 <div id="errorSKUTitle">
		 	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 		<tr>
		 			<td>Line</td>
		 			<td>Reason</td>
		 			<td>LOT</td>
		 			<td>SKU</td>
		 			<td>TITLE</td>
		 		</tr>
		 		<%
		 			for(int i = 0;i<errorSKUTitle.length;i++)
		 			{
		 		%>
		 			<tr>
			 			<td><%=errorSKUTitle[i].get("line",0)+1%></td>
			 			<td><font color="red"><%=errorSKUTitle[i].getString("reason")%></font></td>
			 			<td><%=errorSKUTitle[i].getString("lot")%></td>
			 			<td><%=errorSKUTitle[i].getString("sku")%></td>
			 			<td><%=errorSKUTitle[i].getString("title")%></td>
			 		</tr>
		 		<%
		 			}
		 		%>
		 		</table>
		 </div>
		 <div id="errorCagegoryTitle">
		 	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 		<tr>
		 			<td>Line</td>
		 			<td>Reason</td>
		 			<td>TITLE</td>
		 			<td>Category</td>
		 		</tr>
		 		<%
		 			for(int i = 0;i<errorCategoryTitle.length;i++)
		 			{
		 		%>
		 			<tr>
		 				<td><%=errorCategoryTitle[i].get("line",0)+1%></td>
		 				<td><font color="red"><%=errorCategoryTitle[i].getString("reason")%></font></td>
		 				<td><%=errorCategoryTitle[i].getString("title_name")%></td>
		 				<td><%=errorCategoryTitle[i].getString("catalog_title")%></td>
		 			</tr>
		 		<%
		 			}
		 		%>
		 	</table>
		 </div>
		 <div id="errorContainerType">
		 	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 		<tr>
		 			<td>Line</td>
		 			<td>Reason</td>
		 			<td>ContainerType</td>
		 			<td>Length(INCH)</td>
		 			<td>Width(INCH)</td>
		 			<td>Height(INCH)</td>
		 			<td>Pallet Weight(LB)</td>
		 			<td>Total Weight(LB)</td>
		 		</tr>
		 		<%
		 			for(int i = 0;i<errorContainerType.length;i++)
		 			{
		 		%>
		 			<tr>
		 			<td><%=errorContainerType[i].get("line",0)+1%></td>
		 			<td><font color="red"><%=errorContainerType[i].getString("reason")%></font></td>
		 			<td><%=errorContainerType[i].getString("type_name")%></td>
		 			<td><%=errorContainerType[i].getString("length")%></td>
		 			<td><%=errorContainerType[i].getString("width")%></td>
		 			<td><%=errorContainerType[i].getString("height")%></td>
		 			<td><%=errorContainerType[i].getString("weight")%></td>
		 			<td><%=errorContainerType[i].getString("max_load")%></td>
		 			</tr>
		 		<%
		 			}
		 		%>
		 	</table>
		 </div>
		 <div id="errorBLP">
		 	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 		<tr>
		 			<td>Line</td>
		 			<td>Reason</td>
		 			<td>SKU</td>
		 			<td>Inner Type</td>
		 			<td>Inner Type Name</td>
		 			<td>Inner Length</td>
		 			<td>Inner Widht</td>
		 			<td>Inner Height</td>
		 			<td>ContainerType</td>
		 		</tr>
		 		<%
		 			for(int i = 0; i<errorBLP.length;i++)
		 			{
		 		%>
		 			<tr>
		 				<td><%=errorBLP[i].get("line",0)+1%></td>
		 				<td><font color="red"><%=errorBLP[i].getString("reason")%></font></td>
		 				<td><%=errorBLP[i].getString("p_name")%></td>
		 				<td><%=errorBLP[i].getString("inner_type")%></td>
		 				<td><%=errorBLP[i].getString("inner_type_id")%></td>
		 				<td><%=errorBLP[i].getString("inner_length")%></td>
		 				<td><%=errorBLP[i].getString("inner_width")%></td>
		 				<td><%=errorBLP[i].getString("inner_height")%></td>
		 				<td><%=errorBLP[i].getString("container_type_name")%></td>
			 		</tr>
		 		<%
		 			}
		 		%>
		 	</table>
		 </div>
		 <div id="errorCLP">
		 	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 		<tr>
		 			<td>Line</td>
		 			<td>Reason</td>
		 			<td>SKU</td>
		 			<td>Ship To</td>
		 			<td>Inner Type</td>
		 			<td>Inner Type Name</td>
		 			<td>Inner Length</td>
		 			<td>Inner Widht</td>
		 			<td>Inner Height</td>
		 			<td>Title</td>
		 			<td>ContainerType</td>
		 		</tr>
		 		<%
		 			for(int i = 0;i<errorCLP.length;i++)
		 			{
		 		%>
		 			<tr>
		 				<td><%=errorCLP[i].get("line",0)+1 %></td>
		 				<td><font color="red"><%=errorCLP[i].getString("reason") %></font></td>
		 				<td><%=errorCLP[i].getString("p_name") %></td>
		 				<td><%=errorCLP[i].getString("ship_to") %></td>
		 				<td><%=errorCLP[i].getString("inner_type") %></td>
		 				<td><%=errorCLP[i].getString("box_type") %></td>
		 				<td><%=errorCLP[i].getString("inner_length") %></td>
		 				<td><%=errorCLP[i].getString("inner_width") %></td>
		 				<td><%=errorCLP[i].getString("inner_height") %></td>
		 				<td><%=errorCLP[i].getString("title") %></td>
		 				<td><%=errorCLP[i].getString("container_type") %></td>
			 		</tr>
		 		<%
		 			}
		 		%>
		 	</table>
		 </div>
		 <div id="errorShipTo">
		 	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 		<tr>
		 			<td>Line</td>
		 			<td>Reason</td>
		 			<td>ShipTo</td>
		 		</tr>
		 		<%
		 			for(int i = 0;i<errorShipTo.length;i++)
		 			{
		 		%>
		 			<tr>
		 				<td><%=errorShipTo[i].get("line",0)+1%></td>
		 				<td><font color="red"><%=errorShipTo[i].getString("reason")%></font></td>
		 				<td><%=errorShipTo[i].getString("ship_to_name")%></td>
		 			</tr>
		 		<%
		 			}
		 		%>
		 	</table>
		 </div>
		 <div id="errorShipToAddress">
		 	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 		<tr>
		 			<td>Line</td>
		 			<td>Reason</td>
		 			<td>AccountID</td>
		 			<td>Name</td>
		 			<td>Address1</td>
		 			<td>Address2</td>
		 			<td>City</td>
		 			<td>State</td>
		 			<td>ZipCode</td>
		 			<td>Country</td>
		 		</tr>
		 		<%
		 			for(int i = 0;i<errorShipToAddress.length;i++)
		 			{
		 		%>
		 			<tr>
		 				<td><%=errorShipToAddress[i].get("line",0)+1%></td>
		 				<td><font color="red"><%=errorShipToAddress[i].getString("reason")%></font></td>
		 				<td><%=errorShipToAddress[i].getString("ship_to_name")%></td>
		 				<td><%=errorShipToAddress[i].getString("storage_title")%></td>
		 				<td><%=errorShipToAddress[i].getString("street")%></td>
		 				<td><%=errorShipToAddress[i].getString("home_number")%>&nbsp;</td>
		 				<td><%=errorShipToAddress[i].getString("city")%></td>
		 				<td><%=errorShipToAddress[i].getString("state")%></td>
		 				<td><%=errorShipToAddress[i].getString("zip_code")%></td>
		 				<td><%=errorShipToAddress[i].getString("country_code")%></td>
		 			</tr>
		 		<%
		 			}
		 		%>
		 		</table>
		 </div>
	</div>
	<div align="right" style="width:100%;padding-top: 10px;">
		<form action="<%=ConfigBean.getStringValue("systenFolder")+errorFile.getString("file_path")%>" name="download_form">
		</form>
		<input  type="button" class="error-big" value="DownLoadError" onclick="document.download_form.submit();"/>
		<input type="button" class="normal-green" value="Close" onclick="$.artDialog.close();"/>
	</div>
</body>
</html>
