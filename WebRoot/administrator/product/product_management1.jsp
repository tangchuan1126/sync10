<%@page import="com.cwc.app.key.GlobalKey"%>
<%@page import="com.cwc.app.key.MoreLessOrEqualKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="javax.mail.Transport"%>
<%@page import="com.cwc.app.beans.AdminLoginBean"%>
<%@include file="../../include.jsp"%> 
<%@taglib uri="/turboshop-tag" prefix="tst" %>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long adminId=adminLoggerBean.getAdid();

	//根据登录帐号判断是否为客户
	Boolean bl=DBRowUtils.existDeptAndPostInLoginBean(GlobalKey.CLIENT_ADGID, adminLoggerBean);//如果bl 是true 说明是客户登录
	//如果bl 是true 说明是客户登录
	//如果是用户登录 查询用户下的产品
	DBRow[] titles;
	DBRow[] p1;
	long number=0;
	if(bl){
		number=1;
		titles=adminMgrZwb.getProprietaryByadminId(adminLoggerBean.getAdid());
		
	}else{
		number=0;
		titles=mgrZwb.selectAllTitle();
	}
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript" ></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/simple.css" />          

 
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<title>Configuration</title>
<style>
.content{padding:10px;}
.aui_titleBar{  border: 1px #DDDDDD solid;}
.aui_header{height: 40px;line-height: 40px;}
.aui_title{
  height: 40px;
  width: 100%;
  font-size: 1.2em !important;
  margin-left: 20px;
  font-weight: 700;
  text-indent: 0;
}

 <!-- 高级搜索的样式-->
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;	
	border:1px #bdbdbd solid;	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

.ui-widget-content{
	border:1px #CFCFCF solid;
	border-radius: 0px;
  	-webkit-border-radius: 0px;
	-moz-border-radius:0;
}
.ui-widget-header{
	background:#f1f1f1 !important;
	border-radius: 0;
	border-top:0;
	border-left:0;
	border-right:0;
	border-bottom: 1px #CFCFCF solid;
}
.ui-tabs .ui-tabs-nav li a{
	padding: .5em 1em 13px !important;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
	  height: 40px;
	  margin-top: -4px !important;
	background:#fff;
}
.ui-tabs .ui-tabs-nav{
	padding :0 .2em 0;
}
.ui-tabs{
	padding:0;
}
</style>
<script type="text/javascript">

function down(){
	var catagoryIdSel = 0;
	if(catalogThreeId*1 > 0)
	{
		catagoryIdSel = catalogThreeId;
	}
	else if(catalogTwoId*1 > 0)
	{
		catagoryIdSel = catalogTwoId;
	}
	else if(catalogOneId*1 > 0)
	{
		catagoryIdSel = catalogOneId;
	}
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var para='pcid='+catagoryIdSel+'&cmd='+cmd+'&union_flag='+productType+'&pro_line_id='+lineId+'&title_id='+titleId;
	//alert(para);
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxDownProductAndTitleAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();       //遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}	
		}
	});
}

	function importProductTitles(_target)
	{
	    var fileNames = $("#file_names").val();
	    var obj  = {
		     reg:"xls",
		     limitSize:2,
		     limitNum:1
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: 'Upload Files',width:'770px',height:'530px', lock: true,opacity: 0.1,fixed: true});
	}
	
	function importInitData()
	{
	    var fileNames = $("#file_names").val();
	    var obj  = {
		     reg:"xlsm",
		     limitSize:2,
		     limitNum:1,
		     target:"initData"
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames.length > 0 )
		 {
			uri += "&file_names=" + fileNames;
		 }
		 $.artDialog.open(uri , {id:'file_up',title: 'Upload Files',width:'770px',height:'530px', lock: true,opacity: 0.1,fixed: true});
	}
	
	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,target)
	{
		if($.trim(fileNames).length > 0 )
		{
			if(target=="")
			{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/check_product_title_show.html?fileNames="+fileNames;
				$.artDialog.open(uri,{title: "检测上传用户title信息",width:'850px',height:'530px', lock: true,opacity: 0.1,fixed: true});
			}
			else
			{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/initData_show.html?file_name="+fileNames;
				$.artDialog.open(uri,{title: "Basic Data Initialize",width:'1200px',height:'700px', lock: true,opacity: 0.1,fixed: true});
			}
		}
	}
	function showProductDetails(pcid, pc_name)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_details.html?pc_id="+pcid;
		$.artDialog.open(uri,{title: pc_name,width:'850px',height:'530px', lock: true,opacity: 0.1,fixed: true});
	}
	function deleteBasicDatas()
	{
		var d = $.artDialog({
			lock: true,opacity: 0.1,fixed: true,
		    title: 'Delete Basic Data',
		    content: 'Delete all data except the titles ?',
		    button:[
					{
						name: 'Sure',
						callback: function () {
							ajaxDeleteBasicDatas(d);
							return false ;
						},
						focus:true
						
					},
					{
						name: 'Cancel'
					}]
		});
		d.show();
	}
	//删除
	function ajaxDeleteBasicDatas(d){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/DeleteAllBasicDatasExceptTitlesAction.action',
			type:'post',
			dataType:'json',
			beforeSend:function(){
				 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
		    success:function(data){
			    $.unblockUI();
			    d.close();
			    if(data && data.flag == "true"){
			    	refreshWindow();
			    }
		    },
			error:function(){
		    	$.unblockUI();
				showMessage("System error","error");
		    }
		});
	}
// 	function test(){
<%-- 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/lable_template/lable_template_ui_show.html"; --%>
// 		$.artDialog.open(uri,{title: 'Test',width:'850px',height:'530px', lock: true,opacity: 0.3,fixed: true});
// 	}
	
	
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div style="border-bottom:1px solid #CFCFCF; height:25px;margin-top: 15px;margin-left: 10px;margin-bottom: 3px;">
		<img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">
		<span style="font-size: 13px;font-weight: bold"> Setup » Configuration</span>
</div>
<div class="content">
<!-- <input type="button" value="点击" onclick="test()"/> -->
<form action="" name="download_form" id="download_form"> </form>
	
	<div id="tabs">
		<ul>
			<li><a href="#av1">Advanced Search</a></li>
			<li><a href="#av2">Product Name Search</a></li>		 
		</ul>
		<div id="av1">
				<div id="tiaojian" style="border-bottom:1px #CCC dashed; display:none;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Condition：</span>
							</td>
							<td width="90%" valign="middle" >
								<div id="condition" style="min-width; float:left"></div>
							    	<div style="float:left">							    	 
							    		<a value="Search" style="margin-left:10px" class="buttons big primary" onclick="seachProductByCondition()"><i class="icon-search"></i>&nbsp;Search</a> 
							    	</div>
							</td>
						</tr>
					</table>
				</div>	
				<div id="title"  style="border-bottom:1px #CCC dashed;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">TITLE：</span>
							</td>
							<td width="90%" valign="middle">
							    <div style="overflow:auto" id="titleCondition">
									<%for(int a=0;a<titles.length;a++){ %> 
								 	  <div style="width:200px;float:left;margin-left:10px; height:20px">
								 	  	 <a href="javascript:void(0)" onclick="addCondition(<%=titles[a].get("title_id",0l)%>,'<%=titles[a].getString("title_name")%>','title')" style="color:#069;text-decoration:none;outline:none;">
								 	  	 	<%=titles[a].getString("title_name")%>							 	  	 						
								 	  	 </a>
								 	  </div>
									<%}%>				
								</div>
								<script>
									//计算高度 超过出现滚动条
									function clertHeight(id){
										 $('#'+id).css("height","");
									}
									function computeHeight(id){									 
									   var $av=$('#'+id);
									   var height=$av.height();									
									   if(height>=60){
										  $av.css("height","60px");
									   }
									}
									computeHeight('titleCondition');
								</script>
							</td>
						</tr>
					</table>
				</div>
				<div id="line"  style="border-bottom:1px #CCC dashed; display:none;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Product Line：</span>
							</td>
							<td width="90%" valign="middle">
								 <div style="overflow:auto" id="lineCondition">
								 </div>
							</td>
						</tr>
					</table>
				</div>
				<div id="catalogOne"  style="border-bottom:1px #CCC dashed; display:none;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Parent Category：</span>
							</td>
							<td width="90%" valign="middle" >
								 <div style="overflow:auto;" id="catalogOneCondition">
								 </div>
							</td>
						</tr>
					</table>
				</div>
				<div id="catalogTwo"  style="border-bottom:1px #CCC dashed; display:none;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Sub Category：</span>
							</td>
							<td width="90%" valign="middle" >
								<div style="overflow:auto" id="catalogTwoCondition">
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div id="catalogThree"  style="border-bottom:1px #CCC dashed; display:none;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Sub-Sub Category：</span>
							</td>
							<td width="90%" valign="middle">
								<div style="overflow:auto" id="catalogThreeCondition">
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div id="productType"  style="border-bottom:1px #CCC dashed;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Product Type：</span>
							</td>
							<td width="90%" valign="middle">
								<div style="width:95px;float:left;margin-left:10px;">
							       	<a href="javascript:void(0)" onclick="addCondition(0,'ITEM','productType')" style="color:#069;text-decoration:none;outline:none;">ITEM</a>
							    </div>
							    <div style="width:95px;float:left;margin-left:10px;">
							       	<a href="javascript:void(0)" onclick="addCondition(1,'UNIT','productType')" style="color:#069;text-decoration:none;outline:none;">UNIT</a>
							    </div>		
							</td>
						</tr>
					</table>
			    </div>
		</div>
		<div id="av2">
		    <!-- 名字搜索 -->
	        <div id="easy_search_father">
				<div id="easy_search" style="margin-top:-2px;">
					<a href="javascript:query()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a>
				</div>
		    </div>
	           <table width="400" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="418">
						<div  class="search_shadow_bg">
						 <input name="seachInput" id="seachInput" value=""  type="text" class="search_input" style="font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)query()"/>
						</div>
					</td>
				</tr>
			  </table>	
	    </div>
	</div>
	<script type="text/javascript">  
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	</script>
	
	<div id="result">查询结果显示</div>
	<!-- 隐藏值 -->
	<input type="hidden" id="hiddenProductLineId" value=""/>
	<!-- 分页用隐藏值 -->
	<input type="hidden" id="lineId" />
	<input type="hidden" id="catalogId" />
	<input type="hidden" id="productType" />
	<input type="hidden" id="cmd" />
	<!-- 刷新画面用隐藏值 -->
	<input type="hidden" id="hiddenPageName" value=""/>
	<input type="hidden" id="hiddenPcId" value=""/>
	<input type="hidden" id="hiddenPcName" value=""/>
</div>
</body>
</html>
<script>
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.1' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

jQuery(function($){
    $("#tabs").tabs("select",0);  //默认选中第一个
    addAutoComplete($("#seachInput"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
});

(function(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html?titleId='+titleId,
		type: 'post',
		dataType: 'html',
		async:false,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
})();

function go(p){
	var lineId=$('#lineId').val();
	var catalogId=$('#catalogId').val();
	var cmd=$('#cmd').val();
	var productType=$('#productType').val();
	var params = {p:p,lineId:lineId,catalogId:catalogId,productType:productType,titleId:titleId,cmd:cmd};
	var str = jQuery.param(params);
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html',
		type: 'post',
		dataType: 'html',
		async:false,
		data:str,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
}
var lineId=0;
var catalogOneId=0;
var catalogTwoId=0;
var catalogThreeId=0;
var productType=0;
var params='';
var catalogId=0;
var cmd='';
var titleId=0;
var seachValue='';
//根据条件查询商品
function seachProductByCondition(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开

	lineId=0;
	catalogOneId=0;
	catalogTwoId=0;
	catalogThreeId=0;
	productType=0;
	params='';
	catalogId=0;
	cmd='seach';
	titleId=0;
	if($("div[type='line']",$('#condition')).html()!=null){
		lineId=$("div[type='line']",$('#condition')).attr("zhi");
	}
	if($("div[type='catalogOne']",$('#condition')).html()!=null){
		catalogOneId=$("div[type='catalogOne']",$('#condition')).attr("zhi");
	} 
	if($("div[type='catalogTwo']",$('#condition')).html()!=null){
		catalogTwoId=$("div[type='catalogTwo']",$('#condition')).attr("zhi");
	} 
	if($("div[type='catalogThree']",$('#condition')).html()!=null){
		catalogThreeId=$("div[type='catalogThree']",$('#condition')).attr("zhi");
	} 
	if($("div[type='productType']",$('#condition')).html()!=null){
		productType=$("div[type='productType']",$('#condition')).attr("zhi");
	} 
	if($("div[type='title']",$('#condition')).html()!=null){
		titleId=$("div[type='title']",$('#condition')).attr("zhi");
	}
	if(catalogThreeId!=0){
		catalogId=catalogThreeId;
	}else if(catalogTwoId!=0){
		catalogId=catalogTwoId;
	}else{
		catalogId=catalogOneId;
	}
	params = {lineId:lineId,catalogId:catalogId,productType:productType,titleId:titleId,cmd:cmd};

	$('#lineId').val(lineId);
	$('#catalogId').val(catalogId);
	$('#cmd').val('seach');
	$('#productType').val(productType);
	
	var str = jQuery.param(params);
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html',
		type: 'post',
		dataType: 'html',
		async:false,
		data:str,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
}

//高级查询控件事件
function query(){
	seachValue=$('#seachInput').val();  //获取文本框值
	cmd='name';
	var str='cmd='+cmd+'&seachValue='+seachValue+'&titleId='+titleId;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html',
		type: 'post',
		dataType: 'html',
		async:false,
		data:str,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});
}

// 商品的CLP信息的导出
function exportProductClp(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '}); //遮罩打开
	var para='cmd='+cmd+'&productType='+productType+'&lineId='+lineId+'&catalogId'+catalogId+'&titleId='+titleId+'&seachValue='+seachValue;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/box/AjaxDownCLPAndTitleAndShipToAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();//遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}else{
				showMessage("System error","error");
			}
		},
		error:function(){
			$.unblockUI();//遮罩关闭
			showMessage("System error","error");
		}
	});
}

//title 点击更多时触发
function morePcTitles(pc_id, p_name){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_add_title.html?pc_id="+pc_id;
	$.artDialog.open(url , {title: 'Manage ['+p_name+'] Linked Title',width:'600px',height:'450px', lock: true,opacity: 0.1,fixed: true,close:function(){loadTitlesByPcId(pc_id);}});
}

function refreshWindow(){
	if($("#hiddenPageName").val() == "product_management_show_list"){
		loadClpsByPcId($("#hiddenPcId").val(),$("#hiddenPcName").val());
	} else {
		window.location.reload();
	}
	$("#hiddenPageName").val("");
	$("#hiddenPcId").val("");
	$("#hiddenPcName").val("");
}

// 局部刷新Title区域
function loadTitlesByPcId(pc_id) {
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '}); //遮罩打开
	var para = "pc_id="+pc_id;
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/FindProprietaryByPcIdAction.action',
		type:'post',
		dataType:'json',
		data:para,
		async:false,
		success:function(data){
			$.unblockUI();//遮罩关闭
			$("#myulPcTitles_"+pc_id).html("");
			var html = '';
			if(data && data.length > 0)
			{
				var len = data.length > 5 ? 5: data.length;
				for(var i = 0; i < len; i ++)
				{
					html += '<li style="margin-left:1px; height:20px">';
					html += data[i].title_name;
					html += '</li>';
				}
				if(data.length > 5)
				{
					$("#titleMore_"+pc_id).html("&nbsp;|&nbsp;More&nbsp;["+data.length+"]");
					$("#titleMore_"+pc_id).css("display","");
				}
				else
				{
					$("#titleMore_"+pc_id).css("display","none");
				}
			}
			if('' == html)
			{
				html += '<li style="height: 35px;"></li>';
			}
			$("#myulPcTitles_"+pc_id).html(html);
		},
		error:function(){
			$.unblockUI();//遮罩关闭
			showMessage("System error","error");
		}
	});
}

//局部刷新CLP Type区域
function loadClpsByPcId(pc_id,pc_name){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '}); //遮罩打开
	var para = "pc_id="+pc_id;
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/container/ClpGetByPcidAction.action',
		type:'post',
		dataType:'json',
		data:para,
		async:false,
		success:function(data) {
			$.unblockUI();//遮罩关闭
			$("#clpType_"+pc_id).html("");
			var html = '';
			if(data && data.length > 0) {
				var len = data.length > 2 ? 2: data.length;
				for(var i = 0; i < len; i ++) {
					html += '<fieldset style="border:2px #930 solid;padding:0 7px 7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px;">';
					html += '<legend style="color:green;font-weight:bold;text-decoration:underline;cursor:pointer;" onclick="openLpType(&quot;'+data[i].lpt_id+'&quot;,&quot;'+pc_name+'&quot;);">';
					html += data[i].lp_name;
					html += '</legend>';
					html += '<table style="width:100%;height:100%">';
					html += '<tr>';
					html += '<td style="background:none;border-bottom:none;width:50%;">';
					html += '<div>';
					html += '<div class="box left width30"><span>Inner Type:</span></div>';
					if(data[i].inner_pc_or_lp > 0){
						html += '<div class="box right width60">';
						html += '<a href="javascript:void(0)" style="color:green;" onclick="openLpType(&quot;'+data[i].inner_pc_or_lp+'&quot;,&quot;'+pc_name+'&quot;);">';
						html += '<span style="color:#0066FF">';
						if(data[i].inner_clp){
							html += data[i].inner_clp.lp_name;
						}
						html += '</span></a></div>';
					}else{
						html += '<div class="box right width60"><span style="color:#0066FF">Product</span></div>';
					}
					html += '<div class="clear"></div>';
					html += '</div>';
					html += '<div>';
					html += '<div class="box left width30"><span>Product Qty:</span></div>';
					html += '<div class="box right width60"><span style="color:#0066FF">';
					html += data[i].inner_total_pc;
					html += '</span></div>';
					html += '<div class="clear"></div>';
					html += '</div></td>';
					html += '<td style="background:none;border-bottom:none;padding-bottom:20px;width:50%;">';
					if(data[i].clp_titles && data[i].clp_titles.length > 0){
						html += '<table style="width:100%;height:100%;border-collapse: collapse;" class="innerTable">';
						html += '<tr>';
						html += '<td width="33%" style="vertical-align:center;text-align:center;" >Title</td>';
						html += '<td width="33%" style="vertical-align:center;text-align:center;" >ShipTo</td>';
						html += '<td width="34%" style="vertical-align:center;text-align:center;" >Operation</td>';
						html += '</tr>';
						var titleLen = data[i].clp_titles.length > 2 ? 2: data[i].clp_titles.length;
						for(var j = 0; j < titleLen; j ++) {
							html += '<tr>';
							html += '<td width="33%" style="vertical-align:left;text-align:left;">';
							html += data[i].clp_titles[j].clp_title.title_name;
							html += '</td>';
							html += '<td width="33%" style="vertical-align:left;text-align:left;">';
							html += data[i].clp_shiptos[j].clp_shipto.ship_to_name;
							html += '</td>';
							html += '<td width="34%" style="vertical-align:left;text-align:left;">';
							html += '<a href="javascript:void(0)" style="color: green;" onclick="openClpTitleShipTo(&quot;'+data[i].clp_titles[j].clp_title.title_id+'&quot;,&quot;'+data[i].clp_shiptos[j].clp_shipto.ship_to_id+'&quot;,&quot;'+data[i].clp_titles[j].clp_title.title_name+'&quot;,&quot;'+data[i].clp_shiptos[j].clp_shipto.ship_to_name+'&quot;,&quot;'+pc_id+'&quot;,&quot;'+pc_name+'&quot;);">';
							html += '<span style="color:#0066FF">Configuration</span>';
							html += '</a>';
							html += '</td>';
							html += '</tr>';
						}
						html += '</table>';
					}
					html += '<div style="margin-top:10px;">';
					html += '<a href="javascript:void(0)" style="color: green" onclick="openTitleAndShipTo(&quot;'+data[i].lpt_id+'&quot;,&quot;'+pc_id+'&quot;,&quot;'+pc_name+'&quot;);">';
					if(data[i].clp_titles && data[i].clp_titles.length > 2) {
						html += 'Linked Title/ShipTo <span id="linkedTitleShipToMore_'+data[i].lpt_id+'">';
						html += '&nbsp;|&nbsp;More&nbsp;['+data[i].clp_titles.length+']';
						html += '</span>';
					}else{
						html += 'Linked Title/ShipTo <span id="linkedTitleShipToMore_'+data[i].lpt_id+'"></span>';
					}
					html += '</a></div>';
					html += '</td>';
					html += '</tr>';
					html += '</table>';
					html += '</fieldset>';
				}
			} else {
				html += '<div></div>';
			}
			html += '<div style="position:absolute;height:20px; bottom: 5px;left: 15px; white-space:nowrap;">';  
			html += '<a href="javascript:void(0)" class="btn" onclick="openClpList(&quot;'+pc_id+'&quot;,&quot;'+pc_name+'&quot;);">';
			if(data.length > 2) {
				html += 'Manage CLP Type <span id="clpMore_'+pc_id+'">';
				html += '&nbsp;|&nbsp;More&nbsp;['+data.length+']';
				html += '</span>';
			}else{
				html += 'Manage CLP Type <span id="clpMore_'+pc_id+'"></span>';
			}
			html += '</a>';
			html += '<a href="javascript:void(0)" class="btn" style="margin-left: 5px;font-size:20px;vertical-align: bottom;padding:0;" onclick="addClp(&quot;'+pc_id+'&quot;,&quot;'+pc_name+'&quot;);">';
			html += '&nbsp;&nbsp;+&nbsp;&nbsp;';
			html += '</a>';
			html += '</div>';
			
			$("#clpType_"+pc_id).html(html)
		},
		error:function(){
			$.unblockUI();//遮罩关闭
			showMessage("System error","error");
		}
	});
}


//添加条件
function addCondition(id,name,type){
	//alert(id+","+name+","+type);
	$('#tiaojian').css('display','block');      //条件显示

	if(type=='line'){
		$('#line').css('display','none');
		//把产品线id保存下来
		$('#hiddenProductLineId').val(id);
		loadProductCatalogOne(id);	//加载一级产品分类
		ty='产品线：';
	}else if(type=='catalogOne'){
		$('#catalogOne').css('display','none');
		loadProductCatalogTwo(id);       
		ty='一级分类：';
	}else if(type=='catalogTwo'){
		$('#catalogTwo').css('display','none');
		loadProductCatalogThree(id);
		ty='二级分类：'
	}else if(type=='catalogThree'){
		$('#catalogThree').css('display','none');
		ty='三级分类：'
	}else if(type=='productType'){
		$('#productType').css('display','none');
		ty='商品类型：';
	}else if(type=='title'){
        $('#title').css('display','none');
        loadProductLine(id);
        ty='TITLE：';
	}
	
	var html='<div id="'+type+''+id+'" type="'+type+'" title="'+name+'" zhi="'+id+'" style="border:1px #090 solid;float:left;min-width;font-size:13px;cursor:pointer;margin-left:10px;padding-left:2px;line-height:30px">'+ty+'<span style="color:red;font-weight: bold;">'+name+'&nbsp;×<span></div>'; 
	var av=$(html).appendTo($('#condition')); //添加到页面

	$(av).click(function(){   //绑定删除事件
		if($(this).attr('type')=='line'){
			$('#line').css('display','block');	
			$('#catalogOne').css('display','none');
			$('#catalogTwo').css('display','none');
			$('#catalogThree').css('display','none');
			$("div[type='catalogOne']",$('#condition')).remove();//同时去掉子级	
			$("div[type='catalogTwo']",$('#condition')).remove();//同时去掉子级
			$("div[type='catalogThree']",$('#condition')).remove();//同时去掉子级
		}else if($(this).attr('type')=='catalogOne'){
			$('#catalogOne').css('display','block');
			$('#catalogTwo').css('display','none');
			$('#catalogThree').css('display','none');
			$("div[type='catalogTwo']",$('#condition')).remove();//同时去掉子级
			$("div[type='catalogThree']",$('#condition')).remove();//同时去掉子级
		}else if($(this).attr('type')=='catalogTwo'){
			$('#catalogTwo').css('display','block');
			$('#catalogThree').css('display','none');
			$("div[type='catalogThree']",$('#condition')).remove();//同时去掉子级
		}else if($(this).attr('type')=='catalogThree'){
			$('#catalogThree').css('display','block');
		}else if($(this).attr('type')=='productType'){
			$('#productType').css('display','block');
		}else if($(this).attr('type')=='title'){
 			$('#title').css('display','block');
 			$('#line').css('display','none');
 			$('#catalogOne').css('display','none');
			$('#catalogTwo').css('display','none');
			$('#catalogThree').css('display','none');
			$("div[type='line']",$('#condition')).remove();//同时去掉子级
			$("div[type='catalogOne']",$('#condition')).remove();//同时去掉子级	
			$("div[type='catalogTwo']",$('#condition')).remove();//同时去掉子级
			$("div[type='catalogThree']",$('#condition')).remove();//同时去掉子级
		}
		
		$('#'+this.id).remove();  //删除当前条件
		
		if($('#condition').html()==''){
			$('#tiaojian').css('display','none');
			customRefresh();
		} 
	});
}

//加载产品线根据title查询 
function loadProductLine(titleId){
	clertHeight('lineCondition');   //清除容器高度
	var para = 'id='+titleId+'&atomicBomb=<%=number%>';
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadProductLineAction.action',
		type: 'post',
		dataType: 'json',
		data:para,
		async:false,
		success: function(data){
			if(data.length>0){
				var html='';
				for(var i=0;i<data.length;i++){
					html+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'line'"+')" style="color:#069;text-decoration:none;outline:none;">'+
								data[i].name+
							'</a>'+
					     '</div>';	
				}
				$('#lineCondition').html(html);
				$('#line').css('display','block');
			}
		}
	});	
	computeHeight('lineCondition');	//计算高度
}

//加载一级产品分类
function loadProductCatalogOne(productLineId,titleId,admin_user_id){
	clertHeight('catalogOneCondition');   //清除容器高度
	var para = 'admin_user_id='+admin_user_id+'&id='+productLineId+'&title_id='+titleId+'&atomicBomb=<%=number%>';
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadFirstProductCatalogAction.action',
		type: 'post',
		dataType: 'json',
		data:para,
		async:false,
		success: function(data){
			if(data.length>0){
				var html='';
				for(var i=0;i<data.length;i++){
					html+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'catalogOne'"+')" style="color:#069;text-decoration:none;outline:none;">'+
								data[i].name+
							'</a>'+
					     '</div>';	
				}
				$('#catalogOneCondition').html(html);
				$('#catalogOne').css('display','block');
			}
		}
	});	
	computeHeight('catalogOneCondition');	//计算高度 
}

//加载二级分类
function loadProductCatalogTwo(product_catalog_id){
	clertHeight('catalogTwoCondition');   //清除容器高度
	var product_line_id=$('#hiddenProductLineId').val();
	var para = 'id='+product_catalog_id+'&product_line_id='+product_line_id+'&atomicBomb=<%=number%>';
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadSecondProductCatalogAction.action',
		type: 'post',
		dataType:'json',
		data:para,
		async:false,
		success: function(data){
			if(data.length>0){
				var html='';
				for(var i=0;i<data.length;i++){
					html+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'catalogTwo'"+')" style="color:#069;text-decoration:none;outline:none;">'+
								data[i].name+
							'</a>'+
					     '</div>';	
				}
				$('#catalogTwoCondition').html(html);			
				$('#catalogTwo').css('display','block');
			}
		}
	});	 
	computeHeight('catalogTwoCondition');	//计算高度
}

//加载三级分类
function loadProductCatalogThree(product_catalog_id){
	clertHeight('catalogThreeCondition');   //清除容器高度
	var product_line_id=$('#hiddenProductLineId').val();
	var para = 'id='+product_catalog_id+'&product_line_id='+product_line_id+'&atomicBomb=<%=number%>';
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadThirdProductCatalogAction.action',
		type: 'post',
		dataType:'json',
		data:para,
		async:false,
		success: function(data){
			if(data.length>0){
				var html='';
				for(var i=0;i<data.length;i++){
					html+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'catalogThree'"+')" style="color:#069;text-decoration:none;outline:none;">'+
								data[i].name+
							'</a>'+
					     '</div>';	
				}
				$('#catalogThreeCondition').html(html);
				$('#catalogThree').css('display','block');
			}
		}
	});
	computeHeight('catalogThreeCondition');	//计算高度	 
}

function openClpList(pc_id,product_name){
	$("#hiddenPageName").val("product_management_show_list");
	$("#hiddenPcId").val(pc_id);
	$("#hiddenPcName").val(product_name);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_and_ps_info_list.html?pc_id="+pc_id; 
	$.artDialog.open(uri , {title:"["+ product_name +"]"+ " CLP Type",width:'885px',height:'480px', lock: true,opacity: 0.1,fixed: true,close:function(){loadClpsByPcId(pc_id,product_name);}});
}

function openLpType(clpId,product_name){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_show.html?productName="+product_name+'&innerlpId='+clpId; 
	$.artDialog.open(uri , {title:'Show ['+ product_name + '] CLP Type',width:'600px',height:'450px', lock: true,opacity: 0.1,fixed: true});
}

function addClp(pc_id,product_name){
	$("#hiddenPageName").val("product_management_show_list");
	$("#hiddenPcId").val(pc_id);
	$("#hiddenPcName").val(product_name);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_box_sku_ps_type_add.html?pc_id="+pc_id; 
	$.artDialog.open(uri , {title:"["+ product_name +"] Add CLP Type",width:'650px',height:'480px', lock: true,opacity: 0.1,fixed: true,close:function(){loadClpsByPcId(pc_id,product_name);}});
}

function openClpTitleShipTo(title_id,ship_to_id,titleName,shipToName,pc_id,product_name){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_type_title_ship_to_list.html?title_id="+title_id+"&ship_to_id="+ship_to_id+"&titleName="+titleName+"&shipToName="+shipToName+"&pc_id="+pc_id+"&productName="+product_name; 
	 $.artDialog.open(uri , {title:'['+product_name+'] CLP Type Title And Ship To',width:'800px',height:'450px', lock: true,opacity: 0.1,fixed: true, resize:false});
}

function openTitleAndShipTo(lp_type_id,pc_id,product_name){
	$("#hiddenPageName").val("product_management_show_list");
	$("#hiddenPcId").val(pc_id);
	$("#hiddenPcName").val(product_name);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/title_shipto_list.html?lp_type_id="+lp_type_id+"&pc_id="+pc_id;
	$.artDialog.open(uri , {title:" Add CLP Title/Ship To",width:'730px',height:'480px', lock: true,opacity: 0.1,fixed: true,close:function(){loadClpsByPcId(pc_id,product_name);}});
}

function addBoxPsIdIndex(pc_id , product_name){
  	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_ps_index.html?pc_id="+pc_id; 
	 $.artDialog.open(uri , {title:"["+ product_name +"]"+ " 盒子到ShipTo顺序",width:'830px',height:'480px', lock: true,opacity: 0.1,fixed: true});
}

function customRefresh(){   //无条件时刷新页面
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html',
		type: 'post',
		dataType: 'html',
		async:false,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
}
</script>
