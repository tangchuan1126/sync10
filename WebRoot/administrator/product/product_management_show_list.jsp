<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<%@page import="javax.mail.Transport"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@include file="../../include.jsp"%>
<%@page import="com.cwc.app.api.zr.BoxTypeMgrZr"%>
<%@page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="java.util.*,com.cwc.app.floor.api.zyj.model.*,java.lang.Boolean,com.cwc.spring.util.MvcUtil"%>
<%@taglib uri="/turboshop-tag" prefix="tst"%>

<%
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

Long active = StringUtil.getLong(request, "active", -1L);
String cmd = StringUtil.getString(request,"cmd");
String name= StringUtil.getString(request,"seachValue");
String title_id = StringUtil.getString(request, "titleId");
String productLineId = StringUtil.getString(request, "lineId");
String catalogId = StringUtil.getString(request, "catalogId");
int productType = StringUtil.getInt(request, "productType");


com.cwc.app.floor.api.zyj.service.AdminService adminService = (com.cwc.app.floor.api.zyj.service.AdminService)MvcUtil.getBeanFromContainer("adminService");

int titleId = 0;
HttpSession sess = request.getSession(true);
Map<String,Object> loginUser = (Map<String,Object>) sess.getAttribute("adminSesion");
int corporationType = (Integer)loginUser.get("corporationType");
int customerId = 0;
//存放登录用户类型，表示用户是admin,是Customer,还是Title或者其它
String userType = "";
if(corporationType == 1){
	customerId = (Integer)loginUser.get("corporationId");
	userType = "Customer";
}else if(Boolean.TRUE.equals(loginUser.get("administrator"))){
	userType = "Admin";
}


DBRow[] rows = new DBRow[0];
//用户ID
long userId = 1L;
if(cmd.equals("filter")){
	if(userType.equals("Admin")){
		rows = proprietaryMgrZyj.filterProductAndRelateInfoByPcLineCategoryTitleId(true,catalogId,productLineId,productType, 0, 0,title_id, 0L, 0, 5,0,0, pc, request, active);
	}else if(userType.equals("Customer")){
		rows = proprietaryMgrZyj.filterProductAndRelateInfoByPcLineCategoryTitleId(true,catalogId,productLineId,productType, 0, 0,title_id, userId, 0, 5,0,0, pc, request,active,customerId);
	}
	
}else if(cmd.equals("name")){
	if(userType.equals("Admin")){
		rows = proprietaryMgrZyj.findDetailProductRelateInfoLikeSearch(true, name, title_id, 0L, 0, 5,-1,0,0, pc, request, active);
	}else if(userType.equals("Customer")){
		rows = proprietaryMgrZyj.findDetailProductRelateInfoLikeSearch(true, name, title_id, userId, 0, 5,-1,0,0, pc, request,active,customerId);
	}

}else{
	if(userType.equals("Admin")){
		rows = proprietaryMgrZyj.findAllProductsRelateInfoByTitleAdid(true, title_id, 0L, 0, 5, pc, request, active);
	}else if(userType.equals("Customer")){
		rows = proprietaryMgrZyj.findAllProductsRelateInfoByCustomer(true, title_id,userId, 0, 5, pc, request,active, customerId);
	}else{
		rows = proprietaryMgrZyj.findAllProductsRelateInfoByTitle(true, 0L, 0, 5, pc, request, titleId, active);
	}	
}

Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));

%>
<html>
<head>
<title>商品相关数据Manage查询结果显示页面</title>
<style type="text/css">
ul.myulPcTitles {
	list-style-type: none;
	padding-left: 5px;
	width: 260px;
}
td .box{float:left;padding:4px;}
td .left{text-align: right;}
td .right{text-align: left;}
td .even .box{border-bottom:1px solid #ddd;border-top:1px solid #ddd;}
td .clear{clear:both}
/*.width30{width:90px;word-break: normal;}*/
.width30{width:70px;word-break: normal;}
.width60 span{
width:120px;
display: block;
word-break: break-all;
white-space: pre-wrap;}
.width60 a span{
width: 120px;
display: block;
word-break: break-all;
white-space: pre-wrap;
}
.innerTable td{
border-bottom:none;
padding-left:0;
border:1px solid #DDD;
}
.zebraTable_another td{padding-left:0px;}
.zebraTable_another td
{
	border-bottom: 1px solid #dddddd;

	padding-left:10px;
}

.zebraTable_another tr.alt td 
{
	background: #f9f9f9;

}

.zebraTable_another tr.over td
{
	background: #E6F3C5;

}

.btn{
  color: #fff;
  display: inline-block;
  background-color: #899BD7;
  text-decoration: blink;
  height: 20px;
  font-size: 0.8em;
  line-height: 20px;
  padding: 0 8px;
  cursor: pointer;
}
.btn:hover{
  background-color: #627DD5;
  
}
.panel {
	margin-top:23px;
  margin-bottom: 20px;
  background-color: #fff;
  border: 1px #CFCFCF solid;
  -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
  box-shadow: 0 1px 1px rgba(0,0,0,.05);
  
}
.panel .panelTitle{
	  background: #f1f1f1;
	  height: 40px;
	  line-height: 40px;
	  padding:0 15px;
}
.panel .panelTitle .title-left{
	    float: left;
}
.panel .panelTitle .title-right{
	  float: right;
}
.clear{
	  clear: both;
}
.right-title{
	height:40px;
	background-color:#fff;
}
ul.myulPcTitles{
	padding:0;
	margin:0; 
}

.jstree-default .jstree-clicked {

	background: none;
	border-radius: 2px;
	box-shadow: inset 0 0 1px #999;
}
.badge {
    background-color: #777;
    border-radius: 10px;
    color: #fff;
    display: inline-block;
    font-size: 12px;
    font-weight: 700;
    line-height: 1;
    min-width: 10px;
    padding: 3px 7px;
    text-align: center;
    vertical-align: baseline;
    white-space: nowrap;
}
</style>
<script type="text/javascript">
var userType = "";
var customerId = 0;
var titleId = 0;
$(function(){
	userType = $("#userType").val();
	customerId = parseInt($("#customerId").val());
	titleId = parseInt($("#titleId").val());
	$("a.title-shipto").each(function(){
		if($(this).parent().siblings().size() == 0){
			$(this).parent().css({"margin-top":"40px"});
		}
	});
	$(".zebraTable_another>tbody").children("tr:even").addClass("alt");
});

function downloadTemplate(){
	
	var url = '../../administrator/product/ExportProductCLPsTitlesAndShipTos.xlsm';
	
	document.downloadTemplateForm.action = url;
    document.downloadTemplateForm.submit();
}

function setTitle(id,title_id,p_name){
	var url = '/Sync10-ui/pages/configuration/product_title.html?id=' + id + "&title_id=" + title_id;
	if(userType == "Customer"){
		url = '/Sync10-ui/pages/configuration/product_title.html?id=' + id + "&title_id=0&customerId=" + customerId;
	}else if(userType == "Title"){
		url = '/Sync10-ui/pages/configuration/product_title.html?id=' + id + "&titleId=" + titleId;
	}
	
	$.blockUI({message: ''}); //遮罩打开
	$.artDialog.open(url , {title: '['+p_name+'] Title / Customer',width:'800px',height:'456px', lock: true,opacity: 0.1,fixed: true,close:function(){
		loadTitlesByPcId(id);
	}});
	//$.unblockUI();//遮罩关闭
   
}

//局部刷新Title区域
function loadTitlesByPcId(productId) {
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '}); //遮罩打开
	var baseUrl = '/Sync10/basicdata/productCustomer' ;
	var url = baseUrl + "/" + productId;
	if(userType == "Customer"){
		url = baseUrl + "/" + productId + "/customer/" + customerId;
	}else if(userType == "Title"){
		url = baseUrl + "/" + productId + "/title/" + titleId; 
	}	
	$.ajax({
		url : url,
		type:'get',
		dataType:'json',
		async:false,
		success:function(data){
			$.unblockUI();//遮罩关闭
			//最多显示 Title 个数
			var maxCount = 4;
			var  html = "";
			
			var productName = $.trim($("#product_" + productId).find("legend").text());
			html += "<fieldset style=\"margin-top:10px;box-sizing: border-box;border:2px red solid;padding:7px;width:90%;word-break:break-all;line-height:18px;min-height:90px;  -webkit-border-radius: 5px;-moz-border-radius:5px;\">";
			html += "<legend style=\"color:green;font-weight:bold;text-decoration:underline;cursor:pointer;\" onclick=\"setTitle(" +  productId + "," + 0 + ",'" + productName +  "')\" >";
			html += "Title / Customer";   
			if(data && data.length > maxCount){
				html += "&nbsp;|&nbsp;<span>More[" + data.length + "]</span>";
			}
			html += "</legend>";
			html += "<ul class=\"myulPcTitles\" >";
			html += "<table style=\"width:100%;height:100%;border-collapse: collapse;\" class=\"innerTable\"><tbody>";
			html += "<tr class=\"\"><td width=\"50%\" style=\"vertical-align:center;text-align:center;font-weight: 700;\">Title</td><td style=\"vertical-align:center;text-align:center;font-weight: 700;\">Customer</td></tr>";
			
			
			
			if(data.length > 0){
				
				
				var showCount = (data.length > maxCount) ? maxCount : data.length;
				for(var i=0; i < showCount; i++){	
					var item = data[i];
					var customer_count = Math.min(3,item.customers.length);
	   				for(var j=0; j < customer_count; j++){
	   					html += "<tr class=\"alt\">";
	   					if(j == 0){
	   						html += "<td style=\"vertical-align:left;text-align:left;padding-left: 5px;\"  rowspan=\"" + customer_count + "\" width=\"50%\">"  + item.title.name;
	   					    if(item.customers.length > 3){
	   					    	html += "<span style=\"cursor:pointer;margin-left:3px;\" class=\"badge\" onclick=\"setTitle(" +  productId + "," + item.title.id + ",'" + productName +  "')\">" + item.customers.length + "</span>";
	   					    }
	   						html += "</td>";
	   					}
	   					html += "<td style=\"vertical-align:left;text-align:left;padding-left: 5px;\">" + item.customers[j].name + "</td>";
	   					
	   					html += "</tr>";
	   				}

				}
			}else{
   				html += "<tr><td colspan=\"2\" align=\"center\">No Data</td></tr>";
			}
			html += "</tbody></table>";
			html += "</ul>";
			html += "</fieldset>";					
			$("#myulPcTitles_" + productId).html(html);
		},
		error:function(){
			showMessage("System error","error");
			$.unblockUI();//遮罩关闭
		}
	});
}   
</script>
</head>
<body>
<input type="hidden" id="userType" value="<%=userType%>" />
<input type="hidden" id="customerId" value="<%=customerId%>" />
<input type="hidden" id="titleId" value="<%=titleId%>" />
<form action="" name="downloadTemplateForm" id="downloadTemplateForm">
	
	<div class='panel'>
	<div class="panelTitle">
		<div class="title-left"></div>
		<div class="title-right">
		<div class="buttons-group minor-group" style="margin-top:6px;"> 
		
			<!-- <a name="importProductTitles" class="buttons" value="Import Product Linked Title" onClick="importProductTitles()">
				<i class="icon-share"></i>&nbsp;Import Product Linked Title</a> -->
			<a name="Submit" class="buttons" onClick="downloadTemplate()"><i class="icon-download-alt"></i>&nbsp;Download Template</a>
			<!-- <a class="buttons" value="Export Product Linked Title" onclick="down()" ><i class="icon-reply"></i>&nbsp;Export Product Linked Title</a> -->
			<!-- <a class="buttons" value="Initialize Basic Data" onclick="importInitData()" ><i class="icon-spinner"></i>&nbsp;Initialize Basic Data</a> 
			<a class="buttons" value="Delete Basic Data" onclick="deleteBasicDatas()" ><i class="icon-trash"></i>&nbsp;Delete Basic Data</a> -->
			<!-- <a class="buttons" value="Export Product CLP" onclick="exportProductClp()" ><i class="icon-reply"></i>&nbsp;Export Product CLP</a> -->
		</div>
		
		<input name="file_names" id="file_names" type="hidden" />
		</div>
		<div class="clear"></div>
	</div>
	<table width="100%" border="0" isBottom="true" cellpadding="0" cellspacing="0" class="zebraTable_another">
		<tr>
			<th width="30%" class="right-title" style="vertical-align:center;text-align:center;">Product Info</th>
			<th width="15%" class="right-title" style="vertical-align:center;text-align:center;">Title / Customer</th>
			<th width="55%" class="right-title" style="vertical-align:center;text-align:center;">CLP Type</th>
		</tr>
		<%for(int i=0; i<rows.length; i++ ){
			DBRow[] clpRows = boxTypeMgrZr.getBoxTypes(rows[i].get("pc_id",0),0,0,0,customerId,1);
			int clpCount = clpRows.length;
		%>
		<tr>
			<td  height="60" valign="top" align="left" style='font-size:14px;padding-left:5px;' width="30%" id='product_<%=rows[i].getString("pc_id")%>'>
				<fieldset style="border:2px #00F solid;padding:7px;width:90%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;  -webkit-border-radius: 5px;-moz-border-radius:5px;">
					<legend style="font-size:15px;font-weight:bold;text-decoration:underline;cursor:pointer;"
						onclick="showProductDetails('<%=rows[i].get("pc_id",0L) %>', '<%=rows[i].getString("p_name")%>');">
						<%=rows[i].getString("p_name")%>
					</legend>
					<span style="font-size:12px">Main Code:</span> 
					<span style="font-size:12px;font-weight:bold;"><%=rows[i].getString("p_code")%></span>
					<br>
					<span style="font-size:12px">Dimension:</span> 
					<span style="font-size: 12px; font-weight: bold;"> 
					<%LengthUOMKey lengthUomKey = new LengthUOMKey(); %>
						<span style="font-size: 12px; font-weight: bold;"><%=rows[i].get("length", 0f)%></span>&nbsp;*&nbsp;
						<span style="font-size: 12px; font-weight: bold;"><%=rows[i].get("width", 0f)%></span>&nbsp;*&nbsp;
						<span style="font-size: 12px; font-weight: bold;"><%=rows[i].get("heigth", 0f)%></span>&nbsp;(<%=lengthUomKey.getLengthUOMKey(rows[i].get("length_uom", 0)) %>)
					</span>
					<div style="font-size: 12px;">ProductLine&nbsp;/&nbsp;Category:</div>
					<div style="padding-left: 15px; font-size: 12px">
						<span style="font-size: 12px; font-weight: bold;"> <%
							Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
					 		StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
						  	String product_line_name = "";
						  	DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
						  	if(null != catalog)
						  	{
						  		long product_line_id = catalog.get("product_line_id", 0L);
						  		DBRow productLine = productLineMgrTJH.getProductLineById(product_line_id);
						  		if(null != productLine)
						  		{
						  			product_line_name = productLine.getString("name");
						  			catalogText.append("<img src='img/folderopen.gif'/>"+product_line_name+"<br/>");
							  		String s = "<img src='img/joinbottom.gif'/>";
							  		catalogText.append(s);
						  		}
						  	}
						  	DBRow allFather[] = tree.getAllFather(rows[i].get("catalog_id",0l));
						  	for (int jj=0; jj<allFather.length-1; jj++)
						  	{
						  		catalogText.append("<img src='img/folderopen.gif'/> "+allFather[jj].getString("title")+"<br/>");
						  		String s = (jj==0) ? "<img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>"  : "<img  src='img/empty.gif'/><img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>";
						  		catalogText.append(s);
						  	}
						  	if (catalog!=null)
						  	{
						  		catalogText.append("<img  src='img/page.gif'/>"+catalog.getString("title")+"");
						  		
						  	}
							%> <%=catalogText %>
						</span>
					</div>
				</fieldset>
			</td> 
			<td valign="top" align="left" style='font-size:14px;padding-left:5px;padding-bottom: 10px;' width="15%" id='myulPcTitles_<%=rows[i].get("pc_id",0L) %>'>
						<fieldset style="margin-top:10px;box-sizing: border-box;border:2px red solid;padding:7px;width:90%;word-break:break-all;line-height:18px;min-height:90px;  -webkit-border-radius: 5px;-moz-border-radius:5px;">
							<legend style="color:green;font-weight:bold;text-decoration:underline;cursor:pointer;"
								 onclick="setTitle('<%=rows[i].get("pc_id",0L) %>',0,'<%=rows[i].get("p_name","") %>');">
								Title / Customer 
								<%
									List<ProductCustomer> list =  ((List)rows[i].get("titles",new ArrayList()));
									if(list != null && list.size() > 4){
								%>		
										|&nbsp;<span>More[<%=list.size() %>]</span>
								<%
									}
								%>
							</legend>
							<ul class="myulPcTitles" >
								<table class="innerTable" style="width:100%;height:100%;border-collapse: collapse;">
								<tbody>
									<tr>
										<td width="50%" style="vertical-align:center;text-align:center;font-weight: 700;">Title</td>
										<td style="vertical-align:center;text-align:center;font-weight: 700;">Customer</td>
									</tr>
									<%
									  
									  if(list != null && list.size() > 0){
										  long pc_id = rows[i].get("pc_id",0L);
										  String pc_name = rows[i].get("p_name","");
										  for(int k = 0; k < list.size(); k++){ 
											  Title title = list.get(k).getTitle();
											  List<Customer> customers = list.get(k).getCustomers();
											  if(k < 4){
												  int rowspan_count = Math.min(customers.size(), 3);
												  for(int m = 0; m < rowspan_count ; m++){
													  out.println("<tr class=\"alt\">"); 
													  if(m == 0){
														  out.println("<td style=\"vertical-align:left;text-align:left;padding-left: 5px;\" rowspan=\"" + rowspan_count + "\" width=\"50%\">"  + title.getName()); 
														  if(customers.size() > 3){
															  out.println(" <span onclick=\"setTitle('" + pc_id + "'," + title.getId() + ",'" + pc_name + "');\" class=\"badge\" style=\"cursor:pointer;\">" + customers.size() +"</span>"); 
														  }
														  out.println("</td>");
													  }
													  out.println("<td style=\"vertical-align:left;text-align:left;padding-left: 5px;\">" + customers.get(m).getName() + "</td>"); //<td>BBBBB</td>
													  out.println("</tr>"); //</tr>
												  }
											  }
										  }
									  }else{
									 	  out.println("<tr><td colspan=\"2\" align=\"center\">No Data</td></tr>");
									  }
									 %>
								</tbody>
								</table>								
							</ul>
						</fieldset>
			</td>
			
			<td id="clpType_<%=rows[i].get("pc_id",0L) %>" valign="top" align="left" style='font-size:14px;position:relative;padding-left:5px;' width="55%">
				<% if(clpCount > 0) {
					int eachClpCount = clpRows.length>2?2:clpRows.length;
					for(int c=0; c<eachClpCount; c++){
				%>
				<fieldset style="border:2px #930 solid;padding:0 7px 7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px;">
					<legend style="color:green;font-weight:bold;text-decoration:underline;cursor:pointer;"
						onclick="openLpType('<%=clpRows[c].get("lpt_id",0)%>','<%=rows[i].getString("p_name")%>');">
						<%=clpRows[c].getString("lp_name")%>
					</legend>
					<table style="width:100%;height:100%">
					  <tr>
						<td style="background:none;border-bottom:none;width:30%;">
						  <div>
						  
						  	<div class="box left width30"><span>Inner Type:</span></div>
						  	<%if(clpRows[c].get("inner_pc_or_lp",0l) > 0){ 
						  		
		      					//DBRow innerLp = boxTypeMgrZr.findIlpByIlpId(clpRows[c].get("inner_pc_or_lp",0l));
		      					DBRow cascade = boxTypeMgrZr.getCascadeClpType(clpRows[c].get("inner_pc_or_lp",0l));
				      			
				      			int j = 0;
		      				
						  	if(cascade.get("inner_pc_or_lp", 0l)==0){%>
						  		<div name="lp" class="box right width60">
			      						<a href="javascript:void(0)" style="color:green;background:none;" onclick="openLpType('<%=cascade.get("lpt_id",0)%>','<%=rows[i].getString("p_name")%>');">
			      						<%=  cascade.get("lp_name","") %>
			      						</a>
			      		
							</div>
						  <% }else{%>	
						  	<div name="lpTree" class="box right width60">
			      				
			      				<% do{
			      					
			      					j++;
			      					
			      					String lpName = cascade.get("lp_name","");
			      				%>
			      				<ul>
			      					<li>
			      						<a href="javascript:void(0)" style="color:green;background:none;" onclick="openLpType('<%=cascade.get("lpt_id",0)%>','<%=rows[i].getString("p_name")%>');">
			      						<%= lpName %>
			      						</a>
			      				
			      				<%	
			      					
			      					cascade = cascade.get("subPlateType")==null?null:(DBRow)cascade.get("subPlateType");
			      					
			      				} while(cascade!=null);
			      				%>
			      				
			      				<% for(;j>0;j--){%>
			      					</li>
		      					</ul>
			      				<%} %>
							</div>
						  	
		      			    <%} }else{ %>
		      			    	<!-- <div class="box right width60"><span style="color:#0066FF">Product</span></div> -->
		      			    	<div class="box right width30"><span style="color:#0066FF">Product</span></div>
		      			    <%} %>
		      			    
		      			  	<div class="clear"></div>
		      			  </div>
	      				  <div>
		      			    <div class="box left width30"><span>Product Qty:</span></div>
		      			    <!-- <div class="box right width60"><span style="color:#0066FF"><%=clpRows[c].getString("inner_total_pc") %></span></div> -->
		      			    <div class="box right width30"><span style="color:#0066FF"><%=clpRows[c].getString("inner_total_pc") %></span></div>
		      				<div class="clear"></div>
		      			  </div>
						</td>
						<td style="background:none;border-bottom:none;padding-bottom:20px;width:70%;padding-top:8px;">
							<% 
	      					String titleIds = clpRows[c].getString("title_ids");
	      					String shipToIds = clpRows[c].getString("ship_to_ids");
	      					String customerIds = clpRows[c].getString("customer_ids");
	      					int count = 0;
	      					if(StringUtils.isNotEmpty(titleIds)){
	    						String[] titleIdArr = titleIds.split(",");
	    						String[] shipToIdArr = shipToIds.split(",");
	    						String[] customerIdArr = customerIds.split(",");
	    						int titleShipToCount = titleIdArr.length;
	    					%>
	    					<table style="width:100%;border-collapse: collapse;" class="innerTable">
	    					<tr>
								<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;" >Title</td>
								<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;" >Customer</td>
								<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;" >ShipTo</td>
								<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;" >Operation</td>
							</tr>
							<%
							
							com.cwc.app.floor.api.zyj.service.CustomerService customerService = (com.cwc.app.floor.api.zyj.service.CustomerService)MvcUtil.getBeanFromContainer("customerService");
							for (int k=0;k<titleShipToCount;k++) {
								if(customerId > 0 && Integer.parseInt(customerIdArr[k]) > 0 && Integer.parseInt(customerIdArr[k]) != customerId  ){
									continue;
								}
								count++;
								if(count <= 2){
	    	    					DBRow title = proprietaryMgrZyj.findProprietaryByTitleId(Long.valueOf(titleIdArr[k]));
	    	    					DBRow shipTo = shipToMgrZJ.getShipToById(Long.valueOf(shipToIdArr[k]));
	    	    					Customer customer = null;
	    	    					if(customerIdArr[k] != null  && customerIdArr[k].trim().length() > 0 && Integer.parseInt(customerIdArr[k]) > 0){
	    	    						customer = customerService.getCustomer(Integer.parseInt(customerIdArr[k]));
	    	    					}
	    	    					String customerName = (customer != null) ?  customer.getName() : "*";
	    	    					
	    	    					String titleName=title==null?"*":title.getString("title_name");
	    	    					String shipToName=shipTo==null?"*":shipTo.getString("ship_to_name");
    	    				%>
	    					<tr> 
	    					  <td width="25%" style="vertical-align:middle;text-align:center;padding-left: 5px;"><%=titleName %></td>
	    					  <td width="25%" style="vertical-align:middle;text-align:center;padding-left: 5px;"><%=customerName %></td>
							  <td width="25%" style="vertical-align:middle;text-align:center;padding-left: 5px;"><%=shipToName %></td>
							  <td width="25%" style="vertical-align:middle;text-align:center;padding-left: 5px;">
							  	<a href="javascript:void(0)" style="color: green;"
										onclick="openClpTitleShipTo('<%=Long.valueOf(titleIdArr[k])%>','<%=Long.valueOf(shipToIdArr[k]) %>','<%=titleName %>','<%=shipToName%>','<%=rows[i].get("pc_id",0L) %>','<%=rows[i].getString("p_name")%>','<%=customerIdArr[k] %>','<%=customerName%>');">
										<span style="color:#0066FF">Configuration</span>
								</a>
							  </td>
							</tr>
							  <%}%>
							<%}%>
							</table>
							<%}%>
						  	<div style="margin-top:10px;">
							  	<a href="javascript:void(0)" style="color: green" class="title-shipto"
									onclick="openTitleAndShipTo('<%=clpRows[c].get("lpt_id",0l) %>','<%=rows[i].get("pc_id",0)%>','<%=rows[i].getString("p_name") %>');">
									Title/Customer/ShipTo <span id="linkedTitleShipToMore_<%=clpRows[c].get("lpt_id",0l) %>"> &nbsp;|&nbsp;More&nbsp;[<%=count %>]</span>
								</a>
								<script type="text/javascript"><%if(count > 2){ %>$("#linkedTitleShipToMore_<%=clpRows[c].get("lpt_id",0l) %>").css("display","");<%}else{%>$("#linkedTitleShipToMore_<%=clpRows[c].get("lpt_id",0l) %>").css("display","none");<%}%></script>
							</div> 						
						</td>
					  </tr>
					</table>
				</fieldset>
				<%	} 
				} else{%>
					<div></div>
				<%}%>
			  <div style="position:absolute;height:20px;bottom: 5px;left: 15px;white-space: nowrap;">
				<a href="javascript:void(0)" class="btn" onclick="openClpList('<%=rows[i].get("pc_id",0)%>','<%=rows[i].getString("p_name") %>');">
					Manage CLP Type <span id="clpMore_<%=rows[i].get("pc_id",0L) %>">&nbsp;|&nbsp;More&nbsp;[<%=clpCount %>]</span>
				</a>
				<script type="text/javascript"><%if(clpCount > 2){ %>$("#clpMore_<%=rows[i].get("pc_id",0L) %>").css("display","");<%}else{%>$("#clpMore_<%=rows[i].get("pc_id",0L) %>").css("display","none");<%}%></script>
				<a href="javascript:void(0)" class="btn" style="margin-left: 5px;font-size:20px;vertical-align: bottom;padding:0;"
					onclick="addClp('<%=rows[i].get("pc_id",0)%>','<%=rows[i].getString("p_name") %>');">
					&nbsp;&nbsp;+&nbsp;&nbsp;
				</a>
			</div>
		  </td>
		</tr>
		<%
			}
		%>
	</table>
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
		<tr>
			<td height="28" align="right" valign="middle">
				<%
					int pre = pc.getPageNo() - 1;
					int next = pc.getPageNo() + 1;
					out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount()
							+ " &nbsp;&nbsp;Total：" + pc.getAllCount()
							+ " &nbsp;&nbsp;");
					out.println(HtmlUtil.aStyleLink("gop", "First", "javascript:go(1)",
							null, pc.isFirst()));
					out.println(HtmlUtil.aStyleLink("gop", "Previous", "javascript:go("
							+ pre + ")", null, pc.isFornt()));
					out.println(HtmlUtil.aStyleLink("gop", "Next", "javascript:go("
							+ next + ")", null, pc.isNext()));
					out.println(HtmlUtil.aStyleLink("gop", "Last", "javascript:go("
							+ pc.getPageCount() + ")", null, pc.isLast()));
				%> Goto <input name="jump_p2" type="text" id="jump_p2" style="width: 28px;" value="<%=StringUtil.getInt(request, "p")%>">
				<input name="Submit22" type="button" class="page-go" style="width: 28px; padding-top: 0px;"
				onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
			</td>
		</tr>
	</table>
	</div>
</body>
</html>
