<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long pcid = StringUtil.getLong(request,"pcid",0l);
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
int union_flag = StringUtil.getInt(request,"union_flag");
long pro_line_id = StringUtil.getLong(request,"pro_line_id",0l);

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);


DBRow rows[];

if (cmd.equals("filter"))
{
	rows = productMgrZJ.exportProductShow(pcid,pro_line_id,union_flag,pc);
}
else if (cmd.equals("search"))
{
	rows = productMgr.getSearchProducts4CT(key,pc); 
}
else
{
	rows = productMgr.getAllProducts(pc);	
}

Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>商品导入/导出</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>
		
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

		
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script language="javascript">
<!--
function filter()
{
		document.filter_search_form.cmd.value = "filter";		
		document.filter_search_form.pcid.value = $("#filter_pcid").val();	
		document.filter_search_form.pro_line_id.value = $("#filter_productLine").val();
		document.filter_search_form.union_flag.value = $("input#union_flag:checked").val();		
		document.filter_search_form.submit();
}

function closeWin()
{
	tb_remove();
}

function closeWinRefresh()
{
	window.location.reload();
	tb_remove();
}

function cancelImport(filename)
{
	var para = "filename="+filename;
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/cancelImport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){
					if (date["close"])
					{
						tb_remove();
					}
				}
			});
}

function openCatalogMenu()
{
	$("#category").mcDropdown("#categorymenu").openMenu();
}
//-->

function exportProduct()
{
	var catalog_id= $("#filter_pcid").val();
	var union_flag= $("input#union_flag:checked").val();
	var para = "catalog_id="+catalog_id+"&union_flag="+union_flag;
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/exportProduct.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					catalog_id:$("#filter_pcid").val(),
					union_flag:$("input#union_flag:checked").val(),
					pro_line_id:$("#filter_productLine").val() 
				},
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){
					if(date["canexport"]=="true")
					{
						document.export_product.action=date["fileurl"];
						document.export_product.submit();
					}
					else
					{
						alert("该分类与子类下无所选形式商品，无法导出");
					}
				}
			});
}

function exportProductUnion()
{
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/exportProductUnion.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					catalog_id:$("#filter_pcid").val(),
					pro_line_id:$("#filter_productLine").val() 
				},
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){
					if(date["canexport"]=="true")
					{
						document.export_product.action=date["fileurl"];
						document.export_product.submit();
					}
					else
					{
						alert("该分类与子类下无套装关系，无法导出");
					}
				}
			});
}

function exportProductFile()
{
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/exportProductFile.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					catalog_id:$("#filter_pcid").val(),
					union_flag:$("input#union_flag:checked").val(),
					pro_line_id:$("#filter_productLine").val()
				},
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){
					if(date["canexport"]=="true")
					{
						document.export_product.action=date["fileurl"];
						document.export_product.submit();
					}
					else
					{
						alert("该分类与子类下无所选形式商品，无法导出");
					}
				}
			});
}

function cleanSearchKey()
{
	//为空因控件级联套用
}

function importProduct()
{
	tb_show('上传商品','product_upload_excel.html?TB_iframe=true&height=500&width=1000',false);
}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
.excel-button {
	background-attachment: fixed;
	background: url(../imgs/page_excel.png);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 95px;
	color: #000000;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 商品管理 »   商品导入/导出</td>
  </tr>
</table>
<br>
 <form name="add_union_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProductUnion.action" >
 <input type="hidden" name="set_pid">
 <input type="hidden" name="quantity">
 <input type="hidden" name="p_name">
 </form>
 
 <form name="mod_union_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductUnion.action" >
<input type="hidden" name="set_pid">
<input type="hidden" name="pid">
<input type="hidden" name="quantity">
<input type="hidden" name="p_name">
</form>

<form name="del_union_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/delProductUnion.action" >
<input type="hidden" name="set_pid">
<input type="hidden" name="pid">
</form>
 
 
<form name="del_form" method="post">
<input type="hidden" name="pcid">
</form>

<form name="mod_form" method="post">
<input type="hidden" name="pc_id">
<input type="hidden" name="catalog_id">
<input type="hidden" name="p_name" >
<input type="hidden" name="p_code">

<input type="hidden" name="unit_name">
<input type="hidden" name="unit_price">
<input type="hidden" name="gross_profit">
<input type="hidden" name="weight">
</form>


<form name="filter_search_form" method="get"  action="ct_product_import_or_export.html">
<input type="hidden" name="cmd" />
<input type="hidden" name="pcid" />
<input type="hidden" name="key" />
<input type="hidden" name="pro_line_id"/>
<input type="hidden" name="union_flag" />

</form>

<form name="export_product" method="post"></form>

<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="65%">
	
	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
	
	<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
		<ul id="productLinemenu" class="mcdropdown_menu">
             <li rel="0">所有产品线</li>
             <%
				  DBRow p1[] = productLineMgrTJH.getAllProductLine();
				  for (int i=0; i<p1.length; i++)
				  {
						out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
						 
						out.println("</li>");
				  }
			  %>
           </ul>
           <input type="text" name="productLine" id="productLine" value="" />
           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
	</td>
    <td width="434" align="left" valign="middle" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid;padding-bottom: 5px;">&nbsp;&nbsp;<input name="Submit2" type="button" class="button_long_refresh" value="过滤" onClick="filter()">
      &nbsp;&nbsp;&nbsp; 
      <input name="Submit3" type="submit" class="long-button" value="申请变更" onClick="importProduct()">
     </td>
  </tr>
  <tr>
  	 <td id="categorymenu_td" width="423" align="left" valign="bottom" bgcolor="#eeeeee" style="padding-left:10px;padding-top:20px;"> 
           <ul id="categorymenu" class="mcdropdown_menu">
			  <li rel="0">所有分类</li>
			  <%
				  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
				  for (int i=0; i<c1.length; i++)
				  {
						out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
			
						  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
						  if (c2.length>0)
						  {
						  		out.println("<ul>");	
						  }
						  for (int ii=0; ii<c2.length; ii++)
						  {
								out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
								
									DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
									  if (c3.length>0)
									  {
											out.println("<ul>");	
									  }
										for (int iii=0; iii<c3.length; iii++)
										{
												out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
												out.println("</li>");
										}
									  if (c3.length>0)
									  {
											out.println("</ul>");	
									  }
									  
								out.println("</li>");				
						  }
						  if (c2.length>0)
						  {
						  		out.println("</ul>");	
						  }
						  
						out.println("</li>");
				  }
				  %>
			</ul>
	  <input type="text" name="category" id="category" value="" />
	  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"/>
  	</td>
    <td height="29" align="left" bgcolor="#eeeeee">
	&nbsp;&nbsp;<input name="Submit" type="submit" class="long-button-export" value="导出商品" onClick="exportProduct()">
	&nbsp;&nbsp;<input name="Submit" type="submit" class="long-button-export" value="导出组合" onClick="exportProductUnion()">
	&nbsp;&nbsp;<input name="Submit" type="submit" class="long-button-export" value="商品图片" onClick="exportProductFile()">
	</td>
  </tr>
</table>
	
	</div>
<script type="text/javascript">

	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}

});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
					if(id==<%=pro_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"",<%=pcid%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"",0);
					}
				}

});

<%
if (pcid>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=pcid%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 
<%
if (pro_line_id!=0)
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue('<%=pro_line_id%>');
<%
}
else
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue(0);
<%
}
%>

</script>	
	
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form name="listForm" method="post">

	
    <tr> 
        <th width="297"  class="left-title">名称</th>
        <th width="203"  class="right-title" style="vertical-align: center;text-align: center;">条码</th>
        <th width="176" align="left"  class="right-title" style="vertical-align: center;text-align: left;">类别</th>
        <th width="139" align="center"  class="right-title" style="vertical-align: center;text-align: center;">单位</th>
        <th width="122"  class="right-title" style="vertical-align: center;text-align: center;">重量(Kg)</th>
        <th width="142"  class="right-title" style="vertical-align: center;text-align: center;">单价</th>
    </tr>

    <%
for ( int i=0; i<rows.length; i++ )
{

%>
    <tr  > 
      <td height="80" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >

		
	   <%
	  if (rows[i].get("union_flag",0)==1)
	  {
	  %>
            <fieldset style="border:1px #cccccc solid;padding:7px;width:90%;background:#FFFFFF;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:13px;font-weight:bold;color:#000000;">
<%
	  if (rows[i].get("alive",0)==0)
	  {
			out.println("<img src='../imgs/standard_msg_error.gif' width='16' height='16' align='absmiddle'>");
		}
		%>

<%=rows[i].getString("p_name")%> <a href='javascript:openAddUnionWin(<%=rows[i].get("pc_id",0l)%>);'><img src='../imgs/addp.gif' border='0' align='absmiddle'></a></legend>
<%
DBRow inSetProducts[] = productMgr.getProductsInSetBySetPid(rows[i].get("pc_id",0l));
for (int isp_i=0; isp_i<inSetProducts.length; isp_i++)
{
%>
 <table width="100%" border="0" cellspacing="0" cellpadding="3">
   <tr>
     <th width="74%" style="font-size:12px;font-weight:normal;vertical-align: center;text-align: left;height:30px;border-bottom:1px #eeeeee solid;padding-left:5px;"><%=inSetProducts[isp_i].getString("p_name")%> x <span style='color:#990000'><%=inSetProducts[isp_i].getString("quantity")%> <%=inSetProducts[isp_i].getString("unit_name")%></span></th>
     <th width="26%" style="vertical-align: center;text-align: center;border-bottom:1px #eeeeee solid;padding-left:5px;" >
	 <a href="javascript:openModUnionWin(<%=inSetProducts[isp_i].getString("set_pid")%>,<%=inSetProducts[isp_i].getString("pid")%>);"><img src="../imgs/modp.gif" width="16" height="16" border="0" align="absmiddle"></a>
	 &nbsp;
	 <a href="javascript:delProductUnion(<%=inSetProducts[isp_i].getString("set_pid")%>,<%=inSetProducts[isp_i].getString("pid")%>,'<%=inSetProducts[isp_i].getString("p_name")%>');"><img src="../imgs/delp.gif" width="16" height="16" border="0" align="absmiddle"></a>	 </th>
   </tr>
 </table>
<%
}
%>
            </fieldset>

	  <%
	  }
	  else
	  {
	  	  if (rows[i].get("alive",0)==0)
	  {
			out.println("<img src='../imgs/standard_msg_error.gif' width='16' height='16' align='absmiddle'>");
		}
	  	//不能在套装下的商品添加商品
		if (productMgr.getProductUnionsByPid(rows[i].get("pc_id",0l)).length>0)
		{
		  	out.println("<span style='font-size:14px;'>"+rows[i].getString("p_name")+"</span>");
		}
		else
		{
		  	out.println("<span style='font-size:14px;'>"+rows[i].getString("p_name")+"</span> <a href='javascript:openAddUnionWin("+rows[i].get("pc_id",0l)+");'><img src='../imgs/addp.gif' border='0'  align='absmiddle'></a>");
		}
	  }
	  %>	  </td>
      <td align="center" valign="middle" style='word-break:break-all;' ><%=rows[i].getString("p_code")%></td>
      <td align="left" valign="middle" style="line-height:20px;">
	  <span style="color:#999999">
	  <%
	  DBRow allFather[] = tree.getAllFather(rows[i].get("catalog_id",0l));
	  for (int jj=0; jj<allFather.length-1; jj++)
	  {
	  	out.println("<a class='nine4' href='?cmd=filter&pcid="+allFather[jj].getString("id")+"&key=&union_flag=0'>"+allFather[jj].getString("title")+"</a><br>");
	
	  }
	  %>
	  
	  </span>
	  
	  <%
	  DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
	  if (catalog!=null)
	  {
	  	out.println("<a href='?cmd=filter&pcid="+rows[i].getString("catalog_id")+"&key=&union_flag=0'>"+catalog.getString("title")+"</a>");
	  }
	  %>	
	  
	  
	  </td>
      <td align="center" valign="middle"  ><%=rows[i].getString("unit_name")%></td>
      <td align="center" valign="middle"   ><%=rows[i].get("weight",0f)%></td>
      <td align="center" valign="middle"   ><%=rows[i].get("unit_price",0d)%></td>
    </tr>
    <%
}
%>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="ct_product_import_or_export.html">
    <input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="pcid" value="<%=pcid%>">
<input type="hidden" name="key" value="<%=key%>">
<input type="hidden" name="union_flag" value="<%=union_flag%>">

  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
