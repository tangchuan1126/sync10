<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String storage_name = StringUtil.getString(request,"storage_name");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
<script>
function addpro(theForm)
{
	if (theForm.pc_id.value==0)
	{
		alert("请选择商品");

	}
	else if (theForm.cid.value==0)
	{
		alert("请选择类别");

	}
	else
	{
		theForm.submit();
	}
}

function changeCatalog(obj)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductsByCidJSON.action",
				{product_catalog_id:obj.value},
				function callback(data)
				{  
					if (data!="")
					{
						$("#pc_id").clearAll();
						$("#pc_id").addOption("选择商品...",0);
						$.each(data,function(i){
							$("#pc_id").addOption(data[i].p_name,data[i].pc_id);
						});
					}				
				}
		);
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

	<form name="add_pro_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProductStorage.action">
<br>
<table width="96%" border="0" align="center" cellpadding="7" cellspacing="8">
  <tr> 
    <td width="17%" align="left" valign="middle">
	
	
	
	商品</td>
    <td width="83%" align="left" valign="middle">
	<select name="product_catalog_id" id="product_catalog_id" onChange="changeCatalog(this)" >
	<option value="0">选择商品分类...</option>
          <%
DBRow treeRows[] = catalogMgr.getProductCatalogTree();
String qx;
for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==StringUtil.getLong(request,"cid")?"selected":""%>> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>          </option>
          <%
}
%>

	</select>
	
	
	<select name="pc_id" id="pc_id">
      <option value="0">选择商品...</option>

    </select>



	  	  
	  </td>
  </tr>

  <tr>
    <td align="left" valign="middle"> 所属仓库 </td>
    <td align="left" valign="middle">
	   <select name="cid" id="select2">
	<option value="0">选择类别...</option>
      <%
treeRows = catalogMgr.getProductStorageCatalogTree();
for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
      <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==StringUtil.getLong(request,"cid")?"selected":""%>> <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%> <%=qx%> <%=treeRows[i].getString("title")%> </option>
      <%
}
%>
    </select>	</td>
  </tr>
  <tr>
    <td align="left" valign="middle">存放位置</td>
    <td align="left" valign="middle"> <input name="store_station" type="text"  style="width:100px;"></td>
  </tr>
  <tr>
    <td align="left" valign="middle">初期库存</td>
    <td align="left" valign="middle"><input name="store_count" type="text"  style="width:50px;"></td>
  </tr>
  <tr>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle">&nbsp;</td>
    <td align="left" valign="middle"><input name="Submit" type="button" class="long-button" onClick="addpro(document.add_pro_form)" value="增加"> 
      </td>
  </tr>
</table>
    </form>  
	
</body>
</html>
