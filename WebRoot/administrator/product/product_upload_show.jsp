<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.key.ImportErrorKey"%>
<%@page import="com.cwc.app.key.UploadEditFileKey"%>
<%@ include file="../../include.jsp"%> 
<%

 	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(30);
 	String[] file = new String[2];
 	DBRow[] rows = null;
 	DBRow[] rows_union = null;
 	DBRow[] rows_file = null;
 	int upload_type = 0;
 	try
 	{
 		
 		file = productMgrZJ.importProduct(request);
 		upload_type = Integer.parseInt(file[1]);
 		HashMap<String,DBRow[]> error = productMgrZJ.importCheckProduct(file[0],upload_type);
 		
 		rows = error.get("errorProducts");
 		rows_union = error.get("errorUnions");
 		rows_file = error.get("errorFileNoExits");
 		
 	}
 	catch(FileTypeException e)
 	{
 		out.print("<script>alert('只可上传.xls文件');window.history.go(-1)</script>");
 	}
 	
 	boolean submit = true;
 	String msg = "";
 	
 	ImportErrorKey importErrorKey = new ImportErrorKey();
 %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/poshytip/jquery-1.4.min.js"></script>


<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
			

<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="onLoadInitZebraTable()" >
<table width="100%" cellpadding="0" cellspacing="0" height="100%">
  <tr>
  	<td valign="top" width="98%" align="center">
	<div id="tabs" style="width:95%; height:100%" align="center">
	<ul>
			<%
				if(upload_type==UploadEditFileKey.CommodityData)
				{
			%>
				<li><a href="#error_product">商品错误</a></li>
			<%
				}
				else if(upload_type==UploadEditFileKey.UnionRelation)
				{
			%>
				<li><a href="#error_union">关系错误</a></li>
			<%
				}
				else if(upload_type==UploadEditFileKey.ProductFile)
				{
			%>
				<li><a href="#error_file">文件不存在</a></li>
			<%
				}
			%>
			
			
	</ul>
			<div id="error_product" align="center">
				<table width="100%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;" class="zebraTable">
					 <% 
						if(rows!=null&&rows.length>0&&upload_type==UploadEditFileKey.CommodityData)
						{	
							submit = false;
					%>
						<tr>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">商品名</th>
							<th width="10%" class="right-title "style="vertical-align: center;text-align: left;">商品主条码</th>
							<th width="10%" class="right-title "style="vertical-align: center;text-align: left;">商品备用条码</th>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">错误原因</th>
						</tr>
					<%
						for(int i=0;i<rows.length;i++)
						{
					%>
						  <tr align="center" valign="middle">
							<td height="40" align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("p_name")%>&nbsp;</td>
							<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("p_code") %>&nbsp;</td>
							<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("p_code2") %>&nbsp;</td>
							<td style="border-bottom: 1px solid #dddddd;padding-left:10px;" align="left">
								<% 
									out.print("<font color='red'>位置:商品表内第"+rows[i].getString("errorProductRow")+"行</font><br/>");
									String[] errorP = rows[i].getString("errorMessage").split(",");
									for(int j = 0;j<errorP.length;j++)
									{
										out.println("<font color='red'>"+(j+1)+".</font>"+importErrorKey.getErrorMessageById(errorP[j].toString()));
										if(j<errorP.length-1)
										{
											out.print("<br/>");
										}
									}
								%>
							</td>
						  </tr>
					  <%
							}
						}
						else if(upload_type==UploadEditFileKey.CommodityData)
						{
					  %>
							<tr align="center" valign="middle">
							<th height="100%" align="center" style="padding-left:10px;font-size:xx-large;background-color: white;">上传商品检查无误</th>
							</tr>
					  <%
					  	}
					  %>
			  </table>
			</div>
			<div id="error_union">
				<table width="100%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;" class="zebraTable">
					 <% 
						if(rows_union!=null&&rows_union.length>0&&upload_type==UploadEditFileKey.UnionRelation)
						{	
							submit = false;
					%>
						<tr>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">套装条码</th>
							<th width="10%" class="right-title " style="vertical-align: center;text-align: left;">配件条码</th>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">错误原因</th>
					  	</tr>					
					<%
						
						for(int u=0;u<rows_union.length;u++)
						{
					%>
						  <tr align="center" valign="middle">
							<td height="40" align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows_union[u].getString("product")%>&nbsp;</td>
							<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows_union[u].getString("accessories") %>&nbsp;</td>
							<td style="border-bottom: 1px solid #dddddd;padding-left:10px;" align="left">
								<% 
									out.print("<font color='red'>位置:套装关系内第"+rows_union[u].getString("errorPUnionRow")+"行</font><br/>");
									String[] errorP = rows_union[u].getString("errorMessage").split(",");
									for(int e = 0;e<errorP.length;e++)
									{
										out.println("<font color='red'>"+(e+1)+".</font>"+importErrorKey.getErrorMessageById(errorP[e].toString())+rows_union[u].getString(errorP[e].toString()));
										if(e<errorP.length-1)
										{
											out.print("<br/>");
										}
									}
								%>
							</td>
						  </tr>
					  <%
							}
						}
						else if(upload_type==UploadEditFileKey.UnionRelation)
						{
					  %>
							<tr align="center" valign="middle">
							<th height="100%" align="center" style="padding-left:10px;font-size:xx-large;background-color: white;">
								上传套装关系检查无误
							</th>
							</tr>
					  <%
					  	}
					  %>
			  </table>
			</div>
			<div id="error_union">
				<table width="100%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;" class="zebraTable">
					 <% 
						if(rows_file!=null&&rows_file.length>0&&upload_type==UploadEditFileKey.ProductFile)
						{	
							submit = false;
					%>
						<tr>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">商品名</th>
							<th width="10%" class="right-title " style="vertical-align: center;text-align: left;">文件名</th>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">错误原因</th>
					  	</tr>					
					<%
						
						for(int f=0;f<rows_file.length;f++)
						{
					%>
						  <tr align="center" valign="middle">
							<td height="40" align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows_file[f].getString("p_name")%>&nbsp;</td>
							<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows_file[f].getString("file_name") %>&nbsp;</td>
							<td style="border-bottom: 1px solid #dddddd;padding-left:10px;" align="left">
								<font color="red">服务器无对应文件</font>
							</td>
						  </tr>
					  <%
							}
						}
						else if(upload_type==UploadEditFileKey.ProductFile)
						{
					  %>
							<tr align="center" valign="middle">
							<th height="100%" align="center" style="padding-left:10px;font-size:xx-large;background-color: white;">
								指向文件都存在于服务器
							</th>
							</tr>
					  <%
					  	}
					  %>
			  </table>
			</div>
	</div>
		
	</td>
  </tr>
  <script>
	$("#tabs").tabs({	
		cookie: { expires: 30000 } 
	});
</script>
 <tr>
 	<td valign="bottom">
 		<table width="100%" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="left" valign="middle" class="win-bottom-line">
					<%
						if(!msg.equals(""))
						{
					%>
						<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#000000"><%=msg%></span>
					<%
						}
					%>			  &nbsp;</td>
 				<td align="right" valign="middle" class="win-bottom-line"> 
			      <%  
			      	if(submit)
			      	{
			      %>
			      	<input type="button" name="Submit2" value="确定" class="normal-green" onClick="ajaxSaveProduct()"/>
			      <%
			      	}
			      %>
				  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.cancelImport('<%=file%>');"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
   
</table>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/saveProduct.action" name="saveProduct_form">
	<input type="hidden" name="tempfilename" id="tempfilename" value="<%=file[0]%>"/>
</form>
<script type="text/javascript">
	function savePurchaseDetail()
	{
		
			var tempfilename = $("#tempfilename").val();
			var purchase_id = $("#purchase_id").val();
			alert(tempfilename);
			alert(purchase_id);
		
	}
	
	function ajaxSaveProduct()
	{
		if(<%=submit%>)
		{
			var tempfilename = $("#tempfilename").val();	
				
			var para = "tempfilename="+tempfilename;
			
			$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true, 
			
				fadeOut:  2000
			};
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
			
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/saveProduct.action',
				type: 'post',
				dataType: 'json',
				timeout: 6000000,
				cache:false,
				data:
				{
					tempfilename:tempfilename,
					upload_type:<%=upload_type%>
				},
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert(e);
					alert("提交失败，请重试！");
				},
				
				success: function(date){

					if (date["close"])
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">保存成功！</span>' });
						$.unblockUI();
						parent.closeWinRefresh();
					}
					else
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">'+date["error"]+'</span>' });
						alert();
					}
				}
			});
		}
		else
		{
			alert("<%=msg%>");
		}
	}
</script>

</body>
</html>



