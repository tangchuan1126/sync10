<%@page import="com.cwc.app.key.GlobalKey"%>
<%@page import="com.cwc.app.key.YesOrNotKey"%>
<%@page import="com.cwc.app.key.PriceUOMKey"%>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="org.directwebremoting.json.types.JsonArray"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<%

//参数
String pcid = StringUtil.getString(request,"pcid");
String cmd = StringUtil.getString(request,"cmd","NONE");
String key = StringUtil.getString(request,"key");

String pro_line_id = StringUtil.getString(request,"pro_line_id");
int product_file_types = StringUtil.getInt(request, "product_file_types");
int product_upload_status = StringUtil.getInt(request, "product_upload_status");
int union_flag = StringUtil.getInt(request,"union_flag", -1);
String title_id = StringUtil.getString(request, "title_id");

Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));

AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adminId = adminLoggerBean.getAdid();

//根据登录帐号判断是否为客户,如果bl是true说明是客户登录
Boolean bl = DBRowUtils.existDeptAndPostInLoginBean(GlobalKey.CLIENT_ADGID, adminLoggerBean);

//如果是用户登录 查询用户下的产品
DBRow[] titles;

long number = 0;

if(bl){
	
	number = 1;
	titles = adminMgrZwb.getProprietaryByadminId(adminLoggerBean.getAdid());
	
}else{
	
	titles = mgrZwb.selectAllTitle();
}










LengthUOMKey lengthUOMKey = new LengthUOMKey();
WeightUOMKey weightUOMKey = new WeightUOMKey();
PriceUOMKey priceUOMKey = new PriceUOMKey();

String lengthUoms = "";
ArrayList lengthUomKeys = lengthUOMKey.getLengthUOMKeys();

for(int a=0;a<lengthUomKeys.size();a++){
	
	int keys = Integer.parseInt(String.valueOf(lengthUomKeys.get(a)));
	String value = lengthUOMKey.getLengthUOMKey(keys);
	lengthUoms += ";"+keys+":"+value;
}

lengthUoms = lengthUoms.substring(1);
String weightUoms = "";
ArrayList weightUomKeys = weightUOMKey.getWeightUOMKeys();

for(int a=0;a<weightUomKeys.size();a++){
	
	int keys = Integer.parseInt(String.valueOf(weightUomKeys.get(a)));
	String value = weightUOMKey.getWeightUOMKey(keys);
	weightUoms += ";"+keys+":"+value;
}

weightUoms = weightUoms.substring(1);
String priceUoms = "";
ArrayList priceUomKeys = priceUOMKey.getMoneyUOMKeys();

for(int a=0;a<priceUomKeys.size();a++){
	int keys = Integer.parseInt(String.valueOf(priceUomKeys.get(a)));
	String value = priceUOMKey.getMoneyUOMKey(keys);
	priceUoms += ";"+keys+":"+value;
}

priceUoms = priceUoms.substring(1);

 %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Product</title>
<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-ui-1.8.14.custom.min.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>

<script type="text/javascript" src="<%=ConfigBean.getStringValue("systenUiFolder")%>bower_components/jqgrid/js/jquery.jqGrid.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<link type="text/css" href="../js/jqGrid-4.1.1/js/jquery-ui-1.8.14.custom.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<style type="text/css" media="all">
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../js/art/skins/simple.css" />
<script src="<%=ConfigBean.getStringValue("systenUiFolder")%>lib/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="<%=ConfigBean.getStringValue("systenUiFolder")%>lib/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=ConfigBean.getStringValue("systenUiFolder")%>lib/jqgridtitleheigth.js"></script>
<style type="text/css">

.editselectbg td {
	background: #fbf9ee;
}
   
#gview_union span.ui-jqgrid-title input{
	display: none;
}

.aui_titleBar{ 
	border: 1px #DDDDDD solid;
}

.aui_header{
	height: 40px;
	line-height: 40px;
}

.aui_title{
	height: 40px;
	width: 100%;
	font-size: 1.2em !important;
	margin-left: 20px;
	font-weight: 700;
	text-indent: 0;
}

td.categorymenu_td div{
	margin-top:-3px;
}

.ui-jqgrid{
	margin-bottom:30px;
}

.ui-widget-content{
	border:1px #CFCFCF solid;
	border-radius: 0px;
  	-webkit-border-radius: 0px;
	-moz-border-radius:0;
}

.ui-widget-header{
	background:#f1f1f1 !important;
	border-radius: 0;
	border-top:0;
	border-left:0;
	border-right:0;
	border-bottom: 1px #CFCFCF solid;
}

.ui-tabs .ui-tabs-nav li a{
	padding: .5em 1em 13px !important;
}

.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
	height: 40px;
	margin-top: -4px !important;
	background:#fff;
}

.ui-tabs .ui-tabs-nav{
	padding :0 .2em 0;
}

.ui-tabs{
	padding:0;
}

.ui-jqgrid{
	border:0;
}

.ui-jqgrid-titlebar.ui-widget-header{
	margin-top: -40px;
	height:35px;
	line-height:30px;
}

.panel {

	margin-top:23px;
	margin-bottom: 20px;
	background-color: #fff;
	border: 1px #CFCFCF solid;
	-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
	box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
	
.panel .panelTitle{
	background: #f1f1f1;
	height: 40px;
	line-height: 40px;
	padding:0 15px;
}

.panel .panelTitle .title-left{
	float: left;
}
	
.panel .panelTitle .title-right{
	float: right;
	margin-right: 20px;
	z-index: 1000;
	position: relative;
}
	
.clear{
	clear: both;
}
	
.right-title{
	height:40px;
	background-color:#fff;
}
	
.badge{
    display: inline-block;    
    min-width: 10px;    
    padding: 3px 7px;    
    font-size: 12px;    
    font-weight: 700;    
    line-height: 1;color: #fff;    
    text-align: center;    
    white-space: nowrap;    
    vertical-align: baseline;    
    background-color: #8785F4;    
    border-radius: 15px;
} 

.badge:hover{background-color: #4B48C2;}
		
		
		
.breadnav {
            padding:0 10px; height:25px;margin-bottom: 18px;
        }
        .breadcrumb{
          font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
          font-size: 14px;
          line-height: 1.42857143;
        }
        .breadcrumb {
          padding: 8px 15px;
          margin-bottom: 20px;
          list-style: none;
          background-color: #f5f5f5;
          border-radius: 4px;
        }

        .breadcrumb > li {
          display: inline-block;
        }

        .breadcrumb > .active {
          color: #777;
        }

        .breadcrumb > li + li:before {
            padding: 0 5px;
            color: #ccc;
            content: "/\00a0";
        }
        .breadcrumb a{
		      color: #337ab7;
                text-decoration: none;	
		}
</style>
<script language="javascript">

var pcid = '<%=pcid%>';
var cmd = '<%=cmd%>';
var union_flag = '<%=union_flag%>';
var key = '<%=key%>';
var pro_line_id = '<%=pro_line_id%>';
var product_file_types = '<%=product_file_types%>';
var product_upload_status = '<%=product_upload_status%>';
var title_id = '<%=title_id%>';
var newArt;
var lastJqgridPcid;
var active_product = 1;

$().ready(function() {

	addAutoComplete(
		$("#search_key"),
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
		"merge_info",
		"p_name"
	);
	
	$("#search_key").keydown(function(event){
		
		if (event.keyCode==13) {
			search();
		}
	});
});

function filter(){

	pcid = $("#filter_pcid").val();
	cmd = "filter";
	union_flag = $("input#union_flag:checked").val();
	key = '';
	pro_line_id = $("#filter_productLine").val();
	product_file_types = $("#product_file_types").val();
	product_upload_status = $("#product_upload_status").val();
	title_id = $("#title_id").val();

	jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:'filter',pcid:pcid,union_flag:union_flag,pro_line_id:pro_line_id,product_file_types:product_file_types,product_upload_status:product_upload_status,title_id:title_id},page:1}).trigger('reloadGrid');
	jQuery("#union").jqGrid('setGridParam',{url:"subDataProducts.html?id="+0,page:1});
	jQuery("#union").jqGrid('setCaption'," Suit Relationship: ").trigger('reloadGrid');
}

function afilter(filter_pcid){
	
	jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:'filter',pcid:filter_pcid,union_flag:'<%=union_flag%>'},page:1}).trigger('reloadGrid');
}

<%

StringBuffer countryCodeSB = new StringBuffer("");
DBRow treeRows[] = catalogMgr.getProductCatalogTree();
String qx;

countryCodeSB.append("0:选择商品分类;");

for (int i=0; i<treeRows.length; i++) {
	
	if ( treeRows[i].get("parentid",0) != 0 ){
	 	qx = "├ ";
	} else {
		qx = "";
	}
	
	countryCodeSB.append(treeRows[i].getString("id")+":"+Tree.makeSpace("&nbsp&nbsp&nbsp",treeRows[i].get("level",0))+qx+treeRows[i].getString("title"));
	
	if(i<treeRows.length-1){
		
		countryCodeSB.append(";");
	}
}
%>


function closeWin(){
	
	tb_remove();
}

function modCatalogCloseWin(pc_id){
	
	refProduct($("#gridtest"),pc_id);
}

function openCatalogMenu(){
	
	$("#category").mcDropdown("#categorymenu").openMenu();
}

function showProductImg(img){
	
	if(img != ""){
		
		tb_show('',img+'?TB_iframe=true&height=350&width=700',false);
	} else {
		
		showMessage("No pictures", "alert");
	}
}

function updataProductCatalog(id,catalog_id,p_name) {
	
	$.artDialog.open("mod_product_catalog.html?pc_id="+id+"&cat_id="+catalog_id+"&iframe=false", {title: 'Category Of ['+p_name+']',width:'800px',height:'450px', lock: true,opacity: 0.3});
}

function addProductCode(id, p_name){
	$.blockUI({message: ''}); //遮罩打开
	$("div.blockOverlay").css({opacity:0.1});
	//$.artDialog.open("add_product_code.html?pc_id="+id, {title: 'Codes Of ['+p_name+']',width:'650px',height:'450px', lock: true,opacity: 0.3,close:function(){modCatalogCloseWin(id);}});
	$.artDialog.open("<%=ConfigBean.getStringValue("systenUiFolder")%>pages/product/index.html?pc_id="+id, {title: 'Codes Of ['+p_name+']',width:'750px',height:'419px', lock: true,opacity: 0.1,fixed: true,close:function(){modCatalogCloseWin(id);}});
}

function modProductSn(id, p_name) {
	
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/mod_product_sn.html?pc_id='+id;
	
	var sel = $.artDialog.open(url, {id:"abc123",title: 'SN Length of ['+p_name+']',width:'500px',height:'200px', lock: true,opacity: 0.3,close:function(){modCatalogCloseWin(id);},ok:function(){
		var el=this.DOM.wrap;		
			
		var ifrmebox=el.find(".aui_state_full");		
		var iframeobj = ifrmebox.find("iframe")[0];
		var iframeabcd = iframeobj.contentWindow;
		var res = iframeabcd.save_sn();
	        
		if(res){
			sel.close();
		}else{
			return res;
		}
	        
		},okVal:'Submit'}
	);
}

function modCell(rowId,colName,data){
	$("#gridtest").jqGrid('setCell', rowId, colName, data);
}

function addRow(rowId,data){
	$("#gridtest").jqGrid('addRowData', rowId, data,"first");
}

function addNewProduct(){
	
	var newArt = $.artDialog.open('/Sync10/administrator/product/add_new_product.html', {
        id: "newartdialog",
        title: 'Add Product',
        width: '804px',
        height: '510px',
        lock: true,
        opacity: 0.3
    });
	noa_subedit=false;
	
	if (list2this) {
		ClearEditor(list2this, lastRowIndex, lastColIndex);
    }
}

function updataProductCatalogNoId(){
	
	tb_show('修改商品线及分类','mod_product_catalog.html?pc_id='+lastJqgridPcid+'TB_iframe=true&height=450&width=500',false);
}

function autoComplete(obj) {
	
	addAutoComplete(
		obj,
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
		"merge_info",
		"p_name"
	);
}

function beforeShowAdd(formid){
	
	$("#tr_unit_name").remove();
	$("#tr_p_code").remove();
	$("#tr_weight").remove();
	$("#tr_unit_price").remove();
	$("#tr_volume").remove();
	$("#tr_gross_profit").remove();
	jQuery("#union").jqGrid('setGridParam',{editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditUnionJqgrid.action?set_pid='+union_id,});
}
	
function beforeShowAddProduct(formid){
	
	$("#tr_alive_text").remove();
}
	
function beforeShowDel(formid){
	jQuery("#union").jqGrid('setGridParam',{editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditUnionJqgrid.action?set_pid='+union_id,});
}
	
function afterShowAdd(formid){
	
	autoComplete($("#p_name"));
}
	
function afterShowEdit(formid){
	autoComplete($("#p_name"));
}
function beforeShowEdit(){	
	return false;
}
function errorFormat(serverresponse,status){
	return errorMessage(serverresponse.responseText);
}
	
function refProductUnion(obj,rowid,p_name,iRow,iCol){
	
	var para ="p_name="+p_name;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductGridJSONByPname.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:para,
		async:false,
		error: function(){
			showMessage("System error.", "error");
		},
		success: function(data){
			obj.setRowData(rowid,data);
		}
	});
}
	
function refProduct(obj,rowid,iRow,iCol){
	
	var para ="pc_id="+rowid;
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductGridJSONById.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:para,
		async:false,
		error: function(){
			showMessage("System error.", "error");
		},
		success: function(data) {
			obj.setRowData(rowid,data);
		}
	});
}
	
var alliCol,Timeouteditcell="";

function errorMessage(val){

	if(val.indexOf("[") > -0)
		var error1 = val.split("[");
		
	if(val.indexOf("]") > -0)
		var error2 = error1[1].split("]");
	
	if(error2[0]){
		return error2[0];
	}
}
	
var icolclick=true;
	
function refushUnion(obj,pid,iRow,iCol){
	
	var para ="pid="+pid;

	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getUnionProductDetailsByPid.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:para,
		error: function(){
			showMessage("System error.", "alert");
		},
		success: function(data){

			icolclick=false;
			for(i=0;i<data.length;i++){
			
				obj.setRowData(data[i].set_pid,data[i]);
			}
		}
	});
}
	
function afterComplete(){
	refProduct($("#gridtest"),union_id);
}

function closeS(){
	newArt.close();
}
	
function search() {

	pcid = $("#filter_pcid").val();
	cmd = "search";
	union_flag = $("input#union_flag:checked").val();
	key = $("#search_key").val();
	pro_line_id = '';
	product_file_types = '';
	product_upload_status = '';
	title_id = $("#title_id").val();
	
	jQuery("#gridtest").jqGrid('setGridParam', {
		postData: {
			cmd: cmd,
			pcid: pcid,
			key: key,
			title_id: title_id
		}
	}).trigger('reloadGrid');

	jQuery("#union").jqGrid('setGridParam', {
		url: "/Sync10/administrator/product/subDataProducts.html?id=" + 0,
		page: 1
	});
	
	jQuery("#union").jqGrid('setCaption', " Suit Relationship: ").trigger('reloadGrid');
}

function swichProductAlive(name,pc_id,curAlive){
	var msg;
	
	if (curAlive==1){
		msg = "Inactive : [ "+name+"  ] ?";
	}else{
		msg = "Active : [ "+name+" ]  ?";
	}
	
	$.artDialog({
		content: msg,
		icon: 'warning',
		title:'',
		opacity: 0.1,
		lock: true,
		fixed:true,
		okVal: 'Confirm',
		ok: function () {
			
			var para ="pc_id="+pc_id+"&curAlive="+curAlive;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/swichProductAlive.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				error: function(e){
					showMessage("System error.", "error");
				},
				success: function(data){
					if(data.result=="true"){
						refProduct(jQuery("#gridtest"),pc_id);
					}
				}
			});
		},
		cancelVal: 'Cancel',
		cancel:true
	});
}

function ajaxLoadProductLineMenuPage(title_id, pc_line_id, divId){
	
	var para = "title_id="+title_id+"&product_line_id="+pc_line_id+"&divId="+divId;
	$.ajax({
		url: 'product_line_menu_for_title.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request){
		},
		
		error: function(){
		},
		
		success: function(html)
		{
			$("#productLinemenu_td").html(html);
		}
	});
}

function ajaxLoadCatalogMenuPage(id,title_id,divId){
	var para = "id="+id+"&title_id="+title_id;
	$.ajax({
		url: 'product_catalog_menu_for_productline.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request){
		},
		
		error: function(){
		},
		
		success: function(html)
		{
			$("#categorymenu_td").html(html);
		}
	});
}
	
function showImg(pc_id){
	window.open("product_file.html?pc_id="+pc_id);
}

function make_tab(productId){
	var purchase_id="";
	var factory_type="";
	var supplierSid="";
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/lable_template_ui_show.html?purchase_id='+purchase_id+'&pc_id='+productId+'&factory_type='+factory_type+'&supplierSid='+supplierSid; 
	$.artDialog.open(uri , {title: 'Label List',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
}

function manage_pro_title(pc_id,p_name){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_list_by_product_related_for_select.html?pc_id="+pc_id;
	$.artDialog.open(url , {title: '管理'+p_name+'的TITLE',width:'700px',height:'450px', lock: true,opacity: 0.3,fixed: true});
}

function changeTitle(){
	ajaxLoadProductLineMenuPage($("#title_id").val(), "");
}

function importProductTitles(_target){
	
	var fileNames = $("#file_names").val();
	var obj  = {
		reg:"xls",
		limitSize:2,
		limitNum:1
	}
	
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	
	$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}

function uploadFileCallBack(fileNames){

	if($.trim(fileNames).length > 0 ){
	
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/check_product_title_show.html?fileNames="+fileNames;
		$.artDialog.open(uri,{title: "检测上传用户title信息",width:'850px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}
}
	
function refreshWindow(){
	window.location.reload();
};

function loadData(){

	var titles = <%=new JsonObject(titles).toString()%> ;
	var item=[];
	for(var i=0;i<titles.length;i++){
		var op={};       
		op.id=titles[i].title_id;                                                                                                                                                                                                                                                                                                                                                                                                      
		op.name=titles[i].title_name;
		item.push(op);
	}
	
	var data=[{
	
		key:'Title: ',
		type:'title',
		son_key:'Product Line: ',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadProductLineAction.action',
		
		son_key2:'Parent Category: ',
		url2:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadFirstProductCatalogAction.action',
		
		son_key3:'Sub Category: ',
		url3:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadSecondProductCatalogAction.action',	
				
		son_key4:'Sub-Sub Category: ',
		url4:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadThirdProductCatalogAction.action',

		array:item
	},{key:'Product Type: ',select:'true',type:'leixing',array:[{id:'0',name:'ITEM'},{id:'1',name:'UNIT'}]},
	{key:'Photos Status: ',select:'true',type:'picStuts',array:[{id:'1',name:'Not Complete'},{id:'2',name:'Complete'}]},
	//{key:'Is Active: ',select:'true',type:'isActive',array:[{id:'-1',name:'All'},{id:'1',name:'Active'},{id:'0',name:'Inactive'}]},];
	{key:'Is Active: ',select:'true',type:'isActive',array:[{id:'1',name:'Active'},{id:'0',name:'Inactive'}]},];
	
	initializtion(data);
}	
	
function custom_seach(){
	
	var array=getSeachValue();
	title_id="";
	pro_line_id="";
	product_file_types=0;
	product_upload_status=0;
	union_flag=-1;
	pcid="";
	key="";
	cmd="filter";
	
	for(var i=0;i<array.length;i++){
		
		if(array[i].type=='title'){
			title_id=array[i].val;
		}
		if(array[i].type=='title_son'){
			pro_line_id=array[i].val;
		}
		if(array[i].type=='title_son_son'){
			pcid=array[i].val;
		}else if(array[i].type=='title_son_son_son'){
			pcid=array[i].val;
		}else if(array[i].type=='title_son_son_son_son'){
			pcid=array[i].val;
		}
		
		if(array[i].type=='picType'){
			product_file_types=array[i].val;
		}
		
		if(array[i].type=='picStuts'){
			product_upload_status=array[i].val;
		}
		
		if(array[i].type=='leixing'){
			union_flag=array[i].val;
		}
		
		if(array[i].type=='isActive'){
			active_product=array[i].val;
		}
	}
	
	jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:cmd,pcid:pcid,union_flag:union_flag,pro_line_id:pro_line_id,product_file_types:product_file_types,product_upload_status:product_upload_status,title_id:title_id,active:active_product},page:1}).trigger('reloadGrid');
	jQuery("#union").jqGrid('setGridParam',{url:"subDataProducts.html?id="+0,page:1});
	jQuery("#union").jqGrid('setCaption'," Suit Relationship: ").trigger('reloadGrid');	
}

function eso_button_even(){
		
	document.getElementById("eso_search").onmouseup=function(oEvent) {
		search();
	};
}

$(function(){
	tabletitleauto.settitle({domid:"#gbox_gridtest",height:35,fontsize:14});
});
	
function exportProduct(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	var para = "catalog_id="+pcid+"&union_flag="+union_flag+"&active="+active_product;
	para += '&cmd='+cmd+"&pro_line_id="+pro_line_id+"&product_file_types="+product_file_types+"&product_upload_status="+product_upload_status+"&title_id="+title_id+'&key='+key;
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/exportProduct.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		async:false,
		data:para,
		error: function(){
			showMessage("System error.", "error");
			$.unblockUI();
		},
		success: function(date){
			if(date["canexport"]=="true") {
				document.export_product.action=date["fileurl"];
				document.export_product.submit();
			} else {
				showMessage("No data.", "alert");
			}
			$.unblockUI();
		}
	});
	$.unblockUI();
}

function exportProductUnion(){
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/exportProductUnion.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data: {
			catalog_id:$("#filter_pcid").val(),
			pro_line_id:$("#filter_productLine").val() 
		},
		error: function(){
			showMessage("System error.", "error");
		},
		success: function(date){
			if(date["canexport"]=="true") {
				document.export_product.action=date["fileurl"];
				document.export_product.submit();
			} else {
				showMessage("Cannot export.", "alert");
			}
		}
	});
}
	
function addItemLabel(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/lable_template/lable_template_list.html";
	$.artDialog.open(uri , {title: "Label Template List",width:'875px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}

</script>

<style>
form{
	padding:0px;
	margin:0px;
}

.ui-datepicker {z-index:1200;}
.rotate{
	/* for Safari */
	-webkit-transform: rotate(-90deg);

	/* for Firefox */
	-moz-transform: rotate(-90deg);

	/* for Internet Explorer */
	filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
}

.ui-jqgrid tr.jqgrow td {

	white-space: normal !important;
	height:auto;
	vertical-align:middle;
	padding:3px;
}

<!-- 高级搜索的样式-->
.search_shadow_bg{

	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}

.search_input{

	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;	
	border:1px #bdbdbd solid;	
}

#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}

#easy_search {
	position:absolute;
	left:318px;
	top:-19px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
ul.myul{list-style-type:none;}
ul.myul li{float:left;width:150px;border:0px solid red;line-height:25px;height:25px;border:0px solid silver;}

.product_content{ padding:10px;}
.common-space{width: 15px;display: inline-block;}
.common-tip{  margin-top: -20px;  margin-left: 98px; font-size: 0.8em; position: absolute;color: #fff;padding: 1px 5px 1px 3px;
	transform: rotate(90deg) scale(1) skew(1deg) translate(10px);
	-webkit-transform: rotate(30deg) scale(1) skew(1deg) translate(0px);
	-moz-transform: rotate(30deg) scale(1) skew(1deg) translate(0px);
	-o-transform: rotate(30deg) scale(1) skew(1deg) translate(0px);
	-ms-transform: rotate(30deg) scale(1) skew(1deg) translate(0px); 
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
	border-bottom-left-radius: 5px;
	border-bottom-right-radius: 5px;
	background: -webkit-gradient(linear, 80% 20%, 10% 21%,from(#CE0000),to(#FF475C));
  }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="loadData()">
	
	<!-- <div style="border-bottom:1px solid #CFCFCF; height:25px;padding-top: 15px;margin-left: 10px;margin-bottom: 3px;">
		<img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">
		<span style="font-size: 13px;font-weight: bold"> Setup » Product</span>
	</div> 
	<div class="breadnav">
		<ol class="breadcrumb">
		            <li><a href="#">Setup</a></li>
		            <li class="active">Product</li>
		        </ol>
		</div>-->
	
	<div class="product_content">
	<div id="tabs">
		<ul>
			<li><a href="#av1">Advanced Search</a></li>
			<li><a href="#av2">Common Tools<!-- <span class="common-space">&nbsp;</span><div class="common-tip">Full</div></a> --></li>		 
		</ul>
		<div id="av1">
			<!-- 条件搜索 -->
			<div id="av"></div>
			<input type="hidden" id="atomicBomb" value=""/>
			<input type="hidden" id="title" />
		</div>
		<div id="av2">
			<div id="easy_search_father">
				<div id="easy_search" style="margin-top:-2px;">
					<a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search_2.png" width="70" height="60" border="0"/></a>
				</div>
		    </div>
		    
	  		<table width="400" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="418">
						<div  class="search_shadow_bg">
							<input name="search_key" id="search_key" value=""  type="text" class="search_input" style="  height: 30px;font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)search()"/>
						</div>
					</td>
				</tr>
			</table>	
			<script>eso_button_even();</script>
	    </div>
	</div>
	
	<script type="text/javascript">
	
	var tabs = $("#tabs").tabs({
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
	});
	
	</script>


<div class="panel">
	<div class="panelTitle">
		<div class="title-right">
			<div>
				<input name="file_names" id="file_names" type="hidden" />	
				<div class="buttons-group minor-group" style="margin: 6px;">
				<a name="Submit" class="buttons" onClick="addItemLabel()"><i class="icon-tags"></i>&nbsp;ItemLabel</a>
				
				<a name="Submit" class="buttons" onClick="downloadTemplate()"><i class="icon-download-alt"></i>&nbsp;Download Template</a>
				
 				<a name="Submit" class="buttons" onClick="exportProduct()"><i class="icon-share-alt"></i>&nbsp;ExportProduct</a>
<!-- 			<a name="Submit" class="buttons"  onClick="exportProductUnion()"><i class="icon-share-alt"></i>&nbsp;ExportUnion</a> -->
				</div>  
				<!-- &nbsp;&nbsp;<input name="Submit" type="submit" class="long-button" value="ExportPorduct" onClick="exportProduct()">
				<input name="Submit" type="submit" class="long-button" value="ExportUnion" onClick="exportProductUnion()"> -->
			</div>
			<form name="export_product" method="post"></form>
			<div style="clear:both"></div>
		</div>
	</div>
<table id="gridtest"></table>

<form action="" name="downloadTemplateForm" id="downloadTemplateForm"></form>

<script type="text/javascript">

function downloadTemplate(){
	
	var url = '../../administrator/product/SKU-CATEGORY-TITLE.xlsm';
	
	document.downloadTemplateForm.action = url;
    document.downloadTemplateForm.submit();
}

function cleanSearchKey(divId){
	
	$("#"+divId+"search_product_name").val("");
}

function addProductsPicture()
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_file_upload.html?";
	var addUri =  getSelectedIdAndNames();
 	if($.trim(addUri).length < 1 ){
		showMessage("请先选择商品!","alert");
		return ;
	}else{uri += addUri;}
 	var reg		= new RegExp("&","g"); //创建正则RegExp对象 
	var pc_ids	= addUri.substr(1).replace(reg,"=");  
	var pcIds	= pc_ids.split("=")[1];
	$.artDialog.open(uri , {title: "商品图片上传",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,
		close:function()
		{
			if(pcIds.length > 0)
			{
				var pcIdArr = pcIds.split(",");
				if(pcIdArr.length > 0)
				{
					for(var i = 0; i < pcIdArr.length; i ++)
					{
						modCatalogCloseWin(pcIdArr[i]);
					}
				}
			}
		}
	});
}
function getSelectedIdAndNames(){
	s = jQuery("#gridtest").jqGrid('getGridParam','selarrrow')+"";
	var theFirstName = "";
 	var array = s.split(",");
	var strIds = "";
 	for(var index = 0 , count = array.length ; index < count ; index++ ){
 	   if(0 == index)
 	   {
 		  theFirstName = $("#gridtest").getCell(array[0],"p_name");
 	   }
	   var number = array[index];
 	   var thisid= $("#gridtest").getCell(number,"pc_id");
 	   strIds += (","+thisid);
	}
	if(strIds.length > 1 ){
	    strIds = strIds.substr(1);
	}
	if(strIds+"" === "false"){return "";}
	
	return  "&pc_id="+strIds+"&theFirstName="+theFirstName;
}
function addProductPicture(pc_id,name)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_file_upload.html?pc_id="+pc_id+"&theFirstName="+name;
	$.artDialog.open(uri , {title: "Photos Of "+name+"",width:'875px',height:'500px', lock: true,opacity: 0.3,close:function(){modCatalogCloseWin(pc_id);},fixed: true});
}
function addProductFunc()
{
		alert("11");
}
</script>
<br/>
<div id="pager2"></div>
<table id="union"></table>
<div id="pageunion"></div>  
<script type="text/javascript">
	
	var	lastsel;
	var initSring=function(elem){

		var obj=$(elem);
  		var valsring =obj.val();
		if(valsring.indexOf("(") > -1)
 			obj.val(valsring.split(/\(/)[0]);
 			
  		return false;
	};
	
	function pcodeRule(value, colname){
		if(value == ""){
			return [false, "Main Code: Field is Required"];
		}
		if(!regexp.test(value)){
			return [false, "Main code: Field only support numbers, letters and $ - + /"];
		}
		
		return [true, ""];
	}
	
			var SringTemplate = {editoptions: { dataInit: initSring }};

			var lastColIndex, lastRowIndex, __iRow, list2this, _posts = false,parenttabletrid="";
			
			var regexp = new RegExp("^[A-Za-z0-9$+/-]+$");
			
            $("#gridtest").jqGrid({
     			sortable: true,
                url: '/Sync10/administrator/product/dataProducts.html', //dataDeliveryOrderDetails.html
                postData: {
                    pcid: pcid,
                    cmd: cmd,
                    pro_line_id: '',
                    active: 1
                },
                datatype: "json",
                width: parseInt(document.body.scrollWidth * 0.97),
                height: 235,
                autowidth: false,
                shrinkToFit: true,
                jsonReader: {
                    id: 'pc_id',
                    repeatitems: false,
                    subgrid: {
                        repeatitems: false
                    }
                },
                colNames: ['pc_id', 'catalog_id', '', 'Product Name', 'Main Code', 'Codes', 'Freight Class', 'NMFC Code', 'Category', 'Unit', 'Length', 'Width', 'Height', 'Weight', 'Price', 'Volume', '<em>Length</em><em>UOM</em>', '<em>Weight</em><em>UOM</em>', '<em>Price</em><em>UOM</em>', 'LengthUOMKey', 'WeightUOMKey', 'PriceUOMKey', '<em>Sn</em><em>Length</em>', 'Images', ''],
                colModel: [{
                        name: 'pc_id',
                        index: 'pc_id',
                        hidden: true,
                        sortable: false
                    }, {
                        name: 'catalog_id',
                        index: 'catalog_id',
                        hidden: true,
                        sortable: false
                    }, {
                        name: 'unionimg',
                        index: 'unionimg',
                        sortable: false,
                        width: 20
                    }, {
                        name: 'p_name',
                        index: 'p_name',
                        editable: true,
                        align: 'left',
                        editrules: {
                            required: true
                        },
                        width: 190
                    }, {
                        name: 'p_code',
                        index: 'p_code',
                        editable: true,
                        align: 'left',
                        editrules: {
                            required: true,
                            custom: true,
                            custom_func: pcodeRule
                        },
                        width: 110
                    }, {
                        name: 'p_codes',
                        index: 'p_codes',
                        align: 'left',
                        hidden: true,
                        editable: false,
                        width: 130
                    }, {
                        name: 'freight_class',
                        index: 'freight_class',
                        editable: true,
                        align: 'left',
                        hidden: true,
                        editrules: {
                            minValue: 0,
                            required: true,
                            number: true
                        },
                        width: 110
                    }, {
                        name: 'nmfc_code',
                        index: 'nmfc_code',
                        editable: true,
                        align: 'left',
                        hidden: true,
                        editrules: {
                            required: true
                        },
                        width: 110
                    }, {
                        name: 'catalog_text',
                        index: 'catalog_text',
                        sortable: false,
                        align: 'left',
                        editable: false,
                        edittype: 'button',
                        editoptions: {
                            value: 'Mod Category',
                            class: 'button_long_refresh'
                        },
                        width: 155
                    }, {
                        name: 'unit_name',
                        hidden: true,
                        index: 'unit_name',
                        sortable: false,
                        editable: true,
                        align: 'center',
                        width: 60,
                        editrules: {
                            required: true
                        },
                        width: 35
                    }, {
                        name: 'length',
                        index: 'length',
                        sortable: false,
                        editable: true,
                        align: 'center',
                        editrules: {
                            minValue: 0,
                            required: true,
                            number: true
                        },
                        width: 45
                    }, {
                        name: 'width',
                        index: 'width',
                        sortable: false,
                        editable: true,
                        align: 'center',
                        editrules: {
                            minValue: 0,
                            required: true,
                            number: true
                        },
                        width: 45
                    }, {
                        name: 'heigth',
                        index: 'heigth',
                        sortable: false,
                        editable: true,
                        align: 'center',
                        editrules: {
                            minValue: 0,
                            required: true,
                            number: true
                        },
                        width: 45
                    }, {
                        name: 'weight',
                        index: 'weight',
                        sortable: false,
                        editable: true,
                        align: 'center',
                        editrules: {
                            required: true,
                            number: true
                        },
                        width: 50
                    }, {
                        name: 'unit_price',
                        index: 'unit_price',
                        editable: true,
                        editrules: {
                            required: true,
                            number: true
                        },
                        editable: true,
                        sortable: false,
                        align: 'center',
                        width: 50
                    }, {
                        name: 'volume',
                        index: 'volume',
                        editable: false,
                        align: 'center',
                        editrules: {
                            number: true,
                            required: true
                        },
                        editrules: {
                            required: true,
                            number: true
                        },
                        width: 50
                    },

                    {
                        name: 'length_uom_name',
                        index: 'length_uom_name',
                        editable: true,
                        edittype: 'select',
                        align: 'center',
                        editrules: {
                            required: true
                        },
                        width: 45
                    }, {
                        name: 'weight_uom_name',
                        index: 'weight_uom_name',
                        editable: true,
                        edittype: 'select',
                        editable: false,
                        align: 'center',
                        editrules: {
                            required: true
                        },
                        width: 45
                    }, {
                        name: 'price_uom_name',
                        index: 'price_uom_name',
                        editable: true,
                        edittype: 'select',
                        editable: false,
                        align: 'center',
                        editrules: {
                            required: true
                        },
                        width: 45
                    },

                    {
                        name: 'length_uom',
                        index: 'length_uom',
                        editable: true,
                        hidden: true,
                        editable: false,
                        align: 'center',
                        editrules: {
                            required: true
                        },
                        width: 50
                    }, {
                        name: 'weight_uom',
                        index: 'weight_uom',
                        editable: true,
                        hidden: true,
                        editable: false,
                        align: 'center',
                        editrules: {
                            required: true
                        },
                        width: 50
                    }, {
                        name: 'price_uom',
                        index: 'price_uom',
                        editable: true,
                        hidden: true,
                        editable: false,
                        align: 'center',
                        editrules: {
                            required: true
                        },
                        width: 50
                    }, {
                        name: 'sn_size',
                        index: 'sn_size',
                        sortable: false,
                        editable: false,
                        align: 'center',
                        formatter: function(data) {
                            return data;
                        },
                        editrules: {
                            minValue: 0,
                            required: true,
                            number: true
                        },
                        formatoptions: {
                            decimalPlaces: 0,
                            thousandsSeparator: ","
                        },
                        width: 50
                    }, {
                        name: 'img',
                        index: 'img',
                        sortable: false,
                        align: 'left',
                        width: 105
                    }, {
                        name: 'alive_text',
                        index: 'alive_text',
                        sortable: false,
                        align: 'left',
                        width: 85
                    }

                ],
                rowNum: 50, //-1显示全部
                mtype: "GET",
                rownumbers: true,
                pager: '#pager2',
                sortname: 'pc_id',
                viewrecords: true,
                sortorder: "asc",
                //cellEdit: true, 
                cellsubmit: 'remote',
                //multiselect: true,
                caption: "Product Basic Info",
                onCellSelect: function(rowid, iCol) {
                    //gaowenzhen,2015.2.4===2

                    var inds = $("#" + rowid + "")[0].rowIndex;

                    if (union_td_input_edit != inds) {

                        if (!_posts) {
                            list2this = this;
                            var $this = $(this);
                            $this.jqGrid('setGridParam', {
                                cellEdit: true
                            });
                            $this.jqGrid('editCell', __iRow, iCol, true);
                            $this.jqGrid('setGridParam', {
                                cellEdit: false
                            });
                            lastRowIndex = __iRow;
                            lastColIndex = iCol;
                        }
                    }

                },
                beforeSelectRow: function(rowid, e) {
                    //gaowenzhen,2015.2.4===1 
                   

                    e.stopPropagation();

                    var trobj = $("#" + rowid + "");
                    parenttabletrid = trobj[0].rowIndex;
                    if (union_td_input_edit != parenttabletrid) {
                         noa_subedit=false;

                        if (subtableinputedit)
                            savecelluniontd();

                        var tr = $("#gridtest tbody tr").eq(union_td_input_edit);
                        tr.removeClass('ui-state-disabled').removeClass('editselectbg');

                        
                       addsubtable(rowid);

                        //--------------------



                        if ($(e.target).find("input:text").length < 1 && _posts) {
                            _posts = false;
                        }

                        if (!_posts) {

                            list2this = this;
                            var $this = $(this),
                                $td = $(e.target).closest('td'),
                                $tr = $td.closest('tr'),
                                iRow = $tr[0].rowIndex,
                                iCol = $.jgrid.getCellIndex($td);
                            __iRow = iRow;


                            if (!addsubitmeset && typeof lastRowIndex !== "undefined" && typeof lastColIndex !== "undefined" &&
                                (iRow !== lastRowIndex || iCol !== lastColIndex)) {

                                ClearEditor(this, iRow, iCol);
                                addsubitmeset=false;

                            }
                        }

                        // union_td_input_edit=inds;

                        return true;
                    } else {

                        return false;
                    }

                },
                afterEditCell: function(rowid, cellName, cellValue, iRow) {

                    $("#" + rowid + "").addClass('editselectbg');

                    if (union_td_input_edit != iRow) {
                        union_td_input_edit = "";
                    }

                    if (!_posts) {

                        list2this = this;
                        var cellDOM = this.rows[iRow],
                            oldKeydown,
                            $cellInput = $('input, select, textarea', cellDOM),
                            events = $cellInput.data('events'),
                            $this = $(this);
                        if (events && events.keydown && events.keydown.length) {
                            oldKeydown = events.keydown[0].handler;
                            $cellInput.unbind('keydown', oldKeydown);

                            $cellInput.bind('keydown', function(e) {
                            	
                            	if (e.keyCode == 9) {

                                    e.stopPropagation();
                                    // var tdindex = window.parseInt(iCol) + 1;
                                    // var tds = $(tr).find("td").eq(tdindex);
                                    // if (tds.length > 0) {
                                    //     tds.click();
                                    // }
                                    return false;

                                }


                                $this.jqGrid('setGridParam', {
                                    cellEdit: true
                                });
                                oldKeydown.call(this, e);
                                $this.jqGrid('setGridParam', {
                                    cellEdit: false
                                });

                            });
                        }
                    }

                },
                onSelectCell: function(id, name) {


                    if (id == null) {
                        ids = 0;
                        if (jQuery("#union").jqGrid('getGridParam', 'records') > 0) {
                            jQuery("#union").jqGrid('setGridParam', {
                                url: "/Sync10/administrator/product/subDataProducts.html?id=" + id,
                                page: 1
                            });
                            jQuery("#union").jqGrid('setCaption', " Suit Relationship: ").trigger('reloadGrid');
                        }
                    } else {
                        var p_name = jQuery("#gridtest").jqGrid('getCell', id, 'p_name');
                        var catalog_id = jQuery("#gridtest").jqGrid('getCell', id, 'catalog_id');
                        if (name == "catalog_text") {
                            updataProductCatalog(id, catalog_id, p_name);
                        }
                        if (name == "p_codes") {
                            addProductCode(id, p_name);
                        }
                        if (name == "img") {
                            addProductPicture(id, p_name);
                        }
                        // Yuanxinyu
                        if (name == "sn_size") {
                            modProductSn(id, p_name);
                        }

                        jQuery("#union").jqGrid('setGridParam', {
                            url: "/Sync10/administrator/product/subDataProducts.html?id=" + id,
                            page: 1
                        });
                        jQuery("#union").jqGrid('setCaption', p_name + " Suit Relationship").trigger('reloadGrid');
                    }
                    formatUom(id);
                },
                formatCell: function(id, name, val, iRow, iCol) {

                    formatUom(id);
                },
                beforeEditCell: function(id, name, val, iRow, iCol) {

                },
                beforeSaveCell: function(rowid, celname, value, iRow, iCol) {

                   

                    _posts = true;

                    var tdall = $("#gridtest").jqGrid("getGridParam", "colModel");

                    var tdjosn = tdall[iCol];
                    var _minValue = tdjosn.editrules.minValue;
                    var _number = tdjosn.editrules.number;
                    var valueis = false;
                    valueis = (/\d/.test(value) === _number);

                    if (!valueis) {

                        return false;

                    }

                },
                afterSaveCell: function(rowid, name, val, iRow, iCol) {
                    
                    //gaowenzhen,2015.2.4===set
                    list2this = this;
                    var $this = $(this);

                    setTimeout(function() {

                        //===end


                        if (name == "catalog_text" || name == "p_code" || name == "length" || name == "width" || name == "heigth" || name == "unit_name" || name == "weight" || name == "unit_price") {

                            refProduct(jQuery("#gridtest"), rowid, iRow, iCol);
                        }
                        if (name == "weight" || name == "unit_price") {

                            refushUnion(jQuery("#gridtest"), rowid, iRow, iCol); //这里的rowid已经是配件的ID了
                        }
                        if (name == 'length_uom_name' || name == 'weight_uom_name' || name == 'price_uom_name') {
                            refProduct(jQuery("#gridtest"), rowid, iRow, iCol);
                            formatUom(rowid);
                        }


                        //gaowenzhen,2015.2.4===set

                        if (typeof lastRowIndex != "undefined" && typeof lastColIndex != "undefined") {

                            ClearEditor(this, iRow, iCol);
                        }

                        _posts = false;

                    }, 120);

                    //===end


                },
                cellurl: '/Sync10/action/administrator/product/gridEditProductJqgrid.action',
                editurl: '/Sync10/action/administrator/product/gridEditProductJqgrid.action',
                errorCell: function(serverresponse, status) {
                    showMessage(errorMessage(serverresponse.responseText), "alert");
                }
            });


            //保存子表单元格
            function savecelluniontd() {

                subtableinputedit = false;
				if(unionclosejson){
					
	                var unionobj = $(unionclosejson.objs);
	                var unioncoli = unionclosejson.icol;
	                var unionirow = unionclosejson.irow;
	                var unionirowtr = $(unionclosejson.objs.rows[unionirow]);
	                var uniontd = unionirowtr.find('td').eq(unioncoli);
	                if (uniontd.find("input:text").eq(0).length > 0)
	                    unionobj.jqGrid('saveCell', unionirow, unioncoli);
				}


            }

            //更新子表
            function addsubtable(rowid) {

                var p_name = jQuery("#gridtest").jqGrid('getCell', rowid, 'p_name');

                jQuery("#union").jqGrid('setGridParam', {
                    url: "/Sync10/administrator/product/subDataProducts.html?id=" + rowid,
                    page: 1
                });
                jQuery("#union").jqGrid('setCaption', p_name + " Suit Relationship").trigger('reloadGrid');
            }

           
           //关闭或开启父表编辑

            function ClearEditor(giridobj, iRow, iCol, unionobjtr) {

                var $this = $(giridobj);


                if (unionclosejson && !unionobjtr) {

                    savecelluniontd();
                }



                if (typeof lastRowIndex != "undefined" && typeof lastColIndex != "undefined" && giridobj) {


                    var trcellDOM_ = "";
                    if (giridobj.nodeName == "TABLE") {

                        trcellDOM_ = $(giridobj.rows[lastRowIndex]);
                    }


                    $this.jqGrid('setGridParam', {
                        cellEdit: true
                    });
                  
                    $this.jqGrid('saveCell', lastRowIndex, lastColIndex);
                  

                    $this.jqGrid('setGridParam', {
                        cellEdit: false
                    });



                    if (trcellDOM_.length > 0 && !unionobjtr) {
                       

                     if(!noa_subedit)
                        trcellDOM_.removeClass().addClass('ui-widget-content jqgrow ui-row-ltr');

                        var tdall = trcellDOM_.find("td").eq(lastColIndex);
                        tdall.removeAttr("class").removeAttr('tabindex');

                    }


                }


            }


            //关闭开启的单元格-----########
            $(document).click(function(e) {

                //var clickedElement = e.target;
                if(e.target.name != "titleSe"){
                	e.preventDefault();
                }
                //gets the element where the click event happened


                //if(clickedElement.offsetParent.id!="gview_gridtest"){


                if (list2this) {

                    ClearEditor(list2this, lastRowIndex, lastColIndex);

                }

                //

                if (unionclosejson) {

                    var unionobj = $(unionclosejson.objs);
                    var unioncoli = unionclosejson.icol;
                    var unionirow = unionclosejson.irow;

                    var uniontr = $(unionclosejson.objs.rows[unionirow]);

                    unionobj.jqGrid('setGridParam', {
                        cellEdit: true
                    });

                    unionobj.jqGrid('saveCell', unionirow, unioncoli);
                    unionobj.jqGrid('setGridParam', {
                        cellEdit: false
                    });

                    uniontr.removeClass().addClass('ui-widget-content jqgrow ui-row-ltr');

                    var tdall = uniontr.find("td").eq(unioncoli);
                    tdall.removeAttr("class").removeAttr('tabindex');


                }

              	//若点击Common Tools的输入框 获取焦点
                if(e.target.id == "search_key"){
					window.setTimeout(function(){
						$("#search_key").focus();
					}, 400);	
				}

                if(e.target.name != "titleSe"){
              		return false;
                }
            });

            jQuery("#gridtest").jqGrid('navGrid', "#pager2", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: true
            }, {
                closeAfterEdit: true,
                afterShowForm: afterShowEdit,
                beforeShowForm: beforeShowEdit
            }, {
                closeAfterAdd: true,
                beforeShowForm: true,
                afterShowForm: afterShowAdd,
                errorTextFormat: errorFormat
            }, {
                errorTextFormat: errorFormat
            }, {
                multipleSearch: true,
                showQuery: true,
                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'bw', 'bn', 'in', 'ni', 'ew', 'en', 'cn', 'nc', 'nu', 'nn']
            });
            jQuery("#gridtest").jqGrid('navButtonAdd', '#pager2', {
                caption: "",
                title: "Custom display field",
                onClickButton: function() {
                    jQuery("#gridtest").jqGrid('columnChooser');
                    noa_subedit=false;
                }
            });
           

            jQuery("#gridtest")
                .navGrid('#pager2', {
                    edit: false,
                    add: false,
                    del: false,
                    search: false,
                    refresh: true
                })
                .navButtonAdd('#pager2', {
                    caption: "",
                    buttonicon: "ui-icon ui-icon-plus",
                    onClickButton: function() {
                        addNewProduct();
                    },
                    position: "first"
                });



            var union_id, unionclosejson = null,
                union_td_input_edit = "",
                unionjqGrid = "",
                subtableinputedit = false,
                noa_subedit=false;
            //增加、删除套装配件时记录当前主商品ID

            $("#union").jqGrid({
                sortable: true,
                url: "/Sync10/administrator/product/subDataProducts.html",
                datatype: "json",
                width: parseInt(document.body.scrollWidth * 0.97),
                height: 150,
                autowidth: false,
                shrinkToFit: true,
                caption: " Suit Relationship",
                jsonReader: {
                    id: 'pid',
                    repeatitems: false,
                },


                colNames: ['set_pid', 'pid', 'catalog_id', 'Accessories Name', 'Combinaton Amount', 'UNIT', 'Main Code', 'Category', 'Weight', 'Price', 'Volume', 'LengthUOM', 'WeightUOM', 'LengthUOMKey', 'WeightUOMKey', 'Gross Profit Rate%'],
                colModel: [{
                        name: 'set_pid',
                        index: 'set_pid',
                        editable: true,
                        editoptions: {
                            readOnly: true
                        },
                        hidden: true,
                        sortable: false
                    }, {
                        name: 'pid',
                        index: 'pid',
                        hidden: true,
                        sortable: false
                    }, {
                        name: 'catalog_id',
                        index: 'catalog_id',
                        hidden: true,
                        sortable: false
                    }, {
                        name: "p_name",
                        index: "p_name",
                        editable: false,
                        align: "left",
                        editrules: {
                            required: true
                        },
                        width: 200
                    }, {
                        name: "quantity",
                        index: "quantity",
                        align: "center",
                        editable: true,
                        formatter: 'number',
                        formatoptions: {
                            decimalPlaces: 0,
                            thousandsSeparator: ","
                        },
                        editrules: {
                            required: true,
                            number: true,
                            integer: true
                        },
                        width: 120
                    }, {
                        name: "unit_name",
                        index: "unit_name",
                        hidden: true,
                        align: "center",
                        editable: false,
                        editrules: {
                            required: true
                        },
                        width: 30
                    }, {
                        name: 'p_code',
                        index: 'p_code',
                        editable: false,
                        align: 'left',
                        editrules: {
                            required: true
                        },
                        width: 110
                    }, {
                        name: 'catalog_text',
                        index: 'catalog_text',
                        editable: false,
                        sortable: false,
                        align: 'left',
                        width: 130
                    }, {
                        name: 'weight',
                        index: 'weight',
                        sortable: false,
                        editable: false,
                        align: 'center',
                        editrules: {
                            required: true,
                            number: true
                        },
                        width: 50
                    }, {
                        name: 'unit_price',
                        index: 'unit_price',
                        editrules: {
                            required: true,
                            number: true
                        },
                        editable: false,
                        sortable: false,
                        align: 'center',
                        width: 50
                    }, {
                        name: 'volume',
                        index: 'volume',
                        editable: false,
                        align: 'center',
                        editrules: {
                            number: true,
                            required: true
                        },
                        editrules: {
                            required: true,
                            number: true
                        },
                        width: 50
                    },

                    {
                        name: 'length_uom_name',
                        index: 'length_uom_name',
                        editable: false,
                        align: 'center',
                        editrules: {
                            required: true
                        },
                        width: 80
                    }, {
                        name: 'weight_uom_name',
                        index: 'weight_uom_name',
                        editable: false,
                        align: 'center',
                        editrules: {
                            required: true
                        },
                        width: 80
                    }, {
                        name: 'length_uom',
                        index: 'length_uom',
                        hidden: true,
                        editable: false,
                        align: 'center',
                        editrules: {
                            required: true
                        },
                        width: 80
                    }, {
                        name: 'weight_uom',
                        index: 'weight_uom',
                        hidden: true,
                        editable: false,
                        align: 'center',
                        editrules: {
                            required: true
                        },
                        width: 80
                    }, {
                        name: 'gross_profit',
                        index: 'gross_profit',
                        editable: true,
                        formatter: 'currency',
                        editable: true,
                        editrules: {
                            required: true,
                            number: true
                        },
                        formatoptions: {
                            decimalPlaces: 1,
                            thousandsSeparator: ",",
                            suffix: "%"
                        },
                        align: 'center',
                        width: 110
                    }
                ],
                rowNum: 100, //-1显示全部
                pgtext: false,
                pgbuttons: false,
                mtype: "GET",
                gridview: true,
                rownumbers: true,
                pager: '#pageunion',
                sortname: 'pc_id',
                viewrecords: true,
                sortorder: "asc",
                cellEdit: true,
                cellsubmit: 'remote',
                editurl: '/Sync10/action/administrator/product/gridEditUnionJqgrid.action',
                loadComplete: function(data) {
                    union_id = data.union_id;
                },
                beforeSelectRow: function(rowid, e) {

                    subtableinputedit = false;

                    union_td_input_edit = "";

                    e.stopPropagation();

                    var $td = $(e.target).closest('td'),
                        $tr = $td.closest('tr'),
                        iRow = $tr[0].rowIndex,
                        iCol = $.jgrid.getCellIndex($td);
                    __iRow = iRow;
                    unionclosejson = null;
                    unionclosejson = {
                        objs: this,
                        icol: iCol,
                        irow: __iRow
                    };

                    var inputs = $td.find("input").eq(0);

                    if (inputs.length < 1) {

                        if (list2this) {

                            ClearEditor(list2this, lastRowIndex, lastColIndex, true);

                        }



                        var uonusobjs = $(this);
                        var celledits = uonusobjs.jqGrid("getGridParam", "cellEdit");

                        if (!celledits) {

                            uonusobjs.jqGrid('setGridParam', {
                                cellEdit: true
                            });


                        }


                    }


                },
                afterEditCell: function(id, name, val, iRow, iCol) {

                    if (name == 'p_name') {
                        autoComplete(jQuery("#" + iRow + "_p_name", "#union"));
                    }

                    union_td_input_edit = lastRowIndex;
                    var tr = $("#gridtest tbody tr").eq(union_td_input_edit);
                    tr.addClass('ui-state-disabled');

                    var tr = this.rows[iRow];
                    var td = $(tr).find("td").eq(iCol);
                    var tdinput = td.find("input:text").eq(0);
                    if (tdinput.length > 0){
                        subtableinputedit = true;
                        noa_subedit=true;
                    }
                    else{
                        subtableinputedit = false;
                    }
                    
                    
                    
                    var oldKeydown,
                    $cellInput = $('input, select, textarea', tr),
                    events = $cellInput.data('events');
                    
                    
	                if (events && events.keydown && events.keydown.length) {
	                    oldKeydown = events.keydown[0].handler;
	
	                    $cellInput.unbind('keydown', oldKeydown);
	
	                    $cellInput.bind('keydown', function(e) {
	
	                        if (e.keyCode == 9) {
	
	                            e.stopPropagation();
	                            // var tdindex = window.parseInt(iCol) + 1;
	                            // var tds = $(tr).find("td").eq(tdindex);
	                            // if (tds.length > 0) {
	                            //     tds.click();
	                            // }
	                            return false;
	
	                        }
	
	
	                    });
	                }


                },
                beforeSubmitCell: function(rowid) { 

                    var set_pid = jQuery("#union").getCell(rowid, 'set_pid');
                    var pid = rowid;

                    jQuery("#union").jqGrid('setGridParam', {
                        cellurl: '/Sync10/action/administrator/product/gridEditUnionJqgrid.action?id=' + rowid + '&set_pid=' + set_pid,
                    });
                },

                afterSaveCell: function(rowid, name, val, iRow, iCol) {

                    if (name == "quantity" || name == "p_name" || name == "weight" || name == "unit_price") {
                        var set_pid = jQuery("#union").jqGrid('getCell', rowid, 'set_pid');
                        //  if(name=="p_name")
                        //  {
                        var p_name = jQuery("#union").jqGrid('getCell', rowid, 'p_name')
                        refProductUnion(jQuery("#union"), rowid, p_name, iRow, iCol);
                        //  }
                        refProduct(jQuery("#gridtest"), set_pid, iRow, iCol);
                    }

                   var ptrset=jQuery("#gridtest tbody tr").eq(parenttabletrid);

                   var p_trdisled=ptrset.hasClass('ui-state-disabled');


                   if(parenttabletrid!=union_td_input_edit && !p_trdisled){

                      setTimeout(function(){
                       addsubtable(ptrset.attr("id"));
                      },300);
                       
                   }
                    union_td_input_edit = "";
                    noa_subedit=false;
                },
                errorCell: function(serverresponse, status) {
                    showMessage(errorMessage(serverresponse.responseText), "alert");
                }

            });

            //                      ,{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
            jQuery("#union").jqGrid('navGrid', "#pageunion", {
                edit: false,
                add: false,
                del: true,
                search: false,
                refresh: true
            }, {
                closeAfterEdit: true,
                afterShowForm: afterShowEdit,
                beforeShowForm: beforeShowEdit
            }, {
                closeAfterAdd: true,
                beforeShowForm: beforeShowAdd,
                afterShowForm: afterShowAdd,
                errorTextFormat: errorFormat,
                afterComplete: afterComplete,
                width: 350
            }, {
                beforeShowForm: beforeShowDel,
                afterComplete: afterComplete
            }, {
                multipleSearch: true,
                showQuery: true,
                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'bw', 'bn', 'in', 'ni', 'ew', 'en', 'cn', 'nc', 'nu', 'nn']
            });

            jQuery("#union").jqGrid('navButtonAdd', '#pageunion', {
                caption: "",
                title: "自定义显示字段",
                onClickButton: function() {
                    jQuery("#union").jqGrid('columnChooser');
                }
            });
            //              jQuery("#union").jqGrid('gridResize',{minWidth:350,maxWidth:parseInt(document.body.scrollWidth*0.95),minHeight:80, maxHeight:350, width:600});

            jQuery("#union")
                .navGrid('#pageunion', {
                    edit: false,
                    add: false,
                    del: true,
                    search: false,
                    refresh: true
                })
                .navButtonAdd('#pageunion', {
                    caption: "",
                    buttonicon: "ui-icon ui-icon-plus",
                    onClickButton: function() {
                        addProductUnion();
                        
                        if (list2this) {

                            ClearEditor(list2this, lastRowIndex, lastColIndex, true);

                        }
                          var tr = $("#gridtest tbody tr").eq(lastRowIndex);
                        tr.addClass('ui-state-disabled');

                       subtableinputedit = true;

                    },
                    position: "first"
                });




            </script>
            <form action="" name="download_form" id="download_form">
            </form>
        </div>
    </div>
</body>
<form action="" name="save_product_union" id="save_product_union" method="post">
    <input type="hidden" name="set_pid" id="set_pid" />
    <input type="hidden" name="p_name" id="p_name" />
    <input type="hidden" name="quantity" id="quantity" />
    <input type="hidden" name="oper" id="oper" value="add" />
</form>

</html>
<script>

var addsubitmeset=false;
function ajaxSaveProductUnion(set_pid, pc_id, pc_count) {
    //  $("#set_pid").val(set_pid);
    //  $("#p_name").val(pc_name);
    //  $("#quantity").val(pc_count);
    //  $("#save_product_union").submit();
    // console.log(set_pid+","+ pc_id+","+ pc_count);
    $.ajax({
        url: '/Sync10/action/administrator/product/gridEditUnionJqgrid.action',
        type: 'post',
        dataType: 'json',
        timeout: 60000,
        cache: false,
        data: {
            oper: 'add',
            pc_id: pc_id,
            quantity: pc_count,
            set_pid: set_pid
        },

        beforeSend: function(request) {},

        error: function(serverresponse, status) {

            showMessage(errorMessage(serverresponse.responseText), "alert");
        },
        success: function() {

            reloadProductUnion();
            refProduct($("#gridtest"), set_pid);
            subtableinputedit=false; 
        }
    });

}

function reloadProductUnion() {
    var id = $("#gridtest").jqGrid('getGridParam', 'selrow'); //根据点击行获得点击行的id（id为jsonReader: {id: "id" },）
    var rowData = $("#gridtest").jqGrid("getRowData", id); //根据上面的id获得本行的所有数据
    jQuery("#union").jqGrid('setGridParam', {
        url: "/Sync10/administrator/product/subDataProducts.html?id=" + id,
        page: 1
    });
    jQuery("#union").trigger('reloadGrid'); //.jqGrid('setCaption',rowData.p_name+" Suit Relationship")
}

function addProductUnion() {
    
	var id = $("#gridtest").jqGrid('getGridParam', 'selrow'); //根据点击行获得点击行的id（id为jsonReader: {id: "id" },）
    var rowData = $("#gridtest").jqGrid("getRowData", id); //根据上面的id获得本行的所有数据
    
    var set_pid = rowData.pc_id;
    
    if (set_pid && set_pid > 0) {
    
	    $.ajax({
	        url: '/Sync10/action/administrator/product/gridEditUnionJqgrid.action',
	        type: 'post',
	        dataType: 'json',
	        timeout: 60000,
	        cache: false,
	        data: {
	        	oper: 'validate',
	        	set_pid: set_pid
	        },
	        beforeSend: function(request) {
	        	$.blockUI({message: '<img src="/Sync10/administrator/js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	        },
	        error: function(serverresponse, status) {
	        	$.unblockUI();
	            showMessage(errorMessage(serverresponse.responseText), "alert");
	        },
	        success: function() {
	        	$.unblockUI();
	        	
	        	var uri = '/Sync10/' + 'administrator/product/choice_product_for_union.html?union_flag=0&equal_or_not=2&set_pid=' + set_pid;
	            $.artDialog.open(uri, {
	                title: 'Choice Product',
	                width: '920px',
	                height: '580px',
	                opacity: 0.1,
	                lock: true,
	                fixed: true
	            });
	        }
	    });
    } else {
    	
    	
        showMessage('Select Product.', 'alert');
    }
}

function formatUom(id) {
    var product_id = jQuery("#gridtest").jqGrid('getCell', id, 'pc_id');

    $("#gridtest").jqGrid('setColProp', 'length_uom_name', {
        editable: true,
        editType: "select",
        editoptions: {
            value: "4:inch;1:mm;2:cm;3:dm"
        }
    });
    $("#gridtest").jqGrid('setColProp', 'weight_uom_name', {
        editable: true,
        editType: "select",
        editoptions: {
            value: "2:LBS;1:Kg"
        }
    });
    $("#gridtest").jqGrid('setColProp', 'price_uom_name', {
        editable: true,
        editType: "select",
        editoptions: {
            value: "2:USD;1:RMB"
        }
    });
}

function getAllLengthUom() {
    return "4:inch;1:mm;2:cm;3:dm";
}

function getAllWeightUom() {
    return "2:LBS;1:Kg";
}

function getAllPriceUom() {
        return "2:USD;1:RMB";
    }
    (function() {
        $.blockUI.defaults = {
            css: {
                padding: '8px',
                margin: 0,
                width: '170px',
                top: '45%',
                left: '40%',
                textAlign: 'center',
                color: '#000',
                border: '3px solid #999999',
                backgroundColor: '#ffffff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                '-webkit-box-shadow': '0 1px 12px rgba(0,0,0,0.5)'
            },
            //设置遮罩层的样式
            overlayCSS: {
                backgroundColor: '#000',
                opacity: '0.6'
            },

            baseZ: 99999,
            centerX: true,
            centerY: true,
            fadeOut: 1000,
            showOverlay: true
        };
    })();

function down() {
    $.blockUI({
        message: '<img src="/Sync10/administrator/js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '
    }); //遮罩打开
    var para = 'pcid=' + pcid + '&cmd=' + cmd + '&key=' + key + '&union_flag=' + union_flag + '&pro_line_id=' + pro_line_id + '&product_file_types=' + product_file_types + '&product_upload_status=' + product_upload_status + '&title_id=' + title_id;
    $.ajax({
        url: '/Sync10/action/administrator/product/AjaxDownProductAndTitleAction.action',
        type: 'post',
        dataType: 'json',
        timeout: 60000,
        data: para,
        cache: false,
        success: function(date) {
            $.unblockUI(); //遮罩关闭
            if (date["canexport"] == "true") {
                document.download_form.action = date["fileurl"];
                document.download_form.submit();
            }
        }
    });
}

function customRefresh() { //无条件时刷新页面
    location.reload();
}

function manageCodes(obj){
	$(obj).parent().parent().find("td[aria-describedby='gridtest_p_codes']").click();
}
</script>
