<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.UploadEditFileKey"%>
<%@ include file="../../include.jsp"%> 
<%
long ipa_id = StringUtil.getLong(request,"ipa_id");
int approve_status = StringUtil.getInt(request,"approve_status",-1);
int upload_type = StringUtil.getInt(request,"upload_type");
String backurl = StringUtil.getString(request,"backurl");

DBRow importProduct[];

if(upload_type==UploadEditFileKey.CommodityData)
{
	importProduct = productMgrZJ.getImportProductByIpaId(ipa_id);
}
else
{
	importProduct = productMgrZJ.getImportUnionByIpaId(ipa_id);
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>


<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}  

function checkForm(theForm)
{
	var reason = theForm.note;
	var sld_ids = theForm.sld_ids;
	
	var haveEmpty = false;
	var oneSelected = false;
	
	//先判断是否数组
	if(typeof sld_ids.length == "undefined")
	{
			if (sld_ids.checked)
			{
				oneSelected = true;
			}
			
			if (reason.value=="")
			{
				reason.style.background = "#EDF36D";
				haveEmpty = true;
			}
	}
	else
	{
		for (i=0; i<sld_ids.length; i++)
		{
			if (sld_ids[i].checked)
			{
				oneSelected = true;
			}
			
			if (sld_ids[i].checked&&reason[i].value=="")
			{
				reason[i].style.background = "#EDF36D";
				haveEmpty = true;
			}
		}
	}
	
	if (!oneSelected)
	{
		alert("最少选择一个记录");
		return(false);
	}
	
	if (haveEmpty)
	{
		alert("有商品差异原因没填写");
		return(false);
	}

	return(true);
}

function approveImport(approve_status)
{
	if(confirm("确认审核吗？"))
	{
		document.approve_import_form.approve_status.value = approve_status;
		document.approve_import_form.submit();
	}
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive();onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 » 审核</td>
  </tr>
</table>
<br>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
<%
	if(upload_type == UploadEditFileKey.CommodityData)
	{
%>
	<tr> 
       	<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">商品名称</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">条码</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">条码2</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">商品分类</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">单位</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">采购价格</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">重量</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">体积重</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">长</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">宽</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">高</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">UPC</th>
    </tr>

<%
for (int i=0; i<importProduct.length; i++)
{
%>

    <tr > 
      <td><%=importProduct[i].getString("p_name")%></td>
      <td><%=importProduct[i].getString("p_code")%></td>
      <td><%=importProduct[i].getString("p_code2","&nbsp;")%></td>
	  <td><%=catalogMgr.getDetailProductCatalogById(importProduct[i].get("catalog_id",0l)).getString("title")%></td>
	  <td><%=importProduct[i].getString("unit_name")%></td>
	  <td><%=importProduct[i].getString("unit_price")%></td>
	  <td><%=importProduct[i].getString("weight")%></td>
	  <td><%=importProduct[i].getString("volume")%></td>
	  <td><%=importProduct[i].getString("length")%></td>
	  <td><%=importProduct[i].getString("width")%></td>
	  <td><%=importProduct[i].getString("heigth")%></td>
	  <td><%=importProduct[i].getString("upc","&nbsp;")%></td>
    </tr>
<%
}
%>
<%
	}
	else if(upload_type == UploadEditFileKey.UnionRelation)
	{
%>
	<tr>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">套装名称</th>
  		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">配件名称</th>
	</tr>
	<%
		for(int i = 0;i<importProduct.length;i++)
		{
			DBRow[] unions = productMgrZJ.getImportProductUnionByIpaIdWithSetpid(ipa_id,importProduct[i].get("set_pid",0l));
	%>
			<tr>
				<td valign="middle">
					<%=importProduct[i].getString("product")%>
				</td>
				<td style="font-size:14">
					<%
						for(int j = 0;j<unions.length;j++)
						{
						
					%>
					<table width="100%" cellpadding="0" cellspacing="0" style="border:0px,white">
						<tr>
							<td width="50%" align="left"><%=unions[j].getString("accessories")%></td>
							<td align="right"><strong>X</strong></td>
							<td align="left"><%=unions[j].get("quantity",0f)%></td>
						</tr>
					</table>
					<%
						}
					%>
				</td>
			</tr>
	<%
		}
	%>
<%
	}
%>    
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td align="center" valign="middle" bgcolor="#eeeeee">
	<%
	if (approve_status==1)
	{
	%>
	  <input name="Submit" type="button" class="long-button-greentext" value="同意" onclick="approveImport(2)"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <input name="Submit" type="button" class="long-button-redtext" value="不同意" onclick="approveImport(3)"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%
	}
	%>
    
      <input name="Submit2" type="button" class="long-button" value="返回" onClick="history.back();">
    </td>
  </tr>
</table>
<form name="approve_import_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/approveImportProduct.action">
	<input type="hidden" name="backurl" value="<%=backurl%>"/>
	<input type="hidden" name="ipa_id" value="<%=ipa_id%>"/>
	<input type="hidden" name="upload_type" value="<%=upload_type%>"/>
	<input type="hidden" name="approve_status"/>
</form>
<div id="keep_alive" style="display:none"></div>
<br>
<br>
</body>
</html>
