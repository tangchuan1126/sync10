<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="yesOrNotKey" class="com.cwc.app.key.YesOrNotKey"></jsp:useBean>
<%
long pc_id = StringUtil.getLong(request,"pc_id");
DBRow[] snRows = productMgr.getProductSnByPcId(pc_id);

%>
<style type="text/css">

.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }

.error{display: none;}  
.hint{display:none;}    
.error_show{color: red;margin-left:10px;padding:4px;padding-left:22px;font-size:11px;border:red 1px solid;
-webkit-border-radius: 4px;-moz-border-radius: 4px;border-radius:4px;
background-attachment: fixed;
	background: url(img/error_validation.png);
	background-repeat: no-repeat;
	background-position: 2% ;
	background-color:#FFF5F5;}
.hint_show{color: #4D4D4D;margin-left:10px;padding:4px;padding-left:22px;font-size:11px;border:#c2c8c8 1px solid;
-webkit-border-radius:3px;-moz-border-radius: 3px;border-radius:3px;
background-attachment: fixed;
	background: url(img/hint.png);
	background-repeat: no-repeat;
	background-position: 2% ;
	background-color:#f9f9f9;
	}
#del_lsn_input, .del_lsn_input{background: url(img/del.png) center; width:16px; height:14px;border-style:none;cursor:pointer;}
#add_lsn_input, .add_lsn_input{background: url(img/add.png) center; width:16px; height:14px;border-style:none;cursor:pointer;}
</style>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script src="/Sync10-ui/lib/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../buttons.css"  /> 

<form name="mod_product_sn" method="post" action="" id="mod_product_sn">
<input type="hidden" value="<%=pc_id%>" name="pc_id"/>
<table width="100%" border="0" cellspacing="5" cellpadding="2" style="border-collapse: collapse;">

<%if(snRows.length == 0) {%>
	 <tr>
	    <td align="right" valign="middle" class="STYLE3" style="width:120px; text-align: right;"><label for="serial_product">Serialized Product : </label></td>
	    <td align="left" valign="middle" style="width:150px;">
	    	<%ArrayList yesOrNotKeys = yesOrNotKey.getYesOrNotKeys(); %>
			<div class="side-by-side clearfix" >
	 	     <select id="product_key" name="prodcut_key" class="chzn-select" data-placeholder="Choose a key..." tabindex="1"  style="width:100px;height:30px;" onchange="product_key_change();">
	 	     	<%for(int a=0;a<yesOrNotKeys.size();a++){ %>
			    <option value="<%=Integer.parseInt(String.valueOf(yesOrNotKeys.get(a))) %>" <%if(Integer.parseInt(String.valueOf(yesOrNotKeys.get(a))) == 2) {%>selected <%} %>><%=yesOrNotKey.getYesOrNotKeyName(Integer.parseInt(String.valueOf(yesOrNotKeys.get(a))))%></option>
			    <%}%>
			 </select>
			 </div>
	    </td>
	    <td>
	    	<span id="unit_price_error" class="error"></span> 
	    	<span id="unit_price_hint" class="hint"></span>
	    </td>
  	</tr>
	<tr class='lsn_tr' style="display:none;">
		<td align="right" valign="middle" class="STYLE3" width="120px"><label for="lsn">SN Length : </label></td>
	    <td align="left" valign="middle">
		   	<input class="data_error" name="lsn" type="text" id="lsn"  style="width:100px;height:30px;"/>
		   	<input type="button" class="del_lsn_input" style="display:none;">
		   	<input type="button" class="add_lsn_input" >
	    </td>
	    <td>
		   	<span id="lsn_error" class="error"></span> 
		   	<span id="lsn_hint" class="hint">e.g (4, 8, 12)</span>
	    </td>
    </tr>

<%}else{ %>
	<tr>
	    <td align="right" valign="middle" class="STYLE3" style="width:120px;  text-align: right;  padding-right: 10px;"><label for="serial_product">Serialized Product : </label></td>
	    <td align="left" valign="middle" width="150px">
	    	<%ArrayList yesOrNotKeys = yesOrNotKey.getYesOrNotKeys(); %>
			<div class="side-by-side clearfix">
	 	     <select id="product_key" name="prodcut_key" class="chzn-select" data-placeholder="Choose a key..." tabindex="1"  style="width:100px;height:30px;" onchange="product_key_change();">
	 	     	<%for(int a=0;a<yesOrNotKeys.size();a++){ %>
			    <option value="<%=Integer.parseInt(String.valueOf(yesOrNotKeys.get(a))) %>" <%if(Integer.parseInt(String.valueOf(yesOrNotKeys.get(a))) == 1) {%>selected <%} %>><%=yesOrNotKey.getYesOrNotKeyName(Integer.parseInt(String.valueOf(yesOrNotKeys.get(a))))%></option>
			    <%}%>
			 </select>
			 </div>
	    </td>
	    <td>
	    	<span id="unit_price_error" class="error"></span> 
	    	<span id="unit_price_hint" class="hint"></span>
	    </td>
  	</tr>
	<%
	for(int i=0;i<snRows.length;i++)
	{
	%>
	<tr class='lsn_tr'>
		<td align="left" valign="middle" class="STYLE3" style="width:120px;  text-align: right;  padding-right: 10px;"><label for="lsn"><%if(i == 0) {%>SN length : <%} %></label></td>
	    <td align="left" valign="middle">
		   	<input class="data_error" name="lsn" type="text" id="lsn"  style="width:100px;height:30px;" value="<%=snRows[i].get("sn_size",0) %>"/>
		   	<input type="button" class="del_lsn_input" <%if(i == 0) {%>style="display:none;" <% }%>>
		   	<input type="button" class="add_lsn_input" <%if(i != snRows.length -1) {%>style="display:none;" <% }%>>
	    </td>
	    <td>
		   	<span id="lsn_error" class="error"></span> 
		   	<span id="lsn_hint" class="hint">e.g (4, 8, 12)</span>
	    </td>
    </tr>
    
	<%
	}
	%>

<%} %>

	
 </table>
</form>

<script>

$(function(){
	
	addeventetr();
	$(".chzn-select").chosen({no_results_text: "no this options:"});
})

function product_key_change(){
	var pk = $("#product_key").val();
	if(pk == 2){
		$(".lsn_tr").css("display","none");
	}else{
		$(".lsn_tr").css("display","");
	}
}

var numeric_error_message = "Please enter numeric",
	valid = false;;

function isNumeric( obj ) {
    return !$.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
}

function delLsnInput(obj){
	var $self = $(obj),
		$parent = $self.parent().parent();
	
	if($self.next('.add_lsn_input').css('display') === "none"){
		$self.parent().parent().remove();
	}else{
		$(".add_lsn_input").hide();
		
		$parent.prev().find(".add_lsn_input").show();
		$parent.remove();
	}
	
}

function addeventetr(){
	
	var talbetr=$("#mod_product_sn");
	
	talbetr.on('blur', '.data_error', function(){
		var $this = $(this);
		var $h=$this.parents(".lsn_tr").eq(0);
		$h.find('.hint_show').removeClass("hint_show").addClass("hint");
		
		var val_=$this.val();
		
		if(!val_ || val_=="" || typeof val_=="undefined"){
			$h.find('.error').html("Please enter SN length");
			$h.find('.error').removeClass("error").addClass("error_show");
			valid = false; //false
		}else if(!isNumeric(val_)){
			$h.find('.error').html(numeric_error_message);
			$h.find('.error').removeClass("error").addClass("error_show");
			valid = false; //false
		}else{
			$h.find('.error_show').removeClass("error_show").addClass("error");
			valid = valid && true; // true
		}
		valid = valid && checkSN(this);
		
	}).on('focus', '.data_error', function(){
		var $this = $(this);
		var $h=$this.parents(".lsn_tr").eq(0);
		$h.find('.error_show').removeClass("error_show").addClass("error");
		$h.find('.hint').removeClass("hint").addClass("hint_show");
	}).on('click', '.add_lsn_input', function(){
		addLsnInput(this);
	}).on('click', '.del_lsn_input', function(){
		delLsnInput(this);
	});
	
	
}

function addLsnInput(obj){
	var html = "<tr class='lsn_tr'>";
		html +="<td class='STYLE3' width='120px'></td>";
		html +="<td align='left' valign='middle'>";
		html +="<input class='data_error' name='lsn' type='text' style='width:100px;height:30px;' /> ";
		html +="<input type='button' class='del_lsn_input'> ";
		html +="<input type='button' class='add_lsn_input'>";
		html +="</td>";
		html +="<td>";
		html +="<span class='error'></span> ";
		html +="<span class='hint'>e.g (4, 8, 12)</span>";
		html +="</td>";
		html +="</tr>";
		
	//var $h = $(html);
	
	var $this = $(obj);
	var $h=$this.parents(".lsn_tr").eq(0);
	
	$h.after(html);
	//if($this.index()==0)
	$this.hide();
	
}

function checkSN(obj){
	var data = $(".data_error");
	var isValid = true;
	
	
	data.each(function(i,elem){
		var currVal = $(obj).val();
		var _thisVal = $(this).val();
		var _isExist = $(obj).parent().next();
		var _alIsExist = $(this).parent().next();
		if($("#product_key").val() == 1){
			if(obj != elem){
				if( currVal && _thisVal && currVal === _thisVal){
					_alIsExist.find('.error_show').removeClass("error_show").addClass("error");
					_isExist.find('.error').html("SN length already exists");
					_isExist.find('.error').removeClass("error").addClass("error_show");
					isValid = false;
				}else{
					_alIsExist.find('.error_show').removeClass("error_show").addClass("error");
				}
			}
		}else{
			_alIsExist.find('.error_show').removeClass("error_show").addClass("error");
			_isExist.find('.error').removeClass("error_show").addClass("error");	
			isValid = true;
		}
	});
	
	return isValid;
}


function save_sn(){
	valid = true;
	
	$('.data_error').each(function(){
		$(this).trigger('blur');
	})
	
	var f = document.mod_product_sn;
	var lsnArr = "";
	if($("#product_key").val() == 1){
		if(f.lsn.length == undefined){
			lsnArr = f.lsn.value;
		}else{
			for(var i = 0 ; i< f.lsn.length ; i ++){
				lsnArr += f.lsn[i].value + ",";
			}
			lsnArr = lsnArr.substring(0 , lsnArr.length -1);
		}
	}else{
		valid = true;
		var data = $(".data_error");
		data.each(function(){
			var $this = $(this);
			$this.val("");
		});
	}

	var sta = false;
	if(valid && checkIsExist()){
		$.ajax({
				url:  '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/SnLengthAction.action',
				type: 'post',
				timeout: 60000,
				cache:false,
				dataType: 'json',
				data:{pc_id:f.pc_id.value,lsn:lsnArr},
				async:false,
				
				error: function(jqXHR, textStatus, errorThrown) {
					showMessage("Error while edited SN length","error");
					console.log(errorThrown);
					
				},
				
				success: function(data)
				{
					if(data.STATUS == 'success'){
						
						//var mainwindow=window.top.window.frames["iframe"].frames["main"];
						var mainwindow=window.parent.window;
							
						if(window.top.window.frames["iframe"])                        
						 	mainwindow=window.top.window.frames["iframe"].frames["main"];
						
						showMessage("SN length edited successfully","succeed");
						//setTimeout(function(){window.top.customRefresh();}, 1000);
						mainwindow.modCell(f.pc_id.value,"sn_size",data.SN);
						
						sta = true;
					}
					else
					{
						showMessage("SN length edited error","error");
					}
					
				}
			});
	
	}
	return sta;
	
}

function checkIsExist(){
	var retuExi = true;
	var array = new Array();
	var data = $(".data_error");
	
	var dup = true;

	data.sort(function(a,b){
		if($("#product_key").val() == 1){
			 if(a.value == b.value){
				$(a).parent().next().find('.error').html("SN length already exists");
				$(a).parent().next().find('.error').removeClass("error").addClass("error_show");
				$(b).parent().next().find('.error').html("SN length already exists");
				$(b).parent().next().find('.error').removeClass("error").addClass("error_show");
				dup = false;
			 }
		}else{
			$(a).parent().next().find('.error').removeClass("error_show").addClass("error");
			$(b).parent().next().find('.error').removeClass("error_show").addClass("error");
			dup = true;
		}
	});
	
	return dup;
}

</script>
