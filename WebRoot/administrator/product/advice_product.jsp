<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>建议商品</title>
  </head>
<%
	long ps_id = StringUtil.getLong(request,"ps_id");
	long waybill_id = StringUtil.getLong(request,"waybill_id");
	
	String[] pcids = StringUtil.getString(request,"pcids").split(",");
	
	long[] pc_ids =  new long[pcids.length];
	
	for(int i=0;i<pc_ids.length;i++)
	{
		if(!pcids[i].trim().equals(""))
		{
			pc_ids[i] = Long.parseLong(pcids[i]);
		}
		
	}
	
	DBRow[] adviceProducts = productMgrZJ.splitAdviceProduct(pc_ids,ps_id,waybill_id);
	
	DBRow[] adviceBackUpProducts = productMgrZJ.splitAdviceBackUpProduct(pc_ids,ps_id,waybill_id);
%>
  <body>
    <table height="100%" width="100%">
    	<tr>
    		<td width="100%" style="background-color:#cccccc;font-style:italic;font-weight:bolder;">
    			不完整套装
    		</td>
    	</tr>
    	<tr>
    		<td width="100%" valign="top" style="font-size:14px;">
    			<table width="100%" cellspacing="0" cellpadding="2">
    			<%
    				for(int i = 0;i<adviceBackUpProducts.length;i++)
    				{
    			%>
    				<tr>
    					<td valign="middle" align="left" style="border-bottom:1px;border-bottom-style:solid;">
    						<%=adviceBackUpProducts[i].getString("p_name")%>
    						<table style="padding-left:15px;">
    							<%
    								DBRow[] needProductUnions = productMgrZJ.adviceNeedProductUnion(pc_ids,adviceProducts[i].get("pc_id",0l));
    								for(int j = 0;j<needProductUnions.length;j++)
    								{
    							%>
    							<tr style="">
    								<td align="left"><font style="font-style:italic;font-size:10px;font-weight:bold;"><%=needProductUnions[j].getString("p_name")%></font></td>
    								<td><font style="font-weight:bolder;font-style:italic;font-size:10px;">X</font></td>
    								<td align="right" style="color:red"><font style="font-style:italic;font-size:10px;"><%=needProductUnions[j].get("quantity",0f)%></font></td>
    							</tr>
    							<%
    								}
    							%>
    						</table>
    					</td>
    					<td align="right" style="border-bottom:1px;border-bottom-style:solid;">
    						现有库存:<%=adviceBackUpProducts[i].get("back_up_store_count",0f)%><br/>
    						<%
    							if(waybill_id !=0)
    							{
    						%>
    						<tst:authentication bindAction="com.cwc.app.api.ProductMgr.splitProduct">
    							<input name="Submit52" type="button" class="short-short-button-convert"  onClick="openSplitOrder('back_up_store',<%=adviceBackUpProducts[i].get("pc_id",0l)%>,<%=adviceBackUpProducts[i].get("pc_id",0l)%>,<%=ps_id%>)" value="拆散">
    						</tst:authentication>
    						<%
    							}
    						%>
    						
    					</td>
    				</tr>
    			<%
    				}
    			%>
    			</table>
    		</td>
    	</tr>
    	<tr>
    		<td width="100%" style="background-color:#cccccc;font-style:italic;font-weight:bolder;">
    			完整套装
    		</td>
    	</tr>
    	<tr>
    		<td width="100%" valign="bottom" style="font-size:14px;">
    			<table width="100%" cellspacing="0" cellpadding="2">
    			<%
    				for(int i = 0;i<adviceProducts.length;i++)
    				{
    			%>
    				<tr>
    					<td valign="middle" align="left" style="border-bottom:1px;border-bottom-style:solid;">
    						<%=adviceProducts[i].getString("p_name")%>
    						<table style="padding-left:15px;">
    							<%
    								DBRow[] needProductUnions = productMgrZJ.adviceNeedProductUnion(pc_ids,adviceProducts[i].get("pc_id",0l));
    								for(int j = 0;j<needProductUnions.length;j++)
    								{
    							%>
    							<tr style="">
    								<td align="left"><font style="font-style:italic;font-size:10px;font-weight:bold;"><%=needProductUnions[j].getString("p_name")%></font></td>
    								<td><font style="font-weight:bolder;font-style:italic;font-size:10px;">X</font></td>
    								<td align="right" style="color:red"><font style="font-style:italic;font-size:10px;"><%=needProductUnions[j].get("quantity",0f)%></font></td>
    							</tr>
    							<%
    								}
    							%>
    						</table>
    					</td>
    					<td align="right" style="border-bottom:1px;border-bottom-style:solid;">
    						现有库存:<%=adviceProducts[i].get("store_count",0f)%><br/>
    						<%
    							if(waybill_id !=0)
    							{
    						%>
    							<tst:authentication bindAction="com.cwc.app.api.ProductMgr.splitProduct">
	    							<input name="Submit52" type="button" class="short-short-button-convert"  onClick="openSplitOrder('store',<%=adviceProducts[i].get("pc_id",0l)%>,<%=adviceProducts[i].get("pc_id",0l)%>,<%=ps_id%>)" value="拆散">
	    						</tst:authentication>
    						<%
    							}
    						%>
    						
    					</td>
    				</tr>
    			<%
    				}
    			%>
    			</table>
    		</td>
    	</tr>
    </table>
  </body>
</html>
