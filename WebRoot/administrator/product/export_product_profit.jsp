<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long pcid = StringUtil.getLong(request,"pcid");
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
int union_flag = StringUtil.getInt(request,"union_flag");
String pro_line_id = StringUtil.getString(request,"pro_line_id");

//System.out.println(pcid+" - "+cmd+" - "+key+" - "+union_flag);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script language="javascript">
<!--
function checkForm()
{
	var cansubmit = true;

	if ($("#ca_id").val()==0)
	{
		alert("请选择销售区域");
		cansubmit = false;
	}
	else if ($("#ps_id").val()==0)
	{
		alert("请选择主流发货仓库");
		cansubmit = false;
	}


	if (cansubmit)
	{
		$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">正在导出，请稍后......</div>'});
		//parent.document.export_form.pcid.value = ;
		//parent.document.export_form..value = "";
		//parent.document.export_form..value = "";
		//parent.document.export_form..value = ;

		//parent.document.export_form..value = ;
		//parent.document.export_form..value = ;
		
		//parent.document.export_form.submit();
		
			var para = "pro_line_id=<%=pro_line_id%>&pcid=<%=pcid%>&cmd=<%=cmd%>&key=<%=key%>&union_flag=<%=union_flag%>&ps_id="+$("#ps_id").val()+"&ca_id="+$("#ca_id").val();
		
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/ExportProfitQuote.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					//alert("提交失败，请重试！");
				},
				
				success: function(msg)
				{
					if (msg=="WareHouseErrorException")
					{
						alert("发货仓库不正确，不能导出数据");
						$.unblockUI();
					}
					else
					{
						parent.window.location = "../../"+msg;
						parent.closeWin();
					}
				}
			});

	}
}


//-->
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  >

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><br />

      <table width="600" border="0" align="center" cellpadding="4" cellspacing="0">
      <tr>
        <td>销售区域</td>
        <td width="466" >
		<select name="ca_id" id="ca_id" >
		<option value="0">请选择...</option>
<%
DBRow countryAreas[] = productMgr.getAllCountryAreas(null);

for (int i=0; i<countryAreas.length; i++)
{
%>
<option value="<%=countryAreas[i].get("ca_id",0l)%>"><%=countryAreas[i].getString("name")%></option>
<%
}
%>
    </select>
		</td>
      </tr>
      <tr>
        <td width="118">主流发货仓库</td>
        <td >
		<select name="ps_id" id="ps_id">
		<option value="0">请选择...</option>
          <%
DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();
String qx;

for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
          <option value="<%=treeRows[i].getString("id")%>"> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>          </option>
          <%
}
%>
        </select>		</td>
      </tr>
</table>

	
	
    </td></tr>
  <tr>
    <td align="right" class="win-bottom-line">
      <input type="button" name="Submit2" value="导出" class="normal-green-long" onClick="checkForm();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
    </td>
  </tr>
</table> 
</body>
</html>
