<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ProductStoreLogsDetailOperationTypeKey"%>
<%@ include file="../../include.jsp"%>
<%
	long psld_id = StringUtil.getLong(request,"psld_id");//自身批次ID
	int next_back = StringUtil.getInt(request,"next_back");
	
	DBRow productStoreLogDetail = productStoreLogsDetailMgrZJ.getDetailProductStoreDetailLogsByPsldid(psld_id);
	
	long from_psld_id = 0l;
	
	DBRow[] fromProductStoreLogDetails = new DBRow[0];//来源部分
	DBRow[] toProductStoreLogDetails = new DBRow[0];//目标部分
	if(next_back==1)//使用psld_id查询上级批次（）在右侧
	{
		from_psld_id = productStoreLogDetail.get("from_psld_id",0l);
		
		if(from_psld_id==0)
		{
			fromProductStoreLogDetails = productStoreLogsDetailMgrZJ.getProductStorelogDetailsByToPsld(psld_id);
		}
		else
		{
			fromProductStoreLogDetails = new DBRow[1];
			fromProductStoreLogDetails[0] = productStoreLogsDetailMgrZJ.getDetailProductStoreDetailLogsByPsldid(from_psld_id);
		}
		
		toProductStoreLogDetails = new DBRow[1];
		toProductStoreLogDetails[0] = productStoreLogDetail;
	}
	else //if(next_back==2)//psld_id是来源批次，查询psld_id的下一级批次，在左侧
	{
		fromProductStoreLogDetails = new DBRow[1];
		fromProductStoreLogDetails[0] = productStoreLogDetail;
		
		from_psld_id = psld_id;
		
		if(from_psld_id!=0)
		{
			toProductStoreLogDetails = productStoreLogsDetailMgrZJ.getProductStoreLogDetailsByFromPsldId(from_psld_id);
				
			if(toProductStoreLogDetails.length==0)
			{
				toProductStoreLogDetails = productStoreLogsDetailMgrZJ.getProductStorelogDetailsFromUnionLogByFromPsld(from_psld_id);
			}
		}
	}
	
	ProductStoreLogsDetailOperationTypeKey productStoreLogsDetailOperationTypeKey = new ProductStoreLogsDetailOperationTypeKey();
	
	String fontColor;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>无标题文档</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<script type="text/javascript">
	function nextOrBack(psld_id,next_back)
	{
		document.show_form.psld_id.value = psld_id;
		document.show_form.next_back.value = next_back;
		document.show_form.submit();
	}
</script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<form action="" name="show_form">
	<input type="hidden" name="psld_id"/>
	<input type="hidden" name="next_back"/>
</form>
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td width="50%" valign="top">
				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true" isBottom="true">
				    <tr>
				    	<th nowrap="nowrap" class="right-title" style="vertical-align: center;text-align: center;">仓库</th>
				    	<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">商品名</th>
				    	<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">采购价</th>
				    	<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">运费</th>
				        <th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">已用数量/数量</th>
				        <th nowrap="nowrap" class="right-title" style="vertical-align: center;text-align: center;">创建时间</th>
				  	</tr>
				  	<%
				  		for(int i = 0;i<fromProductStoreLogDetails.length;i++)
				  		{
				  			fontColor = "green";
				  			
				  			if(fromProductStoreLogDetails[i].get("operation_type",0)==ProductStoreLogsDetailOperationTypeKey.OutStore)
				  			{
				  				fontColor = "red";
				  			}
				  			
				  	%>
				  		<tr ondblclick="nextOrBack(<%=fromProductStoreLogDetails[i].get("psld_id",0l)%>,1)">
				  			<td align="center"><%=fromProductStoreLogDetails[i].getString("title")%></td>
				  			<td align="left"><%=fromProductStoreLogDetails[i].getString("p_name")%></td>
				  			<td align="center"><%=fromProductStoreLogDetails[i].get("purchase_unit_price",0d)%></td>
				  			<td align="center"><%=fromProductStoreLogDetails[i].get("shipping_fee",0d)%></td>
				  			<td align="center"><font color="<%=fontColor%>"><%=fromProductStoreLogDetails[i].get("used_quantity",0f)%>/<%=fromProductStoreLogDetails[i].get("quantity",0f)%></font></td>
				  			<td align="center"><%=fromProductStoreLogDetails[i].getString("create_time").subSequence(2,19)%></td>
				  		</tr>
				  	<%
				  		}
				  	%>
				</table>
			</td>
			
			<td width="50%" valign="top">
				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true" isBottom="true">
				    <tr>
				    	<th nowrap="nowrap" class="right-title" style="vertical-align: center;text-align: center;">仓库</th>
				    	<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">商品名</th>
				    	<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">采购价</th>
				    	<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">运费</th>
				        <th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">已用数量/数量</th>
				        <th nowrap="nowrap" class="right-title" style="vertical-align: center;text-align: center;">创建时间</th>
				  	</tr>
				  	<%
				  		
				  		for(int i = 0;i<toProductStoreLogDetails.length;i++)
				  		{
				  			fontColor = "green";
				  			
				  			if(toProductStoreLogDetails[i].get("operation_type",0)==ProductStoreLogsDetailOperationTypeKey.OutStore)
				  			{
				  				fontColor = "red";
				  			}
				  	%>
				  		<tr ondblclick="nextOrBack(<%=toProductStoreLogDetails[i].get("psld_id",0l)%>,2)">
				  			<td align="center"><%=toProductStoreLogDetails[i].getString("title")%></td>
				  			<td align="left"><%=toProductStoreLogDetails[i].getString("p_name")%></td>
				  			<td align="center"><%=toProductStoreLogDetails[i].get("purchase_unit_price",0d)%></td>
				  			<td align="center"><%=toProductStoreLogDetails[i].get("shipping_fee",0d)%></td>
				  			<td align="center"><font color="<%=fontColor%>"><%=toProductStoreLogDetails[i].get("used_quantity",0f)%>/<%=toProductStoreLogDetails[i].get("quantity",0f)%></font></td>
				  			<td align="center"><%=toProductStoreLogDetails[i].getString("create_time").subSequence(2,19)%></td>
				  		</tr>
				  	<%
				  		}
				  	%>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
