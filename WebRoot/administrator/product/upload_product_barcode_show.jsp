<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.LableTemplateKey"%>
<%@ include file="../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<%



String[] file = productMgrZJ.importProductBarcode(request).split(",");

DBRow rows[] = productMgrZJ.productBarcode(file[0]);

long lable_template_id = Long.parseLong(file[1]);
DBRow lable_template = lableTemplateMgrZJ.getDetailLableTemplateById(lable_template_id);

DBRow[] options = lableTemplateMgrZJ.getAllLableTemplate(null);

boolean productnull = true;
StringBuffer pcids = new StringBuffer("");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<script language="javascript">

function searchByName()
{
	document.search_form.cmd.value = "name";
	document.search_form.name.value = $("#filter_name").val();	
	document.search_form.pcid.value = $("#filter_pcid").val();	
	document.search_form.submit();
}

function importPrint()
{
	tb_show('上传要打印条码的商品','printbarcode_upload_excel.html?TB_iframe=true&height=500&width=1000',false);
}

function exportProductBarcode()
{
	var catalog_id= $("#filter_pcid").val();
	var para = "catalog_id="+catalog_id;
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/exportProductBarcode.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){
					if(date["canexport"]=="true")
					{
						document.export_product_barcode.action=date["fileurl"];
						document.export_product_barcode.submit();
					}
					else
					{
						alert("该分类与子类下无所选形式商品，无法导出");
					}
				}
			});
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 


function closeWin()
{
	tb_remove();
}

function noNumbers(e)
	{
		var keynum
		var keychar
		var numcheck
		if(window.event) // IE
		{
		  keynum = e.keyCode
		}
		else if(e.which) // Netscape/Firefox/Opera
		{
		  keynum = e.which
		}
		keychar = String.fromCharCode(keynum)
		numcheck = /[^\d]/
		return !numcheck.test(keychar)
	}
	
		function ajaxLoadPurchasePage(url,pc_id)
		{
			var counts = $("#productcount_"+pc_id).val();
			var para = "print_range_width=<%=lable_template.getString("print_range_width")%>&print_range_height=<%=lable_template.getString("print_range_height")%>&printer=<%=lable_template.getString("printer")%>&paper=<%=lable_template.getString("paper")%>&pc_id="+pc_id+"&counts="+counts;
			$.ajax({
				url:url,
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request)
				{
					
				},
				
				error: function()
				{
					
				},
				
				success: function(html)
				{
					$("#"+pc_id).html(html);
				}
			});
		}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div align="center">
<br>
<form name="export_product_barcode" method="post"></form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »商品条码</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <form name="listForm" method="post">
    <%
	for ( int i=0; i<rows.length; i++ )
	{
		if(rows[i].getString("error").equals("error"))
		{
			productnull = false;
		}
		pcids.append(rows[i].getString("pc_id"));
		if(i<rows.length-1)
		{
			pcids.append(",");
		}
	%>
	 <tr> 
        <th width="150" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
        <th width="157" align="left" class="right-title"  style="vertical-align: center;text-align: left;">商品条码</th>
        <%
      	if(lable_template.get("lable_type",0)==LableTemplateKey.OUTSIZELABEL)
      	{
      	%>
        <th width="151" class="right-title"  style="vertical-align: center;text-align: center;">商品数量</th>
        <%
        }
        %>
        <th width="84" class="right-title"  style="vertical-align: center;text-align: center;">打印数量</th>
    </tr>
    <tr> 
      <td height="60" valign="middle"   style='font-size:14px;' <%=rows[i].getString("error").equals("error")?"bgcolor='red'":"" %>>
        
      <%=rows[i].getString("p_name")%></td>
      <td height="60" align="left" valign="middle" style='font-size:14px;line-height:20px;' <%=rows[i].getString("error").equals("error")?"bgcolor='red'":"" %>>
	  	<%=rows[i].getString("p_code") %>
	  </td>
	  <%
      	if(lable_template.get("lable_type",0)==LableTemplateKey.OUTSIZELABEL)
      	{
      %>
      <td align="center" valign="middle"   style='word-break:break-all;' <%=rows[i].getString("error").equals("error")?"bgcolor='red'":"" %>>
	  	<input id="productcount_<%=rows[i].getString("pc_id")%>" type="text" value="<%=rows[i].getString("count") %>" style="width:50px;color:#FF0000" onchange="chageLabel(this.value,<%=rows[i].getString("pc_id")%>)"> 
	  </td>
	  <%
	  	}
	  %>
      <td align="center" valign="middle"   style='word-break:break-all;color:#0000FF;font-weight:bold' <%=rows[i].getString("error").equals("error")?"bgcolor='red'":"" %>>
		  <input id="copies_<%=rows[i].getString("pc_id")%>" type="text" value="<%=rows[i].getString("printcount")%>" style="width:50px;color:#FF0000"></td>
      </tr>
      <tr>
	 	<td colspan="4" id="<%=rows[i].get("pc_id",0l)%>" align="center" valign="bottom" style="padding-bottom: 20px;">
	 		<script type="text/javascript">
	 			ajaxLoadPurchasePage("<%=ConfigBean.getStringValue("systenFolder")+"administrator/"+lable_template.getString("template_path")%>",<%=rows[i].get("pc_id",0l)%>);
	 		</script>
	 	</td>
	 </tr>
    <%
	}
	%>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
  	<td align="center">
		<input type="button" onClick="printBarcodes()" class="long-button-print" value="打印"/>
	</td>
  </tr>
</table> 
<br>
</div>
<script type="text/javascript">
	function chageLabel(val,pc_id)
		{
			if(val.length>0&&parseFloat(val)==val)
			{
				ajaxLoadPurchasePage("<%=ConfigBean.getStringValue("systenFolder")+"administrator/"+lable_template.getString("template_path")%>",pc_id);
			}
			else
			{
				alert("请告诉我们箱子里到底有多少该产品");
			}
		}
		
	function printBarcodes()
	{
		var pcids = "<%=pcids.toString()%>";
		var pcid = pcids.split(",");
		var print = true;
		var copies = true;
		var errorid;
		<%
      	if(lable_template.get("lable_type",0)==LableTemplateKey.OUTSIZELABEL)
      	{
      	%>
		for(c=0;c<pcid.length;c++)
		{
			if(parseInt($("#productcount_"+pcid[c]).val())!=$("#productcount_"+pcid[c]).val())
			{
				print = false;
				errorid = pcid[c];
				break;
			}
		}
		<%
		}
		%>
		
		for(b=0;b<pcid.length;b++)
		{
			if(parseInt($("#copies_"+pcid[b]).val())!=$("#copies_"+pcid[b]).val())
			{
				copies = false;
				errorid = pcid[b];
				break;
			}
		}
		
		if(print&&copies)
		{
			for(q=0;q<pcid.length;q++)
			{
				if(document.getElementById(pcid[q])!=null)
				{
					visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%","<body leftmargin='0' topmargin='0'>"+document.getElementById(pcid[q]).innerHTML+"</body>");
					visionariPrinter.SET_PRINT_COPIES();
					//visionariPrinter.PREVIEW();
					visionariPrinter.PRINT();
				}	
			}
		}
		else
		{
			if(print)
			{
				alert("打印数量异常，请修改");
				$("#copies_"+errorid).focus();
			}
			if(copies)
			{
				alert("商品数量异常，请修改");
				$("#productcount_"+errorid).focus();
			}
		}
	}
</script>
</body>
</html>
