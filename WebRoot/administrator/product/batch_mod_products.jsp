<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String pids = StringUtil.getString(request,"pids");
String pidsA[] = pids.split(",");

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>

<script>

function bm()
{
	document.batchModForm.submit();
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
  <form name="batchModForm" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/BatchModProduct.action">
  <input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>">
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">

    <tr> 
        <th style="vertical-align: center;text-align: center;" class="left-title">名称</th>
        <th style="vertical-align: center;text-align: center;" class="right-title">条码</th>
        <th width="12%" style="vertical-align: center;text-align: center;" class="right-title">单位</th>
        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">重量</th>
        <th width="9%" style="vertical-align: center;text-align: center;" class="right-title">单价</th>
        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">毛利率</th>
    </tr>
<%
for (int i=0; i<pidsA.length; i++)
{
	DBRow detailPro = productMgr.getDetailProductByPcid( StringUtil.getLong(pidsA[i]) );
%>
    <tr > 
      <td   width="28%" height="60" align="center" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><label>
	   <input name="batch_pc_id" type="hidden"  value="<%=detailPro.getString("pc_id")%>">
        <input name="p_name_<%=detailPro.getString("pc_id")%>" type="text" style="width:200px;" value="<%=detailPro.getString("p_name")%>">
      </label></td>
      <td   width="31%" align="center" valign="middle" style='word-break:break-all;' ><input type="text" name="p_code_<%=detailPro.getString("pc_id")%>"  style="width:200px;" value="<%=detailPro.getString("p_code")%>"></td>
      <td   align="center" valign="middle" ><input type="text" name="unit_name_<%=detailPro.getString("pc_id")%>" style="width:30px;" value="<%=detailPro.getString("unit_name")%>"></td>
      <td   align="center" valign="middle" style='word-break:break-all;'><input type="text" name="weight_<%=detailPro.getString("pc_id")%>" style="width:50px;" value="<%=detailPro.getString("weight")%>"></td>
      <td   align="center" valign="middle" style='word-break:break-all;'><input type="text" name="unit_price_<%=detailPro.getString("pc_id")%>" style="width:50px;" value="<%=detailPro.getString("unit_price")%>"></td>
      <td   align="center" valign="middle" >
      <input type="text" name="gross_profit_<%=detailPro.getString("pc_id")%>" style="width:30px;" value="<%=detailPro.getString("gross_profit")%>">
      </td>
    </tr>
<%
}
%>
</table>
</form>
<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="42%" align="right" valign="middle" style="height:40px;"><input type='button'  class="normal-green-long" name='Submit22' value='保存修改'  onClick="bm()"/>
      <label>
      <input name="Submit" type="button" class="normal-white" value="取消" onClick="closeWin();">
    </label></td>
  </tr>
</table>
</body>
</html>
