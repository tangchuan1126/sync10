<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.key.RetailPriceErrorKey"%>
<%@page import="com.cwc.app.beans.retailprice.RetailPriceQuoteBean"%>
<jsp:useBean id="retailPriceErrorKey" class="com.cwc.app.key.RetailPriceErrorKey"/>
<%@ include file="../../include.jsp"%> 
<%
String file = null;

try
{
	file = quoteMgr.importProductProfit(request);
}
catch(FileTypeException e)
{
	out.print("<script>alert('只可上传.xls文件');window.history.go(-1)</script>");
	return;
}

//检测数据格式
DBRow errorRows[] = quoteMgr.checkExcel(file);
DBRow changeProducts[] = new DBRow[0];
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script language="javascript">
<!--
function sig()
{
	if ($("#reasons").val()=="")
	{
		alert("请填写调整价格原因");
	}
	else
	{
		var para ="reasons="+$("#reasons").val()+"&excel_file=<%=file%>";
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/AddRetailPriceChangeAjax.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
					$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">正在处理，请稍后......</div>'});
				},
				
				error: function(){
					alert("网络错误，请重试！");
				},
				
				success: function(data)
				{
					if (data)
					{
						$.unblockUI();
						alert("提交成功，请等待审核");
						parent.closeWin();
					}
					else
					{
						$.unblockUI();
						alert("系统错误");
					}
				}
		});
	}
}
//-->
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  >
<form name="add_form" id="export_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/AddRetailPriceChangeAjax.action" >
<input type="hidden" name="excel_file" >
<input type="hidden" name="pro_id" >
</form>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<%
if (errorRows.length>0)
{

%>
	<table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
          <td   align="center" valign="middle" style="color:#333333;font-size:20px;font-weight:bold">[以下商品零售报价不正确]</td>
      </tr>
    </table>
	<br>
	<table width="95%" border="0" align="center" cellpadding="4" cellspacing="1" bgcolor="#cccccc">
      <tr>
        <td height="26" align="left" valign="top" bgcolor="#dddddd" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"><strong>商品名称</strong></td>
        <td align="center" valign="top" bgcolor="#dddddd" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"><strong>零售报价</strong></td>
        <td align="center" valign="top" bgcolor="#dddddd" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;color:#CC0000"><strong>错误原因</strong></td>
      </tr>
<%
for (int i=0; i<errorRows.length; i++)
{
%>

      <tr>
        <td width="55%" align="left" valign="top" bgcolor="#eeeeee" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"><%=errorRows[i].getString("name")%></td>
        <td width="21%" align="center" valign="top" bgcolor="#eeeeee" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"><%=errorRows[i].getString("quote_price")%>
		<%
		if ( StringUtil.getInt(errorRows[i].getString("error_type"))==RetailPriceErrorKey.LOST_MONEY )
		{
			out.println("<span style='font-size:10px;color:#CC0000'>(>="+errorRows[i].getString("lowest_price")+")</span>");
		}
		%>
		</td>
        <td width="24%" align="center" valign="top" bgcolor="#eeeeee" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"><%=retailPriceErrorKey.getRetailPriceErrorById( StringUtil.getInt(errorRows[i].getString("error_type")) )%></td>
      </tr>
<%
}
%>
</table>
<%
}
else
{
RetailPriceQuoteBean retailPriceQuoteBean = quoteMgr.getChangeRetailPriceProducts(file);
changeProducts = retailPriceQuoteBean.getProducts();
if (changeProducts.length>0)
{
%>	
    <br>
	<table width="95%" border="0" cellspacing="0" cellpadding="0">
      <tr>
          <td width="20%"   align="left" valign="middle" style="color:#333333;font-size:20px;font-weight:bold"><%=retailPriceQuoteBean.getCountryArea()%></td>
          <td width="80%"   align="right" valign="middle" style="color:#333333;font-size:20px;font-weight:bold">[申请调整以下商品零售报价]</td>
      </tr>
    </table>
	<br>
<table width="95%" border="0" align="center" cellpadding="4" cellspacing="1" bgcolor="#cccccc">
      <tr>
        <td height="26" align="left" valign="top" bgcolor="#dddddd" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"><strong>商品名称</strong></td>
        <td align="center" valign="top" bgcolor="#dddddd" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"><strong>旧零售报价</strong></td>
        <td align="center" valign="top" bgcolor="#dddddd" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;color:#CC0000"><strong>新零售报价</strong></td>
      </tr>
<%

for (int i=0; i<changeProducts.length; i++)
{
%>

      <tr>
        <td width="55%" align="left" valign="top" bgcolor="#eeeeee" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"><%=changeProducts[i].getString("name")%></td>
        <td width="21%" align="center" valign="top" bgcolor="#eeeeee" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"><%=changeProducts[i].getString("old_retail_price")%>
		</td>
        <td width="24%" align="center" valign="top" bgcolor="#eeeeee" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"><%=changeProducts[i].getString("new_retail_price")%></td>
      </tr>
<%
}
%>
</table>
<br>
<table width="95%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td bgcolor="#cccccc"><strong>调整价格原因：</strong></td>
  </tr>
  <tr>
    <td bgcolor="#eeeeee">
      <textarea name="reasons" id="reasons" style="width:400px;height:150px;"></textarea></td>
  </tr>
</table>
<br>
<%
}
else
{
%>

<table width="95%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td align="center" valign="middle" style="color:#FF0000;font-weight:bold;font-size:15px;"><br>
      <br>
      <br>
      <br>
      <br>
      没有商品零售价被修改</td>
  </tr>
</table>
<%
}
}
%>	
	
	
	</td>
  </tr>
  <tr>
    <td align="right" class="win-bottom-line">
	<%
	if (errorRows.length==0&&changeProducts.length>0)
	{
	%>
      <input type="button" name="Submit2" value="申请" class="normal-green-long" onClick="sig();">
	<%
	}
	%>
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
    </td>
  </tr>
</table> 
</body>
</html>
