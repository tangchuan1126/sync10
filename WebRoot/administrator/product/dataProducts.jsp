<%@page import="com.cwc.app.key.ProductFileTypeKey"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.util.StringUtil"%>
<%@page import="com.cwc.db.DBRow"%>
<%@page import="com.cwc.exception.JsonException"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.beans.jqgrid.FilterBean"%>
<%@page import="net.sf.json.JsonConfig"%>
<%@page import="com.cwc.app.beans.jqgrid.RuleBean"%>
<%@page import="com.cwc.json.JsonUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.PriceUOMKey"%>
<jsp:useBean id="priceUOMKey" class="com.cwc.app.key.PriceUOMKey"></jsp:useBean>

<%@ include file="../../include.jsp"%> 
<%! private static int oldpages = -1; %>
<%
	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	
	String pcid = StringUtil.getString(request,"pcid");
	String cmd = StringUtil.getString(request,"cmd");
	String key = StringUtil.getString(request,"key");
	int union_flag = StringUtil.getInt(request,"union_flag");
	String pro_line_id = StringUtil.getString(request,"pro_line_id");
	int product_file_types = StringUtil.getInt(request, "product_file_types");
	int product_upload_status = StringUtil.getInt(request, "product_upload_status");
	String title_id = StringUtil.getString(request, "title_id");
	int active = StringUtil.getInt(request, "active", -1);
	
	boolean search = Boolean.parseBoolean(StringUtil.getString(request,"_search"));
	String sidx = StringUtil.getString(request,"sidx");//排序使用的字段
	String sord = StringUtil.getString(request,"sord");
	String filter = StringUtil.getString(request,"filters");
	FilterBean filterBean = null;
	if(search)
	{
		Map<String, Object> map = new HashMap<String, Object>();  //String-->Property ; Object-->Clss 
		map.put("rules", RuleBean.class);
		JsonConfig configjson = JsonUtils.getJsonConfig();
		configjson.setClassMap(map);
		filterBean = (FilterBean) JsonUtils.toBean(JSONObject.fromObject(filter),FilterBean.class,configjson); 
	}
			
	String c = StringUtil.getString(request,"rows");
	int pages = StringUtil.getInt(request,"page",1);
	PageCtrl pc = new PageCtrl();
	pc.setPageSize(Integer.parseInt(c));
	pc.setPageNo(pages);
	
	if(oldpages != pages||pages==1){
		
		DBRow rows[];
		
		if (cmd.equals("filter")) {
			
			rows = proprietaryMgrZyj.filterProductByPcLineCategoryTitleId(true, pcid, pro_line_id, union_flag, product_file_types, product_upload_status,title_id, 0L,0,0, pc, request, active);
		} else if (cmd.equals("search")) {
			
			rows = proprietaryMgrZyj.findDetailProductLikeSearch(true, key, title_id, 0L,-1,0,0, pc, request, active);
		} else if(cmd.equals("track_product_file")) {
			
			rows = proprietaryMgrZyj.findProductInfosProductLineProductCodeByLineId(true, pro_line_id, title_id, 0L, pc, request);
		} else {
			
			rows = proprietaryMgrZyj.findAllProductsByTitleAdid(true, title_id, 0L, pc, request, active);
		}
		
		for(int i = 0;i<rows.length;i++){
			
			if(rows[i].get("union_flag",0)==0){
				
				rows[i].add("unionimg","<img src='../imgs/product.png'/>");
			} else {
				int unionCount = productMgrZJ.getProductUnionCountByPid(rows[i].get("pc_id",0L));
				rows[i].add("unionimg","<a href='javascript:void(0)' title='View Suit Relationship'><img src='../imgs/union_product.png' border='0'/><span class='badge'>"+unionCount+"</span></a>");
			}
			
		  	StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
		  	String product_line_name = "";
		  	DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
		  	if(null != catalog)
		  	{
		  		/*
		  		long product_line_id = catalog.get("product_line_id", 0L);
		  		DBRow productLine = productLineMgrTJH.getProductLineById(product_line_id);
		  		if(null != productLine)
		  		{
		  			product_line_name = productLine.getString("name");
		  			catalogText.append("<img src='img/folderopen.gif'/> <a class='nine4' href='javascript:void(0)'>"+product_line_name+"</a><br/>");
			  		String s = "<img src='img/joinbottom.gif'/>";
			  		catalogText.append(s);
		  		}*/
		  		DBRow allFather[] = tree.getAllFather(rows[i].get("catalog_id",0l));
			  	for (int jj=0; jj<allFather.length-1; jj++)
			  	{
			  		catalogText.append("<img src='img/folderopen.gif'/><a class='nine4' href='javascript:afilter("+allFather[jj].getString("id")+")'>"+allFather[jj].getString("title")+"</a><br/>");
			  		String s = (jj==0) ? "<img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>"  : "<img  src='img/empty.gif'/><img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>";
			  		catalogText.append(s);
			  	}
			  	
			  	if (catalog!=null)
			  	{
			  		catalogText.append("<img  src='img/page.gif'/><a href='javascript:afilter("+rows[i].getString("catalog_id")+")'>"+catalog.getString("title")+"</a>");
			  	}  
		  	}
		  	else
		  	{
		  		DBRow productLine = productLineMgrTJH.getProductLineById(rows[i].get("catalog_id", 0L));
		  		if(null != productLine)
		  		{
		  			catalogText.append("<img src='img/folderopen.gif'/><a href='javascript:void(0)'>"+productLine.getString("name")+"</a>");
		  		}
		  	}
		  	

			rows[i].add("catalog_text",catalogText.toString());
			String alive_text;
		  	if (rows[i].get("alive",0)==1)
			{
		      alive_text = "<a name='Submit43' style='width:57px;' class='buttons' value=' Inactive' onClick=\"swichProductAlive('"+rows[i].getString("p_name")+"',"+rows[i].getString("pc_id")+","+rows[i].getString("alive")+")\"><i class='icon-minus-sign icon-red'></i>&nbsp;Inactive</a><hidden></hidden><br/></br/>"+
		      				"<a class='buttons' style='width:57px;' value='Label' onClick=\"make_tab("+rows[i].getString("pc_id")+")\"  ><i class='icon-tags'></i>&nbsp;Label</a><hidden></hidden><br/></br/>"+
				       	   "<a class='buttons' style='width:57px;' value='Manage Codes' onClick=\"manageCodes(this);\"><i class='icon-barcode'></i>&nbsp;Codes</a>";
			}
			else
			{
		       alive_text = "<a name='Submit43' style='width:57px;' class='buttons' value='Active' onClick=\"swichProductAlive('"+rows[i].getString("p_name")+"',"+rows[i].getString("pc_id")+","+rows[i].getString("alive")+")\"><i class='icon-ok-sign icon-green'></i>&nbsp;Active</a><hidden></hidden><br/></br/>"+
		       				"<a class='buttons' style='width:57px;' value='Label' onClick=\"make_tab("+rows[i].getString("pc_id")+")\"><i class='icon-tags'></i>&nbsp;Label</a><hidden></hidden><br/></br/>"+
		       				"<a class='buttons' style='width:57px;' value='Manage Codes' onClick=\"manageCodes(this);\"><i class='icon-barcode'></i>&nbsp;Codes</a>";
			} 
			rows[i].add("alive_text",alive_text);
			rows[i].remove("p_img");
			

			
			StringBuffer html =  new StringBuffer();
			html.append("<ul class='myul'>");
			// 显示出来商品文件的个数 和 商品标签的个数
			ProductFileTypeKey productFileTypeKey = new ProductFileTypeKey();
	  		ArrayList<String> selectedList= productFileTypeKey.getProductFileTypesKeys();
		 	Map<Integer,DBRow> productFileMap = transportMgrZr.getProductFileAndTagFile(rows[i].get("pc_id",0l),rows[i].get("pc_id",0l),FileWithTypeKey.PRODUCT_SELF_FILE);
		 	for(int indexOfList = 0 , countOfList = selectedList.size() ; indexOfList < countOfList ;indexOfList++ ){
		 		DBRow tempCount = productFileMap.get(Integer.valueOf(selectedList.get(indexOfList)));//indexOfList+1
		 		int tempCountNumber = 0 ;
	 			tempCountNumber = null != tempCount?tempCount.get("count",0):0;
	 			html.append("<li style='width:50px;'>");
 				html.append(productFileTypeKey.getProductFileTypesKeyValue(selectedList.get(indexOfList)));
 				html.append(":");
 				html.append(tempCountNumber);
 				html.append("</li>");

		 	}
		 	html.append("</ul>");
		 	rows[i].add("img",html.toString());
			
		 	String amozonCode = rows[i].getString("p_code2");
		 	String upcCode = rows[i].getString("upc");
		 	
			DBRow[] productCodes = productCodeMgrZJ.getUseProductCodes(rows[i].get("pc_id",0l));
			String pcodes = "";
			for(int j=0;j<productCodes.length;j++)
			{
				pcodes += productCodes[j].getString("p_code");
				
				if(j<productCodes.length-1)
				{
					pcodes +="\n";
				}
			}
			//rows[i].add("p_codes",pcodes);
			String p_codes_str = "";//"<input name='Submit43' type='button' class='short-short-button' value='条码' onclick='addProductCode("+rows[i].get("pc_id",0l)+")' title='"+pcodes+"'/>";
			if(!StringUtil.isBlank(amozonCode))
			{
				p_codes_str += "Amazon:"+amozonCode+"<br/>";
			}
			if(!StringUtil.isBlank(upcCode))
			{
				p_codes_str += "UPC:"+upcCode;
			}
			rows[i].add("p_codes",p_codes_str);
			rows[i].add("length_uom_name", lengthUOMKey.getLengthUOMKey(rows[i].get("length_uom", 0)));
			rows[i].add("weight_uom_name", weightUOMKey.getWeightUOMKey(rows[i].get("weight_uom", 0)));
			rows[i].add("price_uom_name", priceUOMKey.getMoneyUOMKey(rows[i].get("price_uom", 0)));
			
			// YUANXINYU sn_size
			long pc_id = rows[i].get("pc_id", 0l);
			int sn_length = proprietaryMgrZyj.getProductSnByPcId(pc_id).length;
			
			String sn_size = rows[i].getString("sn_size", "");
			
				if(sn_length == 1 || sn_length == 0){
					rows[i].put("sn_size", sn_size);	
				}else{
					rows[i].put("sn_size", sn_size+" ["+sn_length+"]");
				}
			
			
			String length = rows[i].getString("length");
			String width = rows[i].getString("width");
			String heigth = rows[i].getString("heigth");
			
			String weight = rows[i].getString("weight");
			String unit_price = rows[i].getString("unit_price");
			String volume = rows[i].getString("volume");
			
			if(!"".equals(length) && length != null && length.matches("[0-9]\\d*\\.?00*")){
				rows[i].put("length",length.substring(0,length.indexOf(".")));
			}
			if(!"".equals(width) && width != null && width.matches("[0-9]\\d*\\.?00*")){
				rows[i].put("width",width.substring(0,width.indexOf(".")));
			}
			if(!"".equals(heigth) && heigth != null && heigth.matches("[0-9]\\d*\\.?00*")){
				rows[i].put("heigth",heigth.substring(0,heigth.indexOf(".")));
			}
			
			if(!"".equals(weight) && weight != null && weight.matches("[0-9]\\d*\\.?00*")){
				rows[i].put("weight",weight.substring(0,weight.indexOf(".")));
			}
			if(!"".equals(unit_price) && unit_price != null && unit_price.matches("[0-9]\\d*\\.?00*")){
				rows[i].put("unit_price",unit_price.substring(0,unit_price.indexOf(".")));
			}
			if(!"".equals(volume) && volume != null && volume.matches("[0-9]\\d*\\.?00*")){
				rows[i].put("volume",volume.substring(0,volume.indexOf(".")));
			}
			
		}		
		
		DBRow data = new DBRow();
		data.add("page",pages);
		data.add("total",pc.getPageCount());
		
		data.add("rows",rows);
		oldpages = pages;
		data.add("records",pc.getAllCount());
		out.println(new JsonObject(data).toString());
	}
%>
