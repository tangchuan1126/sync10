<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
		<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
		
		<script type="text/javascript" src="../js/select.js"></script>

	
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="JavaScript1.2">

function addProductCatalog(parentid)
{
	$.prompt(
	
	"<div id='title'>增加子类</div><br />父类：<input name='proTextParentTitle' type='text' id='proTextParentTitle' style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br>新建类别<br><input name='proTextTitle' type='text' id='proTextTitle' style='width:300px;'>",
	
	{
	      submit: promptCheckAddProductCatalog,
   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductCatalogJSON.action",
							{parentid:parentid},//{name:"test",age:20},
							function callback(data)
							{
								$("#proTextParentTitle").setSelectedValue(data.title);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
					
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProductCatalog.action";
						document.add_form.parentid.value = parentid;
						document.add_form.title.value = f.proTextTitle;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function promptCheckAddProductCatalog(v,m,f)
{
	if (v=="y")
	{
		 if(f.proTextTitle == "")
		 {
				alert("请选填写新建分类名称");
				return false;
		 }
		 
		 return true;
	}

}



function modProductCatalog(id)
{
	$.prompt(
	
	"<div id='title'>修改分类</div><br />分类名称<br><input name='proTextTitle' type='text' id='proTextTitle' style='width:300px;'>",
	
	{

   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductCatalogJSON.action",
							{parentid:id},//{name:"test",age:20},
							function callback(data)
							{
								$("#proTextTitle").setSelectedValue(data.title);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductCatalog.action";
						document.mod_form.id.value = id;
						document.mod_form.title.value = f.proTextTitle;		
						document.mod_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}






function delC(id)
{
	if (confirm("确认操作吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/delProductCatalog.action";
		document.del_form.id.value = id;
		document.del_form.submit();
	}
}


</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 商品管理 »   商品分类</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="id" >

</form>

<form method="post" name="mod_form">
<input type="hidden" name="id" >
<input type="hidden" name="title" >
</form>

<form method="post" name="add_form">
<input type="hidden" name="parentid">
<input type="hidden" name="title">
</form>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
  <form name="listForm" method="post">
    <input type="hidden" name="id">
    <input type="hidden" name="parentid">
    <input type="hidden" name="imp_color">

      <tr> 
        <th colspan="2"  class="left-title">类别名称</th>
        <th  class="right-title" style="vertical-align: center;text-align: center;">ID</th>
        <th width="8%"  class="right-title" style="vertical-align: center;text-align: center;">商品数量</th>
        <th width="25%"  class="right-title">&nbsp;</th>
      </tr>
    <tr > 
      <td height="60" colspan="5">
      		<script type="text/javascript">
				d = new dTree('d');
				d.add(0,-1,'商品分类</td><td align="center" valign="middle" width="20%">&nbsp;</td><td align="center" valign="middle">&nbsp;</td><td align="center" valign="middle" width="25%"><input name="Submit" type="button" class="long-button-add" onClick="addProductCatalog(0)" value=" 增加子类"></td></tr></table>');
				<%
					DBRow [] parentCategory = assetsCategoryMgr.getProductParentCatalog();
					for(int i=0;i<parentCategory.length;i++)
					{
						DBRow [] c1 = assetsCategoryMgr.getProductCatalogByParentId(parentCategory[i].get("id",0l));
						if(c1.length != 0)
						{
				%>
					d.add(<%=parentCategory[i].getString("id")%>,0,'<%=parentCategory[i].getString("title")+"    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HS CODE "+parentCategory[i].getString("hs_code")+":"+parentCategory[i].getString("tax")%></td><td align="center" valign="middle" width="20%"><%=parentCategory[i].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(parentCategory[i].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delC(<%=parentCategory[i].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modProductCatalog(<%=parentCategory[i].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addProductCatalog(<%=parentCategory[i].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
				<%
						for(int ii=0;ii<c1.length;ii++)
						{
							DBRow [] c2 = assetsCategoryMgr.getProductCatalogByParentId(c1[ii].get("id",0l));
							if(c2.length != 0)
							{
				%>
				d.add(<%=c1[ii].getString("id")%>,<%=parentCategory[i].getString("id")%>,'<%=c1[ii].getString("title")%></td><td align="center" valign="middle" width="20%"><%=c1[ii].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(c1[ii].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delC(<%=c1[ii].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modProductCatalog(<%=c1[ii].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addProductCatalog(<%=c1[ii].getString("id")%>)" value=" 增加子类">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="移动"/></td></tr></table>','');
				<%
								for(int j=0;j<c2.length;j++)
								{
									DBRow [] c3 = assetsCategoryMgr.getProductCatalogByParentId(c2[j].get("id",0l));
									if(c3.length != 0)
									{
									
				%>
				d.add(<%=c2[j].getString("id")%>,<%=c1[ii].getString("id")%>,'<%=c2[j].getString("title")%></td><td align="center" valign="middle" width="20%"><%=c2[j].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(c2[j].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delC(<%=c2[j].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modProductCatalog(<%=c2[j].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" onClick="addProductCatalog(<%=c2[j].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
				
				<%
										for(int jj=0;jj<c3.length;jj++)
										{
				%>
				d.add(<%=c3[jj].getString("id")%>,<%=c2[j].getString("id")%>,'<%=c3[jj].getString("title")%></td><td align="center" valign="middle" width="20%"><%=c3[jj].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(c3[jj].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delC(<%=c3[jj].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modProductCatalog(<%=c3[jj].getString("id")%>)" value="修改"></td></tr></table>','');
				<%
										}
									}
									else
									{
										
									
				%>
				d.add(<%=c2[j].getString("id")%>,<%=c1[ii].getString("id")%>,'<%=c2[j].getString("title")%></td><td align="center" valign="middle" width="20%"><%=c2[j].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(c2[j].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delC(<%=c2[j].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modProductCatalog(<%=c2[j].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" onClick="addProductCatalog(<%=c2[j].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
				
				<%
									}
								}
							}
							else
							{
				%>
				d.add(<%=c1[ii].getString("id")%>,<%=parentCategory[i].getString("id")%>,'<%=c1[ii].getString("title")%></td><td align="center" valign="middle" width="20%"><%=c1[ii].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(c1[ii].get("id",0l),null).length%></td><td align="center" valign="middle" width="25%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delC(<%=c1[ii].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modProductCatalog(<%=c1[ii].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addProductCatalog(<%=c1[ii].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
				<%
							}
						}
					}
					else
					{
				%>
					d.add(<%=parentCategory[i].getString("id")%>,0,'<%=parentCategory[i].getString("title")%></td><td align="center" width="20%"><%=parentCategory[i].getString("id")%></td><td align="center" valign="middle" width="8%"><%=productMgr.getProductByCid(parentCategory[i].get("id",0l),null).length%><td align="center" valign="middle" width="25%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delC(<%=parentCategory[i].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modProductCatalog(<%=parentCategory[i].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" onClick="addProductCatalog(<%=parentCategory[i].getString("id")%>)" value=" 增加子类"></td></tr></table>','');	
				<%
					}
				}
				%>
				document.write(d);
			</script>
      </td>
    </tr>
  </form>
</table>
<br>
<br>
<br>
</body>
</html>
