<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long set_pid = StringUtil.getLong(request,"set_pid");
long pid = StringUtil.getLong(request,"pid");

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script>

$().ready(function() {

	$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductUnionJSON.action",
			{set_pid:<%=set_pid%>,pid:<%=pid%>},//{name:"test",age:20},
			function callback(data)
			{   
				$("#p_name").val(data.p_name);
				$("#quantity").val(data.quantity);
			}
	);

	function log(event, data, formatted) {

		
	}
	
	/**
	function formatItem(row) {
		return(row[0]);
	}
	
	function formatResult(row) {
		return row[0].replace(/(<.+?>)/gi, '');
	}
	**/

	$("#p_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 310,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];   
			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name+" ["+data[i].unit_name+"]["+data[i].p_code+"]",
    				value:data[i].p_name+" ["+data[i].unit_name+"]["+data[i].p_code+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
});


function checkForm(theForm)
{	
	if(theForm.p_name.value=="")
	{
		alert("组合或商品名不能为空");
		return(false);
	}
	else if(theForm.quantity.value=="")
	{
		alert("数量不能为空");
		return(false);
	}
	else if(!isNum(theForm.quantity.value))
	{
		alert("请正确填写数量");
		return(false);
	}
	else
	{
		parent.ModUnion(theForm.p_name.value,<%=set_pid%>,<%=pid%>,theForm.quantity.value);
		return(false);
	}
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
</script>

<style type="text/css">
<!--
.STYLE1 {font-weight: bold}
.STYLE3 {color: #999999}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<br>
 <form name="add_union_form" method="post" action="aaa" onSubmit="return checkForm(this)">
<table width="98%" border="0" cellspacing="3" cellpadding="2">

  <tr>
    <td colspan="4" align="center" valign="middle" style="font-size:20px;font-weight:bold;">

	[修改商品]

	<br>
<br></td>
  </tr>
  <tr> 
    <td width="20%" align="right"><strong><span class="STYLE1">
商品名
	</span>：</strong></td>
      <td width="35%"><input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:16px;height:28px;font-weight:bold"/></td>
      <td width="9%" align="right" valign="middle"> <strong>数量： </strong></td>
      <td width="36%"><input type="text" id="quantity" name="quantity" style="color:#FF3300;width:40px;font-size:16px;height:28px;font-weight:bold"/>
	  &nbsp;&nbsp;
      <input name="Submit" type="submit" class="long-button" value="修 改"></td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td colspan="3"><span class="STYLE3">输入关键字，能获得系统提示</span></td>
  </tr>
</table>
</form>
</body>
</html>
