<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long id = StringUtil.getLong(request,"id");
DBRow catalog = catalogMgr.getDetailProductStorageCatalogById(id);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改仓库分类</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script>
function addProduct()
{
	var f = document.add_form;
	
	if (f.title.value == "")
	 {
		alert("请填写仓库名称");
	 }
	 else if (f.ccid.value == 0)
	 {
		alert("请选择仓库所属国家");
	 }
	 else if (f.address.value == "")
	 {
		alert("请填写收货地址");
	 }
	 else if (f.deliver_address.value == "")
	 {
		alert("请填写发货地址");
	 }
	 else if (f.contact.value == "")
	 {
		alert("请填写仓库联系人");
	 }
	 else if (f.phone.value == "")
	 {
		alert("请填写仓库电话");
	 }
	 else
	 {
		parent.document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductStorageCatalog.action";
		parent.document.mod_form.id.value = f.id.value;
		parent.document.mod_form.title.value = f.title.value;
		parent.document.mod_form.address.value = f.address.value;
		parent.document.mod_form.deliver_address.value = f.deliver_address.value;
		parent.document.mod_form.contact.value = f.contact.value;
		parent.document.mod_form.phone.value = f.phone.value;
		parent.document.mod_form.ccid.value = f.ccid.value;
		parent.document.mod_form.submit();	
	 }
}


</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_form" method="post" action="" >
<input type="hidden" name="id" value="<%=id%>">
<table width="98%" border="0" cellspacing="5" cellpadding="2">

  <tr>
    <td width="12%" align="right" valign="middle" class="text-line" >&nbsp;</td>
    <td width="88%" align="left" valign="middle" >&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE1 STYLE2" >仓库名称</td>
    <td align="left" valign="middle" ><input name="title" type="text" class="input-line" id="title" value="<%=catalog.getString("title")%>" ></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >所属国家</td>
    <td align="left" valign="middle" >
	<%
	DBRow countrycode[] = orderMgr.getAllCountryCode();
	String selectBg="#ffffff";
	String preLetter="";
	%>
      <select name="ccid" id="ccid" >
	  <option value="0">选择国家...</option>
	  <%
	  for (int i=0; i<countrycode.length; i++)
	  {
	  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
		{
			if (selectBg.equals("#eeeeee"))
			{
				selectBg = "#ffffff";
			}
			else
			{
				selectBg = "#eeeeee";
			}
		}  	
		
		preLetter = countrycode[i].getString("c_country").substring(0,1);
		

	  %>
	    <option style="background:<%=selectBg%>;" <%=catalog.get("native",0l)==countrycode[i].get("ccid",0l)?"selected":""%> value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
	  <%
	  }
	  %>
      </select> 
	</td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >仓库收货地址</td>
    <td align="left" valign="middle" ><textarea name="address" class="input-line" id="address" style="width:400px;height:50px;"><%=catalog.getString("address")%></textarea></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >仓库发货地址</td>
    <td align="left" valign="middle" ><textarea name="deliver_address" class="input-line" id="deliver_address" style="width:400px;height:50px;"><%=catalog.getString("deliver_address")%></textarea></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >仓库联系人</td>
    <td align="left" valign="middle" ><input name="contact" type="text" class="input-line" id="contact" value="<%=catalog.getString("contact")%>" ></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >仓库电话</td>
    <td align="left" valign="middle" ><input name="phone" type="text"  class="input-line" id="phone" value="<%=catalog.getString("phone")%>" ></td>
  </tr>
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="修改" class="normal-green" onClick="addProduct();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeTBWin();">
	</td>
  </tr>
</table> 
</body>
</html>
