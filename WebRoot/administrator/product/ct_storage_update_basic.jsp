<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.ProductStorageTypeKey"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Mod Address Basic Info</title>

<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript" ></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />


<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 

<style type="text/css">
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; text-align: right;width:100px;}
</style>
<%
	long storageId			= StringUtil.getLong(request, "storageId");
	DBRow storageCatalog	= storageCatalogMgrZyj.getStorageCatalogById(storageId);
	DBRow[] countrycode		= orderMgr.getAllCountryCode();
 %>

<script type="text/javascript">
$(function(){
		$(".chzn-select").chosen({no_results_text: "no this options:"});
	});
	$(function(){
		$.ajax({
				url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+<%=storageCatalog.get("native",0)%>,
				dataType:'json',
				type:'post',
				success:function(data){
					if("" != data){
						var provinces = eval(data);
						$("#pro_id").attr("disabled",false);
						if(0 == <%=storageCatalog.get("pro_id",0)%> || -1 == <%=storageCatalog.get("pro_id",0)%>){
								$("#pro_id").attr("disabled",false);
								$("#pro_id").append("<option value=-1 selected='selected'>Input</option>");
								for(var i = 0; i < data.length; i ++){
								
									if(<%=storageCatalog.get("pro_id",0)%> == data[i].pro_id){
										$("#pro_id").append("<option value="+data[i].pro_id+" selected='selected'>"+data[i].pro_name+"</option>");
									}else{
										$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
									}
								}
								$("#addBillProSpan").append("<input type='text' name='address_state_input' id='address_state_input' value='<%=storageCatalog.getString("pro_input") %>' style='width: 292px;height: 23px;'/>");
						}else{
							$("#pro_id").attr("disabled",false);
							for(var i = 0; i < data.length; i ++){
								
									if(<%=storageCatalog.get("pro_id",0)%> == data[i].pro_id){
										$("#pro_id").append("<option value="+data[i].pro_id+" selected='selected'>"+data[i].pro_name+"</option>");
									}else{
										$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
									}
							}
							$("#pro_id").append("<option value=-1>Input</option>");
						}
					}else{
						$("#pro_id").attr("disabled",true);
						$("#pro_id").append("<option value=-1>Input</option>");
						$("#addBillProSpan").append("<input type='text' name='address_state_input' id='address_state_input' value='<%=storageCatalog.getString("pro_input") %>' style='width: 292px;height: 23px;'/>");
					}
					$("#pro_id").trigger("liszt:updated");
		            $("#pro_id").chosen();
				},
				error:function(){
				}
			});
	});
	function submitData(){
		if(volidate()){
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_catalog/UpdateStorageCatalog.action',
				data:$("#subForm").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "true"){
						showMessage("Success.","success");
						setTimeout("windowClose()", 1000);
					}else{
						showMessage("Unsuccess.","error");
					}
				},
				error:function(){
					showMessage("System error.","error");
				}
			})	
		}
	};
	function volidate(){
		if("" == $("#title").val()){
			showMessage("Input Storage Name.","alert");
			return false;
		}
		if("" == $("#ccid_hidden").val() || 0 == $("#ccid_hidden").val()){
			showMessage("Select Nation.","alert");
			return false;
		}
		return true;
	};
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};

function removeAfterProIdInput(){$("#address_state_input").remove();}
function setPro_id(){
	removeAfterProIdInput();
	var node = $("#ccid_hidden");
	var value = node.val();
 	$("#pro_id").empty();
 	$.ajax({
				url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+value,
				dataType:'json',
				type:'post',
				success:function(data){
					if("" != data){
						var provinces = eval(data);
						$("#pro_id").attr("disabled",false);
						for(var i = 0; i < data.length; i ++){
							$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
						}
						$("#pro_id").append("<option value=-1>Input</option>");
					}else{
						$("#pro_id").attr("disabled",true);
						$("#pro_id").append("<option value=-1>Input</option>");
						$("#addBillProSpan").append("<input type='text' name='address_state_input' id='address_state_input' style='width: 292px;height: 23px;'/>");
					}
					$("#pro_id").trigger("liszt:updated");
		            $("#pro_id").chosen();
				},
				error:function(){
				}
			})	
 
 }
 	 function handleProInput(){
	 	var value = $("#pro_id").val();
	 	if(-1 == value){
	 		$("#addBillProSpan").append("<input type='text' name='address_state_input' id='address_state_input'  style='width: 292px;height: 23px;'/>");
	 	}else{
	 		removeAfterProIdInput();
	 	}
	 };
</script>

</head>
<body>
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;height: 360px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			Mod Storage Basic Info
		</legend>	
		<form action="" id="subForm">
			<input type="hidden" name="parentid" value="<%=storageCatalog.get("parentid",0)%>">
			<input type="hidden" name="storId" value="<%=storageCatalog.get("id",0)%>">
		<table width="98%" border="0" cellspacing="5" cellpadding="2">
		  <tr>
		    <td align="left" valign="middle" class="STYLE3" >Storage Name&nbsp;:&nbsp;</td>
		    <td align="left" valign="middle" ><input name="title" type="text" class="input-line" style="width: 292px;height: 23px;" id="title" value="<%=storageCatalog.getString("title") %>"></td>
		  </tr>
		  <tr>
		    <td align="left" valign="middle" class="STYLE3" >Nation&nbsp;:&nbsp;</td>
		    <td>
			    <%
					String selectBg="#ffffff";
					String preLetter="10990";
				%>
		      	 <select id="ccid_hidden" name="ccid" onChange="removeAfterProIdInput();setPro_id();" class="chzn-select" data-placeholder="Select Nation..." style="width:300px;">
			 	 	<option value="0">Select...</option>
					  <%
					  for (int i=0; i<countrycode.length; i++){
					  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
							if (selectBg.equals("#eeeeee")){
								selectBg = "#ffffff";
							}else{
								selectBg = "#eeeeee";
							}
						}  	
						preLetter = countrycode[i].getString("c_country").substring(0,1);
						if(countrycode[i].getString("ccid").equals(storageCatalog.get("native",0)+"")){
					%>	
						 <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>" selected="selected"><%=countrycode[i].getString("c_country")%></option>
					<%	
						}else{
					  %>
			  		  <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
					  <%
						}
					  }
					%>
		   		 </select>
   			</td>
		  </tr>
		 <tr>
			<td align="left" valign="middle" class="STYLE3" >Province&nbsp;:&nbsp;</td>
			<td> 
				<span id="addBillProSpan">
					<select disabled="disabled" name="pro_id" id="pro_id" onchange="handleProInput()" style="margin-right:5px;width:300px;" class="chzn-select" data-placeholder="Select Province..." ></select>
				</span>
			</td>
		</tr>
		<tr>
			<td align="right" valign="middle" class="STYLE3" >StorageType&nbsp;:&nbsp;</td>
			<td>
				<select name="storage_type" class="chzn-select" data-placeholder="Select Storage Type..." style="width:300px">
					<%
					StorageTypeKey storageTypes = new StorageTypeKey();
						List storageTypeList				= storageTypes.getStorageTypeKeys();
						for(int i = 0; i < storageTypeList.size(); i ++){
							if((storageTypeList.get(i)).equals(storageCatalog.get("storage_type", 0)+"")){
					%>			
								<option value="<%=storageTypeList.get(i) %>" selected="selected"><%=storageTypes.getStorageTypeKeyNameEn((String)storageTypeList.get(i)) %></option>
					<%			
							}else{
					%>		
								<option value="<%=storageTypeList.get(i) %>"><%=storageTypes.getStorageTypeKeyNameEn((String)storageTypeList.get(i)) %></option>
					<%				
							}
						}
					%>
				</select>
			</td>
		</tr>
		</table>
		</form>
	</fieldset>
	<table align="right" style="height:20px;">
		<tr align="right">
			<td colspan="2"><input type="button" class="normal-green" value="Submit" onclick="submitData();"/></td>
		</tr>
	</table>
	<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>