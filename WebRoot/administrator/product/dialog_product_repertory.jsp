<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long ps_id = StringUtil.getLong(request,"ps_id");			  //仓库ID
    long productId=StringUtil.getLong(request,"productId");       //商品id
	long titleId=StringUtil.getLong(request,"titleId");	          //批次名
	String titleName=StringUtil.getString(request,"titleName");	  //titleid
    String productName=StringUtil.getString(request,"productName");
	String lotName=StringUtil.getString(request,"lotName");
	
    DBRow[] rows = productStoreMgrZJ.getProductStoreCountGroupBySlc(ps_id,titleId,productId,lotName);
%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<title>库存位置</title>

</head>
<body>
   <fieldset style="height:95%;border:1px #999 solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
		 <legend style="font-size:15px;font-weight:bold;"> 
			<%=productName %>
		 </legend>
		 <div align="left" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
      
		 	<span style="font-size:12px; font-weight: bold; color:black">当前TITLE:</span>
            <span style="font-size:12px; font-weight: bold; color:#00F"><%=titleName%></span><br/>
            <hr size="1px;" color="#dddddd">
            <span style="font-size:12px; font-weight: bold; padding-left:8px;color:black">当前批次:</span>
            <span style="font-size:12px; font-weight: bold; color:#00F"><%=lotName %></span>
     	 </div>
     	 <%
     	 	for(int i=0;i<rows.length;i++)
     	 	{ 
     	 		DBRow location = locationMgrZJ.getDetailLocationCatalogById(rows[i].get("slc_id",0l)); 
     	 %>
	     	 <fieldset style="border:1px green solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
			 	<legend style="font-size:15px;font-weight:bold;"> 
			 		<a href="javascript:void(0)" onclick="openlp(<%=productId%>,'<%=productName %>',<%=titleId%>,'<%=titleName%>','<%=lotName%>','<%=location.getString("slc_position_all")%>',<%=rows[i].get("slc_id",0l)%>)" style="text-decoration:none; color: green" >
				 		<span style="font-size:13px; font-weight: bold;">所在位置库存:</span>
	        			<span style="font-size:13px; font-weight: bold;"><%=locationMgrZJ.getDetailLocationCatalogById(rows[i].get("slc_id",0l)).getString("slc_position_all")%></span>
        			</a>
			 	</legend>
			 	<div style="padding-left: 100px">
			       	  <span style="font-size:13px; font-weight: bold;">可用数量:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=rows[i].get("available",0) %></span>
			          <span style="font-size:13px; font-weight: bold; margin-left: 20px">物理数量:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=rows[i].get("physical",0) %></span>
			    </div>
			 </fieldset> 
		 <%
		 	}
		 %>
      </fieldset>
</body>
</html>
<script>
function openlp(p_id,p_name,title_id,title_name,lot_name,location_name,location_id){
	var ps_id = <%=ps_id%>;
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/dialog_product_repertory_lp.html?p_id="+p_id+"&p_name="+p_name+"&title_id="+title_id+"&title_name="+title_name+"&lot_name="+lot_name+"&location_name="+location_name+"&location_id="+location_id+"&ps_id="+ps_id; 
	$.artDialog.open(uri , {title: '商品托盘库存',width:'800px',height:'650px', lock: true,opacity: 0.3,fixed: true});
}
</script>

