<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../include.jsp"%> 
<%
long sa_id = StringUtil.getLong(request,"sa_id");
long ps_id = StringUtil.getLong(request,"ps_id");
int approve_status = StringUtil.getInt(request,"approve_status");
String backurl = StringUtil.getString(request,"backurl");

DBRow storageLocationDifferences[] = productMgr.getStorageLocationDifferencesBySaId(sa_id,null);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>


<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}  

function checkForm(theForm)
{
	var reason = theForm.note;
	var sld_ids = theForm.sld_ids;
	
	var haveEmpty = false;
	var oneSelected = false;
	
	//先判断是否数组
	if(typeof sld_ids.length == "undefined")
	{
			if (sld_ids.checked)
			{
				oneSelected = true;
			}
			
			if (reason.value=="")
			{
				reason.style.background = "#EDF36D";
				haveEmpty = true;
			}
	}
	else
	{
		for (i=0; i<sld_ids.length; i++)
		{
			if (sld_ids[i].checked)
			{
				oneSelected = true;
			}
			
			if (sld_ids[i].checked && reason[i].value=="")
			{
				reason[i].style.background = "#EDF36D";
				haveEmpty = true;
			}
		}
	}
	
	if (!oneSelected)
	{
		alert("最少选择一个记录");
		return(false);
	}
	
	if (haveEmpty)
	{
		alert("有商品差异原因没填写");
		return(false);
	}

	return(true);
}

function allCheck(all_or_no)
{
	if(all_or_no)
	{
		$("[name$='sld_ids']").attr("checked",true);
	}
	else
	{
		$("[name$='sld_ids']").attr("checked",false);
	}
		
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();keepAlive()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 » 审核</td>
  </tr>
</table>
<br>
<form name="form1" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/correctionStoreAction.action" onSubmit="return checkForm(this)">
<input type="hidden" name="sa_id" value="<%=sa_id%>">
<input type="hidden" name="ps_id" value="<%=ps_id%>">
<input type="hidden" name="backurl" value="<%=backurl%>">

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th style="vertical-align: center;text-align: center;" class="left-title"><input type="checkbox" onclick="allCheck(this.checked)" checked="checked"/></th>
        <th style="vertical-align: center;text-align: center;" class="left-title">仓库</th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;">商品条码</th>
        <th width="45%" style="vertical-align: center;text-align: left;" class="right-title">差异原因</th>
    </tr>

<%
for (int i=0; i<storageLocationDifferences.length; i++)
{
%>

    <tr > 
      <td   width="8%" height="80" align="center" valign="middle" >
	  <%
	  if ( storageLocationDifferences[i].get("approve_status",0)==0 )
	  {
	  %>
        <input name="sld_ids" type="checkbox" value="<%=storageLocationDifferences[i].getString("sld_id")%>" checked>
	<%
	}
	else
	{
	%>
	&nbsp;
	<%
	}
	%>	
		
		
      </td>
      <td   width="8%" height="80" align="center" valign="middle" ><%=storageLocationDifferences[i].getString("storage_name")%></td>
      <td   width="39%" align="center" valign="middle"  ><%=storageLocationDifferences[i].getString("p_code")%></td>
      <td   align="left" valign="middle" id="td_<%=storageLocationDifferences[i].getString("sld_id")%>" ><table width="98%" border="0" cellspacing="4" cellpadding="0">
          <tr>
            <th  style="vertical-align: center;text-align: left;font-size:15px;color:#333333" >实际库存：<span style="font-weight:bold;color:#0000FF"><%=storageLocationDifferences[i].getString("actual_store")%></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 系统库存：<span style="font-weight:bold;color:#FF0000"><%=storageLocationDifferences[i].getString("sys_store")%></span>
			<input type="hidden" name="actual_store_<%=storageLocationDifferences[i].getString("sld_id")%>" value="<%=storageLocationDifferences[i].getString("actual_store")%>">	
			<input type="hidden" name="sys_store_<%=storageLocationDifferences[i].getString("sld_id")%>" value="<%=storageLocationDifferences[i].getString("sys_store")%>">	
			<input type="hidden" name="pc_id_<%=storageLocationDifferences[i].getString("sld_id")%>" value="<%=storageLocationDifferences[i].getString("pc_id")%>">			</th>
          </tr>
          <tr>
            <th  style="vertical-align: center;text-align: left;font-size:15px;">
				<%
	if (approve_status==1||storageLocationDifferences[i].get("approve_status",0)==1)
	{
	%>
			<span style="color:#990000;font-weight:normal"><%=storageLocationDifferences[i].getString("note")%></span>
	<%
	}
	else
	{
	%>
		<input type="text" name="note_<%=storageLocationDifferences[i].getString("sld_id")%>" id="note" style="width:400px;height:25px;font-size:14px;border:#cccccc solid 1px;color:#FF0000" value="<%=storageLocationDifferences[i].getString("note")%>"> 
	<%
	}
	%>
			
					</th>
          </tr>
        </table></td>
    </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td align="center" valign="middle" bgcolor="#eeeeee">
	<%
	if (approve_status==0)
	{
	%>
	  <input name="Submit" type="submit" class="long-button-redtext" value="校正库存"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%
	}
	%>
    
      <input name="Submit2" type="button" class="long-button" value="返回" onClick="history.back();">
    </td>
  </tr>
</table>
</form>
<div id="keep_alive" style="display:none"></div>
<br>
<br>
</body>
</html>
