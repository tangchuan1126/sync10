<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long catalog_id = StringUtil.getLong(request,"catalog_id");
DBRow sonCatalog[] = catalogMgr.getProductCatalogByParentId(catalog_id,null);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加商品</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script>
function addProduct()
{
	var f = document.add_product_form;
	
	if (f.name.value == "")
	 {
		alert("请填写商品名称");
	 }
	 else if (f.code.value == "")
	 {
		alert("请填写商品条码");
	 }
	 else if (f.unit.value == "")
	 {
		alert("请填写单位");
	 }
	 else if (f.unit_price.value == "")
	 {
		alert("请填写单价");
	 }
	 else if (f.gross_profile.value == "")
	 {
		alert("请填写毛利率");
	 }
	 else if (f.weight.value == "")
	 {
		alert("请填写重量");
	 }
	 else if(f.proTextVolume == 0)
	 {
		alert("请填写体积");
		return false;
	 }
	 else
	 {
		parent.document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProduct.action";
		parent.document.mod_form.catalog_id.value = <%=catalog_id%>;		
		parent.document.mod_form.p_name.value = f.name.value;
		parent.document.mod_form.p_code.value = f.code.value;
		parent.document.mod_form.unit_name.value = f.unit.value;
		parent.document.mod_form.unit_price.value = f.unit_price.value;
		parent.document.mod_form.gross_profit.value = f.gross_profile.value;
		parent.document.mod_form.weight.value = f.weight.value;
		parent.document.mod_form.volume.value = f.volume.value;
		parent.document.mod_form.submit();	
	 }
}

if ( <%=sonCatalog.length%>>0 )
{
	alert("商品必须增加到最底层类分，请重新选择");
	parent.openCatalogMenu();
	parent.closeWin();
	
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_product_form" method="post" action="" >

<table width="98%" border="0" cellspacing="5" cellpadding="2">

  <tr>
    <td colspan="2" align="left" valign="middle" class="text-line" style="border-bottom:1px solid #999999"><table width="100%" border="0" cellpadding="5" cellspacing="0" >
      <tr>
        <td width="96%" style="color:#666666;font-size:20px;font-weight:bold;font-family:Arial, 宋体"> 
		<%
	  Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	  DBRow allFather[] = tree.getAllFather(catalog_id);
	  for (int jj=0; jj<allFather.length-1; jj++)
	  {
	  	out.println(allFather[jj].getString("title")+" » ");
	
	  }
	  
	  DBRow catalog = catalogMgr.getDetailProductCatalogById(catalog_id);
	  if (catalog!=null)
	  {
	  	out.println(catalog.getString("title"));
	  }
	  %>		</td>
      </tr>
      <tr>
        <td style="color:#666666;font-size:13px;font-family:宋体">分类下增加商品......</td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td width="8%" align="right" valign="middle" class="text-line" >&nbsp;</td>
    <td width="92%" align="left" valign="middle" >&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE1 STYLE2" >名称</td>
    <td align="left" valign="middle" ><input name="name" type="text" class="input-line" id="name" ></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >条码</td>
    <td align="left" valign="middle" ><input name="code" type="text" class="input-line" id="code" ></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >单位</td>
    <td align="left" valign="middle" ><input name="unit" type="text" id="unit"  style="width:100px;" >
SET/ITEM/PAIR</td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >单价</td>
    <td align="left" valign="middle" ><input name="unit_price" type="text" id="unit_price"   style="width:100px;"></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >毛利率</td>
    <td align="left" valign="middle" ><input name="gross_profile" type="text" id="gross_profile"   style="width:100px;">
      格式：0.00</td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >重量</td>
    <td align="left" valign="middle" ><input name="weight" type="text" id="weight"  style="width:100px;" >
      Kg</td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >体积</td>
    <td align="left" valign="middle" ><input name="volume" type="text" id="volume"  style="width:100px;" >
      m³</td>
  </tr>
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line"><img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#999999">必须把商品增加到最底层分类</span>         </td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="增加" class="normal-green" onClick="addProduct();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
	</td>
  </tr>
</table> 
</body>
</html>
