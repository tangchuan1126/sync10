<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<%
long pcid = StringUtil.getLong(request,"pcid");

long lable_template_id = StringUtil.getLong(request,"lable_template_id");
DBRow lable_template = lableTemplateMgrZJ.getDetailLableTemplateById(lable_template_id);

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);
long cid = StringUtil.getLong(request,"cid");
int type = StringUtil.getInt(request,"type");
String name = StringUtil.getString(request,"name");
String cmd = StringUtil.getString(request,"cmd");

DBRow rows[];

if (cmd.equals("filter"))
{
	rows = productMgr.getProductByPcid(pcid,0,pc);
}
else if(cmd.equals("name"))
{
	rows = productMgrZJ.getDetailProductLikeSearch(name,pc);//productMgr.getSearchProducts4CT(name,pcid,pc);	
}
else
{
	rows = productMgr.getAllProducts(pc);
}

DBRow[] options = lableTemplateMgrZJ.getAllLableTemplate(null);

Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script language="javascript">

function search()
{
	
	document.search_form.name.value = $("#filter_name").val();	
	document.search_form.pcid.value = $("#filter_pcid").val();	
	if($("#filter_pcid").val()==0&&$("#filter_name").val()=="")
	{
		document.search_form.cmd.value = "";	
	}
	else if($("#filter_name").val()!="")
	{
		document.search_form.cmd.value = "name";
	}
	else
	{
		document.search_form.cmd.value = "filter";	
	}
	
	document.search_form.submit();
}

function importPrint()
{
	tb_show('批量制作商品标签','../lable_template/lable_template_show.html?cmd=file&TB_iframe=true&height=500&width=800',false);
}

function noNumbers(e)
	{
		var keynum
		var keychar
		var numcheck
		
		if(window.event) // IE
		{
		  keynum = e.keyCode
		}
		else if(e.which) // Netscape/Firefox/Opera
		{
		  keynum = e.which
		}
		keychar = String.fromCharCode(keynum)
		numcheck = /[^\d]/
		return !numcheck.test(keychar)
	}

function exportProductBarcode()
{
	var catalog_id= $("#filter_pcid").val();
	var para = "catalog_id="+catalog_id;
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/exportProductBarcode.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){
					if(date["canexport"]=="true")
					{
						document.export_product_barcode.action=date["fileurl"];
						document.export_product_barcode.submit();
					}
					else
					{
						alert("该分类与子类下无所选形式商品，无法导出");
					}
				}
			});
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 


function closeWin()
{
	tb_remove();
}

$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#filter_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill:false,//自动填充匹配
		max:15,
		selectFirst:false,
		cacheLength:1,	//不缓存
		updownSelect:true,
		dataType: 'json',
		
		
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},
		scroll:false,
				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name,
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
});
//-->

function printBarcode(pc_id)
{
	var lable_template_id = document.getElementById("lableTemplate_"+pc_id).value;
	if(lable_template_id !=0)
	{ 	
		var printer;
		var paper;
		var print_range_width;
		var print_range_height;
		var template_path;
		
		var para = "lable_template_id="+lable_template_id;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/lableTemplate/getDetailLableTemplateJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert(e);
					alert("提交失败，请重试！");
				},
				
				success: function(data){

					if (data["close"])
					{
						printer = data.printer;
						paper = data.paper;
						print_range_width = data.print_range_width;
						print_range_height = data.print_range_height;
						template_path = data.template_path;
												
						var copies = document.getElementById("copies_"+pc_id).value;
						
						var counts = document.getElementById("productCount_"+pc_id).value;
						document.print_barcode.pc_id.value = pc_id;
					
						document.print_barcode.copies.value = copies;
						document.print_barcode.printer.value= printer;
						document.print_barcode.paper.value = paper;
						document.print_barcode.print_range_width.value = print_range_width;
						document.print_barcode.print_range_height.value = print_range_height;
						document.print_barcode.counts.value = counts;
						
						document.print_barcode.action = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+template_path;
						
						document.print_barcode.submit();
					}
				}
			});
	}
	else
	{
		alert("请选择正确的标签模板！！");
	}
}

	function selectLabel(product_id)
	{
		tb_show('制作商品标签','../lable_template/lable_template_show.html?pc_id='+product_id+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function selectLabels()
	{
		tb_show('制作商品标签','../lable_template/lable_template_show.html?cmd=products&TB_iframe=true&height=500&width=800',false);
	}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<form name="export_product_barcode" method="post"></form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »商品条码</td>
  </tr>
</table>
<br>
	  <form action="print_product_barcode.html" method="get" name="search_form">		
		  <input type="hidden" name="cmd"> 
		  <input type="hidden" name="cid"> 
		  <input type="hidden" name="type"> 
		  <input type="hidden" name="pcid"> 
		   <input type="hidden" name="name"> 
	</form>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
          <td width="54%" height="33" >
		  <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">
	 
		  <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td width="416" rowspan="2" style="padding-left:10px;">
			  <div style="padding-bottom:5px;color:#666666;font-size:12px;">
	可以选择一个商品分类：	</div>
              <ul id="categorymenu" class="mcdropdown_menu">
 <li rel="0">所有分类</li>
 
    <%
	  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
	  for (int i=0; i<c1.length; i++)
	  {
			out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));

			  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
			  if (c2.length>0)
			  {
			  		out.println("<ul>");	
			  }
			  for (int ii=0; ii<c2.length; ii++)
			  {
					out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
					
						DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
						  if (c3.length>0)
						  {
								out.println("<ul>");	
						  }
							for (int iii=0; iii<c3.length; iii++)
							{
									out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
									out.println("</li>");
							}
						  if (c3.length>0)
						  {
								out.println("</ul>");	
						  }
						  
					out.println("</li>");				
			  }
			  if (c2.length>0)
			  {
			  		out.println("</ul>");	
			  }
			  
			out.println("</li>");
	  }
	  %>
</ul>
	  <input type="text" name="category" id="category" value=""  />
	  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />
	  

<script>
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}

});
<%
if (pcid>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=pcid%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 

</script>			  </td>
              <td width="317"  style="border-bottom:1px #dddddd solid; padding-bottom:10px;"><input name="filter_name" type="text" id="filter_name" value="<%=name%>" style="width:200px;" onkeydown="if(event.keyCode==13)search()">&nbsp;&nbsp;&nbsp;&nbsp;
        <input name="Submit3" type="button" class="button_long_search" value="搜索" onClick="search()">			  </td>
            </tr>
            <tr>
              <td align="center" style="padding-top:5px;"><input type="button" onClick="importPrint()" class="long-button-print" value="批量制作"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onClick="exportProductBarcode()" class="long-button-export" value="导出条码"/></td>
            </tr>
          </table>

			</div>
		  </td>
    <td width="46%" align="right" style="padding-right:10px;"><br>
    <br></td>
  </tr>
</table>
	<td><br></td>
  </tr>
</table>
<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" >
  <form name="listForm" method="post">


    <tr> 
        <th width="150" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
		<th width="226"  style="vertical-align: center;text-align: center;" class="right-title">商品条码</th>
        <th width="157" align="left" class="right-title"  style="vertical-align: center;text-align: left;">商品分类</th>
        <th width="153"  style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
    </tr>

    <%
String delPCID = "";
DBRow proGroup[];
for ( int i=0; i<rows.length; i++ )
{
delPCID += rows[i].getString("pc_id")+",";
%>
    <tr > 
      <td height="60" valign="middle"   style='font-size:14px;'>
        
      <%=rows[i].getString("p_name")%></td>
	  <td align="center" valign="middle">
      	<%=rows[i].getString("p_code") %>	  
	  </td>
      <td height="60" align="left" valign="middle"   style='line-height:20px;' >
	   <span style="color:#999999">
	  <%
	  DBRow allCatalogFather[] = catalogTree.getAllFather(rows[i].get("catalog_id",0l));
	  for (int jj=0; jj<allCatalogFather.length-1; jj++)
	  {
	  	out.println("<a class='nine4' href='?cmd=cid&pcid="+allCatalogFather[jj].getString("id")+"&cid="+cid+"&type=0'>"+allCatalogFather[jj].getString("title")+"</a><br>");

	  }
	  %>
	  </span>
	  
	  <%
	  DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
	  if (catalog!=null)
	  {
	  	out.println("<a href='?cmd=cid&pcid="+rows[i].getString("catalog_id")+"&cid="+cid+"&type=0'>"+catalog.getString("title")+"</a>");
	  }
	  %> </td>
      <td align="center" valign="middle"   >
      	<input type="button" class="long-button-print" value="制作标签" onclick="selectLabel(<%=rows[i].get("pc_id",0l)%>)">
	  </td></tr>
    <%
	}
	%>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
          <input type="hidden" name="p" >
		  <input type="hidden" name="cid" value="<%=cid%>">
		<input type="hidden" name="cmd" value="<%=cmd%>">
		<input type="hidden" name="name" value="<%=name%>">
		<input type="hidden" name="type" value="<%=type%>">
		 <input type="hidden" name="pcid" value="<%=pcid%>">
		
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle">
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
<form target="_blank" method="get" name="print_barcode">
	<input type="hidden" id="pc_id" name="pc_id"/>
	<input type="hidden" id="copies" name="copies"/>
	<input type="hidden" id="counts" name="counts"/>
	<input type="hidden" id="print_range_width" name="print_range_width"/>
	<input type="hidden" id="print_range_height" name="print_range_height"/>
	<input type="hidden" id="printer" name="printer"/>
	<input type="hidden" id="paper" name="paper"/>
</form>
</body>
</html>
