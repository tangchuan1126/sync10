<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单报价</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">

@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

 
  
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<style type="text/css">
table.mytable{border:1px solid silver;border-collapse:collapse ;width:100%; }
table td{border:1px solid silver;line-height:30px;height:30px;}
table td.left {font-weight:bold;color:blue;text-align:right;width:100px;}
table td.right{padding:2px;}
</style>
<script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<% 
	String accountAddAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/account/AddAccountAction.action" ;
	long account_with_id = StringUtil.getLong(request,"account_with_id");
	int account_with_type = StringUtil.getInt(request,"account_with_type");
%>
<script type="text/javascript">
	function addAccount(){
		var from = $("#myform");
		$.ajax({
			url:'<%= accountAddAction%>',
			dataType:'json',
			data:from.serialize(),
			beforeSend:function(request){
		     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
			    $.unblockUI();
			    if(data && data.flag === "success"){
						cancel();
						$.artDialog.opener.refreshWindow && $.artDialog.opener.refreshWindow();
				}
			},
			error:function(){alert("系统错误"); $.unblockUI();}
		})
	}
	function cancel(){$.artDialog && $.artDialog.close();}
	
	function currencyChange(){
  
		var account_currency = $("#account_currency");
		var swift_tr = $("#swift_tr");
		if(account_currency.val() != "rmb"){
		    swift_tr.css("display","");
		}else{
		    swift_tr.css("display","none");
		}
	}
</script>
</head>
 <body>
 	<form action="" id="myform">
 		<input type="hidden" name="account_with_id" value='<%=account_with_id %>'/>
 		<input type="hidden" name="account_with_type" value='<%=account_with_type %>'/>
 		
		  	<table class="mytable">
		  		<tr>
		  			<td class="left">货币类型: </td>
		  			<td class="right">
		  				<select name="account_currency" onchange="currencyChange()" id="account_currency">
		  					<option value="rmb">RMB</option>
		  					<option value="usd">USD</option>
		  				</select>
		  			 </td>
		  		</tr>
		  		<tr>
		  			<td class="left">收款户名:</td>
		  			<td class="right"><input type="text" name="account_name" value=""/></td>
		  		</tr>
		  		<tr>
		  			<td class="left">收款账号:</td>
		  			<td class="right"><input type="text" name="account_number" value=""/></td>
		  		</tr>
		  		<tr>
		  			<td class="left">开户行:</td>
		  			<td class="right"><input type="text" name="account_blank" value=""/></td>
		  		</tr>
		  		 
		  			<tr id="swift_tr" style="display:none;">
		  				<td class="left">SWIFT:</td>
		  				<td class="right">
		  				<input type="text" name="account_swift"/>
 
		  				</td>
		  			</tr>
 
		  		<tr>
		  			<td class="left">联系电话:</td>
		  			<td class="right"><input type="text" name="account_phone" value=""/></td>
		  		</tr>
		  	 
		  		<tr>
		  			<td class="left">联系地址:</td>
		  			<td class="right"><input type="text" name="account_address" value=""/></td>
		  		</tr>
		  		<tr>
		  			<td class="left">开户行地址:</td>
		  			<td class="right"><input type="text" name="account_blank_address" value=""/></td>
		  		</tr>
		  		<tr>
			   		 <td align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
				     <td align="right" class="win-bottom-line">
						 <input type="button" name="Submit2" value="保存修改" class="normal-green" onClick="addAccount();">
				  		 <input type="button" name="Submit2" value="取消" class="normal-white" onClick="cancel();"> 
					 </td>
  					</tr>
		  	</table>
  		</form>
</body>

</html>