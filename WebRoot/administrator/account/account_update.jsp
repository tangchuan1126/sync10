<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单报价</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">

@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

 
  
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<style type="text/css">
table.mytable{border:1px solid silver;border-collapse:collapse ;width:100%; }
table td{border:1px solid silver;line-height:30px;height:30px;}
table td.left {font-weight:bold;color:blue;text-align:right;width:100px;}
 table td.right{padding-left:2px;}
</style>
<script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<%
	long account_id = StringUtil.getLong(request,"account_id");
	DBRow account = accountMgrZr.getAccountById(account_id);
	boolean isOuterCurrency = !account.getString("account_currency").toUpperCase().equals("RMB");
	String updateAccountAction =   ConfigBean.getStringValue("systenFolder") + "action/administrator/account/UpdateAccountAction.action" ;
%>
<script type="text/javascript">
	function cancel(){$.artDialog && $.artDialog.close();}
	function submitAccount(){
		var form = $("#myForm");
		$.ajax({
			url:'<%= updateAccountAction%>',
			data:form.serialize(),
			dataType:'json',
			beforeSend:function(request){
		     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
			    $.unblockUI();
			    if(data && data.flag === "success"){
						cancel();
						$.artDialog.opener.refreshWindow && $.artDialog.opener.refreshWindow();
				}
			},
			error:function(){alert("系统错误"); $.unblockUI();}
		})
	}

</script>
</head>
 <body>
 	<form action="" id="myForm">
 		<input type="hidden" name="account_id" value='<%=account.getString("account_id") %>'/>
		  	<table class="mytable">
		  		<tr>
		  			<td class="left">货币类型: </td>
		  			<td class="right"> <%=account.getString("account_currency").toUpperCase() %></td>
		  		</tr>
		  		<tr>
		  			<td class="left">收款户名:</td>
		  			<td class="right"><input type="text" name="account_name" value='<%= account.getString("account_name") %>'/></td>
		  		</tr>
		  		<tr>
		  			<td class="left">收款账号:</td>
		  			<td class="right"><input type="text" name="account_number" value='<%= account.getString("account_number") %>'/></td>
		  		</tr>
		  		<tr>
		  			<td class="left">开户行:</td>
		  			<td class="right"><input type="text" name="account_blank" value='<%= account.getString("account_blank") %>'/></td>
		  		</tr>
		  		<%if(isOuterCurrency){ %>
		  			<tr>
		  				<td class="left">SWIFT:</td>
		  				<td class="right"><input type="text" name="account_swift" value='<%= account.getString("account_swift") %>'/>
		  				</td>
		  			</tr>
		  		<%} %>
		  		<tr>
		  			<td class="left">联系电话:</td>
		  			<td class="right"><input type="text" name="account_phone" value='<%= account.getString("account_phone") %>'/></td>
		  		</tr>
		  	 
		  		<tr>
		  			<td class="left">联系地址:</td>
		  			<td class="right"><input type="text" name="account_address" value='<%=account.getString("account_address") %>'/></td>
		  		</tr>
		  		<tr>
		  			<td class="left">开户行地址:</td>
		  			<td class="right"><input type="text" name="account_blank_address" value='<%= account.getString("account_blank_address") %>'/></td>
		  		</tr>
		  		<tr>
			    <td align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
			    <td align="right" class="win-bottom-line">
					 <input type="button" name="Submit2" value="保存修改" class="normal-green" onClick="submitAccount();">
			  		 <input type="button" name="Submit2" value="取消" class="normal-white" onClick="cancel();"> 
				</td>
  </tr>
		  	</table>
  		</form>
</body>

</html>