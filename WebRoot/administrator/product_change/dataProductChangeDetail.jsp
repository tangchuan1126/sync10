<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.ProductShapeTypeKey"%>
<jsp:useBean id="productShapeTypeKey" class="com.cwc.app.key.ProductShapeTypeKey"/>
<%! private static int oldpages = -1; %>
<%
	
	
	String c = StringUtil.getString(request,"rows");//每页显示多少数据
	int pages = StringUtil.getInt(request,"page",1);//当前请求的是第几页
	int pageSize = Integer.parseInt(c);
	long cid = StringUtil.getLong(request,"cid");
	PageCtrl pc = null;
	if(pageSize!=-1)
	{
		pc = new PageCtrl();
		pc.setPageSize(pageSize);
		pc.setPageNo(pages);
	}
	
	DBRow[] items = productChangeMgrXJ.findProductChangeItems(cid,pc);
	for(int i=0; i<items.length; i++)
	{
		if(items[i].get("type",0l)==1)
	 	{
	 		
			items[i].add("type",productShapeTypeKey.getContainerTypeKeyValue(ProductShapeTypeKey.SPLIT));
	 	}
		if(items[i].get("type",0l)==2)
	 	{
	 		
			items[i].add("type",productShapeTypeKey.getContainerTypeKeyValue(ProductShapeTypeKey.ASSEMBLY));
	 	}
		if(items[i].get("type",0l)==3)
	 	{
	 		
			items[i].add("type",productShapeTypeKey.getContainerTypeKeyValue(ProductShapeTypeKey.RENOVATE));
	 	}
		if(items[i].get("type",0l)==4)
	 	{
	 		
			items[i].add("type",productShapeTypeKey.getContainerTypeKeyValue(ProductShapeTypeKey.DAMAGED));
	 	}
		if(items[i].get("type",0l)==5)
	 	{
	 		
			items[i].add("type",productShapeTypeKey.getContainerTypeKeyValue(ProductShapeTypeKey.CONVERT));
	 	}
	}
	DBRow data = new DBRow();
	data.add("page",pages);//page，当前是第几页

	if(pc!=null)
	{
		data.add("total",pc.getPageCount());//total，总共页数
	}
	else
	{
		data.add("total",1);//total，总共页数
	}
	
	data.add("rows",items);//rows，返回数据
	data.add("records",items.length);//records，总记录数

	out.println(new JsonObject(data).toString());
	
%>