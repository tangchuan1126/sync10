<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 

<% 
String ps_id = StringUtil.getString(request,"ps_id");
long is_open = StringUtil.getLong(request,"is_open");
String startTime = StringUtil.getString(request,"startTime");
String endTime = StringUtil.getString(request,"endTime");
PageCtrl pc = new PageCtrl();
int p = StringUtil.getInt(request,"p");
pc.setPageNo(p);
pc.setPageSize(10);
DBRow[] productChanges = productChangeMgrXJ.findProductChange(pc,ps_id,is_open,startTime,endTime);


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ProductChange</title>
<script type="text/javascript">

</script>
</head>

<body>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table"  style="margin-top:5px;">
	<thead>
	<tr>
		  <th width="10%"   class="right-title"  style="text-align: center;">ID</th>
		  <th width="21%"   class="right-title"  style="text-align: center;">仓库</th>
		  <th width="16%"  class="right-title" style="text-align: center;">创建人</th>
		  <th width="16%"  class="right-title" style="text-align: center;">状态</th>
		  <th width="20%" class="right-title" style="vertical-align: center;text-align: center;">创建时间</th>	
	</tr>
	</thead>
	<tbody id="tbody">
       <%
      	if(productChanges != null && productChanges.length > 0){
      		for(DBRow row : productChanges){
      	%>
      	    <tr style="text-align: center;height:50px">
      	    	<td><%=row.getString("cid")%></td>
      	    	<td><a style="color:#f60;" target="_blank" href="product_change_detail.html?ps_id=<%=row.getString("ps_id")%>&cid=<%=row.getString("cid") %>&name=<%=row.getString("employe_name") %>&is_open=<%=row.getString("is_open") %>&time=<%=row.getString("create_time")%>&title=<%=row.getString("title")%>"><%=row.getString("title")%></a></td>
      	    	<td><%=row.getString("employe_name")%></td>
      	    	<%
      	    		if(row.getString("is_open").equals("1")){
      	    	%>
      	    	<td>Opend</td>
      	    	<%
      	    		}else{
      	    			
      	    		
      	    	%>
      	    	<td>Closed</td>
      	    	<%
      	    		}
      	    	%>
      	    	<td><%=row.getString("create_time").substring(0,19)%></td>
      	    </tr>
      		
      	<% 		
      		}
      		
      	}else{
      	%>	
      		
      		<tr style="background:#E6F3C5;">
   						<td colspan="5" style="text-align:center;height:120px;">无数据</td>
   		    </tr>
       	<% 
      		 
      	}
        %>
      	 
      </tbody>
	</table>
	</form>
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" >
	    <input type="hidden" name="p"/>
	  </form>
	  <tr>
	    <td height="28" align="right" valign="middle" class="turn-page-table">
	        <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
	      跳转到
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
	    </td>
	  </tr>
   </table>
</body>
</html>

