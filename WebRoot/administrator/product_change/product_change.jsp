<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.ProductShapeTypeKey"%>
<jsp:useBean id="productShapeTypeKey" class="com.cwc.app.key.ProductShapeTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<html>
<head>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>		
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>

<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!--  自动填充 -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>

<% 
DBRow[] treeRows = catalogMgr.getProductStorageCatalogTree();   //仓库
int p = StringUtil.getInt(request,"p");

%>
<script type="text/javascript">

	function loadData(){
		var treeRows=<%=new JsonObject(treeRows).toString()%> ;
		var cangku=[];
		for(var a=0;a<treeRows.length;a++){
			var op={};       
			op.id=treeRows[a].id;                                                                                                                                                                                                                                                                                                                                                                                                      
			op.name=treeRows[a].title;
			cangku.push(op);
		}
		var data=[
				 {key:'仓库：',type:'cangku',array:cangku},
				 {key:'状态：',select:'true',type:'is_open',array:[{id:'1',name:'Opend'},{id:'2',name:'Closed'}]}
		 		 ];
		initializtion(data);  //初始化
	}
	var cangku='';
	var status='';

	//点击查询条件
	function custom_seach(){
	   var array=getSeachValue();
	    cangku='';
	    status='';
	   for(var i=0;i<array.length;i++){
		   if(array[i].key=='cangku'){
			   cangku=array[i].val;
		   }
		   if(array[i].key=='is_open'){
			   status=array[i].val;
		   }
	   }
	   $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	   var para='is_open='+status+'&ps_id='+cangku;
	   $.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product_change/product_change_show_list.html',
			type: 'post',
			dataType: 'html',
			data:para,
			async:false,
			success: function(html){
				$.unblockUI();   //遮罩关闭
				$("#showList").html(html);
				onLoadInitZebraTable(); //重新调用斑马线样式
				
			}
		});	
	}
	function go(number){
		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
		   var para='is_open='+status+'&ps_id='+cangku+'&p='+number;
		   $.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product_change/product_change_show_list.html',
				type: 'post',
				dataType: 'html',
				data:para,
				async:false,
				success: function(html){
					$.unblockUI();   //遮罩关闭
					$("#showList").html(html);
					onLoadInitZebraTable(); //重新调用斑马线样式
					
				}
			});	
	}

	function filter(){

		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
		   var para='startTime='+$("#startTime").val()+'&endTime='+$("#endTime").val();
		   $.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product_change/product_change_show_list.html',
				type: 'post',
				dataType: 'html',
				data:para,
				async:false,
				success: function(html){
					$.unblockUI();   //遮罩关闭
					$("#showList").html(html);
					onLoadInitZebraTable(); //重新调用斑马线样式
					
				}
		});	

	}
		
	
</script>

</head>

<body onload="loadData();">
<div class="demo">
<div id="tabs">
	<ul>
		<li><a href="#search">条件搜索</a></li>
		<li><a href="#filter">过滤</a></li>
	</ul>
	<div id="search">
		<div id="av"></div>  
	</div>
	<div id="filter">
		<table>
			<tr>
				<td align="right" valign="middle">开始时间：</td>
			    <td align="left" valign="middle">
	              <input type="text" class="txt" id="startTime" name="startTime" /> 
	            </td>
	            <td align="right" valign="middle">结束时间： </td>
	                  		  
	            <td align="left" valign="middle">     		  
	              <input type="text" class="txt" id="endTime" name="endTime"" />&nbsp;&nbsp;
	            </td>
	            <td align="left" valign="middle">     		  
				  <input type="button" class="button_long_refresh" value="过滤" onclick="filter();"/>
	            </td>
	   	    </tr>
		</table>
	</div>
</div>
</div>
<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,  
		cookie: { expires: 30000 } ,
	});
</script>
<div id="showList"></div>
<form name="dataForm" >
	    <input type="hidden" name="p"/>
</form>
</body>
</html>
<script>
	$(function(){	
		$('#startTime').datetimepicker({
		    dateFormat: "yy-mm-dd",
			timeFormat: 'hh:mm:ss',
			showSecond: true,
			stepHour: 1,
			stepMinute: 1,
			stepSecond: 1
		});
		 $('#endTime').datetimepicker({
		 	dateFormat: "yy-mm-dd",
			timeFormat: 'hh:mm:ss',
			showSecond: true,
			stepHour: 1,
			stepMinute: 1,
			stepSecond: 1
		});
	 	$("#ui-datepicker-div").css("display","none");
		$.blockUI.defaults = {
				 css: { 
				  padding:        '8px',
				  margin:         0,
				  width:          '170px', 
				  top:            '45%', 
				  left:           '40%', 
				  textAlign:      'center', 
				  color:          '#000', 
				  border:         '3px solid #999999',
				  backgroundColor:'#ffffff',
				  '-webkit-border-radius': '10px',
				  '-moz-border-radius':    '10px',
				  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
				  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
				 },
				 //设置遮罩层的样式
				 overlayCSS:  { 
				  backgroundColor:'#000', 
				  opacity:        '0.6' 
				 },
				 
				 baseZ: 99999, 
				 centerX: true,
				 centerY: true, 
				 fadeOut:  1000,
				 showOverlay: true
				};
		
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product_change/product_change_show_list.html',
			type: 'post',
			dataType: 'html',
			async:false,
			success: function(html){
				$("#showList").html(html);
				onLoadInitZebraTable(); //重新调用斑马线样式
			}
		});	
		 
	});
</script>

