<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.ProductShapeTypeKey"%>
<jsp:useBean id="productShapeTypeKey" class="com.cwc.app.key.ProductShapeTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 

<% 
long ps_id = StringUtil.getLong(request,"ps_id");
long is_open = StringUtil.getLong(request,"is_open");
long cid = StringUtil.getLong(request,"cid");
String name = StringUtil.getString(request,"name");
String time = StringUtil.getString(request,"time");
String title = StringUtil.getString(request,"title");
DBRow[] treeRows = catalogMgr.getProductStorageCatalogTree();
boolean edit = false;

//open状态可以修改
if(is_open==1)
{
	edit = true;
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ProductChangeDetail</title>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<style type="text/css">
	body{text-align:left;}
	ol.fundsOl{
		list-style-type: none;	
	}
	ol.fundsOl li{
		width: 350px;
		height: 200px;
		float: left;
		line-height: 25px;
	}
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<!-- jqgrid 引用 -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>


<script type="text/javascript">
function autoComplete(obj)
{
	addAutoComplete(obj,
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
		"merge_info",
		"p_name");
}
function afterShowAdd(formid)
{
	autoComplete($("#p_name"));
}

function afterShowEdit(formid)
{
	autoComplete($("#p_name"));
}
function errorFormat(serverresponse,status)
{
	return errorMessage(serverresponse.responseText);
}

function refreshWindow(){
	window.location.reload();
}
function errorMessage(val)
{
	var error1 = val.split("[");
	var error2 = error1[1].split("]");	
	
	return error2[0];
}
</script>
</head>

<body onload="onLoadInitZebraTable();">
<div class="demo">
<div id="transportTabs">
	<ul>
		<li><a href="#transport_basic_info">基础信息<span> </span></a></li>
	</ul>
	<div id="transport_basic_info">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="80%" valign="top">
					<table width="50%">
						<tr>
							<td width="5%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">仓库：</font></td>
							<td align="left" width="10%">
								<%=title %>
							</td>
							<td width="5%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">创建人：</font></td>
							<td align="left" width="10%"><%=name %></td>
						</tr>
						<tr>
							<td width="5%" align="right"height="25"><font style="font-family: 黑体; font-size: 14px;">状态：</font></td>
							<%
			      	    		if(is_open==1){
			      	    	%>
			      	    	<td align="left" width="10%">Opend</td>
			      	    	<%
			      	    		}else{
			      	    			
			      	    		
			      	    	%>
							<td align="left" width="10%">Closed</td>
			      	    	<%
			      	    		}
			      	    	%>
							<td width="5%" align="right"height="25"><font style="font-family: 黑体; font-size: 14px;">创建时间：</font></td>
							<td align="left" width="10%"><%=time.substring(0,19) %></td>
						</tr>
					 </table>
				   </td>
				 </tr>
			</table>	
	</div>
</div>
</div>
<script>
	$("#transportTabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	 
	});

</script>
	<div id="detail" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	
	
	<table id="gridtest"></table>
	<div id="pager"></div> 
	
	<script type="text/javascript">
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					sortable:true,
					url:'dataProductChangeDetail.html',
					datatype: "json",
					postData:{cid:<%=cid%>},
					width:parseInt(document.body.scrollWidth*0.98),
					height:350,
					autowidth: false,
					shrinkToFit:true,
					jsonReader:{
				   			id:'itemid',
	                        repeatitems : false
	                	},
	                	colNames:['itemid','cid','商品名','商品数量','类型'], 
				   	colModel:[ 
				   		{name:'itemid',index:'itemid',hidden:true,sortable:false},
				   		{name:'cid',index:'cid',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=cid%>},hidden:true,sortable:false},
				   		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
				   		{name:'pc_num',index:'pc_num',editable:<%=edit%>,align:'left',editrules:{required:true}},
				   		{name:'type',width:50,index:'type',editable:<%=edit%>,align:'left',edittype:"select",editrules:{required:true},
					   		editoptions:{value:"1:<%=productShapeTypeKey.getContainerTypeKeyValue(ProductShapeTypeKey.SPLIT)%>;2:<%=productShapeTypeKey.getContainerTypeKeyValue(ProductShapeTypeKey.ASSEMBLY)%>;4:<%=productShapeTypeKey.getContainerTypeKeyValue(ProductShapeTypeKey.DAMAGED)%>;5:<%=productShapeTypeKey.getContainerTypeKeyValue(ProductShapeTypeKey.CONVERT)%>"}}
					   
				   		], 
				   	rowNum:15,
				   	mtype: "GET",
				   	gridview: true, 
					rownumbers:true,
				   	pager:'#pager',
				   	sortname: 'itemid', 
	                viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
				   	multiselect: true,
					caption: "ProductChangeDetail",
				   	afterEditCell:function(id,name,val,iRow,iCol)
				   	{ 
                		if(name=='p_name') 
				   		{
				   			autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
				   		}
				   		select_iRow = iRow;
				   		select_iCol = iCol;
				    }, 
				    cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_change/GridEditProductChangeDetailAction.action',
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_change/GridEditProductChangeDetailAction.action',
				   	errorCell:function(serverresponse, status)
				   	{
				   		alert(errorMessage(serverresponse.responseText));
				   	}
				}); 		   	
				jQuery("#gridtest").jqGrid('navGrid',"#pager",{edit:false,add:<%=edit%>,del:<%=edit%>,search:false,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit},{closeAfterAdd:true,afterShowForm:afterShowAdd,errorTextFormat:errorFormat});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
			   	jQuery("#gridtest").jqGrid('gridResize',{minWidth:350,maxWidth:parseInt(document.body.scrollWidth*0.95),minHeight:80, maxHeight:350});
			   
	</script>
</div>
</body>
</html>

