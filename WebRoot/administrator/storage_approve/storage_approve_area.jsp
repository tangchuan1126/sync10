<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.ApproveStatusKey"%>
<%@ include file="../../include.jsp"%> 
<%
 	DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

 long ps_id = StringUtil.getLong(request,"ps_id");
 long area_id = StringUtil.getLong(request,"area_id");
 int approve_status = StringUtil.getInt(request,"approve_status");
 String sortby = StringUtil.getString(request,"sortby","post_date");
 boolean sort = Boolean.valueOf(StringUtil.getString(request,"sort","true"));
 int p = StringUtil.getInt(request,"p");

 PageCtrl pc = new PageCtrl();
 pc.setPageNo(p);
 pc.setPageSize(30);

 ApproveStatusKey approveStatusKey = new ApproveStatusKey();

 DBRow[] storageApproveArea = storageApproveMgrZJ.filterStorageApproveArea(ps_id,area_id,approve_status,sortby,sort,pc);
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>区域差异审核</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<script language="javascript">
function getAreaByPs(ps_id)
{
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/GetLocationAreasJSON.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:{
				ps_id:ps_id
			},
			
			beforeSend:function(request){
			},
			
			error: function(){
				//console.log("error")
			},
				
			success: function(data)
			{
				$("#area_id").clearAll();
				$("#area_id").addOption("全部区域","0");
				if (data!="")
				{
					$.each(data,function(i){
						$("#area_id").addOption(data[i].title+data[i].area_name,data[i].area_id);
					});
				}		
			}
	});
}

function dateSort(sortby,sort)
{
	document.search_form.sort.value = sort;
	document.search_form.sortby.value = sortby;
	document.search_form.submit();
}

function showDifferent(saa_id)
{ 
 	$.artDialog.open('<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_approve/storage_approve_location.html?saa_id='+saa_id, {title: '位置审核',width:'985px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 » 仓库区域差异审核</td>
  </tr>
</table>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
  <tr>
    <td>
	<form action="storage_approve_area.html" method="post" name="search_form">
	<input type="hidden" name="sort">
	<input type="hidden" name="sortby">
		<select name="ps_id" id="ps_id" onchange="getAreaByPs(this.value)">
		<option value="0">所有仓库</option>
          <%
			String qx;
			
			for ( int i=0; i<treeRows.length; i++ )
			{
				if ( treeRows[i].get("parentid",0) != 0 )
				 {
				 	qx = "├ ";
				 }
				 else
				 {
				 	qx = "";
				 }
		  %>
          <option value="<%=treeRows[i].getString("id")%>"> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>
          </option>
          <%
			}
		  %>
      </select>
	 <select name="area_id" id="area_id">
	 	<option value="0">全部区域</option>
	 </select> 
	<select name="approve_status" id="approve_status">
	  <option value="0" selected>全部状态</option>
	   <option value="<%=ApproveStatusKey.WAITAPPROVE%>" <%=approve_status==ApproveStatusKey.WAITAPPROVE?"selected":""%>><%=approveStatusKey.getApproveStatusById(ApproveStatusKey.WAITAPPROVE)%></option>
	   <option value="<%=ApproveStatusKey.APPROVE%>" <%=approve_status==ApproveStatusKey.APPROVE?"selected":""%>><%=approveStatusKey.getApproveStatusById(ApproveStatusKey.APPROVE)%></option>
	  </select>
	&nbsp;&nbsp;
	<input name="Submit2" type="submit" class="long-button-ok" value="过滤">
	
	</form>
	    </td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th style="vertical-align: center;text-align: center;" class="left-title">区域</th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;">
        <%
        	boolean sortOrder;
        	if(sort)
        	{
        		sortOrder = false;
        	}
        	else
        	{
        		sortOrder = true;
        	}
        %>
        <a href="javascript:dateSort('post_date',<%=sortOrder%>)">提交日期</a>
			<%
				if(sortby.equals("post_date"))
				{
					if(sort)
					{
			%>
				<img src="../imgs/arrow_up.png" width="16" height="16" align="absmiddle">
			<%
					}
					else
					{
			%>
				<img src="../imgs/arrow_down.png" width="16" height="16" align="absmiddle">
			<%
					}
				}
			%>
		</th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;">提交人</th>
        <th width="11%" style="vertical-align: center;text-align: center;" class="right-title">差异位置数/已审核</th>
        <th width="11%" class="right-title" style="vertical-align: center;text-align: center;">状态</th>
        <th width="8%" class="right-title" style="vertical-align: center;text-align: center;">审核人</th>
        <th width="14%" class="right-title" style="vertical-align: center;text-align: center;">
        <a href="javascript:dateSort('approve_date',<%=sortOrder%>)">审核日期</a>
			<%
				if(sortby.equals("approve_date"))
				{
					if(sort)
					{
			%>
				<img src="../imgs/arrow_up.png" width="16" height="16" align="absmiddle">
			<%
					}
					else
					{
			%>
				<img src="../imgs/arrow_down.png" width="16" height="16" align="absmiddle">
			<%
					}
				}
			%>
		</th>
        <th width="14%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
    </tr>

<%
for (int i=0; i<storageApproveArea.length; i++)
{
%>

    <tr > 
      <td   width="15%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold'><%=catalogMgr.getDetailProductStorageCatalogById(storageApproveArea[i].get("ps_id",0l)).getString("title")+storageApproveArea[i].getString("area_name")%></td>
      <td   width="17%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=storageApproveArea[i].getString("post_date")%></td>
      <td   width="10%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
      <%
      	DBRow admin = adminMgr.getDetailAdmin(storageApproveArea[i].get("commit_account",0l));
      	if(admin==null)
      	{
			admin = new DBRow();
      	}
      	out.print(admin.getString("account"));
      %>
      </td>
      <td   align="center" valign="middle" style="font-size:15px;"><%=storageApproveArea[i].getString("difference_location_count")%>/<span style="color:#006600;font-weight:normal"><%=storageApproveArea[i].getString("difference_approve_location_count")%></span></td>
      <td   align="center" valign="middle" >
	  <%
	  if ( storageApproveArea[i].get("approve_status",0)==ApproveStatusKey.WAITAPPROVE)
	  {
	  	out.println("<img src='../imgs/standard_msg_error.gif' width='16' height='16' align='absmiddle'><font color=red><strong>"+approveStatusKey.getApproveStatusById(ApproveStatusKey.WAITAPPROVE)+"</strong></font>");
	  }
	  else
	  {
	  	out.println("<img src='../imgs/standard_msg_ok.gif' width='16' height='16' align='absmiddle'> <font color=green><strong>"+approveStatusKey.getApproveStatusById(ApproveStatusKey.APPROVE)+"</strong></font>");
	  }
	  %>
	  </td>
      <td   align="center" valign="middle" >
      	<% 
      		DBRow approveAdmin = adminMgr.getDetailAdmin(storageApproveArea[i].get("approve_account",0l));
      		if(approveAdmin==null)
      		{
			
      		}
      		else{
      		out.print(approveAdmin.getString("account"));
      		}
      	%>
      	&nbsp;
      </td>
      <td   align="center" valign="middle" >
	  <%
		  	out.println(storageApproveArea[i].getString("approve_date","&nbsp;"));
	  %></td>
      <td   align="center" valign="middle" >
	  <%
	  if(storageApproveArea[i].get("approve_status",0)==ApproveStatusKey.WAITAPPROVE)
	  {
	  %>
	   <input name="Submit" type="button" class="short-button" value="审核" onclick="showDifferent(<%=storageApproveArea[i].get("saa_id",0l)%>)">
	  <%
	  }
	  else
	  {
	  %>
	   <input name="Submit" type="button" class="short-button" value="详细" onclick="showDifferent(<%=storageApproveArea[i].get("saa_id",0l)%>)">
	  <%
	  }
	  %>
      </td>
    </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
    <input type="hidden" name="p">
	<input type="hidden" name="ps_id" value="<%=ps_id%>">
	<input type="hidden" name="area_id" value="<%=area_id%>">
	<input type="hidden" name="approve_status" value="<%=approve_status%>">
	<input type="hidden" name="sortby" value="<%=sortby%>"/>
	<input type="hidden" name="sort" value="<%=sort%>"/>
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
</body>
</html>
