<%@page import="com.cwc.app.key.ContainerLocationDifferentTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.ApproveStatusKey"%>
<%@ include file="../../include.jsp"%> 
<%
	long saa_id = StringUtil.getLong(request,"saa_id");
	DBRow storageApproveArea = storageApproveMgrZJ.getDetailStorageApproveArea(saa_id);
	long ps_id = storageApproveArea.get("ps_id",0l);
	long temp_ps_id = 0l;//临时仓库，常量ID
	
	int showIndex = 0;
	int showContainerIndex = 0;
	DBRow[] storageApproveLocation = storageApproveMgrZJ.getStorageApproveLocationBySaaId(saa_id);
	for(int i = 0;i<storageApproveLocation.length;i++)
    {
    	if(storageApproveLocation[i].get("approve_status",0)==ApproveStatusKey.WAITAPPROVE)
    	{
    		showIndex = i;
    		break;
    	}
    }
    DBRow[] storageApproveContainer = storageApproveMgrZJ.getStorageApproveContainers(saa_id,storageApproveLocation[showIndex].get("slc_id",0l));
    
    for(int i = 0;i<storageApproveContainer.length;i++)
    {
    	if(storageApproveContainer[i].get("approve_status",0)==ApproveStatusKey.WAITAPPROVE)
    	{
    		showContainerIndex = i;
    		break;  
    	}
    }
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>位置差异审核</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
 
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<link rel="stylesheet" href="../js/office_tree/dtree.css" type="text/css"></link>
<script type="text/javascript" src="../js/office_tree/dtree2.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<style>
form
{
	padding:0px;
	margin:0px;
}
.normal-white
{
	margin-top:50px;
}
</style>
<script type="text/javascript">
	jQuery(function($){
		$("#location_tabs").tabs({
			cache: false,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			selected:<%=showIndex%>
		});
		
		
		$(".container_tabs").tabs({
			cache: false,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			selected:<%=showContainerIndex%>
		});
		
		$(".tabs").tabs({
			cache: false,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			selected:0
		});
})

function checkSubmit(id)
{
	var isSubmit = true;
	$("[id='"+id+"']").each(function(){
		var textreason=trim(this.value);		
 		if(textreason.length==0)
 		{
 			this.style.background = "#EDF36D";
 			isSubmit = false;
 			$("#container_"+id).tabs("select",0);
 		}
 		else
 		{
 			this.style.background = "";
 		}
	});
	textOnFacues();
	
	return isSubmit;
}
function closeWindow()
{
	parent.window.location.reload();
	$.artDialog.close();
}

function textOnFacues()
{
	$(":text").each(function(){
		if(this.value.length==0)
		{
			this.focus();
			return false;
		}
	});
}
function trim(str){   
    str = str.replace(/^(\s|\u00A0)+/,'');   
    for(var i=str.length-1; i>=0; i--){   
        if(/\S/.test(str.charAt(i))){   
            str = str.substring(0, i+1);   
            break;   
        }   
    }   
    return str;   
}  
</script>
</head>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
	<div id="location_tabs" style="height:350px;">
      <ul>
      	<%
      		for(int i = 0;i<storageApproveLocation.length;i++)
      		{
      	%>
      		<li style="padding:1px;"><a href="#location_<%=storageApproveLocation[i].get("sal_id",0l)%>"><%=storageApproveLocation[i].getString("slc_position_all")%></a></li>
      	<%
      		}
      	%>
      </ul>
      	<%
      		for(int i = 0;i<storageApproveLocation.length;i++)
      		{
      	%>
      		<div id="location_<%=storageApproveLocation[i].get("sal_id",0l)%>">
      			<div class="container_tabs" style="height:250px;">
      				<ul>
      					<%
		      				DBRow[] storageApproveContainers = storageApproveMgrZJ.getStorageApproveContainers(saa_id,storageApproveLocation[i].get("slc_id",0l));
		      				for(int j = 0;j<storageApproveContainers.length;j++)
		      				{
		      			%>
		      				<li style="padding:1px;"><a href="#container_<%=storageApproveContainers[j].get("sac_id",0l)%>"><%=storageApproveContainers[j].getString("container")%></a></li>
		      			<%
		      				}
		      			%>
      				</ul>
      					<%
		      				for(int j = 0;j<storageApproveContainers.length;j++)
		      				{
		      			%>
		      				<div class="tabs" id="container_<%=storageApproveContainers[j].get("sac_id",0l)%>">
							<form action="<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/storageApprove/approveStorageLocationDifferent.action" onsubmit="return checkSubmit(<%=storageApproveContainers[j].get("sac_id",0l)%>)">
		      					<ul>
		      						<li><a href="#container_count_<%=storageApproveContainers[j].get("sac_id",0l)%>">数量差异</a></li>
		      						<li><a href="#container_loading_<%=storageApproveContainers[j].get("sac_id",0l)%>">关系差异</a></li>
		      					</ul>
		      					<div class="tabs" id="container_count_<%=storageApproveContainers[j].get("sac_id",0l)%>">
		      						
					      			<input type="hidden" value="<%=storageApproveContainers[j].get("sac_id",0l)%>" name="sac_id"/>
					      			<input type="hidden" value="<%=storageApproveContainers[j].get("slc_id",0l)%>" name="slc_id"/>
					      			<input type="hidden" value="<%=storageApproveContainers[j].get("ps_id",0l)%>" name="ps_id"/>
					      			<input type="hidden" value="<%=storageApproveContainers[j].get("approve_con_id",0l)%>" name="con_id"/>
					      			<%
					      				DBRow[] storageLocationDifferents = storageApproveMgrZJ.getLocationDifferents(saa_id,storageApproveContainers[j].get("approve_con_id",0l));
					      				
					      			%>
					      				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
					      					<tr>
					      						<th width="30%" style="vertical-align: center;text-align: center;" class="left-title">商品名</th>
					      						<th width="80" style="vertical-align: center;text-align: center;" class="left-title">物理数量</th>
					      						<th width="80" style="vertical-align: center;text-align: center;" class="left-title">盘点数量</th>
					      						<th style="vertical-align: center;text-align: center;" class="left-title">原因</th>
					      					</tr>
					      				
					      			<%
					      				for(int q = 0;q<storageLocationDifferents.length;q++)
					      				{
					      			%>
					      				<tr>
						      				<td align="left" nowrap="nowrap"><%=storageLocationDifferents[q].getString("p_name")%></td>
						      				<td align="center"><%=storageLocationDifferents[q].getString("sys_store")%></td>
						      				<td align="center"><%=storageLocationDifferents[q].getString("actual_store")%></td>
						      				<td valign="middle" style="padding-top:2px;">
						      					<input type="hidden" name="sld_id" value="<%=storageLocationDifferents[q].get("sld_id",0l)%>"/>
						      					<%
								      				if(storageApproveContainers[j].get("approve_status",0)==ApproveStatusKey.WAITAPPROVE)
								      				{
								      			%>
								      				<input id="<%=storageApproveContainers[j].get("sac_id",0l)%>" name="note_<%=storageLocationDifferents[q].get("sld_id",0l)%>" style="width:80%" value="<%=storageLocationDifferents[q].getString("note")%>">
								      			<%
								      				}
								      				else
								      				{
								      			%>
								      				<%=storageLocationDifferents[q].getString("note")%>
								      			<%
								      				}
								      			%>
						      					
						      				</td>
					      				</tr>
					      			<%
					      				}
					      			%>
					      			</table>
		      					</div>
		      					<div id="container_loading_<%=storageApproveContainers[j].get("sac_id",0l)%>">
		      						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		      							<tr>
		      								<td valign="top" width="50%" style="border-right:1px solid;empty-cells:show">
		      								<script type="text/javascript">
		      								<%="LP"+storageApproveContainers[j].get("con_id",0l)%> = new dTree('<%="LP"+storageApproveContainers[j].get("con_id",0l)%>');
		      								<%="LP"+storageApproveContainers[j].get("con_id",0l)%>.add('0','-1','系统关系','');
												<% 
													DBRow storageLocationDifferentsContainer = storageApproveMgrZJ.getDetailStorageLocationDifferentsContainer(saa_id,storageApproveContainers[j].get("con_id",0l));
													if(storageLocationDifferentsContainer!=null)
													{
														DBRow[] sysContainerTree = storageApproveMgrZJ.getContainerTreeToDTree(ps_id, storageApproveContainers[j].get("con_id",0l));
			      										for(int q = 0;q<sysContainerTree.length;q++)
				      									{
												%>
																<%="LP"+storageApproveContainers[j].get("con_id",0l)%>.add('<%=sysContainerTree[q].get("id",0l)%>','<%=sysContainerTree[q].get("parent_id",0l)%>','<%=sysContainerTree[q].getString("text")%>','');
												<%
														}
			      									}
			      								%>
			      								document.write(<%="LP"+storageApproveContainers[j].get("con_id",0l)%>);
			      								<%="LP"+storageApproveContainers[j].get("con_id",0l)%>.openAll();
			      							</script>
		      								</td>
		      								<td valign="top" width="50%">
		      									<script type="text/javascript">
		      										<%="TempLP"+storageApproveContainers[j].get("con_id",0l)%> = new dTree('<%="TempLP"+storageApproveContainers[j].get("con_id",0l)%>');
		      										<%="TempLP"+storageApproveContainers[j].get("con_id",0l)%>.add('0','-1','盘点关系','');
												<% 
													if(storageLocationDifferentsContainer!=null)
													{
														DBRow[] tempContainerTree = storageApproveMgrZJ.getContainerTreeToDTree(temp_ps_id, storageApproveContainers[j].get("con_id",0l));
			      										for(int q = 0;q<tempContainerTree.length;q++)
				      									{
				      							%>
				      									<%="TempLP"+storageApproveContainers[j].get("con_id",0l)%>.add('<%=tempContainerTree[q].get("id",0l)%>','<%=tempContainerTree[q].get("parent_id",0l)%>','<%=tempContainerTree[q].getString("text")%>','');
				      							<%	
				      									}	
													}
			      								%>
			      								document.write(<%="TempLP"+storageApproveContainers[j].get("con_id",0l)%>);
			      								<%="TempLP"+storageApproveContainers[j].get("con_id",0l)%>.openAll();
			      								</script>
		      								</td>
		      							</tr>
		      						</table>
		      					</div>
		      					<%
					      				if(storageApproveContainers[j].get("approve_status",0)==ApproveStatusKey.WAITAPPROVE)
					      				{
					      			%>
					      				<br/>
						      			<div style="width:100%" align="center">
						      				<input name="Submit" type="submit" class="short-button" value="审核">
						      			</div>
					      			<%
					      				}
					      			%>
					      			</form>
      						</div>
		      			<%
		      				}
		      			%>
      			</div>
      			
      		</div>
      	<%
      		}
      	%>
     </div>
     <div align="right" style="width:100%;padding-top:10px;">
     	<input type="button" name="Submit2" value="关闭" class="normal-white" onClick="closeWindow()">
     </div>
     <script type="text/javascript">
     	textOnFacues();
     </script>
</body>
</html>
