<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.LableTemplateKey"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>



<%


long lable_template_id = StringUtil.getLong(request,"lable_template_id");
DBRow lable_template = lableTemplateMgrZJ.getDetailLableTemplateById(lable_template_id);

//采购单id
long purchase_id = StringUtil.getLong(request,"purchase_id");
long pcid = StringUtil.getLong(request,"pcid");
//商品id
long pc_id = StringUtil.getLong(request,"pc_id");

//接受工厂型号
String factoryType=StringUtil.getString(request,"factory_type");


//收货仓库
String warehouse=StringUtil.getString(request,"warehouse");

//接受供应商id
long supplierSid=StringUtil.getLong(request,"supplierSid");


PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

long cid = StringUtil.getLong(request,"cid");
int type = StringUtil.getInt(request,"type");
String name = StringUtil.getString(request,"name");
String cmd = StringUtil.getString(request,"cmd");



DBRow[] rows;

if(cmd.equals("search"))
{
	rows = productMgrZJ.searchProductsByPurchaseIdKey(purchase_id,name,null);
}
else
{
	rows = productMgrZJ.getProductsByPurchaseId(purchase_id,null);
}

DBRow product = productMgr.getDetailProductByPcid(pc_id);

DBRow[] options = lableTemplateMgrZJ.getAllLableTemplate(null);

Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));

//生产线
DBRow[] productLine= mgrZwb.getAllProductLine(); 

//采购单
//DBRow[] allPurchase= mgrZwb.getAllPurchase();

//供应商
DBRow[] allSupplier=mgrZwb.getAllSupplier();


 
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>标签打印</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript" src="../js/poshytip/jquery-1.4.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 


<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script language="javascript">
var  products =  <%= new JsonObject(productLine).toString()%> ;
var  suppliers =  <%= new JsonObject(allSupplier).toString()%> ;

jQuery(function($){  

	var supplierSid=<%=supplierSid%>; //供应商
	var purchase_id=<%=purchase_id%>; //采购单
	for(var i=0;i<suppliers.length;i++){
       if(suppliers[i].id==supplierSid){
           $("#suppliers").remove();
    	   var selectHtml='<select id="suppliers" class="chzn-select1" data-placeholder="Choose a Country..." style="width:250px;" tabindex="1" onchange="twoMenu()">'+
    	   '<option value="'+supplierSid+'">'+suppliers[i].sup_name+'</option></select>';
    	   $("#suppliersDiv").html("");
		   $("#suppliersDiv").append(selectHtml);

		   $("#purchases").remove();
	       var selectHtml2='<select id="purchases" class="chzn-select2" data-placeholder="Choose a Country..." style="width:250px;" tabindex="1" >'+
	       '<option value="'+purchase_id+'">'+purchase_id+'</option></select>';
	       $("#purchaseDiv").html("");
		   $("#purchaseDiv").append(selectHtml2);
       }
	}

	$(".chzn-select").chosen({no_results_text: "没有该选项:"});
	$(".chzn-select1").chosen({no_results_text: "没有该选项:"});
	$(".chzn-select2").chosen({no_results_text: "没有该选项:"});
	
});


function noNumbers(e)
	{
		var keynum
		var keychar
		var numcheck
		if(e.keyCode==8||e.keyCode==46||e.keyCode==37||e.keyCode==38||e.keyCode==39||e.keyCode==40)
		{
			return true;
		}
		else if(window.event) // IE
		{
		  keynum = e.keyCode
		}
		else if(e.which) // Netscape/Firefox/Opera
		{
		  keynum = e.which
		}
		keychar = String.fromCharCode(keynum)
		numcheck = /[^\d]/
		return !numcheck.test(keychar);
	}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 


function closeWin()
{
	tb_remove();
}
//-->

	function printProductLabel(pc_id,width,height)
	{
		printer = "LabelPrinter";
		paper = width+"X"+height;
		print_range_width = width;
		print_range_height = height;
		
		template_path = "lable_template/productbarcode.html";
													
							
		var counts = document.getElementById("productcount_"+pc_id).value;
		document.print_product_label.pc_id.value = pc_id;
		if(parseInt(document.getElementById("productprintcount_"+pc_id).value)!=document.getElementById("productprintcount_"+pc_id).value)
		{
			alert("打印数量有问题！");
		}
		else
		{
			document.print_product_label.copies.value = $("#productprintcount_"+pc_id).val();
			document.print_product_label.printer.value= printer;
			document.print_product_label.paper.value = paper;
			document.print_product_label.print_range_width.value = print_range_width;
			document.print_product_label.print_range_height.value = print_range_height;
			document.print_product_label.counts.value = counts;
							
			document.print_product_label.action = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+template_path;
			document.print_product_label.submit();
		}
	}
	
	function printBoxLabel(pc_id)
	{
		var counts = document.getElementById("productcount_"+pc_id).value;
		
		if(parseFloat(document.getElementById("productcount_"+pc_id).value)!=document.getElementById("productcount_"+pc_id).value)
		{
			alert("装箱数量有误！");
		}
		else if((parseInt(document.getElementById("boxprintcount_"+pc_id).value))!=document.getElementById("boxprintcount_"+pc_id).value)
		{
			alert("打印数量有误！");
		}
		else
		{
			document.print_box_label.pc_id.value = pc_id
			document.print_box_label.copies.value = $("#boxprintcount_"+pc_id).val();
			document.print_box_label.counts.value = counts;
						
			document.print_box_label.submit();
		}
	}

	
	//第一个 下拉事件
	function fireMenu(){
	   $("#suppliers").remove();
	   var value=$("#productLine").val();
	   var selectHtml='<select id="suppliers" class="chzn-select1" data-placeholder="Choose a Country..." style="width:250px;" tabindex="1" onchange="twoMenu()">'+
	   '<option value="0">请选择供应商</option>';

	   $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/FindSupplierAction.action',
			dataType:'json',
			data:"productLineId=" + value,
			success:function(suppliers){       
				   for(var i=0;i<suppliers.length;i++){
				          if(suppliers[i].product_line_id==value){
				        	  selectHtml+='<option class="op" value="'+suppliers[i].id+'">'+suppliers[i].sup_name+'</option> ';	  
				          }
					   }
				   selectHtml += '</select>';
				   $("#suppliersDiv").html("");
				   $("#suppliersDiv").append(selectHtml);
				   $(".chzn-select1").chosen();			
		    }
		})
 
	   $("#purchases").remove();
	   var selectHtml3='<select id="purchases" class="chzn-select2" data-placeholder="Choose a Country..." style="width:250px;" tabindex="1" onchange="sMenu()">'+
	   '<option value="0">请选择采购单号</option>';
	   $("#purchaseDiv").html("");
	   $("#purchaseDiv").append(selectHtml3);
	   
	   $(".chzn-select2").chosen();
	 
	}

	//第二个 下拉事件
	function twoMenu(){
      $("#purchases").remove();
      var value=$("#suppliers").val();
      var selectHtml='<select id="purchases" class="chzn-select2" data-placeholder="Choose a Country..." style="width:250px;" tabindex="1" >'+
      '<option value="0">请选择采购单号</option>';
      $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/Purchase/FindPurchaseAction.action',
			dataType:'json',
			data:"suppliersId=" + value,
			success:function(purchases){     
	    	  for(var i=0;i<purchases.length;i++){
	    	         if(value==purchases[i].supplier){
	    	        	 selectHtml+='<option class="op" value="'+purchases[i].purchase_id+'">'+purchases[i].purchase_id+'</option> ';
	    	         }
	    	      }
    	      selectHtml += '</select>';
    		   $("#purchaseDiv").html("");
    		   $("#purchaseDiv").append(selectHtml);
    		   $(".chzn-select2").chosen();	 	
		    }
		})

      
     
	}

	//重新加载 条码页面
	function ssMenu(){
		
	  var value=$("#purchases").val();
	  var factoryNum=$("#factoryNum").val();  
	  var factoryId=$("#suppliers").val();
	  var uri = "<%=ConfigBean.getStringValue("systenFolder")+"administrator/"+lable_template.getString("template_path")%>";
	  var counts = $("#productcount_<%=pc_id%>").val();
	  var para = "print_range_width=<%=lable_template.getString("print_range_width")%>&print_range_height=<%=lable_template.getString("print_range_height")%>&printer=<%=lable_template.getString("printer")%>&paper=<%=lable_template.getString("paper")%>&pc_id=<%=pc_id%>&factoryNum="+factoryNum+"&factoryId="+factoryId+"&purchaseId="+value+"&counts="+counts;
	  
		$.ajax({
			url:uri,
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{
				print_range_width:<%=lable_template.getString("print_range_width")%>,
				print_range_height:<%=lable_template.getString("print_range_height")%>,
				printer:'<%=lable_template.getString("printer")%>',
				paper:'<%=lable_template.getString("paper")%>',
				pc_id:<%=pc_id%>,
				rp_id:$("#rp_id").val(),
				test_result:$("#test_result").val()
			},
			
			beforeSend:function(request)
			{			
			},		
			error: function()
			{			
			},		
			success: function(html)
			{
				$("#<%=pc_id%>").html(html);
			}
		});
	  
	}

	function clearValue(){
		var node = $("#factoryNum");
		var txnId = node.val();
		if(txnId === "*工厂型号"){
			node.val("");
		}
	}
	function checkValue(){
		var node = $("#factoryNum");
		if(node.val().length < 1){
			node.val("*工厂型号")
		}
	}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

 
 
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="">
<div align="center">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <th width="240" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
		<th width="240" style="vertical-align: center;text-align: center;" class="right-title">商品条码</th>
        <th width="120" style="vertical-align: center;text-align: center;" class="right-title">打印数量</th>
        <%
      	if(lable_template.get("lable_type",0)==LableTemplateKey.OUTSIZELABEL)
      	{
      	%>
        <th width="120" style="vertical-align: center;text-align: center;" class="right-title">包装商品数</th>
        <%
        }
        %>
        <th width="120" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
    </tr>
    <tr> 
      <td height="100" valign="middle"   style="font-size:14px;border-bottom:1px #eeeeee solid; ">
      <%=product.getString("p_name")%></td>
	  <td align="center" valign="middle" style="font-size:14px;border-bottom: 1px #eeeeee solid;">
      	<%=product.getString("p_code") %>	  
	  </td>
      <td align="center" valign="middle" nowrap="nowrap" style="border-bottom:1px #eeeeee solid; ">
      	<input style="width:25px;" maxlength="3" type="text" onKeyPress="return noNumbers(event)" id="productprintcount_<%=product.get("pc_id",0l) %>" name="productprintcount_<%=product.get("pc_id",0l) %>" value="1"/>
      </td>
      <%
      	if(lable_template.get("lable_type",0)==LableTemplateKey.OUTSIZELABEL)
      	{
      %>
      <td align="center" valign="middle" style="padding-top:5px; border-bottom:1px #eeeeee solid; ">
      	<input name="productcount_<%=product.get("pc_id",0l) %>" id="productcount_<%=product.get("pc_id",0l) %>" type="text" value="1" style="width:25px;color:#FF0000" onKeyPress="return noNumbers(event)" onchange='chageLabel(this.value)'/>
	  </td>
	  <%
      	}
      %>
	  <td style="border-bottom: 1px #eeeeee solid;">
	  	<input type="button" class="long-button-print" value="打印" onclick="printBarCode()"/><br/><br/>
	  	<input class="button_long_search" type="button" onclick="ssMenu()" value="预览" name="browse">
	  </td>
	 </tr>
	 <tr>
	    <td>
	       RMA:<input type="text" name="rp_id" id="rp_id" size="39"/>
	    </td>
	    <td>
	                 测试结果:<input type="text" name="test_result" id="test_result" size="39"/>
	    </td>
	    <td></td>
	    <td></td>
	 </tr>
	 <tr>
	 	<td colspan="5" id="<%=pc_id%>" align="center" valign="bottom" style="padding-top: 20px">
	 	</td>
	 </tr>
</table>


<form action="<%=ConfigBean.getStringValue("systenFolder")+"administrator/"+lable_template.getString("template_path")%>" target="_blank" method="get" name="print_product_label">
	<input type="hidden" id="pc_id" name="pc_id"/>
	<input type="hidden" id="copies" name="copies"/>
	<input type="hidden" id="counts" name="counts"/>
	<input type="hidden" id="print_range_width" name="print_range_width" value="<%=lable_template.getString("print_range_width")%>"/>
	<input type="hidden" id="print_range_height" name="print_range_height" value="<%=lable_template.getString("print_range_height")%>"/>
	<input type="hidden" id="printer" name="printer" value="<%=lable_template.getString("printer") %>"/>
	<input type="hidden" id="paper" name="paper" value="<%=lable_template.getString("paper")%>"/>
	
</form>

<form action="serialNumber.html" name="madeSerialNumber_form">
	<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="pcids" id="pcids" value="<%=pc_id%>"/>
	<input type="hidden" name="pccounts" id="pccounts"/>
</form>

</div>
<script type="text/javascript">
	function ajaxLoadPurchasePage(url)
		{
			var counts = $("#productcount_<%=pc_id%>").val();
			var para = "print_range_width=<%=lable_template.getString("print_range_width")%>&print_range_height=<%=lable_template.getString("print_range_height")%>&warehouse=<%=warehouse%>&printer=<%=lable_template.getString("printer")%>&paper=<%=lable_template.getString("paper")%>&pc_id=<%=pc_id%>&purchaseId=<%=purchase_id%>&factoryNum=<%=factoryType%>&factoryId=<%=supplierSid%>&counts="+counts;
			
			
			$.ajax({
				url:url,
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request)
				{
					
				},
				
				error: function()
				{
					
				},
				
				success: function(html)
				{	
					$("#<%=pc_id%>").html(html);
				}
			});
		}
		<%
		 	if(lable_template.get("lable_type",0)!=LableTemplateKey.SEQUENCELABEL)
		 	{
	 	%>
			ajaxLoadPurchasePage("<%=ConfigBean.getStringValue("systenFolder")+"administrator/"+lable_template.getString("template_path")%>");
		<%
			}
		%>
		function printBarCode()
		{
		    
		    //alert(<%=lable_template.getString("print_range_width")%>);
		   // alert(<%=lable_template.getString("print_range_height")%>);
			ssMenu();
			if(parseInt(document.getElementById("productprintcount_<%=pc_id%>").value)!=document.getElementById("productprintcount_<%=pc_id%>").value)
			{
				alert("打印数量有问题！");
				$("#productprintcount_<%=pc_id%>").focus();
			}
			<%
				if(lable_template.get("lable_type",0)==LableTemplateKey.OUTSIZELABEL)
				{
			%>
			else if(parseFloat(document.getElementById("productcount_<%=pc_id%>").value)!=document.getElementById("productcount_<%=pc_id%>").value)
			{
				alert("包装商品数量有问题！");
				$("#productcount_<%=pc_id%>").focus();
			}
			<%
				}
			%>
			else
			{
				<%
				 	if(lable_template.get("lable_type",0)!=LableTemplateKey.SEQUENCELABEL)//不是打印序列号
				 	{
			 	%>
					 	//获取打印机名字列表
				    	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
					    //判断是否有该名字的打印机
				    	var printer = "<%=lable_template.getString("printer")%>";
				    	var printerExist = "false";
						for(var i = 0;i<printer_count;i++){

							if(printer==visionariPrinter.GET_PRINTER_NAME(i)){
								printerExist = "true";
								break;
							}
						}
						if(printerExist=="true"){
							//判断该打印机里是否配置了纸张大小
							var paper="<%=lable_template.getString("paper")%>";
						    var strResult=visionariPrinter.GET_PAGESIZES_LIST(printer,",");
						    var str=strResult.split(",");
						    var status=false;
						    for(var i=0;i<str.length;i++){
			                       if(str[i]==paper){
			                          status=true;
			                       }
							}
						    if(status==true){
						    	 visionariPrinter.PRINT_INIT(0,0,"<%=Float.parseFloat(lable_template.getString("print_range_width"))%>mm","<%=Float.parseFloat(lable_template.getString("print_range_height"))%>mm","商品标签");
						    	 visionariPrinter.SET_PRINTER_INDEXA ("<%=lable_template.getString("printer")%>");//指定打印机打印
		                         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"<%=lable_template.getString("paper")%>");
			   				     visionariPrinter.ADD_PRINT_HTM(0,"0.5mm","100%","100%","<body leftmargin='0' topmargin='0'>"+document.getElementById("container_<%=pc_id%>").innerHTML+"</body>");
			   					 visionariPrinter.SET_PRINT_COPIES($("#productprintcount_<%=pc_id%>").val());
			   				     //visionariPrinter.PREVIEW();
			   					 visionariPrinter.PRINT();
						    }else{
						    	$.artDialog.confirm("打印机配置纸张大小里没有符合的大小.....", function(){
		             				 this.close();
		                 			}, function(){
		             			});
						    }
						}else{					
							var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
							if(op!=-1){ //判断是否点了取消
								 visionariPrinter.PRINT_INIT(0,0,"<%=Float.parseFloat(lable_template.getString("print_range_width"))%>mm","<%=Float.parseFloat(lable_template.getString("print_range_height"))%>mm","商品标签");	    	
		                         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"<%=lable_template.getString("paper")%>");
			   				     visionariPrinter.ADD_PRINT_HTM(0,"0.5mm","100%","100%","<body leftmargin='0' topmargin='0'>"+document.getElementById("container_<%=pc_id%>").innerHTML+"</body>");
			   					 visionariPrinter.SET_PRINT_COPIES($("#productprintcount_<%=pc_id%>").val());
			   				     //visionariPrinter.PREVIEW();
			   					 visionariPrinter.PRINT();
							}
						}

				<%
					}
					else//打印序列号
					{
				%>
						document.madeSerialNumber_form.pccounts.value = $("#productprintcount_<%=pc_id%>").val();
						document.madeSerialNumber_form.submit();
				<%
					}
				%>
			}
		}
		
		function chageLabel(val)
		{
			if(val.length>0&&parseFloat(val)==val)
			{
				ajaxLoadPurchasePage("<%=ConfigBean.getStringValue("systenFolder")+"administrator/"+lable_template.getString("template_path")%>");
			}
			else
			{
				alert("请告诉我们箱子里到底有多少该产品");
			}
		}
</script>
</body>
</html>
