<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="com.cwc.app.key.ContainerTypeKey" %>
<%@page import="com.fr.base.core.json.JSONObject"%>
<%@page import="com.cwc.json.JsonObject,com.cwc.app.floor.api.zyj.service.*,com.cwc.app.floor.api.zyj.model.*,java.util.*,net.sf.json.JSONArray"%>
<%@ include file="../../../include.jsp"%>
<html>
<head>
<title>Print Tlp</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/blockui/jquery.blockUI.233.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 打印 -->
<script type="text/javascript" src="../../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../../js/print/m.js"></script>
<!-- 替换标签占位符 -->
<script type="text/javascript" src="../../js/labelTemplate/assembleUtils.js"></script>
<link rel="stylesheet" type="text/css" href="../../js/labelTemplate/assembleUtils.css"/>
<!-- 时间控件 -->
<script type="text/javascript" src="../../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- select2选项卡 -->
<link rel="stylesheet" href="../../js/select2-4.0.0-rc.2/dist/css/select2.css">
<link rel="stylesheet" href="../../jquery-select2-custom.css">
<script type="text/javascript" src="../../jquery-select2-custom.js"></script>
<!-- 提示信息 -->
<script type="text/javascript" src="../../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../../js/fullcalendar/stateBox.css" rel="stylesheet"/>
</head>

<%
	long id=StringUtil.getLong(request,"id");
	DBRow row = clpTypeMgrZr.findContainerById(id);
	DBRow tlp = clpTypeMgrZr.findContainerProduct(id);
	DBRow ctp = null;
	if(row != null && row.get("type_id",0l)!=0){
		ctp = clpTypeMgrZr.findContainerCon(row.get("type_id",0l));
	}
	
	String printName=StringUtil.getString(request,"print_name");
	boolean isPrint = StringUtil.getInt(request, "isprint") == 1;
		
	DBRow titleRow = null;
	if(row!=null&&row.get("title_id",0l)!=0){
		titleRow=proprietaryMgrZyj.findProprietaryByTitleId(row.get("title_id",0l));
	}
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	DBRow[] rows = productLableTemp.findAllLable("", ContainerTypeKey.TLP+"", 2, null);
	String date = new TDate().getFormateTime(DateUtil.NowStr(), "MM/dd/yy");
	JSONObject datas = new JSONObject();
	datas.put("DATE",date);
	datas.put("CUSTOMER", "&nbsp;");
	/*
	String supplier = (null!=titleRow)?titleRow.getString("title_name"):"&nbsp;";
	String total_package = clp.get("stack_length_qty", 0l) + "*" + clp.get("stack_width_qty", 0l) + "*" + clp.get("stack_height_qty", 0l);
	DBRow productCode = proprietaryMgrZyj.findDetailProductByPcId(clp.get("pc_id",0l));
	datas.put("TYPEID", StringUtil.isBlank(clp.getString("type_name"))?"&nbsp;":clp.getString("type_name"));
//	datas.put("CLPTYPE", StringUtil.isBlank(clp.getString("lp_name"))?"&nbsp;":clp.getString("lp_name"));
	datas.put("TOTPKG",total_package);
	datas.put("LOT",StringUtil.isBlank(row.getString("lot_number"))?"&nbsp;":row.getString("lot_number"));
	datas.put("TOTPDT",clp.get("inner_total_pc", 0l));
	datas.put("CONID", StringUtil.isBlank(row.getString("container"))?"&nbsp;":row.getString("container"));
	datas.put("PCODE", StringUtil.isBlank(productCode.getString("p_code"))?"&nbsp;":productCode.getString("p_code"));
	datas.put("TITLE", supplier);
	*/
	
	HttpSession sess = request.getSession(true);
	Map<String,Object> loginUser = (Map<String,Object>) sess.getAttribute("adminSesion");
	int corporationType = (Integer)loginUser.get("corporationType");
	int customerId = 0;
	//存放登录用户类型，表示用户是admin,是Customer,还是Title或者其它
	String userType = "";
	if(corporationType == 1){
		customerId = (Integer)loginUser.get("corporationId");
		userType = "Customer";
	}else if(Boolean.TRUE.equals(loginUser.get("administrator"))){
		userType = "Admin";
	}
	
    ProductCustomerSerivce  productCustomerService = (ProductCustomerSerivce)MvcUtil.getBeanFromContainer("productCustomerSerivce");
    CustomerService customerService = (CustomerService)MvcUtil.getBeanFromContainer("customerService");
    TitleService titleService = (TitleService)MvcUtil.getBeanFromContainer("titleService");
    List<Customer> customers = null;
    List<Title> titles = null;

    long  pc_id = 0;
	if(tlp != null)
	{
		DBRow productCode = proprietaryMgrZyj.findDetailProductByPcId(tlp.get("pc_id",0l));
		datas.put("PCODE", StringUtil.isBlank(productCode.getString("p_code"))?"NA":productCode.getString("p_code"));
		datas.put("TOTPDT", tlp.get("cp_quantity",0l));
		datas.put("TOTPKG", tlp.get("cp_quantity",0l));
	    pc_id = tlp.get("pc_id",0l);
	    if(!userType.equals("Customer")){
	        customers = customerService.getCustomers((int)pc_id);
	        titles = titleService.getTitles((int)pc_id);
	    }else{
	    	titles = titleService.getTitles((int)pc_id,customerId);
	        Customer customer = customerService.getCustomer(customerId);
	        customers = new ArrayList<Customer>();
	        if(customer != null){
	        	customers.add(customer);
	        }
	    }
	}
	else
	{
		datas.put("PCODE", "NA");
		datas.put("TOTPDT", "&nbsp;");
		datas.put("TOTPKG", "&nbsp;");
		
	    if(!userType.equals("Customer")){
	    	titles = titleService.getAll();
	        customers = customerService.getAll();
	    }else{
	    	Customer customer = customerService.getCustomer(customerId);
	        customers = new ArrayList<Customer>();
	        if(customer != null){
	        	customers.add(customer);
	        }
	        titles = titleService.getTitlesByCustomerId(customerId);
	    }
	}
	datas.put("TYPEID", ctp!=null?ctp.getString("type_name"):"");
	datas.put("LOT", row!=null?row.getString("lot_number"):"");
	datas.put("TITLE", titleRow!=null?titleRow.getString("title_name"):"");
	datas.put("CONID", row!=null?row.getString("container"):"");	


//手动选择title和customer时，构造选项，gql 2015/04/27
	//String title = "";
	//DBRow titles[] = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(request);
	String title_options = "";
	
	boolean one_title_flag = (titles.size() == 1);
	for (Title title : titles) {
		String title_name = title.getName();
		long title_id = title.getId();
		String selected = "";
		if(one_title_flag){
			selected = "selected='selected'";
		}
		title_options += "<option value='"+ title_id+"' " + selected + ">"+title_name+"</option>";
	}
	
	String customer_options = "";
	
	//DBRow[] customers = printLabelMgrGql.getAllCustomerId();
	boolean one_customer_flag = customers.size() == 1;
	for (Customer customer : customers) {
		String selected = "";
		if(one_customer_flag || customer.getId() == customerId){
			selected = "selected='selected'";
		}
		customer_options += "<option value='"+ customer.getId() +"' "+selected+">"+ customer.getName() +"</option>";
	}  
//手动选择title和customer时，构造选项，gql 2015/04/27  end
%>
<script type="text/javascript">
var productId = "<%=pc_id %>";
var userType = "<%=userType %>";
	$(function(){
		var lable_template = $("#rongqi").html();
		initLableTemplate(lable_template);
	});
	
	//初始化模板
	function initLableTemplate(lable_template){
		var datas = <%=datas%>;
		var html = assembleTemplate(datas,lable_template);
		$("#rongqi").html(html);
		var $rongqi = $("#rongqi");
// 		$("#ui-datepicker-div").remove();
		//初始化标签时间显示可修改状态 gql 
		if($("input[name='input_date']",$rongqi).length){
			$("span[name='dateShow']",$rongqi).css("display","none");
			$("input[name='input_date']",$rongqi).css("display","");
			$("span[name='dateShow']",$rongqi).html('<%=date%>');
			$("input[name='input_date']",$rongqi).val('<%=date%>');
			//添加时间控件		
			$("input[name='input_date']",$rongqi).datepicker({
					dateFormat:"mm/dd/y",
					changeMonth: true,
					changeYear: true
				}).bind("change",function(){
			    	  var input_date = $(this).val();
			    	  var $obj = $(this).prev("span[name='dateShow']").html(input_date);
			      });
				
			$("#ui-datepicker-div").css("display","none");
		}
		
		//TLP标签，初始化pcode可修改状态，并且修改时，修改条码,注意：修改打印内容的date/pcode页面布局时，要修改此处  gql 
		$("input[name='pcodeInput']",$rongqi).css("display","");
		$("div[name='pcodeText']",$rongqi).css("display","none");
		$("input[name='pcodeInput']",$rongqi).val('<%=row!=null&&!"".equals(row.getString("pcode"))?row.getString("pcode"):"NA"%>');
		//pcode输入框值改变时，修改pcode条码和pcode显示内容
		$("input[name='pcodeInput']",$rongqi).bind('change',function(){
		 	var $this = $(this);
		 	var $img =  $this.parent("div").parent("td").find("img[name='pcodeImg']");
		 	var $div =  $this.parent("div").parent("td").find("div[name='pcodeText']");
		 	var pcode_val = $this.val().toLocaleUpperCase().trim();
		 	//验证格式为大写字,数字,$,-,+,=,/
		 	var match = /^(?=[0-9A-Z-$+=/]*$)/g.test(pcode_val);
		 	if(pcode_val.length>0&&match){
		 		var src = '/barbecue/barcode?data='+pcode_val+'&width=1&height=50&type=code39';
			 	$img.attr("src",src);
			 	$div.html(pcode_val);
		 	}else{
		 		setTimeout(function(){
		 			$this.focus();
		 		}, 0);
		 		showMessage("Enter the letters and numbers","alert");
		 		
		 	}
		});
		$("#lable_template").select2({
            placeholder: "Select label...",
            allowClear: true
        });
// 		$(".select2").remove();
		//初始化customer手动选择，隐藏打印时的div
		var customer_options = "<%=customer_options%>";
		$("select[name='select_customer']",$rongqi).append(customer_options);
		$("div[name='show_customer']",$rongqi).css("display","none");
// 		$("select[name='select_customer']").each(function(i, elem){
			$("select[name='select_customer']",$rongqi).select2({
			 placeholder: "Select..."
	        ,allowClear: true
	      }).bind("change",function(event){
	    	  var customerId = $(event.target).val();
	    	  var customer = $(this).children("option[value='" + customerId + "']").text();
	    	  $(this).prev("div[name='show_customer']").find("center").html(customer+"&nbsp;&nbsp;");
	    	  if(userType == "Customer"){
	    		  return ;
	    	  }	    	  
	          
	          if($.trim(customerId).length == 0){
	        	  customerId = 0;
	          }
	          var target = event.target; 
	          var url = "/Sync10/basicdata/title/customer/" + customerId
	          if(parseInt(productId) > 0){
	        	  url = "/Sync10/basicdata/productCustomer/" + productId + "/customer/" + customerId;
	          }
	          
	          $.ajax({
	               type : "get",  
	               url  : url,
	               success : function(data){
	            	   var titleId =  $(target).parent().next().find("select[name='select_title']").val();

	                   if(data && data.length > 0){
		            	   //标识当前选中的Title 是否出现在新的Title 列表中
		            	   var exist = false;
	                	   $(target).parent().next().find("select[name='select_title']").children().remove("option[value!='']");
	                       for(var i = 0; i < data.length; i++){
	                           if(titleId == data[i].id){
	                        	   exist = true;
	                           }
	                           $(target).parent().next().find("select[name='select_title']").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
	                       }
                           if(!exist){
                        	 	$(target).parent().next().find("span.select2-selection__rendered").html("<span class=\"select2-selection__placeholder\">Select...</span>");
                        	 	$(target).parent().next().find("div[name='show_title']").find("center").html("");
                           }else{
                        	    $(target).parent().next().find("select[name='select_title']").val(titleId);
                           }
	                   }
	               }
	          });            	  
	      });
    	  var customer2 = $("select[name='select_customer']",$rongqi).children("option:selected").text();
    	  $("select[name='select_customer']",$rongqi).prev("div[name='show_customer']").find("center").html(customer2+"&nbsp;&nbsp;");			
// 		});
			
		//初始化title手动选择，隐藏打印时的div
		var html = "<%=title_options%>";
		$("select[name='select_title']",$rongqi).append(html);
		$("div[name='show_title']",$rongqi).css("display","none");
// 		$("select[name='select_title']",$rongqi).each(function(i, elem){
		$("select[name='select_title']",$rongqi).select2({
				 placeholder: "Select..."
		        ,allowClear: true
	      }).bind("change",function(){
	    	  var title = $(this).children("option[value='" + $(this).val() + "']").text();
	    	  $(this).prev("div[name='show_title']").find("center").html(title);
	      });
	  	  var title2 = $("select[name='select_title']",$rongqi).children("option:selected").text();
	  	  $("select[name='select_title']",$rongqi).prev("div[name='show_title']").find("center").html(title2);
// 		});
		
		
		
	}
</script>
<style>
.select2-selection{text-align: left;}
</style>
<body>
	<div>&nbsp;</div>
	<div align="right" style=" border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		Label Name:&nbsp; <select id="lable_template" name="lable_template" onchange="changeLable()" style="width: 200px;">
		<option value=""></option>
			<%for(int i=0;i<rows.length;i++){ %>
				<option value="<%=i%>" <%if(i == 0){%>selected<% } %>> &nbsp;<%=rows[i].getString("lable_name")%>&nbsp;</option>
			<%} %>
		</select>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input class="long-button-print" type="button" onclick="printBarCode()" value="  Print">
	</div>
	<%for(int i=0;i<rows.length;i++){ %>
		<div id="lable_<%=i%>" style="display: none;"><%=rows[i].getString("lable_content")%></div>
		<input type="hidden" id="printName_<%=i%>" name="printName" value="<%=rows[i].get("print_name","")%>" />
	<%} %>
	
	<br/>
	<div align="center" >
		<%if(rows!=null&&rows.length>0){%>
			<div id="rongqi" align="left" style="width:390px;background: #FFF;border:1px solid red;">
				<%=rows[0].getString("lable_content")%>
	    	</div>
	    	<input type="hidden" id="printName" name="printName" value="<%=rows[0].get("print_name","")%>" />
		<% } %>
	    <div id="printAll" style="display: none;"></div>
	 </div>
</body>
</html>
<script>
	//选择不同模板，替换显示的模板
	function changeLable(){
		var count = $("#lable_template").val();
		if(count == ""){
			$("#rongqi").css("display","none");
			return;
		}
		$("#rongqi").css("display","");
		
		var printName = $("#printName_"+count).val();
		$("#printName").val(printName);
		var lable_template = $("#lable_"+count).html();
		initLableTemplate(lable_template);
	}
	
	//打印
	function printBarCode(){
		
		if('' == $("#lable_template").select2("val"))
		{
			showMessage("Select label","alert");
			return false;
		}
		
		 setPrintAll();//修改时间打印显示
		 
		 //获取打印机名字列表
    	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
    	
	    //判断是否有该名字的打印机
    	var printer = $("#printName").val();
//     	var printer = "LabelPrinter";
//   		console.log(printer);
    	var printerExist = "false";
    	var containPrinter = printer;
		for(var i = 0;i<printer_count;i++){
			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 )
			{
				printerExist = "true";
				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
				break;
			}
		}
		if(printerExist=="true"){
				//判断该打印机里是否配置了纸张大小
				var paper="102X152";
			    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
			    var str=strResult.split(",");
			    var status=false;
			    for(var i=0;i<str.length;i++){
                       if(str[i]==paper){
                          status=true;
                       }
				}
			    if(status==true){
                   	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Container Label");
                   	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                     visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
   				     visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",document.getElementById("printAll").innerHTML);
   					 visionariPrinter.SET_PRINT_COPIES(1);
//     				 visionariPrinter.PREVIEW();
   					 visionariPrinter.PRINT();
               }else{
            	   $.artDialog.confirm("Please Change Paper", function(){
        				 this.close();
            			}, function(){
        			});
               }	
		}else{
			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
			if(op!=-1){ //判断是否点了取消
				 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Container Label");
               	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
				 visionariPrinter.ADD_PRINT_HTM(5,3,"97%","100%",document.getElementById("printAll").innerHTML);
   				 visionariPrinter.SET_PRINT_COPIES(1);
// 				 visionariPrinter.PREVIEW();
   				 visionariPrinter.PRINT();
			}	
		}
	}

	//设置打印内容  gql,注意：修改打印内容的date/pcode页面布局时，要修改此处 2015/04/27
	function setPrintAll(){
		var $obj = $("#rongqi");
		var html = $obj.html();//获取打印的内容
		$("#printAll").html(html);//赋值打印的内容
		
		setTlpPrintAll($("#printAll"));//设置tlp打印的样式
	}
	
	
	//安卓打印方法
	function supportAndroidprint(){
		 setPrintAll();//修改时间打印显示
		 
		//获取打印机名字列表
	   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
	   	 //判断是否有该名字的打印机
	   	var printer = "<%=printName%>";
	   	var printerExist = "false";
	   	var containPrinter = printer;
		for(var i = 0;i<printer_count;i++){
			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
				printerExist = "true";
				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
				break;
			}
		}
		if(printerExist=="true"){
			return androidIsPrint(containPrinter);
		}else{
			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
			if(op!=-1){ //判断是否点了取消
				return androidIsPrint(containPrinter);
			}
		}
	}

	function androidIsPrint(containPrinter){
		var $obj = $("div[name='lableContent']");
		var html = $obj.html();//获取打印的内容
		$("#printAll").html(html);//赋值打印的内容
		setClpPrintAll($("#printAll"));//设置clp打印的样式
		
		var flag = true ;

		 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
		 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Container Label");
         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");            
    	 visionariPrinter.ADD_PRINT_TABLE(5,3,"97%","100%",$("#printAll").html());
		 visionariPrinter.SET_PRINT_COPIES(1);
//				 visionariPrinter.PREVIEW();
 		flag = flag && visionariPrinter.PRINT();
		return flag ;
	}
	
	
	
</script>