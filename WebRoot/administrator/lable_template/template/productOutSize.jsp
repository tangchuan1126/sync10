<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ include file="../../../include.jsp"%>
<%
String[] copies = StringUtil.getString(request,"copies").split(",");
String[] pc_id = StringUtil.getString(request,"pc_id").split(",");
String[] counts = StringUtil.getString(request,"counts").split(",");

String purchaseId=StringUtil.getString(request,"purchaseId");
String factoryNum=StringUtil.getString(request,"factoryNum");
String factoryId=StringUtil.getString(request,"factoryId");

//收货仓库
String warehouse=StringUtil.getString(request,"warehouse");

float mm_pix = 3.78f;
float print_range_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"))*mm_pix;//99
float print_range_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"))*mm_pix;//48.8f
float print_range_left = 0*mm_pix;
float print_range_top = 0*mm_pix;

float print_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"));
float print_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"));

String printer = StringUtil.getString(request,"printer");
String paper = StringUtil.getString(request,"paper");

float pnameheight;
float borderWidth = 2;

int printsumCount = 0;
%>

<style>
.container
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height-9%>px;

	position:relative;
	left:<%=print_range_left%>px;
	top:<%=7%>px;
	
	font-size:12px;
	font-weight:normal;
	border:1px red solid;
	font-family:Verdana;
}

#container2
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height%>px;

	position:absolute;
	left:<%=print_range_left%>px;
	top:<%=300f*3.78f%>px;
	clip:rect(0,<%=print_range_width%>,<%=print_range_height%>,0);
	font-size:12px;
	font-weight:normal;
	border:"1px #000000 solid";
	font-family:Verdana;
}

.num
{
	font-family:Arial;
}
.STYLE1 {color: #000000;font-weight:bold;font-size:18px;}

.STYLE2 {color: #000000;font-weight:bold;font-size:16px;}

td
{
	font-size:10px;
}

.print-code
{
	font-family: C39HrP48DmTt;font-size:40px;
}
</style>
<div id="keep_alive" style="display:none"></div>


<% 
	for(int c=0;c<pc_id.length;c++)
	{
		DBRow product = productMgr.getDetailProductByPcid(Long.parseLong(pc_id[c]));
		String[] pname = product.getString("p_name").split("/");
		String[] barcode = product.getString("p_code").split("/");
		
		if(pname.length>2)
		{
			pnameheight = 0.2f;
		}
		else
		{
			pnameheight = 0.4f;
		}
%>
<div id="container_<%=product.get("pc_id",0l) %>" align="center" class="container Noprint">
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	     <td height="5px;"></td>
	  </tr>
	  <tr>
	    <td align="center" valign="bottom">
	        <table width="98%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td height="35px;" align="center" style="border-top:#000 3px solid; border-left:#000 3px solid; border-top:#000 3px solid; border-bottom:#000 1px solid; border-right:#000 1px solid; font-size:15px; font-weight: bold;">
	                <%=pname[0]%>
	            </td>
	            <td height="35px;" align="center" style="border-top:#000 3px solid; border-right:#000 3px solid; border-bottom:#000 1px solid; font-size:15px; font-weight: bold;">
	                <%=pname[1]%>
	            </td>
	          </tr>
	          <tr>
	            <td height="30px;" colspan="2" align="center" style="border-right:#000 3px solid; border-left:#000 3px solid; border-bottom:#000 3px solid; font-size:15px; font-weight: bold;">
	                <table width="100%" height="30px; border="0" cellspacing="0" cellpadding="0">
	                   <tr>
	                      <%
								float tr2char = product.getString("p_name").substring((pname[0]+pname[1]).length()+1).length();
								for(int j = 2;j<pname.length;j++)
								{
									if(j<pname.length-2||j==2)
									{
							%>			
							            <%if(pname.length==3){%>
								            <td align="center" style="font-size:15px; font-weight: bold;" >
												<%=pname[j]%>
											</td>
							            <%}else{%>	
								            <td align="center" style="font-size:15px; font-weight: bold; border-right:#000 1px solid;" >
												<%=pname[j]%>
											</td>
							            <%}%>
							            
							<%	
									}
									else
									{
							%>
							           <%if(j==pname.length-1){ %>
							             <td align="center" style="font-size:15px; font-weight: bold;">
											<%=pname[j]%>
									     </td>
							           <%}else{ %>							     
							             <td align="center" style="border-right:#000 1px solid; font-size:15px; font-weight: bold;" >
											<%=pname[j]%>
									     </td>
									   <%} %>		
							<%
									}
								}
							%>
	                   </tr>
	                </table>
	            </td>
	          </tr>
	        </table>
	    </td>
	  </tr>
	  <tr>
	    <td align="center" valign="middle" height="50px;">
	       <table width="98%" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                      <td align="center" valign="middle" ><img src="/barbecue/barcode?data=<%=product.get("pc_id",0l) %>&width=1&height=35&type=code39" /></td>
	                      <td width="30%" style="font-size:15px;" align="right" valign="middle" ><%=product.get("pc_id",0l) %></td>
	                  </tr>
	       </table>
	    </td>
	  </tr>
	  <tr>
	    <td align="center" >
	        <table width="98%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:#000 3px solid; border-top:#000 3px solid; ">
	          <tr>
	            <td width="50%" height="40px;">
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              <tr>
		              	<td rowspan="2" width="40%" align="right">
		              		<img src="<%=ConfigBean.getStringValue("systenFolder")%>administrator/imgs/print/3.ico"  width="40" height="40" >		        
		              	</td>
		                <td align="left" style="font-size:15px; font-weight: bold;" >外箱签</td>
		              </tr>
		              <tr>
		                <td align="right">Made In China </td>
		              </tr>
		           </table>
	            </td>
	            <td height="40px;"  align="center" style=" border-left: #000 1px solid;">
	                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                      <td align="center" valign="middle" ><img src="/barbecue/barcode?data=<%=counts[c]%>&width=1&height=35&type=code39" /></td>
	                      <td width="20%" style="font-size:15px;" align="left" valign="middle" ><%=counts[c]%></td>
	                  </tr>
	                </table>	             
	            </td>
	          </tr>
	        </table>
	    </td>
	  </tr>
	  <tr>
	    <td align="center">
	        <table width="98%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td>
	               <%if(warehouse==""){ %> 
						     <% if(factoryId.equals("0")&& !purchaseId.equals("0")){ %>
						          P<%=purchaseId %>
						     <%}else if(!factoryId.equals("0")&& purchaseId.equals("0")){ %>
						          <%=factoryId %>
						     <%}else if(!factoryId.equals("0")&& !purchaseId.equals("0") && purchaseId!=""){ %> 		     
						          <%=factoryId%>-P<%=purchaseId%>
						     <%}else{ %>	                      
						     <%} %>  
					<%}else{ %>
					     <%=session.getAttribute("billOfLading")%>-<%=warehouse %>
					<%} %>	
	            </td>
	            <td align="right">
	              <%if(factoryNum.equals("*工厂型号")){%>    
		          <%}else{%>
		        	 <%=factoryNum%>  
		          <%}%> 
	            </td>
	          </tr>
	        </table>
	    </td>
	  </tr>
	</table>
</div>
<%
	}
%>

