<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ include file="../../../include.jsp"%>
<%
String[] copies = StringUtil.getString(request,"copies").split(",");
String[] pc_id = StringUtil.getString(request,"pc_id").split(",");
String[] counts = StringUtil.getString(request,"counts").split(",");

String purchaseId=StringUtil.getString(request,"purchaseId");
String factoryNum=StringUtil.getString(request,"factoryNum");
String factoryId=StringUtil.getString(request,"factoryId");

//收货仓库
String warehouse=StringUtil.getString(request,"warehouse");

float mm_pix = 3.78f;
float print_range_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"))*mm_pix;//99
float print_range_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"))*mm_pix;//48.8f
float print_range_left = 0*mm_pix;
float print_range_top = 0*mm_pix;

float print_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"));
float print_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"));

String printer = StringUtil.getString(request,"printer");
String paper = StringUtil.getString(request,"paper");

float pnameheight;
float borderWidth = 2;

int printsumCount = 0;
%>
<style>
.container
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height-9%>px;

	position:relative;
	left:<%=print_range_left%>px;
	top:<%=7%>px;
	
	font-size:12px;
	font-weight:normal;
	border:1px red solid;
	font-family:Verdana;
}

#container2
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height%>px;

	position:absolute;
	left:<%=print_range_left%>px;
	top:<%=300f*3.78f%>px;
	clip:rect(0,<%=print_range_width%>,<%=print_range_height%>,0);
	font-size:12px;
	font-weight:normal;
	border:"1px #000000 solid";
	font-family:Verdana;
}

.num
{
	font-family:Arial;
}
.STYLE1 {color: #000000;font-weight:bold;font-size:18px;}

.STYLE2 {color: #000000;font-weight:bold;font-size:16px;}

td
{
	font-size:10px;
}

.print-code
{
	font-family: C39HrP48DmTt;font-size:40px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id="keep_alive" style="display:none"></div>


<% 
	for(int c=0;c<pc_id.length;c++)
	{
		DBRow product = productMgr.getDetailProductByPcid(Long.parseLong(pc_id[c]));
		String[] pname = product.getString("p_name").split("/");
		String[] barcode = product.getString("p_code").split("/");
		
		if(pname.length>2)
		{
			pnameheight = 0.2f;
		}
		else
		{
			pnameheight = 0.4f;
		}
%>
<div id="container_<%=product.get("pc_id",0l) %>" align="center" class="container Noprint">
   <table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td  height="85px;" colspan="2" align="center" valign="middle" style=" border-bottom:3px #000 solid; border-top:3px #000 solid;" >
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center" valign="middle">
                     <img src="/barbecue/barcode?data=<%=product.get("pc_id",0l) %>&width=1&height=35&type=code39" />
                  </td>
                  <td align="right" width="18%" valign="middle" style=" font-size:14px; font-weight: bold;" ><%=product.get("pc_id",0l) %></td>
                </tr>
             </table>
             
        </td>
      </tr>
      <tr>
        <td height="60px;" width="50%" align="center" style="border-right:1px #000 solid; border-bottom:3px #000 solid;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td rowspan="2" width="40%" align="right">
		              <img src="<%=ConfigBean.getStringValue("systenFolder")%>administrator/imgs/print/3.ico"  width="40" height="40" >			        
              	  </td>
                  <td align="left" style="font-size:15px; font-weight: bold;" >外箱签</td>
                </tr>
                <tr>
                  <td align="right">Made In China</td>
                </tr>
             </table>
          </td>
        <td height="60px;" align="center" style="border-bottom:3px #000 solid; ">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center" valign="middle">
                     <img src="/barbecue/barcode?data=<%=counts[c]%>&width=1&height=35&type=code39" />
                  </td>
                  <td align="center" width="15%" valign="middle" style=" font-size:15px; font-weight: bold;">
                     <%=counts[c]%>
                  </td>
                </tr>
             </table>
           
        </td>
      </tr>
      <tr>
        <td>
             <%if(warehouse==""){ %> 
			     <% if(factoryId.equals("0")&& !purchaseId.equals("0")){ %>
			          P<%=purchaseId %>
			     <%}else if(!factoryId.equals("0")&& purchaseId.equals("0")){ %>
			          <%=factoryId %>
			     <%}else if(!factoryId.equals("0")&& !purchaseId.equals("0") && purchaseId!=""){ %> 		     
			          <%=factoryId%>-P<%=purchaseId%>
			     <%}else{ %>	                      
			     <%} %>  
		<%}else{ %>
		     <%=session.getAttribute("billOfLading")%>-<%=warehouse %>
		<%} %>
        </td>
        <td align="right">
            <%if(factoryNum.equals("*工厂型号")){%>    
            <%}else{%>
        	   <%=factoryNum%>  
            <%}%> 
        </td>
      </tr>
    </table>
</div>
<%
	}
%>

