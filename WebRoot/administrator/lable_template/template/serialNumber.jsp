<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@page import="com.cwc.initconf.Resource"%>
<%@ include file="../../../include.jsp"%>
<%
	long purchase_id = StringUtil.getLong(request,"purchaseId");
	String pcids = StringUtil.getString(request,"pcids");//商品ID集合
	String pccounts = StringUtil.getString(request,"pccounts");//商品打印数量
			
	DBRow purchase  = purchaseMgr.getDetailPurchaseByPurchaseid(String.valueOf(purchase_id));
	long supplier_id = Long.parseLong(purchase.getString("supplier"));

	//float print_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"));
	//float print_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript">
	var isbreak = false;//若发生错误则终止循环
	var pc_id;//为ajax传pc_id特意定义
	function printSerialNumber()
	{
		$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
		var pcids = "<%=pcids%>";
		var pcid = pcids.split(",");
		var pccounts = "<%=pccounts%>"
		var pccount = pccounts.split(",");
		
		for(i = 0;i<pcid.length;i++)//种类循环
		{
			if(isbreak)//有错误终止循环，此处为商品种类循环
			{
				alert("wai")
				break;
			}
			pc_id = pcid[i];
			var para;
			for(j = 1;j<parseInt(pccount[i])+1;j++)//数量循环
			{
				para = "pc_id="+pcid[i]+"&supplier_id=<%=supplier_id%>";
				
				if(isbreak)//有错误，终止循环，此处为商品数量循环
				{
					alert(isbreak+"--------nei");
					break;
				}
				para += "&needcount="+(parseInt(pccount[i])+1-j);
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/serialNmuber/madeSerialNumber.action',
					type: 'post',
					async:false,
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(data)
					{
						isbreak = true;
					},
					
					success: function(data)
					{
						if(data.result == "success")
						{
							var serial_numbers = data.serial_number.split(",");
							
								visionariPrinter.PRINT_INITA(0,0,"100mm","15mm","serialNumber");
								visionariPrinter.SET_PRINT_PAGESIZE(1,"100mm","15mm","45X15");
								visionariPrinter.SET_PRINT_STYLE("FontSize",9);
								visionariPrinter.ADD_PRINT_TEXT(0,"6mm","3mm","10mm","SN");
								visionariPrinter.SET_PRINT_STYLE("FontSize",6);
								visionariPrinter.ADD_PRINT_BARCODE(0,"9mm","36mm","10mm","Codabar",serial_numbers[0]);
								if(serial_numbers.length==2)
								{
									visionariPrinter.SET_PRINT_STYLE("FontSize",9);
									visionariPrinter.ADD_PRINT_TEXT(0,"58mm","3mm","10mm","SN");
									visionariPrinter.SET_PRINT_STYLE("FontSize",6);
									visionariPrinter.ADD_PRINT_BARCODE(0,"61mm","36mm","10mm","Codabar",serial_numbers[1]);
								}
								visionariPrinter.SET_PRINT_COPIES(1);
								
								if(visionariPrinter.PRINT())
								{
									var parasave = "serial_number="+data.serial_number+"&supplier_id=<%=supplier_id%>&pc_id="+pc_id+"&purchase_id=<%=purchase_id%>";
									$.ajax({
												url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/serialNmuber/saveSerialNumber.action',
												type: 'post',
												dataType: 'json',
												async:false,
												timeout: 60000,
												cache:false,
												data:parasave,
												beforeSend:function(request){
													},
													
												error: function(data){
													isbreak = true;
												},
													
												success: function(data)
												{
													if(data.result == "success")
													{
															
													}
													else if(data.result == "error")
													{
														isbreak = true;
														$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green"><%=Resource.getStringValue("","NumberIndexOfExption","")%></span>' });
													}
													else
													{
														isbreak = true;
													}
												}
											});
										j++;
								}
//							if(j%2==0)//第偶数个
//							{
//								visionariPrinter.SET_PRINT_STYLE("FontSize",9);
//								visionariPrinter.ADD_PRINT_TEXT(0,"43mm","3mm","10mm","SN");
//								visionariPrinter.SET_PRINT_STYLE("FontSize",6);
//								visionariPrinter.ADD_PRINT_BARCODE(0,"46mm","36mm","10mm","Codabar",data.serial_number);
//								visionariPrinter.SET_PRINT_COPIES(1);
//								if(visionariPrinter.PRINT()==false)
//								{
//								
//								}
//							}
//							else if(j<parseInt(pccount[i])-1)//没打印完全
//							{
//								visionariPrinter.PRINT_INITA(0,0,"100mm","50mm","serialNumber");
//								visionariPrinter.SET_PRINT_PAGESIZE(1,"100mm","50mm","40X15");
//								visionariPrinter.SET_PRINT_STYLE("FontSize",9);
//								visionariPrinter.ADD_PRINT_TEXT(0,0,"3mm","10mm","SN");
//								visionariPrinter.SET_PRINT_STYLE("FontSize",6);
//								visionariPrinter.ADD_PRINT_BARCODE(0,"3mm","36mm","10mm","Codabar",data.serial_number);	
//							}
//							else//打印最后一个
//							{
//								visionariPrinter.PRINT_INITA(0,0,"100mm","50mm","serialNumber");
//								visionariPrinter.SET_PRINT_PAGESIZE(1,"100mm","50mm","40X15");
//								visionariPrinter.SET_PRINT_STYLE("FontSize",9);
//								visionariPrinter.ADD_PRINT_TEXT(0,0,"3mm","10mm","SN");
//								visionariPrinter.SET_PRINT_STYLE("FontSize",6);
//								visionariPrinter.ADD_PRINT_BARCODE(0,"3mm","36mm","10mm","Codabar",data.serial_number);
//								visionariPrinter.SET_PRINT_COPIES(1);
//								visionariPrinter.PRINT();
//							}
			
							
						}
						else if(data.result == "error")
						{
							isbreak = true;
							$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green"><%=Resource.getStringValue("","NumberIndexOfExption","")%></span>' });
						}
						else
						{
							isbreak = true;
						}
					}
				});			
			}
		}
		
		//循环结束后操作
		if(isbreak)//有错误终止的循环
		{
			$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green"><%=Resource.getStringValue("","NumberIndexOfExption","")%></span>' });
		}
		else
		{
			parent.closeWin();
		}
	}
</script>
</head>
<body onload="printSerialNumber()">
</body>
</html>

