<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.LableTemplateKey"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../../comm.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="../../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../../js/print/m.js"></script>

<% 
int fromNum = StringUtil.getInt(request,"fromNum");
int toNum=StringUtil.getInt(request,"toNum");
int count=StringUtil.getInt(request,"count");

String transportCode=StringUtil.getString(request,"transportCode");
String transportCompany=StringUtil.getString(request,"transportCompany");
String address=StringUtil.getString(request,"address");
String awb=StringUtil.getString(request,"awb");
String deliver_house_number=StringUtil.getString(request,"deliver_house_number");
String deliver_street=StringUtil.getString(request,"deliver_street");

String deliver_city=StringUtil.getString(request,"deliver_city");
String deliver_zip_code=StringUtil.getString(request,"deliver_zip_code");
String deliver_pro_input=StringUtil.getString(request,"address_state_input");
String ccid_hidden=StringUtil.getString(request,"ccid_hidden");

String deliver_contact=StringUtil.getString(request,"deliver_contact");
String deliver_phone=StringUtil.getString(request,"deliver_phone");

String address_state=StringUtil.getString(request,"address_state");




//String[] items=request.getParameterValues("items");

%>




<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>唛头签</title>
</head>
<body>
<div  align="center" ><input type="button" class="long-button-print" value="打印" onclick="print_shipping_mark()"/></div>
<div style="border-bottom:1px #000 dashed;">&nbsp;</div>
<br/>
<div align="center" style="width:450px;">
<!-- 大的容器 -->
  <% for(int i=fromNum;i<=toNum;i++){ %>
  <div style=" width:400px; height:220px; border:1px red solid;" id="container" name="container">
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="190px;" width="" style="border-right:2px #000 solid;" align="left" valign="top" >
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	              <% if(i>99){ %>
	                 <td height="190px;" align="center" style="font-size:120px; font-weight: bolder; border-bottom:2px #000 solid;">
	                   <%= i %>
	                </td>
	              <% }else{ %>
	                <td height="190px;" align="center" style="font-size:150px; font-weight: bolder; border-bottom:2px #000 solid;">
	                   <%= i %>
	                </td>
	              <%} %>
	              </tr>
	              <tr>	  
	                 <td>      
	                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="10%">&nbsp;</td>
                            <td style=" font-size:18px; font-weight: bolder;" >Total:<%= count %></td>
                          </tr>
                        </table>
                     </td> 
	              </tr>
	            </table>
            </td>
            <td height="190px;" align="left" width="160px;" valign="top" >
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="160px;" style=" border-bottom:1px #000 solid;" colspan="2"> 
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">                 
                          <tr>
						    <td width="5px;" align="right" height="17px;" style="font-weight: bolder;"></td>
						    <td style="font-size:12px;" width="140px;"><%=deliver_contact %></td>
						  </tr>
						  <tr>
						    <td align="right" height="17px;" style="font-weight: bolder;"></td>
						    <td style="font-size:12px;"><%=deliver_phone %></td>
						  </tr>
						  <tr>
						     <td height="5px;">&nbsp;</td>
						     <td height="5px;">&nbsp;</td>
						  </tr>
						  <tr>
						    <td align="right" height="17px;" style="font-weight: bolder;"></td>
						    <td style="font-size:12px; word-break:break-all; word-wrap:break-all;"><%=deliver_house_number %></td>
						  </tr>
						  <tr>
						    <td align="right" height="17px;" style="font-weight: bolder;"></td>
						    <td style="font-size:12px; word-break:break-all; word-wrap:break-all;"><%=deliver_street %></td>
						  </tr>
						  <tr>
						    <td align="right" height="17px;" style="font-weight: bolder;"></td>
						    <td style="font-size:12px;"><%=deliver_city %></td>
						  </tr>
						  <tr>
						    <td align="right" height="17px;" style="font-weight: bolder;"></td>
						    <td style="font-size:12px;"><%=deliver_zip_code %></td>
						  </tr>
						  <tr>
						    <td align="right" height="17px;" style="font-weight: bolder;"></td>
						    <td style="font-size:12px;">
						         <%if(address_state.equals("手工输入")){ %>
						           <%=deliver_pro_input %>
						           <%}else{ %>
						           <%=address_state%>
						         <%} %>
						    </td>
						  </tr>
						  <tr>
						    <td align="right" height="17px;" style="font-weight: bolder;"></td>
						    <td style="font-size:12px;"><%=ccid_hidden %></td>
						  </tr>
						</table>          
                    </td>              
                  </tr>
                  <tr>
                    <td colspan="2" align="left" >
				       <table width="100%" border="0" cellspacing="0" cellpadding="0">
				          <tr>
				            <td width="6%"></td>
				            <td width="50px;" style="font-weight: bolder; font-size:12px;">AWB：</td>
				            <td style="font-size:12px;"><%=awb %></td>
				          </tr>
				        </table>
				    </td>          
                  </tr>
                  <tr>
                    <td style="font-weight: bolder; font-size:12px;" height="17px;" width="45%" align="right">转运单号：</td>
                     <td style="font-size:12px;"><%= transportCode %></td>
                  </tr>           
                  <tr>
                    <td style="font-weight: bolder; font-size:12px;" height="17px;" width="45%" align="right">运输公司：</td>
                     <td style="font-size:12px;"><%= transportCompany%></td>
                  </tr>         
               </table>
            </td>
          </tr>
        </table>
 </div>
 <br/>
<% } %>
</div>
<script type="text/javascript">
    function print_shipping_mark(){
      var con=$("div[name='container']");
  	var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
	  if(op!=-1){ //判断是否点了取消
	      for(var i=0;i<con.length;i++){
	         //alert($(con[i]).html());
	   	     visionariPrinter.PRINT_INITA(0,0,"100mm","60mm","商品标签");
	         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"100X60");            
	    	 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%","<body leftmargin='2' topmargin='1'>"+$(con[i]).html()+"</body>");
			 visionariPrinter.SET_PRINT_COPIES(1);
			 //visionariPrinter.PREVIEW();
			 visionariPrinter.PRINT();
	      }
	  }
    }
</script>

</body>
</html>