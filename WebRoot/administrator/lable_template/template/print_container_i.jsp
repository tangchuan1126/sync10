<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="com.cwc.app.key.ContainerTypeKey" %>
<%@ include file="../../../include.jsp"%>
<html>
<head>
<title>打印</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>


<!-- 打印 -->
<script type="text/javascript" src="../../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../../js/print/m.js"></script>
</head>

<%
	long id=StringUtil.getLong(request,"id");
	DBRow row=clpTypeMgrZr.findContainerById(id);
	DBRow ilp=clpTypeMgrZr.findIlpTypeProductByIlpTypeId(row.get("type_id",0l));
	//DBRow product=clpTypeMgrZr.findContainerProduct(ilp.get("ibt_pc_id",0l));
	
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	
%>
<script>
	
</script>
<body>
	<div>&nbsp;</div>
	<div align="right" style=" border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<input class="long-button-print" type="button" onclick="printBarCode()" value="打印">
	</div>
	<br/>
	<div align="center" >
		<div id="rongqi" align="left" style="width:368px; border:1px solid red">
		 	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
			      <tr>
			        <td width="36%" rowspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;" >
			        	<span style="font-size:100px; font-family: Verdana, Geneva, sans-serif;">
			            	<span style="font-size:100px; font-family: Verdana, Geneva, sans-serif;">		                 
			                       I    
						    </span>
			            </span>
			        </td>
			        <td height="36"  colspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;" >
			        	<span style="font-weight: bold;">Date</span>
			        </td>
			        <td width="42%" align="center" style="border-bottom:2px solid black;" >
			        	<span style="font-weight: bold; font-size:18px" id="dateShow"></span>
			        </td>
			      </tr>
			      <tr>
			        <td height="58" colspan="3" style="border-bottom:2px solid black;" >
			      	    <div style="padding-left:8px;font-size: 12px;">Container Type Id</div>
			            <div style="font-weight: bold;font-size:18px; height:50px; line-height:50px;" align="center" >
			               <%=ilp.get("ibt_id",0l) %>
			            </div>				           
			        </td>
			      </tr>
			      <tr>
			        <td height="43" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
				        <div style="padding-left:8px; font-size: 12px;">Pid</div>
				        <div style="font-weight: bold;font-size:18px;" align="center"><%=ilp.get("ibt_pc_id",0l)%></div>			    
			        </td>
			        <td colspan="2" style="border-bottom:2px solid black;" >
				        <div style="padding-left:8px;font-size: 12px;">Product Name</div>
				        <div style="font-weight: bold;font-size:18px;" align="center"><%=ilp.getString("p_name") %>&nbsp;</div>
			        </td>
			      </tr>
			      <tr>
			        <td height="49" colspan="4" style="border-bottom:2px solid black;" >
			        	<div align="left" style="padding-left:8px;font-size: 12px;">Lot</div>
			        	<div align="center" style="font-size:18px;font-weight: bold;"><%=row.getString("lot_number") %>&nbsp;</div>
			        </td>
			      </tr>
			      <tr>
			        <td height="51" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
			        	<div style="padding-left:8px;font-size: 12px;">Customer</div>
				        <div style="font-weight:bold;font-size:18px;" align="center" >
				       		&nbsp;
				        </div>
			        </td>
			        <td colspan="2" style="border-bottom:2px solid black;">			 
				        <div style="padding-left:8px;font-size: 12px;">Supplier</div>
				        <div style="font-weight:bold;" align="center" >
				        	 <%DBRow titleRow=proprietaryMgrZyj.findProprietaryByTitleId(row.get("title_id",0l)); %>
						        <%if(titleRow!=null){ %>
									<%= titleRow.getString("title_name")%>
								<%}else{ %>
									&nbsp;
								<%} %>
				        </div>
			        </td>
			      </tr>
			      <tr>
			        <td height="50" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
			      		<div style="padding-left:8px;font-size: 12px;">Total-Products</div>
			        	<div align="center" style="font-size:18px;font-weight: bold;" >
			        		<%=ilp.get("ibt_total",0l) %>&nbsp;
			        	</div>
			        </td>
			        <td colspan="2" style="border-bottom:2px solid black;">
                       <div style="padding-left:8px;font-size: 12px;">Total-Packages</div>
                       <div align="center" style="font-size:18px;font-weight: bold;" >
					   		<%=ilp.get("ibt_total",0l) %>&nbsp;
                       </div>
			        </td>
			      </tr>
			      <tr>
			        <td height="49" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
                       <div style="padding-left:8px;font-size: 12px;">Piece</div>
                       <div align="center" style="font-size:18px;font-weight: bold;" >
					   		Product
                       </div>  
			        </td>
			        <td colspan="2" style="border-bottom:2px solid black;">                 
                        <div style="padding-left:8px;font-size: 12px;">Pakcages</div>
                        <div align="center" style="font-size:18px;font-weight: bold;" >
					   		<%=ilp.get("ibt_length",0l) %>X<%=ilp.get("ibt_width",0l) %>X<%=ilp.get("ibt_height",0l) %>
                        </div>

			        </td>
			      </tr>		
			      <tr>
			        <td height="83" colspan="4">
			        	<div align="center">
				        	<img src="/barbecue/barcode?data=<%=row.getString("container") %>&width=1&height=40&type=code39&drawText=true&st=true" />
				        </div>
				        <div align="center">	
				        	<%=row.getString("container") %>
				        </div>
			        </td>
			      </tr>
			      <tr>
			        <td colspan="2">&nbsp;</td>
			        <td width="13%">&nbsp;</td>
			        <td>&nbsp;</td>
			      </tr>
			</table>
	    </div>	
	</div>
</body>
</html>
<script>
	function printBarCode(){
		 //获取打印机名字列表
    	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
    	
	    //判断是否有该名字的打印机
    	var printer = "LabelPrinter";
  
    	var printerExist = "false";
    	var containPrinter = printer;
		for(var i = 0;i<printer_count;i++){
			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 )
			{
				printerExist = "true";
				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
				break;
			}
		}
		if(printerExist=="true"){
				//判断该打印机里是否配置了纸张大小
				var paper="102X152";
			    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
			    var str=strResult.split(",");
			    var status=false;
			    for(var i=0;i<str.length;i++){
                       if(str[i]==paper){
                          status=true;
                       }
				}
			    if(status==true){
                   	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","商品标签");
                   	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                     visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
	   				     visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",document.getElementById("rongqi").innerHTML);
	   					 visionariPrinter.SET_PRINT_COPIES(1);
	   				     //visionariPrinter.PREVIEW();
	   					 visionariPrinter.PRINT();
               }else{
               	$.artDialog.confirm("打印机配置纸张大小里没有符合的大小.....", function(){
        				 this.close();
            			}, function(){
        			});
               }	
		}else{
			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
			if(op!=-1){ //判断是否点了取消
				 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","商品标签");
               	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
				 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",document.getElementById("rongqi").innerHTML);
   				 visionariPrinter.SET_PRINT_COPIES(1);
   				 //visionariPrinter.PREVIEW();
   				 visionariPrinter.PRINT();
			}	
		}
	}

	(function(){
		var now = new Date(); 
        var year = now.getFullYear();       //年
        var month = now.getMonth() + 1;     //月
        var day = now.getDate();            //日
        var hh = now.getHours();            //时
        var mm = now.getMinutes();          //分
        var clock = year + "-";
        if(month < 10)
            clock += "0";
        clock += month + "-";
        if(day < 10)
            clock += "0";
        clock += day + " ";
  //      if(hh < 10)
  //          clock += "0";
  //      clock += hh + ":";
  //      if (mm < 10) clock += '0'; 
  //      clock += mm;   
        $('#dateShow').html(clock);
	})();
</script>