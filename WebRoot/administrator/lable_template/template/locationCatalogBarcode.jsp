<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ include file="../../../include.jsp"%>
<%
long slc_id = StringUtil.getLong(request,"slc_id");



float mm_pix = 3.78f;
float print_range_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"))*mm_pix;//99
float print_range_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"))*mm_pix;//48.8f
float print_range_left = 0*mm_pix;
float print_range_top = 0*mm_pix;

float print_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"));
float print_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"));

String printer = StringUtil.getString(request,"printer");
String paper = StringUtil.getString(request,"paper");

float borderWidth = 2;

int printsumCount = 1;

DBRow locationCatalog = locationMgrZJ.getDetailLocationCatalogById(slc_id);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>打印位置条码</title>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<div id="container" align="center" class="container Noprint">
	<table width="<%=print_range_width%>px" height="<%=print_range_height-5%>" border="0" bordercolor="#000000" cellpadding="0" cellspacing="0" style="font-family:Arial Black">
		<tr>
			<td align="center" valign="middle">
				<table width="98%" height="98%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" style="border-right:2px #000000 solid;font-size:65px;font-weight:bold;" height="80%"><%=locationCatalog.getString("slc_type")%></td>
						<td align="center" style="border-bottom:2px #000000 solid;border-bottom:0;font-size:36px;" width="80%"><%=locationCatalog.getString("slc_position")%></td>
					</tr>
					<tr>
						<td align="center" style="border-top:2px #000000 solid;border-right:2px #000000 solid;font-size:18px;font-weight:bold;"><%=locationCatalog.getString("area_name")%></td>
						<td align="center" valign="top"><img src="/barbecue/barcode?data=LL<%=locationCatalog.getString("slc_position_all")%>&width=1&height=35&type=code39" /></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
</body>
</html>
