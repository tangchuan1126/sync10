<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ include file="../../../include.jsp"%>
<%
String[] copies = StringUtil.getString(request,"copies").split(",");
String[] pc_id = StringUtil.getString(request,"pc_id").split(",");

String purchaseId=StringUtil.getString(request,"purchaseId");
String factoryNum=StringUtil.getString(request,"factoryNum");
String factoryId=StringUtil.getString(request,"factoryId");

//收货仓库
String warehouse=StringUtil.getString(request,"warehouse");

float mm_pix = 3.78f;
float print_range_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"))*mm_pix;//99
float print_range_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"))*mm_pix;//48.8f
float print_range_left = 0*mm_pix;
float print_range_top = 0*mm_pix;

float print_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"));
float print_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"));

String printer = StringUtil.getString(request,"printer");
String paper = StringUtil.getString(request,"paper");

float pnameheight;
float borderWidth = 2;

int printsumCount = 0;
%>
<style stype="text/css">
@font-face { font-family: dreamy; font-weight: bold; src: url(http://www.example.com/font.eot);
</style>
<script>
(function(){
	//判断供应商id的字体大小 ,工厂型号字体大小
	<% if(factoryNum.length()<21){ %>
		 $('#warefont').css('font-size','12px');
		 $('#factoryfont').css('font-size','12px');
	<%}else if(factoryNum.length()>=21 && factoryNum.length()<25){ %>			      
		 $('#warefont').css('font-size','11px');
		 $('#factoryfont').css('font-size','11px');
	<%}else if(factoryNum.length()>=25 && factoryNum.length()<27){ %>
		 $('#warefont').css('font-size','10px');
		 $('#factoryfont').css('font-size','10px');
	<%}else if(factoryNum.length()>=27&& factoryNum.length()<34){ %>
		 $('#warefont').css('font-size','9px');
		 $('#factoryfont').css('font-size','9px');
	<%}else if(factoryNum.length()>=34 && factoryNum.length()<41){ %>
		 $('#warefont').css('font-size','8px');
		 $('#factoryfont').css('font-size','8px');
	<%}else if(factoryNum.length()>=41 && factoryNum.length()<45){ %>
		 $('#warefont').css('font-size','7px');
		 $('#factoryfont').css('font-size','7px');
	<%}else if(factoryNum.length()>=45){ %>
		 $('#warefont').css('font-size','6px');
		 $('#factoryfont').css('font-size','6px');
	<%}else{ %>
		 $('#warefont').css('font-size','8px');
		 $('#factoryfont').css('font-size','8px');
	<%} %>
})();	
</script>

<style>
.container
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height%>px;

	position:relative;
	left:<%=print_range_left%>px;
	top:<%=print_range_top%>px;
	
	font-size:<%=print_width/5.8%>px;
	font-weight:normal;
	border:1px red solid;
	font-family:Arial Black;
}

#container2
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height%>px;

	position:absolute;
	left:<%=print_range_left%>px;
	top:<%=300f*3.78f%>px;
	
	font-size:<%=print_width/6.5%>px;
	font-weight:normal;
	border:"1px #000000 solid";
	font-family:Arial Black;
}

.num
{
	font-family:Arial;
}
.STYLE1 {color: #000000;font-weight:bold;font-size:14px;}

.STYLE2 {color: #000000;font-weight:bold;font-size:12px;}

td
{
	font-size:10px;
}

.print-code
{
	font-family: C39HrP48DmTt;font-size:40px;
}
</style>
<div id="keep_alive" style="display:none"></div>
<% 
	for(int c=0;c<pc_id.length;c++)
	{
		DBRow product = productMgr.getDetailProductByPcid(Long.parseLong(pc_id[c]));
		String[] pname = product.getString("p_name").split("/");
		String[] barcode = product.getString("p_code").split("/");
		
		if(pname.length>2)
		{
			pnameheight = 0.25f;
		}
		else
		{
			pnameheight = 0.5f;
		}
%>
<div id="container_<%=product.get("pc_id",0l) %>" align="center" class="container Noprint">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td height="23px;">&nbsp;</td>
	    <td align="left" style="font-size:10px;font-weight: bold;" height="23px;" valign="bottom" >Visionari LLC</td>
	    <td align="right" style="font-size:10px;font-weight: bold;" height="23px;" valign="bottom" >Made In China</td>
	    <td height="23px;">&nbsp;</td>   
	  </tr>
	  <tr>
	    <td height="20px">&nbsp;</td>
		    <% if(product.getString("p_name").length()<=15){ %>
		       <td height="20px" align="left" valign="bottom" style="font-size:18px;font-weight: bold;"><%=product.getString("p_name")%></td>
		    <%}else if(product.getString("p_name").length()>15 && product.getString("p_name").length()<=21){ %>
		         <% int num= product.getString("p_name").length()-15 ;%>
		         <% int size=18-num; %>
		      <td height="20px" align="left" valign="bottom" style="font-size:<%=size%>px; font-weight: bold;"><%=product.getString("p_name")%></td>
		    <%}else if(product.getString("p_name").length()>21 && product.getString("p_name").length()<24){ %>
		      <td height="20px" align="left" valign="bottom" style="font-size:12px; font-weight: bold;"><%=product.getString("p_name")%></td>
		    <%}else if(product.getString("p_name").length()>23 && product.getString("p_name").length()<27){ %>
		      <td height="20px" align="left" valign="bottom" style="font-size:10px; font-weight: bold;"><%=product.getString("p_name")%></td>
		      <%}else if(product.getString("p_name").length()>=27 && product.getString("p_name").length()<31){ %>
		      <td height="20px" align="left" valign="bottom" style="font-size:9px; font-weight: bold;"><%=product.getString("p_name")%></td>
		    <%}else if(product.getString("p_name").length()>36 && product.getString("p_name").length()<40){ %>
		      <td height="20px" align="left" valign="bottom" style="font-size:7px; font-weight: bold;"><%=product.getString("p_name")%></td>
		   <%}else if(product.getString("p_name").length()>40){ %>
		      <td height="20px" align="left" valign="bottom" style="font-size:6px; font-weight: bold;"><%=product.getString("p_name")%></td>
		    <%}else{ %>
		      <td height="20px" align="left" valign="bottom" style="font-size:8px; font-weight: bold;"><%=product.getString("p_name")%></td>
		      <%} %>
	    <td height="20px" align="right" valign="bottom" style="font-size:13px;font-weight: bold;"><%=product.get("pc_id",0l) %></td>
	    <td height="20px">&nbsp;</td>
	  </tr>
	  <tr>
	    <td  colspan="4" style="border-bottom:#000 2px solid; border-top:#000 2px solid;" align="center" valign="middle" height="55px;">                                                               
			    <img src="/barbecue/barcode?data=<%=product.get("pc_id",0l) %>&width=1&height=35&type=code39" />
			   <!--  <img src="/barbecue/barcode?data=<%=product.getString("p_code")%>&width=1&height=35&type=code39" />   -->
	    </td>
	  </tr>
	  <tr>
	     <td colspan="4">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td>&nbsp;</td>
	            <td id="warefont"  align="left" valign="middle" height="22px;" style="font-size:11px;">
		           <%if(warehouse==""){ %> 
						     <% if(factoryId.equals("0")&& !purchaseId.equals("0")){ %>
						          P<%=purchaseId %>
						     <%}else if(!factoryId.equals("0")&& purchaseId.equals("0")){ %>
						          <%=factoryId %>
						     <%}else if(!factoryId.equals("0")&& !purchaseId.equals("0") && purchaseId!=""){ %> 		     
						          <%=factoryId%>-P<%=purchaseId%>
						     <%}else{ %>	                      
						     <%} %>  
					<%}else{ %>
					     <%=session.getAttribute("billOfLading")%>-<%=warehouse %>
					<%} %>	
	            </td>
	             <%if(factoryNum.equals("*工厂型号")){%> 
	             	<td  id="factoryfont" align="right" valign="middle" height="22px;" style="font-size:11px;"></td>   
		         <%}else{%>
		         	<td  id="factoryfont" align="right" valign="middle" height="22px;" style="font-size:11px;"><%=factoryNum%></td> 		     
		         <%}%> 
	            <td>&nbsp;</td>
	          </tr>
	        </table>
	      </td>
	  </tr>
	</table>
</div>
<%
	}
%>
