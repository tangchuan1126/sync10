<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ include file="../../../include.jsp"%>
<%
long aid = StringUtil.getLong(request,"aid");
DBRow asset = assetsMgr.getDetailAssets(aid);



long acatalogid = asset.get("category_id",0l); 
DBRow assetCatalog = assetsCategoryMgr.getDetailAssetsCategory(acatalogid);


float mm_pix = 3.78f;
float print_range_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"))*mm_pix;//99
float print_range_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"))*mm_pix;//48.8f
float print_range_left = 0*mm_pix;
float print_range_top = 0*mm_pix;

float print_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"));
float print_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"));

String printer = StringUtil.getString(request,"printer");
String paper = StringUtil.getString(request,"paper");

float pnameheight = 0.25f;
float borderWidth = 2;

int printsumCount = 0;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>打印资产条码</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
		<script type="text/javascript" src="../js/print/m.js"></script>
		
<script>

function initPrint()
{

}

function printBarCode()
{
		var pcids = "<%=StringUtil.getString(request,"pc_id")%>";
		var pcid = pcids.split(",");
		var copies = "<%=StringUtil.getString(request,"copies")%>";
		var copie = copies.split(",");
		
		var printer = visionariPrinter.SELECT_PRINTER();
		if(printer !=-1)
		{
			visionariPrinter.PRINT_INIT(0,0,"<%=print_width%>mm","<%=print_height%>mm","资产标签");
			//visionariPrinter.SET_PRINT_PAGESIZE(0,"<%=print_width%>mm","<%=print_height%>mm","<%=paper%>");
	
			visionariPrinter.ADD_PRINT_LINE(0,0,0,0,0,0);
			visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%","<body leftmargin='0' topmargin='0'>"+document.getElementById("container").innerHTML+"</body>");
			visionariPrinter.SET_PRINT_COPIES(1);
			visionariPrinter.PRINT();
			
			window.close();
		}
}
</script>

<style>

.container
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height%>px;

	position:absolute;
	left:<%=print_range_left%>px;
	top:<%=print_range_top%>px;

	font-size:12px;
	font-weight:normal;
	border:1px red solid;
	font-family:Verdana;
}

#container2
{
	width:<%=print_range_width%>px;
	height:<%=print_range_height%>px;

	position:absolute;
	left:<%=print_range_left%>px;
	top:<%=300f*3.78f%>px;
	clip:rect(0,<%=print_range_width%>,<%=print_range_height%>,0);
	font-size:12px;
	font-weight:normal;
	border:"1px #000000 solid";
	font-family:Verdana;
}

.num
{
	font-family:Arial;
}
.STYLE1 {color: #000000;font-weight:bold;font-size:11px;}

.STYLE2 {color: #000000;font-weight:bold;font-size:16px;}

td
{
	font-size:10px;
}

.print-code
{
	font-family: C39HrP48DmTt;font-size:38px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id="keep_alive" style="display:none"></div>
	<div id="container" align="center" class="container Noprint">
<table width="<%=print_range_width-2*borderWidth%>px" height="<%=print_range_height-5%>" border="0" bordercolor="#000000" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" width="50%" style="font-size: 18px;font-family: Arial Black">
				Visioari LLC
			</td>
			<td align="center" valign="middle">
				<span style="font-family: C39HrP48DmTt;font-size:38px;">*<%=asset.getString("aid")%>*</span>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" valign="middle">
			<table width="<%=print_range_width-borderWidth*2%>px" height="<%=(print_range_height-3)*pnameheight%>px" style="border:<%=borderWidth%>px #000000 solid;" cellpadding="0" cellspacing="0">
				<tr>
					
						<td class="STYLE1" width="<%=asset.getString("a_name").length()/(asset.getString("a_name").length()+assetCatalog.getString("chName").length())%>%" align="center" valign="middle" style="border-right: 1px #000000 solid;">
						<%=asset.getString("a_name")%>
						</td>
						<td align="center" valign="middle" width="<%=assetCatalog.getString("chName").length()/(asset.getString("a_name").length()+assetCatalog.getString("chName").length())%>%" class="STYLE1"><%=assetCatalog.getString("chName")%></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align="center" valign="middle" colspan="2">
			<table width="<%=print_range_width-borderWidth*2%>px" height="<%=(print_range_height-3)*0.4%>px" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" valign="middle">
						<span style="font-family: C39HrP48DmTt;font-size:38px;">*<%=asset.getString("a_barcode")%>*</span>
					</td>
				</tr>
			</table>
			</td>
		</tr>
</table>
</div>

<script type="text/javascript">
	createContainer("container",3,15,<%=print_width%>,<%=print_height%>);
</script>
	
<table id="title" class="Noprint" style="height: 40px;" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr>
    <td style="background: none repeat scroll 0% 0% rgb(255, 255, 204); padding: 10px; font-size: 15px; font-weight: bold; border-bottom: 1px solid rgb(153, 153, 153); width: 50%;" align="left">
		<input type="button" value="打印条码" onClick="printBarCode()"/>
	</td>
    <td style="background: none repeat scroll 0% 0% rgb(255, 255, 204); padding: 10px; font-size: 13px; font-weight: bold; border-bottom: 1px solid rgb(153, 153, 153); width: 50%;" align="right">
	 <span style="font-family: Arial,Helvetica,sans-serif;"></span>
	&nbsp;&nbsp;
	</td>
  </tr>
</tbody></table>
<script type="text/javascript">
	setComponentPos("title",0,0);
</script>
</body>
</html>
