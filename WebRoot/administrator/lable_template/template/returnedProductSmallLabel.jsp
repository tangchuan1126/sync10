<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ include file="../../../include.jsp"%>

<%
	//商品id
	long pc_id = StringUtil.getLong(request,"pc_id");		//商品Id
	String p_name = StringUtil.getString(request,"p_name");			//商品名称
	String fix_rpi_id = StringUtil.getString(request,"fix_rpi_id") ; //明细Id
	long  rp_id = StringUtil.getLong(request,"rp_id") ; //主单据ID
	boolean isRelation = pc_id != 0l ;
%>

<div  >
	<%
		if(fix_rpi_id.trim().length() > 0 ){
			String[] array = fix_rpi_id.split(",");
			for(String tempFixRpiId : array){
				long showId  = Long.parseLong(tempFixRpiId) ;
				if(isRelation){
					showId = pc_id;
				} 
				
	%>
		<div id="container_<%=tempFixRpiId %>>" style=" border:#F00 1px solid; width:227px; height:113px;margin-top:5px;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			      <tr>
			        <td width="22%" rowspan="3" align="center" style="border-right:#000 2px solid; font-size: 50px;">R</td>
			        <td width="78%" height="32" style=" border-bottom:#000 2px solid;">
			        	<div align="left" style="float:left; width:100px;padding-left:10px;"><%=rp_id %>-<%=tempFixRpiId %></div>
			        	<div align="right" style="float:left; width:65px;">
			        		 
			        				Pid:<%=pc_id %>
			        		 
			        	</div>
			        </td>
			      </tr>
			      <tr>
			        <td height="58" style="border-bottom:#000 2px solid;" align="center">
			      		 
			        	 	<img src="/barbecue/barcode?data=<%=showId %>&width=1&height=35&type=code39" />
			          
			        </td>
			      </tr>
			      <tr>
			        <td height="23" align="center">
			        	<%=p_name %>
			        </td>
			      </tr>
			  </table>
		</div>
	<%} 
	}%>
</div>
