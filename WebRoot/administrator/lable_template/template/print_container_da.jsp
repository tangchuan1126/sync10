<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ include file="../../../include.jsp"%>
<html>
<head>
<title>打印</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>


<!-- 打印 -->
<script type="text/javascript" src="../../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../../js/print/m.js"></script>
</head>
<%
	long id=StringUtil.getLong(request,"id");
	DBRow row=clpTypeMgrZr.findContainerById(id);
%>
<body>
	<div>&nbsp;</div>
	<div align="right" style=" border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<input class="long-button-print" type="button" onclick="printBarCode()" value="打印">
	</div>
	<br/>
	<div align="center" >
		<div id="rongqi" align="left" style="width:298px;height:128px; border:1px solid red">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		    <tr>
		      <td height="75" colspan="2" style="border-bottom:2px solid black; border-top:2px solid black">
	          	<div align="center">
	          		<span >
	          			<img src="/barbecue/barcode?data=<%=row.getString("container") %>&width=1&height=35&type=code39" />
	          		</span>
	          	</div>
	            <div align="center" >
	           	 <span style="font-size:12px"><%=row.getString("container") %></span>
	            </div>
	          </td>
	        </tr>
		    <tr>
		      <td width="39%" height="28" style=" border-right:2px solid black; border-bottom:2px black solid;" align="center">
			      <span style="font-size:12px">
			   		   容器标签
			   	  </span>
		      </td>
		      <td width="61%" style=" border-bottom:2px black solid;" align="center"><%=row.getString("type_id") %></td>
	        </tr>
	      </table> 	
	    </div>	
	</div>
</body>
</html>
<script>
	function printBarCode(){
		 //获取打印机名字列表
    	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
    	
	    //判断是否有该名字的打印机
    	var printer = "LabelPrinter";
  
    	var printerExist = "false";
    	var containPrinter = printer;
		for(var i = 0;i<printer_count;i++){
			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 )
			{
				printerExist = "true";
				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
				break;
			}
		}
		if(printerExist=="true"){
				//判断该打印机里是否配置了纸张大小
				var paper="80X35";
			    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
			    var str=strResult.split(",");
			    var status=false;
			    for(var i=0;i<str.length;i++){
                       if(str[i]==paper){
                          status=true;
                       }
				}
			    if(status==true){
                   	 visionariPrinter.PRINT_INITA(0,0,"80mm","35mm","商品标签");
                   	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                     visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"80X35");
	   				     visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",document.getElementById("rongqi").innerHTML);
	   					 visionariPrinter.SET_PRINT_COPIES(1);
	   				    // visionariPrinter.PREVIEW();
	   					 visionariPrinter.PRINT();
               }else{
               	$.artDialog.confirm("打印机配置纸张大小里没有符合的大小.....", function(){
        				 this.close();
            			}, function(){
        			});
               }	
		}else{
			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
			if(op!=-1){ //判断是否点了取消
				 visionariPrinter.PRINT_INITA(0,0,"80mm","35mm","商品标签");
               	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"80X35");
   				     visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",document.getElementById("rongqi").innerHTML);
   					 visionariPrinter.SET_PRINT_COPIES(1);
   				    // visionariPrinter.PREVIEW();
   					 visionariPrinter.PRINT();
			}	
		}
	}
</script>