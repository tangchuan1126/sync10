<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.fr.base.core.json.JSONObject,com.cwc.app.floor.api.zyj.service.*,com.cwc.app.floor.api.zyj.model.*,java.util.*,net.sf.json.JSONArray"%>
<%@ page import="com.cwc.app.key.ContainerTypeKey" %>
<%@ include file="../../../include.jsp"%>

<%
	long id=StringUtil.getLong(request,"id");
	long detail_type = ContainerTypeKey.CLP;
	long lable_template_type = 2;
	int box_type_id = StringUtil.getInt(request, "clp_type_id");
	long pc_id = StringUtil.getLong(request, "pc_id");
	
	String printName=StringUtil.getString(request,"print_name");
	boolean isPrint = StringUtil.getInt(request, "isprint") == 1;
	
	//获取登录人
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		long login_id=adminLoggerBean.getAdid();
	if(login_id==100198){
		login_id=0;
	}
	
	HttpSession sess = request.getSession(true);
	Map<String,Object> loginUser = (Map<String,Object>) sess.getAttribute("adminSesion");
	int corporationType = (Integer)loginUser.get("corporationType");
	int customerId = 0;
	//存放登录用户类型，表示用户是admin,是Customer,还是Title或者其它
	String userType = "";
	if(corporationType == 1){
		customerId = (Integer)loginUser.get("corporationId");
		userType = "Customer";
	}else if(Boolean.TRUE.equals(loginUser.get("administrator"))){
		userType = "Admin";
	}
	
    ProductCustomerSerivce  productCustomerService = (ProductCustomerSerivce)MvcUtil.getBeanFromContainer("productCustomerSerivce");
    CustomerService customerService = (CustomerService)MvcUtil.getBeanFromContainer("customerService");
    TitleService titleService = (TitleService)MvcUtil.getBeanFromContainer("titleService");
    List<Customer> customers = null;
    List<Title> titles = null;
    if(!userType.equals("Customer")){
        customers = customerService.getCustomersByProdAndCLP((int)pc_id, box_type_id);
        if(customers.size() == 0){
        	customers = customerService.getCustomers((int)pc_id);
        }
        titles = titleService.getTitlesByProdAndCLP((int)pc_id, box_type_id);
        if(titles.size() == 0){
        	titles = titleService.getTitles((int)pc_id);
        }
    }else{
        Customer customer = customerService.getCustomer(customerId);
        customers = new ArrayList<Customer>();
        if(customer != null){
        	customers.add(customer);
        }
        titles = titleService.getTitlesByProdAndCLP((int)pc_id, box_type_id,customerId);
        if(titles.size() == 0){
        	titles = titleService.getTitles((int)pc_id, customerId);
        }
    }
    JSONArray customers_json = new JSONArray();
    customers_json.addAll(customers);
    JSONArray titles_json = new JSONArray();
    titles_json.addAll(titles);
	
	String date = new TDate().getFormateTime(DateUtil.NowStr(), "MM/dd/yy");
	// DATE TYPEID PCODE LOT CUSTOMER TITLE TOTPDT TOTPKG PIECE PACKAGES CONID CLPTYPE
	String piece = null;
	String packeges = null;
	JSONObject datas = new JSONObject();
	datas.put("DATE", date);
	datas.put("CUSTOMER", "&nbsp;");
	
	DBRow row = clpTypeMgrZr.findContainerById(id);	
	DBRow clp=clpTypeMgrZr.selectContainerClpById(row.get("type_id",0l));
	DBRow[] rows = customSeachMgrGql.getLableLableTemplateByType(clp.get("pc_id", 0l) , login_id, lable_template_type, detail_type, request);//获取标签模板
	String total_package = clp.get("stack_length_qty", 0l) + "*" + clp.get("stack_width_qty", 0l) + "*" + clp.get("stack_height_qty", 0l);
	Object t =clp.get("pc_id",0l);
	System.out.println(t);
	DBRow productCode = proprietaryMgrZyj.findDetailProductByPcId(clp.get("pc_id",0l));
	datas.put("TYPEID", StringUtil.isBlank(clp.getString("type_name"))?"&nbsp;":clp.getString("type_name"));
	datas.put("CLPTYPE", StringUtil.isBlank(clp.getString("lp_name"))?"&nbsp;":clp.getString("lp_name"));
	datas.put("TOTPKG",total_package);
	datas.put("LOT",StringUtil.isBlank(row.getString("lot_number"))?"&nbsp;":row.getString("lot_number"));
	datas.put("TOTPDT",clp.get("inner_total_pc", 0l));
	datas.put("CONID", StringUtil.isBlank(row.getString("container"))?"&nbsp;":row.getString("container"));
	datas.put("PCODE", StringUtil.isBlank(productCode.getString("p_code"))?"&nbsp;":productCode.getString("p_code"));
	datas.put("TITLE", "&nbsp;");


	String title_options = "";
	boolean one_title_flag = (titles.size() == 1);
	for (Title title : titles) {
		String title_name = title.getName();
		long title_id = title.getId();
		String selected = "";
		if(one_title_flag){
			selected = "selected='selected'";
		}
		title_options += "<option value='"+ title_id+"' " + selected + ">"+title_name+"</option>";
	}
	
	String customer_options = "";
	
	//DBRow[] customers = printLabelMgrGql.getAllCustomerId();
	boolean one_customer_flag = customers.size() == 1;
	for (Customer customer : customers) {
		String selected = "";
		if(one_customer_flag || customer.getId() == customerId ){
			selected = "selected='selected'";
		}
		customer_options += "<option value='"+ customer.getId() +"' "+selected+">"+ customer.getName() +"</option>";
	} 
	
	
%>
<html>
<head>
<title>Manage LP » CLP </title>
<!--  基本样式和javascript -->
<link type="text/css" href="../../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/blockui/jquery.blockUI.233.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 打印 -->
<script type="text/javascript" src="../../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../../js/print/m.js"></script>
<!-- 替换标签占位符 -->
<script type="text/javascript" src="../../js/labelTemplate/assembleUtils.js"></script>
<link rel="stylesheet" type="text/css" href="../../js/labelTemplate/assembleUtils.css"/>
<!-- 时间控件 -->
<script type="text/javascript" src="../../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- select2选项卡 -->
<link rel="stylesheet" href="../../js/select2-4.0.0-rc.2/dist/css/select2.css">
<script type="text/javascript" src="../../js/select2-4.0.0-rc.2/dist/js/select2.min.js"></script>
</head>
</head>

<script type="text/javascript"> 
var productId = "<%=pc_id %>";
var box_type_id = "<%=box_type_id %>";
 $(function(){
	//初始化时，替换值
	var datas = <%=datas%>;	
	var divs=$("div[name='lableContent']");
	for(var i=0;i<divs.length;i++){
		var content = $(divs[i]).html();
		$(divs[i]).html(assembleTemplate(datas, content));
	}

	//初始化标签时间显示可修改状态 gql 
	if($("input[name='input_date']").length){
		$("span[name='dateShow']").css("display","none");
		$("input[name='input_date']").css("display","");
		$("span[name='dateShow']").html('<%=date%>');
		$("input[name='input_date']").val('<%=date%>');
		//添加时间控件		
		$("input[name='input_date']").each(function(i, elem){
			$(this).datepicker({
				dateFormat:"mm/dd/y",
				changeMonth: true,
				changeYear: true
			}).bind("change",function(){
		    	  var input_date = $(this).val();
		    	  var $obj = $(this).prev("span[name='dateShow']").html(input_date);
		      });
		});
		$("#ui-datepicker-div").css("display","none");
	}
	
	//初始化customer手动选择，隐藏打印时的div
	var customer_options = "<%=customer_options%>";
	$("select[name='select_customer']").append(customer_options);
	$("div[name='show_customer']").css("display","none");
	$("select[name='select_customer']").each(function(i, elem){
		$(this).select2({
		 placeholder: "Select..."
        ,allowClear: true
      }).bind("change",function(event){
    	  var customerId = $(event.target).val();
    	  var customer = $(this).children("option[value='" + customerId + "']").text();
    	  $(this).prev("div[name='show_customer']").find("center").html(customer+"&nbsp;&nbsp;");
    	  
          var target = event.target;
          $.ajax({
               type : "get",
               url  : "/Sync10/basicdata/productCustomer/" + productId + "/" + box_type_id + "/" + customerId + "/title",
               success : function(data){
            	   var titleId =  $(target).parent().next().find("select[name='select_title']").val();
                   if(data && data.length > 0){
	            	   //标识当前选中的Title 是否出现在新的Title 列表中
	            	   var exist = false;
                	   $(target).parent().next().find("select[name='select_title']").children().remove("option[value!='']");
                	  

                       for(var i = 0; i < data.length; i++){
                           if(titleId == data[i].id){
                        	   exist = true;
                           }
                           $(target).parent().next().find("select[name='select_title']").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
                       }
                       if(!exist){
                    	 	$(target).parent().next().find("span.select2-selection__rendered").html("<span class=\"select2-selection__placeholder\">Select...</span>");
                    	 	$(target).parent().next().find("div[name='show_title']").find("center").html("");
                       }else{
                    	   $(target).parent().next().find("select[name='select_title']").val(titleId);
                       }
                   }
               }
          });            	  
      });
	  var customer2 = $(this).children("option[value='" + $(this).val() + "']").text();
	  $(this).prev("div[name='show_customer']").find("center").html(customer2+"&nbsp;&nbsp;");
	});
	
	//初始化title手动选择，隐藏打印时的div
	var html = "<%=title_options%>";
// 	placeholder = title==""?" ":title;
	$("select[name='select_title']").append(html);
	$("div[name='show_title']").css("display","none");
	$("select[name='select_title']").each(function(i, elem){
		$(this).select2({
			 placeholder: "Select..."
	        ,allowClear: true
      }).bind("change",function(){
    	  var title = $(this).children("option[value='" + $(this).val() + "']").text();
    	  var $obj = $(this).prev("div[name='show_title']").find("center").html(title);
      });
	  var title2 = $(this).children("option[value='" + $(this).val() + "']").text();
	  $(this).prev("div[name='show_title']").find("center").html(title2);
	});
		
});
</script> 
</head>
<body  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	 <table  width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="lableshow">
		<%for(int i=0;i<rows.length;i++){							
		%>
			<tr >
				<td>
					<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="lableshow">
						<tr>
							<td width="220" nowrap="nowrap" align="right" valign="middle" style="border-right:2px #eeeeee solid;border-bottom:2px #eeeeee solid;">
								<table align="left" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td align="right" style="padding-left: 15px;">Printer Name:</td>
										<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].getString("print_name")%></strong></td>
									</tr>
									<tr>
										<td align="right" style="padding-left: 15px;">Label Name:</td>
										<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].getString("lable_name")%></strong></td>
									</tr>					
									<tr>
										<td align="right" style="padding-left: 15px;">Paper Size:</td>
										<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].get("lable_type","")%></strong></td>
									</tr>
								</table>
							</td>
							<td align="left" valign="middle" style="padding:10px;padding-left:25px;border-bottom:2px #eeeeee solid;">
								<div name="lableContent" style="background: #FFF;margin-left: 30px;margin-top: 30px;padding:0px;float: left;width:390px;text-align:center;border:1px solid red;">
									<%=rows[i].get("lable_content","")%>
								</div>
							</td>
							<td align="left" valign="middle" style="padding:10px 10px 10px 0px;border-bottom:2px #eeeeee solid;">
								<input type="hidden" id="printName_<%=i%>" name="printName" value="<%=rows[i].get("print_name","")%>" />
								<input value="  Print Label" type="button"  class="long-button-print" onclick="printBarCode(this,'<%=i%>');"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>		
		<%}%>
		<%if(rows==null || rows.length==0){ %>
			<tr>
				<td style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;" colspan="3">NO Data</td>
			</tr>
		<% }%>
	</table>
	<!-- 打印内容  gql-->
	<div name="printAll" id="printAll" style="display: none"></div>
	<!-- gql end -->
</body>
</html>
<script>
	function printBarCode(obj, index){
		
		setPrintAll(obj)//设置打印内容
		 //获取打印机名字列表
	  	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
	  	
	    //判断是否有该名字的打印机
	  	var printer = $("#printName_"+index).val();
// 	  	var printer = "LabelPrinter";
	
	  	var printerExist = "false";
	  	var containPrinter = printer;
		for(var i = 0;i<printer_count;i++){
			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 )
			{
				printerExist = "true";
				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
				break;
			}
		}
		if(printerExist=="true"){
				//判断该打印机里是否配置了纸张大小
				var paper="102X152";
			    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
			    var str=strResult.split(",");
			    var status=false;
			    for(var i=0;i<str.length;i++){
	                     if(str[i]==paper){
	                        status=true;
	                     }
				}
			    if(status==true){
	 					 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
	 					 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Container Label");
	 			         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");            
	 			    	 visionariPrinter.ADD_PRINT_TABLE(5,3,"97%","100%",$("#printAll").html());
	 					 visionariPrinter.SET_PRINT_COPIES(1);
// 	 					 visionariPrinter.PREVIEW();
	 					 visionariPrinter.PRINT();
	             }else{
	             	$.artDialog.confirm("Please Change Paper", function(){
	      				 this.close();
	          			}, function(){
	      			});
	             }	
		}else{
			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
			if(op!=-1){ //判断是否点了取消
				 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Container Label");
				 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
		         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");            
		    	 visionariPrinter.ADD_PRINT_TABLE(5,3,"97%","100%",$("#printAll").html());
				 visionariPrinter.SET_PRINT_COPIES(1);
// 				  visionariPrinter.PREVIEW();
				 visionariPrinter.PRINT();
	 				 
			}	
		}
		
	}
	
	
	//设置打印内容  gql,方法写在assembleUtils.js中，2015/05/05修改
	function setPrintAll(obj){
		var $obj = $(obj).parent("td").prev("td").find("div[name='lableContent']");
		var html = $obj.html();//获取打印的内容
		$("#printAll").html(html);//赋值打印的内容
		
		setClpPrintAll($("#printAll"));//设置clp打印的样式
	}
	
	
	//安卓打印方法
	function supportAndroidprint(){
		//获取打印机名字列表
	   	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
	   	 //判断是否有该名字的打印机
	   	var printer = "<%=printName%>";
	   	var printerExist = "false";
	   	var containPrinter = printer;
		for(var i = 0;i<printer_count;i++){
			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
				printerExist = "true";
				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
				break;
			}
		}
		if(printerExist=="true"){
			return androidIsPrint(containPrinter);
		}else{
			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
			if(op!=-1){ //判断是否点了取消
				return androidIsPrint(containPrinter);
			}
		}
	}

	function androidIsPrint(containPrinter){
		var $obj = $("div[name='lableContent']");
		var html = $obj.html();//获取打印的内容
		$("#printAll").html(html);//赋值打印的内容
		setClpPrintAll($("#printAll"));//设置clp打印的样式
		
		var flag = true ;

		 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
		 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Container Label");
         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");            
    	 visionariPrinter.ADD_PRINT_TABLE(5,3,"97%","100%",$("#printAll").html());
		 visionariPrinter.SET_PRINT_COPIES(1);
//				 visionariPrinter.PREVIEW();
 		flag = flag && visionariPrinter.PRINT();
		return flag ;
	}
	
	
</script>