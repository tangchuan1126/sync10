<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.LableTemplateKey"%>
<%@ include file="../../include.jsp"%> 

<html>
<head>
<title>标签打印</title>

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

</head>
<%
long lable_template_id = StringUtil.getLong(request,"lable_template_id");
long pc_id = StringUtil.getLong(request,"pc_id");

DBRow lable_template = lableTemplateMgrZJ.getDetailLableTemplateById(lable_template_id);
DBRow product = productMgr.getDetailProductByPcid(pc_id);

//生产线
DBRow[] productLine= mgrZwb.getAllProductLine(); 
%>

<body>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <th width="240" align="center" class="right-title"  style="vertical-align: center;text-align:center;">商品名称</th>
		<th width="240" style="vertical-align: center;text-align: center;" class="right-title">商品条码</th>
        <th width="120" style="vertical-align: center;text-align: center;" class="right-title">打印数量</th>
        <th width="120" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
    </tr>
    <tr> 
      <td height="100" valign="middle" align="center" style="font-size:14px;border-bottom:1px #eeeeee solid; ">
      <%=product.getString("p_name")%></td>
	  <td align="center" valign="middle" style="font-size:14px;border-bottom: 1px #eeeeee solid;">
      	<%=product.getString("p_code") %>	  
	  </td>
      <td align="center" valign="middle" nowrap="nowrap" style="border-bottom:1px #eeeeee solid; ">
      	<input style="width:25px;" maxlength="3" type="text" onKeyPress="return noNumbers(event)" id="productprintcount_<%=product.get("pc_id",0l) %>" name="productprintcount_<%=product.get("pc_id",0l) %>" value="1"/>
      </td>
	  <td style="border-bottom: 1px #eeeeee solid;" align="center">
	  	<input type="button" class="long-button-print" value="打印" onclick="printBarCode()"/><br/><br/>
	  	<input class="button_long_search" type="button" onclick="ssMenu()" value="预览" name="browse">
	  </td>
	 </tr>
</table>
<div id="lableContent" style="width:310; border:1px solid red;">
<span style="font-family:Arial Black;font-weight:bold;">Made In China</span>
</div>	
</body>
</html>
<script>
function printBarCode(){
	var size='<%=lable_template.getString("lable_type")%>';

	var str=size.split("X");
	var lableWidth=str[0]+"mm";
	var lableHeight=str[1]+"mm";
	
	var printer='tsc';
	var containPrinter=printer;
	//获取打印机名字列表
    var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
    for(var i = 0;i<printer_count;i++){
        if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){            
          printerExist = "true";
          containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
          break;
        }
    }
    visionariPrinter.PRINT_INITA(0,0,lableWidth,lableHeight,"商品标签");
	visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
    visionariPrinter.SET_PRINT_PAGESIZE(1,lableWidth,lableHeight,size);
    visionariPrinter.ADD_PRINT_HTML(0,0,"100%","100%",$('#lableContent').html());
    visionariPrinter.SET_PRINT_COPIES(1);
    visionariPrinter.PREVIEW();
    //visionariPrinter.PRINT();
}
</script>