<%@ page contentType="text/html;charset=utf-8"%>
<%@ include file="../../include.jsp"%> 
<%
	PageCtrl pc = new PageCtrl();
	pc.setPageSize(30);
	pc.setPageNo(1);
	DBRow rows[] = lableTemplateMgrZJ.getAllTemplateLabel(null);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/select.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
		
<script language="JavaScript" type="text/JavaScript">

function isNum(val)
{	
	var chk = parseFloat(val);
	if(chk != val)
    {
    	return true;//(!reg.test(val));
    }
    else
    {
    	return false;
    }
    
}

function addLableTemplate()
{

	$.prompt(
	
	"<div id='title'>增加标签模板</div><br />标签分类<br><select name='proTextLableType' id='proTextLableType'><option value=0>请选择标签类型...</option><option value=1>商品标签</option><option value=2>位置标签</option><option value=3>资产标签</option><option value=4>外箱标签</option><option value=5>序列号标签</option><option value=6>残损件标签</option><option value=7>未知商品标签</option><option value=8>拆散套装标签</option></select><br>模板名称<br><input name='proTextTemplateName' type='text' id='proTextTemplateName' style='width:100px;'><br>打印机名称<br><input name='proTextPrinter' type='text' id='proTextPrinter' style='width:100px;'><br>纸张名称<br><input name='proTextPaper' type='text' id='proTextPaper' style='width:100px;'><br>纸张宽度<br><input name='proTextPrintRangeWidth' type='text' id='proTextPrintRangeWidth' style='width:100px;'>单位:毫米<br>纸张高度<br><input name='proTextPrintRangeHeight' type='text' id='proTextPrintRangeHeight' style='width:100px;'>单位:毫米<br>模板路径<br><input name='proTextTemplatePath' type='text' id='proTextTemplatePath' style='width:300px;'><br>预览图<br><input name='proTextTemplateImg' type='text' id='proTextTemplateImg' style='width:300px;'>",
	
	{
	      submit: 
		  		function (v,m,f)
				{
					if (v=="y")
					{
						 if(f.proTextLableType == 0)
						 {
								alert("请选择标签分类");
								return false;
						 }
						 else if (f.proTextTemplateName == 0)
						 {
								alert("请填写模板名称");
								return false;
						 }
						 else if (f.proTextPrinter == 0)
						 {
								alert("请填写打印机名称");
								return false;
						 }
						 else if (f.proTextPaper == 0)
						 {
								alert("请填写纸张名称");
								return false;
						 }
						 else if (f.proTextPrintRangeWidth == 0)
						 {
								alert("请填写纸张宽度");
								return false;
						 }
						 else if(isNum(f.proTextPrintRangeWidth))
						 {
						 	alert("纸张宽度格式不对");
						 	return false;
						 }
						 
						 else if (f.proTextPrintRangeHeight == 0)
						 {
								alert("请填写纸张高度");
								return false;
						 }
						 else if(isNum(f.proTextPrintRangeHeight))
						 {
						 	alert("纸张高度格式不对");
						 	return false;
						 }
						 else if (f.proTextTemplatePath == 0)
						 {
								alert("请填写模板路径");
								return false;
						 }
						 else if (f.proTextTemplateImg == 0)
						 {
								alert("请填写模板预览图");
								return false;
						 }
						 return true;
					}
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/lableTemplate/addLableTemplate.action";
						document.add_form.template_name.value = f.proTextTemplateName;		
						document.add_form.lable_type.value = f.proTextLableType;
						document.add_form.printer.value = f.proTextPrinter;
						
						document.add_form.paper.value = f.proTextPaper;
						document.add_form.print_range_width.value = f.proTextPrintRangeWidth;
						document.add_form.print_range_height.value = f.proTextPrintRangeHeight;
						document.add_form.template_path.value = f.proTextTemplatePath;
						document.add_form.img_path.value = f.proTextTemplateImg;
											
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function modLableTemplate(lable_template_id)
{

	$.prompt(
	
	"<div id='title'>修改标签模板</div><br />标签分类<br><select name='proTextLableType' id='proTextLableType'><option value=0>请选择标签类型：：：</option><option value=1>商品标签</option><option value=2>位置标签</option><option value=3>资产标签</option><option value=4>外箱标签</option><option value=5>序列号标签</option><option value=6>残损件标签</option><option value=7>未知商品标签</option><option value=8>拆散套装标签</option></select><br>模板名称<br><input name='proTextTemplateName' type='text' id='proTextTemplateName' style='width:100px;'><br>打印机名称<br><input name='proTextPrinter' type='text' id='proTextPrinter' style='width:100px;'><br>纸张名称<br><input name='proTextPaper' type='text' id='proTextPaper' style='width:100px;'><br>纸张宽度<br><input name='proTextPrintRangeWidth' type='text' id='proTextPrintRangeWidth' style='width:100px;'>单位:毫米<br>纸张高度<br><input name='proTextPrintRangeHeight' type='text' id='proTextPrintRangeHeight' style='width:100px;'>单位:毫米<br>模板路径<br><input name='proTextTemplatePath' type='text' id='proTextTemplatePath' style='width:300px;'><br>预览图<br><input name='proTextTemplateImg' type='text' id='proTextTemplateImg' style='width:300px;'>",
	
	{
	      submit: 
		  		function (v,m,f)
				{
					if (v=="y")
					{
						 if(f.proTextLableType == 0)
						 {
								alert("请选择标签分类");
								return false;
						 }
						 else if (f.proTextTemplateName == 0)
						 {
								alert("请填写模板名称");
								return false;
						 }
						 else if (f.proTextPrinter == 0)
						 {
								alert("请填写打印机名称");
								return false;
						 }
						 else if (f.proTextPaper == 0)
						 {
								alert("请填写纸张名称");
								return false;
						 }
						  else if (f.proTextPrintRangeWidth == 0)
						 {
								alert("请填写纸张宽度");
								return false;
						 }
						 else if(isNum(f.proTextPrintRangeWidth))
						 {
						 	alert("纸张宽度格式不对");
						 	return false;
						 }
						 
						 else if (f.proTextPrintRangeHeight == 0)
						 {
								alert("请填写纸张高度");
								return false;
						 }
						 else if(isNum(f.proTextPrintRangeHeight))
						 {
						 	alert("纸张高度格式不对");
						 	return false;
						 }
						 else if (f.proTextTemplatePath == 0)
						 {
								alert("请填写模板路径");
								return false;
						 }
						 else if (f.proTextTemplateImg == 0)
						 {
								alert("请填写模板预览图");
								return false;
						 }
						 return true;
					}
				}
		  ,
		  loaded:
		  
				function ()
				{
					
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/lableTemplate/getDetailLableTemplateJSON.action",
							{lable_template_id:lable_template_id},//{name:"test",age:20},
							function callback(data)
							{
								$("#proTextLableType").setSelectedValue(data.lable_type);
								$("#proTextTemplateName").val(data.template_name);
								$("#proTextPrinter").val(data.printer);
								
								$("#proTextPaper").val(data.paper);
								$("#proTextPrintRangeWidth").val(data.print_range_width);
								$("#proTextPrintRangeHeight").val(data.print_range_height);
								$("#proTextTemplatePath").val(data.template_path);
								$("#proTextTemplateImg").val(data.img_path);
							}
					);
				},
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/lableTemplate/modLableTemplate.action";
						
						document.mod_form.lable_template_id.value = lable_template_id;
						document.mod_form.template_name.value = f.proTextTemplateName;		
						document.mod_form.lable_type.value = f.proTextLableType;
						document.mod_form.printer.value = f.proTextPrinter;
						
						document.mod_form.paper.value = f.proTextPaper;
						document.mod_form.print_range_width.value = f.proTextPrintRangeWidth;
						document.mod_form.print_range_height.value = f.proTextPrintRangeHeight;
						document.mod_form.template_path.value = f.proTextTemplatePath;
						document.mod_form.img_path.value = f.proTextTemplateImg;
						document.mod_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function delLableTemplate(temlpate,lable_template_id)
{
	if(confirm("确定删除“"+temlpate+"”吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/lableTemplate/delLableTemplate.action";
		document.del_form.lable_template_id.value = lable_template_id;
		document.del_form.submit();
	}
}

function showimg(img)
{	
	if(img != "")
	{
		tb_show('',img+'?TB_iframe=true&height=500&width=800',false);
	}
	else
	{
		alert("该商品暂时没有图片");
	}
}

</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="onLoadInitZebraTable()">
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 标签管理 »   标签模板管理</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <form  method="post" name="add_form">
    <input type="hidden" name="template_name">
    <input type="hidden" name="lable_type">
    <input type="hidden" name="printer">
    <input type="hidden" name="paper">
    <input type="hidden" name="print_range_width">
    <input type="hidden" name="print_range_height">
    <input type="hidden" name="template_path">
    <input type="hidden" name="img_path"/>
  </form>
  
   <form  method="post" name="mod_form">
   	<input type="hidden" name="lable_template_id"/>
    <input type="hidden" name="template_name">
    <input type="hidden" name="lable_type">
    <input type="hidden" name="printer">
    <input type="hidden" name="paper">
    <input type="hidden" name="print_range_width">
    <input type="hidden" name="print_range_height">
    <input type="hidden" name="template_path">
    <input type="hidden" name="img_path"/>
  </form>

  <form  method="post" name="del_form">
    <input type="hidden" name="lable_template_id">
  </form>
  <tr>
    <td><label>
      <input name="Submit3" type="button" class="long-long-button-add" onClick="addLableTemplate()" value="增加标签模板">
    </label></td>
  </tr>
</table>
<br>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form name="listForm" method="post">

	
    <tr> 
        <th width="297"  class="left-title">模板名称</th>
        <th width="203"  class="right-title" style="vertical-align: center;text-align: center;">打印机名称</th>
        <th width="176" align="left"  class="right-title" style="vertical-align: center;text-align: center;">纸张类型</th>
        <th width="139" align="center"  class="right-title" style="vertical-align: center;text-align: center;">纸张宽</th>
        <th width="122"  class="right-title" style="vertical-align: center;text-align: center;">纸张高</th>
        <th width="142"  class="right-title" style="vertical-align: center;text-align: center;">模板文件</th>
        <th width="165"  class="right-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
    </tr>
	<%
		for(int i=0;i<rows.length;i++)
		{
	%>
    <tr> 
      <td height="80" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%=rows[i].getString("template_name") %></td>
      <td align="center" valign="middle" style='word-break:break-all;' ><%=rows[i].getString("printer") %></td>
      <td align="center" valign="middle" style="line-height:20px;"><%=rows[i].getString("paper") %></td>
      <td align="center" valign="middle"><%=rows[i].getString("print_range_width") %></td>
      <td align="center" valign="middle"><%=rows[i].getString("print_range_height") %></td>
      <td align="center" valign="middle"><%=rows[i].getString("template_path") %></td>
      <td align="center" valign="middle" nowrap="nowrap">
      <input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="modLableTemplate(<%=rows[i].get("lable_template_id",0l) %>)">
&nbsp;&nbsp;
<input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delLableTemplate('<%=rows[i].getString("template_name") %>',<%=rows[i].get("lable_template_id",0l) %>)">
&nbsp;&nbsp;
<input name="showImg" type="button" class="short-button" value="查看图片" onClick="showimg('<%=rows[i].getString("img_path")%>')"/>
</td>
    </tr>
    <%
		}
	%>
  </form>
</table>
</body>
</html>









