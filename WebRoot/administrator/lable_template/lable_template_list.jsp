<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
//获取登录人
	DBRow[] rows = customSeachMgrGql.getLableTemp(1);//查询所有的商品基础模板标签
	DBRow[] detailRows = customSeachMgrGql.getLableTemp(2);//查询所有商品详细模板
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>LabeL Template List</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
	.aui_titleBar{  border: 1px #DDDDDD solid;}
	.aui_header{height: 40px;line-height: 40px;}
	.aui_title{
	  height: 40px;
	  width: 100%;
	  font-size: 1.2em !important;
	  margin-left: 20px;
	  font-weight: 700;
	  text-indent: 0;
	}

	td.categorymenu_td div{margin-top:-3px;}
	
	.ui-widget-content{
		border:1px #CFCFCF solid;
		border-radius: 0px;
	  	-webkit-border-radius: 0px;
		-moz-border-radius:0;
	}
	.ui-widget-header{
		background:#f1f1f1 !important;
		border-radius: 0;
		border-top:0;
		border-left:0;
		border-right:0;
		border-bottom: 1px #CFCFCF solid;
	}
	.ui-tabs .ui-tabs-nav li a{
		padding: .5em 1em 13px !important;
	}
	.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
		  height: 40px;
		  margin-top: -4px !important;
		background:#fff;
	}
	.ui-tabs .ui-tabs-nav{
		padding :0 .2em 0;
	}
	.ui-tabs{
		padding:0;
	}
	
	.ui-jqgrid{border:0;}
	.ui-jqgrid-titlebar.ui-widget-header{
		background:#fff !important;
		height:35px;
		line-height:30px;
	}
	
	 
</style>
 
<script type="text/javascript">

jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
});

	function printPage(lable_template_id,lableType){
// 		var href;
// 		href = "../lable_template/print_one_product_ui.html?lableType="+lableType+"&lable_template_id="+lable_template_id;
// 		window.location.href = href;
		
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/lable_template/print_one_product_ui.html?lableType="+lableType+"&lable_template_id="+lable_template_id;
		$.artDialog.open(uri , {title: "Print Label Template",width:'875px',height:'500px', lock: true,opacity: 0.3,fixed: true});
	
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="margin-top:10px;">
<div id="tabs">
   <ul>
   	<li><a href="#tab1" >Basic Template</a></li>	
   	<li><a href="#tab2" >Product Template</a></li>
   </ul>
   <div id="tab1">
   		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" style="margin:0px;border-collapse:separate;border-spacing:2px;"> 
    <tr> 
        <th class="left-title" style="text-align: center;">TemplateName</th>
        <th class="right-title" style="vertical-align: center;text-align: center;">PrinterName</th>
        <th align="left"  class="right-title" style="vertical-align: center;text-align: center;">PaperType</th>
    </tr> 
  <%for(int i=0;i<rows.length;i++){ %>
    <tr onclick="printPage('<%=rows[i].get("lable_id",0l)%>','1')" class="addLable">
      <td align="center" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%=rows[i].get("lable_name","")%></td>
      <td align="center" valign="middle" style="word-break:break-all;"><%=rows[i].get("print_name","")%></td>
      <td align="center" valign="middle" style="line-height:20px;"><%=rows[i].get("lable_type","")%></td>
    </tr>
    <%} %>
  </table>
   </div>
   <div id="tab2"> 
   		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" style="margin:0px;border-collapse:separate;border-spacing:2px;"> 
    <tr> 
        <th class="left-title" style="text-align: center;">TemplateName</th>
        <th class="right-title" style="vertical-align: center;text-align: center;">PrinterName</th>
        <th align="left"  class="right-title" style="vertical-align: center;text-align: center;">PaperType</th>
    </tr> 
  <%for(int i=0;i<detailRows.length;i++){ %>
    <tr class="addLable" onclick="printPage('<%=detailRows[i].get("detail_lable_id",0l)%>','2')">
      <td align="center" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%=detailRows[i].get("lable_name","")%></td>
      <td align="center" valign="middle" style='word-break:break-all;' ><%=detailRows[i].get("print_name","")%></td>
      <td align="center" valign="middle" style="line-height:20px;"><%=detailRows[i].get("lable_type","")%></td>
    </tr>
    <%} %>
  </table>
   
   </div>
</div>

</body>
</html>
