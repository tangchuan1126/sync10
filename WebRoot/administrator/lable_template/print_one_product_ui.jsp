<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.fr.base.core.json.JSONObject"%>
<%@page import="com.cwc.app.key.LableTemplateKey"%>
<%@page import="com.cwc.app.key.ContainerTypeKey"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>



<%
long lable_template_id = StringUtil.getLong(request,"lable_template_id");
long lableType = StringUtil.getLong(request,"lableType");//判断是不是商品基础模板（商品列表页面item label按钮用）

DBRow lable_template = new DBRow();
if(lableType==1){
	lable_template = productLableTemp.getLableTempById(lable_template_id);//判断是不是商品基础模板（商品列表页面item label按钮用）
}else{
	lable_template = customSeachMgrGql.getLableTemplateById(lable_template_id);//商品关联的详细模板列表
}

String lable_temp_type="";
if(lable_template!=null){
	if(lable_template.get("lable_template_type",0l)==1){
		lable_temp_type="pc";
	}else if(lable_template.get("type",0l)==ContainerTypeKey.CLP){
		lable_temp_type="clp";
	}
}
String title_name=StringUtil.getString(request, "title_name");

long purchase_id = StringUtil.getLong(request,"purchase_id");//采购单id
long pcid = StringUtil.getLong(request,"pcid");

long pc_id = StringUtil.getLong(request,"pc_id");//商品id

String factoryType=StringUtil.getString(request,"factory_type");//接受工厂型号

String warehouse=StringUtil.getString(request,"warehouse");//收货仓库

long supplierSid=StringUtil.getLong(request,"supplierSid");//接受供应商id

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

long cid = StringUtil.getLong(request,"cid");
int type = StringUtil.getInt(request,"type");
String name = StringUtil.getString(request,"name");
String cmd = StringUtil.getString(request,"cmd");

	DBRow[] rows;
	if(cmd.equals("search")){
		rows = productMgrZJ.searchProductsByPurchaseIdKey(purchase_id,name,null);
	}else{
		rows = productMgrZJ.getProductsByPurchaseId(purchase_id,null);
	}
	
	String date = new TDate().getFormateTime(DateUtil.NowStr(), "MM/dd/yy");
	DBRow product = productMgr.getDetailProductByPcid(pc_id);
// 	System.out.println(StringUtil.convertDBRowsToJsonString(product));
	JSONObject datas = new JSONObject();
	String code = "NA";
	if(product != null){
		code = product.getString("p_code").length()>0?product.getString("p_code"):"NA";
		if(product!=null&&"pc".equals(lable_temp_type)){
			datas.put("LOGO", "NA");
			datas.put("LOT", "NA");
			datas.put("SN", "NA");
			datas.put("SHIPTO", "NA");
			datas.put("ORIGIN", "NA");
			datas.put("PCID", pc_id);
			datas.put("PCNAME", product.getString("p_name"));
			datas.put("CODE", code);
			datas.put("CODE", product.getString("p_code"));
			datas.put("UPC", product.getString("upc"));
			datas.put("LENGTH", product.getString("length"));
			datas.put("WIDTH", product.getString("width"));
			datas.put("HEIGHT", product.getString("heigth"));
			datas.put("WEIGHT", product.getString("weight"));
			
		}else if(product!=null&&"clp".equals(lable_temp_type)){
			datas.put("DATE",date);
			datas.put("CUSTOMER", "NA");
			datas.put("LOT", "NA");
			datas.put("PCODE", product.getString("p_code"));
			datas.put("TITLE", "");
			datas.put("TYPEID", "NA");
			datas.put("CONID", "NA");	
			datas.put("TOTPDT", 0);
			datas.put("TOTPKG", 0);
			datas.put("PIECE", 0);
			datas.put("PACKAGES",0);
		}
	}
	

DBRow[] options = lableTemplateMgrZJ.getAllLableTemplate(null);

Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));

//生产线
DBRow[] productLine= mgrZwb.getAllProductLine(); 

//采购单
//DBRow[] allPurchase= mgrZwb.getAllPurchase();

//供应商
DBRow[] allSupplier=mgrZwb.getAllSupplier();

String html=StringUtil.getString(request, "html");
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>标签打印</title>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />

<script type="text/javascript" src="../js/poshytip/jquery-1.4.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 


<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 替换标签占位符 -->
<script type="text/javascript" src="../js/labelTemplate/assembleUtils.js"></script>
<!-- 时间控件 -->
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<style stype="text/css">
@font-face { font-family: dreamy; font-weight: bold; src: url(http://www.example.com/font.eot);
</style>
<!-- 标签手动修改样式 gql -->
<style stype="text/css">
	.edit_box{
		border: 1px #CCC dashed;
		margin: 1px 8px;
		height: 30px;
	}
	.date_selector {
		font-size: 12px;
	}
</style>
<!-- 标签手动修改样式 gql end-->

<script language="javascript">
	$(function(){
		
		var datas = <%=datas%>;
		var html = assembleTemplate(datas,$("#lableContent").html());
		var lable_type = '<%=lable_template.getString("lable_type")%>';
		var code = '<%=code%>';
		html = setImg($(html), code, lable_type);//调整条码位置
		$("#lableContent").html(html);
		
		setImg($('#lableContentCopy'), code, lable_type);//调整模板条码位置
		
		if($("#input_date")){
			$("#dateShow").css("display","none");
			$("#input_date").css("display","");
			$("#dateShow").html('<%=date%>');
			$("#input_date").val('<%=date%>');
			
			//添加时间控件		
			$("#input_date").date_input({
				dateToString: function(date) {
					  var month = (date.getMonth() + 1).toString();
					    var dom = date.getDate().toString();
					    if (month.length == 1) month = "0" + month;
					    if (dom.length == 1) dom = "0" + dom;
					    return month + "/" + dom+ "/" +date.getFullYear().toString().substring(2,4);

				}
			});
		}
		
		$("#date").date_input({
			dateToString: function(date) {
				  var month = (date.getMonth() + 1).toString();
				    var dom = date.getDate().toString();
				    if (month.length == 1) month = "0" + month;
				    if (dom.length == 1) dom = "0" + dom;
				    return month + "/" + dom+ "/" +date.getFullYear().toString().substring(2,4);

			}
		});
		
		
	});
	
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="">
<br/>
<%-- <input type="hidden" value="<%=lable_template.getString("lable_content")%>" id="hiddenVal" style="display:none" /> --%>
<div align="center">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <th width="240" align="left" class="right-title"  style="vertical-align: center;text-align: center;">Product Name</th>
		<th width="240" style="vertical-align: center;text-align: center;" class="right-title">Product Code</th>
        <th width="120" style="vertical-align: center;text-align: center;" class="right-title">Print Number</th>
        <th width="120" style="vertical-align: center;text-align: center;" class="right-title">Operation</th>
    </tr>
    <tr> 
      <td height="50" valign="middle"  style="font-size:14px;border-bottom:1px #eeeeee solid;" align="center">
   	  		<%=product!=null?product.getString("p_name"):""%>
   	  </td>
	  <td align="center" valign="middle" style="font-size:14px;border-bottom: 1px #eeeeee solid;">
      		<%=product!=null?product.get("pc_id",0l):"" %>	  
	  </td>
      <td align="center" valign="middle" nowrap="nowrap" style="border-bottom:1px #eeeeee solid; ">
      		<input style="height:30px;" maxlength="3" type="text" onKeyPress="return noNumbers(event)" id="productprintcount_<%=product!=null?product.get("pc_id",0l):"" %>" name="productprintcount_<%=product!=null?product.get("pc_id",0l):"" %>" value="1"/>
      </td>
	  <td style="border-bottom:1px #eeeeee solid;" align="center">
	  		<a class="buttons" value="Print" onclick="printBarCode()"/><i class="icon-print"></i>&nbsp;Print</a>
	  </td>
	 </tr>
</table>
</div>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#E5E5E5" style="margin-left: 5px;">
	<% if(lableType==1||lableType==2){%>
		<tr style="line-height:45px; height: 45px;"> 
				<td height="30px;" align="center">
					<input type="text" value="*Logo" key="*Logo" id="logo" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" /></td>
				<td align="center">
					<input type="text" value="*SN" key="*SN" id="sn" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" />
				</td>
				<td align="center">
					<input type="text" value="*LOT" key="*LOT" id="lot" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" />
				</td>
				<td rowspan="3" align="right" style="padding-right: 16px;">
					<a value="Assignment" class="buttons primary" onclick="ssMenu()"><i class="icon-edit"></i>&nbsp;Assignment</a>
				</td>
			</tr>
			<tr style="line-height: 45px; height: 45px;">
				<td align="center" height="30px;"><input type="text" value="*Ship To" key="*Ship To" id="shipTo" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" /></td>
				<td align="center"><input type="text" value="*Origin" key="*Origin" id="origin" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" /></td>
				<td align="center"><input type="text" value="*Code" key="*Code" id="code" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" /></td>
			</tr>
			<tr style="line-height: 45px;">
				<td align="center" height="30px;"><input type="text" value="*PCName" key="*PCName" id="pcName" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" /></td>
				<td align="center"><input type="text" value="*PCID" key="*PCID" id="pcid" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" /></td>
				<td align="center"><input type="text" value="*Weight" key="*Weight" id="weight" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" /></td>
<!-- 				<td align="center"><input type="text" value="" placeholder="*Weight" id="weight" style="color:#999;height:30px;" size="30px;"/></td> -->
			</tr>
	<% }else{%>
		<% if(lable_template.get("lable_template_type",0l)==1){%>
			<tr style="line-height:45px; height: 45px;"> 
				<td height="30px;" align="center">
					<input type="text" value="*Logo" key="*Logo" id="logo" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" /></td>
				<td align="center">
					<input type="text" value="*SN" key="*SN" id="sn" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" />
				</td>
				<td align="center">
					<input type="text" value="*LOT" key="*LOT" id="lot" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" />
				</td>
				<td rowspan="2" align="right" style="padding-right: 16px;">
					<a value="Assignment" class="buttons primary" onclick="ssMenu()"><i class="icon-edit"></i>&nbsp;Assignment</a>
				</td>
			</tr>
			<tr style="line-height: 45px;">
				<td align="center" height="30px;"><input type="text" value="*Ship To" key="*Ship To" id="shipTo" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" /></td>
				<td align="center"><input type="text" value="*Origin" key="*Origin" id="origin" style="color:#999;height:30px;" size="30px;" onblur="checkValue(this);" onfocus="clearValue(this)" /></td>
				<td align="center">&nbsp;</td>
			</tr>
		<% }%>
	<% }%>
</table>
<div name="lableContent" id="lableContent" style="margin-left:290px;margin-top: 30px;padding:0px;float: left;width:<%=lable_template.get("lable_width",0)%>px;height:<%=lable_template.get("lable_height",0)+5%>px;text-align:center;border:1px solid red;">
 	<%=lable_template.getString("lable_content")%> 
</div>

<!-- gql 替换值用 -->
<div id="lableContentCopy" style="margin-left:290px; display:none; margin-top: 30px;padding:0px;float: left;width:<%=lable_template.get("lable_width",0)%>px;height:<%=lable_template.get("lable_height",0)%>px;position:relative;text-align:center;border:1px solid red;">
 	<%=lable_template.getString("lable_content")%> 
</div>
<!-- gql end -->
</body>
</html>
<script type="text/javascript">
	function clearValue(text){
		var txnVal = $(text).val();
		if(txnVal == "*Logo"||txnVal == "*SN"||txnVal == "*LOT"||txnVal == "*Ship To"||txnVal == "*Origin"||txnVal == "*Date"||
				txnVal == "*Customer"||txnVal == "*Title"||txnVal == "*Weight"||txnVal == "*Code"||txnVal == "*PCName"||txnVal == "*PCID"){
			$(text).val("");
			$(text).css("color","black");
		}
	}
	function checkValue(text){
		var key=$(text).attr('key');
		if($(text).val().length < 1){
			$(text).val(key);
			$(text).css("color","#999");
		}
	}
	//重新加载 条码页面
	function ssMenu(){
		 var datas = <%=datas%>;	 
		    
		 var lot=$("#lot").val();
		 var sn=$("#sn").val();  
		 var logo=$("#logo").val();
		 var shipto=$("#shipTo").val();
		 var origin=$("#origin").val();  
		 var title=$("#title").val();
		 var customer=$("#customer").val();
		 var date=$("#date").val();
		 var weight=$("#weight").val();
		 var code=$("#code").val();
		 var pcName=$("#pcName").val();
		 var pcid=$("#pcid").val();
		 
		 if(lot&&lot!="*LOT"){
			 datas.LOT = lot;
		 }
		 if(sn&&sn!="*SN"){
			 datas.SN=sn;
		 }
		 if(logo&&logo!="*Logo"){
			 datas.LOGO=logo;
		 }
		 if(shipto&&shipto!="*Ship To"){
			 datas.SHIPTO=shipto;
		 }
		 if(origin&&origin!="*Origin"){
			 datas.ORIGIN=origin;
		 }
		 if(title&&title!="*Title"){
			 datas.TITLE = title;
		 }
		 if(customer&&customer!="*Customer"){
			 datas.CUSTOMER = customer;
		 }
		 if(date&&date!="*Date"){
			 datas.DATE=date;
		 }
		 if(weight&&weight!="*Weight"){
			 datas.WEIGHT = weight;
		 }
		 if(code&&code!="*Code"){
			 datas.CODE = code;
		 }
		 if(pcName&&pcName!="*PCName"){
			 datas.PCNAME = pcName;
		 }
		 if(pcid&&pcid!="*PCID"){
			 datas.PCID = pcid;
		 }
		 
		var divs=$("div[name='lableContent']");
		var copy_html = assembleTemplate(datas,$('#lableContentCopy').html());
		for(var i=0;i<divs.length;i++){
			$(divs[i]).html(copy_html);//替换的内容按原来的格式，确保能替换
		}
	}
	
	function printBarCode(){
// 		 setPrintAll();//设置打印内容
		
		 var num=$('#productprintcount_<%=product!=null?product.get("pc_id",0l):0 %>').val();
		
		 var size='<%=lable_template.getString("lable_type")%>';
		 var str=size.split("X");
		 var lableWidth=str[0];
		 var lableHeight=str[1];
// 		 var lableWidth=str[0]+"mm";
// 		 var lableHeight=str[1]+"mm";
		 var printer='<%=lable_template.getString("print_name")%>';
		 //var printer='tsc';
		 var containPrinter=printer;
	
		  //获取打印机名字列表
	     var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
	     for(var i = 0;i<printer_count;i++){
	         if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){            
	           printerExist = "true";
	           containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
	           break;
	         }
	     }
	     visionariPrinter.PRINT_INITA(0,0,lableWidth,lableHeight,"Product Label");
		 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
	     visionariPrinter.SET_PRINT_PAGESIZE(1,lableWidth,lableHeight,size);
	     visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$('#lableContent').html());
	     visionariPrinter.SET_PRINT_COPIES(num);
// 	     visionariPrinter.PREVIEW();
	     visionariPrinter.PRINT();

	}
	

</script>