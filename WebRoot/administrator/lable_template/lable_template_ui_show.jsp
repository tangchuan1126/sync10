<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.exception.purchase.FileException"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.ContainerTypeKey"%>
<%@page import="com.cwc.app.key.LableTemplateKey"%>
<%@page import="com.cwc.app.key.LableTemplateProductCommonKey"%>

<%@ include file="../../include.jsp"%> 
<%
 	PageCtrl pc = new PageCtrl();
	pc.setPageSize(30);
	pc.setPageNo(1);
	String cmd = StringUtil.getString(request,"cmd");
	
	//获取登录人
	 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 	long login_id=adminLoggerBean.getAdid();
	
	if(login_id==100198){
		login_id=0;
	}
	//商品id
	long pc_id = StringUtil.getLong(request,"pc_id");
	//long pc_id = 1054699;
	DBRow product = productMgr.getDetailProductByPcid(pc_id);
	//商品模板标签：1; 容器模板标签：2 type:clp:ContainerTypeKey.CLP; tlp:ContainerTypeKey.TLP
	DBRow rows[];
	if(cmd.equals("products")){
		rows = customSeachMgrGql.getLableLableTemplateByType(pc_id, login_id, 1,0, request);//查询商品模板标签
	}else{
		rows = customSeachMgrGql.getLableLableTemplateByType(pc_id, login_id, 0,0, request);//查询所有的基础模板标签
	}
	long num=StringUtil.getLong(request,"num");//判断选显卡选中
	
	//采购单id
	long purchase_id = StringUtil.getLong(request,"purchase_id");
	
	//工厂类型
	String factory_type=StringUtil.getString(request,"factory_type");
	//供应商id
	long supplierSid=StringUtil.getLong(request,"supplierSid");
	
	//收货仓库
	String warehouse=StringUtil.getString(request,"warehouse");
	
	LableTemplateKey lableTemplateKey = new LableTemplateKey();
	
	//System.out.println("rows长度："+rows.length);
 %>
 <html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>

<style type="text/css">
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
.lableshow tr.alt td {
	background: white;
}

.lableshow tr.over td{
	background: #E6F3C5;
}
</style>
<style>
a:link {
	color: #999999;
	text-decoration: none;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: none;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: none;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: none;
	font-family: Arial;
	font-size:12px;
}

a.hard:link {
	color:#FF0066;
	text-decoration: none;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: none;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: none;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: none;
	font-family: Arial;
	font-size:14px;
}
</style>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 替换标签占位符 -->
<script type="text/javascript" src="../js/labelTemplate/assembleUtils.js"></script>

<script type="text/javascript">

jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	$("#tabs").tabs("select",<%=num%>);
});

function onTable(){
	$(".lableshow tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
	$(".lableshow tr:even").addClass("alt");
}


// $(function(){		
// 	var divs=$("div[name='lableContent']");
// 	for(var i=0;i<divs.length;i++){
// 		//var title_name=$('#title_name',divs[i]).val();
// 		var title_name=$("input[name='title_name']").val();
// 		var p_name=$("input[name='p_name']").val();
// 		var length=$("input[name='length']").val();
// 		var width=$("input[name='width']").val();
// 		var heigth=$("input[name='heigth']").val();
// 		var main_code=$("input[name='main_code']").val();
// 		var upc_code=$("input[name='upc_code']").val();
// 		var weight=$("input[name='weight']").val();
// 		var datas={
<%-- 			PCID:'<%=pc_id%>', --%>
// 			PCNAME:p_name,
// 			MAINCODE:main_code,
// 			UPCCODE:upc_code,
// 			LOT:'',
// 			LENGTH:length,
// 			WIDTH:width,
// 			HEIGHT:heigth,
// 			WEIGHT:weight,
// 			PRICE:'',
// 			TITLE:title_name,
// 			DATA:'',
// 			BILLNO:'',
// 			FACTORYNUM:''
// 		};
// 		$(divs[i]).html(assembleTemplate(datas,$(divs[i]).html()));
// 	}
// });

</script>


<style type="text/css">
	.aui_titleBar{  border: 1px #DDDDDD solid;}
	.aui_header{height: 40px;line-height: 40px;}
	.aui_title{
	  height: 40px;
	  width: 100%;
	  font-size: 1.2em !important;
	  margin-left: 20px;
	  font-weight: 700;
	  text-indent: 0;
	}

	td.categorymenu_td div{margin-top:-3px;}
	
	.ui-widget-content{
		border:1px #CFCFCF solid;
		border-radius: 0px;
	  	-webkit-border-radius: 0px;
		-moz-border-radius:0;
	}
	.ui-widget-header{
		background:#f1f1f1 !important;
		border-radius: 0;
		border-top:0;
		border-left:0;
		border-right:0;
		border-bottom: 1px #CFCFCF solid;
	}
	.ui-tabs .ui-tabs-nav li a{
		padding: .5em 1em 13px !important;
	}
	.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
		  height: 40px;
		  margin-top: -4px !important;
		background:#fff;
	}
	.ui-tabs .ui-tabs-nav{
		padding :0 .2em 0;
	}
	.ui-tabs{
		padding:0;
	}
	
	.ui-jqgrid{border:0;}
	.ui-jqgrid-titlebar.ui-widget-header{
		background:#fff !important;
		height:35px;
		line-height:30px;
	}
	
	 
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="onTable()" style="margin-top:10px;">
	<div id="tabs">
		<ul>
			<li><a href="#product_tab">Product Label</a></li>
<!--  			<li><a href="#box_tab">CLP Label</a></li>	   -->
		</ul>
		<div id="product_tab">
				<table width="98%" align="center" border="0" cellspacing="0" cellpadding="0">
					<%for(int i=0;i<rows.length;i++){
							if(rows[i].get("lable_template_type",0l)!=LableTemplateProductCommonKey.FIRST){
								continue;//商品签
							}
					%>  
						<input type="hidden" name="title_name" value="<%=rows[i].get("title_name","")%>" />
						<input type="hidden" name="p_name"  value="<%=rows[i].get("p_name","")%>" />
						<input type="hidden" name="length" value="<%=rows[i].get("length",0f)%>" />
						<input type="hidden" name="width" value="<%=rows[i].get("width",0f)%>" />
						<input type="hidden" name="heigth" value="<%=rows[i].get("heigth",0f)%>" />
						<input type="hidden" name="main_code" value="<%=rows[i].get("main_code",0l)%>" />
						<input type="hidden" name="upc_code" value="<%=rows[i].get("upc_code",0l)%>" />
						<input type="hidden" name="weight" value="<%=rows[i].get("weight",0f)%>" />
						<tr onclick="printPage(<%=rows[i].get("detail_lable_id",0l)%>,'<%=rows[i].get("title_name","")%>')" >
							<td>
								<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="lableshow">
									<tr>
										<td width="220" nowrap="nowrap" align="right" valign="middle" style="border-right:2px #eeeeee solid;border-bottom:2px #eeeeee solid;">
											<table align="left" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="right" style="padding-left: 15px;">Title:</td>
													<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].get("title_name","")%></strong></td>
												</tr>
												<tr>
													<td align="right" style="padding-left: 15px;">Printer Name:</td>
													<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].getString("print_name")%></strong></td>
												</tr>
												<tr>
													<td align="right" style="padding-left: 15px;">Label Name:</td>
													<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].getString("lable_name")%></strong></td>
												</tr>					
												<tr>
													<td align="right" style="padding-left: 15px;">Paper Size:</td>
													<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].get("lable_type","")%></strong></td>
												</tr>
											</table>
										</td>
										<td align="left" valign="middle" style="padding:10px;padding-left:25px;border-bottom:2px #eeeeee solid;">
											<div name="lableContent" style="background: #FFF;margin-left: 30px;margin-top: 30px;padding:0px;float: left;width:<%=rows[i].get("lable_width",0)%>px;height:<%=rows[i].get("lable_height",0)+10%>px;position:relative;text-align:center;border:1px solid red;" id="lableContent">
												<%=rows[i].get("lable_content","")%>
											</div>							
										</td>
									</tr>
								</table>
							</td>
						</tr>
					<%}%>
				</table>
		</div>
 		<%-- <div id="box_tab">
             <table width="98%" align="center" border="0" cellspacing="0" cellpadding="0">
				<%for(int i=0;i<rows.length;i++){
 						if(!(rows[i].get("lable_template_type",0l)==2&&rows[i].get("type",0l)==ContainerTypeKey.CLP)){//容器标签，且为CLP
  							continue;//外箱签 -->
  						}
 				%> 
					<tr onclick="printPage(<%=rows[i].get("detail_lable_id",0l)%>)" >
						<td>
							<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="lableshow">
									<tr>
										<td width="260" nowrap="nowrap" align="right" valign="middle" style="border-right:2px #eeeeee solid;border-bottom:2px #eeeeee solid;">
											<table align="left" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="right" style="padding-left: 15px;">Printer Name:</td>
													<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].getString("print_name")%></strong></td>
												</tr>
												<tr>
													<td align="right" style="padding-left: 15px;">Label Name:</td>
													<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].getString("lable_name")%></strong></td>
												</tr>					
												<tr>
													<td align="right" style="padding-left: 15px;">Paper Size:</td>
													<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].get("lable_type","")%></strong></td>
												</tr>
											</table>
										</td>
										<td align="left" valign="middle" style="padding:10px;padding-left:25px;border-bottom:2px #eeeeee solid;">
										<%=rows[i].get('lable_height',0)%><%=rows[i].get('lable_width',0)%>
											<div name="lableContent" style="background: #FFF;margin-left: 30px;margin-top: 30px;padding:0px;float: left;width:<%=rows[i].get("lable_width",0)%>px;height:<%=rows[i].get("lable_height",0)%>px;position:relative;text-align:center;border:1px solid red;" id="lableContent">
												<%=rows[i].get("lable_content","")%>

											</div>							
										</td>
									</tr>
								</table>
						</td>
					</tr>		
				<%}%>
				</table>
	    </div> --%>
	</div>
</body>
</html>
<script type="text/javascript">
		function printPage(lable_template_id,title_name){
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/lable_template/print_one_product_ui.html?pc_id=<%=pc_id%>&lable_template_id="+lable_template_id+"&purchase_id=<%=purchase_id%>&warehouse=<%=warehouse%>&factory_type=<%=factory_type%>&supplierSid=<%=supplierSid%>&title_name="+title_name;
			$.artDialog.open(uri , {title: "Print Label",width:'875px',height:'500px', lock: true,opacity: 0.3,fixed: true});
			
// 			var href;
<%-- 			href = "../lable_template/print_one_product_ui.html?pc_id=<%=pc_id%>&lable_template_id="+lable_template_id+"&purchase_id=<%=purchase_id%>&warehouse=<%=warehouse%>&factory_type=<%=factory_type%>&supplierSid=<%=supplierSid%>&title_name="+title_name; --%>
// 			window.location.href = href;
		}
		
		
</script>