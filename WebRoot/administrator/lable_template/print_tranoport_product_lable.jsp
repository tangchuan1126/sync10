<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.initconf.Resource"%>
<%@page import="com.cwc.app.key.LableTemplateKey"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<%

long lable_template_id = StringUtil.getLong(request,"lable_template_id");
DBRow lable_template = lableTemplateMgrZJ.getDetailLableTemplateById(lable_template_id);

//采购单
long purchase_id = StringUtil.getLong(request,"purchase_id");
long supplierSid=StringUtil.getLong(request,"supplierSid");

//转运单id
long transport_id=StringUtil.getLong(request,"transport_id");


String copies = StringUtil.getString(request,"copies");

DBRow[] rows = transportMgrZwb.getTransportProduct(transport_id);

DBRow[] options = lableTemplateMgrZJ.getAllLableTemplate(null);

Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));

StringBuffer pcids = new StringBuffer("");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>标签打印</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<script language="javascript">
function noNumbers(e)
	{
		var keynum
		var keychar
		var numcheck
		if(e.keyCode==8||e.keyCode==46||e.keyCode==37||e.keyCode==38||e.keyCode==39||e.keyCode==40)
		{
			return true;
		}
		else if(window.event) // IE
		{
		  keynum = e.keyCode
		}
		else if(e.which) // Netscape/Firefox/Opera
		{
		  keynum = e.which
		}
		keychar = String.fromCharCode(keynum)
		numcheck = /[^\d]/
		return !numcheck.test(keychar);
	}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 


function closeWin()
{
	tb_remove();
}
//-->

	function printProductLabel(pc_id,width,height)
	{
		printer = "LabelPrinter";
		paper = width+"X"+height;
		print_range_width = width;
		print_range_height = height;
		template_path = "lable_template/productbarcode.html";
													
							
		var counts = document.getElementById("productCount_"+pc_id).value;
		document.print_product_label.pc_id.value = pc_id;
		if(parseInt(document.getElementById("productprintcount_"+pc_id).value)!=document.getElementById("productprintcount_"+pc_id).value)
		{
			alert("打印数量有问题！");
		}
		else
		{
			document.print_product_label.copies.value = $("#productprintcount_"+pc_id).val();
			document.print_product_label.printer.value= printer;
			document.print_product_label.paper.value = paper;
			document.print_product_label.print_range_width.value = print_range_width;
			document.print_product_label.print_range_height.value = print_range_height;
			document.print_product_label.counts.value = counts;
							
			document.print_product_label.action = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+template_path;
			document.print_product_label.submit();
		}
	}
	
	function printBoxLabel(pc_id)
	{
		var counts = document.getElementById("productCount_"+pc_id).value;
		
		if(parseFloat(document.getElementById("productCount_"+pc_id).value)!=document.getElementById("productCount_"+pc_id).value)
		{
			alert("装箱数量有误！");
		}
		else if((parseInt(document.getElementById("boxprintcount_"+pc_id).value))!=document.getElementById("boxprintcount_"+pc_id).value)
		{
			alert("打印数量有误！");
		}
		else
		{
			document.print_box_label.pc_id.value = pc_id
			document.print_box_label.copies.value = $("#boxprintcount_"+pc_id).val();
			document.print_box_label.counts.value = counts;
						
			document.print_box_label.submit();
		}
	}
	
	function ajaxLoadPurchasePage(url,pc_id,factoryId)
		{
			var counts = $("#productCount_"+pc_id).val();
			var para = "print_range_width=<%=lable_template.getString("print_range_width")%>&print_range_height=<%=lable_template.getString("print_range_height")%>&printer=<%=lable_template.getString("printer")%>&paper=<%=lable_template.getString("paper")%>&factoryId=<%=supplierSid%>&purchaseId=<%=purchase_id%>&factoryNum="+factoryId+"&pc_id="+pc_id+"&counts="+counts;
			$.ajax({
				url:url,
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request)
				{
					
				},
				
				error: function()
				{
					
				},
				
				success: function(html)
				{
					$("#"+pc_id).html(html);
				}
			});
		}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="">
<div align="center">
<%
	for(int i=0;i<rows.length;i++){
		DBRow product = rows[i];
		pcids.append(product.getString("transport_pc_id"));
		if(i<rows.length-1){
			pcids.append(",");
		}		
%>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <th width="140" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
		<th width="340" style="vertical-align: center;text-align: center;" class="right-title">商品标签</th>
        <th width="120" style="vertical-align: center;text-align: center;" class="right-title">打印数量</th>
    </tr>
    <tr> 
      <td height="150px" align="center" valign="middle"   style="font-size:14px;">
        <%=product.getString("transport_p_name")%>
      </td>
	  <td id="<%=product.get("transport_pc_id",0l)%>" align="center" valign="middle" style="font-size:14px;">
	      <% DBRow dbr=mgrZwb.getFactorType(purchase_id,product.get("transport_pc_id",0l)); %>
      	<script type="text/javascript">
	 			ajaxLoadPurchasePage("<%=ConfigBean.getStringValue("systenFolder")+"administrator/"+lable_template.getString("template_path")%>",<%=product.get("transport_pc_id",0l)%>,"<%=dbr.getString("factory_type")%>");
	 	</script>  
	  </td>
      <td align="center" valign="middle" nowrap="nowrap" >
      	<input style="width:25px;" maxlength="3" type="text" onKeyPress="return noNumbers(event)" id="productprintcount_<%=product.get("transport_pc_id",0l) %>" name="productprintcount_<%=product.get("transport_pc_id",0l) %>" value="1"/>
      </td>
	 </tr>
</table>
<%
	}
%>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" style="padding-top: 10px;padding-bottom: 15px;border-top: 1px #eeeeee solid" bgcolor="#eeeeee">
			<%
				if(lable_template.get("lable_type",0)!=LableTemplateKey.SEQUENCELABEL)
				{
			%>
				<input type="button" class="long-button-print" onclick="printBarcodes()" value="打印"/>
			<%
				}
				else
				{
			%>
				<input type="button" class="long-button-print" onclick="subSerialNumber()" value="打印序列号"/>	
			<%
				}
			%>
		</td>
	</tr>
</table>
</div>
<form action="serialNumber.html" name="madeSerialNumber_form">
	<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="pcids" id="pcids" value="<%=pcids%>"/>
	<input type="hidden" name="pccounts" id="pccounts"/>
</form>
<script type="text/javascript">
	function printBarcodes()
	{
		var pcids = "<%=pcids%>";
		var pcid = pcids.split(",");
		var print = true;
		var copies = "<%=copies%>"
		var copie = copies.split(",");
		var errorid;
		for(c=0;c<pcid.length;c++)
		{
			if(parseInt($("#productprintcount_"+pcid[c]).val())!=$("#productprintcount_"+pcid[c]).val())
			{
				print = false;
				errorid = pcid[c];
				break;
			}
		}
		if(print)
		{
			

					//获取打印机名字列表
			    	   var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
			    	 //判断是否有该名字的打印机
				       var printer = "<%=lable_template.getString("printer")%>";				
					   var printerExist = "false";
					   var containPrinter = printer;
						for(var i = 0;i<printer_count;i++){
							if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 )
							{
								printerExist = "true";
								containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
								break;
							}
						}
						if(printerExist=="true"){
							//判断该打印机里是否配置了纸张大小
							var paper="<%=lable_template.getString("paper")%>";
						    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
						    var str=strResult.split(",");
						    var status=false;
						    for(var i=0;i<str.length;i++){
			                       if(str[i]==paper){
			                          status=true;
			                       }
							}
						    if(status==true){	
						    	for(q=0;q<pcid.length;q++)
								{
									if(document.getElementById(pcid[q])!=null)
									{		
								    	visionariPrinter.PRINT_INITA(0,0,"<%=Float.parseFloat(lable_template.getString("print_range_width"))%>mm","<%=Float.parseFloat(lable_template.getString("print_range_height"))%>mm","商品标签");
								    	visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印
				                        visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"<%=lable_template.getString("paper")%>");            
								    	visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%","<body leftmargin='0' topmargin='0'>"+document.getElementById(pcid[q]).innerHTML+"</body>");
										visionariPrinter.SET_PRINT_COPIES($("#productprintcount_"+pcid[q]).val());
										//visionariPrinter.PREVIEW();
										visionariPrinter.PRINT();
									}	
								}
						    }else{
						    	$.artDialog.confirm("打印机配置纸张大小里没有符合的大小.....", function(){
		              				 this.close();
		                  			}, function(){
		              			});
						    }
						}else{
											
							var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
							if(op!=-1){ //判断是否点了取消
								for(q=0;q<pcid.length;q++)
								{
									if(document.getElementById(pcid[q])!=null)
									{	
									visionariPrinter.PRINT_INITA(0,0,"<%=Float.parseFloat(lable_template.getString("print_range_width"))%>mm","<%=Float.parseFloat(lable_template.getString("print_range_height"))%>mm","商品标签");
			                        visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"<%=lable_template.getString("paper")%>");            
							    	visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%","<body leftmargin='0' topmargin='0'>"+document.getElementById(pcid[q]).innerHTML+"</body>");
									visionariPrinter.SET_PRINT_COPIES($("#productprintcount_"+pcid[q]).val());
									//visionariPrinter.PREVIEW();
									visionariPrinter.PRINT();
									}	
								}
							}
						}
			
		}
		else
		{
			alert("打印数量异常，请修改");
			$("#productprintcount_"+errorid).focus();
		}
	}
	
	function serialnumber()
			{
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
			
				var pcids = "<%=pcids%>";
				var pcid = pcids.split(",");

				var pccounts = "";
				for(q=0;q<pcid.length;q++)
				{
					if(document.getElementById(pcid[q])!=null)
					{
						pccounts += $("#productprintcount_"+pcid[q]).val();
					}
					if(q<pcid.length-1)
					{
						pccounts +=",";
					}
				}
				
				var para = "purchase_id=<%=purchase_id%>&pcids=<%=pcids%>&pccounts="+pccounts
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/serialNmuber/madeSerialNumber.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(data){
						alert("系统错误，请您稍后再试");
					},
					
					success: function(data){
						if(data.close == "true")
						{
							$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">创建成功！</span>' });
							$.unblockUI();
						}
						else if(data.close == "error")
						{
							$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green"><%=Resource.getStringValue("","NumberIndexOfExption","")%></span>' });
						}
						
					}
				});
			}
			
			function subSerialNumber()
			{
				var pcids = "<%=pcids%>";
				var pcid = pcids.split(",");

				var pccounts = "";
				for(q=0;q<pcid.length;q++)
				{
					
					pccounts += $("#productprintcount_"+pcid[q]).val();
					
					if(q<pcid.length-1)
					{
						pccounts +=",";
					}
				}
				document.madeSerialNumber_form.pccounts.value = pccounts;
				document.madeSerialNumber_form.submit();
			}
</script>
</body>
</html>
