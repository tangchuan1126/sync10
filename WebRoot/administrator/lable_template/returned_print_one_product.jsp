<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.LableTemplateKey"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<head>

<%
	//商品id
	long pc_id = StringUtil.getLong(request,"pc_id");		//商品Id
	String p_name = StringUtil.getString(request,"p_name");			//商品名称
	String fix_rpi_id = StringUtil.getString(request,"fix_rpi_id") ; //明细Ids
	long  rp_id = StringUtil.getLong(request,"rp_id") ; //主单据ID
	
%>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../common.js"></script>

<script type="text/javascript" src="../js/poshytip/jquery-1.4.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 


<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<title>退货标签</title>
<script>
 
</script>
</head>

<body onLoad="ajaxLoadPurchasePage('')">
<div align="center">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	    <tr>
	        <th width="240" align="left" class="right-title"  style="vertical-align: center;text-align: center;">图片明细</th>
			<th width="240" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
	        <th width="120" style="vertical-align: center;text-align: center;" class="right-title">打印数量</th>
	        <th width="120" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
	    </tr>
	    <%
	    	if(fix_rpi_id.trim().length() > 0 ){
	    		String[] array = fix_rpi_id.split(",");
	    		for(String tempfixRpiid : array){
	    			%>
	    			 <tr> 
				      <td height="100" valign="middle"   style="font-size:14px;border-bottom:1px #eeeeee solid;text-align:center;"><%=tempfixRpiid %>&nbsp;</td>
					  <td align="center" valign="middle" style="font-size:14px;border-bottom: 1px #eeeeee solid;"><%=p_name %>&nbsp;</td>
				      <td align="center" valign="middle" nowrap="nowrap" style="border-bottom:1px #eeeeee solid;">	 
			      		 <input style="width:25px;" maxlength="3" type="text" onKeyPress="return noNumbers(event)" id="productprintcount_<%=tempfixRpiid %>" name="productprintcount_<%=tempfixRpiid %>" value="1"/>
			     	  </td>
				      </td>
					  <td style="border-bottom: 1px #eeeeee solid;">
					  	<input type="button" class="long-button-print" value="打印" onclick="printBarCode('<%=tempfixRpiid %>')"/><br/><br/>
					  	 
					  </td>
					 </tr>
	    			<%
	    		}
	    		
	    	}
	    %>
	   
		 
		 <tr>
		 	<td colspan="5" id="<%=pc_id%>" align="center" valign="bottom" style="padding-top: 20px">
		 		
		 	</td>
		 </tr>
	</table>
	 
</div>
</body>
</html>
<script>

function ajaxLoadPurchasePage(text){
 
    var obj = {rp_id:'<%= rp_id%>',
		p_name:'<%= p_name%>',
		fix_rpi_id:'<%=fix_rpi_id%>',
		pc_id:'<%= pc_id%>',
		textval:text
	  }
	var url="<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/template/returnedProductSmallLabel.html";
	$.ajax({
		url:url,
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:jQuery.param(obj),
		beforeSend:function(request){
	
		},	
		error: function(){
			
		},
		success: function(html){
			$("#<%=pc_id%>").html(html);
		}
	});
}

function printBarCode(fixRpiId){

	 	   //获取打印机名字列表
	    	var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
		    //判断是否有该名字的打印机
	    	var printer = "60X30";
	    	var printerExist = "false";
	    	var containPrinter = printer;
			for(var i = 0;i<printer_count;i++){
				if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 ){
					printerExist = "true";
					containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
					break;
				}
			}
			if(printerExist=="true"){
					//判断该打印机里是否配置了纸张大小
					var paper="60X30";
				    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
				    var str=strResult.split(",");
				    var status=false;
				    for(var i=0;i<str.length;i++){
	                       if(str[i]==paper){
	                          status=true;
	                       }
					}
				    if(status==true){
                       	 visionariPrinter.PRINT_INITA(0,0,"60mm","30mm","商品标签");
                       	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"60X30");
   	   				     visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%","<body leftmargin='0' topmargin='0'>"+document.getElementById("container_"+fixRpiId).innerHTML+"</body>");
   	   					 visionariPrinter.SET_PRINT_COPIES($("#productprintcount_"+fixRpiId).val());
   	   				    // visionariPrinter.PREVIEW();
   	   					 visionariPrinter.PRINT();
                   }else{
                   	$.artDialog.confirm("打印机配置纸张大小里没有符合的大小.....", function(){
            				 this.close();
                			}, function(){
            			});
                   }	
			}else{
				var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
				if(op!=-1){ //判断是否点了取消
					 visionariPrinter.PRINT_INITA(0,0,"60mm","30mm","商品标签");
                     visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"60X30");
   				     visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%","<body leftmargin='0' topmargin='0'>"+document.getElementById("container_" + fixRpiId).innerHTML+"</body>");
   					 visionariPrinter.SET_PRINT_COPIES($("#productprintcount_"+fixRpiId).val());
   				     //visionariPrinter.PREVIEW();
   					 visionariPrinter.PRINT();
				}	
			}
}
</script>