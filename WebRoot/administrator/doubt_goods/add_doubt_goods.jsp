<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.util.Config"%>
<%@ include file="../../include.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>疑问商品采集</title>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	
	<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
	<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- 在线图片预览 -->
	 <script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	 
  	<script type="text/javascript">
  		
  		
     
    //jquery file up 回调函数
      function uploadFileCallBack(fileNames,target){
	  $("input[name='file_names']").val(fileNames);
          if(fileNames.length >0)
          {
              $("input[name='file_names']").val(fileNames);
              var fileNameArray = fileNames.split(",");
              var showImg = "";
              for(var i = 0;i<fileNameArray.length;i++)
              {
              	showImg +="<a>"+fileNameArray[i]+"</a><br/>"
              }
          	  $("#show_img").html(showImg);
          	  $("#submit_button").html("<input type=\"submit\" value=\"添加疑问商品\" class=\"long-button\"/>");
      	  }
          
      }
      function onlineScanner(){
    	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
    	 $.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
      }
  	</script>
  </head>
  
  <body onload = "onLoadInitZebraTable()">
   	<!-- 支持多个图片同时上传 ,下面显示图片(用轮播器显示)-->
   	<form id="myform"   method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/doubtGoods/addDoubtGoods.action" %>'>
   		<table>
   			<tr>
   				<td width="416" style="padding-left:10px;">
		             <ul id="productLinemenu" class="mcdropdown_menu">
		             <li rel="0">所有产品线</li>
		             <%
						  DBRow p1[] = productLineMgrTJH.getAllProductLine();
						  for (int i=0; i<p1.length; i++)
						  {
								out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
								 
								out.println("</li>");
						  }
					  %>
		           </ul>
		           <input type="text" name="productLine" id="productLine" value="" />
		           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
			</td>
   				<td>
   					<input type="hidden" name="file_names" />
   				    <input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
   				</td>
   				<td id="submit_button">
   					
   				</td>
   			</tr>
   		</table>
   		<div id="show_img">
   			
   		</div>
   	</form>	
  </body>
  <script type="text/javascript">
  	$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
				}
	});
	
	$("#productLine").mcDropdown("#productLinemenu").setValue(0);
  </script>
</html>