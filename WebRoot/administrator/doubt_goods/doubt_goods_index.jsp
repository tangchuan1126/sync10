<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="javax.mail.Transport"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%
	String cmd = StringUtil.getString(request,"cmd");
	long dg_id = StringUtil.getLong(request,"dg_id");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
	int is_answerd = StringUtil.getInt(request,"is_answerd");
	
	PageCtrl pc = new PageCtrl();
	pc.setPageSize(30);
	pc.setPageNo(StringUtil.getInt(request,"p"));
	
	DBRow[] doubtGoods; 
	if(cmd.equals("search"))
	{
		doubtGoods = new DBRow[1];
		doubtGoods[0] = doubtGoodsMgrZJ.getDetailDoubtProduct(dg_id);
	}
	else
	{
		doubtGoods = doubtGoodsMgrZJ.filterDoubtProduct(pro_line_id,is_answerd,pc);
	}
	
	
	long lable_template_id = StringUtil.getLong(request,"lable_template_id",100012l);
	DBRow lable_template = lableTemplateMgrZJ.getDetailLableTemplateById(lable_template_id);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>疑问商品列表</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript">
	function addDoubtGoods()
	{
		$.artDialog.open("add_doubt_goods.html", {title: "疑问商品采集",width:'870px',height:'500px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function answerDoubtGoods(dg_id)
	{
		$.artDialog.open("answer_doubt_goods.html?dg_id="+dg_id, {title: "回答疑问商品",width:'870px',height:'500px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function showImg(file_name)
	{
		$.artDialog.open("<%=ConfigBean.getStringValue("systenFolder")%>upload/doubt_goods/"+file_name, {title: "临时看图",width:'870px',height:'500px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function filter()
	{
		document.filter_form.pro_line_id.value = $("#filter_productLine").val();
		document.filter_form.is_answerd.value = $("#is_answerd").val();
		document.filter_form.submit();
	}
	
	function showPictrueOnline(fileWithType,fileWithId ,currentName,productFileType,pcId)
	{
   		var obj = {
	   		file_with_type:fileWithType,
	   		file_with_id : fileWithId,
	   		current_name : currentName ,
	   		product_file_type:productFileType,
	   		pc_Id : pcId,
	   		cmd:"multiFile",
	   		table:'product_file',
	   		base_path:'/paypal/' + "upload/"+'product'
		}
	   	if(window.top && window.top.openPictureOnlineShow)
	   	{
			window.top.openPictureOnlineShow(obj);
		}
		else
		{
		   	openArtPictureOnlineShow(obj,'/paypal/');
		}
	}	
	
	function ssMenu(dg_id,pc_id)
	{
		
	  var uri = "<%=ConfigBean.getStringValue("systenFolder")+"administrator/"+lable_template.getString("template_path")%>";
	  var counts = 1;
	  var para = "print_range_width=<%=lable_template.getString("print_range_width")%>&print_range_height=<%=lable_template.getString("print_range_height")%>&printer=<%=lable_template.getString("printer")%>&paper=<%=lable_template.getString("paper")%>&pc_id="+pc_id+"&dg_id="+dg_id+"&counts="+counts;
	  
	  
		$.ajax({
			url:uri,
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request)
			{			
			},		
			error: function()
			{			
			},		
			success: function(html)
			{
				$("#doubt_label").html(html);
				
				printDoubtLabel();
			}
		});
	  
	}
	
	function afterAddPrint(dg_id)
	{
		
	  var uri = "<%=ConfigBean.getStringValue("systenFolder")+"administrator/"+lable_template.getString("template_path")%>";
	  var counts = 1;
	  var para = "print_range_width=<%=lable_template.getString("print_range_width")%>&print_range_height=<%=lable_template.getString("print_range_height")%>&printer=<%=lable_template.getString("printer")%>&paper=<%=lable_template.getString("paper")%>&dg_id="+dg_id+"&counts="+counts;
	  
	  
		$.ajax({
			url:uri,
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request)
			{			
			},		
			error: function()
			{			
			},		
			success: function(html)
			{
				$("#doubt_label").html(html);
				printDoubtLabel();
				
				window.location.reload();
			}
		});
	  
	}
	
	function printDoubtLabel()
	{
		var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
		var printer = "<%=lable_template.getString("printer")%>";
		
		var printerExist = "false";
		
		for(var i = 0;i<printer_count;i++)
		{
			if(printer==visionariPrinter.GET_PRINTER_NAME(i))
			{
			
				printerExist = "true";
				break;
			}
		}
		
		if(printerExist=="false")
		{
			printer = visionariPrinter.SELECT_PRINTER();
			if(printer !=-1)
			{
				printer=visionariPrinter.GET_PRINTER_NAME(printer);
				var strResult=visionariPrinter.GET_PAGESIZES_LIST(printer,",");
				visionariPrinter.PRINT_INITA(0,0,"<%=Float.parseFloat(lable_template.getString("print_range_width"))%>mm","<%=Float.parseFloat(lable_template.getString("print_range_height"))%>mm","商品标签");
				visionariPrinter.SET_PRINTER_INDEXA (printer);//指定打印机打印  
				visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"<%=lable_template.getString("paper")%>");
				visionariPrinter.ADD_PRINT_HTM(0,"0.5mm","100%","100%","<body leftmargin='0' topmargin='0' border='0'>"+document.getElementById("doubt_label").innerHTML+"</body>");
				visionariPrinter.SET_PRINT_COPIES(1);
				//visionariPrinter.PREVIEW();
				visionariPrinter.PRINT();		
			}
		}
		else
		{
			visionariPrinter.PRINT_INITA(0,0,"<%=Float.parseFloat(lable_template.getString("print_range_width"))%>mm","<%=Float.parseFloat(lable_template.getString("print_range_height"))%>mm","商品标签");
			visionariPrinter.SET_PRINTER_INDEXA (printer);//指定打印机打印  
			visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"<%=lable_template.getString("paper")%>");
			visionariPrinter.ADD_PRINT_HTM(0,"0.5mm","100%","100%","<body leftmargin='0' topmargin='0'>"+document.getElementById("doubt_label").innerHTML+"</body>");
			visionariPrinter.SET_PRINT_COPIES(1);
			//visionariPrinter.PREVIEW();
			visionariPrinter.PRINT();
		}
	}
	
	function make_tab(productId)
	 {
		var purchase_id="";
		var factory_type="";
		var supplierSid="";
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/lable_template_show.html?purchase_id='+purchase_id+'&pc_id='+productId+'&factory_type='+factory_type+'&supplierSid='+supplierSid; 
		$.artDialog.open(uri , {title: '标签列表',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	 }
	 
	 function searchDoubtGoods()
	 {
	 	document.search_form.submit();
	 }
</script>
<style type="text/css">
	.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>
</head>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<form name="filter_form" action="">
	<input type="hidden" name="pro_line_id"/>
	<input type="hidden" name="is_answerd"/>
</form>
<div id="doubt_label" style="display:none">
</div>
<br>
<div id="tabs">
		<ul>
			<li><a href="#filter_tab">常用工具</a></li>
			<li><a href="#search_tab">搜索工具</a></li>		 
		</ul>
		<div id="filter_tab">
			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr> 
			       <td width="54%" height="33" >
						  <table width="100%" border="0" cellspacing="0" cellpadding="5">
				            <tr>
				              <td width="416" style="padding-left:10px;padding-top:10px;">
						             <ul id="productLinemenu" class="mcdropdown_menu">
						             <li rel="0">所有产品线</li>
						             <%
										  DBRow p1[] = productLineMgrTJH.getAllProductLine();
										  for (int i=0; i<p1.length; i++)
										  {
												out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
												 
												out.println("</li>");
										  }
									  %>
						           </ul>
						           <input type="text" name="productLine" id="productLine" value="" />
						           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
						           &nbsp;&nbsp;
							</td>
							<td width="140" style="padding-left:10px;">
								<select id="is_answerd" style="height:40px;width:120px;font-size:xx-large;">
						           		<option value="0" <%=is_answerd==0?"selected=\"selected\"":""%>>全部</option>
						           		<option value="1" <%=is_answerd==1?"selected=\"selected\"":""%>>未回答</option>
						           		<option value="2" <%=is_answerd==2?"selected=\"selected\"":""%>>已回答</option>
						           </select>
							</td>
				            <td width="317" height="30">
				          		<input name="Submit4" type="button" class="button_big_refresh" onClick="filter()">
				          		&nbsp;&nbsp;&nbsp;&nbsp;
				          		<input type="button" class="button_big_doubt_good" onclick="addDoubtGoods()"/>			  
				        	</td>
				 		  </tr>
				       </table>
				</td>
			  </tr>
			</table>
		</div>
		<div id="search_tab">
			<div id="easy_search_father">
				<div id="easy_search"><a href="javascript:searchDoubtGoods()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
			</div>
			<form method="post" name="search_form">
				<table width="485" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="418">
							<div  class="search_shadow_bg">
								<input type="text" name="dg_id" id="dg_id" value="<%=dg_id==0?"":dg_id%>" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" onkeydown="if(event.keyCode==13)searchDoubtGoods()"/>
							</div>
						</td>
						<td width="67">
								 
						</td>
					</tr>
				</table>
				<input type="hidden" name="cmd" value="search"/>
			</form>
		</div>
</div>
		
<br/>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		<tr>
			<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">疑问编号</th>
			<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">产品线</th>
			<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">创建人</th>
			<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">创建时间</th>
			<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">回答人</th>
			<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">回答时间</th>
			<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">商品名</th>
			<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
		</tr>
		<%
			for(int i = 0;i<doubtGoods.length;i++)
			{
		%>
		<tr>
			<td height="30px;">
				<%=doubtGoods[i].get("dg_id",0l)%>
			</td>
			<td style="font-size: 15px;">
				<%
					DBRow productLine = productLineMgrTJH.getProductLineById(doubtGoods[i].get("product_line_id",0l));
					if(productLine!=null)
					{
						out.print(productLine.getString("name"));
					}
					else
					{
						out.print("&nbsp;");
					}
				%>
			</td>
			<td style="font-size: 15px;">
				<%
					DBRow createAdmin = adminMgr.getDetailAdmin(doubtGoods[i].get("create_adid",0l));
					if(createAdmin!=null)
					{
						out.print(createAdmin.getString("employe_name"));
					}
				%>
			</td>
			<td>
				<%=doubtGoods[i].getString("create_time").substring(2,19)%>
			</td>
			<td style="font-size: 15px;">
				<%
					DBRow answerAdmin = adminMgr.getDetailAdmin(doubtGoods[i].get("answer_adid",0l));
					if(answerAdmin!=null)
					{
						out.print(answerAdmin.getString("employe_name"));
					}
					else
					{
						out.print("&nbsp;");
					}
				%>
			</td>
			<td>
				<%=doubtGoods[i].getString("answer_time").length()>1?doubtGoods[i].getString("answer_time").substring(2,19):"&nbsp;"%>
				&nbsp;
			</td>
			<td>
				<%=doubtGoods[i].getString("p_name","&nbsp;")%>
				
			</td>
			<td>
				<input type="button" class="short-button" value="回答" onclick="answerDoubtGoods('<%=doubtGoods[i].get("dg_id",0l)%>')"/>
				&nbsp;&nbsp;
				<input type="button" class="short-button" value="打印疑问签" onclick="ssMenu(<%=doubtGoods[i].get("dg_id",0l)%>,<%=doubtGoods[i].get("pc_id",0l)%>)"/>
				<%
					if(answerAdmin!=null)
					{
						out.print("<input type='button' class='short-short-button-convert' value='商品标签' onClick=\"make_tab("+doubtGoods[i].get("pc_id",0l)+")\"  />");
					}
				%>
			</td>
		</tr>
		<%
			}
		%>
	</table>
</form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
    <input type="hidden" name="p" >
	<input type="hidden" name="pro_line_id" value="<%=pro_line_id%>"/>
	<input type="hidden" name="is_answerd" value="<%=is_answerd%>"/>
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle">
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<script type="text/javascript">
$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
		cookie: { expires: 30000 } ,
	});
	
function ref()
{
	window.location.reload();
}

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
				}
});

<%
if (pro_line_id!=0)
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue('<%=pro_line_id%>');
<%
}
else
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue(0);
<%
}
%>

function cleanProductLine(divId)
{
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"filter_name").val("");
}
</script>
</body>
</html>
