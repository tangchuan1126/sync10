<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.util.Config"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@ include file="../../include.jsp"%>
<%@page import="java.util.*"%>
<% 
	long dg_id = StringUtil.getLong(request,"dg_id");
	
	DBRow doubtGoods = doubtGoodsMgrZJ.getDetailDoubtProduct(dg_id);
	
	DBRow[] fileNames = fileMgrZJ.getFileByWithIdAndType(dg_id,FileWithTypeKey.DOUBTGOODS);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>疑问商品回答</title>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	
	<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
	<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- 在线图片预览 -->
	<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
	
	<script type="text/javascript" src="../js/picture_online_show/jquery.opacityrollover.js"></script>
	<script type="text/javascript" src="../js/picture_online_show/jquery.prettyPhoto.js"></script>
	<script type="text/javascript" src="../js/picture_online_show/jquery.smoothZoom.min.js"></script>
	<link type="text/css" href="../js/picture_online_show/css/prettyPhoto.css" rel="stylesheet" />

	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	
	<script type="text/javascript">
		$().ready(function() {
			addAutoComplete($("#p_name"),
					"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
					"merge_info",
					"p_name");
		});
		
		function checkPname()
		{
			if($("#p_name").val().length>0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function imageLoad(_this)
		{
			var node = $(_this);
			var image = new Image();
			image.src = node.attr("src");
			image.onload = function(){
				var imageHeight =  image.height ;
				var imageWidth = image.width;
				 
				var changeHeight = imageHeight * (230 /imageWidth );
				node.attr("height",changeHeight+"px");
				var liNode = (node.parent().parent().parent());
				var spanSize = $(".size_info",liNode).html(imageWidth+"x"+imageHeight);
			};
		}
		
		function showPictrueOnline(currentName)
		{
		    var obj = {
				file_with_type:<%=FileWithTypeKey.DOUBTGOODS%>,
				file_with_id :<%=dg_id%>,
				current_name : currentName ,
				cmd:"multiFile",
				table:'file',
				base_path:'<%=ConfigBean.getStringValue("systenFolder")%>/upload/doubt_goods'
			}
		    if(window.top && window.top.openPictureOnlineShow)
		    {
				window.top.openPictureOnlineShow(obj);
			}
			else
			{
			    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
			}
		}
		function unableIdentify()
		{
			if(confirm("你确定此物我们无法识别它是什么吗？"))
			{
				document.unable_identify_form.submit();
			}
			
		}
		
		
	</script>
  </head>
  
  <body onload = "onLoadInitZebraTable()">
  	<form name="unable_identify_form"   method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/doubtGoods/answerDoubtGoods.action"%>'">
  		<input type="hidden" value="1" name="unable_identify"/>
  		<input type="hidden" name="dg_id" value="<%=dg_id%>"/>
  	</form>
   	<!-- 支持多个图片同时上传 ,下面显示图片(用轮播器显示)-->
   	<form id="myform"   method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/doubtGoods/answerDoubtGoods.action"%>' onsubmit="return checkPname();">
   		确认商品:<input type="text" name="p_name" id="p_name" value="<%=doubtGoods.getString("p_name")%>" style="width:200px;"/>&nbsp;&nbsp;&nbsp;
   		<input type="submit" value="确认商品" class="short-button"/>
   		&nbsp;&nbsp;&nbsp;
   		<input type="submit" value="无法识别" class="short-button" onclick="unableIdentify()"/>
   		<input type="hidden" name="dg_id" value="<%=dg_id%>"/>
   		<div id="show_img" style="padding-top: 15px;">
   			<%
   				for(int i = 0;i<fileNames.length;i++)
   				{
   			%>
   				<a class="image_a" rel="prettyPhoto[gallery2]" href="<%=ConfigBean.getStringValue("systenFolder")+"upload/doubt_goods/"+fileNames[i].getString("file_name")%>" title="<%=fileNames[i].getString("file_name")%>">
   					<img src='<%=ConfigBean.getStringValue("systenFolder")+"upload/doubt_goods/"+fileNames[i].getString("file_name")%>' onload="imageLoad(this)" width="230px"/>	
   				</a>&nbsp;&nbsp;&nbsp;&nbsp;
   			<%
   				}
   			%>
   		</div>
   	</form>
   	<script type="text/javascript">
   		var canvasWidth =  806;
		var canvasHeight = 447;
	 	if($(window).width()* 1 < 1000){
	 	   canvasWidth = 600;
	 	  canvasHeight = 347;
	 	}
		$("a.image_a").prettyPhoto({
			default_width: canvasWidth,
			default_height: canvasHeight,	
			 slideshow:false, /* false OR interval time in ms */
			autoplay_slideshow: false, /* true/false */
			opacity: 0.50, /* opacity of background black */
			theme: 'facebook', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
			modal: true, /* If set to true, only the close button will close the window */	
			overlay_gallery: false,
			changepicturecallback: setZoom,
			callback: closeZoom,
			social_tools: false,
			image_markup: '<div style="width:'+canvasWidth+'px; height:'+canvasHeight+'px;"><img id="fullResImage" src="{path}" /></div>',
			fixed_size: true,
			
			/******************************************
			Enable Responsive settings below if needed.
			Max width and height values are optional.
			******************************************/
			responsive: false,
			responsive_maintain_ratio: true,
			max_WIDTH: '',
			max_HEIGHT: ''
		});		
		 
		function setZoom (){
			$('#fullResImage').smoothZoom('destroy').smoothZoom();
		}
	
		function closeZoom (){
			$('#fullResImage').smoothZoom('destroy');
		}
   	</script>
  </body>
</html>