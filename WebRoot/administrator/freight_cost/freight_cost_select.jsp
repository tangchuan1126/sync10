<%@ page language="java" pageEncoding="utf-8"%>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<%@ include file="../../include.jsp"%> 

<%
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(20);
	
	DBRow[] rows = freightMgrLL.getAllFreightCost();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>运输资源选择</title>    
    
	<link href="../comm.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" src="../../common.js"></script>

	<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	<script type="text/javascript" src="../js/select.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	function closeWindow(){
		$.artDialog.close();
		//self.close();
	}
	function changeIndex(obj,fc_id)
	{
		var para = "fc_id="+fc_id+"&freight_index="+obj.value; 
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/freight_cost/changeFreightIndexJSONAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data){
					if(data["return"]=="false") {
						alert("保存失败!");
					}
				}
			});
	}
	</script>
  </head>
  
  <body onload="onLoadInitZebraTable()">
    <div id="freightCostList">
  <table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" style="padding-top: 17px" class="zebraTable" id="stat_table">
     <tr>
       <th width="6%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
       <th width="5%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
       <th width="5%" nowrap="nowrap" class="right-title"  style="text-align: center;">顺序</th>
       <th width="6%" nowrap="nowrap" class="right-title" style="text-align: center;">&nbsp;</th>

     </tr>
     <%
     if(rows!=null){
	     for(int i = 0;i<rows.length;i++)
	     {
	    	 DBRow row = rows[i];
	    	 long fc_id = row.get("fc_id",0l);
	    	 String fc_project_name = row.getString("fc_project_name");
	    	 String fc_way = row.getString("fc_way");
	    	 String fc_unit = row.getString("fc_unit");
      %>
     <tr height="80px" style="padding-top:3px;">
		<td align="center" valign="middle" style="padding-top:3px;">
			<%=fc_project_name %>	
		</td>
        <td align="center" valign="middle" nowrap="nowrap" >       
      		<%=fc_way %>
        </td>
        <td align="left" valign="middle">
			<%=fc_unit %>
        </td>
        <td align="center" valign="middle">
			<input type="text" value="<%=row.getString("freight_index") %>" name="freight_index" id="freight_index" size="3" onblur="changeIndex(this,'<%=fc_id %>')"/>
        </td>
		<td align="left" valign="middle" style="padding-top: 5px;padding-bottom: 5px" nowrap="nowrap">
			<input type="button" class="short-short-button-mod" onclick="$.artDialog.opener.setFreightCost(<%=fc_id %>,'<%=fc_project_name %>','<%=fc_way %>','<%=fc_unit %>');closeWindow();" value="选择"/><br/><br/>
		</td>
     </tr>
     <%
    	 }
     }
     %>
  </table>
</div>
  </body>
</html>
