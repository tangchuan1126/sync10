<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.ccc.FreightResourcesMgrCCC"%>
<%@page import="com.cwc.app.api.ccc.FreightCostMgrCCC"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<%@ include file="../../include.jsp"%> 

<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

 String fr_id = StringUtil.getString(request,"fr_id");
 String fc_id = StringUtil.getString(request,"fc_id");
 DBRow dbrow = freightCostMgrCCC.getFreightCostById(fc_id);

 String fc_project_name = dbrow.getValue("fc_project_name")==null?"":dbrow.getValue("fc_project_name").toString();
 String fc_way = dbrow.getValue("fc_way")==null?"":dbrow.getValue("fc_way").toString();
 String fc_unit = dbrow.getValue("fc_unit")==null?"1":dbrow.getValue("fc_unit").toString();
 String fc_unit_price = dbrow.getValue("fc_unit_price")==null?"1":dbrow.getValue("fc_unit_price").toString();
 String fr_id_str = dbrow.getValue("fr_id")==null?"1":dbrow.getValue("fr_id").toString();
 String fc_id_str = dbrow.getValue("fc_id")==null?"0":dbrow.getValue("fc_id").toString();

 DBRow[] countyRows = deliveryMgrLL.getCounty();
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>

<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 

<script type="text/javascript">

	var updated = false;
	
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated")=="1"||"1".equals(StringUtil.getString(request,"updated"))) {
	%>
			updated = true;
	<%
		}
	%>
	
	function init() {
		if(updated) {
			parent.location.reload();
			//closeWindow();
		}
	}
	
	$(document).ready(function(){
			init();
	})
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="apply_freight_cost_form_update" id="apply_freight_cost_form_update" action="<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/freight_cost/updateFreightCost.action">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">计费项目信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr height="29">
		          <td align="right" class="STYLE2">项目名称</td>
		          <td align="left" valign="middle">
	  				<input type="text" name="fc_project_name" id="fc_project_name" value="<%=fc_project_name %>" />
	  				<input type="hidden" name="fc_id" id="fc_id" value="<%=fc_id_str%>" />
		          </td>
		        </tr>
		    <tr> 
		      <td align="right" class="STYLE2" nowrap="nowrap">计费方式</td>
		      	<td><input name="fc_way" type="text" id="fc_way" size="15" value="<%=fc_way %>"></td>
		   
		    </tr>
			
		  <tr> 
		      <td align="right" class="STYLE2">单位</td>
		      <td><input name="fc_unit" type="text" id="fc_unit" size="15" value="<%=fc_unit %>"></td>
		    </tr>
		</table>
		
	</fieldset>	
	</td>
  </tr>

</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onclick="submitFreightCost()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="parent.closeWinNotRefresh()" value="取消" >
    </td>
  </tr>
</table>
</form>
<script type="text/javascript">

	function submitFreightCost()
	{
			if($('input[name=fc_project_name]').val().trim()==""||$('input[name=fc_project_name]').val()==null)
			 {
			     $('input[name=fc_project_name]').focus();
			     alert("项目名称不能为空！");
			 }
			 
			 else if($('input[name=fc_way]').val().trim()==""||$('input[name=fc_way]').val()==null)
			 {
			 	 $('input[name=fc_way]').focus();
			 	 alert("计费方式不能为空！");
			 }
			 else if($('input[name=fc_unit]').val().trim()==""||$('input[name=fc_unit]').val()==null)
			 {
			 	 $('input[name=fc_unit]').focus();
			 	 alert("单位不能为空！");
			 }
			 else
			 {
				document.apply_freight_cost_form_update.submit();
			 }	
			 
			 function closeWindow(){
				$.artDialog.close();
				//self.close();
			}
	}
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
</body>
</html>
