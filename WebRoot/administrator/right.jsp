<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../include.jsp"%> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 DBRow detailGroup = adminMgr.getDetailAdminGroup( adminLoggerBean.getAdgid() );
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>

<link type="text/css" href="js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="js/tabs/demos.css" rel="stylesheet" />
	
<link href="comm.css" rel="stylesheet" type="text/css">
<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css">

<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
</style>



</head>

<body >
<table width="95%"  border="0" cellpadding="0" cellspacing="0" style="margin:20px;">
  <tr>
    <td width="50%"  align="left" valign="top"><table width="268" height="125" border="0" cellpadding="0" cellspacing="0" class="profile">
      <tr>
        <td width="130">&nbsp;</td>
        <td align="left" valign="top" style="font-size:12px;line-height:20px;padding-top:5px;">帐号:<strong><%=adminLoggerBean.getAccount()%></strong><br />
          角色:<strong><%=detailGroup.getString("name")%></strong>
		  <br />
<br />
登录成功！		  </td>
      </tr>
    </table></td>
    <td width="50%"  align="left" valign="top">
	
	
	  <fieldset style="border:1px #999999 solid;padding:13px;width:94%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:bold;color:#99999;">急需跟进订单</legend>
	<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#cccccc">
      <tr>
        <td align="center" valign="middle" bgcolor="#999999"><span class="STYLE1">疑问订单</span></td>
        <td    align="center" valign="middle" bgcolor="#999999"><span class="STYLE1">疑问地址</span></td>
        <td   align="center" valign="middle" bgcolor="#999999"><span class="STYLE1">疑问付款</span></td>
     	
		<tst:authentication bindAction="com.cwc.app.api.OrderMgr.verifyOrderCost">
        <td    align="center" valign="middle" bgcolor="#999999"><span class="STYLE1">成本审核</span></td>
		</tst:authentication>
		
		 <td align="center" valign="middle" bgcolor="#999999"><span class="STYLE1">打印疑问</span></td>
		    <td   align="center" valign="middle" bgcolor="#999999"><span class="STYLE1">缺货</span></td>
      </tr>
      <tr>
        <td align="center" valign="middle" bgcolor="#FFFFFF">看顶部</td>
        <td align="center" valign="middle" bgcolor="#FFFFFF">看顶部</td>
        <td align="center" valign="middle" bgcolor="#FFFFFF">看顶部</td>
		
       	<tst:authentication bindAction="com.cwc.app.api.OrderMgr.verifyOrderCost">
        <td align="center" valign="middle" bgcolor="#FFFFFF"><a href="order/ct_order_auto_reflush.html?cmd=trace&trace_type=101" target="_blank" class="hard"><%=orderMgr.getVerifyCostOrderCount(adminLoggerBean.getAdgid())%></a></td>
		</tst:authentication>
		
		<td align="center" valign="middle" bgcolor="#FFFFFF"><a href="order/ct_order_auto_reflush.html?cmd=trace&trace_type=5" target="_blank" class="hard"><%=orderMgr.getPrintDoubtOrderCount(adminLoggerBean.getAdgid())%></a></td>
		 <td align="center" valign="middle" bgcolor="#FFFFFF"><a href="order/ct_order_auto_reflush.html?cmd=trace&trace_type=4" target="_blank" class="hard"><%=orderMgr.getTraceLackingOrderCount(adminLoggerBean.getAdgid())%></a></td>
      </tr>
      <tr>
       
        <td align="center" valign="middle" bgcolor="#999999"><span class="STYLE1">争议退款</span></td>
        <td align="center" valign="middle" bgcolor="#999999"><span class="STYLE1">争议质保</span></td>
        <td align="center" valign="middle" bgcolor="#999999"><span class="STYLE1">其他争议</span></td>
        <td align="center" valign="middle" bgcolor="#999999"><span class="STYLE1"></span></td>
		<td align="center" valign="middle" bgcolor="#999999"><span class="STYLE1"></span></td>
		<td align="center" valign="middle" bgcolor="#999999"><span class="STYLE1"></span></td>
      </tr>
      <tr>
        
        <td align="center" valign="middle" bgcolor="#FFFFFF"><a href="order/ct_order_auto_reflush.html?cmd=trace&trace_type=6" target="_blank" class="hard"><%=orderMgr.getDisputeRefundCount(adminLoggerBean.getAdgid())%></a></td>
        <td align="center" valign="middle" bgcolor="#FFFFFF"><a href="order/ct_order_auto_reflush.html?cmd=trace&trace_type=7" target="_blank" class="hard"><%=orderMgr.getDisputeWarrantyCount(adminLoggerBean.getAdgid())%></a></td>
        <td align="center" valign="middle" bgcolor="#FFFFFF"><a href="order/ct_order_auto_reflush.html?cmd=trace&trace_type=8" target="_blank" class="hard"><%=orderMgr.getOtherDisputeCount(adminLoggerBean.getAdgid())%></a></td>
        <td align="center" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
		<td align="center" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
		<td align="center" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
    </table>
</fieldset>	</td>
  </tr>
 
  <tr>
    <td colspan="2"  align="left" valign="top"><br />

<div class="demo">

<div id="tabs">
	<ul>
		<li><a href="../tright.jsp?ps_id=0">全部仓库<span> </span></a></li>
		<li><a href="../tright.jsp?ps_id=100000">北京仓库<span> </span></a></li>
		<li><a href="../tright.jsp?ps_id=100006">深圳仓库<span> </span></a></li>
		<li><a href="../tright.jsp?ps_id=100043">美国仓库<span> </span></a></li>
	</ul>

</div>

</div>
<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	</script>
	</td>
  </tr>
</table>

</body>
</html>

