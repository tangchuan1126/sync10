<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.net.URLDecoder"%>
<%@ include file="../../include.jsp"%> 
<%
long hct_id = StringUtil.getLong(request,"hct_id");
DBRow detail = helpCenterMgr.getDetailTopicsByHTCID(hct_id);
%>
<%
	if(detail==null)
	{
		detail=new DBRow();
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>帮助中心</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>


<style type="text/css">
<!--
.nav-title
{
	font-size:12px;
	color:#999999;
	border-bottom:1px #dddddd solid;
	height:40px;
}

#content ul li
{
	font-size:14px;
	font-weight:bold;
	color:#0066CC;
}
-->
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  	<td colspan="2"><%@ include file="top.jsp"%></td>
  </tr>
  <tr>
    <td width="250" align="left" valign="top" ><%@ include file="left.jsp"%></td>
    <td width="1189" align="left" valign="top" style="border-left:1px solid #dddddd;padding-left:10px;"><table width="99%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="nav-title"><img src="imgs/nav_logo.gif" width="16" height="16" align="absmiddle">
          <span style="color:#999999" id="title">
		  <%
		  Tree tree = new Tree(ConfigBean.getStringValue("help_center_catalog"));
		  DBRow allFather[] = tree.getAllFather(detail.get("cid",0));
		  for (int jj=0; jj<allFather.length-1; jj++)
		  {
		  	out.println(allFather[jj].getString("title"));
		
		  }
		  %>
		   » 
	      <%
		  DBRow catalog = helpCenterMgr.getDetailCatalog(detail.get("cid",0));
		  if (catalog!=null)
		  {
		  	out.println(catalog.getString("title"));
		  }
		  %>        </td>
        <td align="right" class="nav-title">&nbsp;</td>
      </tr>
    </table>
      <br>
      <table width="98%" border="0" cellspacing="0" cellpadding="0" id="test">

        <tr>
          <td colspan="2" align="left" valign="middle" style="font-size:25px;font-weight:normal;color:#000000;padding-left:10px;font-family:'黑体'">
    <%=detail.getString("title")%>
          <%
		  if (detail.getString("title").equals(""))
		  {
		  	out.println("文章不存在！");
		  }
		  %>		  </td>
        </tr>
        <tr>
          <td align="left" valign="middle" style="font-size:13px;font-weight:normal;color:#999999;padding-bottom:40px;padding-left:10px;font-family:Arial, Helvetica, sans-serif">
		  <% 
          		if(!detail.getString("title").equals(""))
          		{
          	%>
          		<%=detail.getString("author")%> | <%=detail.getString("last_mod_date") %>
          	<%
          		}
          	%>		  </td>
          <td align="right" valign="middle" style="font-size:13px;font-weight:normal;color:#999999;padding-bottom:40px;padding-left:10px;font-family:Arial, Helvetica, sans-serif">
            <a href="javascript:void(0)" onClick="printTopic()" style="text-decoration: none"><img src="../imgs/print.png" align="absbottom" style="border:0; text-align:center"><font color="#000000">&nbsp;打印文章</font></a>       	</td>
        </tr>
        <tr>
          <td colspan="2" align="left" valign="top" id="content" style="padding-left:10px;color:#333333;line-height:30px;">
		<% 
          		if(!detail.getString("title").equals(""))
          		{
      				out.print(detail.getString("content"));
          		}
        %></td>
        </tr>
    </table></td>
  </tr>
  
  <tr>
    <td colspan="2" align="left"   ><%@ include file="foot.jsp"%></td>
  </tr>
</table>
<script type="text/javascript">
	function addColor()
	{
		key = "<%=keyword%>";
		if(key!="")
		{
			var str = document.getElementById('test').innerHTML;
			var title = document.getElementById('title').innerHTML;
   			re = new RegExp(key,'g');//创建含变量的正则表达式
	   		newstr = "<span style='color:red;background-color:yellow'>" + key + "</span>";//将关键字替换成newstr的形式 
	   		targetstr = str.replace(re,newstr);//执行替换
	   		targettitle = title.replace(re,newstr); 
	   		document.getElementById('test').innerHTML = targetstr;//重新输出替换后的内容
	   		document.getElementById('title').innerHTML = targettitle;
		}
	}
	addColor();
	
	function printTopic()
	{
		window.open("content_print.html?hct_id=<%=hct_id%>");
	}
</script>
</body>
</html>
