<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.net.URLDecoder"%>
<%@ include file="../../include.jsp"%> 
<%
long hct_id = StringUtil.getLong(request,"hct_id");
DBRow detail = helpCenterMgr.getDetailTopicsByHTCID(hct_id);
%>
<%
	if(detail==null)
	{
		detail=new DBRow();
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>帮助中心</title>
<link href="../comm.css" rel="stylesheet" type="text/css">



<style type="text/css">
<!--
.nav-title
{
	font-size:12px;
	color:#999999;
	border-bottom:1px #dddddd solid;
	height:40px;
}

#content ul li
{
	font-size:14px;
	font-weight:bold;
	color:#0066CC;
}
-->
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" id="test">

        <tr>
          <td align="left" valign="middle" style="font-size:25px;font-weight:normal;color:#000000;padding-left:10px;font-family:'黑体'">
    <%=detail.getString("title")%>
          <%
		  if (detail.getString("title").equals(""))
		  {
		  	out.println("文章不存在！");
		  }
		  %>		  </td>
        </tr>
        <tr>
          <td align="left" valign="middle" style="font-size:13px;font-weight:normal;color:#999999;padding-bottom:40px;padding-left:10px;font-family:Arial, Helvetica, sans-serif">
		  <% 
          		if(!detail.getString("title").equals(""))
          		{
          	%>
          		<%=detail.getString("author")%> | <%=detail.getString("last_mod_date") %>
          	<%
          		}
          	%>		  </td>
        </tr>
        <tr>
          <td align="left" valign="top" style="padding-left:10px;color:#333333;line-height:30px;" id="content">
		<% 
          		if(!detail.getString("title").equals(""))
          		{
      				out.print(detail.getString("content"));
          		}
        %></td>
        </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
