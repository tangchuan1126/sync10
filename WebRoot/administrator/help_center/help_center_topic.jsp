<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long catalog_id = StringUtil.getLong(request,"catalog_id");
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);


DBRow rows[];
if (cmd.equals("filter"))
{
	rows = helpCenterMgr.getTopic(catalog_id,pc);
}
else if (cmd.equals("search"))
{
	rows = helpCenterMgr.getSearchTopics4CT(key,pc);
}
else
{
	rows = helpCenterMgr.getTopic(0,pc);	
}


Tree tree = new Tree(ConfigBean.getStringValue("help_center_catalog"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>
		
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

		
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script language="javascript">
<!--


function filter()
{
		document.filter_search_form.cmd.value = "filter";		
		document.filter_search_form.catalog_id.value = $("#filter_catalog_id").val();			
		document.filter_search_form.submit();
}

function search()
{
	if ($("#search_key").val()=="")
	{
		alert("请填写关键字");
		$("#search_key").focus();
		return(false);
	}
	else
	{
		document.filter_search_form.cmd.value = "search";		
		document.filter_search_form.key.value = $("#search_key").val();		
		document.filter_search_form.submit();
	}
}

function switchCheckboxAction()
{
	$('input[name=switchCheckboxhct_id]').inverse();
}

function getAllChecked()
{
	 var str=""; 
        $("input[name='switchCheckboxhct_id']").each(function(){ 
            if(this.checked) str+=$(this).val()+",";         
        })
	
	return str;
}

function modTopicIssue(issue)
{
	var hct_ids = getAllChecked();
	if ( hct_ids=="" )
	{
		alert("最少选择一篇文章");
	}
	else
	{
		var str;
		if(issue=="use")
		{
			str="发布"
		}
		if(issue=="cancel")
		{
			str="撤销发布"
		}
		if ( confirm("确认"+str+"选中的文章") )
		{
			document.mod_issue_form.hct_id.value=hct_ids;
			document.mod_issue_form.issue.value=issue;
			document.mod_issue_form.submit();
		}	
	}
}

function modOneTopicIssue(id,issue)
{
	var str;
		if(issue=="use")
		{
			str="发布"
		}
		if(issue=="cancel")
		{
			str="撤销发布"
		}
		if ( confirm("确认"+str+"该文章？") )
		{
			document.mod_issue_form.hct_id.value=id;
			document.mod_issue_form.issue.value=issue;
			document.mod_issue_form.submit();
		}		
}


	function addTopic()
	{
		var id = $("#filter_catalog_id").val();
		if (id==0)
		{
			alert("请选择一个文章分类");
			openCatalogMenu();
		}
		else
		{

			document.mod_form.action="add_topic.html?catalog_id="+id+"";
			document.mod_form.submit();
		}
	}
	
	function modTopic(hct_id,catalog_id)
	{
		var cid =catalog_id ;
		var hctid=hct_id;
		document.mod_form.action="mod_topic.html?catalog_id="+cid+"&hct_id="+hctid+"";
		document.mod_form.submit();
		
	}
	
	function delTopic(title,id)
	{
		if ( confirm("确认删除"+title+"该文章？") )
		{
			document.del_form.hct_id.value=id;
			document.del_form.submit();
		}
	}
	
	function modTopicCatalog()
	{
		var catalog_id = $("#filter_catalog_id").val();
		var ids=getAllChecked();
		if(ids=="")
		{
			alert("最少选择一篇文章");
		}
		else
		{
			if(confirm("确定将所选文章移动到该分类中吗？"))
			{
				document.mod_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/modHelpCenterTopicCatalog.action";
				document.mod_form.catalog_id.value=catalog_id;
				document.mod_form.hct_id.value=ids;
				document.mod_form.submit();
			}
		}
	}
	
function closeWin()
{
	tb_remove();
}

function openCatalogMenu()
{
	$("#category").mcDropdown("#categorymenu").openMenu();
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 帮助中心 »   文章管理</td>
  </tr>
</table>
<br>


<form name="mod_issue_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/modHelpCenterTopicIssue.action" >
<input type="hidden" name="hct_id">
<input type="hidden" name="issue">
</form>
 
 
<form name="del_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/delHelpCenterTopic.action">
<input type="hidden" name="hct_id">
</form>

<form name="mod_form" method="post">
<input type="hidden" name="catalog_id">
<input type="hidden" name="title" >
<input type="hidden" name="content">
<input type="hidden" name="hct_id">
<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>">

</form>


<form name="filter_search_form" method="get"  action="help_center_topic.html">
<input type="hidden" name="cmd" >
<input type="hidden" name="catalog_id" >
<input type="hidden" name="key" >


</form>


<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="65%">
	
	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
	
	<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td width="423" rowspan="2" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
	<div style="padding-bottom:5px;color:#666666;font-size:12px;">
	可以选择一个文章分类：
	</div>

	<ul id="categorymenu" class="mcdropdown_menu" >
	  <li rel="0">所有分类</li>
	  <%
	  DBRow c1[] = helpCenterMgr.getCatalogByParentid(0,null);
	  for (int i=0; i<c1.length; i++)
	  {
			out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));

			  DBRow c2[] = helpCenterMgr.getCatalogByParentid(c1[i].get("id",0l),null);
			  if (c2.length>0)
			  {
			  		out.println("<ul>");	
			  }
			  for (int ii=0; ii<c2.length; ii++)
			  {
					out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
					
						DBRow c3[] = helpCenterMgr.getCatalogByParentid(c2[ii].get("id",0l),null);
						  if (c3.length>0)
						  {
								out.println("<ul>");	
						  }
							for (int iii=0; iii<c3.length; iii++)
							{
									out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
									out.println("</li>");
							}
						  if (c3.length>0)
						  {
								out.println("</ul>");	
						  }
						  
					out.println("</li>");				
			  }
			  if (c2.length>0)
			  {
			  		out.println("</ul>");	
			  }
			  
			out.println("</li>");
	  }
	  %>
</ul>
	  <input type="text" name="category" id="category" value="" />
	  <input type="hidden" name="filter_catalog_id" id="filter_catalog_id" value="0"  />
	  

<script>
$("#category").mcDropdown("#categorymenu",{
		allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_catalog_id").val(id);
				}

});
<%
if (catalog_id>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=catalog_id%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 


</script>	</td>
    <td width="434" height="29" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">
	        &nbsp;&nbsp;
            <input name="Submit2" type="button" class="button_long_refresh" value="过滤" onClick="filter()">   	</td>
  </tr>
  <tr>
    <td height="29" bgcolor="#eeeeee">
	<input type="text" name="search_key" id="search_key" value="<%=key%>" style="width:200px;" onKeyDown="if(event.keyCode==13)search()">
           &nbsp;&nbsp;&nbsp;
<input name="Submit4" type="button" class="button_long_search" value="搜索" onClick="search()"> &nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" value="增加文章" onClick="addTopic()">	</td>
  </tr>
</table>
	
	</div>
	
	
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
  <tr>
    <td><input name="Submit" type="button" class="long-button-mod" value="批量发布" onClick="modTopicIssue('use')">&nbsp;&nbsp;
    	<input name="Submit" type="button" class="long-button-mod" value="批量撤销" onClick="modTopicIssue('cancel')">&nbsp;&nbsp;
    	<input name="Submit" type="button" class="long-button-mod" value="转移" onClick="modTopicCatalog()">
    </td>
  </tr>
</table>  <form name="listForm" method="post">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">


	
    <tr> 
    	<th width="36" align="center" valign="middle"  class="left-title"><input type="checkbox" name="switchCheckbox" id="switchCheckbox" value="checkbox" onClick="switchCheckboxAction()"></th>
        <th width="297"  class="left-title" style="vertical-align: center;text-align: center;">标题</th>
        <th width="203"  class="right-title" style="vertical-align: center;text-align: center;">类别</th>
        <th width="176" align="center"  class="right-title" style="vertical-align: center;text-align: center;">作者</th>
        <th width="139" align="center"  class="right-title" style="vertical-align: center;text-align: center;">最后更新时间</th>
        <th width="139" align="center"  class="right-title" style="vertical-align: center;text-align: center;">发布时间</th>
        <th width="165"  class="right-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
    </tr>

    <%
for ( int i=0; i<rows.length; i++ )
{

%>
    <tr  > 
    <td height="60" align="center" valign="middle"   ><input type="checkbox" id="switchCheckboxhct_id" name="switchCheckboxhct_id" value="<%=rows[i].get("hct_id",0l)%>">
      </td>
      <td align="center" valign="middle"  height="60">
      	<%=rows[i].getString("title") %>
      </td>
      <td align="center" valign="middle"   >
       <span style="color:#999999">
	  <%
	  DBRow allFather[] = tree.getAllFather(rows[i].get("cid",0));
	  for (int jj=0; jj<allFather.length-1; jj++)
	  {
	  	out.println("<a class='nine4' href='?cmd=filter&catalog_id="+allFather[jj].getString("id")+"'>"+allFather[jj].getString("title")+"</a><br>");
	
	  }
	  %>
	  
	  </span>
      
      <%
	  DBRow catalog = helpCenterMgr.getDetailCatalog(rows[i].get("cid",0));
	  if (catalog!=null)
	  {
	  	out.println("<a href='?cmd=filter&catalog_id="+catalog.getString("id")+"&'>"+catalog.getString("title")+"</a>");
	  }
	  %>
      </td>
      <td align="center" valign="middle"   >
      <%=rows[i].getString("author")%>
      </td>
      <td align="center" valign="middle"  ><%=rows[i].getString("last_mod_date")%></td>
      <td align="center" valign="middle"  ><%=rows[i].getString("post_date")%>&nbsp;</td>
      <td align="center" valign="middle" nowrap="nowrap">
		<%
			if(rows[i].get("issue",0)==0)
			{
		%>
			<input name="Submit3" type="button" class="short-button-ok" value="发布" onClick="modOneTopicIssue(<%=rows[i].getString("hct_id")%>,'use')">&nbsp;&nbsp;
			<input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="modTopic(<%=rows[i].getString("hct_id")%>,<%=rows[i].getString("cid")%>)">&nbsp;&nbsp;
			<input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delTopic('<%=rows[i].getString("title")%>',<%=rows[i].getString("hct_id")%>)">
		<%
			}
			else
			{
		%>
			<input name="Submit3" type="button" class="long-button" value="取消发布" onClick="modOneTopicIssue(<%=rows[i].getString("hct_id")%>,'cancel')">
		<%
			}
		 %>	
      </td>
    </tr>
    <%
}
%>

</table>  </form>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="help_center_topic.html">
    <input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="catalog_id" value="<%=catalog_id%>">
<input type="hidden" name="key" value="<%=key%>">

  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
