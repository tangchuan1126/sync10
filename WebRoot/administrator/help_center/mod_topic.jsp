<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改文章</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="fckeditor/fckeditor.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<%
	long catalog_id = StringUtil.getLong(request,"catalog_id");
	long hct_id = StringUtil.getLong(request,"hct_id");
	DBRow detail=helpCenterMgr.getDetailTopicsByHTCID(hct_id);
	String backurl = StringUtil.getString(request,"backurl");
 %>
<script>
function modTopic()
{
	var f = document.add_topic_form;
	
	if (f.title.value == "")
	 {
		alert("请填写文章标题");
	 }
	 else if (CKEDITOR.instances.content.getData() == "")
	 {
		alert("请填写文章内容");
	 }
	 else
	 {
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/modHelpCenterTopic.action";	
						document.mod_form.title.value = f.title.value;
						document.mod_form.content.value = CKEDITOR.instances.content.getData();
						document.mod_form.hct_id.value =<%=hct_id%>;		
						document.mod_form.catalog_id.value=<%=catalog_id%>	
						document.mod_form.submit();	
	 }
}

</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_topic_form" method="post" action="" >

<table width="98%" border="0" cellspacing="5" cellpadding="2">
  <tr>
    <td width="8%" align="right" valign="middle" class="text-line" >&nbsp;</td>
    <td width="92%" align="left" valign="middle" >&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE1 STYLE2" >标题</td>
    <td align="left" valign="middle" ><input name="title" type="text" class="input-line" id="title"  value="<%=detail.get("title","str") %>" style="width:500px;"></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >内容</td>
    <td align="left" valign="middle" >
    <textarea rows="40" cols="80" name="content" id="content"><%=detail.get("content","str") %></textarea>
    		<script type="text/javascript">
			CKEDITOR.replace('content',
				{
					 height:550,
					 
	        		 filebrowserUploadUrl : '/uploader/upload.php',
	        		 filebrowserImageUploadUrl : '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/help_center/uploadHelpCenterTopicImage.action?type=Images',//上传方法地址
				}
			);				
			</script>
	</td>
  </tr>

</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="修改" class="normal-green" onClick="modTopic();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="window.location.href='help_center_topic.html'">
	</td>
  </tr>
</table> 
<form name="mod_form" method="post">
<input type="hidden" name="catalog_id">
<input type="hidden" name="title" >
<input type="hidden" name="content">
<input type="hidden" name="hct_id">
<input type="hidden" name="author" value="<%=detail.getString("author") %>">
<input type="hidden" name="backurl" value="<%=backurl%>">

</form>
</body>
</html>
