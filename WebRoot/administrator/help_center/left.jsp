<%@ page contentType="text/html;charset=UTF-8"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>帮助中心</title>

<script type="text/javascript" src="../js/tree/jquery.js"></script>
<script type="text/javascript" src="../js/tree/jquery.tree.js"></script>


<link rel="stylesheet" href="../js/loadmask/jquery.loadmask.css" type="text/css" media="screen">

	<script type="text/javascript" class="source">
	$(function () { 
		$("#demo_1").tree({
			
			rules : {
				// only nodes of type root can be top level nodes
				//state: "open", // or "open"
				clickable:true,
				multiple:false,	//支持多选
				drag_copy:false	//禁止拷贝
			},
			types : {
				// all node types inherit the "default" node type
				"default" : {
					
					deletable : false,
					draggable : false,
					renameable : false
				},
				"f1" : {
					icon : { 
						image : "drive.png"
					}
				},
				"f2" : {
					icon : { 
						image : "application_export.png" 
					}			
				},
				"f3" : {
					icon : { 
						image : "page_red.png" 
					}			
				}
				
			}
			
			
		});
	});
	</script>
	
	<style type="text/css">
	html, body { margin:0px; padding:0px; }
	body, td, th, pre, code, select, option, input, textarea { font-family:"Trebuchet MS", Sans-serif; font-size:10pt; }
	.demo {
		 float:left; 
		 margin:5px;
		 border:0px solid gray; 
		 font-family:Verdana;
		 font-size:12px;
		 background:white; 

	}
	</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="78%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top" style="padding-top:15px;padding-left:10px;">

	<div class="demo" id="demo_1">
		<ul>


	<%
	DBRow catalogs1[] = helpCenterMgr.getCatalogByParentid(0,null);
	
	for (int i=0; i<catalogs1.length; i++)
	{
	%>
			<li id="<%=catalogs1[i].getString("id")%>" rel="f1" class="open"><a href="#"><ins>&nbsp;</ins><%=catalogs1[i].getString("title")%></a>
				<ul>
					<%
					DBRow catalogs2[] = helpCenterMgr.getCatalogByParentid(catalogs1[i].get("id",0l),null);
					for (int j=0; j<catalogs2.length; j++)
					{
					%>
					
						<li id="<%=catalogs2[j].getString("id")%>" rel="f2"  class="open"><a href="#"><ins>&nbsp;</ins><%=catalogs2[j].getString("title")%></a>
							<ul>

								<%
								DBRow topics[] = helpCenterMgr.getTopicsByCidSortAsc(catalogs2[j].get("id",0l),null);
								for (int k=0; k<topics.length; k++)
								{
								%>
									<li id="<%=topics[k].getString("hct_id")%>" rel="f3" onClick="location='content-<%=topics[k].getString("hct_id")%>.html'"><a href="javascript:void(0);" style="color:#0066CC" title="<%=topics[k].getString("title")%>"><ins>&nbsp;</ins><%=StrUtil.cutString(topics[k].getString("title"),10,"...")%></a></li>  
								<%
								}
								%>
							</ul>
						</li>
					
					<%
					}
					%>

				</ul>
			</li>
	<%
	}
	%> 


			
			
			
			
		</ul>
	</div>


	
	</td>
  </tr>
</table>
</body>
</html>
