<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="../js/select.js"></script>

	
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="JavaScript1.2">

function addHelpCenterCatalog(id)
{
	$.prompt(
	
	"<div id='title'>增加子类</div><br />父类：<input name='proTextParentTitle' type='text' id='proTextParentTitle' style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br>新建类别<br><input name='hccTextTitle' type='text' id='hccTextTitle' style='width:300px;'>",
	
	{
	      submit:promptCheckAddHelpCenterCatalog,
   		  loaded:
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/getHelpCenterCatalogDetailJSON.action",
							{id:id},//{name:"test",age:20},
							function callback(data)
							{
								$("#proTextParentTitle").setSelectedValue(data.title);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
							document.add_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/addHelpCenterCatalog.action"
							document.add_form.parentid.value = id;
							document.add_form.title.value = f.hccTextTitle;		
							document.add_form.submit();	
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function promptCheckAddHelpCenterCatalog(v,m,f)
{
	if (v=="y")
	{
		 if(f.hccTextTitle == "")
		 {
				alert("请选填写新建分类名称");
				return false;
		 }
		 
		 return true;
	}

}



function modHelpCenterCatalog(id)
{
	$.prompt(
	
	"<div id='title'>修改分类</div><br />分类名称<br><input name='hccTextTitle' type='text' id='hccTextTitle' style='width:300px;'><input name='hccParentid' id='hccParentid' type='hidden'>",
	
	{

   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/getHelpCenterCatalogDetailJSON.action",
							{id:id},//{name:"test",age:20},
							function callback(data)
							{	
								$("#hccTextTitle").setSelectedValue(data.title);
								$("#hccParentid").setSelectedValue(data.parentid);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/modHelpCenterCatalog.action";
						document.mod_form.id.value = id;
						document.mod_form.title.value = f.hccTextTitle;		
						document.mod_form.parentid.value=f.hccParentid;
						document.mod_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function modSort(parentid,id,type)
{
	document.mod_sort_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/modHelpCenterCatalogSort.action";
	document.mod_sort_form.parentid.value =parentid;
	document.mod_sort_form.id.value=id;
	document.mod_sort_form.type.value=type;
	document.mod_sort_form.submit();		
}




function delCatalog(id,parentid)
{
	if (confirm("确认操作吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/delHelpCenterCatalog.action";
		document.del_form.id.value = id;
		document.del_form.parentid.value=parentid;
		document.del_form.submit();
	}
}


</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 帮助中心管理 »   内容分类</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="id" >
<input type="hidden" name="parentid" >

</form>

<form method="post" name="mod_form">
<input type="hidden" name="id" >
<input type="hidden" name="title" >
<input type="hidden" name="parentid">
</form>

<form method="post" name="add_form">
<input type="hidden" name="parentid">
<input type="hidden" name="title">
</form>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
  <form name="listForm" method="post">
    <input type="hidden" name="id">
    <input type="hidden" name="parentid">
    <input type="hidden" name="imp_color">

      <tr> 
        <th  class="left-title">类别名称</th>
        <th  class="right-title" style="vertical-align: center;text-align: center;">ID</th>
        <th width="25%"  class="right-title">&nbsp;</th>
      </tr>

    <tr > 
      <td height="39" valign="middle" >&nbsp;&nbsp;&nbsp;&nbsp;/</td>
      <td width="8%" align="center" valign="middle"  >&nbsp;</td>
      <td align="center" valign="middle"  ><input name="Submit" type="button" class="long-button-add" onClick="addHelpCenterCatalog(0)" value=" 增加子类"></td>
    </tr>
    <%

String qx;
int kk=0;
DBRow treeRows[] = helpCenterMgr.getHelpCenterCatalogTree();

for ( int i=0; i<treeRows.length; i++ )
{
	
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
    <tr > 
      <td width="59%" height="60" valign="middle" > &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
        <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
        <%=qx%>
		
				<%
		if (treeRows[i].get("parentid",0) == 0)
		{
			out.println("<span style='font-size:14px;font-weight:bold'>"+treeRows[i].getString("title")+"</span>");
		}
		else
		{
			out.println(treeRows[i].getString("title"));
		}
		%>   
		<div align="right">
			<a title="上移" href="javascript:void(0)" onclick="modSort(<%=treeRows[i].get("parentid",0)%>,<%=treeRows[i].get("id",0)%>,'up')"><img style="border: 0px" src="imgs/up.gif"></a>
			<a title="下移" href="javascript:void(0)" onclick="modSort(<%=treeRows[i].get("parentid",0)%>,<%=treeRows[i].get("id",0)%>,'down')"><img style="border: 0px" src="imgs/down.gif"></a>
			
		</div>
   </td>
      <td width="8%" align="center" valign="middle"  > 
        <%=treeRows[i].getString("id")%>      </td>
      <td align="center" valign="middle" ><input name="Submit2s" type="button" class="short-short-button-del" onClick="delCatalog(<%=treeRows[i].getString("id")%>,<%=treeRows[i].getString("parentid")%>)" value=" 删除">
	  &nbsp;&nbsp;&nbsp;&nbsp; 
        <input name="Submit2" type="button" class="short-short-button-mod" onClick="modHelpCenterCatalog(<%=treeRows[i].getString("id")%>)" value=" 修改">
		&nbsp;&nbsp;&nbsp;&nbsp; 
		<%
		if (treeRows[i].get("level",0)<2)
		{
		%>
        <input name="Submit22" type="button" class="long-button-add" onClick="addHelpCenterCatalog(<%=treeRows[i].getString("id")%>)" value=" 增加子类">
		<%
		}
		%>
		</td>
    </tr>
    <%	
}
%>
  </form>
</table>
<br>
<br>
<br>
<form method="post" name="mod_sort_form">
<input type="hidden" name="parentid">
<input type="hidden" name="id">
<input type="hidden" name="type">
</form>
</body>
</html>
