<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.net.URLDecoder"%>
<%
	long catalog_id = StrUtil.getLong(request, "catalog_id");
	String cmd = StrUtil.getString(request, "cmd");
	String key = StrUtil.getString(request, "search_key");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>帮助中心</title>
	<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>
		
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script type="text/javascript">
	function filter()
	{			
			document.helpcenter_home_form.submit();
	}
	
	function search()
	{
		if ($("#search").val()=="")
		{
			alert("请填写关键字");
			
			document.getElementById("search").focus();
		}
		else
		{
			var para = "search_key="+$("#search").val();
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/urlEncode.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request)
				{
				
				},
				
				error: function()
				{
					alert("提交失败，请重试！");
				},
				
				success: function(date)
				{
					document.filter_search_form.search_key.value =date["key"];	
					document.filter_search_form.cmd.value = "search";		
					document.filter_search_form.action ="indexForKey.html";
					document.filter_search_form.submit();
				}
			});
		}
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="231" height="72" align="center" valign="middle"><img src="imgs/logo.jpg" width="172" height="30"></td>
    <td width="516"></td>
    <td width="" align="right" style="padding-right:20px;"><img src="imgs/ren.jpg" width="58" height="57" align="absmiddle">&nbsp;&nbsp;
    <%
    	String keyword;
		try
		{
			keyword = URLDecoder.decode(key,"utf-8");
		}
		catch(Exception e)
		{
			keyword = "";
		}
	 %>
      <input type="text" name="search_key" id="search" value="<%=keyword%>" style="width:300px;" onKeyDown="if(event.keyCode==13)search()">
	&nbsp;&nbsp;
	<input name="Submit4" type="button" class="button_long_search" value="搜索" onClick="search()">
  </tr>
  <tr>
    <td height="2" colspan="3" bgcolor="#999999"></td>
  </tr>
  <tr>
    <td height="1" colspan="3" bgcolor="#FFFFFF"></td>
  </tr>
  <tr>
    <td height="28" colspan="2" valign="bottom" bgcolor="#eeeeee" style="padding-left:30px;"><img src="imgs/help_center_visionari.gif" width="114" height="24"><span style="border-bottom:1px #dddddd solid">
	  <td height="28" align="right" valign="bottom" bgcolor="#eeeeee"><a href="javascript:void(0)" onClick="filter()" style="text-decoration: none"><img style="border: 0px" src="imgs/helpHome.bmp"> 帮助中心首页</a>&nbsp;&nbsp;</td>
    </span></td>
  </tr>
</table>

<form name="filter_search_form" method="get">
<input type="hidden" name="cmd" >
<input type="hidden" name="catalog_id" >
<input type="hidden" name="search_key"/>


</form>
<form action="index.html" name="helpcenter_home_form"></form>

</body>

</html>
