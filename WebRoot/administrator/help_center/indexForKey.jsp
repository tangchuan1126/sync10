<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.net.URLEncoder"%>
<%@ include file="../../include.jsp"%>
<%@page import="java.net.URLDecoder"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>帮助中心</title>
		<link href="../comm.css" rel="stylesheet" type="text/css">



		<style type="text/css">
<!--
.nav-title {
	font-size: 12px;
	color: #999999;
	border-bottom: 1px #dddddd solid;
	height: 40px;
}

#content ul li {
	font-size: 14px;
	font-weight: bold;
	color: #0066CC;
}
a:hover {color: #0099CC;text-decoration: underline;}
-->
</style>
	<script type="text/javascript">
	  function Light(obj,eventName)
	  {
		  if(eventName=="onMouseOver")
		  {
			  obj.style.textDecoration="underline";
		  }
		  else
		  {
		  	obj.style.textDecoration="none";
		  }
	  }
	</script>
	</head>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2"><%@ include file="top.jsp"%></td>
			</tr>
			<%
				PageCtrl pc = new PageCtrl();
				pc.setPageNo(StringUtil.getInt(request, "p"));
				pc.setPageSize(30);
				DBRow rows[];
				if (cmd.equals("filter")) {
					rows = helpCenterMgr.getTopicsUse(catalog_id, pc);
				} else if (cmd.equals("search")) {
					rows = helpCenterMgr.getSearchTopics4CTUse(keyword, pc);
				} else {
					rows = helpCenterMgr.getTopicsUse(0, pc);
				}
			
				Tree tree = new Tree(ConfigBean
						.getStringValue("help_center_catalog"));
			%>
			<tr>
				<td height="13" colspan="2"></td>
			</tr>
		</table>
		<table width="100%" height="100%" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="250" align="left" valign="top"><%@ include
						file="left.jsp"%></td>
				<td width="1189" align="left" valign="top"
					style="border-left: 1px solid #dddddd; padding-left: 10px;padding-bottom: 10px;">
					<table width="99%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="nav-title">
							<%
								if(rows.length==0)
								{
									out.println("<h2><font color='black'>对不起，未找到与 “"+keyword+"” 有关的文章</font></h2>");
								}
								else
								{
									out.println("<h2><font color='black'>共找到条 "+pc.getAllCount()+" 记录</font></h2>");
								}
							 %><br>

							</td>
						</tr>
					</table>
					<br>



					<form name="listForm" method="post">
<table width="98%" height="55" border="0" align="center" cellpadding="0" cellspacing="0" >

      <%
			for ( int i=0; i<rows.length; i++ )
			{
		%>
			   
			    <tr >
			      <td width="918" colspan="2" align="left" valign="middle"  style="border-bottom:1px #CCCCCC dashed;padding-bottom:10px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="76%" style="padding-top:10px;">
					  <%
			      		String index;
			      		index = rows[i].getString("contentindex");
				
			      		if(index!=null&&!index.equals(""))
			      		{	
							index = StringUtil.cutString(index,300,"......");
						
			      		}
			      	%>			      												
			      
			      
			      
			     · <a style='text-decoration: none;font-size:17px;font-weight:bold;color:#006699' href="javascript:void(0)" onClick="location='content-<%=rows[i].getString("hct_id")%>.html?search_key=<%=URLEncoder.encode(key,"utf-8")%>'" onMouseMove="Light(this,'onMouseOver')" onMouseOut="Light(this,'onMouseOut')"><%=rows[i].getString("title")%></a>					  </td>
                      <td width="24%" align="right">&nbsp;</td>
                  </tr>
<% 
				if(index!=null&&!index.equals(""))
				{
				%>
			    <tr >
			      <td colspan="2" align="left" valign="middle"  style="padding-left:10px;">
				  
				   <span style="color:#999999">
					  <%
				  DBRow allFather[] = tree.getAllFather(rows[i].get("cid",0));
				  for (int jj=0; jj<allFather.length-1; jj++)
				  {
				  	out.println("<a class='nine4' href='index.html?cmd=filter&catalog_id="+allFather[jj].getString("id")+"'>"+allFather[jj].getString("title")+"</a> >  ");
				
				  }
				  %>
			      <%
				  DBRow catalog = helpCenterMgr.getDetailCatalog(rows[i].get("cid",0));
				  if (catalog!=null)
				  {
				  	out.println("<a class='nine4' href='index.html?cmd=filter&catalog_id="+catalog.getString("id")+"&'>"+catalog.getString("title")+"</a>");
				  }
				  
				  %>					 
				  	 </span>
				  
				  </td>
			      </tr>
			    <tr >
			      <td colspan="2" align="left" valign="middle"  ><br>

			<div style='color:#666666;padding-top:5px;line-height:20px;padding-left:10px;'><%=index%></div>						  </td>
	      </tr>
		  <%
		  } 
		  %>

                  </table></td>
	      </tr>
								
<%
}
%>
		</table>  
					</form>


<br>


<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="help_center_topic.html">
    <input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="catalog_id" value="<%=catalog_id%>">
<input type="hidden" name="key" value="<%=key%>">

  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>



				</td>
			</tr>
			
			<tr>
				<td colspan="2" align="left"><%@ include file="foot.jsp"%></td>
			</tr>
		</table>
		<form name="dataForm" action="index.html">
			<input type="hidden" name="p">
			<input type="hidden" name="cmd" value="<%=cmd%>">
			<input type="hidden" name="catalog_id" value="<%=catalog_id%>">
			<input type="hidden" name="key" value="<%=key%>">

		</form>
	</body>
</html>
