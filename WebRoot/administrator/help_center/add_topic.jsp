<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long catalog_id = StringUtil.getLong(request,"catalog_id");
DBRow sonCatalog[] = helpCenterMgr.getCatalogByParentid(catalog_id,null);
String backurl = StringUtil.getString(request,"backurl");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加文章</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="fckeditor/fckeditor.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script>
function addTopic()
{
	var f = document.add_topic_form;
	
	if (f.title.value == "")
	 {
		alert("请填写文章标题");
	 }
	 else if (CKEDITOR.instances.content.getData() == "")
	 {
	 	
		alert("请填写文章内容");
	 }
	 else
	 {
		document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/help_center/addHelpCenterTopic.action";
		document.mod_form.catalog_id.value = <%=catalog_id%>;		
		document.mod_form.title.value = f.title.value;
		document.mod_form.content.value = CKEDITOR.instances.content.getData();
		document.mod_form.submit();	
	 }
}

if ( <%=sonCatalog.length%>>0 )
{
	alert("文章必须增加到最底层分类，请重新选择");
	<%
		String repbackurl=backurl.replaceAll("%2F","/");
	%>
	window.location.href="<%=repbackurl%>";
}

	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="keepAlive()">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_topic_form" method="post" action="" >

<table width="98%" border="0" cellspacing="5" cellpadding="2">

  <tr>
    <td colspan="2" align="left" valign="middle" class="text-line" style="border-bottom:1px solid #999999"><table width="100%" border="0" cellpadding="5" cellspacing="0" >
      <tr>
        <td width="96%" style="color:#666666;font-size:20px;font-weight:bold;font-family:Arial, 宋体"> 
		<%
	  Tree tree = new Tree(ConfigBean.getStringValue("help_center_catalog"));
	  DBRow allFather[] = tree.getAllFather(catalog_id);
	  for (int jj=0; jj<allFather.length-1; jj++)
	  {
	  	out.println(allFather[jj].getString("title")+" » ");
	
	  }
	  
	  DBRow catalog = helpCenterMgr.getDetailCatalog(catalog_id);
	  if (catalog!=null)
	  {
	  	out.println(catalog.getString("title"));
	  }
	  %>		</td>
      </tr>
      <tr>
        <td style="color:#666666;font-size:13px;font-family:宋体">分类下增加文章......</td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td width="8%" align="right" valign="middle" class="text-line" >&nbsp;</td>
    <td width="92%" align="left" valign="middle" >&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE1 STYLE2" >标题</td>
    <td align="left" valign="middle" ><input name="title" type="text" class="input-line" id="title" ></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >内容</td>
    <td align="left" valign="middle" >
    <textarea rows="50" cols="80" name="content" id="content" style="width: 100%;height: 800px"></textarea>
    		<script type="text/javascript">
			CKEDITOR.replace( 'content' ,
				{
					 height:500,
					 
	        		 filebrowserUploadUrl : '/uploader/upload.php',
	        		 filebrowserImageUploadUrl : '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/help_center/uploadHelpCenterTopicImage.action?type=Images',//上传方法地址
				}
			);				
			</script>
	</td>
  </tr>

</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line"><img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#999999">必须把文章增加到最底层分类</span>         </td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存" class="normal-green" onClick="addTopic();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="window.location.href='help_center_topic.html'">
	</td>
  </tr>
</table> 
<form name="mod_form" method="post">
<input type="hidden" name="catalog_id">
<input type="hidden" name="title" >
<input type="hidden" name="content">
<input type="hidden" name="hct_id">
<input type="hidden" name="backurl" value="<%=backurl%>">

</form>

<div style="display:none" id="keep_alive"></div>
</body>
</html>
