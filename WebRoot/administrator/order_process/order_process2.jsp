<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.Date"%>
<%@ include file="../../include.jsp"%> 
<%

	String p = StringUtil.getString(request,"p","0");

	long catalog_id = StringUtil.getLong(request,"catalog_id");
	String pro_line_id = StringUtil.getString(request,"pro_line_id");
	String product_name = StringUtil.getString(request,"product_name");
	TDate tdate = new TDate();
	String input_en_date = StringUtil.getString(request,"input_en_date",tdate.formatDate("yyyy-MM-dd"));
	
	String input_en_dateQLL = StringUtil.getString(request,"endDateQLL");//QLL
	
	tdate.addDay(-30);
	String input_st_date = StringUtil.getString(request,"input_st_date",tdate.formatDate("yyyy-MM-dd"));
	
	String input_st_dateQLL = StringUtil.getString(request,"startDateQLL",tdate.formatDate("yyyy-MM-dd"));//QLL
	
	
	String variable = StringUtil.getString(request,"variable");
	String key = StringUtil.getString(request,"key");
	String cmd = StringUtil.getString(request,"cmd");
	TDate tDate = new TDate();
	//QLL	
		
		
		long pcidQLL = StringUtil.getLong(request,"pcidQLL");
		String storage_idQLL = StringUtil.getString(request,"storage_idQLL");
		String pro_line_idQLLStr = StringUtil.getString(request,"pro_line_idQLL");		
		long pro_line_idQLL;
		String pidQLL = StringUtil.getString(request,"pidQLL");
		if(pro_line_idQLLStr.contains("$"))
		{
			pro_line_idQLL=Long.parseLong(pro_line_idQLLStr.substring(0,pro_line_idQLLStr.indexOf("$")));
		}
		else
		{
			pro_line_idQLL = StringUtil.getLong(request,"pro_line_idQLL");
		}
	
		if ( input_st_dateQLL.equals("") )
		{	
			input_st_dateQLL = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		}
		if ( input_en_dateQLL.equals("") )
		{	
			input_en_dateQLL = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		}
	//QLL		
	
	
	if (input_st_date.equals("") && input_en_date.equals(""))
	{
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();		
	}

	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />


<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript">
function closeWin()
{
	tb_remove();
}

$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#search_product_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		updownSelect:true,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
});


$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#p_product_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		updownSelect:true,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
});

$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#search_product_nameQLL").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		updownSelect:true,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
});

function closeWinRefresh()
{
	window.location.reload();
	tb_remove();
}
$.blockUI.defaults = {
			css: { 
				padding:        '8px',
				margin:         0,
				width:          '170px', 
				top:            '45%', 
				left:           '40%', 
				textAlign:      'center', 
				color:          '#000', 
				border:         '3px solid #999999',
				backgroundColor:'#eeeeee',
				'-webkit-border-radius': '10px',
				'-moz-border-radius':    '10px',
			    '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	    		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			},
			
			// 设置遮罩层的样式
			overlayCSS:  { 
				backgroundColor:'#000', 
				opacity:        '0.8' 
			},
		baseZ: 99999, 
	    centerX: true,
	    centerY: true, 
		
		fadeOut:  1000
	};
	
function ajaxOnloadActualStoragePage(param)
{
	$.ajax({
				url: 'actual_sales.html',
				type: 'post',
				dataType: 'html',
				//timeout: 60000,
				cache:false,
				data:param,
				
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				
				error: function(){
					$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
				},
				
				success: function(html){
					$.unblockUI();
					$("#actualStorageList").html(html);
					//alert(html);
					onLoadInitZebraTable();//表格斑马线
					//$.scrollTo(0,500);//页面向上滚动
				}
			});
}


function ajaxOnloadPlanAndStockPage(param)
{
	$.ajax({
				url: 'planAndStock.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:param,
				
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				
				error: function(){
					$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
				},
				
				success: function(html){
					$.unblockUI();
					$("#planAndStockList").html(html);
					//alert(html);
					onLoadInitZebraTable();//表格斑马线
					//$.scrollTo(0,500);//页面向上滚动
				}
			});
}
function filterPlanAndStock()
{
			document.filter_search_form.action="stock_plan.html";
 		if(checkNull())
		{
			document.filter_search_form.cmdQLL.value="filterPlanAndStock";
			document.filter_search_form.storage_idQLL.value=$("#cidQLL").val();		
			document.filter_search_form.startDateQLL.value = $("#input_st_dateQLL").val();	
			document.filter_search_form.endDateQLL.value = $("#input_en_dateQLL").val();
			document.filter_search_form.pro_line_idQLL.value = $("#filter_pcid2QLL").val();//需要区分是根据类别还是产品线
			document.filter_search_form.pcidQLL.value =  $("#filter_pcidQLL").val();
			document.filter_search_form.pidQLL.value = $("#search_product_nameQLL").val();
			document.filter_search_form.submit();
		}
}


function checkNull()
{
		var cID = $("#filter_pcidQLL").val();
		var pLineID = $("#filter_pcid2QLL").val();
		var storage_id=$("#cidQLL").val();	
		var startDate = $("#input_st_dateQLL").val();	
		var endDate = $("#input_en_dateQLL").val();
		var search_product_nameQLL= $("#search_product_nameQLL").val();
		 
		if(cID!=0 && pLineID !=0  )
		{
			alert("只能根据产品类别或产品线中的一项进行统计或导出!");
			return false;
		}else
		if(cID!=0 && search_product_nameQLL !="")
		{
			alert("只能根据产品类别或商品名称中的一项进行统计或导出!");
			return false;
		}else if(pLineID!=0 && search_product_nameQLL !="")
		{
			alert("只能根据产品线商或品名称中的一项进行统计或导出!");
			return false;
		}else
		if(cID==0 && pLineID ==0 && search_product_nameQLL =="")
		{
			alert("请根据产品类别、产品线、商品名称中的一项进行统计或导出!");//如果什么都不选是按类别还是产品线来默认统计
			return false;
		}else if(storage_id==0||storage_id=="0")
		{
			alert("请选择需要统计或导出的仓库!");
			return false;
		}else 
		{
			return true;
		}
}
function exportPlanAndStock()
{
	if(checkNull())
	{
		var cID = $("#filter_pcidQLL").val();
		var pLineID = $("#filter_pcid2QLL").val();
		var storage_id=$("#cidQLL").val();	
		var startDate = $("#input_st_dateQLL").val();	
		var endDate = $("#input_en_dateQLL").val();
		var search_product_nameQLL= $("#search_product_nameQLL").val();
		var category_id= $("#filter_acid").val();
		var para = "category_id="+cID+"&pLineID="+pLineID+"&search_product_nameQLL="+search_product_nameQLL+"&storage_id="+storage_id+"&startDate="+startDate+"&endDate="+endDate;
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/exportPlanAndStockAction.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					beforeSend:function(request){
					},
					
					error: function(){
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.filter_search_form.action=date["fileurl"];
							document.filter_search_form.submit();
						}
						else
						{
							alert("该统计无数据，无法导出");
						}
					}
				});
		}
}
function searchActualStorageProductNeed()
{
	var pid = $("#search_product_name").val();
	var pro_line_id = $("#filter_pcid2").val();
	var catalog_id = $("#filter_pcid").val();
	
	var p_line_id = pro_line_id.split("-")[0];
		if(catalog_id != 0 && pid != "")
		{
			document.search_form.catalog_id.value = "0";
			if(p_line_id != 0)
			{
				document.search_form.pro_line_id.value=pro_line_id;
			}
			else
			{
				document.search_form.pro_line_id.value=p_line_id;
			}
			document.search_form.product_name.value=pid;
		}
		else if(catalog_id != 0 && p_line_id != 0)
		{
			document.search_form.catalog_id.value = "0";
			document.search_form.pro_line_id.value=pro_line_id;
			document.search_form.product_name.value.value=pid;
		}
		else if(pid != "" && p_line_id != 0)
		{
			document.search_form.catalog_id.value = catalog_id;
			document.search_form.pro_line_id.value=pro_line_id;
			document.search_form.product_name.value="";
		}
		else if(catalog_id != 0 && p_line_id != 0 && pid != "")
		{
			document.search_form.catalog_id.value = "0";
			document.search_form.pro_line_id.value=pro_line_id;
			document.search_form.product_name.value="";
		}
		else if(pro_line_id != "")
		{
			document.search_form.catalog_id.value = catalog_id;
			if(p_line_id != 0)
			{
				document.search_form.pro_line_id.value=pro_line_id;
			}
			else
			{
				document.search_form.pro_line_id.value=p_line_id;
			}
			
			document.search_form.product_name.value=pid;
		}
		document.search_form.cmd.value="filter";
		document.search_form.input_st_date.value=$("#input_st_date").val();
		document.search_form.input_en_date.value=$("#input_en_date").val();
		document.search_form.variable.value="actual";
		
		document.search_form.submit();
}

function ajaxOnloadPlanStoragePage(param)
{
	$.ajax({
				url: 'plan_sales.html',
				type: 'post',
				dataType: 'html',
				//timeout: 60000,
				cache:false,
				data:param,
				
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				
				error: function(){
					$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
				},
				
				success: function(html){
					$.unblockUI();
					$("#planStorageList").html(html);
					//alert(html);
					//onLoadInitZebraTable();//表格斑马线
					//$.scrollTo(0,500);//页面向上滚动
				}
			});
}


function searchPlanStorageProductNeed()
{
	var pid = $("#p_product_name").val();
	var pro_line_id = $("#filter_pid2").val();
	var catalog_id = $("#filter_pid").val();
	
	var p_line_id = pro_line_id.split("-")[0];
		if(catalog_id != 0 && pid != "")
		{
			document.search_form.catalog_id.value = "0";
			if(p_line_id != 0)
			{
				document.search_form.pro_line_id.value=pro_line_id;
			}
			else
			{
				document.search_form.pro_line_id.value=p_line_id;
			}
			document.search_form.product_name.value=pid;
		}
		else if(catalog_id != 0 && p_line_id != 0)
		{
			document.search_form.catalog_id.value = "0";
			document.search_form.pro_line_id.value=pro_line_id;
			document.search_form.product_name.value.value=pid;
		}
		else if(pid != "" && p_line_id != 0)
		{
			document.search_form.catalog_id.value = catalog_id;
			document.search_form.pro_line_id.value=pro_line_id;
			document.search_form.product_name.value="";
		}
		else if(catalog_id != 0 && p_line_id != 0 && pid != "")
		{
			document.search_form.catalog_id.value = "0";
			document.search_form.pro_line_id.value=pro_line_id;
			document.search_form.product_name.value="";
		}
		else if(pro_line_id != "")
		{
			document.search_form.catalog_id.value = catalog_id;
			if(p_line_id != 0)
			{
				document.search_form.pro_line_id.value=pro_line_id;
			}
			else
			{
				document.search_form.pro_line_id.value=p_line_id;
			}
			
			document.search_form.product_name.value=pid;
		}
	document.search_form.cmd.value="filter";
	document.search_form.input_st_date.value=$("#plan_st_date").val();
	document.search_form.input_en_date.value=$("#plan_en_date").val();
	document.search_form.variable.value="plan";
	document.search_form.submit();
}
 

</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<form method="post" name="search_form" action="order_process.html">
	<input type="hidden" name="cmd"/>
	<input type="hidden" name="input_st_date"/>
	<input type="hidden" name="input_en_date"/>
	<input type="hidden" name="product_name" />
	<input type="hidden" name="catalog_id"/>
	<input type="hidden" name="pro_line_id"/>
	<input type="hidden" name="variable">
</form>
<form id="filter_search_form" name="filter_search_form" action="order_process.html" method="post">
    <input name="storage_idQLL" type="hidden" />
    <input name="catalog_idQLL" type="hidden" />
    <input name="startDateQLL" type="hidden" />
    <input name="endDateQLL" type="hidden" />
    <input name="pLineQLL" type="hidden" />
    <input name="pcidQLL" type="hidden"  />
    <input type="hidden" name="pro_line_idQLL" />
    <input type="hidden" name="cmdQLL" />
    <input type="hidden" name="pidQLL" />
    <input type="hidden" name="actionQLL">
  </form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购应用  »    产品需求分布</td>
  </tr>
</table>

<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
	<td align="left" valign="top"><br/>
		<div class="demo">
			<div id="tabs">
				<ul>
					<li><a href="#actural_search_product" onclick="ajaxOnloadActualStoragePage('p=<%=p%>&input_st_date=<%=input_st_date%>&input_en_date=<%=input_en_date%>')";>实际货物需求量<span> </span></a></li>
					<li><a href="#theory_sales" onclick="ajaxOnloaTheoryStoragePage('p=<%=p %>')">理论仓库货物需求<span> </span></a></li>
					<li><a href="#original_sales" onclick="ajaxOnloadOriginalStoragePage('p=<%=p %>')">代发后本来仓库货物需求<span> </span></a></li>
					<li><a href="#plan_search_product" onclick="ajaxOnloadPlanStoragePage('p=<%=p%>&input_st_date=<%=input_st_date%>&input_en_date=<%=input_en_date%>')">计划仓库货物需求<span> </span></a></li>
					<li><a href="#plan_and_stock" onclick="ajaxOnloadPlanAndStockPage('p=<%=p%>')">计划发货与当前库存<span> </span></a></li>
				</ul>
				
				<div id="plan_and_stock">
					<table width="98%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td width="80%">
						
						<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:1000px;">
							<div style="padding-bottom:5px;color:#666666;font-size:12px;">
								&nbsp;&nbsp;&nbsp;可以选择一个产品分类或产品线：
							</div>
						<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
					  <tr>
					    <td width="423" height="29" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
					
						<ul id="categorymenuQLL" class="mcdropdown_menu">
						  <li rel="0">所有分类</li>
						  <%
						  DBRow c1QLL[] = catalogMgr.getProductCatalogByParentId(0,null);
						  for (int i=0; i<c1QLL.length; i++)
						  {
								out.println("<li rel='"+c1QLL[i].get("id",0l)+"'> "+c1QLL[i].getString("title"));
					
								  DBRow c2QLL[] = catalogMgr.getProductCatalogByParentId(c1QLL[i].get("id",0l),null);
								  if (c2QLL.length>0)
								  {
								  		out.println("<ul>");	
								  }
								  for (int ii=0; ii<c2QLL.length; ii++)
								  {
										out.println("<li rel='"+c2QLL[ii].get("id",0l)+"'> "+c2QLL[ii].getString("title"));		
										
											DBRow c3QLL[] = catalogMgr.getProductCatalogByParentId(c2QLL[ii].get("id",0l),null);
											  if (c3QLL.length>0)
											  {
													out.println("<ul>");	
											  }
												for (int iii=0; iii<c3QLL.length; iii++)
												{
														out.println("<li rel='"+c3QLL[iii].get("id",0l)+"'> "+c3QLL[iii].getString("title"));
														out.println("</li>");
												}
											  if (c3QLL.length>0)
											  {
													out.println("</ul>");	
											  }
											  
										out.println("</li>");				
								  }
								  if (c2QLL.length>0)
								  {
								  		out.println("</ul>");	
								  }
								  
								out.println("</li>");
						  }
						  %>
					</ul>
						  <input type="text" name="categoryQLL" id="categoryQLL" value="" />
						  <input type="hidden" name="filter_pcidQLL" id="filter_pcidQLL" value="0"  />
					</td>
					 
					    <td height="29" bgcolor="#eeeeee">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   仓库：&nbsp;&nbsp; 
					        <select name="cidQLL" id="cidQLL"  width="12">
							<option value="0">所有仓库</option>
					          <%
								DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
								String qx;
								
								for ( int i=0; i<treeRows.length; i++ )
								{
									if ( treeRows[i].get("parentid",0) != 0 )
									 {
									 	qx = "├ ";
									 }
									 else
									 {
									 	qx = "";
									 }
									 
									 if (treeRows[i].get("level",0)>1)
									 {
									 	continue;
									 }
								%>
					          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==StringUtil.getLong(request,"storage_idQLL")?"selected":""%>> 
					          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
					          <%=qx%>
					          <%=treeRows[i].getString("title")%>  </option>
					          <%}%>
					        </select> 
					           &nbsp;&nbsp;&nbsp; 
					          商品名称： <input type="text" name="search_product_nameQLL" id="search_product_nameQLL" value="<%=pidQLL%>" style="width:200px;" onclick="cancelCatalogAndProductQLL()"/></td>
					          <td>&nbsp; &nbsp;</td>
					  </tr>
					  <tr>
					  	<td width="423"  height="29" align="left" bgcolor="#eeeeee" style="padding-left:10px;"> 
					           <ul id="categorymenuLineQLL" class="mcdropdown_menu">
					             <li rel="0">所有产品线</li>
					             <%
						  DBRow p1QLL[] = productLineMgrTJH.getAllProductLine();
						  for (int i=0; i<p1QLL.length; i++)
						  {
								out.println("<li rel='"+p1QLL[i].get("id",0l)+"'> "+p1QLL[i].getString("name"));
								 DBRow p2QLL[] = productLineMgrTJH.getProductCatalogByProductLineId(p1QLL[i].get("id",0l));
								  if (p2QLL.length>0)
								  {
								  		out.println("<ul>");	
								  }
								  for (int ii=0; ii<p2QLL.length; ii++)
								  {
										out.println("<li rel='"+p2QLL[ii].get("id",0l)+"'> "+p2QLL[ii].getString("title"));		
										
											DBRow p3QLL[] = catalogMgr.getProductCatalogByParentId(p2QLL[ii].get("id",0l),null);
											  if (p3QLL.length>0)
											  {
													out.println("<ul>");	
											  }
												for (int iii=0; iii<p3QLL.length; iii++)
												{
														out.println("<li rel='"+p3QLL[iii].get("id",0l)+"'> "+p3QLL[iii].getString("title"));
														
														DBRow p4QLL [] = catalogMgr.getProductCatalogByParentId(p3QLL[iii].get("id",0l),null);
														if(p4QLL.length>0)
														{
															out.println("<ul>");
														}
														for(int k=0;k<p4QLL.length;k++)
														{
															out.println("<li rel='"+p4QLL[k].get("id",0l)+"'> "+p4QLL[k].getString("title"));
															DBRow plQLL [] = catalogMgr.getProductCatalogByParentId(p4QLL[k].get("id",0l),null);
															if(plQLL.length>0)
															{
																out.println("<ul>");
																for(int kk=0;kk<plQLL.length;kk++)
																{
																	out.println("<li rel='"+plQLL[kk].get("id",0l)+"'> "+plQLL[kk].getString("title"));
																	out.println("</li>");
																}
																if(plQLL.length>0)
																{
																	out.println("</ul>");
																}
															}
															out.println("</li>");
														}
														if(p4QLL.length>0)
														{
															out.println("</ul>");	
														}
														out.println("</li>");
												}
											  if (p3QLL.length>0)
											  {
													out.println("</ul>");	
											  }
											  
										out.println("</li>");				
								  }
								  if (p2QLL.length>0)
								  {
								  		out.println("</ul>");	
								  }
								  
								out.println("</li>");
						  }
						  %>
					           </ul>
					           <input type="text" name="productLineQLL" id="productLineQLL" value="" />
					           <input type="hidden" name="filter_pcid2QLL" id="filter_pcid2QLL" value="0"  />
					        <script>
							$("#categoryQLL").mcDropdown("#categorymenuQLL",{
									allowParentSelect:true,
									  select: 
									  		
											function (id,name)
											{
												$("#filter_pcidQLL").val(id);
												if(id!=0)
												{
													$("#productLineQLL").mcDropdown("#categorymenuLineQLL").setValue(0);
													$("#search_product_nameQLL").val("");
												}
											}
							
							});
							$("#productLineQLL").mcDropdown("#categorymenuLineQLL",{
									allowParentSelect:true,
									  select: 
									  		
											function (id,name)
											{
												if(name.indexOf("»") > -1 || id==0)
												{
													$("#filter_pcid2QLL").val(id);
													if(id!=0)
													{
														$("#categoryQLL").mcDropdown("#categorymenuQLL").setValue(0);
														$("#search_product_nameQLL").val("");
													}
												}
												else 
												{
													$("#filter_pcid2QLL").val(id+"$");
													if(id!=0)
													{
														$("#categoryQLL").mcDropdown("#categorymenuQLL").setValue(0);
														$("#search_product_nameQLL").val("");
													}
												}
											}
					
							});
					<%
					if (pcidQLL>0)
					{
					%>
					$("#categoryQLL").mcDropdown("#categorymenuQLL").setValue(<%=pcidQLL%>);
					<%
					}
					else
					{
					%>
					$("#categoryQLL").mcDropdown("#categorymenuQLL").setValue(0);
					<%
					}
					%> 
					
					<%
					 if (pro_line_idQLL>0)
					{%>
						$("#productLineQLL").mcDropdown("#categorymenuLineQLL").setValue(<%=pro_line_idQLL%>);
					<%
					}
					else
					{
					%>
						$("#productLineQLL").mcDropdown("#categorymenuLineQLL").setValue(0);
					<%
					}
					%> 
					</script>&nbsp;&nbsp;
					
					</td>
					<td height="29" bgcolor="#eeeeee">
					&nbsp;&nbsp; &nbsp;&nbsp;开始日期：<input name="input_st_dateQLL" type="text" id="input_st_dateQLL" size="10" value="<%=input_st_dateQLL%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
			    	  &nbsp;截止日期：<input name="input_en_dateQLL" type="text" id="input_en_dateQLL" size="10" value="<%=input_en_dateQLL%>" readonly="readonly" /> &nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;<input name="searchQLL" type="button" class="button_long_search" value="统计"  onClick="filterPlanAndStock()"/>
					    <script>
					    	$("#input_st_dateQLL").date_input();
							$("#input_en_dateQLL").date_input();
					    </script>
					    </p></td>
					    <td><input name="ExportQLL" type="button" class="long-button" value="导出" onClick="exportPlanAndStock()" /></td>
					  </tr>
					</table>	
					</div>
					
					</td>
				    <td width="35%" align="right">&nbsp;</td>
				  </tr>
				</table>
				<br/>
					
					<div id="planAndStockList">
	
					</div>	
				</div>
				
				
				<div id="actural_search_product">
					<table width="98%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td width="65%">
						
						<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
						
						<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
					  <tr>
					    <td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
					
						<ul id="categorymenu" class="mcdropdown_menu">
						  <li rel="0">所有分类</li>
						  <%
						  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
						  for (int i=0; i<c1.length; i++)
						  {
								out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
					
								  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
								  if (c2.length>0)
								  {
								  		out.println("<ul>");	
								  }
								  for (int ii=0; ii<c2.length; ii++)
								  {
										out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
										
											DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
											  if (c3.length>0)
											  {
													out.println("<ul>");	
											  }
												for (int iii=0; iii<c3.length; iii++)
												{
														out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
														out.println("</li>");
												}
											  if (c3.length>0)
											  {
													out.println("</ul>");	
											  }
											  
										out.println("</li>");				
								  }
								  if (c2.length>0)
								  {
								  		out.println("</ul>");	
								  }
								  
								out.println("</li>");
						  }
						  %>
					</ul>
						  <input type="text" name="category" id="category" value="" />
						  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />
					</td>
					 
					    <td height="29" bgcolor="#eeeeee">
						<!-- <textarea name="all_catalog" class="input-line" id="all_catalog"></textarea> -->
					           &nbsp;
					          商品名称： <input type="text" name="search_product_name" id="search_product_name" onclick="cancelCatalogAndProductLine()" value="<%=product_name%>" style="width:200px;"/></td>
					  </tr>
					  <tr>
					  	<td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;"> 
					           <ul id="pLinemenu" class="mcdropdown_menu">
					             <li rel="0">所有产品线</li>
					             <%
						  DBRow p1[] = productLineMgrTJH.getAllProductLine();
						  for (int i=0; i<p1.length; i++)
						  {
								out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
					
								  DBRow p2[] = productLineMgrTJH.getProductCatalogByProductLineId(p1[i].get("id",0l));
								  if (p2.length>0)
								  {
								  		out.println("<ul>");	
								  }
								  for (int ii=0; ii<p2.length; ii++)
								  {
										out.println("<li rel='"+p2[ii].get("id",0l)+"'> "+p2[ii].getString("title"));		
										
											DBRow p3[] = catalogMgr.getProductCatalogByParentId(p2[ii].get("id",0l),null);
											  if (p3.length>0)
											  {
													out.println("<ul>");	
											  }
												for (int iii=0; iii<p3.length; iii++)
												{
														out.println("<li rel='"+p3[iii].get("id",0l)+"'> "+p3[iii].getString("title"));
														
														DBRow p4 [] = catalogMgr.getProductCatalogByParentId(p3[iii].get("id",0l),null);
														if(p4.length>0)
														{
															out.println("<ul>");
														}
														for(int k=0;k<p4.length;k++)
														{
															out.println("<li rel='"+p4[k].get("id",0l)+"'> "+p4[k].getString("title"));
															DBRow pl [] = catalogMgr.getProductCatalogByParentId(p4[k].get("id",0l),null);
															if(pl.length>0)
															{
																out.println("<ul>");
																for(int kk=0;kk<pl.length;kk++)
																{
																	out.println("<li rel='"+pl[kk].get("id",0l)+"'> "+pl[kk].getString("title"));
																	out.println("</li>");
																}
																if(pl.length>0)
																{
																	out.println("</ul>");
																}
															}
															out.println("</li>");
														}
														if(p4.length>0)
														{
															out.println("</ul>");	
														}
														out.println("</li>");
												}
											  if (p3.length>0)
											  {
													out.println("</ul>");	
											  }
											  
										out.println("</li>");				
								  }
								  if (p2.length>0)
								  {
								  		out.println("</ul>");	
								  }
								  
								out.println("</li>");
						  }
						  %>
					           </ul>
					           <input type="text" name="productLine" id="productLine" value="" />
					           <input type="hidden" name="filter_pcid2" id="filter_pcid2" value="0"  />
					           <script>
								$("#category").mcDropdown("#categorymenu",{
										allowParentSelect:true,
										  select: 
										  		
												function (id,name)
												{
													$("#filter_pcid").val(id);
													if(id!=0)
													{
														$("#productLine").mcDropdown("#pLinemenu").setValue(0);
														$("#search_product_name").val("");
													}
												}
								
								});
					
								$("#productLine").mcDropdown("#pLinemenu",{
										allowParentSelect:true,
										  select: 
										  		
												function (id,name)
												{
													if(name.indexOf("»") > -1)
													{
														$("#filter_pcid2").val(id);
														if(id!=0)
														{
															$("#category").mcDropdown("#categorymenu").setValue(0);
															$("#search_product_name").val("");
														}
													}
													else
													{
														$("#filter_pcid2").val(id+"-p");
														if(id!=0)
														{
															$("#category").mcDropdown("#categorymenu").setValue(0);
															$("#search_product_name").val("");
														}
													}
												}
								
								});
								<%
					if (catalog_id>0)
					{
					%>
					$("#category").mcDropdown("#categorymenu").setValue(<%=catalog_id%>);
					<%
					}
					else
					{
					%>
					$("#category").mcDropdown("#categorymenu").setValue(0);
					<%
					}
					%> 
					<%
					if (!pro_line_id.equals("0") && !pro_line_id.equals(""))
					{
						long actual_lineId = Long.parseLong(pro_line_id.split("-")[0]);
					%>
					$("#productLine").mcDropdown("#pLinemenu").setValue(<%=actual_lineId%>);
					<%
					}
					else
					{
					%>
					$("#productLine").mcDropdown("#pLinemenu").setValue(0);
					<%
					}
					%> 
					</script>&nbsp;&nbsp;
					
					</td>
					<td height="29" bgcolor="#eeeeee">
					    	发货开始时间：<input name="input_st_date" type="text" id="input_st_date" size="10" value="<%=input_st_date%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
					    	结束时间：<input name="input_en_date" type="text" id="input_en_date" size="10" value="<%=input_en_date%>" readonly="readonly" /> &nbsp;&nbsp;&nbsp;
					    <input name="Submit" type="button" class="button_long_search" value="查询" onClick="searchActualStorageProductNeed()" />
					    <script>
					    	$("#input_st_date").date_input();
							$("#input_en_date").date_input();
					    </script>
					    </p></td>
					  </tr>
					</table>	
	</div>
	
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br/>
<div id="actualStorageList">

</div>
</div>
				
				<div id="plan_search_product">
					<table width="98%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td width="65%">
						
						<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
						
						<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
					  <tr>
					    <td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
					
						<ul id="plan_categorymenu" class="mcdropdown_menu">
						  <li rel="0">所有分类</li>
						  <%
						  DBRow catalog1[] = catalogMgr.getProductCatalogByParentId(0,null);
						  for (int i=0; i<catalog1.length; i++)
						  {
								out.println("<li rel='"+catalog1[i].get("id",0l)+"'> "+catalog1[i].getString("title"));
					
								  DBRow catalog2[] = catalogMgr.getProductCatalogByParentId(catalog1[i].get("id",0l),null);
								  if (catalog2.length>0)
								  {
								  		out.println("<ul>");	
								  }
								  for (int ii=0; ii<catalog2.length; ii++)
								  {
										out.println("<li rel='"+catalog2[ii].get("id",0l)+"'> "+catalog2[ii].getString("title"));		
										
											DBRow catalog3[] = catalogMgr.getProductCatalogByParentId(catalog2[ii].get("id",0l),null);
											  if (catalog3.length>0)
											  {
													out.println("<ul>");	
											  }
												for (int iii=0; iii<catalog3.length; iii++)
												{
														out.println("<li rel='"+catalog3[iii].get("id",0l)+"'> "+catalog3[iii].getString("title"));
														out.println("</li>");
												}
											  if (catalog3.length>0)
											  {
													out.println("</ul>");	
											  }
											  
										out.println("</li>");				
								  }
								  if (catalog2.length>0)
								  {
								  		out.println("</ul>");	
								  }
								  
								out.println("</li>");
						  }
						  %>
					</ul>
						  <input type="text" name="p_category" id="p_category" value="" />
						  <input type="hidden" name="filter_pid" id="filter_pid" value="0"  />
					</td>
					 
					    <td height="29" bgcolor="#eeeeee">
						<!-- <textarea name="all_catalog" class="input-line" id="all_catalog"></textarea> -->
					           &nbsp;
					          商品名称： <input type="text" name="p_product_name" id="p_product_name" value="<%=product_name%>" style="width:200px;" onclick="cancelCatalogAndProductWithPlan()"/></td>
					  </tr>
					  <tr>
					  	<td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;"> 
					           <ul id="p_line_menu" class="mcdropdown_menu">
					             <li rel="0">所有产品线</li>
					             <%
						  DBRow p_line1[] = productLineMgrTJH.getAllProductLine();
						  for (int i=0; i<p_line1.length; i++)
						  {
								out.println("<li rel='"+p_line1[i].get("id",0l)+"'> "+p_line1[i].getString("name"));
					
								  DBRow p_line2[] = productLineMgrTJH.getProductCatalogByProductLineId(p_line1[i].get("id",0l));
								  if (p_line2.length>0)
								  {
								  		out.println("<ul>");	
								  }
								  for (int ii=0; ii<p_line2.length; ii++)
								  {
										out.println("<li rel='"+p_line2[ii].get("id",0l)+"'> "+p_line2[ii].getString("title"));		
										
											DBRow p_line3[] = catalogMgr.getProductCatalogByParentId(p_line2[ii].get("id",0l),null);
											  if (p_line3.length>0)
											  {
													out.println("<ul>");	
											  }
												for (int iii=0; iii<p_line3.length; iii++)
												{
														out.println("<li rel='"+p_line3[iii].get("id",0l)+"'> "+p_line3[iii].getString("title"));
														
														DBRow p4 [] = catalogMgr.getProductCatalogByParentId(p_line3[iii].get("id",0l),null);
														if(p4.length>0)
														{
															out.println("<ul>");
														}
														for(int k=0;k<p4.length;k++)
														{
															out.println("<li rel='"+p4[k].get("id",0l)+"'> "+p4[k].getString("title"));
															DBRow pl [] = catalogMgr.getProductCatalogByParentId(p4[k].get("id",0l),null);
															if(pl.length>0)
															{
																out.println("<ul>");
																for(int kk=0;kk<pl.length;kk++)
																{
																	out.println("<li rel='"+pl[kk].get("id",0l)+"'> "+pl[kk].getString("title"));
																	out.println("</li>");
																}
																if(pl.length>0)
																{
																	out.println("</ul>");
																}
															}
															out.println("</li>");
														}
														if(p4.length>0)
														{
															out.println("</ul>");	
														}
														out.println("</li>");
												}
											  if (p_line3.length>0)
											  {
													out.println("</ul>");	
											  }
											  
										out.println("</li>");				
								  }
								  if (p_line2.length>0)
								  {
								  		out.println("</ul>");	
								  }
								  
								out.println("</li>");
						  }
						  %>
					           </ul>
					           <input type="text" name="p_productLine" id="p_productLine" value="" />
					           <input type="hidden" name="filter_pid2" id="filter_pid2" value="0"  />
					           <script>
					$("#p_category").mcDropdown("#plan_categorymenu",{
							allowParentSelect:true,
							  select: 
							  		
									function (id,name)
									{
										$("#filter_pid").val(id);
										if(id!=0)
										{
											$("#p_productLine").mcDropdown("#p_line_menu").setValue(0);
											$("#p_product_name").val("");
										}
									}
					
					});
					
					$("#p_productLine").mcDropdown("#p_line_menu",{
							allowParentSelect:true,
							  select: 
							  		
									function (id,name)
									{
										if(name.indexOf("»") > -1)
										{
											$("#filter_pid2").val(id);
											if(id!=0)
											{
												$("#p_category").mcDropdown("#plan_categorymenu").setValue(0);
												$("#p_product_name").val("");
											}
										}
										else
										{
											$("#filter_pid2").val(id+"-p");
											if(id!=0)
											{
												$("#p_category").mcDropdown("#plan_categorymenu").setValue(0);
												$("#p_product_name").val("");
											}
										}
										
									}
					
					});
					<%
					if (catalog_id>0)
					{
					%>
					$("#p_category").mcDropdown("#plan_categorymenu").setValue(<%=catalog_id%>);
					<%
					}
					else
					{
					%>
					$("#p_category").mcDropdown("#plan_categorymenu").setValue(0);
					<%
					}
					%> 
					<%
					if (!pro_line_id.equals("0") && !pro_line_id.equals(""))
					{
						long plan_line_id = Long.parseLong(pro_line_id.split("-")[0]);
					%>
					$("#p_productLine").mcDropdown("#p_line_menu").setValue(<%=plan_line_id%>);
					<%
					}
					else
					{
					%>
					$("#p_productLine").mcDropdown("#p_line_menu").setValue(0);
					<%
					}
					%> 
					</script>&nbsp;&nbsp;
					
					</td>
					<td height="29" bgcolor="#eeeeee">
					    	发货开始时间：<input name="input_st_date" type="text" id="plan_st_date" size="10" value="<%=input_st_date%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
					    	结束时间：<input name="input_en_date" type="text" id="plan_en_date" size="10" value="<%=input_en_date%>" readonly="readonly" /> &nbsp;&nbsp;&nbsp;
					    <input name="Submit" type="button" class="button_long_search" value="查询" onClick="searchPlanStorageProductNeed()" />
					    <script>
					    	$("#plan_st_date").date_input();
							$("#plan_en_date").date_input();
					    </script>
					    </p></td>
					  </tr>
					</table>	
	</div>
	
	
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br/>
<div id="planStorageList">

</div>

	</div>
				<div id="theory_sales"></div>
				<div id="original_sales"></div>
			</div>
		</div>
<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	</script>
<br>	

	</td>
</tr>
</table>
<br/>

  <script type="text/javascript">
  
  function ajaxOnloaTheoryStoragePage(param)
  {
  	$.ajax({
				url: 'theory_sales.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:param,
				
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				
				error: function(){
					$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
				},
				
				success: function(html){
					$.unblockUI();
					$("#theory_sales").html(html);
					//onLoadInitZebraTable();//表格斑马线
					//$.scrollTo(0,500);//页面向上滚动
				}
			});
  }
  
  function ajaxOnloadOriginalStoragePage(param)
  {
  	$.ajax({
		url: 'original_sales.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:param,
		
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		
		error: function(){
			$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
		},
		
		success: function(html){
			$.unblockUI();
			$("#original_sales").html(html);
			//onLoadInitZebraTable();//表格斑马线
			//$.scrollTo(0,500);//页面向上滚动
		}
	});
  }
  

<%
	if(cmd.equals("filter") && variable.equals("plan"))
	{		
%>
ajaxOnloadPlanStoragePage("cmd=filter&p=<%=p%>&input_st_date=<%=input_st_date%>&input_en_date=<%=input_en_date%>&catalog_id=<%=catalog_id%>&product_name=<%=product_name%>&pro_line_id=<%=pro_line_id%>&variable=<%=variable%>");

<%
	}
	else
	{
%>
ajaxOnloadPlanStoragePage("p=<%=p%>&input_st_date=<%=input_st_date%>&input_en_date=<%=input_en_date%>");
<%
	}
%>



<%
	if(cmd.equals("filter") && variable.equals("actual"))
	{
		
%>
ajaxOnloadActualStoragePage("cmd=filter&p=<%=p%>&input_st_date=<%=input_st_date%>&input_en_date=<%=input_en_date%>&catalog_id=<%=catalog_id%>&product_name=<%=product_name%>&pro_line_id=<%=pro_line_id%>&variable=<%=variable%>");

<%
	}
	else
	{
%>
ajaxOnloadActualStoragePage('p=<%=p%>&input_st_date=<%=input_st_date%>&input_en_date=<%=input_en_date%>')
<%
	}
%>

ajaxOnloaTheoryStoragePage("p=<%=p%>");
ajaxOnloadOriginalStoragePage("p=<%=p%>")

function go2page(p)
{
    <%
		if(cmd.equals("filter"))
		{
	%>
		ajaxOnloadPlanStoragePage("cmd=filter&p="+p+"&input_st_date=<%=input_st_date%>&input_en_date=<%=input_en_date%>&catalog_id=<%=catalog_id%>&product_name=<%=product_name%>&pro_line_id=<%=pro_line_id%>&variable=<%=variable%>");
	<%
	}
	else
	{
	%>
		ajaxOnloadPlanStoragePage("p="+p+"&input_st_date=<%=input_st_date%>&input_en_date=<%=input_en_date%>")
	<%
	}	
	%>
}

function go2(p)
{
	ajaxOnloaTheoryStoragePage("p="+p);
}

function go3page(p)
{
	<%
		if(cmd.equals("filter"))
		{
	%>
		ajaxOnloadActualStoragePage("cmd=filter&p="+p+"&input_st_date=<%=input_st_date%>&input_en_date=<%=input_en_date%>&catalog_id=<%=catalog_id%>&product_name=<%=product_name%>&pro_line_id=<%=pro_line_id%>&variable=<%=variable%>");
	<%
	}
	else
	{
	%>
	
		ajaxOnloadActualStoragePage("p="+p+"&input_st_date=<%=input_st_date%>&input_en_date=<%=input_en_date%>")
		<%
	}	
	%>
}

function goPage(p)
{
	ajaxOnloadOriginalStoragePage("p="+p);
}
 
 ajaxOnloadPlanAndStockPage("cmdQLL=filterPlanAndStock&p=<%=p%>&startDateQLL=<%=input_st_dateQLL%>&endDateQLL=<%=input_en_dateQLL%>&storage_idQLL=<%=storage_idQLL%>&pcidQLL=<%=pcidQLL%>&pro_line_idQLL=<%=pro_line_idQLLStr%>&pidQLL=<%=pidQLL%>");
 
function goPlanAndStockPage(p)
{
	
	ajaxOnloadPlanAndStockPage("cmdQLL=filterPlanAndStock&p="+p+"&startDateQLL=<%=input_st_dateQLL%>&endDateQLL=<%=input_en_dateQLL%>&storage_idQLL=<%=storage_idQLL%>&pcidQLL=<%=pcidQLL%>&pro_line_idQLL=<%=pro_line_idQLLStr%>&pidQLL=<%=pidQLL%>");
 
}

function cancelCatalogAndProductLine()
{
	$("#category").mcDropdown("#categorymenu").setValue(0);
	$("#productLine").mcDropdown("#pLinemenu").setValue(0);
}

function cancelCatalogAndProductWithPlan()
{
	$("#p_category").mcDropdown("#plan_categorymenu").setValue(0);
	$("#p_productLine").mcDropdown("#p_line_menu").setValue(0);
}

function cancelCatalogAndProductQLL()
{
	$("#productLineQLL").mcDropdown("#categorymenuLineQLL").setValue(0);
	$("#categoryQLL").mcDropdown("#categorymenuQLL").setValue(0);
}
  </script>

</body>
</html>
