<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.api.qll.OrdersProcessMgrQLL"%>
<%@ include file="../../include.jsp"%>
<%
	long storage_id = StringUtil.getLong(request,"storage_idQLL");
	String startDate = StringUtil.getString(request,"startDateQLL");
	String endDate = StringUtil.getString(request,"endDateQLL");
	String pid = StringUtil.getString(request,"pidQLL");
	double average = 0;
	float sum = 0;
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	long pcid = StringUtil.getLong(request,"pcidQLL");
	long pro_line_id = StringUtil.getLong(request,"pro_line_idQLL");
	String pro_line = StringUtil.getString(request,"pro_line_idQLL");
	if(pro_line.contains("$"))
	{
		pro_line_id=Long.parseLong(pro_line.substring(0,pro_line.indexOf("$")));//默认值使用
	}
	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	Tree treeLine = new Tree(ConfigBean.getStringValue("product_line_define"));
	
	DBRow rows[]=null;
	DBRow DateRow[]= null;
 	if(pcid!=0 && pro_line.equals("0") && pid.equals(""))//类别统计
 	{
 		DateRow= ordersProcessMgrQLL.getDate(startDate,endDate);
	 	rows = ordersProcessMgrQLL.buildPlanDate(storage_id,pcid,startDate,endDate,pc);
 	}
 	else
 	if(pcid==0 && !pro_line.equals("0") && !pro_line.equals(""))//产品线统计
 	{
 		DateRow= ordersProcessMgrQLL.getDate(startDate,endDate);
 		rows = ordersProcessMgrQLL.buildPlanLineDate(storage_id,pro_line ,startDate,endDate,pc);
 	}
 	else
 	if(pcid==0 && pro_line.equals("0") && !pid.equals(""))//商品名称统计
 	{
 		DateRow= ordersProcessMgrQLL.getDate(startDate,endDate);
 		rows = ordersProcessMgrQLL.buildPidDate(pid,startDate,endDate,storage_id ,pc); 
 	}
%>
<%
	String start_need_date = StringUtil.getString(request,"storage_idQLL");
	String end_need_date = StringUtil.getString(request,"endDateQLL");
	TDate tDate = new TDate();
	if ( start_need_date.equals("") && end_need_date.equals(""))
	{
		start_need_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		end_need_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	}
%>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>无标题文档</title>
 <script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/select.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
  </head>
   		
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="onLoadInitZebraTable()"   >
  <form id="dataForm" name="dataForm" action="planAndStock.html" method="get">
    <input name="storage_id" type="hidden" value="<%=storage_id %>"/>
    <input name="catalog_id" type="hidden" value="<%=pcid %>"/>
    <input name="startDate" type="hidden" value="<%=startDate %>"/>
    <input name="endDate" type="hidden" value="<%=endDate %>"/>
    <input name="pLine" type="hidden" value="<%=pro_line %>"/>
    <input name="pcid" type="hidden"  value="<%=pcid %>"/>
    <input name="pid" type="hidden" value="<%=pid %>>"/>
    <input type="hidden" name="pro_line_id" value="<%=pro_line_id%>">
    <input type="hidden" name="p"  >
  </form>
		
	 
		<%
			if(rows!=null && rows.length!=0&&!rows[0].getString("date_0").equals(""))
			{
				%>
				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable" > 
				<tr> 
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">产品名称</th>
					   <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">当前库存</th>
					 <%
						if(DateRow!=null && DateRow.length!=0 )
						{
							for(int j=0; j<DateRow.length; j++ )
							{
							%>
								<th  align="center"  class="right-title" style="vertical-align: center;text-align: center;"><%=DateRow[j].getString("date")%></th>
							<% 
							}
						}					 
					  %>
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">平均值</th>
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">总计</th>
					   
				</tr>
				<% 
					for(int i=0; i<rows.length;i++)
					{
						sum = 0 ;
					%>
					<tr>
						 <td width="10%" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%= rows[i].getString("product_title")%></td>
						  <td width="10%" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= rows[i].getString("store_count")%> </td>  
						 <%
							 for(int j=0; j<DateRow.length; j++ )
								{
									if(!rows[i].getString("date_"+j).equals(""))
									{
										if(!rows[i].getString("date_"+j).equals("-"))
										{
											sum+=Float.parseFloat(rows[i].getString("date_"+j));
										}
								%>
									<td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= rows[i].getString("date_"+j)%>  </td>
								<% 
								}}
								if(!rows[0].getString("date_0").equals(""))
								{
								if(sum!=0)
								{
									average =   (Math.round(sum/DateRow.length*10))/10.0; 
								}
						  %>
						 <td width="10%" align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%=average %></td> 
						 <td width="10%" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%=sum %></td>
						 </tr> 
					<% 
						}}
				%>
				 </table>
				<% 
			}
		 %>
		
		<br />
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:goPlanAndStockPage(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:goPlanAndStockPage(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:goPlanAndStockPage(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:goPlanAndStockPage(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:goPlanAndStockPage(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>
  	  
  </body>
</html>
 
