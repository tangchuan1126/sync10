<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%

	long catalog_id = StringUtil.getLong(request,"catalog_id");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
	String product_name = StringUtil.getString(request,"product_name");
	String input_st_date = StringUtil.getString(request,"input_st_date");
	String input_en_date = StringUtil.getString(request,"input_en_date");
	String cmd = StringUtil.getString(request,"cmd");
	String variable = StringUtil.getString(request,"variable","actual");
	long ccid = StringUtil.getLong(request,"ccid");
	long pro_id = StringUtil.getLong(request,"pro_id");
	long ca_id = StringUtil.getLong(request,"ca_id");
	int type = StringUtil.getInt(request,"type");//1代表根据选择地域求和统计，2代表根据选择地域的下一级地域分布统计
	
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	
	TDate tDate = new TDate();

	if (input_st_date.equals("") && input_en_date.equals(""))
	{
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		
	}
	
	DBRow[] rows = null;
	
	DBRow[] DateRow= ordersProcessMgrQLL.getDate(input_st_date,input_en_date);
	rows = orderProcessMgr.productDemandAnalysis(input_st_date,input_en_date,catalog_id,pro_line_id,product_name,ca_id,ccid,pro_id,type,pc);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/select.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>

</head>
<script language="javascript">

</script>
<body>


<%
			if(rows!=null && rows.length!=0)
			{
				%>
				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable" > 
				<tr> 
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">所属产品线</th>
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">商品分类</th>
					  <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">产品名称</th>
					  <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">销售区域</th>
					  <%
					  	if(ca_id>0)
					  	{
					  %>
					  <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">销售国家</th>
					  <%		
					  	}
					  %>
					  <%
					  	if(ccid>0)
					  	{
					  %>
					  <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">销售地区</th>
					  <%		
					  	}
					  %>
					  <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">阶段总需求</th>
					  <th width="10%" align="center" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">阶段平均需求</th>
					 <%
						if(DateRow!=null && DateRow.length!=0 )
						{
							for(int j=0; j<DateRow.length; j++ )
							{
							%>
								<th  align="center"  class="right-title" style="vertical-align: center;text-align: center;"><%=DateRow[j].getString("date")%></th>
							<% 
							}
						}					 
					  %>
				</tr>
				<% 
					for(int i=0; i<rows.length;i++)
					{
				%>
					<tr>
						 <td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%= rows[i].getString("pl_name")%>&nbsp;</td>
						 <td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%= rows[i].getString("title")%></td>
						 <td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%= rows[i].getString("p_name")%></td>
						 <td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
						 	<% 
						 		if(ca_id==0&&type==1)
						 		{
						 			out.print("All");
						 		}
						 		else
						 		{
						 			out.print(rows[i].getString("area_name"));
						 		}
						 		
						 	%>
						 </td>
						 <% 
						 	if(ca_id>0)
						 	{
						 %>
						 	<td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
						 	<% 
						 		if(ccid==0&&type==1)
						 		{
						 			out.print("All");
						 		}
						 		else
						 		{
						 			out.print(rows[i].getString("c_country"));
						 		}
						 		
						 	%>
						 	</td>
						 <%
						 	}
						 %>
						 
						 <% 
						 	if(ccid>0)
						 	{
						 %>
						 	<td width="10%" nowrap="nowrap" align="center" height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
						 	<% 
						 		if(pro_id==0&&type==1)
						 		{
						 			out.print("All");
						 		}
						 		else
						 		{
						 			out.print(rows[i].getString("pro_name"));
						 		}
						 		
						 	%>
						 	</td>
						 <%
						 	}
						 %>
						 <td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= rows[i].getString("sum_quantity")%>  </td>
						 <td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= MoneyUtil.round(rows[i].get("sum_quantity",0d)/DateRow.length,2)%>  </td>
						 
						 <%
							 for(int j=0; j<DateRow.length; j++ )
								{
									if(!rows[i].getString("date_"+j).equals(""))
									{
								%>
									<td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= rows[i].getString("date_"+j)%>  </td>
								<% 
									}
								}
						  %>
						 </tr> 
					<% 
						}
				%>
				 </table>
				<% 
			}
		 %>
<br />

<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:goPage('p=1','actual_sales_analysi.html','actural_search_product_')",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:goPage('p="+pre+"','actual_sales_analysi.html','actural_search_product_')",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:goPage('p="+next+"','actual_sales_analysi.html','actural_search_product_')",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:goPage('p="+pc.getPageCount()+"','actual_sales_analysi.html','actural_search_product_')",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:goPage('p='document.getElementById('jump_p2').value+'','actual_sales_analysi.html')" value="GO"/>
    </td>
  </tr>
</table>
</body>
</html>



