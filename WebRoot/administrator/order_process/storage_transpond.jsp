<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

DBRow [] rows = storageTranspondMgr.getAllStorageTranspond(pc);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
		
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript">
function addStorage_transpond()
{
	var f = document.add_storage_transpond;
	if(f.y_sid.value == 0)
	{
		alert("请选择优先发货仓库");
		return false;
	}
	if(f.catalog_id.value == 0)
	{
		alert("请选择产品分类");
		return false;
	}
	if(f.d_sid.value == 0)
	{
		alert("请选择需要的代发仓库");
		return false;
	}
	if(f.type_id.value == 0)
	{
		alert("请选择商品的代发形式");
		return false;
	}
	else
	{
		f.submit();
	}
}


function modStorage_transpond(transpond_id)
{
	tb_show('修改仓库间商品的代发信息','mod_storage_transpond.html?transpond_id='+transpond_id+'&TB_iframe=true&height=300&width=450',false);
}

function deleteStorage(transpond_id)
{
	if(confirm("您确定要删除吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order_process/delStoragetranspond.action";
		document.del_form.transpond_id.value = transpond_id;
		document.del_form.submit();
	}
}
function closeWin()
{
	tb_remove();
}

function closeWinRefresh()
{
	window.location.reload();
	tb_remove();
}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购应用  »    仓库间商品代发关系</td>
  </tr>
</table>
<br>
<form name="mod_form" id="mod_form">
	<input type="hidden" name="transpondId" />
	<input type="hidden" name="prior_sid" />
	<input type="hidden" name="catalog_id" />
	<input type="hidden" name="d_sid" />
	<input type="hidden" name="type_id"/>
</form>

<form name="del_form" id="del_form">
	<input type="hidden" name="transpond_id" />
</form>

<form name="add_storage_transpond" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order_process/addStorageTranspond.action">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	    <tr> 
	        <th width="19%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">优先发货仓库</th>
	        <th width="24%" class="right-title" style="vertical-align: center;text-align: center;">产品类别</th>
	        <th width="18%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">代发仓库</th>
	        <th width="19%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">代发形式</th>
	        <th width="20%" class="right-title" style="vertical-align: center;text-align: center;">操作</th>
	    </tr>
	    <tr> 
	      <td height="30" align="center" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
				<select name="y_sid" id="y_sid">
					<option value="0">请选择优先发货仓库...</option>
			          <%
						DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
						String qx;
						
						for ( int i=0; i<treeRows.length; i++ )
						{
							if ( treeRows[i].get("parentid",0) != 0 )
							 {
							 	qx = "├ ";
							 }
							 else
							 {
							 	qx = "";
							 }
							 
							 if (treeRows[i].get("level",0)>1)
							 {
							 	continue;
							 }
						%>
			          <option value="<%=treeRows[i].getString("id")%>"> 
			          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
			          <%=qx%>
			          <%=treeRows[i].getString("title")%>          </option>
			          <%
			}
			%>
			        </select> 
		 </td>
	      <td align="center" valign="middle" style='word-break:break-all;' >
	      	  <select name="catalog_id" id="catalog_id">
			      <option value="0">请选择产品分类...</option>
			      <%
					String qx_catalog="";
					Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
					DBRow catalogTree[] = catalogMgr.getProductCatalogTree();
					
					for ( int i=0; i<catalogTree.length; i++ )
					{
						if ( catalogTree[i].get("parentid",0) != 0 )
						 {
						 	qx_catalog = "├ ";
						 }
						 else
						 {
						 	qx_catalog = "";
						 }
					%>
			      <option value="<%=catalogTree[i].getString("id")%>"> <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",catalogTree[i].get("level",0))%> <%=qx_catalog%> <%=catalogTree[i].getString("title")%> </option>
			      <%
					}
				  %>
    </select>
		  </td>
	      <td align="center" valign="middle">
		  		<select name="d_sid" id="d_sid">
					<option value="0">请选择代发仓库...</option>
			          <%
						DBRow storageTree[] = catalogMgr.getProductStorageCatalogTree();
						String qx_s;
						
						for ( int i=0; i<storageTree.length; i++ )
						{
							if ( storageTree[i].get("parentid",0) != 0 )
							 {
							 	qx_s = "├ ";
							 }
							 else
							 {
							 	qx_s = "";
							 }
							 
							 if (storageTree[i].get("level",0)>1)
							 {
							 	continue;
							 }
						%>
						          <option value="<%=storageTree[i].getString("id")%>"> 
						          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageTree[i].get("level",0))%>
						          <%=qx_s%>
						          <%=storageTree[i].getString("title")%>          </option>
						          <%
						}
						%>
			        </select> 
		  </td>
		  
		  <td align="center" valign="middle">
	  		<select id="type_id" name="type_id">
	  			<option value="0">请选择商品代发形式...</option>
	  			<option value="1">原状</option>
	  			<option value="2">拆散</option>
	  		</select>
		  </td>
	      <td align="center" valign="middle">
	      	<input name="Submit" type="button" class="long-button-add" value="增加" onClick="addStorage_transpond()">
	     </td>
	    </tr>
	</table>
</form>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
			<th width="19%" align="center" class="right-title" style="vertical-align: center;text-align: center;">优先发货仓库</th>
	        <th width="24%" class="right-title" style="vertical-align: center;text-align: center;">产品类别</th>
	        <th width="18%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">代发仓库</th>
	        <th width="19%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">代发形式</th>
	        <th width="20%" class="right-title" style="vertical-align: center;text-align: center;">操作</th>	
	        <%
	        	for(int i=0;i<rows.length;i++)
	        	{
	         %>        
	        <tr>
	        	<td height="40" align="center" valign="middle">
	        		<%
	        			DBRow y_storage = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("y_sid",0l));
	        			if(y_storage != null)
	        			{
	        				out.print(y_storage.getString("title"));
	        			}
	        			
	        		 %>
	        	</td>
	        	<td align="center" valign="middle">
	        		<%
	        			DBRow catalog = catalogMgr.getDetailProductCatalogById(rows[i].get("catalog_id",0l));
	        			if(catalog != null)
	        			{
	        				out.print(catalog.getString("title"));
	        			}
	        			
	        		 %>
	        	</td>
	        	<td align="center" valign="middle">
	        		<% 
	        			DBRow d_storage = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("d_sid",0l));
	        			if(d_storage != null)
	        			{
	        				out.print(d_storage.getString("title"));
	        			}
	        			
	        	    %>
	        	</td>
	        	<td align="center" valign="middle">
	        		<%
	        			if(rows[i].get("modality_id",0l) == 1)
	        			{
	        				out.print("原状");
	        			}
	        			else
	        			{
	        				out.print("拆散");
	        			}
	        		 %>
	        	</td>
	        	<td align="center" valign="middle">
	        		<input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="modStorage_transpond(<%=rows[i].getString("id") %>)">
			        &nbsp;&nbsp;&nbsp;&nbsp;
			        <input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="deleteStorage(<%=rows[i].getString("id") %>)">
			        <br>
	        	</td>
	        </tr>
	        <%} %>
	</table>
</form>
<br/>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="storage_transpond.html">
    <input type="hidden" name="p">
  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
</body>
</html>
