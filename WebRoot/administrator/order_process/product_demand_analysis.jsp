<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.Date"%>
<%@ include file="../../include.jsp"%> 
<%
	long pcid = StringUtil.getLong(request,"pcid");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
	String product_name = StringUtil.getString(request,"product_name");
	
	TDate tdate = new TDate();
	String input_en_date = StringUtil.getString(request,"input_en_date",tdate.formatDate("yyyy-MM-dd"));
	
	tdate.addDay(-30);
	
	String input_st_date = StringUtil.getString(request,"input_st_date",tdate.formatDate("yyyy-MM-dd"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">


<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">

@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>


<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
td.actural_search_product_categorymenu_td div{margin-top:-6px;}
</style>
<script type="text/javascript">
	function ajaxLoadCatalogMenuPage(id,divId)
	{
			var para = "id="+id+"&divId="+divId;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#"+divId+"categorymenu_td").html(html);
				}
			});
	}
	
	$().ready(function() 
	{
		addAutoComplete($("#actural_search_product_search_product_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",	  
			"p_name");

		$("#actural_search_product_search_product_name").keydown(function(event){
			if (event.keyCode==13)
			{
				search('actual_sales_analysi.html','actural_search_product_')
			}
		});
		
	});

function ajaxLoadDataPage(param,url)
{
	$.ajax({
				url: url,
				type: 'post',
				dataType: 'html',
				//timeout: 60000,
				cache:false,
				data:param,
				
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				
				error: function(){
					$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
				},
				
				success: function(html){
					$.unblockUI();
					$("#dataList").html(html);
					//alert(html);
					onLoadInitZebraTable();//表格斑马线
					//$.scrollTo(0,500);//页面向上滚动
				}
			});
}

</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br/>
<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#actural_search_product" onclick="ajaxLoadDataPage('','actual_sales_analysi.html')">实际货物需求量</a></li>
		</ul>
		<div id="actural_search_product">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td>
	              	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:1000px;">
						<table width="99%" border="0" align="center" cellpadding="3" cellspacing="0">
							  <tr>
							    <td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
							    	<ul id="actural_search_product_productLinemenu" class="mcdropdown_menu">
							             <li rel="0">所有产品线</li>
							             <%
											  DBRow p1[] = productLineMgrTJH.getAllProductLine();
											  for (int i=0; i<p1.length; i++)
											  {
													out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
													 
													out.println("</li>");
											  }
										  %>
							           </ul>
							           <input type="text" name="productLine" id="actural_search_product_productLine" value="" />
							           <input type="hidden" name="filter_productLine" id="actural_search_product_filter_productLine" value="0"/>
								
							</td>
						    <td width="434" height="29" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">
						    	<input name="groupWith" type="radio" id="groupWith" checked="checked" value="1"/>求和统计<input type="radio" id="groupWith" name="groupWith" value="2"/>分散统计&nbsp;&nbsp;&nbsp;<input type="button" onclick="exportProductDamandAnaly('actural_search_product_')" class="long-button-export" value="导出">
						    </td>
						  </tr>
						  <tr>
						  	<td id="actural_search_product_categorymenu_td" class="actural_search_product_categorymenu_td" width="423" align="left" valign="bottom" bgcolor="#eeeeee" style="padding-left:10px;"> 
						           <ul id="actural_search_product_categorymenu" class="mcdropdown_menu">
									  <li rel="0">所有分类</li>
									  <%
										  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
										  for (int i=0; i<c1.length; i++)
										  {
												out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
									
												  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
												  if (c2.length>0)
												  {
												  		out.println("<ul>");	
												  }
												  for (int ii=0; ii<c2.length; ii++)
												  {
														out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
														
															DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
															  if (c3.length>0)
															  {
																	out.println("<ul>");	
															  }
																for (int iii=0; iii<c3.length; iii++)
																{
																		out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																		out.println("</li>");
																}
															  if (c3.length>0)
															  {
																	out.println("</ul>");	
															  }
															  
														out.println("</li>");				
												  }
												  if (c2.length>0)
												  {
												  		out.println("</ul>");	
												  }
												  
												out.println("</li>");
										  }
										  %>
									</ul>
							  <input type="text" name="category" id="actural_search_product_category" value="" />
							  <input type="hidden" name="filter_pcid" id="actural_search_product_filter_pcid" value="0"/>
							</td>
						  	<td width="434" height="29" nowrap="nowrap" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">
						  	<select id="sale_area" onchange="getAreaCountryByCaId(this.value)">
						  		 <option value="0">全部区域</option>
						  		<%
						  			DBRow[] areas = productMgr.getAllCountryAreas(null);
						  			for(int i = 0;i<areas.length;i++)
						  			{
								%>
									<option value="<%=areas[i].get("ca_id",0l)%>"><%=areas[i].getString("name")%></option>
								<%
						  			}
						  		%>
						  	</select>
								&nbsp;&nbsp;&nbsp;
						      <select name="ccid_hidden" style="display: none" id="ccid_hidden" onChange="getStorageProvinceByCcid(this.value);">
							  <option value="0">全部国家</option>
						      </select>
						      &nbsp;&nbsp;&nbsp;
						      <select name="pro_id" id="pro_id" style="display: none">
						      	<option value="0">全部区域</option>
						      </select>
						  	</td>
						  </tr>
						  <tr>
						  	<td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
						  		 商品名称： <input type="text" name="search_product_name" id="actural_search_product_search_product_name" value="<%=product_name%>" style="width:350px;" onClick="cleanProductLine('actural_search_product_')"/>
						  	</td>
						  	
						    <td height="29" bgcolor="#eeeeee">
						    	发货开始时间：<input name="input_st_date" type="text" id="actural_search_product_input_st_date" size="10" value="<%=input_st_date%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
					    		结束时间：<input name="input_en_date" type="text" id="actural_search_product_input_en_date" size="10" value="<%=input_en_date%>" readonly="readonly" /> &nbsp;&nbsp;&nbsp;
					    		<input name="Submit" type="button" class="button_long_search" value="查询" onclick="search('actual_sales_analysi.html','actural_search_product_')"/>
							</td>
						  </tr>
						</table>
					</div>
				  </td>
	            </tr>
	          </table>
	          <script type="text/javascript">
		          	$("#actural_search_product_input_st_date").date_input();
					$("#actural_search_product_input_en_date").date_input();
						
					$("#actural_search_product_category").mcDropdown("#actural_search_product_categorymenu",{
							  allowParentSelect:true,
							  select: 
									function (id,name)
									{
										$("#actural_search_product_filter_pcid").val(id);
										
										if(id!=0)
										{
											cleanSearchKey("actural_search_product_");
										}
									}
					});
					
					$("#actural_search_product_productLine").mcDropdown("#actural_search_product_productLinemenu",{
							allowParentSelect:true,
							  select: 
									function (id,name)
									{
										$("#actural_search_product_filter_productLine").val(id);
										
										ajaxLoadCatalogMenuPage(id,"actural_search_product_");
										
										if(id!=0)
										{
											cleanSearchKey("actural_search_product_");
										}
									}
					});
					<%
					if (pcid>0)
					{
					%>
					$("#actural_search_product_category").mcDropdown("#actural_search_product_categorymenu").setValue(<%=pcid%>);
					<%
					}
					else
					{
					%>
					$("#actural_search_product_category").mcDropdown("#actural_search_product_actural_search_product_categorymenu").setValue(0);
					<%
					}
					%> 
					
					<%
					if (pro_line_id!=0)
					{
					%>
					$("#actural_search_product_productLine").mcDropdown("#actural_search_product_productLinemenu").setValue('<%=pro_line_id%>');
					<%
					}
					else
					{
					%>
					$("#actural_search_product_productLine").mcDropdown("#actural_search_product_productLinemenu").setValue(0);
					<%
					}
					%>
	          </script>
		</div>
	</div>
</div>

<br/>
<div id="dataList">
</div>
<form name="filter_search_form"></form>
<script>
$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
});

function search(url,cmd)
{
	var param = "product_name="+$("#"+cmd+"search_product_name").val()+"&catalog_id="+$("#"+cmd+"filter_pcid").val()+"&pro_line_id="+$("#"+cmd+"filter_productLine").val()+"&input_st_date="+$("#"+cmd+"input_st_date").val()+"&input_en_date="+$("#"+cmd+"input_en_date").val()+"&ca_id="+$("#sale_area").val()+"&ccid="+$("#ccid_hidden").val()+"&pro_id="+$("#pro_id").val()+"&type="+$('input[name="groupWith"]:checked').val();
	ajaxLoadDataPage(param,url);
}

function goPage(p,url,cmd)
{
	var param = p+"&product_name="+$("#"+cmd+"search_product_name").val()+"&catalog_id="+$("#"+cmd+"filter_pcid").val()+"&pro_line_id="+$("#"+cmd+"filter_productLine").val()+"&input_st_date="+$("#"+cmd+"input_st_date").val()+"&input_en_date="+$("#"+cmd+"input_en_date").val()+"&ca_id="+$("#sale_area").val()+"&ccid="+$("#ccid_hidden").val()+"&pro_id="+$("#pro_id").val()+"&type="+$('input[name="groupWith"]:checked').val();
	ajaxLoadDataPage(param,url);
}

function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"search_product_name").val("");
}
//销售区域切换国家
function getAreaCountryByCaId(ca_id)
{

		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getAreaCountrysJSON.action",
				{ca_id:ca_id},
				function callback(data)
				{ 
					$("#ccid_hidden").clearAll();
					$("#ccid_hidden").addOption("全部国家","0");
					
					if (data!="")
					{
						$("#ccid_hidden").css("display","");
						$("#pro_id").css("display","none");
						$("#pro_id").clearAll();
						
						$.each(data,function(i){
							$("#ccid_hidden").addOption(data[i].c_country,data[i].ccid);
						});
					}
					else
					{
						$("#ccid_hidden").css("display","none");
						$("#pro_id").css("display","none");
					}
				}
		);
}

//国家地区切换
function getStorageProvinceByCcid(ccid)
{

		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("全部区域","0");
					
					if (data!="")
					{
						$("#pro_id").css("display","");
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					else
					{
						$("#pro_id").css("display","none");
					}
					
					if (ccid>0)
					{
						$("#pro_id").addOption("Others","-1");
					}
				}
		);
}

function exportProductDamandAnaly(cmd)
{
		var para = "product_name="+$("#"+cmd+"search_product_name").val()+"&catalog_id="+$("#"+cmd+"filter_pcid").val()+"&pro_line_id="+$("#"+cmd+"filter_productLine").val()+"&input_st_date="+$("#"+cmd+"input_st_date").val()+"&input_en_date="+$("#"+cmd+"input_en_date").val()+"&ca_id="+$("#sale_area").val()+"&ccid="+$("#ccid_hidden").val()+"&pro_id="+$("#pro_id").val()+"&type="+$('input[name="groupWith"]:checked').val();
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order_process/exportProductDamandAnaly.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					beforeSend:function(request){
					},
					
					error: function(){
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.filter_search_form.action=date["fileurl"];
							document.filter_search_form.submit();
						}
						else
						{
							alert("该统计无数据，无法导出");
						}
					}
				});
}
</script>
</body>
</html>
