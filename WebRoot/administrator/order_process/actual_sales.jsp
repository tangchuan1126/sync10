<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%

	long catalog_id = StringUtil.getLong(request,"catalog_id");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
	String product_name = StringUtil.getString(request,"product_name");
	String input_st_date = StringUtil.getString(request,"input_st_date");
	String input_en_date = StringUtil.getString(request,"input_en_date");
	String cmd = StringUtil.getString(request,"cmd");
	String variable = StringUtil.getString(request,"variable","actual");
	long ccid = StringUtil.getLong(request,"ccid");
	long pro_id = StringUtil.getLong(request,"pro_id");
	
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	
	TDate tDate = new TDate();

	if (input_st_date.equals("") && input_en_date.equals(""))
	{
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		
	}
	
	DBRow[] rows = null;
	
	rows = orderProcessMgr.getActualStorageProductQuantity(input_st_date,input_en_date,catalog_id,pro_line_id,ccid,pro_id,product_name,pc,variable);
	
	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/select.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>

</head>
<script language="javascript">

</script>
<body>


<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	    <tr> 
	        <th width="9%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">商品名</th>
	        <th width="8%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">需求数</th>
	        <th width="5%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">发货库</th>
	        <th width="5%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">优先库</th>
	        <th width="7%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">国家</th>
	        <th width="7%"  class="right-title" style="vertical-align: center;text-align: center;">区域</th>
	        <th width="5%"  class="right-title" style="vertical-align: center;text-align: center;">商品单价</th>
	        <th width="10%"  class="right-title" style="vertical-align: center;text-align: center;">发货时间</th>
	        <th width="10%"  class="right-title" style="vertical-align: center;text-align: center;">统计时间</th>
	    </tr>
	
	    <%
	    	if(rows !=null)
	    	{
				for ( int i=0; i<rows.length; i++ )
				{
		
		%>
	    <tr> 
	      <td height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
				
				<%
					DBRow product = orderProcessMgr.getProductById(rows[i].get("product_id",0l));
					if(product != null)
					{
						StringBuffer title = new StringBuffer("");
						DBRow[] treeParentCatalog = productCatalogMgrZJ.getFatherTree(product.get("catalog_id",0l));
						for(int j=0;j<treeParentCatalog.length;j++)
						{
							if(j!=0)
							{
								title.append("->");
							}
							title.append(treeParentCatalog[j].getString("title"));
						}
						 
						out.print("<a href=\"#\" title=\""+title+"\" style=\"text-decoration:none;\">"+product.getString("p_name")+"</a>");
					}
					else
					{
						out.print("&nbsp;");		
					}
				 %>
	      
		 </td>
	      <td align="center" valign="middle">
		  		<%=rows[i].getString("total_quantity") %>
		  </td>
		  
		  <td align="center" valign="middle">
	  		<%
	  			DBRow storage = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("storage_id",0l));
	  			if(storage != null)
	  			{
	  				out.print(storage.getString("title"));
	  			}
	  		 %>
	  		 &nbsp;
		  </td>
		  <td align="center" valign="middle">
	  		<%
	  			DBRow prior_storage = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("prior_storage_id",0l));
	  			if(storage != null)
	  			{
	  				out.print(prior_storage.getString("title"));
	  			}
	  		 %>
	  		 &nbsp;
		  </td>
	      <td align="center" valign="middle">
	      	<%
	      		DBRow nation = supplierMgrTJH.getDetailCountry(rows[i].get("nation_id",0l));
	      		if(nation != null)
	      		{
	      			out.print(nation.getString("c_country"));
	      		}
	      	 %>
	      &nbsp;
	     </td>
	      <td align="center" valign="middle">
	      	<%
	      		DBRow province = supplierMgrTJH.getDetailProvinceById(rows[i].get("province_id",0l));
	      		if(province != null)
	      		{
	      			out.print(province.getString("pro_name"));
	      		}
	      	 %>
	      &nbsp;</td>
	      <td align="center" valign="middle"><%=rows[i].get("unit_price",0d)%>&nbsp;</td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("delivery_date").substring(0,19)%>&nbsp;</td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("stats_date").substring(0,19)%>&nbsp;</td>
	    </tr>
	    <%
		}
	}
	%>
	  
	</table>
<br />

<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:goPage('p=1','actual_sales.html','actural_search_product_')",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:goPage('p="+pre+"','actual_sales.html','actural_search_product_')",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:goPage('p="+next+"','actual_sales.html','actural_search_product_')",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:goPage('p="+pc.getPageCount()+"','actual_sales.html','actural_search_product_')",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:goPage('p='document.getElementById('jump_p2').value+'','actual_sales.html')" value="GO"/>
    </td>
  </tr>
</table>
</body>
</html>



