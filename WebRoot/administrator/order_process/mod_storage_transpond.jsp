<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long sid = StringUtil.getLong(request,"transpond_id");
DBRow row = storageTranspondMgr.getDetailStorageTrandspond(sid);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加资产</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<script>
function modTranspondStorage()
{
	var f = document.mod_storage_form;
	if(f.y_sid.value == 0)
	{
		alert("请选择优先发货仓库");
		return false;
	}
	if(f.catalog_id.value == 0)
	{
		alert("请选择产品分类");
		return false;
	}
	if(f.d_sid.value == 0)
	{
		alert("请选择需要的代发仓库");
		return false;
	}
	if(f.type_id.value == 0)
	{
		alert("请选择商品的代发形式");
		return false;
	}
	 else
	 {
	 	
		parent.document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order_process/modStoragetranspond.action";
		parent.document.mod_form.transpondId.value = <%=sid%>;
		parent.document.mod_form.prior_sid.value = f.y_sid.value;
		parent.document.mod_form.catalog_id.value = f.catalog_id.value;
		parent.document.mod_form.d_sid.value = f.d_sid.value;
		parent.document.mod_form.type_id.value = f.type_id.value;	
		parent.document.mod_form.submit();		

	 }
}

</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}
.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="mod_storage_form" method="post" action="">
<table width="98%" border="0" cellspacing="5" cellpadding="2">
  <tr>
    <td colspan="2" align="left" valign="middle" class="text-line"><table width="100%" border="0" cellpadding="5" cellspacing="0" >
    </table></td>
  </tr>
  <tr height="35">
    <td align="left" valign="middle" class="STYLE1 STYLE2" >优先发货仓库</td>
    <td align="left" valign="middle" >
    	<select name="y_sid" id="y_sid">
			<option value="0">请选择优先发货仓库...</option>>
	          <%
				DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
				String qx;
				
				for ( int i=0; i<treeRows.length; i++ )
				{
					if ( treeRows[i].get("parentid",0) != 0 )
					 {
					 	qx = "├ ";
					 }
					 else
					 {
					 	qx = "";
					 }
					 
					 if (treeRows[i].get("level",0)>1)
					 {
					 	continue;
					 }
				%>
	          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l) == row.get("y_sid",0l)?"selected":"" %>> 
	          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
	          <%=qx%>
	          <%=treeRows[i].getString("title")%>          </option>
	          <%
	}
	%>
	    </select> 
	</td>
    
  </tr>
  <tr height="35">
    <td align="left" valign="middle" class="STYLE3" >商品分类</td>
    <td align="left" valign="middle" >
    	<select name="catalog_id" id="catalog_id">
			      <option value="0">请选择产品分类...</option>
			      <%
					String qx_catalog="";
					Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
					DBRow catalogTree[] = catalogMgr.getProductCatalogTree();
					
					for ( int i=0; i<catalogTree.length; i++ )
					{
						if ( catalogTree[i].get("parentid",0) != 0 )
						 {
						 	qx_catalog = "├ ";
						 }
						 else
						 {
						 	qx_catalog = "";
						 }
					%>
			      <option value="<%=catalogTree[i].getString("id")%>" <%=catalogTree[i].get("id",0l) == row.get("catalog_id",0l)?"selected":"" %>> <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",catalogTree[i].get("level",0))%> <%=qx_catalog%> <%=catalogTree[i].getString("title")%> </option>
			      <%
					}
				  %>
    	</select>
	</td>
  
  </tr>
  <tr height="35">
    <td align="left" valign="middle" class="STYLE3">代发仓库</td>
    <td align="left" valign="middle">
    	<select name="d_sid" id="d_sid">
					<option value="0">请选择代发仓库...</option>
			          <%
						DBRow storageTree[] = catalogMgr.getProductStorageCatalogTree();
						String qx_s;
						
						for ( int i=0; i<storageTree.length; i++ )
						{
							if ( storageTree[i].get("parentid",0) != 0 )
							 {
							 	qx_s = "├ ";
							 }
							 else
							 {
							 	qx_s = "";
							 }
							 
							 if (storageTree[i].get("level",0)>1)
							 {
							 	continue;
							 }
						%>
						          <option value="<%=storageTree[i].getString("id")%>" <%=storageTree[i].get("id",0l) == row.get("d_sid",0l)?"selected":"" %> > 
						          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageTree[i].get("level",0))%>
						          <%=qx_s%>
						          <%=storageTree[i].getString("title")%>          </option>
						          <%
						}
						%>
		</select> 
    </td>
  </tr>
  <tr height="35">
    <td align="left" valign="middle" class="STYLE3" >代发形式</td>
    <td align="left" valign="middle" >
    	<select id="type_id" name="type_id">
	  			<option value="0">请选择商品代发形式...</option>
	  			<%
	  				if(row.get("modality_id",0l) == 1)
	  				{
	  			 %>
	  			<option value="1" selected="selected">套装</option>
	  			<option value="2">散件</option>
	  			<%
	  				}
	  				else if(row.get("modality_id",0l) == 2)
	  				{
	  			 %>
	  			 <option value="1">套装</option>
	  			<option value="2" selected="selected">散件</option>
	  			<%
	  				}
	  			 %>
	  		</select>
    </td>
   
  </tr>
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存修改" class="normal-green" onClick="modTranspondStorage();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
	</td>
  </tr>
</table>
</body>
</html>
