<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%

	long catalog_id = StringUtil.getLong(request,"catalog_id");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
	String product_name = StringUtil.getString(request,"product_name");
	String input_st_date = StringUtil.getString(request,"input_st_date");
	String input_en_date = StringUtil.getString(request,"input_en_date");
	String cmd = StringUtil.getString(request,"cmd");
	String variable = StringUtil.getString(request,"variable","plan");
	
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	DBRow[] rows;

	rows = orderProcessMgr.getActualStorageProductQuantity(input_st_date,input_en_date,catalog_id,pro_line_id,0,0,product_name,pc,variable);//0接收国家与区域ID，因写成了一个方法，这个不用次条件搜索
	
	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>计划仓库的货物需求</title>
<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/select.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
</head>

<body >


<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	    <tr> 
	        <th width="9%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">商品名称</th>
	        <th width="11%"  class="right-title" style="vertical-align: center;text-align: center;">商品类别</th>
	        <th width="8%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">所需商品总数</th>
	        <th width="5%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">计划发货仓库</th>
	        <th width="5%"  class="right-title" style="vertical-align: center;text-align: center;">商品单价</th>
	        <th width="10%"  class="right-title" style="vertical-align: center;text-align: center;">发货时间</th>
	        <th width="10%"  class="right-title" style="vertical-align: center;text-align: center;">统计时间</th>
	    </tr>
	
	    <%
	    	if(rows !=null)
	    	{
	  			for(int i=0;i<rows.length;i++)
	  			{
 		%>
	    <tr> 
	      <td height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
				<%
					DBRow product = orderProcessMgr.getProductById(rows[i].get("product_id",0l));
					if(product != null)
					{
						out.print(product.getString("p_name"));
					}
				 %>
	      &nbsp;
		 </td>
	      <td align="center" valign="middle" style='word-break:break-all;' >      	
		  	 <%
			  DBRow catalog = catalogMgr.getDetailProductCatalogById(rows[i].get("catalog_id",0l));
			  if(catalog != null)
			  {
				   if(catalog.get("parentid",0l) != 0)
				   {
				   		DBRow c1 = catalogMgr.getDetailProductCatalogById(catalog.get("parentid",0l));
				   		if(c1.get("parentid",0l) != 0)
				   		{
				   			DBRow c2 = catalogMgr.getDetailProductCatalogById(c1.get("parentid",0l));
				   			if(c2.get("parentid",0l) != 0)
				   			{
				   				DBRow c3 = catalogMgr.getDetailProductCatalogById(c2.get("parentid",0l));
				   				if(c3.get("parentid",0l) == 0)
				   				{
				   					out.println(c3.getString("title"));
				   				}
				   			}
				   			else
				   			{
				   				out.println(c2.getString("title"));
				   			}
				   		}
				   		else
				   		{
				   			out.println(c1.getString("title"));
				   		}
				   }
				    out.println("<br>("+catalog.getString("title")+")");
			   }
			  
			  %>
			  
			  &nbsp;
	      	</td>
	      <td align="center" valign="middle">
		  		<%=rows[i].getString("quantity") %>
		  </td>
		  
		  <td align="center" valign="middle">
	  		<%
	  			DBRow storage = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("plan_storage_id",0l));
	  			if(storage != null)
	  			{
	  				out.print(storage.getString("title"));
	  			}
	  		 %>
	  		 &nbsp;
		  </td>
	      <td align="center" valign="middle"><%=rows[i].get("unit_price",0d)%>&nbsp;</td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("delivery_date").substring(0,10)%>&nbsp;</td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("deal_date")%>&nbsp;</td>
	    </tr>
	    <%
		}
	}
	%>
	  
	</table>
<br />
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:goPage('p=1','plan_sales.html','plan_search_product_')",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:goPage('p="+pre+"','plan_sales.html','plan_search_product_')",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:goPage('p="+next+"','plan_sales.html','plan_search_product_')",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:goPage('p="+pc.getPageCount()+"','plan_sales.html','plan_search_product_')",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go2page(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>
</body>
</html>



