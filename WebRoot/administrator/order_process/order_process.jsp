<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.Date"%>
<%@ include file="../../include.jsp"%> 
<%
	long pcid = StringUtil.getLong(request,"pcid");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
	String product_name = StringUtil.getString(request,"product_name");
	
	TDate tdate = new TDate();
	String input_en_date = StringUtil.getString(request,"input_en_date",tdate.formatDate("yyyy-MM-dd"));
	
	tdate.addDay(-30);
	
	String input_st_date = StringUtil.getString(request,"input_st_date",tdate.formatDate("yyyy-MM-dd"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />


<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
td.actural_search_product_categorymenu_td div{margin-top:-6px;}
</style>
<script type="text/javascript">
	function ajaxLoadCatalogMenuPage(id,divId)
	{
			var para = "id="+id+"&divId="+divId;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#"+divId+"categorymenu_td").html(html);
				}
			});
	}
	
	$().ready(function() 
	{

	$("#search_product_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		updownSelect:true,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
});

function ajaxLoadDataPage(param,url)
{
	$.ajax({
				url: url,
				type: 'post',
				dataType: 'html',
				//timeout: 60000,
				cache:false,
				data:param,
				
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				
				error: function(){
					$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
				},
				
				success: function(html){
					$.unblockUI();
					$("#dataList").html(html);
					//alert(html);
					onLoadInitZebraTable();//表格斑马线
					//$.scrollTo(0,500);//页面向上滚动
				}
			});
}

</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购应用  »    产品需求分布</td>
  </tr>
</table>
<br/>
<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#actural_search_product" onclick="ajaxLoadDataPage('','actual_sales.html')">实际货物需求量</a></li>
			<li><a href="#theory_sales" onclick="ajaxLoadDataPage('','theory_sales.html')">理论仓库货物需求</a></li>
			<li><a href="#original_sales" onclick="ajaxLoadDataPage('','original_sales.html')">代发后本来仓库货物需求</a></li>
			<li><a href="#plan_search_product" onclick="ajaxLoadDataPage('','plan_sales.html')">计划仓库货物需求</a></li>
		</ul>
		<div id="actural_search_product">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td>
	              	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
						<table width="99%" border="0" align="center" cellpadding="3" cellspacing="0">
							  <tr>
							    <td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
							    	<ul id="actural_search_product_productLinemenu" class="mcdropdown_menu">
							             <li rel="0">所有产品线</li>
							             <%
											  DBRow p1[] = productLineMgrTJH.getAllProductLine();
											  for (int i=0; i<p1.length; i++)
											  {
													out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
													 
													out.println("</li>");
											  }
										  %>
							           </ul>
							           <input type="text" name="productLine" id="actural_search_product_productLine" value="" />
							           <input type="hidden" name="filter_productLine" id="actural_search_product_filter_productLine" value="0"/>
								
							</td>
						    <td width="434" height="29" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">
						    	 商品名称： <input type="text" name="search_product_name" id="actural_search_product_search_product_name" value="<%=product_name%>" style="width:200px;" onClick="cleanProductLine('actural_search_product_')"/>
						    </td>
						  </tr>
						  <tr>
						  	<td id="actural_search_product_categorymenu_td" class="actural_search_product_categorymenu_td" width="423" align="left" valign="bottom" bgcolor="#eeeeee" style="padding-left:10px;padding-top:10px;"> 
						           <ul id="actural_search_product_categorymenu" class="mcdropdown_menu">
									  <li rel="0">所有分类</li>
									  <%
										  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
										  for (int i=0; i<c1.length; i++)
										  {
												out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
									
												  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
												  if (c2.length>0)
												  {
												  		out.println("<ul>");	
												  }
												  for (int ii=0; ii<c2.length; ii++)
												  {
														out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
														
															DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
															  if (c3.length>0)
															  {
																	out.println("<ul>");	
															  }
																for (int iii=0; iii<c3.length; iii++)
																{
																		out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																		out.println("</li>");
																}
															  if (c3.length>0)
															  {
																	out.println("</ul>");	
															  }
															  
														out.println("</li>");				
												  }
												  if (c2.length>0)
												  {
												  		out.println("</ul>");	
												  }
												  
												out.println("</li>");
										  }
										  %>
									</ul>
							  <input type="text" name="category" id="actural_search_product_category" value="" />
							  <input type="hidden" name="filter_pcid" id="actural_search_product_filter_pcid" value="0"/>
							</td>
						    <td height="29" bgcolor="#eeeeee">
						    	发货开始时间：<input name="input_st_date" type="text" id="actural_search_product_input_st_date" size="10" value="<%=input_st_date%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
					    		结束时间：<input name="input_en_date" type="text" id="actural_search_product_input_en_date" size="10" value="<%=input_en_date%>" readonly="readonly" /> &nbsp;&nbsp;&nbsp;
					    		<input name="Submit" type="button" class="button_long_search" value="查询" onclick="search('actual_sales.html','actural_search_product_')"/>
							</td>
						  </tr>
						</table>
					</div>
				  </td>
	            </tr>
	          </table>
	          <script type="text/javascript">
		          	$("#actural_search_product_input_st_date").date_input();
					$("#actural_search_product_input_en_date").date_input();
						
					$("#actural_search_product_category").mcDropdown("#actural_search_product_categorymenu",{
							  allowParentSelect:true,
							  select: 
									function (id,name)
									{
										$("#actural_search_product_filter_pcid").val(id);
										
										if(id!=0)
										{
											cleanSearchKey("actural_search_product_");
										}
									}
					});
					
					$("#actural_search_product_productLine").mcDropdown("#actural_search_product_productLinemenu",{
							allowParentSelect:true,
							  select: 
									function (id,name)
									{
										$("#actural_search_product_filter_productLine").val(id);
										
										ajaxLoadCatalogMenuPage(id,"actural_search_product_");
										
										if(id!=0)
										{
											cleanSearchKey("actural_search_product_");
										}
									}
					});
					<%
					if (pcid>0)
					{
					%>
					$("#actural_search_product_category").mcDropdown("#actural_search_product_categorymenu").setValue(<%=pcid%>);
					<%
					}
					else
					{
					%>
					$("#actural_search_product_category").mcDropdown("#actural_search_product_actural_search_product_categorymenu").setValue(0);
					<%
					}
					%> 
					
					<%
					if (pro_line_id!=0)
					{
					%>
					$("#actural_search_product_productLine").mcDropdown("#actural_search_product_productLinemenu").setValue('<%=pro_line_id%>');
					<%
					}
					else
					{
					%>
					$("#actural_search_product_productLine").mcDropdown("#actural_search_product_productLinemenu").setValue(0);
					<%
					}
					%>
	          </script>
		</div>
		<div id="theory_sales">
		</div>
		<div id="original_sales">
		</div>
		<div id="plan_search_product">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td>
	              	<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
			            <tr>
			              <td>
			              	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
								<table width="99%" border="0" align="center" cellpadding="3" cellspacing="0">
									  <tr>
									    <td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
									    	<ul id="plan_search_product_productLinemenu" class="mcdropdown_menu">
									             <li rel="0">所有产品线</li>
									             <%
													  for (int i=0; i<p1.length; i++)
													  {
															out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
															 
															out.println("</li>");
													  }
												  %>
									           </ul>
									           <input type="text" name="productLine" id="plan_search_product_productLine" value="" />
									           <input type="hidden" name="filter_productLine" id="plan_search_product_filter_productLine" value="0"/>
										
									</td>
								    <td width="434" height="29" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">
								    	 商品名称： <input type="text" name="search_product_name" id="plan_search_product_search_product_name" value="<%=product_name%>" style="width:200px;" onClick="cleanProductLine('plan_search_product_')"/>
								    </td>
								  </tr>
								  <tr>
								  	<td id="plan_search_product_categorymenu_td" width="423" align="left" valign="bottom" bgcolor="#eeeeee" style="padding-left:10px;padding-top:10px;"> 
								           <ul id="plan_search_product_categorymenu" class="mcdropdown_menu">
											  <li rel="0">所有分类</li>
											  <%
												  for (int i=0; i<c1.length; i++)
												  {
														out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
											
														  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
														  if (c2.length>0)
														  {
														  		out.println("<ul>");	
														  }
														  for (int ii=0; ii<c2.length; ii++)
														  {
																out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
																
																	DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
																	  if (c3.length>0)
																	  {
																			out.println("<ul>");	
																	  }
																		for (int iii=0; iii<c3.length; iii++)
																		{
																				out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																				out.println("</li>");
																		}
																	  if (c3.length>0)
																	  {
																			out.println("</ul>");	
																	  }
																	  
																out.println("</li>");				
														  }
														  if (c2.length>0)
														  {
														  		out.println("</ul>");	
														  }
														  
														out.println("</li>");
												  }
												  %>
											</ul>
									  <input type="text" name="category" id="plan_search_product_category" value="" />
									  <input type="hidden" name="filter_pcid" id="plan_search_product_filter_pcid" value="0"/>
									</td>
								    <td height="29" bgcolor="#eeeeee">
								    	发货开始时间：<input name="input_st_date" type="text" id="plan_search_product_input_st_date" size="10" value="<%=input_st_date%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
							    		结束时间：<input name="input_en_date" type="text" id="plan_search_product_input_en_date" size="10" value="<%=input_en_date%>" readonly="readonly" /> &nbsp;&nbsp;&nbsp;
							    				<input name="Submit" type="button" class="button_long_search" value="查询" onclick="search('plan_sales.html','plan_search_product_')"/>
									</td>
								  </tr>
								</table>
							</div>
						  </td>
			            </tr>
			          </table>
				  </td>
	            </tr>
	          </table>
	           <script type="text/javascript">
		          	$("#plan_search_product_input_st_date").date_input();
					$("#plan_search_product_input_en_date").date_input();
						
					$("#plan_search_product_category").mcDropdown("#plan_search_product_categorymenu",{
							  allowParentSelect:true,
							  select: 
									function (id,name)
									{
										$("#plan_search_product_filter_pcid").val(id);
										if(id!=0)
										{
											cleanSearchKey("plan_search_product_");
										}
									}
					});
					
					$("#plan_search_product_productLine").mcDropdown("#plan_search_product_productLinemenu",{
							allowParentSelect:true,
							  select: 
									function (id,name)
									{
										$("#plan_search_product_filter_productLine").val(id);
										ajaxLoadCatalogMenuPage(id,"plan_search_product_");
										if(id!=0)
										{
											cleanSearchKey("plan_search_product_");
										}
									}
					});
					<%
					if (pcid>0)
					{
					%>
					$("#plan_search_product_category").mcDropdown("#plan_search_product_categorymenu").setValue(<%=pcid%>);
					<%
					}
					else
					{
					%>
					$("#plan_search_product_category").mcDropdown("#plan_search_product_categorymenu").setValue(0);
					<%
					}
					%> 
					
					<%
					if (pro_line_id!=0)
					{
					%>
					$("#plan_search_product_productLine").mcDropdown("#plan_search_product_productLinemenu").setValue('<%=pro_line_id%>');
					<%
					}
					else
					{
					%>
					$("#plan_search_product_productLine").mcDropdown("#plan_search_product_productLinemenu").setValue(0);
					<%
					}
					%>
	          </script>
		</div>
	</div>
</div>
<script>
$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
});
$("#actural_search_product_search_product_name").keydown(function(event){
	if (event.keyCode==13)
	{
		search('actual_sales.html','actural_search_product_');
	}
});

$("#plan_search_product_search_product_name").keydown(function(event){
	if (event.keyCode==13)
	{
		search('plan_sales.html','plan_search_product_');
	}
});


function search(url,cmd)
{
	var param = "product_name="+$("#"+cmd+"search_product_name").val()+"&catalog_id="+$("#"+cmd+"filter_pcid").val()+"&pro_line_id="+$("#"+cmd+"filter_productLine").val()+"&input_st_date="+$("#"+cmd+"input_st_date").val()+"&input_en_date="+$("#"+cmd+"input_en_date").val();
	ajaxLoadDataPage(param,url);
}

function goPage(p,url,cmd)
{
	var param = p+"&product_name="+$("#"+cmd+"search_product_name").val()+"&catalog_id="+$("#"+cmd+"filter_pcid").val()+"&pro_line_id="+$("#"+cmd+"filter_productLine").val()+"&input_st_date="+$("#"+cmd+"input_st_date").val()+"&input_en_date="+$("#"+cmd+"input_en_date").val();
	ajaxLoadDataPage(param,url);
}

function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"search_product_name").val("");
}
</script>
<br/>
<div id="dataList">
</div>
</body>
</html>
