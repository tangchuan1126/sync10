<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long sw_id = StringUtil.getLong(request,"sw_id");
long sc_id = StringUtil.getLong(request,"sc_id");

DBRow detailCompany = expressMgr.getDetailCompany(sc_id);
DBRow detailWeight = expressMgr.getDetailWeight(sw_id);
DBRow zones[] = expressMgr.getZoneByScId(sc_id);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加重量</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />


<script language="javascript">
<!--

//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="form1" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/saveFee.action">
<input type="hidden" name="sw_id" value="<%=sw_id%>">
<input type="hidden" name="sc_id" value="<%=sc_id%>">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" align="center" style="font-size:15px;font-weight:bold"><%=detailCompany.getString("name")%></td>
  </tr>
</table>

<table width="98%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td width="11%" height="30" align="center" valign="middle" bgcolor="#D3EB9A" style="font-weight:bold">重量(Kg)</td>
	<%
	for (int i=0; i<zones.length; i++)
	{
	%>
    <td align="center" valign="middle" bgcolor="#D3EB9A" style="font-weight:bold"><%=zones[i].getString("name")%></td>
	<%
	}
	%>
  </tr>
  <tr>
    <td height="25" align="center" valign="middle" bgcolor="#FFFFFF" style="padding-top:10px;padding-bottom:10px;"><%=detailWeight.get("st_weight",0f)%>~<%=detailWeight.get("en_weight",0f)%></td>
		<%
	for (int i=0; i<zones.length; i++)
	{
		DBRow detailFee = expressMgr.getDetailFeeByCompanyWeightZone(sc_id,sw_id,zones[i].get("sz_id",0l));
		if (detailFee==null)
		{
			detailFee = new DBRow();
			detailFee.add("f_weight_fee",0);
			detailFee.add("k_weight_fee",0);
			detailFee.add("sf_id",0);
		}
		
		
	%>
    <td align="center" valign="middle" bgcolor="#FFFFFF">
	

	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="25" align="center">
		  首重 
      <input type="text" name="f_weight_fee_<%=detailFee.get("sf_id",0l)%>_<%=zones[i].get("sz_id",0l)%>" style="width:40px;color:#FF0099" value="<%=MoneyUtil.round(detailFee.get("f_weight_fee",0d)/detailCompany.get("currency_rate",0f),2)%>" ></td>
        </tr>
		
        <tr>
          <td height="25" align="center">
		   续重 
	  <input type="text" name="k_weight_fee_<%=detailFee.get("sf_id",0l)%>_<%=zones[i].get("sz_id",0l)%>" style="width:40px;color:#FF0099" value="<%=MoneyUtil.round(detailFee.get("k_weight_fee",0d)/detailCompany.get("currency_rate",0f),2)%>"></td>
        </tr>
      </table>
	 
   </td>
		<%
	}
	%>
  </tr>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><label>
      <input name="Submit" type="submit" class="long-button-ok" value="保存运费">
    </label></td>
  </tr>
</table>
  </form>
</body>
</html>



