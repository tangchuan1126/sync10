<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String name = StringUtil.getString(request,"name");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<script language="javascript">
<!--
function uploadExcel()
{
	if ( document.upload_form.file.value=="" )
	{
		alert("请选择一个运费价格EXCEL文件");
	}
	else if ( document.upload_form.file.value.indexOf("xls")==-1 )
	{
		alert("只能上xls文件格式");
	}
	else
	{
		$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">正在处理，请稍后......</div>'});
		document.upload_form.submit();
	}
}
//-->
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  >

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/ImportExpressRate.action" name="upload_form" enctype="multipart/form-data" method="post">
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
      <tr>
    <td  align="center" valign="middle" style="color:#333333;font-size:20px;font-weight:bold">导入<%=name%>运费价格表</td>
  </tr>
      <tr>
        <td align="left" valign="top" style="padding-left:20px;">
		<br>
<br>
<br>

		上传运费价格表EXCEL <br>

		<input type="file" name="file" id="file"></td>
      </tr>
</table>
</form>
	
	
    </td></tr>
  <tr>
    <td align="right" class="win-bottom-line">
      <input type="button" name="Submit2" value="上传" class="normal-green-long" onClick="uploadExcel();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
    </td>
  </tr>
</table> 
</body>
<script>
//关闭
function closeWin(){
	$.artDialog.close();
}
</script>
</html>
