<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>快递管理</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
 
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<style type="text/css">
 .tabs_ ul li{font-size:12px;}	
 .tabs_{margin-top:5px;} 
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<% DBRow companys[] = null; %>
<script language="javascript">
<!--
 
function addCompany(psid,domestic)
{

<%
StringBuffer countryCodeSB = new StringBuffer("");
DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();

countryCodeSB.append("<select name='proPsId' id='proPsId' >");
countryCodeSB.append("<option  value='0'>请选择...</option>");
for (int i=0; i<treeRows.length; i++)
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	continue;
	 }
	 
	countryCodeSB.append("<option  value='"+treeRows[i].getString("id")+"'>"+treeRows[i].getString("title")+"</option>");
}
	countryCodeSB.append("</select>");
%>

	var preCName = "国际";
	if (domestic==1)
	{
		preCName = "国内";
	}

	$.prompt(
	
	"<div id='title'>增加 "+preCName+"快递公司</div><br>&nbsp;&nbsp;公司名称 &nbsp;&nbsp;&nbsp;<input name='proCompany' type='text' id='proCompany' style='width:200px;'> <br><br>&nbsp;&nbsp;优惠折扣 &nbsp;&nbsp;&nbsp;<input name='proDiscount' type='text' id='proDiscount' style='width:100px;'>&nbsp;格式：0.00<br><br>&nbsp;&nbsp;燃油附加费 &nbsp;&nbsp;<input name='proOilAdditionFee' type='text' id='proOilAdditionFee' style='width:100px;'>&nbsp;格式：0.00<br><br>&nbsp;&nbsp;适重 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name='st' type='text' id='st' style='width:50px;'> Kg - <input name='en' type='text' id='en' style='width:50px;'> Kg<br><br>&nbsp;&nbsp;处理费 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name='proHandleFee' type='text' id='proHandleFee' style='width:100px;'> RMB<br><br>&nbsp;&nbsp;打印重量折扣 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name='proPrintWeightDiscount' type='text' id='proPrintWeightDiscount' style='width:100px;'> 格式：0.00",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.proCompany=="")
						{
							alert("请填写公司名称");
							return(false);
						}
						else if (f.proDiscount=="")
						{
							alert("请填写优惠折扣");
							return(false);
						}
						else if (!isNum(f.proDiscount))
						{
							alert("请正确填写优惠折扣");
							return(false);
						}
						else if (f.proOilAdditionFee=="")
						{
							alert("请填写燃油附加费");
							return(false);
						}
						else if (!isNum(f.proOilAdditionFee))
						{
							alert("请正确填写燃油附加费");
							return(false);
						}
						else if (f.st==""||f.en=="")
						{
							alert("请填写适重");
							return(false);
						}
						else if (!isNum(f.st)||!isNum(f.en))
						{
							alert("请正确填写适重");
							return(false);
						}
						else if (f.proHandleFee=="")
						{
							alert("请填写处理费");
							return(false);
						}
						else if (!isNum(f.proHandleFee))
						{
							alert("请正确填写处理费");
							return(false);
						}
						else if (f.proPrintWeightDiscount=="")
						{
							alert("请填写打印重量折扣");
							return(false);
						}
						else if (!isNum(f.proPrintWeightDiscount))
						{
							alert("请正确填写打印重量折扣");
							return(false);
						}
						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
							document.add_company_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/addCompany.action";
							document.add_company_form.name.value = f.proCompany;
							document.add_company_form.shipping_fee_discount.value = f.proDiscount;
							document.add_company_form.oil_addition_fee.value = f.proOilAdditionFee;
							document.add_company_form.ps_id.value = psid;
							document.add_company_form.st_weight.value = f.st;
							document.add_company_form.en_weight.value = f.en;
							document.add_company_form.handle_fee.value = f.proHandleFee;
							document.add_company_form.domestic.value = domestic;
							document.add_company_form.print_weight_discount.value = f.proPrintWeightDiscount;
							document.add_company_form.submit();
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 增加: "y", 取消: "n" }
	});
}


function modCompany(sc_id)
{	
	$.prompt(
	
	"<div id='title'>修改快递公司</div><br>&nbsp;&nbsp;公司名称 &nbsp;&nbsp;&nbsp;<input name='proCompany' type='text' id='proCompany' style='width:200px;'> <br><br>&nbsp;&nbsp;优惠折扣 &nbsp;&nbsp;&nbsp;<input name='proDiscount' type='text' id='proDiscount' style='width:100px;'>&nbsp;格式：0.00<br><br>&nbsp;&nbsp;燃油附加费 &nbsp;&nbsp;&nbsp;<input name='proOilAdditionFee' type='text' id='proOilAdditionFee' style='width:100px;'>&nbsp;格式：0.00<br><br>&nbsp;&nbsp;发货仓库 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=countryCodeSB.toString()%><br><br>&nbsp;&nbsp;适重 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name='st' type='text' id='st' style='width:50px;'> Kg - <input name='en' type='text' id='en' style='width:50px;'> Kg<br><br>&nbsp;&nbsp;处理费 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name='proHandleFee' type='text' id='proHandleFee' style='width:100px;'> RMB<br><br>&nbsp;&nbsp;打印重量折扣 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name='proPrintWeightDiscount' type='text' id='proPrintWeightDiscount' style='width:100px;'> 格式：0.00",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.proCompany=="")
						{
							alert("请填写公司名称");
							return(false);
						}
						else if (f.proDiscount=="")
						{
							alert("请填写优惠折扣");
							return(false);
						}
						else if (!isNum(f.proDiscount))
						{
							alert("请正确填写优惠折扣");
							return(false);
						}
						else if (f.proOilAdditionFee=="")
						{
							alert("请填写燃油附加费");
							return(false);
						}
						else if (!isNum(f.proOilAdditionFee))
						{
							alert("请正确填写燃油附加费");
							return(false);
						}
						else if (f.proPsId==0)
						{
							alert("请选择发货仓库");
							return(false);
						}
						else if (f.st==""||f.en=="")
						{
							alert("请填写适重");
							return(false);
						}
						else if (!isNum(f.st)||!isNum(f.en))
						{
							alert("请正确填写适重");
							return(false);
						}
						else if (f.proHandleFee=="")
						{
							alert("请填写处理费");
							return(false);
						}
						else if (!isNum(f.proHandleFee))
						{
							alert("请正确填写处理费");
							return(false);
						}
						else if (f.proPrintWeightDiscount=="")
						{
							alert("请填写打印重量折扣");
							return(false);
						}
						else if (!isNum(f.proPrintWeightDiscount))
						{
							alert("请正确填写打印重量折扣");
							return(false);
						}
						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/getDetailCompanyJSON.action",
							{sc_id:sc_id},
							function callback(data)
							{   
								$("#proCompany").val(data.name);
								$("#proDiscount").val(data.shipping_fee_discount);
								$("#proOilAdditionFee").val(data.oil_addition_fee);
								$("#proPsId").setSelectedValue(data.ps_id);
								$("#st").setSelectedValue(data.st_weight);
								$("#en").setSelectedValue(data.en_weight);
								$("#proHandleFee").setSelectedValue(data.handle_fee);
								$("#proPrintWeightDiscount").setSelectedValue(data.print_weight_discount);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
							document.mod_company_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/modCompany.action";
							document.mod_company_form.sc_id.value = sc_id;
							document.mod_company_form.name.value = f.proCompany;
							document.mod_company_form.shipping_fee_discount.value = f.proDiscount;
							document.mod_company_form.oil_addition_fee.value = f.proOilAdditionFee;
							document.mod_company_form.ps_id.value = f.proPsId;
							document.mod_company_form.st_weight.value = f.st;
							document.mod_company_form.en_weight.value = f.en;
							document.mod_company_form.handle_fee.value = f.proHandleFee;
							document.mod_company_form.print_weight_discount.value = f.proPrintWeightDiscount;
							document.mod_company_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 修改: "y", 取消: "n" }
	});
}


function addWeight(name,sc_id)
{	
	$.prompt(
	
	"<div id='title'>添加重量段["+name+"]</div><br>&nbsp;&nbsp;&nbsp;重量段 &nbsp;&nbsp;&nbsp;<input name='proStWeight' type='text' id='proStWeight' style='width:50px;' > Kg - <input name='proEnWeight' type='text' id='proEnWeight' style='width:50px;'> Kg <br><br>&nbsp;&nbsp;重量步长 <input name='proWeightStep' type='text' id='proWeightStep' style='width:50px;' > Kg",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.proStWeight=="")
						{
							alert("请填写起始重量");
							return(false);
						}

						if (f.proEnWeight=="")
						{
							alert("请填写结束重量");
							return(false);
						}


						else if (f.proWeightStep=="")
						{
							alert("请填写重量步长");
							return(false);
						}

						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{

				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
							document.add_weight_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/addWeight.action";
							document.add_weight_form.st_weight.value = f.proStWeight;
							document.add_weight_form.en_weight.value = f.proEnWeight;
							document.add_weight_form.weight_step.value = f.proWeightStep;
							document.add_weight_form.sc_id.value = sc_id;
							document.add_weight_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function modWeight(name,sw_id)
{	
	$.prompt(
	
	"<div id='title'>修改重量段["+name+"]</div><br>&nbsp;&nbsp;&nbsp;重量段 &nbsp;&nbsp;&nbsp;<input name='proStWeight' type='text' id='proStWeight' style='width:50px;' > - <input name='proEnWeight' type='text' id='proEnWeight' style='width:50px;'>  <br><br>&nbsp;&nbsp;重量步长 <input name='proWeightStep' type='text' id='proWeightStep' style='width:50px;' >",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.proStWeight=="")
						{
							alert("请填写起始重量");
							return(false);
						}

						if (f.proEnWeight=="")
						{
							alert("请填写结束重量");
							return(false);
						}

						if (f.proEnWeight*1<=f.proStWeight*1)
						{
							alert("重量段范围不正确");
							return(false);
						}
						else if (f.proWeightStep=="")
						{
							alert("请填写重量步长");
							return(false);
						}

						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/GetDetailWeight4PageJSON.action",
							{sw_id:sw_id},
							function callback(data)
							{   
								$("#proStWeight").val(data.st_weight);
								$("#proEnWeight").val(data.en_weight);
								$("#proWeightStep").val(data.weight_step);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
							document.mod_weight_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/modWeight.action";
							document.mod_weight_form.st_weight.value = f.proStWeight;
							document.mod_weight_form.en_weight.value = f.proEnWeight;
							document.mod_weight_form.weight_step.value = f.proWeightStep;
							document.mod_weight_form.sw_id.value = sw_id;
							document.mod_weight_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}



function isNum(keyW)
 {
	var reg=  /^(-[0-9]|[0-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
 
 function isRoundNum(keyW)
 {
	var reg=  /^-?\d+$/;

	return( reg.test(keyW) );
 } 
 
 function delWeight(sw_id,st,en)
 {
 	if ( confirm("确定删除重量段："+st+"～"+en) )
	{
		document.del_weight_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/delWeight.action";
		document.del_weight_form.sw_id.value = sw_id;
		document.del_weight_form.submit();
	}
 }
 
function addWeightZoneFee(sc_id,sw_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/shipping/add_weight_zone_fee.html?sc_id="+sc_id+"&sw_id="+sw_id;
	$.artDialog.open(uri,{title:"添加/修改运费",width:'800px',height:'200px', lock: true,opacity: 0.3,fixed: true});
 	//tb_show('添加/修改运费','add_weight_zone_fee.html?sc_id='+sc_id+'&sw_id='+sw_id+'&height=200&width=800',false);
}

function delCompany(name,sc_id)
{
 	if ( confirm("确定删除 "+name+" ？") )
	{
		document.del_company_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/delCompany.action";
		document.del_company_form.sc_id.value = sc_id;
		document.del_company_form.submit();
	}
}
//设置城市
function SetCountry(CompanysName,ZonesName,SzId,ScId,domestic){
	
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/shipping/set_country.html?company_name="+CompanysName+"&zone_name="+ZonesName+"&sz_id="+SzId+"&sc_id="+ScId+"&domestic="+domestic;
	$.artDialog.open(uri,{title:"设置["+CompanysName+"-"+ZonesName+"]",width:'500px',height:'400px', lock: true,opacity: 0.3,fixed: true});
	
}

function addZone(company,sc_id)
{	
	$.prompt(
	
	"<div id='title'>添加地区["+company+"]</div><br>&nbsp;&nbsp;地区名称 &nbsp;&nbsp;&nbsp;<input name='proZone' type='text' id='proZone' style='width:200px;'> <br> ",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.proZone=="")
						{
							alert("请填写地区名称");
							return(false);
						}
						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
							document.add_zone_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/addZone.action";
							document.add_zone_form.name.value = f.proZone;
							document.add_zone_form.sc_id.value = sc_id;
							document.add_zone_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function modZone(company,sz_id)
{	
	$.prompt(
	
	"<div id='title'>修改地区["+company+"]</div><br>&nbsp;&nbsp;地区名称 &nbsp;&nbsp;&nbsp;<input name='proZone' type='text' id='proZone' style='width:200px;'> <br> ",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.proZone=="")
						{
							alert("请填写地区名称");
							return(false);
						}
						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/getDetailZoneJSON.action",
							{sz_id:sz_id},
							function callback(data)
							{   
								$("#proZone").val(data.name);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
							document.mod_zone_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/modZone.action";
							document.mod_zone_form.name.value = f.proZone;
							document.mod_zone_form.sz_id.value = sz_id;
							document.mod_zone_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function delZone(company,zone,sz_id)
{
 	if ( confirm("确定删除 "+company+" "+zone+" ？") )
	{
		document.del_zone_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/delZone.action";
		document.del_zone_form.sz_id.value = sz_id;
		document.del_zone_form.submit();
	}
}

function getShippingFee()
{
	var sc_id = $("#cacul_sc_id").val();
	var weight = $("#weight").val();
	var ccid = $("#cacul_ccid").val();
	var pro_id = $("#cacul_province").val();

	var para = "sc_id="+sc_id+"&weight="+weight+"&ccid="+ccid+"&pro_id="+pro_id;

	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/getShippingFee.action',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request)
		{
			if (weight=="")
			{
				alert("请填写重量");
				return(false);
			}
			else if (sc_id==0)
			{
				alert("请选择发货快递");
				return(false);
			}
			else if (ccid==0)
			{
				alert("请选择收货国家");
				return(false);
			}
			else if (pro_id==0)
			{
				alert("请选择收货地区");
				return(false);
			}
			
			return(true);
		},
		
		error: function(){

		},
		
		success: function(html)
		{
			if (html=="CountryOutSizeException")
			{
				alert("国家 "+$("#cacul_ccid").getSelectedText()+" 不能送达");
			}
			else if (html=="ProvinceOutSizeException")
			{
				alert("地区 "+$("#cacul_province").getSelectedText()+" 不能送达");
			}
			else if (html=="WeightCrossException")
			{
				alert("快递重量段设置重叠！");
			}
			else if (html=="WeightOutSizeException")
			{
				alert("快递重量段设置不正确！");
			}
			else
			{
				var msg = "";
				
				msg += "快递："+html.split("|")[2]+"\n";
				msg += "收货国家："+$("#cacul_ccid").getSelectedText()+"\n";
				msg += "收件地区："+html.split("|")[1]+"\n";
				msg += "原始重量："+html.split("|")[4]+"Kg\n";
				msg += "计费重量："+html.split("|")[3]+"Kg\n";
				msg += "原始运费："+html.split("|")[5]+"\n";
				msg += "成本运费："+html.split("|")[0];
				
			
				alert(msg);
			}
			
		}
	});
}


function switchCompanyLock(sc_id)
{
	var para = "sc_id="+sc_id;
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/switchCompanyLock.action',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request)
		{
			return(true);
		},
		
		error: function(){

		},
		
		success: function(html)
		{
			if ( html==-1 )
			{
				alert("操作失败！");
			}
			else if ( html==1 )
			{
				$("#lockImg_"+sc_id).attr("src","../imgs/lock.gif");
			}
			else
			{
				$("#lockImg_"+sc_id).attr("src","../imgs/unlock.gif");
			}
		}
	});

}

function copyCompany(cur_sc_id,name,domestic)
{	
	$.prompt(
	
	"<div id='title'>复制快递数据</div><br>从快递 <select name='copy_company_sc_id' id='copy_company_sc_id'><option>数据加载中......</option></select> 复制数据到 “"+name+"” <br> ",
 
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.copy_company_sc_id==0)
						{
							alert("请选择一个快递");
							return(false);
						}
						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/GetAllExpressCompanyJSON.action",
									{domestic:domestic,cur_sc_id:cur_sc_id},
									function callback(data)
									{ 
										$("#copy_company_sc_id").clearAll();
										$("#copy_company_sc_id").addOption("请选择......","0");
										
										if (data!="")
										{
											$.each(data,function(i)
											{
												if (data[i].sc_id!=cur_sc_id)
												{
													$("#copy_company_sc_id").addOption(data[i].name,data[i].sc_id);
												}
											});
										}
									}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
							document.copy_company_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/CopyCompany.action";
							document.copy_company_form.cur_sc_id.value = cur_sc_id;
							document.copy_company_form.sc_id.value = f.copy_company_sc_id;
							document.copy_company_form.domestic.value = domestic;
							document.copy_company_form.submit();
					} 
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 复制: "y", 取消: "n" }
	});
}

function showDiv(i)
{
	if (document.getElementById("express_"+i).style.display=="none")
	{
		document.getElementById("express_"+i).style.display="";
	}
	else
	{
        document.getElementById("express_"+i).style.display="none";
	}
}

function changeDevStorage(ps_id)
{
	$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getExpressCompanysByPsIdJSON.action",
		{ps_id:ps_id},
		function callback(data)
		{   
			$("#cacul_sc_id").clearAll();
			$("#cacul_sc_id").addOption("请选择......","0");
										
			if (data!="")
			{
				$.each(data,function(i)
				{
					var preStr;
					if (data[i].domestic==1)
					{
						preStr = " [国内]";
					}
					else
					{
						preStr = " <国际>";
					}
					$("#cacul_sc_id").addOption(data[i].name+preStr,data[i].sc_id);
				});
			}
		}
	);
}

function changeCountry(ccid)
{
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#cacul_province").clearAll();
					$("#cacul_province").addOption("请选择......","");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#cacul_province").addOption(data[i].pro_name,data[i].pro_id);
						});
					}			
										
					if (ccid>0)
					{
					
						$("#cacul_province").addOption("Others","-1");
					}
				}
		); 
}

function importData(name)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/shipping/import_express_rate.html?name="+name;
	$.artDialog.open(uri,{title:"导入"+name+"运费价格表",width:'600px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	//tb_show('导入'+name+'运费价格表','import_express_rate.html?name='+name+'TB_iframe=true&height=300&width=600',false);
}

function modUseTime(sc_id)
{	
	$.prompt(
	
	"<div id='title'>设置快递不推荐使用时段</div><br>&nbsp;&nbsp;<select id='proBeginDay' name='proBeginDay'><option value='0'>周日</option>	<option value='1'>周一</option><option value='2'>周二</option><option value='3'>周三</option><option value='4'>周四</option><option value='5'>周五</option><option value='6'>周六</option></select> &nbsp;&nbsp;<input type='text' name='proBeginHour' id='proBeginHour' value=''/>&nbsp;&nbsp;时开始(0-23)<br><br>&nbsp;&nbsp;持续&nbsp;&nbsp;<input type='text' id='proLastHour' name='proLastHour' value=''/>&nbsp;&nbsp;小时",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
      		 			var reg = new RegExp("^([0-9]|[1][0-9]|[2][0-3])$");
       					
       					var reg2 = new RegExp("^(0|[1-9][0-9]*)$");
       					if(!reg.test(f.proBeginHour))
       					{
        					alert("请输正确的时间（0-23）!");
        					return false;
						}
						else if(!reg2.test(f.proLastHour))
       					{
        					alert("请输正确的时间间隔!");
        					return false;
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/getDetailCompanyJSON.action",
							{sc_id:sc_id},
							function callback(data)
							{   
								$("#proBeginDay").val(data.begin_day);
								$("#proBeginHour").val(data.begin_hour);
								$("#proLastHour").val(data.last_hour);
							}
					);
				}  
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_usetime_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/modCompanyUseTime.action"
						document.mod_usetime_form.begin_day.value = f.proBeginDay;
						document.mod_usetime_form.begin_hour.value = f.proBeginHour;
						document.mod_usetime_form.last_hour.value = f.proLastHour;
						document.mod_usetime_form.sc_id.value = sc_id;
						document.mod_usetime_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 设置: "y", 取消: "n" }
	});
}

function modWeightCurrency(sc_id)
{	
	$.prompt(
	
	"<div id='title'>设置快递基础数据</div><br>&nbsp;&nbsp;重量单位 &nbsp;&nbsp;<input type='radio' name='proWeightUnit' id='proWeightUnit' value='千克' />千克&nbsp;&nbsp;<input type='radio' name='proWeightUnit' id='proWeightUnit'  value='磅' />磅&nbsp;&nbsp;<input type='radio' name='proWeightUnit' id='proWeightUnit'  value='安士' />安士 <br><br>&nbsp;&nbsp;货币单位 &nbsp;&nbsp;<input type='radio' name='proCurrency' id='proCurrency' value='人民币' />人民币&nbsp;&nbsp;<input type='radio'  id='proCurrency' name='proCurrency' value='美元' />美元",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if ( typeof(f.proWeightUnit)=='undefined' )
						{
							alert("请选择重量单位");
							return(false);
						}
						else if ( typeof(f.proCurrency)=='undefined' )
						{
							alert("请选择货币单位");
							return(false);
						}
						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/getDetailCompanyJSON.action",
							{sc_id:sc_id},
							function callback(data)
							{   
								var weight_unit = data.weight_unit;
								var currency = data.currency;

								$("input[name=proWeightUnit][value="+weight_unit+"]").attr("checked",true);
								$("input[name=proCurrency][value="+currency+"]").attr("checked",true); 
							}
					);
				}  
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						var weight_unit = f.proWeightUnit;
						var weight_rate;
						var currency = f.proCurrency;
						var currency_rate;
						
						//1千克 = 35.2739619 盎司,1千克 = 2.20462262 磅
						if (weight_unit=="千克")
						{
							weight_rate = 1;
						}
						else if (weight_unit=="磅")
						{
							weight_rate = 2.2;
						}
						else 
						{
							weight_rate = 35.27;
						}

						if (currency=="人民币")   
						{
							currency_rate = 1;
						}
						else 
						{
							currency_rate = 6.35;
						}
						
						document.mod_weight_currency_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/express/ModWeightCurrency.action";
						document.mod_weight_currency_form.weight_unit.value = weight_unit;
						document.mod_weight_currency_form.weight_rate.value = weight_rate;
						document.mod_weight_currency_form.currency.value = currency;
						document.mod_weight_currency_form.currency_rate.value = currency_rate;
						document.mod_weight_currency_form.sc_id.value = sc_id;
						document.mod_weight_currency_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 设置: "y", 取消: "n" }
	});
}
jQuery(function($){
	 
	 
	$(".tabs_").tabs();
})

function closeWinRefresh(){
	window.location.reload();
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<br>
<form name="mod_weight_form" method="post">
<input type="hidden" name="st_weight">
<input type="hidden" name="en_weight">
<input type="hidden" name="weight_step">
<input type="hidden" name="sw_id">
</form>

<form name="mod_usetime_form" method="post">
	<input type="hidden" name="begin_day"/>
	<input type="hidden" name="begin_hour"/>
	<input type="hidden" name="last_hour"/>
	<input type="hidden" name="sc_id">
</form>

<form name="mod_weight_currency_form" method="post">
<input type="hidden" name="weight_unit">
<input type="hidden" name="weight_rate">
<input type="hidden" name="currency">
<input type="hidden" name="currency_rate">
<input type="hidden" name="sc_id">
</form>

<form name="del_zone_form" method="post">
<input type="hidden" name="sz_id">
</form> 

<form name="mod_zone_form" method="post">
<input type="hidden" name="sz_id">
<input type="hidden" name="name">
</form> 


<form name="add_zone_form" method="post">
<input type="hidden" name="sc_id">
<input type="hidden" name="name">
</form> 

<form name="mod_company_form" method="post">
<input type="hidden" name="sc_id">
<input type="hidden" name="name">
<input type="hidden" name="shipping_fee_discount">
<input type="hidden" name="oil_addition_fee">
<input type="hidden" name="ps_id">
<input type="hidden" name="st_weight">
<input type="hidden" name="en_weight">
<input type="hidden" name="handle_fee">
<input type="hidden" name="print_weight_discount">
</form>  

<form name="del_company_form" method="post">
<input type="hidden" name="sc_id">
</form>  

<form name="del_weight_form" method="post">
<input type="hidden" name="sw_id">
</form>  

<form name="add_weight_form" method="post">
<input type="hidden" name="st_weight">
<input type="hidden" name="en_weight">
<input type="hidden" name="weight_step">
<input type="hidden" name="sc_id">
</form>

<form name="add_company_form" method="post">
<input type="hidden" name="name">
<input type="hidden" name="shipping_fee_discount">
<input type="hidden" name="oil_addition_fee">
<input type="hidden" name="ps_id">
<input type="hidden" name="st_weight">
<input type="hidden" name="en_weight">
<input type="hidden" name="handle_fee">
<input type="hidden" name="domestic">
<input type="hidden" name="print_weight_discount">
</form>  

<form name="copy_company_form" method="post">
<input type="hidden" name="cur_sc_id">
<input type="hidden" name="sc_id">
<input type="hidden" name="domestic">
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   快递管理</td>
  </tr>
</table>
<br>

<div style="display:none;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:1000px;">
<table width="1058" border="0" align="center" cellpadding="0" cellspacing="0" height="106">
  <tr>
    <td width="16%" height="30" align="center" style="font-size:20px;font-weight:bold">运费计算器</td>
    
    <td width="47%" align="left" style="border-left:1px #CCCCCC solid;padding-left:10px;">
	
收货国家
&nbsp;&nbsp;
        <select name="cacul_ccid" id="cacul_ccid" onChange="changeCountry(this.value)">
<%
DBRow countrycode[] = orderMgr.getAllCountryCode();
for (int i=0; i<countrycode.length; i++)
{
%>
	  <option value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
<%
}
%>
      </select>	</td>
	  <td width="22%" align="left" style="border-left:1px #CCCCCC solid;padding-left:10px;"> 发货仓库&nbsp;&nbsp;
	<select name="cacul_ps_id" id="cacul_ps_id" onChange="changeDevStorage(this.value)">
		<option value="0">请选择......</option>
          <%
String qx;

for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
          <option value="<%=treeRows[i].getString("id")%>" > 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>          </option>
          <%
}
%>
        </select>
	</td>
    <td width="15%" align="center">
	<input name="Submit2" type="button" class="long-button-ok" value="计算" onClick="getShippingFee()"></td>
  </tr>
  <tr>
    <td height="30" align="center">
	重量
        <input type="text" name="weight" id="weight" style="width:80px;">
      Kg	</td>
    <td align="left" style="border-left:1px #CCCCCC solid;padding-left:10px;">
	收货地区&nbsp;&nbsp;
	<select name="cacul_province" id="cacul_province"  >
	 <option value="0">请选择......</option>
	  </select>
	</td>
 <td align="left" style="border-left:1px #CCCCCC solid;padding-left:10px;">
	发货快递
	&nbsp;&nbsp;
      <select name="cacul_sc_id" id="cacul_sc_id">
        <option value="0">请选择......</option>
      </select>
	</td>
    <td align="left">&nbsp;</td>
  </tr>
</table>
</div>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
      <tr> 
        <th class="left-title"  style="vertical-align: center;text-align: center;">所属仓库</th>
        <th width="49%"  style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
        <th width="26%"  style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
      </tr>
    <%
for ( int j=0; j<treeRows.length; j++ )
{
%>
    <tr > 
     <td width="25%" height="60" valign="middle" bgcolor="#f8f8f8">
     <a href="javascript:void(0);" onClick="showDiv('<%=j%>')">
     <img src="../js/images/next.gif" border="0" /> 
        <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",treeRows[j].get("level",0))%>
      				<%
		if (treeRows[j].get("parentid",0) == 0)
		{
			out.println("<span style='font-size:14px;font-weight:bold'>"+treeRows[j].getString("title")+" warehouse</span>");
		}
		else
		{
			out.println(treeRows[j].getString("title"));
		}
		%>
	   </a>	  </td>
      <td align="right" valign="middle" bgcolor="#f8f8f8" style="padding-right:20px;">&nbsp;	  	  </td>
      <td align="center" valign="middle" bgcolor="#f8f8f8" style="padding-right:20px;">&nbsp;</td>
    </tr>
    <tr id="express_<%=j%>" style="display:none">
      <td colspan="3" valign="middle" bgcolor="#f8f8f8" style="padding:15px;">
      		<% String fixId = treeRows[j].get("id",0l)+""; %>
      		<div class="tabs_" id="tabs_<%=fixId %>">
      			<ul >
      				<li><a href="#express_incountry_<%=fixId %>">国内快递</a></li>
      				<li><a href="#express_outcontry_<%=fixId %>">国际快递</a></li>
      			</ul>
      			<div id="express_incountry_<%=fixId %>">
      				 <input name="Submit" type="button" class="short-short-button-redtext" value="增加" onClick="addCompany(<%=treeRows[j].get("id",0l)%>,1)"> <br />
			 		  <div class="tabs_">
			 		 	<ul>
				 			<%
							companys = expressMgr.getAllExpressCompanysByPsIdDomestic(treeRows[j].get("id",0l),1,null);
							
				 			for (int i=0; i< companys.length; i++){
				 				 
							%>
				 		  		<li><a href='#express_outcontry_<%=fixId %>_<%=companys[i].getString("sc_id") %>'> <%=companys[i].getString("name")%></a></li>
				 		 	<%} %>	
			 		 	 </ul>
			 		 	 <%
			 		   	 	for(int i = 0 ; i < companys.length; i++) {
				 		   	 	DBRow zones[] = expressMgr.getZoneByScId(companys[i].get("sc_id",0l));
								DBRow feeWeightSection[] = expressMgr.getFeeWeightSection(companys[i].get("sc_id",0l));
				
								//重新分装下数据，减少数据库查询
								DBRow zone_fee = new DBRow();
								for (int z=0; z<zones.length; z++)
								{
									String key;
									DBRow fee[] = expressMgr.getFeeBySzId(zones[z].get("sz_id",0l));
									
									for (int f=0; f<fee.length; f++)
									{
										key = zones[z].getString("sz_id")+"_"+fee[f].getString("st_weight")+"_"+fee[f].getString("en_weight");
										zone_fee.add(key,fee[f]);
									}		
								}
			 		   	 	%>
			 		   	 		<div id='express_outcontry_<%=fixId %>_<%=companys[i].getString("sc_id") %>'>
			 		   	 			
			 		   	 			 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
				 		   	 		  <tr >
										  <td width="13%" rowspan="2" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;" ><img src="../imgs/order_shipping.gif">&nbsp; <%=companys[i].getString("name")%></td>
										  <td height="35" colspan="6" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;">重量单位：<font color="#0099CC"><%=companys[i].getString("weight_unit")%></font>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  货币单位：<font color="#0099CC"><%=companys[i].getString("currency")%></font> 
											  &nbsp;&nbsp;&nbsp;<a href="javascript:modWeightCurrency(<%=companys[i].getString("sc_id")%>)"><img src="../imgs/application-blue.png" width="14" height="14" border="0" align="absmiddle"></a> </td>
									   </tr>
										<tr > 
										<td width="21%" height="35" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;"><img src="../imgs/156.gif" width="16" height="16">&nbsp; 折扣：<%=companys[i].get("shipping_fee_discount",0d)%>&nbsp;&nbsp;/&nbsp;&nbsp;燃油附加费：<%=companys[i].get("oil_addition_fee",0d)%></td>
										<td width="17%" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;">适重：<%=companys[i].get("st_weight",0f)%>Kg - <%=companys[i].get("en_weight",0f)%>Kg </td>
										<td width="15%" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;">处理费：<%=companys[i].get("handle_fee",0d)%> RMB</td>
										<td width="9%" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;">打印重折：<%=companys[i].get("print_weight_discount",0d)%></td>
										<td width="10%" valign="middle" bgcolor="#eeeeee" style="color:#CCCCCC;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;">
											  <%=companys[i].getString("imp_class")%>&nbsp;	  </td>
										<td   align="center" valign="middle" bgcolor="#eeeeee" style="border-right:1px #CCCCCC solid;border-left:1px #CCCCCC solid;border-top:2px #CCCCCC solid;" ><a href="javascript:modCompany(<%=companys[i].get("sc_id",0l)%>)"><img src="../imgs/modp.gif" width="16" height="16" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:delCompany('<%=companys[i].getString("name")%>',<%=companys[i].get("sc_id",0l)%>)"><img src="../imgs/delp.gif" width="16" height="16" border="0"></a>
											  &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:modUseTime(<%=companys[i].get("sc_id",0l)%>)"><img src="../imgs/alarm-clock--arrow.png" border="0" id="lockImg_<%=companys[i].get("sc_id",0l)%>"></a>&nbsp;&nbsp;&nbsp;&nbsp;
											  <a href="javascript:copyCompany(<%=companys[i].getString("sc_id")%>,'<%=companys[i].getString("name")%>',<%=companys[i].get("domestic",0l)%>)"><img src="../imgs/detail.gif" width="16" height="16" border="0"></a>	  
											   &nbsp;&nbsp;
											   <a href="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/ExportExpressFee.action?sc_id=<%=companys[i].getString("sc_id")%>">导出</a>	  
												    &nbsp;&nbsp;
											   <a href="javascript:importData('<%=companys[i].getString("name")%>')">导入</a>	   </td>
									    </tr>
			 		   	 			 </table>
			 		   	 			 
					 		   	 		<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
										<tr>
										  <td width="15%" align="center" valign="middle" bgcolor="#D3EB9A" style="font-weight:bold;height:30px;">重量(Kg) <a href="javascript:addWeight('<%=companys[i].getString("name")%>',<%=companys[i].get("sc_id",0l)%>)">  <img src="../imgs/add.png" alt="增加区域" width="16" height="16" border="0" align="absmiddle"></a></td>
											  <%
											  if (zones.length==0){
											  %>
											  <td align="center" valign="middle"   bgcolor="#D3EB9A" >?		  </td>
											  <%
											  }
											  for (int z=0; z<zones.length; z++) {
											  %>
											  <td  align="center" valign="middle"  bgcolor="#D3EB9A"  style="font-weight:bold"><a href="javascript:modZone('<%=companys[i].getString("name")%>',<%=zones[z].getString("sz_id")%>)"><%=zones[z].getString("name")%></a> &nbsp;&nbsp;	
											  <a href="javascript:SetCountry('<%=companys[i].getString("name")%>','<%=zones[z].getString("name")%>',<%=zones[z].getString("sz_id")%>,<%=companys[i].get("sc_id",0l)%>,<%=companys[i].get("domestic",0l)%>);"><img src="../imgs/application-blue.png" width="14" height="14" border="0" align="absmiddle"></a>
											  &nbsp;  
											    <a href="javascript:delZone('<%=companys[i].getString("name")%>','<%=zones[z].getString("name")%>',<%=zones[z].getString("sz_id")%>)"><img src="../imgs/del.gif" width="12" height="12" border="0" align="absmiddle"></a></td>
											  <%
											  }
											  %>		  
											  <td width="10%"  align="center" valign="middle"  bgcolor="#D3EB9A"  style="font-weight:bold"><a href="javascript:addZone('<%=companys[i].getString("name")%>',<%=companys[i].get("sc_id",0l)%>)"><img src="../imgs/add.png" alt="增加区域" width="16" height="16" border="0"></a></td>
										</tr>
										
										<%
										String rowBg;
										for (int k=0; k<feeWeightSection.length; k++)
										{
											if (k%2==0)
											{
												rowBg = "#FFFFFF";
											}
											else
											{
												rowBg = "#FFFFCC";
											}
										%>		
										<tr>
										  <td align="center" valign="middle" bgcolor="<%=rowBg%>" height="25" >
											  
											  <a href="javascript:modWeight('<%=companys[i].getString("name")%>',<%=feeWeightSection[k].get("sw_id",0l)%>)"><%=MoneyUtil.round(feeWeightSection[k].get("st_weight",0f)*companys[i].get("weight_rate",0f), 2)%> ~ <%=MoneyUtil.round(feeWeightSection[k].get("en_weight",0f)*companys[i].get("weight_rate",0f),2)%>(<%=MoneyUtil.round(feeWeightSection[k].get("weight_step",0f)*companys[i].get("weight_rate",0f),2)%>)</a>		  </td>
											  <%
											  if (zones.length==0)
											  {
											  %>
											  <td align="center" valign="middle"  bgcolor="<%=rowBg%>"  >?		  </td>
											  <%
											  }
											  
											  for (int z=0; z<zones.length; z++)
											  {
											  %>
										  <td align="center" valign="middle"  bgcolor="<%=rowBg%>"  >
													<%
													Object obj = zone_fee.get(zones[z].getString("sz_id")+"_"+feeWeightSection[k].getString("st_weight")+"_"+feeWeightSection[k].getString("en_weight"),new Object());
							
													if (obj instanceof DBRow)
													{
														DBRow fee = (DBRow)obj;
														out.println(MoneyUtil.round(fee.get("f_weight_fee",0d)/companys[i].get("currency_rate",0f),2)+"/"+MoneyUtil.round(fee.get("k_weight_fee",0d)/companys[i].get("currency_rate",0f),2));
														
													}
													else
													{
														out.println("&nbsp;");
													}
													%>			</td>
											 
											  <%
											  }
											  %>
											   <td align="center" valign="middle"  bgcolor="<%=rowBg%>"  ><a href="javascript:addWeightZoneFee(<%=companys[i].get("sc_id",0l)%>,<%=feeWeightSection[k].get("sw_id",0l)%>)"><img src="../imgs/application_edit.png" width="16" height="16" border="0"></a>
											   &nbsp;&nbsp;&nbsp;
											   <a href="javascript:delWeight(<%=feeWeightSection[k].get("sw_id",0l)%>,<%=feeWeightSection[k].get("st_weight",0f)%>,<%=feeWeightSection[k].get("en_weight",0f)%>)"><img src="../imgs/del.gif" width="12" height="12" border="0" align="absmiddle"></a>		   </td>
										</tr>
										<%
										}
										%>
							      	</table>
			 		   	 			 
			 		   	 		</div>
			 		   	 	<% 	
			 		   	 	}
			 		 	 %>
			 		 	
			 		 </div>
			 		
			 	</div>
			 	<div id="express_outcontry_<%=fixId %>">
			 		<input name="Submit" type="button" class="short-short-button-redtext" value="增加" onClick="addCompany(<%=treeRows[j].get("id",0l)%>,0)" /><br />

			 		<div class="tabs_">
			 		 	<ul>
				 			<%
							companys = expressMgr.getAllExpressCompanysByPsIdDomestic(treeRows[j].get("id",0l),0,null);
							
				 			for (int i=0; i< companys.length; i++){
				 				 
							%>
				 		  		<li><a href='#express_outcontry_<%=fixId %>_<%=companys[i].getString("sc_id") %>'> <%=companys[i].getString("name")%></a></li>
				 		 	<%} %>	
			 		 	 </ul>
			 		 	 <%
			 		   	 	for(int i = 0 ; i < companys.length; i++) {
				 		   	 	DBRow zones[] = expressMgr.getZoneByScId(companys[i].get("sc_id",0l));
								DBRow feeWeightSection[] = expressMgr.getFeeWeightSection(companys[i].get("sc_id",0l));
				
								//重新分装下数据，减少数据库查询
								DBRow zone_fee = new DBRow();
								for (int z=0; z<zones.length; z++)
								{
									String key;
									DBRow fee[] = expressMgr.getFeeBySzId(zones[z].get("sz_id",0l));
									
									for (int f=0; f<fee.length; f++)
									{
										key = zones[z].getString("sz_id")+"_"+fee[f].getString("st_weight")+"_"+fee[f].getString("en_weight");
										zone_fee.add(key,fee[f]);
									}		
								}
			 		   	 	%>
			 		   	 		<div id='express_outcontry_<%=fixId %>_<%=companys[i].getString("sc_id") %>'>
			 		   	 			
			 		   	 			 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
				 		   	 		  <tr >
										  <td width="13%" rowspan="2" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;" ><img src="../imgs/order_shipping.gif">&nbsp; <%=companys[i].getString("name")%></td>
										  <td height="35" colspan="6" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;">重量单位：<font color="#0099CC"><%=companys[i].getString("weight_unit")%></font>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  货币单位：<font color="#0099CC"><%=companys[i].getString("currency")%></font> 
											  &nbsp;&nbsp;&nbsp;<a href="javascript:modWeightCurrency(<%=companys[i].getString("sc_id")%>)"><img src="../imgs/application-blue.png" width="14" height="14" border="0" align="absmiddle"></a> </td>
									   </tr>
										<tr > 
										<td width="21%" height="35" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;"><img src="../imgs/156.gif" width="16" height="16">&nbsp; 折扣：<%=companys[i].get("shipping_fee_discount",0d)%>&nbsp;&nbsp;/&nbsp;&nbsp;燃油附加费：<%=companys[i].get("oil_addition_fee",0d)%></td>
										<td width="17%" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;">适重：<%=companys[i].get("st_weight",0f)%>Kg - <%=companys[i].get("en_weight",0f)%>Kg </td>
										<td width="15%" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;">处理费：<%=companys[i].get("handle_fee",0d)%> RMB</td>
										<td width="9%" valign="middle" bgcolor="#eeeeee" style="color:#666666;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;">打印重折：<%=companys[i].get("print_weight_discount",0d)%></td>
										<td width="10%" valign="middle" bgcolor="#eeeeee" style="color:#CCCCCC;padding-left:10px;font-size:15px;border-left:1px #CCCCCC solid;font-weight:bold;border-top:2px #CCCCCC solid;">
											  <%=companys[i].getString("imp_class")%>&nbsp;	  </td>
										<td   align="center" valign="middle" bgcolor="#eeeeee" style="border-right:1px #CCCCCC solid;border-left:1px #CCCCCC solid;border-top:2px #CCCCCC solid;" ><a href="javascript:modCompany(<%=companys[i].get("sc_id",0l)%>)"><img src="../imgs/modp.gif" width="16" height="16" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:delCompany('<%=companys[i].getString("name")%>',<%=companys[i].get("sc_id",0l)%>)"><img src="../imgs/delp.gif" width="16" height="16" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;
 <a href="javascript:modUseTime(<%=companys[i].get("sc_id",0l)%>)"><img src="../imgs/alarm-clock--arrow.png" border="0" id="lockImg_<%=companys[i].get("sc_id",0l)%>"></a>											  &nbsp;&nbsp;
											  <a href="javascript:copyCompany(<%=companys[i].getString("sc_id")%>,'<%=companys[i].getString("name")%>',<%=companys[i].get("domestic",0l)%>)"><img src="../imgs/detail.gif" width="16" height="16" border="0"></a>	  
											   &nbsp;&nbsp;
											   <a href="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/ExportExpressFee.action?sc_id=<%=companys[i].getString("sc_id")%>">导出</a>	  
												    &nbsp;&nbsp;
											   <a href="javascript:importData('<%=companys[i].getString("name")%>')">导入</a>	   </td>
									    </tr>
			 		   	 			 </table>
			 		   	 			 
					 		   	 		<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
										<tr>
										  <td width="15%" align="center" valign="middle" bgcolor="#D3EB9A" style="font-weight:bold;height:30px;">重量(Kg) <a href="javascript:addWeight('<%=companys[i].getString("name")%>',<%=companys[i].get("sc_id",0l)%>)">  <img src="../imgs/add.png" alt="增加区域" width="16" height="16" border="0" align="absmiddle"></a></td>
											  <%
											  if (zones.length==0){
											  %>
											  <td align="center" valign="middle"   bgcolor="#D3EB9A" >?		  </td>
											  <%
											  }
											  for (int z=0; z<zones.length; z++) {
											  %>
											  <td  align="center" valign="middle"  bgcolor="#D3EB9A"  style="font-weight:bold"><a href="javascript:modZone('<%=companys[i].getString("name")%>',<%=zones[z].getString("sz_id")%>)"><%=zones[z].getString("name")%></a> &nbsp;&nbsp;	
											  <a href="javascript:SetCountry('<%=companys[i].getString("name")%>','<%=zones[z].getString("name")%>',<%=zones[z].getString("sz_id")%>,<%=companys[i].get("sc_id",0l)%>,<%=companys[i].get("domestic",0l)%>);"><img src="../imgs/application-blue.png" width="14" height="14" border="0" align="absmiddle"></a>
											  &nbsp;  
											    <a href="javascript:delZone('<%=companys[i].getString("name")%>','<%=zones[z].getString("name")%>',<%=zones[z].getString("sz_id")%>)"><img src="../imgs/del.gif" width="12" height="12" border="0" align="absmiddle"></a></td>
											  <%
											  }
											  %>		  
											  <td width="10%"  align="center" valign="middle"  bgcolor="#D3EB9A"  style="font-weight:bold"><a href="javascript:addZone('<%=companys[i].getString("name")%>',<%=companys[i].get("sc_id",0l)%>)"><img src="../imgs/add.png" alt="增加区域" width="16" height="16" border="0"></a></td>
										</tr>
										
										<%
										String rowBg;
										for (int k=0; k<feeWeightSection.length; k++)
										{
											if (k%2==0)
											{
												rowBg = "#FFFFFF";
											}
											else
											{
												rowBg = "#FFFFCC";
											}
										%>		
										<tr>
										  <td align="center" valign="middle" bgcolor="<%=rowBg%>" height="25" >
											  
											  <a href="javascript:modWeight('<%=companys[i].getString("name")%>',<%=feeWeightSection[k].get("sw_id",0l)%>)"><%=MoneyUtil.round(feeWeightSection[k].get("st_weight",0f)*companys[i].get("weight_rate",0f), 2)%> ~ <%=MoneyUtil.round(feeWeightSection[k].get("en_weight",0f)*companys[i].get("weight_rate",0f),2)%>(<%=MoneyUtil.round(feeWeightSection[k].get("weight_step",0f)*companys[i].get("weight_rate",0f),2)%>)</a>		  </td>
											  <%
											  if (zones.length==0)
											  {
											  %>
											  <td align="center" valign="middle"  bgcolor="<%=rowBg%>"  >?		  </td>
											  <%
											  }
											  
											  for (int z=0; z<zones.length; z++)
											  {
											  %>
										  <td align="center" valign="middle"  bgcolor="<%=rowBg%>"  >
													<%
													Object obj = zone_fee.get(zones[z].getString("sz_id")+"_"+feeWeightSection[k].getString("st_weight")+"_"+feeWeightSection[k].getString("en_weight"),new Object());
							
													if (obj instanceof DBRow)
													{
														DBRow fee = (DBRow)obj;
														out.println(MoneyUtil.round(fee.get("f_weight_fee",0d)/companys[i].get("currency_rate",0f),2)+"/"+MoneyUtil.round(fee.get("k_weight_fee",0d)/companys[i].get("currency_rate",0f),2));
														
													}
													else
													{
														out.println("&nbsp;");
													}
													%>			</td>
											 
											  <%
											  }
											  %>
											   <td align="center" valign="middle"  bgcolor="<%=rowBg%>"  ><a href="javascript:addWeightZoneFee(<%=companys[i].get("sc_id",0l)%>,<%=feeWeightSection[k].get("sw_id",0l)%>)"><img src="../imgs/application_edit.png" width="16" height="16" border="0"></a>
											   &nbsp;&nbsp;&nbsp;
											   <a href="javascript:delWeight(<%=feeWeightSection[k].get("sw_id",0l)%>,<%=feeWeightSection[k].get("st_weight",0f)%>,<%=feeWeightSection[k].get("en_weight",0f)%>)"><img src="../imgs/del.gif" width="12" height="12" border="0" align="absmiddle"></a>		   </td>
										</tr>
										<%
										}
										%>
							      	</table>
			 		   	 			 
			 		   	 		</div>
			 		   	 	<% 	
			 		   	 	}
			 		 	 %>
			 		 	
			 		 </div>
			 
			 	</div>
      		</div>
      		 
		</td>
    </tr>
<%
}
%>
    
</table>

 
	  
   	  </td>
    </tr>
 
</table>

<br>
	

</body>
</html>



