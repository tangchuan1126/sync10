<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderSaveKey"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../include.jsp"%> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 DBRow detailGroup = adminMgr.getDetailAdminGroup( adminLoggerBean.getAdgid() );
 DBRow supperLiRow = adminLoggerBean.getAttach();
 long supplier_id = 0l ;
		 if(supperLiRow != null){
			 supplier_id= supperLiRow.get("id",0l);
		 }

 DBRow[] nofinishPurchase = purchaseMgr.getNoFinishPurchase(supplier_id);
 DBRow[] nofinishDeliveryOrder = deliveryMgrZJ.getDeliveryOrderNoFinishBySupplier(supplier_id);

 PurchaseKey purchasekey = new PurchaseKey();
 PurchaseMoneyKey purchasemoneykey = new PurchaseMoneyKey();
 PurchaseArriveKey purchasearrivekey = new PurchaseArriveKey();
 DeliveryOrderKey deliveryOrderKey = new DeliveryOrderKey();
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>

<link type="text/css" href="js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery/ui/ui.core.js"></script>
	<script type="text/javascript" src="js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="js/tabs/demos.css" rel="stylesheet" />
	
<link href="comm.css" rel="stylesheet" type="text/css"/>
<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.h2{
	font-size:14px;
	font-style: Arila Black; 
	font-weight:bold;
	margin:10px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
</style>



</head>

<body >
<table width="95%"  border="0" cellpadding="0" cellspacing="0" style="margin:20px;">
  <tr>
    <td width="50%"  align="left" valign="top">
    <table width="268" height="125" border="0" cellpadding="0" cellspacing="0" class="profile">
      <tr>
        <td width="130">&nbsp;</td>
        <td align="left" valign="top" style="font-size:12px;line-height:20px;padding-top:5px;">帐号:<strong><%=adminLoggerBean.getAttach().getString("sup_name")%></strong><br/><br/>
          角色:<strong><%=detailGroup.getString("name")%></strong>
		  <br />
<br />
登录成功！		  </td>
      </tr>
    </table>
    </td>
    <td width="50%"  align="left" valign="top"> 
    	 
	<br /></td>
  </tr>
 
  <tr>
    <td colspan="2"  align="left" valign="top">
		<br/>
		<div align="center" class="h2">未完成的采购单</div>
    	 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
		            <tr>
		              <td width="100%" style="padding-top:3px;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable"">
								<tr>
									<th width="9%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">采购单号</th>
							        <th width="9%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">存储仓库</th>
							        <th width="10%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">创建时间</th>
							        <th width="7%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">总金额</th>
							        <th width="10%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">流程状态</th>
							        <th width="10%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">到货状态</th>
							        <th width="17%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">最新更新日期</th>
								</tr>
								  <%
								  	for(int i=0;i<nofinishPurchase.length;i++)
								  	{
								  %>
								  	<tr align="center" valign="middle">
									  	<td height="80" nowrap="nowrap"><a href="javascript:void(0)" onclick="window.location.href='supplier_login/purchase/supplier_purchase_detail.html?purchase_id=<%=nofinishPurchase[i].get("purchase_id",0) %>'">P<%=nofinishPurchase[i].getString("purchase_id")%></a></td>
									  	<td nowrap="nowrap"><%DBRow storage = catalogMgr.getDetailProductStorageCatalogById(nofinishPurchase[i].get("ps_id",0l));out.print(storage.getString("title"));%></td>
										<td nowrap="nowrap"><div align="left"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(nofinishPurchase[i].getString("purchase_date")))%></div></td>
									  	<td nowrap="nowrap"><%=purchaseMgr.getPurchasePrice(nofinishPurchase[i].get("purchase_id",0l))%></td>
								  		<td nowrap="nowrap">
									  		<%=purchasekey.getQuoteStatusById(nofinishPurchase[i].getString("purchase_status"))%>
									  	</td>
									  	<td nowrap="nowrap"><%=purchasearrivekey.getQuoteStatusById(nofinishPurchase[i].getString("arrival_time"))%></td>
									  	<td nowrap="nowrap" ><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(nofinishPurchase[i].getString("updatetime")))%></td>
								  </tr>
								  <%
								  	}
								   %>
							</table>
					 </td>
		              <td width="45%"></td>
		              <td width="12%" align="right" valign="middle"></td>
		            </tr>
		          </table>
		          <br/>
		          <div align="center" class="h2">未完成的交货单</div>
		          <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
		            <tr>
		              <td width="100%" style="padding-top:3px;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable"">
								<tr> 
							        <th width="12%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">交货单号</th>
							        <th width="8%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">目的仓库</th>
							        <th width="11%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">创建时间</th>
									<th width="7%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">状态</th>
							        <th width="14%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">货运公司</th>
							        <th width="14%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">运单号</th>
							        <th width="17%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;"></th>
							  	</tr>
								  <%
								  	for(int j=0;j<nofinishDeliveryOrder.length;j++)
								  	{
								  %>
								  	<tr align="center" valign="middle">
									  	<td height="40" nowrap="nowrap"><a href="supplier_login/delivery/delivery_order_detail.html?delivery_order_id=<%=nofinishDeliveryOrder[j].get("delivery_order_id",0l) %>"><%=nofinishDeliveryOrder[j].getString("delivery_order_number")%></a></td>
									  	<td nowrap="nowrap"><%=nofinishDeliveryOrder[j].getString("title")%></td>
									  	<td nowrap="nowrap"><%=nofinishDeliveryOrder[j].getString("delivery_date")%></td>
									  	<td nowrap="nowrap"><%=deliveryOrderKey.getDeliveryOrderStatusById(nofinishDeliveryOrder[j].get("delivery_order_status",0))%></td>
									  	<td nowrap="nowrap" ><%=nofinishDeliveryOrder[j].getString("waybill_name")%></td>
									  	<td nowrap="nowrap" ><%=nofinishDeliveryOrder[j].getString("waybill_number")%></td>
								  	  	<td align="center" nowrap="nowrap">
								  	  		<%
								  	  			if(nofinishDeliveryOrder[j].get("delivery_order_status",0) == DeliveryOrderKey.READY&&nofinishDeliveryOrder[j].get("save_status",0)==DeliveryOrderSaveKey.HASSAVE)
								  	  			{
								  	  		%>
								  	  			<input type="button" class="short-button" value="已起运" onclick="intransitDelivery(<%=nofinishDeliveryOrder[j].get("delivery_order_id",0l)%>,'<%=nofinishDeliveryOrder[j].getString("delivery_order_number")%>')"/>
								  	  		<%
								  	  			}  	  			
								  	  		%>
								  	  		<%
								  	  			if(nofinishDeliveryOrder[j].get("delivery_order_status",0)== DeliveryOrderKey.READY)
								  	  			{
								  	  		%>
								  	  			<input type="button" class="short-short-button-redtext" value="删除" onclick="delDelivery(<%=nofinishDeliveryOrder[j].get("delivery_order_id",0l)%>,'<%=nofinishDeliveryOrder[j].getString("delivery_order_number")%>')"/>
								  	  		<%
								  	  			}
								  	  		%>
								  	  	</td>
								  </tr>
								  <%
								  	}
								   %>
							</table>
					 </td>
		              <td width="45%"></td>
		              <td width="12%" align="right" valign="middle"></td>
		            </tr>
		          </table>
    	<!-- div class="demo">

		<div id="tabs">
			<ul>
				<li><a href="#nofinishPurchase">未完成采购单(<%=nofinishPurchase.length%>)<span> </span></a></li>
				<li><a href="#nofinishDeliveryOrder">未完成交货单(<%=nofinishDeliveryOrder.length%>)<span> </span></a></li>
			</ul>
			<div id="nofinishPurchase">
				 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
		            <tr>
		              <td width="100%" style="padding-top:3px;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable"">
								<tr>
									<th width="9%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">采购单号</th>
							        <th width="9%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">存储仓库</th>
							        <th width="10%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">创建时间</th>
							        <th width="7%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">总金额</th>
							        <th width="7%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">转款</th>
							        <th width="10%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">流程状态</th>
							        <th width="10%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">到货状态</th>
							        <th width="17%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">最新更新日期</th>
								</tr>
								  <%
								  	for(int i=0;i<nofinishPurchase.length;i++)
								  	{
								  %>
								  	<tr align="center" valign="middle">
									  	<td height="80" nowrap="nowrap"><a href="javascript:void(0)" onclick="window.location.href='supplier_login/purchase/supplier_purchase_detail.html?purchase_id=<%=nofinishPurchase[i].get("purchase_id",0) %>'">P<%=nofinishPurchase[i].getString("purchase_id")%></a></td>
									  	<td nowrap="nowrap"><%DBRow storage = catalogMgr.getDetailProductStorageCatalogById(nofinishPurchase[i].get("ps_id",0l));out.print(storage.getString("title"));%></td>
										<td nowrap="nowrap"><div align="left"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(nofinishPurchase[i].getString("purchase_date")))%></div></td>
									  	<td nowrap="nowrap"><%=purchaseMgr.getPurchasePrice(nofinishPurchase[i].get("purchase_id",0l))%></td>
									  	<td nowrap="nowrap"><%=purchasemoneykey.getQuoteStatusById(nofinishPurchase[i].getString("money_status"))%></td>
								  		<td nowrap="nowrap">
									  		<%=purchasekey.getQuoteStatusById(nofinishPurchase[i].getString("purchase_status"))%>
									  	</td>
									  	<td nowrap="nowrap"><%=purchasearrivekey.getQuoteStatusById(nofinishPurchase[i].getString("arrival_time"))%></td>
									  	<td nowrap="nowrap" ><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(nofinishPurchase[i].getString("updatetime")))%></td>
								  </tr>
								  <%
								  	}
								   %>
							</table>
					 </td>
		              <td width="45%"></td>
		              <td width="12%" align="right" valign="middle"></td>
		            </tr>
		          </table>
			</div>
			
			<div id="nofinishDeliveryOrder">
				 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
		            <tr>
		              <td width="100%" style="padding-top:3px;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable"">
								<tr> 
							        <th width="12%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">交货单号</th>
							        <th width="8%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">目的仓库</th>
							        <th width="11%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">创建时间</th>
									<th width="7%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">状态</th>
							        <th width="14%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">货运公司</th>
							        <th width="14%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">运单号</th>
							        <th width="17%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;"></th>
							  	</tr>
								  <%
								  	for(int j=0;j<nofinishDeliveryOrder.length;j++)
								  	{
								  %>
								  	<tr align="center" valign="middle">
									  	<td height="40" nowrap="nowrap"><a href="supplier_login/delivery/delivery_order_detail.html?delivery_order_id=<%=nofinishDeliveryOrder[j].get("delivery_order_id",0l) %>"><%=nofinishDeliveryOrder[j].getString("delivery_order_number")%></a></td>
									  	<td nowrap="nowrap"><%=nofinishDeliveryOrder[j].getString("title")%></td>
									  	<td nowrap="nowrap"><%=nofinishDeliveryOrder[j].getString("delivery_date")%></td>
									  	<td nowrap="nowrap"><%=deliveryOrderKey.getDeliveryOrderStatusById(nofinishDeliveryOrder[j].get("delivery_order_status",0))%></td>
									  	<td nowrap="nowrap" ><%=nofinishDeliveryOrder[j].getString("waybill_name")%></td>
									  	<td nowrap="nowrap" ><%=nofinishDeliveryOrder[j].getString("waybill_number")%></td>
								  	  	<td align="center" nowrap="nowrap">
								  	  		<%
								  	  			if(nofinishDeliveryOrder[j].get("delivery_order_status",0) == DeliveryOrderKey.READY&&nofinishDeliveryOrder[j].get("save_status",0)==DeliveryOrderSaveKey.HASSAVE)
								  	  			{
								  	  		%>
								  	  			<input type="button" class="short-button" value="已起运" onclick="intransitDelivery(<%=nofinishDeliveryOrder[j].get("delivery_order_id",0l)%>,'<%=nofinishDeliveryOrder[j].getString("delivery_order_number")%>')"/>
								  	  		<%
								  	  			}  	  			
								  	  		%>
								  	  	</td>
								  </tr>
								  <%
								  	}
								   %>
							</table>
					 </td>
		              <td width="45%"></td>
		              <td width="12%" align="right" valign="middle"></td>
		            </tr>
		          </table>
			</div>
		</div>
		
		</div>
	<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	</script-->
	</td>
  </tr>
</table>
<script type="text/javascript">
	function intransitDelivery(delivery_order_id,number)
	{
		if(confirm("确定"+number+"的货物已发送了？"))
		{
			var para = "delivery_order_id="+delivery_order_id;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/intransitDelivery.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
				}
			});
		}							
	}
	
	function delDelivery(delivery_order_id,number)
	{
		if(confirm("确定删除交货单"+number+"？"))
		{
			var para = "delivery_order_id="+delivery_order_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/delDeliveryOrder.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
</script>
</body>
</html>

