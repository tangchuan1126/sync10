 <%@page import="com.fr.third.org.apache.poi.util.SystemOutLogger"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.api.zr.BoxTypeMgrZr"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set,com.cwc.app.floor.api.zyj.service.ProductCustomerSerivce,com.cwc.app.floor.api.zyj.model.*"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,com.cwc.app.key.StorageTypeKey,java.util.ArrayList"%>
<jsp:useBean id="containerHasSnTypeKey" class="com.cwc.app.key.ContainerHasSnTypeKey"></jsp:useBean>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>CLP Type Ship To</title>
		<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />
		
		<!-- 基本css 和javascript -->
		<link href="../comm.css" rel="stylesheet" type="text/css">
		<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
		<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
		<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
		<script src="../js/zebra/zebra.js" type="text/javascript"></script>
		<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
		<!-- 引入Art -->
		<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
		<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
		<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
		<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
		<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
		<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
		<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
		<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet" />
	
		<!-- menu.js -->
		<script type="text/javascript" src='../js/easyui/jquery.easyui.menu.js'></script>
		<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">
		<!-- 遮罩 -->
		<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
		<!-- 下拉菜单 -->
<!-- <link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> --> 

		<!-- select2选项卡 -->
		<link rel="stylesheet" href="../jquery-select2-custom.css">
		<link rel="stylesheet" href="../js/select2-4.0.0-rc.2/dist/css/select2.css">
		<script type="text/javascript" src="../jquery-select2-custom.js"></script>

		<link rel="stylesheet" type="text/css" href="../buttons.css"  />
		<style type="text/css">
			.set {
				color: #0066FF;
				font-weight: bold;
				font-size: 12px;
				font-family: Arial, Helvetica, sans-serif;
			}
			.title {
				font-size: 12px;
				color: green;
				font-weight: blod;
			}
			a.common{width:16px;height:16px;display:block;float:left;border: 1px solid silver ; margin-left: 3px;}
			a.up{background-image: url('../imgs/arrow_up.png');}
			a.down{background-image: url('../imgs/arrow_down.png');}
			td .box{float:left;padding:4px;}
			td .left{text-align: right;}
			td .right{text-align: left;}
			td .leftWidth11{width:120px;}
			td .leftWidth22{width:90px;}
			td .rigthWidth11{width:170px;}
			td .rigthWidth22{width:90px;}
			td .even .box{border-bottom:1px solid #ddd;border-top:1px solid #ddd;}
			td .clear{clear:both}
			td .leftWidth1{width:100px;}
			td .leftWidth2{width:60px;}
			td .rigthWidth1{width:110px;}
			td .rigthWidth2{width:70px;}
			
		</style>
		<%
		HttpSession sess = request.getSession(true);
		Map<String,Object> loginUser = (Map<String,Object>) sess.getAttribute("adminSesion");
		int corporationType = (Integer)loginUser.get("corporationType");
		int customerId = 0;
		//存放登录用户类型，表示用户是admin,是Customer,还是Title或者其它
		String userType = "";
		if(corporationType == 1){
			customerId = (Integer)loginUser.get("corporationId");
			userType = "Customer";
		}else if(Boolean.TRUE.equals(loginUser.get("administrator"))){
			userType = "Admin";
		}
		String cmd = StringUtil.getString(request,"cmd");
		long main_id = StringUtil.getLong(request,"lp_type_id");
		long pc_id = StringUtil.getLong(request,"pc_id");
		int ship_to_value = StringUtil.getInt(request, "ship_to_value");
		int title_value = StringUtil.getInt(request, "title_value");
		
		
		
		int customer_value = StringUtil.getInt(request, "customer_value");
		if(userType.equals("Admin")){
			customer_value = StringUtil.getInt(request, "customer_value");
		}else if(userType.equals("Customer")){
			customer_value = customerId;
		}
		
		DBRow[] title_shiptos= null;
		
		title_shiptos = clpTypeMgrZr.findLpTliteAndShipTo(main_id,title_value,customer_value,ship_to_value) ;
		 
		DBRow lpRow = clpTypeMgrZr.selectContainerClpById(main_id);

		
		//DBRow[] titles = proprietaryMgrZyj.findTitleByAdidAndProduct(0, pc_id, request);
		DBRow[] shipTos = shipToMgrZJ.getAllShipTo();	
		DBRow from_ship_to_name=	shipToMgrZJ.getShipToById(ship_to_value);
		DBRow from_title_name =	proprietaryMgrZyj.findProprietaryByTitleId(title_value) ;

		//DBRow[] clptitle = clpTypeMgrZr.findClpTitle(main_id,title_value,0);
	  //	DBRow[] clpshipto = clpTypeMgrZr.findClpShipTo(main_id,0,ship_to_value);
		
		
		
		
		
		
		
		String deleteCLType = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/DeleteLpTitleAndShipToAction.action";
		String lockChangeAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/LockchangeBoxTypeAction.action";
		String addClpBoxSkuPsTypeAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/AddLpTitleShipToAction.action";
		ProductCustomerSerivce productCustomerSerivce = (ProductCustomerSerivce)MvcUtil.getBeanFromContainer("productCustomerSerivce");
		com.cwc.app.floor.api.zyj.service.CustomerService customerService = (com.cwc.app.floor.api.zyj.service.CustomerService)MvcUtil.getBeanFromContainer("customerService");
		 List<Customer> customers = null;
	     Object obj = loginUser.get("administrator");
	     List<Title> titles = null;
	     if(((Boolean)loginUser.get("administrator")).equals(Boolean.TRUE)){
			 if(title_value > 0){
		   		   customers = productCustomerSerivce.getCustomers((int)pc_id, title_value);
			  }else{
					customers = productCustomerSerivce.getCustomers((int)pc_id);
			  }	
			  titles = productCustomerSerivce.getTitles((int)pc_id);
	     }else{
	    	  Customer customer = customerService.getCustomer(customerId);
	    	  customers = new ArrayList<Customer>();
	    	  if(customer != null){
	    		  customers.add(customer);
	    	  }
	    	  titles = productCustomerSerivce.getTitles((int)pc_id, customerId);
	     }	
		%>

		<script type="text/javascript">
		var customerId = "<%=customerId %>";
		jQuery(function($){
			$("#tabs").tabs({
				cache: true,
				spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
				cookie: { expires: 30000 } ,
			});
		    $("#tabs").tabs("select",0);  //默认选中第一个
		    /* $(function(){
		 		$(".chzn-select").chosen({no_results_text: "no this options:"});
		 	}); */
		 	
		    $("#sku_lp_title_id").select2({
				placeholder: "Title..."
		       ,allowClear: true
		     });
		 	
		    $("#sku_lp_customer_id").select2({
				placeholder: "Customer..."
		       ,allowClear: true
		     });
		    
		    $("#sku_lp_to_ps_id").select2({
				placeholder: "Ship To..."
		       ,allowClear: true
		     });
		    
		});
		
		 function addNew(){
			 var title_id = $("#to_ps_id").val();
			 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/title_shipto_add.html?main_id="+<%=main_id%>+"&pc_id="+<%=pc_id%>; 
			 $.artDialog.open(uri , {title:" Add CLP Type",width:'650px',height:'480px', lock: true,opacity: 0.3,fixed: true});
		 }
		 
		
		 function del(id){
			 $.artDialog({
				    content: 'Are You Sure Delete ?',
				    icon: 'question',
				    width: 200,
				    height: 100,
				    lock: true,
				    opacity: 0.3,
				    title:'Delete',
				    okVal: 'Confirm',
				    ok: function () {
						$.ajax({
							url:'<%= deleteCLType%>' + "?lp_title_ship_id="+id,
							dataType:'json',
							type:'post',
							success:function(data){
							 	if(data != null && data.flag === "success"){
					    		 	 window.location.reload();
							 	}else{
							 	   showMessage("System error, try again later.","error");
								}
							 },
							 error:function(){
							     showMessage("System error, try again later","error");
							 }
						})
				    },
				    cancelVal: 'Cancel',
				    cancel: function(){
					}
				});	 
		 }
		 
		 function refreshWindow(){
			window.location.reload();
		 }
		
	

		function searchTitle()
		{
			$("#title_value").val($("#sku_lp_title_id").val());
			$("#customer_value").val($("#sku_lp_customer_id").val());
			$("#toolForm").submit();
		}

		function searchCustomer()
		{
			$("#title_value").val($("#sku_lp_title_id").val());
			$("#customer_value").val($("#sku_lp_customer_id").val());
			$("#toolForm").submit();
		}
		
		function searchShipTo()
		{
			$("#ship_to_value").val($("#sku_lp_to_ps_id").val());
			$("#toolForm").submit();
		}
		
		function closeWin(){
			$.artDialog && $.artDialog.close();
			//parent.location.reload();
	 	}
		
		function windowClose(){
			$.artDialog && $.artDialog.close();
			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
		};
		
 	function addClpType(_art){
 		
		if(validate()){
			var from_ship_to_value = $("#ship_to_value").val();
			var from_title_value = $("#title_value").val();
			var from_title_name = $("#from_title_name").val();
			var from_ship_to_name = $("#from_ship_to_name").val();
			var titleShiptoExist = $("#titleShiptoExist").val();
			if(1!=titleShiptoExist)
			{
				if(parseInt(customerId) > 0){
					$("#sku_lp_customer_id").val(customerId);
				}
			   	ajaxAddClpType(_art);
			}
			else
			{
				showMessage("Exist.","alert");
			}
		}
 	}
 	function checkInt(value){
 	    var reg = /^[1-9]\d*$/;
 	    return reg.test(value) ;
 	}
 	function ajaxAddClpType(_art){
 	   $.ajax({
		url:'<%=addClpBoxSkuPsTypeAction%>',
		data:$("#addClpTypeForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
			
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
		  if(data != null && data.flag){
			  
			  if(data.flag === "error" ){
							
						showMessage("The title/Ship To exist.","error");
				
			  }
			  if(data.flag === "success"){
				
				  showMessage("Success  Add !","success");
				  
				  if(data.message * 1 > 0){
						refreshWindow();
					}
			  }
			  if(data.flag === "exist" ){
					
					showMessage("The title/Ship To exist.","error");
			
		  }
		  }else{
		      showMessage("System error,try it again later.","error");
		
		  }
		  $.unblockUI();
		  
		},
		error:function(){
		     showMessage("System error,try it again later.","error");
		     $.unblockUI();
		   
		}
	})
 	}
 	
 	function validate(){
		var sku_lp_to_ps_id = $("#sku_lp_to_ps_id");
		var sku_lp_title_id = $("#sku_lp_title_id");
		var sku_lp_customer_id = $("#sku_lp_customer_id");
		if(checkInt(sku_lp_to_ps_id.val()) * 1 == 0 && checkInt(sku_lp_title_id.val()) * 1 == 0 && checkInt(sku_lp_customer_id.val()) * 1 == 0){
			showMessage("Select Title Or Customer Or Ship To","alert");

			return false ;
		}

		return  true;
		
 	}
	 	
		</script>
	</head>
	<body >
	
		<div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
			 <form action="" id="toolForm" name="toolForm">
				 <input type="hidden" value="<%=main_id %>" name="lp_type_id"/>
				  <input type="hidden" name="pc_id" value='<%=pc_id %>'/>
			 	 <input type="hidden" value="query" name="cmd"/>  
			 	 <input type="hidden" value="<%=title_value %>" name="title_value" id="title_value"/>
			 	  <input type="hidden" value="<%=customer_value %>" name="customer_value" id="customer_value"/>
			 	 <input type="hidden" value="<%=ship_to_value %>" name="ship_to_value" id="ship_to_value"/>
			</form>
	 <form name="addClpTypeForm" id="addClpTypeForm" method="post" action=""> 
			 <input type="hidden" value="<%=main_id %>" name="post_lp_type_id"/>
			 <input type="hidden" name="post_pc_id" value='<%=pc_id %>'/>
			 
		 	 <input type="hidden" value="<%=title_value %>" name="from_title_value" id="from_title_value"/>
			 <input type="hidden" value="<%=ship_to_value %>" name="from_ship_to_value" id="from_ship_to_value"/>
			<% if(from_title_name!=null){ %>
			 <input type="hidden" value="<%=from_title_name.getString("title_name")%>" name="from_title_name" id="from_title_name"/>
		<%	 }else{ %>
			<input type="hidden" value="0" name="from_title_name" id="from_title_name"/>
		<%}
			if(from_ship_to_name!=null){%>
			 <input type="hidden" value="<%=from_ship_to_name.getString("SHIP_TO_NAME")%>" name="from_ship_to_name" id="from_ship_to_name"/>
			 <%}else{ %>
			 <input type="hidden" value="0" name="from_ship_to_name" id="from_ship_to_name"/>
			 <%} %>
				 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="line-height:25px;background-color: #E5E5E5;border: 1px solid #BBB;border-radius: 5px;">	
				<tr>
				   <td width="10%" align="right">CLP Type : </td>
			       <td width="40%" colspan="3" align="left"><%=lpRow.getString("LP_NAME") %> </td>
				</tr>
				<tr>
				   <td width="10%" align="right">CLP Packaging Type : </td>
			       <td width="40%" colspan="3" align="left"><%=lpRow.getString("type_name") %> </td>
				</tr>
			</table>
		</div>
		<br/>
		
		<table width="99%">
			<tr>
			  <td width="30%">
			     <div class="side-by-side clearfix">
					<select id="sku_lp_title_id" name="sku_lp_title_id" onchange="searchTitle()" data-placeholder="Title..." tabindex="1" style="width:100%">
						<option value=""></option>
							<%if(titles != null && titles.size() > 0){
								String select_title = request.getParameter("title_value");
								for(Title title : titles){ 
							%>	
						  	<option <%=((select_title == null && titles.size() == 1) || title.getId() == title_value)?"selected":"" %> value='<%=title.getId() %>'><%=title.getName() %></option>
							<%} 
								}%>
							</select>
				</div>
			 </td>

			  <td width="30%" style="padding-left: 6px;">
			     <div class="side-by-side clearfix">

					<select id="sku_lp_customer_id" name="customer_id" onchange="searchCustomer()" data-placeholder="Customer..." tabindex="1" style="width:100%">
						<option value=""></option>
							<%if(customers != null && customers.size() > 0){
								String select_customer = request.getParameter("customer_value");
								for(Customer item : customers){ 
							%>	
						  	<option <%=((select_customer == null && customers.size() == 1) || item.getId() == customer_value)?"selected":"" %> value='<%=item.getId() %>'><%=item.getName() %></option>
							<%} 
								}%>
					</select>
				</div>
			 </td>
			            
			 <td width="30%" style="padding-left: 6px;">
			    <div class="side-by-side clearfix">
				<select id="sku_lp_to_ps_id" name="sku_lp_to_ps_id" onchange="searchShipTo()" class="chzn-select" data-placeholder="Ship To..." tabindex="1"  style="width:100%">
					<option value="">Ship To...</option>
						<%if(shipTos != null && shipTos.length > 0){
							for(DBRow shipTo : shipTos){
						 %>	
							<option <%=(shipTo.get("ship_to_id",0l) == ship_to_value)?"selected":"" %> value='<%=shipTo.get("ship_to_id",0l) %>'><%=shipTo.getString("ship_to_name") %></option>
						<%} 
						}%>
				</select>
				</div>
			  </td>
			 <td width="10%" align="left" style="padding-left: 6px;">
				<a value="Add" class="buttons" onclick="addClpType()" ><i class="icon-plus"></i>&nbsp;Add</a>
			</td>
			</tr>
		</table>
			
<table width="99%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="true" isBottom="true" class="zebraTable" style="margin:3px;margin-top:5px; border-collapse:collapse;margin-bottom: 50px;">
	<tr>
		<th width="30%" style="vertical-align: middle; text-align: center; border-width:1px;"class="right-title">Title</th>
		<th width="30%" style="vertical-align: middle; text-align: center;"class="right-title">Customer</th>
		<th width="30%" style="vertical-align: middle; text-align: center;"class="right-title">Ship To</th>
		<th width="10%"  style="vertical-align: middle; text-align: center;"class="right-title">Operation</th>
	</tr>
<%
int titleShiptoExist = 0;
if(title_shiptos.length>0){ 
	
	for(int i = 0; i < title_shiptos.length; i ++){

		DBRow title_shipto = title_shiptos[i];
	
		long ship_to_id = title_shipto.get("ship_to_id",0l);
		long title_id = title_shipto.get("title_id",0);
		int customer_id = title_shipto.get("customer_id",0);
		 
		long delid = title_shipto.get("lp_title_ship_id",0l);
		DBRow shipToName = shipToMgrZJ.getShipToById(ship_to_id);
		DBRow title_name =  proprietaryMgrZyj.findProprietaryByTitleId(title_id);
		
		Customer customer = null;
		
		 if(customer_id > 0){
			 customer = customerService.getCustomer(customer_id);
		 }
		 if(ship_to_id==ship_to_value&&title_id==title_value && customer_id==customer_value){ 
			
			 titleShiptoExist = 1;
		 }
	%>

	
	
<tr height="40px">
	
	<td style="vertical-align: middle; text-align: center;">
		<%if(title_id!=0){ %>
			<%=title_name.get("title_name") %>
		<%}else{ %>
			*
		<%}%>
	</td>
	
	<td style="vertical-align: middle; text-align: center;">
		<%if(customer != null){ %>
			<%=customer.getName() %>
		<%}else{ %>
			*
		<%}%>
	</td>
	
	<td style="vertical-align: middle; text-align: center;">
		<%if(ship_to_id!=0){ %>
			<%=shipToName.get("SHIP_TO_NAME") %>
		<%}else{ %>
			*
		<%}%>
	</td>				
		
	<td><div class="buttons-group minor-group"><a style="float: right;margin-right: 10%; " class="buttons" value="Delete" onclick="del('<%=delid %>');" >Delete</a></div></td>
</tr>	

<%}%>

<%}else{%>
	<tr>
    		 	<td colspan="4" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Datas</td>
    		</tr>
<% } %>
<input type="hidden" value="<%=titleShiptoExist %>" name="titleShiptoExist" id="titleShiptoExist"/>
</table>
		
		
		
		<table width="100%" height="10%" border="0" cellpadding="0" cellspacing="0" style="position:fixed;bottom:0;margin:3px;">
	<tr>
    <td width="60%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="40%" align="right" class="win-bottom-line">
		<a name="Submit2" value="Close" class="buttons  primary big" onClick="windowClose();" style="margin-right:30px;">Close</a> 
	</td>
  	</tr>
	</table>
	<script>onLoadInitZebraTable();</script>
	</form>
		<script>
		$.blockUI.defaults = {
				css: { 
					padding:        '8px',
					margin:         0,
					width:          '170px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #999999',
					backgroundColor:'#eeeeee',
					'-webkit-border-radius': '10px',
					'-moz-border-radius':    '10px',
					'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
					'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
				},
				//设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.6' 
				},
				baseZ: 99999, 
				centerX: true,
				centerY: true, 
				fadeOut:  1000,
				showOverlay: true
			};

		</script>
	</body>
</html>
