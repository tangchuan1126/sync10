<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改SKU容器类型</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.STYLE2{font-weight:bold;color:blue;}
</style>
<%
 	String updateBoxTypeAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/UpdateIlpSkuTypeAction.action";
 
 	long box_id = StringUtil.getLong(request,"ibt_id");
	DBRow  ilp = boxTypeMgrZr.findIlpByIlpId(box_id);

 	String productName=StringUtil.getString(request,"productName");
 	
	  
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	DBRow[] containers = boxTypeMgrZr.getBoxContainerType(containerTypeKey.ILP);
	
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
jQuery(function($){
	  $("#container_type_id option[value='"+'<%=ilp.get("ibt_lp_type",0l) %>'+"']").attr("selected",true);
})
 
function checkInt(value){
    var reg = /^[1-9]\d*$/;
    return reg.test(value) ;
}
function checkFloat(num)
{
    var reg = new RegExp("^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9][1-9]*[0-9]*))$"); //匹配正浮点数
    if( !reg.test(num) )
    {
        return false;
    }
    return true;
}
 
function addBoxType(){
 
 	if(validateFrom()){
   		 ajaxAddBoxType();
	}
}

function validateFrom(){
    var container_type_id  = $("#container_type_id");
    var box_total_length = $("#ibt_total_length");
    var box_total_width = $("#ibt_total_width");
    var box_total_height = $("#ibt_total_height");
    var box_length = $("#ibt_length");
    var box_width = $("#ibt_width");
    var box_height = $("#ibt_height");
    var box_weight = $("#ibt_weight");
    if(container_type_id.val() * 1 == 0){
	    showMessage("请选择容器类型.","alert");
		container_type_id.focus();
		return false ;
	}
  
 
	if(!checkInt($.trim(box_total_length.val()))){
	    showMessage("长数量.输入不合法..比如(1)","alert");
	    box_total_length.focus();
		return false;
	}
	if(!checkInt($.trim(box_total_width.val()))){
	    showMessage("宽数量.输入不合法..比如(1)","alert");
	    box_total_width.focus();
		return false;
	}
	if(!checkInt($.trim(box_total_height.val()))){
	    showMessage("高数量.输入不合法..比如(1)","alert");
	    box_total_height.focus();
		return false;
	}
	if(!checkFloat($.trim(box_length.val()))){
	    showMessage("盒子长度.输入不合法..比如(100.00)","alert");
	    box_length.focus();
		return false;
	}



	if(!checkFloat($.trim(box_width.val()))){
	    showMessage( "盒子宽度..比如(100.00)","alert");
	    box_width.focus();
		return false;
	}
	if(!checkFloat($.trim(box_height.val()))){
	    showMessage("盒子高度.输入不合法..比如(100.00)","alert");
	    box_height.focus();
		return false;
	}
	if(!checkFloat($.trim(box_weight.val()))){
	    showMessage("盒子重量.输入不合法..比如(100.00)","alert");
	    box_weight.focus();
		return false;
	}
	return true ;
}
 
 
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function closeWin()
	{
		$.artDialog.close();
	}
	function updateBoxType(){
	    if(validateFrom()){
		    ajaxUpate();
		}
	}
	function ajaxUpate(){
	    $.ajax({
		url:'<%=updateBoxTypeAction%>',
		data:$("#addBoxTypeForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
		  $.unblockUI();
		  if(data != null){
				if(data.flag == "dataFormat"){
				    showMessage("数据格式错误.","alert");
				    $("#box_total_length").focus();
				}
				if(data.flag === "productNotFound"){
				    showMessage("系统中无此商品.","alert");
				    $("#box_product_name").html("");
				}
				if(data.flag == "success"){
				    setTimeout("windowClose()", 100);
				}
				if(data.flag == "error"){
				    showMessage("系统错误,修改失败.","alert");
				}
		  }else{
		      showMessage("系统错误,修改失败.","alert");
		  }
		},
		error:function(){
		    $.unblockUI();
		}
	})
	}
	function setContainerValue(){
		var container_type_id = $("#container_type_id");
		if(container_type_id.val() * 1 != 0){
			var option = $("option:selected",container_type_id);
			
		    $("#ibt_length").val(option.attr("clength"));
		    $("#ibt_width").val(option.attr("cwidth"));
		    $("#ibt_height").val(option.attr("cheight"));
		    $("#ibt_weight").val(option.attr("cweight"));
		}else{
		    $("#ibt_length").val("0");
		    $("#ibt_width").val("0");
		    $("#ibt_height").val("0");
		    $("#ibt_weight").val("0");
		}
		
	}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 
<form name="addBoxTypeForm" id="addBoxTypeForm" method="post" action="">
<input  type="hidden" value='<%=ilp.get("ibt_id",0l) %>' name="box_type_id"/>
 <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	
<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:90%;">
  <legend style="font-size:15px;font-weight:normal;color:#999999;">修改盒子类型</legend>
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
 			<tr>
				<td align="right" valign="middle" class="STYLE2" width="110px" >商品:</td>
				<td align="left" valign="middle" > 
					<span style="color: #f60;font-weight:bold;"><%=productName %></span>
 			    </td>
			</tr>
 			<tr>
			  	 <td align="right" valign="middle" class="STYLE2" width="90px">BLP容器类型:</td>
			     <td align="left" valign="middle">
			     	<select id="container_type_id" name="container_type_id" onchange="setContainerValue()">
			     		<option value="0">请选择BLP容器类型</option>
			     		<%
			     			if(containers != null && containers.length > 0){
			     				for(DBRow row : containers){
			     					%>
			     						<option value="<%=row.get("type_id",0l) %>" 
			     								clength="<%=row.get("length",0.0f) %>"
			     								cwidth ="<%=row.get("width",0.0f) %>" 
			     								cheight="<%=row.get("height",0.0f)%>"
			     								cweight="<%=row.get("weight",0.0f) %>"
			     						 >
			     							<%=row.getString("type_name") %>
			     						</option>
			     					<% 	
			     				}
			     			}
			     		%>
			     	</select> 
 			     </td>
			</tr>
		</table>
		
		 <div style="border-bottom:1px dashed  silver ; text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="STYLE2" >
		 	商品数量：(商品在该<span style='color:#f60;'>类型盒子</span>上,长宽高各自能放几个.)
		 </div>
		 
		  <table width="100%" border="0" cellspacing="3" cellpadding="2">
   			 <tr>
				 <td align="right" valign="middle" class="STYLE2" width="110px">长数量:</td>
			     <td align="left" valign="middle" >
			   	 		<input name="ibt_total_length" id="ibt_total_length" type="text" value='<%= ilp.get("ibt_total_length",1) %>' width="20px">
			   	 </td>
			 </tr>	
			  <tr>
				 <td align="right" valign="middle" class="STYLE2" >宽数量:</td>
			     <td align="left" valign="middle" >
			   	 		<input name="ibt_total_width" id="ibt_total_width" type="text" value='<%= ilp.get("ibt_total_width",1) %>'  width="20px">
			   	 </td>
			 </tr>	
			  <tr>
				 <td align="right" valign="middle" class="STYLE2" >高数量:</td>
			     <td align="left" valign="middle" >
			   	 		<input name="ibt_total_height" id="ibt_total_height" type="text" value='<%= ilp.get("ibt_total_height",1) %>' width="20px">
			   	 </td>
			 </tr>	
   		 </table>
    	<div style="border-bottom:1px dashed  silver ;  text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="STYLE2" >
    	盒子类型属性：(盒子类型基本属性.)
    	</div>
   		 
			<table width="100%" border="0" cellspacing="3" cellpadding="2">
				
			<tr>
				<td align="right" valign="middle" class="STYLE2" width="110px">长(mm):</td>
			    <td align="left" valign="middle" >
			    <input name="ibt_length" id="ibt_length" type="text" value='<%= ilp.get("ibt_length",0.0d) %>' size="25px">
			    </td>
			  </tr>
			<tr>
				<td align="right" valign="middle" class="STYLE2" >宽(mm):</td>
			    <td align="left" valign="middle" >
			    <input name="ibt_width" id="ibt_width" type="text" value='<%= ilp.get("ibt_width",0.0d) %>' size="25px">
			    </td>
			  </tr>
			<tr>
				<td align="right" valign="middle" class="STYLE2" >高(mm):</td>
			    <td align="left" valign="middle" >
			    <input name="ibt_height" id="ibt_height" type="text" value='<%= ilp.get("ibt_height",0.0d) %>' size="25px">
			    </td>
			  </tr>
			<tr>
				<td align="right" valign="middle" class="STYLE2" >容器重量(kg):</td>
			    <td align="left" valign="middle" >
			    <input name="ibt_weight" id="ibt_weight" type="text" value='<%= ilp.get("ibt_weight",0.0d) %>' size="25px">
			    </td>
			  </tr>
   		 </table>
 </fieldset>
</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
      <input type="button" name="Submit2" value="修改类型" class="normal-green" onClick="updateBoxType();">
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table>
</form>	
</body>
</html>

