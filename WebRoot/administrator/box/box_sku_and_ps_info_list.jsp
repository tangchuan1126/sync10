<%@page import="com.fr.base.core.json.JSONObject"%>
<%@page import="com.fr.base.core.json.JSONArray,com.cwc.app.floor.api.zyj.service.AdminService"%>
<%@page import="com.cwc.app.util.DBRowUtils,java.util.Map,com.cwc.app.floor.api.zyj.service.ProductCustomerSerivce,com.cwc.app.floor.api.zyj.model.*"%>

<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList,org.apache.commons.lang.StringUtils" %>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<jsp:useBean id="yesOrNotKey" class="com.cwc.app.key.YesOrNotKey"></jsp:useBean>
<html>
<head>
<title>CLP Type</title>

<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jstree/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/Sync10-ui/bower_components/jquery.browser/dist/jquery.browser.min.js"></script>

<!-- select2选项卡 -->
<link rel="stylesheet" href="../jquery-select2-custom.css">
<link rel="stylesheet" href="../js/select2-4.0.0-rc.2/dist/css/select2.css">
<script type="text/javascript" src="../jquery-select2-custom.js"></script>

<!-- <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script> -->
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript" ></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="/Sync10-ui/lib/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉菜单 -->
<!-- <link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> -->

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<!-- <script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> -->

<script type="text/javascript" src="../js/jstree/dist/jstree.min.js"></script>
<link type="text/css" rel="stylesheet" href="../js/jstree/dist/themes/default/style.min.css" />

<%
	HttpSession sess = request.getSession(true);
	Map<String,Object> loginUser = (Map<String,Object>) sess.getAttribute("adminSesion");
	int corporationType = (Integer)loginUser.get("corporationType");
	int customerId = 0;
	int titleId = 0;
	//存放登录用户类型，表示用户是admin,是Customer,还是Title或者其它
	String userType = "";
	if(corporationType == 1){
		customerId = (Integer)loginUser.get("corporationId");
		userType = "Customer";
	}else if(Boolean.TRUE.equals(loginUser.get("administrator"))){
		userType = "Admin";
	}
	

   String cmd = StringUtil.getString(request,"cmd");
   long pc_id=StringUtil.getLong(request,"pc_id");
   int basic_container_type = StringUtil.getInt(request,"basic_container_type");
   int title_value = StringUtil.getInt(request, "title_value");
   int ship_to_value = StringUtil.getInt(request, "ship_to_value");
   int customer_value = StringUtil.getInt(request, "customer_value");
   int active = StringUtil.getInt(request, "active");
   
   DBRow[] lpRow=null;
   
   if(cmd.trim().equals("query")){
	   if(customerId > 0){
		   lpRow = boxTypeMgrZr.getBoxTypes(pc_id,basic_container_type,title_value,ship_to_value,customerId, active);
	   }else{
		   lpRow = boxTypeMgrZr.getBoxTypes(pc_id,basic_container_type,title_value,ship_to_value,customer_value, active);
	   }
	   
   }else{
	   lpRow = boxTypeMgrZr.getBoxTypes(pc_id,0,0,0,customerId, active);
   }
   
   // 商品相关的信息
   DBRow productInfo = productMgrZyj.findProductInfosByPcid(pc_id);
   // CLP信息
   DBRow[] containers = boxTypeMgrZr.selectContainerTypeAddAllSel(ContainerTypeKey.CLP);
   //titles
   //DBRow[] titles = proprietaryMgrZyj.findTitleByAdidAndProduct(0, pc_id, request);
   

   
   ProductCustomerSerivce productCustomerSerivce = (ProductCustomerSerivce)MvcUtil.getBeanFromContainer("productCustomerSerivce");
   List<Customer> customers = null;
   if(userType.equals("Admin")){
	   if(title_value > 0){
		   customers = productCustomerSerivce.getCustomers((int)pc_id, title_value);
	   }else{
		   customers = productCustomerSerivce.getCustomers((int)pc_id);
	   }
   }else{
	   com.cwc.app.floor.api.zyj.service.CustomerService customerService = (com.cwc.app.floor.api.zyj.service.CustomerService)MvcUtil.getBeanFromContainer("customerService");
	   customers = new ArrayList<Customer>();
	   Customer customer = customerService.getCustomer(customerId);
	   if(customer != null){
		   customers.add( customer ); 
	   }
   }
   

   
   List<Title> titles = productCustomerSerivce.getTitles((int)pc_id);
   //客户卖场
   DBRow[] shipTos = shipToMgrZJ.getAllShipTo();
   
   //启用状态
   //DBRow allRow = new DBRow(); allRow.add("active", 0L); allRow.add("active_name", "All");
   DBRow activeRow = new DBRow(); activeRow.add("active", 1L); activeRow.add("active_name", "Active");
   DBRow inactiveRow = new DBRow(); inactiveRow.add("active", 2L); inactiveRow.add("active_name", "Inactive");
   DBRow[] actives = new DBRow[]{/* allRow, */ activeRow, inactiveRow};
   
   String lockChangeAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/LockchangeBoxTypeAction.action";
   


%>
<script type="text/javascript">
var userType = "<%=userType%>";
var customerId = "<%=customerId %>";
jQuery(function($){
	if(userType == "Customer"){
		$("#sku_lp_customer_id").val(customerId).attr("disabled","true");
	}
	
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
    $("#tabs").tabs("select",0);  //默认选中第一个
    /* $(function(){
 		$(".chzn-select").chosen({no_results_text: "no this options:"});
 	}); */
    
    $("#container_type_id").select2({
		placeholder: "Packaging Type..."
       ,allowClear: true
     });
    $("#sku_lp_title_id").select2({
		placeholder: "Title..."
       ,allowClear: true
     });
    $("#sku_lp_customer_id").select2({
		placeholder: "Customer..."
       ,allowClear: true
     });
    $("#sku_lp_to_ps_id").select2({
		placeholder: "Ship To..."
       ,allowClear: true
     });
    $("#sku_lp_active").select2({
		placeholder: "Is Active..."
       ,allowClear: true
     });
});
function print(container_type, type_id, is_has_sn){
	//var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch_i.html?container_type="+container_type+"&type_id="+type_id+"&is_has_sn="+is_has_sn;
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch_common.html?container_type="+container_type+"&type_id="+type_id+"&is_has_sn="+is_has_sn+"&detail_type="+'<%=ContainerTypeKey.CLP%>'+"&pcid="+'<%=pc_id%>'; 
	$.artDialog.open(url , {title: 'Batch create and print CLP',width:'700px',height:'450px', lock: true,opacity: 0.1,fixed: true});
}
function searchClp()
{
	$("#basic_container_type").val($("#container_type_id").val());
	$("#toolForm").submit();
}
function searchTitle()
{
	$("#title_value").val($("#sku_lp_title_id").val());
	$("#customer_value").val($("#sku_lp_customer_id").val());
	//loadCustomer();
	$("#toolForm").submit();
}
function searchCustomer()
{
	$("#title_value").val($("#sku_lp_title_id").val());
	$("#customer_value").val($("#sku_lp_customer_id").val());
	//loadCustomer();
	$("#toolForm").submit();
}

function searchShipTo()
{
	$("#ship_to_value").val($("#sku_lp_to_ps_id").val());
	$("#toolForm").submit();
}
function searchActive()
{
	$("#active").val($("#sku_lp_active").val());
	$("#toolForm").submit();
}

var pc_id = <%=pc_id%>;
function loadCustomer(){
	var url = "";
	var titleId = $("#title_value").val();
	if(userType == "Admin"){
		url = "/Sync10/basicdata/productCustomer/" + pc_id + "/" + titleId + "/customer";
		alert(url);
		$.ajax({
			 type : "get",
			 url  : url,
			 success : function(data){
				 if(data && data.length > 0){
					 alert("1");
					 $("#sku_lp_customer_id option[value!='0']").remove();
					 for(var i=0; i<data.length;i++){
						 $("#sku_lp_customer_id").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
					 }
					 
					 $("#sku_lp_customer_id_chzn").empty();
					 
					 $("#sku_lp_customer_id").chosen({no_results_text: "no this options:"});
				 }
			 }
		});		
	}
	
}
</script>
<style type="text/css">
td .box{float:left;padding:4px;}
td .left{text-align: right;line-height: 22px; }
td .right{text-align: left;line-height: 22px; }
td .leftWidth1{width:110px;line-height: 22px; }
td .even .box{border-bottom:1px solid #ddd;border-top:1px solid #ddd;}
td .clear{clear:both}
.width30{width:90px;word-break: normal;}
.width60 span{
width:170px;
display: block;
word-break: break-all;
white-space: pre-wrap;}
.width60 a span{
width: 170px;
display: block;
word-break: break-all;
white-space: pre-wrap;
}
.pr0{padding-right:0;}
.innerTable td{
border-bottom:none;
padding-left:0;
}
.zebraTable td{padding-left:0px;}
.long-long-button-self {
	background-attachment: fixed;
	background: url(../imgs/button_long_long.gif);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 130px;
	color: #000000;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	float:right;
	margin-right:3%;
	margin-top:3px;
}
.content_clp_button {
    /*background-image: url("../imgs/linebg.jpg");*/
    height: 24px;
    text-align: right;
    vertical-align: top;
    background-color: #f1f1f1;
}
.content_clp_button td{
	border-bottom: 0;
	padding-right: 0;
}
.content_clp_button.over td{
	/*background-image: url("../imgs/linebg.jpg") !important;*/
}
input.lighter[type="button"], button.lighter {
    background: url("../imgs/button_long_long.gif") no-repeat scroll center center rgba(0, 0, 0, 0);
    border: 0 solid;
    color: #000000;
    cursor: pointer;
    height: 20px;
    width: 130px;
}
.lighter1 {
    background: url("../imgs/button_short.gif") no-repeat scroll center center rgba(0, 0, 0, 0);
    border: 0 solid;
    color: #000000;
    cursor: pointer;
    height: 20px;
    width: 70px;
}
.content_clp_text_td {
    padding-bottom: 8px;
    padding-left: 5px;
    padding-top: 5px;
    width: 20%;
}

.jstree-default .jstree-clicked{

	background: none;
	border-radius: 2px;
	box-shadow: inset 0 0 1px #999;
}

.innerTable td{
border-bottom:none;
padding-left:0;
border:1px solid #DDD;
}

.chzn-container{
	min-width:183px;
}
.chzn-drop{
	min-width: 181px;
}
.chzn-select{
	
}
</style>
</head>
<body>
		 <div id="tool" style="width:99%;margin-left:2px;margin-top:10px;">
			 <form action="" id="toolForm" name="toolForm">
			 	 <input type="hidden" value='<%=pc_id %>' name="pc_id" id="pc_id" />
			 	 <input type="hidden" value="query" name="cmd"/>
			 	 <input type="hidden" value="<%=basic_container_type %>" name="basic_container_type" id="basic_container_type"/>
			 	 <input type="hidden" value="<%=title_value %>" name="title_value" id="title_value"/>
			 	 <input type="hidden" value="<%=customer_value %>" name="customer_value" id="customer_value"/>
			 	 <input type="hidden" value="<%=ship_to_value %>" name="ship_to_value" id="ship_to_value"/>
			 	 <input type="hidden" value="<%=active %>" name="active" id="active"/>
			 </form>
			 <div>
			   <%String length_uom = lengthUOMKey.getLengthUOMKey(productInfo.get("length_uom", 0));%>
	      	   <%String weight_uom = weightUOMKey.getWeightUOMKey(productInfo.get("weight_uom", 0));%>
			   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" 
			   style="line-height:25px;background-color: #E5E5E5;border: 1px solid #BBB;border-radius: 5px;">
			     <tr>
			       <td width="10%" align="right">SKU :</td>
			       <td width="40%" colspan="3" align="left"><%="&nbsp;"+productInfo.getString("p_name")%></td>
			 	   <td width="10%" align="right">Category :</td>
			 	   <td width="40%" colspan="3" align="left"><%="&nbsp;"+productInfo.getString("title")%></td>
			 	 </tr>
			 	 <tr>
			 	   <td width="10%" align="right">Length :</td>
			 	   <td width="15%" align="left"><%="&nbsp;"+productInfo.get("length",0f)+"&nbsp;"+length_uom %></td>
			 	   <td width="10%" align="right">Width :</td>
			 	   <td width="15%" align="left"><%="&nbsp;"+productInfo.get("width",0f)+"&nbsp;"+length_uom %></td>
			 	   <td width="10%" align="right">Height :</td>
			 	   <td width="15%" align="left"><%="&nbsp;"+productInfo.get("heigth",0f)+"&nbsp;"+length_uom %></td>
			 	   <td width="10%" align="right">Weight :</td>
			 	   <td width="15%" align="left"><%="&nbsp;"+productInfo.get("weight",0f)+"&nbsp;"+weight_uom %></td>
			 	 </tr>
			   </table>
			 </div>
			 	<div style="margin-top:10px;">
			 	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				      <tr>
				        <td width="18%">
				        	<div class="side-by-side clearfix">
					 	     <select id="container_type_id" onchange="searchClp()" data-placeholder="Packaging Type..." tabindex="1"  style="width:100%">
					 	     	<option value=""></option>
					 	     	<%for(int a=0;a<containers.length;a++){ %>
							    <option <%=(containers[a].get("type_id",0l) == basic_container_type)?"selected":"" %> value="<%=containers[a].get("type_id",0l)%>" ><%=containers[a].getString("type_name")%></option>
							    <%}%>
							 </select>
							 </div>
				        </td>
				        <td width="18%" style="padding-left: 9px;">
				        	<div class="side-by-side clearfix">
							<select id="sku_lp_title_id" onchange="searchTitle()" data-placeholder="Title..." tabindex="1"  style="width:100%">
								<option value=""></option>
								<%if(titles != null && titles.size() > 0){
									String select_title = request.getParameter("title_value");
									for(Title title : titles){ 
								%>	
							  		<option <%=((select_title ==null && titles.size() == 1) || title.getId() == title_value)?"selected":"" %> value='<%=title.getId() %>'><%=title.getName() %></option>
								<%} 
								}%>
							</select>
							</div>
				        </td>
				        <td width="18%" style="padding-left: 9px;">
				        	<div class="side-by-side clearfix">
							<select id="sku_lp_customer_id" onchange="searchCustomer()" data-placeholder="Customer..." tabindex="1"  style="width:100%">
								<option value=""></option>
								<%if(customers != null && customers.size() > 0){
									String select_customer = request.getParameter("customer_value");
									for(Customer customer : customers){ 
								%>	
							  		<option <%=((select_customer == null && customers.size() == 1) || customer.getId() == customer_value)?"selected":"" %> value='<%=customer.getId() %>'><%=customer.getName() %></option>
								<%} 
								}%>
							</select>
							</div>
				        </td>	

				        <td width="18%" style="padding-left: 9px;">
				        	<div class="side-by-side clearfix">
					 	    <select id="sku_lp_to_ps_id" onchange="searchShipTo()" data-placeholder="Ship To..." tabindex="1"  style="width:100%">
							<option value=""></option>
							<%if(shipTos != null && shipTos.length > 0){
								for(DBRow shipTo : shipTos){
							 %>	
							  		<option <%=(shipTo.get("ship_to_id",0l) == ship_to_value)?"selected":"" %> value='<%=shipTo.get("ship_to_id",0l) %>'><%=shipTo.getString("ship_to_name") %></option>
							<%} 
							}%>
							</select>
							</div>
				        </td>
				        <td width="18%" style="padding-left: 9px;">
				        	<div class="side-by-side clearfix">
					 	    <select id="sku_lp_active" onchange="searchActive()" data-placeholder="Is Active..." tabindex="1"  style="width:100%">
							<option value=""></option>
							<%if(actives != null && actives.length > 0){
								for(DBRow activeX : actives){
							 %>	
							  		<option <%=(activeX.get("active",0l) == active)?"selected":"" %> value='<%=activeX.get("active",-1l) %>'><%=activeX.getString("active_name") %></option>
							<%} 
							}%>
							</select>
							</div>
				        </td>
				        <td align="left" style="padding-left: 9px;">
				       
				        	<a value="Add CLP Type" class="buttons" onclick="addClp()"/><i class="icon-plus"></i>&nbsp;Add CLP Type</a>
				        
				        </td>
				      </tr>
				 </table>
			 	</div>
		 </div>
	<table width="99%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="true" isBottom="true" class="zebraTable" style="margin-left:3px;margin-top:10px;margin-bottom:50px;">
<!--  		  
		  <tr> 
 			<th width="32%" style="vertical-align: center;text-align: center;" class="right-title">CLP Type</th>
	        <th width="28%" style="vertical-align: center;text-align: center;" class="right-title">Configuration</th>
	        <th width="23%" style="vertical-align: center;text-align: center;" class="right-title">Title / Ship To</th>
	    	<th width="17%" style="vertical-align: center;text-align: center;" class="right-title">Operation</th>
	      </tr>
-->
	      <tr> 
 			<th width="36%" style="vertical-align: center;text-align: center;" class="right-title">CLP Type</th>
	        <th width="29%" style="vertical-align: center;text-align: center;" class="right-title">Configuration</th>
	        <th width="35%" style="vertical-align: center;text-align: center;" class="right-title">Title / Ship To</th>
	      </tr>
	     <tbody>
		 <%if(lpRow.length > 0){ %>
	     <%for(int i=0;i<lpRow.length;i++){ %>
	      	<tr>
	      		<td class="content_clp_text_td">
	      			<div>
		      			<div class="box left leftWidth1"><span>CLP Type:</span></div>
		      			<div class="box right width60" ><span style="color:#0066FF"><%=lpRow[i].getString("lp_name")%></span></div>
		      			<div class="clear"></div>
		      		</div>
	      			<div>
		      			<div class="box left leftWidth1"><span>Packaging Type:</span></div>
		      			<div class="box right width60">
		      			<a href="javascript:void(0)" style="color: green;" onclick="openContainerType('<%=lpRow[i].get("basic_type_id",0)%>');">
							<span style="color:#0066FF"><%=lpRow[i].getString("type_name") %></span>
						</a>
		      			</div>
		      			<div class="clear"></div>
		      		</div>
	      			<div>
		      			<div class="box left leftWidth1"><span>Serialized  Product:</span></div>
		      			<div class="box right width60"><span style="color:#0066FF"><%=yesOrNotKey.getYesOrNotKeyName(lpRow[i].get("is_has_sn",0)) %></span></div>
		      			<div class="clear"></div>
		      		</div>
		      		<div>
		      			<div class="box left leftWidth1"><span>Weight:</span></div>
		      			<div class="box right width60">
		      				<span style="color:#0066FF"><%=lpRow[i].get("total_weight",0f)%>&nbsp;&nbsp;<%=weightUOMKey.getWeightUOMKey(lpRow[i].get("weight_uom", 0))%><div style="float:right;"><%if(lpRow[i].get("active",0l)==1){}else{ %><img alt="lock" src="../imgs/lockadmin.gif"><%} %></div></span>
		      			</div>
		      			<div class="clear"></div>
		      		</div>
	      		</td>
	      		<td class="content_clp_text_td">
	      			<div>
		      			<div class="box left width30"><span>Stack:</span></div>
		      			<div class="box right width60"><span style="color:#0066FF"><%=lpRow[i].get("stack_length_qty",0l) %> * <%=lpRow[i].get("stack_width_qty",0l) %> * <%=lpRow[i].get("stack_height_qty",0l) %></span></div>
		      			<div class="clear"></div>
		      		</div>
		      		<div>
		      			<div class="box left width30"><span>Product Qty:</span></div>
		      			<div class="box right width60"><span style="color:#0066FF"><%=lpRow[i].get("inner_total_pc",0l) %></span></div>
		      			<div class="clear"></div>
		      		</div>
		      		
		      		<%if(lpRow[i].get("inner_pc_or_lp",0l) > 0){ 
		      			
		      			//DBRow innerLp = boxTypeMgrZr.findIlpByIlpId(lpRow[i].get("inner_pc_or_lp",0l));
		      			//获取树形数据
		      			DBRow cascade = boxTypeMgrZr.getCascadeClpType(lpRow[i].get("inner_pc_or_lp",0l));
		      			
		      			int j = 0;
		      		%>
		      		<div>
		      			<div class="box left width30">
		      				<span>Inner CLP Type:</span>
		      			</div>
		      			<%if(cascade.get("inner_pc_or_lp", 0l)==0){ %>
		      				<div name="lp" class="box right width60">
		      			
		      						<a href="javascript:void(0)" style="color:green;background:none;" onclick="openLpType('<%=cascade.get("lpt_id",0)%>');">
		      						<%= cascade.get("lp_name","") %>
		    						</a>
						</div>
		      		<%	} else{%>
		      			<div name="lpTree" class="box right width60">
		      				
		      				<% do{
		      					
		      					j++;
		      					
		      					String lpName = cascade.get("lp_name","");
		      				%>
		      				<ul>
		      					<li>
		      						<a href="javascript:void(0)" style="color:green;background:none;" onclick="openLpType('<%=cascade.get("lpt_id",0)%>');">
		      						<%= lpName %>
		      						</a>
		      				
		      				<%	
		      					
		      					cascade = cascade.get("subPlateType")==null?null:(DBRow)cascade.get("subPlateType");
		      					
		      				} while(cascade!=null);
		      				%>
		      				
		      				<% for(;j>0;j--){%>
		      					</li>
	      					</ul>
		      				<%} %>
						</div>
						<%} %>
		      			<div class="clear"></div>
		      		</div>
	      			<div>
		      			<div class="box left width30"><span>Inner CLP Qty:</span></div>
		      			<div class="box right width60"><span style="color:#0066FF"><%=lpRow[i].getString("inner_total_lp") %></span></div>
		      			<div class="clear"></div>
		      		</div>
		      		<%} %>
	      		</td>
	      		<td class="content_clp_text_td">
	      		<% 
	      		int titleArrLen = 0;
	      		int count = 0;
	      		String titleIds = lpRow[i].getString("title_ids");
	      		String shipToIds = lpRow[i].getString("ship_to_ids");
	      		String customerIds = lpRow[i].getString("customer_ids");
	      		if(StringUtils.isNotEmpty(titleIds)){
	    			String[] titleIdArr = titleIds.split(",");
	    			String[] shipToIdArr = shipToIds.split(",");
	    			String[] customerIdArr = customerIds.split(",");
	    			int index = 0;
	    			if(title_value > 0 && ship_to_value > 0 && customer_value > 0){
	    				for (int t=0; t<titleIdArr.length; t++) {
	    					if(String.valueOf(title_value).equals(titleIdArr[t])){
	    						if(String.valueOf(ship_to_value).equals(shipToIdArr[t])){
		    						if(String.valueOf(customer_value).equals(customerIdArr[t])){
		    							index = t;
		    						}	    							
	    						}
	    					}
	    				}
	    			} else if(title_value > 0 && ship_to_value<=0 && customer_value == 0){
	    				for (int t=0; t<titleIdArr.length; t++) {
	    					if(String.valueOf(title_value).equals(titleIdArr[t])){
	    						index = t;
	    					}
	    				}
	    			} else if(title_value <= 0 && ship_to_value > 0 && customer_value == 0){
	    				for (int t=0; t<shipToIdArr.length; t++) {
	    					if(String.valueOf(ship_to_value).equals(shipToIdArr[t])){
	    						index = t;
	    					}
	    				}
	    			} else if(title_value >= 0 && ship_to_value > 0){
	    				for (int t=0; t<shipToIdArr.length; t++) {
	    					if(String.valueOf(ship_to_value).equals(shipToIdArr[t])){
		    					if(String.valueOf(title_value).equals(titleIdArr[t])){
		    						index = t;
		    					}	    						
	    					}
	    				}
	    			} else if(customer_value >= 0 && ship_to_value > 0){
	    				for (int t=0; t<shipToIdArr.length; t++) {
	    					if(String.valueOf(ship_to_value).equals(shipToIdArr[t])){
		    					if(String.valueOf(customer_value).equals(customerIdArr[t])){
		    						index = t;
		    					}	    						
	    					}
	    				}
	    			} else if(title_value >= 0 && customer_value > 0){
	    				for (int t=0; t<shipToIdArr.length; t++) {
	    					if(String.valueOf(title_value).equals(titleIdArr[t])){
		    					if(String.valueOf(customer_value).equals(customerIdArr[t])){
		    						index = t;
		    					}	   						
	    					}
	    				}
	    			}
	    			
	    			if(index > 3){
	    				String oldTitle = titleIdArr[0];
	    				String oldCustomer = customerIdArr[0];
	    				String oldShipTo = shipToIdArr[0];
	    				titleIdArr[0]=titleIdArr[index];
	    				customerIdArr[0]=customerIdArr[index];
	    				shipToIdArr[0]=shipToIdArr[index];
	    				titleIdArr[index]=oldTitle;
	    				customerIdArr[index]=oldCustomer;
	    				shipToIdArr[index]=oldShipTo;
	    			}
	    		%>
	      		<fieldset class="set pr0" style="border: 0px #999 solid;width:94%">
                    <legend>
                        <span name="displayTitle" onclick="moreClick('<%=lpRow[i].get("lpt_id",0) %>')">
                            <span style="text-decoration:underline;cursor:pointer;">
<!--                                 Title / Ship To -->
                            </span>
                            
                        </span>
                    </legend>
                    <table style="width:100%;height:100% ; border-collapse: collapse;" class="innerTable">
                    	<tr class="">
							<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;">Title</td>
							<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;">Customer</td>
							<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;">ShipTo</td>
							<td width="25%" style="vertical-align:middle;text-align:center;font-weight: 700;">Operation</td>
						</tr>
								
                        <%
                        com.cwc.app.floor.api.zyj.service.CustomerService customerService = (com.cwc.app.floor.api.zyj.service.CustomerService)MvcUtil.getBeanFromContainer("customerService");
                        titleArrLen = titleIdArr.length;
                        count = 0;
                        for (int k=0;k<titleIdArr.length;k++) {
    	    				DBRow title = proprietaryMgrZyj.findProprietaryByTitleId(Long.valueOf(titleIdArr[k]));
    	    				if(customerId > 0 && Integer.parseInt(customerIdArr[k]) > 0 && customerId != Integer.parseInt(customerIdArr[k])){
    	    					continue;
    	    				}
    	    				count++;
    	    				Customer customer = null;
    	    				if(customerIdArr[k] != null  && customerIdArr[k].trim().length() > 0 && Integer.parseInt(customerIdArr[k]) > 0){
    	    					customer = customerService.getCustomer(Integer.parseInt(customerIdArr[k]));
    	    				}
    	    				String customerName = (customer != null) ?  customer.getName() : "*";
    	    				DBRow shipTo = shipToMgrZJ.getShipToById(Long.valueOf(shipToIdArr[k]));
    	    				
    	    				String titleName=title==null?"*":title.getString("title_name");
    	    				String shipToName=shipTo==null?"*":shipTo.getString("ship_to_name");
    	    				if(count > 3){
    	    				%>
                                <tr name="displayTitleAndShipTo_<%=lpRow[i].get("lpt_id",0) %>" style="display:none">
                            <% }else { %>
                                <tr>
                            <%} %>
                            	
                            	<td style="vertical-align:middle;text-align:center;height:20px;width:25%">
                            		
                            		<%if(title != null){%>
                            		<a href="javascript:void(0)" style="color: green;  padding-left: 5px;"
										onclick="openClpTitleShipTo('<%=Long.valueOf(titleIdArr[k])%>','0','<%=titleName %>','<%=shipToName%>','0','<%=customerName %>');">
										<span style="color:#0066FF"><%=titleName %></span>
									</a>
                            		<%} else {%>
                            		<span style="color:#0066FF;padding-left: 5px;"><%=titleName %></span>	
                            		<%}%>
                            		
								</td>
                            	<td style="vertical-align:middle;text-align:center;height:20px;width:25%">
                            		
                            		<%if(customer != null){%>
                            		<a href="javascript:void(0)" style="color: green;  padding-left: 5px;"
										onclick="openClpTitleShipTo('0','0','<%=titleName %>','<%=shipToName%>','<%=Long.valueOf(customerIdArr[k]) %>','<%=customerName %>');">
										<span style="color:#0066FF"><%=customerName %></span>
									</a>
                            		<%} else {%>
                            		<span style="color:#0066FF;padding-left: 5px;"><%=customerName %></span>	
                            		<%}%>
                            		
								</td>								
                                <td style="vertical-align:middle;text-align:center;height:20px;width:25%">
                                	
                                    
                                    <%if(shipTo != null){%>
                            		<a href="javascript:void(0)" style="color: green;  padding-left: 5px;"
										onclick="openClpTitleShipTo('0','<%=Long.valueOf(shipToIdArr[k]) %>','<%=titleName %>','<%=shipToName%>','0','<%=customerName %>');">
                                    	<span style="color:#0066FF"><%=shipToName %></span>
                                    </a>
                            		<%} else {%>
                            		<span style="color:#0066FF;padding-left: 5px;"><%=shipToName %></span>
                            		<%}%>
                                </td>
                                
                                <td style="vertical-align:middle;text-align:center;height:20px;width:25%">
                                	<a href="javascript:void(0)" style="color: green;  padding-left: 5px;"
										onclick="openClpTitleShipTo('<%=Long.valueOf(titleIdArr[k])%>','<%=Long.valueOf(shipToIdArr[k]) %>','<%=titleName %>','<%=shipToName%>','<%=Long.valueOf(customerIdArr[k]) %>','<%=customerName %>');">
                                    	<span style="color:#0066FF">Configuration</span>
                                    </a>
                                </td>
                            </tr>
                        <%} %>
                    </table>
                </fieldset>
                <%}else{%>
	      			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	      		<%} %>
	      		
	      		<%if(count > 3){ %>
                               <div style="float: right"><span  onclick="moreClick('<%=lpRow[i].get("lpt_id",0) %>')" style="text-decoration:underline;cursor:pointer;color:#00F" id="moreOrLess_<%=lpRow[i].get("lpt_id",0) %>">More...</span></div>
                            <%} %>
	      		</td>
<!--  	      		
	      		<td>	
	      			
	      			<div class="buttons-group minor-group">
					<a class="buttons" onclick="showBoxContainers('<%=lpRow[i].get("lpt_id",0l)%>','<%=lpRow[i].get("stack_length_qty",0l)%>*<%=lpRow[i].get("stack_width_qty",0l) %>*<%=lpRow[i].get("stack_height_qty",0l)%>');" value="CLP Lookup">CLP Lookup</a>
					<a class="buttons" onclick="print('<%=ContainerTypeKey.CLP %>','<%=lpRow[i].get("lpt_id",0l)%>','<%=lpRow[i].get("is_has_sn",0)%>')" value="Print CLP Label">Print CLP Label</a>
	      			<a class="buttons" onclick="openTitleAndShipTo('<%=lpRow[i].get("lpt_id",0l) %>');" value="Linked Title/Customer/ShipTo" >Linked Title/Customer/ShipTo</a>
				<%if(lpRow[i].get("active",0l)==1){ %>
					<a class="buttons" onclick="lockType('<%=lpRow[i].get("lpt_id",0l)%>','2');" value="Active" id="active">active</a>
                <% }else{ %>
					<a class="buttons" onclick="lockType('<%=lpRow[i].get("lpt_id",0l)%>','1');" value="Inactive" id="Inactive">Inactive</a>	
     			<%	} %>	 
     				</div>
	      		</td>
-->	      		
	      	</tr>
	      	<tr class="content_clp_button">
	      		<td class="right" colspan="3">
	      			<div class="buttons-group minor-group">	
					<a class="buttons" onclick="showBoxContainers('<%=lpRow[i].get("lpt_id",0l)%>');" value="CLP Lookup">CLP Lookup</a>
					<a class="buttons" onclick="print('<%=ContainerTypeKey.CLP %>','<%=lpRow[i].get("lpt_id",0l)%>','<%=lpRow[i].get("is_has_sn",0)%>')" value="Print CLP Label">Print CLP Label</a>
	      			<a class="buttons" onclick="openTitleAndShipTo('<%=lpRow[i].get("lpt_id",0l) %>');" value="Title/Customer/ShipTo" >Title/Customer/ShipTo</a>
				<%if(lpRow[i].get("active",0l)==1){ %>
					<a class="buttons" onclick="lockType('<%=lpRow[i].get("lpt_id",0l)%>','2');" value="Inactive" id="Inactive">Inactive</a>	
                <% }else{ %>
					<a class="buttons" onclick="lockType('<%=lpRow[i].get("lpt_id",0l)%>','1');" value="Active" id="active">Active</a>		
     			<%	} %>  
     				</div> 
     			</td>	
     			
	      	</tr>
	      <%} %>
	      <%}else{%>
	      		<tr>
	      		 	<td colspan="3" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Datas</td>
	      		</tr>
	      	<%} %>
	     </tbody> 
	</table> 
	<div>
	<table width="100%" height="10px" border="0" cellpadding="0" cellspacing="0" style="position:fixed;bottom:0;">
	<tr>
    <td width="60%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="40%" align="right" class="win-bottom-line">
		<a name="Submit2" value="Close" class="buttons primary big" onClick="windowClose();" style="margin-right:30px;">Close</a> 
	</td>
  	</tr>
	</table>
	</div>
	<script>onLoadInitZebraTable();</script>
</body>
</html>
<script>

$(document).ready(function(){
	
	$("div[name='lpTree']").jstree();
});

function refreshWindow(){
	window.location.reload();
}

function windowClose(){
	
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};

function deleteType(id){
    $.artDialog.confirm('确认删除操作?', function(){
	     $.ajax({
			 url:'<%=ConfigBean.getStringValue("systenFolder")%>' + "action/administrator/box/DeleteIlpAction.action?ibt_id="+id,
			 dataType:'json',
			 beforeSend:function(request){
		      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
			 success:function(data){
				$.unblockUI();
			 	if(data != null && data.flag === "success"){
	    		 	 window.location.reload();
			 	}else{
			 	   showMessage("系统错误,稍后再试.","error");
				}
			 },
			 error:function(){
			     $.unblockUI();
			     showMessage("系统错误,稍后再试.","error");
			 }
		  })
	}, function(){
	});
}

function addClp(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_box_sku_ps_type_add.html?pc_id="+<%= pc_id%>; 
	 $.artDialog.open(uri , {title:"["+ '<%= productInfo.getString("p_name")%>' +"]"+ " Add CLP Type",width:'650px',height:'480px', lock: true,opacity: 0.1,fixed: true, close:function(){refreshWindow();}});
}

function openContainerType(type_id){
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/show_container_type.html?type_id="+type_id; 
    $.artDialog.open(uri , {title: "Show Packaging Type",width:'420px',height:'350px', lock: true,opacity: 0.1,fixed: true});
}

function openLpType(innerlpId){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_show.html?productName="+'<%=productInfo.getString("p_name")%>&innerlpId='+innerlpId; 
	$.artDialog.open(uri , {title:'Show ['+'<%= productInfo.getString("p_name")%>'+ '] CLP Type',width:'560px',height:'420px', lock: true,opacity: 0.1,fixed: true});
}

function moreClick(lpt_id){
	var moreOrLessValue = $("#moreOrLess_"+lpt_id).text();
	$("tr[name='displayTitleAndShipTo_"+lpt_id+"']").toggle();
	if(moreOrLessValue == 'More...'){
		$("#moreOrLess_"+lpt_id).text('Less...');
	} else if(moreOrLessValue == 'Less...'){
		$("#moreOrLess_"+lpt_id).text('More...');
	}
}

function openClpTitleShipTo(title_id,ship_to_id,titleName,shipToName,customer_id,customerName){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_type_title_ship_to_list.html?title_id="+title_id+"&ship_to_id="+ship_to_id+ "&customer_id=" + customer_id +  "&titleName="+titleName+"&shipToName="+shipToName + "&customerName=" + customerName +"&pc_id="+'<%=pc_id%>'+"&productName="+'<%=productInfo.getString("p_name") %>'+"&active=1"; 
	$.artDialog.open(uri , {title:'['+'<%=productInfo.getString("p_name")%>'+'] Title/Customer/Ship To',width:'800px',height:'450px', lock: true,opacity: 0.1,fixed: true, resize:false});
}

function showBoxContainers(box_type_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/ilp_container_list.html?box_type_id="+box_type_id+"&pcid="+'<%=pc_id%>'; 
	$.artDialog.open(uri,{title: "CLP Lookup",width:'700px',height:'400px', lock: true,opacity: 0.1,fixed: true, resize:false});
}

function lockType(id,lockId){
	$.ajax({
		url:'<%=lockChangeAction%>' + "?id="+id + "&lockId="+lockId,
		type:'post',
		dataType:'json',
		timeout: 60000,
		cache:false,

		beforeSend:function(request){
		//	$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">操作中...</div>'});
			 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   },
		error:function(){
			$.unblockUI();
			showMessage("操作失败,请重试!","error");
	   },
	   success:function(msg){
	       $.unblockUI();
		   if(msg && msg.flag === "success"){
    		 	 window.location.reload();
		   }else{ showMessage("操作失败,请重试!","error"); }
			 
	   }
	 
	});
}

function openTitleAndShipTo(lp_type_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/title_shipto_list.html?lp_type_id="+lp_type_id+"&pc_id="+<%= pc_id%>;
	$.artDialog.open(uri , {title:'['+'<%=productInfo.getString("p_name")%>'+'] CLP Type Title/Customer/Ship To',width:'850px',height:'500px', lock: true,opacity: 0.1,fixed: true, resize:false,close:function(){refreshWindow();}});
}
</script>

