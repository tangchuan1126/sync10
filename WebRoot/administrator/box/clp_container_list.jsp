<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<%
String cmd = StringUtil.getString(request,"cmd");
String pcid = StringUtil.getString(request,"pcid");
int p = StringUtil.getInt(request,"p");
long sku_lp_type_id = StringUtil.getLong(request,"sku_lp_type_id");
String search_key = StringUtil.getString(request,"search_key");

PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(10);
DBRow[] containerList  = null ;

if(cmd.equals("query")){

	containerList  =  clpTypeMgrZr.getClpContainerBySearchKey(pc,sku_lp_type_id,search_key) ;
}else{

	containerList  =  clpTypeMgrZr.getClpContainerBy(pc,sku_lp_type_id) ;
}
DBRow clpType = clpTypeMgrZr.getClpTypeAndPsInfo(sku_lp_type_id);

boolean isContainBlp = !(clpType.get("basic_type_id",0) == 0);
String blpTypeName = isContainBlp ? boxTypeMgrZr.getBlpTypeName(clpType.get("basic_type_id",0)) : "";

String deleteBoxContainerAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/box/DeleteBoxContainerAction.action";
ContainerTypeKey containerTypeKey = new ContainerTypeKey();
%>


<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>CLP Containers</title>
<!-- 基本css 和javascript -->
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script> 

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

 
 
 

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

   
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

  

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />


<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- menu.js -->
<script type="text/javascript" src='../js/easyui/jquery.easyui.menu.js'></script>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">


 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<style type="text/css">
 .searchbarGray{
        color:#c4c4c4;font-family: Arial
   }
 .set {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}

</style>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '230px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
</script>
<script type="text/javascript">
//如果有货物了就不应该修改了
function updateContainer(con_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_container_update.html?con_id="+con_id;
	$.artDialog.open(uri,{title: "修改盒子",width:'500px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}
//如果上面已经有货物了。就不应该删除了
function  deleteContainer(con_id){
    $.artDialog.confirm('确认删除操作?', function(){
	     $.ajax({
			 url:'<%= deleteBoxContainerAction%>' + "?con_id="+con_id,
			 dataType:'json',
			 beforeSend:function(request){
		      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
			 success:function(data){
				$.unblockUI();
			 	if(data != null && data.flag === "success"){
	    		 	 window.location.reload();
			 	}else{
			 	   showMessage("系统错误,稍后再试.","error");
				}
			 },
			 error:function(){
			     $.unblockUI();
			     showMessage("系统错误,稍后再试.","error");
			 }
		  })
	}, function(){
	});
    
}
function refreshWindow(){
   window.location.reload();
}
jQuery(function($){
 	  $("#tabs").tabs({
			cache: true,
			cookie: { expires: 30000 } 
		});
 })
 	function addNewClp(){
 	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_container_add.html?sku_lp_type_id="+'<%= sku_lp_type_id%>';
		$.artDialog.open(uri,{title: "Add CLP Container",width:'700px',height:'350px', lock: true,opacity: 0.3,fixed: true});
	}
	function updateClp(con_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_container_update.html?con_id="+con_id;
		$.artDialog.open(uri,{title: "修改CLP托盘",width:'650px',height:'420px', lock: true,opacity: 0.3,fixed: true});
	}
	function printContainer(id,container_type){
        //uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/print_container.html?id="+id+"&container_type="+container_type; 
		uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/container_template_ui_show.html?id="+id+"&container_type="+container_type+"&detail_type=3&pcid="+'<%=pcid%>';
		$.artDialog.open(uri , {title: "Print CLP",width:'840px',height:'580px', lock: true,opacity: 0.3,fixed: true});
	}
</script>
</head>
  
   <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">
  	 	<div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
			 <ul>
				 <li><a href="#tool">Common Tools</a></li>
 			 </ul>
			 <div id="tool">
			 	 <form action="" id="toolForm" name="toolForm">
 			 	 		<input type="hidden" value="query" name="cmd"/>
 			 	 		<input type="hidden" name="sku_lp_type_id" value='<%=sku_lp_type_id %>'/>
 			 	 	 
 						 <table width="100%" height="" border="0" cellpadding="0" cellspacing="0">
				            <tr>
				             <td width="50%" style="padding-top:3px;">
 							 	 
							 Conatiner No : <input type="text" name="search_key" id="search_key" value='<%=search_key %>' style="width: 200px;" />		 
							  <input class="button_long_refresh" type="submit" value="Search" name="Submit21">
 							</td>
				              <td width="2%" >
				              </td>
				              <td width="20%" align="right" valign="middle">
   								 	<input type="button" value="Add CLP" class="long-button-add" onclick="addNewClp()"/> 
							 </td>
				            </tr>
				          </table>
  				 	 </form>
		  	 </div>
				 
		  
	 </div>
	<br/>
    <table width="98%" border="0" align="center" isNeed="true" isBottom="true" cellpadding="0" cellspacing="0"  class="zebraTable">
    <tr> 
        <th width="25%" class="right-title" style="vertical-align: center;text-align: center;">Conatiner No</th>
        <th width="25%" class="left-title" style="vertical-align: center;text-align: center;">RFID</th>
        <th width="40%" style="vertical-align: center;text-align: center;" class="left-title">CLP Type</th>
 		<th width="20%" style="vertical-align: center;text-align: center;" class="left-title">Operation</th> 
     </tr>
   <tbody>
  <%
  
  	if(containerList!=null && containerList.length > 0 ){
  		
  		for(int i = 0;i < containerList.length; i++){
  			DBRow container = containerList[i];
 		%>
    		<tr>
    			<td style="padding: 10px;"><%=container.getString("container") %>&nbsp;</td>
    			<td style="padding: 10px;"><%=container.getString("hardwareId") %>&nbsp;</td>
    			<td style="padding: 10px;">	
    			 
    		
    			<p><%= containerTypeKey.getContainerTypeKeyValue(container.get("container_type",0))%>  :  <%= clpTypeMgrZr.getClpTypeNameNeedPName(clpType) %></p>
    			 
    			</td>
<%--    			<td>--%>
<%--    				<input type="button" class="short-short-button-mod" onclick="updateClp('<%=container.get("con_id",0l) %>');" value="修改" name="Submit1">--%>
<%--    			</td>--%>
				<td>
					    			       <input value="  Print Label" type="button"  class="long-button-print" onclick="printContainer('<%=containerList[i].get("con_id",0l) %>','<%=container.get("container_type",0)%>')"/>
					
				</td>
    		</tr>
    		 
   	   <%}
  }else{%>
  		<tr>
		     <td colspan="4" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Data</td>			
		</tr>
  <%} %>
</tbody>
    </table>
    <br/>
    <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" action="">
	          <input type="hidden" name="p" />
	          <input type="hidden" name="search_key" value='<%=search_key %>' />
	          <input type="hidden" name="cmd" value='<%=cmd %>'/>
	          <input type="hidden" name="sku_lp_type_id" value='<%=sku_lp_type_id %>'/>
	  </form>
	        <tr> 
	          
	    <td height="28" align="right" valign="middle"> 
	      <%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
	     Goto 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
	    </td>
	        </tr>
</table>
  </body>
</html>
