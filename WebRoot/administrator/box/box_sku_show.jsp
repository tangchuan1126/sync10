<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="yesOrNotKey" class="com.cwc.app.key.YesOrNotKey"></jsp:useBean>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Show Box Type</title>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/jstree/jquery-1.11.2.min.js"></script>

<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<script type="text/javascript" src="../js/jstree/dist/jstree.min.js"></script>
<link type="text/css" rel="stylesheet" href="../js/jstree/dist/themes/default/style.min.css" />

<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.STYLE1{width: 150px;color:#333;}
	.margin_p{color:#333;margin-left:10px;padding:0px;border-bottom:1px dashed silver;text-align:left; margin-top:10px;text-indent:15px;padding-bottom:2px;}
	fieldset td{color:#0066FF;}
	
.jstree-default .jstree-clicked{

	background: none;
	border-radius: 2px;
	box-shadow: inset 0 0 1px #999;
}
</style>
<%
	String productName = StringUtil.getString(request,"productName");
	long innerlpId = StringUtil.getLong(request, "innerlpId");
	DBRow innerLp = boxTypeMgrZr.findIlpByIlpId(innerlpId);
	DBRow subInnerLp = null;
	if(innerLp.get("inner_pc_or_lp", 0) > 0){
		subInnerLp = boxTypeMgrZr.findIlpByIlpId(innerLp.get("inner_pc_or_lp",0));
	} else {
		subInnerLp = new DBRow();
	}
	DBRow containerInfo = containerMgrZYZ.getDetailContainerType(innerLp.get("basic_type_id", 0));
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
function closeWin()
{
	$.artDialog.close();
} 	
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="addBoxTypeForm" id="addBoxTypeForm" method="post" action="">
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  	  <tr>
   		 <td colspan="2" align="center" valign="middle">	
			<fieldset style="border:0px #cccccc solid;padding:0; margin:10px;width:90%;height:85%">
<!--   				<legend style="font-size:15px;font-weight:normal;color:#999999;margin-left:15px;">Show CLP Type</legend> -->
 					<table width="100%" border="0" cellspacing="3" cellpadding="2">
			 			<tr>
							<td align="right" valign="middle" class="STYLE1" >SKU : </td>
							<td align="left" valign="middle" > 
								<span style="color: #f60;"><%=productName %></span>
			 			    </td>
						</tr>
						<tr>
							 <td align="right" valign="middle" class="STYLE1">Packaging Type : </td>
						     <td align="left" valign="middle" >
						     	<a href="javascript:void(0)" style="color: green;"
									onclick="openContainerType('<%=containerInfo.get("type_id",0)%>');">
									<span style="color:#0066FF"><%=containerInfo.getString("type_name") %></span>
								</a>
			 			   	</td>
						</tr>
 						<tr>
						  	 <td align="right" valign="middle" class="STYLE1">Serialized  Product : </td>
						     <td align="left" valign="middle" >
						     	<%=yesOrNotKey.getYesOrNotKeyName(innerLp.get("is_has_sn",0)) %>
			 			     </td>
						</tr>
						<tr>
							<td align="right" valign="middle" class="STYLE1" >Inner Type : </td>
							<td align="left" valign="middle" >
							
							<%if(innerLp.get("inner_pc_or_lp",0) > 0){ 
		      			
				      			//获取树形数据
				      			DBRow cascade = boxTypeMgrZr.getCascadeClpType(innerLp.get("inner_pc_or_lp",0l));
				      			
				      			int j = 0;
				      			
				      		 	if(cascade.get("inner_pc_or_lp", 0l)==0){%>
						  		<div name="lp" class="box right width60">
			      							<a href="javascript:void(0)" style="color:green;background:none;" onclick="openLpType('<%=cascade.get("lpt_id",0)%>');">
			      						<%=  cascade.get("lp_name","") %>
			      						</a>
			      		
							</div>
							
							<% }else{%>	
				      		
				      		
							<div name="lpTree" class="box right width60">
		      				
			      				<% do{
			      					
			      					j++;
			      					
			      					String lpName = cascade.get("lp_name","");
			      				%>
			      				<ul>
			      					<li>
			      						<a href="javascript:void(0)" style="color:green;background:none;" onclick="openLpType('<%=cascade.get("lpt_id",0)%>');">
			      						<%= lpName %>
			      						</a>
			      				
			      				<%	
			      					
			      					cascade = cascade.get("subPlateType")==null?null:(DBRow)cascade.get("subPlateType");
			      					
			      				} while(cascade!=null);
			      				%>
			      				
			      				<% for(;j>0;j--){%>
			      					</li>
		      					</ul>
			      				<%} %>
							</div>
							<%} }else{ %>
								Product
		      				<%} %>
			 			    </td>
						</tr>
					</table>
				  <table width="100%" border="0" cellspacing="3" cellpadding="2" id="baohan">
				  	 <tr>
				  	 	 <td colspan="2" class="margin_p">
				 			<%if(innerLp.get("inner_pc_or_lp",0) > 0){
		      				%>
			      				CLP Type Qty : (The Qty Of CLP Type In CLP Type.)&nbsp;&nbsp;
		      				<%}else{ %>
		      					Product Qty : (The Qty Of Product In CLP Type.)&nbsp;&nbsp;
		      				<%} %>
				 		 </td>
					 </tr>
					 <tr>
						 <td align="right" valign="middle" class="STYLE1">Stack Length Qty : </td>
					     <td align="left" valign="middle">
					   	 	<%=innerLp.get("stack_length_qty",0) %>
					   	 </td>
				     </tr>
				     <tr>
						 <td align="right" valign="middle" class="STYLE1" >Stack Width Qty : </td>
					     <td align="left" valign="middle">
					   	 	<%=innerLp.get("stack_width_qty",0) %>
					   	 </td>
					 </tr>	
				    <tr>
						 <td align="right" valign="middle" class="STYLE1" >Stack Height Qty : </td>
					     <td align="left" valign="middle">
					   	 	<%=innerLp.get("stack_height_qty",0) %>
					   	 </td>
				    </tr>
		   		 </table>
				<table width="100%" border="0" cellspacing="3" cellpadding="2">
				   <tr>
				   	   <td style="border-bottom:1px dashed silver;margin-top:10px;padding-bottom:2px;" align="right" valign="middle" class="STYLE1">
				 	   	CLP Type Name :
				 	   </td>
				 	   <td style="border-bottom:1px dashed silver; margin-top:10px;padding-bottom:2px;" align="left" valign="middle">
					   	 	<%=innerLp.getString("lp_name") %>
					   </td>
				   </tr>
				   <tr>
					 <td align="right" valign="middle" class="STYLE1" >Weight : </td>
				     <td align="left" valign="middle" >
				        <%=innerLp.get("total_weight",0f) %>&nbsp;&nbsp;<%=weightUOMKey.getWeightUOMKey(innerLp.get("weight_uom", 0))%>
				     </td>
				  </tr>
	   		 </table>
	    	 <table width="100%" border="0" cellspacing="3" cellpadding="2">
				   <tr>
					 <td align="left" valign="middle" class="margin_p" style="margin-top: -5px;border-bottom:none;">
					 	<%=innerLp.getString("lp_name") %> &nbsp;Contains Product Qty :&nbsp;<%=innerLp.get("inner_total_pc",0) %>
					 </td>
				  </tr>
				  <%if(innerLp.get("inner_pc_or_lp",0) > 0){ %>
				  <tr>
				  	 <td align="left" valign="middle" class="margin_p" style="margin-top: -5px;border-bottom:none;">
			      		<%=subInnerLp.getString("lp_name") %> &nbsp;Contains Product Qty :&nbsp;<%=subInnerLp.get("inner_total_pc",0) %>
				     </td>
				  </tr>
				  <%} %>
	   		 </table>
 		</fieldset>
	</td>
  </tr>
  <tr>
    <td width="60%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="40%" align="right" class="win-bottom-line">
		<a name="Submit2" value="Close" class="buttons  primary big" onClick="closeWin();">Close</a> 
	</td>
  </tr>
</table>
</form>	
</body>
</html>
<script>

$(document).ready(function(){
	
	$("div[name='lpTree']").jstree();
});

function openContainerType(type_id){
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/show_container_type.html?type_id="+type_id; 
    $.artDialog.open(uri , {title: "Show Packaging Type",width:'420px',height:'350px', lock: true,opacity: 0.3,fixed: true});
}

function openLpType(innerlpId){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_show.html?productName="+'<%=productName%>&innerlpId='+innerlpId; 
	$.artDialog.open(uri , {title:'Show ['+'<%=productName%>'+ '] CLP Type',width:'560px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}
</script>

