<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<jsp:useBean id="yesOrNotKey" class="com.cwc.app.key.YesOrNotKey"></jsp:useBean>
<html>
<head>
<title>Add ILP Type</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript" ></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />




 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 
<%
   String cmd = StringUtil.getString(request,"cmd");
   long title_id=StringUtil.getLong(request,"title_id");
   long pc_id=StringUtil.getLong(request,"pc_id");
   String pc_name=StringUtil.getString(request,"product_name");
   int basic_container_type = StringUtil.getInt(request,"basic_container_type");
   DBRow[] ilpRow=null;
   if(cmd.trim().equals("query")&& basic_container_type!=0){
	    ilpRow=boxTypeMgrZr.seachIlpContainerType(pc_id,basic_container_type);
   }else{
	    ilpRow=boxTypeMgrZr.selectIlpType(pc_id); 
   }
   DBRow[] containerRow=boxTypeMgrZr.selectContainerTypeAddAllSel(ContainerTypeKey.ILP);
   
   // 商品相关的信息
   DBRow productInfo = productMgrZyj.findProductInfosByPcid(pc_id);
%>
<script type="text/javascript">
jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
    $("#tabs").tabs("select",0);  //默认选中第一个
    $(function(){
 		$(".chzn-select").chosen({no_results_text: "no this options:"});
 	});
    
});
function print(container_type, type_id, is_has_sn){
	//var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch_i.html?container_type="+container_type+"&type_id="+type_id+"&is_has_sn="+is_has_sn;
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch_common.html?container_type="+container_type+"&type_id="+type_id+"&is_has_sn="+is_has_sn+"&detail_type=1&pcid="+'<%=pc_id%>'; 
	$.artDialog.open(url , {title: 'Batch print ILP container',width:'700px',height:'450px', lock: true,opacity: 0.3,fixed: true});
}
function searchIlp()
{
	$("#basic_container_type").val($("#container_type_id").val());
	$("#toolForm").submit();
}
</script>
<style type="text/css">
td .box{float:left;padding:4px;}
td .left{text-align: right;}
td .right{text-align: left;}
td .leftWidth1{width:150px;}
td .leftWidth2{width:90px;}
td .rigthWidth1{width:170px;}
td .rigthWidth2{width:90px;}
td .even .box{border-bottom:1px solid #ddd;border-top:1px solid #ddd;}
td .clear{clear:both}
</style>
</head>
<body>
	<div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
		 <ul>
			 <li><a href="#tool">Filter</a></li>
		 </ul>
		 <div id="tool">
			 <form action="" id="toolForm" name="toolForm">
			 	 <input type="hidden" value='<%=pc_id %>' name="pc_id" />
			 	 <input type="hidden" value="query" name="cmd"/>
			 	 <input type="hidden" value="<%=pc_name %>" name="product_name"/>
			 	  <input type="hidden" value="<%=basic_container_type %>" name="basic_container_type" id="basic_container_type"/>
			 </form>
			 <div>
			   <%String length_uom = lengthUOMKey.getLengthUOMKey(productInfo.get("length_uom", 0));%>
	      	   <%String weight_uom = weightUOMKey.getWeightUOMKey(productInfo.get("weight_uom", 0));%>
			   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			     <tr>
			       <td width="10%" align="right">SKU :</td>
			       <td width="40%" colspan="3" align="left"><%="&nbsp;"+productInfo.getString("p_name")%></td>
			 	   <td width="10%" align="right">Category : </td>
			 	   <td width="40%" colspan="3" align="left"><%="&nbsp;"+productInfo.getString("title")%></td>
			 	 </tr>
			 	 <tr>
			 	   <td width="10%" align="right">Length :</td>
			 	   <td width="15%" align="left"><%="&nbsp;"+productInfo.get("length",0)+"&nbsp;"+length_uom %></td>
			 	   <td width="10%" align="right">Width :</td>
			 	   <td width="15%" align="left"><%="&nbsp;"+productInfo.get("width",0)+"&nbsp;"+length_uom %></td>
			 	   <td width="10%" align="right">Height :</td>
			 	   <td width="15%" align="left"><%="&nbsp;"+productInfo.get("heigth",0)+"&nbsp;"+length_uom %></td>
			 	   <td width="10%" align="right">Weight :</td>
			 	   <td width="15%" align="left"><%="&nbsp;"+productInfo.get("weight",0)+"&nbsp;"+weight_uom %></td>
			 	 </tr>
			   </table>
			 </div>
			 <br>
			 	<div>
			 	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				      <tr>
				        <td width="36%">
				        	<div class="side-by-side clearfix">
					 	     <select id="container_type_id" onchange="searchIlp()" class="chzn-select" data-placeholder="Choose a ILPBaseType..." tabindex="1"  style="width:250px">
					 	     	<%for(int a=0;a<containerRow.length;a++){ %>
							    <option <%=(containerRow[a].get("type_id",0l) == basic_container_type)?"selected":"" %> value="<%=containerRow[a].get("type_id",0l)%>" ><%=containerRow[a].getString("type_name")%></option>
							    <%}%>
							 </select>
							 </div>
				        </td>
				        <td align="left">
				        	<input type="button" value="Add ILP Type" class="long-long-button-add" onclick="addIlp()"/>
				        </td>
				      </tr>
				 </table>
			 	</div>
		 </div>
	</div>	
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="true" isBottom="true" class="zebraTable" style="margin-left:3px;margin-top:5px;">
		  <tr> 
 			<th width="55%" style="vertical-align: center;text-align: center;" class="right-title">ILP Type</th>
	        <th width="35%" style="vertical-align: center;text-align: center;" class="right-title">Configuration</th>
	    	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Operation</th>
	      </tr>
	     <tbody>
	     <%if(ilpRow.length > 0){ %>
	     <%for(int i=0;i<ilpRow.length;i++){ %>
	     
	      	<tr>
	      		<td>
	      			<div>
		      			<div class="box left leftWidth1"><span>ILP Type : </span></div>
		      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><%=ilpRow[i].getString("lp_name") %></span></div>
		      			<div class="clear"></div>
		      		</div>
	      			<div>
		      			<div class="box left leftWidth1"><span>ILP Packaging Type : </span></div>
		      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><%=ilpRow[i].getString("type_name") %></span></div>
		      			<div class="clear"></div>
		      		</div>
	      			<div>
		      			<div class="box left leftWidth1"><span>Serailized  Product : </span></div>
		      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><%=yesOrNotKey.getYesOrNotKeyName(ilpRow[i].get("is_has_sn",0)) %></span></div>
		      			<div class="clear"></div>
		      		</div>
	      		</td>
	      		<td>
	      			<div>
		      			<div class="box left leftWidth2"><span>Stack : </span></div>
		      			<div class="box right rigthWidth2"><span style="font-weight:bold; color:#0066FF"><%=ilpRow[i].get("stack_length_qty",0l) %> * <%=ilpRow[i].get("stack_width_qty",0l) %> * <%=ilpRow[i].get("stack_height_qty",0l) %></span></div>
		      			<div class="clear"></div>
		      		</div>
		      		<div>
		      			<div class="box left leftWidth2"><span>Product Qty : </span></div>
		      			<div class="box right rigthWidth2"><span style="font-weight:bold; color:#0066FF"><%=ilpRow[i].get("inner_total_pc",0l) %></span></div>
		      			<div class="clear"></div>
		      		</div>
	      		</td>
	      		<td>
	      		<!-- 
	      			<input class="short-short-button-mod" type="button" onclick="update('<%=ilpRow[i].get("lpt_id",0l)%>','<%=pc_name %>');" value="修改" name="Submit1"> 
      				<br />
      				<br />
					<input class="short-short-button-del" type="button" onclick="deleteType('<%=ilpRow[i].get("lpt_id",0l)%>');" value="删除" name="Submit2">
					<br />
					<br />
				 -->	
					<input class="long-button-add" type="button" onclick="showBoxContainers('<%=ilpRow[i].get("lpt_id",0l)%>','<%=ilpRow[i].get("stack_length_qty",0l)%>*<%=ilpRow[i].get("stack_width_qty",0l) %>*<%=ilpRow[i].get("stack_height_qty",0l)%>');" value="Manage ILP">
					<br/>
					<br/>
					<input class="long-button" type="button" onclick="print('<%=ContainerTypeKey.ILP %>','<%=ilpRow[i].get("lpt_id",0l)%>','<%=ilpRow[i].get("is_has_sn",0)%>')" value="Print Add ILP">						
	      		</td>
	      	</tr>
	      <%} %>
	      <%}else{%>
	      		<tr>
	      		 	<td colspan="4" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Datas</td>
	      			
	      		</tr>
	      	<%} %>
	     </tbody> 
	</table> 
	<script>onLoadInitZebraTable();</script>
</body>
</html>
<script>
function refreshWindow(){
	window.location.reload();
}

function deleteType(id){
    $.artDialog.confirm('确认删除操作?', function(){
	     $.ajax({
			 url:'<%=ConfigBean.getStringValue("systenFolder")%>' + "action/administrator/box/DeleteIlpAction.action?ibt_id="+id,
			 dataType:'json',
			 beforeSend:function(request){
		      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
			 success:function(data){
				$.unblockUI();
			 	if(data != null && data.flag === "success"){
	    		 	 window.location.reload();
			 	}else{
			 	   showMessage("系统错误,稍后再试.","error");
				}
			 },
			 error:function(){
			     $.unblockUI();
			     showMessage("系统错误,稍后再试.","error");
			 }
		  })
	}, function(){
	});
}

function update(id,name){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/ilp_sku_update.html?ibt_id="+id+"&productName="+name; 
    $.artDialog.open(uri , {title: '修改内箱类型',width:'530px',height:'480px', lock: true,opacity: 0.3,fixed: true});
}

function addIlp(){
	 
	var basic_type = $("#container_type_id").val();
 	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/ilp_sku_add.html?pcid=<%=pc_id%>&basic_type="+basic_type; 
	$.artDialog.open(uri , {title:'['+'<%=pc_name%>'+ '] Add ILP Type',width:'600px',height:'470px', lock: true,opacity: 0.3,fixed: true});
}

function showBoxContainers(box_type_id,box_type_name){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/ilp_container_list.html?box_type_id="+box_type_id+"&box_type_name="+box_type_name+"&pcid="+'<%=pc_id%>'; 
	$.artDialog.open(uri,{title: "Manage [<span style='color:#f60'>"+box_type_name+"</span>] ILP Type Container",width:'700px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}
</script>

