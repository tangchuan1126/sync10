<%@page import="com.cwc.app.key.ContainerHasSnTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.ContainerHasSnTypeKey" %>
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<jsp:useBean id="containerHasSnTypeKey" class="com.cwc.app.key.ContainerHasSnTypeKey"></jsp:useBean>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add Box Type</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.STYLE2{font-weight:bold;color:blue;width: 150px;}
	.STYLE3{font-weight:bold;color:blue;}
	.margin_p{margin-left:10px;padding:0px;}
	.font_{color:#f60;font-weight:bold;}
</style>
<%
	String addBoxTypeAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/AddBoxSkuTypeAction.action";
	String updateBoxTypeAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/UpdateBoxTypeAction.action";
	String getSkuPsIndexAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/GetSkuPsIndexAction.action";
	DBRow box = null;
 	 
	
	long pcid = StringUtil.getLong(request,"pcid");
	long basic_type = StringUtil.getLong(request, "basic_type");
	DBRow product =  orderMgrZr.getDetailById(pcid,"product","pc_id");
	String p_name =	(product != null) ?  product.getString("p_name"):"";
	long title_id = StringUtil.getLong(request, "title_id");
	box =( box == null ? new DBRow():box) ;
	product =( product == null ? new DBRow():product) ;
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	DBRow[] containers = boxTypeMgrZr.selectContainerTypeAddAllSel(containerTypeKey.BLP);
	int sort = boxTypeMgrZr.getBoxTypeSort(pcid);
	
	DBRow[] lipRow=boxTypeMgrZr.selectIlp(pcid); 
	//客户卖场
	//DBRow[] pss = storageCatalogMgrZyj.getProductStorageCatalogByType(StorageTypeKey.CUSTOMER,null); 
	DBRow[] shipTos = shipToMgrZJ.getAllShipTo();
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
jQuery(function($){

    affra();
 
    $(function(){
 		$(".chzn-select").chosen({no_results_text: "没有该选项:"});
 	});
    $("#container_type_id option[value='<%=basic_type%>']").attr("selected",true);
    setContainerValue();
    $("#to_ps_id option[value='<%=title_id%>']").attr("selected",true);
    containerChange();
})
function affra(){
    if('<%= p_name%>'.length < 1){
	$.artDialog.confirm('product is not exist', function(){
			
			$.artDialog && $.artDialog.close(); 
		}, function(){
			$.artDialog && $.artDialog.close();
		});
	}
}
 
function checkInt(value){
    var reg = /^[1-9]\d*$/;
    return reg.test(value) ;
}
function checkFloat(num)
{
    var reg = new RegExp("^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9][1-9]*[0-9]*))$"); //匹配正浮点数
    if( !reg.test(num) )
    {
        return false;
    }
    return true;
}
 
function addBoxType(_art){
 
 	if(validateFrom()){
   		 ajaxAddBoxType(_art);
	}
}

function validateFrom(){
    var container_type_id  = $("#container_type_id");
    var box_type_name = $("#box_type_name") ;
    var box_total_length = $("#box_total_length");
    var box_total_width = $("#box_total_width");
    var box_total_height = $("#box_total_height");
    var box_length = $("#box_length");
    var box_width = $("#box_width");
    var box_height = $("#box_height");
    var box_weight = $("#box_weight");
    if(container_type_id.val() * 1 == 0){
	    showMessage("Please select BLP basic type.","alert");
		container_type_id.focus();
		return false ;
	}
  
 
	if(!checkInt($.trim(box_total_length.val()))){
	    showMessage("Stack Length Qty.not legal. e.g.(1)","alert");
	    box_total_length.focus();
		return false;
	}
	if(!checkInt($.trim(box_total_width.val()))){
	    showMessage("Stack Width Qty.not legal. e.g.(1)","alert");
	    box_total_width.focus();
		return false;
	}
	if(!checkInt($.trim(box_total_height.val()))){
	    showMessage("Stack Height Qty.not legal. e.g.(1)","alert");
	    box_total_height.focus();
		return false;
	}
	if(!checkFloat($.trim(box_length.val()))){
	    showMessage("Box length.not legal. e.g.(100.00)","alert");
	    box_length.focus();
		return false;
	}



	if(!checkFloat($.trim(box_width.val()))){
	    showMessage( "Box width.not legal. e.g.(100.00)","alert");
	    box_width.focus();
		return false;
	}
	if(!checkFloat($.trim(box_height.val()))){
	    showMessage("Box height.not legal. e.g.(100.00)","alert");
	    box_height.focus();
		return false;
	}
	if(!checkFloat($.trim(box_weight.val()))){
	    showMessage("Box weight.not legal. e.g.(100.00)","alert");
	    box_weight.focus();
		return false;
	}
	return true ;
}
 
function ajaxAddBoxType(_art){
    $.ajax({
		url:'<%=addBoxTypeAction%>',
		data:$("#addBoxTypeForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
		  $.unblockUI();
	 
		  if(data != null){
				if(data.flag == "dataFormat"){
				    showMessage("The data format error.","alert");
				    $("#box_total_length").focus();
				}
				 if(data.flag === "productNotFound"){
				     showMessage("The product is not found.","error");
				 }
				if(data.flag == "success"){
				    setTimeout("windowClose()", 100);
				}
				if(data.flag == "error"){
				    showMessage("System error.","alert");
				}
				if(data.flag == "this_type_exist")
				{
					showMessage("The basic type and the put way is exist.","alert");
				}
		  }else{
		      showMessage("System error.","alert");
		  }
		},
		error:function(){
		    $.unblockUI();
		}
	})
}
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow($("#to_ps_id").val());
	};
	function closeWin()
	{
		$.artDialog.close();
	}
	 
	
	 
	function setContainerValue(){
		var container_type_id = $("#container_type_id");
		if(container_type_id.val() * 1 != 0){
			var option = $("option:selected",container_type_id);
			
		    $("#box_length").val(option.attr("clength"));
		    $("#box_width").val(option.attr("cwidth"));
		    $("#box_height").val(option.attr("cheight"));
		    $("#box_weight").val(option.attr("cweight"));
		    $("#box_length_uom").text(option.attr("lengthuom"));
		    $("#box_width_uom").text(option.attr("lengthuom"));
		    $("#box_height_uom").text(option.attr("lengthuom"));
		    $("#box_weight_uom").text(option.attr("weightuom"));
		    $("#length_uom").val(option.attr("lengthuomkey"));
		    $("#weight_uom").val(option.attr("weightuomkey"));
		   // console.log($("#length_uom").val());
		}else{

		    $("#box_length").val("0");
		    $("#box_width").val("0");
		    $("#box_height").val("0");
		    $("#box_weight").val("0");
		    $("#box_length_uom").text("");
		    $("#box_width_uom").text("");
		    $("#box_height_uom").text("");
		    $("#box_weight_uom").text("");
		    $("#length_uom").val("");
		    $("#weight_uom").val("");
		}
		
	}
	 var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	   api.button([
	   		{
				name: 'Submit',
				callback: function () {
					addBoxType(this);
					
					return false ;
	 			},
	 			focus:true
				
			},
			{
				name: 'Cancel'				
			}]
		);
	   function validateNumber(e, pnumber){
		    if (!/^\d+$/.test(pnumber)){
		    	$(e).val(/^\d+/.exec($(e).val()));
		    }
		    countValue();
		    return false;
	 	}
	 	// 计算容器上盒子的数量
	 	// 计算容器上商品的总共的数量
	 	function countValue(){
	 	 	var sku_lp_box_type = $("#bh_type");
	 	 	var boxType = sku_lp_box_type.val() * 1;
	 	 
	 	 	var box_total_sku = $("option:selected",sku_lp_box_type).attr("total") * 1;
	 	 	var box_type_name =  $("option:selected",sku_lp_box_type).html();
	 	  	var total_box = $("#total_box");
	 	  	var total_piece = $("#total_piece");

	 	  	var sku_lp_box_length = $("#box_total_length").val() * 1;
	 	  	var sku_lp_box_width = $("#box_total_width").val() * 1;
	 	  	var sku_lp_box_height = $("#box_total_height").val() * 1;
	 	  	if( boxType * 1 == 0 ){
	 	 	  	$("#boxtype").hide();
	 	 	  	$("#box_number").hide();
				var tbn = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height;
		 	  	$("#total_piece").html(tbn );
	 	 	}else{
	 	 	//	console.log("boxType"+boxType);
	 	 	 	var notity = "The ILP Type In BLP[<span class='font_'>"+box_type_name+"</span>] Contains Product Qty[<span  class='font_'>"+box_total_sku+"</span>]" ;
				var tbn = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height;
				$("#total_box").html(tbn);
				$("#box_number").show();
		 	  	$("#boxtype").show().html(notity);
		 	  	$("#total_piece").html(tbn * box_total_sku);
	 	 	}
	  	}
	 	
	 	
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="addBoxTypeForm" id="addBoxTypeForm" method="post" action="">
	<input  type="hidden" value='<%=pcid %>' name="box_pc_id"/>
	<input  type="hidden" value='' name="length_uom"  id="length_uom"/>
	<input  type="hidden" value='' name="weight_uom"  id="weight_uom"/>
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  	  <tr>
   		 <td colspan="2" align="center" valign="top">	
			<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:90%;">
  				<legend style="font-size:15px;font-weight:normal;color:#999999;">Add Box Type</legend>
 					<table width="100%" border="0" cellspacing="3" cellpadding="2">
			 			<tr>
							<td align="right" valign="middle" class="STYLE2" >SKU:</td>
							<td align="left" valign="middle" > 
								<span style="color: #f60;font-weight:bold;"><%=product.getString("p_name") %></span>
			 			    </td>
						</tr>
						<tr>
							 <td align="right" valign="middle" class="STYLE2">SHIPTO:</td>
						     <td>
						     	<div class="side-by-side clearfix">
						     		<select name="to_ps_id" id="to_ps_id" class="chzn-select" data-placeholder="Choose a ShipTo..." tabindex="1"  style="width:350px">
						     			<option value="0">Select SHIPTO...</option>
										<%if(shipTos != null && shipTos.length > 0){
											for(DBRow shipTo : shipTos){
										 %>	
										  		<option value='<%=shipTo.get("ship_to_id",0l) %>'><%=shipTo.getString("ship_to_name") %></option>
										<%} 
										}%>
						     	 	</select>
						     	 </div>
			 			   	 </td>
						</tr>
 						<tr>
						  	 <td align="right" valign="middle" class="STYLE2">BLP Basic Type:</td>
						     <td align="left" valign="middle">
						     <div class="side-by-side clearfix">
						     	<select id="container_type_id" name="container_type_id" onchange="setContainerValue()" class="chzn-select" data-placeholder="Choose a BLPBaseType..." tabindex="1"  style="width:350px">
						     		<%if(containers != null && containers.length > 0){%>
						     			<%for(DBRow row : containers){%>
						     						<option value="<%=row.get("type_id",0l) %>" 
						     								clength="<%=row.get("length",0.0f) %>"
						     								cwidth ="<%=row.get("width",0.0f) %>" 
						     								cheight="<%=row.get("height",0.0f)%>"
						     								cweight="<%=row.get("weight",0.0f) %>"
						     								lengthuom="<%=lengthUOMKey.getLengthUOMKey(row.get("length_uom",0))%>"
						     								weightuom="<%=weightUOMKey.getWeightUOMKey(row.get("weight_uom",0))%>"
						     								lengthuomkey="<%=row.get("length_uom",0) %>"
						     								weightuomkey="<%=row.get("weight_uom",0) %>"
						     						 ><%=row.getString("type_name") %></option>
						     					<% 	
						     				}
						     			}
						     		%>
						     	</select> 
						     </div>
			 			     </td>
						</tr>
						<tr>
							<td align="right" valign="middle" class="STYLE2" >Inner has SN:</td>
							<td align="left" valign="middle" > 
							<div class="side-by-side clearfix">
								<select name="is_has_sn" class="chzn-select" data-placeholder="Choose a HasSnType..." tabindex="1"  style="width:250px">
									<%
										ArrayList containerHasSnTypeKeys = containerHasSnTypeKey.getContainerHasSnTypeKeys();
										for(int i=0; i<containerHasSnTypeKeys.size();i++)
										{
											int isHasSn = Integer.parseInt((String)containerHasSnTypeKeys.get(i));
											String isHasSnStr = containerHasSnTypeKey.getContainerHasSnTypeKeyValue(isHasSn);
									%>
										<option value="<%=isHasSn%>" <%=isHasSn==ContainerHasSnTypeKey.NOSN?"selected=selected":"" %>><%=isHasSnStr%></option>
									<%		
										}
									%>
								</select>
							</div>
			 			    </td>
						</tr>
						<tr>
							<td align="right">
								<span class="STYLE2">Inner Type:</span>
							</td>
							<td>
							
								<div class="side-by-side clearfix">
									<select name="bh_type" id="bh_type" onchange="containerChange()" class="chzn-select" data-placeholder="Choose a InnerType..." tabindex="1"  style="width:250px">		
									   <option value="0">Product</option>
									   <%if(lipRow!=null){ %>
										   <%for(int a=0;a<lipRow.length;a++){ %>
										   <option value="<%=lipRow[a].get("lpt_id",0l) %>"
										   total="<%=lipRow[a].get("stack_length_qty",0l)*lipRow[a].get("stack_width_qty",0l) * lipRow[a].get("stack_height_qty",0l)%>"
										   ><%=lipRow[a].get("stack_length_qty",0l)%>*<%=lipRow[a].get("stack_width_qty",0l) %>*<%=lipRow[a].get("stack_height_qty",0l)%>  [<%=lipRow[a].getString("type_name")%>]</option> 
										   <%} %>
									   <%} %>
									</select>
								</div>
							</td>
						</tr>
					</table>
				  <table width="100%" border="0" cellspacing="3" cellpadding="2" id="baohan">
				  	 <tr>
				  	 	 <td colspan="2" style="border-bottom:1px dashed silver;text-align:left; margin-top:10px;text-indent:35px;padding-bottom:2px;" class="STYLE3">
				 			<span class="changeContainer">ILP</span> Qty：(The quantity of <span class="changeContainer"></span> in Box Type.)
				 		 </td>
					 </tr>
					 <tr>
						 <td align="right" valign="middle" class="STYLE2">Stack Length Qty:</td>
					     <td align="left" valign="middle" >
					   	 		<input name="box_total_length" id="box_total_length" type="text" onkeyup="return validateNumber(this,value);" width="20px">
					   	 </td>
				     </tr>
				     <tr>
						 <td align="right" valign="middle" class="STYLE2" >Stack Width Qty:</td>
					     <td align="left" valign="middle" >
					   	 		<input name="box_total_width" id="box_total_width" type="text" onkeyup="return validateNumber(this,value);" width="20px">
					   	 </td>
					 </tr>	
				    <tr>
						 <td align="right" valign="middle" class="STYLE2" >Stack Height Qty:</td>
					     <td align="left" valign="middle" >
					   	 		<input name="box_total_height" id="box_total_height" type="text" onkeyup="return validateNumber(this,value);" width="20px">
					   	 </td>
				    </tr>
		   		 </table>
		    	<div style="border-bottom:1px dashed  silver ;  text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="STYLE3" >
		    	BLP Basic Type Info
		    	</div>
   		 
				<table width="100%" border="0" cellspacing="3" cellpadding="2">	
					<tr>
						<td align="right" valign="middle" class="STYLE2">Length:</td>
					    <td align="left" valign="middle" >
					    <input name="box_length" id="box_length" type="text"   size="25px">
					    <span id="box_length_uom"></span>
					    </td>
					  </tr>
					<tr>
					  <td align="right" valign="middle" class="STYLE2" >Width:</td>
				      <td align="left" valign="middle" >
				         <input name="box_width" id="box_width" type="text"   size="25px">
				         <span id="box_width_uom"></span>
				      </td>
				   </tr>
				   <tr>
					 <td align="right" valign="middle" class="STYLE2" >Height:</td>
				     <td align="left" valign="middle" >
				        <input name="box_height" id="box_height" type="text"   size="25px">
				        <span id="box_height_uom"></span>
				     </td>
				   </tr>
				   <tr>
					 <td align="right" valign="middle" class="STYLE2" >Weight:</td>
				     <td align="left" valign="middle" >
				        <input name="box_weight" id="box_weight" type="text"   size="25px">
				         <span id="box_weight_uom"></span>
				     </td>
				  </tr>
	   		 </table>
	   		<div style="border-bottom:1px dashed  silver ;  text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="STYLE3" >
	    		BLP which contains [<span style='color: #f60;'><%=p_name %></span>] Sort：<span style="color:#000000;"><%=sort %> </span>
	    	</div>
	    	 <div id="showValue" style="border:1px solid silver; padding:5px;text-align: left;">
   		 		<p class="margin_p">Product:[<span  class="font_"><%=product.getString("p_name") %></span>]</p> 
   		 		<p class="margin_p" style="margin-top: -5px;" id="boxtype"></p>
   		 		<p class="margin_p" style="margin-top: -5px;">Product Total:<span id="total_piece" class="font_"></span></p>
   		 </div>
 		</fieldset>
	</td>
  </tr>
  <!-- 
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
  		<input type="button" name="Submit2" value="添加类型" class="normal-green" onClick="addBoxType();">
	   <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
   -->
</table>
</form>	
</body>
</html>
<script>
function containerChange(){
	var $tb=$('#baohan');
	var type=$('#bh_type').val();
	//console.log("type: "+type);
	if(type==0){
		$(".changeContainer").html("Product");	
	}
	else
	{
		$(".changeContainer").html("ILP");	
	}
	countValue();
	/*
		var html='<tr>'+
			  	 	'<td colspan="2" style="border-bottom:1px dashed silver;text-align:left; margin-top:10px;text-indent:35px;padding-bottom:2px;" class="STYLE3">'+
			 			'商品数量:(商品在该<span style="color:#f60;">BLP类型</span>上,长宽高各自能放几个.)'+
			 		'</td>'+
				 '</tr>'+
				 '<tr>'+
					 '<td align="right" valign="middle" class="STYLE2">横向数量:</td>'+
				     '<td align="left" valign="middle" >'+
				   	 		'<input name="box_total_length" id="box_total_length" type="text" width="20px">'+
				   	 '</td>'+
			     '</tr>'+
			     '<tr>'+
					 '<td align="right" valign="middle" class="STYLE2" >纵向数量:</td>'+
				     '<td align="left" valign="middle" >'+
				   	 		'<input name="box_total_width" id="box_total_width" type="text" width="20px">'+
				   	 '</td>'+
				 '</tr>'+	
			     '<tr>'+
					 '<td align="right" valign="middle" class="STYLE2" >垂直数量:</td>'+
				     '<td align="left" valign="middle" >'+
				   	 		'<input name="box_total_height" id="box_total_height" type="text" width="20px">'+
				   	 '</td>'+
			    '</tr>';
		$tb.html(html);
	}else{
		var html='<tr>'+
			  	 	'<td colspan="2" style="border-bottom:1px dashed silver;text-align:left; margin-top:10px;text-indent:35px;padding-bottom:2px;" class="STYLE3">'+
			 			'ILP数量:(ILP在该<span style="color:#f60;">BLP类型</span>上,长宽高各自能放几个.)'+
			 		'</td>'+
				 '</tr>'+
				 '<tr>'+
					 '<td align="right" valign="middle" class="STYLE2">ILP横向数量:</td>'+
				     '<td align="left" valign="middle" >'+
				   	 		'<input name="box_total_length" id="box_total_length" type="text" width="20px">'+
				   	 '</td>'+
			     '</tr>'+
			     '<tr>'+
					 '<td align="right" valign="middle" class="STYLE2" >ILP纵向数量:</td>'+
				     '<td align="left" valign="middle" >'+
				   	 		'<input name="box_total_width" id="box_total_width" type="text" width="20px">'+
				   	 '</td>'+
				 '</tr>'+	
			     '<tr>'+
					 '<td align="right" valign="middle" class="STYLE2" >ILP垂直数量:</td>'+
				     '<td align="left" valign="middle" >'+
				   	 		'<input name="box_total_height" id="box_total_height" type="text" width="20px">'+
				   	 '</td>'+
			    '</tr>';
		$tb.html(html);
		
	}
	*/
}
</script>

