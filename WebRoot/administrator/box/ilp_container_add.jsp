<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add CLP Container</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="/Sync10-ui/lib/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.STYLE2{font-weight:bold;color:blue;}
	table.inner{border-collapse:collapse;}
	table.inner td{border:1px solid silver;padding: 8px;}
</style>
<%
	 long box_type_id = StringUtil.getLong(request,"box_type_id");
	 DBRow boxType = boxTypeMgrZr.findIlpByIlpId(box_type_id);
	 long pc_id = boxType.get("pc_id",0l);
	//	out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+pc_id);
	 String box_type_name=StringUtil.getString(request,"box_type_name");
	 DBRow product = orderMgrZr.getDetailById(pc_id,"product","pc_id");
	 boxType.add("p_name",product.getString("p_name"));
	 String addBoxContainerAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/box/AddBoxContainerAction.action";
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.1' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
jQuery(function($){
	 
    
})
 
function checkInt(value){
    var reg = /^[1-9]\d*$/;
    return reg.test(value) ;
}
function checkFloat(num)
{
    var reg = new RegExp("^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9][1-9]*[0-9]*))$"); //匹配正浮点数
    if( !reg.test(num) )
    {
        return false;
    }
    return true;
}
 
function addBoxContainer(_art){
 
 	//if(validateFrom()){
   		 ajaxAddBoxContainer(_art);
	//}
}

function validateFrom(){
    var container  = $("#container");
    var hardwareId = $("#hardwareId");
   if(container.val().trim().length < 1){
		showMessage("Please input conatiner No","alert");
		container.focus();
		return false ;
	}
    if(hardwareId.val().trim().length < 1){
		showMessage("Please input container RFID","alert");
		hardwareId.focus();
		return false ;
	}
	return true;
 
}
 
function ajaxAddBoxContainer(_art){
    $.ajax({
		url:'<%=addBoxContainerAction%>',
		data:$("#addBoxContainerForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	      
	      _art.button({
              name: 'Submit',
              disabled: true
          });
	    },
		success:function(data){
		  $.unblockUI();
	 
		  if(data != null){
			 if(data.flag == "success" && data.id * 1 > 0){
			     windowClose();
			  }
			  if(data.flag == "error" && data.message === "hardwareIdExits"){
				    showMessage("RFID Exists in the system,Please try again!","error");
				    
				    _art.button({
			              name: 'Submit',
			              disabled: false
			          });
			  }
		  }else{
		      showMessage("System error,please try it again later.","error");
		      
		      _art.button({
	              name: 'Submit',
	              disabled: false
	          });
		  }
		},
		error:function(){
		    showMessage("System error,please try it again later.","error");
		    
		    _art.button({
	              name: 'Submit',
	              disabled: false
	          });
		    
		    $.unblockUI();
		}
	})
}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function closeWin(){
	$.artDialog && $.artDialog.close();
}
var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
api.button([
		{
			name: 'Submit',
			callback: function () {
				addBoxContainer(this);
				
				return false ;
			},
			focus:true
			
		},
		{
			name: 'Cancel' 
		}]
	);
 
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 
<form name="addBoxContainerForm" id="addBoxContainerForm" method="post" action="">
<input  type="hidden"  value='<%=box_type_id %>' name="box_type_id"/>
<input  type="hidden"  value='<%=ContainerTypeKey.CLP%>' name="container_type"/>
<input type="hidden" name="is_has_sn" value="<%=boxType.get("is_has_sn",0)%>"/>

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	
<div style="border:0px #cccccc solid;padding:15px;margin:10px;width:90%;">
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
 			<tr>
				<td align="right" valign="middle" class="STYLE2" width="90px" >SKU:</td>
				<td align="left" valign="middle" > 
						<span style='color:#f60;font-weight:bold;'><%=product.getString("p_name") %></span>
 			    </td>
			</tr>
			<tr>
				<td align="right" valign="middle" class="STYLE2" width="90px" >CLP Type:</td>
				<td align="left" valign="middle" > 
						<span style='color:#f60;font-weight:bold;'><%=box_type_name%></span>
 			    </td>
			</tr>
 			<tr>
 				<td align="right" valign="middle" class="STYLE2" width="90px" ></td>
			     <td align="left" valign="middle">
			     	 <table class="inner">
			     	 	<tr>
			     	 		<td style="text-align: right;width:120px;">Stack Length Qty:</td>
			     	 	 	<td style="width:40px;text-align: center;"><%= boxType.get("stack_length_qty",0) %></td>
			     	 		<td rowspan="3">Total: <%= boxType.get("inner_total_pc",0) %></td>
			     	 	</tr>
			     	 	<tr>
			     	 		<td style="text-align: right;width:120px;">Stack Width Qty:</td>
 			     	 		<td style="width:40px;text-align: center;"><%= boxType.get("stack_width_qty",0) %></td>
			     	 	</tr>
			     	 	<tr>
			     	 		<td style="text-align: right;width:120px;">Stack Height Qty:</td>
 			     	 		<td style="width:40px;text-align: center;"><%= boxType.get("stack_height_qty",0) %></td>
			     	 	</tr>
			     	  
			     	 </table>
 			     </td>
			</tr>
			
		</table>
		
		 <div style="border-bottom:1px dashed  silver ; text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="STYLE2" >
		 </div>
		 <table width="100%" border="0" cellspacing="3" cellpadding="2">
<%--		 	<tr>--%>
<%--		 		<td align="right" valign="middle" class="STYLE2" width="90px" >容器条码:</td>--%>
<%--		 		<td  align="left" valign="middle"> <input type="text" id="container" name="container" style="width:200px;"/></td>--%>
<%--		 	</tr>--%>
		 	 <tr>
		 		<td align="right" valign="middle" class="STYLE2" width="90px" >RFID:</td>
		 		<td  align="left" valign="middle" ><input type="text" id="hardwareId" name="hardwareId" style="width:200px;"/></td>
		 	</tr>
		 </table>
		  
    	 
 </div>
</td>
</tr>
<!--  
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
     <input type="button" name="Submit2" value="添加内箱" class="normal-green" onClick="addBoxContainer();">
	
 
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
  -->
</table>
</form>	
</body>
</html>

