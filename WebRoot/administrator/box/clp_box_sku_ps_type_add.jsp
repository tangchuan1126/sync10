<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List, com.cwc.app.key.StorageTypeKey,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<%@ page import="com.cwc.app.key.ContainerHasSnTypeKey" %>
<%@page import="com.cwc.app.key.YesOrNotKey"%>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<jsp:useBean id="containerHasSnTypeKey" class="com.cwc.app.key.ContainerHasSnTypeKey"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add CLP Type</title>


<link href="../comm.css" rel="stylesheet" type="text/css">
<!-- <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script> -->
<script type="text/javascript" src="../js/jstree/jquery-1.11.2.min.js"></script>
<!-- select2选项卡 -->
<link rel="stylesheet" href="../jquery-select2-custom.css">
<link rel="stylesheet" href="../js/select2-4.0.0-rc.2/dist/css/select2.css">
<script type="text/javascript" src="../jquery-select2-custom.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../../common.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />



<script src="/Sync10-ui/lib/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉菜单 -->
<!-- <link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> --> 


<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<link rel="stylesheet" type="text/css" href="../buttons.css"  />
 
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.STYLE2{font-weight:bold;color:blue;width: 150px;}
	.STYLE3{font-weight:bold;color:blue;}
	.margin_p{margin-left:10px;padding:0px;padding-top: 10px;}
	.font_{color:#f60;font-weight:bold;}
	.changeContainer{color:#f60;}
</style>
<%
long pc_id = StringUtil.getLong(request,"pc_id");
long title_id = StringUtil.getLong(request,"title_id");

 DBRow[]  lpRow = boxTypeMgrZr.getBoxTypes(pc_id,0,0,0);
//查询product_name 
DBRow product = orderMgrZr.getDetailById(pc_id,"product","pc_id");

String sn_size = product.getString("sn_size","");

product = (product == null) ? new DBRow() : product; 
//clp type
DBRow[] lpContainerTypes = boxTypeMgrZr.getBoxContainerType(ContainerTypeKey.CLP);
//客户卖场
//DBRow[] pss = storageCatalogMgrZyj.getProductStorageCatalogByType(StorageTypeKey.CUSTOMER,null); 
DBRow[] shipTos = shipToMgrZJ.getAllShipTo();
//sku 盒子类型
DBRow[] boxTypes = boxTypeMgrZr.getBoxSkuTypeByPcId(pc_id);
//title
DBRow[] titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true,0,"",null,request);

String addClpBoxSkuPsTypeAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/AddClpBoxSkuPsTypeAction.action";
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.1' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
 	function changeContainer(){
		var sku_lp_box_type = $("#sku_lp_box_type").val();
		if(sku_lp_box_type * 1 != 0){
			$(".changeContainer").html("CLP Type");
		}else{
		    $(".changeContainer").html("Product");
		}
		countValue();
 	}
 	jQuery(function($){
 	   changeContainer();
 	  $("#sku_lp_to_ps_id option[value='<%=title_id%>']").attr("selected",true);
 	 changeBlpTypes();
 	changeClpBasicTypes();
 	    //getIndexPcIdSku();
	 	$("#lp_type_id").select2({
			placeholder: "Select Packaging Type..."
	       ,allowClear: true
	     });
	    $("#sku_lp_box_type").select2();
	    $("#is_has_sn").select2();
 	})
 	function addClpType(_art){
		if(validate()){
			
			var sku_lp_box_length = $("#sku_lp_box_length");
			var sku_lp_box_width =$("#sku_lp_box_width");
			var sku_lp_box_height = $("#sku_lp_box_height");
			var	sku_lp_box_type = $("#sku_lp_box_type").val();

			//var	weight = $("#weight").val;

	 	  	var pc_length = $("#pc_length").val();
	 	  	var pc_width = $("#pc_width").val();
	 	  	var pc_heigth = $("#pc_heigth").val();
		  	var pc_weight = $("#pc_weight").val();
	 	  	
	 	  	var box_length = $("#box_length").val();
	 	  	var box_width = $("#box_width").val();
	 	  	var box_height = $("#box_height").val();   
	 	  	var box_weight = $("#box_weight").val();
	 	  	
	 	    var Inner_Type_weight=	$("#Inner_Type_weight").val();
	 		
	     	var Inner_Type_length=	$("#Inner_Type_length").val();
	   		
	    	var Inner_Type_height=	$("#Inner_Type_height").val();

	    	var Inner_Type_width=	$("#Inner_Type_width").val();
	 		
	    	var Inner_Type_length_uom=	$("#Inner_Type_length_uom").val();
	 		
	    	var Inner_Type_weight_uom=	$("#Inner_Type_weight_uom").val();
	 		

		  	var weight_uom = $("#weight_uom").val();
	 		var length_uom = $("#length_uom").val();
	 		var pc_weight_uom = $("#pc_weight_uom").val();
	 		var pc_length_uom = $("#pc_length_uom").val();

			if(sku_lp_box_type==0){
				 var lengthTo =	unitConverter.lengthUnitConverter(pc_length,pc_length_uom,length_uom);
		  		 var lengthFrom =	unitConverter.lengthUnitConverter(box_length,length_uom,length_uom);
		  		 
				 var widthTo =	unitConverter.lengthUnitConverter(pc_width,pc_length_uom,length_uom);
		  		 var widthFrom = unitConverter.lengthUnitConverter(box_width,length_uom,length_uom);
		  		 
				 var heigthTo =	unitConverter.lengthUnitConverter(pc_heigth,pc_length_uom,length_uom);
		  		 var heigthFrom =	unitConverter.lengthUnitConverter(box_height,length_uom,length_uom);
		  		 
		  		 var weightTo =	unitConverter.lengthUnitConverter(pc_weight,pc_weight_uom,weight_uom);
		  		 var weightFrom =	unitConverter.lengthUnitConverter(box_weight,weight_uom,weight_uom);
		  		 
		
		  		
		  		var validates = true; 
		  		
		  		var  message = "<ol>";
		  		
		  		 var lengthSum = lengthTo*sku_lp_box_length.val() ;
		  		 
		  		 var widthSum =  widthTo*sku_lp_box_width.val() ;
		  		
		    	 var heigthSum = heigthTo*sku_lp_box_height.val() ;
		    	 
		     	 var weightSum = weightTo*sku_lp_box_length.val()*sku_lp_box_width.val()*sku_lp_box_height.val();

		  		 if(lengthSum>lengthFrom){
		  			validates = false;
		  			message += "<li width='250px'>Inner Length too long.  </li>";
		  		 }
		  		 
		  		 if(widthSum>widthFrom){
		  			 
		  			validates = false;
		  			message += "<li width='250px'>Inner width too long.  </li>";
		  		 }
		  		 
		  		 if(heigthSum>heigthFrom){
		  			 
		  			validates = false;
		  			message += "<li width='250px'>Inner height too long.  </li>";
		  		 }
		  		 if(weightSum>weightFrom){
		  			 
			  			validates = false;
			  			message += "<li width='250px'>Inner Weight too heavy.  </li>";
			  		 }
			  		 
		  		message + "</ol>"
		  		 if(validates==false){
		  			 $.artDialog({
						    content: message,
						    icon: 'question',
						    width: 230,
						    height: 100,
						    lock: true,
						    opacity: 0.3,
						    title:'Warning',
						    okVal: 'Confirm',
						    ok: function () {
						    	 ajaxAddClpType(_art);
						    },
						    cancelVal: 'Cancel',
						    cancel: function(){
							}
						});	 
		  		 }else{
					   ajaxAddClpType(_art);
				   }
		  		 

		}else if(sku_lp_box_type!=0){
			
			 var lengthTo =	unitConverter.lengthUnitConverter(Inner_Type_length,Inner_Type_length_uom,length_uom);
	  		 var lengthFrom =	unitConverter.lengthUnitConverter(box_length,length_uom,length_uom);
		  		
			 var widthTo =	unitConverter.lengthUnitConverter(Inner_Type_width,Inner_Type_length_uom,length_uom);
	  		 var widthFrom = unitConverter.lengthUnitConverter(box_width,length_uom,length_uom);
		  	
			 var heigthTo =	unitConverter.lengthUnitConverter(Inner_Type_height,Inner_Type_length_uom,length_uom);
	  		 var heigthFrom =	unitConverter.lengthUnitConverter(box_height,length_uom,length_uom);
		  		
	  		 var weightTo =	unitConverter.lengthUnitConverter(Inner_Type_weight,Inner_Type_weight_uom,weight_uom);
	  		 var weightFrom =	unitConverter.lengthUnitConverter(box_weight,weight_uom,weight_uom);
	  		
	  	     var validates = true; 
	  		
	  	 	 var  message = "<ol>";
	  		
	  		 var lengthSum = lengthTo*sku_lp_box_length.val() ;
	  		 
	  		 var widthSum =  widthTo*sku_lp_box_width.val() ;
	  		
	    	 var heigthSum = heigthTo*sku_lp_box_height.val() ;
	    	 
	    	 var sl = sku_lp_box_length.val()*sku_lp_box_width.val()*sku_lp_box_height.val();
	    	 
	     	 var weightSum = weightTo*sl;
	     	 if(lengthSum>lengthFrom){
		  			validates = false;
		  			message += "<li>Inner Length too long.  </li>";
		  	 }
		  		 
		  	if(widthSum>widthFrom){
		  			validates = false;
		  			message += "<li>Inner width too long.  </li>";
		  	}
		  		 
		  	if(heigthSum>heigthFrom){
		  			validates = false;
		  			message += "<li>Inner height too long.  </li>";
		  	 }
		  	if(weightSum>weightFrom){
			  			validates = false;
			  			message += "<li>Inner Weight too heavy.  </li>";
			}
		  	
		  	message+="</ol>";
		  		 
		  	if(validates==false){
		  			 $.artDialog({
						    content: message,
						    icon: 'question',
						    width: 230,
						    height: 100,
						    lock: true,
						    opacity: 0.3,
						    title:'Warning',
						    okVal: 'Confirm',
						    ok: function () {
						    	 ajaxAddClpType(_art);
						    },
						    cancelVal: 'Cancel',
						    cancel: function(){
							}
						});	 
		   }else{
			   ajaxAddClpType(_art);
		   }
		  		 
		}
			
		}
 	}
 	function checkInt(value){
 	    var reg = /^[1-9]\d*$/;
 	    return reg.test(value) ;
 	}
 	function ajaxAddClpType(_art){
 		
 	   $.ajax({
		url:'<%=addClpBoxSkuPsTypeAction%>',
		data:$("#addClpTypeForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
			_art.button({
	                name: 'Submit',
	                disabled: true
	            });
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
		  if(data != null && data.flag){
			  if(data.flag === "error" ){
					if(data.message === "InnerType_Error"){
						showMessage("The InnerType you select is inactive now.","alert");
					}				  
				 	if(data.message === "this_type_exist"){
						showMessage("The basic type and the put way is exist.","alert");
					}
					if(data.message === "skuNotMatch"){
						showMessage("Product and Box not match.","alert");
					}
					if(data.message === "skuLpTotalBoxLetter"){
						showMessage("The box quantity on CLP is wrong.","alert");
					}
					if(data.message === "skuLpTotalPieceLetter"){
						showMessage("The Product quantity on CLP is wrong.","alert");
					}

			  }
			  if(data.flag === "success"){
					if(data.message * 1 > 0){
						$.artDialog({
						    content: 'Continue to add CLP type?',
						    icon: 'question',
						    width: 200,
						    height: 100,
						    lock: true,
						    opacity: 0.3,
						    title:'Tips',
						    okVal: 'Confirm',
						    ok: function () {

								//window.location.reload();
						    			    	
						    	$("#lp_type_id").select2("val", "");
						    	$("#select2-lp_type_id-container").attr("title", "");
						    	
						    	changeClpBasicTypes();
						    	
						    	var sku_lp_box_type = $("#sku_lp_box_type").val();
						    	//重新获取下拉选项
						    	changeBlpTypes();						    	
						    	$("#sku_lp_box_type").select2("val", sku_lp_box_type);
						    },
						    cancelVal: 'Cancel',
						    cancel: function(){
						    	windowClose();
							}
						});
					}
			  }
		  }else{
		      showMessage("System error,try it again later.","error");
		  }
		  $.unblockUI();
		  _art.button({
              name: 'Submit',
              disabled: false
          });
		},
		error:function(){
		     showMessage("System error,try it again later.","error");
		     $.unblockUI();
		     _art.button({
	              name: 'Submit',
	              disabled: false
	          });
		}
	})
 	}
 	function validate(){
		var lp_type_id = $("#lp_type_id");
		var sku_lp_to_ps_id = $("#sku_lp_to_ps_id");
		var sku_lp_box_length = $("#sku_lp_box_length");
		var sku_lp_box_width =$("#sku_lp_box_width");
		var sku_lp_box_height = $("#sku_lp_box_height");
		var	sku_lp_box_type = $("#sku_lp_box_type").val();
		
 	  	
		
		if(lp_type_id.val() * 1 == 0){
			showMessage("Please select CLP Packaging type","alert");
			return false ;
		}
	 
	/*	if(sku_lp_to_ps_id.val() * 1 == 0){
			showMessage("Please select Ship To","alert");
			return false ;
		}
	*/
		if(checkInt(sku_lp_box_length.val()) * 1 == 0 ){
			showMessage("Stack Length Qty.not legal. e.g.(1)","alert");
			return false ;
		}
		if(checkInt(sku_lp_box_width.val() )* 1 == 0 ){
			showMessage("Stack Width Qty.not legal. e.g.(1)","alert");
			return false ;
		}
		if(checkInt(sku_lp_box_height.val() )* 1 == 0 ){
			showMessage("Stack Height Qty.not legal. e.g.(1)","alert");
			return false ;
		}
		

  		
		
		
		return  true;
 	}
 	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function validateNumber(e, pnumber){
	    if (!/^\d+$/.test(pnumber)){
	    	$(e).val(/^\d+/.exec($(e).val()));
	    }
	    countValue();
	    return false;
 	}
 	// 计算容器上盒子的数量
 	// 计算容器上商品的总共的数量
 	function countValue(){
 	 	var sku_lp_box_type = $("#sku_lp_box_type");
 	 	var boxType = sku_lp_box_type.val() * 1;
 	 	var box_total_sku = $("option:selected",sku_lp_box_type).attr("total") * 1;
 	 	var innerClpWeight = $("option:selected",sku_lp_box_type).attr("totalWeight") * 1;
 	 	var innerClpWeightUom = $("option:selected",sku_lp_box_type).attr("clpUom") * 1;
 	 	var box_type_name =  $("option:selected",sku_lp_box_type).html();
 	 	var lp_type_id =  $("option:selected",lp_type_id).html();
 	 	var innerClpLength = $("option:selected",sku_lp_box_type).attr("ibt_length") * 1;
 	 	var innerClpHeight = $("option:selected",sku_lp_box_type).attr("ibt_height") * 1;
 	 	var innerClpWidth = $("option:selected",sku_lp_box_type).attr("ibt_width") * 1;
 	 	
 	 	if(lp_type_id == ""){
 	 		lp_type_id = $("#lp_type_id").data("placeholder");
 	 	}
 	
 	 	var innerClpLength_uom = $("option:selected",sku_lp_box_type).attr("length_uom") * 1;
 	  	
 	  	
 	  	var sku_lp_box_length = $("#sku_lp_box_length").val() * 1;
 	  	var sku_lp_box_width = $("#sku_lp_box_width").val() * 1;
 	  	var sku_lp_box_height = $("#sku_lp_box_height").val() * 1;
 	  	
 	  	var sl = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height;
 	  	var lpname = "CLP"+sku_lp_box_length+"*"+sku_lp_box_width+"*"+sku_lp_box_height+"["+lp_type_id+"]";
 	  	
 	  	var weight_uom = $("#weight_uom").val();
 		var length_uom = $("#length_uom").val();
 		var pc_weight_uom = $("#pc_weight_uom").val();
 		var pc_length_uom = $("#pc_length_uom").val();
 	  	var pc_weight = $("#pc_weight").val();
 	  	var box_weight = $("#box_weight").val();
 	  	var box_weighn_no = parseFloat(box_weight); 
 	  	var uom_pcOrClp = 0;
 	  	var weight_pcOrClp = 1;
 	  	
 	  	
 	  	if( boxType * 1 == 0 ){
 	 	  	$("#boxtype").show().text(lpname+" Contains Product Qty : "+sl);
 	 	  	$("#box_number").hide();
 	 	  	uom_pcOrClp = pc_weight_uom;
 	 	    weight_pcOrClp = pc_weight;
 	 	}else{
 	 		lpname += "["+box_type_name+"]";
	 	  	$("#boxtype").show().text(lpname+" Contains Product Qty : "+box_total_sku*sl);
	 	  	$("#box_number").show().text(box_type_name+" Contains Product Qty : "+box_total_sku);
	 	  	uom_pcOrClp = innerClpWeightUom;
	 	  	weight_pcOrClp = innerClpWeight;
	 	  	
 	 	}
 	  	$("#lp_name").val(lpname);
 	
 	  	//weight 计算
 	  	if(weight_uom==1 && uom_pcOrClp==1){
 	  		var	weight = (((parseFloat(weight_pcOrClp)*10000) *sl)/10000 + box_weighn_no).toFixed(3)
 	  	}
 	  	if(weight_uom==2 && uom_pcOrClp==1){
			var	pc_weight_lbs = ( (parseFloat(weight_pcOrClp)*10000)* 2.2046 )/10000;
 	  		var	weight = (((parseFloat(pc_weight_lbs)*10000) *sl)/10000 + box_weighn_no).toFixed(3)
 	  	}
 	  	if(weight_uom==1 && uom_pcOrClp==2){
 	  		var	pc_weight_kg = ( (parseFloat(weight_pcOrClp)*10000)* 0.4536 )/10000;
 	  		var	weight = (((parseFloat(pc_weight_kg)*10000) *sl)/10000 + box_weighn_no).toFixed(3)	 
 	  	}
 	  	if(weight_uom==2 && uom_pcOrClp==2){
 	  		var	weight = (((parseFloat(weight_pcOrClp)*10000) *sl)/10000 + box_weighn_no).toFixed(3)
 	  	}
 	  	
 	  	$("#weight").val(weight);
 	  	
 		$("#Inner_Type_weight").val(innerClpWeight);
 		
 		$("#Inner_Type_length").val(innerClpLength);
 		
 		$("#Inner_Type_height").val(innerClpHeight);

 		$("#Inner_Type_width").val(innerClpWidth);
 		
 		$("#Inner_Type_length_uom").val(innerClpLength_uom);
 		
 		$("#Inner_Type_weight_uom").val(innerClpWeightUom);
 		
 		
 	  	
 	  	
 		

  	  		
  	}
 	function closeWin(){
		$.artDialog && $.artDialog.close();
 	}
 	/* $(function(){
 		$(".chzn-select").chosen({no_results_text: "no this option:"});
 	}); */
 	
 	 var api =  art.dialog.open.api;	// 			$.artDialog.open扩展方法
 	   api.button([
 	   		{
 				name: 'Submit',
 				callback: function () {
 					addClpType(this);
 					
 					return false ;
 	 			},
 	 			focus:true
 				
 			},
 			{
 				name: 'Cancel', 
 				callback: function() {
 					windowClose();
 					return false;
 				}
 			}]
 		);
 	 function changeBlpTypes()
 	 {
 		 $("#sku_lp_box_type").empty();
		 $("#sku_lp_box_type").append('<option value="0" >Product</option>');
 		 var para = 'pc_id=<%=pc_id%>&ship_to='+$("#sku_lp_to_ps_id").val();
 		$.ajax({
 			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/box_ps/BoxFindByPcShipToAction.action',
 			data:para,
 			dataType:'json',
 			type:'post',
 			async:false,
 			beforeSend:function(request){
 		      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
 		    },
 			success:function(data){
 			  $.unblockUI();
 			 
 			  if(data && data.length > 0){
 				 $.each(data,function(i){
 					 var boxType = data[i];
 					
 					 var option = '';
 					option += "<option value='"+boxType.lpt_id+"' total='"+boxType.inner_total_pc+"' totalWeight='"+boxType.total_weight+"' clpUom='"+boxType.weight_uom+"' ibt_length='"+boxType.ibt_length+"'ibt_width='"+boxType.ibt_width+"' ibt_length='"+boxType.ibt_length+"' ibt_height='"+boxType.ibt_height+"'length_uom='"+boxType.length_uom+"' 	>";
 					option += boxType.lp_name+"</option>";
 	 				
 	 				$("#sku_lp_box_type").append($(option));
 	 			//	var aa = option.attr("ibt_length")
 	 	//		$("#Inner_Type_length").val(option.attr(boxType.ibt_length));
 	 	//	    $("#Inner_Type_width").val(option.attr(boxType.ibt_width));
 	 	//	    $("#Inner_Type_height").val(option.attr(boxType.ibt_height));
 	 	//	    $("#Inner_Type_weight").val(option.attr(boxType.ibt_weight));
 				});
 				/*
 				 for(var i = 0; i < data.length; i ++)
				 {
 					var boxType = data[i];
					var option = '';
					option += "<option value='"+boxType.box_type_id+"' total='"+boxType.box_total_piece+"'>";
					option += boxType.box_total_length+"*"+boxType.box_total_width+"*"+boxType.box_total_height;
	 				option += " [BLP Type]</option>";
	 				//alert(option);
	 				$("#sku_lp_box_type").append($(option));
				 }*/
 				// alert($("#sku_lp_box_type option").length);
 				
 			  }
 			},
 			error:function(){
 			     showMessage("System error,try it again later.","error");
 			     $.unblockUI();
 			}
 		});
 		var selectObj = $("#sku_lp_box_type");
        //selectObj.parent().children().remove('div');
        /* selectObj.removeClass();
        selectObj.addClass("chzn-select");
        selectObj.chosen();
 		$(".chzn-select").chosen({no_results_text: "no this option:"}); */
 		

 	 }
 	 function changeClpBasicTypes()
 	 {
 		var container_type_id = $("#lp_type_id");
		if(container_type_id.val() * 1 != 0){
			var option = $("option:selected",container_type_id);
		    $("#box_length").val(option.attr("clength"));
		    $("#box_width").val(option.attr("cwidth"));
		    $("#box_height").val(option.attr("cheight"));
		    $("#box_weight").val(option.attr("cweight"));
			$("#length_uom").val(option.attr("lengthuomkey"));
		    $("#weight_uom").val(option.attr("weightuomkey"));
		    $("#box_weight_uom").text(option.attr("weightuom"));
		}else{
		    $("#box_length").val("0");
		    $("#box_width").val("0");
		    $("#box_height").val("0");
		    $("#box_weight").val("0");	
		    $("#length_uom").val("");
		    $("#weight_uom").val("");
		    $("#box_weight_uom").text("");
		}
		
		countValue();
 	 }
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 
<form name="addClpTypeForm" id="addClpTypeForm" method="post" action="">
<input type="hidden" name="sku_lp_pc_id" value='<%=pc_id %>'/>
<input  type="hidden" value='' name="length_uom"  id="length_uom"/>
<input  type="hidden" value='' name="weight_uom"  id="weight_uom"/>
<input  type="hidden" value='<%=product.get("weight_uom",0l) %>' name="pc_weight_uom"  id="pc_weight_uom"/>
<input  type="hidden" value='<%=product.get("length_uom",0l) %>' name="pc_length_uom"  id="pc_length_uom"/> 
 <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	
<!-- <fieldset style="border:0px #cccccc solid;padding:5px;margin:10px;width:90%;"> -->
<!--   <legend style="font-size:15px;font-weight:normal;color:#999999;">Add CLP Type</legend> -->
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
 			<tr>
				<td align="right" valign="middle" class="STYLE2" >SKU&nbsp;:&nbsp;</td>
				<td align="left" valign="middle" > 
					<span style="color: #f60;font-weight:bold;"><%= product.getString("p_name") %></span>
 			    </td>
			</tr>
 			<tr>
			  	 <td align="right" valign="middle" class="STYLE2">Packaging Type&nbsp;:&nbsp;</td>
			     <td align="left" valign="middle">
				     <div class="side-by-side clearfix">
				     	<select id="lp_type_id" name="lp_type_id" onchange="changeClpBasicTypes()" data-placeholder="Select Packaging Type..." tabindex="1"  style="width:350px">
				     		<option value=""></option>
				     		<%if(lpContainerTypes != null && lpContainerTypes.length > 0){ %>
				     			<%for(DBRow containerType : lpContainerTypes) {%>
				     				<option value='<%=containerType.get("type_id",0l) %>'
				     					clength="<%=containerType.get("length",0.0f) %>"
	     								cwidth ="<%=containerType.get("width",0.0f) %>" 
	     								cheight="<%=containerType.get("height",0.0f)%>"
	     								cweight="<%=containerType.get("weight",0.0f) %>"
	     								lengthuom="<%=lengthUOMKey.getLengthUOMKey(containerType.get("length_uom",0))%>"
	     								weightuom="<%=weightUOMKey.getWeightUOMKey(containerType.get("weight_uom",0))%>"
	     								lengthuomkey="<%=containerType.get("length_uom",0) %>"
	     								weightuomkey="<%=containerType.get("weight_uom",0) %>"
				     				><%=containerType.getString("type_name") %></option>
				     			<%} 
				     		}%>
				     	</select>
				     </div>
 			     </td>
			</tr>
			<tr>
				<td align="right" valign="middle" class="STYLE2">Inner Type&nbsp;:&nbsp;</td>
				<td>
					<div class="side-by-side clearfix" id="lp_types">
						<select id="sku_lp_box_type" name="sku_lp_box_type" onchange="changeContainer();" data-placeholder="Choose a InnerType..." tabindex="1"  style="width:350px">
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right" valign="middle" class="STYLE2" >Serialized Product&nbsp;:&nbsp;</td>
				<td align="left" valign="middle" > 
					<div class="side-by-side clearfix">
						<select id="is_has_sn" name="is_has_sn" tabindex="1"  style="width:250px">
							<%
							YesOrNotKey yesOrNotKey = new YesOrNotKey();
							ArrayList<String> yesOrNotKeys = yesOrNotKey.getYesOrNotKeys();
							 
								for(int i=0; i<yesOrNotKeys.size();i++)
								{
								
									int isHasSn = Integer.parseInt((String)yesOrNotKeys.get(i));
									int snNo = 0;
									if("".equals(sn_size)){
										snNo = yesOrNotKey.NO;
									}else{
										snNo = yesOrNotKey.YES;
									}
									String isHasSnStr = yesOrNotKey.getYesOrNotKeyName(isHasSn);
							%>
								<option value="<%=isHasSn%>" <%=isHasSn==snNo?"selected=selected":"" %> ><%=isHasSnStr %></option>
							<%		
								}
							%>
						</select>
					</div>
 			    </td>
			</tr>
		</table>
		
		 <div style="border-bottom:1px dashed  silver ; text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="STYLE3" >
		 	<span class="changeContainer"></span> Qty&nbsp;:&nbsp;(The Qty of <span class="changeContainer"></span> in CLP Type.)
		 </div>
		 
		  <table width="100%" border="0" cellspacing="3" cellpadding="2">
   			 <tr>
				 <td align="right" valign="middle" class="STYLE2">Stack Length Qty&nbsp;:&nbsp;</td>
			     <td align="left" valign="middle" >
			   	 		<input name="sku_lp_box_length" id="sku_lp_box_length" type="text" width="20px" style="height:28px;"  onkeyup="return validateNumber(this,value);"/>
			   	 </td>
			 </tr>	
			  <tr>
				 <td align="right" valign="middle" class="STYLE2" >Stack Width Qty&nbsp;:&nbsp;</td>
			     <td align="left" valign="middle" >
			   	 		<input name="sku_lp_box_width" id="sku_lp_box_width" type="text"   width="20px" style="height:28px;" onkeyup="return validateNumber(this,value);"/>
			   	 </td>
			 </tr>	
			  <tr>
				 <td align="right" valign="middle" class="STYLE2" >Stack Height Qty&nbsp;:&nbsp;</td>
			     <td align="left" valign="middle" >
			   	 		<input name="sku_lp_box_height" id="sku_lp_box_height" type="text"   width="20px" style="height:28px;" onkeyup="return validateNumber(this,value);"/>
			   	 </td>
			 </tr>
			 
			 <tr>
			   <!--  
				 <td align="right" valign="middle" class="STYLE2" >LP Type Name:</td>
			     <td align="left" valign="middle" >
			   	 		<input name="lp_name" id="lp_name" type="text"   width="20px" />
			   	 </td>
			   -->
			   	 <td align="right" valign="middle" class="STYLE2" >Weight&nbsp;:&nbsp;</td>
			     <td align="left" valign="middle" >
			        <input name="weight" id="weight" type="text" size="20px" style="height:25px;" readonly="readonly">
			        <span id="box_weight_uom"></span>
			     </td>
			 </tr>	
			 </table>
			 <div style="border-bottom:1px dashed  silver ; text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="STYLE3" >
			 
			 </div>
			 <table width="100%" border="0" cellspacing="3" cellpadding="2">
			  <tr>
			   <!--   
				 <td align="right" valign="middle" class="STYLE2" >Weight:</td>
				     <td align="left" valign="middle" >
				        <input name="weight" id="weight" type="text"  size="25px">
				        <span id="box_weight_uom"></span>
				     </td>
			   	 </td>
			   -->
			     <td align="right" valign="middle" class="STYLE2" >LP Type Name&nbsp;:&nbsp;</td>
			     <td align="left" valign="middle" >
			   	 	<input name="lp_name" id="lp_name" type="text" style="width:350px;height:28px;" />  
			   	 </td>	 
			 </tr>
			 <input name="pc_weight" id="pc_weight" type="hidden"  value="<%=product.get("weight",0.0f)%>"  width="20px" />
			 <input name="box_length" id="box_length" type="hidden"   width="20px" />
			 <input name="box_width" id="box_width" type="hidden"   width="20px" />
			 <input name="box_height" id="box_height" type="hidden"   width="20px" />
			 <input name="box_weight" id="box_weight" type="hidden"  value="0"  width="20px" />
			 
			 <input name="pc_length" id="pc_length" type="hidden"  value="<%=product.get("length",0.0f)%>"  width="20px" />
			 
			 <input name="pc_width" id="pc_width" type="hidden"  value="<%=product.get("width",0.0f)%>"  width="20px" />
			 
			 <input name="pc_heigtht" id="pc_heigth" type="hidden"  value="<%=product.get("heigth",0.0f)%>"  width="20px" />
			 
			 <input name="Inner_Type_length" id="Inner_Type_length" type="hidden"   width="20px" />
			 <input name="Inner_Type_width" id="Inner_Type_width" type="hidden"   width="20px" />
			 <input name="Inner_Type_height" id="Inner_Type_height" type="hidden"   width="20px" />
			 <input name="Inner_Type_weight" id="Inner_Type_weight" type="hidden"  value="0"  width="20px" />
			 <input name="Inner_Type_length_uom" id="Inner_Type_length_uom" type="hidden"  value="0"  width="20px" />
			 <input name="Inner_Type_weight_uom" id="Inner_Type_weight_uom" type="hidden"  value="0"  width="20px" />
	 
			</table>
			
   		 
   		 <div id="showValue" style="border:1px solid silver;margin: 5px; padding:5px;text-align: left;">
   		 	<p class="margin_p" style="margin-top: -5px;" id="boxtype"></p>
   		 	<p class="margin_p" style="margin-top: -5px;" id="box_number"></p>
   		 </div>
<!--  </fieldset> -->
</td>
</tr>
</table>
</form>	
</body>
</html>

