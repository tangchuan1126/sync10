<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.fr.base.core.json.JSONObject,com.cwc.app.floor.api.zyj.service.*,com.cwc.app.floor.api.zyj.model.*,net.sf.json.JSONArray"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.key.ContainerTypeKey"%>
<%@include file="../../include.jsp"%>

<%

	int container_type = StringUtil.getInt(request, "container_type");
	long pcid = StringUtil.getLong(request,"pcid");
	int type_id	= StringUtil.getInt(request, "type_id");	
	int is_has_sn = StringUtil.getInt(request, "is_has_sn");
	long detail_type = StringUtil.getInt(request, "detail_type");
	//int box_type_id = StringUtil.getInt(request, "box_type_id");
	//this is a container label
	long lable_template_type = 2;
	
	
	//获取登录人
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 	long login_id=adminLoggerBean.getAdid();
	if(login_id==100198){
		login_id=0;
	}
	
	Map<String,Object> loginUser = (Map<String,Object>) session.getAttribute("adminSesion");
	int corporationType = (Integer)loginUser.get("corporationType");
	int customerId = 0;
	int titleId = 0;
	//存放登录用户类型，表示用户是admin,是Customer,还是Title或者其它
	String userType = "";
	if(corporationType == 1){
		customerId = (Integer)loginUser.get("corporationId");
		userType = "Customer";
	}else if(Boolean.TRUE.equals(loginUser.get("administrator"))){
		userType = "Admin";
	}
    ProductCustomerSerivce  productCustomerService = (ProductCustomerSerivce)MvcUtil.getBeanFromContainer("productCustomerSerivce");
    CustomerService customerService = (CustomerService)MvcUtil.getBeanFromContainer("customerService");
    TitleService titleService = (TitleService)MvcUtil.getBeanFromContainer("titleService");
    List<Customer> customers = null;
    List<Title> titles = null;
    if(!userType.equals("Customer")){
        customers = customerService.getCustomersByProdAndCLP((int)pcid, type_id);
        if(customers.size() == 0){
        	customers = customerService.getCustomers((int)pcid);
        }
        titles = titleService.getTitlesByProdAndCLP((int)pcid, type_id);
        if(titles.size() == 0){
        	titles = titleService.getTitles((int)pcid);
        }
    }else{
        Customer customer = customerService.getCustomer(customerId);
        customers = new ArrayList<Customer>();
        if(customer != null){
        	customers.add(customer);
        }
        titles = titleService.getTitlesByProdAndCLP((int)pcid, type_id,customerId);
        if(titles.size() == 0){
        	titles = titleService.getTitles((int)pcid, customerId);
        }
    }
    JSONArray customers_json = new JSONArray();
    customers_json.addAll(customers);
    JSONArray titles_json = new JSONArray();
    titles_json.addAll(titles);
    
    
	DBRow[] rows = customSeachMgrGql.getLableLableTemplateByType(pcid, login_id, lable_template_type, detail_type, request);
	
	
	//关联的容器模板替换值  gql 2015/04/27
	String date = new TDate().getFormateTime(DateUtil.NowStr(), "MM/dd/yy");
//	 DATE TYPEID PCODE LOT CUSTOMER TITLE TOTPDT TOTPKG PIECE PACKAGES CONID CLPTYPE
	//String title = null;	
	//String customer = "";	
	String piece = null;
	String packeges = null;
	JSONObject datas = new JSONObject();
	datas.put("DATE", date);
	datas.put("CUSTOMER", "&nbsp;");
	
// 	DBRow row = clpTypeMgrZr.findContainerById(id);	
	datas.put("LOT", "&nbsp;");
// 	DBRow titleRow=proprietaryMgrZyj.findProprietaryByTitleId(row.get("title_id",0l));
	//title = "&nbsp;";	
  //	datas.put("TITLE", title);
	

	 if(detail_type == ContainerTypeKey.CLP){
 		DBRow clp=clpTypeMgrZr.selectContainerClpById(type_id);
// 		DBRow blpRow=clpTypeMgrZr.selectContainerBlpById(clp.get("inner_pc_or_lp",0l));
		//piece = (clp.get("inner_pc_or_lp",0l)==0)?"Product":("CLP&nbsp;"+blpRow.get("stack_length_qty",0l)+"X"+blpRow.get("stack_width_qty",0l)+"X"+blpRow.get("stack_height_qty",0l));
// 		piece = (clp.get("inner_pc_or_lp",0l)==0) ? "Product" : blpRow.get("lp_name","");
// 		title = (null!=titleRow)?titleRow.getString("title_name"):"&nbsp;";
		datas.put("TITLE", "&nbsp;");
		datas.put("TYPEID", clp.get("type_name", "")); //datas.put("TYPEID", clp.get("ltp_id",0l)); clp:license_plate_type,lp_name; tlp：container_type，type_name
		
 		String p_code = rows!=null && rows.length>0 ? rows[0].get("main_code", "") : "";
		
		datas.put("PCODE",p_code); 
//		datas.put("NAME", clp.getString("p_name"));
//		datas.put("CONID", "&nbsp;");	
		datas.put("TOTPDT", clp.get("inner_total_pc",0l));
		datas.put("TOTPKG", clp.get("stack_length_qty",0l) + "*" + clp.get("stack_width_qty",0l) + "*" + clp.get("stack_height_qty",0l));
		datas.put("PIECE", "&nbsp;");
		datas.put("CLPTYPE", clp.get("lp_name",""));
		datas.put("PACKAGES", "&nbsp;");
	}else if(detail_type == ContainerTypeKey.TLP){
		
	}
	
	
	//手动选择title和customer时，构造选项，gql 2015/04/27
	String title_options = "";
	boolean one_title_flag = (titles.size() == 1);
	for (Title title : titles) {
		String title_name = title.getName();
		long title_id = title.getId();
		String selected = "";
		if(one_title_flag){
			selected = "selected='selected'";
		}
		title_options += "<option value='"+ title_id+"' " + selected + ">"+title_name+"</option>";
	}
	
	String customer_options = "";
	boolean one_customer_flag = customers.size() == 1;
	for (Customer customer : customers) {
		String selected = "";
		if(one_customer_flag || customer.getId() == customerId){
			selected = "selected='selected'";
		}
		customer_options += "<option value='"+ customer.getId() +"' "+selected+">"+ customer.getName() +"</option>";
	}  
	
	//关联的容器模板替换值  gql 2015/04/27  end
	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Batch create and print CLP</title>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<!-- 替换标签占位符 -->
<script type="text/javascript" src="../js/labelTemplate/assembleUtils.js"></script>
<link rel="stylesheet" type="text/css" href="../js/labelTemplate/assembleUtils.css"/>
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<!-- 时间控件 -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<!-- select2选项卡 -->
<link rel="stylesheet" href="../js/select2-4.0.0-rc.2/dist/css/select2.css">
<script type="text/javascript" src="../js/select2-4.0.0-rc.2/dist/js/select2.min.js"></script>

<script type="text/javascript">
var productId = "<%=pcid %>";
var box_type_id = "<%=type_id %>";
$(function(){
	//初始化时，替换值
	var datas = <%=datas%>;	
	var divs=$("div[name='lableContent']");
	for(var i=0;i<divs.length;i++){
		var content = $(divs[i]).html();
		$(divs[i]).html(assembleTemplate(datas, content));
	}

	//初始化标签时间显示可修改状态 gql 
	if($("input[name='input_date']")){
		$("span[name='dateShow']").css("display","none");
		$("input[name='input_date']").css("display","");
		$("span[name='dateShow']").html('<%=date%>');
		$("input[name='input_date']").val('<%=date%>');
		//添加时间控件		
		$("input[name='input_date']").each(function(i, elem){
			$(this).datepicker({
				dateFormat:"mm/dd/y",
				changeMonth: true,
				changeYear: true
			}).bind("change",function(){
		    	  var input_date = $(this).val();
		    	  var $obj = $(this).prev("span[name='dateShow']").html(input_date);
		      });
		});
		$("#ui-datepicker-div").css("display","none");
	}
	//TLP标签，初始化pcode可修改状态，并且修改时，修改条码,注意：修改打印内容的date/pcode页面布局时，要修改此处  gql 
	<%if(detail_type==ContainerTypeKey.TLP){ %>
		$("input[name='pcodeInput']").css("display","");
		$("div[name='pcodeText']").css("display","none");
		//pcode输入框值改变时，修改pcode条码和pcode显示内容
		$("input[name='pcodeInput']").bind('change',function(){
		 	var $this = $(this);
		 	var $img =  $this.parent("div").parent("td").find("img[name='pcodeImg']");
		 	var $div =  $this.parent("div").parent("td").find("div[name='pcodeText']");
		 	var pcode_val = $this.val();
		 	var src = '/barbecue/barcode?data='+pcode_val+'&width=1&height=50&type=code39';
		 	$img.attr("src",src);
		 	$div.html(pcode_val);
		});
	<%}%>
	
	//初始化customer手动选择，隐藏打印时的div
	var customer_options = "<%=customer_options%>";
// 	var placeholder = customer==""?" ":customer;
	$("select[name='select_customer']").append(customer_options);
	$("div[name='show_customer']").css("display","none");
	$("select[name='select_customer']").each(function(i, elem){
		$(this).select2({
		 placeholder: "Select..."
        ,allowClear: true
      }).bind("change",function(event){
    	  var customerId = $(event.target).val();
    	  var customer = $(this).children("option[value='" + customerId + "']").text();
    	  $(this).prev("div[name='show_customer']").find("center").html(customer+"&nbsp;&nbsp;");
    	  
    	  if(customerId == ""){
    		  customerId = 0;
    	  }
    	  
          var target = event.target;
          $.ajax({
               type : "get",
               url  : "/Sync10/basicdata/productCustomer/" + productId + "/" + box_type_id + "/" + customerId + "/title",
               success : function(data){
            	   var titleId =  $(target).parent().next().find("select[name='select_title']").val();

                   if(data && data.length > 0){
	            	   //标识当前选中的Title 是否出现在新的Title 列表中
	            	   var exist = false;
                	   $(target).parent().next().find("select[name='select_title']").children().remove("option[value!='']");
                	   
                       for(var i = 0; i < data.length; i++){
                           if(titleId == data[i].id){
                        	   exist = true;
                           }
                           $(target).parent().next().find("select[name='select_title']").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
                       }
                       if(!exist){
                    	 	$(target).parent().next().find("span.select2-selection__rendered").html("<span class=\"select2-selection__placeholder\">Select...</span>");
                    	 	$(target).parent().next().find("div[name='show_title']").find("center").html("");
                       }else{
                    	   $(target).parent().next().find("select[name='select_title']").val(titleId);
                       }
                   }
               }
          });            	  

      });
	  var customer2 = $(this).children("option[value='" + $(this).val() + "']").text();
	  $(this).prev("div[name='show_customer']").find("center").html(customer2+"&nbsp;&nbsp;");
	});
	
	//初始化title手动选择，隐藏打印时的div
	var html = "<%=title_options%>";
	
	$("select[name='select_title']").append(html);
	$("div[name='show_title']").css("display","none");
	$("select[name='select_title']").each(function(i, elem){
		$(this).select2({
			 placeholder: "Select..."
	        ,allowClear: true
      }).bind("change",function(){
    	  var title = $(this).children("option[value='" + $(this).val() + "']").text();
    	  $(this).prev("div[name='show_title']").find("center").html(title+"&nbsp;");
      });
	  var title2 = $(this).children("option[value='" + $(this).val() + "']").text();
	  $(this).prev("div[name='show_title']").find("center").html(title2);
	});
		
});

</script>

</head>
<body>
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			Add And Print [<%=new ContainerTypeKey().getContainerTypeKeyValue(1) %>]
		</legend>	
		<form action="" id="subForm" method="post">
		<input type="hidden" name="container_type" value="<%=container_type %>"/>
		<input type="hidden" name="type_id" value="<%=type_id %>"/>
		<input type="hidden" name="is_has_sn" value="<%=is_has_sn%>"/>
			<table>
				<tr>
					<td>Print Quantity</td>
					<td>
						<input name="print_count" id="print_count" />
						<span style="color:red">(Please enter the quantity to print batch)</span>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<br/>
	<div style="border:0px solid red;" align="center">
		<%for(int i=0;i<rows.length;i++){							
		%>
			<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="lableshow">
<!-- 				<tr onclick="printPrev(this)"> -->
				<tr>
					<td align="left" valign="middle" style="padding:10px;padding-left:25px;border-bottom:2px #eeeeee solid;">
						<div name="lableContent" style="background: #FFF;margin-left: 30px;margin-top: 30px;padding:0px;float: left;width:390px;height:{{height}}px;position:relative;text-align:center;border:1px solid red;" >
							<%=rows[i].get("lable_content","")%>
						</div>							
					</td>
					<td align="left" valign="middle" style="padding:10px 10px 10px 0px;border-bottom:2px #eeeeee solid;">
						<input type="hidden" id="printName_<%=i%>" name="printName" value="<%=rows[i].get("print_name","")%>" />
						<input value="  Print Label" type="button"  class="long-button-print" onclick="printBarCode(this,'<%=i%>');"/>
					</td>
				</tr>
			</table>
		<%}%>
		<%if(rows==null || rows.length==0){ %>
			<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="lableshow">
				<tr>
					<td style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;" colspan="3">NO Data</td>
				</tr>
			</table>
		<% }%>
	</div>
	<div id="av" align="left" style="width:368px; border:1px solid red;display:none;"></div>
	<!-- 打印内容  gql-->
	<div name="printAll" id="printAll" style="display: none"></div>
	<!-- gql end -->
</body>
<script type="text/javascript">
	//设置打印内容  gql,方法写在assembleUtils.js中，2015/05/05修改
	function setPrintAll(obj){
		var $obj = $(obj).parent("td").prev("td").find("div[name='lableContent']");
		var html = $obj.html();//获取打印的内容
		$("#printAll").html(html);//赋值打印的内容
		
		setClpPrintAll($("#printAll"));//设置clp打印的样式
	}
	
	
 // 	修改之前，点击标签替换值，后直接打印
	function printBarCode(targetTr, index){
		if(validateCount()){
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/container/ContainerAddBatchAction.action',
				data:$("#subForm").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.length > 0){
						for(var i = 0; i < data.length; i ++){
							$.ajax({
								url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/container/AquireContainerMessagePrintAction.action',
								type: 'post',
								dataType: 'html',
								data:'id='+data[i].con_id+'&detail_type='+'<%=detail_type%>'+'&pcid='+'<%=pcid%>',
								async:false,
								success: function(datas){
// 									var contentDiv = $(targetTr).find("div[name='lableContent']");
									//console.log(contentDiv.html());
// 									var contentPrint = assembleTemplate(JSON.parse(datas), contentDiv.html());

									setPrintAll(targetTr);
									var contentPrint = assembleTemplate(JSON.parse(datas), $("#printAll").html());
									printInstantly(contentPrint,index);
								}
							});	
							
						}
						$.artDialog && $.artDialog.close();
					}else{
						showMessage("submit unsuccessfully","alert");
					}
				},
				error:function(){
					showMessage("System error, please try again later","error");
				}
			});	
		}
	};
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function validateCount(){
		if('' == $("#print_count").val()){
// 			alert("Please input the quantity");
			showMessage("Please input the quantity","alert");
			return false;
		}
		return true;
	}
	
	//打印
	function printInstantly(html, index){
		
    	var printer_count =  visionariPrinter.GET_PRINTER_COUNT(); //获取打印机名字列表
    	
	    //判断是否有该名字的打印机
//     	var printer = "LabelPrinter";
    	 var printer = $("#printName_"+index).val();
  console.log(printer);
    	var printerExist = "false";
    	var containPrinter = printer;
		for(var i = 0;i<printer_count;i++){
			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 )
			{
				printerExist = "true";
				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
				break;
			}
		}
		if(printerExist=="true"){
				//判断该打印机里是否配置了纸张大小
				var paper="102X152";
			    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
			    var str=strResult.split(",");
			    var status=false;
			    for(var i=0;i<str.length;i++){
                       if(str[i]==paper){
                          status=true;
                       }
				}
			    if(status==true){
                   	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Container Label");
                   	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                     visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
   				     visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",html);
   					 visionariPrinter.SET_PRINT_COPIES(1);
   //				     visionariPrinter.PREVIEW();
    					 visionariPrinter.PRINT();
               }else{
            	   $.artDialog.confirm("Please Change Paper", function(){
        				 this.close();
            			}, function(){
        			});
               }	
		}else{
			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
			if(op!=-1){ //判断是否点了取消
				 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Container Label");
               	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
				 visionariPrinter.ADD_PRINT_HTM(5,3,"97%","100%",html);
   				 visionariPrinter.SET_PRINT_COPIES(1);
//    				 visionariPrinter.PREVIEW();
   				 visionariPrinter.PRINT();
			}	
		}
	}
	
</script>



</html>
