<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.api.zr.BoxTypeMgrZr"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<jsp:useBean id="containerHasSnTypeKey" class="com.cwc.app.key.ContainerHasSnTypeKey"></jsp:useBean>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>BOX TYPE</title>
<style type="text/css">
.set {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
.title{font-size:12px;color:green;font-weight:blod;}
a.common{width:16px;height:16px;display:block;float:left;border: 1px solid silver ; margin-left: 3px;}
a.up{background-image: url('../imgs/arrow_up.png');}
a.down{background-image: url('../imgs/arrow_down.png');}
td .box{float:left;padding:4px;}
td .left{text-align: right;}
td .right{text-align: left;}
td .leftWidth1{width:150px;}
td .leftWidth2{width:100px;}
td .rigthWidth1{width:170px;}
td .rigthWidth2{width:90px;}
td .even .box{border-bottom:1px solid #ddd;border-top:1px solid #ddd;}
td .clear{clear:both}
</style>
<%
String cmd = StringUtil.getString(request,"cmd");
String search_key = "" ;
long pcid = StringUtil.getLong(request,"pc_id");
int basic_container_type = StringUtil.getInt(request,"basic_container_type");
DBRow product = orderMgrZr.getDetailById(pcid,"product","pc_id");
DBRow[] boxs = boxTypeMgrZr.getBoxTypesByPcIdAndContainerTypeId(pcid,basic_container_type);
String deleteBoxType =  ConfigBean.getStringValue("systenFolder")+"action/administrator/box/DeleteTypeAction.action";
ContainerTypeKey containerTypeKey = new ContainerTypeKey();
String ExportBoxTypeAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/ExportBoxSkuTypeAction.action";
String exChangeAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/ExchangeBoxTypeSortAction.action";
%>
<script type="text/javascript">
	var isOpenUpFileBack = false ;
 
	function addNew(){
		var basic_type = $("#container_type_id").val();
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_add.html?pcid="+'<%= pcid%>&basic_type='+basic_type; 
		$.artDialog.open(uri , {title:'['+'<%= product.getString("p_name")%>'+ '] Add BOX Type',width:'630px',height:'470px', lock: true,opacity: 0.3,fixed: true});
	}
	function update(id){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_update.html?type_id="+id; 
		$.artDialog.open(uri , {title: '修改盒子类型',width:'530px',height:'480px', lock: true,opacity: 0.3,fixed: true});
	}
	function deleteType(id){
	    $.artDialog.confirm('Are you sure to delete?', function(){
		     $.ajax({
				 url:'<%= deleteBoxType%>' + "?type_id="+id,
				 dataType:'json',
				 beforeSend:function(request){
			      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			    },
				 success:function(data){
					$.unblockUI();
				 	if(data != null && data.flag === "success"){
		    		 	 window.location.reload();
				 	}else{
				 	   showMessage("System error,please try it again.","error");
					}
				 },
				 error:function(){
				     $.unblockUI();
				     showMessage("System error,please try it again.","error");
				 }
			  })
		}, function(){
		});
	}
	function importBoxType(_target){
	    var targetNode = $("#"+_target);
	    var fileNames = $("input[name='file_names']",targetNode).val();
	    var obj  = {
		     reg:"xls",
		     limitSize:2,
		     target:_target,
		     limitNum:1
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
			 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					  this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
			 }});
	}
	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,_target){
	    var targetNode = $("#"+_target);
	    $("input[name='file_names']",targetNode).val(fileNames);
		if($.trim(fileNames).length > 0 ){
 			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/check_box_show.html?fileNames="+fileNames;
			$.artDialog.open(uri,{title: "检测上传Box信息",width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true});
			isOpenUpFileBack  = true ;
		}
	}
	function clearFile(){
	    $("input[name='file_names']").val("");
	}
	function exportBoxType(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_export_select.html";
		$.artDialog.open(uri,{title: "导出类型选择",width:'300px',height:'400px', lock: true,opacity: 0.3,fixed: true});
	}
	function showExportMenu(_this){
		//获取位置
			var x = $(_this).offset().left;
			var y = $(_this).offset().top;
	    $('#mm').menu('show', {
			left: x-30,
			top: y+20
		}); 
	}
	function exportAll(){
	    $.artDialog.confirm('确认导出所有的记录?', function(){
			exportTemplate("all");
			
		}, function(){
		});
		
	}
	function exportSelect(){
		var export_select = $("#export_select option:selected");
		var cmd = export_select.attr("cmd");
		if(cmd){
			if(cmd === "all"){
			    exportAll();
			}else{
			    exportTemplate(cmd);
			 }
		}
	}
	function exportTemplate(cmd){
	 	 cmd = cmd ? cmd :"template" ;
		 $("#changeCmd").val(cmd);
		 ajaxExport($("#dataForm").serialize());
	}
	function ajaxExport(para){
		$.ajax({
			url:'<%=ExportBoxTypeAction%>',
			type:'post',
 			dataType:'json',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request){
				$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">正在导出，请稍后......</div>'});
		   },
			error:function(){
				$.unblockUI();
				showMessage("导出数据失败,请重试!","error");
		   },
		   success:function(msg){
			   
				if(msg && msg.flag == "success" && msg["fileurl"].length > 0){
					$.unblockUI();
					window.location="../../"+msg["fileurl"];
				 
				}else{
					$.unblockUI();
					showMessage("此选项下没有数据,无法导出","alert");
					
		   }
		  }
		});
	}
	function addBoxByType(box_type_id,box_type_name){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_container_add.html?box_type_id="+box_type_id;
		$.artDialog.open(uri,{title: "Add["+box_type_name+"] BOX Type",width:'500px',height:'400px', lock: true,opacity: 0.3,fixed: true});
	}
	function showBoxContainers(box_type_id,box_type_name){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_container_list.html?box_type_id="+box_type_id+"&pcid="+'<%=pcid%>';
		$.artDialog.open(uri,{title: "Manage [<span style='color:#f60'>"+box_type_name+"</span>] BOX Type Container",width:'750px',height:'400px', lock: true,opacity: 0.3,fixed: true});
	}
	function exchange(change,changeTo){
		$.ajax({
			url:'<%=exChangeAction%>' + "?change="+change + "&changeTo="+changeTo,
			type:'post',
			dataType:'json',
			timeout: 60000,
			cache:false,
 
			beforeSend:function(request){
				$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">正在调整顺序...</div>'});
		   },
			error:function(){
				$.unblockUI();
				showMessage("调整默认顺序失败,请重试!","error");
		   },
		   success:function(msg){
		       $.unblockUI();
			   if(msg && msg.flag === "success"){
	    		 	 window.location.reload();
			   }else{ showMessage("调整默认顺序失败,请重试!","error"); }
				 
		   }
		 
		});
 	}

	function addilp(container_type_id){
		 var cmd='query';
		 var pc_id=<%=pcid%>;
		 var product_name='<%= product.getString("p_name")%>';
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/ilp_sku_list.html?pc_id="+pc_id+"&product_name="+product_name+"&container_type_id="+container_type_id; 
		 $.artDialog.open(uri , {title:"["+ product_name +"]"+ "ILP类型添加",width:'830px',height:'480px', lock: true,opacity: 0.3,fixed: true});
	}
	function print(container_type, type_id, is_has_sn)
	{
		//var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch_b.html?container_type="+container_type+"&type_id="+type_id+"&is_has_sn="+is_has_sn;
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch_common.html?container_type="+container_type+"&type_id="+type_id+"&is_has_sn="+is_has_sn+"&detail_type=2&pcid="+'<%=pcid%>';
		$.artDialog.open(url , {title: 'Batch print BLP container',width:'700px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	}
</script>
</head>
<body onload = "onLoadInitZebraTable()">
	 <table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="true" isBottom="true" class="zebraTable" style="margin-left:3px;margin-top:2px;">
			  <tr> 
  			  	<th width="55%" style="vertical-align: center;text-align: center;" class="right-title">BLP Type Info</th>
		        <th width="35%" style="vertical-align: center;text-align: center;" class="right-title">BLP Basic Container Type Info</th>
		    	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Operation</th>
		      </tr>
		      <tbody>
		      	<%if(boxs != null && boxs.length > 0){
		      		for(int index = 0 , count = boxs.length ; index < count ; index++ ){
		      			DBRow box = boxs[index];
		      			boolean isFirst = (index == 0) ;
 						boolean isLast = (index ==( boxs.length -1)) ;
		      			box.add("p_name",product.getString("p_name"));
		      		%>
		      		
		      		<tr>
		      			<td style="padding: 10px;">
		      			 	<div >
				      			<div class="box left leftWidth1"><span>BLP Basic Type:</span></div>
				      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><%=box.getString("type_name") %></span></div>
				      			<div class="clear"></div>
				      		</div>
		      			 	<div >
				      			<div class="box left leftWidth1"><span>BLP Type Name:</span></div>
				      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF">[<%= boxTypeMgrZr.getBlpTypeName(box) %>]</span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<div >
				      			<div class="box left leftWidth1"><span>Inner has SN:</span></div>
				      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><%=containerHasSnTypeKey.getContainerHasSnTypeKeyValue(box.get("is_has_sn",0)) %></span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<div >
				      			<div class="box left leftWidth1"><span>Stack Length Qty:</span></div>
				      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><%= box.get("stack_length_qty",0)%></span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<div >
				      			<div class="box left leftWidth1"><span>Stack Width Qty:</span></div>
				      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><%= box.get("stack_width_qty",0)%></span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<div >
				      			<div class="box left leftWidth1"><span>Stack Height Qty:</span></div>
				      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><%= box.get("stack_height_qty",0)%></span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<%if(0!=box.get("inner_pc_or_lp",0l)){
				      		
								DBRow lipRow = boxTypeMgrZr.findIlpByIlpId(box.get("inner_pc_or_lp",0l));
								String ilpName = lipRow.get("stack_length_qty",0l) + "*" + lipRow.get("stack_width_qty",0l) + "*"+ lipRow.get("stack_height_qty",0l);
							%>
								<div >
					      			<div class="box left leftWidth1"><span>Inner ILP Total:</span></div>
					      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><%= box.get("inner_total_lp",0)%></span></div>
					      			<div class="clear"></div>
					      		</div>	
					      		<div >
					      			<div class="box left leftWidth1"><span>Inner ILP Type:</span></div>
					      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><a href="javascript:void(0)" onclick="addilp(<%=box.get("box_inner_type",0l)%>)">[<%=ilpName %>]</a></span></div>
					      			<div class="clear"></div>
					      		</div>
					      		<%} %>	 
					      		<div >
					      			<div class="box left leftWidth1"><span>Inner product Total:</span></div>
					      			<div class="box right rigthWidth1"><span style="font-weight:bold; color:#0066FF"><%= box.get("inner_total_pc",0)%></span></div>
					      			<div class="clear"></div>
					      		</div>
		      			</td>
		      			<td>
		      			<%String length_uom = "("+lengthUOMKey.getLengthUOMKey(box.get("length_uom", 0))+")";%>
	      				<%String weight_uom = "("+weightUOMKey.getWeightUOMKey(box.get("weight_uom", 0))+")";
	      				%>
		      				<div >
				      			<div class="box left leftWidth2"><span>Lengh:</span></div>
				      			<div class="box right rigthWidth2"><span style="font-weight:bold; color:#0066FF"><%= box.get("ibt_length",0d)+"&nbsp;"+length_uom%></span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<div >
				      			<div class="box left leftWidth2"><span>Width:</span></div>
				      			<div class="box right rigthWidth2"><span style="font-weight:bold; color:#0066FF"><%= box.get("ibt_width",0d)+"&nbsp;"+length_uom%></span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<div >
				      			<div class="box left leftWidth2"><span>Height:</span></div>
				      			<div class="box right rigthWidth2"><span style="font-weight:bold; color:#0066FF"><%= box.get("ibt_height",0d)+"&nbsp;"+length_uom%></span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<div >
				      			<div class="box left leftWidth2"><span>Weight:</span></div>
				      			<div class="box right rigthWidth2"><span style="font-weight:bold; color:#0066FF"><%= box.get("ibt_weight",0d)+"&nbsp;"+weight_uom%></span></div>
				      			<div class="clear"></div>
				      		</div>
		      			</td>
		      			<td style="padding:10px;">
		      			<!--  
		      				<input class="short-short-button-mod" type="button" onclick="update('<%=box.get("box_type_id",0) %>');" value="修改" name="Submit1"> 
		      				<br />
		      				<br />
							<input class="short-short-button-del" type="button" onclick="deleteType('<%=box.get("box_type_id",0) %>');" value="删除" name="Submit2">
							<br />
							<br />
						-->	
							 <input class="long-button" type="button" onclick="showBoxContainers('<%=box.get("lpt_id",0) %>','<%= boxTypeMgrZr.getBlpTypeName(box) %>');" value="Manage BLP">
							<br />
							<br />
							 <input class="long-button" type="button" onclick="print('<%=ContainerTypeKey.BLP %>','<%=box.get("lpt_id",0)%>','<%=box.get("is_has_sn",0)%>')" value="Print Add BLP">
							<br />
							<br />
								<%if(!isFirst && count > 1){ %>
									<a class="common up" title="Up" onclick="exchange('<%=box.get("lpt_id",0l) %>','<%=boxs[index-1].get("lpt_id",0l)%>')" href="javascript:void(0)"></a> 
								<%} %>
								<%if(!isLast && count > 1  ){ %>
									<a class="common down" title="Down" onclick="exchange('<%=box.get("lpt_id",0l) %>','<%=boxs[index+1].get("lpt_id",0l)%>')" href="javascript:void(0)"></a> 
								<%} %>
		      			</td>
		      		</tr>
		      			<tr class="split">
		      				<td colspan="4" align="right"></td>
		      			</tr>
		      	<%	}
		      	}else{%>
		      		<tr>
		      		 	<td colspan="4" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Datas</td>
		      			
		      		</tr>
		      	<%} %>
		      		
		      </tbody>
	</table>
 
	<script type="text/javascript">
	  $("#tabs").tabs({
			cache: true,
			cookie: { expires: 30000 } 
		});
	</script>
	<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
</body>
</html>