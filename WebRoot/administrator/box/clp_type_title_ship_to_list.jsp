<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="yesOrNotKey" class="com.cwc.app.key.YesOrNotKey"></jsp:useBean>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<html>
<head>
<title>CLP TYPE TITLE SHIP TO</title>
<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript" ></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />


<link rel="stylesheet" type="text/css" href="../buttons.css"  />

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 
<%
   long pc_id=StringUtil.getLong(request,"pc_id");

   long title_id = StringUtil.getLong(request,"title_id");
   long customer_id = StringUtil.getLong(request,"customer_id");
   long ship_to_id = StringUtil.getLong(request,"ship_to_id");
   String titleName = StringUtil.getString(request,"titleName");
   String customerName = StringUtil.getString(request,"customerName");
   String shipToName = StringUtil.getString(request,"shipToName");
   
   String productName=StringUtil.getString(request,"productName");
   
   int active = StringUtil.getInt(request, "active");
   
   DBRow[] lpRow = boxTypeMgrZr.getClpTypesByTitleAndShipTo(pc_id, title_id,customer_id, ship_to_id, active); 
   
   String lockChangeAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/LockchangeBoxTypeAction.action";
   
   String exchangeClpToPsSortAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/ExchangeClpToPsSortAction.action";
   
   //启用状态
   //DBRow allRow = new DBRow(); allRow.add("active", 0L); allRow.add("active_name", "All");
   DBRow activeRow = new DBRow(); activeRow.add("active", 1L); activeRow.add("active_name", "Active");
   DBRow inactiveRow = new DBRow(); inactiveRow.add("active", 2L); inactiveRow.add("active_name", "Inactive");
   DBRow[] actives = new DBRow[]{/* allRow, */ activeRow, inactiveRow};
%>
<script type="text/javascript">
jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
    $("#tabs").tabs("select",0);  //默认选中第一个
    $(function(){
 		$(".chzn-select").chosen({no_results_text: "no this options:"});
 	});
    
});
function print(container_type, type_id, is_has_sn){
	//var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch_i.html?container_type="+container_type+"&type_id="+type_id+"&is_has_sn="+is_has_sn;
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch_common.html?container_type="+container_type+"&type_id="+type_id+"&is_has_sn="+is_has_sn+"&detail_type=1&pcid="+'<%=pc_id%>'; 
	$.artDialog.open(url , {title: 'Batch print ILP container',width:'700px',height:'450px', lock: true,opacity: 0.3,fixed: true});
}

function searchActive()
{
	$("#active").val($("#sku_lp_active").val());
	$("#toolForm").submit();
}
</script>
<style type="text/css">
td .box{float:left;padding:4px;}
td .left{text-align: right;}
td .right{text-align: left;}
td .leftWidth1{width:120px;}
td .leftWidth2{width:90px;}
.rigthWidth1 span{
width:175px;
display: block;
word-break: break-all;
white-space: pre-wrap;}
.rigthWidth1 a span{
width: 175px;
display: block;
word-break: break-all;
white-space: pre-wrap;
}
td .even .box{border-bottom:1px solid #ddd;border-top:1px solid #ddd;}
td .clear{clear:both}
.long-button {
	background-attachment: fixed;
	background: url(../imgs/button_long.gif);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 95px;
	color: #000000;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	margin-right:3%;
	margin-top:3px;
}

.content_clp_button {  
  height: 24px;
  text-align: right;
  vertical-align: top;
  background-color: #f1f1f1;
</style>
</head>
<body>
	<div id="tabs" style="width:98%;margin-left:3px;margin-top:10px;">
		<form action="" id="toolForm" name="toolForm">
		 	<input type="hidden" value="<%=active %>" name="active" id="active"/>
		 	<input type="hidden" value="<%=title_id %>" name="title_id" id="title_id"/>
		 	<input type="hidden" value="<%=ship_to_id %>" name="ship_to_id" id="ship_to_id"/>
		 	<input type="hidden" value="<%=titleName %>" name="titleName" id="titleName"/>
		 	<input type="hidden" value="<%=shipToName %>" name="shipToName" id="shipToName"/>
		 	<input type="hidden" value="<%=pc_id %>" name="pc_id" id="pc_id"/>
		 	<input type="hidden" value="<%=productName %>" name="productName" id="productName"/>
		 	<input type="hidden" value="<%=customer_id %>" name="customer_id" id="customer_id"/>
		 	<input type="hidden" value="<%=customerName %>" name="customerName" id="customerName"/>
		</form>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<%if(title_id != 0){%>
			<tr>
				<td>
				<div>
		      		<div class="box left leftWidth2"><span>Title:</span></div>
		      		<div class="box right"><span style="color:#0066FF"><%=titleName %></span></div>
		      		<div class="clear"></div>
		      	</div>
				</td>
			<!-- </tr> -->
			<% } %>

			<%if(customer_id != 0){%>
			<!-- <tr> -->
				<td>
				<div>
		      		<div class="box left leftWidth2"><span>Customer:</span></div>
		      		<div class="box right"><span style="color:#0066FF"><%=customerName %></span></div>
		      		<div class="clear"></div>
		      	</div>
				</td>
			<!-- </tr> -->
			<% } %>
			
			<%if(ship_to_id != 0){%>
			<!-- <tr> -->
				<td>
				<div>
		      		<div class="box left leftWidth2"><span>Ship To:</span></div>
		      		<div class="box right"><span style="color:#0066FF"><%=shipToName %></span></div>
		      		<div class="clear"></div>
		      	</div>
				</td>
			<!-- </tr> -->
			<% } %>
			
				<td width="25%">
		        	<div class="side-by-side clearfix" style="">
			 	    <select id="sku_lp_active" onchange="searchActive()" class="chzn-select" data-placeholder="Is Active..." tabindex="1"  style="width:200px">
					<option value="0">Is Active...</option>
					<%if(actives != null && actives.length > 0){
						for(DBRow activeX : actives){
					 %>	
					  		<option <%=(activeX.get("active",0l) == active)?"selected":"" %> value='<%=activeX.get("active",-1l) %>'><%=activeX.getString("active_name") %></option>
					<%} 
					}%>
					</select>
					</div>
		        </td>
	        </tr>
		</table>
	</div>	
	<table width="99%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="true" isBottom="true" class="zebraTable" style="margin-left:3px;margin-top:10px;margin-bottom:49px;">
		  <tr> 
 			<th width="43%" style="vertical-align: center;text-align: center;" class="right-title">CLP Type</th>
	        <th width="40%" style="vertical-align: center;text-align: center;" class="right-title">Configuration</th>
	    	<!-- <th width="17%" style="vertical-align: center;text-align: center;" class="right-title">Operation</th> -->
	      </tr>
	     <tbody>
	     <%if(lpRow.length > 0){ %>
	     <%for(int i=0;i<lpRow.length;i++){ %>
	      	<tr>
	      		<td>
	      			<div>
		      			<div class="box left leftWidth1"><span>CLP Type:</span></div>
		      			<div class="box right rigthWidth1"><span style="color:#0066FF"><%=lpRow[i].getString("lp_name") %></span></div>
		      			<div class="clear"></div>
		      		</div>
	      			<div>
		      			<div class="box left leftWidth1"><span>CLP Packaging Type:</span></div>
		      			<div class="box right rigthWidth1">
		      			<a href="javascript:void(0)" style="color: green;"
							onclick="openContainerType('<%=lpRow[i].get("basic_type_id",0)%>');">
							<span style="color:#0066FF"><%=lpRow[i].getString("type_name") %></span>
						</a>
		      			</div>
		      			<div class="clear"></div>
		      		</div>
	      			<div>
		      			<div class="box left leftWidth1"><span>Serailized  Product:</span></div>
		      			<div class="box right rigthWidth1"><span style="color:#0066FF; float:left"><%=yesOrNotKey.getYesOrNotKeyName(lpRow[i].get("is_has_sn",0)) %></span></div>
		      			<div class="clear"></div>
		      		</div>
		      		<div>
		      			<div class="box left leftWidth1"><span>Weight:</span></div>
		      			<div class="box right rigthWidth1">
		      				<span style="color:#0066FF"><%=lpRow[i].get("total_weight",0f)%>&nbsp;&nbsp;<%=weightUOMKey.getWeightUOMKey(lpRow[i].get("weight_uom", 0))%><div style="float:right;"><%if(lpRow[i].get("active",0l)==1){}else{ %><img alt="lock" src="../imgs/lockadmin.gif"><%} %></div></span>
		      			</div>
		      			<div class="clear"></div>
		      		</div>
	      		</td>
	      		<td>
	      			<div>
		      			<div class="box left leftWidth2"><span>Stack:</span></div>
		      			<div class="box right rigthWidth1"><span style="color:#0066FF"><%=lpRow[i].get("stack_length_qty",0l) %> * <%=lpRow[i].get("stack_width_qty",0l) %> * <%=lpRow[i].get("stack_height_qty",0l) %></span></div>
		      			<div class="clear"></div>
		      		</div>
		      		<div>
		      			<div class="box left leftWidth2"><span>Product Qty:</span></div>
		      			<div class="box right rigthWidth1"><span style="color:#0066FF"><%=lpRow[i].get("inner_total_pc",0l) %></span></div>
		      			<div class="clear"></div>
		      		</div>
		      		<%if(lpRow[i].get("inner_pc_or_lp",0l) > 0){ 
		      			DBRow innerLp = boxTypeMgrZr.findIlpByIlpId(lpRow[i].get("inner_pc_or_lp",0l));
		      		%>
		      		<div>
		      			<div class="box left leftWidth2"><span>Inner CLP Type:</span></div>
		      			<div class="box right rigthWidth1">
		      			<a href="javascript:void(0)" style="color: green;"
							onclick="openLpType('<%=lpRow[i].get("inner_pc_or_lp",0)%>');">
							<span style="color:#0066FF"><%=innerLp.getString("lp_name") %></span>
						</a>
						</div>
		      			<div class="clear"></div>
		      		</div>
	      			<div>
		      			<div class="box left leftWidth2"><span>Inner CLP Qty:</span></div>
		      			<div class="box right rigthWidth1"><span style="color:#0066FF"><%=lpRow[i].getString("inner_total_lp") %></span></div>
		      			<div class="clear"></div>
		      		</div>
		      		<%} %>
	      		</td>
	      		
	      	</tr>
	      	<tr>
	      		<td colspan="2" class="content_clp_button" style="text-align: right;">
	      		<div class="buttons-group minor-group">
						<a class="buttons" onclick="showBoxContainers('<%=lpRow[i].get("lpt_id",0l)%>','<%=lpRow[i].get("stack_length_qty",0l)%>*<%=lpRow[i].get("stack_width_qty",0l) %>*<%=lpRow[i].get("stack_height_qty",0l)%>');" value="CLP Lookup">CLP Lookup</a>
						<a class="buttons" onclick="print('<%=ContainerTypeKey.CLP %>','<%=lpRow[i].get("lpt_id",0l)%>','<%=lpRow[i].get("is_has_sn",0)%>')" value="Print CLP Label">Print CLP Label</a>
						<%if(lpRow[i].get("active",0l)==1){ %>
						<a class="buttons" onclick="lockType('<%=lpRow[i].get("lpt_id",0l)%>','2');" value="Inactive" id="Inactive">Inactive</a>	
	                <% }else{ %>
						<a class="buttons" onclick="lockType('<%=lpRow[i].get("lpt_id",0l)%>','1');" value="Active" id="active">Active</a>	
	     			<%	} %>
	     			
	     			<%if(title_id != 0 && ship_to_id != 0){%>
	     			
	     			<%  if(lpRow.length>1){
	     					if(i==0){
	     			%>
	     				<input style="margin-top:3px" type="image" src="../imgs/down_arrow_16.png" title="Down" onclick="exchange('<%=lpRow[i].get("lp_title_ship_id",0l) %>','<%=lpRow[i+1].get("lp_title_ship_id",0l)%>','<%=lpRow[i].get("ship_to_sort",0l) %>','<%=lpRow[i+1].get("ship_to_sort",0l)%>')">
	     			<% 		}else if(i == lpRow.length-1 && i!=0){ %>
	     				<input style="margin-top:3px" type="image" src="../imgs/up_arrow_16.png" title="Up" onclick="exchange('<%=lpRow[i].get("lp_title_ship_id",0l) %>','<%=lpRow[i-1].get("lp_title_ship_id",0l)%>','<%=lpRow[i].get("ship_to_sort",0l) %>','<%=lpRow[i-1].get("ship_to_sort",0l)%>')">
	     			<%		} else{ %>
	     				<input style="margin-top:3px" type="image" src="../imgs/down_arrow_16.png" title="Down" onclick="exchange('<%=lpRow[i].get("lp_title_ship_id",0l) %>','<%=lpRow[i+1].get("lp_title_ship_id",0l)%>','<%=lpRow[i].get("ship_to_sort",0l) %>','<%=lpRow[i+1].get("ship_to_sort",0l)%>')">
	     				<span><input style="margin-top:5px" type="image" src="../imgs/up_arrow_16.png" title="Up" onclick="exchange('<%=lpRow[i].get("lp_title_ship_id",0l) %>','<%=lpRow[i-1].get("lp_title_ship_id",0l)%>','<%=lpRow[i].get("ship_to_sort",0l) %>','<%=lpRow[i-1].get("ship_to_sort",0l)%>')"></span>
	     			<%		}
	     				} }
	     			%>
	     			
     				</div>	
   				</td>
	      	</tr>
	      <%} %>
	      <%}else{%>
	      		<tr>
	      		 	<td colspan="2" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Datas</td>
	      		</tr>
	      	<%} %>
	     </tbody> 
	</table> 
	<table width="100%" height="10%" border="0" cellpadding="0" cellspacing="0" style="position:fixed;bottom:0;">
	<tr>
    <td width="60%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="40%" align="right" class="win-bottom-line">
		<a name="Submit2" value="Close" class="buttons primary big" onClick="closeWin();" style="margin-right:15px;">Close</a> 
	</td>
  	</tr>
	</table>
	<script>onLoadInitZebraTable();</script>
</body>
</html>
<script>
function openContainerType(type_id){
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/show_container_type.html?type_id="+type_id; 
    $.artDialog.open(uri , {title: "Show Basic Container Type",width:'420px',height:'350px', lock: true,opacity: 0.3,fixed: true});
}

function openLpType(innerlpId){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_show.html?productName="+'<%=productName%>&innerlpId='+innerlpId; 
	$.artDialog.open(uri , {title:'Show ['+'<%=productName %>'+ '] CLP Type',width:'560px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}

function showBoxContainers(box_type_id,box_type_name){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/ilp_container_list.html?box_type_id="+box_type_id+"&box_type_name="+box_type_name+"&pcid="+'<%=pc_id%>'; 
	$.artDialog.open(uri,{title: "Manage [<span style='color:#f60'>"+box_type_name+"</span>] CLP Type Container",width:'700px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}

function closeWin()
{
	$.artDialog.close();
}

function lockType(id,lockId){
	$.ajax({
		url:'<%=lockChangeAction%>' + "?id="+id + "&lockId="+lockId,
		type:'post',
		dataType:'json',
		timeout: 60000,
		cache:false,

		beforeSend:function(request){
			$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">Loading...</div>'});
	   },
		error:function(){
			$.unblockUI();
			showMessage("Failed to load, please try again!","error");
	   },
	   success:function(msg){
	       $.unblockUI();
		   if(msg && msg.flag === "success"){
    		 	 window.location.reload();
		   }else{ showMessage("Failed to load, please try again!","error"); }
			 
	   }
	 
	});
}

function exchange(changeId, changeIdTo, changeSort, changeSortTo){
    $.ajax({
		url:'<%=exchangeClpToPsSortAction%>',
		data:"exchangeId="+changeId+"&exchangeIdTo="+changeIdTo+"&exchangeSort="+changeSort+"&exchangeSortTo="+changeSortTo,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
		  $.unblockUI();
	 
		  if(data != null && data.flag){
			if(data.flag  === "success"){
				window.location.reload();
			}else{
				showMessage("系统错误,调整顺序失败.","error");
			}
		  }else{
		      showMessage("系统错误,调整顺序失败.","error");
		  }
		},
		error:function(){
		    showMessage("系统错误,调整顺序失败.","error");
		    $.unblockUI();
		}
	 });
}
</script>

