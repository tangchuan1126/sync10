<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List, com.cwc.app.key.StorageTypeKey,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改SKU盒子仓库CLP类型</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.STYLE2{font-weight:bold;color:blue;}
	.margin_p{margin-left:10px;padding:0px;}
	.font_{color:#f60;font-weight:bold;}
	.changeContainer{color: #f60;}
</style>
<%
long pc_id = StringUtil.getLong(request,"pc_id");
long title_id = StringUtil.getLong(request,"title_id");
long sku_lp_type_id = StringUtil.getLong(request,"sku_lp_type_id");
//查询product_name 
DBRow product = orderMgrZr.getDetailById(pc_id,"product","pc_id");
product = (product == null) ? new DBRow() : product; 
//clp type
DBRow[] lpContainerTypes = boxTypeMgrZr.getBoxContainerType(ContainerTypeKey.CLP);
//客户卖场
DBRow[] pss = storageCatalogMgrZyj.getProductStorageCatalogByType(StorageTypeKey.CUSTOMER,null); 
//sku 盒子类型
DBRow[] boxTypes = boxTypeMgrZr.getBoxSkuTypeByPcId(pc_id);

String updateClpBoxSkuPsTypeAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/UpdateClpBoxSkuPsTypeAction.action";

DBRow clpType = clpTypeMgrZr.getCLpTypeByTypeId(sku_lp_type_id);

//title
DBRow[] titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true,0,0,null,request);
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
 	function changeContainer(){
		var sku_lp_box_type = $("#sku_lp_box_type").val();
		if(sku_lp_box_type * 1 != 0){
			$(".changeContainer").html("盒子");
		}else{
		    $(".changeContainer").html("商品");
		}
		countValue();
 	}
 	jQuery(function($){
 	   initField();
 	   changeContainer();
  	})
 	function updateClpType(){
		if(validate()){
		    ajaxUpdateClpType();
		}
 	}
 	function initField(){
		$("#lp_type_id option[value='<%=clpType.get("lp_type_id",0l) %>']").attr("selected",true);
		$("#sku_lp_to_ps_id option[value='<%=clpType.get("sku_lp_to_ps_id",0l) %>']").attr("selected",true);
		$("#sku_lp_box_type option[value='<%=clpType.get("sku_lp_box_type",0l) %>']").attr("selected",true);
		$("#sku_lp_title_id option[value='<%=clpType.get("sku_lp_title_id",0l) %>']").attr("selected",true);
 		$("#sku_lp_box_length").val('<%=clpType.get("sku_lp_box_length",0l) %>');
		$("#sku_lp_box_width").val('<%=clpType.get("sku_lp_box_width",0l) %>');
		$("#sku_lp_box_height").val('<%=clpType.get("sku_lp_box_height",0l) %>');
	
  	}
 	function checkInt(value){
 	    var reg = /^[1-9]\d*$/;
 	    return reg.test(value) ;
 	}
 	function ajaxUpdateClpType(){
 	   $.ajax({
		url:'<%=updateClpBoxSkuPsTypeAction%>',
		data:$("#addClpTypeForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
		  $.unblockUI();
	 
		  if(data != null && data.flag){
			  if(data.flag === "error" ){
					if(data.message === "skuNotMatch"){
						showMessage("商品和盒子类型不匹配.","error");
					}
					if(data.message === "skuLpTotalBoxLetter"){
						showMessage("CLP类型上,盒子数量计算错误.","error");
					}
					if(data.message === "skuLpTotalPieceLetter"){
						showMessage("CLP类型上,商品总数计算有误.","error");
					}
			  }
			  if(data.flag === "success"){
					if(data.message * 1 > 0){
					    windowClose();
					}
			  }
		  }else{
		      showMessage("系统错误,更新失败.","error");
		  }
		},
		error:function(){
		     showMessage("系统错误,更新失败.","error");
		     $.unblockUI();
		}
	})
 	}
 	function validate(){
		var lp_type_id = $("#lp_type_id");
		var sku_lp_to_ps_id = $("#sku_lp_to_ps_id");
		var sku_lp_box_length = $("#sku_lp_box_length");
		var sku_lp_box_width =$("#sku_lp_box_width");
		var sku_lp_box_height = $("#sku_lp_box_height");
		if(lp_type_id.val() * 1 == 0){
			showMessage("请选择容器类型","alert");
			return false ;
		}
	 
		if(sku_lp_to_ps_id.val() * 1 == 0){
			showMessage("请选择客户仓库","alert");
			return false ;
		}
		if(checkInt(sku_lp_box_length.val()) * 1 == 0 ){
			showMessage("请输入合法长数量,(比如 1 )","alert");
			return false ;
		}
		if(checkInt(sku_lp_box_width.val() )* 1 == 0 ){
			showMessage("请输入合法宽数量,(比如 1 )","alert");
			return false ;
		}
		if(checkInt(sku_lp_box_height.val() )* 1 == 0 ){
			showMessage("请输入合法高数量,(比如 1 )","alert");
			return false ;
		}
		return  true;
 	}
 	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function validateNumber(e, pnumber){
	    if (!/^\d+$/.test(pnumber)){
	    	$(e).val(/^\d+/.exec($(e).val()));
	    }
	    countValue();
	    return false;
 	}
 	// 计算容器上盒子的数量
 	// 计算容器上商品的总共的数量
 	function countValue(){
 	 	var sku_lp_box_type = $("#sku_lp_box_type");
 	 	var boxType = sku_lp_box_type.val() * 1;
 	 	var box_total_sku = $("option:selected",sku_lp_box_type).attr("total") * 1;
 	 	var box_type_name =  $("option:selected",sku_lp_box_type).html();
 	  	var total_box = $("#total_box");
 	  	var total_piece = $("#total_piece");

 	  	var sku_lp_box_length = $("#sku_lp_box_length").val() * 1;
 	  	var sku_lp_box_width = $("#sku_lp_box_width").val() * 1;
 	  	var sku_lp_box_height = $("#sku_lp_box_height").val() * 1;
 	  	if( boxType * 1 == 0 ){
 	 	  	$("#boxtype").hide();
 	 	  	$("#box_number").hide();
 	 	 
			var tbn = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height;
			
 	 	  	
	 	  	$("#total_piece").html(tbn );
 	 	  	
 	 	  	
 	 	}else{
 	 	 	var notity = "盒子类型[<span class='font_'>"+box_type_name+"</span>]可以装商品[<span  class='font_'>"+box_total_sku+"</span>]个" ;
			var tbn = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height;
			$("#total_box").html(tbn);
			$("#box_number").show();
	 	  	$("#boxtype").show().html(notity);
	 	  	$("#total_piece").html(tbn * box_total_sku);
 	 	}
  	}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 
<form name="addClpTypeForm" id="addClpTypeForm" method="post" action="">
<input type="hidden" name="sku_lp_pc_id" value='<%=pc_id %>'/>
 <input type="hidden" name="sku_lp_type_id" value='<%=sku_lp_type_id %>'/>
 <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	
<fieldset style="border:2px #cccccc solid;padding:5px;margin:10px;width:90%;">
  <legend style="font-size:15px;font-weight:normal;color:#999999;">添加CLP类型</legend>
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
 			<tr>
				<td align="right" valign="middle" class="STYLE2" width="90px" >商品:</td>
				<td align="left" valign="middle" > 
					<span style="color: #f60;font-weight:bold;"><%= product.getString("p_name") %></span>
 			    </td>
			</tr>
 			<tr>
			  	 <td align="right" valign="middle" class="STYLE2" width="90px">CLP容器类型:</td>
			     <td align="left" valign="middle">
			     	<select id="lp_type_id" name="lp_type_id">
			     		<option value="0">选择CLP容器类型</option>
			     		<%if(lpContainerTypes != null && lpContainerTypes.length > 0){ %>
			     			<%for(DBRow containerType : lpContainerTypes) {%>
			     				<option value='<%=containerType.get("type_id",0l) %>'><%=containerType.getString("type_name") %></option>
			     			<%} 
			     		}%>
			     	</select>
 			     </td>
			</tr>
			<tr>
				<td align="right" valign="middle" class="STYLE2" width="90px">客户仓库</td>
				<td>
					<select id="sku_lp_to_ps_id" name="sku_lp_to_ps_id">
						<option value="0">选择客户仓库</option>
						<%if(pss != null && pss.length > 0){
							for(DBRow catalog : pss){
						 %>	
						  		<option value='<%=catalog.get("id",0l) %>'><%=catalog.getString("title") %></option>
						<%} 
						}%>
					</select>
				</td>
			</tr>
			
			<tr>
			 <td align="right" valign="middle" class="STYLE2" width="90px">Title</td>
				<td>
					<select id="sku_lp_title_id" name="sku_lp_title_id">
						<option value="0">选择Title</option>
						<%if(titles != null && titles.length > 0){
							for(DBRow title : titles){
						 %>	
						  		<option value='<%=title.get("title_id",0l) %>'><%=title.getString("title_name") %></option>
						<%} 
						}%>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right" valign="middle" class="STYLE2" width="90px">SKU盒子类型</td>
				<td>
					<select id="sku_lp_box_type" name="sku_lp_box_type" onchange="changeContainer();">
						<option value="0" >商品本身</option>
						<%if(boxTypes != null && boxTypes.length > 0){
							for(DBRow boxType : boxTypes){
								boxType.add("p_name",product.getString("p_name"));
							%>
								<option value='<%=boxType.get("box_type_id",0l) %>'
										total='<%=boxType.get("box_total",0) %>'>
										<%= boxTypeMgrZr.getBlpTypeName(boxType) %>
								</option>
							<%
							}
						} %>
					</select>
				</td>
			</tr>
		</table>
		
		 <div style="border-bottom:1px dashed  silver ; text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="STYLE2" >
		 	<span class="changeContainer">盒子</span>数量：(<span class="changeContainer">盒子</span>在该类型CLP上,长宽高各自能放几个.)
		 </div>
		 
		  <table width="100%" border="0" cellspacing="3" cellpadding="2">
   			 <tr>
				 <td align="right" valign="middle" class="STYLE2" width="90px">长数量:</td>
			     <td align="left" valign="middle" >
			   	 		<input name="sku_lp_box_length" id="sku_lp_box_length" type="text" width="20px"  onkeyup="return validateNumber(this,value);"/>
			   	 </td>
			 </tr>	
			  <tr>
				 <td align="right" valign="middle" class="STYLE2" >宽数量:</td>
			     <td align="left" valign="middle" >
			   	 		<input name="sku_lp_box_width" id="sku_lp_box_width" type="text"   width="20px" onkeyup="return validateNumber(this,value);"/>
			   	 </td>
			 </tr>	
			  <tr>
				 <td align="right" valign="middle" class="STYLE2" >高数量:</td>
			     <td align="left" valign="middle" >
			   	 		<input name="sku_lp_box_height" id="sku_lp_box_height" type="text"   width="20px" onkeyup="return validateNumber(this,value);"/>
			   	 </td>
			 </tr>	
   		 </table>
   		 <div id="showValue" style="border:1px solid silver; padding:5px;text-align: left;">
   		 	该类型CLP : <br />
   		 		<p class="margin_p">商品:[<span  class="font_">18路-四孔老电源</span>]</p> 
   		 		<p class="margin_p" style="margin-top: -5px;" id="boxtype"></p>
   		 		<p class="margin_p" style="margin-top: -5px;" id="box_number"><span class="changeContainer">盒子</span>数量:<span id="total_box" class="font_"></span></p>
   		 		<p class="margin_p" style="margin-top: -5px;">商品总数:<span id="total_piece" class="font_"></span></p>
   		 </div>
 </fieldset>
</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
  		<input type="button" name="Submit2" value="更新类型" class="normal-green" onClick="updateClpType();">
	   <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table>
</form>	
</body>
</html>

