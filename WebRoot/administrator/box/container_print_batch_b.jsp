<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.key.ContainerTypeKey"%>
<%@include file="../../include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add And Print Conatiner</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>

<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<%
int container_type = StringUtil.getInt(request, "container_type");	
long type_id	= StringUtil.getLong(request, "type_id");	
int is_has_sn = StringUtil.getInt(request, "is_has_sn");
%>

<script type="text/javascript">
	function print(){
		if(validateCount()){
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/container/ContainerAddBatchAction.action',
				data:$("#subForm").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.length > 0){
						for(var i = 0; i < data.length; i ++){
							$.ajax({
								url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/container/container_template_b.html',
								type: 'post',
								dataType: 'html',
								data:'id='+data[i].con_id,
								async:false,
								success: function(html){
									printInstantly($(html).find("div[name=rongqi]").html());
								}
							});	
							
						}
						$.artDialog && $.artDialog.close();
					}else{
						showMessage("Submit unsuccessfully, please try again later","alert");
					}
				},
				error:function(){
					showMessage("System error, please try again later","error");
				}
			});	
		}
	};
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function validateCount(){
		if('' == $("#print_count").val()){
			alert("Please input the quantity");	
			return false;
		}
		return true;
	}

	function printInstantly(html){
		 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","容器标签");
         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");            
    	 visionariPrinter.ADD_PRINT_TABLE(5,3,"97%","100%",html);
		 visionariPrinter.SET_PRINT_COPIES(1);
		 //visionariPrinter.PREVIEW();
		 visionariPrinter.PRINT();
	}

</script>

</head>
<body>
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			Add And Print[<%=new ContainerTypeKey().getContainerTypeKeyValue(container_type) %>]
		</legend>	
		<form action="" id="subForm" method="post">
		<input type="hidden" name="container_type" value="<%=container_type %>"/>
		<input type="hidden" name="type_id" value="<%=type_id %>"/>
		<input type="hidden" name="is_has_sn" value="<%=is_has_sn%>"/>
			<table>
				<tr>
					<td>Print Quantity</td>
					<td>
						<input name="print_count" id="print_count" />
						<span style="color:red">Please enter the quantity to print batch)</span>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<br/>
	<div style="border:0px solid red;" align="center">
		<a href="javascript:print();">
			<img src="<%=ConfigBean.getStringValue("systenFolder")+"administrator/lable_template/template_img/blp.jpg"%>">
		</a>
	</div>
	<div id="av" align="left" style="width:368px; border:1px solid red;display:none;"></div>
</body>
</html>