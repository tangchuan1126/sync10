<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@ page import="com.cwc.app.key.ContainerTypeKey" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>添加盒子SKU仓库顺序类型</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 


<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.STYLE2{font-weight:bold;color:blue;}
</style>
<%
StorageTypeKey storageTypeKey = new StorageTypeKey();
	
	String validateBoxSkuPsExitsAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/ValidateBoxSkuPsExitsAction.action"; 
	String getSkuPsIndexAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/GetSkuPsIndexAction.action";
	String addBoxSkuPsIndexAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/AddBoxSkuPsIndexAction.action";
	String addBoxSkuPsIndexPreCheckFirstAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/AddBoxSkuPsIndexPreCheckFirstAction.action";
	
	DBRow box = null;
 	 
	
	long pcid = StringUtil.getLong(request,"pc_id");
	long title_id = StringUtil.getLong(request, "title_id");
	DBRow product =  orderMgrZr.getDetailById(pcid,"product","pc_id");
	String p_name =	(product != null) ?  product.getString("p_name"):"";
 
	box =( box == null ? new DBRow():box) ;
	product =( product == null ? new DBRow():product) ;
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	DBRow[] containers = boxTypeMgrZr.getBoxSkuTypeByPcId(pcid);
	
	
	//客户卖场
	//DBRow[] pss = storageCatalogMgrZyj.getProductStorageCatalogByType(StorageTypeKey.CUSTOMER,null); 
	DBRow[] shipTos = shipToMgrZJ.getAllShipTo();
	
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '35%', 
			left:           '30%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
jQuery(function($){
    affra();
    $("#to_ps_id option[value='<%=title_id%>']").attr("selected",true);
    getIndexPcIdSku();
});
	 
	function affra(){
	    if('<%= p_name%>'.length < 1){
		$.artDialog.confirm('当前商品不存在', function(){
				$.artDialog && $.artDialog.close(); 
			}, function(){
				$.artDialog && $.artDialog.close();
			});
		}
	}
 
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function closeWin()
	{
		$.artDialog.close();
	}
	 
	
	function addBoxSkuPsIndex(){
	    var flag = validate();
	    if(flag){
		    //添加之前判断一次 改商品 选择的这种盒子 去这个仓库是否已经添加了。
		    ajaxAddBoxPreCheck();
			//ajaxAddBoxSkuPs();
 		}
	}
	 
	 
	 
	function  ajaxAddBoxPreCheck(){
	    $.ajax({
		url:'<%=validateBoxSkuPsExitsAction%>',
		data:$("#addBoxSkuPsIndexForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
		  $.unblockUI();
	 
		  if(data != null && data.flag){
				if(data.flag  === "true"){
					affraIsExits();
 				}else{
 				   ajaxAddBoxSkuPs();
 				}
		  }else{
		      showMessage("系统错误,添加失败.","error");
		  }
		},
		error:function(){
		    showMessage("系统错误,添加失败.","error");
		    $.unblockUI();
		}
	 });
	}

	function affraIsExits(){
		var to_ps_id = $("#to_ps_id option:selected").html();
		var box_type_id = $("#box_type_id option:selected").html();
		var notify =  "[商品<span style='color:#f60;font-weight:bold;'>"+'<%= p_name%>'+"</span>]  <br/>"
				  + "用盒子类型[<span style='color:#f60;font-weight:bold;'>"+box_type_id+"</span>] <br />"
		    	  + "发往[仓库:"+to_ps_id+"]数据已经添加";
		 $.artDialog.confirm(notify, function(){
		     	
			}, function(){
				
		}); 	 
	}
	
	function validate(){
		var box_type_id = $("#box_type_id");
		var to_ps_id = $("#to_ps_id");
		if(box_type_id.val() * 1 == 0){
			showMessage("请先选择盒子SKU类型.","alert");
			box_type_id.focus();
			return  false ;
		}
		if(to_ps_id.val() * 1 == 0){
			showMessage("请先选择客户仓库.","alert");
			return false ;
		}
		return true ;
 
	}
	function  ajaxAddBoxSkuPs(){
	 
	    $.ajax({
			url:'<%=addBoxSkuPsIndexAction%>',
			data:$("#addBoxSkuPsIndexForm").serialize(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
			success:function(data){
			  $.unblockUI();
		 
			  if(data != null && data.flag){
					if(data.flag  === "success"){
					    windowClose();				
					}else{
					      showMessage("系统错误,添加失败.","error");
					}
			  }else{
			      showMessage("系统错误,添加失败.","error");
			  }
			},
			error:function(){
			    showMessage("系统错误,添加失败.","error");
			    $.unblockUI();
			}
		});
	}
	 
	function showContainerValue(){
		var box_type_id = $("#box_type_id");
		var notify = $("#notify");
		if(box_type_id.val() * 1 == 0){
		    notify.slideUp(200);
		}else{
		    notify.slideDown(200);
		    var option = $("option:selected",box_type_id);
		    var cLength = option.attr("cLength") * 1,
		    	cWidth = option.attr("cWidth") * 1,
		   	    cHeight = option.attr("cHeight") * 1,
		   		ctotal = option.attr("ctotal") * 1,
		   		pcTotal = option.attr("pcTotal") * 1;
 			$("#box_type_name").html(option.html());
 			
 			$("#box_qty").html(cLength + " * " + cWidth + " * " + cHeight + " = " + ctotal) ;
 			$("#box_total_piece").html(pcTotal);
		}
	}
	//获取index 
	function getIndexPcIdSku(){
		var box_type_id = $("#box_type_id");
		var to_ps_id = $("#to_ps_id");
		if(box_type_id.val() * 1 != 0 && to_ps_id.val() * 1 != 0){
		    ajaxGetIndex();
		}else{
 		    $("#box_index").html("");
		}
	}
	function ajaxGetIndex(){
	    $.ajax({
		url:'<%=getSkuPsIndexAction%>',
		data:$("#addBoxSkuPsIndexForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
			  $("#box_index").html("");
 	    },
		success:function(data){
		  $.unblockUI();
	 
		  if(data != null && data.index){
				if(data.index  * 1 > 0){
				  $("#box_index").html(data.index);				
				}else{
				    showMessage("系统错误,获取顺序失败.","error");
				}
		  }else{
		      showMessage("系统错误,获取顺序失败.","error");
		  }
		},
		error:function(){
		    showMessage("系统错误,获取顺序失败.","error");
 		}
	 });
	}
	
	$(function(){
 		$(".chzn-select").chosen({no_results_text: "没有该选项:"});
 	});
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 
<form name="addBoxSkuPsIndexForm" id="addBoxSkuPsIndexForm" method="post" action="">

<input  type="hidden" value='<%=pcid %>' name="box_pc_id"/>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="left" valign="top">
<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:90%;">
	<div id="notify" style="border:1px solid silver ; width:90%;padding:10px;text-align: left;display:none;">
		<div style="margin-left: 10px;">
				 商品[<span style="color:#f60;font-weight: bold;" ><%=p_name %></span>]<br />
				BLP类型[<span style="color:#f60;font-weight: bold;" id="box_type_name"> </span>]
				可以装商品[<span style="color:#f60;font-weight: bold;" id="box_total_piece"> </span>]个 <br />
				BLP总数<span style="color:#f60;font-weight: bold;" id="box_qty"> </span><br />
		</div>
	</div>
  <legend style="font-size:15px;font-weight:normal;color:#999999;">添加盒子SKU顺序</legend>
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
 			<tr>
				<td align="right" valign="middle" class="STYLE2" width="120px" >BLP内商品:</td>
				<td align="left" valign="middle" > 
					<span style="color: #f60;font-weight:bold;"><%=product.getString("p_name") %></span>
 			    </td>
			</tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE2">SHIPTO:</td>
			     <td>
			     	<div class="side-by-side clearfix">
			     		<select name="to_ps_id" id="to_ps_id" onchange="getIndexPcIdSku();" class="chzn-select" data-placeholder="Choose a ShipTo..." tabindex="1"  style="width:350px">
			     			<option value="0">选择SHIPTO</option>
							<%if(shipTos != null && shipTos.length > 0){
								for(DBRow shipTo : shipTos){
							 %>	
							  		<option value='<%=shipTo.get("ship_to_id",0l) %>'><%=shipTo.getString("ship_to_name") %></option>
							<%} 
							}%>
			     	 	</select>
			     	 </div>
 			   	 </td>
			</tr>
 			<tr>
			  	 <td align="right" valign="middle" class="STYLE2">BLP\SKU类型选择:</td>
			     <td align="left" valign="middle">
			     <div class="side-by-side clearfix">
			     	<select id="box_type_id" name="box_type_id" onchange="showContainerValue();getIndexPcIdSku()" class="chzn-select" data-placeholder="Choose a BoxType..." tabindex="1"  style="width:350px">
			     		<option value="0">请选择盒子SKU类型</option>
			     		<%if(containers != null && containers.length > 0){ %>
			     			<%for(DBRow container :  containers){ %>
			     			 <%container.add("p_name",p_name); %>
			     				<option value='<%= container.get("box_type_id",0l)%>'
			     						 cLength = '<%=container.get("box_total_length",0) %>'
			     						 cWidth = '<%=container.get("box_total_width",0) %>'
			     						 cHeight = '<%=container.get("box_total_height",0) %>'
			     						 ctotal = '<%=container.get("box_total",0) %>'
			     						 pcTotal = '<%=container.get("box_total_piece",0) %>'
			     				 ><%=boxTypeMgrZr.getBlpTypeName(container) %>  BLP类型</option>
			     			<%} 
			     		}%>
			     	</select> 
			     	</div>
 			     </td>
			</tr>
        	<tr>
			  	<td align="right" valign="middle" class="STYLE2" width="90px">BLP发货顺序:</td>
			    <td align="left" valign="middle" > 
			    	 <!-- 这里显示默认的顺序 -->
			    	 <span style="font-weight: bold;color:#f60;" id="box_index"></span>
 			    </td>
			</tr>
		</table>
 </fieldset>
</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
  		<input type="button" name="Submit2" value="添加" class="normal-green" onClick="addBoxSkuPsIndex();">
	    <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table>
</form>	
</body>
</html>

