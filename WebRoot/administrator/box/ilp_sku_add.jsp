<%@page import="com.cwc.app.key.ContainerHasSnTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.ContainerHasSnTypeKey" %>
<%@ page import="com.cwc.app.key.ContainerTypeKey" %>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.PriceUOMKey"%>
<jsp:useBean id="priceUOMKey" class="com.cwc.app.key.PriceUOMKey"></jsp:useBean>
<jsp:useBean id="containerHasSnTypeKey" class="com.cwc.app.key.ContainerHasSnTypeKey"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add ILP TYPE</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.STYLE2{font-weight:bold;color:blue;width: 150px;}
	.SYTLE3{font-weight:bold;color:blue;}
</style>
<%
	String addBoxTypeAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/AddIlpSkuTypeAction.action";
	String updateBoxTypeAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/UpdateBoxTypeAction.action";
	DBRow box = null;
 	 
	
	long pcid = StringUtil.getLong(request,"pcid");
	long basic_type = StringUtil.getLong(request, "basic_type");
	DBRow product =  orderMgrZr.getDetailById(pcid,"product","pc_id");
	String p_name =	(product != null) ?  product.getString("p_name"):"";
 
	box =( box == null ? new DBRow():box) ;
	product =( product == null ? new DBRow():product) ;
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	DBRow[] containers = boxTypeMgrZr.selectContainerTypeAddAllSel(containerTypeKey.ILP);
	int sort = boxTypeMgrZr.getBoxTypeSort(pcid);
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
api.button([
		{
			name: 'Submit',
			callback: function () {
				addBoxType(this);
				return false ;
			},
			focus:true
			
		},
		{
			name: 'Cancel'
		}]
	);
jQuery(function($){

    affra();
    $(function(){
 		$(".chzn-select").chosen({no_results_text: "没有该选项:"});
 	});
    $("#container_type_id option[value='<%=basic_type%>']").attr("selected",true);
    setContainerValue();
    
})
function affra(){
    if('<%= p_name%>'.length < 1){
	$.artDialog.confirm('[ <%= p_name%> ] is not Exist.', function(){
			
			$.artDialog && $.artDialog.close(); 
		}, function(){
			$.artDialog && $.artDialog.close();
		});
	}
}
 
function checkInt(value){
    var reg = /^[1-9]\d*$/;
    return reg.test(value) ;
}
function checkFloat(num)
{
    var reg = new RegExp("^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9][1-9]*[0-9]*))$"); //匹配正浮点数
    if( !reg.test(num) )
    {
        return false;
    }
    return true;
}
 
function addBoxType(_this){
 
 	if(validateFrom()){
   		 ajaxAddBoxType(_this);
	}
}

function validateFrom(){
    var container_type_id  = $("#container_type_id");
    var box_type_name = $("#box_type_name") ;
    var box_total_length = $("#box_total_length");
    var box_total_width = $("#box_total_width");
    var box_total_height = $("#box_total_height");
    var box_length = $("#box_length");
    var box_width = $("#box_width");
    var box_height = $("#box_height");
    var box_weight = $("#box_weight");
    if(container_type_id.val() * 1 == 0){
	    showMessage("Please select ILP basic type.","alert");
		container_type_id.focus();
		return false ;
	}
  
 
	if(!checkInt($.trim(box_total_length.val()))){
	    showMessage("Stack Length Qty.not legal. e.g.(1)","alert");
	    box_total_length.focus();
		return false;
	}
	if(!checkInt($.trim(box_total_width.val()))){
	    showMessage("Stack Width Qty.not legal. e.g.(1)","alert");
	    box_total_width.focus();
		return false;
	}
	if(!checkInt($.trim(box_total_height.val()))){
	    showMessage("Stack Height Qty.not legal. e.g.(1)","alert");
	    box_total_height.focus();
		return false;
	}
	if(!checkFloat($.trim(box_length.val()))){
	    showMessage("ILP length.not legal. e.g.(100.00)","alert");
	    box_length.focus();
		return false;
	}



	if(!checkFloat($.trim(box_width.val()))){
	    showMessage( "ILP width.not legal. e.g.(100.00)","alert");
	    box_width.focus();
		return false;
	}
	if(!checkFloat($.trim(box_height.val()))){
	    showMessage("ILP height.not legal. e.g.(100.00)","alert");
	    box_height.focus();
		return false;
	}
	if(!checkFloat($.trim(box_weight.val()))){
	    showMessage("ILP weight.not legal. e.g.(100.00)","alert");
	    box_weight.focus();
		return false;
	}
	return true ;
}
 
function ajaxAddBoxType(_art){
    $.ajax({
		url:'<%=addBoxTypeAction%>',
		data:$("#addBoxTypeForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
		  $.unblockUI();
	 
		  if(data != null){
				if(data.flag == "dataFormat"){
				    showMessage("The data format error.","alert");
				    $("#box_total_length").focus();
				}
				 if(data.flag === "productNotFound"){
				     showMessage("The product is not found.","error");
				 }
				if(data.flag == "success"){
				    setTimeout("windowClose()", 100);
				}
				if(data.flag == "error"){
				    showMessage("System error.","alert");
				}
				if(data.flag == "this_type_exist")
				{
					showMessage("The basic type and the put way is exist.","alert");
				}
		  }else{
		      showMessage("System error.","alert");
		  }
		},
		error:function(){
		    $.unblockUI();
		}
	})
}
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function closeWin()
	{
		$.artDialog.close();
	}
	 
	 
	function setContainerValue(){
		var container_type_id = $("#container_type_id");
		if(container_type_id.val() * 1 != 0){
			var option = $("option:selected",container_type_id);
			
		    $("#box_length").val(option.attr("clength"));
		    $("#box_width").val(option.attr("cwidth"));
		    $("#box_height").val(option.attr("cheight"));
		    $("#box_weight").val(option.attr("cweight"));
		    
		    $("#box_length_uom").text(option.attr("lengthuom"));
		    $("#box_width_uom").text(option.attr("lengthuom"));
		    $("#box_height_uom").text(option.attr("lengthuom"));
		    $("#box_weight_uom").text(option.attr("weightuom"));
		    $("#length_uom").val(option.attr("lengthuomkey"));
		    $("#weight_uom").val(option.attr("weightuomkey"));
		}else{
		    $("#box_length").val("0");
		    $("#box_width").val("0");
		    $("#box_height").val("0");
		    $("#box_weight").val("0");
		    
		    $("#box_length_uom").text("");
		    $("#box_width_uom").text("");
		    $("#box_height_uom").text("");
		    $("#box_weight_uom").text("");
		    
		    $("#length_uom").val("");
		    $("#weight_uom").val("");
		}
		
	}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="addBoxTypeForm" id="addBoxTypeForm" method="post" action="">
	<input  type="hidden" value='<%=pcid %>' name="box_pc_id"/>
	<input  type="hidden" value='' name="length_uom"  id="length_uom"/>
	<input  type="hidden" value='' name="weight_uom"  id="weight_uom"/>
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  	  <tr>
   		 <td colspan="2" align="center" valign="top">	
			<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:90%;">
  				<legend style="font-size:15px;font-weight:normal;color:#999999;">Add ILP Type</legend>
 					<table width="100%" border="0" cellspacing="3" cellpadding="2">
			 			<tr>
							<td align="right" valign="middle" class="STYLE2" >SKU:</td>
							<td align="left" valign="middle" > 
								<span style="color: #f60;font-weight:bold;"><%=product.getString("p_name") %></span>
			 			    </td>
						</tr>
						<tr>
							<td align="right" valign="middle" class="STYLE2" >Inner has SN:</td>
							<td align="left" valign="middle" >
							<div class="side-by-side clearfix"> 
								<select name="is_has_sn" class="chzn-select" data-placeholder="Choose a HasSnType..." tabindex="1"  style="width:250px">
									<%
										ArrayList containerHasSnTypeKeys = containerHasSnTypeKey.getContainerHasSnTypeKeys();
										for(int i=0; i<containerHasSnTypeKeys.size();i++)
										{
											int isHasSn = Integer.parseInt((String)containerHasSnTypeKeys.get(i));
											String isHasSnStr = containerHasSnTypeKey.getContainerHasSnTypeKeyValue(isHasSn);
									%>
										<option value="<%=isHasSn%>" <%=isHasSn==ContainerHasSnTypeKey.NOSN?"selected=selected":"" %>><%=isHasSnStr%></option>
									<%		
										}
									%>
								</select>
							</div>
			 			    </td>
						</tr>
 						<tr>
						  	 <td align="right" valign="middle" class="STYLE2">ILP Basic Type:</td>
						     <td align="left" valign="middle">
						     <div class="side-by-side clearfix"> 
						     	<select id="container_type_id" name="container_type_id" onchange="setContainerValue()" class="chzn-select" data-placeholder="Choose a ILPBaseType..." tabindex="1"  style="width:250px">
						     		<%if(containers != null && containers.length > 0){%>
						     			<%for(DBRow row : containers){%>
						     						<option value="<%=row.get("type_id",0l) %>" 
						     								clength="<%=row.get("length",0.0f) %>"
						     								cwidth ="<%=row.get("width",0.0f) %>" 
						     								cheight="<%=row.get("height",0.0f)%>"
						     								cweight="<%=row.get("weight",0.0f) %>"
						     								lengthuom="<%=lengthUOMKey.getLengthUOMKey(row.get("length_uom",0))%>"
						     								weightuom="<%=weightUOMKey.getWeightUOMKey(row.get("weight_uom",0))%>"
						     								lengthuomkey="<%=row.get("length_uom",0) %>"
						     								weightuomkey="<%=row.get("weight_uom",0) %>"
						     						 ><%=row.getString("type_name") %></option>
						     					<% 	
						     				}
						     			}
						     		%>
						     	</select> 
						     </div>
			 			     </td>
						</tr>
					</table>
		
					<div style="border-bottom:1px dashed  silver ; text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="SYTLE3" >
					 	Product Qty：(The quantity of Product in <span style='color:#f60;'> ILP Type</span>.)
					</div>
		 
				  <table width="100%" border="0" cellspacing="3" cellpadding="2">
		   			 <tr>
						 <td align="right" valign="middle" class="STYLE2">Stack Length Qty:</td>
					     <td align="left" valign="middle" >
					   	 		<input name="box_total_length" id="box_total_length" type="text"   width="20px">
					   	 </td>
					 </tr>	
					  <tr>
						 <td align="right" valign="middle" class="STYLE2" >Stack Width Qty:</td>
					     <td align="left" valign="middle" >
					   	 		<input name="box_total_width" id="box_total_width" type="text"   width="20px">
					   	 </td>
					 </tr>	
					  <tr>
						 <td align="right" valign="middle" class="STYLE2" >Stack Height Qty:</td>
					     <td align="left" valign="middle" >
					   	 		<input name="box_total_height" id="box_total_height" type="text"   width="20px">
					   	 </td>
					 </tr>	
		   		 </table>
		    	<div style="border-bottom:1px dashed  silver ;  text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="SYTLE3" >
		    	ILP Basic Type Info：
		    	</div>
   		 
				<table width="100%" border="0" cellspacing="3" cellpadding="2">	
					<tr>
						<td align="right" valign="middle" class="STYLE2">Length:</td>
					    <td align="left" valign="middle" >
					    <input name="box_length" id="box_length" type="text"   size="25px">
					     <span id="box_length_uom"></span>
					    </td>
					  </tr>
					<tr>
					  <td align="right" valign="middle" class="STYLE2" >Width:</td>
				      <td align="left" valign="middle" >
				         <input name="box_width" id="box_width" type="text"   size="25px">
				          <span id="box_width_uom"></span>
				      </td>
				   </tr>
				   <tr>
					 <td align="right" valign="middle" class="STYLE2" >Height:</td>
				     <td align="left" valign="middle" >
				        <input name="box_height" id="box_height" type="text"   size="25px">
				         <span id="box_height_uom"></span>
				     </td>
				   </tr>
				   <tr>
					 <td align="right" valign="middle" class="STYLE2" >Weight:</td>
				     <td align="left" valign="middle" >
				        <input name="box_weight" id="box_weight" type="text"   size="25px">
				        <span id="box_weight_uom"></span>
				     </td>
				  </tr>
	   		 </table>
	   		<div style="border-bottom:1px dashed  silver ;  text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="STYLE3" >
	    		ILP which contains [<span style='color: #f60;'><%=p_name %></span>] Sort：<span style="color:#000000;"><%=sort %> </span>
	    	</div>
 		</fieldset>
	</td>
  </tr>
      <!--  
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">

  		<input type="button" name="Submit2" value="添加类型" class="normal-green" onClick="addBoxType();">
	   <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">

	</td>
  </tr>
  	   -->
</table>
</form>	
</body>
</html>

