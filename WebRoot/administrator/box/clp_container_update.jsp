<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改CLP容器</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.STYLE2{font-weight:bold;color:blue;}
	table.inner{border-collapse:collapse;}
	table.inner td{border:1px solid silver;padding: 8px;}
</style>
<%
	 long con_id = StringUtil.getLong(request,"con_id");
	 DBRow clpContainer = orderMgrZr.getDetailById(con_id,"container","con_id");
 	 DBRow clpType = clpTypeMgrZr.getClpTypeAndPsInfo(clpContainer.get("type_id",0l));
	 DBRow boxType = boxTypeMgrZr.getBoxTypeBuId(clpType.get("sku_lp_box_type",0l)) ;
	 boolean isContainBox =!(clpType.get("sku_lp_box_type",0l) == 0l);	//是否是包含有盒子
	 long pc_id = clpType.get("sku_lp_pc_id",0l);
	 DBRow product = orderMgrZr.getDetailById(pc_id,"product","pc_id");
	 String updateClpContainerAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/box/UpdateClpContainerAction.action";
	 if(isContainBox)	{ boxType.add("p_name",product.getString("p_name"));}
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
jQuery(function($){
	 
    
})
 
function checkInt(value){
    var reg = /^[1-9]\d*$/;
    return reg.test(value) ;
}
function checkFloat(num)
{
    var reg = new RegExp("^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9][1-9]*[0-9]*))$"); //匹配正浮点数
    if( !reg.test(num) )
    {
        return false;
    }
    return true;
}
 
function updateClpContainer(){
 
 	if(validateFrom()){
 	   ajaxUpdateClpContainer();
	}
}

function validateFrom(){
    var container  = $("#container");
    var hardwareId = $("#hardwareId");
    if(container.val().trim().length < 1){
		showMessage("请先填写盒子条码","alert");
		container.focus();
		return false ;
	}
    if(hardwareId.val().trim().length < 1){
		showMessage("请先填写盒子RFID","alert");
		hardwareId.focus();
		return false ;
	}
	return true;
 
}
 
function ajaxUpdateClpContainer(){
    $.ajax({
		url:'<%=updateClpContainerAction%>',
		data:$("#updateBoxContainerForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
		  $.unblockUI();
	 
		  if(data != null){
			 if(data.flag === "success" ){
			     windowClose();
			  }
			  if(data.flag === "error" && data.message === "hardwareIdExits"){
				    showMessage("硬件号系统中已经存在,请重新输入!","error");
			  }
		  }else{
		      showMessage("系统错误,添加失败.","error");
		  }
		},
		error:function(){
		    showMessage("系统错误,添加失败.","error");
		    $.unblockUI();
		}
	})
}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function closeWin(){
	$.artDialog && $.artDialog.close();
}
 
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 
<form name="updateBoxContainerForm" id="updateBoxContainerForm" method="post" action="">
<input type="hidden" name="con_id" value='<%=con_id %>'/>
  <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	
<fieldset style="border:2px #cccccc solid;padding:5px;margin:10px;width:95%;">
  <legend style="font-size:15px;font-weight:normal;color:#999999;">修改CLP</legend>
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
 			<tr>
				<td align="right" valign="middle" class="STYLE2" width="110px" >CLP商品:</td>
				<td align="left" valign="middle" > 
						<span style='color:#f60;font-weight:bold;'><%=product.getString("p_name") %></span>
  			    </td>
			</tr>
			<%if(isContainBox) {%>
			<tr>
				<td align="right" valign="middle" class="STYLE2" width="110px" >CLP盒子:</td>
				<td align="left" valign="middle">
				 
					 <a href="javascript:void(0)"  style='color:#f60;font-weight:bold;'><%= (boxType != null) ? boxTypeMgrZr.getBlpTypeName(boxType) : "" %></a>
					 
				</td>
			</tr>
			<tr>
				<td align="right" valign="middle" class="STYLE2">CLP盒子商品数量:</td>
			 	<td align="left" valign="middle">  <%=  (boxType != null ? boxType.get("box_total",0)+"":"")%> </td>
				
			</tr>
			<%} %>
 			<tr>
			  	 <td align="right" valign="middle" class="STYLE2"  >CLP类型:</td>
			     <td align="left" valign="middle">
			     	 <table class="inner">
			     	 	<tr>
			     	 		<td rowspan="3" style="text-align: center;">
			     	 			类型名称: <br />
			     	 			<%=clpTypeMgrZr.getClpTypeName(clpType) %>
			     	 			
			     	 		</td>
			     	 			<td><%= isContainBox ? "盒子":"商品"%>长数量: <%=clpType.get("sku_lp_box_length",0) %></td>
			     	 		<td rowspan="3">总商品数:
			     	 				
			     	 				 <%= isContainBox ? (clpType.get("sku_lp_total_box",0) + " * " + boxType.get("box_total",0) +" = ") : ("") %> 
			     	 				 <%=clpType.get("sku_lp_total_piece",0) %></td>
			     	 	</tr>
			     	 	<tr>
 			     	 		<td><%= isContainBox ? "盒子":"商品"%>宽数量: <%=clpType.get("sku_lp_box_width",0) %> </td>
			     	 	</tr>
			     	 	<tr>
 			     	 		<td><%= isContainBox ? "盒子":"商品"%>高数量: <%=clpType.get("sku_lp_box_height",0) %></td>
			     	 	</tr>
			     	  
			     	 </table>
 			     </td>
			</tr>
			<tr>
				<td  align="right" valign="middle" class="STYLE2">客户仓库: </td>
				<td style='color:#f60;font-weight: bold;'> <%= clpType.getString("title")%></td>
			</tr>
		</table>
		
		 <div style="border-bottom:1px dashed  silver ; text-align: left; margin-top: 10px;text-indent: 35px;padding-bottom: 2px;" class="STYLE2" >
		 	 具体容器 [条码\RFID]:
		 </div>
		 <table width="100%" border="0" cellspacing="3" cellpadding="2">
		 	<tr>
		 		<td align="right" valign="middle" class="STYLE2" width="110px" >容器条码:</td>
		 		<td  align="left" valign="middle"> <input value='<%= clpContainer.getString("container")%>' type="text" id="container" name="container" style="width:200px;"/></td>
		 	</tr>
		 	 <tr>
		 		<td align="right" valign="middle" class="STYLE2" width="90px" >RFID:</td>
		 		<td  align="left" valign="middle" ><input value='<%= clpContainer.getString("hardwareId") %>' type="text" id="hardwareId" name="hardwareId" style="width:200px;"/></td>
		 	</tr>
		 </table>
		  
    	 
 </fieldset>
</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
     <input type="button" name="Submit2" value="修改CLP" class="normal-green" onClick="updateClpContainer();">
	
 
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table>
</form>	
</body>
</html>

