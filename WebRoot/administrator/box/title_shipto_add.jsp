<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List, com.cwc.app.key.StorageTypeKey,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<%@ page import="com.cwc.app.key.ContainerHasSnTypeKey" %>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<jsp:useBean id="containerHasSnTypeKey" class="com.cwc.app.key.ContainerHasSnTypeKey"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add CLP Title And Ship To</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 


<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.STYLE2{font-weight:bold;color:blue;width: 150px;}
	.STYLE3{font-weight:bold;color:blue;}
	.margin_p{margin-left:10px;padding:0px;}
	.font_{color:#f60;font-weight:bold;}
	.changeContainer{color:#f60;}
</style>
<%

long main_id = StringUtil.getLong(request,"main_id");
long pc_id = StringUtil.getLong(request,"pc_id");
int ship_to_value = StringUtil.getInt(request, "ship_to_value");

DBRow[] shipTos = shipToMgrZJ.getAllShipTo();
//sku 盒子类型
///DBRow[] boxTypes = boxTypeMgrZr.getBoxSkuTypeByPcId(pc_id);
//title
DBRow[] titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true,0,"",null,request);

String addClpBoxSkuPsTypeAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/AddLpTitleShipToAction.action";
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
$(function(){
		$(".chzn-select").chosen({no_results_text: "no this option:"});
	});
	
	 var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	   api.button([
	   		{
				name: 'Submit',
				callback: function () {
					addClpType(this);
					
					return false ;
	 			},
	 			focus:true
				
			},
			{
				name: 'Cancel' 
				
			}]
		);
	

 	function addClpType(_art){
		if(validate()){
			 ajaxAddClpType(_art);
		}
 	}
 	function checkInt(value){
 	    var reg = /^[1-9]\d*$/;
 	    return reg.test(value) ;
 	}
 	function ajaxAddClpType(_art){
 	   $.ajax({
		url:'<%=addClpBoxSkuPsTypeAction%>',
		data:$("#addClpTypeForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
			_art.button({
	                name: 'Submit',
	                disabled: true
	            });
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
		  if(data != null && data.flag){
			  if(data.flag === "error" ){
							
						showMessage("The title/Ship To exist.","error");
				
						_art.button({
			                name: 'Submit',
			                disabled: false
			            });
			  }
			  if(data.flag === "success"){
				
					if(data.message * 1 > 0){
						var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/title_shipto_list.html?lp_type_id="+<%=main_id%>+"&pc_id="+<%=pc_id%>; 
						 $.artDialog.open(url , {title:" Add LP Title/Ship To",width:'730px',height:'480px', lock: true,opacity: 0.3,fixed: true});
					   windowClose();	
					}
					
					
			  }
		  }else{
		      showMessage("System error,try it again later.","error");
		      _art.button({
	              name: 'Submit',
	              disabled: false
	          });
		  }
		  $.unblockUI();
		  
		},
		error:function(){
		     showMessage("System error,try it again later.","error");
		     $.unblockUI();
		     _art.button({
	              name: 'Submit',
	              disabled: false
	          });
		}
	})
 	}
 	function validate(){
		var sku_lp_to_ps_id = $("#sku_lp_to_ps_id");
		var sku_lp_title_id = $("#sku_lp_title_id");
		
		if(checkInt(sku_lp_to_ps_id.val()) * 1 == 0 && checkInt(sku_lp_title_id.val()) * 1 == 0 ){
			showMessage("Please select Ship To Or Title","alert");
			return false ;
		}
		
	


		return  true;
		
 	}
 	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
		
	
	};
	function validateNumber(e, pnumber){
	    if (!/^\d+$/.test(pnumber)){
	    	$(e).val(/^\d+/.exec($(e).val()));
	    }
	    countValue();
	    return false;
 	}
 	// 计算容器上盒子的数量
 	// 计算容器上商品的总共的数量
 
 	function closeWin(){
		$.artDialog && $.artDialog.close();
 	}
 	
	


</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 
<form name="addClpTypeForm" id="addClpTypeForm" method="post" action="">

 <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	
<fieldset style="border:2px #cccccc solid;padding:5px;margin:10px;width:90%;">
  <legend style="font-size:15px;font-weight:normal;color:#999999;">Add CLP Title And Ship To</legend>
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
 			<input type="hidden" name="pc_id" value='<%=pc_id %>'/>
 			<input type="hidden" name="main_id" value='<%=main_id %>'/>
			<tr>
				<td align="right" valign="middle" class="STYLE2">SHIPTO:</td>
				<td>
					<div class="side-by-side clearfix">
						<select id="sku_lp_to_ps_id" name="sku_lp_to_ps_id" class="chzn-select" data-placeholder="Choose a ShipTo..." tabindex="1"  style="width:350px">
							<option value="0">Select SHIPTO...</option>
							<%if(shipTos != null && shipTos.length > 0){
								for(DBRow shipTo : shipTos){
							 %>	
							  		<option value='<%=shipTo.get("ship_to_id",0l) %>'><%=shipTo.getString("ship_to_name") %></option>
							<%} 
							}%>
						</select>
					</div>
				</td>
			</tr>
		
 			
		 	 <td align="right" valign="middle" class="STYLE2">Title:</td>
				<td>
					<div class="side-by-side clearfix">
						<select id="sku_lp_title_id" name="sku_lp_title_id" class="chzn-select" data-placeholder="Choose a Title..." tabindex="1"  style="width:350px">
							<option value="0">Select Title...</option>
							<%if(titles != null && titles.length > 0){
								for(DBRow title : titles){
							 %>	
							  		<option value='<%=title.get("title_id",0l) %>'><%=title.getString("title_name") %></option>
							<%} 
							}%>
						</select>
					</div>
				</td>
	 		</tr>
	
				
		</table>
		
		
 </fieldset>
</td>
</tr>

</table>
</form>	
</body>
</html>

