 <%@page import="com.fr.third.org.apache.poi.util.SystemOutLogger"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.api.zr.BoxTypeMgrZr"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,com.cwc.app.key.StorageTypeKey,java.util.ArrayList"%>
<jsp:useBean id="containerHasSnTypeKey" class="com.cwc.app.key.ContainerHasSnTypeKey"></jsp:useBean>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>CLP Type Ship To</title>
		<!-- 基本css 和javascript -->
		<link href="../comm.css" rel="stylesheet" type="text/css">
		<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
		<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
		<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
		<script src="../js/zebra/zebra.js" type="text/javascript"></script>
		<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
		<!-- 引入Art -->
		<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="/Sync10-ui/lib/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/art/plugins/iframeTools.source.js" type="text/javascript"></script>
		<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
		<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
		<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
		<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
		<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
		<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet" />
		<style type="text/css" media="all">
			@import "../js/thickbox/global.css";
			@import "../js/thickbox/thickbox.css";
		</style>
		<!-- menu.js -->
		<script type="text/javascript" src='../js/easyui/jquery.easyui.menu.js'></script>
		<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">
		<!-- 遮罩 -->
		<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
		<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 
		<style type="text/css">
			.set {
				color: #0066FF;
				font-weight: bold;
				font-size: 12px;
				font-family: Arial, Helvetica, sans-serif;
			}
			.title {
				font-size: 12px;
				color: green;
				font-weight: blod;
			}
			a.common{width:16px;height:16px;display:block;float:left;border: 1px solid silver ; margin-left: 3px;}
			a.up{background-image: url('../imgs/arrow_up.png');}
			a.down{background-image: url('../imgs/arrow_down.png');}
			td .box{float:left;padding:4px;}
			td .left{text-align: right;}
			td .right{text-align: left;}
			td .leftWidth11{width:120px;}
			td .leftWidth22{width:90px;}
			td .rigthWidth11{width:170px;}
			td .rigthWidth22{width:90px;}
			td .even .box{border-bottom:1px solid #ddd;border-top:1px solid #ddd;}
			td .clear{clear:both}
			td .leftWidth1{width:100px;}
			td .leftWidth2{width:60px;}
			td .rigthWidth1{width:110px;}
			td .rigthWidth2{width:70px;}
		</style>
		<%
		 long pc_id = StringUtil.getLong(request,"pc_id");
		 long title_id = StringUtil.getLong(request,"title_id");
		 long ship_to_id = StringUtil.getLong(request,"ship_to_id",-1);	
		 //查询product_name 
		 DBRow product = orderMgrZr.getDetailById(pc_id,"product","pc_id");
		 product = (product == null) ? new DBRow() : product;
		 DBRow[] clpTypes = LPTypeMgrZJ.getCLPTypeForSKUShipTo(pc_id,ship_to_id) ;
		
		 Map<String, LinkedList<DBRow>> clpMap = new HashMap<String, LinkedList<DBRow>>();
		 for(int i = 0; i < clpTypes.length; i ++)
		 {
			 DBRow clpType = clpTypes[i];
			 String title = clpType.getString("ship_to_name");
			// System.out.println("title:"+title);
			 LinkedList<DBRow> values = clpMap.get(title);
			 if(null == values)
			 {
				values = new LinkedList<DBRow>();
			 }
			 if(1 == clpType.get("clp_sort",0))
			 {
				 values.addFirst(clpType);
			 }
			 else
			 {
				 values.add(clpType);
			 }
			 clpMap.put(title, values);
		 }
		 String deleteCLType = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/DeleteClpBoxSkuPsTypeAction.action";
		 DBRow[] shipTos = shipToMgrZJ.getAllShipTo();
		 String exchangeClpToPsSortAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/box/ExchangeClpToPsSortAction.action";
		 
		 String lockChangeAction =   ConfigBean.getStringValue("systenFolder")+"action/administrator/box/LockchangeBoxTypeAction.action";
		%>
		<script type="text/javascript">
		jQuery(function($){
			$("#tabs").tabs({
				cache: true,
				spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
				cookie: { expires: 30000 } ,
			});
	        $("#tabs").tabs("select",0);  //默认选中第一个
	        $(function(){
	     		$(".chzn-select").chosen({no_results_text: "没有该选项:"});
	     	});
		});
		</script>
		<script type="text/javascript">
		 function addNew(){
			 var title_id = $("#to_ps_id").val();
			 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_box_sku_ps_type_add.html?pc_id="+<%= pc_id%>+"&title_id="+title_id; 
			 $.artDialog.open(uri , {title:"["+ '<%= product.getString("p_name")%>' +"]"+ " Add CLP Type",width:'650px',height:'480px', lock: true,opacity: 0.3,fixed: true});
		 }
		 function update(id){
			 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_box_sku_ps_type_update.html?pc_id="+<%= pc_id%>+"&title_id="+<%= title_id%>+"&sku_lp_type_id="+id; 
			 $.artDialog.open(uri , {title:"["+ '<%= product.getString("p_name")%>' +"]"+ "CLP类型修改",width:'530px',height:'500px', lock: true,opacity: 0.3,fixed: true});   
		 }
		 function deleteType(id){
			    $.artDialog.confirm('确认删除操作?', function(){
				     $.ajax({
						 url:'<%= deleteCLType%>' + "?sku_lp_type_id="+id,
						 dataType:'json',
						 beforeSend:function(request){
					      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
					    },
						 success:function(data){
							$.unblockUI();
						 	if(data != null && data.flag === "success"){
				    		 	 window.location.reload();
						 	}else{
						 	   showMessage("系统错误,稍后再试.","error");
							}
						 },
						 error:function(){
						     $.unblockUI();
						     showMessage("系统错误,稍后再试.","error");
						 }
					  })
				}, function(){
				});
		 }
		 function refreshWindow(){
			window.location.reload();
		 }
		 jQuery(function($){
			$("#to_ps_id option[value='<%=ship_to_id%>']").attr("selected",true);
		  });
		 function createClp(sku_lp_type_id){
			 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_container_list.html?sku_lp_type_id="+sku_lp_type_id+"&detail_type=3&pcid="+'<%=pc_id%>'; 
			 $.artDialog.open(uri , {title:"["+ '<%= product.getString("p_name")%>' +"]"+ " Add CLP Type",width:'730px',height:'480px', lock: true,opacity: 0.3,fixed: true});
		
		 }
		 function exchange(changeId, changeIdTo, changeSort, changeSortTo){
		     $.ajax({
				url:'<%=exchangeClpToPsSortAction%>',
				data:"exchangeId="+changeId+"&exchangeIdTo="+changeIdTo+"&exchangeSort="+changeSort+"&exchangeSortTo="+changeSortTo,
				dataType:'json',
				type:'post',
				beforeSend:function(request){
			      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			    },
				success:function(data){
				  $.unblockUI();
			 
				  if(data != null && data.flag){
						if(data.flag  === "success"){
						    refreshWindow();
			 				}else{
			 				  showMessage("系统错误,调整顺序失败.","error");
			 				}
				  }else{
				      showMessage("系统错误,调整顺序失败.","error");
				  }
				},
				error:function(){
				    showMessage("系统错误,调整顺序失败.","error");
				    $.unblockUI();
				}
			 });
		 }
		 function print(container_type, type_id, is_has_sn)
		 {
			//var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch_c.html?container_type="+container_type+"&type_id="+type_id+"&is_has_sn="+is_has_sn;
		 	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch_common.html?container_type="+container_type+"&type_id="+type_id+"&is_has_sn="+is_has_sn+"&detail_type=3&pcid="+'<%=pc_id%>';
		 	$.artDialog.open(url , {title: 'Batch print Clp Container',width:'700px',height:'450px', lock: true,opacity: 0.3,fixed: true});
		 }
		 function changeTitle()
		{
			$("#ship_to_id").val($("#to_ps_id").val());
			$("#toolForm").submit();
		};
		
		
		function lockType(id,lockId){
			$.ajax({
				url:'<%=lockChangeAction%>' + "?id="+id + "&lockId="+lockId,
				type:'post',
				dataType:'json',
				timeout: 60000,
				cache:false,

				beforeSend:function(request){
					$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">操作中...</div>'});
			   },
				error:function(){
					$.unblockUI();
					showMessage("操作失败,请重试!","error");
			   },
			   success:function(msg){
			       $.unblockUI();
				   if(msg && msg.flag === "success"){
		    		 	 window.location.reload();
				   }else{ showMessage("操作失败,请重试!","error"); }
					 
			   }
			 
			});
		}
		
		 function openTitleAndShipTo(lp_type_id){
			 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/title_shipto_list.html?lp_type_id="+lp_type_id+"&pc_id="+<%= pc_id%>;
			 $.artDialog.open(uri , {title:" Add LP Title/Ship To",width:'730px',height:'480px', lock: true,opacity: 0.3,fixed: true});
		
		 }
		</script>
	</head>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
		<div id="tabs" style="width: 98%; margin-left: 2px; margin-top: 3px;">
			<ul>
				<li>
					<a href="#tool">Filter</a>
				</li>
			</ul>
			<div id="tool">
				<form action="" id="toolForm" name="toolForm">
					<input type="hidden" value="query" name="cmd" />
					<input type="hidden" name="pc_id" value='<%=pc_id %>' />
					<input type="hidden" name="title_id" value='<%=title_id %>' />
					<input type="hidden" name="ship_to_id" id="ship_to_id" value='<%=ship_to_id %>' />
				</form>
					<table>
						<tr>
							<td>
								<div class="side-by-side clearfix">
									<select name="to_ps_id" id="to_ps_id" onchange="changeTitle()" class="chzn-select" data-placeholder="Choose a ShipTo..." tabindex="1"  style="width:350px">
										<option value='-1'>
											Select ShipTo
										</option>
										<%
											if(shipTos != null && shipTos.length > 0)
												 		{
												 	 		for(DBRow shipTo : shipTos)
												 	 		{
												 	 			%>
										<option value='<%=shipTo.get("ship_to_id",0l) %>'><%=shipTo.getString("ship_to_name") %></option>
										<% 
												 	 		}
												 	 } %>
									</select>
								</div>
							</td>
							<td>
								<input type="button" value="Add CLP" class="long-button-add" onclick="addNew()" />
							</td>
						</tr>
					</table>
			</div>
		</div>
		 <table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="true" isBottom="true" class="zebraTable" style="margin-left:3px;margin-top:5px;">
			<tr>
				<th width="20%" style="vertical-align: center; text-align: center;"
					class="right-title">
					ShipTo
				</th>
				<th width="80%" style="vertical-align: center; text-align: center;"
					class="right-title">
					CLP Type Info
				</th>
			</tr>
			<tbody>
				<%if(clpMap != null && clpMap.size() > 0){ 
		       		Set<String> storageTitles = clpMap.keySet();
		       		Iterator<String> it = storageTitles.iterator();
		       		while(it.hasNext())
		       		{
		       			String storageTitle = it.next();
		       			List<DBRow> clpTypeList = clpMap.get(storageTitle);
		       %>
				<tr>
					<td>
						<%=storageTitle %>
					</td>
					<td>
						<%for(int i = 0 , count=clpTypeList.size() ; i < count ; i ++){
			       			DBRow clpType = clpTypeList.get(i);
			       			boolean isProduct =( clpType.get("inner_pc_or_lp",0) == 0) ;
			       			boolean isFirst = i==0;//clpType.get("clp_sort",0) == 1 ;//clp_sort  sku_lp_sort
  							boolean isLast = (i ==( clpTypeList.size() -1)) ;
  							//System.out.println(clpTypeList.size()+","+clpType.get("sku_lp_sort",0));
		       			%>
							<fieldset class="set"
								style="padding-bottom: 4px; border: 2px solid #993300; width: 95%; padding-left: 5px; margin-top: 10px;">
								<legend>
									<span class="title"> <span style="font-weight: bold;">
											[Product Total:<%= clpType.get("inner_total_pc",0)%>]&nbsp; [Sort:<%= clpType.get("ship_to_sort",0)%>]
									</span> </span>
								</legend>
								<table>
									<tr>
										<td style="width: 65%;">
											<div >
								      			<div class="box left leftWidth11"><span>CLP Basic Type:</span></div>
								      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%=clpType.getString("type_name") %></span></div>
								      			<div class="clear"></div>
								      		</div>
								      		<div >
								      			<div class="box left leftWidth11"><span>CLP Type Name:</span></div>
								      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF">
								      				<%= clpType.get("stack_length_qty",0)%>*<%= clpType.get("stack_width_qty",0)%>*<%= clpType.get("stack_height_qty",0)%>
								      			</span></div>
								      			<div class="clear"></div>
								      		</div>
								      		<div >
								      			<div class="box left leftWidth11"><span>Title:</span></div>
								      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%=clpType.getString("title_name") %></span></div>
								      			<div class="clear"></div>
								      		</div>
											<div >
								      			<div class="box left leftWidth11"><span>Inner has SN:</span></div>
								      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%=containerHasSnTypeKey.getContainerHasSnTypeKeyValue(clpType.get("is_has_sn",0)) %></span></div>
								      			<div class="clear"></div>
								      		</div>
											<div >
								      			<div class="box left leftWidth11"><span>Stack Length Qty:</span></div>
								      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%= clpType.get("stack_length_qty",0)%></span></div>
								      			<div class="clear"></div>
								      		</div>
								      		<div >
								      			<div class="box left leftWidth11"><span>Stack Width Qty:</span></div>
								      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%= clpType.get("stack_width_qty",0)%></span></div>
								      			<div class="clear"></div>
								      		</div>
								      		<div >
								      			<div class="box left leftWidth11"><span>Stack Height Qty:</span></div>
								      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%= clpType.get("stack_height_qty",0)%></span></div>
								      			<div class="clear"></div>
								      		</div>
											<%if(!isProduct){ %>
											<div >
								      			<div class="box left leftWidth11"><span>Inner BLP Type:</span></div>
								      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%= boxTypeMgrZr.getBlpTypeName(clpType.get("lpt_id",0l)) %></span></div>
								      			<div class="clear"></div>
								      		</div>
											<div >
								      			<div class="box left leftWidth11"><span>Product total in BLP:</span></div>
								      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%= clpType.get("inner_total_pc",0)%></span></div>
								      			<div class="clear"></div>
								      		</div>
											<%} %>
										</td>
										<%String length_uom = "("+lengthUOMKey.getLengthUOMKey(clpType.get("length_uom", 0))+")";%>
	      								<%String weight_uom = "("+weightUOMKey.getWeightUOMKey(clpType.get("weight_uom", 0))+")";%>
										<td style="width: 30%;">
<!-- 											<table> -->
<%-- 												<tr><td align="right">Length:</td><td><%= clpType.get("length",0.0d)+"&nbsp;"+length_uom%></td></tr> --%>
<%-- 												<tr><td align="right">Width:</td><td> <%= clpType.get("width",0.0d)+"&nbsp;"+length_uom%></td></tr> --%>
<%-- 												<tr><td align="right">Height:</td><td><%= clpType.get("height",0.0d)+"&nbsp;"+length_uom%></td></tr> --%>
<%-- 												<tr><td align="right">Weight:</td><td> <%=clpType.get("weight",0.0d)+"&nbsp;"+weight_uom%></td></tr> --%>
<!-- 											</table> -->
										<%--	<p style="margin-top: 10px;">
												<span style="width:30px;text-align: right;">Length：</span><span><%= clpType.get("length",0.0d)+"&nbsp;"+length_uom%></span>
												Length&nbsp;： <%= clpType.get("length",0.0d)+"&nbsp;"+length_uom%>
											</p>	
											<p>
												<span style="width:30px;text-align: right;">Width：</span><span><%= clpType.get("width",0.0d)+"&nbsp;"+length_uom%></span>
												Width&nbsp;&nbsp;&nbsp;： <%= clpType.get("width",0.0d)+"&nbsp;"+length_uom%>
											</p>	
											<p>
												<span style="width:30px;text-align: right;">Height：</span><span> <%= clpType.get("height",0.0d)+"&nbsp;"+length_uom%></span>
												Height&nbsp;： <%= clpType.get("height",0.0d)+"&nbsp;"+length_uom%>
											</p>	 
											<p>
												<span style="width:30px;text-align: right;">Weight：</span><span> <%= clpType.get("weight",0.0d)+"&nbsp;"+weight_uom%></span>
												Weight&nbsp;： <%= clpType.get("weight",0.0d)+"&nbsp;"+weight_uom%>
											</p>--%>
											<div >
				      		<div class="box left leftWidth2"><span>Lengh:</span></div>
				      			<div class="box right rigthWidth2"><span style="font-weight:bold; color:#0066FF"><%= clpType.get("length",0d)+"&nbsp;"+length_uom%></span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<div >
				      			<div class="box left leftWidth2"><span>Width:</span></div>
				      			<div class="box right rigthWidth2"><span style="font-weight:bold; color:#0066FF"><%= clpType.get("width",0d)+"&nbsp;"+length_uom%></span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<div >
				      			<div class="box left leftWidth2"><span>Height:</span></div>
				      			<div class="box right rigthWidth2"><span style="font-weight:bold; color:#0066FF"><%= clpType.get("height",0d)+"&nbsp;"+length_uom%></span></div>
				      			<div class="clear"></div>
				      		</div>
				      		<div >
				      			<div class="box left leftWidth2"><span>Weight:</span></div>
				      			<div class="box right rigthWidth2"><span style="font-weight:bold; color:#0066FF"><%= clpType.get("weight",0d)+"&nbsp;"+weight_uom%></span></div>
				      			<div class="clear"></div>
				      		</div>
										</td>
										<td style="width: 10%;">
											<p style="margin-top: 3px;">
											<input style="float: right;margin-right: 10%; " type="button" class="long-button-add" value="Manage CLP" onclick="createClp('<%=clpType.get("lpt_id",0l) %>');" />
								  <br/><br/><input class="long-button" type="button" style="float: right;margin-right: 10%;clear: right"  onclick="print('<%=ContainerTypeKey.CLP %>','<%=clpType.get("lpt_id",0l)%>','<%=clpType.get("is_has_sn",0)%>')" value="Create & Print">
								  <br/>
								  <br/>
								  			<input style="float: right;margin-right: 10%; " type="button" class="long-button" value="Linked Title/Ship To" onclick="openTitleAndShipTo('<%=clpType.get("lpt_id",0l) %>');" />
								  <br/>
								  <br/>
						 	  			<%if(clpType.get("active",0l)==1){ %>
											<input style="float: right;margin-right: 10%;  " class="long-button" type="button" onclick="lockType('<%=clpType.get("lpt_id",0l)%>','2');" value="Active" id="active">	
				                            <% }else{ %>
											<input style="float: right;margin-right: 10%; " class="long-button" type="button" onclick="lockType('<%=clpType.get("lpt_id",0l)%>','1');" value="Inactive" id="Inactive">		
			 								<%	} %>	
			 						 					
											<br><br>
											<%if(!isFirst && count > 1){ %>
												<a class="common up" title="Up" onclick="exchange('<%=clpType.get("lp_title_ship_id",0l) %>','<%=clpTypeList.get(i-1).get("lp_title_ship_id",0l)%>','<%=clpType.get("ship_to_sort",0l) %>','<%=clpTypeList.get(i-1).get("ship_to_sort",0l)%>')" href="javascript:void(0)"></a> 
											<%} %>
											<%if(!isLast  && count > 1){ %>
												<a class="common down" title="Down" onclick="exchange('<%=clpType.get("lp_title_ship_id",0l) %>','<%=clpTypeList.get(i+1).get("lp_title_ship_id",0l)%>','<%=clpType.get("ship_to_sort",0l) %>','<%=clpTypeList.get(i+1).get("ship_to_sort",0l)%>')" href="javascript:void(0)"></a> 
											<%} %>
											
											
											</p>
										</td>
									</tr>
								</table>
									<!-- 
				       					<input type="button" name="Submit1" value="修改" onclick="update('<%=clpType.get("sku_lp_type_id",0l) %>');" class="short-short-button-mod" /> <br /><br />
										<input class="short-short-button-del" type="button" name="Submit2" value="删除" onclick="deleteType('<%=clpType.get("sku_lp_type_id",0l) %>');"><br /><br />
			       					 -->
							</fieldset>
					<% 
		       			}	
		       		%>
					</td>
				</tr>
				<tr class="split">
     				<td colspan="2" align="right"></td>
     			</tr>
     			<%
	       		}
	       		%>
				<%
		       	 }else{
		       	%>
				<tr>
					<td colspan="4"
						style="text-align: center; line-height: 180px; height: 180px; background: #E6F3C5; border: 1px solid silver;">
						No Datas
					</td>
				</tr>
				<% 
		       	 }
		       	%>
			</tbody>
		</table>
		<script>
		$.blockUI.defaults = {
				css: { 
					padding:        '8px',
					margin:         0,
					width:          '170px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #999999',
					backgroundColor:'#eeeeee',
					'-webkit-border-radius': '10px',
					'-moz-border-radius':    '10px',
					'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
					'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
				},
				//设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.6' 
				},
				baseZ: 99999, 
				centerX: true,
				centerY: true, 
				fadeOut:  1000,
				showOverlay: true
			};

		</script>
	</body>
</html>