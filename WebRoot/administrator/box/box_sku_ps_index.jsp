<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.api.zr.BoxTypeMgrZr"%>
<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.LinkedList"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<jsp:useBean id="containerHasSnTypeKey" class="com.cwc.app.key.ContainerHasSnTypeKey"></jsp:useBean>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>box ship to</title>

<!-- 基本css 和javascript -->
<style type="text/css">
.set {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
.title{font-size:12px;color:green;font-weight:blod;}
a.common{width:16px;height:16px;display:block;float:left;border: 1px solid silver ; margin-left: 3px;}
a.up{background-image: url('../imgs/arrow_up.png');}
a.down{background-image: url('../imgs/arrow_down.png');}
a.delete{width:13px;height:13px;display:block;float:left; border:1px solid silver;background-image: url('../imgs/del.gif'); margin-left: 3px;margin-top: 2px;}
td .box{float:left;padding:4px;}
td .left{text-align: right;}
td .right{text-align: left;}
td .leftWidth11{width:120px;}
td .leftWidth22{width:90px;}
td .rigthWidth11{width:170px;}
td .rigthWidth22{width:90px;}
td .even .box{border-bottom:1px solid #ddd;border-top:1px solid #ddd;}
td .clear{clear:both}
</style>
<%
String exchangeBoxSkuPsSortAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/box/ExchangeBoxSkuPsSortAction.action"; 

String cmd = StringUtil.getString(request,"cmd");
long pcid = StringUtil.getLong(request,"pc_id");
long ship_to_id = StringUtil.getLong(request,"ship_to_id");
DBRow product = orderMgrZr.getDetailById(pcid,"product","pc_id");
//System.out.println("cmd:"+cmd+"pcid:"+pcid+"ship_to_id:"+ship_to_id);
DBRow[] boxPsIndex = boxSkuPsMgrZr.getAllBoxSkuByPcIdAndPsId(pcid,ship_to_id, 0);
  //同一个ps_id 下面的
 Map<String,LinkedList<DBRow>> map = new HashMap<String,LinkedList<DBRow>>();
if(boxPsIndex != null && boxPsIndex.length > 0){
	for(DBRow temp : boxPsIndex ){
		String title = temp.getString("ship_to_name");
		LinkedList<DBRow> values = map.get(title);
		if(values == null){
			values = new LinkedList<DBRow>();
		}
	if(temp.get("ship_to_sort",0 ) == 1){
			values.addFirst(temp);
		}else{
			values.add(temp);
		}
		
		map.put(title,values);
	}
}
//DBRow[] pss = storageCatalogMgrZyj.getProductStorageCatalogByType(StorageTypeKey.CUSTOMER,null); 
DBRow[] shipTos = shipToMgrZJ.getAllShipTo();

%>
<script type="text/javascript">
	 function exchangePs(change,changeto){
	     $.ajax({
			url:'<%=exchangeBoxSkuPsSortAction%>',
			data:"exchange="+change+"&exchange_to="+changeto,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
			success:function(data){
			  $.unblockUI();
		 
			  if(data != null && data.flag){
					if(data.flag  === "success"){
					    refreshWindow();
 	 				}else{
 	 				  showMessage("System error,exchange sort unsuccessfully.","error");
 	 				}
			  }else{
			      showMessage("System error,exchange sort unsuccessfully.","error");
			  }
			},
			error:function(){
			    showMessage("System error,exchange sort unsuccessfully.","error");
			    $.unblockUI();
			}
		 });
	 }
</script>
</head>
<body onload = "onLoadInitZebraTable()">
	 <table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="true" isBottom="true" class="zebraTable" style="margin-left:3px;margin-top:5px;">
			  <tr> 
  			  	<th width="17%" style="vertical-align: center;text-align: center;" class="right-title">ShipTo</th>
		        <th width="83%" style="vertical-align: center;text-align: center;" class="right-title">BLP Type Info/BLP Basic Container Type Info</th>
<%--		        <th width="25%" style="vertical-align: center;text-align: center;" class="right-title">BLP基础容器类型属性</th>--%>
<%--		    	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>--%>
 		      </tr>
		      <tbody>
		      	<%if(map != null && map.size() > 0){
		      		Set<String> sets = map.keySet() ;
		      		Iterator<String> it = sets.iterator();
		      		while(it.hasNext()){
		      			String setValue = it.next();
		      			List<DBRow> arrayDBRows = map.get(setValue);
		      		%>
		      			<tr>
		      				<td>
		      					<%=setValue %>
		      				</td>
		      				<td>
		      					<%
		      					if(arrayDBRows != null && arrayDBRows.size() > 0){
		      						for(int index = 0 , count = arrayDBRows.size() ; index < count ; index++ )	{
		      							DBRow queryRow = arrayDBRows.get(index);
		      							boolean isFirst = queryRow.get("ship_to_sort",0) == 1 ;
		      							String color = (isFirst ? "#993300":"silver");
		      							boolean isLast =(index ==( arrayDBRows.size() -1)) ;
		      							queryRow.add("p_name",product.getString("p_name"));
		      							%>
		      							<fieldset class="set" style="border:2px solid <%=color %>;width:95%;padding-left: 5px;margin-top:10px;">
		      								<legend>
												<span class="title">
													<span style="font-weight:bold; ">
													 [<%= boxTypeMgrZr.getBlpTypeName(queryRow)%>] &nbsp;
													[ship_to_sort : <%=queryRow.get("ship_to_sort",0) %>]
													</span>
												</span>
											</legend>
												<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0">
												<tr>
													<td width="65%">
														<div >
											      			<div class="box left leftWidth11"><span>BLP Basic Type:</span></div>
											      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%=queryRow.getString("type_name") %></span></div>
											      			<div class="clear"></div>
											      		</div>
											      		<div >
											      			<div class="box left leftWidth11"><span>BLP Type Name:</span></div>
											      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF">[<%= boxTypeMgrZr.getBlpTypeName(queryRow) %>]</span></div>
											      			<div class="clear"></div>
											      		</div>
											      		<div >
											      			<div class="box left leftWidth11"><span>Inner has SN:</span></div>
											      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%=containerHasSnTypeKey.getContainerHasSnTypeKeyValue(queryRow.get("is_has_sn",0)) %></span></div>
											      			<div class="clear"></div>
											      		</div>
											      		<div >
											      			<div class="box left leftWidth11"><span>Stack Length Qty:</span></div>
											      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%= queryRow.get("stack_length_qty",0)%></span></div>
											      			<div class="clear"></div>
											      		</div>
											      		<div >
											      			<div class="box left leftWidth11"><span>Stack Width Qty:</span></div>
											      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%= queryRow.get("stack_width_qty",0)%></span></div>
											      			<div class="clear"></div>
											      		</div>
											      		<div >
											      			<div class="box left leftWidth11"><span>Stack Height Qty:</span></div>
											      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%= queryRow.get("stack_height_qty",0)%></span></div>
											      			<div class="clear"></div>
											      		</div>
														<%if(0!=queryRow.get("box_inner_type",0l)){
															DBRow lipRow = boxTypeMgrZr.findIlpByIlpId(queryRow.get("box_inner_type",0l));
															String ilpName = lipRow.get("stack_length_qty",0l) + "*" + lipRow.get("stack_width_qty",0l) + "*"+ lipRow.get("stack_height_qty",0l);
														%>
														<div >
											      			<div class="box left leftWidth11"><span>Inner ILP Total:</span></div>
											      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><%= queryRow.get("inner_total_pc",0)%></span></div>
											      			<div class="clear"></div>
											      		</div>
											      		<div >
											      			<div class="box left leftWidth11"><span>Inner ILP Type:</span></div>
											      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"><a href="javascript:void(0)" onclick="addilp(<%=queryRow.get("basic_type_id",0l)%>)">[<%=ilpName %>]</a></span></div>
											      			<div class="clear"></div>
											      		</div>
											      		<%} %>
											      		<div >
											      			<div class="box left leftWidth11"><span>Inner product Total:</span></div>
											      			<div class="box right rigthWidth11"><span style="font-weight:bold; color:#0066FF"> <%= queryRow.get("inner_total_pc",0)%></span></div>
											      			<div class="clear"></div>
											      		</div>
													</td>
													<td width="25%">
													<%String length_uom = "("+lengthUOMKey.getLengthUOMKey(queryRow.get("length_uom", 0))+")";%>
	      											<%String weight_uom = "("+weightUOMKey.getWeightUOMKey(queryRow.get("weight_uom", 0))+")";%>
														<p style="margin-top: 10px;">
															<span style="font-weight:bold; color:#0066FF">Lengh&nbsp;： <%= queryRow.get("length",0.0d)+"&nbsp;"+length_uom%></span>
														</p>	
														<p>
															<span style="font-weight:bold; color:#0066FF">Width&nbsp;&nbsp;： <%= queryRow.get("width",0.0d)+"&nbsp;"+length_uom%></span>
														</p>	
														<p>
															<span style="font-weight:bold; color:#0066FF">Height&nbsp;： <%= queryRow.get("height",0.0d)+"&nbsp;"+length_uom%></span>
														</p>	 
														<p>
															<span style="font-weight:bold; color:#0066FF">Weight： <%= queryRow.get("weight",0.0d)+"&nbsp;"+weight_uom%></span>
														</p>	
													</td>
													<td width="10%">
								      					<input class="long-button" type="button" onclick="showBoxContainers('<%=queryRow.get("lpt_id",0) %>','<%= boxTypeMgrZr.getBlpTypeName(queryRow) %>');" value="Manage BLP">
													<br />
													 <input class="long-button" type="button" onclick="print('<%=ContainerTypeKey.BLP %>','<%=queryRow.get("basic_type_id",0)%>','<%=queryRow.get("is_has_sn",0)%>')" value="Print And BLP">
													<br />
													<%if(!isFirst){ %>
														<a class="common up" title="Up" onclick="exchangePs('<%=queryRow.get("lp_title_ship_id",0l) %>','<%=arrayDBRows.get(index-1).get("lp_title_ship_id",0l)%>')" href="javascript:void(0)"></a> 
													<%} %>
													<%if(!isLast){ %>
														<a class="common down" title="Down" onclick="exchangePs('<%=queryRow.get("lp_title_ship_id",0l) %>','<%=arrayDBRows.get(index+1).get("lp_title_ship_id",0l)%>')" href="javascript:void(0)"></a> 
													<%} %>
		<%--														<a class="delete" title="删除" href="javascript:void(0)"></a>--%>
								      				</td>		
												</tr>
											</table>
		      								</fieldset>		
		      								 
		      							<% 
		      						}
		      					}
		      					%>
		      				</td>
		      			</tr>
		      			<tr class="split">
		      				<td colspan="3" align="right"></td>
		      			</tr>
		      		<% 	
		      		}
		      	%>	 
			    <%}else{%>
		      		<tr>
		      		 	<td colspan="3" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Datas</td>
		      			
		      		</tr>
		      	<%} %>
		      		
		      </tbody>
	</table>
</body>
</html>
