<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.ContainerTypeKey"%>
<%@page import="com.fr.base.core.json.JSONObject,com.cwc.app.floor.api.zr.FloorClpTypeMgrZr,com.cwc.app.floor.api.zyj.service.*,com.cwc.app.floor.api.zyj.model.*"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@include file="../../include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Batch Print TLP</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<!-- 替换标签占位符 -->
<script type="text/javascript" src="../js/labelTemplate/assembleUtils.js"></script>
<link rel="stylesheet" type="text/css" href="../js/labelTemplate/assembleUtils.css"/>
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<!-- 时间控件 -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- select2选项卡 -->
<link rel="stylesheet" href="../js/select2-4.0.0-rc.2/dist/css/select2.css">
<link rel="stylesheet" href="../jquery-select2-custom.css">
<script type="text/javascript" src="../jquery-select2-custom.js"></script>
<!-- 提示信息 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="../buttons.css"  />
<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />
<%
int container_type = StringUtil.getInt(request, "container_type");	
long type_id	= StringUtil.getLong(request, "type_id");	
int is_has_sn = StringUtil.getInt(request, "is_has_sn");

//手动选择title和customer时，构造选项，gql 2015/04/27
	DBRow[] rows = productLableTemp.findAllLable("", ContainerTypeKey.TLP+"", 2, null);

	
	HttpSession sess = request.getSession(true);
	Map<String,Object> loginUser = (Map<String,Object>) sess.getAttribute("adminSesion");
	int corporationType = (Integer)loginUser.get("corporationType");
	int customerId = 0;
	//存放登录用户类型，表示用户是admin,是Customer,还是Title或者其它
	String userType = "";
	if(corporationType == 1){
		customerId = (Integer)loginUser.get("corporationId");
		userType = "Customer";
	}else if(Boolean.TRUE.equals(loginUser.get("administrator"))){
		userType = "Admin";
	}
	
    ProductCustomerSerivce  productCustomerService = (ProductCustomerSerivce)MvcUtil.getBeanFromContainer("productCustomerSerivce");
    CustomerService customerService = (CustomerService)MvcUtil.getBeanFromContainer("customerService");
    TitleService titleService = (TitleService)MvcUtil.getBeanFromContainer("titleService");
    List<Customer> customers = null;
    List<Title> titles = null;
	long pc_id = 0L;
	FloorClpTypeMgrZr floorClpTypeMgrZr = (FloorClpTypeMgrZr)MvcUtil.getBeanFromContainer("floorClpTypeMgrZr");
	DBRow con_product = floorClpTypeMgrZr.findContainerProduct(type_id);
	if(con_product != null){
		pc_id = con_product.get("pc_id", 0L);
	    if(!userType.equals("Customer")){
	        customers = customerService.getCustomers((int)pc_id);
	        titles = titleService.getTitles((int)pc_id);
	    }else{
	        Customer customer = customerService.getCustomer(customerId);
	        customers = new ArrayList<Customer>();
	        if(customer != null){
	        	customers.add(customer);
	        }
	        titles = titleService.getTitles((int)pc_id,customerId);
	    }
	}else{
	    if(!userType.equals("Customer")){
	    	titles = titleService.getAll();
	        customers = customerService.getAll();
	    }else{
	    	Customer customer = customerService.getCustomer(customerId);
	        customers = new ArrayList<Customer>();
	        if(customer != null){
	        	customers.add(customer);
	        }
	        titles = titleService.getTitlesByCustomerId(customerId);
	    }
	}
	
	String date = new TDate().getFormateTime(DateUtil.NowStr(), "MM/dd/yy");
	String title_options = "";
	
	boolean one_title_flag = (titles.size() == 1);
	for (Title title : titles) {
		String title_name = title.getName();
		long title_id = title.getId();
		String selected = "";
		if(one_title_flag){
			selected = "selected='selected'";
		}
		title_options += "<option value='"+ title_id+"' " + selected + ">"+title_name+"</option>";
	}
	
	String customer_options = "";
	boolean one_customer_flag = customers.size() == 1;
	for (Customer customer : customers) {
		String selected = "";
		if(one_customer_flag || customer.getId() == customerId){
			selected = "selected='selected'";
		}
		customer_options += "<option value='"+ customer.getId() +"' "+selected+">"+ customer.getName() +"</option>";
	}  
	
	JSONObject datas = new JSONObject();
	datas.put("CUSTOMER", "&nbsp;");
	datas.put("PCODE", "NA");
	datas.put("TOTPDT", "&nbsp;");
	datas.put("TOTPKG", "&nbsp;");
	datas.put("LOT", "&nbsp;");
	datas.put("TITLE", "&nbsp;");
	
//手动选择title和customer时，构造选项，gql 2015/04/27  end
%>

<script type="text/javascript">
var productId = "<%=pc_id %>";
var userType = "<%=userType %>";
	$(function(){
		var lable_template = $("#rongqi").html();
		initLableTemplate(lable_template);
	});
	
	//选择不同模板，替换显示的模板
	function changeLable(){
		var count = $("#lable_template").val();
		if(count == ""){
			$("#rongqi").css("display","none");
			return;
		}
		$("#rongqi").css("display","");
		
		var printName = $("#printName_"+count).val();
		$("#printName").val(printName);
		var lable_template = $("#lable_"+count).html();
		initLableTemplate(lable_template);
	}
	
	//初始化模板
	function initLableTemplate(lable_template){
		<%-- var datas = <%=datas%>;
		var html = assembleTemplate(datas,lable_template);
		$("#rongqi").html(html); --%>
		
		$("#rongqi").html(lable_template);
		var $rongqi = $("#rongqi");
		//初始化标签时间显示可修改状态 gql 
		if($("input[name='input_date']",$rongqi).length){
			$("span[name='dateShow']",$rongqi).css("display","none");
			$("input[name='input_date']",$rongqi).css("display","");
			$("span[name='dateShow']",$rongqi).html('<%=date%>');
			$("input[name='input_date']",$rongqi).val('<%=date%>');
			//添加时间控件		
			$("input[name='input_date']",$rongqi).datepicker({
					dateFormat:"mm/dd/y",
					changeMonth: true,
					changeYear: true
				}).bind("change",function(){
			    	  var input_date = $(this).val();
			    	  var $obj = $(this).prev("span[name='dateShow']").html(input_date);
			      });
				
			$("#ui-datepicker-div").css("display","none");
		}
		
		//TLP标签，初始化pcode可修改状态，并且修改时，修改条码,注意：修改打印内容的date/pcode页面布局时，要修改此处  gql 
		$("input[name='pcodeInput']",$rongqi).css("display","");
		$("div[name='pcodeText']",$rongqi).css("display","none");
<%-- 		$("input[name='pcodeInput']",$rongqi).val('<%=row!=null?row.getString("pcode"):"NA"%>'); --%>
		$("input[name='pcodeInput']",$rongqi).val('NA'); 
		//pcode输入框值改变时，修改pcode条码和pcode显示内容
		$("input[name='pcodeInput']",$rongqi).bind('change',function(){
		 	var $this = $(this);
		 	var $img =  $this.parent("div").parent("td").find("img[name='pcodeImg']");
		 	var $div =  $this.parent("div").parent("td").find("div[name='pcodeText']");
		 	var pcode_val = $this.val().toLocaleUpperCase().trim();
		 	//验证格式为大写字,数字,$,-,+,=,/
		 	var match = /^(?=[0-9A-Z-$+=/]*$)/g.test(pcode_val);
		 	if(pcode_val.length>0&&match){
		 		var src = '/barbecue/barcode?data='+pcode_val+'&width=1&height=50&type=code39';
			 	$img.attr("src",src);
			 	$div.html(pcode_val);
		 	}else{
		 		setTimeout(function(){
		 			$this.focus();
		 		}, 0);
		 		showMessage("Enter the letters and numbers","alert");
		 	}
		});
		$("#lable_template").select2({
            placeholder: "Select label...",
            allowClear: true
        });
		
		//初始化customer手动选择，隐藏打印时的div
		var customer_options = "<%=customer_options%>";
		$("select[name='select_customer']",$rongqi).append(customer_options);
		$("div[name='show_customer']",$rongqi).css("display","none");
		$("select[name='select_customer']",$rongqi).select2({
			 placeholder: "Select..."
	        ,allowClear: true
	      }).bind("change",function(event){
	    	  var customerId = $(this).val();
	    	  var customer = $(this).children("option[value='" + customerId + "']").text();
	    	  $(this).prev("div[name='show_customer']").find("center").html(customer+"&nbsp;&nbsp;");
	    	  if(userType == "Customer"){
	    		  return ;
	    	  }	    	  
	          
	          if($.trim(customerId).length == 0){
	        	  customerId = 0;
	          }
	          var target = event.target; 
	          var url = "/Sync10/basicdata/title/customer/" + customerId
	          if(parseInt(productId) > 0){
	        	  url = "/Sync10/basicdata/productCustomer/" + productId + "/customer/" + customerId;
	          }
	          
	          $.ajax({
	               type : "get",  
	               url  : url,
	               success : function(data){
	            	   var titleId =  $(target).parent().next().find("select[name='select_title']").val();

	                   if(data && data.length > 0){
		            	   //标识当前选中的Title 是否出现在新的Title 列表中
		            	   var exist = false;
	                	   $(target).parent().next().find("select[name='select_title']").children().remove("option[value!='']");
	                       for(var i = 0; i < data.length; i++){
	                           if(titleId == data[i].id){
	                        	   exist = true;
	                           }
	                           $(target).parent().next().find("select[name='select_title']").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
	                       }
                           if(!exist){
                        	 	$(target).parent().next().find("span.select2-selection__rendered").html("<span class=\"select2-selection__placeholder\">Select...</span>");
                        	 	$(target).parent().next().find("div[name='show_title']").find("center").html("");
                           }else{
                        	    $(target).parent().next().find("select[name='select_title']").val(titleId);
                           }
	                   }
	               }
	          });   
	      });
	  	  var customer2 = $("select[name='select_customer']",$rongqi).children("option:selected").text();
		  $("select[name='select_customer']",$rongqi).prev("div[name='show_customer']").find("center").html(customer2+"&nbsp;&nbsp;");			
		//初始化title手动选择，隐藏打印时的div
		var html = "<%=title_options%>";
		$("select[name='select_title']",$rongqi).append(html);
		$("div[name='show_title']",$rongqi).css("display","none");
		$("select[name='select_title']",$rongqi).select2({
				 placeholder: "Select..."
		        ,allowClear: true
	      }).bind("change",function(){
	    	  var title = $(this).children("option[value='" + $(this).val() + "']").text();
	    	  $(this).prev("div[name='show_title']").find("center").html(title);
	      });
	  	  var title2 = $("select[name='select_title']",$rongqi).children("option:selected").text();
	  	  $("select[name='select_title']",$rongqi).prev("div[name='show_title']").find("center").html(title2);
	}
</script>

</head>
<style>
.select2-selection{text-align: left;}
</style>
<body>
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			Create And Print[<%=new ContainerTypeKey().getContainerTypeKeyValue(container_type) %>]
		</legend>	
		<form action="" id="subForm" method="post">
		<input type="hidden" name="container_type" value="<%=container_type %>"/>
		<input type="hidden" name="type_id" value="<%=type_id %>"/>
		<input type="hidden" name="is_has_sn" value="<%=is_has_sn%>"/>
			<table>
				<tr>
					<td><span style="color:red">* </span>Quantity</td>
					<td>
						<input name="print_count" id="print_count" style="height: 28px;"/>
						<!-- <span style="color:red">Input the quantity.</span> -->
					</td>
					<td>
						<div align="right" style=" padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
							Label Name:&nbsp; <select id="lable_template" name="lable_template" onchange="changeLable()" style="width: 200px;">
								<option value=""></option>
								<%for(int i=0;i<rows.length;i++){ %>
									<option value="<%=i%>"  <%if(i == 0){%>selected<% } %>> &nbsp;<%=rows[i].getString("lable_name")%>&nbsp;</option>
								<%} %>
							</select>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</td>
					<td>
						<div align="right" style=" width:100px;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
							<!-- <input class="long-button-print" type="button" onclick="print('102')" value="  Print"> -->
							<a value="  Print" class="buttons" onclick="print('102')" style="width: 45px;"><i class="icon-print"></i>&nbsp;Print</a>
						</div>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<%for(int i=0;i<rows.length;i++){ %>
		<div id="lable_<%=i%>" style="display: none;"><%=rows[i].getString("lable_content")%></div>
		<input type="hidden" id="printName_<%=i%>" name="printName" value="<%=rows[i].get("print_name","")%>" />
	<%} %>
	<div align="center" >
		 <%if(rows!=null&&rows.length>0){%>
			<div id="rongqi" align="left" style="width:368px; border:1px solid red">
				<%=rows[0].getString("lable_content")%>
	    	</div>
	    	<input type="hidden" id="printName" name="printName" value="<%=rows[0].get("print_name","")%>" />
		 <% } %>
	    <div id="printAll" style="display: none;"></div>
	</div>
</body>
</html>
<script>
	//设置打印内容  gql,注意：修改打印内容的date/pcode页面布局时，要修改此处 2015/04/27
	function setPrintAll(){
		var $obj = $("#rongqi");
		var html = $obj.html();//获取打印的内容
		$("#printAll").html(html);//赋值打印的内容
		
		setTlpPrintAll($("#printAll"));//设置tlp打印的样式
	}

	function print(targetTr){
		if(validateCount())
		{
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/container/ContainerAddBatchAction.action',
				data:$("#subForm").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.length > 0)
					{
						setPrintAll();//设置tlp打印样式
						for(var i = 0; i < data.length; i ++)
						{
							/* var $obj = $("#printAll");
							var $conId = $("td[name='td_conid']",$obj);
							var $typeId = $("div[name='show_typeid']",$obj);
							var typeHtml = '<b><center>'+data[i].typeid+'&nbsp;</center><b>';
// 							'<img src="/barbecue//barcode?data='+data[i].con_id+'&width=1&height=40&type=code39&drawText=true&st=true" />'+
							var ctnHtml ='<img src="/barbecue/barcode?data='+data[i].con_id+'&width=1&height=50&type=code39&drawText=true&st=true"><br>'
					        			+'<div style="font-size: 30px;font-weight:bold;"><center>'+data[i].container+'</center></div>'; 
					        		 
					        $conId.html(ctnHtml);
					        $typeId.html(typeHtml);
						
							var html=$obj.html();
							printInstantly(html); */
							var temp = {
									"TYPEID":data[i].typeid,
									"TOTPDT":data[i].totpdt,
									"TOTPKG":data[i].totpkg,
									"LOT":data[i].lot,
									"CUSTOMER":data[i].customer,
									"TITLE":data[i].title,
									"CONID":data[i].conid,
									"PCODE":data[i].pcode,
							};
							
							var json_data = JSON.stringify(temp); 
							//var contentPrint = assembleTemplate(JSON.parse(data[i]), $("#printAll").html());
							var contentPrint = assembleTemplate(JSON.parse(json_data), $("#printAll").html());
							printInstantly(contentPrint);
									
						}
						$.artDialog && $.artDialog.close();
					}
					else
					{
						showMessage("Unsuccessfully","alert");
					}
				},
				error:function(){
					showMessage("System error","error");
				}
			})	
		}
	};
	function windowClose()
	{
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	
	function validateCount()
	{
		if('' == $("#print_count").val())
		{
			showMessage("Input the quantity","alert");
			return false;
		}
		if('' == $("#lable_template").select2("val"))
		{
			showMessage("Select label","alert");
			return false;
		}
		return true;
	}
	
	//打印
	function printInstantly(html){
    	var printer_count =  visionariPrinter.GET_PRINTER_COUNT(); //获取打印机名字列表
    	
	    //判断是否有该名字的打印机
    	var printer = $("#printName").val();
//     	var printer = "LabelPrinter";

    	var printerExist = "false";
    	var containPrinter = printer;
		for(var i = 0;i<printer_count;i++){
			if(visionariPrinter.GET_PRINTER_NAME(i).indexOf(printer) >= 0 )
			{
				printerExist = "true";
				containPrinter=visionariPrinter.GET_PRINTER_NAME(i);
				break;
			}
		}
		if(printerExist=="true"){
				//判断该打印机里是否配置了纸张大小
				var paper="102X152";
			    var strResult=visionariPrinter.GET_PAGESIZES_LIST(containPrinter,",");
			    var str=strResult.split(",");
			    var status=false;
			    for(var i=0;i<str.length;i++){
                       if(str[i]==paper){
                          status=true;
                       }
				}
			    if(status==true){
                   	 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Container Label");
                   	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                     visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
   				     visionariPrinter.ADD_PRINT_HTM(0,0,"97%","100%",html);
   					 visionariPrinter.SET_PRINT_COPIES(1);
//     				 visionariPrinter.PREVIEW();
   					 visionariPrinter.PRINT();
               }else{
            	   $.artDialog.confirm("Please Change Paper", function(){
        				 this.close();
            			}, function(){
        			});
               }	
		}else{
			var op=visionariPrinter.SELECT_PRINTER();//弹出手动选择打印机界面
			if(op!=-1){ //判断是否点了取消
				 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Container Label");
               	 visionariPrinter.SET_PRINTER_INDEXA (containPrinter);//指定打印机打印  
                 visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");
				 visionariPrinter.ADD_PRINT_HTM(5,3,"97%","100%",html);
   				 visionariPrinter.SET_PRINT_COPIES(1);
//    				 visionariPrinter.PREVIEW();
   				 visionariPrinter.PRINT();
			}	
		}
	}

	
	
</script>