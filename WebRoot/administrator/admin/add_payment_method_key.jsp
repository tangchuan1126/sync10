<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.PaymentMethodKey,java.util.ArrayList,com.cwc.app.key.BillTypeKey,com.cwc.app.key.InvoiceStateKey,com.cwc.app.key.BillStateKey" %>

 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>收款账号配置</title>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
 
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
	table{width:98%;border-collapse:collapse;}
	td.left{width:100px;text-align:right;}
	div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:20px;text-align:center;border:1px solid silver;margin:0px auto;width:100px;height:30px;line-height:30px;background:white;}
  
</style>
<%
	PaymentMethodKey methodKey = new PaymentMethodKey();
	String exitsTipPayMentMethd = ConfigBean.getStringValue("systenFolder") + "action/administrator/PayMentMethod/ExitsTipPayMentMethod.action";
	String addPayMentMethod =   ConfigBean.getStringValue("systenFolder") + "action/administrator/payMentMethod/AddPayMentMethodAction.action";
	String updatePayMentMethod = ConfigBean.getStringValue("systenFolder") + "action/administrator/payMentMethod/UpdatePayMentMethodAction.action"; 
	
	long id = StringUtil.getLong(request,"id");
	DBRow row = new DBRow();
	if(id != 0l){
		row = orderMgrZr.getDetailById(id,"account_payee","id");
	}
%>
<script type="text/javascript">
	function keyTypeChange(){
		var key  = $("#key");
		if( key.val() * 1 == -1){
			return ;
		}
		windowLock();
		$.ajax({
			url:'<%= exitsTipPayMentMethd%>' + "?key="+key.val(),
			dataType:'text',
			success:function(data){
				windowUnLock();
				if(data === "true"){
					 // 如果是已经有了，那么就不能添加is_tip的了
					addSelectIsTip([0])
				}else{
					addSelectIsTip([0,1]); 
				}
			},
			error:function(){
				windowUnLock();
			}
		})
	}
	function windowLock(){$(".cssDivschedule").css("display","block");}
	function windowUnLock(){$(".cssDivschedule").fadeOut();}
	function addSelectIsTip(array){
		var is_tip = $("#is_tip");
		$("option",is_tip).remove();
	 
		var str = "";
		for(var index = 0 , count = array.length ; index <  count ; index++ ){
			var temp = array[index] * 1 ;
			if(temp == 0){
				str += "<option value='0'>否</option>";
			}
			if(temp == 1){
				str += "<option value='1'>是</option>";
			}
		}
		is_tip.html(str);
	}
	function save(){
		var key = $("#key").val();
		if(key * 1 < 0){
			showMessage("选择收款类型","alert")
			return ;
		}
		var is_tip = $("#is_tip").val();
		var accout_name = $("#accout_name").val();
		var account = $("#account").val();
		
		if(accout_name.length < 1){
			showMessage("请填写收款人","alert");
			return ;
		}
		if(account.length < 1){
			showMessage("请填写收款账号","alert");
			return ;
		}

		windowLock();
 		if('<%= id%>' * 1  > 0){
 			updatePayMentMethod(accout_name,account,'<%= id%>');
 	 	}else{
 	 	  
			$.ajax({
				url:'<%= addPayMentMethod%>?' + "key="+key+"&account_name="+accout_name+"&account="+account+"&is_tip="+is_tip,
				dataType:'text',
				success:function(data){
					windowUnLock();
					if(data === "success"){
						$.artDialog.close();
					   $.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
					}else{
						showMessage("系统错误请稍后再试","error");
					}
				},
				error:function(data){
					windowUnLock();
					showMessage("系统错误请稍后再试","error");
				}
			})
 	 	}
	}
		
	function updatePayMentMethod(account_name , account , id){
			$.ajax({
				url:'<%= updatePayMentMethod%>' + "?id=" + id + "&account_name=" + account_name + "&account=" + account,
				dataType:'text',
				success:function(data){
					windowUnLock();
					if(data === "success"){
						$.artDialog.close();
						 $.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
					}else{
						showMessage("系统错误请稍后再试","error");
					}
				},
				error:function(data){
					windowUnLock();
					showMessage("系统错误请稍后再试","error");
				} 
			})
	}
	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
	jQuery(function($){
 
		if('<%= id%>' * 1 > 0){
 
			// 表示的是修改收款账号
			//初始化类型和是否提示信息
			 $("#key option[value='"+'<%= row.get("key_type",0)%>'+"']").attr("selected",true);
			 $("#is_tip option[value='"+'<%= row.get("is_tip",0)%>'+"']").attr("selected",true);
			$("#key").attr("disabled",true);
			$("#is_tip").attr("disabled",true);
		}	
	})
	function cancel(){
		$.artDialog.close();
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="">
	<input type="hidden" id="is_tip_input" />
 	  <div class="cssDivschedule">
	 		<div class="innerDiv">
	 			请稍等.......
	 		</div>
	  </div>
	<h1 style="font-size:12px;">添加收款账号</h1>
	<table>
		<tr>
			<td class="left">收款类型:</td>
			
		
			
			<td class="right">	
				<select id="key" name="key" onchange="keyTypeChange();">
					<option value="-1">请选择</option>	
					<%
				 		ArrayList<String> keys = methodKey.getStatus();
				 		if(keys != null && keys.size() > 0){
				 			for(String keyType : keys){
				 			%>
				 			<option value='<%= keyType %>'><%= methodKey.getStatusById(Integer.parseInt(keyType))  %></option>
				 			<% 	
				 			}
				 		}
				 	%> 
				 	</select>
			</td>
		</tr>
		<tr>
			<td class="left">收款人:</td>
			<td class="right">
				<input type="text" name="account_name" id="accout_name"  style="width:250px;" value='<%= row.getString("account_name") %>'/>
			</td>
		</tr>
		<tr>
			<td class="left">收款账号:</td>
			<td class="right">
				<input type="text" name="name" id="account" style="width:250px;" value='<%= row.getString("account") %>'/>
			</td>
		</tr>
		<tr>
			<td class="left">提示信息:</td>
			<td class="right">
				<select id="is_tip" name="is_tip">
					<option value="0">否</option>
					<option value="1">是</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center;padding-top:10px;">
				<input type="button" class="short-button" value="保存" onclick="save();" />
				<input type="button" class="short-button" value="取消" onclick="cancel();" />
			</td>
		</tr>
	</table>
</body>
</html>
