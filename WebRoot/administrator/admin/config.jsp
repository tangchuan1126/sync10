<%@ page contentType="text/html;charset=utf-8"%>
<%@ include file="../../include.jsp"%> 

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script language="javascript" src="../../common.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 
<style type="text/css">
 	span.colorShow{text-align:center;height:20px;line-height:20px;width:80px;border:1px solid silver;display:block;margin-left:200px;background:white;cursor:pointer;}
 	span.colorResult{display:block;float:left;text-align:center;height:28px;line-height:28px;width:100px;border:1px solid silver;display:block;margin-left:10px;background:white;cursor:pointer;}
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
$(function(){
	 $("#tabs").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
		});
	$("#period_info").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
		});
		
 })
 
function modify(name)
{
	theForm = 	$("form[name='"+name+"']")  ;

	//if ( theForm.webtitle.value=="" )
	//{
	//	alert("<%=systemConfig.getStringConfigDescription("webtitle")%>不能为空");
	//}
	//else if ( theForm.url.value=="" )
	//{
	//	alert("<%=systemConfig.getStringConfigDescription("url")%>不能为空");
	//}
	//else 
	//{
		theForm.submit();
	//}
}

function showBMsg()
{
	var result = window.showModalDialog('upload_b_if.html',window,"dialogWidth:20;dialogHeight:15;status:no;help:no");

	if ( !(typeof result == 'undefined') )
	{
		document.newform.logo.value = result;
		document.getElementById('img_s').src = "<%=ConfigBean.getStringValue("systenFolder")%>.<%=ConfigBean.getStringValue("upload_pro_img_tmp")%>"+result;
	}
}

function flushAllHtmlCache()
{
	if ( confirm("执行该操作后，会清空所有缓冲，确定执行该操作吗？"))
	{
		document.cache_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/flushAllHtmlCache.action";
		document.cache_form.submit();
	}
}

function trackingWaybill()
{
	if ( confirm("确定执行运单追踪通讯吗？"))
	{
		document.tracking_waybill_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/waybill/trackingWaybill.action";
		document.tracking_waybill_form.submit();
	}
}

function cleanHotSearchKey()
{
	if ( confirm("执行该操作后，会清空搜索热点关键字，确定执行该操作吗？"))
	{
		document.cache_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/cleanHotSearchKey.action";
		document.cache_form.submit();
	}
}

function synAccount2Workflow()
{
	if ( confirm("确定重新同步帐号数据到工作流引擎？"))
	{
		document.syn_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/admin/SynAccount2Workflow.action";
		document.syn_form.submit();
	}
}
//-->
</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style></head>

<body onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 系统管理 »   系统变量</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
<form action="" name="syn_form" method="post">
</form>
<form name="tracking_waybill_form" method="post">
</form>

<form action="" name="cache_form" method="post">
</form>
  <tr>
    <td width="19%">
      <input name="Submit2" type="button" class="long-button" value="清空缓存" onClick="flushAllHtmlCache()">
	  &nbsp;&nbsp;&nbsp;
	   <input name="Submit22" type="button" class="long-long-button-ok" value="  同步帐号到工作流" onClick="synAccount2Workflow()">
	   &nbsp;&nbsp;&nbsp;
	   <input name="Submit22" type="button" class="long-button" value="运单追踪" onClick="trackingWaybill()">
	   
    </td>
  </tr>
</table>
<br>
<div id="tabs" >
	 <ul>
		 <li><a href="#system_info">系统信息</a></li>
		 <li><a href="#email_info">邮件信息</a></li>		
		 <li><a href="#order_info">订单信息</a></li>
		 <li><a href="#list_info">列表信息</a></li>
		 <li><a href="#coefficient_info">利率信息</a></li>
		 <li><a href="#profit_info">汇率信息</a></li>	
		 <li><a href="#period_info">周期信息</a></li>
		 <li><a href="#reason_info">退货信息</a></li>	 
		 <li><a href="#transport_info">转运单信息</a></li> 
		 <li><a href="#file_up_info">文件上传路径配置</a></li> 
		 <li><a href="#text_description">文字描述</a></li>
		 <li><a href="#b2b_order_info">B2B订单信息</a></li>
	 </ul>
	 <div id="system_info">
		<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="sys_form">
			<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
	 				<input type="hidden" name="logo" value="<%=systemConfig.getStringConfigValue("logo")%>">
		     	<tr> 
		       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
		    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		     	</tr>
			    <tr> 
			     	<td height="31" valign="middle" > 
			      	  <%=systemConfig.getStringConfigDescription("webtitle")%>      </td>
			     	<td valign="middle" > <input name="webtitle" type="text" value="<%=systemConfig.getStringConfigValue("webtitle")%>" size="70"></td>
			    </tr>
			    <tr> 
			     	<td height="31" valign="middle" > 
			       	<%=systemConfig.getStringConfigDescription("url")%>      </td>
			     	<td valign="middle" > <input name="url" type="text" value="<%=systemConfig.getStringConfigValue("url")%>" size="50">
			    		  结尾不要跟 /</td>
			    </tr>
			    <tr> 
			    	<td height="31" valign="middle" > 
			      		<%=systemConfig.getStringConfigDescription("admin_login_path")%>      </td>
			    	<td valign="middle" >/administrator/ &nbsp;<input name="admin_login_path" type="text" value="<%=systemConfig.getStringConfigValue("admin_login_path")%>" size="15">      </td>
			    </tr>
			     <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("foot")%>      </td>
			      <td valign="middle" >
			  		<textarea name="foot" cols="50" rows="7"><%=systemConfig.getStringConfigValue("foot")%></textarea></td>
			    </tr>	
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("short_message_balance")%>      </td>
			      <td valign="middle" >
			  		<%=systemConfig.getStringConfigValue("short_message_balance")%></td>
			    </tr>		
			    <tr> 
				   <td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('sys_form')"></td>
				</tr>
			</table>
		</form>
	 </div>
	 <div id="email_info">
	 	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="email_form">
			<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
			    <tr> 
		       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
		    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		     	</tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("sendmail_ssl_flag")%>      </td>
			      <td valign="middle" >  
			        <input type="radio" name="sendmail_ssl_flag" value="0" <%=systemConfig.getIntConfigValue("sendmail_ssl_flag")==0?"checked":""%>> 普通登录
			 		  &nbsp;&nbsp;&nbsp;
			        <input type="radio" name="sendmail_ssl_flag" value="1"  <%=systemConfig.getIntConfigValue("sendmail_ssl_flag")==1?"checked":""%>> SSL登录       </td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("mailAuthor")%>      </td>
			      <td valign="middle" > <input name="mailAuthor" type="text" value="<%=systemConfig.getStringConfigValue("mailAuthor")%>" size="50">
			     		   一般跟“发送邮件帐号”一致</td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("sendMailSmtp")%>      </td>
			      <td valign="middle" > <input name="sendMailSmtp" type="text" value="<%=systemConfig.getStringConfigValue("sendMailSmtp")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("sendMailAccount")%>      </td>
			      <td valign="middle" > <input name="sendMailAccount" type="text" value="<%=systemConfig.getStringConfigValue("sendMailAccount")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("sendMailPwd")%>      </td>
			      <td valign="middle" > <input name="sendMailPwd" type="text" value="<%=systemConfig.getStringConfigValue("sendMailPwd")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("sendMailSender")%>      </td>
			      <td valign="middle" > <input name="sendMailSender" type="text" value="<%=systemConfig.getStringConfigValue("sendMailSender")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("sendMailSmtpPort")%>      </td>
			      <td valign="middle" > <input name="sendMailSmtpPort" type="text" value="<%=systemConfig.getStringConfigValue("sendMailSmtpPort")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("sendMailReplyMail")%>      </td>
			      <td valign="middle" > <input name="sendMailReplyMail" type="text" value="<%=systemConfig.getStringConfigValue("sendMailReplyMail")%>" size="50"></td>
			    </tr>
			    <tr> 
			  		<td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('email_form')" ></td>
			 	</tr>
 			</table>
  		  </form>
	  </div>
	  <div id="order_info">
		 <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="order_form">
			<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
			    <tr> 
		       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
		    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		     	</tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("ask_type")%>      </td>
			      <td valign="middle" > 
			      <textarea name="ask_type" cols="50" rows="7"><%=systemConfig.getStringConfigValue("ask_type")%></textarea></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("business")%>      </td>
			      <td valign="middle" > 
				   <textarea name="business" cols="50" rows="7"><%=systemConfig.getStringConfigValue("business")%></textarea>	  </td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("clients_service")%>      </td>
			      <td valign="middle" > 
				   <textarea name="clients_service" cols="50" rows="7"><%=systemConfig.getStringConfigValue("clients_service")%></textarea>	  </td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("invalid_color")%>      </td>
			      <td valign="middle" > <input name="invalid_color" type="text" value="<%=systemConfig.getStringConfigValue("invalid_color")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("listorder_date_interval")%>      </td>
			      <td valign="middle" > <input name="listorder_date_interval" type="text" value="<%=systemConfig.getStringConfigValue("listorder_date_interval")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("only_deliver")%>      </td>
			      <td valign="middle" > <input name="only_deliver" type="text" value="<%=systemConfig.getStringConfigValue("only_deliver")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("only_print")%>      </td>
			      <td valign="middle" > <input name="only_print" type="text" value="<%=systemConfig.getStringConfigValue("only_print")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("order_flush_second")%>      </td>
			      <td valign="middle" > <input name="order_flush_second" type="text" value="<%=systemConfig.getStringConfigValue("order_flush_second")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("order_page_count")%>      </td>
			      <td valign="middle" > <input name="order_page_count" type="text" value="<%=systemConfig.getStringConfigValue("order_page_count")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("payment_status_color")%>      </td>
			      <td valign="middle" > <input name="payment_status_color" type="text" value="<%=systemConfig.getStringConfigValue("payment_status_color")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("payment_status_false_color")%>      </td>
			      <td valign="middle" > <input name="payment_status_false_color" type="text" value="<%=systemConfig.getStringConfigValue("payment_status_false_color")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("print_deliver")%>      </td>
			      <td valign="middle" > <input name="print_deliver" type="text" value="<%=systemConfig.getStringConfigValue("print_deliver")%>" size="50"></td>
			    </tr>
			    <tr> 
			  	  <td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('order_form')"></td>
			 	</tr>
		    </table>
		 </form>
	  </div>
	  <div id="list_info">
		 <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="list_form">
			<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
			    <tr> 
		       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
		    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		     	</tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("order_page_count")%>      </td>
			      <td valign="middle" > <input name="order_page_count" type="text" value="<%=systemConfig.getStringConfigValue("order_page_count")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle"> 
			        <%=systemConfig.getStringConfigDescription("page_size")%>      </td>
			      <td valign="middle" > <input name="page_size" type="text" value="<%=systemConfig.getStringConfigValue("page_size")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle"> 
			        <%=systemConfig.getStringConfigDescription("page_count")%>      </td>
			      <td valign="middle" > <input name="page_count" type="text" value="<%=systemConfig.getStringConfigValue("page_count")%>" size="50"></td>
			    </tr>
			    <tr> 
			  	  <td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('list_form')"></td>
			 	</tr>
		    </table>
		 </form>
	  </div>
	  <div id="coefficient_info">
		<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="coefficient_form">
			<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
			    <tr> 
		       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
		    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		     	</tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("product_cost_coefficient")%>      </td>
			      <td valign="middle" > <input name="product_cost_coefficient" type="text" value="<%=systemConfig.getStringConfigValue("product_cost_coefficient")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("shipping_cost_coefficient")%>      </td>
			      <td valign="middle" > <input name="shipping_cost_coefficient" type="text" value="<%=systemConfig.getStringConfigValue("shipping_cost_coefficient")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("weight_cost_coefficient")%>      </td>
			      <td valign="middle" > <input name="weight_cost_coefficient" type="text" value="<%=systemConfig.getStringConfigValue("weight_cost_coefficient")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("min_order_profit_ebay")%>      </td>
			      <td valign="middle" > <input name="min_order_profit_ebay" type="text" value="<%=systemConfig.getStringConfigValue("min_order_profit_ebay")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("min_order_profit_website")%>      </td>
			      <td valign="middle" > <input name="min_order_profit_website" type="text" value="<%=systemConfig.getStringConfigValue("min_order_profit_website")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("min_order_profit_directpay")%>      </td>
			      <td valign="middle" > <input name="min_order_profit_directpay" type="text" value="<%=systemConfig.getStringConfigValue("min_order_profit_directpay")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("min_order_profit_manual")%>      </td>
			      <td valign="middle" > <input name="min_order_profit_manual" type="text" value="<%=systemConfig.getStringConfigValue("min_order_profit_manual")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("min_order_profit_direct_buy")%>      </td>
			      <td valign="middle" > <input name="min_order_profit_direct_buy" type="text" value="<%=systemConfig.getStringConfigValue("min_order_profit_direct_buy")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("min_order_profit_local_buy")%>      </td>
			      <td valign="middle" > <input name="min_order_profit_local_buy" type="text" value="<%=systemConfig.getStringConfigValue("min_order_profit_local_buy")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("min_order_profit_pay")%>      </td>
			      <td valign="middle" > <input name="min_order_profit_pay" type="text" value="<%=systemConfig.getStringConfigValue("min_order_profit_pay")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("min_order_profit_quote")%>      </td>
			      <td valign="middle" > <input name="min_order_profit_quote" type="text" value="<%=systemConfig.getStringConfigValue("min_order_profit_quote")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("min_order_profit_split")%>      </td>
			      <td valign="middle" > <input name="min_order_profit_split" type="text" value="<%=systemConfig.getStringConfigValue("min_order_profit_split")%>" size="50"></td>
			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("min_order_profit_warranty")%>      </td>
			      <td valign="middle" > <input name="min_order_profit_warranty" type="text" value="<%=systemConfig.getStringConfigValue("min_order_profit_warranty")%>" size="50"></td>
			    </tr>
				<tr> 
			  	  <td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('coefficient_form')"></td>
			 	</tr>
			 </table>
		 </form>
	  </div>
	  <div id="profit_info">
		 <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="profit_form">
			<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
		    <tr> 
	       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
	    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
	     	</tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("USD")%>  (USD)    </td>
		      <td valign="middle" > <input name="USD" type="text" value="<%=systemConfig.getStringConfigValue("USD")%>" size="50"></td>
		    </tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("AUD")%>   (AUD)    </td>
		      <td valign="middle" > <input name="AUD" type="text" value="<%=systemConfig.getStringConfigValue("AUD")%>" size="50"></td>
		    </tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("EUR")%>   (EUR)    </td>
		      <td valign="middle" > <input name="EUR" type="text" value="<%=systemConfig.getStringConfigValue("EUR")%>" size="50"></td>
		    </tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("CAD")%>   (CAD)    </td>
		      <td valign="middle" > <input name="CAD" type="text" value="<%=systemConfig.getStringConfigValue("CAD")%>" size="50"></td>
		    </tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("CHF")%>   (CHF)    </td>
		      <td valign="middle" > <input name="CHF" type="text" value="<%=systemConfig.getStringConfigValue("CHF")%>" size="50"></td>
		    </tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("CZK")%>    (CZK)   </td>
		      <td valign="middle" > <input name="CZK" type="text" value="<%=systemConfig.getStringConfigValue("CZK")%>" size="50"></td>
		    </tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("GBP")%>   (GBP)    </td>
		      <td valign="middle" > <input name="GBP" type="text" value="<%=systemConfig.getStringConfigValue("GBP")%>" size="50"></td>
		    </tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("NOK")%>   (NOK)    </td>
		      <td valign="middle" > <input name="NOK" type="text" value="<%=systemConfig.getStringConfigValue("NOK")%>" size="50"></td>
		    </tr>
		      <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("exchange_rate_deviation")%></td>
		      <td valign="middle" > <input name="exchange_rate_deviation" type="text" value="<%=systemConfig.getStringConfigValue("exchange_rate_deviation")%>" size="50">%</td>
		    </tr>
		    <tr> 
		  	  <td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('profit_form')"></td>
		 	</tr>
		 </table>
		</form>
	  </div>
	  <div id="period_info">
		  <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="period_form">
			<ul>
				<li><a href="#order_period">订单周期</a></li>
				<li><a href="#waybill_period">运单周期</a></li>
				<li><a href="#transport_period">交货转运周期</a></li>
				<li><a href="#purchase_period">采购单周期</a></li>
				<li><a href="#repair_period">返修单周期</a></li>
				<li><a href="#other">其他周期</a></li>
			</ul>
			<div id="order_period">
				<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
					 <tr> 
			       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
			    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
			     	</tr>
					<tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("doubt_order_handle_period")%></td>
				      <td valign="middle"  ><input name="doubt_order_handle_period" type="text" value="<%=systemConfig.getStringConfigValue("doubt_order_handle_period")%>" size="15">
						 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("doubt_order_address_period")%></td>
				      <td valign="middle"  ><input name="doubt_order_address_period" type="text" value="<%=systemConfig.getStringConfigValue("doubt_order_address_period")%>" size="15">
						 天</td>
				    </tr>
					<tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("doubt_order_pay_period")%></td>
				      <td valign="middle"  ><input name="doubt_order_pay_period" type="text" value="<%=systemConfig.getStringConfigValue("doubt_order_pay_period")%>" size="15">
				  		    天</td>
				    </tr>
				    
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("delivery_exception_day")%></td>
				      <td valign="middle"  ><input name="delivery_exception_day" type="text" value="<%=systemConfig.getStringConfigValue("delivery_exception_day")%>" size="15">
				  		    天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("customs_exception_day")%></td>
				      <td valign="middle"  ><input name="customs_exception_day" type="text" value="<%=systemConfig.getStringConfigValue("customs_exception_day")%>" size="15">
				  		    天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("overdue_deliveryed_day")%></td>
				      <td valign="middle"  ><input name="overdue_deliveryed_day" type="text" value="<%=systemConfig.getStringConfigValue("overdue_deliveryed_day")%>" size="15">
				  		    天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("notyet_deliveryed_day")%></td>
				      <td valign="middle"  ><input name="notyet_deliveryed_day" type="text" value="<%=systemConfig.getStringConfigValue("notyet_deliveryed_day")%>" size="15">
				  		    天</td>
				    </tr>
		    	</table>
			</div>
			<div id="waybill_period">
				<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
					<tr> 
			       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
			    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
			     	</tr>
					<tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("waybill_stockout_period")%></td>
				      <td valign="middle"  ><input name="waybill_stockout_period" type="text" value="<%=systemConfig.getStringConfigValue("waybill_stockout_period")%>" size="15">
				  		    天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("waybill_overdue_send_period")%></td>
				      <td valign="middle"  ><input name="waybill_overdue_send_period" type="text" value="<%=systemConfig.getStringConfigValue("waybill_overdue_send_period")%>" size="15">
				  		   时</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("waybill_overdue_track_day")%></td>
				      <td valign="middle"  ><input name="waybill_overdue_track_day" type="text" value="<%=systemConfig.getStringConfigValue("waybill_overdue_track_day")%>" size="15">
				  		    天</td>
				    </tr>
				</table>
			</div>
			<div id="transport_period">
				<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
				    <tr> 
			       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
			    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
			     	</tr>
			     	<tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_ready_period")%></td>
				      <td valign="middle"  ><input name="transport_ready_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_ready_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_packing_period")%></td>
				      <td valign="middle"  ><input name="transport_packing_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_packing_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_intransit_period")%></td>
				      <td valign="middle"  ><input name="transport_intransit_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_intransit_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_alreadyreceive_period")%></td>
				      <td valign="middle"  ><input name="transport_alreadyreceive_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_alreadyreceive_period")%>" size="15">
				     	 时</td>
				    </tr>
				    
				    
			     	<tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_declaration_period")%></td>
				      <td valign="middle"  ><input name="transport_declaration_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_declaration_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_clearance_period")%></td>
				      <td valign="middle"  ><input name="transport_clearance_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_clearance_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_product_file_period")%></td>
				      <td valign="middle"  ><input name="transport_product_file_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_product_file_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_tag_period")%></td>
				      <td valign="middle"  ><input name="transport_tag_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_tag_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_third_tag_period")%></td>
				      <td valign="middle"  ><input name="transport_third_tag_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_third_tag_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_stock_in_set_period")%></td>
				      <td valign="middle"  ><input name="transport_stock_in_set_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_stock_in_set_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_certificate_period")%></td>
				      <td valign="middle"  ><input name="transport_certificate_period" type="text" value="<%=systemConfig.getStringConfigValue("transport_certificate_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("transport_quality_inspection")%></td>
				      <td valign="middle"  ><input name="transport_quality_inspection" type="text" value="<%=systemConfig.getStringConfigValue("transport_quality_inspection")%>" size="15">
				     	 天</td>
				    </tr>
			     </table>
			</div>
			<div id="purchase_period">
				<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
				    <tr> 
			       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
			    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
			     	</tr>
			     	<tr> 
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("purchase_invoice_period")%></td>
				      <td valign="middle"  ><input name="purchase_invoice_period" type="text" value="<%=systemConfig.getStringConfigValue("purchase_invoice_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("purchase_drawback_period")%></td>
				      <td valign="middle"  ><input name="purchase_drawback_period" type="text" value="<%=systemConfig.getStringConfigValue("purchase_drawback_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("purchase_price_period")%></td>
				      <td valign="middle"  ><input name="purchase_price_period" type="text" value="<%=systemConfig.getStringConfigValue("purchase_price_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("purchase_had_affirm_price")%></td>
				      <td valign="middle"  ><input name="purchase_had_affirm_price" type="text" value="<%=systemConfig.getStringConfigValue("purchase_had_affirm_price")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("purchase_arrive_examine")%></td>
				      <td valign="middle"  ><input name="purchase_arrive_examine" type="text" value="<%=systemConfig.getStringConfigValue("purchase_arrive_examine")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("purchase_delivery_product_period")%></td>
				      <td valign="middle"  ><input name="purchase_delivery_product_period" type="text" value="<%=systemConfig.getStringConfigValue("purchase_delivery_product_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("purchase_tag_period")%></td>
				      <td valign="middle"  ><input name="purchase_tag_period" type="text" value="<%=systemConfig.getStringConfigValue("purchase_tag_period")%>" size="15">
				     	 天</td>
				    </tr>
				     <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("purchase_third_tag_period")%></td>
				      <td valign="middle"  ><input name="purchase_third_tag_period" type="text" value="<%=systemConfig.getStringConfigValue("purchase_third_tag_period")%>" size="15">
				     	 天</td>
				    </tr>
				     <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("purchase_quality_period")%></td>
				      <td valign="middle"  ><input name="purchase_quality_period" type="text" value="<%=systemConfig.getStringConfigValue("purchase_quality_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("purchase_product_model")%></td>
				      <td valign="middle"  ><input name="purchase_product_model" type="text" value="<%=systemConfig.getStringConfigValue("purchase_product_model")%>" size="15">
				     	 天</td>
				    </tr>
				 </table>
			</div>
			<div id="repair_period">
				<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
				    <tr> 
			       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
			    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
			     	</tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_declaration_period")%></td>
				      <td valign="middle"  ><input name="repair_declaration_period" type="text" value="<%=systemConfig.getStringConfigValue("repair_declaration_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_clearance_period")%></td>
				      <td valign="middle"  ><input name="repair_clearance_period" type="text" value="<%=systemConfig.getStringConfigValue("repair_clearance_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_tag_period")%></td>
				      <td valign="middle"  ><input name="repair_tag_period" type="text" value="<%=systemConfig.getStringConfigValue("repair_tag_period")%>" size="15">
				     	 天</td>
				    </tr>
				    	<tr> 
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_certificate_period")%></td>
				      <td valign="middle"  ><input name="repair_certificate_period" type="text" value="<%=systemConfig.getStringConfigValue("repair_certificate_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_product_file_period")%></td>
				      <td valign="middle"  ><input name="repair_product_file_period" type="text" value="<%=systemConfig.getStringConfigValue("repair_product_file_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_quality_inspection")%></td>
				      <td valign="middle"  ><input name="repair_quality_inspection" type="text" value="<%=systemConfig.getStringConfigValue("repair_quality_inspection")%>" size="15">
				     	 天</td>
				    </tr>
				     <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_stock_in_set_period")%></td>
				      <td valign="middle"  ><input name="repair_stock_in_set_period" type="text" value="<%=systemConfig.getStringConfigValue("repair_stock_in_set_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_packing_period")%></td>
				      <td valign="middle"  ><input name="repair_packing_period" type="text" value="<%=systemConfig.getStringConfigValue("repair_packing_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_intransit_period")%></td>
				      <td valign="middle"  ><input name="repair_intransit_period" type="text" value="<%=systemConfig.getStringConfigValue("repair_intransit_period")%>" size="15">
				     	 天</td>
				    </tr>
				     <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_alreadyreceive_period")%></td>
				      <td valign="middle"  ><input name="repair_alreadyreceive_period" type="text" value="<%=systemConfig.getStringConfigValue("repair_alreadyreceive_period")%>" size="15">
				     	 天</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_ready_period")%></td>
				      <td valign="middle"  ><input name="repair_ready_period" type="text" value="<%=systemConfig.getStringConfigValue("repair_ready_period")%>" size="15">
				     	 天</td>
				    </tr>
			     	<tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_certificate")%></td>
				      <td valign="middle"  >
				      <textarea name="repair_certificate" cols="50" rows="7"><%=systemConfig.getStringConfigValue("repair_certificate")%></textarea>
				      </td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_product_file")%></td>
				      <td valign="middle"  >
				      <textarea name="repair_product_file" cols="50" rows="7"><%=systemConfig.getStringConfigValue("repair_product_file")%></textarea>
				     	</td>
				    </tr>
				    <tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("repair_tag_types")%></td>
				      <td valign="middle"  >
				       <textarea name="repair_tag_types" cols="50" rows="7"><%=systemConfig.getStringConfigValue("repair_tag_types")%></textarea>
				      </td>
				    </tr>
			    </table>
			</div>
			<div id="other">
				<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
				    <tr> 
			       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
			    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
			     	</tr>
					<tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("dispute_refund_period")%></td>
				      <td valign="middle"  ><input name="dispute_refund_period" type="text" value="<%=systemConfig.getStringConfigValue("dispute_refund_period")%>" size="15">
				   		   天</td>
				    </tr>
					<tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("dispute_warranty_period")%></td>
				      <td valign="middle"  ><input name="dispute_warranty_period" type="text" value="<%=systemConfig.getStringConfigValue("dispute_warranty_period")%>" size="15">
				     	 天</td>
				    </tr>
					<tr>
				      <td height="31" valign="middle"  ><%=systemConfig.getStringConfigDescription("other_dispute_period")%></td>
				      <td valign="middle"  ><input name="other_dispute_period" type="text" value="<%=systemConfig.getStringConfigValue("other_dispute_period")%>" size="15">
				     	 天</td>
				    </tr>
				  </table>
			</div>
			<input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('period_form')">
	 	 </form>
	  </div>
	  <div id="reason_info">
		 <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="reason_form">
			<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
			<tr> 
	       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
	    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
	     	</tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("return_product_reason")%>      </td>
		      <td valign="middle" > 
		      <textarea name="return_product_reason" cols="50" rows="7"><%=systemConfig.getStringConfigValue("return_product_reason")%></textarea></td>
		    </tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("bad_feedback_reason")%>      </td>
		      <td valign="middle" > 
		      <textarea name="bad_feedback_reason" cols="50" rows="7"><%=systemConfig.getStringConfigValue("bad_feedback_reason")%></textarea></td>
		    </tr>
		    <tr> 
		      <td height="31" valign="middle" > 
		        <%=systemConfig.getStringConfigDescription("warranty_reason")%>      </td>
		      <td valign="middle" > 
		      <textarea name="warranty_reason" cols="50" rows="7"><%=systemConfig.getStringConfigValue("warranty_reason")%></textarea></td>
		    </tr>
		    <tr> 
		  	  <td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('reason_form')"></td>
		 	</tr>
		</table>
	   </form>
	 </div>
	 <div id="transport_info">
	 	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="transport_info">
	 		<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
				<tr> 
		       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
		    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		     	</tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("transport_certificate")%>      </td>
			      <td valign="middle" > 
			      <textarea name="transport_certificate" cols="50" rows="7"><%=systemConfig.getStringConfigValue("transport_certificate")%></textarea></td>
			    </tr>
			   <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("transport_product_file")%>      </td>
			      <td valign="middle" > 
			      <textarea name="transport_product_file" cols="50" rows="7"><%=systemConfig.getStringConfigValue("transport_product_file")%></textarea></td>
			    </tr>
			     <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("transport_tag_types")%>      </td>
			      <td valign="middle" > 
			      <textarea name="transport_tag_types" cols="50" rows="7"><%=systemConfig.getStringConfigValue("transport_tag_types")%></textarea></td>
			    </tr>
			    <tr> 
			  	  <td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('transport_info')"></td>
			 	</tr>
			</table>
	 	</form>
	 </div>
	 <div id="file_up_info">
	 			<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="file_up_info">
	 		<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
				<tr> 
		       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
		    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		     	</tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("file_path_product")%>      </td>
			      <td valign="middle" > 
			    	 <input name="file_path_product" type="text" value="<%=systemConfig.getStringConfigValue("file_path_product")%>">
			     </td>
 			    </tr>
 			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("file_path_product_label")%>      </td>
			      <td valign="middle" > 
			    	 <input name="file_path_product_label" type="text" value="<%=systemConfig.getStringConfigValue("file_path_product_label")%>">
			     </td>
 			    </tr>
 			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("file_path_transport")%>      </td>
			      <td valign="middle" > 
			    	 <input name="file_path_transport" type="text" value="<%=systemConfig.getStringConfigValue("file_path_transport")%>">
			     </td>
 			    </tr>
 			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("file_path_product")%>      </td>
			      <td valign="middle" > 
			    	 <input name="file_path_product" type="text" value="<%=systemConfig.getStringConfigValue("file_path_product")%>">
			     </td>
 			    </tr>
 			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("purchase_upload_file_dir")%>      </td>
			      <td valign="middle" > 
			    	 <input name="purchase_upload_file_dir" type="text" value="<%=systemConfig.getStringConfigValue("purchase_upload_file_dir")%>">
			     </td>
 			    </tr>
 			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("file_path_financial")%>      </td>
			      <td valign="middle" > 
			    	 <input name="file_path_financial" type="text" style="width:120px;" value="<%=systemConfig.getStringConfigValue("file_path_financial")%>">
			     </td>
 			    </tr>
 			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("purchase_tag_types")%></td>
			      <td valign="middle" > 
			    	<textarea name="purchase_tag_types" cols="50" rows="7"><%=systemConfig.getStringConfigValue("purchase_tag_types")%></textarea>
			     </td>
 			    </tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("file_path_receive")%>      </td>
			      <td valign="middle" > 
			    	 <input name="file_path_receive" type="text" style="width:120px;" value="<%=systemConfig.getStringConfigValue("file_path_receive")%>">
			     </td>
 			    </tr>
 			     <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("file_path_knowledge")%>      </td>
			      <td valign="middle" > 
			    	 <input name="file_path_knowledge" type="text" style="width:120px;" value="<%=systemConfig.getStringConfigValue("file_path_knowledge")%>">
			     </td>
 			    </tr>
 			     <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("file_path_admin")%>      </td>
			      <td valign="middle" > 
			    	 <input name="file_path_admin" type="text" style="width:120px;" value="<%=systemConfig.getStringConfigValue("file_path_admin")%>">
			     </td>
 			    </tr>
			    <tr> 
			  	  <td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('file_up_info')"></td>
			 	</tr>
			</table>
	 	</form>
	 </div>
	 <div id="text_description">
	 	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="text_description">
	 		<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
				<tr> 
		       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
		    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		     	</tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("product_photo_description")%>      </td>
			      <td valign="middle" > 
			     	 <textarea name="product_photo_description" cols="50" rows="7"><%=systemConfig.getStringConfigValue("product_photo_description")%></textarea>
			     </td>
 			    </tr>
 			    <tr> 
			  	  <td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('text_description')"></td>
			 	</tr>
 			</table>
	 	</form>
	 </div>
	 <div id="b2b_order_info">
	 	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="b2b_order_info">
	 		<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="not"  class="zebraTable">
				<tr> 
		       		<th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
		    		<th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		     	</tr>
			    <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("b2b_order_certificate")%>      </td>
			      <td valign="middle" > 
			      <textarea name="b2b_order_certificate" cols="50" rows="7"><%=systemConfig.getStringConfigValue("b2b_order_certificate")%></textarea></td>
			    </tr>
			   <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("b2b_order_product_file")%>      </td>
			      <td valign="middle" > 
			      <textarea name="b2b_order_product_file" cols="50" rows="7"><%=systemConfig.getStringConfigValue("b2b_order_product_file")%></textarea></td>
			    </tr>
			     <tr> 
			      <td height="31" valign="middle" > 
			        <%=systemConfig.getStringConfigDescription("b2b_order_tag_types")%>      </td>
			      <td valign="middle" > 
			      <textarea name="b2b_order_tag_types" cols="50" rows="7"><%=systemConfig.getStringConfigValue("b2b_order_tag_types")%></textarea></td>
			    </tr>
			    <tr> 
			  	  <td colspan="2"><input name="Submit3" type="button" class="long-button-ok" value="保存修改" onClick="modify('b2b_order_info')"></td>
			 	</tr>
			</table>
	 	</form>
	 </div>
</div>
</body>
</html>
