<%@page import="com.fr.base.core.json.JSONArray"%>
<%@page import="com.cwc.app.api.zr.androidControl.AndroidPermissionUtil"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList,com.cwc.app.api.zr.androidControl.AndroidPermission" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Android Role Permission</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
 
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/accordion/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="../js/accordion/jquery-ui.min.css" />
<link rel="stylesheet" type="text/css" href="../js/accordion/jquery-ui.theme.min.css" />
  
<%
	long adgid = StringUtil.getLong(request, "adgid");
	DBRow[] permission = androidControlMgrZr.getAllPermission()	;
	AndroidPermission[]  permissions = AndroidPermissionUtil.convertDBRowToAndroidPermission(permission);
	
	List<AndroidPermission>  rootPermissions = AndroidPermissionUtil.convertArrayToTreeType(permissions);
	JSONArray permissionsJson = AndroidPermissionUtil.convertPermissionToJson(rootPermissions); 
	//System.out.println(permissionsJson.toString() + "....");
%>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '34%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>
 
 
</script>
<style type="text/css">
	*{margin: 0px;padding:0px;}
	ul{margin-left:20px; list-style-type:none;}
	ul li.data{border-bottom:1px solid silver;line-height: 35px;height: 35px;clear:both;}
	ul li label{cursor: pointer;display: block;border: 1px solid silver; padding-left:10px;padding-right:10px; float:left;}
	div.leftDiv{float: left;padding-top: 10px;}
 	h1{line-height: 35px;height: 35px;margin-top: 10px;}
	#load{background-color: white;}
</style>
<script type="text/javascript">
	
	var arrayPermissRoot = [{
			name:"Access Control",
			sub:[
			    {
				name: "Gate",
				sub:[
				         	{name:"Gate CheckIn"},
				         	{name:"Gate Check Out", sub:[]},
				         	{name:"Photo Caputrue", sub:[]}
				    ]
				},
				{
					name: "Yard",
					sub:[
					         	{name:"Patrol ",sub:[]},
					         	{name:"Shuttle", sub:[]} 
					    ]
				},
				{
					name: "Ware House",
					sub:[
					         	{name:"Dock Check In ",sub:[]},
					         	{name:"Load-Work", sub:[]} 
					    ]
				}
			]
		},
		{
			name:"Access ",
			sub:[
			     {name:"ces"}
			    ]
		}]
	
 	function addSubPermission(permission){
		var html = "" ;
		var subPermission = permission.sub ;
 		if(subPermission && subPermission.length > 0){
			html += "<h1>"+permission.name+"</h1>" ;
			html += "<ul>";
			for(var index = 0 , count = subPermission.length ; index < count ; index++ ){
				var temp = subPermission[index];
				html += addSubPermission(temp);
			}
			html += "</ul>";
		}else{
			html += "<li class='data'>"+permission.name+"<input type='checkbox' /></li>";
		}
 	 	return html ;
	}
	
   
		
		
	
	
	function initPermissionTree(){
		var html = "" ;
		arrayPermissRoot = jQuery.parseJSON( '<%= permissionsJson%>'); 
		 for(var index = 0 , count = arrayPermissRoot.length ; index < count ; index++ ){
			 var permission = arrayPermissRoot[index];
			 html += addSubPermission(permission,html);
		 }
		return html ;
	}

	jQuery(function($){
 	   // $("#control").html(initPermissionTree());
		$("#control").accordion({
			heightStyle: "content"
		});
		$("#checkbox_1").prop("indeterminate",true);
		alert($("#checkbox_1").length);
	})
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  
  <div id="control">
   
 	 
  		 <h1>12121212</h1>
  		<ul>
  			<li>
  				 
  					<h1 style="margin-top: 0px;">Gate</h1>
  					
  					<ul>
  						<li class="data"><label for="checkbox_1">Gate Check In </label> &nbsp;&nbsp;&nbsp; <div class="leftDiv"> <input id="checkbox_1" type="checkbox"  /></div></li>
  						<li class="data">Gate Check Out</li>
  						<li class="data">Photo Capture</li>
  					</ul>
   			</li>
  			<li>
   					<h1>Yard</h1>
  					<ul>
  						<li class="data">Gate Check In</li>
  						<li class="data">Gate Check Out</li>
  						<li class="data">Photo Capture</li>
  					</ul>
   			</li>
  			<li>
   					<h1>Dock</h1>
  					<ul>
  						<li class="data">Gate Check In</li>
  						<li class="data">Gate Check Out</li>
  						<li class="data">Photo Capture</li>
  					</ul>
   			</li>
  		</ul>	    
  </div>
  
</body>
</html>

