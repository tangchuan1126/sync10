<%@ page contentType="text/html;charset=utf-8"%>

<%@ include file="../../include.jsp"%> 
<%
DBRow rows[] = adminMgr.getAllAdminGroup(null);

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script language="javascript" src="../../common.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		

<script language="JavaScript" type="text/JavaScript">
<!--

function modRole(adgid) 
{
	$.prompt(
	"<div id='title'>修改角色</div><br>角色名称：<input name='proName' type='text' id='proName' style='width:200px;'><br><br>默认首页：<input name='proIndexPage' type='text' id='proIndexPage' style='width:200px;'><br><br>角色描述：<input name='proDescription' type='text' id='proDescription' style='width:200px;'>",
	{
   		  loaded:
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/admin/getDetailRoleJSON.action",
							{adgid:adgid},//{name:"test",age:20},
							function callback(data)
							{
								$("#proName").setSelectedValue(data.name);
								$("#proDescription").val(data.description);
								$("#proIndexPage").val(data.index_page);
							}
					);
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/modRole.action";
						document.mod_form.adgid.value = adgid;
						document.mod_form.name.value = f.proName;
						document.mod_form.index_page.value = f.proIndexPage;
						document.mod_form.description.value = f.proDescription;
						document.mod_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}





function checkForm(theForm)
{
	if (theForm.name.value=="")
	{
		alert("请填写角色名称");
		return(false);
	}
	
	return(true);
}

function delRole(name,adgid)
{
	if ( confirm("确认删除角色 ["+name+"]") )
	{
		document.conf_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delRole.action";
		document.conf_form.adgid.value=adgid;
		document.conf_form.submit();
	}	
}
function androidRole(adgid){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/ct_role_android.html?adgid="+adgid;
	$.artDialog.open(uri,{title: "Android Role Permission",width:'500px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}
//-->
</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style></head>

<body onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 系统管理 »   角色管理 </td>
  </tr>
</table>
  <form name="mod_form" method="post" >
    <input type="hidden" name="adgid" >
   <input name="name" type="hidden">
   <input name="description" type="hidden" >
   <input type="hidden" name="index_page" >
  </form>
<br>
<br>
<div align="center">
<fieldset style="width:98%">
  <legend>增加角色</legend>
  <table width="98%" height="38" border="0" align="center" cellpadding="3" cellspacing="0">
    <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/addRole.action" method="post" name="add_form" onSubmit="return checkForm(this)">
      <tr valign="middle"> 
        <td width="4%">&nbsp; </td>
        <td width="96%">角色名称 
          <input name="name" type="text" id="account"> 
          &nbsp;&nbsp;&nbsp; 描述 
          <input name="description" type="text" id="account2" size="40">         
          &nbsp;&nbsp;&nbsp; 
          <input name="Submit2" type="submit" class="long-button-ok"  value=" 增加角色">
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		 <span style="color:#999999"> （角色添加后，请设置该角色权限）</span>  		  </td>
      </tr>
    </form>
  </table>
</fieldset>
</div>
<br>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0"  class="zebraTable" isNeed="not">
  <form name="conf_form" method="post">
    <input type="hidden" name="adgid">
  </form>

    <tr> 
      <th width="21%"  style="vertical-align: center;text-align: center;" class="left-title"> 角色名称</th>
      <th width="29%"  style="vertical-align: center;text-align: center;" class="right-title">描述</th>
      <th width="29%"  style="vertical-align: center;text-align: center;" class="right-title">默认首面</th>
      <th width="21%"  style="vertical-align: center;text-align: center;" class="right-title">操作</th>
    </tr>

  <%
for ( int i=0; i<rows.length; i++ )
{

%>
  <tr > 
    <td height="50" valign="middle"   > 
      <%=rows[i].getString("name")%> (<%=adminMgr.getAdminByAdgid(rows[i].get("adgid",0l),null).length%>)   </td>
    <td valign="middle"   > 
      <%=rows[i].getString("description")%>
    &nbsp;</td>
    <td valign="middle"   ><%=rows[i].getString("index_page")%></td>
    <td align="center" valign="middle"  >
    <!-- add zhangrui -->
     <input name="Submit" type="button" class="short-button"  value="Android权限" onClick="androidRole(<%=rows[i].getString("adgid")%>)"/> 
    
		 <input name="Submit" type="button" class="short-short-button-mod"  value=" 修改" onClick="modRole(<%=rows[i].getString("adgid")%>)"/> 
      &nbsp;&nbsp;
	 <input name="Submit" type="button" class="short-button"  value="设置权限" onClick="location='ct_role_righs.html?adgid=<%=rows[i].getString("adgid")%>&backurl=<%=StringUtil.getCurrentURL()%>'"/> 
      &nbsp;&nbsp;
      <input name="Submit" type="button" class="short-short-button-del"  value=" 删除" onClick="delRole('<%=rows[i].getString("name")%>',<%=rows[i].getString("adgid")%>)"/>    </td>
  </tr>
  <%
}
%>
</table>
<br>
<br>
</body>
</html>
