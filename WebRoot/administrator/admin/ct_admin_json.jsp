<%@ page contentType="application/json;charset=utf-8"%>
<%@ page import="java.util.*,com.cwc.app.util.DBRowUtils,us.monoid.json.JSONArray,us.monoid.json.JSONObject" %>
<%@ include file="../../include.jsp"%>
<%
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"account");
//获取office_location办公地点信息
Tree location_tree = new Tree(ConfigBean.getStringValue("office_location"));

long proPsId = StringUtil.getLong(request, "proPsId"); //仓库
String proJsId = StringUtil.getString(request,"proJsId");//角色
long proAdgid = StringUtil.getLong(request, "proAdgid"); //部门
long filter_lid = StringUtil.getLong(request,"filter_lid");//办公地点
int	lock_state	= StringUtil.getInt(request,"lock_state");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

DBRow rows[];
if(cmd.equals("search")){
	rows = adminMgrGZY.getDetailAdminByAccount(pc,key);
}else if(cmd.equals("filter")){
 		rows = adminMgrGZY.getadvanSearch(pc,request);
}else{
	rows = adminMgrGZY.getAllAdmin(pc);
}

//文件下载
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
String ExportAdminsTitlesAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/admin/ExportAdminsTitlesAction.action";

JSONArray data = new JSONArray();
 
String lockStr;

for ( int i=0; i<rows.length; i++ ){
	if ( rows[i].get("llock",0)==0 ){
		lockStr	= "屏蔽";
	}else{
		lockStr	= "解除";
	}
	Map<String,Object> row = DBRowUtils.dbRowAsMap(rows[i]);
	row.put("lockStr",lockStr);

	DBRow detailPSC = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("ps_id",0l));
	if (detailPSC!=null){
		row.put("psTitle", detailPSC.getString("title"));
	}

	DBRow role;
	DBRow[] relation;
	relation = adminMgrGZY.getAdminGroupRelation(rows[i].get("adid",01));
	List<Map<String,Object>> roles = new ArrayList<Map<String,Object>>();
	row.put("roles",roles);
	for( int j = 0; j<relation.length; j++){
		if(relation.length!=0 && relation!=null){
			Map<String,Object> m = new HashMap<String,Object>();
			roles.add(m);
			
			role = adminMgr.getDetailAdminGroup(relation[j].get("adgid",01));
			if(role==null){
				role = new DBRow();
			}
			
			if(relation[j].getString("proJsId").equals("1")){
				m.put("roleName","员工");
			} 
			if(relation[j].getString("proJsId").equals("5")){
				m.put("roleName","副主管");
			}
			if(relation[j].getString("proJsId").equals("10")){
				m.put("roleName","主管");
			}
			DBRow allLocationFather[] = location_tree.getAllFather(relation[j].get("AreaId",0l));
			List<Map<String,Object>> locations = new ArrayList<Map<String,Object>>();
			
			for (int k=0; k<allLocationFather.length-1; k++){
				Map<String,Object> loc = new HashMap<String,Object>();
				loc.put("href","?cmd=filter&filter_lid="+allLocationFather[k].getString("id"));
				loc.put("office_name",allLocationFather[k].getString("office_name"));
				locations.add(loc);
			}
		  	DBRow location = officeLocationMgr.getDetailOfficeLocation(StringUtil.getLong(relation[j].getString("AreaId")));
		  	if(null!=location){
		  		Map<String,Object> loc = new HashMap<String,Object>();
		  		loc.put("href","?cmd=filter&filter_lid="+relation[j].getString("AreaId"));
		  		loc.put("office_name",location.getString("office_name"));
		  		locations.add(loc);
		  	}
		  	m.put("locations",locations);
		}//if
	}//for
	
	data.put(row);
}//for rows
out.println(new JSONObject().put("data",data).put("pageCtrl",new JSONObject(pc)));
%>
      	
