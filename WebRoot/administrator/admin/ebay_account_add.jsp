<%@ page contentType="text/html;charset=utf-8"%>
 

<%@ include file="../../include.jsp"%> 
<%
 	String addEbayAcountUri = ConfigBean.getStringValue("systenFolder") + "action/administrator/admin/EbayAccountAdd.action";
 	

 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Ebay Account 添加</title>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
 
 

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">
function cancel(){$.artDialog && $.artDialog.close();}
function submitMy(){
   var form = $("#myform");
   if($.trim($("#user_id").val()).length < 1){
		alert("请先输入Ebay Name.");
		return ;
   }
   if($.trim($("#password").val()).length < 1){
	alert("请先输入Ebay password.");
	return ;
}
   if($.trim($("#user_token").val()).length < 1){
       	alert("请先输入EBay Token");
		return ;
   }
   $.ajax({
		url:'<%= addEbayAcountUri%>',
		data:form.serialize(),
		dataType:'json',
		success:function(data){
			if(data && data.flag === "success"){
			    $.artDialog && $.artDialog.close();
				$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			}else{
				alert("系统错误!");
			}
		}
   })
}
</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style></head>

<body>
 
<form id="myform">
<table width="98%" border="0" cellspacing="7" cellpadding="2">
		<tr>
			<td width="23%"  style="font-weight:bold;text-align:right;">Ebay Name &nbsp;:</td>
			<td><input type="text" name="user_id" id="user_id"/></td>
		</tr>
	 	<tr>
			<td width="23%"  style="font-weight:bold;text-align:right;">Password &nbsp;:</td>
			<td><input type="text" name="password" id="password"/></td>
		</tr>
		<tr>
			<td width="23%" style="font-weight:bold;text-align:right;">Ebay Token &nbsp;: </td>
	 
			<td>
<textarea style="width:337px;height:176px;" name="user_token" id="user_token">
</textarea>
			</td>
			
		</tr>
		<tr>
			<td colspan="2" style="text-align:center;">
				<input type="button" value="确定" class="normal-green" onclick="submitMy();"/>
				<input type="button" value="取消" class="normal-white" onclick="cancel();"/>
			</td>
		</tr>
	</table>
</form>
</body>
</html>
