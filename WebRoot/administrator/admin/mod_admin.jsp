<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
Tree location_tree = new Tree(ConfigBean.getStringValue("office_location"));//获取office_location办公地点信息
long adid = StringUtil.getLong(request,"adid");//点击修改按钮时传来的admin id
String account = StringUtil.getString(request,"account");//点击修改按钮时传来的admin account
DBRow row = adminMgr.getDetailAdmin(adid);
long lid = row.get("AreaId",01);//根据传来的id查询出办公地点id
String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
%>	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改admin资料</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 在线图片预览 -->
	 <script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<script> 
$().ready(function() {
	$("#proEntrytime").click(function(){
		$(this).date_input();
	})
});

function closeWindow(){
	$.artDialog.close();
}
//弹出dialog窗口增加部门及职务
function addDeptAndRole(){
	$("#grl").hide();
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/Add_AdminGroup_role.html"; 
    $.artDialog.open(uri , {title: "修改部门/职务/办公地点",width:'550px',height:'220px', lock: true,opacity: 0.3,fixed: true});
};
//显示部门/职务/办公地点
function showValue(group,proAdgid,role,proJsId,filter_lid,location){
	var div = '<div id="divGR'+proAdgid+'" name="divGR"><input type="hidden" id="group" name="group" value="'+proAdgid+'" /><input type="hidden" id="role"  name="role" value="'+proJsId+'" /><input type="hidden" id="filter_lid"  name="filter_lid" value="'+filter_lid+'" />['+group+':'+role+':'+location+']<img alt="删除" src="../imgs/del.gif" onclick="deleteGR('+proAdgid+')" /></div>';
 	$(div).appendTo($("#groupRole"));
};
//删除所选部门及角色
function deleteGR(id){
	$("#divGR"+id).remove();
}

	function modAdmin() {
		
		var div = $("div[name='divGR']");	
					 
		var ids = "";
		var attr = "" ;
		var adgid =0;
		if(div.length>0){
			$(div).each(
				function(i){				
					 var group = $(this).find("input[name='group']").val();
					 var role = $(this).find("input[name='role']").val();
					 var filter_lid = $(this).find("input[name='filter_lid']").val();
					 var tempStr ;
					   tempStr  = ","+group+"-"+role+"-"+filter_lid;					   
					 attr+= tempStr;
					 adgid = group;
				});
			
			 if(attr.length > 1 ){attr = attr.substr(1);}
		}
		var theForm = document.mod_admin_form;
		var validateMobileCode = /^0?(13[0-9]|15[012356789]|18[0236789]|14[57])[0-9]{8}$/;
		var validateEmail = /^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.){1,4}[a-z]{2,3}$/;
		if(theForm.account.value == ""){
			alert("请填写帐号");
			return (false);
		} else if (theForm.proPsId.value == 0) {
			alert("请选择仓库");
			return (false);
		} else if (theForm.proEmployeName.value == "") {
			alert("请填写雇员姓名");
			return (false);
		}else if(theForm.proEntrytime.value == ""){
			alert("请填写入职时间");
			return (false);
		}else if(theForm.proMobilePhone.value!=""&&(!validateMobileCode.test(theForm.proMobilePhone.value))){
			alert("请输入正确的手机号");
		}else if(theForm.proMSN.value !="" &&(!checkEmail(theForm.proMSN.value))){
			alert("请正确填写MSN");
			return (false);
		} else if (theForm.proEmail.value !="" &&(!checkEmail(theForm.proEmail.value))) {
			alert("请正确填写Email");
			return (false);
		}else if(theForm.proConfirmPwd.value!=theForm.proPwd.value){
				alert("两次输入密码不一致");
				return (false);
		}else {
			parent.document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/admin/modAdminGZY.action";
			
			parent.document.mod_form.pwd.value = theForm.proPwd.value;
			parent.document.mod_form.employe_name.value = theForm.proEmployeName.value; //雇员名字
			parent.document.mod_form.adid.value = theForm.adid.value; //id
			
			parent.document.mod_form.ps_id.value = theForm.proPsId.value; //仓库
			parent.document.mod_form.email.value = theForm.proEmail.value;
			parent.document.mod_form.skype.value = theForm.proSkype.value;
			parent.document.mod_form.QQ.value = theForm.proQQ.value;
			parent.document.mod_form.mobile.value = theForm.proMobilePhone.value;

			if(adgid == 0){
			  parent.document.mod_form.adgid.value = theForm.adgid.value; //部门
			}else{
				parent.document.mod_form.adgid.value = adgid; //部门
			}
			parent.document.mod_form.arrGRL.value =attr; //部门/职务/办公地点
			
			parent.document.mod_form.msn.value = theForm.proMSN.value;
			parent.document.mod_form.account.value = theForm.account.value; //用户名
			parent.document.mod_form.entrytime.value = theForm.proEntrytime.value;//入职时间

			//图片上传
			parent.document.mod_form.filePath.value = theForm.filePath.value;
			parent.document.mod_form.file_names.value = theForm.file_names.value;
			parent.document.mod_form.sn.value = theForm.sn.value;
			parent.document.mod_form.path.value = theForm.path.value;
			
			parent.document.mod_form.areaId.value = theForm.areaId.value;
			
			
			parent.document.mod_form.submit();
		}
		
	}
	//文件上传
	function uploadFile(_target){
	 var fixWidth = "770px" , fixHeight = "530px" ;
	    
	    if(window.screen.availWidth  < 1400){
			fixWidth = "650px";
			fixHeight = "400px";
		}
	    var targetNode = $("#"+_target);
	    var fileNames = $("input[name='file_names']",targetNode).val();
	    var obj  = {
		     reg:"picture",
		     limitSize:2,
		     target:_target,
		     limitNum:1
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:fixWidth,height:fixHeight, lock: true,opacity: 0.3,fixed: true,
			 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
			 }});
	}
	//jquery file up 回调函数
	// 回显要求看上去已经是上传了的文件。所以上传是要加上前缀名的sn
	function uploadFileCallBack(fileNames,target){
	    $("p.new").remove();
	    var targetNode = $("#"+target);
		$("input[name='file_names']",targetNode).val(fileNames);
		if($.trim(fileNames).length > 0 ){
			var array = fileNames.split(",");
			var lis = "";
			for(var index = 0 ,count = array.length ; index < count ; index++ ){
				var a =  createA(array[index]) ;
				
				if(a.indexOf("href") != -1){
				    lis += a;
				}
			}
		 	//判断是不是有了
		 	if($("a",$("#over_file_td")).length > 0){
				var td = $("#over_file_td");
		
				td.append(lis);
			}else{
				$("#file_up_tr").attr("style","");
				$("#jquery_file_up").append(lis);
			}
		} 
	}
	function createA(fileName){
	    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
	    var showName = $("#sn").val()+"_" + <%=adid%> + "_" + fileName;
	    var  a = "<p class='new'><a href="+uri+">"+showName+"</a></p>";
	    return a ;
	  
	}
	//在线预览图片
	function showPictrueOnline(currentName){
	    var obj = {
			current_name : currentName ,
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_admin")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}
	
function showArea(psId){

	$.ajax({
 		type:'post',
 		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/DynamincDataAjaxAction.action',
 		dataType:'json',
 		data:"psId="+psId,
 		success:function(msg){
 			
 			var str = "<option value='0'>选择区域...</option>";
 			if(msg!=null){
 				
 				for(var i=0;i<msg.length;i++){
 					if('<%=row.getString("AreaId")%>'==msg[i].area_id){
 						str += "<option value='"+msg[i].area_id+"' selected='selected'>"+msg[i].area_name+"</option>";
 					}else{
 						str += "<option value='"+msg[i].area_id+"'>"+msg[i].area_name+"</option>";
 					}
 					
 				}
  			$("#areaId").html(str);
 			}
 		},
 		error:function(){
		showMessage("系统错误","error");
	}
  	});
}
</script>
<style type="text/css">
<!--
div.mcdropdown {
	width: 250px;
}
.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}

form
{
	padding:0px;
	margin:0px;
}

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_quote.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="">
<form name="mod_admin_form" id="mod_admin_form" method="post" action="" >
<input type="hidden" name="adgid" id="adgid" value="<%=row.get("adgid",0l) %>">
  <br>
  <table width="93%" border="0" cellpadding="0" cellspacing="0" style="padding-left:40px;padding-top:20px;">
  <tr>
    <td>
<fieldset style="border:2px #cccccc solid;padding:15px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">修改管理员[<%=account %>]</legend>
<input type="hidden"  id="adid" name="adid" value="<%=row.getString("adid") %>"/>
<input type="hidden"  id="account" name="account" value="<%=row.getString("account") %>"/>
<table width="100%" border="0" cellspacing="3" cellpadding="2">
    <tr> 
    <td height="30" align="right" class="STYLE2">密码</td>
    <td>&nbsp;</td>
    <td colspan="2">
    	<input name='proPwd' type='password' id='proPwd'  style='width:200px;'>
	</td>
    </tr>
    <tr>
    	<td height="30" align="right" class="STYLE2">确认密码</td>
    	<td>&nbsp;</td>
    	<td colspan="2">
    		<input name='proConfirmPwd' type='password' id='proConfirmPwd'  style='width:200px;'>
		</td>
    </tr>
    <tr> 
      <td width="11%" height="30" align="right" class="STYLE2">仓库</td>
      <td width="2%">&nbsp;</td>
      <td colspan="2">
	  	
      <select name="proPsId" id="proPsId" onChange="showArea(this.value);">
	  <option value="0">选择仓库...</option>
	  <%
	DBRow treeRows[] = productStorageMgrZJ.getProductStorageCatalogsByTypeShipTo(StorageTypeKey.SELF,0);
	for (int i=0; i<treeRows.length; i++){
	   
	if ( treeRows[i].get("parentid",0) > 0 ){
	 	continue;
	 }
	 if(treeRows[i].getString("id").equals(row.getString("ps_id"))){
	 %>
	 	    <option style="background:#fffffffff;" value="<%=treeRows[i].getString("id")%>" selected="selected" ><%=treeRows[i].getString("title")%></option>
	 <%
	 }else{
	  %>
	    <option style="background:#fffffffff;" value="<%=treeRows[i].getString("id")%>" ><%=treeRows[i].getString("title")%></option>
	  <%
	  }
	  }
	  %>
      </select>
	  </td>
  </tr>
  
  <tr> 
      <td width="11%" height="30" align="right" class="STYLE2">工作区域</td>
      <td width="2%">&nbsp;</td>
		<td colspan="2">
			<select name="areaId" id="areaId" >
				<option value="0">选择区域...</option>
			</select>
		</td>
	</tr>
	
		<tr>
		<td width="20%" height="30" align="right" class="STYLE2">部门/职务/办公地点</td>
		<td>&nbsp;</td>
    	<td colspan="2">
    		 <input name="btn1" type="button" class="long-button-add" onclick="addDeptAndRole()" value=" 修改">
    	</td>
	</tr>
  <tr>
  	  <td width="12%" colspan="2"></td>
      <td id="groupRole" name="groupRole" ></td>
   </tr>
    <tr id="grl">
    <td width="12%" colspan="2"></td>
      <td>
      		<%
	DBRow role;
	DBRow[] relation;
	relation = adminMgrGZY.getAdminGroupRelation(adid);
	for( int j = 0; j<relation.length; j++){
		
			role = adminMgr.getDetailAdminGroup(relation[j].get("adgid",01));
			out.println("<div>[");
			
				out.println(role.getString("name"));
		
				if(relation[j].getString("proJsId").equals("1")){
					out.println("员工");
				} 
				if(relation[j].getString("proJsId").equals("5")){
					out.println("副主管");
				}

				if(relation[j].getString("proJsId").equals("10")){
					out.println("主管");
				}
						  	
				  DBRow allLocationFather[] = location_tree.getAllFather(relation[j].get("AreaId",0l));
				  for (int k=0; k<allLocationFather.length-1; k++){
				  	out.println(allLocationFather[k].getString("office_name")+">>");
				  }
		  
			  	DBRow location = officeLocationMgr.getDetailOfficeLocation(StringUtil.getLong(relation[j].getString("AreaId")));
			  	if(null!=location){
			  	out.println(location.getString("office_name"));
			  	}
			  	
			  	out.print("]</div>");	
	}	
%>   
      </td>
  </tr>
 
   <tr>
    <td height="30" align="right" class="STYLE2">雇员姓名<br></td>
    <td>&nbsp;</td>
    <td><input name='proEmployeName' type='text' id='proEmployeName' value='<%=row.getString("employe_name")%>'  size="40" ></td>
    </tr>
   
   
    <tr>
    <td height="30" align="right" class="STYLE2">入职时间<br></td>
    <td>&nbsp;</td>
    <td><input name="proEntrytime" type="text" id="proEntrytime" value='<%=row.getString("Entrytime")%>' size="40"  readonly="readonly"></td>
    </tr>
    
  <tr> 
    <td height="30" align="right" class="STYLE2">SKYPE</td>
    <td>&nbsp;</td>
    <td><input name="proSkype" type="text" value='<%=row.getString("skype") %>' id="proSkype" size="40"  ></td>
    </tr>
	<tr>
      <td height="30" align="right"  class="STYLE2">手机号码</td>
      <td>&nbsp;</td>
      <td><input name="proMobilePhone" type="text" value='<%=row.getString("mobilePhone") %>' id="proMobilePhone" size="40"  "></td>
      </tr>
    <tr>
      <td height="30" align="right"  class="STYLE2">QQ</td>
      <td>&nbsp;</td>
      <td><input name="proQQ" type="text" value='<%=row.getString("proQQ") %>' id="proQQ" size="40"  " ></td>
      </tr>
      
      <tr>
      <td height="30" align="right"  class="STYLE2">MSN</td>
      <td>&nbsp;</td>
      <td><input name="proMSN" type="text" value='<%=row.getString("msn") %>' id="proMSN" size="40"  " ></td>
      </tr>
      
       <tr>
      <td height="30" align="right"  class="STYLE2">内部邮箱</td>
      <td>&nbsp;</td>
      <td><input name="proEmail" type="text" value='<%=row.getString("email") %>' id="proEmail" size="40"  " ></td>
      </tr>
      <tr>
				    <td height="25" align="right"  class="STYLE2" nowrap="nowrap">上传文件:</td>
					  <td>&nbsp;</td>
				       <td >
				      	<div id="file_up_span">
							<input type="hidden" id="sn" name="sn" value="A_admin"/>
							<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_admin")%>" />
							<%
								if(row.getString("file_path")!=null && row.getString("file_path").length()>0){
									out.print("");
							%>
							   <input type="button" style="color:gray;" class="long-button" disabled="disabled"  value="上传文件" />
							<%
								}
								else{
							%>
							<input type="button" class="long-button"  onclick="uploadFile('jquery_file_up');" value="上传文件" />
							<%} %>
						</div>
					   </td>
					   <td align="left" nowrap="nowrap"></td>	
					 </tr>
	   <tr id="file_up_tr" style='display:none;'>
			 		 	<td align="right" class="STYLE2">&nbsp;</td>
			 		 	<td>&nbsp;</td>
			 		 	<td>
			              	<div id="jquery_file_up">	
			              		<input type="hidden" name="file_names" id="file_names" value=""/>
			              	</div>
			 		 	</td>
	    </tr>
		<tr>
			<td height="30" align="right"  class="STYLE2">&nbsp;</td>
      		<td>&nbsp;</td>
      		<td>
      		<input type="hidden" name="filePath" id="filePath" value="<%=row.getString("file_path") %>"/>
      		<!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 		    <%if(StringUtil.isPictureFile(row.getString("file_path"))){ %>
		 		            <a href="javascript:void(0)" onclick="showPictrueOnline('<%=row.getString("file_path") %>');" ><%=row.getString("file_path") %></a>
			 		
			 		 <%}else{ %>
			 		        <a href='<%= downLoadFileAction%>?file_name=<%=row.getString("file_path") %>&folder=<%= systemConfig.getStringConfigValue("file_path_admin")%>'><%=row.getString("file_path") %></a>
      		<%} %>
      		</td>
		</tr>	  
</table>	
	</fieldset>
	</td>
  </tr>

</table>

<br>
<br>
<br>
<table width="93%" border="0" cellspacing="0" cellpadding="0"  style="height:80px;overflow:auto;">
  <tr>
    <td width="21%">&nbsp;</td>
    <td>	

	<input name="Submit22" type="button" onClick="modAdmin()" class="normal-green" value="保存修改">
	 </td>
    <td width="17%" align="left" valign="middle">
           <input name="Submit2" type="button" class="normal-white" value="取消" onClick="closeWindow();">
     </td>
  </tr>
</table>

</form>
<script type="text/javascript">

showArea($("#proPsId").val());

function closeWindow(){
	$.artDialog.close();
}
</script>
</body>
</html>
