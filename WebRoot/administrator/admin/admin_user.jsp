<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="java.util.*"%>
<%@ include file="../../include.jsp"%> 
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>系统人员</title>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/thickbox.css";
</style>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
 	<style type="text/css">
 			 
			*{margin:0px;padding:0px;font-size:12px;}
			ul.myul{list-style-type:none;}
			ul.myul li{float:left;width:150px;border:0px solid red;line-height:25px;height:25px;border:0px solid silver;}
			table.mytable {border-collapse:collapse ;width:98%;margin:0px auto;}
			table.mytable td.left{width:100px;}
			table.mytable tr td{border-bottom:1px solid silver;}
			span.span_name{display:block;float:left;margin-left:3px;cursor:pointer;}
			span.span_checkBox{display:block;float:left;margin-top:4px;}
		</style>
		 
		<%
			int singlecheck =  StringUtil.getInt(request,"single_check"); //1表示的是单选
			String userids =  StringUtil.getString(request,"user_ids"); //表示的是要回显的checkbox
			String notCheckUser =   StringUtil.getString(request,"not_check_user"); // 表示某些User是不能被选中的
			long ps_id = StringUtil.getLong(request,"ps_id");
			long proJsId = StringUtil.getLong(request,"proJsId");
			String handle_method = StringUtil.getString(request,"handle_method");
			String set_id_val_node	= StringUtil.getString(request, "set_id_val_node");
			String set_name_val_node = StringUtil.getString(request, "set_name_val_node");
			String psId_disabled =  StringUtil.getString(request, "psId_disabled");
		%>
		<%
			DBRow[] rows = transportMgrZr.getAllUserAndDept(proJsId,ps_id);
			//遍历出一个List 集合
			Map<String,List<DBRow>> mapUsers = new HashMap<String,List<DBRow>>();
			Map<String,String> mapAdgidNameAndValue = new HashMap<String,String>();
			List<String> adgidIds = new ArrayList<String>();
			if(rows != null && rows.length > 0){
				for(int index = 0 , count = rows.length ; index < count ;index++){
					DBRow temp = rows[index];
					String adgid = temp.getString("adgid");
					if(adgid.trim().length() > 0 ){
						List<DBRow> listTemp = mapUsers.get(adgid);
						if(listTemp == null || (listTemp != null && listTemp.size() < 1)){
							listTemp = new ArrayList<DBRow>();
						}
						listTemp.add(temp);
						mapUsers.put(adgid,listTemp);
						if(mapAdgidNameAndValue.get(adgid) == null){
							adgidIds.add(adgid);
							mapAdgidNameAndValue.put(adgid,temp.getString("name"));
						}
					}
				}
			}
		 	DBRow[] productCatelog =  catalogMgr.getProductStorageCatalogTree();
		%>
		<script type="text/javascript">
			var userIds = '<%= userids%>';
			var notCheckUser = '<%= notCheckUser%>';
		 	jQuery(function($){
			 	
			 	$(".adgid_check_checked").live("click",function(){
					var _this = $(this);
					var target = _this.attr("target");
					if(_this.attr("checked")){
						// checked 部门下的所有的人都选择
						$("#"+target).find("input[type='checkbox']").attr("checked","checked");
						
					}else{
						$("#"+target).find("input[type='checkbox']").attr("checked",false);
					}
				})
				$(".user_check_checked").live("click",function(){
					var _this = $(this);
					if('<%= singlecheck%>' * 1  == 1){
						otherUncheck(_this);
					}else{
					var parent = _this.parent().parent().parent();
					 var adgid = parent.attr("id").split("ul_")[1];
					 
					 checkAllLiOnUlIsChecked(adgid);
					}
				})
				//singlecheck 为1 那么表示的是单选
				showUserChecked(userIds);
				showNotUserchecked(notCheckUser);
				initPsIdAndProJsId();
			})
			function showNotUserchecked(notCheckUser){
		 		if(notCheckUser.length > 0 ){
					var array = notCheckUser.split(",");
					for(var index = 0 , count = array.length ; index < count ; index++ ){
							var node = $("#user_"+array[index]).attr("checked",false).attr("disabled","disabled");
					 		if('<%= singlecheck%>' * 1 >= 1){
								var parent = node.parent().parent().parent();
					 			var adgid = parent.attr("id").split("ul_")[1];
							 	checkAllLiOnUlIsChecked(adgid);
					 		}
					}
				}
			}
			function showUserChecked(userIds){
				if(userIds.length > 0 ){
					var array = userIds.split(",");
					for(var index = 0 , count = array.length ; index < count ; index++ ){
							var node = $("#user_"+array[index]).attr("checked",true);
							// 然后检查是不是这个部门下的所有的都选择了。选择了那么部门的应该选择上
							if('<%= singlecheck%>' * 1 >= 1){
								var parent = node.parent().parent().parent();
					 			var adgid = parent.attr("id").split("ul_")[1];
								 checkAllLiOnUlIsChecked(adgid);
							}
					}
				}
			}
			function otherUncheck(_this){
		 		$(".user_check_checked").not(_this).attr("checked",false);
			}
			//注册部门选择 然后全选
			function checkAllLiOnUlIsChecked(adgid){
			 
				var ul = $("#ul_"+adgid);
			 
				var checks = $("input[type='checkbox']",ul);
				var flag = true ;
				for(var index = 0 , count = checks.length ; index < count ; index++ ){
					if(!$(checks.get(index)).attr("checked")){
						flag = false ;
						break ;
					}
				}
				var target = $("#"+ul.attr("target"));
				target.attr("checked",flag?"checked":false);
			 
			}
			function search(){
				$("#ps_id").prop('disabled', false);
				var form =  $("#myform");
				form.submit();
			}
			function initPsIdAndProJsId(){
			 
			 	$("#proJsId option[value='<%= proJsId%>']").attr("selected",true);
			 	$("#ps_id option[value='<%= ps_id%>']").attr("selected",true) ;
			 	if('<%=psId_disabled%>'== 'true')
			 	{
			 		$("#ps_id").prop('disabled', true);
			 	}
			}
			function selecteUsers(){
				//选中的UserId
				var checks = $(".user_check_checked");
				var ids = "" ;
				var names = "";
				checks.each(function(){
					var _this = $(this);
					var liParent = _this.parent().parent();
					if(_this.attr("checked")){
						var tempIds = _this.attr("id").replace ("user_","");
						ids += ","+tempIds;
						names += ","+$(".span_name",liParent).html();
					}
				})
				ids =  ids.length > 1 ? ids.substr(1):"";
				names = names.length > 1 ? names.substr(1):"";
			 
				 $.artDialog && $.artDialog.close();
				 $.artDialog.opener.setParentUserShow  && $.artDialog.opener.setParentUserShow(ids,names,'<%= handle_method%>','<%=set_id_val_node%>','<%=set_name_val_node%>');	
			}
			function cancel(){ $.artDialog && $.artDialog.close();}
	 	/*	function changeState(_this){
				var node = $(_this);
				var checkbox = $("#"+node.attr("target"));
				if(checkbox && checkbox.length > 0 ){
					if( !checkbox.attr("checked")){
					    checkbox.attr("checked","true");
					}else{ checkbox.removeAttr("checked");}
					 
				} 
			}*/
			function changeState(_this){
				var node = $(_this);
				var checkbox = $("#"+node.attr("target"));
				if(checkbox && checkbox.length > 0 ){
					if( !checkbox.attr("checked")){
					    if('<%= singlecheck%>' * 1  == 1){
					    	$(".user_check_checked").attr("checked",false);
						}
					    checkbox.attr("checked","true");
					}else{ checkbox.removeAttr("checked");}
					 
				} 
			}
		</script>
</head>
<body>
  	 
	<div style="line-height:30px;height:30px;padding-left:20px;">  
		<form id="myform">
			<input type="hidden" value="<%=singlecheck %>" name="single_check"/>
			<input type="hidden" value="<%=userids %>" name="user_ids"/>
			<input type="hidden" value="<%=notCheckUser %>" name="not_check_user"/>
			<input type="hidden" value="<%=handle_method %>" name="handle_method" />
			<input type="hidden" value="<%=set_id_val_node %>" name="set_id_val_node" />
			<input type="hidden" value="<%=set_name_val_node %>" name="set_name_val_node" />
			<input type="hidden" value="<%=psId_disabled %>" name="psId_disabled" />
			User:<select name="proJsId" id="proJsId">
					 <option value="-1">ALL</option>
					 <option value="1">Worker</option>
					 <option value="5">Lead</option>
					 <option value="10">SuperVisor</option>
					 <option value="15">Lead+SuperVisor</option>
				</select>
			 WareHouse:<select name="ps_id" id="ps_id" style="width: 300px;">
			 			<option value="-1" >全部</option>
			 			<%
			 				if(productCatelog != null && productCatelog.length > 0){
			 					for(DBRow tempCatelog: productCatelog){
			 						%>
			 							<option value='<%=tempCatelog.get("id",0) %>'><%=tempCatelog.getString("title") %></option>
			 						<% 
			 					}
			 					
			 				}
			 			%>
			 		</select>
			 		<input type="button" value="inquiry" class="button_long_search" onclick="search();"/>	
			 		
 		</form>
		 
	</div>
	 <table class="mytable" >
	 	<%
	 		if(adgidIds != null && adgidIds.size() > 0 ){
	 			
	 			for(int index = 0 ,count = adgidIds.size() ; index < count ; index++ ){
	 				if(adgidIds.get(index).equals("100025")){
	 					continue ;
	 				}
	 				%>
	 				<tr>
	 					<td class="left">
	 						<span class="span_checkBox" style="margin-top:-1px;">
	 							<input type="checkbox" class="adgid_check_checked" target="ul_<%=adgidIds.get(index) %>" id="adgid_<%= adgidIds.get(index)%>" <%=singlecheck == 1?"disabled":"" %>/>
	 						</span>
	 						<span class="span_name"><%= mapAdgidNameAndValue.get(adgidIds.get(index)) %></span>
	 					</td>
	 					<td>
	 					<%
	 						List<DBRow> tempListDBRow = mapUsers.get(adgidIds.get(index));
	 						if(tempListDBRow != null && tempListDBRow.size() > 0 ){
	 					%>
	 							<ul class="myul" id="ul_<%=adgidIds.get(index) %>" target="adgid_<%= adgidIds.get(index)%>">
	 								<%for(DBRow row : tempListDBRow) {%>
	 									<li><span class="span_checkBox"><input type="checkbox" class="user_check_checked" id="user_<%= row.get("adid",0l)%>"/></span><span class="span_name" onclick="changeState(this);" target="user_<%= row.get("adid",0l)%>"><%= row.getString("employe_name") %></span></li>
	 								<%} %>	
	 							</ul>
	 					</td>
	 				</tr>
	 				<%
	 					}
	 			}
	 		}
	 	%>
	 		<!--  
			<tr>
				<td class="left">
					<span class="span_checkBox" style="margin-top:-1px;"><input type="checkbox" /></span>
					<span class="span_name">总经理</span>
				</td>
				<td>
					<ul class="myul">
						<li><span class="span_checkBox"><input type="checkbox" /></span><span class="span_name">张三</span></li>
						<li><span class="span_checkBox"><input type="checkbox" /></span><span class="span_name">张三</span></li>
						<li><span class="span_checkBox"><input type="checkbox" /></span><span class="span_name">张三</span></li>
					</ul>
				</td>
			</tr>
		 -->
		 
			 
		 
		 </table>
		 <div style="text-align:center;margin-top:3px;" >	
		 
		 <input type="button" class="normal-green-long" value="Confirm" onclick="selecteUsers();"/>
		 		&nbsp;	&nbsp;
		 	<input type="button" class="normal-green-long" value="Cancel" onclick="cancel();">
		 	
		  
		 </div>
</body>
</html>