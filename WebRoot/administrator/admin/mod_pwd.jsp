<%@ page contentType="text/html;charset=utf-8"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Modify Password</title>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script language="JavaScript" type="text/JavaScript">
<!--
function checkForm()
{
	if ( document.mod_pwd_form.oldpwd.value=="" )
	{
		alert("Please input the old password");
		
		return(false);
	}
	if ( document.mod_pwd_form.pwd.value=="" )
	{
		alert("Please input the new password");
		return(false);
	}
	if ( document.mod_pwd_form.pwd2.value=="" )
	{
		alert("Please input the confirm password");
		return(false);
	}
	if ( document.mod_pwd_form.pwd2.value!=document.mod_pwd_form.pwd.value )
	{
		alert("New password not match");
		return(false);
	}
	
	return(true);
}
//-->
</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style></head>

<body>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; Change Password</td>
  </tr>
</table>
<br>
<br>
<table width="98%" border="0" cellpadding="3" cellspacing="0"  class="thetable">
<thead>
  
  <tr>
  </thead>
    <td bgcolor="#FFFFFF"><table width="100%" border="0" align="center" cellpadding="4" cellspacing="1">
        <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/modAdminPwd.action" method="post" name="mod_pwd_form" onSubmit="return checkForm();">
          <tr> 
            <td width="162"  align="right" >Old Password:</td>
            <td width="1227"> <input name="oldpwd" type="password" id="oldpwd3"></td>
          </tr>
          <tr> 
            <td align="right" valign="middle" style="padding-right:4px;">New Password:</td>
            <td> <input name="pwd" type="password" id="pwd"></td>
          </tr>
          <tr> 
            <td align="right" valign="middle" style="padding-right:4px;">Confirm Password:</td>
            <td> <input name="pwd2" type="password" id="pwd2"></td>
          </tr>
          <tr> 
            <td align="right" valign="middle">&nbsp;</td>
            <td><br> <input name="Submit" type="submit" class="long-button-ok" value="Submit"></td>
          </tr>
        </form>
      </table></td>
  </tr>
</table>
</body>
</html>
