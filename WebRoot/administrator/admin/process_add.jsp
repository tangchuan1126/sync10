<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.YesOrNotKey"%>
<jsp:useBean id="produrePeriodStatusKey" class="com.cwc.app.key.ProdurePeriodStatusKey"></jsp:useBean>
<jsp:useBean id="noticeTypeKey" class="com.cwc.app.key.NoticeTypeKey"></jsp:useBean>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	int order_type	= StringUtil.getInt(request, "order_type");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>流程添加</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
	div.cssDivschedule{
			width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
			background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
			background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
	.detailNoticeCss
	{
	text-align: center;border: 1 solid silver;width: 20%;
	}
</style>
<%
List produrePeriodStatusList = produrePeriodStatusKey.getProdureNoticeStatusKeys();
 %>

<script type="text/javascript">
$(function(){
	$(".cssDivschedule").css("display","none");
	
});
function submitData(){
	if(volidate()){
	$(".cssDivschedule").css("display","block");
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/admin/processAddAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag_check_produres == "0" && data.flag_check_produres_activity == "0" && data.flag_check_produres_activity == "0"){
					showMessage("添加成功","success");
					setTimeout("windowClose()", 1000);
				}else{
					var error = data.error;
					$(".cssDivschedule").css("display","none");
					showMessage("添加失败，验证未通过<br/>"+error,"alert");
				}
			},
			error:function(){
				$(".cssDivschedule").css("display","none");
				showMessage("系统错误","error");
			}
		})	
	}
};
function volidate(){
	if('' == $("#process_name").val())
	{
		alert("请填写流程名");
		return false;
	}
	else if('' == $("#process_key").val() || !/^[1-9]+[0-9]*$/.test($("#process_key").val()))
	{
		alert("流程key值只能为数字");
		return false;
	}
	else if('' == $("#process_coloum").val())
	{
		alert("请填写流程在所属单据中的字段名");
		return false;
	}
	else if('' == $("#process_coloum_page").val())
	{
		alert("请填写流程在页面中使用的字段名");
		return false;
	}
	else if('' == $("#process_person").val())
	{
		alert("请填写流程负责人");
		return false;
	}
	else if('' == $("#process_person").val())
	{
		alert("请填写流程负责人");
		return false;
	}
	else if('' == $("#process_period").val() || !/^[1-9]+[0-9]*$/.test($("#process_period").val()))
	{
		alert("流程周期只能为数字");
		return false;
	}
	else
	{
		var activirys = $("#processDetails tr");
		for(var i = 1; i < activirys.length; i ++)
		{
			var activity_name = $("input[name=activity_name]", $(activirys[i])).val();
			var activity_val = $("input[name=activity_val]", $(activirys[i])).val();
			if('' == activity_name)
			{
				alert("阶段名不能为空");
				return false;
			}
			if('' == activity_val)
			{
				alert("阶段值不能为空");
				return false;
			}
		}
		var notices = $("#processNotices tr");
		for(var i = 1; i < notices.length; i ++)
		{
			var notice_name = $("input[name=notice_name]", $(notices[i])).val();
			var notice_coloum_name = $("input[name=notice_coloum_name]", $(notices[i])).val();
			if('' == notice_name)
			{
				alert("通知名不能为空");
				return false;
			}
			if('' == notice_coloum_name)
			{
				alert("通知字段名不能为空");
				return false;
			}
		}

	}
	return true;
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function addProcessDetail()
{
	$("#processDetailsSet").css("display", "block");
	var html = 
				'<tr>'
			+		'<td class="detailNoticeCss"><input name="activity_name"/></td>'
			+		'<td class="detailNoticeCss"><input name="activity_val"/></td>'
			+		'<td class="detailNoticeCss">'
			+			'<select name="val_status">'
							<%
								for(int i = 0; i < produrePeriodStatusList.size(); i ++)
								{
							%>
			+					'<option value="<%=produrePeriodStatusList.get(i) %>"><%=produrePeriodStatusKey.getProdureNoticeStatusKeyName(String.valueOf(produrePeriodStatusList.get(i))) %></option>'
							<%		
								}
							%>
			+			'</select>'
			+		'</td>'
			+		'<td class="detailNoticeCss">'
			+		showYesOrNotSelect('isThisActivityCheckedDefault')
			+		'</td>'
			+		'<td>'
			+			'<input type="button" class="short-short-button" value="删除" onclick="delThis(this, \'processDetails\')"/>'
			+		'</td>'
			+	'</tr>';
	$("#processDetails tr:last").after($(html));
}
function addProcessNotice()
{
	$("#processNoticesSet").css("display", "block");
	var html = 
				'<tr>'
			+		'<td class="detailNoticeCss">'
			+			'<input name="notice_name"/>'
			+		'</td>'
			+		'<td class="detailNoticeCss">'
			+			'<input name="notice_coloum_name"/>'
			+		'</td>'
			+		'<td class="detailNoticeCss">'
			+		showYesOrNotSelectYess('notice_default_selected')
			+		'</td>'
			+		'<td class="detailNoticeCss">'
			+			'<select name="notice_type">'
							<%
								List noticeTypeKeys = noticeTypeKey.getNoticeTypeKeys();
								for(int i = 0; i < noticeTypeKeys.size(); i ++)
								{
							%>
			+					'<option value="<%=noticeTypeKeys.get(i) %>"><%=noticeTypeKey.getNoticeTypeKeyName(String.valueOf(noticeTypeKeys.get(i))) %></option>'
							<%		
								}
							%>
			+			'</select>'
			+		'</td>'
			+		'<td class="detailNoticeCss">'
			+		showYesOrNotSelectYess('notice_is_display')
			+		'</td>'
			+		'<td>'
			+			'<input type="button" class="short-short-button" value="删除" onclick="delThis(this, \'processNotices\')"/>'
			+		'</td>'
			+	'</tr>';
	$("#processNotices tr:last").after($(html));
}

function delThis(delBut, tableName)
{
	$(delBut).parent().parent().remove();
	if(1 == $("#"+tableName+" tr").length)
	{
		$("#"+tableName+"Set").css("display", "none");
	}
}
function showYesOrNotSelect(selectName)
{
	var isNotSelected = "";
	var html = '';
	html += '<select name="'+selectName+'" onchange="activityRelateSelectChange(this)">';
	<%
	YesOrNotKey yesOrNotKey = new YesOrNotKey();
	ArrayList<String> yesOrNotKeys = yesOrNotKey.getYesOrNotKeys();
	for(int i = 0; i < yesOrNotKeys.size(); i ++)
	{
		if(String.valueOf(YesOrNotKey.NO).equals((String)yesOrNotKeys.get(i)))
		{
	%>
			isNotSelected = "selected='selected'";
	<%	}
	%>
	html += '<option value="<%=yesOrNotKeys.get(i) %>" '+isNotSelected+'><%=yesOrNotKey.getYesOrNotKeyName(yesOrNotKeys.get(i)) %></option>'
	<%		
	}
	%>
	html += '</select>';
	return html;
}

function showYesOrNotSelectYess(selectName)
{
	var isNotSelected = "";
	var html = '';
	html += '<select name="'+selectName+'">';
	<%
	for(int i = 0; i < yesOrNotKeys.size(); i ++)
	{
		if(String.valueOf(YesOrNotKey.NO).equals((String)yesOrNotKeys.get(i)))
		{
	%>
			isNotSelected = "selected='selected'";
	<%	}
	%>
	html += '<option value="<%=yesOrNotKeys.get(i) %>" '+isNotSelected+'><%=yesOrNotKey.getYesOrNotKeyName(yesOrNotKeys.get(i)) %></option>'
	<%		
	}
	%>
	html += '</select>';
	return html;
}
function activityRelateSelectChange(_this)
{
	$("select[name="+$(_this).attr("name")+"]").val('<%=YesOrNotKey.NO%>');
	$(_this).val(<%=YesOrNotKey.YES%>);
}
</script>

</head>
<body>
<div class="cssDivschedule" style="">
	
</div>
<form action="" id="subForm" method="post">
<input type="hidden" name="order_type" value="<%=order_type %>"/>
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			创建流程
			 <input type="button" value="添加子流程" class="long-button-add" onclick="addProcessDetail()"/>
			 <input type="button" value="添加通知" class="long-button-add" onclick="addProcessNotice()"/>
		</legend>	
		
			<table style="width:100%;margin-top: 10px;">
				<tr>
					<td style="text-align: right;"><label title="注:流程名指页面、发送短信或邮件等中文此流程的描述(任何)" style="color: blue;">流程名:</label></td>
					<td style="text-align: left;">
						<input name="process_name" id="process_name"/>
					</td>
					<td style="text-align: right;"><label title="注:流程key值指此流程在某单据或在整个系统中不重复的key值(数字)" style="color: blue;">流程key值:</label></td>
					<td style="text-align: left;">
						<input name="process_key" id="process_key"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;"><label title="注:在单据中的字段名指此流程在相应单据中存储此流程阶段的字段名，单据内不能重名(英文)" style="color: blue;">在单据中的字段名:</label></td>
					<td style="text-align: left;">
						<input name="process_coloum" id="process_coloum"/>
					</td>
					<td style="text-align: right;"><label title="注:page使用此流程属性名指在页面中遍历等使用时属性名，页面内不能重名(英文)" style="color: blue;">page使用此流程属性名:</label></td>
					<td style="text-align: left;">
						<input name="process_coloum_page" id="process_coloum_page"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;"><label title="'负责人'三个字是否需要显示,一般有需要显示子流程时，需要显示此三个字" style="color: blue;">"负责人"是否显示:</label></td>
					<td style="text-align: left;">
						<input type="radio" name="process_person_is_display" value='<%=YesOrNotKey.YES %>' checked="checked"/>显示
						<input type="radio" name="process_person_is_display" value='<%=YesOrNotKey.NO %>'/>不显示
					</td>
					<td style="text-align: right;"><label title="注:选择负责人的文本框前面的提示" style="color: blue;">选负责人前的提示:</label></td>
					<td style="text-align: left;">
						<input name="process_person" id="process_person"/>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;"><label title="当创建或者更新单据，选择此流程为需要时，此流程是否立即开始" style="color: blue;">流程创建时是否开始:</label></td>
					<td style="text-align: left;">
						<input type="radio" name="process_start_time_is_create" value='<%=YesOrNotKey.YES %>' checked="checked"/>是
						<input type="radio" name="process_start_time_is_create" value='<%=YesOrNotKey.NO %>'/>否
					</td>
					<td style="text-align: right;"><label title="规定此流程的阶段在某段时间内应该被完成" style="color: blue;">流程周期:</label></td>
					<td style="text-align: left;">
						<input name="process_period" id="process_period" />
					</td>
				</tr>
				<tr>
					<td style="text-align: right;"><label title="注:流程提示指在页面中描述此流程填写的提示，与流程不在同行显示" style="color: blue;">流程提示是否显示:</label></td>
					<td style="text-align: left;">
						<input type="radio" name="process_note_is_display" value='<%=YesOrNotKey.YES %>' checked="checked"/>显示
						<input type="radio" name="process_note_is_display" value='<%=YesOrNotKey.NO %>'/>不显示
					</td>
					<td style="text-align: right;"><label title="此流程的子流程,如需要、不需要等是否显示" style="color: blue;">子流程是否显示:</label></td>
					<td style="text-align: left;">
						<input type="radio" name="activity_is_need_display" value='<%=YesOrNotKey.YES %>' checked="checked"/>显示
						<input type="radio" name="activity_is_need_display" value='<%=YesOrNotKey.NO %>'/>不显示
					</td>
				</tr>
				<tr>
					<td style="text-align: right;"><label title="注:流程提示指在页面中描述此流程填写的提示" style="color: blue;">流程提示:</label></td>
					<td style="text-align: left;" colspan="3">
						<input name="process_note" id="process_note" style="width: 545px;"/>
					</td>
				</tr>
			</table>
	</fieldset>
	<fieldset id="processDetailsSet" style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;margin-top: 10px;display: none;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			子流程
		</legend>	
		<table id="processDetails" style="width: 100%;border-collapse: collapse;">
			<tr style="height: 25px;">
				<td class="detailNoticeCss">子流程名</td>
				<td class="detailNoticeCss">子流程值</td>
				<td class="detailNoticeCss">子流程值状态</td>
				<td class="detailNoticeCss">是否默认选中</td>
				<td class="detailNoticeCss">&nbsp;</td>
				<td class="detailNoticeCss">&nbsp;</td>
			</tr>
		</table>
	</fieldset>
	<fieldset id="processNoticesSet" style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;margin-top: 10px;display: none;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			流程通知
		</legend>	
		<table id="processNotices" style="width: 100%;">
			<tr style="height: 25px;">
				<td class="detailNoticeCss">通知名</td>
				<td class="detailNoticeCss">通知字段名</td>
				<td class="detailNoticeCss">通知默认是否选中</td>
				<td class="detailNoticeCss">通知类型</td>
				<td class="detailNoticeCss">通知是否显示</td>
			</tr>
		</table>
	</fieldset>
</form>
	<table align="right">
		<tr align="right">
			<td colspan="2"><input type="button" class="normal-green" value="提交" onclick="submitData();"/></td>
		</tr>
	</table>
</body>
<script type="text/javascript">
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
 
	 var  _self = $("body"),
	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	 
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,7000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,3000);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
 }
}
</script>
</html>