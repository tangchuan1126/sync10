<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%
HashMap operateTypeHM = new HashMap();
operateTypeHM.put("1","<font color=blue>访问页面</font>");
operateTypeHM.put("2","<font color=red>提交事件</font>");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

long adgid = StringUtil.getLong(request,"adgid");
long adid = StringUtil.getLong(request,"adid");
int operate_type = StringUtil.getInt(request,"operate_type");

String input_st_date = StringUtil.getString(request,"input_st_date");
String input_en_date = StringUtil.getString(request,"input_en_date");

TDate tDate = new TDate();

if (input_st_date.equals("")&&input_en_date.equals(""))
{
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}

long st_datemark = tDate.getDateTimeFullTime(input_st_date+" 0:00:00");
long en_datemark = tDate.getDateTimeFullTime(input_en_date+" 23:59:59");

DBRow sysLogs[] = null;

if (adid>0)
{
	sysLogs = systemConfig.getSysLogsByAdidDate( st_datemark, en_datemark, adid, operate_type, pc);
}
else if (adgid>0)
{
	sysLogs = systemConfig.getSysLogsByAdgidDate( st_datemark, en_datemark, adgid, operate_type, pc);
}
else
{
	sysLogs = systemConfig.getAllSysLogsByDate(st_datemark,en_datemark,operate_type, pc);
}

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.search_form.input_st_date.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.search_form.input_en_date.value = date;
}

function onMOBg(row,cl,index)
{
	row.style.background=cl;
}

function filter()
{
	document.search_form.cmd.value="filter";
	document.search_form.submit();
}

function getDetailSysLog(sl_id)
{
	tb_show('详细日志','detail_sys_log.html?sl_id='+sl_id+'&height=550&width=900',false);
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 系统管理 »   系统日志</td>
  </tr>
</table>
<br>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
   
      <td width="69%" height="31" bgcolor="#E3F2E3"  style="padding-left:10px;">
	   <form action="sys_logs.html" method="post" name="search_form">
		<input type="hidden" name="cmd">

			开始 
              <input name="input_st_date" type="text" id="input_st_date" size="9" value="<%=input_st_date%>" readonly> 
              <script language="JavaScript" type="text/javascript" >
    <!--
    	stfooCalendar = new dynCalendar('stfooCalendar', 'stcalendarCallback', '../js/images/');
    //-->
    </script> 
              &nbsp; &nbsp; &nbsp;结束 
              <input name="input_en_date" type="text" id="input_en_date" size="9" value="<%=input_en_date%>"  readonly>
			   <script language="JavaScript" type="text/javascript">
    <!--
    	enfooCalendar = new dynCalendar('enfooCalendar', 'encalendarCallback', '../js/images/');
    //-->
	</script>
               &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	    <select name="adgid" id="adgid">
		<option value="0">全部角色</option>
          <%
DBRow roles[] = adminMgr.getAllAdminGroup(null);

for ( int i=0; i<roles.length; i++ )
{
%>
          <option value="<%=roles[i].get("adgid",0l)%>" <%=roles[i].get("adgid",0l)==adgid?"selected":""%>> 

          <%=roles[i].getString("name")%>          </option>
          <%
}
%>
        </select> 
              &nbsp; &nbsp; &nbsp;
			  
              <select name="adid" id="adid">
			  <option value="0">全部管理员</option>
          <%
DBRow admins[] = adminMgr.getAllAdmin(null);

for ( int i=0; i<admins.length; i++ )
{
%>
          <option value="<%=admins[i].get("adid",0l)%>" <%=admins[i].get("adid",0l)==adid?"selected":""%>> 

          <%=admins[i].getString("account")%>          </option>
          <%
}
%>
         </select>
              &nbsp; &nbsp; &nbsp;
              <select name="operate_type" id="operate_type">
                <option value="0">所有行为</option>
				 <option value="1" <%=operate_type==1?"selected":""%>>访问页面</option>
				  <option value="2" <%=operate_type==2?"selected":""%>>提交事件</option>
              </select>
              &nbsp; &nbsp; &nbsp;
         <label>
	          <input name="Submit" type="button" class="button_long_refresh" onClick="filter()" value=" 过滤 ">
         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</label>
    </form>	  </td>
  </tr>

</table>



<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form name="listForm" method="post">


    <tr> 
        <th style="vertical-align: center;text-align: center;" class="left-title">类型/执行操作/执行人/角色/时间</th>
        <th style="vertical-align: center;text-align: center;" class="right-title">操作参数</th>
        <th width="16%" style="vertical-align: center;text-align: left;" class="right-title">来路</th>
        <th width="14%" style="vertical-align: center;text-align: center;" class="right-title">IP</th>
        <th width="17%" style="vertical-align: center;text-align: center;" class="right-title">浏览器</th>
        <th width="13%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
        </tr>

    <%


for ( int i=0; i<sysLogs.length; i++ )
{

%>
    <tr > 
      <td   width="23%" height="60" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
	  <%
	  if (sysLogs[i].getString("account").equals(""))
	  {
	  	out.println("<span style='color:red;font-weight:bold'>无登录</span>");
	  }
	  else
	  {
	  	out.println("<span style='font-weight:bold'>"+sysLogs[i].getString("account")+"</span> <span style='color:#999999'>["+sysLogs[i].getString("name")+"]</span>");
	  }
	  %> 
 [<%=sysLogs[i].getString("post_date")%>] 
	  <br>
[<%=operateTypeHM.get(sysLogs[i].getString("operate_type"))%>] 
	   <br>
      <%=StringUtil.cutString(sysLogs[i].getString("operation"), 200, "......")%></td>
      <td   width="17%" valign="middle" style='word-break:break-all;' ><%=StringUtil.cutString(sysLogs[i].getString("operate_para"), 200, "......")%>&nbsp;</td>
      <td   align="left" valign="middle" ><span style="word-break:break-all;"><%=StringUtil.cutString(sysLogs[i].getString("ref"), 200, "......")%></span>&nbsp;</td>
      <td   align="center" valign="middle" style='word-break:break-all;'><%=sysLogs[i].getString("ip")%></td>
      <td   align="center" valign="middle" style='word-break:break-all;'><%=sysLogs[i].getString("visitor")%></td>
      <td   align="center" valign="middle" ><label>
        <input name="Submit2" type="button" class="short-button" value="查看详细" onClick="getDetailSysLog(<%=sysLogs[i].getString("sl_id")%>)">
      </label></td>
    </tr>
    <%
}
%>
</form>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p">
		  <input type="hidden" name="input_st_date" value="<%=input_st_date%>">
		<input type="hidden" name="input_en_date" value="<%=input_en_date%>">
		<input type="hidden" name="adgid" value="<%=adgid%>">
		<input type="hidden" name="adid" value="<%=adid%>">
		<input type="hidden" name="operate_type" value="<%=operate_type%>">
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
</body>
</html>
