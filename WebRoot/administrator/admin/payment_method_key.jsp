<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.fr.base.Inter"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.PaymentMethodKey,java.util.ArrayList,com.cwc.app.key.BillTypeKey,com.cwc.app.key.InvoiceStateKey,com.cwc.app.key.BillStateKey" %>

 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>收款账号配置</title>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">

div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:500px;height:30px;background:white;margin-top:200px;line-height:30px;font-size:14px;}
   .rows td{line-height:30px;height:30px;}
</style>
<%
	String addPayMentMethodKey = ConfigBean.getStringValue("systenFolder") + "administrator/payment_method_key/add_payment_method_key.html";
	String deletePayMentMethod =   ConfigBean.getStringValue("systenFolder") + "action/administrator/payMentMethod/DeletePayMentMethodAction.action";


	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(20);
	PaymentMethodKey methodKey = new PaymentMethodKey();
 	DBRow[] rows = null ;
 	String cmd = StringUtil.getString(request,"cmd");
 	int isTip = StringUtil.getInt(request,"is_tip");
 	int key = StringUtil.getInt(request,"key");
 	if(cmd.equals("query")){
 		rows = payMentMethodMgrZr.getPayMentMethodByKey(pc,key,isTip);
 	}else{
 		rows = payMentMethodMgrZr.getAllPayMentMethod(pc);
 	}
 	
%>
<script type="text/javascript">
	jQuery(function($){
	 	$("#tabs").tabs();
	 	if('<%= cmd%>' === "query"){
			// 回显数据
			$("#pageCmd").val("query");
			$("#pageIsTip").val('<%= isTip%>');
			$("#pageKey").val('<%= key%>');
			$("#key option[value='"+'<%= key%>'+"']").attr("selected",true);
			$("#is_tip option[value='"+'<%= isTip%>'+"']").attr("selected",true);
		}
	 		
	})
	function go(number){
		$("#pageCount").val(number);
		$("#pageForm").submit();
	}
	function query(){
		var form = $("#queryForm");
		form.submit();
	}
	function add(){
		var uri = '<%=addPayMentMethodKey%>'; 
	 
		$.artDialog.open(uri , {title: '新增收款账号',width:'400',height:'200', lock: true,opacity: 0.3,fixed: true});
	}
	function deletePayMentMethod(keyId){

		$.artDialog.confirm('你确认删除操作？', function(){
	 
		     $.ajax({
				 url:'<%= deletePayMentMethod%>' + "?id="+keyId,
				 dataType:'text',
				 success:function(data){
				 	if(data === "success"){
		    		  $.artDialog.tips('操作成功');
		    		  window.location.reload();
				 	}else{
				 		$.artDialog.tips('系统错误,请稍后再试');
					}
				 },
				 error:function(){
					  $.artDialog.tips('系统错误,请稍后再试');
				 }
			  })
		}, function(){
		    $.artDialog.tips('你取消了操作');
		});
	}
	function updatePayMentMethod(id){
		// 修改和添加用同一个页面
		var uri = '<%=addPayMentMethodKey%>' + "?id="+id; 
		$.artDialog.open(uri , {title: '修改收款账号[ '+ id +" ]",width:'400',height:'200', lock: true,opacity: 0.3,fixed: true});
	}
	function refreshWindow(){
		window.location.reload();
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">

      <div class="cssDivschedule">
	 		<div class="innerDiv">
	 			请稍等.......
	 		</div>
	  </div>
	    <div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
				 <ul>
					 <li><a href="#tool">常用工具</a></li>
			 	 
				 </ul>
				 <div id="tool" style="height:30px;overflow:hidden;">
				 	<form action="" id="queryForm">
				 	<input type="hidden" name="cmd" value="query"/>
					 收款类型:
						 <select id="key" name="key">
						 	 <option value="-1">全部</option>
						 	<%
						 		ArrayList<String> keys = methodKey.getStatus();
						 		if(keys != null && keys.size() > 0){
						 			for(String keyType : keys){
						 			%>
						 			<option value='<%= keyType %>'><%= methodKey.getStatusById(Integer.parseInt(keyType))  %></option>
						 			<% 	
						 			}
						 		}
						 	%> 
						 </select>
						是否提示账号:
						<select id="is_tip" name="is_tip">
							<option value="-1">全部</option>
							<option value="0">否</option>
							<option value="1">是</option>
						</select>

					 <input type="button" value="查询" class="button_long_search" onclick="query();"/>
					 <input type="button" value="添加" class="long-button-add" onclick="add();"/>
			 	  </form>
				 </div>
		 </div>
		 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="not"  class="zebraTable" style="margin-left:3px;margin-top:5px;">
			
			  <tr> 
			  	<th width="5%" style="vertical-align: center;text-align: center;" class="right-title">Id</th>
			  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">收款类型</th>
			  	<th width="25%" style="vertical-align: center;text-align: center;" class="right-title">收款人</th>
		        <th width="25%" style="vertical-align: center;text-align: center;" class="right-title">收款账号</th>
		        <th width="15%" style="vertical-align: center;text-align: center;" class="right-title">是否提示账号</th>
		    	<th width="5%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
		      </tr>
		      <tbody class="rows">
		      <%
		      	if(rows != null && rows.length > 0){
		   			for(DBRow row : rows){
		   			%>
		   				<tr>
		   					<td><%= row.get("id",0) %></td>
		   					<td><%= methodKey.getStatusById(row.get("key_type",0))  %></td>
		   					<td><%= row.getString("account_name") %></td>
		   					<td><%= row.getString("account") %></td>
		   					<td style="text-align:center;"><%= (row.get("is_tip",0)== 0 ? "否" : "是") %></td>
		   					<td style="padding-top:3px;padding-bottom:3px;">
		   						<input class="short-button" type="button" onclick="deletePayMentMethod('<%= row.get("id",0) %>');" value="删除" />
		   						<input class="short-button" type="button" onclick="updatePayMentMethod('<%= row.get("id",0) %>');" value="修改" />
		   						
		   					</td>
		   				</tr>
		   			<%	
		   			}
		      	}else{
		      %>
		      	<tr>
		      	 	<td colspan="6" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
		      	</tr>
		      <% 		
		      	}
		      %>
		      
		      
		      </tbody>
		   </table>
		   <form action="" id="pageForm">
		   		<input type="hidden" name="cmd" id="pageCmd" />
				<input type="hidden" name="p" id="pageCount"/>
				<input type="hidden" name="is_tip" id="pageIsTip" />
				<input type="hidden" name="key" id="pageKey" />
			</form>
 			<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
				  <tr>
				    <td height="28" align="right" valign="middle"><%
						int pre = pc.getPageNo() - 1;
						int next = pc.getPageNo() + 1;
						out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
						out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
						out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
						out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
						out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
						%>
						      跳转到
						      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
						      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
					 </td>
				 </tr>
			</table>
</body>
</html>
