<%@ page contentType="text/html;charset=utf-8"%>
<%@ include file="../../include.jsp"%> 
<%
String folder[] = systemConfig.getAllDBBK();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>

<script language="javascript" src="../../common.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script language="JavaScript" type="text/JavaScript">
<!--

function importDB(folder,i)
{
	theForm = document.getElementById("dbform" + i);
	

	if( confirm("确认恢复数据库到 "+folder+" 版本？"))
	{
		document.getElementById("handle_status").className="ErrorMsg";
		document.getElementById("handle_status").innerHTML="正在恢复数据库，请勿刷新浏览器……";
		theForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/importDB.action";
		theForm.folder.value=folder;
		theForm.submit();
	}
}

function delDataBaseBK(folder,i)
{
	var theForm = document.getElementById("dbform" + i);

	if( confirm("确认删除所有备份？"))
	{
		theForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delDataBaseBK.action";
		theForm.folder.value=folder;
		theForm.submit();
	}
}

function backupDBNow()
{
	theForm = document.add_form;

	if( confirm("该操作比较消耗系统资源，请勿在访问高峰期操作。\n确认立即备份系统？"))
	{
		document.getElementById("handle_status").className="ErrorMsg";
		document.getElementById("handle_status").innerHTML="正在备份数据库，请勿刷新浏览器……";
		theForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/backupDBNow.action";
		theForm.submit();
	}
}

function dbDBNow(v)
{
	theForm = document.add_form;

	if( confirm("确认下载？"))
	{
		theForm.action="export_db.html?file="+v;
		theForm.submit();
	}
}
//-->
</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

form
{
	padding:0px;
	margin:0px;
}
-->
</style></head>
<body>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" >
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 系统管理 »   帐号管理 </td>
  </tr>
</table>
<br>
<br>
<div align="center">
<fieldset style="width:98%">
  <legend>数据库自动备份设置</legend>
  <table width="98%" height="40" border="0" align="center" cellpadding="3" cellspacing="0">
    <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="add_form" >
      <tr valign="middle"> 
        <td align="left">自动备份 : 
          <input name="backup_db_flag" type="radio" value="1" <%=systemConfig.getIntConfigValue("backup_db_flag")==1?"checked":""%>>
          是 
          <input type="radio" name="backup_db_flag" value="0" <%=systemConfig.getIntConfigValue("backup_db_flag")==0?"checked":""%>>
          否&nbsp; <input name="Submit6" type="submit" class="short-button" value="保存设置">
          &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;备份 
          <input name="backup_db_count" type="text" id="backup_db_count" style="width:30px;" value="<%=systemConfig.getIntConfigValue("backup_db_count")%>">
          天 
          <input name="Submit2" type="submit" class="short-button" value="保存设置">
          &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
          <input name="Submit5" type="button" class="long-button" id="backup_b" onClick="backupDBNow()"  value="立即备份">
          <span id="handle_status"></span></td>
      </tr>
    </form>
  </table>
</fieldset>
</div>
<br>

<br>
<%

for (int i=0; i<folder.length; i++)
{
%>
<form name="dbform<%=i%>" method="post">
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0"  class="thetable">
  
    <input type="hidden" name="folder">
  <thead>
    <tr> 
      <td width="60%" valign="middle" class="left-title">&nbsp;<strong>备份日期</strong>: 
		<font face="Arial, Helvetica, sans-serif">
        <%=folder[i].substring(0,4)%>-<%=folder[i].substring(4,6)%>-<%=folder[i].substring(6,8)%>
		<%=folder[i].substring(8,10)%>:<%=folder[i].substring(10,12)%>:<%=folder[i].substring(12,14)%>        </font> </td>
      <td width="40%" align="right" valign="middle" class="right-title">&nbsp; 
        <input name="Submit3" type="button" class="long-button" onClick="delDataBaseBK('<%=folder[i]%>',<%=i%>)" value="删除备份"></td>
    </tr>
  </thead>
  <tr> 
    <td height="41" colspan="2" valign="middle" style="line-height:18px;"> 
      <%
DBRow file[] = systemConfig.getAllDBBKTableSql(folder[i]);
float filesize;
String bkName;
for ( int j=0; j<file.length; j++ )
{
	filesize = StringUtil.getFloat(file[j].getString("size"))/1024;
	
	if (file[j].getString("name").indexOf("database")>=0)
	{
		bkName = "数据库";
	}
	else
	{
		bkName = "商城图片";
	}
%>
     <DIV style="FLOAT: left;width:20%;">
				<DIV style="padding-top:5px;">
        <input name="tablename" type="hidden" value="<%=file[j].getString("name")%>">
		
<fieldset style="width:90%">
  <legend><%=bkName%></legend>
		<table width="246" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td width="52%" align="center" valign="middle"><img src="../imgs/zip_logo.jpg" width="42" height="34"></td>
    <td width="48%" rowspan="2" align="center" valign="middle"><label>
	<%
	if (file[j].getString("name").indexOf("database")>=0)
	{
	%>
      <input name="Submit" type="button" class="short-button-red" value="恢复">
      <br>
      <br>
	 <%
	 }
	 %>
      <input name="Submit4" type="button" class="short-button" value="下载">
    </label></td>
  </tr>
  <tr>
    <td align="center" valign="middle" style="line-height:20px;"><font color="#666666"><%=file[j].getString("name")%></font>
	<br>

	<font color="#993366">[<%=StringUtil.formatNumber("#0.00",filesize)%>k</font>]</td>
    </tr>
</table>
</fieldset>
          
				</div>
		</div>
      <%
}
%>    </td>
  </tr>

</table>
</form>
<br>
<%
}
%>
</body>
</html>
