<%@ page contentType="text/html;charset=utf-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,org.jbpm.api.*,org.jbpm.api.task.Task,org.jbpm.api.history.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>工作流管理</title>
<script language="javascript" src="../../common.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="JavaScript" type="text/JavaScript">
<!--

//-->
</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>

<script>
function delPI(name,id)
{
	if ( confirm("确认删除实例 "+ name +" ？") )
	{
		window.location = "<%=ConfigBean.getStringValue("systenFolder")%>action/jbpm/DeleteInstance.action?id="+id;
	}
}
</script>
</head>

<body  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 系统管理 »   工作流管理</td>
  </tr>
</table>
<br>
		 <fieldset style="border:1px #999999 solid;padding:10px;-webkit-border-radius:5px;-moz-border-radius:5px;width:96%;margin-left:15px;">
<legend style="font-size:15px;font-weight:normal;color:#333333;">流程管理</legend>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
  <form name="process_form" method="post">
    <tr>
      <th align="center" valign="middle"  class="left-title">部署工作流</th>
      <th colspan="4" align="center" valign="middle"  class="left-title">
        <input name="Submit" type="button" class="long-long-button-ok" value="部署：退货流程" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/refund/DeployRefundProcess.action'">
		&nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：订单评价" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/orderrate/DeployOrderRateProcess.action'">
		
		&nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：帮助中心" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/hcpublic/DeployHcPublicProcess.action'">
		
		&nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：产品问题" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/newquestion/DeployNewQuestionProcess.action'">
		
		&nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：问题回复" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/question/DeployAnswerQuestionProcess.action'">
		
		
		&nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：采购价格" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/purchaseprice/DeployPurchasePriceProcess.action'">
		<br>

<br>

        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：采购已转帐" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/purtransfer/DeployPurTransferProcess.action'">

		&nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：采购价格确定" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/purcfprice/DeployPurcfpriceProcess.action'">

		&nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：采购可转账" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/purcftransfer/DeployPurcftransferProcess.action'">

		&nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：采购需结清" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/purpayment/DeployPurPaymentProcess.action'">
		
        <br>
      <br>
	  
	  <input name="Submit3" type="button" class="long-long-button-ok" value="部署：交货单审核" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/deliveryapprove/DeployDeliveryApproveProcess.action'">
	  &nbsp;&nbsp;
	  	  <input name="Submit3" type="button" class="long-long-button-ok" value="部署：供货已发 " onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/deliveryIntransit/DeployDeliveryIntransitProcess.action'">
		  &nbsp;&nbsp;
	  	  <input name="Submit3" type="button" class="long-long-button-ok" value="部署：采购单审核 " onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/purchaseapprove/DeployPurchaseApproveProcess.action'">
		   &nbsp;&nbsp;
	  	  <input name="Submit3" type="button" class="long-long-button-ok" value="部署：查看采购单 " onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/noticepurchaser/DeployNoticePurchaserProcess.action'">
			   &nbsp;&nbsp;
	  	  <input name="Submit3" type="button" class="long-long-button-ok" value="部署：采购入库  " onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/warehouse/DeployWarehouseProcess.action'">	  
		  &nbsp;&nbsp;
	  	  <input name="Submit3" type="button" class="long-long-button-ok" value="部署：采购到货超时  " onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/warehouse/DeployDelayPurchaseProcess.action'">	  
		  
	      <br>
      <br>

	  	  <input name="Submit3" type="button" class="long-long-button-ok" value="部署：调价申请  " onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/DeployChangeRetailPriceProcess.action'">	
		  	&nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：客服退款" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/deployRefundProcessAction.action'">
		&nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：资金需申请" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/jbpm/applyMoneyPrecessAction.action'">
        &nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：转账需申请" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/jbpm/fundsTransferPrecessAction.action'"> 
		
	  <br> <br>
<input name="Submit3" type="button" class="long-long-button-ok" value="部署：转运到货需审核" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/purpayment/transportApproveProcess.action'">
        &nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：转运发货需审核" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/purpayment/transportOutboundApproveProcess.action'">
       &nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：转运已起运" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/purpayment/transportDeliveryProcess.action'">
	   &nbsp;&nbsp;
        <input name="Submit3" type="button" class="long-long-button-ok" value="部署：返修发货需审核" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/workflow/purpayment/damagedRepairOutboundApproveProcess.action'">
	  </th>
    </tr>
    <tr>
      <th width="20%" align="center" valign="middle"  class="left-title">ID</th> 
      <th width="23%" align="center" valign="middle"  class="left-title">Name</th>
      <th width="16%" align="center" valign="middle"  class="left-title">Version</th>
      <th width="27%" align="center" valign="middle"  class="left-title">DeploymentIdD</th>
      <th width="14%"  class="right-title">&nbsp;</th>
    </tr>
<%
List<ProcessDefinition> processDefinitionlist = jbpmMgr.getProcessDefinitionList();
for(ProcessDefinition pd : processDefinitionlist)
{
%>
    <tr >
      <td height="39" align="left" valign="middle" ><%=pd.getId()%></td>
      <td height="39" align="left" valign="middle" ><%=pd.getName()%></td>
      <td height="39" align="left" valign="middle" ><%=pd.getVersion()%></td>
      <td height="39" align="left" valign="middle" ><%=pd.getDeploymentId()%></td>
      <td align="center" valign="middle"  >
        <input name="Submit2" type="button" class="short-short-button-del" value="销毁" onClick="location='<%=ConfigBean.getStringValue("systenFolder")%>action/jbpm/DeleteDeployProcess.action?deployment_id=<%=pd.getDeploymentId()%>'">     </td>
    </tr>
<%
}
%>
  </form>
</table>
</fieldset>
         <br>
<br>
		 <fieldset style="border:1px #999999 solid;padding:10px;-webkit-border-radius:5px;-moz-border-radius:5px;width:96%;margin-left:15px;">
<legend style="font-size:15px;font-weight:normal;color:#333333;">流程实例</legend>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
  <form name="process_instance_form" method="post">
    <tr>
      <th width="20%" align="center" valign="middle"  class="left-title">ID</th>
      <th width="23%" align="center" valign="middle"  class="left-title"> Activity</th>
      <th width="16%" align="center" valign="middle"  class="left-title">State</th>
      <th width="27%" align="center" valign="middle"  class="left-title">&nbsp;</th>
      <th width="14%"  class="right-title">&nbsp;</th>
    </tr>
  <%
int pProInstanc = StringUtil.getInt(request,"p");

PageCtrl pcProInstanc = new PageCtrl();
pcProInstanc.setPageNo(pProInstanc);
pcProInstanc.setPageSize(30);

List<ProcessInstance> processInstanceList = jbpmMgr.getProcessInstanceList(pcProInstanc);
for (ProcessInstance pi : processInstanceList)
{
%>
    <tr >
      <td height="39" align="left" valign="middle" ><%=pi.getId()%></td>
      <td height="39" align="left" valign="middle" ><%=pi.findActiveActivityNames()%></td>
      <td height="39" align="left" valign="middle" ><%=pi.getState()%></td>
      <td height="39" align="left" valign="middle" >&nbsp;</td>
      <td align="center" valign="middle"  >
      <input name="Submit23" type="button" class="short-short-button-del" value="删除" onClick="delPI('<%=pi.getId()%>','<%=pi.getId()%>')"></td> 
    </tr>
<%
}
%>
  </form>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
    <input type="hidden" name="p">	
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pcProInstanc.getPageNo() - 1;
int next = pcProInstanc.getPageNo() + 1;
out.println("页数：" + pcProInstanc.getPageNo() + "/" + pcProInstanc.getPageCount() + " &nbsp;&nbsp;总数：" + pcProInstanc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pcProInstanc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pcProInstanc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pcProInstanc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pcProInstanc.getPageCount() + ")",null,pcProInstanc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
</fieldset>
</body>
</html>
