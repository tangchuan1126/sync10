<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  import="java.util.*"%>
<%@ include file="../../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<%
long userAdid = StringUtil.getLong(request,"userAdid");
DBRow userRow = adminMgr.getDetailAdmin(userAdid);

%>
<script type="text/javascript">
function submitData(){

	/**
	 * 1:卡号不存在
	 * 2：密码验证通过
	 * 3：密码验证不通过
	 * 4：卡号已经注册
	 * 5: 注册成功
	 */
	
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/admin/UpdateAdminRegisterToken.action',
		data:$("#subForm").serialize(),
		dataType:'json',
		type:'post',
		success:function(data){
			if(data){
				var adminMsg = data.flag;
				if('1' == adminMsg){
					showMessage("此卡号不存在，请确认...","error");
				}else if('2' == adminMsg){
					showMessage("修改成功...","success");
					setTimeout("windowClose()", 1000);
				}else if('3' == adminMsg){
					showMessage("密码验证不通过，请确认...","error");
				}
				//else if('4' == adminMsg){
				//	showMessage("注册成功...","error");
				//	setTimeout("windowClose()", 1000);
				//}
				else if('5' == adminMsg){
					showMessage("注册成功...","success");
					setTimeout("windowClose()", 1000);
				}else if('6' == adminMsg){
					showMessage("口令同步成功","success");
					setTimeout("windowClose()", 1000);
				}else if('7' == adminMsg){
					showMessage("口令同步未成功，请确认...","error");
				}
			}else{
				showMessage("登记失败","error");
			}
		},
		error:function(){
			showMessage("系统错误","error");
		}
	})	
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};

</script>
</head>
<body>
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			登记令牌
		</legend>	
		<form action="" id="subForm">
			<br/>
			<input type="hidden" name="userAdid" value="<%=userAdid %>"/>
			<table>
				<tr>
					<td style="font-size: 15px;font-weight: bold;">用户名：</td>
					<td><span style="font-size: 20px;font-weight: bold;color: green"><%=userRow.getString("employe_name") %></span></td>
				</tr>
				<tr>
					<td style="font-size: 15px;font-weight: bold;">序列号：</td>
					<td><input name="token_sn" value="<%=userRow.getString("register_token") %>"/></td>
				</tr>
				<tr>
					<td style="font-size: 15px;font-weight: bold;">动态口令：</td>
					<td><input name="token_sn_sninfo" value=""/></td>
				</tr>
			</table>
		</form>
	</fieldset>
	<br/><br/>
	<table align="right">
		<tr align="right">
			<td colspan="2"><input type="button" class="normal-green" value="提交" onclick="submitData();"/></td>
		</tr>
	</table>
	<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>