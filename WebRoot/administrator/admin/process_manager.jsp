<%@ page contentType="text/html;charset=utf-8"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.YesOrNotKey"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"></jsp:useBean>
<jsp:useBean id="yesOrNotKey" class="com.cwc.app.key.YesOrNotKey"></jsp:useBean>
<jsp:useBean id="noticeTypeKey" class="com.cwc.app.key.NoticeTypeKey"></jsp:useBean>
<jsp:useBean id="produrePeriodStatusKey" class="com.cwc.app.key.ProdurePeriodStatusKey"></jsp:useBean>
<jsp:useBean id="displayOrNotKey" class="com.cwc.app.key.DisplayOrNotKey"></jsp:useBean>
<jsp:useBean id="checkedOrNotKey" class="com.cwc.app.key.CheckedOrNotKey"></jsp:useBean>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>流程管理</title>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
 	span.colorShow{text-align:center;height:20px;line-height:20px;width:80px;border:1px solid silver;display:block;margin-left:200px;background:white;cursor:pointer;}
 	span.colorResult{display:block;float:left;text-align:center;height:28px;line-height:28px;width:100px;border:1px solid silver;display:block;margin-left:10px;background:white;cursor:pointer;}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:15px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
span.valueName{width:40%;border:0px solid red;display:block;float:left;text-align:right;}
span.valueSpan{width:56%;border:0px solid green;float:left;overflow:hidden;text-align:left;text-indent:4px;}
span.stateName{width:50%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:50%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;word-break:break-all;}
.produresRelateInfo tr{height: 25px;};
tr.split td{height:24px;line-height:24px;background-image:url("../imgs/linebg_24.jpg");}
.zebraTableTdAlt { background: #f9f9f9;}
.zebraTableTdAltOver { background: #E6F3C5;}
.zebraTableSubTableTdValue{ padding-left: 4px;};

</style>
<script language="JavaScript" type="text/JavaScript">
$(function(){
	 $("#tabs").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
		});
 })
function addProdures(order_type)
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/process_add.html?order_type="+order_type;
	$.artDialog.open(url , {title: '添加流程信息',width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
}

function onLoadInitZebraTable(){
	$("#produresListFirstTr~tr").mouseover(function() {$(this).addClass("zebraTableTdAltOver");}).mouseout(function() {$(this).removeClass("zebraTableTdAltOver");});
}
function updateProdures(produres_id,process_name, associate_order_type)
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/process_update.html?produres_id="+produres_id+"&process_name="+process_name+"&order_type="+associate_order_type;
	$.artDialog.open(url , {title: '更新流程:'+process_name,width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
}
function refreshWindow()
{
	window.location.reload();
}
 </script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
	<div id="tabs" >
		 <ul>
			 <li><a href="#transport">转运单</a></li>
			 <li><a href="#purchase">采购单</a></li>
		 </ul>
		 <div id="transport">
		 <input type="button" value="添加流程" class="long-button-add" onclick="addProdures(<%=ModuleKey.TRANSPORT_ORDER %>);"/><br/><br/>
		 	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" id="produresListTable">
		 		<tr id="produresListFirstTr">
		 			<th width="25%" class="right-title"  style="text-align:center;">主单据信息</th>
		 			<th width="25%" class="right-title"  style="text-align:center;">显示信息</th>
		 			<th width="25%" class="right-title"  style="text-align:center;">阶段信息</th>
		 			<th width="20%" class="right-title"  style="text-align:center;">消息</th>
		 			<td width="5%" class="right-title"  style="text-align:center;">操作</td>
		 		</tr>
		 		<%
		 			DBRow[] produres = produresMgrZyj.getProduresByOrderType(ModuleKey.TRANSPORT_ORDER);
		 			if(produres.length > 0)
		 			{
		 				for(int i = 0; i < produres.length; i ++)
		 				{
		 					String trClass = "";
		 					if(1 == i%2)
		 					{
		 						trClass = "zebraTableTdAlt";
		 					}
		 		%>
		 				<tr class="<%=trClass %>">
		 					<td>
		 						<fieldset class="set">
		 							<legend>
		 								流程名:<span style="font-weight:bold;color: #f60;"><%=produres[i].getString("process_name") %></span>&nbsp;
		 								流程Key值:<span style="font-weight:bold;color: #f60;"><%=produres[i].get("process_key", 0) %></span>
		 							</legend>
		 							<table class="produresRelateInfo" style="" cellpadding="2" cellspacing="0">
		 								<tr>
		 									<td>流程ID:</td>
		 									<td class="zebraTableSubTableTdValue">[&nbsp;<%=produres[i].get("produres_id", 0L) %>&nbsp;]</td>
		 								</tr>
		 								<tr>
		 									<td>在单据中属性名:</td>
		 									<td class="zebraTableSubTableTdValue"><%=produres[i].getString("process_coloum") %></td>
		 								</tr>
		 								<tr>
		 									<td>在页面中属性名:</td>
		 									<td class="zebraTableSubTableTdValue"><%=produres[i].getString("process_coloum_page") %></td>
		 								</tr>
		 								<tr>
		 									<td>关联单据类型:</td>
		 									<td class="zebraTableSubTableTdValue"><%=moduleKey.getModuleName(produres[i].get("associate_order_type", 0)) %></td>
		 								</tr>
		 								<tr>                                                                     
		 									<td>是否单据创建便开始:</td>
		 									<td class="zebraTableSubTableTdValue"><%=yesOrNotKey.getYesOrNotKeyName(produres[i].get("process_start_time_is_create", 0)) %></td>
		 								</tr>
		 								<tr>
		 									<td>流程阶段周期:</td>
		 									<td class="zebraTableSubTableTdValue"><%=produres[i].get("process_period",0) %>天</td>
		 								</tr>
		 								<tr>
		 									<td>默认选中阶段:</td>
		 									<td class="zebraTableSubTableTdValue">
			 									<%
													int[] defaultSelectedVal = {produres[i].get("activity_default_selected",0)};
													String defaultSelectedStr = "";
													DBRow[] activitieRows = produresMgrZyj.getProduresDetailsByProduresId(produres[i].get("produres_id",0),defaultSelectedVal,null);
													if(activitieRows.length > 0)
													{
														defaultSelectedStr = activitieRows[0].getString("activity_name");
													}
												%>
												<%=defaultSelectedStr %>
		 									</td>
		 								</tr>
		 							</table>
		 						</fieldset>
		 					</td>
		 					<td>
		 						<table>
		 							<tr>
		 								<td>流程注解是否显示:</td>
		 								<td class="zebraTableSubTableTdValue"><%=yesOrNotKey.getYesOrNotKeyName(produres[i].get("process_note_is_display", 0)) %></td>
		 							</tr>
		 							<tr>
		 								<td>流程注解内容:</td>
		 								<td class="zebraTableSubTableTdValue"><%=produres[i].getString("process_note") %></td>
		 							</tr>
		 							<tr>
		 								<td>选人框前文字是否显示:</td>
		 								<td class="zebraTableSubTableTdValue"><%=yesOrNotKey.getYesOrNotKeyName(produres[i].get("process_person_is_display",0)) %></td>
		 							</tr>
		 							<tr>
		 								<td>选人文本框前的文字:</td>
		 								<td class="zebraTableSubTableTdValue"><%=produres[i].getString("process_person") %></td>
		 							</tr>
		 							<tr>
		 								<td>子流程是否显示:</td>
		 								<td class="zebraTableSubTableTdValue"><%=yesOrNotKey.getYesOrNotKeyName(produres[i].get("activity_is_need_display",0)) %></td>
		 							</tr>
		 						</table>
		 					</td>
		 					<td>
		 						<table>
		 						<%
		 							DBRow[] activities = produresMgrZyj.getProduresDetailsByProduresId(produres[i].get("produres_id",0),null,null);
		 							if(activities.length > 0)
		 							{
		 								for(int j = 0; j < activities.length; j ++)
		 								{
		 						%>
		 								<tr>
		 									<td><%=activities[j].getString("activity_name") %>:</td>
		 									<td class="zebraTableSubTableTdValue"><%=activities[j].get("activity_val",0) %></td>
		 									<td class="zebraTableSubTableTdValue"><%=produrePeriodStatusKey.getProdureNoticeStatusKeyName(activities[j].get("val_status",0)) %></td>
		 								</tr>
		 						<%			
		 								}
		 							}
		 							else
		 							{
		 								out.println("<tr><td colspan='3'>&nbsp;</td></tr>");
		 							}
		 						%>
		 						</table>
		 					</td>
		 					<td>
		 						<table>
		 						<%
		 							DBRow[] produreNotices = produresMgrZyj.getProduresNoticesByProduresId(produres[i].get("produres_id",0L),0);
		 							if(produreNotices.length > 0)
		 							{
		 								for(int k = 0; k < produreNotices.length; k ++)
		 								{
		 						%>
		 								<tr>
		 									<td><%=produreNotices[k].getString("notice_name") %></td>
		 									<td class="zebraTableSubTableTdValue">(<%=produreNotices[k].getString("notice_coloum_name") %>)</td>
		 									<td class="zebraTableSubTableTdValue"><%=displayOrNotKey.getDisplayOrNotKeyName(produreNotices[k].get("notice_is_display",0)) %></td>
		 									<td class="zebraTableSubTableTdValue">默认<%=checkedOrNotKey.getCheckedOrNotKeyName(produreNotices[k].get("notice_default_selected",0)) %></td>
		 								</tr>
		 						<%			
		 								}
		 							}
		 							else
		 							{
		 								out.println("<tr><td>&nbsp;</td></tr>");
		 							}
		 						%>	
		 						</table>
		 					</td>
		 					<td>
		 						<input type="button" onclick='updateProdures("<%=produres[i].get("produres_id", 0L) %>", "<%=produres[i].getString("process_name") %>", "<%=produres[i].get("associate_order_type", 0) %>")' class="short-button" value="更新"/>
		 					</td>
		 				</tr>
		 		<%
		 				}
		 			}
		 			else
		 			{
		 		%>
		 		
		 		<%		
		 			}
		 		%>
		 	</table>
		 </div>
		 <div id="purchase">
		 <input type="button" value="添加流程" class="long-button-add" onclick="addProdures(<%=ModuleKey.PURCHASE_ORDER %>);"/><br/><br/>
		 	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" id="produresListTable">
		 		<tr id="produresListFirstTr">
		 			<th width="25%" class="right-title"  style="text-align:center;">主单据信息</th>
		 			<th width="25%" class="right-title"  style="text-align:center;">显示信息</th>
		 			<th width="25%" class="right-title"  style="text-align:center;">阶段信息</th>
		 			<th width="20%" class="right-title"  style="text-align:center;">消息</th>
		 			<td width="5%" class="right-title"  style="text-align:center;">操作</td>
		 		</tr>
		 		<%
		 			DBRow[] purchaseProdures = produresMgrZyj.getProduresByOrderType(ModuleKey.PURCHASE_ORDER);
		 			if(purchaseProdures.length > 0)
		 			{
		 				for(int i = 0; i < purchaseProdures.length; i ++)
		 				{
		 					String trClass = "";
		 					if(1 == i%2)
		 					{
		 						trClass = "zebraTableTdAlt";
		 					}
		 		%>
		 				<tr class="<%=trClass %>">
		 					<td>
		 						<fieldset class="set">
		 							<legend>
		 								流程名:<span style="font-weight:bold;color: #f60;"><%=purchaseProdures[i].getString("process_name") %></span>&nbsp;
		 								流程Key值:<span style="font-weight:bold;color: #f60;"><%=purchaseProdures[i].get("process_key", 0) %></span>
		 							</legend>
		 							<table class="produresRelateInfo" style="" cellpadding="2" cellspacing="0">
		 								<tr>
		 									<td>流程ID:</td>
		 									<td class="zebraTableSubTableTdValue">[&nbsp;<%=purchaseProdures[i].get("produres_id", 0L) %>&nbsp;]</td>
		 								</tr>
		 								<tr>
		 									<td>在单据中属性名:</td>
		 									<td class="zebraTableSubTableTdValue"><%=purchaseProdures[i].getString("process_coloum") %></td>
		 								</tr>
		 								<tr>
		 									<td>在页面中属性名:</td>
		 									<td class="zebraTableSubTableTdValue"><%=purchaseProdures[i].getString("process_coloum_page") %></td>
		 								</tr>
		 								<tr>
		 									<td>关联单据类型:</td>
		 									<td class="zebraTableSubTableTdValue"><%=moduleKey.getModuleName(purchaseProdures[i].get("associate_order_type", 0)) %></td>
		 								</tr>
		 								<tr>                                                                     
		 									<td>是否单据创建便开始:</td>
		 									<td class="zebraTableSubTableTdValue"><%=yesOrNotKey.getYesOrNotKeyName(purchaseProdures[i].get("process_start_time_is_create", 0)) %></td>
		 								</tr>
		 								<tr>
		 									<td>流程阶段周期:</td>
		 									<td class="zebraTableSubTableTdValue"><%=purchaseProdures[i].get("process_period",0) %>天</td>
		 								</tr>
		 								<tr>
		 									<td>默认选中阶段:</td>
		 									<td class="zebraTableSubTableTdValue">
			 									<%
													int[] defaultSelectedValPurchase = {purchaseProdures[i].get("activity_default_selected",0)};
													String defaultSelectedStrPurchase = "";
													DBRow[] activitieRowsPurchase = produresMgrZyj.getProduresDetailsByProduresId(purchaseProdures[i].get("produres_id",0),defaultSelectedValPurchase,null);
													if(activitieRowsPurchase.length > 0)
													{
														defaultSelectedStrPurchase = activitieRowsPurchase[0].getString("activity_name");
													}
												%>
												<%=defaultSelectedStrPurchase %>
		 									</td>
		 								</tr>
		 							</table>
		 						</fieldset>
		 					</td>
		 					<td>
		 						<table>
		 							<tr>
		 								<td>流程注解是否显示:</td>
		 								<td class="zebraTableSubTableTdValue"><%=yesOrNotKey.getYesOrNotKeyName(purchaseProdures[i].get("process_note_is_display", 0)) %></td>
		 							</tr>
		 							<tr>
		 								<td>流程注解内容:</td>
		 								<td class="zebraTableSubTableTdValue"><%=purchaseProdures[i].getString("process_note") %></td>
		 							</tr>
		 							<tr>
		 								<td>选人框前文字是否显示:</td>
		 								<td class="zebraTableSubTableTdValue"><%=yesOrNotKey.getYesOrNotKeyName(purchaseProdures[i].get("process_person_is_display",0)) %></td>
		 							</tr>
		 							<tr>
		 								<td>选人文本框前的文字:</td>
		 								<td class="zebraTableSubTableTdValue"><%=purchaseProdures[i].getString("process_person") %></td>
		 							</tr>
		 							<tr>
		 								<td>子流程是否显示:</td>
		 								<td class="zebraTableSubTableTdValue"><%=yesOrNotKey.getYesOrNotKeyName(purchaseProdures[i].get("activity_is_need_display",0)) %></td>
		 							</tr>
		 						</table>
		 					</td>
		 					<td>
		 						<table>
		 						<%
		 							DBRow[] activitiesPurchase = produresMgrZyj.getProduresDetailsByProduresId(purchaseProdures[i].get("produres_id",0),null,null);
		 							if(activitiesPurchase.length > 0)
		 							{
		 								for(int j = 0; j < activitiesPurchase.length; j ++)
		 								{
		 						%>
		 								<tr>
		 									<td><%=activitiesPurchase[j].getString("activity_name") %>:</td>
		 									<td class="zebraTableSubTableTdValue"><%=activitiesPurchase[j].get("activity_val",0) %></td>
		 									<td class="zebraTableSubTableTdValue"><%=produrePeriodStatusKey.getProdureNoticeStatusKeyName(activitiesPurchase[j].get("val_status",0)) %></td>
		 								</tr>
		 						<%			
		 								}
		 							}
		 							else
		 							{
		 								out.println("<tr><td colspan='3'>&nbsp;</td></tr>");
		 							}
		 						%>
		 						</table>
		 					</td>
		 					<td>
		 						<table>
		 						<%
		 							DBRow[] produreNoticesPurchase = produresMgrZyj.getProduresNoticesByProduresId(purchaseProdures[i].get("produres_id",0L),0);
		 							if(produreNoticesPurchase.length > 0)
		 							{
		 								for(int k = 0; k < produreNoticesPurchase.length; k ++)
		 								{
		 						%>
		 								<tr>
		 									<td><%=produreNoticesPurchase[k].getString("notice_name") %></td>
		 									<td class="zebraTableSubTableTdValue">(<%=produreNoticesPurchase[k].getString("notice_coloum_name") %>)</td>
		 									<td class="zebraTableSubTableTdValue"><%=displayOrNotKey.getDisplayOrNotKeyName(produreNoticesPurchase[k].get("notice_is_display",0)) %></td>
		 									<td class="zebraTableSubTableTdValue">默认<%=checkedOrNotKey.getCheckedOrNotKeyName(produreNoticesPurchase[k].get("notice_default_selected",0)) %></td>
		 								</tr>
		 						<%			
		 								}
		 							}
		 							else
		 							{
		 								out.println("<tr><td>&nbsp;</td></tr>");
		 							}
		 						%>	
		 						</table>
		 					</td>
		 					<td>
		 						<input type="button" onclick='updateProdures("<%=purchaseProdures[i].get("produres_id", 0L) %>", "<%=purchaseProdures[i].getString("process_name") %>", "<%=purchaseProdures[i].get("associate_order_type", 0) %>")' class="short-button" value="更新"/>
		 					</td>
		 				</tr>
		 		<%
		 				}
		 			}
		 			else
		 			{
		 		%>
		 		
		 		<%		
		 			}
		 		%>
		 	</table>
		 </div>
	</div>
  </body>
</html>
