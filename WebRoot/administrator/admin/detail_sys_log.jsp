<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.HashMap"%>
<%
long sl_id = StringUtil.getLong(request,"sl_id");
DBRow detail = systemConfig.getDetailSysLogBySlid(sl_id);

HashMap operateTypeHM = new HashMap();
operateTypeHM.put("1","<font color=blue>访问页面</font>");
operateTypeHM.put("2","<font color=red>提交事件</font>");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#999999">
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#eeeeee"><strong>类型</strong></td>
    <td width="83%" align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px;"><%=operateTypeHM.get(detail.getString("operate_type"))%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF"><strong>执行操作</strong></td>
    <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px;"><%=detail.getString("operation")%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#eeeeee"><strong>操作参数</strong></td>
    <td align="left" valign="top" bgcolor="#FFFFFF" style='WORD-WRAP: break-word;TABLE-LAYOUT: fixed;word-break:break-all;padding:10px;'><%=detail.getString("operate_para")%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF"><strong>来路</strong></td>
    <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px;"><%=detail.getString("ref")%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF"><strong>执行人</strong></td>
    <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px;"><%=detail.getString("account")%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#eeeeee"><strong>角色</strong></td>
    <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px;"><%=detail.getString("adgid")%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#FFFFFF"><strong>IP</strong></td>
    <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px;"><%=detail.getString("ip")%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#eeeeee"><strong>浏览器 </strong></td>
    <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px;"><%=detail.getString("visitor")%></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="middle" bgcolor="#eeeeee"><strong>执行时间</strong></td>
    <td align="left" valign="top" bgcolor="#FFFFFF" style="padding:10px;"><%=detail.getString("post_date")%></td>
  </tr>
</table>
</body>
</html>
