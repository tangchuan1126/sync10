<%@ page contentType="text/html;charset=utf-8"%>
<%@ include file="../../include.jsp"%> 

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script language="javascript" src="../../common.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">


<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style></head>

<body  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 基础数据管理 »   汇率</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0"   class="zebraTable">
  <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batchModifySystemConfig.action" method="post" name="newform">
  <input type="hidden" name="logo" value="<%=systemConfig.getStringConfigValue("logo")%>">
    <tr> 
      <td width="29%" height="31" valign="middle" > 
        <%=systemConfig.getStringConfigDescription("USD")%>  (USD)    </td>
      <td width="71%" valign="middle" > <%=systemConfig.getStringConfigValue("USD")%> : 1 RMB </td>
    </tr>
    <tr> 
      <td height="31" valign="middle" > 
        <%=systemConfig.getStringConfigDescription("AUD")%>   (AUD)    </td>
      <td valign="middle" > <%=systemConfig.getStringConfigValue("AUD")%>: 1 RMB</td>
    </tr>
    <tr> 
      <td height="31" valign="middle" > 
        <%=systemConfig.getStringConfigDescription("EUR")%>   (EUR)    </td>
      <td valign="middle" > <%=systemConfig.getStringConfigValue("EUR")%>: 1 RMB</td>
    </tr>
    <tr> 
      <td height="31" valign="middle" > 
        <%=systemConfig.getStringConfigDescription("CAD")%>   (CAD)    </td>
      <td valign="middle" > <%=systemConfig.getStringConfigValue("CAD")%>: 1 RMB</td>
    </tr>
    <tr> 
      <td height="31" valign="middle" > 
        <%=systemConfig.getStringConfigDescription("CHF")%>   (CHF)    </td>
      <td valign="middle" > <%=systemConfig.getStringConfigValue("CHF")%>: 1 RMB</td>
    </tr>
    <tr> 
      <td height="31" valign="middle" > 
        <%=systemConfig.getStringConfigDescription("CZK")%>    (CZK)   </td>
      <td valign="middle" ><%=systemConfig.getStringConfigValue("CZK")%>: 1 RMB</td>
    </tr>
    <tr> 
      <td height="31" valign="middle" > 
        <%=systemConfig.getStringConfigDescription("GBP")%>   (GBP)    </td>
      <td valign="middle" > <%=systemConfig.getStringConfigValue("GBP")%>: 1 RMB</td>
    </tr>
    <tr> 
      <td height="31" valign="middle" > 
        <%=systemConfig.getStringConfigDescription("NOK")%>   (NOK)    </td>
      <td valign="middle" ><%=systemConfig.getStringConfigValue("NOK")%>: 1 RMB</td>
    </tr>
  </form>
</table>
<br>
</body>
</html>
