<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<html>
  <head>
 
    <title>添加部门及角色</title>
  
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>

<%--多功能树形下拉UI控件--%>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
  </head>
  
  <body>
  <form>
   <fieldset style="border:2px #cccccc solid;padding:15px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">添加部门、职务及办公地点</legend>
  <table>
  		<tr> 
      <td width="12%" height="30" align="right" class="STYLE2">部门</td>
      <td width="2%">&nbsp;</td>
      <td colspan="2">
	  	
      <select name="proAdgid" id="proAdgid" ">
	  <option value="0">选择部门...</option>
	  <%
	

	DBRow adminGroup[] = adminMgr.getAllAdminGroup(null);
	for (int i=0; i<adminGroup.length; i++)
	{
	
	  %>
	    <option style="background:#fffffffff;" value="<%=adminGroup[i].getString("adgid")%>" ><%=adminGroup[i].getString("name")%></option>
	  <%
	  }
	  %>
      </select> 
	  </td>
  </tr>
  <tr> 
      <td width="12%" height="30" align="right" class="STYLE2">职务</td>
      <td width="2%">&nbsp;</td>
      <td colspan="2">
      <select name="proJsId" id="proJsId" >
	  <option value="0">选择角色...</option>	  
		<option value='1'>员工</option>
		<option value='5'>副主管</option>
		<option value='10'>主管</option>
      </select> 
	  </td>
  </tr>
  <tr>
    <td align="right" valign="middle" class="STYLE2" colspan="1" width="15%">办公地点</td>
    <td>&nbsp;</td>
			    <td align="left" valign="middle" colspan="2">
			    	<ul id="locationmenu" class="mcdropdown_menu">
					  <li rel="0">所有办公地点</li>
					  <%
					  DBRow c1[] = officeLocationMgr.getChildOfficeLocation(0);
					  for (int i=0; i<c1.length; i++)
					  {
							out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("office_name"));
				
							  DBRow c2[] = officeLocationMgr.getChildOfficeLocation(c1[i].get("id",0l));
							  if (c2.length>0)
							  {
							  		out.println("<ul>");	
							  }
							  for (int ii=0; ii<c2.length; ii++)
							  {
									out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("office_name"));		
									
										DBRow c3[] = officeLocationMgr.getChildOfficeLocation(c2[ii].get("id",0l));
										  if (c3.length>0)
										  {
												out.println("<ul>");	
										  }
											for (int iii=0; iii<c3.length; iii++)
											{
													out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("office_name"));
													out.println("</li>");
											}
										  if (c3.length>0)
										  {
												out.println("</ul>");
										  }
										  
									out.println("</li>");				
							  }
							  if (c2.length>0)
							  {
							  		out.println("</ul>");	
							  }
							  
							out.println("</li>");
					  }
					  %>
				</ul>
					  <input type="text" name="location" id="location" value="" />
					  <input type="hidden" name="filter_lid" id="filter_lid" value=""  />
					  
				
				<script>
				$("#location").mcDropdown("#locationmenu",{
						allowParentSelect:true,
						  select: 
								function (id,name)
								{
									$("#filter_lid").val(id);
									$("#location").val(name);
								}
				
				});
				$("#location").mcDropdown("#locationmenu").setValue(0);
				
				</script>
			    </td>
   </tr>
  <tr align="center">
  	<td colspan="3"><input type="button" name="addGroupRole" id="addGroupRole" onclick="submitForm();" value="添加"/></td>
  </tr>
  </table>
  </fieldset>
 </form>
     
  </body>
  <script>
  //保存添加部门\职务及办公地点
	function submitForm() {
		var group = $("#proAdgid").find("option:selected").text();
		var proAdgid = $("#proAdgid").val();
		var role = $("#proJsId").find("option:selected").text();
		var proJsId = $("#proJsId").val();		
		var location = $("#location").val();
		var filter_lid = $("#filter_lid").val();
		
		$.artDialog.opener.showValue(group,proAdgid,role,proJsId,filter_lid,location);
		$.artDialog && $.artDialog.close();
	}

  </script>

</html>
