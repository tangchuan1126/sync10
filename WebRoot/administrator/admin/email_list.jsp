<%@ page contentType="text/html;charset=utf-8"%>
<%@ include file="../../include.jsp"%> 
<%

if (StringUtil.getString(request,"cmd").equals("export"))
{

	
	DBRow orders[] = orderMgr.getOrdersByFilter(request,null);
	
	String d = DateUtil.NowStr();
	String filename = StringUtil.replaceString(d, "-", "");
	filename = StringUtil.replaceString(filename, " ", "_");
	filename = StringUtil.replaceString(filename, ":", "");

	response.setContentType("text/plain"); 
	response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + ".txt\"");

	for ( int j=0; j<orders.length; j++ )
	{
		out.write(orders[j].getString("client_id"));  
		out.write(",");  
	}

	return;
}
//注解
String field = StringUtil.getString(request,"field");
String val = StringUtil.getString(request,"val");
String st = StringUtil.getString(request,"st");
String en = StringUtil.getString(request,"en");
int p = StringUtil.getInt(request,"p");
String business = StringUtil.getString(request,"business");
int handle = StringUtil.getInt(request,"handle");
int handle_status = StringUtil.getInt(request,"handle_status",0);

%>
<link href="../comm.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script>

function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.st.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.en.value = date;
}

</script>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle" />&nbsp;&nbsp; 系统管理 »   邮件列表</td>
  </tr>
</table>
<br />
<table width="98%" height="58" border="0" align="center" cellpadding="1" cellspacing="0">
  <tr align="left" valign="middle">
    <td width="77%" height="56"><table width="100%" border="0" cellspacing="7" cellpadding="1">
      <form action="email_list.html" method="post" name="filterForm" id="filterForm">
	  <input type="hidden" name="cmd" value="export">
        <tr>
          <td><%
TDate tDate = new TDate();
tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

String input_st_date,input_en_date;
if ( st.equals("") )
{	
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}
else
{	
	input_st_date = st;
}


if ( en.equals("") )
{	
	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
}
else
{	
	input_en_date = en;
}
%>
            开始
            <input name="st" type="text" id="st" size="9" value="<%=input_st_date%>" readonly="readonly" />
                <script language="JavaScript" type="text/javascript" >
    <!--
    	stfooCalendar = new dynCalendar('stfooCalendar', 'stcalendarCallback', '../js/images/');
    //-->
    </script>
            &nbsp; &nbsp; &nbsp;结束
            <input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  readonly="readonly" />
            <script language="JavaScript" type="text/javascript">
    <!--
    	enfooCalendar = new dynCalendar('enfooCalendar', 'encalendarCallback', '../js/images/');
    //-->
	</script>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <select name="business">
              <%
String businessl[] = systemConfig.getStringConfigValue("business").split(",");
for (int i=0; i<businessl.length; i++)
{
%>
              <option value="<%=businessl[i]%>" <%=business.equals(businessl[i])?"selected":""%>> <%=businessl[i]%> </option>
              <%
}
%>
            </select></td>
        </tr>
        <tr>
          <td height="0"><input type="radio" name="handle" value="1" <%=handle==1?"checked":""%> />
            待抄单
            <input type="radio" name="handle" value="2" <%=handle==2?"checked":""%> />
            待打印
            <input type="radio" name="handle" value="3" <%=handle==3?"checked":""%> />
            已打印
            <input type="radio" name="handle" value="4" <%=handle==4?"checked":""%> />
            已上网
            <input name="handle" type="radio" value="0" <%=handle==0?"checked":""%> />
            全部
            <label for="delivered"></label>
            |
            <input name="handle_status" type="radio" value="0"  <%=handle_status==0?"checked":""%> />
            正常
            <input type="radio" name="handle_status" value="1"  <%=handle_status==1?"checked":""%> />
            疑问
            <input type="radio" name="handle_status" value="2"  <%=handle_status==2?"checked":""%> />
            归档
            <input type="radio" name="handle_status" value="-1"  <%=handle_status==-1?"checked":""%> />
            全部
            <input name="Submit4" type="submit" class="long-button" value="    导 出    "  />
            &nbsp; &nbsp;</td>
        </tr>
      </form>
    </table></td>
  </tr>
</table>
<br />
