<%@ page contentType="text/html;charset=utf-8"%>
 

<%@ include file="../../include.jsp"%> 
<%
DBRow rows[] = ebayMgrZr.getAllEbayAccount();
String getSessionIdAction =  ConfigBean.getStringValue("systenFolder") + "action/administrator/admin/GetEbaySessionIdAction.action";
String getEbayToken =  ConfigBean.getStringValue("systenFolder") + "action/administrator/admin/GetEbayTokenAction.action";
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Ebay Account List</title>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
 
 
<script language="JavaScript" type="text/JavaScript">
 
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};

	var dataJson ;
 	function addNew(){
 	  	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/ebay_account_add.html"; 
 		 $.artDialog.open(uri , {title: '新增Ebay账号',width:'500px',height:'320px', lock: true,opacity: 0.3,fixed: true});
 	}
 	function updateEbayAccount(ebay_account_id){
 	   var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/ebay_account_update.html?"+"ebay_account_id="+ebay_account_id; 
		 $.artDialog.open(uri , {title: '更新Ebay账号',width:'500px',height:'320px', lock: true,opacity: 0.3,fixed: true});
 	}
 	function getSession(ebay_account_id){
 	 	var sandBoxUri = "https://signin.sandbox.ebay.com/ws/eBayISAPI.dll?SignIn" ;
 	 	var ebayUri = "https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&RuName=YourRuNameHere&SessID=YourSessionIDHere  " ;
		$.ajax({
			url:'<%= getSessionIdAction%>',
			dataType:'json',
			beforeSend:function(request){$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});},
			success:function(data){
			    $.unblockUI();
				if(data.ru_name && data.session_id){
					dataJson = data ;
					dataJson["ebay_account_id"] = ebay_account_id;
		 
					window.open(sandBoxUri+"&RuName="+data.ru_name+"&SessID="+data.session_id);
				}else{alert("获取Session Id 失败");}
			}
		})
 	}
	function getNewToken(ebay_account_id){
		if(!dataJson){alert("请先获取Session");return ;}
	 
		if(ebay_account_id * 1 == dataJson["ebay_account_id"] *1 ){
			$.ajax({
				url:'<%= getEbayToken%>',
				data:jQuery.param(dataJson),
				dataType:'json',
				beforeSend:function(request){$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});},
				success:function(data){
				    $.unblockUI();
					if(data.flag === "success"){
						window.location.reload();
					}else{
						if(data.message.length > 0 ){alert(data.message);}else{alert("系统错误!")}
					}
				}
			})
		}else{alert("不是同一个账号的Ebay Token 更新!");return ;}
	}
 	function refreshWindow(){window.location.reload();}
 	 
</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">sandBoxUri
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
} 
span.specil {color:#f60;font-weight:bold;}
-->
</style></head>

<body onLoad="onLoadInitZebraTable()">
<div class="note">
	<h1 style="font-size:14px;font-weight:bold;">获取新的EbayToken</h1>
	<table>
		<tr>
			<td>方法一:</td>
			<td>
				1.登录ebay开发者网站<a target="_blank" href="https://developer.ebay.com/base/membership/signin/?ReturnUrl=https%3a%2f%2fdeveloper.ebay.com%2fDevZone%2faccount%2fdefault.aspx&mo=true">Ebaye Developer</a><br />
				2.输入账号:vvmezhangrui &nbsp;&nbsp; 密码:Beijing_vvme_0<br />	
				3.在Tools栏中选择<span class="specil"> Get a User Token </span><br />	
				4.在新的弹出页面中选择<span class="specil">Select the environment</span>为Production<br />	
				5.在<span class="specil">Select a key set or enter one manually</span>为Manual Entry<br />	
				6.developer  :88f39232-2e13-4baf-bbd9-a5694e58f12a<br />	
				  application :Visionar-8d75-480d-aa32-a66d3a89574b<br />	
				  certificate :eb9b8d5b-97b2-4adc-8bc8-9cff565f0607<br />	
				7.点击Generate Token.这个时候需要登录Ebay账号授权.比如是(hiseller1111,smart_tattoo)<br />
				8.点击<span class="specil">同意</span> ---> <span class="specil">Save Token</span><br />
				9.复制新的Token,然后点击系统中对应的Ebay账号修改Token保存到数据库中
			</td>
			<td>方法二:</td>
			<td>
				1.点击<span class="specil">GetSession</span>,成功过后会返回打开一个新的页面<br/>
				2.在新的页面需要登录你要获取的Ebay账号。(获取授权)<br/>
				3.授权成功过后,然后点击页面的<span class="specil">获取Token</span><br/>
				4.每个账号的更新都需要授权,也就是说每次都要获取一个新的Session.<br/>如果Session获取成功,但是Token没有成功,那么可以不用刷新该页面。再次点击<span class="specil">获取Token</span>按钮
			</td>
		</tr>
		
	</table>

</div>
<div style="width:98%;">
&nbsp;&nbsp;<input type="button" value="添加" class="long-button-add" onclick="addNew()"/>
</div>
<br /> 
 
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0"  class="zebraTable" isNeed="not">
  

    <tr> 
      <th width="15%"  style="vertical-align: center;text-align: center;" class="left-title">账号</th>
      <th width="11%"  style="vertical-align: center;text-align: center;" class="right-title">密码</th>
      <th width="56%"  style="vertical-align: center;text-align: center;" class="right-title">Token</th>
      <th width="10%"  style="vertical-align: center;text-align: center;" class="right-title">操作</th>
    </tr>
	<%
		if(rows != null && rows.length > 0 ){
			for(DBRow row : rows){
				%>
				<tr>
					<td><%= row.getString("user_id")%></td>
					<td><%= row.getString("password")%>&nbsp;</td>
					<td><%= row.getString("user_token")%></td>
					<td>
						<input type="button" class="short-short-button-mod" value="修改" onclick="updateEbayAccount('<%=row.getString("id") %>')"/><br />
						<input type="button" class="short-button" value="获取Sesion" onclick="getSession('<%=row.getString("id") %>');"/><br />
						<input type="button" class="short-button" value="获取新Token" onclick="getNewToken('<%=row.getString("id") %>');" />
					</td>
					
				</tr>
				
				<%
			}
				
		}
	%>
  
</table>
<br>
<br>
</body>
</html>
