<%@ page contentType="text/html;charset=utf-8"%>
<%@ include file="../../include.jsp"%>
<%
	String cmd = StringUtil.getString(request,"cmd");
	String key = StringUtil.getString(request,"key");
	int search_mode = StringUtil.getInt(request, "search_mode");
	//获取office_location办公地点信息
	Tree location_tree = new Tree(ConfigBean.getStringValue("office_location"));

	long proPsId = StringUtil.getLong(request, "proPsId"); //仓库
	String proJsId = StringUtil.getString(request,"proJsId");//角色
	long proAdgid = StringUtil.getLong(request, "proAdgid"); //部门
	long filter_lid = StringUtil.getLong(request,"filter_lid");//办公地点
	int	lock_state	= StringUtil.getInt(request,"lock_state");

	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);

	DBRow rows[];
	if(cmd.equals("search")){
		rows = adminMgr.getSearchAdmins(key, search_mode, pc);
				//adminMgrGZY.getDetailAdminByAccount(pc,key);
	}else if(cmd.equals("filter")){
  		rows = adminMgrGZY.getadvanSearch(pc,request);
	}else{
		rows = adminMgrGZY.getAllAdmin(pc);
	}
	
	//文件下载
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String ExportAdminsTitlesAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/admin/ExportAdminsTitlesAction.action";
%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript" ></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />

<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />

<title>admin</title>
<style type="text/css">
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

</style>
<script language="JavaScript" type="text/JavaScript">
function go(p)
{
	document.dataForm.p.value = p;
	document.dataForm.submit();
}
$(document).ready(function(){
  $("#location").mcDropdown("#locationmenu",{
		allowParentSelect:true,
		  select: 
				function (id,name)
				{					
					$("#filter_lid").val(id);
				}
});

<%
				if (filter_lid > 0)
				{
				%>
				$("#location").mcDropdown("#locationmenu").setValue(<%=filter_lid%>);//设置文本框中的值
				<%
				}
				else
				{
				%>
				$("#location").mcDropdown("#locationmenu").setValue(0);
				<%
				}
				%> 
	addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/GetSearchAdminsJSONAction.action",
			"merge_field",
			"adid");
  
  });

//弹出dialog窗口修改管理员
function modAdmins(account,id){
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/mod_admin.html?account="+account+"&adid="+id; 
    $.artDialog.open(uri , {title: "修改用户",width:'750px',height:'500px', lock: true,opacity: 0.3,fixed: true});
} 
//弹出dialog窗口添加管理员
function addAdmins(){
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/Add_Admin.html"; 
    $.artDialog.open(uri , {title: "增加用户",width:'750px',height:'500px', lock: true,opacity: 0.3,fixed: true});
} 

function lockAdmin(cmd,adid,account)
{
	document.conf_form.adid.value = adid;
	if (cmd==0)
	{
		document.conf_form.account.value=account;
		document.conf_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/lockAdmin.action";
	}
	else
	{
		document.conf_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/unLockAdmin.action";
	}
	document.conf_form.submit();
}

function delAdmin(adid,account)
{
	if (confirm("确认删除帐号吗？")){
		
		document.conf_form.account.value = account;
		document.conf_form.adid.value = adid;
		document.conf_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delAdmin.action";
		document.conf_form.submit();
	}
	else
	{
		return(false);
	}
}

function search(){
	if ($("#search_key").val()=="")
	{
		alert("请填写关键字");
		return(false);
	}
	
}
function regisToken(userAdid){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/register_token.html?userAdid="+userAdid;
	$.artDialog.open(url , {title: '登记令牌',width:'450px',height:'300px', lock: true,opacity: 0.3,fixed: true});
};

function manageTitle(userAdid, userName){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_list_by_admin_selected.html?user_adid="+userAdid+"&userName="+userName;
	$.artDialog.open(url , {title: '管理&nbsp;['+userName+']&nbsp;title',width:'700px',height:'450px', lock: true,opacity: 0.3,fixed: true});
}

function refreshWindow(){
	window.location.reload();
};

//在线预览图片
function showPictrueOnline(currentName){
    var obj = {
		current_name : currentName ,
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_admin")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}

//用户TITLE导入
function importAdminTitle(_target){

	var targetNode = $("#"+_target);
	var fileNames = $("input[name='file_names']",targetNode).val();
	
	var obj  = {
	     reg:"xls",
	     limitSize:2,
	     target:_target,
	     limitNum:1
	};
	
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	
	if(fileNames.length > 0){
		uri += "&file_names=" + fileNames;
	}
	
	$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}

//回调函数 预览Excel文件内容
function uploadFileCallBack(fileNames){
	
	if($.trim(fileNames).length > 0){
	
		$.blockUI({ 
			message:'<img src="../imgs/sending.gif" align="absmiddle"/>&nbsp;<span style="font-size:13px;font-weight:bold;color:#666666">检查文件内容是否有错误<br/>请稍后......</span>'});
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/check_admin_title_show.html?fileNames="+fileNames;
		$.artDialog.open(uri,{title: "检测上传用户title信息",width:'850px',height:'530px', lock: true,opacity: 0.3,fixed: true});
		$.unblockUI();
	}
}

//用户TITLE导出
function exportAdminTitle(){

	var para = "";
	if('search' == '<%=cmd%>'){
		para = "cmd=<%=cmd%>&account=<%=key%>";
	}else{
		para = "cmd=<%=cmd%>&proPsId=<%=proPsId%>&proJsId=<%=proJsId%>&proAdgid=<%=proAdgid%>&filter_lid=<%=filter_lid%>&lock_state=<%=lock_state%>";
	}
	$.ajax({
		url:'<%=ExportAdminsTitlesAction%>',
		type:'post',
		dataType:'json',
		timeout: 60000,
		cache:false,
		data:para,
		beforeSend:function(request){
			$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">正在导出，请稍后......</div>'});
		},
		error:function(){
			showMessage("提交失败，请重试！","error");
		},
		success:function(msg){
			if(msg && msg.flag == "true"){
				window.location="../../"+msg["fileurl"];
				$.unblockUI();
			}else{
				$.unblockUI();
	   		}
	  	}
	});
}
function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
		
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
		   searchRightButton();
		}  
	}  
}
function searchRightButton()
{
	var val = $("#search_key").val();
			
	if (val=="")
	{
		alert("Pleae input search key.");
	}
	else
	{
		val = val.replace(/\'/g,'');
		$("#search_key").val(val);
		document.search_form.key.value = val;
		document.search_form.search_mode.value = 2;
		document.search_form.submit();
	}
}
function search()
{
	var val = $("#search_key").val();
			
	if(val.trim()=="")
	{
		alert("Pleae input search key.");
	}
	else
	{
		var val_search = "\'"+val.toLowerCase()+"\'";
		$("#search_key").val(val_search);
		document.search_form.key.value = val_search;
		document.search_form.search_mode.value = 1;
		document.search_form.submit();
	}
}
//-->
</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 2px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
div.mcdropdown {
	width: 250px;
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
.search_shadow_bg{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
}
-->
span.InfoLeft{
	width:60px;
	display:block;
	float:left;
	text-align:right;
	border:0px solid red;
}
span.InfoRight{
	display:block;
	text-align:left;
	float:left;
	text-indent:5px;
}
p{
	clear:both;
	text-align:center;
}
</style>
</head>

<body onload="onLoadInitZebraTable()" leftmargin="10">
<br>
<table width="98%" border="0" align="center" cellpadding="5" cellspacing="0">
	<tr>
		<td class="page-title">
			<img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">
			&nbsp;&nbsp; 系统管理 »   帐号管理
		</td>
	</tr>
</table>
<br>
<br>

<form  method="post" name="mod_form" id="mod_form">
	<input name="pwd"   type="hidden" >
	<input name="employe_name" type="hidden" >
	<input name="adid"   type="hidden" >
	<input name="ps_id"   type="hidden" >
	<input name="email"   type="hidden" >
	<input name="skype"   type="hidden" >
	<input name="QQ"   type="hidden" >
	<input name="mobile"   type="hidden" >
	<input name="msn"   type="hidden" >
	<input name="account"   type="hidden" >
	<input name="entrytime"   type="hidden" >
	<input name="arrGRL" type="hidden" />
	<input name="adgid" type="hidden">
	<!-- 记录页数，以便在修改过后还能转到当前页 -->
	<input name="p" type="hidden" />
	<input name="file_names" type="hidden">
	<input name="sn" type="hidden">
	<input name="path" type="hidden"/>
	<input name="filePath" type="hidden"/>
	<input name="areaId" type="hidden" />
</form>

<form method="post" name="add_form" >
	<input name="account"  type="hidden" >
	<input name="pwd"   type="hidden" >
	<input name="employe_name" type="hidden" >
	<input name="ps_id"   type="hidden" >
	<input name="email"   type="hidden" >
	<input name="skype"   type="hidden" >
	<input name="msn"   type="hidden" >
	<input name="QQ"   type="hidden" >
	<input name="mobile"   type="hidden" >
	<input name="entrytime" type="hidden" />
	<input name="arrGRL" type="hidden" />
	<input name="adgid" type="hidden" />
	<input name="areaId" type="hidden" />
</form>

<form name="conf_form">
	<input type="hidden" name="adid">
	<input type="hidden" name="account">
</form>
<form action="ct_admin.html" method="post" name="search_form">
	<input type="hidden" name="key"/>
	<input type="hidden" name="cmd" value="search"/>
	<input type="hidden" name="search_mode"/>
</form>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<div class="demo" >
			<div id="tabs">
				<ul>
					<li><a href="#usual_tools">常用工具</a></li>
					<li><a href="#advan_search">高级搜索</a></li>		 
				</ul>
     
				<div id="usual_tools">
					<!-- action不填写默认是调用 ct_admin.html-->
<!-- 					<form method="post" action="ct_admin.html" onSubmit="search()" > -->
						<input type="hidden" id="cmd" name="cmd" value="search"/>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="30%" style="padding-top:3px;">
									<div id="easy_search_father">
									<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
									</div>
									<table width="485" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="418">
												<div  class="search_shadow_bg">
												 <input name="search_key" type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="search_key" onkeydown="if(event.keyCode==13)search()" value="<%=key%>"/>
												</div>
											</td>
											<td width="67">
												 
											</td>
										</tr>
									</table>
									<script>eso_button_even();</script>
								</td>
								<td>
<!-- 									根据账号查询 -->
<!-- 					 				<input type="text"  size="30" id="account" name="account"  /> -->
<!-- 									<input type="submit" class="button_long_search" id="btnSubmit" name="btnSubmit" value="搜    索"  /> -->
						    		<input name="Submit2" type="button" class="long-button-add" onClick="addAdmins()" value=" 增加管理员"> 
									<input name="Submit2" type="button" class="long-long-button" onClick="importAdminTitle('mod_form')" value="用户title导入"> 
						      		<input name="Submit2" type="button" class="long-long-button" onClick="exportAdminTitle()" value="用户title导出"> 
		            			</td>
							</tr>
						</table>
<!-- 		  			</form>  -->
				</div>

				<div id="advan_search">
					<form name="filterForm" action="ct_admin.html" method="post">
						<input type="hidden" id="cmd" name="cmd" value="filter"/>
	          			<table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
	            			<tr>
	            				<td width="40%">
			      					<select name="proAdgid" id="proAdgid">
					  					<option value="0">请选择部门...</option>
										<%DBRow adminGroup[] = adminMgr.getAllAdminGroup(null);
										for (int i=0; i<adminGroup.length; i++){
											if(StringUtil.getString(request,"proAdgid").equals(adminGroup[i].getString("adgid"))){%>
										<option style="background:#fffffffff;" value="<%=adminGroup[i].getString("adgid")%>" selected="selected">
											<%=adminGroup[i].getString("name")%>
										</option>
											<%}else{%>
										<option style="background:#fffffffff;" value="<%=adminGroup[i].getString("adgid")%>">
											<%=adminGroup[i].getString("name")%>
										</option>
										<%}}%>
									</select>
									&nbsp;&nbsp;
									<select name="proJsId" id="proJsId" >
										<%if(StringUtil.getString(request,"proJsId").equals("1")){%>
										<option value="0">请选择职务...</option>
										<option value='1' selected="selected">员工</option>
										<option value='5' >副主管</option>
										<option value='10'>主管</option>
										<%}else if(StringUtil.getString(request,"proJsId").equals("5")){ %>
					 					<option value="0">请选择职务...</option>
										<option value='1' >员工</option>
										<option value='5' selected="selected">副主管</option>
										<option value='10'>主管</option>
					 					<%}else if(StringUtil.getString(request,"proJsId").equals("10")){ %>
									 	<option value="0">请选择职务...</option>
										<option value='1' >员工</option>
										<option value='5' >副主管</option>
										<option value='10' selected="selected">主管</option>
										<% }else{ %>
									 	<option value="0">请选择职务...</option>
										<option value='1'>员工</option>
										<option value='5'>副主管</option>
										<option value='10'>主管</option>
					 					<%}%>
	      							</select>&nbsp;&nbsp;
			      					<select name="proPsId" id="proPsId" >
				  						<option value="0">请选择仓库...</option>
										<%DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
										for (int i=0; i<treeRows.length; i++){
											if(StringUtil.getString(request,"proPsId").equals(treeRows[i].getString("id"))){ %>
										<option style="background:#fffffffff;" value="<%=treeRows[i].getString("id")%>" selected="selected" >
											<%=treeRows[i].getString("title")%>
										</option>
											<% }else{ %>
				    					<option style="background:#fffffffff;" value="<%=treeRows[i].getString("id")%>" >
				    						<%=treeRows[i].getString("title")%>
				    					</option>
				  						<%}}%>
									</select>
									&nbsp;&nbsp;
    	  							<select name="lock_state">
    	  								<option value="-1">请选择锁定状态...</option>
    	  								<% if(2 == lock_state){ %>		
    	  								<option value="2" selected="selected">未锁定</option>
    	  								<option value="1">已锁定</option>
    	  								<% }else if(1 == lock_state){ %>		
						   	  			<option value="2">未锁定</option>
						   	  			<option value="1" selected="selected">已锁定</option>
    	  								<% }else{ %>		
					    	  			<option value="2">未锁定</option>
					    	  			<option value="1">已锁定</option>
    	  								<% } %>	    
    	  							</select>
    	  							&nbsp;&nbsp;
	       						</td>
	              				<td width="45%" height="35" align="left" valign="middle" nowrap="nowrap">
			      					<ul id="locationmenu" class="mcdropdown_menu">
										<li rel="0">请选择办公地点</li>
		  								<%DBRow l1[] = officeLocationMgr.getChildOfficeLocation(0);
										for (int i=0; i<l1.length; i++){
											out.println("<li rel='"+l1[i].get("id",0l)+"'> "+l1[i].getString("office_name"));
					  						DBRow l2[] = officeLocationMgr.getChildOfficeLocation(l1[i].get("id",0l));
					  						if (l2.length>0){
												out.println("<ul>");	
					  						}
					  						for (int ii=0; ii<l2.length; ii++){
												out.println("<li rel='"+l2[ii].get("id",0l)+"'> "+l2[ii].getString("office_name"));		
												DBRow l3[] = officeLocationMgr.getChildOfficeLocation(l2[ii].get("id",0l));
								  				if (l3.length>0){
													out.println("<ul>");	
								  				}
												for (int iii=0; iii<l3.length; iii++){
													out.println("<li rel='"+l3[iii].get("id",0l)+"'> "+l3[iii].getString("office_name"));
													out.println("</li>");
												}
								  				if (l3.length>0) {
													out.println("</ul>");
								  				}
								  
												out.println("</li>");				
					  						}
					  						if (l2.length>0){
												out.println("</ul>");	
					  						}
											out.println("</li>");
			  							}%>
									</ul>
		  							<input type="text" name="location" id="location" value="" />
		  							<input type="hidden" name="filter_lid" id="filter_lid" value="0" />	           
								</td>
	              				<td>
	              					<input type="submit" id="seachByCondition" name="seachByCondition" class="button_long_refresh" value="过    滤" />
									<input name="Submit2" type="button" class="long-long-button" onClick="exportAdminTitle()" value="用户title导出"> 
								</td>
							</tr>
	          			</table>
		  			</form> 
				</div>
  			</div>
			</div>
  		</td>
	</tr>
</table>
<br>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="true" class="zebraTable" isBottom="true">
	<tr> 
		<th width="18%" nowrap="nowrap" class="right-title" style="text-align:center">员工信息</th>
		<th width="25%"  nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">部门/职务/办公地点</th>
		<th width="22%" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">联系信息</th>
		<th width="20%" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">日期信息</th>
		<th width="15%" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">用户文件</th>
	</tr>
<% 
String lockStr;

for ( int i=0; i<rows.length; i++ ){
	if ( rows[i].get("llock",0)==0 ){
		lockStr	= "屏蔽";
	}else{
		lockStr	= "解除";
	}
%>
	<tr align="center">
  		<td height="50" valign="middle">
  			<fieldset  class="set" style="border:2px solid #993300;text-align:left；width:150px;">
		  		<legend>
					<span class="title"><%=rows[i].getString("employe_name")%></span>
				</legend>
				<p>
					<span class="alert-text InfoLeft">编号：</span>
					<span class="InfoRight"><%=rows[i].getString("adid")%></span>
				</p>
				<p>
					<span class="alert-text InfoLeft">登录账号：</span>
					<span class="InfoRight"><%=rows[i].getString("account")%> <%=rows[i].get("llock",0)==0?"&nbsp;":"<img src='../imgs/lockadmin.gif'>"%></span>
				</p>
				<p>
					<span class="alert-text InfoLeft">仓库：</span>
					<span class="InfoRight">
						<%DBRow detailPSC = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("ps_id",0l));
						if (detailPSC!=null){
							out.println(detailPSC.getString("title"));
						}%>
					</span>
				</p>
  			</fieldset>		
  		</td>
    	<td valign="middle"  nowrap="nowrap" >
		<%
			DBRow role;
			DBRow[] relation;
			relation = adminMgrGZY.getAdminGroupRelation(rows[i].get("adid",01));
			for( int j = 0; j<relation.length; j++){
				if(relation.length!=0 && relation!=null){
					role = adminMgr.getDetailAdminGroup(relation[j].get("adgid",01));
					if(role==null){
						role = new DBRow();
					}
					out.println("<div>");
					out.println(role.getString("name"));
					if(relation[j].getString("proJsId").equals("1")){
						out.println("员工");
					} 
					if(relation[j].getString("proJsId").equals("5")){
						out.println("副主管");
					}
					if(relation[j].getString("proJsId").equals("10")){
						out.println("主管");
					}
					DBRow allLocationFather[] = location_tree.getAllFather(relation[j].get("AreaId",0l));
					for (int k=0; k<allLocationFather.length-1; k++){
						out.println("<a class='nine4' href='?cmd=filter&filter_lid="+allLocationFather[k].getString("id")+"'>"+allLocationFather[k].getString("office_name")+"</a>");
					}
				  	DBRow location = officeLocationMgr.getDetailOfficeLocation(StringUtil.getLong(relation[j].getString("AreaId")));
				  	if(null!=location){
				  		out.println("<a href='?cmd=filter&filter_lid="+relation[j].getString("AreaId")+"'>"+location.getString("office_name")+"</a>");
				  	}
				  	out.print("</div>");	
				}else
					out.println("<font color=red>角色已删除</font>");
			}	
		%>   
		</td>
		<td height="50" valign="middle"  nowrap="nowrap" style="padding-top:10px;padding-bottom:10px;">
			<p>
				<span class="alert-text InfoLeft">手机号码:</span>
				<span class="InfoRight"><%=rows[i].getString("mobilePhone") %></span>
			</p>
			<p>
				<span class="alert-text InfoLeft">QQ:</span>
				<span class="InfoRight"><%=rows[i].getString("proQQ") %></span>
			</p>
			<p>
				<span class="alert-text InfoLeft">MSN:</span>
				<span class="InfoRight"><%=rows[i].getString("msn") %></span>
			</p>
			<p>
				<span class="alert-text InfoLeft">SKYPE:</span>
				<span class="InfoRight"><%=rows[i].getString("skype") %></span>
			</p>
			<p>
				<span class="alert-text InfoLeft">Email:</span>
				<span class="InfoRight"><%=rows[i].getString("email") %></span>
			</p>
		</td>
    	<td valign="middle" nowrap="nowrap"> 
	    	<p>
				<span class="alert-text InfoLeft">入职时间:</span>
				<span class="InfoRight"><%=rows[i].getString("Entrytime")%></span>
			</p>
			<p>
				<span class="alert-text InfoLeft">最后登录:</span>
				<span class="InfoRight"><%=rows[i].getString("last_login_date")%></span>
			</p>
			<p>
				<span class="alert-text InfoLeft">令牌ID:</span>
				<span class="InfoRight"><%=rows[i].getString("register_token")%></span>
			</p>
		</td>
		<td>
			<!-- 如果是图片文件那么就是要调用在线预览的图片 -->
			<%if(StringUtil.isPictureFile(rows[i].getString("file_path"))){ %>
			<a href="javascript:void(0)" onclick="showPictrueOnline('<%=rows[i].getString("file_path") %>');" ><%=rows[i].getString("file_path") %></a>
			<%}else{ %>
			<a href='<%= downLoadFileAction%>?file_name=<%=rows[i].getString("file_path") %>&folder=<%= systemConfig.getStringConfigValue("file_path_admin")%>'>
				<%=rows[i].getString("file_path") %>
			</a>
     		<%} %>
		</td>
	</tr>
	<tr class="split">
		<td colspan="5" style="padding-top:1px;text-align:right;">
			<input name="Submit11" type="button" class="short-button"  value="管理title"  
	  			onClick="manageTitle('<%=rows[i].getString("adid")%>', '<%=rows[i].getString("employe_name")%>')" />
	  		<input name="Submit11" type="button" class="short-button"  value="登记令牌"  
	  			onClick="regisToken(<%=rows[i].getString("adid")%>)" />
	   		<input name="Submit3" type="submit" class="short-button" 
	   			onClick="lockAdmin(<%=rows[i].getString("llock")%>,<%=rows[i].getString("adid")%>,'<%=rows[i].getString("account")%>')" value="<%=lockStr%>" />
	        <input name="Submit42" type="button" class="short-short-button-mod"  value=" 修改"  
	        	onClick="modAdmins('<%=rows[i].getString("account")%>','<%=rows[i].getString("adid")%>')" />
	        <input name="Submit4" type="button" class="short-button"  value="扩展权限"  
	        	onClick="modifyOper('<%=rows[i].getString("adgid")%>',<%=rows[i].getString("adid")%>);" />
			<input name="Submit" type="button" class="short-short-button-del" 
				onClick="return delAdmin(<%=rows[i].getString("adid")%>,'<%=rows[i].getString("account")%>')" value=" 删除" />
  		</td>
	</tr>
<%}%>
</table>
<script type="text/javascript">
	function modifyOper(adgid,adid){
		 
		 window.location.href="ct_admin_righs.html?adgid="+adgid+"&adid="+adid+"&backurl=<%=StringUtil.getCurrentURL() %>" ;
	 
	}
</script>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	<form name="dataForm"  action="ct_admin.html"><!-- 只能以get方式提交 -->
		<input type="hidden" name="p"><!-- 点出go按钮或者点击翻页按钮时为此文本框赋值 -->
	    <input type="hidden" name="cmd" value="<%=cmd %>"/>
	    <input type="hidden" name="proAdgid" value="<%=proAdgid %>"/>
	    <input type="hidden" name="proPsId" value="<%=proPsId %>"/>
	    <input type="hidden" name="proJsId" value="<%=proJsId%>"/>
	    <input type="hidden" name="filter_lid" value="<%=filter_lid %>"/>
	    <input type="hidden" name="lock_state" value="<%=lock_state%>"/>
  	</form>
	<tr> 
    	<td align="right" valign="middle" >
      	<%
			int pre = pc.getPageNo() - 1;
			int next = pc.getPageNo() + 1;
			out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " 总数：" + pc.getAllCount() + " ");
			out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
			out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
			out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
			out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
		%>
      	跳转到 
      	<input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
      	<input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" 
      		onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    	</td>
  	</tr>
</table>
<br>
<br>
<script>
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	cookie: { expires: 30000 } ,
});
</script>
</body>
</html>
