<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String action = ConfigBean.getStringValue("systenFolder")+"action/administrator/admin/addAdminGZY.action";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script>
$().ready(function() {
	$("#input_st_date").click(function(){
		$(this).date_input();
	})
});
//弹出dialog窗口增加部门及职务
function addDeptAndRole(){
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/Add_AdminGroup_role.html"; 
    $.artDialog.open(uri , {title: "增加部门/职务/办公地点",width:'550px',height:'220px', lock: true,opacity: 0.3,fixed: true});
};
//显示部门/职务/办公地点
function showValue(group,proAdgid,role,proJsId,filter_lid,location){
	var div = '<div id="divGR'+proAdgid+'" name="divGR"><input type="hidden" id="group" name="group" value="'+proAdgid+'" /><input type="hidden" id="role"  name="role" value="'+proJsId+'" /><input type="hidden" id="filter_lid"  name="filter_lid" value="'+filter_lid+'" />['+group+':'+role+':'+location+']<img alt="删除" src="../imgs/del.gif" onclick="deleteGR('+proAdgid+')" /></div>';
 	$(div).appendTo($("#groupRole"));
};

function showArea(psId){
	
	$.ajax({
  		type:'post',
  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/DynamincDataAjaxAction.action',
  		dataType:'json',
  		data:"psId="+psId,
  		success:function(msg){
  			
  			var str = "<option value='0'>选择区域...</option>";
  			if(msg!=null){
  				for(var i=0;i<msg.length;i++){
  					str += "<option value='"+msg[i].area_id+"'>"+msg[i].area_name+"</option>";
  				}
	  			$("#areaId").html(str);
  			}
  		},
  		error:function(){
			showMessage("系统错误","error");
		}
   	});
}

//删除所选部门及角色
function deleteGR(id){
	$("#divGR"+id).remove();
}

	
	function addAdmin() {
	
		var div = $("div[name='divGR']");			
 
		var ids = "";
		var attr = "" ;
		var adgid = 0;
		$(div).each(
				function(i){				
					 var group = $(this).find("input[name='group']").val();
					 var role = $(this).find("input[name='role']").val();
					 var filter_lid = $(this).find("input[name='filter_lid']").val();
					 var tempStr ;
					   tempStr  = ","+group+"-"+role+"-"+filter_lid;
					   attr+= tempStr;
					   adgid = group;
					  
				});
		 if(attr.length > 1 ){attr = attr.substr(1);}
		
				
		var validateMobileCode = /^0?(13[0-9]|15[012356789]|18[0236789]|14[57])[0-9]{8}$/;
		var validateQQ = /^[1-9]\d{4,8}$/;
		var validateEmail = /^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.){1,4}[a-z]{2,3}$/;
		var theForm = document.add_handle_form;
		
		if(theForm.proAccount.value ==""){
			alert("请填写帐号");
			return (false);
		}else if(theForm.proPwd.value == ""){
			alert("请输入密码");
			return (false);
		}else if(theForm.proConfirmPwd.value == ""){
			alert("请确认密码");
			return (false);
		}else if (theForm.proPsId.value == 0) {
			alert("请选择仓库");
			return (false);
		}else if (theForm.group.value == 0 && theForm.role.value == 0 && theForm.filter_lid.value == 0) {
			alert("请选择部门及职务");
			return (false);
		} /*else if (theForm.proEmployeName.value == "") {
			alert("请填写雇员姓名");
			return (false);
		}else if(theForm.input_st_date.value == ""){
			alert("请填写入职时间");
			return (false);
		}else if(theForm.mobilePhone.value!=""&&(!validateMobileCode.test(theForm.mobilePhone.value))){
			alert("请输入正确的手机号");
			return(false);
		}else if(theForm.proQQ.value!=""&&(!validateQQ.test(theForm.proQQ.value))){
			alert("请填写正确的QQ");
			return (false);
		}else if(theForm.proMSN.value !="" &&(!checkEmail(theForm.proMSN.value))){
			alert("请正确填写MSN");
			return (false);
		} */
		 else if (theForm.proEMail.value !=""&&(!checkEmail(theForm.proEMail.value))){
			alert("请正确填写Email");
			return (false);
		}else if(theForm.proPwd.value!=theForm.proConfirmPwd.value){
			alert("两次输入的密码不一致");
			return (false);
		}else {
		
			parent.document.add_form.action = "<%=action%>";
			parent.document.add_form.pwd.value = theForm.proPwd.value; //密码
			
			parent.document.add_form.employe_name.value = theForm.proEmployeName.value; //雇员姓名
			
			parent.document.add_form.ps_id.value = theForm.proPsId.value; //仓库
			parent.document.add_form.email.value = theForm.proEMail.value;
			parent.document.add_form.skype.value = theForm.proSkype.value;
			parent.document.add_form.QQ.value = theForm.proQQ.value;
			parent.document.add_form.mobile.value = theForm.mobilePhone.value;

			parent.document.add_form.adgid.value = adgid; //部门
			parent.document.add_form.arrGRL.value =attr; //部门/职务/办公地点
			
			parent.document.add_form.msn.value = theForm.proMSN.value;
			parent.document.add_form.account.value = theForm.proAccount.value; //账号
			parent.document.add_form.entrytime.value = theForm.input_st_date.value; //日期
			
			parent.document.add_form.areaId.value = theForm.areaId.value;
			
			parent.document.add_form.submit();
		}		
	}
</script>
<style type="text/css">
<!--

div.mcdropdown {
	width: 250px;
}

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}

form
{
	padding:0px;
	margin:0px;
}

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_quote.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="">

<form name="add_handle_form" id="add_handle_form" method="post" action="" >

  <br>
  <table width="93%" border="0" cellpadding="0" cellspacing="0" style="padding-left:40px;padding-top:20px;">
  <tr>
    <td>
	 <fieldset style="border:2px #cccccc solid;padding:15px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">创建管理员</legend>
	
<table width="100%" border="0" cellspacing="3" cellpadding="2">
  
  <tr> 
      <td width="11%" height="30" align="right" class="STYLE2">登录账号</td>
      <td width="2%">&nbsp;</td>
    <td colspan="2"><input name='proAccount' type='text' id='proAccount' style='width:200px;'></td>
    </tr>
  


    <tr> 
    <td height="30" align="right" class="STYLE2">密码</td>
    <td>&nbsp;</td>
    <td colspan="2">
    	<input name='proPwd' type="password" id='proPwd'  style='width:200px;'>
	</td>
    </tr>
    <tr>
    	<td height="30" align="right" class="STYLE2">确认密码</td>
    	<td>&nbsp;</td>
    	<td colspan="2">
    		<input name='proConfirmPwd' type='password' id='proConfirmPwd'  style='width:200px;'>
		</td>
    </tr>
     <tr> 
      <td width="11%" height="30" align="right" class="STYLE2">仓库</td>
      <td width="2%">&nbsp;</td>
      <td colspan="2">
	  	
      <select name="proPsId" id="proPsId" onChange="showArea(this.value);">
	  <option value="0">选择仓库...</option>
	  <%
	DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
	for (int i=0; i<treeRows.length; i++)	
	{
	   
	if ( treeRows[i].get("parentid",0) > 0 )
	 {
	 	continue;
	 }
	  %>
	    <option style="background:#fffffffff;" value="<%=treeRows[i].getString("id")%>" ><%=treeRows[i].getString("title")%></option>
	  <%
	  	}
	  %>
      </select>
	  </td>
  	</tr>
  	
  	<tr> 
      <td width="11%" height="30" align="right" class="STYLE2">工作区域</td>
      <td width="2%">&nbsp;</td>
		<td colspan="2">
			<select name="areaId" id="areaId" >
				<option value="0">选择区域...</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td width="20%" height="30" align="right" class="STYLE2">部门/职务/办公地点</td>
		<td>&nbsp;</td>
    	<td colspan="2">
    		 <input name="btn1" type="button" class="long-button-add" onclick="addDeptAndRole()" value=" 增加">
    	</td>
	</tr>
  	<tr>
  		<td width="12%" colspan="2"></td>
  		<td id="groupRole" name="groupRole" ></td>
  	</tr>
 
   <tr>
    <td height="30" align="right" class="STYLE2">雇员姓名<br></td>
    <td>&nbsp;</td>
    <td><input name='proEmployeName' type='text' id='proEmployeName'  size="40" ></td>
    </tr>
   
   
    <tr>
    <td height="30" align="right" class="STYLE2">入职时间<br></td>
    <td>&nbsp;</td>
    <td><input name="input_st_date" type="text" id="input_st_date" size="40"  readonly="readonly"></td>
    </tr>
    
  <tr> 
    <td height="30" align="right" class="STYLE2">SKYPE</td>
    <td>&nbsp;</td>
    <td><input name="proSkype" type="text" id="proSkype" size="40"  ></td>
    </tr>


	<tr>
      <td height="30" align="right"  class="STYLE2">手机号码</td>
      <td>&nbsp;</td>
      <td><input name="mobilePhone" type="text" id="mobilePhone" size="40"  "></td>
      </tr>
    <tr>
      <td height="30" align="right"  class="STYLE2">QQ</td>
      <td>&nbsp;</td>
      <td><input name="proQQ" type="text" id="proQQ" size="40"  " ></td>
      </tr>
      
      <tr>
      <td height="30" align="right"  class="STYLE2">MSN</td>
      <td>&nbsp;</td>
      <td><input name="proMSN" type="text" id="proMSN" size="40"  " ></td>
      </tr>
      
       <tr>
      <td height="30" align="right"  class="STYLE2">内部邮箱</td>
      <td>&nbsp;</td>
      <td><input name="proEMail" type="text" id="proEMail" size="40"  " ></td>
      </tr>
</table>
	</fieldset>
	</td>
  </tr>

</table>

<br>
<br>
<br>
<table width="93%" border="0" cellspacing="0" cellpadding="0"  style="height:80px;overflow:auto;">
  <tr>
    <td width="21%">&nbsp;</td>
    <td>	
	<input name="Submit22" type="button" onClick="addAdmin()" class="normal-green" value="提交">
	 </td>
    <td width="17%" align="left" valign="middle">
           <input name="Submit2" type="button" class="normal-white" value="取消" onClick="closeWindow();">
     </td>
  </tr>
</table>
</form>
<script type="text/javascript">
function closeWindow(){
	$.artDialog.close();
}
</script>
</body>
</html>
