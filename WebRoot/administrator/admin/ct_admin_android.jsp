<%@page import="com.cwc.app.key.ControlTypeKey"%>
<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="com.cwc.authentication.AdminAuthenCenter"%>
<%@ include file="../../include.jsp"%> 
<%
long adgid = StringUtil.getLong(request,"adgid");
long adid = StringUtil.getLong(request,"adid");

DBRow treeRows[] = adminMgr.getControlTreeAllByControlTypeSort(ControlTypeKey.Left);
DBRow detailAdmin = adminMgr.getDetailAdmin(adid);
String backurl = StringUtil.getString(request,"backurl");


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript">		
function grantRolePageActionRights()
{
	document.rights_form.submit();
}


</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

form
{
	padding:0px;
	margin:0px;
}
-->
</style></head>

<body>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 系统管理 » 扩展权限管理 </td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="13%" align="left" valign="middle"><img src="../imgs/account.gif" alt="管理员"  align="absmiddle"> &nbsp;&nbsp; <span style="font-size:13px;font-weight:bold;color:#FF0000"><%=detailAdmin.getString("account")%></span>&nbsp;&nbsp;&nbsp;<span style="color:#999999"></span></td>
  </tr>
</table>
<br>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/admin/grantAdminPageActionRights.action" method="post" name="rights_form">
<input type="hidden" name="adid" value="<%=adid%>">
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0"  class="thetable">
  <thead>
  <tr> 
      <td width="62%" class="left-title">&nbsp;</td>
    </tr>
  </thead>
  <%
String className;
DBRow actions[];
for ( int i=0; i<treeRows.length; i++ )
{

	if ( i%2==0 )
	{
		className = "row-line1";
	}
	else
	{
		className = "row-line2";
	}

	
	actions = adminMgr.getActionsByPage(null,treeRows[i].get("id",0l));
%>
 <tr > 
    <td height="60"  class="<%=className%>" style="padding-top:10px;"> 
      <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0)*6)%>
	  <%
	  if (treeRows[i].get("parentid",0)==0)
	  {
	  %>
	 <img src="../imgs/expand_tag.gif" width="9" height="9" align="absmiddle"> 
	 <%
	 }
	 else
	 {
	 %>
	 <img src="../imgs/page.gif" width="14" height="16" align="absmiddle">
	 <%
	 }
	 %>
	  &nbsp;&nbsp;
	  <label for="ctid<%=treeRows[i].getString("id")%>">
    <span style="font-size:15px;<%=AdminAuthenCenter.havePageRoleRights(treeRows[i].getString("link"),adgid)?"font-weight:bold":""%>"><%=treeRows[i].getString("title")%></span>
	</label>

	<%
	if ( AdminAuthenCenter.havePageRoleRights(treeRows[i].getString("link"),adgid) )
	{
		out.println("<input name='ctid' type='checkbox' id='ctid"+treeRows[i].getString("id")+"' value='"+treeRows[i].getString("id")+"' checked style='border:#FF0000 4px sold' disabled='disabled'>");
	}
	else if ( AdminAuthenCenter.havePageAdminRights(treeRows[i].getString("link"),adid) )
	{
		out.println("<input name='admin_ctid' type='checkbox' id='ctid"+treeRows[i].getString("id")+"' value='"+treeRows[i].getString("id")+"' checked style='border:#00FF00  4px sold' >");
	}
	else
	{
		out.println("<input name='admin_ctid' type='checkbox' id='ctid"+treeRows[i].getString("id")+"' value='"+treeRows[i].getString("id")+"'  >");
	}
	%>

    


	<%
	if (actions.length>0)
	{
	%>
	<div style="margin-bottom:18px;margin-top:10px;color:#990000;">
	
		<%
	for (int j=0; j<actions.length; j++)
	{
	%>
	<div style="margin:15px;">
	<%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0)*9)%>
	 <img src="../imgs/action.gif" width="16" height="16" align="absmiddle">
	 <label for="ataid<%=actions[j].getString("ataid")%>"><%=actions[j].getString("description")%></label> 
	 	 
	 	<%
	if ( AdminAuthenCenter.haveActionRoleRights(actions[j].getString("action"),adgid) )
	{
		out.println("<input name='ataid' type='checkbox' id='ataid"+actions[j].getString("ataid")+"' value='"+actions[j].getString("ataid")+"' checked  style='background:#FF0000'  disabled='disabled'>");
	}
	else if ( AdminAuthenCenter.haveActionAdminRights(actions[j].getString("action"),adid) )
	{
		out.println("<input name='admin_ataid' type='checkbox' id='ataid"+actions[j].getString("ataid")+"' value='"+actions[j].getString("ataid")+"' checked  style='background:#00FF00' >");
	}
	else
	{
		out.println("<input name='admin_ataid' type='checkbox' id='ataid"+actions[j].getString("ataid")+"' value='"+actions[j].getString("ataid")+"'  >");
	}
	%>
	 
	 
	    </div>
	<%
	}
	%>
	</div>
	<%
	}
	%>
	</td>
  </tr>
  <%
}
%>
</table>
</form>
<br>
<br>
<table width="98%" height="35" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="middle"><label>
      <input name="Submit" type="button" class="long-button-ok" value="保存设置" onClick="grantRolePageActionRights()">
    </label>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input name="Submit2" type="button" class="long-button" value="返回" onClick="location='<%=backurl%>'"></td>
  </tr>
</table>
<br>
<br>
</body>
</html>
