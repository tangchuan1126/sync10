<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
String asset_id = StringUtil.getString(request,"asset_id");
String imei = StringUtil.getString(request,"imei");
String asset_name = StringUtil.getString(request,"asset_name");
String type = StringUtil.getString(request,"type");

String geoType = "";
String geoName = "";
if("geoFencing".equals(type)){
	geoType = "3,4,5";
	geoName = "Geofencing";
}else if("geoLine".equals(type)){
	geoType = "2";
	geoName = "Route";
}else if("geoPoint".equals(type)){
	geoType = "1";
	geoName = "Key Point";
}

DBRow row = new DBRow();
row.add("geo_type",geoType);
row.add("asset_id",asset_id);
row.add("search","exist");

String[] pn = StringUtil.getString(request,"p").split(",");
int pn1 = Integer.parseInt(pn[0]);
int pn2 = Integer.parseInt(pn[1]);

PageCtrl pc1 = new PageCtrl();
pc1.setPageNo(pn1);
pc1.setPageSize(36);
DBRow[] existAlarm = googleMapsMgrCc.getAssetAlarm(row,pc1);

row.add("search","notExist");
PageCtrl pc2 = new PageCtrl();
pc2.setPageNo(pn2);
pc2.setPageSize(36);
DBRow[] notExistAlarm = googleMapsMgrCc.getAssetAlarm(row,pc2);
%>
<html>
  <head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
.text-overflow {
	display:block;
	word-break:keep-all;
	white-space:nowrap;
	overflow:hidden;
	text-overflow:ellipsis;
}
</style>
  </head>
  <body>
  	<div align="left" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	    <table style="font-weight:bold; font-size:16px; color:#00F">
	    	<tr>
	    		<td style="text-align: right">NAME：</td>
	    		<td><%=asset_name %></td>
	    	</tr>
	    	<tr style="text-align: left">
	    		<td style="text-align: right">IMEI：</td>
	    		<td><%=imei %></td>
	    	</tr>
	    </table>
	</div>
	<br/>
  	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	    <table style="width: 100%;">
	    	<tr style=" font-weight:bold; font-size:16px; color:#00F;">
	    		<td style="width: 49%;">&nbsp;Existing <%=geoName %></td>
	    		<td style="width: 2%;"></td>
	    		<td style="width: 49%;">&nbsp;Free <%=geoName %></td>
	    	</tr>
	    	<tr>
	    		<td>
					<div id="exitAlarm" style="background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto" >
						<%
						if(existAlarm != null && existAlarm.length>0){
							for(int i=0; i<existAlarm.length; i++){
								DBRow alarm = existAlarm[i];
								int alarmType = alarm.get("alarm_type",0);
								%>
								<div title="<%=alarm.getString("name") %>" style="width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
									<div class="text-overflow"  style="width: 85%; float: left; " >
										<%
										if(alarmType == 4){
											out.println("<span style='color: blue;'>[I]</span>");
										}
										if(alarmType == 5){
											out.println("<span style='color: green;'>[O]</span>");
										}
										if(alarmType == 6){
											out.println("<span style='color: red;'>[B]</span>");
										}
										%>
										<%=alarm.get("name","") %>
									</div>
									<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/delete_grey.png'); margin: 1px; cursor: pointer;" 
											onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/delete_blue.png&quot;)'" 
											onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/delete_grey.png&quot;)'"
											onclick="removeAlarm(<%=alarm.getString("asset_alarm_id") %>)">
									</div>
								</div>
								<%
							}
						}else{
							%>
							<center>
								No Existing <%=geoName %>...
							</center>
							<%
						}
						%>
					</div>
					<div align="center">
			        <%
						int pre1 = pc1.getPageNo() - 1;
						int next1 = pc1.getPageNo() + 1;
						int pre2 = pc2.getPageNo() - 1;
						int next2 = pc2.getPageNo() + 1;
						out.println("Page：" + pc1.getPageNo() + "/" + pc1.getPageCount() + " &nbsp;&nbsp;Total：" + pc1.getAllCount() + " &nbsp;&nbsp;");
						out.println(HtmlUtil.aStyleLink("gop","First","javascript:go('1,"+pc2.getPageNo()+"')",null,pc1.isFirst()));
						out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go('" + pre1 + ","+ pc2.getPageNo() + "')",null,pc1.isFornt()));
						out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go('" + next1 + ","+ pc2.getPageNo() +"')",null,pc1.isNext()));
						out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go('" + pc1.getPageCount() + ","+pc2.getPageNo()+"')",null,pc1.isLast()));
					%>
				      <input name="jump_p1" type="text" id="jump_p1" style="width:28px;" value="<%=pc1.getPageNo() %>"/>
				      <input name="Submit11" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p1').value+',<%=pc2.getPageNo() %>')" value="GO"/>
					</div>
				</td>
				<td></td>
	    		<td>
					<div id="notExitAlarm" style="background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto" >
					<%
						if(notExistAlarm != null && notExistAlarm.length>0){
							for(int i=0; i<notExistAlarm.length; i++){
								DBRow alarm = notExistAlarm[i];
								%>
								<div title="<%=alarm.getString("name") %>" style="width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
									<div class="text-overflow" style="width: 85%; float: left; " >
										<%=alarm.get("name","") %>
									</div>
									<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_grey.png'); margin: 1px; cursor: pointer;" 
												onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'" 
												onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'"
												onclick="addAlarm(<%=alarm.getString("id") %>,<%=alarm.getString("geotype") %>)">
									</div>
								</div>
								<%
							}
						}else{
							%>
							<center>
								No Free <%=geoName %>...
							</center>
							<%
						}
					%>
					</div>
					<div align="center">
			        <%
						out.println("Page：" + pc2.getPageNo() + "/" + pc2.getPageCount() + " &nbsp;&nbsp;Total：" + pc2.getAllCount() + " &nbsp;&nbsp;");
						out.println(HtmlUtil.aStyleLink("gop","First","javascript:go('"+pc1.getPageNo()+",1')",null,pc2.isFirst()));
						out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go('" + pc1.getPageNo() + ","+ pre2 + "')",null,pc2.isFornt()));
						out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go('" + pc1.getPageNo() + ","+ next2 +"')",null,pc2.isNext()));
						out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go('" + pc1.getPageNo() + ","+pc2.getPageCount()+"')",null,pc2.isLast()));
					%>
				      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc2.getPageNo() %>"/>
				      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go('<%=pc1.getPageNo() %>,'+document.getElementById('jump_p2').value)" value="GO"/>
					</div>
				</td>
	    	</tr>
	    </table>
	</div>
	<form name="dataForm" action="">
	    <input type="hidden" name="p"/>
	    <input type="hidden" name="asset_id" value="<%=asset_id %>"/>
	    <input type="hidden" name="imei" value='<%=imei %>'/>
	    <input type="hidden" name="asset_name" value="<%=asset_name %>"/>
	    <input type="hidden" name="type" value="<%=type %>"/>
	</form>
	<form id="refreshPage">
		<input type="hidden" id="asset_id" name="asset_id" value="<%=asset_id %>"/>
		<input type="hidden" id="imei" name="imei" value="<%=imei %>"/>
		<input type="hidden" id="asset_name" name="asset_name" value="<%=asset_name %>"/>
		<input type="hidden" id="type" name="type" value="<%=type %>"/>
		<input type="hidden" id="p" name="p"/>
	</form>
  </body>
<script type="text/javascript">
function removeAlarm(assetAlarmId){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/RemoveAssetAlarmAction.action',
		data:'id='+assetAlarmId,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    },
		success:function(data){
			if(data && data.flag == "true"){
				refresh();
			}
		},
		error:function(){
		}
	});
}

function addAlarm(alarmId,type){
	var alarmType = type;
	if(type==2){  //线路2对应越线报警3   关键点1对应关键点报警1
		alarmType = 3;
	}
	if("geoFencing"=="<%=type %>"){  //围栏需要选择报警类型
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/select_alarm_type.html?alarm_id='+alarmId;
		$.artDialog.open(url, {title: "Alarm Type",width:'280px',height:'180px', lock: false,opacity: 0.3});
		return;
	}
	addAlarmAjax(alarmId,alarmType);
}
function addAlarmAjax(alarmId,alarmType){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/AddAssetAlarmAction.action',
		data:'asset_id='+<%=asset_id %>+"&alarm_id="+alarmId+"&alarm_type="+alarmType,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    },
		success:function(data){
			if(data && data.flag == "true"){
				refresh();
			}
		},
		error:function(){
		}
	});
}
function refresh(){
	var p = "";
	var pCount1 = <%=pc1.getPageCount() %>;
	var pn1 = <%=pc1.getPageNo() %>;
	var count1 = <%=pc1.getAllCount() %>;
	var size1 = <%=pc1.getPageSize() %>;
	if(pCount1 <= 1){
		p = "1";
	}else{
		if(pn1 < pCount1){
			p = pn1;
		}else{
			var c = count1 - (pCount1 - 1)*size1;
			if(c == 1){
				p = pn1-1;
			}
		}
	}
	var pCount2 = <%=pc2.getPageCount() %>;
	var pn2 = <%=pc2.getPageNo() %>;
	var count2 = <%=pc2.getAllCount() %>;
	var size2 = <%=pc2.getPageSize() %>;
	if(pCount2 <= 1){
		p += ",1";
	}else{
		if(pn2 < pCount2){
			p += ","+pn2;
		}else{
			var c = count2 - (pCount2 - 1)*size2;
			if(c == 1){
				p += ","+(pn2-1);
			}else{
				p += ","+pn2;
			}
		}
	}
	$("#p").val(p);
	document.forms.refreshPage.submit();
}
</script>
</html>