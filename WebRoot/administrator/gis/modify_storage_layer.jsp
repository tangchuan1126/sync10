<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*"%>
<%@page import="com.cwc.util.*,com.cwc.app.util.*"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.util.StringUtil"%>
<%
	String systenFolder = ConfigBean.getStringValue("systenFolder");
	String lat =StringUtil.getString(request,"lat","");
	String lng =StringUtil.getString(request,"lng","");
	String psId = StringUtil.getString(request, "psId");
	String storage_name = StringUtil.getString(request, "storage_name");
%>
<head>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript"
	src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript"
	src="../js/scrollto/jquery.scrollTo-min.js"></script>


<script type="text/javascript"
	src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css"
	type="text/css" />

<link rel="stylesheet"
	href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css"
	type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<style type="text/css">
td.topBorder {
	border-top: 1px solid silver;
}
</style>
<style type="text/css">
.ui-datepicker {
	font-size: 0.9em;
}
</style>
<script src="../js/maps/verify.js"></script>
<script type="text/javascript">
var systenFolder = "<%=systenFolder%>";
</script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script type="text/javascript">
$(function () { 
	//添加button
	var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	var updateDialog = null;
	var addDialog = null;
	api.button([
			{
				name: 'Submit',
				callback: function () {
					commitData('<%=psId%>');
					return false;
				},
				focus:true,
				focus:"default"
			},
			{
				name: 'Cancel',
				callback:function (){
					closeWindow();
					return false;
				},
			}] 
		);
	
	if("<%=lat%>"!="" && "<%=lng%>"!=""){
		convertCoordinateAjax(<%=psId%>,"<%=lat%>","<%=lng%>");
	}
	if("<%=psId%>"!=""){
		$("#storage").val("<%=psId%>");
	}
		$("#_title").show();
		$("#_dock").show();
		
});


function convertCoordinateAjax(psId ,lat,lng){
	$.ajax({
		url:<%=ConfigBean.getStringValue("systenFolder")%>+'action/administrator/gis/convertCoordinate.action',
		data:'psId='+psId+'&lat='+lat+'&lng='+lng,
		dataType:'json',
		type:'post',
		async:true, 
		success:function(data){
			$.unblockUI();
	    	if(data&&data.flag=="true"){
	        var x =data.x;
	        var y =data.y;
	    	$("#x").val(x);
	    	$("#y").val(y);
	    	}
		},
		error:function(){
			showMessage("System error!!","error");
		}
	});
	}
function closeWindow(){
	$.artDialog.close();
	$.artDialog.opener.jsmap.clearDemoLayer();
}
function fillValue(x,y){
	$("#width").val(x);
	$("#height").val(y);
}
function fillAngle(angle){
	$("#angle").val(angle);
}
function commitData(psId){
	var type=$("input[name='type']:checked").val();
	var layers="area";
	var aotulocations=[];
	var child_type=""
	var checkbox =$("input[name='set_local']").attr("checked");
	if(type==5 &&checkbox){
		aotulocations=$.artDialog.opener.getAotuLocations();
	    child_type=$("input[name='child_type']:checked").val();
		var layer="";
		if(child_type==1){
			layer=",location";
		}
		if(child_type==2){
			layer=",staging";
		}
		if(child_type==3){
			layer=",docks";
		}
		if(child_type==4){
			layer=",parking";
		}
		layers+=layer;
	    
	}
	if(type==1||type==3|type==4){
		if(!$("#zone").val()){
			$("#zone_con").hide();
			$("#zone_del").show();
			$("#zone_del").attr('title','Choose zone please!');
			verifyMgs['v_zone']=0;
			/* showMessage("Choose zone please!","error");
			return ; */
		}else{
			$("#zone_con").show();
			$("#zone_del").hide();
		}
	}else if(type ==2){
		if(!$("#dock").val()){
			$("#dock_con").hide();
			$("#dock_del").show();
			$("#dock_del").attr('title','Choose dock please!');
			verifyMgs['v_dock']=0;
			/* showMessage("Choose dock please!","error");
			return ; */
		}else{
			$("#dock_con").show();
			$("#dock_del").hide();
		}
	}else{
		verifyMgs['v_dock']=1;
	}
	var name =$("#name").val();
	if($.isEmptyObject(name)){
		verifyName();
		/* showMessage("name cannot be empty!","error");
		return ; */
	}
	if(name.indexOf("_")>-1){
		$("#name_con").hide();
		$("#name_del").show();
		$("#name_del").attr("title","name not contain '_' !");
		verifyMgs['verifyName']=0;
		/* showMessage("name not contain '_' !","error");
		return ; */
	}
	if(verifyMgs['verifyName']==0 || verifyMgs['verifyName']==2 ){
		$("#name").focus();
		return;
	}
	if(verifyMgs['verifyX']==0 || verifyMgs['verifyX']==2){
		$("#x").focus();
		return;
	}
	if(verifyMgs['verifyY']==0 ||verifyMgs['verifyY']==2){
		$("#y").focus();
		return;
	}
	if(verifyMgs['verifyHeight']==0 || verifyMgs['verifyHeight']==2){
		$("#height").focus();
	}
	if(verifyMgs['verifyWidth']==0 ||verifyMgs['verifyWidth']==2){
		$("#width").focus();
		return;
	}
	if(verifyMgs['verifyAngle']==0 || verifyMgs['verifyAngle']==2){
		$("#angle").focus();
		return;
	}
	if(type==1||type==3|type==4){
		if(!$("#zone").val()){
			if(verifyMgs['v_zone']==0){
				return;		
			}
		}
			
		
	}else if(type ==2){
		if(verifyMgs['v_dock']==0){
			return;
		}
	}
	if(type!=5){
		var layer="";
		if(type==1){
			
			layer=",location";
		}
		if(type==2){
			layer=",staging";
		}
		if(type==3){
			layer=",docks";
		}
		if(type==4){
			layer=",parking";
		}
		layers+=layer;
	}
	
	var data = $("#storage_layer").serialize()+"&ps_id=<%=psId%>&location_type="+type+"&storage_name=<%=storage_name%>"+"&aotulocations="+aotulocations+"&child_type="+child_type;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/modifyStorageLayer.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data: data,
		cache:false,
		async:false, 
		beforeSend:function(request){
		 	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success: function(data){
			$.unblockUI();
			if(data && data.flag=="true"){
				showMessage("Save succeed","succeed");
				$.artDialog.opener.reloadLayer(psId,layers);
				closeWindow();
			}else if(data && data.flag=="false"){ 
				if(data.repeat=="true"){
				showMessage("Name can not repeat!","error");
				}else if(data.repeat=="AotuTrue"){
				showMessage("AutolocationName can not repeat!","error");
				}
		 	}else if(data && data.flag=="authError"){
		 		showMessage("Insufficient permissions!","error");	
		 	}
			
		},
		error:function(){
				$.unblockUI();
				$("#errInfo").html("Sava failed!");
			}
	});
}
//选择title
 function selectTitle(){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_title.html';
	$.artDialog.open(url, {title: "Select Title ["+name+"]",width:'825px',height:'435px', lock: true,opacity: 0.3});
}
 var focusTime=1;
 function focusSelectTitle(){
 	if(focusTime==1){
 		focusTime++;
 		selectTitle();
 	}
 }
//选择door
function selectDoor(){
	    var type=$("input[name='type']:checked").val();
		var psId=$("#storage").val();
		var name=$("#name").val()?$("#name").val():"";
	if(type==5){
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_door.html?ps_id='+psId;
		$.artDialog.open(url, {title: "Select Dock ["+name+"]",width:'825px',height:'435px', lock: true,opacity: 0.3});
	}
	if(type==2){
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_door_single.html?ps_id='+psId;
		$.artDialog.open(url, {title: "Select Dock ["+name+"]",width:'790px',height:'425px', lock: true,opacity: 0.3});	
	}
}

//选择单个area
function selectAreaSingle(){
	 var type=$("input[name='type']:checked").val();
	var _type="";
	if(type==1){
		_type=1;
	}
	if(type==4){
		_type=3;
	}
	if(type==3){
		_type=2;
	}
	var psId=$("#storage").val();
	var name=$("#name").val()?$("#name").val():"";
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_area_single.html?ps_id='+psId+'&area_type='+_type;
	$.artDialog.open(url, {title: "Select Area ["+name+"]",width:'790px',height:'425px', lock: true,opacity: 0.3});
}

//回填title
function backFillTitle(names, ids){
	$("#title").val(names);
	$("#title_id").val(ids);
	$("#title").focus();
}
function backFillArea(names, ids){
	$("#zone").val(names);
	$("#area_id").val(ids);
}
function backFillDoor(names, ids){
	$("#dock").val(names);
	$("#sd_id").val(ids);
}
function typeChange(type){
	var name=$('#name').val();
	var psId='<%=psId%>';
	verifyName(psId,name,type);
	if(type=="1"||type=="3"||type=="4"){
		$("#_zone").show();
		$("#_title").hide();
		$("#_dock").hide();
		$("#set_l").hide();
	}
	if(type=="2"){
		$("#_zone").hide();
		$("#_title").hide();
		$("#_dock").show();
		$("#set_l").hide();
	}
	if(type=="5"){
		$("#dock_con").hide();
		$("#dock_del").hide();
		$("#_zone").hide();
		$("#_title").show();
		$("#_dock").show();
		$("#set_l").show();
		
		
	}
	if(type=="-1"){
		$("#_zone").hide();
		$("#_title").hide();
		$("#_dock").hide();
		$("#set_l").hide();
	}
}

function setLocation(){
	 $("#set_location").slideToggle("slow");
}


function createLocation(){
	var x =$("#x").val();
	var y =$("#y").val();
	var x_length =$("#height").val();
	var y_length =$("#width").val();
	var angle =$("#angle").val();
	var h=$("#horizontally").val();
	var z=$("#vertically").val();
	var psId=$("#storage").val();
	var loc_name=$("#loc_name").val();
	var v_interval=$("#v_interval").val();
	var h_interval=$("#h_interval").val();
	var verify =/^(-?\d+)(\.\d+)?$/;
	
   if($.isEmptyObject(loc_name)){
	   verifyLocName();
   }

   if($.isEmptyObject(z)){
	   verifyVer();
   }

   if(verifyMgs['verifyX']==0 || verifyMgs['verifyX']==2 || verifyMgs['verifyY']==0 ||verifyMgs['verifyY']==2
		   || verifyMgs['verifyWidth']==0 || verifyMgs['verifyWidth']==2 || verifyMgs['verifyHeight']==0 || verifyMgs['verifyHeight']==2
		   || verifyMgs['verifyAngle']==0 || verifyMgs['verifyAngle']==2 || verifyMgs['verifyVer']==0 || verifyMgs['verifyVer']==2 
		   || verifyMgs['verifyHor']==0 || verifyMgs['verifyHor']==2 || verifyMgs['verifyVi']==0 || verifyMgs['verifyVi']==2
		   || verifyMgs['verifyHi']==0 || verifyMgs['verifyHi']==2 || verifyMgs['verifyLocName']==0 || verifyMgs['verifyLocName']==2){
	   return;
   }
   $.artDialog.opener.autoCreatLocaiton(psId,x,y,x_length,y_length,angle,h,z,loc_name,h_interval,v_interval);
	
}
var verifyMgs=[];
function verifyName (psId,name){
	var vMsg="";
	if(!name){
		vMsg="Name is Empty!";
	
	}
	  var type=$("input[name='type']:checked").val();
	  var data ={"ps_id":psId,"name":name,"type":type};
	  var url =systenFolder+'action/administrator/gis/verifyName.action';
	  verifyMgs['verifyName']=verify('name','name_con','name_del','',url,data,vMsg);
	
}
function verifyX(){
	var vMsg="";
	if($("#x").val()==''){
		vMsg="X is Empty!";
	}else{
		vMsg='Data format error!';
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs['verifyX']=verify('x','x_con','x_del',reg,'','',vMsg);
}
function verifyY(){
	var vMsg="";
	if($("#y").val()==""){
		vMsg="Y is Empty!";
	}else{
		vMsg='Data format error!';
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs['verifyY']= verify('y','y_con','y_del',reg,'','',vMsg);
}
function verifyWidth(){
	var vMsg="";
	if($("#width").val()==''){
		vMsg="Width is Empty!";
	}else{
		vMsg='Data format error!';
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs['verifyWidth']=verify('width','width_con','width_del',reg,'','',vMsg);
}
function verifyHeight(){
	var vMsg="";
	if($("#height").val()==''){
		vMsg="Height is Empty!";
	}else{
		vMsg='Data format error!';
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs['verifyHeight']=verify('height','height_con','height_del',reg,'','',vMsg);
}
function verifyAngle(){
	var vMsg="";
	if($("#angle").val()==''){
		vMsg="Angle is Empty!";
	}else{
		vMsg='Data format error!';
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs['verifyAngle']=verify('angle','angle_con','angle_del',reg,'','',vMsg); 
}
function verifyVer(){
	var vMsg="";
	if($("#vertically").val()==''){
		vMsg="Vertically is Empty!";
	}else{
		vMsg='Data format error!';
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs['verifyVer']= verify('vertically','v_con','v_del',reg,'','',vMsg); 
}
function verifyHor(){
	var vMsg="";
	if($("#horizontally").val()==''){
		vMsg="Horizontally is Empty!";
	}else{
		vMsg='Data format error!';
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs['verifyHor']=verify('horizontally','h_con','h_del',reg,'','',vMsg); 
}
function verifyVi(){
	var vMsg="";
	if($("#v_interval").val()==''){
		vMsg="V_interval is Empty!";
	}else{
		vMsg='Data format error!';
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs['verifyVi']=verify('v_interval','vi_con','vi_del',reg,'','',vMsg); 
}
function verifyHi(){
	var vMsg="";
	if($("#h_interval").val()==''){
		vMsg="H_interval is Empty!";
	}else{
		vMsg='Data format error!';
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs['verifyHi']= verify('h_interval','hi_con','hi_del',reg,'','',vMsg); 
}
function verifyLocName(psId,name){
	var vMsg="";
	  if(!name){
		  vMsg="LocName is Empty!";
	  }
		  var type=1;
		  var data ={"ps_id":psId,"name":name,"type":type};
		  var url =systenFolder+'action/administrator/gis/verifyName.action';
		  verifyMgs['verifyLocName']=verify('loc_name','locname_con','locname_del','',url,data,vMsg);

}

function clearLocation(){
	$.artDialog.opener.clearAutoLocation();
}
</script>
</head>
<body>
	<form id="storage_layer">
	<input type="hidden" id ="storage" value="<%=psId %>">
		<table width="100%"  border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td align="center" valign="top" colspan="2">
						<table>
							<tr>
								<td align="right" width="30%" >Name：</td>
								<td align="left" height="70%">
									<input type="text" style="width: 80%;" name="name" id="name" value="" onchange="this.value=this.value.trim(),verifyName('<%=psId%>',this.value)" />
									<img id="name_con" src="../imgs/maps/confirm.jpg" style="display: none;">
					            	<img id="name_del" title="Name Is Repeat!" src="../imgs/maps/delete_red.jpg" style="display: none;">
								</td>
								
							</tr>
							<tr>
							<td align="right">
							Type:
							</td>
								<td  > 
							 <input type="radio" width="20%" name="type" value="1" onclick="typeChange(1)">Location
							 <input type="radio" width="20%" name="type" value="2" onclick="typeChange(2)">Staging
								</td>
							</tr>
							<tr>
							<td></td>
							<td colspan="1" align="left">
							 <input type="radio" width="20%" name="type" value="3" onclick="typeChange(3)">Dock
							 <input type="radio" width="20%" name="type" value="4" onclick="typeChange(4)">Parking
							 <input type="radio" width="20%" name="type" value="5" checked="checked" onclick="typeChange(5)">Zone
							</td>
							</tr>
							<tr>
								<td align="right" >X Position：</td>
								<td align="left"><input type="text"
									style="width: 80%;" name="x" id="x"
									value=""  onchange="this.value=this.value.trim(),verifyX()" />
							   	  <img id="x_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="x_del" title="Data Error" src="../imgs/maps/delete_red.jpg" style="display: none">
								</td>
								
							</tr>
							<tr>
								<td align="right" >Y Position：</td>
								<td align="left" ><input type="text"
									style="width: 80%;" name="y" id="y"
									value=""  onchange="this.value=this.value.trim(),verifyY()"/>
								  <img id="y_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="y_del"  title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
								</td>
							</tr>
							<tr>
								<td align="right" >X length：</td>
								<td align="left" ><input type="text"
									style="width: 80%;" name="height" id="height"
									value="10"  onchange="this.value=this.value.trim(),verifyHeight()"/>
								  <img id="height_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="height_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"> 
								</td>
							</tr>
							<tr>
								<td align="right" >Y length：</td>
								<td align="left" ><input type="text"
									style="width: 80%;" name="width" id="width"
									value="100"  onchange="this.value=this.value.trim(),verifyWidth()"/>
								  <img id="width_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="width_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
								</td>
							</tr>
							<tr>
								<td align="right" >Offset angle：</td>
								<td align="left" ><input type="text"
									style="width: 80%;" name="angle" id="angle"
									value="0"  onchange="this.value=this.value.trim(),verifyAngle()"/>
								  <img id="angle_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="angle_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"> 
								</td>
							</tr>
							<tr id="_title" style="display: none">
								<td align="right" >Title：</td>
								<td align="left" >
								<input type="text"
									style="width: 80%;" name="title" id="title"
									value="" onfocus="focusSelectTitle()" onclick="selectTitle()"/>
								<input type="hidden"
									style="width: 80%;" name="title_id" id="title_id"
									value="" />
								</td>
							</tr>
							<tr id="_zone" style="display: none">
								<td align="right" >Zone：</td>
								<td align="left" >
								<input type="text"
									style="width: 80%;" name="zone" id="zone"
									value="" onfocus="selectAreaSingle()" />
								<img id="zone_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="zone_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
								<input type="hidden"
									style="width: 80%;" name="area_id" id="area_id"
									value=""/>
								</td>
							</tr>
							<tr id="_dock" style="display: none">
								<td align="right" >Dock：</td>
								<td align="left" >
								<input type="text"
									style="width: 80%;" name="dock" id="dock"
									value="" onfocus="selectDoor()" />
								<img id="dock_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="dock_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
								<input type="hidden"
									style="width: 80%;" name="sd_id" id="sd_id"
									value=""  />
								</td>
							</tr>
						</table>
				</td>
			</tr>
			<tr>
			<td colspan="2" style="text-align: center"><span
					style="color: red;" id="errInfo">&nbsp;</span></td>
			</tr>
			</table>
			<div align="center" id="set_l" style="width:100%; overflow: auto; cursor: default;">
				<input type="checkbox" name="set_local" value="setLocation" onclick="setLocation()">Draw Child Layer
					<div id="set_location" style="margin:0px;text-align:center;background:#FFFFFF;border:solid 1px #DDDDDD;display: none;">
						<table  width="100%"  border="0" cellpadding="0" cellspacing="0">
							<tr>
							<td colspan="3" align="center">
							 <input type="radio" width="25%" name="child_type" value="1" checked="checked">Location
							 <input type="radio" width="25%" name="child_type" value="2">Staging
							 <input type="radio" width="25%" name="child_type" value="3">Dock
							 <input type="radio" width="25%" name="child_type" value="4">Parking
							</td>
							</tr>
							<tr >
  								<td align="right" width="35%"><p style="font-size: 14" >vertically:</p></td>
								<td width="35%">
								<input type="text" value="" id="vertically" style="width: 80%"  onchange="this.value=this.value.trim(),verifyVer()">
								<img id="v_con" src="../imgs/maps/confirm.jpg" style="display: none;">
					            <img id="v_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none;">
								</td>
								<td rowspan="2" align="center" width="30%">
								<input type="button" value="Create"  style="height:45px;width:50px; border: 0px; background-image: url(../imgs/maps/create_loca.png);" onclick="createLocation()">
								</td>
							</tr>
							<tr >
								<td align="right" ><p style="font-size: 14">horizontally:</p></td>
								<td><input type="text" value="" id="horizontally" style="width: 80%"  onchange="this.value=this.value.trim(),verifyHor()">
								<img id="h_con" src="../imgs/maps/confirm.jpg" style="display: none;">
					            <img id="h_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none;">
								</td>
							</tr>
							<tr >
								<td align="right" ><p style="font-size: 14">Loc Name:</p></td>
								<td><input type="text" value="" id="loc_name" style="width: 80%"   onchange="this.value=this.value.trim(),verifyLocName('<%=psId%>',this.value)" >
								<img id="locname_con" src="../imgs/maps/confirm.jpg" style="display: none;">
					            <img id="locname_del" title="Name is Repeat!" src="../imgs/maps/delete_red.jpg" style="display: none;"></td>
							</tr>
							<tr >
								<td align="right" ><p style="font-size: 14">V_Interval:</p></td>
								<td>
								<input type="text" value="0" id="v_interval" style="width: 80%" onchange="this.value=this.value.trim(),verifyVi()" >
								<img id="vi_con" src="../imgs/maps/confirm.jpg" style="display: none;">
					            <img id="vi_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none;">
								</td>
								<td rowspan="2" align="center" width="30%">
								<input type="button" value="Clear"  style="height:45px;width:50px; border: 0px; background-image: url(../imgs/maps/create_loca.png);" onclick="clearLocation()">
								</td>
							</tr>
							<tr >
								<td align="right" ><p style="font-size: 14">H_Interval:</p></td>
								<td><input type="text" value="0" id="h_interval" style="width: 80%" onchange="this.value=this.value.trim(),verifyHi()"  >
								<img id="hi_con" src="../imgs/maps/confirm.jpg" style="display: none;">
					            <img id="hi_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none;"></td>
							</tr>
						</table>
					</div>
			</div>
	</form>
</body>
