<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="containerTypeKey" class="com.cwc.app.key.ContainerTypeKey"></jsp:useBean>
<jsp:useBean id="WebcamPositionTypeKey" class="com.cwc.app.key.WebcamPositionTypeKey"></jsp:useBean>
<%
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(3);
    com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp queryKmlInfoMgr = (com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp)MvcUtil.getBeanFromContainer("proxyQueryKmlInfoMgrIfaceWp");
	String titleId=StringUtil.getString(request,"title");	          //titleid
	String positionName=StringUtil.getString(request,"position_all");	  //位置名称
	String lot_number_id=StringUtil.getString(request,"lot_num"); //批次
	String catalog_id=StringUtil.getString(request,"catalog_id");// 类型
	long line=StringUtil.getLong(request,"line"); //生产线
	long ps_id=StringUtil.getLong(request,"ps_id");       //仓库
	long slc_id=queryKmlInfoMgr.getPositionId(ps_id, positionName, WebcamPositionTypeKey.LOCATION);// 位置Id
	//DBRow[] rows = productStoreMgrZJ.getProductStoreCount(Long.toString(ps_id), 0l, slc_id, titleId, 0l, lot_number_id, "", line);// 商品信息
	DBRow data1=new DBRow();
	data1.add("PC_ID", "0001");
	data1.add("P_NAME", "VHE211K/10609042");
	data1.add("PHYSICAL", "79");
	data1.add("AVAILABLE", "110");
	DBRow data2=new DBRow();
	data2.add("PC_ID", "0001");
	data2.add("P_NAME", "VHE211K/10609042");
	data2.add("PHYSICAL", "79");
	data2.add("AVAILABLE", "110");
	DBRow data3=new DBRow();
	data3.add("PC_ID", "0001");
	data3.add("P_NAME", "VHE211K/10609042");
	data3.add("PHYSICAL", "79");
	data3.add("AVAILABLE", "110");
	DBRow data4=new DBRow();
	data4.add("PC_ID", "0001");
	data4.add("P_NAME", "VHE211K/10609042");
	data4.add("PHYSICAL", "79");
	data4.add("AVAILABLE", "110");
	DBRow rows[]=new DBRow[4];
	rows[0]=data1;
	rows[1]=data2;
	rows[2]=data3;
	rows[3]=data4;
%>

<html>
  <head>
<script language="javascript" src="../../common.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<script type="text/javascript" src="../js/select.js"></script>		
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link href="../comm.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<!--  自动填充 -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/art/plugins/jquery.artDialog.source.js"></script>
<script type="text/javascript" src="../js/art/plugins/iframeTools.source.js"></script>
<script type="text/javascript" src="../js/art/plugins/iframeTools.js"></script>	
<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>
<title>商品信息</title>

  </head>
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
     <fieldset style="border:1px #C00 solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
		 <legend style="font-size:15px;font-weight:bold;"> 
			<%=positionName %>
		 </legend>
		 <div align="left" style="border:2px #dddddd solid;background:#E8E8E8;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
      
		 	<span style="font-size:12px; font-weight: bold; color:black">当前位置:</span>
            <span style="font-size:12px; font-weight: bold; color:#00F"><%=positionName %></span><br/>
            <hr size="1px;" color="#dddddd">
     	 </div>
     	  	 <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	     	   <tr> 
		    <!--   <th width="5%" style="vertical-align: center;text-align: center;" class="left-title">&nbsp;</th>--> 
		      <th width="20%" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
		      <th width="20%"  style="vertical-align: center;text-align: center;" class="right-title">位置信息</th>
		      <th width="20%" class="right-title"  style="vertical-align: center;text-align: center;">数值库存</th>
		      <th width="17%" class="right-title"  style="vertical-align: center;text-align: center;">残损</th>
		      <th width="18%" class="right-title"  style="vertical-align: center;text-align: center;">周期平均</th>
		 	 </tr>
		 	 
     	 <%if(rows!=null && rows.length>0){%>
			<%for(int i=0;i<rows.length;i++){ %>	
			 <tr > 
			    <!-- <td width="60" height="60" align="center" valign="middle"   > 
					  <a name="" id=""></a>
				      <input name="pid_batch"  type="checkbox" value="">
				      <input type="hidden" value="" id=""/>
			      </td> -->  
				  <td height="60" valign="middle"   style='font-size:14px;' >
			       <fieldset style="border:1px blue solid;padding:7px;width:240px;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
				      <legend style="font-size:15px;font-weight:bold;color:black;"> 
							 <%=rows[i].getString("p_name")%>
					  </legend>	
						    <p style="font-size: 12px">商品分类:
						    </p>
						    <%if(catalog_id.equals("")){ 
						    DBRow[] dbrows = proprietaryMgrZyj.findProductCatagoryParentsIdAndNameByTitleId(true, 0, Long.toString(line), "","", titleId, 0, 0, null, request);
						    if(dbrows.length>0){
						    for(int ii=0;ii<dbrows.length;ii++){
						    	DBRow dbrow=dbrows[ii];
						    
						    %>
						    <p style="padding-left: 70px;font-size: 12px">
						         <span>
						         <a href="javascript:void(0)" onclick="" style="text-decoration:none"><%=dbrow.getString("name")%></a>
								 </span>
							</p>
							
							<%    }
						        }
							  }else{
								  
							  }%>
								  								
				   </fieldset>	
		         </td>
			     <td align="center" valign="middle"   >
					  <fieldset style="border:1px green solid;padding:7px;width:80%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
							<legend style="font-size:15px;font-weight:bold;color:green;">
								
							   <span style="font-size:13px;color:#999999;font-weight:normal"><%=positionName %></span>
							</legend>  
								<p align="left"><a href="javascript:void(0)" onclick="openlot()" style="text-decoration:none">按批次查看库存</a></p>
								<p align="left"><a href="javascript:void(0)" onclick="openlp()" style="text-decoration:none">按配置查看</a></p>
								<p align="left"><a href="javascript:void(0)" onclick="opentitle()" style="text-decoration:none">按title查看</a></p>
									<div>可用数量：</div>
								 	<div>物理数量：</div>
					  </fieldset>
				  &nbsp;
			  </td>
			    <td align="center" valign="middle"   style='word-break:break-all;' nowrap="nowrap"> 
				<input name="" type="hidden" value=""  >
				
				<span style="display: block;overflow: inherit"><font color="blue">有效库存:</font></span>
				<span style="display: block;overflow: inherit"><font color="red">盘点库存:</font></span>
				<span style="display: block;overflow: inherit">出库中:</span>
			  </td>
			   <td align="center" valign="middle"   style='word-break:break-all;font-weight:bold'>
			  	功能残损:<br/>
			  	外观残损:
			  </td>
		      <td align="center" valign="middle"   style='word-break:break-all;font-weight:bold'>
			  	<div style="width:80%" align="left">
			  		采样周期:<br/>
			  		<font color="green">需求数量:</font><br/>
				  	<font color="blue">计划备货:</font><br/>
				  	<font color="">发货数量:</font><br/> 
				  	销售系数:<br/>
				  	备货天数:
			  	</div>	  	
			  </td>
		 </tr>
			 <%}%>
		 <%}%>
		 </table>
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	 <form name="dataForm">
      <input type="hidden" name="p" >
	  <input type="hidden" name="title_id" id="title_id" value="<%=titleId%>"/>
	  <input type="hidden" name="lot_number_id" id="lot_number_id" value="<%=lot_number_id%>">	  
	  <input type="hidden" name="positionName" id="positionName" value="<%=positionName%>">	  
	 </form>
		        <tr> 
          
			    <td height="28" align="right" valign="middle"> 
			      <%
			int pre = pc.getPageNo() - 1;
			int next = pc.getPageNo() + 1;
			out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
			out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
			out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
			out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
			out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
			      跳转到 
			      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
			      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
			    </td>
			        </tr>
		 </table>
		 
      </fieldset>
  </body>
</html>