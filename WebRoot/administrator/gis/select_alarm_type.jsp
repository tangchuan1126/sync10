<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
String alarm_id = StringUtil.getString(request,"alarm_id");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <!--  基本样式和javascript -->
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<script type="text/javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- jquery UI 加上 autocomplete -->
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
  </head>
  
  <body>
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td colspan="2" align="left" valign="top">
				<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:80%;">
			    	<span >Select alarm type：</span>
			    	<br/><br/>
					<input type="checkbox" id="driveIn" name="driveIn">Drive in&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="driveOut" name="driveIn">Drive out
				</fieldset>
	           </td>
	        </tr>
		   <tr>
			    <td width="51%" align="right" valign="middle" class="win-bottom-line">&nbsp;</td>
    			<td width="49%" align="right" class="win-bottom-line">
				 <input type="button" class="short-short-button" value="Confirm"  onClick="submit();">
				 <input type="button" class="short-short-button" value="Cancle"  onClick="cancle();">
				</td>
	 	 </tr>
	   </table>
  </body>
<script type="text/javascript">
	function submit(){
		var alarmId = <%=alarm_id %>;
		var alarmType = 0;
		var driveIn = $("#driveIn").attr("checked");
		var driveOut = $("#driveOut").attr("checked");

		if(!driveIn && !driveOut){
			alert("Please select alarm type!");
			return;
		}
		if(driveIn){//驶入报警
			alarmType = 4;
		}
		if(driveOut){//驶出报警
			alarmType = 5;
		}
		if(driveIn && driveOut){//进出报警
			alarmType = 6;
		}
		$.artDialog.opener.addAlarmAjax(alarmId,alarmType);
		$.artDialog && $.artDialog.close();
	}

	function cancle(){
		$.artDialog && $.artDialog.close();
	}
</script>
</html>
