<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
	String systenFolder = ConfigBean.getStringValue("systenFolder");
	long psId = StringUtil.getLong(request, "ps_id");
	int location_type = StringUtil.getInt(request, "type");
	String key = StringUtil.getString(request, "key");
	String positionName = StringUtil.getString(request, "positionName").toUpperCase();
	DBRow[] datas = null;
	DBRow data = new DBRow();
	long position_id = 0l;
	String angle ="";
	if (location_type == 1) {
		datas = googleMapsMgrCc.getLocationInfoByName(psId,positionName);
		if (datas.length > 0) {
			data = datas[0];
			position_id = data.get("slc_id", 0l);
		}
	} else if (location_type == 2) {
		datas = googleMapsMgrCc.getStagingInfoByName(psId, positionName);
		if (datas.length > 0) {
			data = datas[0];
			position_id = data.get("id", 0l);
		}
	} else if (location_type == 3) {
		datas = googleMapsMgrCc.getDocksInfoByName(psId, positionName);
		if (datas.length > 0) {
			data = datas[0];
			position_id = data.get("sd_id", 0l);
		}
	} else if (location_type == 4) {
		datas = googleMapsMgrCc.getParkingInfoByName(psId, positionName);
		if (datas.length > 0) {
			data = datas[0];
			position_id = data.get("yc_id", 0l);
		}
	}
	angle = data.getString("angle");
	if(StringUtil.isBlank(angle)){
		angle="0";
	}

%>
<html>
  <head>
	<title>Modify data</title>
    <!--  基本样式和javascript -->
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<script type="text/javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- jquery UI 加上 autocomplete -->
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	<script src="../js/maps/verify.js"></script>
	<!-- stateBox 信息提示框 -->
	<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
	<script type="text/javascript">
	var systenFolder = "<%=systenFolder%>";
	</script>
	<script>
	$.blockUI.defaults = {
			css: { 
				padding:        '8px',
				margin:         0,
				width:          '170px', 
				top:            '45%', 
				left:           '40%', 
				textAlign:      'center', 
				color:          '#000', 
				border:         '3px solid #999999',
				backgroundColor:'#eeeeee',
				'-webkit-border-radius': '10px',
				'-moz-border-radius':    '10px',
				'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
				'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			},
			//设置遮罩层的样式
			overlayCSS:  { 
				backgroundColor:'#000', 
				opacity:        '0.6' 
			},
			baseZ: 99999, 
			centerX: true,
			centerY: true, 
			fadeOut:  1000,
			showOverlay: true
		};
	$(function(){
		//添加button
		var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
		var updateDialog = null;
		var addDialog = null;
		api.button([
				{
					name: 'Submit',
					callback: function () {
						save();
						return false;
					},
					focus:true,
					focus:"default"
				},
				{
					name: 'Cancel',
					callback:function (){
						closeWindow();
						return false;
					},
				}] 
			);
	});
	
	</script>
  </head>
  
  <body>
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td align="center" valign="top">
				  <fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:80%;">
				  	<form id="modify_data">
						<table >
							<tr>
								<td align="right" valign="middle" width="40%">Name：</td>
							    <td align="left" valign="middle" width="60%">
							    	<input type="text" style="width: 80%; background: " id="positionName" name="positionName" value="<%=positionName %>" onchange="this.value=this.value.trim();verifyName('<%=psId %>',this.value,'<%=location_type%>')" >
							    	<input type="hidden"  id="positionName_old" name="positionName_old"  value="<%=positionName %>">
							    	<input type="hidden"  id="position_id" name="position_id"  value="<%=position_id %>">
							    	<input type="hidden"  id="ps_id" name="ps_id"  value="<%=psId %>">
						    	    <img id="name_con" src="../imgs/maps/confirm.jpg" style="display: none;">
					            	<img id="name_del" title="Name is Repeat!" src="../imgs/maps/delete_red.jpg" style="display: none;">
					            </td>
					        
					   	    </tr>
							<tr>
								<td align="right" valign="middle">X position：</td>
							    <td align="left" valign="middle" >
					              <input type="text" style="width: 80%;" id="x" name="x" value="<%=data.getString("x")%>" onchange="this.value=this.value.trim(),verifyX()"/> 
					             <img id="x_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             <img id="x_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Y position：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 80%;" id="y" name="y" value="<%=data.getString("y")%>" onchange="this.value=this.value.trim(),verifyY()"/> 
					              <img id="y_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="y_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
					            </td>
					           
					   	    </tr>
							<tr>
								<td align="right" valign="middle">X length：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 80%;" id="height" name="height" value="<%=data.getString("height")%>" onchange="this.value=this.value.trim(),verifyHeight()"/>
					              <img id="height_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="height_del"  title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"> 
					            </td>
					            
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Y length：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 80%;" id="width" name="width" value="<%=data.getString("width")%>" onchange="this.value=this.value.trim(),verifyWidth()"/> 
					              <img id="width_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="width_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"> 
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Offset angle：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 80%;" id="angle" name="angle" value="<%=angle%>" onchange="this.value=this.value.trim(),verifyAngle()"/> 
					              <img id="angle_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="angle_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"> 
					            </td>
					   	    </tr>
					   	    <tr id="_zone" style="display: none">
								<td align="right" >Zone：</td>
								<td align="left" >
								<input type="text" style="width: 80%;" name="zone" id="zone" value="<%=data.getString("area_name")%>" onclick="selectAreaSingle()" />
								<input type="hidden" name="area_id" id="area_id" value="<%=data.getString("area_id")%>"/>
								</td>
								 <td  align="left" valign="middle"></td>
							</tr>
							<tr id="_dock" style="display: none">
								<td align="right" >Dock：</td>
								<td align="left" >
								<input type="text" style="width: 80%;" name="dock" id="dock" value="<%=data.getString("doorId")%>" onclick="selectDoor()" />
								<input type="hidden" name="sd_id" id="sd_id" value="<%=data.getString("sd_id") %>"  />
								</td>
								 <td  align="left" valign="middle"></td>
							</tr>
						 </table>
						 <input type="hidden" id="type" value="<%=location_type%>"/>
				  	</form>
				 </fieldset>
	           </td>
	        </tr>
	   </table>
  </body>
  <script type="text/javascript">
  $(function(){
	  
	  var type =$("#type").val();
		if(type=="1"||type=="3"||type=="4"){
			$("#_zone").show();
			$("#_dock").hide();
		}
		if(type=="2"){
			$("#_zone").hide();
			$("#_dock").show();
		}
		var dialogId="layer_info";
		var layer=$.artDialog.opener.jsmap.storageProvenLayer["<%=key%>"];
		layer.setVisible(false);
		$.artDialog.opener.dragStorageLayer(layer,dialogId,"<%=angle%>");
		
		
  });

  function convertCoordinateAjax(psId ,lat,lng){
		$.ajax({
			url:<%=ConfigBean.getStringValue("systenFolder")%>+'action/administrator/gis/convertCoordinate.action',
			data:'psId='+psId+'&lat='+lat+'&lng='+lng,
			dataType:'json',
			type:'post',
			//async:true, 
			beforeSend:function(request){
		    },
			success:function(data){
		    	if(data&&data.flag=="true"){
		        var x =data.x;
		        var y =data.y;
		    	$("#x").val(x);
		    	$("#y").val(y);
		    	}
			},
			error:function(){
			}
		});
		}
  var verifyMgs={};
  function verifyName (psId,name,type){
	  var oldName="<%=positionName %>";
	  if(oldName!=name){
		  var vMsg="";
		  if(!name){
			  vMsg="Name is Empty!";
		  }
		  var data ={"ps_id":psId,"name":name,"type":type};
		  var url =systenFolder+'action/administrator/gis/verifyName.action';
		  verifyMgs[verifyName]=verify('positionName','name_con','name_del','',url,data,vMsg);
	  }else{
  			verifyMgs[verifyName]=1;
  			$("#name_con").show();
  			$("#name_del").hide();
  		  }
	  
  }
  function verifyX(){
	  var vMsg="";
	  if($("#x").val()==''){
		  vMsg="x is Empty!";
  	}else{
  		vMsg="Data format error";
  	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs[verifyX]=verify('x','x_con','x_del',reg,'','',vMsg);
  }
  function verifyY(){
	  var vMsg="";
	  if($("#y").val()==''){
		  vMsg="y is Empty!";
  	}else{
  		vMsg="Data format error";
  	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs[verifyY]=verify('y','y_con','y_del',reg,'','',vMsg);
  }
  function verifyWidth(){
	  var vMsg="";
	  if($("#width").val()==''){
		  vMsg="width is Empty!";
  	}else{
  		vMsg="Data format error";
  	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs[verifyWidth]=verify('width','width_con','width_del',reg,'','',vMsg);
  }
  function verifyHeight(){
	  var vMsg="";
	  if($("#height").val()==''){
		  vMsg="height is Empty!";
  	}else{
  		vMsg="Data format error";
  	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs[verifyHeight]=verify('height','height_con','height_del',reg,'','',vMsg);
  }
  function verifyAngle(){
	  var vMsg="";
	  if($("#angle")){
		  vMsg="angle is Empty!";
  	}else{
  		vMsg="Data format error";
  	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs[verifyAngle]= verify('angle','angle_con','angle_del',reg,'','',vMsg); 
  }
  	function save(){
  		if(verifyMgs[verifyName]==0 || verifyMgs[verifyName]==2){
  			$("#positionName").focus();
  			return;
  		}
  		if(verifyMgs[verifyX]==0 || verifyMgs[verifyX]==2){
  			$("#x").focus();
  			return;
  		}
  		if(verifyMgs[verifyY] ==0 || verifyMgs[verifyY] ==2){
  			$("#y").focus();
  			return;
  		}
  		if(verifyMgs[verifyWidth]==0 || verifyMgs[verifyWidth]==2){
  			$("#width").focus();
  			return;
  		}
  		if(verifyMgs[verifyHeight]==0 ||verifyMgs[verifyHeight]==2){
  			$("#height").focus();
  			return;
  		}
  		if(verifyMgs[verifyAngle]==0 || verifyMgs[verifyAngle]==2){
  			$("#angle").focus();
  			return;
  		}
  		var isChanged = ($("#x").val()!="<%=data.getString("x")%>"||
					  		   $("#y").val()!="<%=data.getString("y")%>"||
					  		   $("#height").val()!="<%=data.getString("height")%>"||
					  		   $("#width").val()!="<%=data.getString("width")%>"||
					  		   $("#angle").val()!="<%=angle%>") ? 'true':'false';
  		var changed=($("zone").val()!="<%=data.getString("area_name")%>"||$("dock").val()!="<%=data.getString("doorId")%>")?'true':'false';
  		if($("#positionName").val()!="<%=positionName%>" || isChanged=="true"||changed=="true"){
	  		if(dataValid()){
	  			$.ajax({
	  				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/modifyPosition.action',
	  				data:$("#modify_data").serialize()+"&isChanged="+isChanged+"&location_type=<%=location_type%>&from=map",
	  				dataType:'json',
	  				type:'post',
	  				beforeSend:function(request){
	  					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	  				},
	  				success:function(data){
	  					$.unblockUI();
	  					if(data && data.flag == 'true'){
  					    $.artDialog.opener.modifyPositionLatlng(<%=psId%>,'<%=positionName%>',$("#positionName").val(),data.latlng,<%=location_type %>);
				  		closeWindow();
	  					}else if(data && data.flag == 'authError'){
  						showMessage("Insufficient permissions!","error");
	  					}else if (data && data.flag == 'false'){
  						showMessage("Sava failed!","error");
	  					}
	  				},
	  				error:function(){
	  					$.unblockUI();
	  					$("#errInfo").html("Sava failed!");
	  				}
	  			 });
	  		}
	  	}else{
	  	}
  	}
  	
  	
  	function dataValid(){
  		if($("#positionName").val().trim() == ""){
  			$("#errInfo").html("Name cannot be empty.");
  			return false;
  		}
  		var reg = /^-?\d+(\.\d+)?$/;
  		if(!reg.test($("#x").val())){
  			$("#errInfo").html("X position must be number.");
  			return false;
  		}
  		if(!reg.test($("#y").val())){
  			$("#errInfo").html("Y position must be number.");
  			return false;
  		}
  		if(!reg.test($("#height").val())){
  			$("#errInfo").html("X length must be number.");
  			return false;
  		}
  		if(!reg.test($("#width").val())){
  			$("#errInfo").html("Y length must be number.");
  			return false;
  		}
  		if($("#angle").val() != ""){
	  		if(!reg.test($("#angle").val())){
	  			$("#errInfo").html("Offset angle must be number.");
	  			return false;
	  		}
  		}
  		$("#errInfo").html(" ");
  		return true;
  	}

  	document.onkeydown=function(evt){
		var evt=evt?evt:(window.event?window.event:null);//兼容IE和FF
		if (evt.keyCode==13){
			save();
		}
  	}
	function closeWindow() {
		$.artDialog.close();
		$.artDialog.opener.jsmap.clearDemoLayer();
		$.artDialog.opener.layerSetMap('<%=key%>');
	}
	
	//选择door
	function selectDoor(){
		var type=$("#type").val();
		var psId=$("#ps_id").val();
		var name=$("#name").val();
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_door_single.html?ps_id='+psId;
		$.artDialog.open(url, {title: "Select Dock ["+name+"]",width:'790px',height:'425px', lock: true,opacity: 0.3});	

	}
	//选择单个area
	function selectAreaSingle(){
		var type=$("#type").val();
		var _type="";
		if(type==1){
			_type=1;
		}
		if(type==4){
			_type=3;
		}
		if(type==3){
			_type=2;
		}
		var psId=$("#ps_id").val();
		var name=$("#name").val();
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_area_single.html?ps_id='+psId+'&area_type='+_type;
		$.artDialog.open(url, {title: "Select Area ["+name+"]",width:'790px',height:'425px', lock: true,opacity: 0.3});
	}
	function backFillArea(names, ids){
		$("#zone").val(names);
		$("#area_id").val(ids);
	}
	function backFillDoor(names, ids){
		$("#dock").val(names);
		$("#sd_id").val(ids);
	}
	
	function fillValue(x,y){
		$("#width").val(x);
		$("#height").val(y);
	}
	function fillAngle(angle){
		$("#angle").val(angle);
	}
  </script>
</html>
