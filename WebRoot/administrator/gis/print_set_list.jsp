<%@page import="com.cwc.app.key.BCSKey"%>
<%@page import="com.cwc.app.key.PrintTaskKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<%
 	long psId = StringUtil.getLong(request,"ps_id");
%>


<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print Set</title>
     <base href="<%=ConfigBean.getStringValue("systenFolder") %>administrator/">
<!-- 基本css 和javascript -->
<link href="comm.css" rel="stylesheet" type="text/css">
<!-- <script language="javascript" src="common.js"></script>  -->

<script type="text/javascript" src="js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>


<script type="text/javascript" src="js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="js/autocomplete/jquery.ui.tabs.js"></script>

   
<script type='text/javascript' src='js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="js/autocomplete/jquery.ui.tabs.css" />
 
  
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" />
<script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
 
<link rel="stylesheet" href="js/file-upload/css/bootstrap.min.css">
<script type="text/javascript" src='js/showMessage/showMessage.js'></script>
<link type="text/css" href="js/fullcalendar/stateBox.css" rel="stylesheet"/>

<style type="text/css" media="all">
</style>
 
 <script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css" />
 <!-- 遮罩 -->
<script type="text/javascript" src="js/blockui/jquery.blockUI.233.js"></script>

<style type="text/css">
 	.button {
	 	 border-image: none;
	    border-radius: 4px;
	    border-style: solid;
	    border-width: 1px;
	    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
	    color: #333333;
	    cursor: pointer;
	    display: inline-block;
	    font-size: 14px;
	    line-height: 20px;
	    margin-bottom: 0;
	    padding: 4px 12px;
	    text-align: center;
	    text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
	    vertical-align: middle;
	    font-weight:bold;
    }
 	.addButton{
	 	background-color: #5bb75b;
	    background-image: -moz-linear-gradient(center top , #62c462, #51a351);
	    background-repeat: repeat-x;
	    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
	    color: #ffffff;
	    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    }
</style>
 
 <%
	 DBRow[] allPrintSet = null ;
 	if(psId != 0l){	
 		allPrintSet = androidPrintMgr.getAllAndroidPrinterServerByPsId(psId);
 	}else{
 		allPrintSet = androidPrintMgr.getAllPrintServer();
 	}
 %>
 <script>
 
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '230px', 
			top:            '35%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '1px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
</script>

<script type="text/javascript">
 
 function cancel(task_id){
	  $.ajax({
		  url:'<%=ConfigBean.getStringValue("systenFolder")%>action/checkin/PrintTaskOperAction.action',
		  data:'task_id='+task_id+'&Method=cancel',
		  dataType:'json',
		  beforeSend:function(request){
		      $.blockUI({message: 'Submit... '});
		  },
		  success:function(data){
			  $.unblockUI();
			  if(data.ret * 1 == 1){
				  $("#div_"+task_id).remove();
			  }
			  if($("div",$("#failed")).length < 1){
				  $("#failed").append($("<p style='padding:10px;border:1px dashed silver'>No Records</p>"))
			  }
		  },
		  error:function(){
			  $.unblockUI();
		 	  showMessage("System Error.","error");
		  }
	  })
 }
 function deleteServer(id,serverName){
	 $.artDialog({
		 	title:'Notify',
		 	lock: true,
		 	opacity: 0.3,
		 	icon:'question',
 		    content: "<span style='font-weight:bold;'>Delete ["+serverName+"] ?</span>",
		    button: [
		        {
		            name: 'YES',
		            callback: function () {
		            	ajaxDelete(id);
  		            },
		            focus: true
		        },
		        {
		            name: 'NO'
		        }
		    ]
		});
 }
 function ajaxDelete(id){
	 $.ajax({
		 url:'<%=ConfigBean.getStringValue("systenFolder")%>action/checkin/PrintServerSetOperAction.action',
		 dataType:'json',
		 data:'Method=Delete&printer_server_id='+id,
		 beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		 success:function(data){
			$.unblockUI();
		 	if(data != null && data.ret * 1 ==  '<%= BCSKey.SUCCESS%>'){
    		 	 window.location.reload();
		 	}else{
		 	   showMessage("System Error","error");
			}
		 },
		 error:function(){
		     $.unblockUI();
		     showMessage("System Error,Try Later.","error");
		 }
	  })
 }
 function refreshWindow(){
	 window.location.reload();
 }
 function openAdd(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/print/print_set_add.html?ps_id=<%=psId%>";
	 $.artDialog.open(uri,{title: "Add Print Server",width:'400px',height:'130px', lock: true,opacity: 0.3,fixed: true});
 }
 /**
 *选择server
 */
 function choosePrinterServer(s_id,s_name){
	 $.artDialog.opener.backFillPrinterServer(s_id,s_name);
	 $.artDialog.close();
	 
 }
</script>
</head>
  
   <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="width: 98%;" onload="onLoadInitZebraTable()">
  	 	 <div style="margin-top:5px;border:1px solid silver ;padding:10px;    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5); border-radius: 4px;background-color: #f2dede;border-color: #eed3d7;    color: #b94a48;">
  	 	 <span class="btn btn-success fileinput-button" onclick="openAdd();">
  	 	 	<i class="icon-plus icon-white"></i>
			PrintServer
		</span>
  	 	 </div>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable" style="margin-top:5px;">
	    <tr> 
	        <th width="25%" class="right-title" style="vertical-align: center;text-align: center;">Server Name</th>
  	    	<th width="25%" style="vertical-align: center;text-align: center;" class="left-title">Storage</th> 
  	    	<th width="25%" style="vertical-align: center;text-align: center;" class="left-title">Service</th> 
  	     	<th width="15%" style="vertical-align: center;text-align: center;" class="left-title">Opera</th> 
	    </tr>
	   <tbody>
   	 	 	<%
  	 	 		if(allPrintSet != null && allPrintSet.length > 0 ){
  	 	 			for(DBRow set :  allPrintSet){
  	 	 				%>
  	 	 					<tr onclick="choosePrinterServer('<%= set.get("printer_server_id", 0l)%>','<%= set.getString("printer_server_name")%>')">
  	 	 						<td><%=set.getString("printer_server_name") %></td>
  	 	 						<td style="text-align: center;"><%=set.getString("title") %></td>
  	 	 						<td style="text-align: center;"><%=set.getString("area_name") %></td>
   	 	 						<td style="padding: 10px;text-align: center;">
   	 	 							<input type="checkbox" onclick="choosePrinterServer('<%= set.get("printer_server_id", 0l)%>','<%= set.getString("printer_server_name")%>')">
  	 	 							<%-- <button class="btn btn-danger delete" type="button" onclick="deleteServer('<%= set.get("printer_server_id", 0l)%>','<%= set.getString("printer_server_name")%>')">
										<i class="icon-trash icon-white"></i>
										Delete 
									</button> --%>
  	 	 							&nbsp;
  	 	 						</td>
  	 	 					</tr>
  	 	 				<%
  	 	 			}
  	 	 		}else{
  	 	 			%>
  	 	 			<tr>
		     			<td colspan="4" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Records</td>			
					</tr>
  	 	 			<% 
  	 	 		}
  	 	 	%>
	   </tbody>
	   
  	 </table>
 	</body>
</html>
