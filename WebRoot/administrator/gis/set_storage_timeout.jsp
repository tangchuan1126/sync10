<%@page import="antlr.StringUtils"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*,com.cwc.service.android.action.mode.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="setReportScreenTypeKey" class="com.cwc.app.key.SetReportScreenTypeKey" />
<jsp:useBean id="setReportScreenParamsTypeKey" class="com.cwc.app.key.SetReportScreenParamsTypeKey" />
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
    <script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
    <script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	
   <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
   <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
   <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
   <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

    <link rel="stylesheet" type="text/css" href="../js/easyui/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../js/easyui/themes/icon.css" />
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">

	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>

	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>


	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
	
	<!-- stateBox 信息提示框 -->
	<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
<style type="text/css">
    html, body {
	   margin: 0px;
	   padding: 0px;
    }
    .tabs{
      margin: 0;
      padding: 0
    }
    img{
     padding:0 0 0 8;
    }
    .header{
      font-size:18px;
      color:green;
      margin:0 0 0 30%
    }
    .box{
      border-radius:4px;
      padding: 10px;
      width:auto;
      margin:10 0 0 0;
      border:1px #cccccc solid;
    }
</style>
</head>
<body >
<%
	long ps_id =StringUtil.getLong(request, "ps_id");
	String title =StringUtil.getString(request, "title");
// 	DBRow[] datas =googleMapsMgrCc.getStorageTimeOutBypsId(ps_id);
    ArrayList screen_list=setReportScreenTypeKey.getAllKey();
%>
<fieldset class="box">
<legend class="header"><span>LOAD-RECEIVE-PROCESSING</span></legend>
<div class="tabs"  style="border: 0 solid #aaaaaa;height:160px;padding:0">
	<ul>
		<li ><a href="#<%=setReportScreenTypeKey.IncomingToWindow%>"><%=setReportScreenTypeKey.getKeyValue(setReportScreenTypeKey.IncomingToWindow) %></a></li>
		<li ><a href="#<%=setReportScreenTypeKey.LoadReceive%>"><%=setReportScreenTypeKey.getKeyValue(setReportScreenTypeKey.LoadReceive) %></a></li>
		<li ><a href="#<%=setReportScreenTypeKey.NeedAssign %>"><%=setReportScreenTypeKey.getKeyValue(setReportScreenTypeKey.NeedAssign) %></a></li>
		<li ><a href="#<%=setReportScreenTypeKey.RemainJobs%>"><%=setReportScreenTypeKey.getKeyValue(setReportScreenTypeKey.RemainJobs) %></a></li>
		<li ><a href="#<%=setReportScreenTypeKey.UnderProcessing%>"><%=setReportScreenTypeKey.getKeyValue(setReportScreenTypeKey.UnderProcessing) %></a></li>
	</ul>
	<div id="<%=setReportScreenTypeKey.IncomingToWindow%>">
	     <div style="margin: 0 0 5 0"> 
	      <input style="margin-left:0px" type="button" class="short-button" value="Edit">
	     </div>
	      <%
	       HoldThirdValue<DBRow, DBRow, DBRow> ascreen=checkInMgrZr.getLoadReceiveScreenParamSet(ps_id,setReportScreenTypeKey.IncomingToWindow);
	       String ahandletime=ascreen.a!=null&&ascreen.a.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.HandleTime))?ascreen.a.getString("value"):"0";
	       String ared=ascreen.b!=null&&ascreen.b.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.Red))?ascreen.b.getString("value"):"0";
	       String ayellow=ascreen.c!=null&&ascreen.c.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.Yellow))?ascreen.c.getString("value"):"0";
	      %>
         <table>
	      <tr>
	        <td align="center">Handle Time:</td>
	        <td><input  type="text" value="<%=ahandletime %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_time(this)"/>
	            <span>(Min)<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	      <tr>
	        <td>Yellow Color:</td>
	        <td><input  type="text" value="<%=ayellow %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_color(this)" />
	            <span>%<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	      <tr>
	        <td>Red Color:</td>
	        <td><input  type="text" value="<%=ared %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_color(this)" />
	            <span>%<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	     </table>
    </div>
    <div id="<%=setReportScreenTypeKey.LoadReceive%>">
         <div style="margin: 0 0 5 0"> 
	      <input style="margin-left:0px" type="button" class="short-button" value="Edit">
	     </div>
         <%
	       HoldThirdValue<DBRow, DBRow, DBRow> bscreen=checkInMgrZr.getLoadReceiveScreenParamSet(ps_id,setReportScreenTypeKey.LoadReceive);
           String bhandletime=bscreen.a!=null&&bscreen.a.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.HandleTime))?bscreen.a.getString("value"):"0";
	       String bred=bscreen.b!=null&&bscreen.b.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.Red))?bscreen.b.getString("value"):"0";
	       String byellow=bscreen.c!=null&&bscreen.c.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.Yellow))?bscreen.c.getString("value"):"0";
	      %>
         <table>
	      <tr>
	        <td align="center">Handle Time:</td>
	        <td><input  type="text" value="<%=bhandletime %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_time(this)"/>
	            <span>(Min)<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	      <tr>
	        <td>Yellow Color:</td>
	        <td><input  type="text" value="<%=byellow %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_color(this)" />
	            <span>%<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	      <tr>
	        <td>Red Color:</td>
	        <td><input  type="text" value="<%=bred %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_color(this)" />
	            <span>%<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	     </table>
    </div>
    <div id="<%=setReportScreenTypeKey.NeedAssign%>">
         <div style="margin: 0 0 5 0"> 
	      <input style="margin-left:0px" type="button" class="short-button" value="Edit">
	     </div>
         <%
	       HoldThirdValue<DBRow, DBRow, DBRow> cscreen=checkInMgrZr.getLoadReceiveScreenParamSet(ps_id,setReportScreenTypeKey.NeedAssign);
           String chandletime=cscreen.a!=null&&cscreen.a.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.HandleTime))?cscreen.a.getString("value"):"0";
	       String cred=cscreen.b!=null&&cscreen.b.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.Red))?cscreen.b.getString("value"):"0";
	       String cyellow=cscreen.c!=null&&cscreen.c.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.Yellow))?cscreen.c.getString("value"):"0";
	      %>
         <table>
	      <tr>
	        <td align="center">Handle Time:</td>
	        <td><input  type="text" value="<%=chandletime %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_time(this)"/>
	            <span>(Min)<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	      <tr>
	        <td>Yellow Color:</td>
	        <td><input  type="text" value="<%=cyellow %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_color(this)" />
	            <span>%<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	      <tr>
	        <td>Red Color:</td>
	        <td><input  type="text" value="<%=cred %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_color(this)" />
	            <span>%<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	     </table>
    </div>
    <div id="<%=setReportScreenTypeKey.RemainJobs%>">
         <div style="margin: 0 0 5 0"> 
	      <input style="margin-left:0px" type="button" class="short-button" value="Edit">
	     </div>
         <%
	       HoldThirdValue<DBRow, DBRow, DBRow> dscreen=checkInMgrZr.getLoadReceiveScreenParamSet(ps_id,setReportScreenTypeKey.RemainJobs);
           String dhandletime=dscreen.a!=null&&dscreen.a.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.HandleTime))?dscreen.a.getString("value"):"0";
	       String dred=dscreen.b!=null&&dscreen.b.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.Red))?dscreen.b.getString("value"):"0";
	       String dyellow=dscreen.c!=null&&dscreen.c.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.Yellow))?dscreen.c.getString("value"):"0";
	      %>
         <table>
	      <tr>
	        <td align="center">Handle Time:</td>
	        <td><input  type="text" value="<%=dhandletime %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_time(this)"/>
	            <span>(Min)<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	      <tr>
	        <td>Yellow Color:</td>
	        <td><input  type="text" value="<%=dyellow %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_color(this)" />
	            <span>%<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	      <tr>
	        <td>Red Color:</td>
	        <td><input  type="text" value="<%=dred %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_color(this)" />
	            <span>%<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	     </table>
    </div>
    <div id="<%=setReportScreenTypeKey.UnderProcessing%>">
         <div style="margin: 0 0 5 0"> 
	      <input style="margin-left:0px" type="button" class="short-button" value="Edit">
	     </div>
        <%
	       HoldThirdValue<DBRow, DBRow, DBRow> fscreen=checkInMgrZr.getLoadReceiveScreenParamSet(ps_id,setReportScreenTypeKey.UnderProcessing);
           String fhandletime=fscreen.a!=null&&fscreen.a.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.HandleTime))?fscreen.a.getString("value"):"0";
	       String fred=fscreen.b!=null&&fscreen.b.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.Red))?fscreen.b.getString("value"):"0";
	       String fyellow=fscreen.c!=null&&fscreen.c.getString("type_key").equals(String.valueOf(setReportScreenParamsTypeKey.Yellow))?fscreen.c.getString("value"):"0";
	      %>
         <table>
	      <tr>
	        <td align="center">Handle Time:</td>
	        <td><input  type="text" value="<%=fhandletime %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_time(this)"/>
	            <span>(Min)<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	      <tr>
	        <td>Yellow Color:</td>
	        <td><input  type="text" value="<%=fyellow %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_color(this)" />
	            <span>%<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	      <tr>
	        <td>Red Color:</td>
	        <td><input  type="text" value="<%=fred %>" onchange="this.value=this.value.trim(),verify(this)"  onkeyup="validate_color(this)" />
	            <span>%<img id="con" src="../imgs/maps/confirm.jpg" style="display: none"><img id="del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none"></span></td>
	      </tr>
	     </table>
    </div>
</div>
</fieldset>
<script type="text/javascript">
$(function(){
	//添加button
		/*  var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
		api.button([
				{
					name: 'Submit',
					callback: function () {
						commitData();
						return false;
					},
					focus:true,
					focus:"default"
				},
				{
					name: 'Cancel',
					callback:function (){
						closeWindow();
						return false;
					},
				}] 
			);  */
		//加载选项卡
	    $(".tabs").tabs({
	          spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	          cache:true
	          /* event:"mouseover",
	           fx:{
	        	  opacity:0.5,
	        	  height:"slideUp"
	          } */
	     });
	    $(".tabs").tabs("select",0);
	    //给button绑定时间 save和edit 之间的转换
	    $("input[type='text']").attr("readonly","readonly").css("border","none");
	    $("input[type='button']").live('click',function(evt){
	    	var _this=$(this);
	    	//处理edit
	    	if(_this.attr("value")=="Edit"){
	    		_this.attr("value","Save");
	    		_this.parent().parent().children("table").find("input").removeAttr("readonly").css("border"," 1px solid #CCCCCC");
	    	//处理Save	
	    	}else{
	    		//发送请求 保存设置
	    		commitData(_this);
	    	}
	    });
  });
  function validate_time(target){
	  _this=$(target);
	  var reg=/^-?\d+$/;
	  if(!reg.test(_this.val())){
		  _this.val("");
	  }
  }
  function validate_color(target){
	   _this=$(target);
	  var reg=/^(\d{1,2}(\.\d{1,2})?|100(\.0{1,2})?)$/;
	  if(!reg.test(_this.val())){
		  _this.val("");
	  }
  }
  function closeWindow(){
	  $.artDialog.close();
  }
  
  function commitData(target){
	 var inputarray=target.parent().parent().children("table").find("input");
	 var screen_type_id=target.parent().parent().attr("id");
	 var handletime = inputarray[0].value;
	 var yellow = inputarray[1].value;
	 var red =inputarray[2].value;
	 var reg = /^-?\d+(\.\d+)?$/; 
	 if(!reg.test(handletime)||handletime*1<=0){
		 showMessage("Data Error","error");
		 return ;
	 }
	 if(!reg.test(red)||red*1<=0){
		 showMessage("Data Error","error");
		 return ;
	 }
	 if(!reg.test(yellow)||yellow*1<=0){
		 showMessage("Data Error","error");
		 return ;
	 }
	 //加个验证 黄色值必须大于红色值
	 if(!isNaN(yellow)&&!isNaN(red)&&(parseInt(red)<=parseInt(yellow))){
		 showMessage("The value of 'Red Color' must greater than 'Yellow Color'","error");
		 return ;
	 }
	  $.ajax({
		    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/UpdateReportScreenSetAction.action?ps_id='+<%=ps_id%>+'&handle_time='+handletime+'&red='+red+'&yellow='+yellow+'&screen_type_key='+screen_type_id,
			dataType:'json',
			type:'post',
			async:true, 
			beforeSend:function(request){
		    },
			success:function(data){
// 		    	if(data&&data.id!=0){
// 		    		showMessage("Successful operation!","alert");
// 		    		closeWindow();
// 		    	}
				showMessage("Successful operation!","alert");
				target.attr("value","Edit");
				target.parent().parent().children("table").find("input").attr("readonly","readonly").css("border","none");
				target.parent().parent().children("table").find("img").hide();
			},
			error:function(){
				showMessage("System Error!","error");
			}
		});
	  
  }
  
  function verify(el){
	    var value =el.value;
  		var vMsg="Data format error";
  		var reg = /^-?\d+(\.\d+)?$/; 
  		var bl =reg.test(value);
  		var image_con=$(el).parent().children("span").children("#con");
  		var image_del=$(el).parent().children("span").children("#del")
  	  	if(bl && value*1 > 0){
  	  	      image_con.show();
  	  	      image_del.hide();
  	  	}else{
  	  	      image_del.attr("titile",vMsg);
  	  	      image_con.hide();
  	 	      image_del.show();	
  	  	}
  }
</script>
</body>
