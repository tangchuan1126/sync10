<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
	DBRow[] allGroups = googleMapsMgrCc.findAllGroup();

%>
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">

	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>

	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>


	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
	
	<!-- stateBox 信息提示框 -->
	<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
</style>
<style type="text/css">
	.ui-datepicker{font-size: 0.9em;}
</style>
<script type="text/javascript">
$(function(){
	$("#group").change(function(){  
		if($("#group").val()=="0"){
			//$("#group").attr("style","display:none");
			$("#newGroup_td").html('<input id="newGroup" type="text"  name="newGroup" style="width:100%;line-height:20px;color:silver;" value="*Group name" onfocus="inputIn()" onblur="outInput()" />');
		}else{
			$("#newGroup_td").html("&nbsp;");
		}
	});
    $("#gps_tracker").blur(function(){
        var imei = $("#gps_tracker").val().trim();
        if(imei == ""){
			return;
         }
		 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/checkin/AjaxFindAssetByGPSNumberAction.action',
			data:'gps_tracker='+$("#gps_tracker").val().trim(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		    
			},
			success:function(data){
				if(!$.isEmptyObject(data)){
					$("#errInfo").html("GPS num already exists");
					$("#flag").val("true");
				}else{
					$("#errInfo").html("");
					$("#flag").val("");
				}
				
			},
			error:function(){
				showMessage("System error","error"); 
			}
		 });
	 });
});
function inputIn(){
 	$("#newGroup").val("").css("color","black");
}
function outInput(){
	var  st = $("#newGroup").val();
	if($.trim(st).length < 1) {
		$("#newGroup").val("*Group name").css("color","silver");
	}
}
function addAsset(){
	 if($('#assetName').val().trim()==""){
		 $("#errInfo").html("Name cannot be empty");
		 return;
	 }
	 if($('#gps_tracker').val().trim()==""){
		 $("#errInfo").html("GPS num cannot be empty");
		 return;
	 }
	 if($('#callnum').val().trim()==""){
		 $("#errInfo").html("Phone num cannot be empty");
		 return;
	 }
	 if($("#flag").val()!="true"){
		 var group ="";
		 if($('#group').val()=="0"){
			 group = $('#newGroup').val().trim();
			 if(group == "" || group == "*Group name"){
				 $("#errInfo").html("Group name cannot be empty");
				 return;
			 }
		 }else{ 
			 group = $('#group option:selected').text().trim();
		 }
		 $("#groupName").val(group);
		 $.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/AddAssetAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 6000,
				cache:false,
				data:$("#add_asset").serialize(),
				beforeSend:function(request){
					
				},
				error: function(e){
					
				},
				success: function(data){
					//alert("添加成功");
					$.artDialog.opener.initAssetTree();
					closeWindow();
				}
		});
	 }else{
		 $("#errInfo").html("GPS num already exists");
	 }
}
function closeWindow(){
	$.artDialog.close();
}
</script>
</head>
<body >
	<form id="add_asset">
	  <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td colspan="2" align="center" valign="top">
				  <fieldset style="border:2px #cccccc solid;padding:15px;margin:0px 10px 0px 10px;width:80%;">
						<table style="width: 80%">
							<tr>
								<td align="right" valign="middle" width="40%">Name：</td>
							    <td align="left" valign="middle" width="60%">
					              <input type="text" style="width: 100%;" name="assetName" id="assetName"/> 
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">License plate：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 100%;" name="gate_liscense_plate" /> 
					            </td>
					   	    </tr>
					   	    <tr>
					   	    	<td align="right" valign="middle">GPS num： </td>
					                  		  
					            <td align="left" valign="middle">     		  
					              <input type="text" style="width: 100%;" name="gps_tracker" id="gps_tracker" />
					            </td>
						  	</tr>
						  	<tr>
					   	    	<td align="right" valign="middle">Phone num： </td>
					                  		  
					            <td align="left" valign="middle">     		  
					              <input type="text" style="width: 100%;" name="callnum" id="callnum" />
					            </td>
						  	</tr>
						    <tr>
					   	    	<td align="right" valign="middle">Group： </td>
					                  		  
					            <td align="left" valign="middle" id="group_td">     		  
						            <select id="group" name="groupId" style="width:100%;font-size:12px;">
										<option value="-1"></option>
										<%
											for(DBRow group:allGroups){
										%>
											<option value="<%=group.getString("id") %>"><%=group.getString("name") %></option>
										<%		
												
											}
										%>
											<option value="0" style="color: silver">New group</option>
									</select>
					            </td>
					            
						   </tr>
						   <tr>
						   		<td></td>
						   		<td id="newGroup_td">&nbsp;</td>
						   		<input type="hidden" name="groupName" id="groupName"/>
						   		<input type="hidden" name="flag" id="flag"/>
						   </tr>
						 </table>
					  </fieldset>
	           </td>
	        </tr>
		   <tr>
			   <td colspan="2" style="text-align: center"><span style="color: red;" id="errInfo">&nbsp;</span></td>
	        </tr>
		   <tr>
			    <td width="51%" align="right" valign="middle" class="win-bottom-line">&nbsp;</td>
    			<td width="49%" align="right" class="win-bottom-line">
				 <input type="button" name="Submit2"  class="short-short-button" value="Add"  onClick="addAsset();">
				 <input type="button" name="Submit2"  class="short-short-button" value="Cancel"  onClick="closeWindow();">
				</td>
	 	 </tr>
	   </table>
	</form>
</body>
