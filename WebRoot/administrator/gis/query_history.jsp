<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
	long asset_id = StringUtil.getLong(request, "asset_id");
%>
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">

	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>

	<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>


	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
	
	<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";

--%>
@import "../js/thickbox/thickbox.css";
</style>

	<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
	<script src="../js/jgrowl/jquery.jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css" />
	<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" />

	<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
	<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
	<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>
	<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
</style>
<style type="text/css">
	.ui-datepicker{font-size: 0.9em;}
</style>
<!-- 遮罩效果 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '30%', 
			left:           '25%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script type="text/javascript">

$(function(){
	 $('#startTime').datetimepicker({
		    dateFormat: "yy-mm-dd",
			timeFormat: 'hh:mm:ss',
			showSecond: true,
			stepHour: 1,
			stepMinute: 1,
			stepSecond: 1
		});
	 $('#endTime').datetimepicker({
		 	dateFormat: "yy-mm-dd",
			timeFormat: 'hh:mm:ss',
			showSecond: true,
			stepHour: 1,
			stepMinute: 1,
			stepSecond: 1
		});
	 $("#ui-datepicker-div").css("display","none");

	 //初始化时区
	 var d = new Date();
	 var timeZone = -d.getTimezoneOffset()/60;
	 $("#timeZone").val(timeZone);
	 var year = d.getFullYear();
	 var month = d.getMonth()+1;
	 var day = d.getDate();
	 var hours = d.getHours();
	 var min = d.getMinutes();
	 var sec = d.getSeconds();
	 month = month > 9 ? month : "0"+month;
	 day = day > 9 ? day : "0"+day;
	 hours = hours > 9 ? hours : "0"+hours;
	 min = min > 9 ? min : "0"+min;
	 sec = sec > 9 ? sec : "0"+sec;
	 var startTime = year+"-"+month+"-"+day+" 00:00:00";
	 var endTime = year+"-"+month+"-"+day+" "+hours+":"+min+":"+sec;
	 $("#startTime").val(startTime);
	 $("#endTime").val(endTime);
});

function queryHistory(){
	var startTime = $("#startTime").val();
	var endTime = $("#endTime").val();
	var asset_id = <%=asset_id %>;
	var timeZone = $("#timeZone").val();
 	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/carsCommandsAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 30000,
			cache:false,
			data:{asset_id:asset_id,startTime:startTime,endTime:endTime,option:4,timeZone:timeZone},
			beforeSend:function(request){
		     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			error: function(e){
				$("#queryTip").html("System error.");
				$.unblockUI();
			},
			success: function(data){
				 $.unblockUI();
				if(data.length==0){
					$("#queryTip").html("No records in this time.");
			    }else{
					$("#queryTip").html("Found "+data.length+" records.");
					$.artDialog.opener.parseHistory(data);
				}
			}
		});
}
function historyPlay(){
	var flag = $.artDialog.opener.historyPlayAfterFitMapBounds();
	 if(flag==1){
	 	$.artDialog.close();
	 }else{
		 $("#queryTip").html("Please query record first");
	 }
}
 </script>
</head>
<body >
	  <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td colspan="2" align="center" valign="top">
				  <fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:80%;">
						<table >
							<tr style="display: none;">
								<td align="right" valign="middle">Time zone：</td>
							    <td align="left" valign="middle">
							    	<select class="txt" style="width: 100%;" id="timeZone" name="timeZone">
							    		<%
							    		for(int i=-12; i<=12; i++){
							    			String s = "";
							    			if(i>=0) s = "+0"+i;
							    			if(i>9) s = "+"+i;
							    			if(i<0) s = "-0"+(-i);
							    			if(i<-9) s = "-"+(-i);
							    			s = "GMT"+s+"00";
							    			out.println("<option value='"+i+"'>"+s+"</option>");
							    		}
							    		%>
							    	</select>
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Begin：</td>
							    <td align="left" valign="middle">
					              <input type="text" class="txt" id="startTime" name="startTime" /> 
					            </td>
					   	    </tr>
					   	    <tr>
					   	    	<td align="right" valign="middle">End： </td>
					                  		  
					            <td align="left" valign="middle">     		  
					              <input type="text" class="txt" id="endTime" name="endTime"" />
					            </td>
						   </tr>
						 </table>
					  </fieldset>
	           </td>
	        </tr>
	        <tr>
		   	      <td colspan="2" style="vertical-align: middle;">
		   	   		<span id="queryTip" style="color:red;"></span>
		   	   	  </td>
	   	   </tr>
		   <tr>
			    <td width="51%" align="right" valign="middle" class="win-bottom-line">&nbsp;</td>
    			<td width="49%" align="right" class="win-bottom-line">
				 <input type="button" name="Submit2"  class="short-short-button" value="Query"  onClick="queryHistory();">
				 <input type="button" name="Submit2"  class="short-button" value="Playback"  onClick="historyPlay();">
				</td>
	 	 </tr>
	   </table>
</body>
