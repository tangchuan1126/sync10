
<%@page import="com.cwc.util.StringUtil"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
String systenFolder = ConfigBean.getStringValue("systenFolder");
long psId = StringUtil.getLong(request, "ps_id");
String key = StringUtil.getString(request, "key");
String areaName = StringUtil.getString(request, "area_name").toUpperCase();
DBRow[] areas = googleMapsMgrCc.getAreaPositionByName(psId, areaName);
DBRow area = new DBRow();
String angle="";
if(areas.length>0){
	area = areas[0];
	angle=area.getString("angle");
	if(StringUtil.isBlank(angle)){
		angle="0";
	}
}
%>
<html>
  <head>
	<title>Modify zone</title>
    <!--  基本样式和javascript -->
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<script type="text/javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- jquery UI 加上 autocomplete -->
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	<script src="../js/maps/verify.js"></script>
		<!-- stateBox 信息提示框 -->
	<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
	<script type="text/javascript">
	var systenFolder = "<%=systenFolder%>";
	</script>
	 <script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '30%', 
			left:           '25%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
</script>
  </head>
  
  <body>
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td align="center" valign="top">
				  <fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:80%;">
				  	<form id="modify_area">
						<table >
							<tr>
								<td align="right" valign="middle" width="40%">Name：</td>
							    <td align="left" valign="middle" width="60%">
							    	<input type="text" style="width: 80%; background: " id="zone_name" name="zone_name" value="<%=areaName %>" onblur="this.value=this.value.trim()" onchange="verifyName('<%=psId%>',this.value,'5')">
							    	<input type="hidden"  id="zone_name_old" name="zone_name_old"  value="<%=areaName %>">
							    	<input type="hidden"  id="zone_id" name="zone_id"  value="<%=area.getString("area_id") %>">
							    	<input type="hidden"  id="ps_id" name="ps_id"  value="<%=psId %>">
							    	<img id="name_con" src="../imgs/maps/confirm.jpg" style="display: none;">
					            	<img id="name_del" title="Name is Repeat!" src="../imgs/maps/delete_red.jpg" style="display: none;">
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">X position：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 80%;" id="x" name="x" value="<%=area.getString("x")%>" onchange="this.value=this.value.trim(),verifyX()"/> 
					             <img id="x_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             <img id="x_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Y position：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 80%;" id="y" name="y" value="<%=area.getString("y")%>" onchange="this.value=this.value.trim(),verifyY()"/> 
					             <img id="y_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             <img id="y_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">X length：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 80%;" id="height" name="height" value="<%=area.getString("height")%>" onchange="this.value=this.value.trim(),verifyHeight()"/> 
					            <img id="height_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             <img id="height_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Y length：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 80%;" id="width" name="width" value="<%=area.getString("width")%>" onchange="this.value=this.value.trim(),verifyWidth()"/> 
					              <img id="width_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="width_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Offset angle：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 80%;" id="angle" name="angle" value="<%=angle%>" onchange="this.value=this.value.trim(),verifyAngle()"/> 
					              <img id="angle_con" src="../imgs/maps/confirm.jpg" style="display: none">
					              <img id="angle_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
					            </td>
					   	    </tr>
						 </table>
				  	</form>
				 </fieldset>
	           </td>
	        </tr>
	        <tr>
		   		<td style="text-align: center"><span style="color: red;" id="errInfo">&nbsp;</span></td>
	   	   </tr>
	   </table>
  </body>
  <script type="text/javascript">
  $(function(){
	//添加button
		var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
		var updateDialog = null;
		var addDialog = null;
		api.button([
				{
					name: 'Submit',
					callback: function () {
						save();
						return false;
					},
					focus:true,
					focus:"default"
				},
				{
					name: 'Cancel',
					callback:function (){
						closeWindow();
						return false;
					},
				}] 
			);
		var dialogId="area_info";
		var layer=$.artDialog.opener.jsmap.storageProvenLayer["<%=key%>"];
		layer.setVisible(false);
		$.artDialog.opener.dragStorageLayer(layer,dialogId,"<%=angle%>");
		
  });
  
  function convertCoordinateAjax(psId ,lat,lng){
		$.ajax({
			url:<%=ConfigBean.getStringValue("systenFolder")%>+'action/administrator/gis/convertCoordinate.action',
			data:'psId='+psId+'&lat='+lat+'&lng='+lng,
			dataType:'json',
			type:'post',
			//async:true, 
			beforeSend:function(request){
		    },
			success:function(data){
		    	if(data&&data.flag=="true"){
		        var x =data.x;
		        var y =data.y;
		    	$("#x").val(x);
		    	$("#y").val(y);
		    	}
			},
			error:function(){
			}
		});
		}
  	function save(){
  		var isChanged = ($("#x").val()!='<%=area.getString("x")%>'||
					  		   $("#y").val()!='<%=area.getString("y")%>'||
					  		   $("#height").val()!='<%=area.getString("height")%>'||
					  		   $("#width").val()!='<%=area.getString("width")%>'||
					  		   $("#angle").val()!='<%=angle%>') ? 'y':'n';
  		if(verifyMgs[verifyName]==0 || verifyMgs[verifyName]==2){
  			$("#zone_name").focus();
  			return ;	
  		}
  		if(verifyMgs[verifyX]==0 || verifyMgs[verifyX]==2){
  			$("#x").focus();
  			return;
  		}
  		if(verifyMgs[verifyY]==0 || verifyMgs[verifyY]==2){
  			$("#y").focus();
  			return;
  		}
  		if(verifyMgs[verifyWidth]==0 || verifyMgs[verifyWidth]==2){
  			$("#width").focus();
  			return;
  		}
  		if(verifyMgs[verifyHeight]==0 || verifyMgs[verifyHeight]==2){
  			$("#height").focus();
  			return;
  		}
  		if( verifyMgs[verifyAngle]==0 ||  verifyMgs[verifyAngle]==2){
  			$("#angle").focus();
  			return;
  		}
  		if($("#zone_name").val()!='<%=areaName%>'||isChanged=="y"){
  			$.ajax({
  				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/ModifyAreaAction.action',
  				data:$("#modify_area").serialize()+"&isChanged="+isChanged,
  				dataType:'json',
  				type:'post',
  				async:false, 
  				beforeSend:function(request){
				 	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
  				success:function(data){
  					$.unblockUI();
  					if(data && data.flag == 'true'){
  						$.artDialog.opener.modifyPositionLatlng(<%=psId%>,'<%=areaName%>',$("#zone_name").val(),data.latlng,5);
				  		closeWindow();
  					}else if(data && data.flag == 'authError'){
  						showMessage("Insufficient permissions!","error");
  					}else if(data && data.flag == 'false'){
  						$("#errInfo").html("Sava failed!");
  					}
  				},
  				error:function(){
  					$.unblockUI();
  					$("#errInfo").html("Sava failed!");
  				}
  			 });
  		}
  	}
  	
  	function cancel(){
  		closeWindow();
  	}
  	var verifyMgs={};
  	 function verifyName (psId,name,type){
  		  var oldName="<%=areaName %>";
  		  if(oldName!=name){
	  		 var vMsg='';
	  		 if(name==''){
	  			vMsg='Name is Empty!';
	  		 }
  		  	var data ={"ps_id":psId,"name":name,"type":type};
  		  	var url =systenFolder+'action/administrator/gis/verifyName.action';
  			verifyMgs[verifyName]=verify('zone_name','name_con','name_del','',url,data,vMsg);
  		  }else{
  			verifyMgs[verifyName]=1;
  			$("#name_con").show();
  			$("#name_del").hide();
  		  }
  	  }
    function verifyX(){
    	var vMsg='';
    	if($("#x").val()==''){
    		vMsg='x is Empty!';
    		/* $("#x_con").hide();
			  $("#x_del").show(); 
			  $("#x_del").attr("title","x is Empty!");
			  verifyMgs[verifyX]=0;*/
    	}else{
    		vMsg="Data format error";
    	}
    	/* else{ */
    		var reg = /^-?\d+(\.\d+)?$/; 
    	  	verifyMgs[verifyX]=verify('x','x_con','x_del',reg,'','',vMsg);
    	/* } */
  	
    }
    function verifyY(){
    	var vMsg='';
    	if($("#y").val()==''){
    		vMsg='y is Empty!';
    		/* $("#y_con").hide();
			  $("#y_del").show();
			  $("#y_del").attr("title","y is Empty!");
    		verifyMgs[verifyY]=0; */
    	}else{
    		vMsg="Data format error";
    	}
    	/* else{ */
    		var reg = /^-?\d+(\.\d+)?$/; 
    	  	verifyMgs[verifyY]=verify('y','y_con','y_del',reg,'','',vMsg);
    	/* } */
  	
    }
    function verifyWidth(){
    	var vMsg='';
    	if($("#width").val()==''){
    		vMsg='width is Empty!';
    		/* $("#width_con").hide();
			$("#width_del").show();
			$("#width_del").attr("title","width is Empty!");
  			verifyMgs[verifyWidth]=0; */
    	}else{
    		vMsg="Data format error";
    	}/* else{ */
    		var reg = /^-?\d+(\.\d+)?$/; 
    	  	verifyMgs[verifyWidth]=verify('width','width_con','width_del',reg,'','',vMsg);
    	/* } */
  	
    }
    function verifyHeight(){
    	var vMsg='';
    	if($("#height").val()==''){
    		vMsg='height is Empty!';
    		/* $("#height_con").hide();
			$("#height_del").show();
			$("#height_del").attr("title","height is Empty!");
  			verifyMgs[verifyHeight]=0; */
    	}else{
    		vMsg="Data format error";
    	}/* else{ */
    		var reg = /^-?\d+(\.\d+)?$/; 
    	  	verifyMgs[verifyHeight]=verify('height','height_con','height_del',reg,'','',vMsg);
    	/* } */
  	
    }
    function verifyAngle(){
    	var vMsg='';
    	if($("#angle").val()==''){
    		vMsg="angle is Empty!";
    		/* $("#angle_con").hide();
			$("#angle_del").show();
			$("#angle_del").attr("title","angle is Empty!");
  			verifyMgs[verifyAngle]=0; */
    	}else{
    		vMsg="Data format error";
    	}/* else{ */
    		var reg = /^-?\d+(\.\d+)?$/; 
    	  	verifyMgs[verifyAngle]=verify('angle','angle_con','angle_del',reg,'','',vMsg);
    	/* } */
  	 
    }
  	function dataValid(){
  		if($("#zone_name").val().trim() == ""){
  			$("#errInfo").html("Name cannot be empty.");
  			return false;
  		}
  	
  		return true;
  	}
  
  	document.onkeydown=function(evt){
  	  var evt=evt?evt:(window.event?window.event:null);//兼容IE和FF
  	  
  	  if (evt.keyCode==13){
  		  
  		  if($("#zone_name").val()!="<%=areaName%>"||
  		  		   $("#x").val()!="<%=area.getString("x")%>"||
  		  		   $("#y").val()!="<%=area.getString("y")%>"||
  		  		   $("#height").val()!="<%=area.getString("height")%>"||
  		  		   $("#width").val()!="<%=area.getString("width")%>"||
  		  		   $("#angle").val()!="<%=area.get("angle",0l)%>"){
  				save();
  			} else {
  				closeWindow();

  			}
  		}
  	}
	function closeWindow() {
		$.artDialog.close();
		$.artDialog.opener.jsmap.clearDemoLayer();
		$.artDialog.opener.layerSetMap('<%=key%>');
	}
	
	function fillValue(x,y){
		$("#width").val(x);
		$("#height").val(y);
	}
	function fillAngle(angle){
		$("#angle").val(angle);
	}
	
	function deleteArea(){
		var layers="area";
		var psId= $("#ps_id").val();
		var type ="5";
		var key ="area_"+$("#zone_name").val();
		var area_id =$("#zone_id").val();
		$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/StorageLocationDeleteAction.action',
				data:{"ps_id":psId,"type":"5","location_id":area_id},
				dataType:'json',
				type:'post',
				async:false, 
				beforeSend:function(request){
				 	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				success:function(data){
					if(data && data.flag == 'true'){
					$.unblockUI();
					$.artDialog.opener.clearStroageBoundsSingle(psId,key);
					$.artDialog.opener.jsmap.loadStorageLayer(psId,layers);
					}
				},
				error:function(){
					$("#errInfo").html("Sava failed!");
					$.unblockUI();
				}
			 });
		closeWindow();
	}
  </script>
</html>
