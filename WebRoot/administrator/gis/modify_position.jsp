<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<% 
		int type =StringUtil.getInt(request, "type");	
%>
<html>
  <head>
	<title>Modify data</title>
    <!--  基本样式和javascript -->
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<script type="text/javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- jquery UI 加上 autocomplete -->
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
  </head>
  
  <body>
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td align="center" valign="top">
				  <fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:80%;">
				  	<form id="modify_data">
						<table id="modityTable">
							<tr>
								<td align="right" valign="middle">X position：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 100%;" id="x" name="x" value="<%=data.getString("x")%>" onblur="this.value=this.value.trim()"/> 
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Y position：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 100%;" id="y" name="y" value="<%=data.getString("y")%>" onblur="this.value=this.value.trim()"/> 
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">X length：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 100%;" id="l" name="l" value="<%=data.getString("height")%>" onblur="this.value=this.value.trim()"/> 
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Y length：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 100%;" id="w" name="w" value="<%=data.getString("width")%>" onblur="this.value=this.value.trim()"/> 
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Offset angle：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 100%;" id="angle" name="angle" value="<%=data.get("angle",0l)%>" onblur="this.value=this.value.trim()"/> 
					            </td>
					   	    </tr>
						 </table>
				  	</form>
				 </fieldset>
	           </td>
	        </tr>
	        <tr>
		   		<td style="text-align: center"><span style="color: red;" id="errInfo">&nbsp;</span></td>
	   	   </tr>
		   <tr>
    			<td width="49%" align="right" class="win-bottom-line">
				 <input type="button" name="Submit2"  class="short-short-button" value="Save"  onClick="save()">
				 <input type="button" name="Submit2"  class="short-short-button" value="Cancel"  onClick="cancel()">
				</td>
	 	 </tr>
	   </table>
  </body>
  <script type="text/javascript">
  $(document).ready(
    function () {
    	
    	var html=null;
    	$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/modify.action',
				data:$("#modify_data").serialize()+"&type="+<%=type%>,
				dataType:'json',
				type:'post',
				beforeSend:function(request){
				},
				success:function(data){
					if( data.flag == 'true'){
						html=reatElement(type,data);
						$("#modityTable").html(html);
					}
				},
				error:function(){
					$("#errInfo").html("Sava failed!");
				}
			 });
    	
    });
	  function reatElement(type,data){
		var _data=data._data;
		var  html=null;
		if(type==1){
		html="<tr>"+
			"<td align='right' valign='middle'>Name：</td>"+
		    "<td align='left' valign='middle'>"+
		    "<input type='text' style='width: 100%; background: ' id='positionName' name='positionName' value='<%=positionName %>' onblur='this.value=this.value.trim()'>"+
		    "<input type='hidden'  id='positionName_old' name='positionName_old'  value='<%=positionName %>'>"+
		    "<input type='hidden'  id='position_id' name='position_id'  value='<%=position_id %>'>"+
		    "<input type='hidden'  id='ps_id' name='ps_id'  value='<%=psId %>'>"+
            "</td>"+
   	   		"</tr>";
			
		}
		  
		  
	  }
  	function save(){
  		var isChanged = ($("#x").val()!="<%=data.getString("x")%>"||
					  		   $("#y").val()!="<%=data.getString("y")%>"||
					  		   $("#l").val()!="<%=data.getString("height")%>"||
					  		   $("#w").val()!="<%=data.getString("width")%>"||
					  		   $("#angle").val()!="<%=data.get("angle",0l)%>") ? 'y':'n';
  		
  		if($("#positionName").val()!="<%=positionName%>"||
  		   $("#x").val()!="<%=data.getString("x")%>"||
  		   $("#y").val()!="<%=data.getString("y")%>"||
  		   $("#l").val()!="<%=data.getString("height")%>"||
  		   $("#w").val()!="<%=data.getString("width")%>"||
  		   $("#angle").val()!="<%=data.get("angle",0l)%>"){
  		if(dataValid()){
  			$.ajax({
  				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/modifyPosition.action',
  				data:$("#modify_data").serialize()+"&isChanged="+isChanged+"&type="+<%=type%>,
  				dataType:'json',
  				type:'post',
  				beforeSend:function(request){
  				},
  				success:function(data){
  					if(data && data.flag == 'true'){
  						$("#errInfo").html("Modify successed");
  						$.artDialog.opener.modifydataLatlng(<%=psId%>,'<%=positionName%>',$("#positionName").val(),data.latlng);
  					}
  					$.artDialog && $.artDialog.close();
  				},
  				error:function(){
  					$("#errInfo").html("Sava failed!");
  				}
  			 });
  		}
  	}
  	}
  	
  	function cancel(){
  		$.artDialog && $.artDialog.close();
  	}
  
  	function dataValid(){
  		if($("#positionName").val().trim() == ""){
  			$("#errInfo").html("Name cannot be empty.");
  			return false;
  		}
  		var reg = /^-?\d+(\.\d+)?$/;
  		if(!reg.test($("#x").val())){
  			$("#errInfo").html("X position must be number.");
  			return false;
  		}
  		if(!reg.test($("#y").val())){
  			$("#errInfo").html("Y position must be number.");
  			return false;
  		}
  		if(!reg.test($("#l").val())){
  			$("#errInfo").html("X length must be number.");
  			return false;
  		}
  		if(!reg.test($("#w").val())){
  			$("#errInfo").html("Y length must be number.");
  			return false;
  		}
  		if($("#angle").val() != ""){
	  		if(!reg.test($("#angle").val())){
	  			$("#errInfo").html("Offset angle must be number.");
	  			return false;
	  		}else{
	  			var angle = parseFloat($("#angle").val());
	  			if(angle>=90 || angle<=-90){
	  				$("#errInfo").html("Valid range of offset angle is [-90,90].");
	  	  			return false;
	  			}
	  		}
  		}
  		$("#errInfo").html(" ");
  		return true;
  	}
  
  	document.onkeydown=function(evt){
  	  var evt=evt?evt:(window.event?window.event:null);//兼容IE和FF
  	  
  	  if (evt.keyCode==13){
  		  
  		  if($("#zone_name").val()!="<%=positionName%>"||
  		  		   $("#x").val()!="<%=data.getString("x")%>"||
  		  		   $("#y").val()!="<%=data.getString("y")%>"||
  		  		   $("#l").val()!="<%=data.getString("height")%>"||
  		  		   $("#w").val()!="<%=data.getString("width")%>"||
  		  		   $("#angle").val()!="<%=data.get("angle",0l)%>"){
  				save();
  			} else {
  				closeWindow();

  			}
  		}
  	}
	function closeWindow() {
		$.artDialog.close();
	}
  </script>
</html>
