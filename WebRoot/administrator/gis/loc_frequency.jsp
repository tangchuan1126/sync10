<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
	long asset_id = StringUtil.getLong(request, "asset_id");
%>
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">

	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>

	<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>


	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
	
	<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";

--%>
@import "../js/thickbox/thickbox.css";
</style>

	<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
	<script src="../js/jgrowl/jquery.jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css" />
	<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" />

	<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
	<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
	<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>
	<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
<style type="text/css">
	td.topBorder{
		border-top: 1px solid silver;
	}
</style>
<style type="text/css">
	.ui-datepicker{font-size: 0.9em;}
</style>
<script type="text/javascript">
function dataValid(){
	var interval = $("#interval").val();
	var batch = $("#batch").val();
	var err = null;
	$("#errInfo").html("");
	if(batch == ""){
		err = "Batch cannot be empty,valid range [1-16]";
		return err;
	}else{
		var reg = batch.match(/\D+/);
		if(reg != null){
			err = "Valid range of batch is [1-16]";
			return err;
		}
		var i = parseInt(batch);
		if(i<1 || i>65535){
			err = "Valid range of batch is [1-16]";
			return err;
		}
	}
	if(interval == ""){
		err = "Interval cannot be empty,valid range [1-65535]";
		return err;
	}else{
		var reg = interval.match(/\D+/);
		if(reg != null){
			err = "Valid range of interval is [1-65535]";
			return err;
		}
		var i = parseInt(interval);
		if(i<1 || i>65535){
			err = "Valid range of interval is [1-65535]";
			return err;
		}
	}
	return err;
}
function saveCmd(){
	var err = dataValid();
	if(err!=null){
		$("#errInfo").html(err);
		return;
	}
	var asset = window.parent.$("#asset").data("asset_"+"<%=asset_id %>");
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/AddCmdAction.action',
		data:$("#loc_frequency").serialize()+"&aid="+asset.asset_id+"&call="+asset.asset_callnum+"&name="+asset.asset_name,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    	
		},
		success:function(data){
			$.artDialog.close();
		},
		error:function(){
			$("#errInfo").html("System error...");
		}
	 });
}
function cancel(){
	$.artDialog.close();
}
</script>
</head>
<body >
	  <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td align="center" valign="top">
				  <fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:80%;">
				  	<form id="loc_frequency">
						<table >
							<tr>
								<td align="right" valign="middle">Interval(S)：</td>
							    <td align="left" valign="middle">
							    	<input type="text" style="width: 100%; background: " id="interval" name="interval" value="120">
					            </td>
					   	    </tr>
							<tr>
								<td align="right" valign="middle">Batch：</td>
							    <td align="left" valign="middle">
					              <input type="text" style="width: 100%;" id="batch" name="batch" value="1"/> 
					            </td>
					   	    </tr>
						 </table>
				  	</form>
				 </fieldset>
	           </td>
	        </tr>
	        <tr>
		   		<td style="text-align: center"><span style="color: red;" id="errInfo">&nbsp;</span></td>
	   	   </tr>
		   <tr>
    			<td width="49%" align="right" class="win-bottom-line">
				 <input type="button" name="Submit2"  class="short-short-button" value="Confirm"  onClick="saveCmd();">
				 <input type="button" name="Submit2"  class="short-short-button" value="Cancel"  onClick="cancel();">
				</td>
	 	 </tr>
	   </table>
</body>
