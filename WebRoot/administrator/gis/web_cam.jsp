<%@page import="com.cwc.util.StringUtil"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String rtsp = StringUtil.getString(request, "rtsp");
%>
<html>
  <head>
    <title>仓库监控</title>
    <!--  基本样式和javascript -->
	<link type="text/css" href="../comm.css" rel="stylesheet" />
	<script type="text/javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- jquery UI 加上 autocomplete -->
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
    <script type="text/javascript">
	function isInsalledFFVLC(){
         var numPlugins=navigator.plugins.length;
		 var plu = "";
         for  (i=0;i<numPlugins;i++)
         {
              plugin=navigator.plugins[i];
			  plu += plugin.name+"\n";
              if(plugin.name.indexOf("VideoLAN") > -1 || plugin.name.indexOf("VLC") > -1)
            {            
                 return true;
            }
         }
		// alert(plu);
         return false;
    }

	function alertInfo(){
		var flag = isInsalledFFVLC();
		//alert(flag);
		var vlc = document.getElementById("vlc");
		if(flag){
			//var id=vlc.playlist.add("<%=rtsp %>");
			//vlc.playlist.playItem(id);
		}else{
			$("#vlc").remove();
			$("#playerInfo").show();
		}
	}
	function getVlcPlayer(){
		var url = "http://www.videolan.org/vlc";
		window.open(url);
	}
</script>
  </head>
  <body onload="alertInfo()">
    <!-- 
		<embed id="real" autostart="true" src="<%=rtsp %>" type="audio/x-pn-realaudio-plugin" width="100%" height="100%" controls="ImageWindow"> </embed> 
     -->
		<embed type="application/x-vlc-plugin" id="vlc"
			target="<%=rtsp %>"
			 height="100%" width="100%"
			 autoplay="yes" loop="no"
			 version="VideoLAN.VLCPlugin.2"/>
	  <div id="playerInfo" style="display: none;" align="center" >
	  	<span>Not install VLC player plugin</span>
	  	</br>
	  	<a href="javascript:" onclick="getVlcPlayer()">Get VLC media player</a>
	  </div>
  </body>
</html>
