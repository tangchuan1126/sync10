<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*"%>
<%@page import="com.cwc.util.*,com.cwc.app.util.*"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.util.StringUtil"%>
<%
	String id = StringUtil.getString(request, "id");
	String ip = StringUtil.getString(request, "ip");
	String port = StringUtil.getString(request, "port");
	String username = StringUtil.getString(request, "username");
	String password = StringUtil.getString(request, "password");
	String inner_radius = StringUtil.getString(request, "inner_radius");
	String outer_radius = StringUtil.getString(request, "outer_radius");
	String x = StringUtil.getString(request, "x");
	String y =StringUtil.getString(request, "y");
	String ps_id =StringUtil.getString(request, "ps_id");
	String s_degree =StringUtil.getString(request, "s_degree");
	String e_degree =StringUtil.getString(request, "e_degree");
	String pageType =StringUtil.getString(request, "pageType","1");
	String lat =StringUtil.getString(request,"lat","");
	String lng =StringUtil.getString(request,"lng","");
	DBRow[] allStorageKml = googleMapsMgrCc.getAllStorageKml();
%>
<head>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript"
	src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript"
	src="../js/scrollto/jquery.scrollTo-min.js"></script>


<script type="text/javascript"
	src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css"
	type="text/css" />

<link rel="stylesheet"
	href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css"
	type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/maps/verify.js" type="text/javascript"></script>
<style type="text/css">
td.topBorder {
	border-top: 1px solid silver;
}
</style>
<style type="text/css">
.ui-datepicker {
	font-size: 0.9em;
}
</style>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
	$.blockUI.defaults = {
			css: { 
				padding:        '8px',
				margin:         0,
				width:          '170px', 
				top:            '30%', 
				left:           '25%', 
				textAlign:      'center', 
				color:          '#000', 
				border:         '3px solid #999999',
				backgroundColor:'#eeeeee',
				'-webkit-border-radius': '10px',
				'-moz-border-radius':    '10px',
				'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
				'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			},
			//设置遮罩层的样式
			overlayCSS:  { 
				backgroundColor:'#000', 
				opacity:        '0.6' 
			},
			baseZ: 99999, 
			centerX: true,
			centerY: true, 
			fadeOut:  1000,
			showOverlay: true
		};
</script>
<script type="text/javascript">
$(function () { 
	//添加button
	var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	var updateDialog = null;
	var addDialog = null;
	api.button([
			{
				name: 'Submit',
				callback: function () {
					commitData();
					return false;
				},
				focus:true,
				focus:"default"
			},
			{
				name: 'Cancel',
				callback:function (){
					closeWindow('<%=id%>','cancel');
					return false;
				},
			}] 
		);
	if("<%=lat%>"!="" && "<%=lng%>"!=""){
	convertCoordinateAjax(<%=ps_id%>,"<%=lat%>","<%=lng%>");
	}
	if("<%=ps_id%>"!=""){
	$("#storage").val("<%=ps_id%>");
	}else{
	$("#storage option:first").prop("selected", 'selected');
	}
	
	
});
function fillAngle(a,b){
	$("#s_degree").val(a);
	$("#e_degree").val(b);
}
function fillRadius(a,b){
	$("#inner_radius").val(a);
	$("#outer_radius").val(b);
}
function convertCoordinateAjax(psId,lat,lng){
	$.ajax({
		url:<%=ConfigBean.getStringValue("systenFolder")%>+'action/administrator/gis/convertCoordinate.action',
		data:'psId='+psId+'&lat='+lat+'&lng='+lng,
		dataType:'json',
		type:'post',
		
		
		async:true, 
		beforeSend:function(request){
	    },
		success:function(data){
	    	if(data&&data.flag=="true"){
	        var x =data.x;
	        var y =data.y;
	    	$("#x").val(x);
	    	$("#y").val(y);
	    	}
		},
		error:function(){
		}
	});
	}
	function closeWindow(camparameterId,flag) {
		var pageType='<%=pageType%>';
		if(pageType=="0"){
		$.artDialog.opener.jsmap.clearDomeWebcam('<%=ps_id%>')
		}
		$.artDialog.opener.reLoadWebcam('<%=ps_id%>',camparameterId,flag);
		$.artDialog.opener.reSetObjectDraggable(6,<%=ps_id%>,<%=id%>);
		$.artDialog.close();
	}
	function updateData(){
     var objIp = document.getElementById("ip");
     var objPort = document.getElementById("port");
     if(!objIp.value){
    	 verifyIP();
     }
     if(!objPort.value){
    	 verifyPort();
     }
     
   	 if(verifyMgs['verifyIP']==0 || verifyMgs['verifyIP']==2){
   		 objIp.focus();
   		 return; 
   	 }
   	 if(verifyMgs['verifyPort']==0 || verifyMgs['verifyPort']==2){
   		 objPort.focus();
   		 return;
   	 }
   	 if(verifyMgs[verifyX]==0 || verifyMgs[verifyX] ==2){
   		 $("#x").focus();
   		return;
   	 }
   	 if(verifyMgs[verifyY]==0 || verifyMgs[verifyY]==2){
   		$("#y").focus();
   		return;
   	 }
   	 if( verifyMgs[verfyInner_radius]==0 ||  verifyMgs[verfyInner_radius]==2){
   		 $("#inner_radius").focus();
   		 return;
   	 }
   	if( verifyMgs[verfyInner_radius]==0 ||  verifyMgs[verfyInner_radius]==2){
  		 $("#outer_radius").focus();
  		 return;
  	 }
   	if( verifyMgs[verfyS_degree]==0 ||  verifyMgs[verfyS_degree]==2){
 		 $("#s_degree").focus();
 		 return;
 	 }
   	if( verifyMgs[verfyS_degree]==0 ||  verifyMgs[verfyS_degree]==2){
		 $("#e_degree").focus();
		 return;
	 }
    	 var ischanged =0;
    	 if($("#x").val()!="<%=x%>" 
    			 || $("#y").val()!="<%=y%>" 
    			 ||$("#inner_radius").val()!="<%=inner_radius%>"
    			 ||$("#outer_radius").val()!="<%=outer_radius%>"
    			 ||$("#s_degree").val()!="<%=s_degree%>"
    			 ||$("#e_degree").val()!="<%=e_degree%>"){
    		 ischanged=1;
    	 }
    	 var camparameter_data={};
    	 $.each($("#webparameter").serializeArray(),function(i,field){
    		 camparameter_data[field.name]=field.value;
    		 if(field.name=='username'){
    			 camparameter_data['user']=field.value;
    		 }
    	 })
    	 camparameter_data['ps_id']='<%=ps_id%>';
    	 $.ajax({
    	     url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/camparameter.action',
    		type:'post',
    		dataType:'json',
    		data:$("#webparameter").serialize()+'&ischanged='+ischanged+'&pageType='+<%=pageType%>+'&ps_id='+$("#storage").val(),
    		async:false, 
    		beforeSend:function(request){
			 	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
    		success:function(data){
    			$.unblockUI();
    			if(data && data.flag=="true"){
    				showMessage("Save succeed","succeed");
    				if(data.id){
    					camparameter_data.id=data.id;
    				}
    				camparameter_data['latlng']=data.latlng;
    				var camparameter_data1=$.artDialog.opener.jsmap.storageBounds[camparameter_data.ps_id+'_webcam'];
    				if(!camparameter_data1 ){
						$.artDialog.opener.jsmap.storageBounds[camparameter_data.ps_id+'_webcam']=[camparameter_data];
					}else{
						var isExist=false;
						for(var i=0;i<camparameter_data1.length;i++){
							if(camparameter_data1[i].id==camparameter_data.id){
								camparameter_data1[i]=camparameter_data;
								isExist=true;
							}
						}
						if(!isExist){
							camparameter_data1[camparameter_data1.length]=camparameter_data;
						}
					} 
				    closeWindow(camparameter_data.id,'add_or_update');
    			}else if(data && data.flag=="authError"){
    				showMessage("Insufficient permissions!","error");
    			}
    		},
    	  error:function(){
    		  $.unblockUI();
    		showMessage("System error","error");
    	  }
          });
	}
	
	function commitData(){
		  if($("#ip").val()!="<%=ip%>" || $("#port").val()!="<%=port%>" ||$("#username").val()!="<%=username%>"||$("#password").val()!="<%=password%>"||$("#x").val()!="<%=x%>" || $("#y").val()!="<%=y%>" ||$("#inner_radius").val()!="<%=inner_radius%>"||$("#outer_radius").val()!="<%=outer_radius%>"||$("#s_degree").val()!="<%=s_degree%>"||$("#e_degree").val()!="<%=e_degree%>"||$("#storage").val()!="<%=ps_id%>"){
			  updateData(); 
		  }else{
		      closeWindow();  
		  }
		
	}
	document.onkeydown=function(evt){
		  var evt=evt?evt:(window.event?window.event:null);//兼容IE和FF
		  if (evt.keyCode==13){
			  commitData();
			}
	}
	var verifyMgs={};
	function verifyPort(){
		var vMsg="";
		if($('#port').val()==''){
			vMsg="Port is empty!";
		}else{
			vMsg="Data format error";
		}
			var regInt = new RegExp("^([1-9][0-9]*)$");
			 var v_flag=verify('port','VPortRight','VPortError',regInt,'','',vMsg);
			 verifyMgs['verifyPort']=v_flag;
	}
	function verifyIP(){
		var vMsg="";
		if($('#ip').val()==''){
			vMsg="IP is empty!";
		}else{
			vMsg="Data format error";
		}
			var regIp = new RegExp("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
			var v_flag=verify('ip','VIPRight','VIPError',regIp,'','',vMsg);
			verifyMgs['verifyIP']=v_flag;
	}
	function verifyX(){
		  var vMsg="";
		  if($("#x").val()==''){
			  vMsg="x is Empty!";
	  	}else{
	  		vMsg="Data format error";
	  	}
		  var reg = /^-?\d+(\.\d+)?$/; 
		  verifyMgs[verifyX]=verify('x','x_con','x_del',reg,'','',vMsg);
	  }
	  function verifyY(){
		  var vMsg="";
		  if($("#y").val()==''){
			  vMsg="y is Empty!";
	  	}else{
	  		vMsg="Data format error";
	  	}
		  var reg = /^-?\d+(\.\d+)?$/; 
		  verifyMgs[verifyY]=verify('y','y_con','y_del',reg,'','',vMsg);
	  }
	  function verfyInner_radius(){
		  var vMsg="";
		  if($("#inner_radius").val()==''){
			  vMsg="inner_radius is Empty!";
	  	}else{
	  		vMsg="Data format error";
	  	}
		  var reg = /^-?\d+(\.\d+)?$/; 
		  verifyMgs[verfyInner_radius]=verify('inner_radius','inner_radius_con','inner_radius_del',reg,'','',vMsg);
	  }
	  function verfyOuter_radius(){
		  var vMsg="";
		  if($("#outer_radius").val()==''){
			  vMsg="inner_radius is Empty!";
	  	}else{
	  		vMsg="Data format error";
	  	}
		  var reg = /^-?\d+(\.\d+)?$/; 
		  verifyMgs[verfyInner_radius]=verify('outer_radius','outer_radius_con','outer_radius_del',reg,'','',vMsg);
	  }
	  function verfyS_degree(){
		  var vMsg="";
		  if($("#s_degree").val()==''){
			  vMsg="inner_radius is Empty!";
	  	}else{
	  		vMsg="Data format error";
	  	}
		  var reg = /^-?\d+(\.\d+)?$/; 
		  verifyMgs[verfyS_degree]=verify('s_degree','s_degree_con','s_degree_del',reg,'','',vMsg);
	  }
	  function verfyE_degree(){
		  var vMsg="";
		  if($("#e_degree").val()==''){
			  vMsg="inner_radius is Empty!";
	  	}else{
	  		vMsg="Data format error";
	  	}
		  var reg = /^-?\d+(\.\d+)?$/; 
		  verifyMgs[verfyS_degree]=verify('e_degree','e_degree_con','e_degree_del',reg,'','',vMsg);
	  }
</script>
</head>
<body>
	<form id="webparameter">
		<table width="100%" height="100%" border="0" cellpadding="0"
			cellspacing="0"  onchange="" >
			<tr>
				<td colspan="4" align="center" valign="top">
			
						<table>
						<tr style="display:none;">
						<th width="45%" colspan="2">Storage:</th>
						<td width="55%" colspan="2">
						<select id="storage" style="width: 100%">
						<%
					if (allStorageKml != null) {
						for (int i = 0; i < allStorageKml.length; i++) {
							DBRow kml = allStorageKml[i];
				         %>
						<option value="<%=kml.getString("ps_id")%>"><%=kml.getString("title")%></option>
						<%
                         }
					}  
                         %>
						</select>
						</td>
						</tr>
						
							<tr>
								<td align="right" valign="ip" width="20%">IP：</td>
								<td align="left" valign="ip" width="30%" style="position: relative;"><input type="text"
									style="width: 85%;" name="ip" id="ip" value="<%=ip%>" onblur="verifyIP()"/>
									<span  style="position: absolute;top: 5px;height: 16px;width:16px;">
									<img  title="IP is incorrect, please re-enter" src="../imgs/maps/delete_red.jpg" id="VIPError" style="display: none;">
									<img  src="../imgs/maps/confirm.jpg" id="VIPRight" style="display: none;">
									</span>
								</td>
								<td align="right" valign="port" width="20%">Port：</td>
								<td align="left" valign="port" width="30%" style="position: relative;"><input type="text"
									style="width: 85%;" name="port" id="port" value="<%=port%>" onblur="verifyPort()"/>
									<span  style="position: absolute;top: 5px;height: 16px;width:16px;">
									<img  title="Port is incorrect, please re-enter" src="../imgs/maps/delete_red.jpg" id="VPortError" style="display: none;">
									<img  src="../imgs/maps/confirm.jpg" id="VPortRight" style="display: none;">
									</span>
								</td>
							</tr>
							<tr>
								<td align="right" valign="username">Username：</td>
								<td align="left" valign="username"><input type="text"
									style="width: 85%;" name="username" id="username"
									value="<%=username%>" />
								</td>
								<td align="right" valign="password">Password：</td>

								<td align="left" valign="password"><input type="text"
									style="width: 85%;" name="password" id="password"
									value="<%=password%>" />
								</td>
							</tr>
							<tr>
								<td align="right" valign="x">X：</td>
								<td align="left" valign="x" style="position: relative;"><input type="text"
									style="width: 85%;" name="x" id="x"
									value="<%=x%>"  onchange="verifyX()" />
									<span style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img id="x_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             		<img id="x_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
									</span>
								</td>
								<td align="right" valign="y">Y：</td>
								<td align="left" valign="y" style="position: relative;"><input type="text"
									style="width: 85%;" name="y" id="y"
									value="<%=y%>"  onchange="verifyY()" />
									<span style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img id="y_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             		<img id="y_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
									</span>
								</td>
							</tr>
							<tr>
								<td align="right" valign="inner_radius">Inner_radius：</td>
								<td align="left" valign="inner_radius" style="position: relative;"><input type="text"
									style="width: 85%;" name="inner_radius" id="inner_radius"
									value="<%=inner_radius%>" onchange="verfyInner_radius()"/>
									<span style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img id="inner_radius_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             		<img id="inner_radius_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
									</span>
								</td>
								<td align="right" valign="outer_radius">Outer_radius：</td>
								<td align="left" valign="outer_radius" style="position: relative;"><input type="text"
									style="width: 85%;" name="outer_radius" id="outer_radius"
									value="<%=outer_radius%>"   onchange="verfyOuter_radius()"/>
									<span style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img id="outer_radius_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             		<img id="outer_radius_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
									</span>
								</td>
								
							</tr>
							<tr>
								<td align="right" valign="s_degree">S_degree：</td>
								<td align="left" valign="s_degree" style="position: relative;"><input type="text"
									style="width: 85%;" name="s_degree" id="s_degree"
									value="<%=s_degree%>"   onchange="verfyS_degree()"/>
									<span style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img id="s_degree_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             		<img id="s_degree_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
									</span>
								</td>
								<td align="right" valign="e_degree">E_degree：</td>
								<td align="left" valign="e_degree" style="position: relative;"><input type="text"
									style="width: 85%;" name="e_degree" id="e_degree"
									value="<%=e_degree%>" onchange="verfyE_degree()"/>
									<span style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img id="e_degree_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             		<img id="e_degree_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
									</span>
								</td>
								
							</tr>
						</table>
				</td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: center"><span
					style="color: red;" id="errInfo">&nbsp;</span></td>
			</tr>
			<input name="id" type="hidden" value="<%=id%>" />
		</table>
	</form>
</body>
