<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
long psId= StringUtil.getLong(request,"ps_id");
String area_id= StringUtil.getString(request,"area_id");
String area_name =StringUtil.getString(request,"area_name");
DBRow[] zonePerson=null;
if(area_id!=null && !area_id.equals("")){
	zonePerson=googleMapsMgrCc.getAreaPersonByPsIdandArea(psId,Long.parseLong(area_id));
}else{
	DBRow[] areas = googleMapsMgrCc.getAreaPositionByName(psId, area_name);
	DBRow area = new DBRow();
	if(areas.length>0){
		area = areas[0];
		area_id=area.getString("area_id");
		zonePerson=googleMapsMgrCc.getAreaPersonByPsIdandArea(psId,Long.parseLong(area_id));
	}
}

String adids="";
if(zonePerson != null && zonePerson.length>0){
	for(int i=0; i<zonePerson.length; i++){
		DBRow d = zonePerson[i];
		if(!adids.equals("")){
			adids+=","+d.getString("adid");
		}else{
			adids+=d.getString("adid");
		}
	}
}
%>
<html>
  <head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入提示信息 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<style type="text/css">
.text-overflow {
	display:block;
	word-break:keep-all;
	white-space:nowrap;
	overflow:hidden;
	text-overflow:ellipsis;
}
#menu_ {
	width: 150px;
	position: absolute;
	display:none;
	background: #ddd;
	border: solid #ccc 1px;
}

#menu_ ul {
	list-style: none;
	margin: 0px;
	padding: 0px;
}

#menu_ ul li a {
	margin: 0px;
	padding: 5px;
	text-decoration: none;
	display: inline-block;
	height: 20px;
	width: 120px;
	color: #333;
	font-size: 13px;
}

#menu_ ul li a:hover {
	background: #eee;
	border: solid #fff 1px;
	height: 18px;
}
</style>
  </head>
  <body>
  <div id="container" onclick="hideMenu()" >
  	<div id="zonePersons" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;height: 95%;" tabindex='0'>
	    <table style="width: 100%;height: 95%;">
	    	<tr>
	    		<td>
					<div id="notExitDoor" style="background-color:#FFF; width: 100%; border:2px #dddddd solid; height:100%; overflow:auto" >
					<%
						if(zonePerson != null && zonePerson.length>0){
							for(int i=0; i<zonePerson.length; i++){
								DBRow d = zonePerson[i];
								%>
								<div title="<%=d.getString("employe_name") %>" style="width: 110px; height: 25px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;" >
									<div class="text-overflow" style="width: 100%;height:100%; float: left; text-align: center;background-color: #CCFFFF;position: relative;" onmouseover="this.style.backgroundColor='#2DF7AB'" onmouseout="this.style.backgroundColor='#CCFFFF'" oncontextmenu="showMenu(this,event)">
										<%=d.getString("employe_name") %>
										<input type="hidden" class="adid" name="adid" value="<%=d.getString("adid") %>">
										<div style="width: 12px; height: 12px; float: right;position:absolute;right:0px;top:0px; margin: 1px; cursor: pointer; background-image: url(http://localhost:8080/Sync10/administrator/imgs/maps/delete_grey.png);" onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/delete_blue.png&quot;)'" onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/delete_grey.png&quot;)'" onclick="removeTitle(this.parentNode)">
										</div>
									</div>
								</div>
								<%
							}
						}
					%>
					<div title="add person" id="add_person" style="width: 110px; height: 25px; border: solid; border-color: #eeeeee; border-width: 1px; margin: 5px; float: left; cursor: default;" >
									<div class="text-overflow" style="width: 100%;height:100%; float: left; text-align: center;background-color: #eeeeee;cursor: pointer;" onclick="selectPerson()">
										<div id="add_bg_img" style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_blue.png'); margin: 1px; cursor: pointer;margin-top: 8px;margin-right: 50px;" 
												onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'" 
												onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'"
												">
										</div>
									</div>
					</div>
				</td>
	    	</tr>
	    </table>
	</div>
	</div>
		<!-- 右键菜单样式 -->
		<div id="menu_" style="width:auto;">
			<ul>
				<li id="modifyZone" onclick="assignArea()" ><a
					href="#">Reassign </a></li>
			</ul>
		</div>
  </body>
<script type="text/javascript">
var adid,employe_name,eleNode,oldAdids='<%=adids%>';
//注册键盘事件
document.onkeydown = function(e) {
    //捕捉回车事件
    var ev = (typeof event!= 'undefined') ? window.event : e;
    if(ev.ctrlKey&& ev.keyCode==65){
    	selectPerson();
    	return false;
    }else if(ev.ctrlKey && ev.keyCode==83){
    	commitData();
    	return false;
    }else if(ev.ctrlKey&& ev.keyCode==67){
    	closeWindow();
    	return false;
    }
}
$(document).ready(function(){
//屏蔽鼠标右键菜单
	$(document).find("#zonePersons").bind("contextmenu",function(e){
		return false;
	});
	var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	var updateDialog = null;
	var addDialog = null;
	api.button([
			{
				name: 'Submit',
				callback: function () {
					commitData();
					return false;
				},
				focus:true,
				focus:"default"
			},
			{
				name: 'Cancel'
				
				
			}] 
		);
	function cancel(){$.artDialog && $.artDialog.close();} 
	/* $(document).find("#menu").bind("contextmenu",function(e){
		return false;
	}); */
});
function closeWindow() {
	$.artDialog.close();
}
function commitData(){
	var adidNodes=document.getElementsByName("adid");
	var adids="";
	var contrastFlag=false;
	if(adidNodes.length>0){
		for(var i=0;i<adidNodes.length;i++){
			if($.inArray(adidNodes[i].value,oldAdids.split(','))<0){
				contrastFlag=true;
			}else if(oldAdids.split(',').length>adidNodes.length){
				contrastFlag=true;
			}
			if(adids!=""){
				adids+=","+adidNodes[i].value;
			}else{
				adids+=adidNodes[i].value;
			}
		}
	}else{
		contrastFlag=true;
	}
	if(contrastFlag){
		$.ajax({
			data:'adids='+adids+'&areaId=<%=area_id%>&oldAdids='+oldAdids,
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/zonePersonHaldeAction.action',
			dataType:'json',
			type:'post',
			beforeSend:function(request){
			 	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
				$.unblockUI();
				if(data && data.flag=="true"){
					showMessage("Save succeed","succeed");
					setTimeout('closeWindow()',1500);
					$.artDialog.opener.reloadPerson(<%=psId%>);
				}
			},
	   	 	error:function(){
	   	 		$.unblockUI();
	   			showMessage("System error","error");
	   	  	}
		});
	}else{
		showMessage("didn't any change","error");
		setTimeout('closeWindow()',1500);
	}
	
	
}

function selectPerson(){
	var adidNodes=document.getElementsByName("adid");
	var adids="";
	for(var i=0;i<adidNodes.length;i++){
		if(adids!=""){
			adids+=","+adidNodes[i].value;
		}else{
			adids+=adidNodes[i].value;
		}
	}
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/select_person.html?psId=<%=psId%>&adids='+adids;
	$.artDialog.open(url, {title: "Select person ["+'<%=area_name%>'+"]",width:'825px',height:'435px', lock: true,opacity: 0.3});
}
//回填persons
function backFillPerson(names, ids){
	var names_=names.split(',');
	var ids_=ids.split(',');
	for(var i=0;i<names_.length;i++){
		var node="<div title='"+names_[i]+"' style='width: 110px; height: 25px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;' >"+
		"<div class='text-overflow' style='width: 100%;height:100%; float: left; text-align: center;background-color: #CCFFFF;position: relative;' onmouseover=\"this.style.backgroundColor='#2DF7AB'\" onmouseout=\"this.style.backgroundColor='#CCFFFF'\" >"+
		names_[i]+"<input type='hidden' name='adid' class='adid' value='"+ids_[i]+"'>"+
		"<div style='width: 12px; height: 12px; float: right;position:absolute;right:0px;top:0px; margin: 1px; cursor: pointer; background-image: url(http://localhost:8080/Sync10/administrator/imgs/maps/delete_grey.png);' onmousemove=\"this.style.backgroundImage='url(&quot;../imgs/maps/delete_blue.png&quot;)'\" onmouseout=\"this.style.backgroundImage='url(&quot;../imgs/maps/delete_grey.png&quot;)'\" onclick=\"removeTitle(this.parentNode)\"></div>"+
		"</div> </div>";
		$("#add_person").before(node);
		
	}
	$("#zonePersons").focus();
}
//移除除员工
function removeTitle(ele){
	var info='Is it really will be removed '+ele.parentNode.title+' from <%=area_name%>';
	maskLayer(ele,info);
}
//显示右键菜单
function showMenu(ele,event){
	eleNode=ele;
	employe_name=ele.childNodes[0].data.trim();
	adid=ele.childNodes[1].value;
	var evt = event || window.event;
	var container = document.getElementById('zonePersons'); 
	var rightedge = container.clientWidth-evt.clientX;
    var bottomedge = container.clientHeight-evt.clientY;
    var menu = document.getElementById('menu_');
	var display=$('#menu_').css('display');
	if(display='inherit'){
		$('#menu_').css('display','none');
	}
	display=$('#menu_').css('display');
	if(display=='none'){
		$('#menu_').css('display','inherit');
		if (rightedge < menu.offsetWidth)
			 menu.style.left = container.scrollLeft + evt.clientX - menu.offsetWidth + "px";
		else
			 menu.style.left = container.scrollLeft + evt.clientX + "px";
		 if (bottomedge < menu.offsetHeight)
	            menu.style.top = container.scrollTop + evt.clientY - menu.offsetHeight + "px";
        else
            menu.style.top = container.scrollTop + evt.clientY + "px";
	}
}
/*隐藏菜单*/
function hideMenu() {
	$('#menu_').css('display','none');
} 
/*指派工作区域*/
 function assignArea(){
	var area_name='<%=area_name%>';
	var psId='<%=psId%>';
	var type="-1"
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_area_single.html?ps_id='+psId+'&area_type='+type+'&employe_name='+employe_name+'&area_name='+area_name;
	$.artDialog.open(url, {title: "Select Area ["+'<%=area_name%>'+"]",width:'790px',height:'425px', lock: true,opacity: 0.3});
	hideMenu();
}
 /**
  * 刷新person
  */
  function reloadZonePerson(el){
	 if(oldAdids.indexOf(adid)==0){
		 oldAdids=oldAdids.substring(adid.length+1,oldAdids.length);
	 }else{
		 oldAdids=oldAdids.substring(0,oldAdids.indexOf(adid)-1)+oldAdids.substring((oldAdids.indexOf(adid)+adid.length),oldAdids.length);
	 }
	 $.ajax({
     		data:'areaId='+$(el).find("#area_id").val()+'&adid='+adid,
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/assignWorkAreaAction.action',
			dataType:'json',
			type:'post',
     		success:function(data){
     			if(data && data.flag=="true"){
     				showMessage("Distribution of success","succeed");
     				$(eleNode.parentNode).remove();
     			 	 $.artDialog.opener.reloadPerson(<%=psId%>);
     			}
     		},
     		error:function(){
     			showMessage("System error","error");
  		}
     	}); 
	  
 }
  /**
   * 遮罩层
   */
   function maskLayer(el,info){
  	 $.artDialog({
  		 	title:'Notify',
  		 	lock: true,
  		 	opacity: 0.3,
  		 	icon:'question',
   		    content: "<span style='font-weight:bold;'>"+info+"?</span>",
  		    button: [
  		        {
  		            name: 'YES',
  		            callback: function () {
  		            	$(el.parentNode).remove();
    		            },
  		            focus: true
  		        },
  		        {
  		            name: 'NO'
  		        }
  		    ]
  		});
   }
</script>
</html>