<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.util.StringUtil"%>
<%
	long ic_id = StringUtil.getLong(request, "ic_id");
	long staging_id =StringUtil.getLong(request, "staging_id");
	DBRow[] showDatas = googleMapsMgrCc.selectContainerPlates(ic_id,staging_id);
	DBRow[] ctnrRows = googleMapsMgrCc.distinctContainerPlates(ic_id,staging_id);
%>
<html>
<head>
<!-- 基本css 和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

   
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
 
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
 
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
body{
 overflow: hidden;
}
</style>
<style type="text/css">
.ui-datepicker {
	font-size: 0.9em;
}
</style>
<script type="text/javascript">
$(function(){
	onLoadInitZebraTable();	
	
})
</script>
</head>
<body>
		
		<%
		  if(ctnrRows.length>0){
				for(int i=0;i<ctnrRows.length;i++){
					DBRow row=ctnrRows[i];
					long outer_icId=row.get("ic_id", 0l);
							%>
						 <fieldset style="border:2px blue solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
						 <legend style="font-size:15px;font-weight:bold;"> 
							<%=row.getString("ctnr") %>
						 </legend>
						 <table style="width:100%;border: 1;" class="zebraTable" >
							<tr >
								<td style="height:25px; line-height:25px; background-color:#000000; text-align: center;font-size: 12;font-weight: bold;color: #FFFFFF">Plate_no</td>
								<td style="height:25px; line-height:25px; background-color:#000000; text-align: center;font-size: 12;font-weight: bold;color: #FFFFFF">Box_number</td>
							</tr>
						<% if(showDatas.length>0){
							for(int j=0;j<showDatas.length;j++){
								DBRow showRow=showDatas[j];
							    long inner_icId =showRow.get("ic_id", 0l);
								if(outer_icId==inner_icId){
							%> 
						 <tr>
						 <td style="text-align: center;"><%=showRow.getString("plate_no") %></td>
						 <td style="text-align: center;"><%=showRow.getString("box_number") %></td>
						 </tr>
					 <%				}
								}
							}%>
					
						 </table>
						 </fieldset>	
					<%	
					}
				}
				%>
	
</body>
</html>
