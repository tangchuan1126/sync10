<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*"%>
<%@page import="com.cwc.util.*,com.cwc.app.util.*"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.util.StringUtil"%>
<%
	long id = StringUtil.getLong(request, "id");
	long psId = StringUtil.getLong(request, "ps_id");
	String name = StringUtil.getString(request, "name");
    String status =StringUtil.getString(request,"status");
	String x = StringUtil.getString(request, "x");
	String y =StringUtil.getString(request, "y");
	String lat =StringUtil.getString(request,"lat","");
	String lng =StringUtil.getString(request,"lng","");
	String pageType =StringUtil.getString(request, "pageType","1");
%>
<head>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript"
	src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript"
	src="../js/scrollto/jquery.scrollTo-min.js"></script>


<script type="text/javascript"
	src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css"
	type="text/css" />
<link rel="stylesheet"
	href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css"
	type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script src="../js/maps/verify.js" type="text/javascript"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<style type="text/css">
td.topBorder {
	border-top: 1px solid silver;
}
</style>
<style type="text/css">
.ui-datepicker {
	font-size: 0.9em;
}
</style>
<script type="text/javascript">
$(function () { 
	//添加button
	var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
	var updateDialog = null;
	var addDialog = null;
	api.button([
			{
				name: 'Delete',
				callback: function () {
					doDelete('<%=name%>','<%=id%>','<%=psId%>');
					return false;
				},
			},
			{
				name: 'Submit',
				callback: function () {
					commitData();
					return false;
				},
				focus:true,
				focus:"default"
			},
			{
				name: 'Cancel',
				callback:function (){
					closeWindow('<%=id%>','cancel');
					return false;
				},
			}] 
		);
	if("<%=lat%>"!="" && "<%=lng%>"!=""){
		convertCoordinateAjax("<%=psId%>","<%=lat%>","<%=lng%>");
	}
	$("select[name='type']").val('<%=status%>')
	
});
function convertCoordinateAjax(psId ,lat,lng){
	$.ajax({
		url:<%=ConfigBean.getStringValue("systenFolder")%>+'action/administrator/gis/convertCoordinate.action',
		data:'psId='+psId+'&lat='+lat+'&lng='+lng,
		dataType:'json',
		type:'post',
		async:true, 
		beforeSend:function(request){
	    },
		success:function(data){
	    	if(data&&data.flag=="true"){
	        var x =data.x;
	        var y =data.y;
	    	$("#x").val(x);
	    	$("#y").val(y);
	    	}
		},
		error:function(){
		}
	});
	}
function closeWindow(lightId,flag) {
	var pageType ='<%=pageType%>';
	if(pageType=='0'){
	$.artDialog.opener.jsmap.clearDomeLight(<%=psId%>);
	}
	$.artDialog.opener.jsmap.reLoadLightLayer(<%=psId%>,lightId,flag);
	$.artDialog.opener.reSetObjectDraggable(9,<%=psId%>,<%=id%>);
	$.artDialog.close();
}
function updateData(){
   	 var ischanged =0;
   	 if($("#x").val()!="<%=x%>" || $("#y").val()!="<%=y%>"){
   		 ischanged=1;
   	 }
   	 var x=$("#x").val();
   	 var y=$("#y").val();
		
   
}
function save(){
		var isChanged = ($("#x").val()!="<%=x%>"||
				  		   $("#y").val()!="<%=y%>") ? "y":"n";
		var status =$("select[name='type']").val();
		
		if($("#name").val()==''){
			verifyName();
		}
		
		if(verifyMgs['verifyName']==0 || verifyMgs['verifyName']==2){
			$("#name").focus();
			return;
		}
		if(verifyMgs[verifyX]==0 || verifyMgs[verifyX]==2){
			$("#x").focus();
			return;
		}
		if(verifyMgs[verifyY]==0 || verifyMgs[verifyY]==2){
			$("#y").focus();
			return;
		}
		var light_data={};
		$.each($('#light').serializeArray(),function(i,field){
			light_data[field.name]=field.value;
		})
		light_data['status']=status;
		if($("#name").val()!='<%=name%>'||isChanged=="y"||status!='<%=status%>'){
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/modifyLight.action',
				data:$("#light").serialize()+"&isChanged="+isChanged+"&status="+status,
				dataType:'json',
				type:'post',
				async:false, 
				beforeSend:function(request){
			 	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				success:function(data){
					$.unblockUI();
					if(data && data.flag == 'true'){
						if(light_data.id=='0'){
							light_data.id=data.id;
						}
						light_data['latlng']=data.latlng;
						 var light_data1=$.artDialog.opener.jsmap.storageBounds[light_data.ps_id+'_light'];
						if(!light_data1 ){
							$.artDialog.opener.jsmap.storageBounds[light_data.ps_id+'_light']=[light_data];
						}else{
							var isExist=false;
							for(var i=0;i<light_data1.length;i++){
								if(light_data1[i].id==light_data.id){
									light_data1[i]=light_data;
									isExist=true;
								}
							}
							if(!isExist){
								light_data1[light_data1.length]=light_data;
							}
						}  
						/* if(data.id){
							closeWindow('','add_or_update');
						}else{ */
							closeWindow(light_data.id,'add_or_update');
						/* } */

					}else if(data && data.flag == 'repeat'){
					showMessage("Light name already exists! ");
					}else if(data && data.flag == 'authError'){
					showMessage("Insufficient permissions!","error");
					}else{
					$("#errInfo").html("Sava failed!");	
					}
				},
				error:function(){
					$.unblockUI();
					$("#errInfo").html("Sava failed!");
				}
			 });
		}
	}
	
function commitData(){
	 if($("#name").val()!="<%=name%>"
			 ||$("#x").val()!="<%=x%>" 
			 || $("#y").val()!="<%=y%>"
			 ||$("select[name='type']").val()!="<%=status%>"){
	  save(); 
	  }else{
      closeWindow();  
	  }
		
}
	document.onkeydown=function(evt){
	  var evt=evt?evt:(window.event?window.event:null);//兼容IE和FF
	  if (evt.keyCode==13){
		  commitData();
	   }
	}
	
function doDelete(name,id,psId){
	 $.artDialog({
		 	title:'Notify',
		 	lock: true,
		 	opacity: 0.3,
		 	icon:'question',
		    content: "<span style='font-weight:bold;'>Delete ["+name+"] ?</span>",
		    button: [
		        {
		            name: 'YES',
		            callback: function () {
		            	deleteLight(id,psId);
		            },
		            focus: true
		        },
		        {
		            name: 'NO'
		        }
		    ]
		});
	
	
}
function deleteLight(id,psId){
	 $.ajax({
	     url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/StorageLocationDeleteAction.action',
		type:'post',
		dataType:'json',
		data:{"location_id":id,"ps_id":psId,"type":9},
		async:false, 
		beforeSend:function(request){
	     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success: function(data){
			if(data && data.flag=="true"){
				showMessage("Delete succeed ","succeed");
			}else{
				showMessage("Delete fail ","error");
			}
		 $.unblockUI();
   		 closeWindow('<%=id%>');
		},
   	 	error:function(){
   		showMessage("System error","error");
   		$.unblockUI();
   	  }
         });
}
var verifyMgs={};
function verifyName(){
	var vMsg="";
	if($("#name").val()==''){
		vMsg="name is empty!";
	}
		if('<%=name%>'!=""&& $("#name").val()=='<%=name%>'){
			$("#VNameError").hide();
			 $("#VNameRight").show();
			 verifyMgs['verifyName']=1;
		}else{
			var v_flag=verify('name','VNameRight','VNameError','','<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/verifyName.action','ps_id=<%=psId%>&type=9&name='+$('#name').val(),vMsg);
			verifyMgs['verifyName']=v_flag;
		}
}
function verifyX(){
	  var vMsg="";
	  if($("#x").val()==''){
		  vMsg="x is Empty!";
	}else{
		vMsg="Data format error";
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs[verifyX]=verify('x','x_con','x_del',reg,'','',vMsg);
}
function verifyY(){
	  var vMsg="";
	  if($("#y").val()==''){
		  vMsg="y is Empty!";
	}else{
		vMsg="Data format error";
	}
	  var reg = /^-?\d+(\.\d+)?$/; 
	  verifyMgs[verifyY]=verify('y','y_con','y_del',reg,'','',vMsg);
}
</script>
</head>
<body>
	<form id="light">
		<table width="100%" height="100%" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td align="center" valign="top" colspan="2">
						<table>
							<tr>
								<td align="right" >Name：</td>
								<td align="left" style="position:relative;"><input type="text"
									style="width: 85%;" name="name" id="name"
									value="<%=name%>" onblur="verifyName()" /><span style="height: 16px;width: 16px;position: absolute;top: 5px;" id="vNameMsg">
									<img  title="Name is exist" src="../imgs/maps/delete_red.jpg" id="VNameError" style="display: none;">
									<img  src="../imgs/maps/confirm.jpg" id="VNameRight" style="display: none;">
									</span>
								</td>
								
							</tr>
							<tr>
								<td align="right" >X Position：</td>
								<td align="left" style="position: relative;"><input type="text"
									style="width: 85%;" name="x" id="x"
									value="<%=x%>" onchange="verifyX()"/>
									<span style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img id="x_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             		<img id="x_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
									</span>
								</td>
								
							</tr>
							<tr>
								<td align="right" >Y Position：</td>
								<td align="left" style="position: relative;"><input type="text"
									style="width: 85%;" name="y" id="y"
									value="<%=y%>" onchange="verifyY()" />
									<span style="position: absolute;top: 5px;height: 16px;width:16px;">
										<img id="y_con" src="../imgs/maps/confirm.jpg" style="display: none">
					             		<img id="y_del" title="Data Error!" src="../imgs/maps/delete_red.jpg" style="display: none">
									</span>
								</td>
								
							</tr>
							<tr>
								<td align="right" >Status：</td>
								<td align="left" >
								<select name="type" style="width: 85%">
								<option value="0">OFF </option>
								<option value="1">ON</option>
								</select>
								</td>
							</tr>
				
						</table>
				</td>
			</tr>
			<input id="id" name="id" type="hidden" value="<%=id%>" />
			<input id ="ps_id" name="ps_id" type="hidden" value="<%=psId%>" />
		</table>
	</form>
</body>
