<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
int areaType = StringUtil.getInt(request,"area_type"); 
String area_name = StringUtil.getString(request,"area_name");
String[] area_names = area_name.split(",");
String area_id = StringUtil.getString(request,"area_id");
String[] area_ids = area_id.split(",");
String psId = StringUtil.getString(request,"psId");
String objId = StringUtil.getString(request,"objId","");
DBRow[] areaList = locationAreaXmlImportMgrCc.getAreaByPsId(psId);
%>
<html>
  <head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
.text-overflow {
	display:block;
	word-break:keep-all;
	white-space:nowrap;
	overflow:hidden;
	text-overflow:ellipsis;
}
</style>
  </head>
  <body>
  	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	    <table style="width: 100%;">
	    	<tr style=" font-weight:bold; font-size:16px; color:#00F;">
	    		<td style="width: 49%;">&nbsp;Existence Zone</td>
	    		<td style="width: 2%;"></td>
	    		<td style="width: 49%;">&nbsp;Free zone</td>
	    	</tr>
	    	<tr>
	    		<td>
					<div id="exitZone" style="background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto" >
						<%
						if(!area_name.equals("")){
							for(int i=0; i<area_names.length; i++){
								String area = area_names[i];
								String id = "";
								if(areaList != null && areaList.length>0){
									for(int j=0; j<areaList.length; j++){
										DBRow row = (DBRow)areaList[j];
										if(area.equals(row.getString("area_name"))){
											id = row.getString("area_id");
											break;
										}
									}
								}
								%>
								<div zone="<%=area%>" style="width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
									<div class="text-overflow"  style="width: 85%; float: left; " ondblclick="removeTitle(this.parentNode,<%=id %>)">
										<%=area%>
										<input type="hidden" id="area_id" value="<%=id %>">
									</div>
									<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/delete_grey.png'); margin: 1px; cursor: pointer;" 
											onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/delete_blue.png&quot;)'" 
											onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/delete_grey.png&quot;)'"
											onclick="removeZone(this.parentNode,<%=id %>)">
									</div>
								</div>
								<%
							}
						}else{
							%>
							<center>
								no data
							</center>
							<%
						}
						%>
					</div>
				</td>
				<td></td>
	    		<td>
					<div id="notExitZone" style="background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto" >
					<%
						if(areaList != null && areaList.length>0){
							for(int i=0; i<areaList.length; i++){
								DBRow area = (DBRow)areaList[i];
								String areaId = ","+area.getString("area_id")+",";
								if((","+area_id+",").indexOf(areaId)==-1){
								%>
								<div zone="<%=area.getString("area_name") %>" style="width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
									<div class="text-overflow" style="width: 85%; float: left; " ondblclick="addZone(this.parentNode)">
										<%=area.getString("area_name") %>
										<input type="hidden" id="area_id" value="<%=area.getString("area_id") %>">
									</div>
									<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_grey.png'); margin: 1px; cursor: pointer;" 
												onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'" 
												onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'"
												onclick="addZone(this.parentNode)">
									</div>
								</div>
								<%
								}
							}
						}else{
							%>
							<center>
								no data
							</center>
							<%
						}
					%>
					</div>
				</td>
	    	</tr>
	    	<tr>
				<td colspan="3" align="right" class="win-bottom-line">
					<input type="button" name="Submit1" class="short-short-button" value="Confirm" onClick="confirm();"> 
					<input type="button" name="Submit2" class="short-short-button" value="Cancel" onClick="closeWindow();">
				</td>
			</tr>
	    </table>
	</div>
	<!-- model -->
	<div id="zone_model" style="display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
		<div class="text-overflow"  style="width: 85%; float: left; " ondblclick="removeZone(this.parentNode)">
			<input type="hidden" id="area_id">
		</div>
		<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/delete_grey.png'); margin: 1px; cursor: pointer;" 
				onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/delete_blue.png&quot;)'" 
				onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/delete_grey.png&quot;)'"
				onclick="removeZone(this.parentNode)">
		</div>
	</div>
	<div id="zone_free_model" style="display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
		<div class="text-overflow" style="width: 85%; float: left; " ondblclick="addZone(this.parentNode)">
			<input type="hidden" id="area_id">
		</div>
		<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_grey.png'); margin: 1px; cursor: pointer;" 
					onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'" 
					onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'"
					onclick="addZone(this.parentNode)">
		</div>
	</div>
	<!-- model end-->
  </body>
<script type="text/javascript">
//删除title
function removeZone(ele){
	if($("#notExitZone").find("div").length == 0){
		$("#notExitZone").html("");
	}
	var je = $(ele);
	var id = je.find("#area_id").val();
	var area = je.attr("zone");
	var node = creatTitleNode(area,id,true);
	$("#notExitZone").append(node);
	je.remove();
}
//添加zone
function addZone(ele){
	if($("#exitZone").find("div").length == 0){
		$("#exitZone").html("");
	}
	var je = $(ele);
	var id = je.find("#area_id").val();
	var zone = je.attr("zone");
	var node = creatTitleNode(zone,id,false);
	$("#exitZone").append(node);
	je.remove();
}
//创建zone标签
function creatTitleNode(zone,id,isFree){
	var node = null;
	if(isFree){
		node = $("#zone_free_model").clone();
	}else{
		node = $("#zone_model").clone();
	}
	node.removeAttr("id");
	node.attr("zone",zone);
	node.find("#area_id").val(id);
	node.find("div").first().append(zone);
	node.css("display","");
	return node;
}
function confirm(){
	var ids = "";
	var names = "";
	var ts = $("#exitZone #area_id");
	for(var i=0; i<ts.length; i++){
		ids += $(ts[i]).val()+",";
		names +=  $(ts[i].parentNode.parentNode).attr("zone")+",";
	}
	if(ids!=""){
		ids = ids.substr(0,ids.length-1);
		names = names.substr(0,names.length-1);
	}
	$.artDialog.opener.backFillZone(names,ids,"<%=objId%>");
	$.artDialog.close();
}
function closeWindow() {
	$.artDialog.close();
}
</script>
</html>
