<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>运单创建服务</title>
<style type="text/css" media="all">
 @import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script> 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<style type="text/css">
	*{font-size:12px;}
	table.mytable{background:#D3EB9A;width:100%;border-collapse:collapse;}
 
	table.mytable tr td{line-height:35px;height:35px;border:0px solid silver;padding-top:4px;}
	input.font_red{color:#FF3300;width:40px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;}
	ul.uninos_product {list-style-type:none;padding-left:5px;display:none;}
	 
</style>
<%
	long waybillId = StringUtil.getLong(request,"waybill_id");
	DBRow[] wayBillItems = waybillMgrZR.getAllOrderItemsInWayBillById(waybillId);
	WarrantyTypeKey warrantyTypeKey = new WarrantyTypeKey(); 
	String putOrRemoveFormCartAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/serviceOrder/AddProductToServiceOrderCartAction.action";
 %>
 <script type="text/javascript">
 	function showUnionProduct(pc_id){
		var ul = $("#union_" + pc_id);
		if(ul.length > 0 ){
			$(ul).toggle();
		}
	}
	function putOrRemoveFromSession(pc_id,_this){
	 	var isChecked = $(_this).attr("checked") ? 1 : 0;
	 	var object = {is_checked:isChecked,pc_id:pc_id};
		$.ajax({
			url:'<%= putOrRemoveFormCartAction%>',
			dataType:'json',
			data:object,
			success:function(returnData){
				
			},
			error:function(){

			}
		}) 
	
	}
 </script>
</head>
<body>
 	 	<div id="tabs">
 	 		<ul>
		 		 <li><a href="#waybillItem" >运单商品</a></li>
			</ul>
			<div id="waybillItem" style="min-height:430px;">
				<table class="mytable">
						<tr>	
					 
							<td style="padding-left:20px;width:380px;">搜索商品 <input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:17px;height:20px;font-family:Arial, Helvetica, sans-serif;font-weight:bold"/></td>
						 
							<td style="width:100px;">数量&nbsp;<input type="text" value="" style="color:#FF3300;width:40px;font-size:17px;height:20px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;" /></td>
							 
							<td>质保类型
								<select>
								<%
									ArrayList warrantyTypeList = warrantyTypeKey.getWarrantyType();
									for(int i=0;i<warrantyTypeList.size();i++)
									{
								%>
									<option value="<%=warrantyTypeList.get(i)%>"><%=warrantyTypeKey.getWarrantyTypeById(warrantyTypeList.get(i).toString())%></option>
								<%
									}
								%>
								</select>
							</td>
						</tr>
						<tr>
							<td style="padding-left:20px;width:380px;">质保原因 <input maxlength="300"  name="reason" type="text" id="reason" style="width:300px;font-size:17px;height:20px;font-family:Arial, Helvetica, sans-serif;font-weight:bold"/></td>
							<td colspan="2">
								<input type="button" class="long-button" value="添加质保原因" onclick="" />&nbsp;&nbsp;(多使用回车，效率更高哦) 
							</td>
						</tr>
				</table>
			 	<%
			 		if(wayBillItems != null && wayBillItems.length > 0 ){
			 			for(DBRow tempRow : wayBillItems){
			 				DBRow[] productUnions = productMgr.getProductUnionsBySetPid(tempRow.get("pc_id",0l));
			 		%>
			 			<div style="padding:4px;clear:both;">
			 				<!-- head -->
			 				<h1 style="margin:0px;padding:0px;letter-spacing:0px;">
			 	 
			 					<span style="font-weight:bold;cursor:pointer;text-decoration:underline;color:black;" onclick="showUnionProduct(<%=tempRow.get("pc_id",0l) %>);"><%=tempRow.getString("p_name") %></span>
			 			 
				 				<span style="display:block;float:left;margin-right:2px;">
				 					<input type="checkbox" style="margin-top:2px;" onclick="putOrRemoveFromSession('<%= tempRow.get("pc_id",0l)%>',this)"/>
				 				</span>
			 				</h1>
			 				 <%
			 					if(productUnions != null && productUnions.length > 0){
			 				 %>
								<ul  id="union_<%=tempRow.get("pc_id",0l) %>" class="uninos_product">
									<%for(DBRow tempUnons : productUnions) {%>
										<li><input type="checkbox" />	<%=tempUnons.getString("p_name")  %></li>
									<%} %>
								</ul>	
							 <%}%>
			 			</div>
			 				
			 		<% 	
			 			}
			 		}
			 	%>
				
			</div>
 	 	</div>
 	 	
 	 	<script type="text/javascript">
 	 		$("#tabs").tabs();

 	 		addAutoComplete($("#p_name"),
 				"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
 				"p_name");
 	 	</script>
</body>
</html>