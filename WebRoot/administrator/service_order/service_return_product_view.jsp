<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.StorageReturnProductStatusKey"%>
<%@page import="com.cwc.app.key.ReturnProductKey"%>
<%@page import="com.cwc.app.key.StorageReturnProductTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductSourceKey"%>
<%@page import="com.cwc.app.key.ReturnProductPictureRecognitionkey"%>
<%@page import="com.cwc.app.key.ReturnProductCheckItemTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultTypeKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.text.SimpleDateFormat"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>服务关联退货</title>
<%
long rp_id = StringUtil.getLong(request, "rp_id");
DBRow returnRow = returnProductMgrZr.getReturnProductByRpId(rp_id);
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/return_order/ReturnProductFileDownLoadAction.action";
StorageReturnProductStatusKey returnProductStatusKey = new StorageReturnProductStatusKey();
ReturnProductKey returnProductKey = new ReturnProductKey();
StorageReturnProductTypeKey returnProductTypeKey = new StorageReturnProductTypeKey();
ReturnProductSourceKey returnProductSourceKey = new ReturnProductSourceKey();
ReturnProductPictureRecognitionkey pictureRecognitionkey = new ReturnProductPictureRecognitionkey();
ReturnProductCheckItemTypeKey checkItemTypeKey = new ReturnProductCheckItemTypeKey();
ReturnProductItemHandleResultRequestTypeKey itemHandleRequestType = new ReturnProductItemHandleResultRequestTypeKey();
ReturnProductItemHandleResultTypeKey handleResultTypeKey = new ReturnProductItemHandleResultTypeKey();
%>
<style type="text/css">
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
</style>

</head>
<body>
<table style="width:100%;">
<%
	if(null != returnRow)
	{
%>
	<tr style="width:100%;">
		<td style="width:50%;" align="left" valign="top">
		 	<fieldset class="set" style="padding-bottom:4px;padding-top:5px;border:2px solid #993300;width: 80%;">
 				 <legend>
					<span class="title">
						<span style="font-weight:bold;">[<%= returnRow.get("rp_id",0l)%>] 
							<font color="green"><%=  returnProductKey.getReturnProductStatusById(returnRow.get("status",8)) %></font>
							<font color="#f60">[<%=returnProductSourceKey.getStatusById(returnRow.get("return_product_source",1))  %>]</font>
						</span>
					</span> 
				</legend>
					<p>
						<span class="alert-text stateName" style="font-weight:bold;">采集:</span>
						<span class="stateValue" style="font-weight:bold;"><%= (returnRow.get("gather_picture_status",0) == 0 ? "图片采集未完成":"图片采集完成") %></span>
					</p>
					<p>
						<span class="alert-text stateName" style="font-weight:bold;">图片:</span>
						<span class="stateValue" style="font-weight:bold;"><%= pictureRecognitionkey.getStatusById(returnRow.get("picture_recognition",ReturnProductPictureRecognitionkey.RecognitionNoneFinish))%></span>
					</p>
					<p>
						<span class="alert-text stateName" style="font-weight:bold;">退货:</span>
						<span class="stateValue" style="font-weight:bold;"><%= checkItemTypeKey.getStatusById(returnRow.get("return_product_check",ReturnProductCheckItemTypeKey.OUTCHECK))%></span>
					</p>
					
					<p>
						<span class="alert-text stateName"><img title="退货类型" src="../imgs/invoice.gif" /></span>
  						<span class="stateValue"><%=returnProductTypeKey.getStatusById(returnRow.get("return_product_type",1))%></span>
					</p>
					<p>
  						<span class="alert-text stateName"><img title="操作人" src="../imgs/order_client.gif" /></span>
  						<span class="stateValue">
  							<%
	  							DBRow userInfo = null;
	  						    userInfo = waybillMgrZR.getUserNameById(returnRow.get("create_user",0l));
	  						    String userName = "";
	  							if(userInfo != null ){
	  								  userName =  userInfo.getString("employe_name");
	  							}
  							%>
  							<%=userName %> &nbsp;
  							<%
  								if(1 == returnRow.get("create_type",0))
  								{
  									out.println("(仓库)");
  								}
  								else
  								{
  									out.println("(客服)");
  								}
  							%>
  						</span>
  					</p>
	  				<p>
	  					<span class="alert-text stateName"><img  title="创建时间" src="../imgs/alarm-clock--arrow.png" /></span>
	  					<span class="stateValue"><%=tDate.getFormateTime(returnRow.getString("create_date"))%></span>
	  				</p>
	  				<%
	  					if(0 != returnRow.get("oid",0L) || 0 != returnRow.get("wid",0L) || 0 != returnRow.get("sid",0L))
	  					{
	  				%>
	  				<p>
						<span class="alert-text stateName" style="font-weight:bold;">关联:</span>
						<span class="stateValue" style="font-weight:bold;">
							<%
								if(0 != returnRow.get("oid",0L))
								{
							%>	
								订单:<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/ct_order_auto_reflush.html?&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=<%=returnRow.get("oid",0L)%>' target="_blank" style="cursor: pointer;"><%=returnRow.get("oid",0L)%></a>
							<%
								}
								if(0 != returnRow.get("wid",0L))
								{
							%>
								运单:<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/waybill/waybill_list.html?cmd=waybillId&search_mode=1&oid=<%=returnRow.get("wid",0L) %>' target="_blank" style="cursor: pointer;"><%=returnRow.get("wid",0L) %></a>
							<%
								}
								if(0 != returnRow.get("sid",0L))
								{
							%>
								服务单:<a href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_list.html?cmd=search&search_mode=1&key='<%=returnRow.get("sid",0L) %>'" target="_blank" style="cursor: pointer;"><%=returnRow.get("sid",0L) %></a>
							<%
								}
							%>
						</span>
					</p>
					<%
						}
	  				%>
		</fieldset>
	 </td>
 		<td style="width:50%;" valign="top">
 			<fieldset class="set" style="padding-bottom:4px;padding-top:5px;border:2px solid #993300;width: 80%;">
 				 <legend>
					地址信息
				</legend>
				 <p>	
  					<span class="alert-text valueName">First Name:</span>
  					<span class="valueSpan"><%= returnRow.getString("first_name")%>&nbsp;</span>
				 </p>
				  <p>	
  					<span class="alert-text valueName">Last Name:</span>
  					<span class="valueSpan"><%= returnRow.getString("last_name")%>&nbsp;</span>
				 </p>
			 	 <p>	
  					<span class="alert-text valueName">Street:</span>
  					<span class="valueSpan"><%= returnRow.getString("address_street")%>&nbsp;</span>
				 </p>
				  <p>	
  					<span class="alert-text valueName">City:</span>
  					<span class="valueSpan"><%= returnRow.getString("address_city")%>&nbsp;</span>
				 </p>
				 <p>	
  					<span class="alert-text valueName">Country:</span>
  					<span class="valueSpan"><%= returnRow.getString("address_country")%>&nbsp;</span>
				 </p>
				  <p>	
  					<span class="alert-text valueName">State:</span>
  					<span class="valueSpan"><%= returnRow.getString("address_state")%>&nbsp;</span>
				 </p>
  				<p>	
  					<span class="alert-text valueName">Tel:</span>
  					<span class="valueSpan"><%=returnRow.getString("tel")%>&nbsp;</span>
				 </p>
				 <p>	
  					<span class="alert-text valueName">Zip:</span>
  					<span class="valueSpan"><%= returnRow.getString("address_zip")%>&nbsp;</span>
				 </p>
			</fieldset>
 		</td>
 	</tr>
 	<tr style="width:80%;">
			<td colspan="2" align="left" valign="top" style="width:80%;">
				<!--  目前是查询reson 表中的数据到时候要改 -->
				<fieldset class="set" style="padding-bottom:4px;border:2px solid #993300;">
					<legend>
						<span class="title">
							<span style="font-weight:bold;">
							<%
							DBRow productStoreCatalog = catalogMgr.getDetailProductStorageCatalogById(returnRow.get("ps_id",0l));
							if(null != productStoreCatalog)
							{
								out.println(productStoreCatalog.getString("title")+"&nbsp;|&nbsp;");
							}
							%>
							退货商品</span>
						</span> 
				</legend>
				<%
					DBRow[] itemsRow = returnProductItemsFixMgrZr.getReturnItemsByRpId(returnRow.get("rp_id",0l));
					if(itemsRow != null && itemsRow.length > 0 ){
						
						for(DBRow item : itemsRow){
							%>
							<p>
								 <%=item.getString("p_name") %> x	<span style="color:blue"><%=item.get("quantity",0.0f) %> <%=item.getString("unit_name") %></span> <br />
								 
								 <%if(item.get("return_product_check",ReturnProductCheckItemTypeKey.OUTCHECK) != ReturnProductCheckItemTypeKey.OUTCHECK){ %>
									├ 商品:<%=checkItemTypeKey.getStatusById(item.get("return_product_check",0)) %><br />
								 <%} %>
								 
								 <%if(item.get("handle_result_request",0) != 0){ %>
	 								├ 处理要求:<span style="color:blue"><%= itemHandleRequestType.getStatusById(item.get("handle_result_request",0))%></span><br />
								 <% }%>
								 	├ 退货处理:<%=(item.get("is_return_handle",0) == 0 ? "未完成":"完成") %><br/>
								 <%=!"".equals(item.getString("return_reason"))?"├退货原因:"+item.getString("return_reason"):""%>
							</p>
							
							<% 
						}
					}
				%>
				</fieldset><br/>
					<%
 					int[] fileTypes = {FileWithTypeKey.ProductReturn};
	 				DBRow[] files = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(returnRow.get("rp_id", 0L), fileTypes);
					if(files.length > 0)
					{
						for(int f = 0; f < files.length; f ++)
						{
							DBRow file = files[f];
				%>
						 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	 <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
		 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
		 			 	 <p>
	 			 	 	<%
	 			 	 		if(StringUtil.isPictureFile(file.getString("file_name")))
	 			 	 	 	{ 
	 			 	 	%>
			 			 	 
			 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.ProductReturn %>','<%=returnRow.get("rp_id", 0L)%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
		 			 	 <%
		 			 	 	}
							else if(StringUtil.isOfficeFile(file.getString("file_name")))
							{
						%>
		 			 	 		<a href="javascript:void(0)"  file_id='<%=file.get("file_id",0l) %>' onclick="openOfficeFileOnlineReturnFile(this,'<%=ConfigBean.getStringValue("systenFolder")%>','returnImg')" file_is_convert='<%=file.get("file_is_convert",0) %>'><%=file.getString("file_name") %></a>
		 			 	 <%	}
							else
							{
						%>
		 			 	 	  	<a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder=returnImg'><%=file.getString("file_name") %></a> 
		 			 	 <%
		 			 	 	}
						%>
						&nbsp;<%=null == adminMgrLL.getAdminById(file.getString("upload_adid"))?"":adminMgrLL.getAdminById(file.getString("upload_adid")).getString("employe_name") %>
						<% if(!"".equals(file.getString("upload_time"))){
								out.println("&nbsp;"+DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(file.getString("upload_time"))));
							}
						%>
								&nbsp;<a href="javascript:deleteFile('<%=file.get("file_id",0l) %>','<%=file.getString("file_name") %>')">删除</a>
							</p><br/>
				<%	
						}
					}
 				%>
			</td>
		</tr>
		<tr style="width:100%;">	
 			<td style="padding:5px;" colspan="2">
 				备注:<%=returnRow.getString("note") %>
 			</td>
	 	</tr>

<%		
	}
	else
	{
%>
	<tr>
		<td align="center" style="margin-top: 10px;margin-bottom: 10px;">
			<span style="color: red ;font-size: 15px;">此退货单不存在</span>
		</td>
	</tr>
<%		
	}
%>
	
</table>
</body>
</html>