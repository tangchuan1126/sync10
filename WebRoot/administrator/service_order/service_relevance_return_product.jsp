<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>服务关联退货</title>
<%
	long sid = StringUtil.getLong(request, "sid");
%>



<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<script type="text/javascript">
$(function(){

});
	function submitData(){
		if(volidate()){
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/return_order/ServiceRelevanceReturnProductOrderAction.action',
				data:$("#subForm").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "success"){
						showMessage("关联成功","success");
						setTimeout("windowClose()", 1000);
					}else{
						showMessage("添加失败，此退货单已关联服务单","error");
					}
				},
				error:function(){
					showMessage("系统错误","error");
				}
			})	
		}
	};
	function volidate(){
		if($("#rp_id").val() == ""){
			alert("请填写所关联的退货单号");
			return false;
		}
		return true;
	};
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function viewReturnInfo()
	{
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_return_product_view.html',
			data:{rp_id:$("#rp_id").val()},
			dataType:'html',
			type:'post',
			success:function(data){
				$("#view_return_info").html(data);
			},
			error:function(){
				showMessage("系统错误","error");
			}
		})	
		
	}
</script>

</head>
<body>
	
<form action="" id="subForm">
<input type="hidden" name="sid" value='<%=sid %>'/>
	<table style="width: 100%;">
		<tr>
			<td>
				退货单号:
			</td>
			<td>
				<input id="rp_id" name="rp_id" value=""/>
				<input type="button" class="long-button" onclick="viewReturnInfo()" value="查看退货单信息"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="view_return_info"></div>
			</td>
		</tr>
	</table>
</form>
<table align="right">
	<tr align="right">
		<td colspan="2"><input type="button" class="normal-green" value="提交" onclick="submitData();"/></td>
	</tr>
</table>

<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
  </script>
</body>
</html>