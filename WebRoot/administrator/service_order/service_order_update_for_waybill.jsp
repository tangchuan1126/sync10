<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.api.OrderLock"%>
<%@ page import="com.cwc.app.api.qll.VertualTerminalMgrQLL"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%
	long oid = StringUtil.getLong(request,"oid");
	long sid = StringUtil.getLong(request,"sid");
	long wid = StringUtil.getLong(request,"wid");
	WarrantyTypeKey warrantyTypeKey = new WarrantyTypeKey(); 
	cartReturnProductReason.clearReturnProductReasonCart(StringUtil.getSession(request));
	DBRow[] orderItems = waybillMgrZR.getAllOrderItemsInWayBillById(wid);
	
	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>更新服务单</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<style>
.ebayTitle
{
	 color: #0066FF;
    display: block;
    float: left;
    font-size: 12px;
    font-weight: bold;
    width: 100px;
    text-align:right;
   
}
 
</style>


<script>
function showProductUnion(pc_id)
{
	
	if($("#product_tr tr[id="+pc_id+"]").css("display")=="none")
	{
		$("#product_tr tr[id="+pc_id+"]").css("display","");
	}
	else
	{
		$("#product_tr tr[id="+pc_id+"]").css("display","none");
	}
}

function allNoCheck(typeId_id_count)
{
	$("input:checkbox[value^='"+typeId_id_count+"_']").attr("checked",false);
	if($("input:checkbox[value='"+typeId_id_count+"']").attr("checked")=="checked")
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/serving/PutOderOrWaybillProductToCartAction.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:
			{
				type:"select",
				value:typeId_id_count
			},
			
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			
			error: function(){
				loadCart(1);
				$("#action_info").text("商品添加失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException")
				{
					alert("商品不存在，不能添加到订单！");
					$("#p_name").select();
				}
				else if(msg=="ProductDataErrorException")
				{
					alert("商品基础数据错误，不能添加到订单！请联系调度");
					$("#p_name").select();
				}
				else if (msg=="ok")
				{
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("1");
					$("#reason").val("");
					loadCart(0);
					cartProQuanHasBeChange = true;
				}
				else
				{
					loadCart(1);
					alert(msg);
				}
			}
		});	
	}
	else
	{
		delOrderOrWaybillProductFromCart(typeId_id_count);
	}
}

function productNotCheck(obj)
{
	if($(obj).attr("checked")=="checked")
	{
		var array		= obj.value.split("_");
		var typeId		= array[0];
		var setId		= array[1];
		var setCount	= array[2];
		var orderId		= array[3];
		var setId_count	= typeId + "_" + setId + "_" + setCount+"_"+orderId;
		$("[value='"+setId_count+"']").attr("checked",false);
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/serving/PutOderOrWaybillProductToCartAction.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:
			{
				type:"select",
				value:obj.value
			},
			
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			
			error: function(){
				loadCart(1);
				$("#action_info").text("商品添加失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException")
				{
					alert("商品不存在，不能添加到订单！");
					$("#p_name").select();
				}
				else if(msg=="ProductDataErrorException")
				{
					alert("商品基础数据错误，不能添加到订单！请联系调度");
					$("#p_name").select();
				}
				else if (msg=="ok")
				{
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("1");
					$("#reason").val("");
					loadCart(0);
					cartProQuanHasBeChange = true;
				}
				else
				{
					loadCart(1);
					alert(msg);
				}
			}
		});	
	}
	else
	{
		delOrderOrWaybillProductFromCart(obj.value);
	}
}
function delOrderOrWaybillProductFromCart(pid)
{
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/serving/RemoveOrderOrWaybillProductFromCartAction.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{
				value:pid
			},
					
			error: function(){
				loadCart(1);
				$("#action_info").text("商品质保原因删除失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart(0);
			}
		});	
}
function delOrderOrWaybillProductFromCartList(value, pc_name)
{
	if (confirm("确认删除 "+pc_name+"？"))
	{
		delOrderOrWaybillProductFromCart(value);
	}
}
var cartIsEmpty = <%=cartWarranty.isEmpty(session)%>;

function checkCart()
{
	if (cartIsEmpty)
	{	
		alert("请先添加商品的质保理由");
		return(false);
	}
	else
	{
		return(true);
	}
}

function customProduct(pid,p_name,cmd)
{
	$.artDialog.open("../product/custom_product.html?cmd="+cmd+"&pid="+pid+"&p_name="+p_name, {title: "定制套装",width:'400px',height:'300px',fixed:true, lock: true,opacity: 0.3});
	//tb_show('定制套装',"../product/custom_product.html?cmd="+cmd+"&pid="+pid+"&p_name="+p_name+"&TB_iframe=true&height=300&width=400",false);
}

$().ready(function() {

	function log(event, data, formatted) {

		
	}
	
	addAutoComplete($("#p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
			"p_name");
	
	//加载购物车
	loadCart(1);
	//绑定购物车表单AJAX
	ajaxCartForm();

	
	$("#p_name").keydown(function(event){
		if (event.keyCode==13)
		{
			$("#quantity").select();
		}
	});

	$("#quantity").keydown(function(event){
		if (event.keyCode==13)
		{
			$("#reason").select();
		}
	});
	
	$("#reason").keydown(function(event)
		{
			if(event.keyCode==13)
			{
				put2Cart();
			}
		}
	)
	loadServiceItemToCart();
});
function loadServiceItemToCart()
{
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/LoadServiceItemToCartAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:{sid:<%=sid%>},
		beforeSend:function(request){
			$("#action_info").text("加载购物车......");
		},
		error: function(){
			loadCart(1);
			$("#action_info").text("加载购物车失败！");
		},
		success: function(html){
			loadCart(0);
		}
	});
}
//=================== 购物车 =========================

function loadCart(isAjaxDataBase)
{
	//alert(para);
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_update_product.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:{sid:<%=sid%>,isAjaxDataBase:isAjaxDataBase},
		
		beforeSend:function(request){
			$("#action_info").text("加载购物车......");
		},
		
		error: function(){
			$("#action_info").text("加载购物车失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
			$("#action_info").text("");
			$("#p_name").focus();
		}
	});
}


function put2Cart()
{
	var p_name = $("#p_name").val();
	var quantity = $("#quantity").val();
	var reason = $("#reason").val();
	var warranty_type = $("#warranty_type").val();
	
	 if (p_name=="")
	{
		alert("请填写商品名称");
		$("#p_name").focus();
	}
	else if (quantity=="")
	{
		alert("请填写数量");
	}
	else if ( !isNum(quantity) )
	{
		alert("请正确填写数量");
		$("#quantity").select();
	}
	else if (quantity<=0)
	{
		alert("数量必须大于0");
		$("#quantity").select();
	}
	else if(!reason.length>0)
	{
		alert("请填写质保原因");
		$("#reason").select();
	}
	
	else
	{
		var para = "p_name="+p_name+"&quantity="+quantity+"&reason="+reason;
	
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/serving/PutOderOrWaybillProductToCartAction.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:
			{
				type:"add",
				p_name:p_name,
				quantity:quantity,
				reason:reason,
				warranty_type:warranty_type
			},
			
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			
			error: function(){
				loadCart(1);
				$("#action_info").text("商品添加失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException")
				{
					alert("商品不存在，不能添加到订单！");
					$("#p_name").select();
				}
				else if(msg=="ProductDataErrorException")
				{
					alert("商品基础数据错误，不能添加到订单！请联系调度");
					$("#p_name").select();
				}
				else if (msg=="ok")
				{
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("1");
					$("#reason").val("");
					loadCart(0);
				}
				else
				{
					loadCart(1);
					alert(msg);
				}
			}
		});	
	}
}

function applicationWarranty()
{
	if(cartProQuanHasBeChange)
	{
		alert("请先保存质保修改商品的数量与原因");
	}
	else
	{
		if(checkCart())
		{
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty/applicationWarranty.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:
				{
					sid:<%=sid%>
				},
				
				beforeSend:function(request){
					
				},
				
				error: function(){
					
				},
				
				success: function(msg)
				{
					window.location.href = "../order/return_product.html?rp_id="+msg;
				}
			});
		}
	}
	
}
function applicationServing()
{
	if(cartProQuanHasBeChange)
	{
		alert("请先保存质保修改商品的数量与原因");
	}
	else
	{
		if(checkCart())
		{
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/ModServiceItemsAction.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:
				{
					sid:<%=sid%>,
					oid:<%=oid%>,
					wid:<%=wid%>
				},
				
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				
				error: function(){
					$.unblockUI();
				},
				
				success: function(msg)
				{
					//window.location.href = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_reason_solution.html?sid='+msg;
					setTimeout("windowClose();",1000);
				}
			});
		}
	}
	
}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};

function delProductFromCartSelect(pid)
{
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty/removeReturnProductReason.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{
				pc_id:pid
			},
					
			error: function(){
				loadCart(1);
				$("#action_info").text("商品质保原因删除失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart(0);
			}
		});	
}
function delProductFromCart(pid,p_name)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty/removeReturnProductReason.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{
				pc_id:pid
			},
					
			error: function(){
				loadCart(1);
				$("#action_info").text("商品质保原因删除失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart(0);
			}
		});	
	}
}


function cleanCart()
{
	if (confirm("确定清空质保原因？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty/cleanReturnProductReason.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			
			error: function(){
				loadCart(1);
				$("#action_info").text("清空购物车失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCart(0);
				$(":checkbox").attr("checked",false);
			}
		});	
	}
}
function parentTrueOrder(oid,tel,ccid,note,parentid,pro_id,address_name,address_city,address_street,address_zip,address_state)
{
	var para = "oid="+oid+"&tel="+tel+"&record_order_note="+note+"&handle_status=<%=HandStatusleKey.NORMAL%>&ccid="+ccid+"&pro_id="+pro_id+"&address_name="+address_name+"&address_city="+address_city+"&address_street="+address_street+"&address_zip="+address_zip+"&address_state="+address_state;
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/newRecordOrder.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			async:false,
			data:para,
			
			beforeSend:function(request){
			},
			
			error: function(){
			},
			
			success: function(msg){
				
			}
		});
}


//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   beforeSubmit:  // pre-submit callback    
				function()
				{
					$("[id*='all_']")
					$("[name*='return_reason_']").each(function(){
						if(!(this.value.length>0))
						{
							alert("必须填写质保理由");
							return false;
						}
					});
								
				}	
		   ,  
		   async:false,
		   success:       // post-submit callback  
				function(data)
				{
					loadCart(0);
					cartProQuanHasBeChange = false;
				}
		       
	};
	

   $('#cart_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}

var cartProQuanHasBeChange = false;

function onChangeQuantity(obj)
{
	if ( !isNum(obj.value) )
	{
		alert("请正确填写数字");
		cartProQuanHasBeChange = true;
		return(false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}

function onChangeWarrantyType(obj)
{
	obj.style.background="#FFB5B5";
	cartProQuanHasBeChange = true;
	return(true);
}

function onChangeReturnReason(obj)
{
	if(obj.value.length>0)
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
	else
	{
		alert("请输入质保原因");
		cartProQuanHasBeChange = true;
		return(false);
	}
}


function isNum(keyW)
{
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
//=================== 购物车 =========================


var cartIsEmpty = <%=cart.isEmpty(session)%>;

function checkCart()
{
	if (cartIsEmpty)
	{	
		alert("请先添加商品到订单");
		return(false);
	}
	else
	{
		return(true);
	}
}

//除了正常抄单，购物车不能为空外，其他状态抄单都可以为空
function trueOrder()
{

	if (checkCart())
	{
		if (cartProQuanHasBeChange)
		{
			alert("请先保存质保修改商品的数量与原因");
		}
		else if($("#address_name").val()=="")
		{
			alert("请先填写Name");
			next(1);
		}
		else if($("#address_city").val()=="")
		{
			alert("请先填写City");
			next(1);
		}
		else if($("#address_street").val()=="")
		{
			alert("请先填写Street");
			next(1);
		}
		else if($("#address_zip").val()=="")
		{
			alert("请先填写Zip");
			next(1);
		}
		else if (checkSelectCountry())
		{
			alert("请选择递送国家");
			next(1);
		}
		else if ($("#pro_id").val()==0)
		{
			alert("请选择地区");
			next(1);
		}
		else if ( $("#delivery_note").val()!=""&&/.*[\u4e00-\u9fa5]+.*$/.test($("#delivery_note").val()) )
		{
			alert("配送备注不能包含中文");
		}	
		else
		{			
			var note = ""
					if (note!="")
					{
						note = "："+note;
					}
		}
	}
}



function doubtTypeOrder(type)
{
		if (cartProQuanHasBeChange)
		{
			alert("请先保存购物车修改商品的数量");
		}
		else if (checkSelectCountry())
		{
			alert("请选择递送国家");
		}
		else if ($("#pro_id").val()==0)
		{
			alert("请选择地区");
		}
		else
		{

				$.prompt(
				
				"<div id='title'>疑问订单</div><br /><span style='font-size:13px;font-weight:bold;'><input type='hidden' name='handleStatusWinText' id='handleStatusWinText' value='"+type+"'/><textarea name='OrderNoteWinText' id='OrderNoteWinText'  style='width:320px;height:60px;'></textarea>",
				
				{
					  submit: 
							function (v,m,f)
							{
								if (v=="y")
								{
									if ( typeof(f.handleStatusWinText)=="undefined")
									{
										alert("请选择疑问类型");
									    return false;
									}
									else if(f.OrderNoteWinText  == "")
									{
									   alert("请填写问题备注");
									    return false;
									}
									return true;
								}
			
							}
					  ,
			
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
									var outOrderNoteWinText = f.OrderNoteWinText;
								}
							}
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
				
			
		}
		

}


function closeTBWin()
{
	tb_remove();
}

function changeCountry(obj)
{
	
}


function pasteEmail()
{
	if ( document.getElementById("emailContainer").style.display == "" )
	{
		document.getElementById("paste_email").innerHTML = "粘贴邮件内容";
		document.getElementById("emailContainer").style.display = "none";
	}
	else
	{
		document.getElementById("paste_email").innerHTML = "隐藏";
		document.getElementById("emailContainer").style.display = "";
	}
}

</script>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
 <div class="demo" align="center">
  	 <div id="tabs" style="width: 98%">
  	    <ul>
		  <li><a href="#orderItem" >订单商品</a></li>
		</ul>
		<div id="orderItem" style="min-height:430px;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						
						<td width="100%" valign="middle" bgcolor="#D3EB9A">
							<table>
								<tr>
									<td height="35" align="center" valign="middle" style="padding-left:5px;">
										搜索商品<input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold"/>
										
									</td>
									<td>
										&nbsp;&nbsp;&nbsp;数量 <input name="quantity" type="text" id="quantity"  style="color:#FF3300;width:40px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold" value="1"/>
										&nbsp;&nbsp;&nbsp;质保类型
										<select id="warranty_type">
											<%
												ArrayList warrantyTypeList = warrantyTypeKey.getWarrantyType();
												for(int i=0;i<warrantyTypeList.size();i++)
												{
											%>
												<option value="<%=warrantyTypeList.get(i)%>"><%=warrantyTypeKey.getWarrantyTypeById(warrantyTypeList.get(i).toString())%></option>
											<%
												}
											%>
										</select>
									</td>
								</tr>
								
								<tr>
									<td height="35" align="center" valign="middle" style="padding-left:5px;">
										质保原因<input maxlength="300"  name="reason" type="text" id="reason" style="width:300px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold"/>	
									</td>
									<td>
										&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="long-button" value="添加质保理由" onClick="put2Cart()"> &nbsp;&nbsp;(多使用回车，效率更高哦)
									</td>
								</tr>
							</table>
					     </td>
					  </tr>
					  <tr>
					  	<td>
					  		<table width="100%">
					  			<tr>
					  				<td width="50%">
					  				<table border="0" cellpadding="0" cellspacing="0" id= "product_tr">
					  					<%
					  						for(int i = 0;i<orderItems.length;i++)
					  						{
					  							DBRow product = productMgr.getDetailProductByPcid(orderItems[i].get("pc_id",0l));
					  							
					  							DBRow[] productUnions = productMgr.getProductUnionsBySetPid(product.get("pc_id",0l));
					  					%>
					  					<tr>
					  						<td style="font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold">
					  							<% 
					  							if(productUnions.length>0)
					  							{
					  							%>
					  							<span onclick="showProductUnion('<%="WI"+orderItems[i].get("waybill_order_item_id",0L)+"_"+product.get("pc_id",0l)%>')" style="text-decoration:underline;cursor:pointer;"><%=product.getString("p_name")%></span>
					  							<%
					  							}
					  							else
					  							{
					  							%>
					  							<%=product.getString("p_name")%>
					  							<%
					  							}
					  							%>
					  							
					  						</td>
					  						<td align="left">
					  							<%
					  								if(product.get("orignal_pc_id",0)==0)
					  								{
					  							%>
					  							<input type="checkbox" value='<%="WI"+orderItems[i].get("waybill_order_item_id",0L)+"_"+product.get("pc_id",0l)+"_"+orderItems[i].get("quantity",0F)+"_"+orderItems[i].get("order_item_id",0L)%>' onclick="allNoCheck(this.value)"/>
					  							<%
					  								}
					  							%>
					  						</td>
					  					</tr>
					  					<% 
					  							if(productUnions.length>0)
					  							{
					  					%>
					  					<tr id="<%="WI"+orderItems[i].get("waybill_order_item_id",0L)+"_"+product.get("pc_id",0l)%>" style="display:none">
					  						<td>
					  							<table border="0" cellpadding="0" cellspacing="0">
					  								<%
					  									for(int j = 0;j<productUnions.length;j++)
					  									{
					  										DBRow unionProduct = productMgr.getDetailProductByPcid(productUnions[j].get("pid",0l));
					  								%>
					  								<tr>
					  									<td><%=unionProduct.getString("p_name")%></td>
					  									<td>
					  									<input type="checkbox" value="<%="WI"+orderItems[i].get("waybill_order_item_id",0L)+"_"+product.get("pc_id",0l)+"_"+orderItems[i].get("quantity",0F)+"_"+orderItems[i].get("order_item_id",0L)+"_"+unionProduct.get("pc_id",0l)+"_"+productUnions[j].get("quantity",0F)%>" onclick="productNotCheck(this)"/>
					  									</td>
					  								</tr>
					  								<%
					  									}
					  								%>
					  							</table>
					  						</td>
					  					</tr>
					  					<%
					  							}
					  					%>
					  					
					  					<%
					  						}
					  					%>
					  				</table>
					  				</td>
					  				<td>
					  				</td>
					  			</tr>
					  		</table>
					  	</td>
					  </tr>
					  <tr>
						<td align="left" valign="middle" style="padding-top:10px;">
							<form action='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/ModServiceItemsSessionAction.action' method="post" name="cart_form" id="cart_form">
								<div id="mini_cart_page"></div>
							</form>	
						</td>
    				 </tr>
	
  <tr>
    <td colspan="2" align="right" valign="middle" style="color:#FF0000" id="action_info"></td>
  </tr>
</table>
	      <table width="98%" border="0" cellspacing="0" cellpadding="3" style="padding-top: 10px;">
	      	<tr>
	      		<td align="center"><input onclick="applicationServing()" type="button" style="font-weight: bold" class="normal-green" value="修改服务"/></td>
	      	</tr>
	      </table>
		</div>
	</div>
</div>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/recordOrder.action" method="post" name="save_form" id="save_form">
	<input type="hidden" name="sid"/>
</form>
<script>
	$("#tabs").tabs({
		cache: false,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		load: function(event, ui) {onLoadInitZebraTable();}	,
		selected:1,
		show:function(event,ui)
			 {
			 	var isChangeLastTabs = true;
			 	if(lastTabs ==2)
			 	{
			 		saveCart();
			 	}
			 	if(ui.index==2)
			 	{
			 		loadCart(0);
			 	}
			 	if(ui.index==3)
			 	{
			 		cleanWayBill();
			 		checkDeliveryInfo(ui.index);
			 		trueOrder();
			 		loadWaybill();
			 	}
			 			 	
			 	if(isChangeLastTabs)
			 	{
			 		lastTabs = ui.index;
			 	}
			 	
			 }
		});
		
		
	function next(index)
	{
		lastTabs = index;
		$("#tabs").tabs("select",index);
	}
	
	
	function checkDeliveryInfo(index)
	{
		var address_name = $("#address_name").val();
		var address_street = $("#address_street").val();
		var address_city = $("#address_city").val();
		var address_zip = $("#address_zip").val();
		var tel = $("#tel").val();
		var ccid = $("#ccid_hidden").val();
		var pro_id = $("#pro_id").val();
		var address_state = $("#address_state").val();
		
		if(address_name==""||address_street==""||address_city==""||address_zip==""||ccid==0||pro_id==0||(pro_id==-1&&address_state==""))
		{
			alert("地址信息不完整！");
			next(1);
		}
		else
		{
			next(index)
		}
	}
	
	function saveCart()
	{
		$("#cart_form").submit();
	}
	
	function cleanWayBill()
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/cleanWayBIllCart.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			async:false,
			
			beforeSend:function(request){
				
			},
				
			error: function(){
			},
				
			success: function(html){
			}
		});
	}

	// add by zhangrui 
	function showInfo(){
		var div_info = $("#div_info");
		if(div_info.css("display") === "none"){
			div_info.slideDown("slow");
		}else{
			div_info.slideUp("slow");
		};
	}
</script>			
</body>
</html>
