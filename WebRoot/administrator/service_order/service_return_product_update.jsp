<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.ReturnTypeKey"%>
<%@page import="com.cwc.app.key.PaymentMethodKey"%>
<%@ include file="../../include.jsp"%> 
<%
String cmd = StringUtil.getString(request,"cmd");
long rp_id	= StringUtil.getLong(request, "rp_id");
long sid = StringUtil.getLong(request,"sid");
long oid = StringUtil.getLong(request,"oid");
//DBRow[] orderItems = orderMgr.getPOrderItemsByOid(oid);
DBRow[] orderItems = serviceOrderMgrZyj.getServiceOrderItemsByServiceId(sid);
DBRow returnProductRow = returnProductMgrZr.getReturnProductByRpId(rp_id);
int is_need_return = StringUtil.getInt(request, "is_need_return");
cartReturn.clearCart(session);
if(returnProductRow==null)
{
	returnProductRow = new DBRow();
} 
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/return_order/ReturnProductFileDownLoadAction.action";
String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
int[] fileTypes = {FileWithTypeKey.ProductReturn};
DBRow[] files = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(rp_id, fileTypes);
String loadReturnItemToCartAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/service_order/LoadReturnItemToCartAction.action";
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>更新退货</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">

@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<%--  <script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script> --%>
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<style type="text/css">
.normal-return-green-long {
	background-attachment: fixed;
	background: url(../imgs/product/normal_green_long.gif);
	background-repeat: no-repeat;
	background-position: center center;
	height: 33px;
	width: 86px;
	color: #000000;
	border:0px;
	font-size:11.5px;
	font-weight:normal;
	font-family:����;
	margin-left:5px;
	margin-right:5px;
}
*{padding:0px;margin:0px;}
	div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:500px;height:30px;background:white;margin-top:200px;line-height:30px;font-size:14px;}
   table tr td{line-height:25px;height:25px;}
   .td_left{border:1px solid red;width:100px;text-align:right;}
   .set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	span.addNewSpan{display:block;width:70px;height:20px;float:right;}
	ul.addressList{list-style-type:none;margin-left:10px;}
	ul.addressList li {text-indent:5px;line-height:25px;height:25px;margin-top:2px;border-bottom:1px dotted silver;cursor:pointer;}
	
	p.selectType{border:1px solid silver;background:#E6F3C5;height:35px;line-height:35px;}
	ul.type{border:1px solid silver;background:#E6F3C5;list-style-type:none;height:35px;line-height:35px;}
	ul.type li {margin-top:4px;float:left;border:1px solid silver;width:100px;line-height:25px;height:25px;margin-left:10px;text-align:center;cursor:pointer;}
	ul.type li.on{background:white;}
	span.button{-moz-transition: all 0.218s ease 0s; -moz-user-select: none;background: -moz-linear-gradient(center top , #F5F5F5, #F1F1F1) repeat scroll 0 0 #F5F5F5;border: 1px solid rgba(0, 0, 0, 0.1);
    border-radius: 2px 2px 2px 2px;color: #444444; cursor: pointer;font-size: 14px;font-weight: bold;height: 27px;line-height: 27px; min-width: 54px;
    outline: medium none;padding: 0 8px;text-align: center;display:block;margin-top:10px;
   }
   p.say {margin-top:8px;width:500px;}
   .infoList table td{}
   table.addressInfo td.right{text-align:right;font-weight:bold;color:#0066FF;}
   table.basic{border-collapse :collapse ;border:1px solid silver;}
   
   table.basic th{background:#e8f1fa;width:200px;font-size:12px; border:1px solid silver;}
   table.basic td{background:white;width:200px;text-indent:30px;border:1px solid silver;}
   table.itemTable{border-collapse :collapse ;border:1px solid silver;width:100%;}
   table.itemTable th{background:#e8f1fa;font-size:12px;font-weight:normal;line-height:35px;height:35px;border:1px solid silver;}
   table.itemTable td{line-height:30px;height:30px;border:1px solid silver;}
   table.itemTable td.name{text-indent:15px;}
   table.itemTable td.t{text-indent:10px;}
   table.itemTable tfoot  td{height:25px;border:none;text-align:center;}
   ul.itemDetail{list-style-type:none;margin-left:20px;margin-top:-2px;margin-bottom:5px;}
   ul.itemDetail li{border:0px solid silver;height:14px;line-height:14px;}
   input.noborder{width:250px;border:none;border-bottom:1px solid silver;}
<!--
td
{
	font-size:12px;
}
.STYLE1 {color: #FF0000}

form
{
	padding:0px;
	margin:0px;
}

-->
.set{border:2px silver solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:15px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
#fileTable tr td{border: 1px solid silver; };
</style>

<script>
var tip = []; 
var ccid = 0;
var pro_id = 0;
var country_name ;
var address_state_name  ;
var infoList ;
var fristForm ;
var isShipping = false;
var key ;
function showProductUnion(pc_id)
{
	if($("#"+pc_id).css("display")=="none")
	{
		$("#"+pc_id).css("display","");
	}
	else
	{
		$("#"+pc_id).css("display","none");
	}
}

function setPro_id(fillFlag,val,html){
	removeAfterProIdInput();
	var node = $("#ccid_hidden");
	var value = node.val();
 
	if(value*1 != 10929){
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:value},
				function callback(data){ 
					$("#pro_id").attr("disabled",false);
					$("#pro_id").clearAll();
					$("#pro_id").addOption("请选择......","0");
					if (data!=""){
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					if (value>0){
						$("#pro_id").addOption("手工输入","-1");
					}
					$("#addBillProSpan").html($("#pro_id"));
					// 如果是这个下面有 对应的那么就选中 如果是么有的那么就是 和点击手工输入一样的操作
					if(val){
						if(val === "-1"){
							$("option[value='"+val+"']",$("#pro_id")).attr("selected",true);
						 
							$("#addBillProSpan").append($("<input type='text' id='address_state_input' value='"+html+"' />"));
						 
						}else{
							$("option[value='"+val+"']",$("#pro_id")).attr("selected",true);
							removeAfterProIdInput();
						}
					}
				}
				
			);
		if(!fillFlag){
			if(value * 1 != 0 ){
				// 得到Option
				var option = $("option:selected",node);
				$("#address_country_code").val(option.attr("code"));
			}else{
				$("#address_country_code").val("");
			}
		}
	 }
 }
function allNoCheck(obj)
{
	var selVal = obj.value;
	$("input:checkbox[value^='"+selVal+"_']").attr("checked",false);
	if($("input:checkbox[value='"+selVal+"']").attr("checked"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty_return/PutReturnProductToCartAction.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:
			{
				type:"select",
				value:selVal
			},
			
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			
			error: function(){
				loadCartReturn();
				$("#action_info").text("商品添加失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException")
				{
					alert("商品不存在，不能添加到订单！");
					$("#p_name").select();
				}
				else if(msg=="ProductDataErrorException")
				{
					alert("商品基础数据错误，不能添加到订单！请联系调度");
					$("#p_name").select();
				}
				else if (msg=="ok")
				{
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("1");
					$("#reason").val("");
					loadCartReturn();
				}
				else
				{
					alert(1);
					loadCartReturn();
					alert(msg);
				}
			}
		});	
	}
	else
	{
		delProductFromCartSelect(selVal);
	}
}

function productNotCheck(obj)
{
	var productItem		= obj.value;
	var productItemArr	= productItem.split("_");
	var serviceItemId	= productItemArr[0];
	var unitId			= productItemArr[1];
	var unitCount		= productItemArr[2];
	$("input:checkbox[value='"+serviceItemId+"_"+unitId+"_"+unitCount+"']").attr("checked",false);
	if($("input:checkbox[value='"+productItem+"']").attr("checked"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty_return/PutReturnProductToCartAction.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:
			{
				type:"select",
				value:obj.value
			},
			
			beforeSend:function(request){
				$("#action_info").text("正在添加商品......");
			},
			
			error: function(){
				loadCartReturn();
				$("#action_info").text("商品添加失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				
				if (msg=="ProductNotExistException")
				{
					alert("商品不存在，不能添加到订单！");
					$("#p_name").select();
				}
				else if(msg=="ProductDataErrorException")
				{
					alert("商品基础数据错误，不能添加到订单！请联系调度");
					$("#p_name").select();
				}
				else if (msg=="ok")
				{
					//商品成功加入购物车后，要清空输入框
					$("#p_name").val("");
					$("#quantity").val("1");
					$("#reason").val("");
					loadCartReturn();
				}
				else
				{
					alert(1);
					loadCartReturn();
					alert(msg);
				}
			}
		});	
	}
	else
	{
		delProductFromCartSelect(productItem);
	}
}


$().ready(function() {

	show_button();
	function log(event, data, formatted) {

		
	}
	addAutoComplete($("#p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
			"p_name");
	
	ajaxCartForm();
	var flag = $("input:radio[name=return_product_flags]:checked").val();
	if('<%=ReturnTypeKey.NOT_NEED%>'*1 != flag)
	{
		$.ajax({
			url: '<%=loadReturnItemToCartAction%>',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:{rp_id:<%=rp_id%>},
			beforeSend:function(request){
				$("#action_info").text("加载购物车......");
			},
			error: function(){
				loadCartReturn();
				$("#action_info").text("加载购物车失败！");
			},
			success: function(html){
				loadCartReturn();
			}
		});
	}
	else
	{
		loadCartReturn();
	}
	//处理省市
	setPro_id(false,'<%=returnProductRow.get("pro_id",0l)%>','<%=returnProductRow.getString("address_state")%>');
});


//把表单ajax化
function ajaxCartForm()
{
	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   //beforeSubmit:  // pre-submit callback    
			//	function(){
			//		loadCart();
			//	}	
		  // ,  
		   success:       // post-submit callback  
				function(){
					loadCartReturn();
					cartProQuanHasBeChange = false;
				}
		       
	};
	

   $('#cart_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
}

function loadCartReturn()
{
	//alert(para);

	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_return_product_items_update.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		
		beforeSend:function(request){
			
		},
		
		error: function(){
			alert("加载购物车失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
		}
	});
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 

function checkForm()
{
	var theForm = document.search_union_form;
	
	if(theForm.p_name.value=="")
	{
		alert("商品名不能为空");
		return(false);
	}
	else if(theForm.quantity.value=="")
	{
		alert("数量不能为空");
		return(false);
	}
	else if(!isNum(theForm.quantity.value))
	{
		alert("请正确填写数量");
		return(false);
	}
	else
	{

				//先检查购物车中的商品在当前仓库是否已经建库
				var para = "type=add&value="+theForm.p_name.value+"_"+theForm.quantity.value;
			
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty_return/PutReturnProductToCartAction.action',
					type: 'post',
					dataType: 'html',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(){
						alert("添加商品失败！");
					},
					
					success: function(msg)
					{				
						if (msg=="ProductNotExistException")
						{
							alert("商品不存在！");
						}
						else if (msg=="ProductUnionSetCanBeProductException")
						{
							alert("套装不能作为定制商品");
						}
						else if (msg=="ProductNotCreateStorageException")
						{
							//
						}
						else if (msg=="ok")
						{
							//商品成功加入购物车后，要清空输入框
							theForm.p_name.value = "";
							theForm.quantity.value = "";
							loadCartReturn();
							return(false);
						}
						else
						{
							alert(msg);
						}

					}
				});	
				

	}
}


function delProductFromCart(value,p_name)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty_return/RemoveSelectProductFromCartReturnAction.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"value="+value,
					
			error: function(){
				alert("商品删除失败！");
			},
			
			success: function(msg){
				loadCartReturn();
			}
		});	
	}
}

function delProductFromCartSelect(pid)
{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty_return/RemoveSelectProductFromCartReturnAction.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"value="+pid,
					
			error: function(){
				alert("商品删除失败！");
			},
			
			success: function(msg){
				loadCartReturn();
			}
		});	
}

var cartProQuanHasBeChange = false;
var cartIsEmpty;

function onChangeQuantity(obj)
{
	if ( !isNum(obj.value) )
	{
		alert("请正确填写数字");
		return(false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}
function needSaveTemp(obj)
{
	obj.style.background="#FFB5B5";
	cartProQuanHasBeChange = true;
}
function post(oid,cmd,is_send_mail)
{
	var is_need_return = $("input:radio[name=return_product_flags]:checked").val();
	/*if($('#note').val()=="")
	{
		alert("请填写备注");
	}
	else
	{*/
		if(<%=ReturnTypeKey.NEED%>*1 == is_need_return || <%=ReturnTypeKey.VIRTUAL%>*1 == is_need_return)
		{
			if($("#ps_id").val()==0)
			{
				alert("请选择退货仓库");
			}
			else if($("input:radio[name=return_product_flags]:checked").val()==1&&cartIsEmpty)
			{
				alert("请添加需要退货的商品");
			}
			else if (cartProQuanHasBeChange)
			{
				alert("请先保存修改数据");
			}
			else
			{
				var addressInfo = "&first_name="+$("#first_name").val()
				+ "&last_name="+$("#last_name").val()
				+ "&address_street="+$("#address_street").val()
				+ "&address_city="+$("#address_city").val()
				+ "&ccid="+$("#ccid_hidden").val()
				+ "&pro_id="+$("#pro_id").val()
				+ "&address_zip="+$("#address_zip").val()
				+ "&tel="+$("#tel").val()
				+ "&address_country="+$("#ccid_hidden option:selected").html()
				+ "&address_country_code="+$("#ccid_hidden option:selected").attr("code");

				if(-1 == $("#pro_id").val())
				{
					addressInfo += '&address_state='+$("#address_state_input").val();
				}
				else
				{
					addressInfo += '&address_state='+$("#pro_id option:selected").html();
				}
					$("#note1").val($('#note').val());
					$("#oid1").val(oid);
					$("#return_product_flag4").val($("input:radio[name=return_product_flags]:checked").val());
					$("#ps_id1").val($("#ps_id").val());
					$("#is_send_mail").val(is_send_mail);
				
					$("#file_names_virtual").val($("#file_names").val());
					
					$.ajax({
						url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/ModReturnOrderAction.action',
						data:$("#listForm").serialize()+addressInfo,
						dataType:'json',
						type:'post',
						beforeSend:function(request){
							$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
						},
						error: function(){
							$.unblockUI();
							//showMessage("系统错误","error");
						},
						success:function(data){
							setTimeout("windowClose()",1000);
						}
					})	
				
			}
		}
		else if(<%=ReturnTypeKey.NOT_NEED%>*1 == is_need_return)
		{
			$("#is_need_return").val(is_need_return);
			/*if(<%=ReturnTypeKey.VIRTUAL%>*1 == is_need_return && ((null == $("#file_names").val() || "" == $("#file_names").val()) && 0 == <%=files.length%>*1))
			{
				alert("请先上传文件");
			}
			else
			{
				var addressInfoFile = "&first_name="+$("#first_name").val()
				+ "&last_name="+$("#last_name").val()
				+ "&address_street="+$("#address_street").val()
				+ "&address_city="+$("#address_city").val()
				+ "&ccid="+$("#ccid_hidden").val()
				+ "&pro_id="+$("#pro_id").val()
				+ "&address_zip="+$("#address_zip").val()
				+ "&tel="+$("#tel").val()
				+ "&address_country="+$("#ccid_hidden option:selected").html()
				+ "&address_country_code="+$("#ccid_hidden option:selected").attr("code");

				if(-1 == $("#pro_id").val())
				{
					addressInfoFile += '&address_state='+$("#address_state_input").val();
				}
				else
				{
					addressInfoFile += '&address_state='+$("#pro_id option:selected").html();
				}
				serviceFileFormSub(addressInfoFile);
			}*/
			serviceFileFormSub();
		}
	//}
}
function serviceFileFormSub()
{
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/SaveReturnOrderForVirtualAndNotAction.action',
		data:$("#serviceFileForm").serialize()+"&note="+$("#note").val(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		error: function(){
			$.unblockUI();
		},
		success:function(data){
			if(data && data.flag == "true"){
				//showMessage("上传成功","success");
				setTimeout("windowClose()",1000);
			}else{
				//showMessage("上传失败","error");
				window.location.reload();
			}
		}
	})	
}
function show_button()
{
	/*var flag = $("input:radio[name=return_product_flags]:checked").val();
	if(flag==1)
	{
		$("#return_product_row").show();
		$("#serivce_virtual").hide();
	}
	else if(flag==2)
	{
		$("#serivce_virtual").show();
		$("#return_product_row").hide();
	}
	else if(flag==2)
	{
		$("#return_product_row").hide();
		$("#serivce_virtual").hide();
	}*/
}
$(function(){
	var is_need_return = $("input:radio[name=return_product_flags]:checked").val();
	if('<%=ReturnTypeKey.NEED%>'*1 == is_need_return)
	{
		$("#return_product_row").show();
		$("#serivce_virtual").hide();
		$("#return_address_info").hide();
		$("#sendMailButton").attr("style","display:inline;");
		$("#notSendMailButton").attr("style","display:inline;");
	}
	else if('<%=ReturnTypeKey.VIRTUAL%>'*1 == is_need_return)
	{
		$("#serivce_virtual").show();
		$("#return_product_row").show();
		$("#return_address_info").hide();
		$("#sendMailButton").attr("style","display:none");
		$("#notSendMailButton").val("提交");
	}
	else
	{
		$("#return_product_row").hide();
		$("#serivce_virtual").hide();
		$("#return_address_info").hide();
		$("#sendMailButton").attr("style","display:none");
		$("#notSendMailButton").val("提交");
	}
	$("input:radio[name=return_product_flags]").click(
		function(){
			var val = $(this).val();
			if('<%=ReturnTypeKey.NEED%>'*1 == val)
			{
				$("#return_product_row").show();
				$("#serivce_virtual").hide();
				$("#return_address_info").hide();
				$("#sendMailButton").attr("style","display:inline;");
				$("#notSendMailButton").attr("style","display:inline;");
				$("#notSendMailButton").val("提交不发送邮件");
			}
			else if('<%=ReturnTypeKey.VIRTUAL%>'*1 == val)
			{
				$("#serivce_virtual").show();
				$("#return_product_row").show();
				$("#return_address_info").hide();
				$("#sendMailButton").attr("style","display:none");
				$("#notSendMailButton").val("提交");
			}
			else
			{
				$("#return_product_row").hide();
				$("#serivce_virtual").hide();
				$("#return_address_info").hide();
				$("#sendMailButton").attr("style","display:none");
				$("#notSendMailButton").val("提交");
			}
		}
	);
	
});
//文件上传
function uploadFile(_target){
    var obj  = {
	     reg:"all",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
	}});
}
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_names']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			 
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
		var td = $("#over_file_td");
		td.append(lis);
	} 
	

}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var  a = "<p class='new'><a href="+uri+">"+fileName+"</a>&nbsp;&nbsp;"
    		+'<input type="button" class="short-short-button" value="删除" onclick="delNewUploadFile(this)"/>'
    		+"</p>";
    return a ;
}

function delNewUploadFile(delBut)
{
	$(delBut).parent().remove();
}
function onlineScanner()
{
 	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
 		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
//图片在线显示
function showPictrueOnline(fileWithType,fileWithId , currentName){
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>/returnImg'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}
function deleteFile(file_id, file_name){
	if(confirm("确定要删除文件：["+file_name+"]吗？")){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'file',file_id:file_id,pk:'file_id'},
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}else{
					//showMessage("系统错误,请稍后重试","error");
				}
			},
			error:function(){
				//showMessage("系统错误,请稍后重试","error");
			}
		})
	}
}
function windowClose()
{
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
}
function windowCloseNotFresh()
{
	$.artDialog && $.artDialog.close();
}
function removeAfterProIdInput(){
	
	$("#address_state").remove();
 }
function fixAddress(){
	 var node = $("#pro_id");
	 var value = node.val();

	 if(value*1 == -1){
		 addInputAfterProid(node);
	 }else{
		 removeAfterProIdInput();
	}
}
function addInputAfterProid(node){
	 var input  = "<input type='text' id='address_state_input' />";
	 $(input).insertAfter(node);
}
function removeAfterProIdInput(){
	
	$("#address_state").remove();
}
function warrantySelectPro(pro_id)
{
	$("#pro_id").val(pro_id)
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form id="listForm" method="post" action="">
	<input id="note1" name="note" type="hidden"/>
	<input id="oid1" name="oid" type="hidden" />
	<input id="return_product_flag4" name="return_product_flag" type="hidden"/>
	<input id="ps_id1" name="ps_id" type="hidden"/>
	<input id="sid" name="sid" type="hidden" value="<%=sid%>"/>
	<input id="rp_id" name="rp_id" type="hidden" value="<%=rp_id%>"/>
	<input id="is_send_mail" name="is_send_mail" value="" type="hidden"/>
	<input type="hidden" name="file_names_virtual" id="file_names_virtual"/>
    <input type="hidden" name="sn" value="SR"/>
	<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.ProductReturn %>" />
	<input type="hidden" name="system_file_path" value="returnImg" />
</form>
<table height="100%" width="100%">
	<tr height="90%" valign="top">
		<td width="100%" valign="top">
			<table width="100%" align="left">
				<tr>
					<td align="right"><strong>是否退货</strong></td>
					<td align="left">
						<input type="radio" name="return_product_flags" id="return_product_flag_1" value="1" <% if(ReturnTypeKey.NEED==is_need_return||0==is_need_return){out.println("checked='checked'");} %>>需要退货
				      	<input type="radio" name="return_product_flags" id="return_product_flag_2" value="2" <% if(ReturnTypeKey.VIRTUAL==is_need_return){out.println("checked='checked'");} %>>虚拟退货
					  	<input type="radio" name="return_product_flags" id="return_product_flag_3" value="3" <% if(ReturnTypeKey.NOT_NEED==is_need_return){out.println("checked='checked'");} %>>无需退货
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;</td>
					<td align="left">
						<table width="96%" align="left" id="return_product_row">
						  <tr>
						    <td>
								<fieldset style="border:1px #999999 solid;padding:10px;-webkit-border-radius:4px;-moz-border-radius:4px;width:98%;">
								<legend style="font-size:15px;font-weight:normal;color:#666666;">添加需要退货商品</legend>
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
									   <tr>
									    	<td width="70%" style="padding-bottom:10px;">
								 				<table border="0" cellpadding="0" cellspacing="0">
								  					<%
								  						for(int i = 0;i<orderItems.length;i++)
								  						{
								  							DBRow product = productMgr.getDetailProductByPcid(orderItems[i].get("pid",0l));
								  							
								  							DBRow[] productUnions = productMgr.getProductUnionsBySetPid(product.get("pc_id",0l));
								  					%>
								  					<tr>
								  						<td style="font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold">
								  							<% 
								  							if(productUnions.length>0)
								  							{
								  							%>
								  							<span onclick="showProductUnion(<%=product.get("pc_id",0l)%>)" style="text-decoration:underline;cursor:pointer;"><%=product.getString("p_name")%></span>
								  							<%
								  							}
								  							else
								  							{
								  							%>
								  							<%=product.getString("p_name")%>
								  							<%
								  							}
								  							%>
								  							
								  						</td>
								  						<td align="left">
								  							<%
								  								if(product.get("orignal_pc_id",0)==0)
								  								{
								  							%>
								  							<input type="checkbox" value='<%=orderItems[i].get("soi_id",0L)+"_"+product.get("pc_id",0L)+"_"+orderItems[i].get("quantity",0F)%>' onclick="allNoCheck(this)"/>&nbsp;x&nbsp;<%=orderItems[i].get("quantity",0F) %>
								  							&nbsp;<%="SI_"+orderItems[i].get("soi_id",0L) %>
								  							<%
								  								}
								  							%>
								  						</td>
								  					</tr>
								  					<% 
								  							if(productUnions.length>0)
								  							{
								  					%>
								  					<tr id="<%=product.get("pc_id",0l)%>" style="display:none">
								  						<td>
								  							<table border="0" cellpadding="0" cellspacing="0">
								  								<%
								  									for(int j = 0;j<productUnions.length;j++)
								  									{
								  										DBRow unionProduct = productMgr.getDetailProductByPcid(productUnions[j].get("pid",0l));
								  								%>
								  								<tr>
								  									<td><%=unionProduct.getString("p_name")%></td>
								  									<td><input onclick="productNotCheck(this)" name="<%=product.get("pc_id",0l)%>" id="<%=unionProduct.get("pc_id",0l)%>" type="checkbox" value="<%=orderItems[i].get("soi_id",0l)+"_"+product.get("pc_id",0l)+"_"+orderItems[i].get("quantity",0F)+"_"+unionProduct.get("pc_id",0l)%>"/></td>
								  								</tr>
								  								<%
								  									}
								  								%>
								  							</table>
								  						</td>
								  					</tr>
								  					<%
								  							}
								  					%>
								  					
								  					<%
								  						}
								  					%>
								  				</table>
											</td>
						    				<td width="30%" valign="top" align="right" style="padding-bottom:10px;"><strong>退货仓库</strong>
						    					<select name="ps_id" id="ps_id">
											        <option value="0">请选择...</option>
												        <%
															DBRow treeRows[] = catalogMgr.getProductReturnStorageCatalogTree();
															String qx;
															for ( int i=0; i<treeRows.length; i++ )
															{
																if ( treeRows[i].get("parentid",0) != 0 )
																 {
																	qx = "├ ";
																 }
																 else
																 {
																	qx = "";
																 }
																if(treeRows[i].get("id",0l) == returnProductRow.get("ps_id",0L))
																{
														%>
															<option value="<%=treeRows[i].getString("id")%>" selected="selected"> 
													          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%><%=qx%>
													          <%=treeRows[i].getString("title")%>
															</option>
														<%		
																}
																else
																{
														%>
															<option value="<%=treeRows[i].getString("id")%>"> 
													          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%><%=qx%>
													          <%=treeRows[i].getString("title")%>
															</option>		
														<%			
																}
															}
														%>
										      </select>
										   </td>
						   				</tr>
						  				<tr>
						    				<td colspan="2" bgcolor="#eeeeee" style="padding-left:10px;padding-left:toppx;padding-bottom:5px;border-bottom:1px #999999 solid">
												<form name="search_union_form" method="post" action="">
													 商品<input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:150px;"/> 
													 数量<input name="quantity" type="text" id="quantity"  style="color:#FF3300;width:40px;"/>&nbsp;&nbsp;
												    <input name="Submit" type="button" class="short-button" value="添加" onClick="checkForm()">	
												</form>
											</td>
						  				</tr>
						  				<tr>
										    <td colspan="2">
												<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty_return/ModReturnProductQualityCartAction.action" method="post" name="cart_form" id="cart_form" >
													<div id="mini_cart_page" style="color:#CCCCCC">数据加载中……</div>
												</form>
											</td>
						 				</tr>
									</table>
						         </fieldset>	
							</td>
						  </tr>
						</table><br/>
						<table width="96%" align="left" id="return_address_info">
							<tr>
						  	<td>
						  		<fieldset class="set" style="border:2px solid #993300;padding:5px;">
						  		<legend>地址信息</legend>
						 		<div>
			 			 			 <!--  address_name,address_street,address_city,address_zip -->
									<table class="addressInfo">
										<tr>
											<td class="right">First Name :</td>
											<td>&nbsp;<input type="text" id="first_name" value="<%=returnProductRow.getString("first_name")%>"/></td>
										</tr>
										<tr>
											<td class="right">Last Name :</td>
											<td>&nbsp;<input type="text" id="last_name" value="<%=returnProductRow.getString("last_name")%>"/></td>
										</tr>
										 <tr>
											<td class="right">Street :</td>
											<td>&nbsp;<input type="text" id="address_street" value="<%=returnProductRow.getString("address_street")%>" size="52.5px;"/></td>
										</tr>
										 <tr>
											<td class="right">City :</td>
											<td>&nbsp;<input type="text" id="address_city" value="<%=returnProductRow.getString("address_city")%>" size="52.5px;"/></td>
										</tr>
										 <tr>
											<td class="right">Country :</td>
											<td>
									<!-- 国家省份信息 -->
										<%
											DBRow countrycode[] = orderMgr.getAllCountryCode();
											String selectBg="#ffffff";
											String preLetter="";
										%>
								      	&nbsp;<select id="ccid_hidden" onChange="removeAfterProIdInput();setPro_id();">
									 	 	<option value="0">请选择...</option>
											  <%
											  for (int i=0; i<countrycode.length; i++){
											  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
													if (selectBg.equals("#eeeeee")){
														selectBg = "#ffffff";
													}else{
														selectBg = "#eeeeee";
													}
												}  	
												preLetter = countrycode[i].getString("c_country").substring(0,1);
											  %>
									  		  <option <%=returnProductRow.getString("ccid").equals(countrycode[i].getString("ccid"))?"selected=\"selected\"":""%> style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
											  <%
											  }
											%>
						     			 </select>
										</td>
										</tr>
										 <tr>
											<td class="right">State :</td>
											<td>
												&nbsp;<span id="addBillProSpan"><select id="pro_id" onchange="fixAddress()" style="margin-right:5px;" disabled="disabled"></select></span>
												 <script type="text/javascript">
								     			 	setPro_id();
								     			 	warrantySelectPro(<%=returnProductRow.get("pro_id",0l)%>);//质保账单时进入页面默认选中省份
								     			 </script>
											</td>
										</tr>
										 <tr>
											<td class="right">Zip :</td>
											<td>&nbsp;<input type="text" id="address_zip" value="<%=returnProductRow.getString("address_zip")%>"/></td>
										</tr>
										 <tr>
											<td class="right">Tel :</td>
											<td>&nbsp;<input type="text"  id="tel" value="<%=returnProductRow.getString("tel")%>"/></td>
										</tr>
										<input type="hidden" id="address_country_code" name="address_country_code" value="<%=returnProductRow.getString("address_country_code")%>"/>
									</table>
									
									<p>
										<ul class="addressList">
			 							</ul>
									</p>
						 			
						 		</div>
						 	 
							</fieldset>
						  	</td>
						  </tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;</td>
					<td align="left">
					<br/>
						<form action='' method="post" id="serviceFileForm">
						<input type="hidden" name="backurl" value='<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_return_product_update.html?sid=<%=sid %>&oid=<%=oid %>&rp_id=<%=rp_id %>&cmd=<%=cmd %>'/>
						<input type="hidden" name="sid" value='<%=sid %>'/>
						<input type="hidden" name="rp_id" value="<%=rp_id %>"/>
						<input type="hidden" name="oid" value='<%=oid %>'/>
						<input type="hidden" name="is_need_return" id="is_need_return"/>
						<table id="serivce_virtual" width="100%" align="left">
							<tr>
								<td width="8%">上传文件:</td>
								<td width="92%" align="left">
									<div id="jquery_file_up">	
						          		<input type="hidden" name="file_names" id="file_names" value=""/>
						          		<ol class="fileUL"> 
						          		</ol>
						          	</div>
						          	<input type="hidden" name="sn" value="SR"/>
									<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.ProductReturn %>" />
									<input type="hidden" name="system_file_path" value="returnImg" />
									<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件" />
									<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
								</td>
							</tr>
							<tr>
								<td width="8%">&nbsp;</td>
								<td width="92%" align="left" valign="top" >
									<table width="70%" style="border-collapse: collapse;">
										<%
											if(files.length > 0){
												for(int f = 0; f < files.length; f ++){
													DBRow file = files[f];
										%>
											<tr style="height: 20px;">
												<td width="50%" align="left">
												 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
								 			 	 <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
								 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
								 			 	 	<%if(StringUtil.isPictureFile(file.getString("file_name"))){ %>
										 			 	 <p>
										 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.ProductReturn %>','<%=rp_id%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
										 			 	 </p>
									 			 	 <%} else if(StringUtil.isOfficeFile(file.getString("file_name"))){%>
									 			 	 		<p>
									 			 	 			<a href="javascript:void(0)"  file_id='<%=file.get("file_id",0l) %>' onclick="openOfficeFileOnlineReturnFile(this,'<%=ConfigBean.getStringValue("systenFolder")%>','returnImg')" file_is_convert='<%=file.get("file_is_convert",0) %>'><%=file.getString("file_name") %></a>
									 			 	 		</p>
									 			 	 <%}else{ %>
									 			 	 	  		<p><a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder=returnImg'><%=file.getString("file_name") %></a></p> 
									 			 	 	  <%} %>
												
												</td>
												<td width="15%" align="left">
													<%=null == adminMgrLL.getAdminById(file.getString("upload_adid"))?"":adminMgrLL.getAdminById(file.getString("upload_adid")).getString("employe_name") %>
												</td>
												<td width="25%" align="left">
												<% if(!"".equals(file.getString("upload_time"))){
														out.println(DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(file.getString("upload_time"))));
													}
												%>
												</td>
												<td width="10%">
														<a href="javascript:deleteFile('<%=file.get("file_id",0l) %>','<%=file.getString("file_name") %>')">删除</a>
												</td>
											</tr>
										<%	
												}
											}
										%>
									</table>
									<table id="newFileTable"><tr><td id="over_file_td"></td></tr></table>
								</td>
							</tr>
						</table>
						</form>
					</td>
				</tr>
				<tr>
				    <td align="right"><strong>备注信息</strong></td>
				    <td align="left">
						&nbsp;<textarea name="note" id="note" style="width:810px;height:75px;border:1px #999999 solid"><%=returnProductRow.getString("note") %></textarea>
					</td>
			  	</tr>
			</table>
		</td>
	</tr>
	<tr height="10%" valign="bottom"> 
		<td align="right" valign="middle" class="win-bottom-line" width="98%">
		  <input name="Submit" id="sendMailButton" type="button" class="normal-return-green-long" value="提交并发送邮件" onClick="post(<%=oid%>,'<%=cmd%>',1)">
	      <input name="Submit" id="notSendMailButton" type="button" class="normal-return-green-long" value="提交不发送邮件" onClick="post(<%=oid%>,'<%=cmd%>',2)">
	      <input name="cancel" type="button" class="normal-white" value="取消" onClick="windowCloseNotFresh();">
	    </td>
	</tr>
</table>

</body>
</html>