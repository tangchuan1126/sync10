<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.InvoiceStateKey"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.PaymentMethodKey,java.util.ArrayList,com.cwc.app.key.BillTypeKey,java.util.ArrayList,com.cwc.app.key.InvoiceStateKey" %>
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单报价</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">

@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<style type="text/css">
*{padding:0px;margin:0px;}
	div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:500px;height:30px;background:white;margin-top:200px;line-height:30px;font-size:14px;}
   table tr td{line-height:25px;height:25px;}
   .td_left{border:1px solid red;width:100px;text-align:right;}
   .set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	span.addNewSpan{display:block;width:70px;height:20px;float:right;}
	ul.addressList{list-style-type:none;margin-left:10px;}
	ul.addressList li {text-indent:5px;line-height:25px;height:25px;margin-top:2px;border-bottom:1px dotted silver;cursor:pointer;}
	
	p.selectType{border:1px solid silver;background:#E6F3C5;height:35px;line-height:35px;}
	ul.type{border:1px solid silver;background:#E6F3C5;list-style-type:none;height:35px;line-height:35px;}
	ul.type li {margin-top:4px;float:left;border:1px solid silver;width:100px;line-height:25px;height:25px;margin-left:10px;text-align:center;cursor:pointer;}
	ul.type li.on{background:white;}
	span.button{-moz-transition: all 0.218s ease 0s; -moz-user-select: none;background: -moz-linear-gradient(center top , #F5F5F5, #F1F1F1) repeat scroll 0 0 #F5F5F5;border: 1px solid rgba(0, 0, 0, 0.1);
    border-radius: 2px 2px 2px 2px;color: #444444; cursor: pointer;font-size: 14px;font-weight: bold;height: 27px;line-height: 27px; min-width: 54px;
    outline: medium none;padding: 0 8px;text-align: center;display:block;margin-top:10px;
   }
   p.say {margin-top:8px;width:500px;}
   .infoList table td{}
   table.addressInfo td.right{text-align:right;font-weight:bold;color:#0066FF;}
   table.basic{border-collapse :collapse ;border:1px solid silver;}
   
   table.basic th{background:#e8f1fa;width:200px;font-size:12px; border:1px solid silver;}
   table.basic td{background:white;width:200px;text-indent:30px;border:1px solid silver;}
   table.itemTable{border-collapse :collapse ;border:1px solid silver;width:100%;}
   table.itemTable th{background:#e8f1fa;font-size:12px;font-weight:normal;line-height:35px;height:35px;border:1px solid silver;}
   table.itemTable td{line-height:30px;height:30px;border:1px solid silver;}
   table.itemTable td.name{text-indent:15px;}
   table.itemTable td.t{text-indent:10px;}
   table.itemTable tfoot  td{height:25px;border:none;text-align:center;}
   ul.itemDetail{list-style-type:none;margin-left:20px;margin-top:-2px;margin-bottom:5px;}
   ul.itemDetail li{border:0px solid silver;height:14px;line-height:14px;}
   input.noborder{width:250px;border:none;border-bottom:1px solid silver;}
</style>
<%
	String getAddressInfoList = ConfigBean.getStringValue("systenFolder")+"action/administrator/order/GetAddressInfoListAction.action";
	String addBillAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/AddBillAction.action";
	String getAccountInfo = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/GetAccountInfoAction.action";
	String addEbayBillAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/AddEbayBillAction.action";
	String addBillByWeightAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/bill/AddBillByWeightAction.action";
	// 用于在详情的页面点击修改的时候直接跳转到修改商品的页面	
	String updateTarget = StringUtil.getString(request,"update_target");
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
  
	
 	// update 和 add 都是同一个页面
 	
 	long billId = StringUtil.getLong(request,"bill_id");
	long adid = 0l;
 	DBRow billRow = new DBRow();
 	if(billId != 0l){
 		billRow = orderMgrZr.getDetailById(billId,"bill_order","bill_id");
 		adid = billRow.get("create_adid",0l);
 		 
 	}else{
 		  adid = adminLoggerBean.getAdid(); 
 	}
 	BillTypeKey billTypekey = new BillTypeKey();
 
 	DBRow userInfo = waybillMgrZR.getUserNameById(adid);
 	
 	
 	//创建质保账单使用参数
 	long sid = StringUtil.getLong(request,"sid");
 	long oid = StringUtil.getLong(request,"oid");
 	DBRow orderDeliveryInfo = orderMgr.getDetailPOrderByOid(oid);
 	if(orderDeliveryInfo==null)
 	{
 		orderDeliveryInfo = new DBRow();
 	}
%>
<script type="text/javascript">
 
 var tip = []; 
 var ccid = 0;
 var pro_id = 0;
 var country_name ;
 var address_state_name  ;
 var infoList ;
 var fristForm ;
 var isShipping = false;
 var key ;
 jQuery(function($){
	 $("option[value='"+<%= billRow.get("key_type",0)%>+"']",$("#sel")).attr("selected",true);
	 $("#sel").chosen({no_results_text: "没有匹配形式"}).change(function(){
		  key =  $(this).val() ;
		 if(key * 1 == -1){
			 //把后面的信息都移除
			 $("#memberDiv").html("");
		 }
		 else
		 {
			 $("#memberDiv").html("");
			 ajaxGetAccountInfo($(this).val());
		    if(key*1==<%=PaymentMethodKey.Warranty%> || key * 1 == <%= PaymentMethodKey.PP%>)
			 {
			 	$("#warranty_payer").css("display","");
			 }
			 else
			 {
			 	$("#warranty_payer").css("display","none");
			 }
			 
		 }
	 });
	
	 $(".chzn-search input").width(110);
	var billId = '<%= billId%>';
	
		if(billId * 1 >0 ){
			initForm();
		}
	//如果是质保账单进来的时候把Client_id和PayerEmail设置成一样的
	if('<%= sid%>' * 1 > 0){
		$("#payer_email").val($("#client_id").val());
	}
 

	 
 })
   
 // update initForm()
 function initForm(){
	 // 收款信息
	 
	if('<%= billRow.getString("invoice_id")%>'.length > 0){
		$.artDialog.tips("<span style='color:#f60;'>你修改的账单,已经包含有Invoice,保存后记得UpdateInvoice!</span>", 3.5);
		//重新计算invoice_state 在原来的基础上加3
		var oldInvoiceState = '<%= billRow.getString("invoice_state")%>' * 1 + 3;
	 
		$("#invoice_state").val(oldInvoiceState);
	}
	$("#create_time").html('<%= billRow.getString("create_date")%>');
	$("#account_name").html('<%= billRow.getString("account_name")%>');
	$("#account").html('<%= billRow.getString("account")%>');
	$("#account_payee").val('<%= billRow.get("account_payee",0)%>');
	if('<%= billRow.get("account_payee",0)%>' * 1 == -2){
	
		$("#account").html("<input class='noborder' type='text' value="+'<%= billRow.getString("account")%>'+"/>'");
		$("#account_name").html("<input class='noborder' type='text' value="+'<%= billRow.getString("account_name")%>'+"/>'");
	 
	}else{
		$("#create_time").html('<%= billRow.getString("create_date")%>');
		$("#account_name").html('<%= billRow.getString("account_name")%>');
	}
	$("#key_type").val('<%= billRow.get("key_type",0)%>');
	$("#tel").val('<%= billRow.getString("tel")%>');
	//地址信息 client_id,address_name,address_street,address_city,address_zip
	 $("#client_id").val('<%= billRow.getString("client_id")%>');
	 $("#baseInfo input[name='address_name']").val('<%= billRow.getString("address_name")%>');
	 $("#baseInfo input[name='address_street']").val('<%= billRow.getString("address_street")%>');

	 $("#baseInfo input[name='address_city']").val('<%= billRow.getString("address_city")%>');
	 $("#baseInfo input[name='address_zip']").val('<%= billRow.getString("address_zip")%>');
	 $("option[value='"+'<%= billRow.get("ccid",0)%>'+"']",$("#ccid_hidden")).attr("selected",true);
	 var pro_id = '<%= billRow.get("pro_id",0)%>';
	 
	 setPro_id(false,pro_id ,'<%= billRow.getString("address_state")%>');
	//memo,note
		$("#memo_note").val('<%= billRow.getString("memo_note")%>');
 
		// 让 li 中的一个选中billRow.getString("note")
		// 这里如果读取错误的话 应该要报错。。。回头再做
		
		
		// update 的时候先不支持 在 ebay 和 shipping , Products的切换. 在点击商品信息的时候去Load 东西
		// 如果是Ebay的话 就要单独的处理。如果是shipping,Products 那么吧Item读入Session 中 然后调用loadCart方法
		var target = '<%= billTypekey.getStatusById(billRow.get("bill_type",1)).toLowerCase()%>';
		typeLiselected(target);
		 
		//li remove
		$(".type li[target!='"+target+"']").remove();
	 
 }
 function ajaxGetAccountInfo(key){
	 var div = $("#memberDiv");
	 div.append("<img src='../imgs/ajax-loader.gif' style='margin-top:4px;margin-left:4px;'/>")
	$.ajax({
		url:'<%= getAccountInfo%>' + "?key="+key,
		dataType:'json',
		success:function(data){
			 $("img",div).remove();
			
			if(data){
				var selected = createMemberSelect(data);
				div.append($(selected));
			  	$("#member").chosen({no_results_text: "没有该人"}).change(function(){
			   
					var value = ($(this).val());
					if(value != "-1"){
						if(value == "-2"){
							 $("#key_type").val(key);
							//-2 表示的是手工输入
							var key_type = ($("#sel").val());
							var tipAccountName = "";
							var tipAcccount = "";
							for(var index = 0 , count = tip.length ; index < count ; index++ ){
								if(key_type  === tip[index]["key_type"]){
									tipAccountName = "*"+tip[index]["account_name"];
									tipAcccount = "*"+tip[index]["account"];
									break;
								}	 
							}
							$("#account_name").html("<input type='text' class='noborder' onfocus=inputIn(this,'"+tipAccountName+"') onblur=outInput(this,'"+tipAccountName+"') value='"+tipAccountName+"' />");
							$("#account").html("<input type='text' class='noborder' onfocus=inputIn(this,'"+tipAcccount+"') onblur=outInput(this,'"+tipAcccount+"') value='"+tipAcccount+"'/>")
							$("#account_payee").val("-2");
						}else{
							 $("#key_type").val(key);
							var option = $("option:selected",$("#member"));
							
							var optionHtml = option.html();
						 
							var target = option.attr("target");
							$("#account_name").html(optionHtml);
							$("#account").html(value);
							$("#account_payee").val(target);
							if(key * 1 == 8){
								$("#payer_email").val(value);
							}
						}
					}
				 });
			  	$(".chzn-search input").width(110);
			} 
		}
	})	
 }
 
 function createMemberSelect(data){
		var selected = ' <select data-placeholder="收款人" class="chzn-select" style="width:120px;" id="member">';
		var options = "<option value='-1'>选择收款人</option><option value='-2'>手工输入</option>";
		for(var index = 0 , count = data.length ; index < count ; index++ ){
			if(data[index]["is_tip"] * 1 == 0)
			{
				options += "<option target='"+data[index]["id"]+"' value='"+data[index]["account"]+"'>"+data[index]["account_name"]+"</option>";
			}else{
				var  o = new Object();
				o = {
					key_type:data[index]["key_type"],
					account_name:data[index]["account_name"],
					account:data[index]["account"]
				};
				tip.push(o)
			}
		}
		options += "</select>";
		selected += options;
		showTipInSpan();
		return selected;

	 }
 	function showTipInSpan(){
		  var key_type = ($("#sel").val());
		  var tipAccountName = "";
		  var tipAcccount = "";
		  var payer_info_tip = "";
		  for(var index = 0 , count = tip.length ; index < count ; index++ ){
		   if(key_type  === tip[index]["key_type"]){
		    tipAccountName = "*"+tip[index]["account_name"];
		    tipAcccount = "*"+tip[index]["account"];
		    payer_info_tip = "*" + tip[index]["payer_info_tip"];
		    break;
		   }  
		  }
		  $("#account_name").html(tipAccountName);
		  $("#account").html(tipAcccount);
		  //$("#payer_email").val(payer_info_tip);
	  
	 }
 	function inputIn(_this,value){
		var node = $(_this);
		if(node.val() === value){
			node.css("color","black").val("");
		}
	 }
	 function outInput(_this,value){
		var node = $(_this);
		if($.trim(node.val()).length < 1) {
			node.val( value).css("color","silver");
		}
			
	 }
 
 //验证质保付款账号
 function validatePayerEmail()
 {
 	var payer_email = $("#payer_email").val();
 	if($.trim(payer_email).length<1&&$("#key_type").val()===<%=PaymentMethodKey.Warranty%>)
 	{
 		showMessage("免费质保需填写我们的PayerEmail","alert");
		return false;
 	}
 	
 	return true;
 }
 // 验证收款人信息
 function validateAccountInfo(){
	 var account_payee = $("#account_payee").val() ;
	 var account = $("#account");
	 
	 var account_name = $("#account_name");
 
	 if( $.trim(account_payee).length < 1 || account_payee * 1 == -1){
		 showMessage("请选择收款人","alert");
		 return false;
	 }else if(( account_payee * 1 == -2 && '<%= billId%>' * 1 == 0 )&& ( $.trim($("input",account).val().length) < 1||$.trim($("input",account_name).val().length) < 1 )){
		 showMessage("请选择收款人","alert");
		 return false;
	 } 
	 // 如果是Paypal 没有输入Payer_email应该提示
 
	 if(key * 1 == '<%= PaymentMethodKey.PP%>'){
		 var payerEmail = $("#payer_email");
		 if(payerEmail.length == 1 && payerEmail.val().length < 1){
				showMessage("请输入客户Paypal Email 地址","alert");
				return false ;
		}
		 
		 
	 }
	 return true;
}
 // 验证地址信息
 function validateAddressInfo(){
	 var  ccidNode = $("#ccid_hidden");
	 var  pro_idNode = $("#pro_id");
	 var ccid =  ccidNode.val(); 
	 var pro_id = pro_idNode.val();
	 
	 var country_name = $("option:selected",ccidNode).html();
	 if(ccid * 1 == 0 || pro_id * 1 == 0 ||  (pro_id * 1 == -1 && $.trim($("#address_state_input").val()).length < 1)){
		   
		  showMessage("请先选择国家和省份","alert");
		
		  return false;
	 }else{
 
		 return true;
	 }
 }
 // 在这里判断是不是 有些东西都输入了 比如地址信息。收款信息
 function getHtml(_this,target){
	var accountFlag = validateAccountInfo(); 
	var addressFlag = validateAddressInfo();
	
	if(target === "shipping")
	{
		$("#tabs").tabs( "select" , 1 );
 
	}
	else if(target === "products")
	{
		if(accountFlag && addressFlag)
		{
			$("#tabs").tabs( "select" , 1 );
		}
		else
		{
			$("#tabs").tabs( "select" , 0 );
		}
	}
	else if(target === "ebay")
	{
		if(accountFlag && addressFlag){
		 
			$("#tabs").tabs( "select" , 1 );
			 
		}else{
			 
			$("#tabs").tabs( "select" , 0 );
		}
	}
	else if(target === "warranty")
	{
		var payerFlag = validatePayerEmail();
		if(accountFlag&&addressFlag&&payerFlag){
		 
			$("#tabs").tabs( "select" , 1 );
			 
		}else{
			 
			$("#tabs").tabs( "select" , 0 );
		}
	}


		
	// /administrator/order/quote_record_order.html
	if(target === "products" || target === "shipping"||target==="warranty")
	{
		gotoProducts(target);
	}
	else
	{
		$(".cssDivschedule").css("display","block");
		var data = {bill_id:'<%= billRow.get("bill_id",0l)%>'}
		 var uri =  '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/' + target + ".html";
		 $.ajax({
			url:uri,
			dataType:'html',
		 	type:'post',
		 	data:jQuery.param(data),
			success:function(data){
			 $(".cssDivschedule").fadeOut();
			 	$("#shippingInfo").html(data);
			 	 typeLiselected(target);
			}
		 })	
	}
 }
 // 让TypeLi 中的一个选中;
 function typeLiselected(target){
	 
		if(target === "shipping")
		{
			isShipping = true ;
		}
		else
		{
			isShipping = false;
		}
	 	$(".type li").removeClass("on");
		$(".type li[target='"+target+"']").addClass("on");
 }
 function gotoProducts(ur){
	 var  ccidNode = $("#ccid_hidden");
	 var  pro_idNode = $("#pro_id");
	 
	 ccid =  ccidNode.val(); 
	 pro_id = pro_idNode.val();
	 country_name = $("option:selected",ccidNode).html();
	  
	 address_state_name =  (pro_id * 1 == -1?$("#address_state_input").val(): $("option:selected",pro_idNode).html());
	 var uri =  ur+".html";
	 var da = {ccid:ccid,pro_id:pro_id,country_name:country_name,address_state_name:address_state_name}	;
	 // update 时候添加的Ps_id 和 bill_id
	  
	 if('<%= billId%>' * 1 != 0){
		 
			da["bill_id"] = '<%= billId%>';
			da["ps_id"] = '<%= billRow.get("ps_id",0)%>'
	 }
	 $(".cssDivschedule").css("display","block");
	 uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/' + uri;
	 //uri = '../../paypal_html/'+uri;
	 $.ajax({
			url:uri,
			dataType:'html',
			data: jQuery.param(da),
		 	type:'post',
			success:function(data){
			 $(".cssDivschedule").fadeOut();
			 	$("#shippingInfo").html(data);
			 	typeLiselected(ur);
			}
		})	
	 
 }
 function setPro_id(fillFlag,val,html){
	removeAfterProIdInput();
	var node = $("#ccid_hidden");
	var value = node.val();
 
	if(value*1 != 10929){
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:value},
				function callback(data){ 
					$("#pro_id").attr("disabled",false);
					$("#pro_id").clearAll();
					$("#pro_id").addOption("请选择......","0");
					if (data!=""){
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					if (value>0){
						$("#pro_id").addOption("手工输入","-1");
					}
					$("#addBillProSpan").html($("#pro_id"));
					// 如果是这个下面有 对应的那么就选中 如果是么有的那么就是 和点击手工输入一样的操作
					if(val){
						if(val === "-1"){
							$("option[value='"+val+"']",$("#pro_id")).attr("selected",true);
						 
							$("#addBillProSpan").append($("<input type='text' id='address_state_input' value='"+html+"' />"));
						 
						}else{
							$("option[value='"+val+"']",$("#pro_id")).attr("selected",true);
							removeAfterProIdInput();
						}
					}
				}
				
			);
		if(!fillFlag){
			if(value * 1 != 0 ){
				// 得到Option
				var option = $("option:selected",node);
				$("#address_country_code").val(option.attr("code"));
			}else{
				$("#address_country_code").val("");
			}
		}
	 }
 }
 
 function warrantySelectPro(pro_id)
 {
 	$("#pro_id").val(pro_id)
 }
 
 
 function fixAddress(){
	 var node = $("#pro_id");
	 var value = node.val();
 
	 if(value*1 == -1){
		 addInputAfterProid(node);
	 }else{
		 removeAfterProIdInput();
	}
 }
 function addInputAfterProid(node){
	 var input  = "<input type='text'   id='address_state_input' />";
	 $(input).insertAfter(node);
 }
 function removeAfterProIdInput(){
	
	$("#address_state").remove();
 }
 function addDiv(){
	 $(".cssDivschedule").css("display","block");
 }
 function clearDiv(){
	 $(".cssDivschedule").fadeOut("slow");
}
 function getAddressInfoList(){
 
	 var client_id = $("#client_id").val();
	 if(client_id.length < 1){
		 
		 return ;
	}
	$(".cssDivschedule").css("display","block");
	$.ajax({
		url:'<%= getAddressInfoList%>',
		dataType:'json',
		data:jQuery.param({client_id:client_id}),
		success:function(data){
			$(".cssDivschedule").fadeOut();
			if(data && data.length > 0){
				addAddressInfoList(data);
			} else{
				var ul = $("ul.addressList");
				$("li",ul).remove();
			}
		}
	})	
		
 }
 function addAddressInfoList(data){
	 
	var ul = $("ul.addressList");
	$("li",ul).remove();
	ul.slideUp();
	var lis = "";
	for(var index = 0 , count = data.length ; index < count ; index++ ){
		 
		  lis += "<li><input type='radio' name='radio_address' ";
		  lis += "address_country='"+data[index]["address_country"]+"'";
		  lis += "address_city='" + data[index]["address_city"]+"'";
		  lis += "address_zip='" + data[index]["address_zip"]+"'";
		  lis += "address_state='"+ data[index]["address_state"] +"'";
		  lis += "address_street='"+data[index]["address_street"]+"' address_name='"+data[index]["address_name"]+"' code='"+data[index]["address_country_code"]+"' onclick='fillValue(this)'/>&nbsp; ";
		  lis += data[index]["address_name"]+": "+data[index]["address_street"]+ ","+data[index]["address_city"]+", "+ data[index]["address_state"]+","+ data[index]["address_country"]+"</li>";
	}
	
	ul.append($(lis));
	ul.slideDown();
		
 }
 function validateEmailAddress(){
 }
 function fillValue(_this){
	var node = $(_this);
	var table = $(".addressInfo");
 	$("input[name='address_name']").val(node.attr("address_name"));
	$("input[name='address_street']").val(node.attr("address_street"));
	$("input[name='address_city']").val(node.attr("address_city"));
	$("input[name='address_country_code']").val(node.attr("code"));
	$("input[name='address_zip']").val(node.attr("address_zip"));
	 // 初始化country 
	 var code = node.attr("code");
	 var selected = $("#ccid_hidden");
	 $("option[code='"+$.trim(code).toUpperCase()+"']",selected).attr("selected",true);
	 setPro_id(true);
 	
 }
 function changeTab(_this,target){
	 // 让第二个Tab选中
	 	 typeLiselected(target);
		 getHtml(_this,target);
 }
 function pasteEmail(){
		var a = $("#pasteEmail_a");
		if(a.attr("_target") === "open"){
			$("#textareaEmail").css("display","block");
			a.attr("_target","close");
			$("#paste_email").html("隐藏");
		}else{
			$("#textareaEmail").css("display","none");
			a.attr("_target","open");
			$("#paste_email").html("粘贴邮件内容");
		}
	}
	function fixSaveValueInt(){
			var subtotal = parseFloat($.trim($("#subtotal").html()));
	 	 	var totalDiscount = parseFloat($.trim($("#total_discount").val()));
	 	 	var shippingFee = parseFloat($.trim($("#shipping_fee").val()));
	 		var t = subtotal  + shippingFee;
	 		var intValue = parseInt(t+"");
	 		$("#total_discount").val(totalDiscount+t-intValue);
	 		$("#save").val(intValue);
 	}
 	function setPayerEmailByHand(){
 	 	if(key * 1 == <%= PaymentMethodKey.PP%>){
			var client_id = $("#client_id");
			var payer_email = $("#payer_email");
			payer_email.val(client_id.val());
 	 	}
 	}
 
</script>



</head>
<body>
<!-- 遮盖层 -->
  <div class="cssDivschedule">
 		<div class="innerDiv">
 			请稍等.......
 		</div>
 </div>
 <form action="" id="baseInfo"> 
 <input type="hidden" id="sid" name="sid" value="<%=sid%>"/>
 <input type="hidden" id="oid" name="oid" value="<%=oid %>"/>
 <input type="hidden" id="account_payee" value="" />
 <input type="hidden" name="key_type" id="key_type" value="<%= billRow.get("key_type",0) %>" />
 <input type="hidden" id="invoice_state" name="invoice_state" value='<%= InvoiceStateKey.NewAdd %>'/>
 <div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
		 <ul>
			 <li><a href="#bill">账单模板</a></li>
			 <li><a href="#shopping">商品信息</a></li>	 
			 <li><a href="#yulan">预览</a></li>	
		 </ul>
		 <div id="bill" style="height:450px;overflow:auto;text-align:left;width:98%;">
			 <div style="width:40%;float:left;">
			 	<div style="">
				 	<fieldset class="set" style="border:2px solid #993300;padding:10px;">
				  		<legend>收款信息</legend>
				 		<img src="../imgs/visionari.gif" />
					 	<h3 style="color: #333333;margin-top: 10px;">Visionari LLC</h3>
					 	<p><span id="employe_name" style="font-weight:bold;"><%= (userInfo != null?userInfo.getString("employe_name"):"")%></span><span style="margin-left:10px;" id="create_time"><%=DateUtil.NowStr() %></span></p>
						 <p>
						    	<div style="float:left;">
								    	<select data-placeholder="收款款方式" class="chzn-select" style="width:120px;" id="sel">
											<option value="-1">选择收款方式</option>
											<%
												PaymentMethodKey key = new PaymentMethodKey();
												ArrayList<String> payMentList=  key.getStatus();
												for(String name : payMentList){
											%>
													<option value="<%=name %>"><%=key.getStatusById(Integer.parseInt(name)) %></option>
											<% 	 
												}
											%>
										</select>
								</div>
									<div style="float:left;" id="memberDiv">
									 
									</div>
						    </p>
					    <p style="clear:both;">&nbsp;&nbsp;&nbsp;收款人:<span id="account_name"></span></p>
					    <p style="margin-top: 5px;">收款账户:<span id="account"></span></p>
					</fieldset>
			 	</div>
			 	<div style="text-align:left;">
			 		 <span style="font-weight:bold;color:#f60;">Note to recipient</span><br/>
			 		 <textarea id="note" style="height:60px;width:95%;margin-left:5px;" name="note">
<%if(billId !=0l){ 
out.println(billRow.getString("note"));
 } %>
			 		 </textarea>
			 		<p>Characters: 1000</p>
			 	</div>	
			</div>
		 	<div style="width:57%;float:left;">
		 		<fieldset class="set" style="border:2px solid #993300;padding:5px;">
			  		<legend>地址信息</legend>
			 		<div>
 			 			 <!--  client_id,address_name,address_street,address_city,address_zip -->
						<table class="addressInfo">
							<tr>
								<td class="right">Client ID :</td>
								<td>&nbsp;<input type="text" value="<%=orderDeliveryInfo.getString("client_id")%>" id="client_id"  onkeyup="setPayerEmailByHand();" style="width:200px;" name="client_id" onblur="getAddressInfoList()"/></td>
								 
							</tr>
							
							<tr id="warranty_payer" style="">
								<td class="right">Payer Email :</td>
								<td>&nbsp;<input type="text" value="<%=billRow.getString("payer_email")%>" id="payer_email"  style="width:200px;" name="payer_email" /></td>
								 
							</tr>
							<tr>
								<td class="right">Name :</td>
								<td>&nbsp;<input type="text" name="address_name" value="<%=orderDeliveryInfo.getString("address_name")%>"/></td>
								
							</tr>
							 <tr>
								<td class="right">Street :</td>
								<td>&nbsp;<input type="text" name="address_street" value="<%=orderDeliveryInfo.getString("address_street")%>"/></td>
							</tr>
							 <tr>
								<td class="right">City :</td>
								<td>&nbsp;<input type="text" name="address_city" value="<%=orderDeliveryInfo.getString("address_city")%>"/></td>
							</tr>
							<tr>
								<td class="right">Zip :</td>
								<td>&nbsp;<input type="text" name="address_zip" value="<%=orderDeliveryInfo.getString("address_zip")%>"/></td>
							</tr>
							 <tr>
								<td class="right">Country :</td>
								<td>&nbsp;
									 
						<!-- 国家省份信息 -->
							<%
								DBRow countrycode[] = orderMgr.getAllCountryCode();
								String selectBg="#ffffff";
								String preLetter="";
							%>
					      	 <select  id="ccid_hidden"   onChange="removeAfterProIdInput();setPro_id();">
						 	 	<option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++){
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
										if (selectBg.equals("#eeeeee")){
											selectBg = "#ffffff";
										}else{
											selectBg = "#eeeeee";
										}
									}  	
									preLetter = countrycode[i].getString("c_country").substring(0,1);
								  %>
						  		  <option <%=orderDeliveryInfo.getString("ccid").equals(countrycode[i].getString("ccid"))?"selected=\"selected\"":""%> style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								  <%
								  }
								%>
			     			 </select>
							</td>
								
							</tr>
							 <tr>
								<td class="right">State :</td>
								<td> 
									&nbsp;
									<span id="addBillProSpan">
										<select  disabled id="pro_id" onchange="fixAddress()" style="margin-right:5px;"></select>
									</span>
									
									 <script type="text/javascript">
					     			 	setPro_id();
					     			 	warrantySelectPro(<%=orderDeliveryInfo.get("pro_id",0l)%>);//质保账单时进入页面默认选中省份
					     			 </script>
								</td>
								 
							</tr>
							 
							 <tr>
								<td class="right">Tel :</td>
								<td><input type="text"  id="tel" name="tel" value="<%=orderDeliveryInfo.getString("tel")%>"/></td>
							</tr>
							<input type="hidden" id="address_country_code" name="address_country_code" value="<%=orderDeliveryInfo.getString("address_country_code")%>"/>
						</table>
						
						<p>
							<ul class="addressList">
 							</ul>
						</p>
			 			
			 		</div>
			 	 
				</fieldset>
		 	</div>
		 	<div style="clear:both;">
		 		<span><b>Memo </b>(your recipient won't see this)</span>  
		 		<textarea name="memo_note" id="memo_note" style="width:95%;margin-left:5px;height:35px;">
<%if(billId !=0l){ 
out.println(billRow.getString("memo_note"));
 } %>		 			
		 		</textarea>
		 		<p>Characters: 150</p>
		 	</div>
		 	 
		 	 <ul class="type" style="margin-top:10px;text-align:center;">
					  		<li class="ui-corner-all" id="li_ebay" target="ebay"  onclick="changeTab(this,'ebay')">Ebay</li>
					  		<li class="ui-corner-all" id="li_products" target="products"  onclick="changeTab(this,'products')">Products</li>
					  		<li class="ui-corner-all" id="li_shipping" target="shipping" onclick="changeTab(this,'shipping')">Shipping</li>
					  		<li class="ui-corner-all" id="li_warranty" target="warranty" onclick="changeTab(this,'warranty')">Warranty</li>
			 </ul>
			 
		 </div>
		 </form>
		 <div id="shopping" style="height:450px;overflow:auto;">
		 	 
			  	<ul class="type">
			  		<li class="ui-corner-all" id="li_ebay" target="ebay" onclick="getHtml(this,'ebay')">Ebay</li>
			  		<li class="ui-corner-all" id="li_products" target="products" onclick="getHtml(this,'products')">Products</li>
			  		<li class="ui-corner-all" id="li_shipping_" target="shipping" onclick="getHtml(this,'shipping')">Shipping</li>
			  		<li class="ui-corner-all" id="li_warranty" target="warranty" onclick="getHtml(this,'warranty')">Warranty</li>
			  	</ul>
			   
		 		<div style="margin-top:2px;" id="shippingInfo">
		 			  
		 		</div>
		 		 
				
		 </div>
		
		 <div id="yulan" style="height:450px;overflow:auto;padding:10px;">
		  
			 <div style="width:95%;border:1px solid silver;margin:0px auto;padding:20px;">
			  		 <div style="width:40%;float:left;border:0px solid green;">
			  		 	<img src="../imgs/visionari.gif" />
					 	<h3 style="color: #333333;margin-top: 10px;">Visionari LLC</h3>
					 	<p><span id="employe_name_" style="font-weight:bold;"><%= (userInfo != null?userInfo.getString("employe_name"):"")%></span><span style="margin-left:10px;" id="create_time_"><%=DateUtil.NowStr() %></span></p>
					    <p style="" id="account_name_"></p>
					    <p style="margin-top: 10px;" id="account_"></p>
					   
			  		 </div>
			  		 <div style="width:55%;float:right;border:0px solid red;margin-bottom:30px;">
			  			<h1 style="color: #CCCCCC;text-transform:uppercase;font-size:16px;text-align:right;margin-right:20px;margin-top:10px;">Invoice</h1>
			  		 	<table id="invoiceDetails" class="basic" summary="Invoice details" style="float:right;margin-top:20px; ">
							<tbody>
								<tr>
									<th>Invoice number</th>
									 
									<td><%= billId !=0l ? billId+"" : "XXXXXXXXXXXXXXXX" %>&nbsp;</td>
								</tr>
								<tr>
									<th>Invoice date</th>
									<td><%=DateUtil.NowStr() %>&nbsp;</td>
								</tr>
								<tr>
									<th>Payment terms</th>
									<td>Due on receipt</td>
								</tr>
								
								<tr>
									<th>Due date</th>
									<td><%=DateUtil.NowStr() %>&nbsp;</td>
								</tr>
							</tbody>
						</table>
			  		 </div>
			  		 <div style="clear:both;border:0px solid silver;margin-top:20px; " id="sendto">
			  		  
			  		 	<p class="ui-corner-all" style="width:90%;background:#e8f1fa;padding:5px;text-indent:10px;line-height:35px;height:35px;">
			  		 		 <span style="font-weight:bold;font-size:14px;">Send To : </span> <span id="addressInfoStr"></span>
			  		 	</p>
			  		 </div>
			  		 <div style="border:0px solid silver;margin-top:20px;">
			  		 	 <table class="itemTable">
			  		 			<thead>
			  		 				<tr>
			  		 					<th width="60%">Description</th>
			  		 					<th width="16%">Quantity</th>
			  		 					<th width="16%">Unit price</th>
			  		 					<th>Amount</th>
			  		 				</tr>
			  		 			</thead>
			  		 			<tbody id="tbodyView">
			  		 	  		<!--  <tr>
			  		 					<td class="name">
 											KIT/SLIM/9005/3K ▲ 
 												<ul class="itemDetail">
 													<li>├ BOX/KIT/SLIM x 1.0 SET</li>
 													<li>├ BOX/KIT/SLIM x 1.0 SET</li>
 													<li>├ BOX/KIT/SLIM x 1.0 SET</li>
 													
 													<li>├ BOX/KIT/SLIM x 1.0 SET</li>
 												</ul>
			  		 					</td>
			  		 					<td class="t">5</td>
			  		 				 	<td class="t">444</td>
			  		 				 	<td class="t">22222</td>
			  		 				</tr>
			  		 				<tr style="background:#e6f3c5;">
			  		 					<td class="name">kit/jsdf/888/3s</td>
			  		 					<td class="t">5</td>
			  		 				 	<td class="t">444</td>
			  		 				 	<td class="t">22222</td>
			  		 				</tr>
			  		 		 -->
			  		 			</tbody>
			  		 			<tfoot>
			  		 				<tr>
			  		 					<td></td>
			  		 					<td colspan="3" style="border-left:1px solid silver;">
			  		 						 <table style="width:100%;border-collapse:collapse;">
			  		 						 	<tr style="background:#e8f1fa">
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Rate Type</td>
			  		 						 		<td id="rate_type_td">RMB</td>
			  		 						 	</tr>
			  		 						 	<tr >
			  		 						 		<td style="width:50%;font-weight:bold;text-align:left;text-indent:20px;" id="">Subtotal</td>
			  		 						 		<td id="subtotal_td">$0.00</td>
			  		 						 	</tr>
			  		 						 	<tr style="background:#e8f1fa">
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >TotalDiscount</td>
			  		 						 		<td id="total_discount_td">$0.00</td>
			  		 						 	</tr>
			  		 							<tr >
			  		 						 		<td id="" style="width:50%;font-weight:bold;text-align:left;text-indent:20px;" >Shipping Fee</td>
			  		 						 		<td id="shipping_fee_td">$0.00</td>
			  		 						 	</tr>
			  		 						 	<tr style="background:#e8f1fa">
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Save</td>
			  		 						 		<td id="save_td">$0.00</td>
			  		 						 	</tr>
			  		 						 </table>
			  		 					</td>
			  		 					
			  		 				</tr>
			  		 			</tfoot>
			  		 	 </table>
			  		 </div>
			 </div> 
			 <div style="margin-top:10px;text-align:center; background: none repeat scroll 0 0 #F8F8F8;border-top: 1px solid #CCCCCC;line-height:50px;height:50px;">
			   	<input type="button" value="保存" onclick="save()" class="normal-green-long" style="cursor:pointer;margin-top:10px;" />
			 </div>
			 		
		 </div>
		  
 </div>
 	<script type="text/javascript">
 	// 点击第二步的时候declare
 	function gotoSecendValidate(_this){
 		 var  ccidNode = $("#ccid_hidden");
		 var  pro_idNode = $("#pro_id");
		 var ccid =  ccidNode.val(); 
		 var pro_id = pro_idNode.val();
		 
		 var country_name = $("option:selected",ccidNode).html();
		 if(ccid * 1 == 0 || pro_id * 1 == 0 ||  (pro_id * 1 == -1 && $.trim($("#address_state_input").val()).length < 1)){
			   
			  showMessage("请先选择国家和省份","alert");
			  $("#tabs").tabs('select', 0);
			  return false;
		 }
		 // 收款信息的检验
		 var account_payee = $("#account_payee").val() ;
		 var account = $("#account");
		 
		 var account_name = $("#account_name");
		  
		 if( $.trim(account_payee).length < 1 || account_payee * 1 == -1){
			 return gotoTabIndex1();
		 }else if((account_payee * 1 == -2 && '<%= billId%>' == 0) && ($.trim($("input",account).val().length) < 1||$.trim($("input",account_name).val().length) < 1 )){
			 return gotoTabIndex1();
		 } 
		 return true;
 	}
 	function gotoTabIndex1(){
 		showMessage("请先选择收款人信息","alert");
	 	$("#tabs").tabs('select', 0);
	 	return false;
 	}
 	 
 	$("#tabs").tabs({
 		select: function(event, ui) {
			if( ui.index  * 1 == 1){
				 // 如果直接点击第二个标签。那么就让Shipping选中
				/* 
				if(isShipping){
					getFirstFormValue();
					return true;	
				}
			 	var flag =  gotoSecendValidate();
			 
			  	if(!flag){
				  	return flag;
				}else{	 
					 if($(".type li.on").length < 1){
						 getHtml($("#li_shipping_"),"shipping");
					 }
					 getFirstFormValue();		
				}
				*/
				//alert(fristForm);
				// 要是是update的情况那么就是没有Li 。然后点击"商品信息的时候就去加载"
				 
				
				
			};
			if(ui.index  * 1 == 2){
				// 先判断第二个。然后在判断第三个
				var flag =  gotoSecendValidate();
			  	if(!flag){
				  	return flag;
				 }else{
				// 读取各个List 中的值
			 
				 	if($("#bill_type").val() === "1"){
					 	//表示的是保存Ebay中的数据
				 		var ebayForm = $("#ebayForm");
				 		if($("#tbody tr",ebayForm).length > 0){
							//读取ebayForm 中的值,添加EbayForm中的值到预览的界面
							var array = readEbayitemForm();
					 
							addItemsInInvoice(array);
							var object = addEbayTotalInInvoice();
							addTotalInInvoice(object);
							addSendToInInvoice();
							addAccountInInvoice();
					 		
					 	}else{
					 		return letThirdSelected();
						};
					}else{
						 
						if($("#itemsForm").length  > 0){
							var array = readItemForm();
					 		
							var object = readTotal();
							if(array && array.length > 0 ){
								addItemsInInvoice(array);
								addTotalInInvoice(object);
								addSendToInInvoice();
								addAccountInInvoice();
							}else{
								 
								var weight = $("#weight");
								if(weight && parseFloat(weight.val()) > 0 && $(".feeon").length > 0 ){
									// 计算运费的
									addSendToInInvoice();
									addAccountInInvoice();
									addNoItemsInInvoice();
									addTotalByShippingInInvoice();
								}else{
									 
									return letThirdSelected();
								}
							}
						}else{
							return letThirdSelected();
						}
					}
				}
			}
 		}
 	});

 	//得到 第一个页面的数据
 	function getFirstFormValue(){
 		var form = $("#baseInfo");
		var address_country_node = $("#ccid_hidden");
		var address_country = $("option:selected",address_country_node).html();
		var address_state_node = $("#pro_id");
		var address_state  ;
		if(address_state_node.val() * 1 == -1)
		{
			address_state = $("#address_state_input").val();
		}
		else
		{
			address_state = $("option:selected",address_state_node).html();
		}
		var fixObject = 
		{
				address_state:address_state,
				address_country:address_country
		};
		fristForm = (form.serialize()+"&"+jQuery.param(fixObject));
		 
 	}
 	function addTotalByShippingInInvoice(){
 	 	//构造出Object 然后调用addTotalInInvoice 方法
 	  	var object = new Object();
 
 
	 
 
		var  shipping_fee = $(".feeon .b_fee").html();
		 
		var save = $("#save").html();
		var rate = $("#rate").val();
	 
		 
		var rate_type = $("option:selected",$("#rate")).attr("target");
		object["subtotal"] = 0.0;
		object["total_discount"] = 0.0;
		object["shipping_fee"] = shipping_fee;
		object["save"] = shipping_fee;
		object["rate"] = rate;
		object["rate_type"] = rate_type; 
		object["total_weight"] = $("#weight").val();
		 
		addTotalInInvoice(object);
		return object;
 	}
 	// 添加没有商品的时候的table
 	function addNoItemsInInvoice(){
 		var tbody = $("#tbodyView"); 
 	 	$("tr",tbody).remove();
 	 	tbody.append("<tr><td style='height:80px;background:#e6f3c5;text-align:center;' colspan='4'>无商品</td></tr>");
 	}
 	function addEbayTotalInInvoice(){
 		var object = new Object();
 	  
		var subtotal = $("#subtotal");
		subtotal = subtotal ? subtotal.val():"0.0";
		var discount = $("#discount").val();
		var  shipping_fee = $("#shipping_fee").val();
		var total = $("#total").val();
		var rate = $("#rate").val();
	 	var total_quantity = $("#total_quantity").val();
	 
		var rate_type = $("option:selected",$("#rate")).attr("target");
		object["subtotal"] = subtotal;
		object["total_discount"] = discount;
		object["shipping_fee"] = shipping_fee;
		object["save"] = total;
		object["rate"] = rate;
		object["rate_type"] = rate_type; 
		object["total_quantity"] = total_quantity;
	 
		
		return object;
 	}
 	function readEbayitemForm(){
 	 	var array = [];
 	 	var trs =$("#tbody tr",ebayForm);
 	  
 	 	var index = 1;
 	 	trs.each(function(){
			var object = new Object();
			// actual_price,quantity,amount,item_number,name
			var _this = $(this);
			object["item_number_"+index] =  getValueByClassNameAndTr(".item_number",_this);
			object["name_"+index] =  getValueByClassNameAndTr(".name",_this);
			object["actual_price_"+index] =  getValueByClassNameAndTr(".countPrice",_this);
			object["quantity_"+index] =  getValueByClassNameAndTr(".countQuantity",_this);
			object["amount_"+index] =  getValueByClassNameAndTr(".amount",_this);
		 
			array.push(object);
			index++ ;
 	 	}) 
 	 	return array;
 	}
 	function getValueByClassNameAndTr(className,node){

		return $(className,node).val();
 	 	
 	}
 
 	function letThirdSelected(){
 		showMessage("请先添加商品或者计算运费","alert");
		$("#tabs").tabs('select', 1);
		return false;
 	}
 	//sc_id快递
 	function getCcidPsIdProId(){
		var object = new Object();
		var ccid = $("#ccid").val();
		var pro_id = $("#pro_id_").val();
		var ps_id = $("#ps_id").val();
		var sc_id = $(".feeon").attr("scid");
		var bill_type = $("#bill_type").val();
		var quantity = $("#totalQuantity").val();
		var weight = $("#total_weight").val();
		var account_payee = $("#account_payee").val();
		var weight_typeNode = $("#weight-type");
		var weight_type = "Kg";
		//重新计算weight 根据weight-type
		 
		if(weight_typeNode.val() != "1"){
			 weight = parseFloat(weight_typeNode.val()) * parseFloat(weight)
			 weight_type = $("option:selected",weight_typeNode).html();
		}
		// count 和 count_name
		var account = $("#account_").html();
		var account_name = $("#account_name_").html();
		var key_type = $("#key_type").val();
		
		object = {
				ccid:ccid,
				ps_id:ps_id,
				pro_id:pro_id,
				sc_id:sc_id,
				bill_type:bill_type,
				total_quantity:quantity,
				total_weight:weight,
				account_payee:account_payee,
				weight_type:weight_type,
				account:account,
				account_name:account_name,
				key_type:key_type
		}
		 
		return object;
 	}
 	function ebayAccountInfo(){
 	// count 和 count_name
 		var object = new Object();
		var account = $("#account_").html();
		var account_name = $("#account_name_").html();
		var key_type = $("#key_type").val();
		var ccid = $("#ccid_hidden").val();
		var pro_id = $("#pro_id").val();
		object = {
				account:account,
				account_name:account_name,
				key_type:key_type,
				ccid:ccid,
				pro_id:pro_id
		}

	 
		return object;
 	}
 	function updateBill(){

 	}
 	// 保存
 	function save(){
 		getFirstFormValue();
	 
			var bill_type = $("#bill_type").val();
			 if(bill_type * 1 <= 1)
			 {
				 saveEbay();
			}
			else
			{
				//如果只是运费的应该调用另外的方法
				var weight = $("#weight");
				if(weight.length > 0 && !weight.attr("disabled"))
				{
					saveBillByWeight();
				}
				else
				{
					saveValues();
				}
			}
	 
 	}
 	function saveBillByWeight(){
 	 	var object = addTotalByShippingInInvoice();
		var str = fristForm + "&" + jQuery.param(object)+ "&" +jQuery.param(getCcidPsIdProId());
		 if('<%= billId%>' * 1 != 0){
			 str +=( "&bill_id=" + '<%= billId%>');
		 }
		 
 		ajaxSaveBill('<%= addBillByWeightAction%>',str);
 	}
 
 	function saveEbay(){
 	 	var object =  addEbayTotalInInvoice();
		var str = fristForm + "&" +  jQuery.param(object) + "&" + $("#ebayForm").serialize() + "&" +jQuery.param(ebayAccountInfo());
 		if('<%= billId%>' * 1 != 0){
			str += "&bill_id="+ '<%= billId%>';
 	 	 }
	  
		ajaxSaveBill('<%= addEbayBillAction%>',str);
	 
 	}
 	function saveValues(){
 		var object = readTotal();
		var str = fristForm + "&" +$("#itemsForm").serialize() + "&"+jQuery.param(object) +"&" + jQuery.param(getCcidPsIdProId());
	 	if('<%= billId%>' * 1 != 0){
		 	str += "&bill_id=" + '<%= billId%>';
		}
		//alert(fristForm);
	 	//alert($("#itemsForm").serialize());
 		//alert(jQuery.param(object) );
 		 
   		//return ;
 		ajaxSaveBill('<%= addBillAction%>',str);
		 
 	}
 	function ajaxSaveBill(uri,str){
 		$.ajax({
			url:uri,
			dataType:'json',
			data:str,	
			beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			error: function(){
				$.unblockUI();
			},
			success:function(data)
			{
				if(data.result=="success")
				{
					if(data.oid==0)
					{
						 
						showMessage("添加成功","alert");
						$.artDialog && $.artDialog.close();
						$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();	
					}
					else
					{
						window.location.href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/new_record_order.html?oid='+data.oid;
					}
				}
				else
				{
					showMessage("添加失败","error");
				}
				
			}
		
		})
 	}
 	//读取收款人的信息显示在预览界面
 	function addAccountInInvoice(){
 	 	var account_name = $("#account_name").html();
 	 	var account = $("#account").html();
 	 	var account_payee = $("#account_payee").val();
 	 	if(account_payee === "-2"){
 	 	 	account_name = $("input",$("#account_name")).val();
 	 	 	account = $("input",$("#account")).val();
 	 	}
 	 	$("#account_name_").html(account_name);
		$("#account_").html(account);
 	 	
 	}
 	// 添加SendTo 到Invoice中
 	function addSendToInInvoice(){
		var baseForm = $("#baseInfo");
		//address_name,address_street,address_city,address_zip,state,country
		var str = "";
		str += getInputValueByNameAndForm("address_name",baseForm) + "," ;
		str += getInputValueByNameAndForm("address_street",baseForm) + "," ;
		str += getInputValueByNameAndForm("address_city",baseForm) + "," ;
		
		var address_country_node = $("#ccid_hidden");
		var address_country = $("option:selected",address_country_node).html();
		var address_state_node = $("#pro_id");
		var address_state  ;
		if(address_state_node.val() * 1 == -1){
			address_state = $("#address_state_input").val();
		}else{
			address_state = $("option:selected",address_state_node).html();
		}
		str += address_state + "," + address_country;
		$("#addressInfoStr").html(str); 
	 
 	 	
 	}
 	function addTotalInInvoice(object){
 	 	if(object){
	 	 	for(var i in object){
		 	 	if(i != "rate"){
		 	 		$("#" + i+"_td").html(object[i]);
			 	}
		 	 
	 	 	}
 	 	}
 	}
 	function addItemsInInvoice(array){

 	 	/*
				<tr style="background:#e6f3c5;">
					<td class="name">kit/jsdf/888/3s</td>
					<td class="t">5</td>
				 	<td class="t">444</td>
				 	<td class="t">22222</td>
				</tr>
 	 	*/
 	 	var tbody = $("#tbodyView"); 
 	 	$("tr",tbody).remove();
		for(var index = 0 , count = array.length ; index < count ; index++ ){
			 
			 
			
			var background  = (index%2 == 0)?"":"#e6f3c5";
			var tr  = "<tr style='background:"+background+";'>";
				tr += "<td class='name'>"+array[index]["name_"+(index+1)]+(array[index]["ul"]?"<ul class='itemDetail'>"+array[index]["ul"].html()+"</ul>":"")+"</td>";
				tr += "<td class='t'>"+array[index]["quantity_"+(index+1)]+"</td>";
				tr += "<td class='t'>"+array[index]["actual_price_"+(index+1)]+"</td>";
				tr += "<td class='t'>"+array[index]["amount_"+(index+1)]+"</td></tr>";
		 
				array[index]["ul"]?array[index]["ul"].get(0):"";
				 
				tbody.append($(tr));
		}
		
 	}
 	// 读取 总价 总折扣商品总价格当前汇率 . 运费 等等。
 	function readTotal(){
 		$("#key_type").val($("#sel").val());
 	 	var object = new Object();
 	 	var itemsForm = $("#itemsForm");
		var subtotal = $("#subtotal");
		subtotal = subtotal ? subtotal.html():"0.0";
		var totalDiscount = $("#total_discount").val();
		var  shipping_fee = $("#shipping_fee").val();
		var save = $("#save").val();
		var rate = $("#rate").val();
		var key_type = $("#key_type").val();
	 
		var total_cost = $("#totalCost").html();
		var rate_type = $("option:selected",$("#rate")).attr("target");
		object["subtotal"] = subtotal;
		object["total_discount"] = totalDiscount;
		object["shipping_fee"] = shipping_fee;
		object["save"] = save;
		object["rate"] = rate;
		object["rate_type"] = rate_type; 
		object["total_cost"] = total_cost;
		object["key_type"] = key_type;
		
		return object;
 	 	
 	}
  
 	// 如果是定制的商品那么就是要把定制的数据信息读取出来
	function readItemForm(){
		var nameArray = ['product_type','pc_id','weight','unit_price','unit_name','name','amount','actual_price','quantity'];
		var itemsForm = $("#itemsForm");
		
		var index = 1;
		var items = []; 
		while(getInputValueByNameAndForm('pc_id_'+index,itemsForm)){
			var str ="";
			var object = new Object();
			for(var j = 0 , count = nameArray.length ; j < count ; j++ ){
				var attr = nameArray[j]+"_"+index;
				object[attr] = getInputValueByNameAndForm(nameArray[j]+"_"+index,itemsForm);
			}
			// 读取定制商品信息
			var ul = $("input[name='pc_id_"+index+"']",itemsForm).parent().find("ul");
			 if(ul.length > 0){
				 object["ul"] = ul;
			 };
			items.push(object);

			index++;
		}
		return items;
		
	}
 	function getInputValueByNameAndForm(name,node){
		return  $("input[name='"+name+"']",node).val();
 	}
 	function gotoShow(){
 	 	var shipping_fee = $("#shipping_fee").val();
 	 	 
 	 	if(shipping_fee && (shipping_fee === "0" || shipping_fee.length < 1 || parseFloat(shipping_fee) < 0.1)){
			showMessage("请先计算或者输入运费","alert");
			return ;
 	 	}
 		$("#tabs").tabs('select', 2);
 	}
 	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 	<%
 		if(sid>0||billRow.get("bill_type",0)==BillTypeKey.warranty)
 		{
 	%>
 		$(".type li[target!='warranty']").remove();
 	<%
 		}
 		else
 		{
 	%>
 		$(".type li[target='warranty']").remove();
 	<%
 		}
 	%>


 	jQuery(function($){
 	 	var updateTarget = '<%= updateTarget%>';
 		 if('<%= updateTarget%>'.length > 0 ){
			var node = $("#"+updateTarget);
			node.click();
 	 	}
 	})
 	</script>
 
</body>

</html>