<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
cartReturnProductReason.flush(session);
DBRow cartProducts[] = cartReturnProductReason.getProductReason();
WarrantyTypeKey warrantyTypeKey = new WarrantyTypeKey();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>运单申请服务商品</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script>

</script>

<style>
a.del-cart-product:link {
	color:#FF0000;
	text-decoration: none;
}
a.del-cart-product:visited {
	color: #FF0000;
	text-decoration: none;
}
a.del-cart-product:hover {
	color: #FF0000;
	text-decoration: underline;

}
a.del-cart-product:active {
	color: #FF0000;
	text-decoration: none;

}
</style> 

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="0" cellpadding="5" id="cart_pc_table">
  
<%
if (cartProducts.length==0)
{
%>
      <tr>
        <td colspan="4" align="center" valign="middle" style="color:#999999"><<暂无商品>>
		<script>cartIsEmpty=true;</script>		</td>
      </tr>
<%
} 
else
{
%>
      <tr >
        <td height="30" bgcolor="#eeeeee" style="padding-left:10px;"><strong>商品</strong></td>
        <td bgcolor="#eeeeee" align="center"><strong>申请服务数</strong></td>
        <td align="center" bgcolor="#eeeeee"><strong>质保类型</strong></td>
        <td bgcolor="#eeeeee"><strong>质保原因</strong></td>
		<td bgcolor="#eeeeee" align="left"><strong>订单明细(/套装)ID</strong></td>
        <td align="center" valign="middle" bgcolor="#eeeeee">&nbsp;</td>
      </tr>
	  
<%
String bgColor;
for (int i=0; i<cartProducts.length; i++)
{
	if (i%2==0)
	{
		bgColor = "#FFFFFF";
	}
	else
	{
		bgColor = "#eeeeee";
	}
	
%>

      <tr bgcolor="<%=bgColor%>" name='tr_pc_<%=cartProducts[i].get("cart_pid",0l)%>'>
        <td width="43%" height="25"  style="padding-left:10px;padding-top:5px;padding-bottom:5px;">
			<%=cartProducts[i].getString("p_name")%>
			<input type="hidden" name="pc_id" value="<%=cartProducts[i].get("cart_pid",0l)%>"/>
		</td>
        <td width="12%" align="center">
			<input name="quantity" type="text" id="quantitys" style="width:40px;" value="<%=cartProducts[i].getString("cart_quantity")%>" onChange="return onChangeQuantity(this)" /><%=cartProducts[i].getString("unit_name")%>
		</td>
		<td width="10%" align="center">
			<select name="waranty_type" onchange="onChangeWarrantyType(this)">
				<%
					ArrayList warrantyTypeList = warrantyTypeKey.getWarrantyType();
					for(int j=0;j<warrantyTypeList.size();j++)
					{
				%>
					<option <%=cartProducts[i].getString("warranty_type").equals(warrantyTypeList.get(j))?"selected='selected'":""%> value="<%=warrantyTypeList.get(j)%>"><%=warrantyTypeKey.getWarrantyTypeById(warrantyTypeList.get(j).toString())%></option>
				<%
					}
				%>
			</select>
		</td>
		<td>
			<input maxlength="300" name="return_reason" value="<%=cartProducts[i].getString("return_reason")%>" onchange="onChangeReturnReason(this)" <%=cartProducts[i].getString("return_reason").trim().equals("")?"style='background-color:#FFB5B5'":""%> />
		</td>
		<td width="13%" align="left">
			<%
				String associateProductId = cartProducts[i].getString("order_item_type");
				if(!"".equals(associateProductId) && !"0".equals(associateProductId))
				{
					associateProductId += "_"+cartProducts[i].get("order_item_id",0L);
				}
				else
				{
					associateProductId = "";
				}
				out.println(associateProductId);
			%>
		</td>
        <td width="2%" align="left" valign="middle">
        	<%
        		String value = cartProducts[i].getString("order_item_type")+cartProducts[i].get("order_item_id",0L);
        		String item_pc_unit_info = cartProducts[i].getString("order_item_type")+"_"+cartProducts[i].get("order_item_id",0L)
					 + "_"+cartProducts[i].get("cart_pid",0l)+"_"+cartProducts[i].get("set_or_part_pc_id",0);
        		//如果是套装cart_pid是商品ID，否则是散件ID
	        	if(0 == cartProducts[i].get("set_or_part_pc_id",0))//套装
				{
	        		value += "_"+cartProducts[i].get("cart_pid",0l)+"_"+cartProducts[i].get("cart_total_count",0F)+"_"+cartProducts[i].get("porder_item_id",0L);
				}
	        	else
	        	{
	        		value += "_"+cartProducts[i].get("set_or_part_pc_id",0l)+"_"+cartProducts[i].get("set_pc_count",0F)+"_"+cartProducts[i].get("porder_item_id",0L)
	        			   + "_"+cartProducts[i].get("cart_pid",0l)+"_"+cartProducts[i].get("cart_total_count",0F);
	        	}
        		
        	%>
        	<input type="hidden" name="item_pc_unit_info" value='<%=item_pc_unit_info %>'/>
			<a href="javascript:void(0)" onClick="delOrderOrWaybillProductFromCartList('<%=value%>','<%=cartProducts[i].getString("p_name")%>')" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>
		</td>
      </tr>
<%
} 
%>
      <tr>
        <td height="50" style="border-top:2px #eeeeee solid"><input name="Submit433" type="submit" class="short-button" value="修改">          &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
        
        <input name="Submit432" type="button" class="long-button-yellow" value="清空质保" onClick="cleanCart()">
        <script>cartIsEmpty=false;</script>
        </td>
        <td height="50" style="border-top:2px #eeeeee solid">&nbsp;</td>
        <td colspan="3" align="right" valign="middle" style="border-top:2px #eeeeee solid">总数：<%=cartReturnProductReason.getSumQuantity()%></td>
      </tr>
<%
}

//long ent = System.currentTimeMillis();
//System.out.println("Cart Page time:"+(ent-stt));
%>
</table>

</body>
</html>
