<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.ReturnTypeKey"%>
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<%@page import="com.cwc.app.key.ServiceOrderStatusKey" %>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
long oid			= StringUtil.getLong(request, "oid");
long sid			= StringUtil.getLong(request, "sid");
DBRow serviceRow	= serviceOrderMgrZyj.getServiceOrderDetailBySid(sid);
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>服务单详细</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<style type="text/css">
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
span.valueSpan{width:56%;border:0px solid green;float:left;overflow:hidden;text-align:left;text-indent:4px;}
span.stateName{width:20%;float:left;text-align:right;font-weight:normal;}
</style>
<script type="text/javascript">
function onlineScanner(){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}
//图片在线显示
function showPictrueOnline(fileWithType,fileWithId , currentName){
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("service_virtual")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
}
function viewOrderListByOid(oid)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/ct_order_auto_reflush.html?&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val='+oid;
	window.open(uri);
}
function openBillOrderListByBillId(bill_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/bill_manager.html?search_mode=1&comefrom=tool&searchkey="'+bill_id+'"';;
	window.open(uri);
}
function openWaybillOrderListByWid(wid)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/waybill/waybill_list.html?cmd=waybillId&search_mode=1&oid='+wid;
	window.open(uri);
}
</script>
</head>
<body>
<table style="border-collapse: collapse; width: 100%;" id="serviceTable">
 	<tr>
 		<td>
 			<fieldset class="set" style="border:2px solid #993300;text-align:left">
 				<legend>
					服务单[&nbsp;<%=serviceRow.get("sid",0L) %>&nbsp;]&nbsp;|&nbsp;<img title="创建人" src="../imgs/order_client.gif" />
					<%=null==adminMgrLL.getAdminById(serviceRow.getString("create_user"))?"":adminMgrLL.getAdminById(serviceRow.getString("create_user")).getString("employe_name")%>
					&nbsp;|&nbsp;<%=new ServiceOrderStatusKey().getServiceOrderStatusById(serviceRow.get("status",0)) %>
 				</legend>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
  					<img  title="创建时间" src="../imgs/alarm-clock--arrow.png"/>
  					<%=serviceRow.getString("create_date").substring(5,16)%>
  				</p>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
 				原始订单:<a href="javascript:void(0);" onclick="viewOrderListByOid(<%=serviceRow.getString("oid")%>)"><%=serviceRow.getString("oid")%></a>
 				</p>
 				<%
 					DBRow[] billRows		= serviceOrderMgrZyj.getServiceBillOrdersBySid(serviceRow.get("sid",0L));//账单
 					if(billRows.length > 0)
 					{
 				%>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
 				关联账单:<a href="javascript:void(0);" onclick='openBillOrderListByBillId(<%=billRows[0].get("bill_id",0L) %>)' style="cursor: pointer;"><%=billRows[0].get("bill_id",0L) %></a>
 				</p>
 				<%
 					}
 				%>
 				<%
 					if(0 != serviceRow.get("wid",0L))
 					{
 				%>
 					<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
	 				关联运单:<a href="javascript:void(0);" onclick='openWaybillOrderListByWid(<%=serviceRow.get("wid",0L) %>)' style="cursor: pointer;"><%=serviceRow.get("wid",0L) %></a>
	 				</p>
 				<%		
 					}
 				%>
 				<%
 					if(0 != serviceRow.get("is_need_return",0))
 					{
 				%>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
	 				是否退货:<%=new ReturnTypeKey().getReturnTypeKeyValue(serviceRow.get("is_need_return",0)) %>
	 			</p>		
 				<%
 					}
 				%>
 				<%
 					WarrantyTypeKey warrantyTypeKey = new WarrantyTypeKey();
	 				DBRow[] serviceItems = serviceOrderMgrZyj.getServiceOrderItemsByServiceId(serviceRow.get("sid",0L));
	 				if(serviceItems.length > 0)
	 				{
	 			%>
	 				<div style='border-top: 2px silver dotted;line-height: 20px;padding-top: 2px;padding-bottom: 2px;float: left; width: 100%;'>
	 			<%		
		 				for(int j = 0; j < serviceItems.length; j ++)
		 				{
	 			%>
 						<%=serviceItems[j].getString("p_name") %>&nbsp;x&nbsp;
 						<span style="color:blue">
	 						<%=serviceItems[j].get("quantity",0F) %>&nbsp;
	 						<%=serviceItems[j].getString("unit_name")%>
	 					</span>&nbsp;
	 					<span style="color:blue;font-weight: bold;">
							<%=warrantyTypeKey.getWarrantyTypeById(serviceItems[j].get("warranty_type",0))   %>
						</span><br/>
	 					<span style="color: green;">服务原因:</span><%=serviceItems[j].getString("service_reason") %><br/>
	 			<%		
	 					}
	 			%>
	 				</div>
	 			<%
	 				}
	 			%>
	 		</fieldset>
 		</td>
 		<td valign="middle">
 		<%
 			DBRow[] returnOrders	= returnProductOrderMgrZyj.getReturnOrderBySid(serviceRow.get("sid",0L));//退货单
 			if(returnOrders.length > 0)
 			{
		 		for(int n = 0; n < returnOrders.length; n ++)
				{
		 			if(ReturnTypeKey.NEED == returnOrders[n].get("is_need_return",0))
		 			{
 		%>
		 			<fieldset class="set" style="border:2px solid #993300;text-align:left">
		 				<legend>
		 					退货单[&nbsp;<%=returnOrders[n].get("rp_id", 0L) %>&nbsp;]&nbsp;<strong>|</strong>&nbsp;
		 					<%=new ReturnTypeKey().getReturnTypeKeyValue(returnOrders[n].get("is_need_return",0)) %>
		 					<%
		 						if(1 == returnOrders[n].get("is_send_mail",0))
		 						{
		 					%>
		 						&nbsp;<strong>|</strong>&nbsp;已发送邮件
		 					<%		
		 						}
		 					%>
		 				</legend>
				 			<p style="text-align:left;clear:both;">
				  				<span class="stateName">First Name:</span>
				  				<span class="stateValue"><%= returnOrders[n].getString("first_name")%></span>
			  				</p>
			  				<p style="text-align:left;clear:both;">
				  				<span class="stateName">Last Name:</span>
				  				<span class="stateValue"><%= returnOrders[n].getString("last_name")%></span>
			  				</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">Tel:</span>
			  					<span class="stateValue"><%=returnOrders[n].getString("tel")%></span>
							</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">Street:</span>
		  						<span class="stateValue"><%= returnOrders[n].getString("address_street")%></span>
							</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">City:</span>
			  					<span class="stateValue"><%= returnOrders[n].getString("address_city")%></span>
							</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">State:</span>
			  					<span class="stateValue"><%= returnOrders[n].getString("address_state")%></span>
							</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">Country:</span>
			  					<span class="stateValue"><%= returnOrders[n].getString("address_country")%></span>
							</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">Zip:</span>
			  					<span class="stateValue"><%= returnOrders[n].getString("address_zip")%></span>
							</p>
			 				<%		
				 				DBRow[] itemsRow = returnProductItemsFixMgrZr.getReturnItemsByRpId(returnOrders[n].get("rp_id", 0L));
				 				if(itemsRow != null && itemsRow.length > 0 )
				 				{
				 			%>
				 				<div style='border-top: 2px silver dotted;line-height: 20px;padding-top: 2px;padding-bottom: 2px;float: left; width: 100%;'>
				 			<%
									for(DBRow item : itemsRow)
									{
									DBRow product = productMgr.getDetailProductByPcid(item.get("pid",0l));
									%>	
									<p>
		 								<%=product.getString("p_name") %> x	
										<span style="color:blue"><%=item.get("quantity",0.0f) %>&nbsp;<%=product.getString("unit_name") %></span>
		 							</p>
		 							<p>
		 								<span style="color: green;">退货原因:</span><%=item.getString("return_reason") %>
		 							</p>
						 	<%
									}
							%>
								</div>
							<%
								}
				 			%>
				 		</fieldset>
		 		<%
		 				}
		 				else if(ReturnTypeKey.VIRTUAL == returnOrders[n].get("is_need_return",0))
		 				{
		 					int[] fileTypes = {FileWithTypeKey.RETURN_VIRTUAL};
			 				DBRow[] files = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(returnOrders[n].get("rp_id", 0L), fileTypes);
							if(files.length > 0){
								for(int f = 0; f < files.length; f ++)
								{
									DBRow file = files[f];
						%>
								 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
				 			 	 <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
				 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
				 			 	 <p>
			 			 	 	<%
			 			 	 		if(StringUtil.isPictureFile(file.getString("file_name")))
			 			 	 	 	{ 
			 			 	 	%>
					 			 	 
					 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.RETURN_VIRTUAL %>','<%=returnOrders[n].get("rp_id", 0L)%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
				 			 	 <%
				 			 	 	}
									else if(StringUtil.isOfficeFile(file.getString("file_name")))
									{
								%>
				 			 	 		<a href="javascript:void(0)"  file_id='<%=file.get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("service_virtual")%>')" file_is_convert='<%=file.get("file_is_convert",0) %>'><%=file.getString("file_name") %></a>
				 			 	 <%	}
									else
									{
								%>
				 			 	 	  	<a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("service_virtual")%>'><%=file.getString("file_name") %></a> 
				 			 	 <%
				 			 	 	}
								%>
								&nbsp;<%=null == adminMgrLL.getAdminById(file.getString("upload_adid"))?"":adminMgrLL.getAdminById(file.getString("upload_adid")).getString("employe_name") %>
								<% if(!"".equals(file.getString("upload_time"))){
										out.println("&nbsp;"+DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(file.getString("upload_time"))));
									}
								%>
									</p><br/>
						<%	
								}
							}
		 				}
	 				}
 				}
		 		%>
 		</td>
 	</tr>
 	<tr>
 		<td valign="middle">
	 		<%
 				DBRow[] serviceHandles = serviceOrderMgrZyj.getServiceOrderReasonSolutionsBySid(serviceRow.get("sid",0L));
	 			for(int m = 0; m < serviceHandles.length; m ++)
	 			{
	 				String lastStyleStr = "";
	 				if(m != (serviceHandles.length - 1))
	 				{
	 					lastStyleStr = " style='border-bottom: 2px silver dotted;'";
	 				}
 			%>
	 			<div><span style="color: green;">客户要求:</span><%=serviceHandles[m].getString("reason") %></div>
	 			<div><span style="color: green;">解决方案:</span><%=serviceHandles[m].getString("solution") %></div>
	 			<div <%=lastStyleStr %>><span style="color: green;">服务备注:</span><%=serviceHandles[m].getString("description") %></div>
 			<%		
	 			}
 			%>
 		</td>
 	</tr>
</table>   
</body>
</html>