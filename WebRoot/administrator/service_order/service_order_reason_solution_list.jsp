<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%

long sid				= StringUtil.getLong(request, "sid");
DBRow[] serviceHandles	= serviceOrderMgrZyj.getServiceOrderReasonSolutionsBySid(sid);
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>服务单处理列表</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
.serviceReasonTable tr td{border: 1px solid silver;line-height: 25px; };
</style>
<script type="text/javascript">
function addServiceReasonSolution()
{
	var html = '<div name="seriveReasonSolutionDivs" style="border: 2px solid #FFFFCC;width: 100%;margin-top: 5px;margin-bottom: 5px;">'
				+'<table style="width: 100%;padding-top:3px;padding-bottom:3px;">'
				+'	<tr style="width: 100%;">'
				+'		<td style="width: 90%;">'
				+'			<table class="serviceReasonTable" style="width: 100%;border-collapse: collapse;">'
				+'				<tr>'
				+'					<td style="width: 15%;">客户要求</td>'
				+'					<td style="width: 85%;">'
				+'						<textarea name="reason" style="width:590px;height:80px;border:1px #999999 solid"></textarea>'
				+'					</td>'
				+'				</tr>'
				+'				<tr>'
				+'					<td style="width: 15%;">解决方案</td>'
				+'					<td style="width: 85%;">'
				+'						<textarea name="solution" style="width:590px;height:80px;border:1px #999999 solid"></textarea>'
				+'					</td>'
				+'				</tr>'
				+'				<tr>'
				+'					<td style="width: 15%;">服务备注</td>'
				+'					<td style="width: 85%;">'
				+'						<textarea name="description" style="width:590px;height:80px;border:1px #999999 solid"></textarea>'
				+'					</td>'
				+'				</tr>'
				+'			</table>'
				+'		</td>'
				+'		<td style="width: 10%;" align="center">'
				+'			<input type="button" class="short-short-button" value="删除" onclick="delServiceReasonSolution(this)"/>'		
				+'		</td>'
				+'	</tr>'
				+'</table>'
				+'</div>';
	$("div[name^=seriveReasonSolutionDivs]:last").after($(html));
}
function delServiceReasonSolution(delBut)
{
	$(delBut).parent().parent().parent().parent().parent().remove();
}
function subForm()
{
	var canSub = true;
	var reasons = $("textarea[name=reason]");
	for(var i = 0; i < reasons.length; i ++)
	{
		if('' == reasons[i].value || '' == reasons[i].value.trim())
		{
			canSub = false;
			break;
		}
	}
	if(canSub)
	{
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/SaveServiceOrderReasonSolutionAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
				showMessage("申请成功","success");
				windowClose();
			},
			error:function(){
				$.unblockUI();
				showMessage("系统错误","error");
			}
		})
	}
	else
	{
		alert("请填写客户要求");
	}
}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function refreshWindow()
{
	window.location.reload();
}
function updateServiceReasonSolution(srs_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_reason_solution_update.html?srs_id='+srs_id;
	$.artDialog.open(uri , {title: '服务单处理['+srs_id+']',width:'800px',height:'330', lock: true,opacity: 0.3,fixed: true});
}
</script>
</head>
<body>
<form action="" method="post" id="subForm">
	<input type="hidden" name="sid" value='<%=sid %>'/>
	<%
	if(serviceHandles.length > 0)
	{
		for(int i = 0; i < serviceHandles.length; i ++)
		{
	%>	
	<div name="seriveReasonSolutionDivs" style="border: 2px solid #FFFFCC;width: 100%;margin-top: 5px;margin-bottom: 5px;">
		<table style="width: 100%;padding-top:3px;padding-bottom:3px;">
			<tr style="width: 100%;">
				<td style="width: 90%;">
					<table class="serviceReasonTable" style="width: 100%;border-collapse: collapse;">
						<tr>
							<%
								if(!"".equals(serviceHandles[i].getString("creator")))
								{
							%>
								<td colspan="2">
									<img title="创建人" src="../imgs/order_client.gif" />
									<%=null==adminMgrLL.getAdminById(serviceHandles[i].getString("creator"))?"":adminMgrLL.getAdminById(serviceHandles[i].getString("creator")).getString("employe_name")%>
									&nbsp;<img  title="创建时间" src="../imgs/alarm-clock--arrow.png"/>
									<%=!"".equals(serviceHandles[i].getString("create_time"))?serviceHandles[i].getString("create_time").substring(5,16):""%>
								</td>
							<%		
								}
							%>
						</tr>
						<tr>
							<td style="width: 15%;">客户要求</td>
							<td style="width: 85%;">
								<%=serviceHandles[i].getString("reason") %>
							</td>
						</tr>
						<tr>
							<td style="width: 15%;">解决方案</td>
							<td style="width: 85%;">
								<%=serviceHandles[i].getString("solution") %>
							</td>
						</tr>
						<tr>
							<td style="width: 15%;">服务备注</td>
							<td style="width: 85%;">
								<%=serviceHandles[i].getString("description") %>
							</td>
						</tr>
					</table>
				</td>
				<td style="width: 10%;" align="center">
					<input type="button" class="short-short-button" value="修改" onclick='updateServiceReasonSolution(<%=serviceHandles[i].get("srs_id",0L) %>)'/>
					<%
						if(i == (serviceHandles.length-1))
						{
					%>
						<br/><br/><input type="button" class="short-short-button" value="添加" onclick="addServiceReasonSolution()"/>		
					<%		
						}
					%>
				</td>
			</tr>
		</table>
	</div>
	<%		
		}
	}
	else
	{
	%>
	<div name="seriveReasonSolutionDivs" style="border: 2px solid #FFFFCC;width: 100%;margin-top: 5px;margin-bottom: 5px;">
		<table style="width: 100%;padding-top:3px;padding-bottom:3px;">
			<tr style="width: 100%;">
				<td style="width: 90%;">
					<table class="serviceReasonTable" style="width: 100%;border-collapse: collapse;">
						<tr>
							<td style="width: 15%;">客户要求</td>
							<td style="width: 85%;">
								<textarea id="reason" name="reason" style="width:590px;height:80px;border:1px #999999 solid"></textarea>
							</td>
						</tr>
						<tr>
							<td style="width: 15%;">解决方案</td>
							<td style="width: 85%;">
								<textarea id="solution" name="solution" style="width:590px;height:80px;border:1px #999999 solid"></textarea>
							</td>
						</tr>
						<tr>
							<td style="width: 15%;">服务备注</td>
							<td style="width: 85%;">
								<textarea id="description" name="description" style="width:590px;height:80px;border:1px #999999 solid"></textarea>
							</td>
						</tr>
					</table>
				</td>
				<td colspan="2" align="center">
						<input type="button" class="short-short-button" value="添加" onclick="addServiceReasonSolution()"/>		
				</td>
			</tr>
		</table>
	</div>
	<%
	}
	%><br/><br/>
	<div align="center"><input type="button" class="normal-green" value="提交" onclick="subForm()"/></div>
</form>
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>