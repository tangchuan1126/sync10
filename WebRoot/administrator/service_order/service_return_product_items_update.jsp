<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultTypeKey"%>
<%
//long stt = System.currentTimeMillis();

cartReturn.flush(session);
DBRow cartProducts[] = cartReturn.getDetailProducts();
ReturnProductItemHandleResultRequestTypeKey handleResultRequestTypeKey = new ReturnProductItemHandleResultRequestTypeKey();
ArrayList<String> returnRequestTypelist = ( ArrayList<String> )handleResultRequestTypeKey.getStatus();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>创建退货单明细</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script>

</script>

<style>
a.del-cart-product:link {
	color:#FF0000;
	text-decoration: none;
}
a.del-cart-product:visited {
	color: #FF0000;
	text-decoration: none;
}
a.del-cart-product:hover {
	color: #FF0000;
	text-decoration: underline;

}
a.del-cart-product:active {
	color: #FF0000;
	text-decoration: none;

}
</style> 
<script type="text/javascript">
function checkItem(pid)
{
	var check_product_context = $("#check_product_context_"+pid).val();
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/service_order/check_return_product_item_temp.html?pid=" + pid+"&check_product_context="+check_product_context; 
	$.artDialog.open(uri , {title: '商品测试',width:'400px',height:'230px', lock: true,opacity: 0.3,fixed: true});
}
function setCheckProductInfo(val,pid)
{
	$("#check_product_context_span_"+pid).text(val);
	$("#check_product_context_"+pid).val(val);
	$("#check_product_context_span_"+pid)[0].style.background="#FFB5B5";
	cartProQuanHasBeChange = true;
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="3px">
  
<%
if (cartProducts.length==0)
{
%>
      <tr>
        <td colspan="3" align="center" valign="middle" style="color:#999999"><<暂无商品>>
		<script>cartIsEmpty=true;</script></td>
      </tr>
<%
} 
else
{
%>
	<tr style="height: 35px;" valign="top" bgcolor="#f8f8f8">
		<td>商品名</td><td>数量</td><td>处理要求、测试</td><td>退货原因</td><td>服务单明细ID</td><td>&nbsp;</td>
	</tr>      
	  
<%
String bgColor;
for (int i=0; i<cartProducts.length; i++)
{
	if (i%2==0)
	{
		bgColor = "#FFFFFF";
	}
	else
	{
		bgColor = "#f8f8f8";
	}
	
%>
	
      <tr bgcolor="<%=bgColor%>">
        <td width="30%"  style="padding-top:5px;padding-bottom:5px;">
			<%=cartProducts[i].getString("p_name")%> 
			<input type="hidden" name="pids" id="pids" value="<%=cartProducts[i].getString("cart_pid")%>">
			<input type="hidden" name="product_type" id="product_type" value="<%=cartProducts[i].getString("cart_product_type")%>">	
		</td>
	    <td width="10%">
			<input name="quantitys" type="text" id="quantitys" style="width:40px;" value="<%=cartProducts[i].getString("cart_quantity")%>" onChange="return onChangeQuantity(this)"  /> 
	        <%=cartProducts[i].getString("unit_name")%>
        </td>
        <td width="27%">
	       <select name="handle_result_request" onchange="needSaveTemp(this)">
 				<option value="-1">处理要求</option>
 				<%
	 				for(String str : returnRequestTypelist)
	 				{ 
 				%>
 					<option value="<%=str %>" <%=str.equals(cartProducts[i].getString("handle_result_request"))?"selected='selected'":"" %>><%=handleResultRequestTypeKey.getStatusById(Integer.parseInt(str)) %></option>
 				<%
 					}
				%>
 			</select><br/>
	 		 <a href="javascript:void(0)" title="商品测试" onclick="checkItem('<%=cartProducts[i].get("order_item_id",0L)+"_"+cartProducts[i].getString("cart_pid")%>')">测试</a>&nbsp;
	 		 <input type="hidden" id="check_product_context_<%=cartProducts[i].get("order_item_id",0L)+"_"+cartProducts[i].getString("cart_pid")%>" name="check_product_context" value='<%=cartProducts[i].getString("check_product_context") %>'/>
       		 <span id="check_product_context_span_<%=cartProducts[i].get("order_item_id",0L)+"_"+cartProducts[i].getString("cart_pid")%>"><%=cartProducts[i].getString("check_product_context") %></span>
       </td>
       <td width="20%">
       		<input type="text" name="return_reason" value='<%=cartProducts[i].getString("return_reason") %>' onchange="needSaveTemp(this)"/>
       </td>
       <td width="11%">
       	<%="SI_"+cartProducts[i].get("order_item_id",0L) %>
       </td>
        <td width="3%" align="center" valign="middle">
			<%
        		String value = ""+cartProducts[i].get("order_item_id",0L);
        		//如果是套装cart_pid是商品ID，否则是散件ID
	        	if(0 == cartProducts[i].get("set_or_part_pc_id",0))//套装
				{
	        		value += "_"+cartProducts[i].get("cart_pid",0l)+"_"+cartProducts[i].get("cart_total_count",0F);
				}
	        	else
	        	{
	        		value += "_"+cartProducts[i].get("set_or_part_pc_id",0l)+"_"+cartProducts[i].get("set_pc_count",0F)
	        			   + "_"+cartProducts[i].get("cart_pid",0l);
	        	}
        	%>
        	<input type="hidden" name="order_item_id" value='<%=cartProducts[i].get("order_item_id",0L) %>'/>
        	<input type="hidden" name="set_or_part_pc_id" value='<%=cartProducts[i].get("set_or_part_pc_id",0l) %>'/>
			<a href="javascript:void(0)" onClick="delProductFromCart('<%=value%>','<%=cartProducts[i].getString("p_name")%>')" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>		
		</td>
      </tr>
<%
} 
%>
      <tr>
        <td height="50" colspan="6" style="border-top:2px #eeeeee solid;">
	        <input name="Submit433" type="submit" class="short-button" value="保存修改"> 
	          <script>cartIsEmpty=false;</script>
        </td>
      </tr>
<%
}

//long ent = System.currentTimeMillis();
//System.out.println("Cart Page time:"+(ent-stt));
%>
</table>

</body>
</html>
