<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.ReturnTypeKey"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<jsp:useBean id="billTypeKey" class="com.cwc.app.key.BillTypeKey"></jsp:useBean>
<jsp:useBean id="methodKey" class="com.cwc.app.key.PaymentMethodKey"></jsp:useBean>
<jsp:useBean id="billState" class="com.cwc.app.key.BillStateKey"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
long sid			= StringUtil.getLong(request, "sid");
DBRow[] rows		= serviceOrderMgrZyj.getServiceBillOrdersBySid(sid);
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>服务单列表</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
.set{border:2px silver solid;padding:2px;width:80%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:20px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
#bill_info tr td{line-height: 20px;font-size: 20;}
</style>
<script type="text/javascript">
function viewBill(bid)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/bill_manager.html?searchkey=\"'+bid+'\"&search_mode=1&comefrom=tool';
	window.open(uri);
}
</script>
</head>
<body>
<%
if(rows.length > 0)
{
	for(int i = 0; i < rows.length; i ++)
	{
%>
<fieldset class="set" style="padding-bottom:4px;border:2px solid silver;">
	<legend>
		<span class="title">
			<span style="font-weight:bold;">
				<a href="javascript:void(0);" onclick="viewBill('<%= rows[i].get("bill_id",0l) %>');" style="cursor: pointer;">
				[<%= rows[i].get("bill_id",0l) %>]
				</a>
			&nbsp;
			<%=billTypeKey.getStatusById(rows[i].get("bill_type",0)) %> &nbsp;
			<%
				if(0 != rows[i].get("sid", 0L))
				{
					System.out.println("S"+rows[i].get("sid", 0L));
				}
			%>
			</span>
		</span>
	</legend>
		<table style="width:100%;" id="bill_info">
			<tr>
				<td align="right" style="width:20%;">收款方式:</td>
				<td align="left" style="width:80%;"><%= methodKey.getStatusById(rows[i].get("key_type",0)) %></td>
			</tr>
			<tr>
				<td align="right" style="width:20%;">收款人:</td>
				<td align="left" style="width:80%;">
					<%
				 		DBRow accountRow = orderMgrZr.getDetailById(rows[i].get("account_payee",0l),"account_payee","id");
				 	%>
				 	 <%=  accountRow != null ? accountRow.getString("account_name"):rows[i].getString("account_name")%>
				</td>
			</tr>
			<tr>
				<td align="right" style="width:20%;">状态:</td>
				<td align="left" style="width:80%;"><%= billState.getStatusById(rows[i].get("bill_status",1))%></td>
			</tr>
			<tr>
				<td align="right" style="width:20%;">Client Id :</td>
				<td align="left" style="width:80%;"><%= rows[i].getString("client_id")%></td>
			</tr>
		</table>
</fieldset>
<%
	}
}
else
{
%>
<table style="width:100%;"><tr><td style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">未申请账单</td></tr></table>
<%
}
%>
</body>
</html>