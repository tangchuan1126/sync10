<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
	long srs_id = StringUtil.getLong(request, "srs_id");
	DBRow row	= serviceOrderMgrZyj.getServiceReasonSolutionBySrsid(srs_id);
	
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>更新客户要求解决方案</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<style type="text/css">
.serviceReasonTable tr td{border: 1px solid silver;line-height: 25px;border-collapse: collapse;};
</style>
<script type="text/javascript">
function subForm()
{
	var canSub = true;
	var reasons = $("textarea[name=reason]");
	for(var i = 0; i < reasons.length; i ++)
	{
		if('' == reasons[i].value || '' == reasons[i].value.trim())
		{
			canSub = false;
			break;
		}
	}
	if(canSub)
	{
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/UpdateServiceOrderReasonSolutionAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
				setTimeout("windowClose()", 1000);
			},
			error:function(){
				$.unblockUI();
				showMessage("系统错误","error");
			}
		})
	}
	else
	{
		alert("请填写客户要求");
	}
}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<form action="" id="subForm" method="post">
		<input type="hidden" name="srs_id" id="srs_id" value='<%=srs_id %>'/>
		<table width="100%" height="100%">
			<tr width="100%" height="90%" valign="top">
				<td width="100%" height="90%" valign="top">
					<table class="serviceReasonTable" style="width: 100%;height: 100%;border-collapse: collapse;">
						<tr>
							<td style="width: 15%;">客户要求</td>
							<td style="width: 85%;">
								<textarea id="reason" name="reason" style="width:100%;height:80px;border:1px #999999 solid"><%=row.getString("reason") %></textarea>
							</td>
						</tr>
						<tr>
							<td style="width: 15%;">解决方案</td>
							<td style="width: 85%;">
								<textarea id="solution" name="solution" style="width:100%;height:80px;border:1px #999999 solid"><%=row.getString("solution") %></textarea>
							</td>
						</tr>
						<tr>
							<td style="width: 15%;">服务备注</td>
							<td style="width: 85%;">
								<textarea id="description" name="description" style="width:100%;height:80px;border:1px #999999 solid"><%=row.getString("description") %></textarea>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr width="100%" height="10%" valign="bottom">
				<td align="right" valign="middle" class="win-bottom-line" width="100%">
					<input type="button" class="normal-green" value="提交" onclick="subForm()"/>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>