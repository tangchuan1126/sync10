<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.api.zyj.ReturnProductOrderMgrZyj"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.ReturnTypeKey"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
long sid = StringUtil.getLong(request, "sid");
DBRow serviceOrderDetail = serviceOrderMgrZyj.getServiceOrderDetailBySid(sid);
int is_need_return = serviceOrderDetail.get("is_need_return", 0);
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
int[] fileTypes = {FileWithTypeKey.RETURN_VIRTUAL};
DBRow[] files = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(sid, fileTypes);
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>更新服务单是否退货</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<style type="text/css">
.set{border:2px silver solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:15px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
#fileTable tr td{border: 1px solid silver; };
</style>
<script type="text/javascript">

function saveIsNeed()
{
	var is_need_returns = $("input:radio[name=is_need_returns]:checked").val();
	if(2 == is_need_returns && <%=files.length%>*1 == 0)
	{
		alert("请先上传文件");
	}
	else
	{
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/SaveServiceIsNeedReturnAction.action',
			data:{sid:<%=sid%>,is_need_return:is_need_returns},
			dataType:'json',
			type:'post',
			success:function(data)
			{
				setTimeout("windowClose()",1000);
			},
			error:function()
			{
				showMessage("系统错误","error");
				window.location.reload();
			}
		});
	}
}
$(function(){
	var is_need_return = $("input:radio[name=is_need_returns]:checked").val();
	if('<%=ReturnTypeKey.NEED%>'*1 == is_need_return)
	{
		$("#returnProductList").attr("style","");
		$("#serivce_virtual").attr("style","display:none");
		$("#serivce_virtual_file").attr("style","display:none");
	}
	else if('<%=ReturnTypeKey.VIRTUAL%>'*1 == is_need_return)
	{
		$("#serivce_virtual").attr("style","");
		$("#serivce_virtual_file").attr("style","");
		$("#returnProductList").attr("style","display:none");
	}
	else
	{
		$("#returnProductList").attr("style","display:none");
		$("#serivce_virtual").attr("style","display:none");
		$("#serivce_virtual_file").attr("style","display:none");
	}
	$("input:radio[name=is_need_returns]").click(
		function(){
			var val = $(this).val();
			if('<%=ReturnTypeKey.NEED%>'*1 == val)
			{
				$("#returnProductList").attr("style","");
				$("#serivce_virtual").attr("style","display:none");
				$("#serivce_virtual_file").attr("style","display:none");
			}
			else if('<%=ReturnTypeKey.VIRTUAL%>'*1 == val)
			{
				$("#serivce_virtual").attr("style","");
				$("#serivce_virtual_file").attr("style","");
				$("#returnProductList").attr("style","display:none");
			}
			else
			{
				$("#returnProductList").attr("style","display:none");
				$("#serivce_virtual").attr("style","display:none");
				$("#serivce_virtual_file").attr("style","display:none");
			}
		}
	);
	
});
</script>

<script type="text/javascript">
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	//文件上传
	function uploadFile(_target){
	    var obj  = {
		     reg:"all",
		     limitSize:8,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
			 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		}});
	}
	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,target){
		if($.trim(fileNames).length > 0 ){
			var targetNode = $("#"+target);
			$("input",targetNode).val(fileNames);
			$("#is_need_return").val($("input:radio[name=is_need_returns]:checked").val());
			$("#serviceFileForm").submit();
		}
	}
	function serviceFileFormSub(){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/UploadServiceVirtualReturnFilesAction.action',
			data:$("#serviceFileForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag == "true"){
					showMessage("上传成功","success");
					window.location.reload();
				}else{
					showMessage("上传失败","error");
					window.location.reload();
				}
			},
			error:function(){
				showMessage("系统错误","error");
				window.location.reload();
			}
		})	
	}
	 function onlineScanner(){
	 	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
	 		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	 	}
	//图片在线显示
		function showPictrueOnline(fileWithType,fileWithId , currentName){
		    var obj = {
				file_with_type:fileWithType,
				file_with_id : fileWithId,
				current_name : currentName ,
				cmd:"multiFile",
				table:'file',
				base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>'
			}
		    if(window.top && window.top.openPictureOnlineShow){
				window.top.openPictureOnlineShow(obj);
			}else{
			    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
			}
		    
		}
		function deleteFile(file_id, file_name){
			if(confirm("确定要删除文件：["+file_name+"]吗？")){
				$.ajax({
					url:'<%= deleteFileAction%>',
					dataType:'json',
					data:{table_name:'file',file_id:file_id,pk:'file_id'},
					success:function(data){
						if(data && data.flag === "success"){
							window.location.reload();
						}else{
							showMessage("系统错误,请稍后重试","error");
						}
					},
					error:function(){
						showMessage("系统错误,请稍后重试","error");
					}
				})
			}
		}
</script>

</head>
<body>
<form action="" method="post" id="subForm">
</form>
	<form action='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/UploadServiceVirtualReturnFilesAction.action' method="post" id="serviceFileForm">
	<input type="hidden" name="backurl" value='<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_is_need_return_update.html?sid=<%=sid %>'/>
	<input type="hidden" name="sid" value='<%=sid %>'/>
	<input type="hidden" name="is_need_return" id="is_need_return"/>
		<table style="width: 100%">
			<tr>
				<td align="right" style="width:10%;">是否退货:</td>
				<td align="left" style="width: 90%">
				<%
					DBRow[] returnOrders	= returnProductOrderMgrZyj.getReturnOrderBySid(sid);
					int returnOrderLen		= returnOrders.length;
					if(ReturnTypeKey.NEED == is_need_return)
					{
				%>
					<input type="radio" name="is_need_returns" value="1" checked="checked"/>需要退货
				<%		
					}
					else if(ReturnTypeKey.VIRTUAL == is_need_return)
					{
				%>
					<input type="radio" name="is_need_returns" value="2" checked="checked"/>虚拟退货
					<input type="radio" name="is_need_returns" value="3"/>无需退货
				<%			
					}
					else
					{
				%>
					<input type="radio" name="is_need_returns" value="2"/>虚拟退货
					<input type="radio" name="is_need_returns" value="3" checked="checked"/>无需退货
				<%			
					}
				%>
				</td>
			</tr>
			<tr id="returnProductList">
				<td colspan="2" align="left">
					<table style="width: 100%">
						<tr>
							<td>
							<%
								for(int i = 0; i < returnOrderLen; i ++)
								{
									DBRow rowTemp = returnOrders[i];
							%>
								<fieldset class="set" style="padding-bottom:4px;border:2px solid silver;">
				 					<legend>
											<span class="title">
												<span style="font-weight:bold;">退货单[<%=returnOrders[i].get("rp_id", 0L) %>]</span>
											</span> 
									</legend>
									<table style="width:100%;margin-bottom: 2px;border-bottom: 1px silver solid;">
										<tr style="width:100%;">
											<td style="width:8%;" align="right">
							  					<span class="alert-text valueName">Name:</span>
							  				</td>
							  				<td style="width:17%;" align="left">
							  					<span class="valueSpan"><%= rowTemp.getString("address_name")%></span>
											</td>
											 <td style="width:8%;" align="right">
							  					<span class="alert-text valueName">Tel:</span>
							  				 </td>
							  				 <td style="width:17%;" align="left">
							  					<span class="valueSpan"><%=rowTemp.getString("tel")%></span>
											 </td>
											<td style="width:8%;" align="right">
							  					<span class="alert-text valueName">Street:</span>
						  					</td>
						  					<td colspan="3" align="left">
							  					<span class="valueSpan"><%= rowTemp.getString("address_street")%></span>
											</td>
										 </tr>
										 <tr style="width:100%;">
										 	<td style="width:8%;" align="right">	
							  					<span class="alert-text valueName">City:</span>
							  				</td>
							  				<td style="width:17%;" align="left">
							  					<span class="valueSpan"><%= rowTemp.getString("address_city")%></span>
											</td>
											 <td style="width:8%;" align="right">
							  					<span class="alert-text valueName">State:</span>
							  				</td>
							  				<td style="width:17%;" align="left">
							  					<span class="valueSpan"><%= rowTemp.getString("address_state")%></span>
											 </td>
											 <td style="width:8%;" align="right">	
							  					<span class="alert-text valueName">Zip:</span>
							  				 </td>
							  				 <td style="width:17%;" align="left">
							  					<span class="valueSpan"><%= rowTemp.getString("address_zip")%></span>
											 </td>
											 <td style="width:9%;" align="right">
							  					<span class="alert-text valueName">Country:</span>
							  				 </td>
							  				 <td style="width:16%;" align="left">
							  					<span class="valueSpan"><%= rowTemp.getString("address_country")%></span>
											 </td>
										</tr>
									</table>
				 				<%
				 					DBRow[] itemsRow = returnProductItemsFixMgrZr.getReturnProductReason(returnOrders[i].get("rp_id", 0L));
				 					if(itemsRow != null && itemsRow.length > 0 ){
				 						
				 						for(DBRow item : itemsRow){
				 						DBRow product = productMgr.getDetailProductByPcid(item.get("pc_id",0l));
				 							%>
				 							<div>
				 								 <%=product.getString("p_name") %> x	<span style="color:blue"><%=item.get("quantity",0.0f) %> <%=product.getString("unit_name") %></span><br />
				 								<p style="margin-left:4px;font-weight:normal;">
				 							 		退货原因:<%=item.getString("return_reason") %>
				 								</p>
				 							</div>
				 							
				 							<% 
				 						}
				 					}
				 				%>
				 				</fieldset>
			 				<%
								}
							%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr id="serivce_virtual">
				<td width="10%" align="right" nowrap="nowrap" class="STYLE3" style="margin-top: 5px;">
					上传文件:
				</td>
				<td width="90%" align="left">
				
					<input type="hidden" name="sn" value="SR"/>
					<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.RETURN_VIRTUAL %>" />
					<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("service_virtual")%>" />
					<input type="button"  class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件" />
					<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
					<div id="jquery_file_up">	
		          		<input type="hidden" name="file_names" id="file_names" value=""/>
		          		<ol class="fileUL"> 
		          		</ol>
		          	</div>
				</td>
			</tr>
			<tr id="serivce_virtual_file">
			<td width="10%" align="right" valign="middle" nowrap="nowrap" class="STYLE3">&nbsp;</td>
			<td width="90%" align="left" valign="top" >
				<table width="75%" style="border-collapse: collapse;" id="fileTable">
					<%
						if(files.length > 0){
							for(int f = 0; f < files.length; f ++){
								DBRow file = files[f];
					%>
						<tr style="height: 20px;">
							<td width="50%" align="left">
							 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
			 			 	 <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
			 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
			 			 	 	<%if(StringUtil.isPictureFile(file.getString("file_name"))){ %>
					 			 	 <p>
					 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.RETURN_VIRTUAL %>','<%=sid%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
					 			 	 </p>
				 			 	 <%} else if(StringUtil.isOfficeFile(file.getString("file_name"))){%>
				 			 	 		<p>
				 			 	 			<a href="javascript:void(0)"  file_id='<%=file.get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("service_virtual")%>')" file_is_convert='<%=file.get("file_is_convert",0) %>'><%=file.getString("file_name") %></a>
				 			 	 		</p>
				 			 	 <%}else{ %>
	 		 			 	 	  		<p><a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("service_virtual")%>'><%=file.getString("file_name") %></a></p> 
	 		 			 	 	  <%} %>
							
							</td>
							<td width="15%" align="left">
								<%=null == adminMgrLL.getAdminById(file.getString("upload_adid"))?"":adminMgrLL.getAdminById(file.getString("upload_adid")).getString("employe_name") %>
							</td>
							<td width="25%" align="left">
							<% if(!"".equals(file.getString("upload_time"))){
									out.println(DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(file.getString("upload_time"))));
								}
							%>
							</td>
							<td width="10%">
									<a href="javascript:deleteFile('<%=file.get("file_id",0l) %>','<%=file.getString("file_name") %>')">删除</a>
							</td>
						</tr>
					<%	
							}
						}
					%>
				</table>
				</td>
			</tr>
			<tr id="gotoBillBtn">
				<td colspan="2" align="right">
					<input type="button" class="long-button" onclick="saveIsNeed();" value="保存"/>
				</td>
			</tr>
		</table>
	</form>
		
	<script type="text/javascript">
	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
  </script>
  
</body>
</html>