<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ReturnTypeKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<%@page import="com.cwc.app.key.ServiceOrderStatusKey"%>
<%@page import="com.cwc.app.key.ReturnProductKey"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%

long oid			= StringUtil.getLong(request, "oid");
long wid			= StringUtil.getLong(request, "wid");
DBRow[] rows	= new DBRow[0];
if(0 != wid)
{
	rows = serviceOrderMgrZyj.getServiceOrdersByWaybillId(wid);
}
else
{
	rows	= serviceOrderMgrZyj.getServiceOrdersByPOrderId(oid);
}
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>服务单列表</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<style type="text/css">
<%--.serviceTdClass--%>
#serviceTable tr td{border: 1px solid silver; };
.serviceItem{border: 1px solid yellow; }
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
span.valueName{width:40%;border:0px solid red;display:block;float:left;text-align:right;}
span.valueSpan{width:56%;border:0px solid green;float:left;overflow:hidden;text-align:left;text-indent:4px;}
span.stateName{width:22%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:78%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
tr.split td input{margin-top:2px;}
</style>
<script type="text/javascript">
function onlineScanner(){
 	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
 		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
 	}
//图片在线显示
	function showPictrueOnline(fileWithType,fileWithId , currentName){
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			cmd:"multiFile",
			table:'file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("service_virtual")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}
	function deleteFile(file_id, file_name){
		if(confirm("确定要删除文件：["+file_name+"]吗？")){
			$.ajax({
				url:'<%= deleteFileAction%>',
				dataType:'json',
				data:{table_name:'file',file_id:file_id,pk:'file_id'},
				success:function(data){
					if(data && data.flag === "success"){
						window.location.reload();
					}else{
						showMessage("系统错误,请稍后重试","error");
					}
				},
				error:function(){
					showMessage("系统错误,请稍后重试","error");
				}
			})
		}
	}
	function viewOrderListByOid(oid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/ct_order_auto_reflush.html?&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val='+oid;
		window.open(uri);
	}
function showServiceHandles(sid)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_reason_solution_list.html?sid='+sid;
	$.artDialog.open(uri , {title: '服务单['+sid+']处理列表',width:'800px',height:'600', lock: true,opacity: 0.3,fixed: true});
}
function refreshWindow()
{
	window.location.reload();
};
function serviceIsNeedReturnProduct(sid,oid)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_return_product.html?sid='+sid+'&oid='+oid;
	$.artDialog.open(uri , {title: '服务单['+sid+']申请退货',width:'900px',height:'600', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
}
function applyServiceBillOrder(sid)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/add_bill_service.html?oid=<%=oid%>&sid='+sid;
	$.artDialog.open(uri , {title: '服务单['+sid+']申请账单',width:'900px',height:'600', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
}
function serviceBillOrders(sid)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_bill_order_list.html?sid='+sid;
	$.artDialog.open(uri , {title: '服务单['+sid+']账单列表',width:'500px',height:'600', lock: true,opacity: 0.3,fixed: true});
}
function modServiceBillOrder(sid,bill_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/mod_service_bill_order.html?oid=<%=oid%>&bill_id='+bill_id+'&sid='+sid;
	$.artDialog.open(uri , {title: '修改服务单['+sid+']的账单['+bill_id+']',width:'900px',height:'600', lock: true,opacity: 0.3,fixed: true});
}
function openWaybillOrderListByWid(wid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/waybill/waybill_list.html?cmd=waybillId&search_mode=1&oid='+wid;
		window.open(uri);
	}
function modServiceIsNeedReturn(sid, oid, is_need_return,rp_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_return_product_update.html?sid='+sid+'&oid='+oid+'&is_need_return='+is_need_return+'&rp_id='+rp_id;
	var str = '服务单['+sid+']修改退货';
	if(0 != rp_id)
	{
		str += '['+rp_id+']';
	}
	$.artDialog.open(uri , {title: str,width:'930px',height:'450', lock: true,opacity: 0.3,fixed: true});
}
function modServiceOrder(sid,wid)
{
	if(0 == wid*1)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_update.html?oid=<%=oid%>&sid='+sid;
	}
	else
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_update_for_waybill.html?oid=<%=oid%>&sid='+sid+'&wid='+wid;
	}
	$.artDialog.open(uri , {title: '修改服务单['+sid+']',width:'1100px',height:'600', lock: true,opacity: 0.3,fixed: true});
}
function openBillOrderListByBillId(bill_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/bill_manager.html?search_mode=1&comefrom=tool&searchkey="'+bill_id+'"';;
	window.open(uri);
}
function test(sid,oid)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_is_need_return.html?sid='+sid+'&oid='+oid;
	$.artDialog.open(uri , {title: '服务单['+sid+']是否需要退货',width:'900px',height:'600', lock: true,opacity: 0.3,fixed: true});
}
function changeServiceOrderStatus(sid, status,info)
{
	if(confirm("确定要将服务单：["+sid+"]置为"+info+"吗？")){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/UpdateServiceOrderStatusAction.action',
			dataType:'json',
			data:{sid:sid,status:status},
			success:function(data){
				if(data && data.flag === "success")
				{
					window.location.reload();
				}
			},
			error:function(){
			}
		})
	}
}
function refreshWindow()
	{
		window.location.reload();
	};
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload = "onLoadInitZebraTable()">

<table width="98%" border="0" align="center"  style="margin-top:5px;" cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" isNeed="true" isBottom="true">
<tr>
	<th width="28%" class="right-title"  style="text-align:center;">服务信息</th>
    <th width="33%"  class="right-title"  style="text-align: center;">服务明细</th>
    <th width="39%"  class="right-title"  style="text-align: center;">退货情况</th>
</tr>
<%
if(rows.length > 0)
{
	for(int i = 0; i < rows.length; i ++)
	{
		DBRow[] billRows		= serviceOrderMgrZyj.getServiceBillOrdersBySid(rows[i].get("sid",0L));//账单
%>
 	<tr>
 		<td>
 			<fieldset class="set" style="border:2px solid #993300;text-align:left">
 				<legend>
 					<strong>
 						[&nbsp;<%=rows[i].get("sid",0L) %>&nbsp;]&nbsp;|
 						<%=new ServiceOrderStatusKey().getServiceOrderStatusById(rows[i].get("status",0)) %>
 					</strong>
 				</legend>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
 					<img title="创建人" src="../imgs/order_client.gif" />
 					<%=null==adminMgrLL.getAdminById(rows[i].getString("create_user"))?"":adminMgrLL.getAdminById(rows[i].getString("create_user")).getString("employe_name")%>
 				</p>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
  					<img  title="创建时间" src="../imgs/alarm-clock--arrow.png"/>
  					<%=rows[i].getString("create_date").substring(5,16)%>
  				</p>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
 				原始订单:<a href="javascript:void(0);" onclick="viewOrderListByOid(<%=rows[i].getString("oid")%>)"><%=rows[i].getString("oid")%></a>
 				</p>
 				<%
 					if(billRows.length > 0)
 					{
 				%>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
 				关联账单:<a href="javascript:void(0);" onclick='openBillOrderListByBillId(<%=billRows[0].get("bill_id",0L) %>)' style="cursor: pointer;"><%=billRows[0].get("bill_id",0L) %></a>
 				</p>
 				<%
 					}
 				%>
 				<%
 					if(0 != rows[i].get("wid",0L))
 					{
 				%>
 					<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
	 				关联运单:<a href="javascript:void(0);" onclick='openWaybillOrderListByWid(<%=rows[i].get("wid",0L) %>)' style="cursor: pointer;"><%=rows[i].get("wid",0L) %></a>
	 				</p>
 				<%		
 					}
 				%>
 				<%
 					if(0 != rows[i].get("is_need_return",0))
 					{
 				%>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
	 				是否退货:<%=new ReturnTypeKey().getReturnTypeKeyValue(rows[i].get("is_need_return",0)) %>
	 			</p>		
 				<%
 					}
 				%>
 			</fieldset>
 		</td>
 		<td>
 		<%
 			DBRow[] serviceItems = serviceOrderMgrZyj.getServiceOrderItemsByServiceId(rows[i].get("sid",0L));
 			if(serviceItems.length > 0)
 			{
 		%>
 			<div style="border: 2px silver solid;line-height: 20px;padding-top: 3px;padding-bottom: 3px;margin-top: 2px;margin-bottom: 2px;">
 				<%
 					WarrantyTypeKey warrantyTypeKey = new WarrantyTypeKey();
	 				for(int j = 0; j < serviceItems.length; j ++)
	 				{
	 			%>
	 				<div style="line-height: 20px;">
 						<%=serviceItems[j].getString("p_name") %>&nbsp;x&nbsp;
 						<span style="color:blue">
	 						<%=serviceItems[j].get("quantity",0F) %>&nbsp;
	 						<%=serviceItems[j].getString("unit_name")%>
	 					</span>&nbsp;
	 					<span style="color:blue;font-weight: bold;">
							<%=warrantyTypeKey.getWarrantyTypeById(serviceItems[j].get("warranty_type",0))   %>
						</span>
	 				</div>
	 				<div>
	 					<span style="color: green;">服务原因:</span><%=serviceItems[j].getString("service_reason") %>
	 				</div>
	 			<%		
	 				}
	 			%>
	 		</div>
	 	<%
 			}
 			else
 			{
 				out.println("&nbsp;");
 			}
	 	%>
 		</td>
 		<td valign="middle">
 		<%
 			DBRow[] returnOrders	= returnProductOrderMgrZyj.getReturnOrderBySid(rows[i].get("sid",0L));//退货单
 			if(returnOrders.length > 0)
 			{
		 		for(int n = 0; n < returnOrders.length; n ++)
				{
		 			if(ReturnTypeKey.NOT_NEED != returnOrders[n].get("is_need_return",0))
		 			{
 		%>
		 			<fieldset class="set" style="border:2px solid #993300;text-align:left">
		 				<legend>
		 					退货单<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/return_product_list.html?cmd=search&key=<%=returnOrders[n].get("rp_id", 0L)%>&search_mode=1' target="_blank">[&nbsp;<%=returnOrders[n].get("rp_id", 0L) %>&nbsp;]</a>
							&nbsp;|
		 					<%
							DBRow productStoreCatalog = catalogMgr.getDetailProductStorageCatalogById(returnOrders[n].get("ps_id",0l));
							if(null != productStoreCatalog)
							{
								out.println(productStoreCatalog.getString("title")+"&nbsp;|");
							}
							%>
		 					<%=new ReturnTypeKey().getReturnTypeKeyValue(returnOrders[n].get("is_need_return",0)) %>
		 					<%
		 						if(1 == returnOrders[n].get("is_send_mail",0))
		 						{
		 					%>
		 						&nbsp;|已发送邮件
		 					<%		
		 						}
		 					%>
		 				</legend>
			 				<%		
				 				DBRow[] itemsRow = returnProductItemsFixMgrZr.getReturnItemsByRpId(returnOrders[n].get("rp_id", 0L));
				 				if(itemsRow != null && itemsRow.length > 0 )
				 				{
				 			%>
				 				<div style='line-height: 20px;float: left; width: 100%;'>
				 			<%
									for(DBRow item : itemsRow)
									{
									DBRow product = productMgr.getDetailProductByPcid(item.get("pid",0l));
									%>	
									<p>
		 								<%=product.getString("p_name") %> x	
										<span style="color:blue"><%=item.get("quantity",0.0f) %>&nbsp;<%=product.getString("unit_name") %></span>
		 							</p>
		 							<p>
		 								<span style="color: green;">退货原因:</span><%=item.getString("return_reason") %>
		 							</p>
						 	<%
									}
							%>
								</div>
							<%
								}
				 			%>
				 		</fieldset>
		 		<%
		 				}
		 				if(ReturnTypeKey.VIRTUAL == returnOrders[n].get("is_need_return",0))
		 				{
		 					int[] fileTypes = {FileWithTypeKey.RETURN_VIRTUAL};
			 				DBRow[] files = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(returnOrders[n].get("rp_id", 0L), fileTypes);
							if(files.length > 0){
								for(int f = 0; f < files.length; f ++)
								{
									DBRow file = files[f];
						%>
								 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
				 			 	 <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
				 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
				 			 	 <p>
			 			 	 	<%
			 			 	 		if(StringUtil.isPictureFile(file.getString("file_name")))
			 			 	 	 	{ 
			 			 	 	%>
					 			 	 
					 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.RETURN_VIRTUAL %>','<%=returnOrders[n].get("rp_id", 0L)%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
				 			 	 <%
				 			 	 	}
									else if(StringUtil.isOfficeFile(file.getString("file_name")))
									{
								%>
				 			 	 		<a href="javascript:void(0)"  file_id='<%=file.get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("service_virtual")%>')" file_is_convert='<%=file.get("file_is_convert",0) %>'><%=file.getString("file_name") %></a>
				 			 	 <%	}
									else
									{
								%>
				 			 	 	  	<a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("service_virtual")%>'><%=file.getString("file_name") %></a> 
				 			 	 <%
				 			 	 	}
								%>
								&nbsp;<%=null == adminMgrLL.getAdminById(file.getString("upload_adid"))?"":adminMgrLL.getAdminById(file.getString("upload_adid")).getString("employe_name") %>
								<% if(!"".equals(file.getString("upload_time"))){
										out.println("&nbsp;"+DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(file.getString("upload_time"))));
									}
								%>
										&nbsp;<a href="javascript:deleteFile('<%=file.get("file_id",0l) %>','<%=file.getString("file_name") %>')">删除</a>
									</p><br/>
						<%	
								}
							}
		 				}
	 				}
 				}
		 		%>
 			<br/>&nbsp;
 		</td>
 	</tr>
 	<tr class="split">
 		<td colspan="3" align="right">
 		<%
 			if(ServiceOrderStatusKey.CANCEL != rows[i].get("status",0))
 			{
 		%>
 			<input class = "short-button" type="button" value="服务处理" onclick='showServiceHandles(<%=rows[i].get("sid",0L) %>)'/>
 			<%
 			//服务单关联的账单
				if(billRows.length > 0)
				{
			%>
				<input class = "short-button" type="button" value="修改账单" onclick='modServiceBillOrder(<%=rows[i].get("sid",0L) %>,<%=billRows[0].get("bill_id",0L) %>)'/>
			<%		
				}
				else
				{
			%>
				<input class = "short-button" type="button" value="创建账单" onclick='applyServiceBillOrder(<%=rows[i].get("sid",0L) %>)'/>
			<%		
				}
			%>
 			<%
 				long rp_id = 0L; 
 				int returnStatus = 0;
 				if(returnOrders.length > 0)
 				{
 					rp_id = returnOrders[0].get("rp_id",0L);
 					returnStatus = returnOrders[0].get("status",0);
 				}
 				if(0 == rp_id)
 				{
 			%>
				<input class = "short-button" type="button" value="修改服务" onclick='modServiceOrder(<%=rows[i].get("sid",0L) %>,<%=rows[i].get("wid",0L) %>)'/>
 				<input class = "short-button" type="button" value="申请退货" onclick='serviceIsNeedReturnProduct(<%=rows[i].get("sid",0L) %>,<%=rows[i].get("oid",0L) %>)'/>
 			<%
 				}
 				else
 				{
 					//如果没有开始识别商品
 					DBRow[] returnItemFix = returnProductItemsFixMgrZr.getReturnProductItmesFix(rp_id);
 					if(0 == returnItemFix.length && (returnStatus == ReturnProductKey.WAITINGPRODUCT || returnStatus == ReturnProductKey.WAITING) && 2 == returnOrders[0].get("create_type",0))
 					{
			%>
				<input class = "short-button" type="button" value="修改退货" onclick='modServiceIsNeedReturn(<%=rows[i].get("sid",0L) %>,<%=rows[i].get("oid",0L) %>,<%=rows[i].get("is_need_return",0) %>,<%=rp_id %>)'/>
			<%		
 					}
 				}
			%>
			<input class = "short-button" type="button" value="服务完成" onclick='changeServiceOrderStatus(<%=rows[i].get("sid",0L) %>,<%=ServiceOrderStatusKey.FINISH %>,"完成")'/>
			<%
				if(0 == rp_id && 0==billRows.length)
				{
			%>
			<input class = "short-button" type="button" value="取消" onclick='changeServiceOrderStatus(<%=rows[i].get("sid",0L) %>,<%=ServiceOrderStatusKey.CANCEL %>,"取消")'/>
 			<%
				}
 			%>
 		<%
 			}
 		%>
 		</td>
 	</tr>
<%
	}
}
else
{
%>
<tr>
	<td colspan="3" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
</tr>
<%
}
%>
</table>
</body>
</html>