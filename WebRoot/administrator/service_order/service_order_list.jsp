<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ReturnTypeKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<%@page import="com.cwc.app.key.ServiceOrderStatusKey"%>
<%@page import="com.cwc.app.api.zr.ReturnProductItemsFixMgrZr"%>
<%@page import="com.cwc.app.key.ReturnProductKey"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
int search_mode = StringUtil.getInt(request,"search_mode");
int status = StringUtil.getInt(request, "status");
int is_need_return = StringUtil.getInt(request, "is_need_return");
long create_adid = StringUtil.getLong(request, "create_adid",-1);
TDate date = new TDate();
date.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
String	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
String 	input_st_date =	  date.getStringYear()+"-"+date.getStringMonth()+"-"+date.getStringDay();
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(10);
DBRow[] rows = new DBRow[0];
if("search".equals(cmd))
{
	rows = serviceOrderMgrZyj.searchServiceOrderByNumber(key,search_mode,pc);
}
else if("filter".equals(cmd))
{
	input_st_date = StringUtil.getString(request,"st");
	input_en_date = StringUtil.getString(request,"end");
	rows = serviceOrderMgrZyj.getServiceOrderByPage(pc, status,is_need_return,input_st_date,input_en_date,create_adid);
}
else
{
	rows = serviceOrderMgrZyj.getServiceOrderByPage(pc);
}
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/return_order/ReturnProductFileDownLoadAction.action";
String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>服务单列表</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
 

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
 
 

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>

<style type="text/css">

	
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
span.valueName{width:40%;border:0px solid red;display:block;float:left;text-align:right;}
span.valueSpan{width:56%;border:0px solid green;float:left;overflow:hidden;text-align:left;text-indent:4px;}
span.stateName{width:20%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
tr.split td input{margin-top:2px;}
</style>
<script type="text/javascript">
	jQuery(function($){
		 $("select[id='create_adid'] option[value='"+<%= create_adid%>+"']").attr("selected",true);
		addAutoComplete($("#search_key"),
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/service_order/GetSearchServiceOrderJSONAction.action",
				"merge_field","sid");
	})
	function search()
	{
		var val = $("#search_key").val();
				
		if(val.trim()=="")
		{
			alert("请输入要查询的关键字");
		}
		else
		{
			var val_search = "\'"+val.toLowerCase()+"\'";
			$("#search_key").val(val_search);
			document.search_form.key.value = val_search;
			document.search_form.search_mode.value = 1;
			document.search_form.submit();
		}
	}
	
	function searchRightButton()
	{
		var val = $("#search_key").val();
				
		if (val=="")
		{
			alert("你好像忘记填写关键词了？");
		}
		else
		{
			val = val.replace(/\'/g,'');
			$("#search_key").val(val);
			document.search_form.key.value = val;
			document.search_form.search_mode.value = 2;
			document.search_form.submit();
		}
	}
	function filterServiceOrders()
	{
		document.filter_form.status.value = $("#status").val();
		document.filter_form.is_need_return.value = $("#is_need_return").val();
		document.filter_form.st.value = $("#st").val();
		document.filter_form.end.value = $("#end").val();
		document.filter_form.create_adid.value = $("#create_adid").val();
		document.filter_form.submit();
	}
	function eso_button_even()
	{
		document.getElementById("eso_search").oncontextmenu=function(event) 
		{  
			if (document.all) window.event.returnValue = false;// for IE  
			else event.preventDefault();  
		};  
			
		document.getElementById("eso_search").onmouseup=function(oEvent) 
		{  
			if (!oEvent) oEvent=window.event;  
			if (oEvent.button==2) 
			{  
			   searchRightButton();
			}  
		}  
	}
	function onlineScanner(){
 	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
 		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
 	}
//图片在线显示
	function showPictrueOnline(fileWithType,fileWithId , currentName){
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			cmd:"multiFile",
			table:'file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>returnImg'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}
	function deleteFile(file_id, file_name){
		if(confirm("确定要删除文件：["+file_name+"]吗？")){
			$.ajax({
				url:'<%= deleteFileAction%>',
				dataType:'json',
				data:{table_name:'file',file_id:file_id,pk:'file_id'},
				success:function(data){
					if(data && data.flag === "success"){
						window.location.reload();
					}else{
						showMessage("系统错误,请稍后重试","error");
					}
				},
				error:function(){
					showMessage("系统错误,请稍后重试","error");
				}
			})
		}
	}
	function viewOrderListByOid(oid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/ct_order_auto_reflush.html?&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val='+oid;
		window.open(uri);
	}
	function showServiceHandles(sid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_reason_solution_list.html?sid='+sid;
		$.artDialog.open(uri , {title: '服务单['+sid+']处理列表',width:'800px',height:'600', lock: true,opacity: 0.3,fixed: true});
	}
	function serviceIsNeedReturnProduct(sid,oid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_is_need_return.html?sid='+sid+'&oid='+oid;
		$.artDialog.open(uri , {title: '服务单['+sid+']是否需要退货',width:'900px',height:'600', lock: true,opacity: 0.3,fixed: true});
	}
	function modServiceBillOrder(sid,oid,bill_id)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/mod_service_bill_order.html?bill_id='+bill_id+'&sid='+sid+'&oid='+oid;
		$.artDialog.open(uri , {title: '修改服务单['+sid+']的账单['+bill_id+']',width:'900px',height:'600', lock: true,opacity: 0.3,fixed: true});
	}
	function applyServiceBillOrder(sid,oid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/add_bill_service.html?oid='+oid+'&sid='+sid;
		$.artDialog.open(uri , {title: '服务单['+sid+']申请账单',width:'900px',height:'600', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	function modServiceIsNeedReturn(sid, is_need_return)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_is_need_return_update.html?sid='+sid+'&is_need_return='+is_need_return;
		$.artDialog.open(uri , {title: '修改服务单['+sid+']是否需要退货',width:'900px',height:'600', lock: true,opacity: 0.3,fixed: true});
	}
	function serviceIsNeedReturnProduct(sid,oid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_is_need_return.html?sid='+sid+'&oid='+oid;
		$.artDialog.open(uri , {title: '服务单['+sid+']是否需要退货',width:'900px',height:'600', lock: true,opacity: 0.3,fixed: true});
	}
	function modServiceOrder(sid,wid,oid)
	{
		if(0 == wid*1)
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_update.html?oid='+oid+'&sid='+sid;
		}
		else
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_update_for_waybill.html?oid='+oid+'&sid='+sid+'&wid='+wid;
		}
		$.artDialog.open(uri , {title: '修改服务单['+sid+']',width:'1100px',height:'600', lock: true,opacity: 0.3,fixed: true});
	}
	function openBillOrderListByBillId(bill_id)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/bill_manager.html?search_mode=1&comefrom=tool&searchkey="'+bill_id+'"';;
		window.open(uri);
	}
	function openWaybillOrderListByWid(wid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/waybill/waybill_list.html?cmd=waybillId&search_mode=1&oid='+wid;
		window.open(uri);
	}
	function serviceIsNeedReturnProduct(sid,oid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_return_product.html?sid='+sid+'&oid='+oid;
		$.artDialog.open(uri , {title: '服务单['+sid+']申请退货',width:'900px',height:'600', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	function modServiceIsNeedReturn(sid, oid, is_need_return,rp_id)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_return_product_update.html?sid='+sid+'&oid='+oid+'&is_need_return='+is_need_return+'&rp_id='+rp_id;
		var str = '服务单['+sid+']修改退货';
		if(0 != rp_id)
		{
			str += '['+rp_id+']';
		}
		$.artDialog.open(uri , {title: str,width:'930px',height:'450', lock: true,opacity: 0.3,fixed: true});
	}
	function changeServiceOrderStatus(sid, status,info)
	{
		if(confirm("确定要将服务单：["+sid+"]置为"+info+"态吗？")){
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service_order/UpdateServiceOrderStatusAction.action',
				dataType:'json',
				data:{sid:sid,status:status},
				success:function(data){
					if(data && data.flag === "success")
					{
						window.location.reload();
					}
				},
				error:function(){
				}
			})
		}
	}
	function refreshWindow()
	{
		window.location.reload();
	};
	function go(index){
		var form = $("#pageForm");
		$("input[name='p']",form).val(index);
		form.submit();
	}
	function serviceOrderView(sid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_view.html?sid='+sid;
		$.artDialog.open(uri, {title:"查看服务单["+sid+"]详细",width:'800px',height:'600px',lock:true,opacity:0.3,fixed:true});
	}
	function relevanceReturnProduct(sid)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_relevance_return_product.html?sid='+sid;
		$.artDialog.open(uri, {title:"服务单["+sid+"]关联退货单",width:'800px',height:'600px',lock:true,opacity:0.3,fixed:true});
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload = "onLoadInitZebraTable()">
<form action='<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_list.html' method="post" name="search_form">
	<input type="hidden" name="key"/>
	<input type="hidden" name="cmd" value="search"/>
	<input type="hidden" name="search_mode"/>
</form>
<form action='<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_list.html' method="post" name="filter_form">
	<input type="hidden" name="status"/>
	<input type="hidden" name="is_need_return"/>
	<input type="hidden" name="st"/>
	<input type="hidden" name="end"/>
	<input type="hidden" name="create_adid"/>
	<input type="hidden" name="cmd" value="filter"/>
</form>
<div class="demo" style="width:98%;margin:0px auto;" >
	<div id="tabs">
		<ul>
			<li><a href="#service_search">常用工具</a></li>
			<li><a href="#filter_search">高级搜索</a></li>
		</ul>
		<div id="service_search">
			<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
									 <input name="search_key" type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="search_key" onkeydown="if(event.keyCode==13)search()" value="<%=key%>"/>
									</div>
								</td>
							</tr>
						</table>
				</td>
	              <td width="45%"></td>
	              <td width="12%" align="right" valign="middle">
				  </td>
	            </tr>
	          </table>
			<input type="hidden" name="cmd" value="search">
		</div>
		<div id="filter_search">
		<br/>
			<table>
				<tr>
					<td align="left">
					开始:<input type="text" id="st" value="<%= input_st_date %>" class="st" style="width:100px;"/>&nbsp;-&nbsp;
						<input type="text" id="end" value="<%= input_en_date %>" class="end" style="width:100px;"/>
						&nbsp;<select id="create_adid">
				 				<option value="-1">全部客服报价人</option>
				 			<%
								DBRow admins[] = adminMgr.getAllAdmin(null);
								for (int i=0; i<admins.length; i++){
							%>
								   <option value="<%=admins[i].get("adid",0l)%>" > 
								    <%=admins[i].getString("account")%>  </option>
							 <%
								}
								%>
				 			</select>
						&nbsp;<select id="status">
							<option value="0">服务状态</option>
							<%
								ServiceOrderStatusKey serviceOrderStatusKey = new ServiceOrderStatusKey();
								ArrayList<String> serviceOrderStatusList = serviceOrderStatusKey.getServiceOrderStatus();
								for(int i = 0; i < serviceOrderStatusList.size(); i ++)
								{
							%>
								<option value='<%=serviceOrderStatusList.get(i) %>' <%=serviceOrderStatusList.get(i).equals(String.valueOf(status))?" selected=selected":"" %>><%=serviceOrderStatusKey.getServiceOrderStatusById(serviceOrderStatusList.get(i)) %></option>
							<%
								}
							%>
						</select>&nbsp;&nbsp;
						<select id="is_need_return">
							<option value="0">是否退货</option>
							<%
								ReturnTypeKey returnTypeKey = new ReturnTypeKey();
								ArrayList<String> returnTypeKeyList = returnTypeKey.getReturnTypeKeys();
								for(int i = 0; i < returnTypeKeyList.size(); i ++)
								{
							%>
								<option value='<%=returnTypeKeyList.get(i) %>' <%=returnTypeKeyList.get(i).equals(String.valueOf(is_need_return))?" selected=selected":"" %>><%=returnTypeKey.getReturnTypeKeyValue(returnTypeKeyList.get(i)) %></option>
							<%
								}
							%>
						</select>&nbsp;&nbsp;
						<input type="button" class="short-button" value="过滤" onclick="filterServiceOrders()"/>
					</td>
				</tr>
			</table>
		<br/>
		</div>
	</div>
</div>
<table width="98%" border="0" align="center"  style="margin-top:5px;" cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" isNeed="true" isBottom="true">
<tr>
	<th width="20%" class="right-title"  style="text-align:center;">服务信息</th>
    <th width="25%"  class="right-title"  style="text-align: center;">服务明细</th>
    <th width="30%"  class="right-title"  style="text-align: center;">退货情况</th>
    <th width="25%"  class="right-title"  style="text-align: center;">服务处理</th>
</tr>
<%
if(rows.length > 0)
{
	for(int i = 0; i < rows.length; i ++)
	{
		DBRow[] billRows		= serviceOrderMgrZyj.getServiceBillOrdersBySid(rows[i].get("sid",0L));//账单
%>
 	<tr>
 		<td>
 			<fieldset class="set" style="border:2px solid #993300;text-align:left">
 				<legend>
 					<strong>
 						[&nbsp;<%=rows[i].get("sid",0L) %>&nbsp;]&nbsp;|&nbsp;
 						<%=new ServiceOrderStatusKey().getServiceOrderStatusById(rows[i].get("status",0)) %>
 					</strong>
 				</legend>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
 					<img title="创建人" src="../imgs/order_client.gif" />
 					<%=null==adminMgrLL.getAdminById(rows[i].getString("create_user"))?"":adminMgrLL.getAdminById(rows[i].getString("create_user")).getString("employe_name")%>
 				</p>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
  					<img  title="创建时间" src="../imgs/alarm-clock--arrow.png"/>
  					<%=rows[i].getString("create_date").substring(5,16)%>
  				</p>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
 				原始订单:<a href="javascript:void(0);" onclick="viewOrderListByOid(<%=rows[i].getString("oid")%>)"><%=rows[i].getString("oid")%></a>
 				</p>
 				<%
 					if(billRows.length > 0)
 					{
 				%>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
 				关联账单:<a href="javascript:void(0);" onclick='openBillOrderListByBillId(<%=billRows[0].get("bill_id",0L) %>)' style="cursor: pointer;"><%=billRows[0].get("bill_id",0L) %></a>
 				<%
 					if(0 != rows[i].get("warranty_oid",0L))
 					{
 				%>
 					&rarr; 订单:<a href="javascript:void(0);" onclick="viewOrderListByOid(<%=rows[i].getString("warranty_oid")%>)"><%=rows[i].getString("warranty_oid")%></a>
 				<%		
 					}
 				%>
 				</p>
 				<%
 					}
 				%>
 				<%
 					if(0 != rows[i].get("wid",0L))
 					{
 				%>
 					<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
	 				关联运单:<a href="javascript:void(0);" onclick='openWaybillOrderListByWid(<%=rows[i].get("wid",0L) %>)' style="cursor: pointer;"><%=rows[i].get("wid",0L) %></a>
	 				</p>
 				<%		
 					}
 				%>
 				<%
 					if(0 != rows[i].get("is_need_return",0))
 					{
 				%>
 				<p style="text-align:left;text-indent:5px;font-weight:normal;margin-top:3px;">
	 				是否退货:<%=new ReturnTypeKey().getReturnTypeKeyValue(rows[i].get("is_need_return",0)) %>
	 			</p>		
 				<%
 					}
 				%>
 			</fieldset>
 		</td>
 		<td>
 		<%
 			DBRow[] serviceItems = serviceOrderMgrZyj.getServiceOrderItemsByServiceId(rows[i].get("sid",0L));
 			if(serviceItems.length > 0)
 			{
 		%>
 			<div style="border: 2px silver solid;line-height: 20px;padding-top: 3px;padding-bottom: 3px;margin-top: 2px;margin-bottom: 2px;">
 				<%
 					WarrantyTypeKey warrantyTypeKey = new WarrantyTypeKey();
	 				for(int j = 0; j < serviceItems.length; j ++)
	 				{
	 			%>
	 				<div style="line-height: 20px;">
 						<%=serviceItems[j].getString("p_name") %>&nbsp;x&nbsp;
 						<span style="color:blue">
	 						<%=serviceItems[j].get("quantity",0F) %>&nbsp;
	 						<%=serviceItems[j].getString("unit_name")%>
	 					</span>&nbsp;
	 					<span style="color:blue;font-weight: bold;">
							<%=warrantyTypeKey.getWarrantyTypeById(serviceItems[j].get("warranty_type",0))   %>
						</span>
	 				</div>
	 				<div>
	 					<span style="color: green;">服务原因:</span><%=serviceItems[j].getString("service_reason") %>
	 				</div>
	 			<%		
	 				}
	 			%>
	 		</div>
	 	<%
 			}
 			else
 			{
 				out.println("&nbsp;");
 			}
	 	%>
 		</td>
 		<td valign="middle">
 		<%
 			DBRow[] returnOrders	= returnProductOrderMgrZyj.getReturnOrderBySid(rows[i].get("sid",0L));//退货单
 			if(returnOrders.length > 0)
 			{
		 		for(int n = 0; n < returnOrders.length; n ++)
				{
		 			if(ReturnTypeKey.NOT_NEED != returnOrders[n].get("is_need_return",0))
		 			{
 		%>
		 			<fieldset class="set" style="border:2px solid #993300;text-align:left">
		 				<legend>
		 					退货单<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/return_product_list.html?cmd=search&key=<%=returnOrders[n].get("rp_id", 0L)%>&search_mode=1' target="_blank">[&nbsp;<%=returnOrders[n].get("rp_id", 0L) %>&nbsp;]</a>
		 					&nbsp;<strong>|</strong>&nbsp;
		 					<%
							DBRow productStoreCatalog = catalogMgr.getDetailProductStorageCatalogById(returnOrders[n].get("ps_id",0l));
							if(null != productStoreCatalog)
							{
								out.println(productStoreCatalog.getString("title")+"&nbsp;|&nbsp;");
							}
							%>
		 					<%=new ReturnTypeKey().getReturnTypeKeyValue(returnOrders[n].get("is_need_return",0)) %>
		 					<%
		 						if(1 == returnOrders[n].get("is_send_mail",0))
		 						{
		 					%>
		 						&nbsp;<strong>|</strong>&nbsp;已发送邮件
		 					<%		
		 						}
		 					%>
		 				</legend>
				 			<p style="text-align:left;clear:both;">
				  				<span class="stateName">First Name:</span>
				  				<span class="stateValue"><%= returnOrders[n].getString("first_name")%></span>
			  				</p>
			  				<p style="text-align:left;clear:both;">
				  				<span class="stateName">Last Name:</span>
				  				<span class="stateValue"><%= returnOrders[n].getString("last_name")%></span>
			  				</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">Tel:</span>
			  					<span class="stateValue"><%=returnOrders[n].getString("tel")%></span>
							</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">Street:</span>
		  						<span class="stateValue"><%= returnOrders[n].getString("address_street")%></span>
							</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">City:</span>
			  					<span class="stateValue"><%= returnOrders[n].getString("address_city")%></span>
							</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">State:</span>
			  					<span class="stateValue"><%= returnOrders[n].getString("address_state")%></span>
							</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">Country:</span>
			  					<span class="stateValue"><%= returnOrders[n].getString("address_country")%></span>
							</p>
							<p style="text-align:left;clear:both;">
			  					<span class="stateName">Zip:</span>
			  					<span class="stateValue"><%= returnOrders[n].getString("address_zip")%></span>
							</p>
			 				<%		
				 				DBRow[] itemsRow = returnProductItemsFixMgrZr.getReturnItemsByRpId(returnOrders[n].get("rp_id", 0L));
				 				if(itemsRow != null && itemsRow.length > 0 )
				 				{
				 			%>
				 				<div style='border-top: 2px silver dotted;line-height: 20px;padding-top: 2px;padding-bottom: 2px;float: left; width: 100%;'>
				 			<%
				 					//out.println("<div style='border-bottom: 2px silver solid;line-height: 20px;padding-top: 2px;padding-bottom: 2px;'></div>");
									for(DBRow item : itemsRow)
									{
									DBRow product = productMgr.getDetailProductByPcid(item.get("pid",0l));
									%>	
									<p>
										<%
											String not_on_rma_color = "black";
											String not_on_rma_title = "";
											if(1 == item.get("is_product_not_on_rma",0))
											{
												not_on_rma_color = "#f60";
												not_on_rma_title = "非原退货单上商品";
											}
										%>
		 								<span style='font-weight:bold;color:<%=not_on_rma_color %>;' title='<%=not_on_rma_title %>'><%=null==product?"":product.getString("p_name") %></span> x	
										<span style="color:blue"><%=item.get("quantity",0.0f) %>&nbsp;<%=null==product?"":product.getString("unit_name") %></span>
		 							</p>
		 							<p>
		 								<span style="color: green;">退货原因:</span><%=item.getString("return_reason") %>
		 							</p>
						 	<%
									}
							%>
								</div>
							<%
								}
				 			%>
				 		</fieldset>
		 		<%
		 				}
		 				if(ReturnTypeKey.VIRTUAL == returnOrders[n].get("is_need_return",0))
		 				{
		 					int[] fileTypes = {FileWithTypeKey.ProductReturn};
			 				DBRow[] files = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(returnOrders[n].get("rp_id", 0L), fileTypes);
							if(files.length > 0){
								for(int f = 0; f < files.length; f ++)
								{
									DBRow file = files[f];
						%>
								 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
				 			 	 <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
				 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
				 			 	 <p>
			 			 	 	<%
			 			 	 		if(StringUtil.isPictureFile(file.getString("file_name")))
			 			 	 	 	{ 
			 			 	 	%>
					 			 	 
					 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.ProductReturn %>','<%=returnOrders[n].get("rp_id", 0L)%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
				 			 	 <%
				 			 	 	}
									else if(StringUtil.isOfficeFile(file.getString("file_name")))
									{
								%>
				 			 	 		<a href="javascript:void(0)"  file_id='<%=file.get("file_id",0l) %>' onclick="openOfficeFileOnlineReturnFile(this,'<%=ConfigBean.getStringValue("systenFolder")%>','returnImg')" file_is_convert='<%=file.get("file_is_convert",0) %>'><%=file.getString("file_name") %></a>
				 			 	 <%	}
									else
									{
								%>
				 			 	 	  	<a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder=returnImg'><%=file.getString("file_name") %></a> 
				 			 	 <%
				 			 	 	}
								%>
								&nbsp;<%=null == adminMgrLL.getAdminById(file.getString("upload_adid"))?"":adminMgrLL.getAdminById(file.getString("upload_adid")).getString("employe_name") %>
								<% if(!"".equals(file.getString("upload_time"))){
										out.println("&nbsp;"+DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(file.getString("upload_time"))));
									}
								%>
										&nbsp;<a href="javascript:deleteFile('<%=file.get("file_id",0l) %>','<%=file.getString("file_name") %>')">删除</a>
									</p><br/>
						<%	
								}
							}
		 				}
	 				}
 				}
		 		%>
 			<br/>&nbsp;
 		</td>
 		<td valign="middle">
 			<%
 				if(serviceOrderMgrZyj.getServiceOrderReasonSolutionCountBySid(rows[i].get("sid",0L)) > 2)
 				{
	 				DBRow[] serviceHandles = serviceOrderMgrZyj.getServiceOrderReasonSolutionsBySid(rows[i].get("sid",0L),2);
	 				for(int m = 0; m < serviceHandles.length; m ++)
	 				{
 			%>
 				<div><span style="color: green;">客户要求:</span><%=serviceHandles[m].getString("reason") %></div>
 				<div><span style="color: green;">解决方案:</span><%=serviceHandles[m].getString("solution") %></div>
 				<div style='border-bottom: 2px silver dotted;'><span style="color: green;">服务备注:</span><%=serviceHandles[m].getString("description") %></div>
 			<%		
	 				}
	 		%>
	 			<a href="javascript:void(0);" onclick="showServiceHandles(<%=rows[i].get("sid",0L) %>)" style="cursor: pointer;">更多</a>
	 		<%
	 			}
 				else
 				{
 					DBRow[] serviceHandles = serviceOrderMgrZyj.getServiceOrderReasonSolutionsBySid(rows[i].get("sid",0L));
	 				for(int m = 0; m < serviceHandles.length; m ++)
	 				{
	 					String lastStyleStr = "";
	 					if(m != (serviceHandles.length - 1))
	 					{
	 						lastStyleStr = " style='border-bottom: 2px silver dotted;'";
	 					}
 			%>
	 				<div><span style="color: green;">客户要求:</span><%=serviceHandles[m].getString("reason") %></div>
	 				<div><span style="color: green;">解决方案:</span><%=serviceHandles[m].getString("solution") %></div>
	 				<div <%=lastStyleStr %>><span style="color: green;">服务备注:</span><%=serviceHandles[m].getString("description") %></div>
 			<%		
	 				}
 				}
 			%>
 			<br/>&nbsp;
 		</td>
 	</tr>
 	<tr class="split">
 		<td colspan="4" align="right">
 		<%
 			if(ServiceOrderStatusKey.CANCEL != rows[i].get("status",0))
 			{
 		%>
 			<input class = "short-button" type="button" value="服务处理" onclick='showServiceHandles(<%=rows[i].get("sid",0L) %>)'/>
 			<%
 			//服务单关联的账单
				if(billRows.length > 0)
				{
			%>
				<input class = "short-button" type="button" value="修改账单" onclick='modServiceBillOrder(<%=rows[i].get("sid",0L) %>,<%=rows[i].get("oid",0L) %>,<%=billRows[0].get("bill_id",0L) %>)'/>
			<%		
				}
				else
				{
			%>
				<input class = "short-button" type="button" value="创建账单" onclick='applyServiceBillOrder(<%=rows[i].get("sid",0L) %>,<%=rows[i].get("oid",0L) %>)'/>
			<%		
				}
			%>
 			<%
 				long rp_id = 0L; 
 				int returnStatus = 0;
 				if(returnOrders.length > 0)
 				{
 					rp_id = returnOrders[0].get("rp_id",0L);
 					returnStatus = returnOrders[0].get("status",0);
 				}
 				if(0 == rp_id)
 				{
 			%>
 				<input class = "short-button" type="button" value="修改服务" onclick='modServiceOrder(<%=rows[i].get("sid",0L) %>,<%=rows[i].get("wid",0L) %>,<%=rows[i].get("oid",0L) %>)'/>
 				<input class = "short-button" type="button" value="申请退货" onclick='serviceIsNeedReturnProduct(<%=rows[i].get("sid",0L) %>,<%=rows[i].get("oid",0L) %>)'/>
 				<input class = "short-button" type="button" value="关联退货单" onclick='relevanceReturnProduct(<%=rows[i].get("sid",0L) %>)'/>
 			<%
 				}
 				else
 				{
 					//还未上传商品图片，刚刚创建或等待退货时，客服的可以修改
 					DBRow[] returnItemFix = returnProductItemsFixMgrZr.getReturnProductItmesFix(rp_id);
 					if(0 == returnItemFix.length && (returnStatus == ReturnProductKey.WAITINGPRODUCT || returnStatus == ReturnProductKey.WAITING) && 2 == returnOrders[0].get("create_type",0))
 					{
			%>
				<input class = "short-button" type="button" value="修改退货" onclick='modServiceIsNeedReturn(<%=rows[i].get("sid",0L) %>,<%=rows[i].get("oid",0L) %>,<%=rows[i].get("is_need_return",0) %>,<%=rp_id %>)'/>
			<%		
 					}
				}
			%>
			<%
				if(ServiceOrderStatusKey.FINISH != rows[i].get("status",0))
				{
			%>
			<input class = "short-button" type="button" value="服务完成" onclick='changeServiceOrderStatus(<%=rows[i].get("sid",0L) %>,<%=ServiceOrderStatusKey.FINISH %>,"完成")'/>
			<%		
				}
			%>
			<%
				if(0 == rp_id && 0==billRows.length)
				{
			%>
			<input class = "short-button" type="button" value="取消" onclick='changeServiceOrderStatus(<%=rows[i].get("sid",0L) %>,<%=ServiceOrderStatusKey.CANCEL %>,"取消")'/>
 			<%
				}
 			%>
<%-- 			<input class = "short-button" type="button" value="查看服务" onclick="serviceOrderView('<%=rows[i].get("sid",0L) %>')"/>--%>
 		<%
 			}
 		%>
 		</td>
 	</tr>
<%
	}
}
else
{
%>
<tr>
	<td colspan="4" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
</tr>
<%
}
%>
</table>
<script>
$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>
<form action="" id="pageForm">
		<input type="hidden" name="p" id="pageCount"/>
	
		<input type="hidden" name="cmd" value="<%=cmd %>"/>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<script type="text/javascript">
$('.st , .end').datepicker({
	dateFormat:"yy-mm-dd",
	changeMonth: true,
	changeYear: true
});
$("#ui-datepicker-div").css("display","none");
</script>
</body>

</html>