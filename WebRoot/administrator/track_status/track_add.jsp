<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>物流添加</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
	div.cssDivschedule{
			width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
			background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
			background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
	
</style>
<%
	WaybillInternalTrackingKey waybillInternalTrackingKey = new WaybillInternalTrackingKey();
	List trackStatusList = waybillInternalTrackingKey.getWaybillInternalTrack();
	String companyName = StringUtil.getString(request, "companyName");
	
 %>

<script type="text/javascript">
$(function(){
	$(".cssDivschedule").css("display","none");
});
	function submitData(){
		if(volidate()){
		$(".cssDivschedule").css("display","block");
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/track_status/AddTrack.action',
				data:$("#subForm").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "true"){
						showMessage("添加成功","success");
						setTimeout("windowClose()", 1000);
					}else{
						$(".cssDivschedule").css("display","none");
						showMessage("添加失败，此公司的此物流编码已存在","error");
					}
				},
				error:function(){
					$(".cssDivschedule").css("display","none");
					showMessage("系统错误","error");
				}
			})	
		}
	};
	function volidate(){
		if($("#trackCode").val() == ""){
			alert("物流编码不能为空");
			return false;
		}else if(!/^[A-Z]{2}$/.test($("#trackCode").val())){
			alert("物流编码的格式为两个大写字母组成，如：AC");
			return false;
		}
		if($("#trackDefinition").val() == ""){
			alert("物流描述不能为空");
			return false;
		}
		return true;
	};
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};

</script>

</head>
<body>
<div class="cssDivschedule" style="">
	
</div>
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			创建物流编码
		</legend>	
		<form action="" id="subForm">
		<input type="hidden" name="shipping_company" value="<%=companyName %>" >
			<table>
				<tr>
					<td>物流编码</td>
					<td>
						<input name="tracking_status_code" id="trackCode" />
					</td>
				</tr>
				<tr>
					<td>物流公司</td>
					<td>
						<%=companyName %>
					</td>
				</tr>
				<tr>
					<td>物流状态</td>
					<td>
						<select name="tracking_status">
							<%
								if(null != trackStatusList && trackStatusList.size() > 0){
									for (int i = 0; i < trackStatusList.size(); i++) {
										String trackStatus = (String)trackStatusList.get(i);
										if(!(WaybillInternalTrackingKey.UnKnown+"").equals(trackStatus) && !(WaybillInternalTrackingKey.NotYetDeliveryComplete+"").equals(trackStatus)){
							%>
									<option value="<%=trackStatus %>"><%=waybillInternalTrackingKey.getWaybillInternalTrack(trackStatus) %></option>
							<%
										}
									}
								}
							 %>
						</select>
					</td>
				</tr>
				<tr>
					<td>物流描述</td>
					<td>
						<textarea rows="7" cols="50" name="tracking_definition" id="trackDefinition"></textarea>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<table align="right">
		<tr align="right">
			<td colspan="2"><input type="button" class="normal-green" value="提交" onclick="submitData();"/></td>
		</tr>
	</table>
	<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>