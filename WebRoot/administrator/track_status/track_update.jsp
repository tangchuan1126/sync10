<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>物流修改</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<%
	WaybillInternalTrackingKey waybillInternalTrackingKey = new WaybillInternalTrackingKey();
	List trackStatusList = waybillInternalTrackingKey.getWaybillInternalTrack();
	
	DBRow trackStatusInfo = trackStatusMgrZyj.getTrackStatusById(request);
	

 %>

<script type="text/javascript">

	function submitData(){
		if(volidate()){
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/track_status/updateTrack.action',
				data:$("#subForm").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "true"){
						showMessage("修改成功","success");
						setTimeout("windowClose()", 1000);
					}else{
						showMessage("修改失败，此公司的此物流编码已存在","error");
					}
				},
				error:function(){
					showMessage("系统错误","error");
				}
			})	
		}
	};
	function volidate(){
		if($("#trackCode").val() == ""){
			alert("物流编码不能为空");
			return false;
		}else if(!/^[A-Z]{2}$/.test($("#trackCode").val())){
			alert("物流编码的格式为两个大写字母组成，如：AC");
			return false;
		}
		if($("#trackDefinition").val() == ""){
			alert("物流描述不能为空");
			return false;
		}
		return true;
	};
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};

</script>

</head>
<body>
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			创建物流编码
		</legend>	
		<form action="" id="subForm">
			<input type="hidden" name="track_id" value='<%=trackStatusInfo.get("track_id",0) %>'/>
			<input type="hidden" name="shipping_company" value='<%=trackStatusInfo.getString("shipping_company") %>'/>
			<table>
				<tr>
					<td>物流编码</td>
					<td>
						<input name="tracking_status_code" id="trackCode" value='<%=trackStatusInfo.getString("tracking_status_code") %>'/>
					</td>
				</tr>
				<tr>
					<td>物流公司</td>
					<td>
						<%=trackStatusInfo.getString("shipping_company") %>
					</td>
				</tr>
				<tr>
					<td>物流状态</td>
					<td>
						<select name="tracking_status">
							<%
								if(null != trackStatusList && trackStatusList.size() > 0){
									for (int i = 0; i < trackStatusList.size(); i++) {
										String trackStatus = (String)trackStatusList.get(i);
										if(!(WaybillInternalTrackingKey.UnKnown+"").equals(trackStatus) && !(WaybillInternalTrackingKey.NotYetDeliveryComplete+"").equals(trackStatus)){
											if(trackStatusInfo.get("tracking_status",-1) == Integer.valueOf(trackStatus)){
							%>
									<option value="<%=trackStatus %>" selected="selected"><%=waybillInternalTrackingKey.getWaybillInternalTrack(trackStatus) %></option>
							<%
											}else{
							%>	
									<option value="<%=trackStatus %>"><%=waybillInternalTrackingKey.getWaybillInternalTrack(trackStatus) %></option>		
										
							<%				
											}
										}
									}
								}
							 %>
						</select>
					</td>
				</tr>
				<tr>
					<td>物流描述</td>
					<td>
						<textarea rows="7" cols="50" name="tracking_definition" id="trackDefinition"><%=trackStatusInfo.getString("tracking_definition") %></textarea>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<table align="right">
		<tr align="right">
			<td colspan="2"><input type="button" class="normal-green" value="提交" onclick="submitData();"/></td>
		</tr>
	</table>
	<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>