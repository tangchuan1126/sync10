<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.WaybillInternalTrackingKey" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>物流列表</title>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<script src="../js/fullcalendar/colorpicker.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/css/colorpicker.css" rel="stylesheet" />
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<%
	Map<String, DBRow[]> trackMap = trackStatusMgrZyj.getTrackStatusMapByShipCom();
	WaybillInternalTrackingKey waybillInternalTrackingKey = new WaybillInternalTrackingKey();
	
 %>
<script type="text/javascript">
	 $(function(){
		 $("#tabs").tabs({
				cache: true,
				spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
				cookie: { expires: 30000 } ,
			});

	  });
	  function addTrack(companyName){
			var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/track_status/track_add.html?companyName="+companyName;
			$.artDialog.open(url , {title: '添加物流信息',width:'600px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	  };
	  function refreshWindow(){
			window.location.reload();
	  };
	  function updateRrack(trackId){
		  var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/track_status/track_update.html?trackStatusId="+trackId;
		  $.artDialog.open(url , {title: '修改物流信息',width:'600px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	  };
	  function deleteRrack(trackId){
			$.artDialog.confirm('你确认删除此物流信息？', function(){
				deleteTrackById(trackId);
			}, function(){
			});
	  };
	  function deleteTrackById(trackId){
		 	$.ajax({
		 		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/track_status/DelTrackStatus.action?trackStatusId='+trackId,
				type:'post',
				dataType:'json',
				success:function(data){
		 			if(data && data.flag === "success"){
		 				window.location.reload();
		 			}
			    }
			})
	   }
</script>
<style type="text/css">
	tbody.mytbody tr td{
		line-height:30px;
		height:30px;
	}
</style>
</head>
  
  <body onload= "onLoadInitZebraTable()">
  		<div id="tabs" >
			 <ul>
			 	<%	 
			 		if(null != trackMap && trackMap.size() > 0){ 
				 			Set keySet = trackMap.keySet(); 
				 			for (Iterator iterator = keySet.iterator(); iterator.hasNext();) { 
				 				String comName = (String)iterator.next(); 
				%>
								<li><a href='#<%=comName %>'><%=comName %></a></li>
				<%				
							}
	 				}
			 	%>
			 </ul>
			 	<%	
			 		if(null != trackMap && trackMap.size() >0){
				 			Set keySet = trackMap.keySet();
				 			for (Iterator iterator = keySet.iterator(); iterator.hasNext();) {
				 				String compName = (String)iterator.next();
				%>
					 			<div id="<%=compName %>"> 
<%--					 			<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:98%;">--%>
					 				<input type="button" value="添加" class="long-button-add" onclick="addTrack('<%=compName %>');"/>
<%--					 			</div>--%>
									<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"  class="zebraTable" style="margin-left:3px;margin-top:5px;">
									  <tr> 
									  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">物流编码</th>
									  	<th width="42%" style="vertical-align: center;text-align: center;" class="right-title">物流描述</th>
								        <th width="18%" style="vertical-align: center;text-align: center;" class="right-title">物流公司</th>
								        <th width="15%" style="vertical-align: center;text-align: center;" class="right-title">物流状态</th>
								       	<th width="15%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
								      </tr>
								      <tbody class="mytbody">
								      	<%
								      		DBRow[] trackList = trackMap.get(compName);
								      		if(null != trackList && trackList.length >0){
								      			for(int i = 0; i < trackList.length; i ++){
								      				DBRow trackStatus = trackList[i];
								      	%>
								      			<tr>
								      				<td><%= trackStatus.getString("tracking_status_code") %></td>
								      				<td style="word-break:keep-all;"><%= trackStatus.getString("tracking_definition") %></td>
								      				<td><%= trackStatus.getString("shipping_company") %></td>
								      				<td><%= waybillInternalTrackingKey.getWaybillInternalTrack(trackStatus.getString("tracking_status"))    %></td>
								      				<td>
								      					<input type="button" class="short-short-button-mod" onclick="updateRrack('<%=trackStatus.get("track_id",0) %>')" value="修改"/>&nbsp;&nbsp;
									      	  			<input type="button" class="short-short-button-del" onclick="deleteRrack('<%=trackStatus.get("track_id",0)%>')" value="删除">
								      				</td>
								      			</tr>
								      	<%	
								      			}
								      		}
								      	 %>
								      
								      </tbody>
						    		 </table>
			 					 </div>
				<%				
							}
	 				}
			 	%>
			 		 
		 				
		</div>
		
  </body>
   
</html>
