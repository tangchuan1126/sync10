<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.util.StringUtil"%>
<%@page import="com.cwc.db.DBRow"%>
<%@page import="com.cwc.exception.JsonException"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.beans.jqgrid.FilterBean"%>
<%@page import="net.sf.json.JsonConfig"%>
<%@page import="com.cwc.app.beans.jqgrid.RuleBean"%>
<%@page import="com.cwc.json.JsonUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%! private static int oldpages = -1; %>
<%
	
	long repair_order_id = StringUtil.getLong(request,"repair_order_id");//100005;
			
	boolean search = Boolean.parseBoolean(StringUtil.getString(request,"_search"));//是否使用了搜索功能
	String sidx = StringUtil.getString(request,"sidx");//排序使用的字段
	String sord = StringUtil.getString(request,"sord");//排序顺序
	
	String filter = StringUtil.getString(request,"filters");//搜索条件
	
	FilterBean filterBean = null;//搜索条件bean
	
	if(search)//将搜索条件字符串转换成搜索条件bean，为方便后来搜索
	{
		Map<String, Object> map = new HashMap<String, Object>();  //String-->Property ; Object-->Clss 
		map.put("rules", RuleBean.class);
		JsonConfig configjson = JsonUtils.getJsonConfig();
		configjson.setClassMap(map);
		filterBean = (FilterBean) JsonUtils.toBean(JSONObject.fromObject(filter),FilterBean.class,configjson); 
	}
			
	String c = StringUtil.getString(request,"rows");//每页显示多少数据
	int pages = StringUtil.getInt(request,"page",1);//当前请求的是第几页
	int pageSize = Integer.parseInt(c);
	PageCtrl pc = null;
	if(pageSize!=-1)
	{
		pc = new PageCtrl();
		pc.setPageSize(pageSize);
		pc.setPageNo(pages);
	}
	
	DBRow repair_order = repairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
	
	
	if(oldpages != pages||pages==1)
	{
		DBRow[] details = repairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id,pc,sidx,sord,filterBean);
		DBRow sum = repairOrderMgrZyj.getSumRepairFreightCost(Long.toString(repair_order_id));
		double sum_price = sum.get("sum_price", 0d);
		double sum_weight = 0;
		for(int j = 0;j<details.length; j++)
		{
			double count = 0;
			
			int status = repair_order.get("repair_status",0);
			
			switch (status) 
			{
				case 4:
						count = details[j].get("repair_reap_count",0d);
					break;
				default:
						count = details[j].get("repair_total_count",0d);
					break;
					
			}
			sum_weight += details[j].get("weight", 0d)*count;
		}
				
		for(int i=0; i<details.length; i++)
		{
			double count = 0;
			int status = repair_order.get("repair_status",0);
			switch (status) 
			{
				case 4:
						count = details[i].get("repair_reap_count",0d);
					break;
				default:
						count = details[i].get("repair_total_count",0d);
					break;
			}
			
			details[i].add("freight_cost",sum_price * (details[i].get("weight", 0d)*count/sum_weight)/count);
		 	StringBuffer html =  new StringBuffer();
		 	html.append("<span style='cursor: pointer;'>");
			// 显示出来商品文件的个数 和 商品标签的个数
		 	// 读取配置文件中的配置的数据
			String value = systemConfig.getStringConfigValue("repair_product_file");
	  		String file_with_class = StringUtil.getString(request,"file_with_class");
	  		String[] arraySelected = value.split("\n");
	  		//将arraySelected组成一个List
	  		ArrayList<String> selectedList= new ArrayList<String>();
	  		for(String tempSelect : arraySelected){
	  				if(tempSelect.indexOf("=") != -1){
	  					String[] tempArray = tempSelect.split("=");
	  					String tempHtml = tempArray[1];
	  					selectedList.add(tempHtml);
	  				}
	  		}
	  		//long pc_id , long fileWithId,int file_with_type
		 	Map<Integer,DBRow> productFileMap = transportMgrZr.getProductFileAndTagFile(details[i].get("pc_id",0l),repair_order_id,FileWithTypeKey.REPAIR_PRODUCT_FILE);
		 
		 	for(int indexOfList = 0 , countOfList = selectedList.size() ; indexOfList < countOfList ;indexOfList++ ){
		 		DBRow tempCount = productFileMap.get(indexOfList+1);
		 		int tempCountNumber = null != tempCount?tempCount.get("count",0):0;
 				html.append(selectedList.get(indexOfList).trim());
 				html.append(":");
 				html.append(tempCountNumber);
 				if(1 == indexOfList%2)
 				{
 					html.append("<br/>");
 				}
 				else
 				{
 					html.append("&nbsp;");
 				}
		 	}
		 	html.append("<br />");
		 	// 读取商品标签的个数
		 	String valueTag = systemConfig.getStringConfigValue("repair_tag_types");
		 	String[] arraySelectedTag = valueTag.split("\n");
		 	ArrayList<String> selectedListTag = new ArrayList<String>();
	  		for(String tempSelect : arraySelectedTag){
	  				if(tempSelect.indexOf("=") != -1){
	  					String[] tempArray = tempSelect.split("=");
	  					String tempHtml = tempArray[1];
	  					selectedListTag.add(tempHtml);
	  				}
	  		}
		 	Map<Integer,DBRow> productTagFileMap = transportMgrZr.getProductFileAndTagFile(details[i].get("pc_id",0l),repair_order_id,FileWithTypeKey.REPAIR_PRODUCT_TAG_FILE);
 		 	for(int indexOfList = 0 , countOfList = selectedListTag.size() ; indexOfList < countOfList ;indexOfList++ ){
		 		DBRow tempCount = productTagFileMap.get(indexOfList+1);
		 		int tempCountNumber = null!=tempCount?tempCount.get("count",0):0;
 				html.append(selectedListTag.get(indexOfList).trim());
 				html.append(":");
 				html.append(tempCountNumber);
 				if(1 == indexOfList%2)
 				{
 					html.append("<br/>");
 				}
 				else
 				{
 					html.append("&nbsp;");
 				}
		 	}
 		 	html.append("</span>");
		 	details[i].add("button",html.toString());
		}
		DBRow data = new DBRow();
		data.add("page",pages);//page，当前是第几页
		
		if(pc!=null)
		{
			data.add("total",pc.getPageCount());//total，总共页数
		}
		else
		{
			data.add("total",1);//total，总共页数
		}
		
		
		data.add("rows",details);//rows，返回数据
		data.add("records",details.length);//records，总记录数

		out.println(new JsonObject(data).toString());
	}
%>