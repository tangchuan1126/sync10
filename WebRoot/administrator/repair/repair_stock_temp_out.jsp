<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>

<%
	long repair_order_id = StringUtil.getLong(request,"repair_order_id");
	String inserted = StringUtil.getString(request,"inserted");
	DBRow[] rows = repairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id,null,null,null,null);
	DBRow repairOrder = repairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
	long purchase_id = repairOrder.get("purchase_id",0l);
%>
<html>
  <head>
    <title>返修单出库</title>
		<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
		<link href="../comm.css" rel="stylesheet" type="text/css">
		<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type='text/javascript' src='../js/jquery.form.js'></script>
		<script type="text/javascript" src="../js/select.js"></script>
		<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
		<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
		
		<%-- Frank ****************** --%>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
		<%-- ***********************  --%>
		
		<%-- Frank ****************** --%>
		<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
		<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
		<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
		<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
		<%-- ***********************  --%>
		

		<script type="text/javascript">
			$().ready(function() {	
				<%-- Frank ****************** --%>
				addAutoComplete($("#product_name"),
						"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
						"merge_info",
						"p_name");
				<%-- ***********************  --%>	
			});

			function closeWindow(){
				$.artDialog.close();
			}

			function deleteRow(input)
			{  
		          var s=input.parentNode.parentNode.rowIndex;
		          document.getElementById("tables").deleteRow(s); 
		    }

		    function addRow() 
		    {
			    if($('#product_name').val()=="") {
				    alert('请输入商品!');
				    return;
			    }
			    if($('#stockOut_count').val()==""||$('#stockIn_count').val()=="0") {
				    alert('请输入实到数量!');
				    return;
			    }
			    //判断返修单详细中是否存在此商品，如果存在不让添加
			    $.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/GetRepairDetailsByRepairProductNameAction.action',
					data:'repair_order_id=<%=repair_order_id%>&product_name='+$('#product_name').val(),
					dataType:'json',
					type:'post',
					success:function(data){
						if(data && data.flag == "product_not_exist")
						{
							showMessage("此商品不存在","error");
						}
						else if(data && data.flag == "not_exist"){
							var row = document.getElementById("tables").insertRow(document.getElementById("tables").rows.length);
					    	var add1=row.insertCell(0).innerHTML = "<input type='hidden' name='product_name' value='"+document.getElementById("product_name").value+"'/>"+document.getElementById("product_name").value;
							var add2=row.insertCell(1).innerHTML = "0";
							var add3=row.insertCell(2).innerHTML = "<input type='text' name='stockOut_count' value='"+document.getElementById("stockOut_count").value+"'/>";
							var add4=row.insertCell(3).innerHTML = "<input type='button' value='删除' onclick='deleteRow(this)'/>";
							$('#product_name').val('');
							$('#stockOut_count').val('');
						}
						else if(data && data.flag == "exist"){
							showMessage("不能在返修单详细中重复添加此商品","error");
						}
						else
						{
							showMessage("系统错误","error");
						}
					},
					error:function(){
						showMessage("系统错误","error");
					}
				})	

		    }

		    function submitStockTempOut() {
			    var table = document.getElementById("tables");
			    if(document.getElementById("tables").rows.length>0) {
			    	$('#stockOutFrm').submit();
			    }
		    }

		    var inserted = false;
			<%
				if(inserted.equals("1")) {
			%>
					inserted = true;
			<%
				}
			%>
			
		    function init() {
				if(inserted) {
					parent.location.reload();
					closeWindow();
				}
			}

			$(document).ready(function(){
				init();
			});
		</script>
  </head>
  
  <body>
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
	<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">返修单出库</legend>
		<form id="stockOutFrm" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/repairStockTempOutAction.action">
		<input type="hidden" name="backurl" value='<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_stock_temp_out.html?repair_order_id=<%=repair_order_id %>&inserted=1'/>
		<input type="hidden" id="repair_order_id" name="repair_order_id" value="<%=repair_order_id %>">
		<input type="hidden" name="is_need_notify_executer" value="true"/>
		<table id="tables" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td width="40%">商品名称</td>
				<td width="20%">应出数量</td>
				<td width="20%">实出数量</td>
				<td width="20%">&nbsp;</td>
			</tr>
			<%
				for(int i=0; rows!=null && i<rows.length; i++) {
					String repair_p_name = rows[i].getString("repair_p_name");
					String repair_total_count = rows[i].getString("repair_total_count");
			%>
			<tr>
				<td>
					<input type="hidden" name="product_name" value="<%=repair_p_name %>"/>
					<%=repair_p_name %>
				</td>
				<td>
					<%=repair_total_count %>
				</td>
				<td>
					<%
						if(purchase_id==0)
						{
					%>
						<input type="text" name="stockOut_count" value="<%=repair_total_count%>"/>
					<%
						}
						else
						{
					%>
						<input type="hidden" name="stockOut_count" value="<%=repair_total_count%>"/><%=repair_total_count%>
					<%
						}
					%>
					
				</td>
				<td>
					<%
						if(purchase_id==0)
						{
					%>
						<input type="button" value="删除" onclick="deleteRow(this)"/>
					<%
						}
					%>
					
				</td>
			</tr>
			<%
				}
			%>
			</table>
			</form>
			<%
				if(purchase_id==0)
				{
			%>
			<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td width="40%">
					<input type="text" id="product_name" value=""/>
				</td>
				<td width="20%">
					0
				</td>
				<td width="20%">
					<input type="text" id="stockOut_count" value=""/>
				</td>
				<td width="20%"><input type="button" value="添加" onclick="addRow()"/></td>
			</tr>
			</table>
			<%
				}
			%>
	</fieldset>	

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="insert" type="button" class="normal-green-long" onclick="submitStockTempOut()" value="确定" >
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>
</table>
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
  </body>
</html>
