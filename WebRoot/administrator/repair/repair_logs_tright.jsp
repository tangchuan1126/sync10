<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%
	long repair_order_id = StringUtil.getLong(request,"repair_order_id");
	int repair_type = StringUtil.getInt(request,"repair_type");
	TDate tdate = new TDate();
	
	DBRow [] rows;
	
	rows = repairOrderMgrZyj.getRepairLogsByType(repair_order_id,repair_type);
	
	HashMap followuptype = new HashMap();
	//操作日志类型:1表示进度跟进2:表示财务跟进;3修改日志;1货物状态;5运费流程;6进口清关;7出口报关8标签流程;9:单证流程
	followuptype.put(1,"创建记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>返修单日志</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body >
<form name="transfer_form" method="post">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
	<input type="hidden" name="transfer_type"/>
	<input type="hidden" name="money_status"/>
</form>

<form action="" method="post" name="followup_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
</form>
	
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="10%"  class="left-title " style="vertical-align: center;text-align: center;">操作员</th>
        <th width="35%"  class="left-title " style="vertical-align: center;text-align: center;">记录备注</th>
        <th width="13%"  class="right-title " style="vertical-align: center;text-align: center;">记录时间</th>
        <th width="10%"  class="right-title " style="vertical-align: center;text-align: center;">操作类型</th>
        <th width="10%"  class="right-title " style="vertical-align: center;text-align: center;">预计完成</th>
  	</tr>
  	<% 
  		for(int i=0;i<rows.length;i++)
  		{
  	%>
  		<tr align="center" valign="middle">
		    <td height="30"><%=adminMgrLL.getAdminById(rows[i].getString("repairer_id")).getString("employe_name") %></td>
		    <td align="left">
		    	<%
		    		out.print(rows[i].getString("repair_content"));
		    	%>	      
		    </td>
		    <td><%=tdate.getFormateTime(rows[i].getString("repair_date")) %></td>
		    <td><%=followuptype.get(rows[i].get("repair_type",0))%></td>
		    <td><%=  rows[i].getString("time_complete")%>&nbsp;</td>
	  	</tr>
  	<%
  		}
  	%>
 	
</table>
</body>
</html>



