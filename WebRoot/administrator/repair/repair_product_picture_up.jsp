<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.RepairProductFileKey"%>
<%@ include file="../../include.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<jsp:useBean id="repairProductFileKey" class="com.cwc.app.key.RepairProductFileKey"/> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>实物图片上传</title>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
  	<%
		String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
  		String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
  		String fileUpAction= ConfigBean.getStringValue("systenFolder")+"action/administrator/repair/RepairProductPictureUpAction.action";
  		
  		String pc_id = StringUtil.getString(request,"pc_id");
 		DBRow[] productRows = transportMgrZr.getProductByPcIds(pc_id);
  		String pc_name = StringUtil.getString(request,"pc_name");
  		long repair_order_id = StringUtil.getLong(request,"repair_order_id");
  		DBRow repairOrderRow = repairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
  		//读取配置文件中的repair_product_file
  	 
  		String value = systemConfig.getStringConfigValue("repair_product_file");
   		String file_with_class = StringUtil.getString(request,"file_with_class");
  		String[] arraySelected = value.split("\n");
  		//将arraySelected组成一个List
  		ArrayList<String> selectedList= new ArrayList<String>();
  		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
  				String tempStr = arraySelected[index];
  				if(tempStr.indexOf("=") != -1){
  					String[] tempArray = tempStr.split("=");
  					String tempHtml = tempArray[1];
  					selectedList.add(tempHtml);
  				}
  		}
  		// 获取所有的关联图片 然后在页面上分类
  		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
  		DBRow[] imagesRows = transportMgrZr.getAllProductFileByPcId(pc_id,FileWithTypeKey.REPAIR_PRODUCT_FILE,repair_order_id);
  		 
		if(imagesRows != null && imagesRows.length > 0 ){
			for(int index = 0 , count = imagesRows.length ; index < count ; index++ ){
				DBRow tempRow = imagesRows[index];
				String product_file_type = tempRow.getString("product_file_type");
				List<DBRow> tempListRow = imageMap.get(product_file_type);
				if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1)){
					tempListRow = new ArrayList<DBRow>();
				}
				tempListRow.add(tempRow);
				imageMap.put(product_file_type,tempListRow);
		
			}
		}
  		String backurl =  ConfigBean.getStringValue("systenFolder") +"administrator/repair/repair_product_picture_up.html?repair_order_id="+repair_order_id+"&pc_id="+pc_id+"&pc_name="+pc_name ;

  	%>
  	<script type="text/javascript">
  		
  		function submitForm(){
  	  		var file = $("#file");
  	  		if(file.val().length < 1){
  	  	  		showMessage("请选择上传文件","alert");
				return ;
  	  	  	}
			var myform = $("#myform");
			myform.submit();
  	  	}
  	  	function fileWithClassTypeChange(){
			var file_with_className = $("#file_with_className");
			var selectedHtml = $("#file_with_class option:selected").html();
			file_with_className.val(selectedHtml);
	
		 
    	 }
   	 jQuery(function($){
	   	  $("#tabs").tabs({
	   			cache: true,
	   			select: function(event, ui){
					 $("#file_with_class option[value='"+(ui.index+1)+"']").attr("selected",true);
						var file_with_class = $("#file_with_class").val();
						$("#backurl").val("<%= backurl%>" + "&file_with_class="+file_with_class);
				}
	   	 });
	   	if('<%= file_with_class%>'.length > 0){
			 $("#tabs").tabs( "select" , '<%= file_with_class%>' * 1-1 );
			 $("#file_with_class option[value='"+'<%= file_with_class%>'+"']").attr("selected",true);
		}
	   	 fileWithClassTypeChange();
	   	fileWithClassChangeTag();
   	  })
  	function deleteFile(file_id){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'product_file',file_id:file_id,folder:'product',pk:'pf_id'},
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}else{
					showMessage("系统错误,请稍后重试","error");
				}
			},
			error:function(){
				showMessage("系统错误,请稍后重试","error");
			}
		})
	}
   	function downLoad(fileName , tableName , folder){
   		 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ '<%= systemConfig.getStringConfigValue("file_path_product")%>');
   }
    function fileWithClassChangeTag(){
   		var file_with_class = $("#file_with_class").val();
   		$("#tabs").tabs( "select" , file_with_class * 1-1 );
   		 
		$("#backurl").val("<%= backurl%>" + "&file_with_class="+file_with_class);
 
	}

  	//文件上传
  	//做成文件上传后,然后页面刷新提交数据如果是有添加文件
    function uploadFile(_target){
        var targetNode = $("#"+_target);
        var fileNames = $("input[name='file_names']").val();
        var obj  = {
    	     reg:"picture_office",
    	     limitSize:2,
    	     target:_target
    	 }
        var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
    	uri += jQuery.param(obj);
    	 if(fileNames && fileNames.length > 0 ){
    		uri += "&file_names=" + fileNames;
    	}
    	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
        		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
       		 }});
    }
  	//jquery file up 回调函数
    function uploadFileCallBack(fileNames,target){
        if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
            $("input[name='file_names']").val(fileNames);
            var myform = $("#myform");
            var file_with_class = $("#file_with_class").val();
			$("#backurl").val("<%= backurl%>" + "&file_with_class="+file_with_class);
            myform.submit();
    	}
    }
  	</script>
  	<style type="text/css">
  		.zebraTable td {line-height:25px;height:25px;}
		.right-title{line-height:20px;height:20px;}
		ul.ul_p_name{list-style-type:none;margin-left:-44px;}
		ul.ul_p_name li{margin-top:1px;margin-left:3px;line-height:25px;height:25px;padding-left:5px;padding-right:5px;float:left;border:1px solid silver;}
  		ul.fileUL{list-style-type:none;clear:both;}
		ul.fileUL li{line-height:20px;height:20px;padding-left:3px;padding-right:3px;margin-left:2px;float:left;margin-top:1px;border:1px solid silver;}
  	</style>
  </head>
  
  <body onload = "onLoadInitZebraTable()">
   
   	<form id="myform"   method="post" action="<%=fileUpAction %>">
   		<input type="hidden" name="pc_id" value="<%=pc_id %>" />
 		  <input type="hidden" name="backurl" id="backurl" value=""/> 
   		<input type="hidden" name="repair_order_id" value="<%=repair_order_id %>"/>
   		<table>
   		<tr>
   			<td style="width:70px;text-align:right;">
   				关联商品:
   			</td>
   			<td colspan="2" style="text-align:left;">
   				 <%
   				 	if(productRows != null && productRows.length > 0){
   				 		 %>
   					<ul class="ul_p_name" style="float:left;">	
   				 		 <% 
   				 		for(DBRow tempRow : productRows){
   				 		 %>
   				 		 	<li> <%=tempRow.getString("p_name") %></li>
   				 		 <% 
   				 		}
  						 %>
  					</ul>
  						 <% 
   				 	}
   				 %>
   			</td>
   		</tr>
   			<tr>
   				<td style="text-align:right;">文件类型:</td>
   				<td>
   					<select id="file_with_class" name="file_with_class" onchange="fileWithClassTypeChange();fileWithClassChangeTag();">
   					 	<%if(arraySelected != null && arraySelected.length > 0){
 									for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
 									String tempStr = arraySelected[index];
 									String tempValue = "" ;
 									String tempHtml = "" ;
 									 
 									if(tempStr.indexOf("=") != -1){
 										String[] tempArray = tempStr.split("=");
 										tempValue = tempArray[0];
 										tempHtml = tempArray[1];
 									}
 								%>		
 									<option value="<%=tempValue %>"><%=tempHtml %></option>
 
 								<%	} 
 								}
 								%>
   					</select>
   					<input type="button"  class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件" />
   				</td>
   				<td>
   					<input type="hidden" name="file_with_class_name" id="file_with_className"/>
   					<input type="hidden" name="sn" value="R_product"/>
		 			<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.REPAIR_PRODUCT_FILE %>" />
		 			<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_product")%>" />
   					<input type="hidden" name="file_names" />
   				</td>
   			</tr>
   			<tr>
   				<td>&nbsp;</td>
   				<td colspan="2">
   					  图片流程阶段:<span style="color:green;"><%= repairProductFileKey.getStatusById(repairOrderRow.get("product_file",0))%></span>
   				</td>
   			</tr>
   		</table>
   		 <!-- 显示出所有的实物图片 tabs-->
   		 <div id="tabs" style="margin-top:15px;">
		   		 	<ul>
		   		 		<%
					 		if(selectedList != null && selectedList.size() > 0){
					 			for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
					 			%>
					 				<li><a href="#repair_product_<%=index %>"> <%=selectedList.get(index) %></a></li>
					 			<% 	
					 			}
					 		}
				 		%>
		   		 	</ul>
   		 	<%
   		 		for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
   		 				List<DBRow> tempListRows = imageMap.get((index+1)+"");
   		 				
   		 			%>
   		 			 <div id="repair_product_<%= index%>">
   		 			 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
   		 			 		<tr>
			   		 			 <%
			   		 			 	String product_photo_description = systemConfig.getStringConfigValue("product_photo_description");
			   		 			 	if(!"".equals(product_photo_description))
			   		 			 	{
			   		 			 		String[] product_photo_description_arr = product_photo_description.split("\n");
			   		 			 		if(index < product_photo_description_arr.length)
			   		 			 		{
			   		 			 %>
			   	   		 			 	<td colspan="3">注:<%=product_photo_description_arr[index] %></td>
			   	   		 		 <%				
			   		 			 		}
			   		 			 	}
			   		 			 %>
	   		 			 	</tr>
   		 			 			<tr> 
			  						<th width="35%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
			  						<th width="30%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
			  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
			  					</tr>
   		 			
   		 			 	<%
   		 			 		if(tempListRows != null && tempListRows.size() > 0){
   		 			 		 
   		 			 			 for(DBRow row : tempListRows){
   		 			 			%>
   		 			 				<tr>
   		 			 					<td>
   		 			 					<a href='<%= downLoadFileAction%>?file_name=<%=row.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_product")%>'><%=row.getString("file_name") %></a>
   		 			 					</td>
   		 			 					<td>
   		 			 						<%= row.getString("p_name")%> &nbsp;
   		 			 					</td>
   		 			 					<td>
    		 			 				 
   		 			 						<% if(!(repairOrderRow.get("product_file",0) == RepairProductFileKey.FINISH && repairOrderRow.get("product_file_over",0.0d) > 0)){ %>
   		 			 							<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
   		 			 				 		<%}else{out.print("&nbsp;");} %>	
   		 			 				 	 </td>
   		 			 				</tr>	
   		 			 			<% 
   		 			 		 }
   		 			 		}else{
   		 			 			%>
 		 			 			<tr>
 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
 								</tr>
   		 			 			<%
   		 			 		}
   		 			 	%>
   		 			 	 	</table>
   		 			 </div>
   		 			<% 
   		 		}
   		 	%>
   		 	
   		 </div>
   		 

   	</form>	
  </body>
  <script type="text/javascript">
//stateBox 信息提示框
  function showMessage(_content,_state){
  	var o =  {
  		state:_state || "succeed" ,
  		content:_content,
  		corner: true
  	 };
   
  	 var  _self = $("body"),
  	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
  	_self.append(_stateBox);	
  	 
  	if(o.corner){
  		_stateBox.addClass("ui-corner-all");
  	}
  	if(o.state === "succeed"){
  		_stateBox.addClass("ui-stateBox-succeed");
  		setTimeout(removeBox,1500);
  	}else if(o.state === "alert"){
  		_stateBox.addClass("ui-stateBox-alert");
  		setTimeout(removeBox,2000);
  	}else if(o.state === "error"){
  		_stateBox.addClass("ui-stateBox-error");
  		setTimeout(removeBox,2800);
  	}
  	_stateBox.fadeIn("fast");
  	function removeBox(){
  		_stateBox.fadeOut("fast").remove();
   }
  }
  </script>
</html>
