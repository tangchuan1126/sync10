<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>

<%
	String repair_order_id = StringUtil.getString(request,"repair_order_id");
	String inserted = StringUtil.getString(request,"inserted");
	DBRow[] rows = repairOrderMgrZyj.getRepairOrderDetailByRepairId(Long.parseLong(repair_order_id),null,null,null,null);
%>
<html>
  <head>
    <title>返修单处理</title>
		<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
		<link href="../comm.css" rel="stylesheet" type="text/css">
		<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type='text/javascript' src='../js/jquery.form.js'></script>
		<script type="text/javascript" src="../js/select.js"></script>
		<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
		<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
		
		<%-- Frank ****************** --%>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
		<%-- ***********************  --%>
		
		<%-- Frank ****************** --%>
		<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
		<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
		<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
		<%-- ***********************  --%>
		<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
		

		<script type="text/javascript">
			$().ready(function() {	
				<%-- Frank ****************** --%>
				addAutoComplete($("#product_name"),
						"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
						"merge_info",
						"p_name");
				<%-- ***********************  --%>	
			});

			function closeWindow(){
				$.artDialog.close();
			}

			function deleteRow(input){  
		          var s=input.parentNode.parentNode.rowIndex;
		          document.getElementById("tables").deleteRow(s); 
		          //var num=document.getElementById("tables").rows.length;  
		         
		    }

		   /*function addRow() {
			    if($('#product_name').val()=="") {
				    alert('请输入商品!');
				    return;
			    }
			    if($('#stockIn_count').val()==""||$('#stockIn_count').val()=="0") {
				    alert('请输入实到数量!');
				    return;
			    }
			    
		    	var row = document.getElementById("tables").insertRow(document.getElementById("tables").rows.length);
		    	var add1=row.insertCell(0).innerHTML = "<input type='hidden' name='product_name' value='"+document.getElementById("product_name").value+"'/>"+document.getElementById("product_name").value;
				var add2=row.insertCell(1).innerHTML = "0";
				var add3=row.insertCell(2).innerHTML = "<input type='text' name='stockIn_count' value='"+document.getElementById("stockIn_count").value+"'/>";
				var add4=row.insertCell(3).innerHTML = "<input type='button' value='删除' onclick='deleteRow(this)'/>";
				$('#product_name').val('');
				$('#stockIn_count').val('');

		    }*/
			function volidateRepairNumber(){
				var table = $("#tables tr");
				var isVolidate = true;
				for(var i = 0; i < table.length; i ++){
					var productName		= $("#tables tr:nth-child("+(i+2)+")").find("td:nth-child(1)").text();
					var productArrive	= $("#tables tr:nth-child("+(i+2)+")").find("td:nth-child(2)").text();
					var repairNum		= $("#tables tr:nth-child("+(i+2)+")").find("td:nth-child(3) input[name='repair_count']").val();
					var destroyCount	= $("#tables tr:nth-child("+(i+2)+")").find("td:nth-child(4) input[name='destroy_count']").val();
					var repairOld		= $("#tables tr:nth-child("+(i+2)+")").find("td:nth-child(5) input[name='repair_count_old']").val();
					var destroyOld		= $("#tables tr:nth-child("+(i+2)+")").find("td:nth-child(5) input[name='destroy_count_old']").val();
					if(parseFloat(repairNum) < parseFloat(repairOld))
					{
						alert(productName+" 翻新的数量不能小于之前翻新数");
						isVolidate = false;
						break;
					}
					if(parseFloat(destroyCount) <　parseFloat(destroyOld))
					{
						alert(productName+" 销毁的数量不能小于之前销毁数");
						isVolidate = false;
						break;
					}
					if(parseFloat(productArrive) < (parseFloat(repairNum)+parseFloat(destroyCount)))
					{
						alert(productName+" 翻新数与销毁数总和不能大于实际到货数");
						isVolidate = false;
						break;
					}
				}
				return isVolidate;
			}
		    function submitStockTempIn() {
			    var table = document.getElementById("tables");
			    if(document.getElementById("tables").rows.length>0 && volidateRepairNumber()) {
					$("#stockInFrm").submit();
			    }
		    }
		    function windowClose(){
				$.artDialog && $.artDialog.close();
				$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			};

		    var inserted = false;
			<%
				if(inserted.equals("1")) {
			%>
					inserted = true;
			<%
				}
			%>
			
		    function init() {
				if(inserted) {
					parent.location.reload();
					closeWindow();
				}
			}

			$(document).ready(function(){
				init();
			});
		</script>
  </head>
  
  <body>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
	<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">返修单处理</legend>
		<form id="stockInFrm" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/UpdateHandleRepairProductAction.action">
		<input type="hidden" name="backurl" value='<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_handle_damage_product.html?repair_order_id=<%=repair_order_id %>&inserted=1'/>
		<input type="hidden" id="repair_order_id" name="repair_order_id" value="<%=repair_order_id %>">
		<table id="tables" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td width="35%">商品名称</td>
				<td width="15%">实到数量</td>
				<td width="20%">已翻新数量</td>
				<td width="20%">已销毁数量</td>
				<td width="10%">&nbsp;</td>
			</tr>
			<%
				for(int i=0; rows!=null && i<rows.length; i++) {
					String repair_p_name = rows[i].getString("repair_p_name");
					String repair_reap_count = rows[i].getString("repair_reap_count");
					//repair_send_count
			%>
			<tr>
				<td><%=repair_p_name %></td>
				<td><%=repair_reap_count %></td>
				<td>
					<input type="text" name="repair_count" size="10px" value='<%=rows[i].get("repair_count",0.0) %>'/>
				</td>
				<td>
					<input type="text" name="destroy_count" size="10px" value='<%=rows[i].get("destroy_count",0.0) %>'/>
				</td>
				<td>
<%--					<input type="button" value="删除" onclick="deleteRow(this)"/>--%>
					<input type="hidden" name="repair_detail_id" value='<%=rows[i].get("repair_detail_id",0L) %>'/>
					<input type="hidden" name="repair_pc_id" value='<%=rows[i].get("repair_pc_id",0L) %>'/>
					<input type="hidden" name="product_name" value="<%=repair_p_name %>"/>
					<input type="hidden" name="repair_count_old" value='<%=rows[i].get("repair_count",0.0) %>'/>
					<input type="hidden" name="destroy_count_old" value='<%=rows[i].get("destroy_count",0.0) %>'/>
				</td>
			</tr>
			<%
				}
			%>
			</table>
			</form>
	</fieldset>	

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="insert" type="button" class="normal-green-long" onclick="submitStockTempIn()" value="确定" >
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
      
    </td>
  </tr>
</table>
  </body>
  <script type="text/javascript">
	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
  </script>
</html>
