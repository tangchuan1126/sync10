<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.RepairOrderKey"%>
<%@page import="com.cwc.app.key.ProductStorageTypeKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.RepairProductFileKey"%>
<%@page import="com.cwc.app.key.RepairQualityInspectionKey"%>
<%@page import="com.cwc.app.key.RepairStockInSetKey"%>
<%@page import="com.cwc.app.key.RepairTagKey"%>
<%@page import="com.cwc.app.key.RepairDeclarationKey"%>
<%@page import="com.cwc.app.key.RepairClearanceKey"%>
<%@page import="com.cwc.app.key.RepairCertificateKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="repairClearanceKey" class="com.cwc.app.key.RepairClearanceKey"/>
<jsp:useBean id="repairDeclarationKey" class="com.cwc.app.key.RepairDeclarationKey"/>
<jsp:useBean id="fileWithTypeKey" class="com.cwc.app.key.FileWithTypeKey"/>
<jsp:useBean id="repairCertificateKey" class="com.cwc.app.key.RepairCertificateKey"/>
<jsp:useBean id="repairTagKey" class="com.cwc.app.key.RepairTagKey"/>
 <jsp:useBean id="repairStockInSetKey" class="com.cwc.app.key.RepairStockInSetKey"/>
<jsp:useBean id="repairQualityInspectionKey" class="com.cwc.app.key.RepairQualityInspectionKey"/>
<jsp:useBean id="repairProductFileKey" class="com.cwc.app.key.RepairProductFileKey"/>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%> 
<%
	String key = StringUtil.getString(request,"key");
	
	long send_psid = StringUtil.getLong(request,"send_psid",0);
	long receive_psid = StringUtil.getLong(request,"receive_psid",0);
	int status = StringUtil.getInt(request,"status",0);
	int declaration = StringUtil.getInt(request,"declarationStatus",0);
	int clearance = StringUtil.getInt(request,"clearanceStatus",0);
	int invoice = StringUtil.getInt(request,"invoiceStatus",0);
	int drawback = StringUtil.getInt(request,"drawbackStatus",0);
	int day = StringUtil.getInt(request,"day",3);
	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	int analysisType = StringUtil.getInt(request,"analysisType",0);
	int analysisStatus = StringUtil.getInt(request,"analysisStatus",0);
	int repair_status = StringUtil.getInt(request,"repair_status",0);
	int stock_in_set = StringUtil.getInt(request,"stock_in_set",0);
	long dept = StringUtil.getLong(request,"dept",0);
	long create_account_id = StringUtil.getLong(request,"create_account_id",0);
	long dept1 = StringUtil.getLong(request,"dept1",0);
	long create_account_id1 = StringUtil.getLong(request,"create_account_id1",0);
	
	long product_line_id = StringUtil.getLong(request,"product_line_id");
	String store_title = StringUtil.getString(request,"store_title");
	String product_line_title = StringUtil.getString(request,"product_line_title");
	
	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(systemConfig.getIntConfigValue("order_page_count"));
	
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
	
	String input_st_date,input_en_date;
	if ( st.equals("") )
	{	
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		st = input_st_date;
	}
	else
	{	
		input_st_date = st;
	}
									
	if ( en.equals("") )
	{	
		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		en = input_en_date;
	}
	else
	{	
		input_en_date = en;
	}
	
	String cmd = StringUtil.getString(request,"cmd");
	int search_mode = StringUtil.getInt(request,"search_mode");
	
	DBRow[] rows; 
	if(cmd!=null)
	{
		if(cmd.equals("filter"))
		{
			rows = repairOrderMgrZyj.fillterRepairOrder(send_psid,receive_psid,pc,status, declaration, clearance, invoice, drawback,0,stock_in_set, create_account_id);
		}
		else if(cmd.equals("search"))
		{
			rows = repairOrderMgrZyj.searchTransportByNumber(key,search_mode,pc);
		}
		else if(cmd.equals("followup")) 
		{
			rows = repairOrderMgrZyj.fillterRepairOrder(send_psid,receive_psid,pc,repair_status, declaration, clearance, invoice, drawback,day,stock_in_set, create_account_id1);
		}
		else if(cmd.equals("analysis"))
		{
			rows = repairOrderMgrZyj.getAnalysis(st,en,analysisType,analysisStatus,day,pc);
		}
		else if(cmd.equals("ready_delivery"))
		{
			rows = repairOrderMgrZyj.getNeedTrackReadyDelivery(product_line_id,pc);
		}
		else if(cmd.equals("ready_send_ps")||cmd.equals("packing_send_ps")||cmd.equals("tag_send_ps")||cmd.equals("product_file_send_ps")||cmd.equals("quality_inspection_send_ps"))
		{
			rows = repairOrderMgrZyj.getNeedTrackSendRepairOrderByPsid(send_psid,cmd,pc);
		}
		else if(cmd.equals("intransit_receive_ps")||cmd.equals("alreadyRecive_receive_ps"))
		{
			rows = repairOrderMgrZyj.getNeedTrackReceiveRepairOrderByPsid(receive_psid,cmd,pc);
		}
		else
		{
			rows = repairOrderMgrZyj.fillterRepairOrder(0,0,pc,0,0,0,0,0,0,0,0);
		}
	}
	else
	{
		rows = repairOrderMgrZyj.getAllDeliveryOrder(pc);
	}
	
	RepairOrderKey repairOrderKey = new RepairOrderKey();
	DBRow[] accounts = repairOrderMgrZyj.getAccountView();
	DBRow[] accountCategorys = repairOrderMgrZyj.getAllAssetsCategory();
	DBRow[] adminGroups = repairOrderMgrZyj.getAllAdminGroup();
	DBRow[] productLineDefines = repairOrderMgrZyj.getAllProductLineDefine();
	HashMap followuptype = new HashMap();
	//操作日志类型:1表示进度跟进2:表示财务跟进;3修改日志;1货物状态;5运费流程;6进口清关;7出口报关8标签流程;9:单证流程
	followuptype.put(1,"创建记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	followuptype.put(13,"到货通知仓库");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>返修单处理</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;border: 2px solid #993300;} 
p{text-align:left;}
tr.split td input{margin-top:2px;}
ul.processUl{list-style-type:none;}
ul.processUl li {line-height:20px;height:20px;border-bottom:1px dashed  silver;clear:both;}
ul.processUl li span.right{dispaly:block;float:right;margin-right:3px;width:57px;text-align:left;}
ul.processUl li span.left{dispaly:block;float:left;}
-->
</style>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
 

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
 
 

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />


<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script src="../js/zebra/zebra.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<style>

a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
span.stateName{width:15%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
.zebraTable td { border-bottom: 0px solid #DDDDDD;padding-left: 10px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
</style>
<script>
	function search()
	{
		var val = $("#search_key").val();
				
		if(val.trim()=="")
		{
			alert("请输入要查询的关键字");
		}
		else
		{
			var val_search = "\'"+val.toLowerCase()+"\'";
			$("#search_key").val(val_search);
			document.search_form.key.value = val_search;
			document.search_form.search_mode.value = 1;
			document.search_form.submit();
		}
	}
	
	function searchRightButton()
	{
		var val = $("#search_key").val();
				
		if (val=="")
		{
			alert("你好像忘记填写关键词了？");
		}
		else
		{
			val = val.replace(/\'/g,'');
			$("#search_key").val(val);
			document.search_form.key.value = val;
			document.search_form.search_mode.value = 2;
			document.search_form.submit();
		}
	}
	
	function eso_button_even()
	{
		document.getElementById("eso_search").oncontextmenu=function(event) 
		{  
			if (document.all) window.event.returnValue = false;// for IE  
			else event.preventDefault();  
		};  
			
		document.getElementById("eso_search").onmouseup=function(oEvent) 
		{  
			if (!oEvent) oEvent=window.event;  
			if (oEvent.button==2) 
			{  
			   searchRightButton();
			}  
		}  
	}
	
	function promptCheck(v,m,f)
	{
		if (v=="y")
		{
			 if(f.content == "")
			 {
					alert("请填写适当备注");
					return false;
			 }
			 
			 else
			 {
			 	if(f.content.length>300)
			 	{
			 		alert("内容太多，请简略些");
					return false;
			 	}
			 }
			 
			 return true;
		}
	}

	function changeType(obj) {
		$('#invoice').attr('style','display:none');
		$('#declaration').attr('style','display:none');
		$('#clearance').attr('style','display:none');
		$('#drawback').attr('style','display:none');
		if($(obj).val()==1) {
			$('#clearance').attr('style','');
		}else if($(obj).val()==2) {
			$('#declaration').attr('style','');
		}else if($(obj).val()==3) {
			$('#invoice').attr('style','');
		}else if($(obj).val()==4) {
			$('#drawback').attr('style','');
		}
	}

	function followup(repair_order_id){
 	 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_follow_up.html"; 
 			uri += "?repair_order_id="+repair_order_id;
	 		$.artDialog.open(uri , {title: "返修单跟进["+repair_order_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function goFundsTransferListPage(id)
	{
	 window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;      
	}
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}
	function goFundsTransferListPage(id)
	{
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id;      
	}
	function goApplyFunds(id)
	{
		var id="'F"+id+"'";
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=searchby_index&search_mode=1&apply_id="+id);       
	}
	$(document).ready(function(){
		//alert('');
		getLevelSelect(0, nameArray, widthArray, centerAccounts, null,vname,'<%=dept%>');
		getLevelSelect(1, nameArray, widthArray, centerAccounts, $("#dept"),vname,'<%=create_account_id%>');
		getLevelSelect(0, nameArray1, widthArray1, centerAccounts, null,vname1,'<%=dept1%>');
		getLevelSelect(1, nameArray1, widthArray1, centerAccounts, $("#dept1"),vname1,'<%=create_account_id1%>');
	});
	<%//level_id,value,name
		String str = "var centerAccounts = new Array(";
		str += "new Array('01',0,'选择部门')";
		str += ",new Array('01.0',0,'选择职员')";
		for(int i=0;i<adminGroups.length;i++) {//部门
			DBRow adminGroup = adminGroups[i];
			str += ",new Array('0"+(i+2)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
			str += ",new Array('0"+(i+2)+".0',0,'选择职员')";
			for(int ii=0;ii<accounts.length;ii++) {
				DBRow account = accounts[ii];
				if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
					str += ",new Array('0"+(i+2)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
			}
		}
		str+= ");";
		out.println(str);
	%>
	var nameArray = new Array('dept','create_account_id');
	var widthArray = new Array(120,120);
	var vname = new Array('nameArray','widthArray','centerAccounts','vname');
	var nameArray1 = new Array('dept1','create_account_id1');
	var widthArray1 = new Array(120,120);
	var vname1 = new Array('nameArray1','widthArray1','centerAccounts','vname1');
	function getLevelSelect(level,nameArray, widthArray, selectArray,o,vnames,value) {
			if(level<nameArray.length) {
				var name = nameArray[level];
				var width = widthArray[level];
				var levelId = o==null?"":$("option:selected",o).attr('levelId');
				var onchangeStr = "";
				if(level==nameArray.length-1)
					onchangeStr = "";
				else
					onchangeStr = "getLevelSelect("+(level+1)+","+vnames[0]+","+vnames[1]+","+vnames[2]+",this,"+vnames[3]+")";
				var selectHtml = "&nbsp;&nbsp;<select name='"+name+"' id='"+name+"' style='width:"+width+"px;' onchange="+onchangeStr+"> ";
				//alert(selectArray);
				for(var i=0;i<selectArray.length;i++) {
					if(levelId!="") {
						var levelIdChange = selectArray[i][0].replace(levelId+".");
						var levelIds = levelIdChange.split(".");	
						//alert(levelIdChange);
						if(levelIdChange!=selectArray[i][0]&&levelIds.length==1){
							//alert(levelId+",value="+selectArray[i][0]+",change='"+levelIdChange+"',"+levelIds.length);
							selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
						}
					}
					else {
						var levelIdChange = selectArray[i][0];
						//alert(levelIdChange);
						var levelIds = levelIdChange.split(".");
						if(levelIds.length==1){
							//alert(levelId+","+selectArray[i][0]+levelId1);
							selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
						}
					}
				}
				selectHtml += "</select>";
				$('#'+name+'_div').html('');
				$('#'+name+'_div').append(selectHtml);
				//alert(selectHtml);
				$('#'+name).val(value);
				getLevelSelect(level+1,nameArray,widthArray,centerAccounts,$('#'+name),vnames);
			}
	}
	jQuery(function($){
 
		addAutoComplete($("#search_key"),
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/repair/GetSearchRepairJSONAction.action",
				"merge_field","repair_order_id");
	})
	
	function refreshWindow(){
		go($("#jump_p2").val());
	}
	function repair_certificate(repair_order_id,file_with_type){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_certificate.html"; 
		uri += "?repair_order_id="+repair_order_id + "&file_with_type="+file_with_type;
		$.artDialog.open(uri, {title: '单证流程',width:'700px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	function clearanceButton(repair_order_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_clearance.html?repair_order_id="+repair_order_id; 
		$.artDialog.open(uri , {title: "清关流程["+repair_order_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function declarationButton(repair_order_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_declaration.html?repair_order_id="+repair_order_id;
		$.artDialog.open(uri , {title: "报关流程["+repair_order_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function stock_in_set(repair_order_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_stockinset.html?repair_order_id="+repair_order_id;
		$.artDialog.open(uri , {title: "运费流程["+repair_order_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function quality_inspectionKey(repair_order_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_quality_inspection.html?repair_order_id="+repair_order_id;
		$.artDialog.open(uri , {title: "质检流程["+repair_order_id+"]",width:'570px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	}
	function product_file(repair_order_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_product_file.html?repair_order_id="+repair_order_id;
		$.artDialog.open(uri , {title: "实物图片["+repair_order_id+"]",width:'850px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	function tag(repair_order_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_tag.html?repair_order_id="+repair_order_id;
		$.artDialog.open(uri , {title: "制签["+repair_order_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
	
	function repairTrackCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/getRepairTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'first'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg += "<a style='text-decoration:none' href='javascript:trackReadyDeliveryCount()'>工厂交货("+data.track_delivery_count+")</a>&nbsp;&nbsp;";
				mesg += "<a style='text-decoration:none' href='javascript:trackSendCount()'>仓库发货("+data.track_send_count+")</a>&nbsp;&nbsp;";
				mesg += "<a style='text-decoration:none' href='javascript:trackReciveCount()'>仓库收货("+data.track_recive_count+")</a>"
				$("#repair_followup").html(mesg);
			}		
		});
	}
	
	function trackReadyDeliveryCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/getRepairTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'ready_delivery'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a style='text-decoration:none' href='javascript:repairTrackCount()'>产品线:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a style='text-decoration:none' href='javascript:needTrackReadyDelivery("+data[i].product_line_id+")'>"+data[i].product_line_name+"("+data[i].readyneedtrackcountforline+")</a>&nbsp;&nbsp;"
				});
				$("#repair_followup").html(mesg);
			}		
		});
	}
	
	function trackSendCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/getRepairTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'send_store_transport'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a style='text-decoration:none' href='javascript:repairTrackCount()'>仓库:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a style='text-decoration:none' href='javascript:trackSendCountByPsid("+data[i].ps_id+",\""+data[i].title+"\")'>"+data[i].title+"("+data[i].need_track_send_count+")</a>&nbsp;&nbsp;"
				});
				$("#repair_followup").html(mesg);
			}		
		});
	}
	
	function trackSendCountByPsid(ps_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/getRepairTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:
			{
				type:'send_store_transport_ps',
				ps_id:ps_id
			},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a style='text-decoration:none' href='javascript:trackSendCount()'>"+title+"</a>:&nbsp;&nbsp;";
				mesg += "<a style='text-decoration:none' href='javascript:needTrackSendRepair("+data.ps_id+",\"ready_send_ps\",\""+title+"\")'>备货中("+data.need_track_ready_transprot_count+")</a>&nbsp;&nbsp;"
				mesg += "<a style='text-decoration:none' href='javascript:needTrackSendRepair("+data.ps_id+",\"packing_send_ps\",\""+title+"\")'>装箱中("+data.need_track_packing_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendRepair("+data.ps_id+",\"tag_send_ps\",\""+title+"\")'>制签("+data.need_track_tag_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendRepair("+data.ps_id+",\"product_file_send_ps\",\""+title+"\")'>实物图片("+data.need_track_product_file_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendRepair("+data.ps_id+",\"quality_inspection_send_ps\",\""+title+"\")'>质检报告("+data.need_track_quality_inspection_transport_count+")</a>&nbsp;&nbsp;"
				$("#repair_followup").html(mesg);
			}		
		});
	}
	
	function trackReciveCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/getRepairTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'recive_store_transport'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a style='text-decoration:none' href='javascript:repairTrackCount()'>仓库:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a style='text-decoration:none' href='javascript:trackReciveCountByPsid("+data[i].ps_id+",\""+data[i].title+"\")'>"+data[i].title+"("+data[i].need_track_recive_count+")</a>&nbsp;&nbsp;"
				});
				$("#repair_followup").html(mesg);
			}		
		});
	}
	
	function trackReciveCountByPsid(ps_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/getRepairTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:
			{
				type:'recive_store_transport_ps',
				ps_id:ps_id
			},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg += "<a style='text-decoration:none' href='javascript:trackReciveCount()'>"+title+"</a>:&nbsp;&nbsp;";
				mesg += "<a style='text-decoration:none' href='javascript:needTrackReceiveRepair("+data.ps_id+",\"intransit_receive_ps\",\""+title+"\")'>运输中("+data.need_track_intransit_count+")</a>&nbsp;&nbsp;"
				mesg += "<a style='text-decoration:none' href='javascript:needTrackReceiveRepair("+data.ps_id+",\"alreadyRecive_receive_ps\",\""+title+"\")'>已收货("+data.need_track_alreadyrecive_transport_count+")</a>&nbsp;&nbsp;"
				$("#repair_followup").html(mesg);
			}		
		});
	}
	
	function needTrackReadyDelivery(product_line_id)
	{
		document.track_form.product_line_id.value = product_line_id;
		document.track_form.cmd.value = "ready_delivery";
		document.track_form.submit();
	}
	
	function needTrackSendRepair(ps_id,type,title)
	{
		document.track_form.send_psid.value = ps_id;
		document.track_form.cmd.value = type;
		document.track_form.store_title.value = title;
		document.track_form.submit();
	}
	
	function needTrackReceiveRepair(ps_id,type,title)
	{
		document.track_form.receive_psid.value = ps_id;
		document.track_form.cmd.value = type;
		document.track_form.store_title.value = title;
		document.track_form.submit();
	}
	function goodsArriveDelivery(repair_order_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_goods_arrive_delivery.html?repair_order_id='+repair_order_id;
		$.artDialog.open(url, {title: '到货派送['+repair_order_id+"]",width:'500px',height:'200px', lock: true,opacity: 0.3});
	}
	function showStockIn(repair_order_id) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_stock_temp_in.html?repair_order_id='+repair_order_id;
		$.artDialog.open(url, {title: '返修单入库',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function handleDamageProduct(repair_order_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_handle_damage_product.html?repair_order_id='+repair_order_id;
		$.artDialog.open(url, {title: '处理货物',width:'800px',height:'500px', lock: true,opacity: 0.3});
	}
	function showStockOut(repair_order_id) 
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_stock_temp_out.html?repair_order_id='+repair_order_id;
		$.artDialog.open(url, {title: '返修单出库',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br/>
<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#repair_search">常用工具</a></li>
			<li><a href="#repair_filter">高级搜索</a></li>

			<li><a href="#repair_followup">需跟进</a></li>
			<!--
			<li><a href="#repair_analysis">返修单监控</a></li>
			-->
		</ul>
		<div id="repair_search">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
									 <input name="search_key" type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="search_key" onkeydown="if(event.keyCode==13)search()" value="<%=key%>"/>
									</div>
								</td>
								<td width="67">
									 
								</td>
							</tr>
						</table>
						<script>eso_button_even();</script>
				</td>
	              <td width="33%"></td>
	              <td width="24%" align="right" valign="middle">
			  		<a href="javascript:addRepairOrder();"><img src="../imgs/cerate_transport.jpg" width="129" height="51" border="0"/></a>
				  </td>
	            </tr>
	          </table>
		</div>
		
		<div id="repair_filter">
		    <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left">
						<div style="float:left;">
						<select id="send_ps" name="send_ps">
							<option value="0">转运仓库</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("id",0l)%>" <%=send_psid==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
							<%
								}
							%>
						</select>
						&nbsp;&nbsp;
						<select id="receive_ps" name="receive_ps">
							<option value="0">目的仓库</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("id",0l)%>" <%=receive_psid==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
							<%
								}
							%>
						</select>
						&nbsp;&nbsp;
						<select id="repair_status" name="repair_status">
							<option value="0">货物状态</option>
							<option value="<%=RepairOrderKey.NOFINISH%>" <%=repair_status==RepairOrderKey.NOFINISH||repair_status==0?"selected":"" %>><%=repairOrderKey.getRepairOrderStatusById(RepairOrderKey.NOFINISH)%></option>
					 		<option value="<%=RepairOrderKey.READY%>" <%=repair_status==RepairOrderKey.READY?"selected":""%>><%=repairOrderKey.getRepairOrderStatusById(RepairOrderKey.READY)%></option>
							<option value="<%=RepairOrderKey.PACKING%>" <%=repair_status==RepairOrderKey.PACKING?"selected":""%>><%=repairOrderKey.getRepairOrderStatusById(RepairOrderKey.PACKING)%></option>
						 	<option value="<%=RepairOrderKey.INTRANSIT%>" <%=repair_status==RepairOrderKey.INTRANSIT?"selected":""%>><%=repairOrderKey.getRepairOrderStatusById(RepairOrderKey.INTRANSIT)%></option>
  						 	<option value="<%=RepairOrderKey.AlREADYRECEIVE%>" <%=repair_status==RepairOrderKey.AlREADYRECEIVE?"selected":""%>><%=repairOrderKey.getRepairOrderStatusById(RepairOrderKey.AlREADYRECEIVE)%></option>
 							<option value="<%=RepairOrderKey.APPROVEING%>" <%=repair_status==RepairOrderKey.APPROVEING?"selected":""%>><%=repairOrderKey.getRepairOrderStatusById(RepairOrderKey.APPROVEING)%></option>
							<option value="<%=RepairOrderKey.FINISH%>" <%=repair_status==RepairOrderKey.FINISH?"selected":""%>><%=repairOrderKey.getRepairOrderStatusById(RepairOrderKey.FINISH)%></option>
							<option value="<%=RepairOrderKey.PART_REPAIR%>" <%=repair_status==RepairOrderKey.PART_REPAIR?"selected":""%>><%=repairOrderKey.getRepairOrderStatusById(RepairOrderKey.PART_REPAIR)%></option>
							<option value="<%=RepairOrderKey.FINISH_REPAIR%>" <%=repair_status==RepairOrderKey.FINISH_REPAIR?"selected":""%>><%=repairOrderKey.getRepairOrderStatusById(RepairOrderKey.FINISH_REPAIR)%></option>
						</select>
						
						<select id="stock_in_set" name="stock_in_set">
							<option value="0">运费流程</option>
							<%
								ArrayList<String> stockInsetKey = repairStockInSetKey.getStatus();
								for(String s : stockInsetKey){
									%>
									<option value='<%=s %>'><%=  repairStockInSetKey.getStatusById(Integer.parseInt(s+""))%>	</option>
									<%
								}
							%>
						</select>
						</div>
						&nbsp;&nbsp;
						<div style="float:left;" id="dept_div" name="dept_div">
						</div>
						&nbsp;&nbsp;
						<div id='create_account_id_div' name='create_account_id_div' style="float:left;">
						</div>
					</td>
				  </tr>
				  <tr>
				  	<td align="left" nowrap="nowrap" style="font-family: 宋体;font-size: 12px;">
				  <select name="declarationStatus" id="declarationStatus">
				  	<option value="0">出口报关流程</option>
	          		<%	
	          			ArrayList statuses21 = repairDeclarationKey.getStatuses();
	          			for(int i=0;i<statuses21.size();i++) {
	          				int statuse = Integer.parseInt(statuses21.get(i).toString());
	          				String key1 = repairDeclarationKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"' "+(declaration==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
	          	<select name="clearanceStatus" id="clearanceStatus">
	          		<option value="0">进口清关流程</option>
	          		<%	
	          			ArrayList statuses31 = repairClearanceKey.getStatuses();
	          			for(int i=0;i<statuses31.size();i++) {
	          				int statuse = Integer.parseInt(statuses31.get(i).toString());
	          				String key1 = repairClearanceKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"' "+(clearance==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
	          	<select name="invoiceStatus" id="invoiceStatus">
	          		<option value="0">发票流程</option>
	          		<%
	          			ArrayList statuses11 = invoiceKey.getInvoices();
	          			for(int i=0;i<statuses11.size();i++) {
	          				int statuse = Integer.parseInt(statuses11.get(i).toString());
	          				String key1 = invoiceKey.getInvoiceById(statuse);
	          				out.println("<option value='"+statuse+"' "+(invoice==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
	          	<select name="drawbackStatus" id="drawbackStatus">
	          		<option value="0">退税流程</option>
	          		<%	
	          			ArrayList statuses41 = drawbackKey.getDrawbacks();
	          			for(int i=0;i<statuses41.size();i++) {
	          				int statuse = Integer.parseInt(statuses41.get(i).toString());
	          				String key1 = drawbackKey.getDrawbackById(statuse);
	          				out.println("<option value='"+statuse+"' "+(drawback==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
						<input type="button" class="button_long_refresh" value="过滤" onclick="filter()"/></td>
				</tr>
			</table>
		</div>
						
	 	<div id="repair_followup">
	 		
	 	</div>
	</div>
</div>
<script>
	$("#st").date_input();
	$("#en").date_input();
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		show:function(event,ui)
			 {
			 	if(ui.index==2)
			 	{
			 		<%
			 			if(cmd.equals("ready_delivery")||cmd.equals("tag_delivery")||cmd.equals("quality_inspection_delivery")||cmd.equals("product_file_delivery"))
			 			{
			 		%>
			 			needTrackDeliveryProductLine(<%=product_line_id%>,"<%=product_line_title%>","<%=cmd%>");
			 		<%
			 			}
			 			else if(cmd.equals("ready_send_ps")||cmd.equals("packing_send_ps"))
			 			{
			 		%>
			 			trackSendCountByPsid(<%=send_psid%>,"<%=store_title%>")
			 		<%	
			 			}
			 			else if(cmd.equals("intransit_receive_ps")||cmd.equals("alreadyRecive_receive_ps"))
			 			{
			 		%>
			 			trackReciveCountByPsid(<%=receive_psid%>,"<%=store_title%>")
			 		<%
			 			}
			 			else
			 			{
			 		%>
			 			repairTrackCount();
			 		<%
			 			}
			 		%>
			 	}
			 }
	});
	</script>

	<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true" isBottom="true">
    <tr> 
        <th width="11%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">返修单基本信息</th>
        <th width="13%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">库房信息</th>
        <th width="15%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">运输信息</th>
        <th width="26%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">资金情况</th>
        <th width="20%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">流程信息</th>
        <th width="15%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">跟进</th>
  	</tr>
  	<%
  		for(int i = 0 ;i<rows.length;i++)
  		{
  	%>
  		<tr align="center">
  			<td height="40" nowrap="nowrap">
  				<fieldset class="set">
  				<!-- 如果是交货行返修单那么就是Id显示成蓝色 -->
  					<%
  						String fontColor = rows[i].get("purchase_id",0l) > 0l ? "mediumseagreen;":"#f60;";
  					%>
  					<legend>
	  					<a style="color:<%=fontColor %>" target="_blank" href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_order_detail.html?repair_order_id=<%=rows[i].getString("repair_order_id")%>'>R<%=rows[i].getString("repair_order_id")%></a>
	  					&nbsp;<%=repairOrderKey.getRepairOrderStatusById(rows[i].get("repair_status",0)) %>
  					</legend>
  					<p class="showP" style="clear:both;">
  						 创建人:
  						 <%
  						 	DBRow createAdmin = adminMgr.getDetailAdmin(rows[i].get("create_account_id",0l));
  						 	if(createAdmin!=null)
  						 	{
  						 		out.print(createAdmin.getString("employe_name"));
  						 	} 
  						 %> 
  					</p>
  					<p class="" style="clear:both;">
  						允许装箱:
  						 <%
  						 	DBRow packingAdmin = adminMgr.getDetailAdmin(rows[i].get("packing_account",0l));
  						 	if(packingAdmin!=null)
  						 	{
  						 		out.print(packingAdmin.getString("employe_name"));
  						 	} 
  						 %> 
  					</p>
  					<p class="showP" style="clear:both;">
  						创建时间:<%=tDate.getFormateTime(rows[i].getString("repair_date"))%>
  					</p>
  					<%
  						if(rows[i].get("purchase_id",0l) > 0 ){
  						%>
  							<p>
  								采购单号:P<%=rows[i].getString("purchase_id") %>
  							</p>
  							<p>
  								&nbsp;&nbsp;批次:<%=rows[i].getString("repair_number") %>
  							</p>
  						<% 	
  						}
  					%>
  					<p>
  					   更新时间:<%= tDate.getFormateTime(rows[i].getString("updatedate")) %>
  					</p>
  				</fieldset>
  			</td>
  			<td align="left">
  				<p>
  				提货仓库:
  				<% 
  					int fromPsType = rows[i].get("from_ps_type",0);
  					//如果是供应商的Type那么就需要去查询供应商的名称
 	  				if(fromPsType == ProductStorageTypeKey.SupplierWarehouse){
 	  					DBRow temp = supplierMgrTJH.getDetailSupplier(rows[i].get("send_psid",0l));
 	  					if(temp != null){
 	  						out.println(temp.getString("sup_name"));
 	  					}
 	  				}
 	  				else
 	  				{
 	  					DBRow storageCatalog = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("send_psid",0l));
 	  					if(storageCatalog != null){
 	  						out.print(storageCatalog.getString("title"));
 	  					}
 	  				}
  				%>
  				</p>
  				<p>
  					收货仓库:<%=catalogMgr.getDetailProductStorageCatalogById(rows[i].get("receive_psid",0l)).getString("title")%> 
  				</p>
  				<p>
  					&nbsp;&nbsp;&nbsp;&nbsp;收货人:
  					<%	 
  						long deliveryer_id = rows[i].get("deliveryer_id",0l);
  						if(deliveryer_id != 0l){
  							out.println(adminMgr.getDetailAdmin(deliveryer_id).getString("employe_name"));
  						}
  					%>
  				</p>
  				<p>
  					送至时间:<%
  						String deliveryTime = rows[i].getString("deliveryed_date");
  						 out.print(deliveryTime.length() > 10 ?deliveryTime.substring(0,10): deliveryTime);
  					%>
  				</p>
  				<p>
  					&nbsp;&nbsp;&nbsp;&nbsp;入库人:<%
  						long warehousinger_id = rows[i].get("warehousinger_id",0l);
						if(warehousinger_id != 0l){
							out.println(adminMgr.getDetailAdmin(warehousinger_id).getString("employe_name"));
						}
  					%>
  				</p>
  				<p>
  					入库时间:
  					<%  
  						String warehousingTime = rows[i].getString("warehousing_date");
  						out.print(warehousingTime.length() > 10 ?warehousingTime.substring(0,10): warehousingTime);
  					%>
  				</p>
  			</td>
  			<td align="left">
  				运单号:<%=rows[i].getString("repair_waybill_number")%><br/>
  				货运公司:<%=rows[i].getString("repair_waybill_name")%><br/>
  				承运公司:<%=rows[i].getString("carriers")%><br/>
  				<%
  					long repair_send_country = rows[i].get("repair_send_country",0l);
  					DBRow send_country_row = repairOrderMgrZyj.getCountyById(Long.toString(repair_send_country));
  					long repair_receive_country = rows[i].get("repair_receive_country",0l);
  					DBRow receive_country_row = repairOrderMgrZyj.getCountyById(Long.toString(repair_receive_country));					
  				%>
  				始发国/始发港:<%=send_country_row==null?"无":send_country_row.getString("c_country")%>/<%=rows[i].getString("repair_send_place").equals("")?"无":rows[i].getString("repair_send_place")%><br/>
  				目的国/目的港:<%=receive_country_row==null?"无":receive_country_row.getString("c_country")%>/<%=rows[i].getString("repair_receive_place").equals("")?"无":rows[i].getString("repair_receive_place")%><br/>
  				总体积:<%=repairOrderMgrZyj.getRepairOrderVolume(rows[i].get("repair_order_id",0l))%> cm³<br/>
  				总重量:<%=repairOrderMgrZyj.getRepairWeight(rows[i].get("repair_order_id",0L)) %> Kg<br/>
  				总金额:<%=repairOrderMgrZyj.getRepairSendPrice(rows[i].get("repair_order_id",0L)) %> RMB
 			<%
			if(rows[i].get("stock_in_set",1)==1) {
				out.println("<font color='red'><b>");
			}
			%>
  			</td>
  			<td>
  				<fieldset class="set" style="border-color:green;">
					<legend>运费:<%=repairOrderMgrZyj.getSumRepairFreightCost(rows[i].getString("repair_order_id")).get("sum_price",0d) %>RMB</legend>
  						<%
						if(rows[i].get("stock_in_set",1)==1) {
							out.println("</b></font>");
						}
					%>
					<table style="width:100%" align="left">
					<%
						DBRow[] applyMoneys = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(10015,rows[i].get("repair_order_id",0L),FinanceApplyTypeKey.REPAIR_ORDER);
							//applyMoneyMgrLL.getApplyMoneyByBusiness(rows[i].getString("repair_order_id"),associateTypeId);//
						for(int j=0; j<applyMoneys.length; j++){
							String freightCostStr = "";
							if(!"RMB".equals(applyMoneys[0].getString("currency")))
							{
								freightCostStr = "/"+applyMoneys[j].get("standard_money",0f)+"RMB";
							}
					%>	
							<tr><td align='left'>
							<a href="javascript:void(0)" onClick='goApplyFunds(<%=applyMoneys[j].get("apply_id",0) %>)'>F<%=applyMoneys[j].get("apply_id",0) %></a>
									<%
										List imageListStock = applyMoneyMgrLL.getImageList(applyMoneys[j].getString("apply_id"),"1");
										String stockStatusName = "";
										//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
									 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneys[j].getString("status")) && imageListStock.size() < 1)
									 	{
									 		stockStatusName = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
									 	}else{
									 		stockStatusName = fundStatusKey.getFundStatusKeyNameById(applyMoneys[j].getString("status"));	
									 	}
									 	stockStatusName = "("+stockStatusName+")";
  								 %>
									<%=stockStatusName %>
									<%=applyMoneys[j].get("amount",0f) %><%=applyMoneys[0].getString("currency") %><%=freightCostStr %>
								</td></tr>
  								 <% 
							 			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneys[j].getString("apply_id"));
							 				//applyMoneyMgrLL.getApplyTransferByBusiness(rows[i].getString("repair_order_id"),6);
							 			if(applyTransferRows.length>0) {
							 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
							 					if(applyTransferRows[ii].get("apply_money_id",0) == applyMoneys[j].get("apply_id",0)) {
								  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
								  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
								  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
								  				int moneyStatus = applyTransferRows[ii].get("status",0);
								  				String transferMoneyStandardFreightStr = "";
								  				if(!"RMB".equals(currency))
								  				{
								  					transferMoneyStandardFreightStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
								  				}
								  				if(moneyStatus != 0 )
								  					out.println("<tr><td align='left'><a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardFreightStr+"</td></tr>");
								  				else
								  					out.println("<tr><td align='left'><a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardFreightStr+"</td></tr>");
								  				}	
							 				}
  								 }
  							 }
  						%>
				 	</table>
			 	</fieldset>
  			</td>
  			<td nowrap="nowrap" style="text-align:left;">
  			<!--   对于有的流程 运费,清关,报关有了跟进中的时候才显示 花费多长的时间 -->
  				<!-- 流程的跟进如果是完成+ 已经上传文件的显示绿色。没有上传文件的就是红色。完成了的用粗体显示 -->
  				<ul class="processUl">
  					<li>
  						<span class="left">货物状态:
						<span><%=repairOrderKey.getRepairOrderStatusById(rows[i].get("repair_status",0)) %></span>
						<%
  							 DBRow[] transportPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("repair_order_id")), ModuleKey.REPAIR_ORDER, 4);
  							 if(transportPersons != null && transportPersons.length > 0){
  								 for(DBRow tempUserRow : transportPersons){
  								 %>
  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
  								 <% 
  								 }
  							 }
  						%>
						</span>
						<span class="right" style="">
						<%=rows[i].getString("all_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("repair_date"),"dd"))+"天":rows[i].getString("all_over")+"天完成"%>
						</span>
  					</li>
  					<li>
  						<span class="left">
	  			出口报关:
	  					<%
	  						//计算颜色
	  						String declarationKeyClass = "" ;
  							int declarationInt = rows[i].get("declaration",repairDeclarationKey.NODELARATION);
  							if(declarationInt == repairDeclarationKey.FINISH){
  								declarationKeyClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("repair_order_id",0l),FileWithTypeKey.REPAIR_DECLARATION);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									declarationKeyClass += " fontGreen";
  								}else{
  									declarationKeyClass += " fontRed";
  								}
  							}else if(declarationInt != repairDeclarationKey.NODELARATION ){
  								declarationKeyClass +=  "spanBold spanBlue";
  							}
	  					%>
	  					<span class='<%= declarationKeyClass%>'><%=repairDeclarationKey.getStatusById(rows[i].get("declaration",RepairStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>
  						<%
  							 DBRow[] declarationPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("repair_order_id")), ModuleKey.REPAIR_ORDER, 7);
  							 if(declarationPersons != null && declarationPersons.length > 0){
  								 for(DBRow tempUserRow : declarationPersons){
  								 %>
  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
  								 <% 
  								 }
  							 }
  						%>
  						</span>
  						<span class="right" style="">
 	  			<%
	  						if(rows[i].getString("declaration_over").trim().length() > 0){
	  							out.println(rows[i].getString("declaration_over")+"天完成");
	  						}else{
	  							if(rows[i].get("declaration",RepairDeclarationKey.NODELARATION) != RepairDeclarationKey.NODELARATION){
		  							 String time = transportMgrZr.getProcessSpendTime(rows[i].get("repair_order_id",0l),7,RepairDeclarationKey.DELARATING);
		  							if(time.trim().length() > 0 ){
		  								out.println(df.format(tDate.getDiffDate(time,"dd")));
		  							}
	  							}
	  						}
	  					%>
	  					</span>
  					</li>
  					<li>
  					<span class="left">
	  			进口清关:
  						<%
	  						//计算颜色
	  						String clearanceKeyClass = "" ;
  							int clearanceInt = rows[i].get("clearance",repairClearanceKey.NOCLEARANCE);
  							if( clearanceInt == RepairClearanceKey.FINISH){
  								clearanceKeyClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("repair_order_id",0l),FileWithTypeKey.REPAIR_CLEARANCE);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									clearanceKeyClass += " fontGreen";
  								}else{
  									clearanceKeyClass += " fontRed";
  								}
  							}else if(clearanceInt != RepairClearanceKey.NOCLEARANCE){
  								clearanceKeyClass +=  "spanBold spanBlue";
  							}
	  					%>
			  			<span class="<%= clearanceKeyClass%>"><%=repairClearanceKey.getStatusById(rows[i].get("clearance",RepairClearanceKey.NOCLEARANCE)) %></span>
			  			<%
  							 DBRow[] clearancePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("repair_order_id")), ModuleKey.REPAIR_ORDER, 6);
  							 if(clearancePersons != null && clearancePersons.length > 0){
  								 for(DBRow tempUserRow : clearancePersons){
  								 %>
  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
  								 <% 
  								 }
  							 }
  						%>
			  		</span>
			  		<span class="right">
	  			<%
	  						if(rows[i].getString("clearance_over").trim().length() > 0){
	  							out.println(rows[i].getString("clearance_over")+"天完成");
	  						}else{
	  							if(rows[i].get("clearance",RepairClearanceKey.NOCLEARANCE) != RepairClearanceKey.NOCLEARANCE){
		  							 String time = transportMgrZr.getProcessSpendTime(rows[i].get("repair_order_id",0l),7,RepairClearanceKey.CLEARANCEING);
		  							if(time.trim().length() > 0 ){
		  								out.println(df.format(tDate.getDiffDate(time,"dd"))+"天");
		  							}
	  							}
	  						}
	  		 	%>
			  		 	</span>
  					</li>
  					<li>
  						<span class="left">
  							制签状态:
  							<%
	  						//计算颜色
	  						String tagClass = "" ;
  							int tagInt = rows[i].get("tag",RepairTagKey.NOTAG);
  							if(tagInt == RepairTagKey.FINISH){
  								tagClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("repair_order_id",0l),FileWithTypeKey.REPAIR_TAG);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									tagClass += " fontGreen";
  								}else{
  									tagClass += " fontRed";
  								}
  							}else if(tagInt != RepairTagKey.NOTAG){
  								tagClass += "spanBold spanBlue";
  							}
	  					%>
  							<span class="<%=tagClass %>"><%=repairTagKey.getRepairTagById(rows[i].get("tag",RepairTagKey.NOTAG))%></span>
  							<%
  							 DBRow[] tagPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("repair_order_id")), ModuleKey.REPAIR_ORDER, 8);
  							 if(tagPersons != null && tagPersons.length > 0){
  								 for(DBRow tempUserRow : tagPersons){
  								 %>
  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
  								 <% 
  								 }
  							 }
  						%>
  						</span>
  						<span class="right">	
  							<%=rows[i].get("tag",01)==1?"":(rows[i].getString("tag_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("repair_date"),"dd"))+"天":rows[i].getString("tag_over")+"天完成")%>
  						</span>
  					</li>
  					<li>
  						<span class="left">
  							单证状态:
  							<%
	  						//计算颜色
	  						String certificateClass = "" ;
  							int certificateInt = rows[i].get("certificate",RepairCertificateKey.NOCERTIFICATE);
  							if(certificateInt == RepairCertificateKey.FINISH){
  								certificateClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("repair_order_id",0l),FileWithTypeKey.REPAIR_CERTIFICATE);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									certificateClass += " fontGreen";
  								}else{
  									certificateClass += " fontRed";
  								}
  							}else if(certificateInt != RepairCertificateKey.NOCERTIFICATE){
  								certificateClass += "spanBold spanBlue";
  							}
  							%>
  							<span class="<%=certificateClass %>"><%=repairCertificateKey.getStatusById(rows[i].get("certificate",RepairCertificateKey.NOCERTIFICATE))%></span>
  							<%
	  							 DBRow[] certificatePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("repair_order_id")), ModuleKey.REPAIR_ORDER, 9);
	  							 if(certificatePersons != null && certificatePersons.length > 0){
	  								 for(DBRow tempUserRow : certificatePersons){
	  								 %>
	  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
	  								 <% 
	  								 }
	  							 }
  							%>
  						</span>
  						<span class="right">
  							<%=rows[i].get("certificate",01)==1?"":(rows[i].getString("certificate_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("repair_date"),"dd"))+"天":rows[i].getString("certificate_over")+"天完成")%><br/>
  						</span>
  					</li>
  					<li>
  						<span class="left">	
	  			质检流程:
  							<%
	  						//计算颜色
	  						String qualityInspectionClass = "" ;
  							int qualityInspectionInt = rows[i].get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY);
  							if(qualityInspectionInt == RepairQualityInspectionKey.FINISH){
  								qualityInspectionClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("repair_order_id",0l),FileWithTypeKey.REPAIR_QUALITYINSPECTION);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									qualityInspectionClass += " fontGreen";
  								}else{
  									qualityInspectionClass += " fontRed";
  								}
  							}else if(qualityInspectionInt != RepairQualityInspectionKey.NO_NEED_QUALITY){
  								qualityInspectionClass += "spanBold spanBlue";
  							}
  							%>
  							<span class="<%=qualityInspectionClass %>"><%=repairQualityInspectionKey.getStatusById(rows[i].get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY)+"") %></span>
  							<%
	  							 DBRow[] qualityInspectionPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("repair_order_id")), ModuleKey.REPAIR_ORDER, 11);
	  							 if(qualityInspectionPersons != null && qualityInspectionPersons.length > 0){
	  								 for(DBRow tempUserRow : qualityInspectionPersons){
	  								 %>
	  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
	  								 <% 
	  								 }
	  							 }
  							%>
  						</span>
  						<span class="right">
	  			<%=rows[i].get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY) == RepairQualityInspectionKey.NO_NEED_QUALITY?"":(rows[i].getString("quality_inspection_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("repair_date"),"dd"))+"天":rows[i].getString("quality_inspection_over")+"天完成")%>
  						</span>
  					</li>
  					<li>
  						<span class="left">
  							运费流程:
  							<%
	  						//计算颜色
	  						String stockInSetClass = "" ;
  							int stockInSetInt = rows[i].get("stock_in_set",RepairStockInSetKey.SHIPPINGFEE_NOTSET);
  							if(stockInSetInt == RepairStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH){
  								stockInSetClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("repair_order_id",0l),FileWithTypeKey.REPAIR_STOCKINSET);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									stockInSetClass += " fontGreen";
  								}else{
  									stockInSetClass += " fontRed";
  								}
  							}else if(stockInSetInt != RepairStockInSetKey.SHIPPINGFEE_NOTSET){
  								stockInSetClass += "spanBold spanBlue";
  							}
  							%>
  							<span class='<%=stockInSetClass %>'><%=repairStockInSetKey.getStatusById(rows[i].get("stock_in_set",RepairStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>
	  						<%
	  							 DBRow[] stockInSetPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("repair_order_id")), ModuleKey.REPAIR_ORDER, 5);
	  							 if(stockInSetPersons != null && stockInSetPersons.length > 0){
	  								 for(DBRow tempUserRow : stockInSetPersons){
	  								 %>
	  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
	  								 <% 
	  								 }
	  							 }
  							%>
	  					</span>
	  					<span class="right">
	  					<%
	  						if(rows[i].getString("stock_in_set_over").trim().length() > 0){
	  							out.println(rows[i].getString("stock_in_set_over")+"天");
	  						}else{
	  							if(rows[i].get("stock_in_set",RepairStockInSetKey.SHIPPINGFEE_NOTSET) != RepairStockInSetKey.SHIPPINGFEE_NOTSET){
		  							 String time = transportMgrZr.getProcessSpendTime(rows[i].get("repair_order_id",0l),5,repairStockInSetKey.SHIPPINGFEE_SET);
		  							if(time.trim().length() > 0 ){
		  								out.println(df.format(tDate.getDiffDate(time,"dd"))+"天完成");
		  							}
	  							}
	  						}
	  					%>
	  					</span>
  					</li>
  					<li>
  						<span class="left">
 	  						实物图片:
 	  						<%
	  						//计算颜色
	  						String productFileClass = "" ;
  							int productFileInt = rows[i].get("product_file",RepairProductFileKey.NOPRODUCTFILE);
  							if(productFileInt == RepairProductFileKey.FINISH){
  								productFileClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("product_file",rows[i].get("repair_order_id",0l),FileWithTypeKey.REPAIR_PRODUCT_FILE);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									productFileClass += " fontGreen";
  								}else{
  									productFileClass += " fontRed";
  								}
  							}else if(productFileInt != RepairProductFileKey.NOPRODUCTFILE){
  								productFileClass += "spanBold spanBlue";
  							}
  							%>
 	  						<span class="<%=productFileClass %>"><%=repairProductFileKey.getStatusById(rows[i].get("product_file",RepairProductFileKey.NOPRODUCTFILE)) %></span>
	  						<%
	  							 DBRow[] productFilePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("repair_order_id")), ModuleKey.REPAIR_ORDER, 10);
	  							 if(productFilePersons != null && productFilePersons.length > 0){
	  								 for(DBRow tempUserRow : productFilePersons){
	  								 %>
	  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
	  								 <% 
	  								 }
	  							 }
  							%>
	  					</span>
	  					<span class="right">
	  			<%=rows[i].get("product_file",RepairProductFileKey.NOPRODUCTFILE) == RepairProductFileKey.NOPRODUCTFILE?"":(rows[i].getString("product_file_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("repair_date"),"dd"))+"天":rows[i].getString("product_file_over")+"天完成")%>
  						</span>
  					</li>
  				</ul>
		  	</td>
		 
  			 
  			<td align="left">
  				<%
  					DBRow[] repairLogsRow = repairOrderMgrZyj.getRepairOrderLogs(rows[i].get("repair_order_id",0l),4);
  					if(repairLogsRow.length > 0){
  						int count = repairLogsRow.length >= 4 ? 3:repairLogsRow.length;
  						for(int m = 0; m < count; m ++){
  							DBRow repairLog = repairLogsRow[m];
  				%>			
  							 <div style="font-size:12px;margin-bottom:3px;line-height:15px;word-wrap:break-word;width:90%;border-bottom:1px dashed silver;padding-bottom:3px;padding-top:3px;">
								<span style="font-size:12px;">
							 		<font style="color:#f60;"><%=followuptype.get(repairLog.get("repair_type",0))%></font>:
									<strong><%=adminMgrLL.getAdminById(repairLog.getString("repairer_id")).getString("employe_name") %></strong>
									<span style="color:#999999;font-size:11px;font-farmliy:Verdana"><%= tDate.getFormateTime(repairLog.getString("repair_date")) %></span>
								</span><br />
								<%=repairLog.getString("repair_content") %>
 							  </div>
  				<%			
  						}
  					}
  					if(repairLogsRow.length >= 4){
  				%>		
  					<a href="javascript:void(0)" onclick='repairLogs(<%=rows[i].get("repair_order_id",0l)%>)' style="color:green;">更多</a>
  				<%	
  					}
  				%>
  			</td>
	  	</tr>
	  	<tr class="split">
  			<td colspan="6" style="text-align:right;padding-right:20px;">
  				<%
  					if(rows[i].get("repair_status",0)==RepairOrderKey.READY&&rows[i].get("purchase_id",0l)==0)
  					{
  				%>
  					&nbsp;&nbsp;<input type="button" value="装箱" class="short-short-button" onclick="readyPacking(<%=rows[i].get("repair_order_id",0l)%>)"/>
  				<%
  					}
  				%>
  				<%
  					if(rows[i].get("repair_status",0)==RepairOrderKey.PACKING)
  					{
  				%>
  					<tst:authentication bindAction="com.cwc.app.api.zyj.RepairOrderMgrZyj.rebackRepairDamage">
  					<input type="button" value="停止装箱" class="lonrepairredtext" onclick="reBackRepair(<%=rows[i].get("repair_order_id",0l)%>)"/>
  					</tst:authentication>
  				<%
  					}
  				%>
  				<% 
				if(rows[i].get("repair_status",0)==RepairOrderKey.PACKING||(rows[i].get("purchase_id",0l)!=0l&&rows[i].get("repair_status",0)==RepairOrderKey.READY))
				 	{
			  	%>
 					 <input name="button" type="button" class="long-button" value="返修单出库" onClick="showStockOut(<%=rows[i].get("repair_order_id",0l)%>)"/>
				<%
					}
				%>
					<%
  					if(rows[i].get("repair_status",0)==RepairOrderKey.INTRANSIT)
  					{
  				%>
  					<tst:authentication bindAction="com.cwc.app.api.zyj.RepairOrderMgrZyj.reStorageRepair">
  					&nbsp;&nbsp;<input type="button" value="中止运输" class="long-button-yellow" onclick="reStorageRepair(<%=rows[i].get("repair_order_id",0l)%>)"/> 
  					</tst:authentication>
  				<%
  					}
  				%>
				<%
  					if(RepairOrderKey.INTRANSIT == rows[i].get("repair_status",0))
  					{
  				%>
  				<input type="button" value="到货派送" class="long-button" onclick="goodsArriveDelivery(<%=rows[i].get("repair_order_id",0l)%>)"/>
  				&nbsp;&nbsp;<input type="button" value="到货并入库" class="long-button" onclick="showStockIn(<%=rows[i].get("repair_order_id",0l)%>)"/>
  				<%		
  					}
  				%>
  				<%
  					if(RepairOrderKey.AlREADYRECEIVE == rows[i].get("repair_status",0))
  					{
  				%>
  				&nbsp;&nbsp;<input type="button" value="入库" class="short-short-button" onclick="showStockIn(<%=rows[i].get("repair_order_id",0l)%>)"/>
  				<%		
  					}
  				%>
  				<%
  					if(RepairOrderKey.FINISH == rows[i].get("repair_status",0)||RepairOrderKey.PART_REPAIR == rows[i].get("repair_status",0))
  					{
  				%>
  				&nbsp;&nbsp;<input type="button" value="处理货物" class="long-button" onclick="handleDamageProduct(<%=rows[i].get("repair_order_id",0l)%>)"/>
  				<%		
  					}
  				%>
  				<%
  					if(rows[i].get("repair_status",0)==RepairOrderKey.READY && 0==applyFundsMgrZyj.getApplyMoneyByAssociationIdAndAssociationTypeIdNoCancel(rows[i].get("repair_order_id",0l),FinanceApplyTypeKey.REPAIR_ORDER).length)
  					{
  				%>
					&nbsp;&nbsp;<input type="button" value="删除" class="short-short-button-del" onclick="delRepair(<%=rows[i].get("repair_order_id",0l)%>)"/> 
  				<%		
  					}
  				%>
	  	  		<% 
	  	  			if(rows[i].get("product_file",RepairProductFileKey.NOPRODUCTFILE) != RepairProductFileKey.NOPRODUCTFILE)
	  	  			{ 
	  	  		%>
	  	  			&nbsp;&nbsp;<input  type="button" class="long-button" value="实物图片" onclick="product_file(<%=rows[i].get("repair_order_id",0l)%>)"/>
	  	  		<%
	  	  			}
	  	  		%>
  				
  				<%
  				 	if(rows[i].get("declaration",1) != RepairDeclarationKey.NODELARATION ){  				 	//有报关 
  				%>
  				 		&nbsp;&nbsp;<input class="short-short-button-merge" type="button" onclick="declarationButton(<%=rows[i].get("repair_order_id",0l)%>)" value="报关"/> 	
  				<%
  					}
  				%>
	  	  		<%
  				 	if(rows[i].get("clearance",1) != RepairClearanceKey.NOCLEARANCE ){// 有清关
  					%>
  					&nbsp;&nbsp;<input class="short-short-button-merge" type="button" onclick="clearanceButton(<%=rows[i].get("repair_order_id",0l)%>)" value="清关"/> 
  					<% 		
  				  	}
  				 %>
  				  <%if(rows[i].get("stock_in_set",1) != repairStockInSetKey.SHIPPINGFEE_NOTSET){%>
  				 	&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="运费" onclick="stock_in_set(<%=rows[i].get("repair_order_id",0l) %>,<%=FileWithTypeKey.REPAIR_STOCKINSET %>)"/>
  				 <%	 
  					 }
  				 %>
  				 <%if(rows[i].get("quality_inspection",1) != repairQualityInspectionKey.NO_NEED_QUALITY){%>
  				 	&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="质检" onclick="quality_inspectionKey(<%=rows[i].get("repair_order_id",0l) %>,<%=FileWithTypeKey.REPAIR_QUALITYINSPECTION %>)"/>
  				 <%	 
  					 }
  				 %>
  				 <%
  				 	if(rows[i].get("tag",RepairTagKey.NOTAG) != RepairTagKey.NOTAG){
  				 		%>
					&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="制签" onclick="tag(<%=rows[i].get("repair_order_id",0l) %>,<%=FileWithTypeKey.REPAIR_TAG %>)"/>
  				 		<% 
  				 	}
  				 %>
	  	  		<%
	  	  			if(rows[i].get("certificate",1) !=  RepairCertificateKey.NOCERTIFICATE){ //单证不等于 不需要显示
	  	  		%>
	  	  			&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="单证" onclick="repair_certificate(<%=rows[i].get("repair_order_id",0l) %>,<%=FileWithTypeKey.REPAIR_CERTIFICATE %>)"/>
	  	  		<%
	  	  			}
	  	  		%>
	  	  		&nbsp;&nbsp;<input class="short-short-button-merge" type="button" onclick="followup(<%=rows[i].get("repair_order_id",0l)%>)" value="跟进"/>
  			</td>
	  	</tr>
  	<%
  		}
  	%>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>
<form action='<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_order_index.html' method="get" name="search_form">
	<input type="hidden" name="key"/>
	<input type="hidden" name="cmd" value="search"/>
	<input type="hidden" name="search_mode"/>
</form>

<form action='<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_order_index.html' method="post" name="filter_form">
	<input type="hidden" name="send_psid"/>
	<input type="hidden" name="receive_psid"/>
	<input type="hidden" name="status"/>
	<input type="hidden" name="declarationStatus"/>
	<input type="hidden" name="clearanceStatus"/>
	<input type="hidden" name="drawbackStatus"/>
	<input type="hidden" name="invoiceStatus"/>
	<input type="hidden" name="stock_in_set"/>
	<input type="hidden" name="dept"/>
	<input type="hidden" name="create_account_id"/>	
	<input type="hidden" name="cmd" value="filter"/>
</form>

<form action='<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_order_index.html' method="post" name="track_form">
	<input type="hidden" name="send_psid"/>
	<input type="hidden" name="receive_psid"/>
	<input type="hidden" name="product_line_id"/>
	<input type="hidden" name="cmd"/>
	<input type="hidden" name="store_title"/>
	<input type="hidden" name="product_line_title"/>
</form>

<form name="dataForm" method="post">
        <strong>
        <input type="hidden" name="p" />
        <input type="hidden" name="status" value="<%=status%>" />
        <input type="hidden" name="cmd" value="<%=cmd%>" /><input type="hidden" name="number" value="<%=key%>" />
        <input type="hidden" name="receive_psid" value="<%=receive_psid%>"/>
        <input type="hidden" name="send_psid" value="<%=send_psid%>"/>
        <input type="hidden" name="declarationStatus" value="<%=declaration%>"/>
		<input type="hidden" name="clearanceStatus" value="<%=clearance%>"/>
		<input type="hidden" name="drawbackStatus" value="<%=drawback%>"/>
		<input type="hidden" name="invoiceStatus" value="<%=invoice%>"/>
		<input type="hidden" name="st" value="<%=st%>"/>
	    <input type="hidden" name="en" value="<%=en%>"/>
	    <input type="hidden" name="analysisType" value="<%=analysisType%>"/>
	    <input type="hidden" name="analysisStatus" value="<%=analysisStatus%>"/>
	    <input type="hidden" name="day" value="<%=day%>"/>
	    <input type="hidden" name="stock_in_set" value="<%=stock_in_set %>"/>
	    <input type="hidden" name="dept" value="<%=dept %>"/>
		<input type="hidden" name="create_account_id" value="<%=create_account_id %>"/>	
		<input type="hidden" name="dept1" value="<%=dept1 %>"/>
		<input type="hidden" name="create_account_id1" value="<%=create_account_id1 %>"/>	
		<input type="hidden" name="product_line_id" value="<%=product_line_id%>"/>
		<input type="hidden" name="store_title" value="<%=store_title%>"/>
        </strong>
        
		
		
  </form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/creatDeliveryOrder.action" name="add_deliveryOrder_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/administrator_delivery_order_detail.html"/>
</form>

<form action="" method="post" name="followup_form">
	<input type="hidden" name="repair_order_id"/>
	<input type="hidden" name="type" value="1"/>
	<input type="hidden" name="stage" value="1"/>
	<input type="hidden" name="repair_type" value="1"/>
	<input type="hidden" name="repair_content"/>
</form>
<script type="text/javascript">
	function search()
	{
		if($("#search_key").val().length>0)
		{
			document.search_form.key.value = $("#search_key").val();
			document.search_form.submit();
		}
		else
		{
			alert("请输入返修单号");
		}
	}
	
	function filter()
	{
		document.filter_form.send_psid.value = $("#send_ps").getSelectedValue();
		document.filter_form.receive_psid.value = $("#receive_ps").getSelectedValue();
		document.filter_form.status.value = $("#repair_status").getSelectedValue();
		document.filter_form.declarationStatus.value = $("#declarationStatus").getSelectedValue();
		document.filter_form.clearanceStatus.value = $("#clearanceStatus").getSelectedValue();
		document.filter_form.invoiceStatus.value = $("#invoiceStatus").getSelectedValue();
		document.filter_form.drawbackStatus.value = $("#drawbackStatus").getSelectedValue();
		document.filter_form.stock_in_set.value = $("#stock_in_set").getSelectedValue();
		document.filter_form.dept.value = $("#dept").getSelectedValue();
		document.filter_form.create_account_id.value = $("#create_account_id").getSelectedValue();
		   
		document.filter_form.submit();
	}
	
	function delRepair(repair_order_id)
	{
		if(confirm("确定删除返修单R"+repair_order_id+"？"))
		{
			var para = "repair_order_id="+repair_order_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/delRepairOrderActioin.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function closeWin()
	{
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}

	function repairLogs(repair_order_id)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_logs.html?repair_order_id='+repair_order_id;
		$.artDialog.open(uri, {title: '日志 返修单号:'+repair_order_id,width:'970px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function addRepairOrder()
	{
		var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/add_repair_order.html";
		$.artDialog.open(uri, {title: '创建返修单',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function createDeliveryOrder(purchase_id)
	{
		document.add_deliveryOrder_form.purchase_id.value = purchase_id;
		document.add_deliveryOrder_form.submit();	
		tb_remove();
	}	
	
	function readyPacking(repair_order_id)
	{
		$.artDialog.open('<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_storage_show.html?repair_order_id='+repair_order_id, {title: '返修缺货商品:'+repair_order_id,width:'700px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function reBackRepair(repair_order_id)
	{
		if(confirm("确定返修单R"+repair_order_id+"停止装箱？（将按照返修商品回退库存）"))
		{
			var para = "repair_order_id="+repair_order_id+"&is_need_notify_executer=true";
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/reBackRepairAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function reStorageRepair(repair_order_id)
	{
		if(confirm("确定返修单R"+repair_order_id+"中止运输？"))
		{
			var para = "repair_order_id="+repair_order_id+"&is_need_notify_executer=true";
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/reStorageRepairAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
</script>
</body>
</html>



