<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey,com.cwc.app.api.ll.Utils,java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@ include file="../../include.jsp"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	DBRow row =Utils.getDBRowByName("apply_money", request);
	float amount = row.getValue("amount")==null?StringUtil.getFloat(request,"amount",0f):row.get("amount",0f);
	String payee = row.getValue("payee")==null?"":row.getValue("payee").toString();
	String payment_information = row.getValue("payment_information")==null?"":row.getValue("payment_information").toString();
	String remark = row.getValue("remark")==null?"":row.getValue("remark").toString();
	String apply_id = row.getValue("apply_id")==null?"0":row.getValue("apply_id").toString();
	String associationId = StringUtil.getString(request,"associationId");
	long purchase_id	= StringUtil.getLong(request, "purchase_id");

	DBRow[] accounts = repairOrderMgrZyj.getAccountView();
	DBRow[] accountCategorys = repairOrderMgrZyj.getAllAssetsCategory();
	DBRow[] adminGroups = repairOrderMgrZyj.getAllAdminGroup();
	DBRow[] productLineDefines = repairOrderMgrZyj.getAllProductLineDefine();
	
	double applyTransferFreightTotal = StringUtil.getDouble(request, "applyTransferFreightTotal");
	double maxMoney				= MoneyUtil.round((amount - applyTransferFreightTotal),2) ;
    double currencyEx			= Double.parseDouble(systemConfig.getStringConfigValue("USD")) ;
    double canApplyMaxRMBMoney	= MoneyUtil.round(maxMoney + maxMoney * 0.01 * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation")),2);
    double canApplyMaxUSDMoney	= MoneyUtil.round((maxMoney + maxMoney * 0.01 * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))) /currencyEx ,2);
    String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>返修申请运费</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 时间控件 -->
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script>

	$(document).ready(function(){
		getLevelSelect(0, nameArray, widthArray, centerAccounts, null,vname,1);
		getLevelSelect(1, nameArray, widthArray, centerAccounts, $("#center_account_type_id"),vname,0);
		getLevelSelect(2, nameArray, widthArray, centerAccounts, $("#center_account_type1_id"),vname,100019);
		getLevelSelect(0, nameArray1, widthArray1, pays, null,vname1);
		//$('#subject').val(<%=StringUtil.getString(request,"subject")%>);
		var currency = $("select[name='currency']").val();
		var showValue = "" ;
	    if(currency === "USD"){
			showValue = parseFloat('<%= maxMoney/currencyEx%>');
		}else{
		    showValue = parseFloat('<%= maxMoney%>');
		}
		$("#amount").val(decimal(showValue,2));
	});
	function handleAmountByCurrency(){
		var amount = $("#amount").val();
		var currency = $("#currency").val();
		if($.trim(amount).length > 0){
		 	if(currency === "USD"){
		 	  var tempValue = parseFloat(amount) / parseFloat('<%= currencyEx%>');
		 	  $("#amount").val(decimal(tempValue,2));
		 	}else{
		 	   var tempValue = parseFloat(amount) * parseFloat('<%= currencyEx%>');
		  	  $("#amount").val(decimal(tempValue,2));
		 	}
		}
	}
	function decimal(num,v){
		var vv = Math.pow(10,v);
		return Math.round(num*vv)/vv;
	} 
	
<%//level_id,value,name
	String str = "var pays = new Array(";
	str += "new Array('01',0,'任意')";
	str += ",new Array('01.0',0,'任意')";
	str += ",new Array('01.0.0',0,'任意')";
	str += ",new Array('02',1,'公司账户')";
	str += ",new Array('02.0',0,'公司账户')";
	str += ",new Array('02.0.0',0,'选择部门')";
	for(int i=0;i<accounts.length;i++) {
		DBRow account = accounts[i];
		if(account.get("account_parent_id",0)==0 && account.get("account_category_id",0)==1)//部门列表
			str += ",new Array('02.0."+(i+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
	}	
	str += ",new Array('03',2,'职员账户')";
	str += ",new Array('03.0',0,'选择部门')";
	str += ",new Array('03.0.0',0,'选择职员')";
	for(int i=0;i<adminGroups.length;i++) {//部门
		DBRow adminGroup = adminGroups[i];
		str += ",new Array('03."+(i+1)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
		str += ",new Array('03."+(i+1)+".0',0,'选择职员')";
		for(int ii=0;ii<accounts.length;ii++) {
			DBRow account = accounts[ii];
			if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
				str += ",new Array('03."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
		}
	} 
	str += ",new Array('04',3,'供应商')";
	str += ",new Array('04.0',0,'选择生产线')";
	str += ",new Array('04.0.0',0,'选择供应商')";
	for(int i=0;i<productLineDefines.length;i++) {
		DBRow productLineDefine = productLineDefines[i];
		str += ",new Array('04."+(i+1)+"',"+productLineDefine.get("id",0)+",'"+productLineDefine.getString("name")+"')";
		str += ",new Array('04."+(i+1)+".0',0,'选择供应商')";
		for(int ii=0;ii<accounts.length;ii++) {
			DBRow account = accounts[ii];
			if(account.get("account_parent_id",0)==productLineDefine.get("id",0) && account.get("account_category_id",0)==3)//供应商列表
				str += ",new Array('04."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
		}
	}

	str+= ");";
	out.println(str);
%>

<%//level_id,value,name
str = "var centerAccounts = new Array(";
str += "new Array('01',0,'任意')";
str += ",new Array('01.0',0,'任意')";
str += ",new Array('01.0.0',0,'任意')";
str += ",new Array('02',1,'公司账户')";
str += ",new Array('02.0',0,'公司账户')";
str += ",new Array('02.0.0',0,'选择部门')";
for(int i=0;i<accounts.length;i++) {
	DBRow account = accounts[i];
	if(account.get("account_parent_id",0)==0 && account.get("account_category_id",0)==1)//部门列表
		str += ",new Array('02.0."+(i+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
}	
str += ",new Array('03',2,'职员账户')";
str += ",new Array('03.0',0,'选择部门')";
str += ",new Array('03.0.0',0,'选择职员')";
for(int i=0;i<adminGroups.length;i++) {//部门
	DBRow adminGroup = adminGroups[i];
	str += ",new Array('03."+(i+1)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
	str += ",new Array('03."+(i+1)+".0',0,'选择职员')";
	for(int ii=0;ii<accounts.length;ii++) {
		DBRow account = accounts[ii];
		if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
			str += ",new Array('03."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
	}
}
str+= ");";
out.println(str);
%>

var nameArray = new Array('center_account_type_id','center_account_type1_id','center_account_id');
var widthArray = new Array(120,120,200);
var nameArray1 = new Array('payee_type_id','payee_type1_id','payee_id');
var widthArray1 = new Array(120,120,200);
var vname = new Array('nameArray','widthArray','pays','vname');
var vname1 = new Array('nameArray1','widthArray1','pays','vname1');

function getLevelSelect(level,nameArray, widthArray, selectArray,o,vnames,value) {
		if(level<nameArray.length) {
			var name = nameArray[level];
			var width = widthArray[level];
			var levelId = o==null?"":$("option:selected",o).attr('levelId');
			var onchangeStr = "";
			if(level==nameArray.length-1)
				onchangeStr = "exec(this)";
			else
				onchangeStr = "getLevelSelect("+(level+1)+","+vnames[0]+","+vnames[1]+","+vnames[2]+",this,"+vnames[3]+")";

			var selectHtml = "<select name='"+name+"' id='"+name+"' style='width:"+width+"px;' onchange="+onchangeStr+"> ";
			for(var i=0;i<selectArray.length;i++) {
				if(levelId!="") {
					var levelIdChange = selectArray[i][0].replace(levelId+".");
					var levelIds = levelIdChange.split(".");	
					
					if(levelIdChange!=selectArray[i][0]&&levelIds.length==1){
						//alert(levelId+",value="+selectArray[i][0]+",change='"+levelIdChange+"',"+levelIds.length);
						selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
					}
				}
				else {
					var levelIdChange = selectArray[i][0];
					var levelIds = levelIdChange.split(".");
					if(levelIds.length==1){
						//alert(levelId+","+selectArray[i][0]+levelId1);
						selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
					}
				}
				
			}
			selectHtml += "</select>";
			$('#'+name+'_div').html('');
			$('#'+name+'_div').append(selectHtml);
			$('#'+name).val(value);
		    
			$("#"+name).chosen();
			$("#"+name).chosen({no_results_text: "没有该项!"});
	
			getLevelSelect(level+1,nameArray,widthArray,pays,$('#'+name),vnames);

			//修改输入框,破坏结构
			if($(o).attr("id")=="payee_type_id"||$(o).attr("id")=="payee_type1_id")
				$('#payee').val('');
		}
}

function exec(o) {
	if($(o).attr("id")=="payee_id") {
		if($("option:selected",o).val()!=0)
				$('#payee').val($("option:selected",o).attr('displayName'));
		else
				$('#payee').val('');
	}
}

function setSubject(o) {
	if($(o).val()==4) {
		$("#tr1").attr("style","style","display:none");
	}else {
		$("#tr1").attr("style","display:none");
	}
}
function setParentUserShow(ids,names,methodName)
{
	eval(methodName)(ids,names);
}
function setParentUserShow(user_ids , user_names){
	$("#adminUserIds").val(user_ids);
	$("#adminUserNames").val(user_names);
}
function adminUser(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIds").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShow'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
// 回显要求看上去已经是上传了的文件。所以上传是要加上前缀名的sn
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_names']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
	 	//判断是不是有了
	 	if($("a",$("#over_file_td")).length > 0){
			var td = $("#over_file_td");
	
			td.append(lis);
		}else{
			$("#file_up_tr").attr("style","");
			$("#jquery_file_up").append(lis);
		}
	} 
}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+"_" + fileName;
    var  a = "<p class='new'><a href="+uri+">"+showName+"</a>&nbsp;&nbsp;(New)</p>";
    return a ;
  
}
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="apply_money_form" id="apply_money_form" action='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/applyMoneyRepairFreightAction.action' method="post">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">资金申请单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
	        <tr height="29">
	          <td align="right" class="STYLE2">返修单号:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" bgcolor="#eeeeee">
	          	<%="R"+associationId%>
	          </td>
	        </tr>
	        <tr> 
		      <td align="right" class="STYLE2">费用项目:</td>
		      <td>&nbsp;</td>
		      <td>
		      	运费
		      	<input type="hidden" name="subject" id="subject" value="1">
		      	<!-- 
				<select name="subject" id="subject" onchange="setSubject(this)">
					<option value="1">运费</option>
					<option value="2">报关</option>
					<option value="3">清关</option>
					<option value="4">退税</option>
				</select>
				-->
			  </td>
		    </tr>
		    <tr id="tr1" name="tr1" style="display:none"> 
		      <td align="right" class="STYLE2">成本中心:</td>
		      <td>&nbsp;</td>
		      <td>
				<div id='center_account_type_id_div' name='center_account_type_id_div' style="float:left;">
				</div>
				<div id='center_account_type1_id_div' name='center_account_type1_id_div' style="float:left;">
				</div>
				<div id='center_account_id_div' name='center_account_id_div' style="float:left;">
		        </div>
			  </td>
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2">币种:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<select id="currency" name="currency" onchange="handleAmountByCurrency()">
		    		<option value="RMB" <%=row.getString("currency").equals("RMB")?"selected":""%>>RMB</option>
		    		<option value="USD" <%=row.getString("currency").equals("USD")?"selected":""%>>USD</option>
		    	</select>
		      </td>
		    </tr>
		  	<tr> 
		      <td align="right" class="STYLE2">金额:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<%
		      		float apply_money = batchMgrLL.getApplyMoneyFreightCost(6,Long.parseLong(associationId));
		      	%>
		      	<input name="amount" id="amount" type="text" size="15" value="<%=(amount-apply_money)%>">
		      </td>
		    </tr>
			<tr> 
		      <td align="right" class="STYLE2">最迟转款时间:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<input type="text" name="last_time" id="st_date"  value=""  style="border:1px #CCCCCC solid;width:70px;"/>
		      </td>
		    </tr>		  
		    <tr>
		      <td align="right" class="STYLE2">收款人:</td>
		      <td>&nbsp;</td>
		     
		      <td>
		      	<div style="float:left;" id="payee_type_id_div" name="payee_type_id_div">		      	
				</div>
				<div id='payee_type1_id_div' name='payee_type1_id_div' style="float:left;">
				</div>
				<div id='payee_id_div' name='payee_id_div' style="float:left;">
		        </div>
		        <br/><br/>
		      	<input name="payee" id="payee" value="<%=payee%>"/>
		      </td>
		   
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2">收款信息:</td>
		      <td>&nbsp;</td>
		      <td>		      		    		     
		      	<textarea rows="4" cols="40" name="payment_information" id="payment_information" ><%=payment_information%></textarea>
		      </td>
		    </tr>
		    <tr>
			      <td class="STYLE2" align="right">负责人:</td>
			      <td>&nbsp;</td>
			      <td>
				     <input type="text" id="adminUserNames" name="adminUserNames" style="width:180px;" onclick="adminUser()"/>
				     <input type="hidden" id="adminUserIds" name="adminUserIds" value=""/>
				            通知：
			   		<input type="checkbox"  name="isMail" id="isMail"/>邮件
			   		<input type="checkbox"  name="isMessage" id="isMessage" />短信
			   		<input type="checkbox"  name="isPage" id="isPage" />页面
			   		<input type="hidden"  name="isNeedMail" id="isNeedMail"/>
			   		<input type="hidden"  name="isNeedMessage" id="isNeedMessage" />
			   		<input type="hidden"  name="isNeedPage" id="isNeedPage" />
		     	 </td>
		    </tr>			    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="remark" rows="4" cols="40"><%=remark%></textarea></td>
		    </tr>
		    <tr>
		    	<td align="right" class="STYLE2">凭证:</td>
		     	<td>&nbsp;</td>
		    	<td>
		    		<span id="file_up_span">
						<input type="hidden" id="sn" name="sn" value="F_applyMoney"/>
						<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_financial")%>" />
						<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="上传凭证" />
					</span>
		    	</td>
		    </tr>
		    <tr id="file_up_tr" style='display:none;'>
	 		 	<td align="right" class="STYLE2">完成凭证:</td>
	 		 	<td>&nbsp;</td>
	 		 	<td>
	              	<div id="jquery_file_up">	
	              		<input type="hidden" name="file_names" value=""/>
	              	</div>
	 		 	</td>
		 	</tr>
		</table>
		<input id="apply_id" name="apply_id" id="apply_id" type="hidden" value="<%=apply_id%>"/>
		<input id="association_id" name="association_id" id="association_id" type="hidden" value="<%=associationId%>"/>
		<input name="creater_id" id="creater_id" type="hidden" value="<%=adminLoggerBean.getAdid() %>"/>
		<input name="creater" id="creater" type="hidden" value="<%=adminLoggerBean.getEmploye_name() %>"/>
		<input name="create_time" id="create_time" type="hidden" value="<%=(new SimpleDateFormat("yyyy-MM-dd")).format(new java.util.Date()) %>"/>
		<input type="hidden" name="association_type_id" id="association_type_id" value='<%=FinanceApplyTypeKey.REPAIR_ORDER %>' />
		<input type="hidden" name="types" id="types" value="10015" />
	</fieldset>	
	</td>
  </tr>

</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="insert" type="button" class="normal-green-long" onclick="submitApply()" value="<%=(!StringUtil.getString(request,"subject").equals("")?"确定":"下一步")%>">
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
  </form>
<script type="text/javascript">
<!--
	function submitApply()
	{
		if(!checkPayAcount()) {
			return;
		}
	
		if($('#amount').val().trim()==""||$('#amount').val()==null)
		 {
		     $('#amount').focus();
		     alert("金额不能为空！");
		 }
		 else if(isNaN($('#amount').val().trim()))
	 	 {
		    $('#amount').focus();
		     alert("金额必须为数字！");
	 	 }
		 else if($('#amount').val().trim()<=0)
		 {
		 	 $('#amount').focus();
		     alert("金额必须大于零！");
		 }
		 else if($('#payee').val().trim()==""||$('#payee').val()==null)
		 {
		 	 $('#payee').focus();
		 	 alert("请填写收款人！");
		 }
		 else if($("#payment_information").val().trim()==""||$("#payment_information").val()==null)
		 {
		 	 $("#payment_information").focus();
		 	 alert("请填写付款信息！");
		 } 
		 else if($("#payment_information").val().trim().length>600)
		 {
		 	 $("#payment_information").focus();
		 	 alert("付款信息不能超过600字符！");
		 }			 
		 else if($("#remark").val().trim().length>=200)
		 {
		     $("#remark").focus();
		     alert("备注字数不能超过200！");
		 }
		 else if("" == $("#st_date").val() || 0 == $("#st_date").val().trim().length)
		 {
			$("#st_date").focus();
			alert("最迟转款时间不能为空");
		 }
		 else
		 {
		   	if($("input:checkbox[name=isMail]").attr("checked"))
		   	{
				$("#isNeedMail").val(2);
		   	}
		   	if($("input:checkbox[name=isMessage]").attr("checked"))
		   	{
				$("#isNeedMessage").val(2);
		   	}
		   	if($("input:checkbox[name=isPage]").attr("checked"))
		   	{
				$("#isNeedPage").val(2);
		   	}
		   	$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/applyMoneyRepairFreightAction.action',
				data:$("#apply_money_form").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "success"){
						showMessage("申请成功","success");
						setTimeout("windowClose()", 1000);
					}else{
						showMessage("申请失败","error");
					}
				},
				error:function(){
					showMessage("系统错误","error");
				}
			})	
		 }	
	}

	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function checkPayAcount() {
		 var amount = $("#amount").val();
		 var currency = $("#currency").val();
		// 判断金额是不是都正确
		if(currency === "USD" && parseFloat(amount) >  parseFloat ('<%=canApplyMaxUSDMoney%>')){
			 alert("申请金额不正确，最多只能申请"+parseFloat('<%=canApplyMaxUSDMoney%>')+" USD");
			 return false;
		}
		if(currency === "RMB" && parseFloat(amount) > parseFloat('<%=canApplyMaxRMBMoney%>')){
			 alert("申请金额不正确，最多只能申请"+parseFloat('<%=canApplyMaxRMBMoney%>')+" RMB");
			 return false;
		}
		return true;
	}

	function closeWindow(){
		$.artDialog.close();
	}
//-->
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 
	$(".chzn-select").chosen(); 
	$(".chzn-select1").chosen(); 
	$(".chzn-select2").chosen(); 
	$("#st_date").date_input();

	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 	}
	}
</script>
</body>
</html>

