<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>

<%
	String file_name = StringUtil.getString(request,"file_name");
	
	DBRow[] prefillDetails = new DBRow[0];
	
	if(file_name.length()>0)
	{
		prefillDetails = repairOrderMgrZyj.excelshow(file_name);
	} 
%>
<html>
  <head>
    <title>生成交货明细</title>

  </head>
  <body>
  		参考模板:<span class="STYLE12"><a href="../repair/repair_template.xls">下载</a></span>
		<input type="button" class="long-button" onclick="uploadFile('');" value="上传货物列表"/>
		<table  width="90%">
			<tr>
				<td>
					<table width="100%" id="tables">
						<tr>
							<td width="80%" align="center">商品名</td>
							<td width="5%" align="center" nowrap="nowrap">交货数</td>
							<td width="5%" align="center" nowrap="nowrap">箱号</td>
							<td width="5%">&nbsp;</td>
						</tr>
						<%
							for(int i=0;i<prefillDetails.length;i++)
							{
						%>
						<tr>
							<td width="80%" align="left">
								<%=prefillDetails[i].getString("p_name")%>
								<input type="hidden" name="pc_id" value="<%=prefillDetails[i].get("pc_id",0l)%>"/>
							</td>
							<td width="5%"><input style="width:50px;" id="<%=prefillDetails[i].get("pc_id",0l)%>_deliver_count" name="<%=prefillDetails[i].get("pc_id",0l)%>_deliver_count" value="<%=prefillDetails[i].getString("prefill_deliver_count")%>" onchange="countCheck(this)"/></td>
							<td width="5%"><input style="width:50px;" id="<%=prefillDetails[i].get("pc_id",0l)%>_box" name="<%=prefillDetails[i].get("pc_id",0l)%>_box" value="<%=prefillDetails[i].getString("prefill_box")%>"/></td>
							<td width="5%"><input type="button" value="删除" onclick="deleteRow(this)" class="short-short-button-del"/></td>
						</tr>
						<%
							}
						%>
						<tr>
							<td width="5%" align="left"><input style="width:200px;" type="text" name="p_name" id="p_name"/></td>
							<td width="5%"><input type="text" style="width:50px;" id="deliver_count"/></td>
							<td width="5%"><input type="text" style="width:50px;" id="backup_count"/></td>
							<td width="5%"><input type="text" style="width:50px;" id="box"/></td>
							<td width="5%"><input type="button" value="添加" onclick="addRow()" class="short-button"/></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<script type="text/javascript">
			
			function addRow() 
			{
				var p_name = $("#p_name").val();
				var deliver_count = parseFloat($("#deliver_count").val());
			   	var backup_count = parseFloat($("#backup_count").val());
			   	var box = $("#box").val();
			   	
				var pc_id = 0;
				
				if(p_name.trim()=="")
				{
					alert("请输入商品名");
				}
				else if(deliver_count!=$("#deliver_count").val()||deliver_count<0)
				{
					alert("交货数输入有误");
				}
				else if(backup_count!=$("#backup_count").val()||backup_count<0)
				{
					alert("备件数输入有误");
				}
				else
				{
					$.ajax({
						url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductByPnameJSON.action',
						type: 'post',
						async:false,
						dataType: 'json',
						timeout: 60000,
						cache:false,
						data:{p_name:p_name},
						beforeSend:function(request){
							
						},
						
						error: function(e){
							isProductExit = false;
						},
						
						success: function(data)
						{
							if(data.product_id=="0")
							{
								alert("无法识别的商品");
							}
							else
							{
								pc_id = data.product_id;
								
								var all_count = deliver_count+backup_count;
				
								if(all_count>0)
								{
									var row = document.getElementById("tables").insertRow(document.getElementById("tables").rows.length-1);
									row.insertCell(0).innerHTML = "<input type=\"hidden\" name=\"pc_id\" value=\""+pc_id+"\"/>"+$("#p_name").val();
									row.insertCell(1).innerHTML = "<input type=\"text\" style=\"width:50px;\" name=\""+pc_id+"_deliver_count\" value=\""+deliver_count+"\"/>";
									row.insertCell(2).innerHTML = "<input type=\"text\" style=\"width:50px;\" name=\""+pc_id+"_backup_count\" value=\""+backup_count+"\"/>";;
									row.insertCell(3).innerHTML = "<input type=\"text\" style=\"width:50px;\" name=\""+pc_id+"_box\" value=\""+box+"\"/>";;;
									row.insertCell(4).innerHTML = "<input type='button' class=\"short-short-button-del\" onclick=\"deleteRow(this)\" value='删除'/>";
									
									$("#p_name").val("");
									$("#deliver_count").val("");
									$("#backup_count").val("");
									$("#box").val("");
								}
							}
						}
					});
				}	
			}
			
			function deleteRow(input)
			{  
		          var s=input.parentNode.parentNode.rowIndex;
		          document.getElementById("tables").deleteRow(s); 
		    }
		</script>
  </body>
</html>
