<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.ApproveRepairKey"%>
<%@ include file="../../include.jsp"%> 
<%
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

long send_psid		= StringUtil.getLong(request,"send_psid");
long receive_psid	= StringUtil.getLong(request,"receive_psid");

int filter_approve_status	= StringUtil.getInt(request,"filter_approve_status");
String sorttype				= StringUtil.getString(request,"sorttype");

DBRow repairApprove[]	= repairOrderMgrZyj.fillterRepairApprove(send_psid,receive_psid,pc,filter_approve_status,sorttype);
DBRow treeRows[]		= catalogMgr.getProductStorageCatalogTree();
String qx;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>返修发货审核</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
	function filter()
	{
		document.filter_form.send_psid.value = $("#send_ps_id").val();
		document.filter_form.receive_psid.value = $("#receive_ps_id").val();
		document.filter_form.filter_approve_status.value = $("#approve_status").val(); 
		document.filter_form.submit(); 
	}
	
	function sort(sorttype)
	{
		document.filter_form.sorttype.value = sorttype;
		document.filter_form.submit();
	}
</script>
<script type="text/javascript">
	function deleteApprove(id) {
		if(confirm("您确定要取消吗?")) {
			$('#repair_approve_id').val(id);
			$('#delete_form').submit();
		}
	}
</script>
<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购及返修应用 &raquo;返修单申请完成审核</td>
  </tr>
</table>
<br>
<table width="98%">
	<tr>
		<td>
                     接收仓库:
        <select name="receive_ps_id" id="receive_ps_id" style="border:1px #CCCCCC solid" >
		          <option value="0">所有仓库</option>
		         <%
					for ( int i=0; i<treeRows.length; i++ )
					{
						if ( treeRows[i].get("parentid",0) != 0 )
						 {
						 	qx = "├ ";
						 }
						 else
						 {
						 	qx = "";
						 }
						 
						 if (treeRows[i].get("level",0)>1)
						 {
						 	continue;
						 }
				%>
					          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==receive_psid?"selected":""%>> <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%> <%=qx%> <%=treeRows[i].getString("title")%> </option>
				<%
					}
				%>
        </select>
		转运仓库:
		<select name="send_ps_id" id="send_ps_id" style="border:1px #CCCCCC solid" >
	          <option value="0">所有仓库</option>
	         <%
				for ( int i=0; i<treeRows.length; i++ )
				{
					if ( treeRows[i].get("parentid",0) != 0 )
					 {
					 	qx = "├ ";
					 }
					 else
					 {
					 	qx = "";
					 }
					 
					 if (treeRows[i].get("level",0)>1)
					 {
					 	continue;
					 }
			%>
				          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==send_psid?"selected":""%>> <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%> <%=qx%> <%=treeRows[i].getString("title")%> </option>
			<%
				}
			%>
        </select>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<select name="approve_status" id="approve_status" style="border:1px #CCCCCC solid" >
			<option value="-1">全部</option>
			<option value="<%=ApproveRepairKey.WAITAPPROVE%>" <%=filter_approve_status ==ApproveRepairKey.WAITAPPROVE?"selected":"" %>>待审核</option>
			<option value="<%=ApproveRepairKey.APPROVE %>" <%=filter_approve_status ==ApproveRepairKey.APPROVE?"selected":"" %>>已审核</option>
		</select>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" class="button_long_refresh" value="过滤" onClick="filter()"/>
		</td>
	</tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th style="vertical-align: center;text-align: center;" class="left-title">接收仓库</th>
        <th style="vertical-align: center;text-align: center;" class="left-title">转运仓库</th>
		<th width="11%" class="left-title" style="vertical-align: center;text-align: center;">转运单号</th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;"><a href="javascript:sort('subdate')">提交日期<%=sorttype.equals("subdate")?"<img style=\"border: 0px\" src=\"../imgs/arrow_down.png\">":"" %></a></th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;">提交人</th>
        <th width="11%" style="vertical-align: center;text-align: center;" class="right-title" >差异商品数/已审核</th>
        <th width="11%" class="right-title" style="vertical-align: center;text-align: center;" >状态</th>
        <th width="8%" class="right-title" style="vertical-align: center;text-align: center;" >审核人</th>
        <th width="14%" class="right-title" style="vertical-align: center;text-align: center;"><a href="javascript:sort('approvedate')">审核日期<%=sorttype.equals("approvedate")?"<img style=\"border: 0px\" src=\"../imgs/arrow_down.png\">":"" %></a></th>
        <th width="14%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
    </tr>
<%
for (int i=0; i<repairApprove.length; i++)
{
%>
    <tr > 
      <td   width="8%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold'><%=repairApprove[i].getString("receivetitle")%></td>
      <td   width="8%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold'><%=repairApprove[i].getString("sendtitle")%></td>
	  <td   align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%="R"+repairApprove[i].getString("repair_order_id")%></td>
      <td   width="13%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=repairApprove[i].getString("commit_date")%></td>
      <td   width="10%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=repairApprove[i].getString("commit_account")%></td>
      <td   align="center" valign="middle" style="font-size:15px;"><%=repairApprove[i].getString("different_count")%>/<span style="color:#006600;font-weight:normal"><%=repairApprove[i].getString("difference_approve_count")%></span></td>
      <td   align="center" valign="middle" >
	  <%
	  if (repairApprove[i].get("different_count",0)==0)
	  {
	  	out.println("<font color=black><strong>无需审批</strong></font>");
	  }
	  else if ( repairApprove[i].get("approve_status",0)==ApproveRepairKey.WAITAPPROVE)
	  {
	  	out.println("<img src='../imgs/standard_msg_error.gif' width='16' height='16' align='absmiddle'> <font color=red><strong>审核中</strong></font>");
	  }
	  else
	  {
	  	out.println("<img src='../imgs/standard_msg_ok.gif' width='16' height='16' align='absmiddle'> <font color=green><strong>已审核</strong></font>");
	  }
	  %>
	  </td>
      <td   align="center" valign="middle" ><%=repairApprove[i].getString("approve_account")%>&nbsp;</td>
      <td   align="center" valign="middle" >
	  <%
	  if (repairApprove[i].getString("approve_date").trim().equals("1999-01-01 00:00:00.0"))
	  {
	  	out.println("&nbsp;");
	  }
	  else
	  {
	  	out.println(repairApprove[i].getString("approve_date"));
	  }
	  %>&nbsp;</td>
      <td   align="center" valign="middle" >
	  <%
	//System.out.println(repairApprove[i].get("difference_approve_count",0));
	  if (repairApprove[i].get("difference_approve_count",0)==0) {
	  %>
	  <input name="Submit" type="button" class="short-button" value="取消审核" onClick="deleteApprove(<%=repairApprove[i].getString("ta_id") %>)">
	  <%
	  }
	  if (repairApprove[i].get("different_count",0)==0)
	  {
	  	out.println("&nbsp;");
	  }
	  else if (repairApprove[i].get("approve_status",0)==ApproveRepairKey.WAITAPPROVE)
	  {
	  %>
	   <input name="Submit" type="button" class="short-button" value="审核" onClick="location='repair_approve_detail.html?ta_id=<%=repairApprove[i].getString("ta_id")%>&approve_status=<%=repairApprove[i].get("approve_status",0)%>&backurl=<%=StringUtil.getCurrentURL(request)%>'">
	  <%
	  }
	  else
	  {
	  %>
	  <input name="Submit" type="button" class="short-button" value="详细" onClick="location='repair_approve_detail.html?ta_id=<%=repairApprove[i].getString("ta_id")%>&approve_status=<%=repairApprove[i].get("approve_status",0)%>'">
	  <%
	  }
	  %>
      </td>
    </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p"/>
		  <input type="hidden" id="send_psid" name="send_psid" value="<%=send_psid%>"/>
		  <input type="hidden" id="receive_psid" name="receive_psid" value="<%=receive_psid%>"/>
		  <input type="hidden" id="filter_approve_status" name="filter_approve_status" value="<%=filter_approve_status %>"/>
		  <input type="hidden" id="sorttype" name="sorttype" value="<%=sorttype%>"/>

  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<form action="repair_approve.html" name="filter_form" method="post">
	<input type="hidden" id="send_psid" name="send_psid" value="<%=send_psid%>"/>
	<input type="hidden" id="receive_psid" name="receive_psid" value="<%=receive_psid%>"/>
	<input type="hidden" id="filter_approve_status" name="filter_approve_status" value="<%=filter_approve_status %>"/>
	<input type="hidden" id="sorttype" name="sorttype" value="<%=sorttype%>"/>
</form>
<form name="delete_form" id="delete_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/repairApproveCancelAction.action">
	<input type="hidden" name="repair_approve_id" id="repair_approve_id">
	<input type="hidden" name="backurl" id="backurl" value="<%=StringUtil.getCurrentURL(request)%>">
	<input type="hidden" name="is_need_notify_executer" value="true"/>
</form>
<br>
</body>
</html>
