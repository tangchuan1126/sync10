<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.RepairOrderKey"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.RepairClearanceKey"%>
<%@page import="com.cwc.app.key.RepairDeclarationKey"%>
<%@page import="com.cwc.app.key.RepairWayKey"%>
<%@page import="com.cwc.app.key.RepairOrderKey"%>
<%@page import="com.cwc.app.key.RepairStockInSetKey"%>
<%@page import="com.cwc.app.key.RepairProductFileKey"%>
<%@page import="com.cwc.app.key.RepairTagKey"%>
<%@page import="com.cwc.app.key.RepairCertificateKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="com.cwc.app.key.RepairQualityInspectionKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.RepairLogTypeKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="repairClearanceKey" class="com.cwc.app.key.RepairClearanceKey"/>
<jsp:useBean id="repairDeclarationKey" class="com.cwc.app.key.RepairDeclarationKey"/>
<jsp:useBean id="repairWayKey" class="com.cwc.app.key.RepairWayKey"/>
<jsp:useBean id="repairOrderKey" class="com.cwc.app.key.RepairOrderKey"/>
<jsp:useBean id="repairStockInSetKey" class="com.cwc.app.key.RepairStockInSetKey"></jsp:useBean>
<jsp:useBean id="repairProductFileKey" class="com.cwc.app.key.RepairProductFileKey"/> 
<jsp:useBean id="repairTagKey" class="com.cwc.app.key.RepairTagKey"/> 
<jsp:useBean id="repairCertificateKey" class="com.cwc.app.key.RepairCertificateKey"/> 
<jsp:useBean id="financeApplyTypeKey" class="com.cwc.app.key.FinanceApplyTypeKey"/>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="repairQualityInspectionKey" class="com.cwc.app.key.RepairQualityInspectionKey"/> 
<%
	long repair_order_id = StringUtil.getLong(request,"repair_order_id");
 	TDate tDate = new TDate();
	//fileFlag
	String fileFlag= StringUtil.getString(request,"flag");
	DBRow repairOrder = repairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
	int isPrint = StringUtil.getInt(request,"isPrint",0);
	
	int number = 0;
	boolean edit = false;
	
	//准备中的返修单,并且没有货款申请,返修单明细可修改
	if(repairOrder.get("repair_status",0)==RepairOrderKey.READY&&applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,repair_order_id,FinanceApplyTypeKey.REPAIR_ORDER).length==0)
	{
		edit = true;
	}
	HashMap followuptype = new HashMap();
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改返修单详细记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	followuptype.put(13,"到货通知仓库");
	
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	//读取配置文件中的purchase_tag_types
		String tagType = systemConfig.getStringConfigValue("repair_tag_types");
		String[] arraySelected = tagType.split("\n");
		//将arraySelected组成一个List
		ArrayList<String> selectedList= new ArrayList<String>();
		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
				String tempStr = arraySelected[index];
				if(tempStr.indexOf("=") != -1){
					String[] tempArray = tempStr.split("=");
					String tempHtml = tempArray[1];
					selectedList.add(tempHtml);
				}
		}
		// 获取所有的关联图片 然后在页面上分类
		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
		int[] productFileTyps = {FileWithTypeKey.REPAIR_PRODUCT_TAG_FILE};
		DBRow[] imagesRows = purchaseMgrZyj.getAllProductTagFilesByPcId(repair_order_id,productFileTyps );
		if(imagesRows != null && imagesRows.length > 0 )
		{
			for(int index = 0 , count = imagesRows.length ; index < count ; index++ )
			{
				DBRow tempRow = imagesRows[index];
				String product_file_type = tempRow.getString("product_file_type");
				List<DBRow> tempListRow = imageMap.get(product_file_type);
				if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1))
				{
					tempListRow = new ArrayList<DBRow>();
				}
				tempListRow.add(tempRow);
				imageMap.put(product_file_type,tempListRow);
			}
		}
		// 单证
		String valueCertificate = systemConfig.getStringConfigValue("repair_certificate");
	String[] arraySelectedCertificate = valueCertificate.split("\n");
	//将arraySelected组成一个List
	ArrayList<String> selectedListCertificate= new ArrayList<String>();
	for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
			String tempStr = arraySelectedCertificate[index];
			if(tempStr.indexOf("=") != -1){
				String[] tempArray = tempStr.split("=");
				String tempHtml = tempArray[1];
				selectedListCertificate.add(tempHtml);
			}
	}
	int file_with_type = StringUtil.getInt(request,"file_with_type");
	String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
	String file_with_class = StringUtil.getString(request,"file_with_class");
	DBRow[] imageListCertificate = fileMgrZJ.getFileByWithIdAndType(repair_order_id,FileWithTypeKey.REPAIR_CERTIFICATE);
 	Map<String,List<DBRow>> mapCertificate = new HashMap<String,List<DBRow>>();
	if(imageListCertificate != null && imageListCertificate.length > 0){
		//map<string,List<DBRow>>
		for(int index = 0 , count = imageListCertificate.length ; index < count ;index++ ){
			DBRow temp = imageListCertificate[index];
			List<DBRow> tempListRow = mapCertificate.get(temp.getString("file_with_class"));
			if(tempListRow == null){
				tempListRow = new ArrayList<DBRow>();
			}
			tempListRow.add(temp);
			mapCertificate.put(temp.getString("file_with_class"),tempListRow);
		}
	}
	// 实物图片
	String valueProductFile = systemConfig.getStringConfigValue("repair_product_file");
	String[] arrayProductFileSelected = valueProductFile.split("\n");
 	String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/repair/repair_order_detail.html?repair_order_id="+repair_order_id;
 	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>返修单详细</title>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<style type="text/css">
	body{text-align:left;}
	ol.fundsOl{
		list-style-type: none;	
	}
	ol.fundsOl li{
		width: 350px;
		height: 200px;
		float: left;
		line-height: 25px;
	}
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<!-- jqgrid 引用 -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>

<script type="text/javascript">
	var select_iRow;
	var select_iCol;				
	
	function print()
	{
		visionariPrinter.PRINT_INIT("返修单");
			
			visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../repair/print_repair_order_detail.html?repair_order_id=<%=repair_order_id%>");
			visionariPrinter.PREVIEW();
	}
		
	function uploadRepairOrderDetail()
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_upload_excel.html?repair_order_id=<%=repair_order_id%>';
		$.artDialog.open(url, {title: '上传返修单',width:'800px',height:'500px', lock: true,opacity: 0.3});
		//tb_show('上传返修单','repair_upload_excel.html?repair_order_id=<%=repair_order_id%>&TB_iframe=true&height=500&width=800',false);
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
			 
	function clearPurchase()
	{
		if(confirm("确定清空该采购单内所有商品吗？"))
		{
			document.del_form.submit();
		}
	}
	
	function downloadRepairOrder(repair_order_id)
	{
		var para = "repair_order_id="+repair_order_id;
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/downRepairOrder.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
	}
	
	
	function autoComplete(obj)
	{
		addAutoComplete(obj,
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
	}
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	function refreshWindow(){
		window.location.reload();
	}
	
	function ajaxModProductName(obj,rowid,col)
	{
		var unit_name;//单字段修改商品名时更换单位专用
		var para ="repair_detail_id="+rowid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/getRepairDetailJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}
	
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
	
	function createWaybill(repair_order_id)
	{
		$.artDialog.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_waybill.html?repair_order_id="+repair_order_id, {title: '返修单生成快递单',width:'670px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function uploadInvoice(repair_order_id)
	{
		$.artDialog.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_upload_invoice.html?repair_order_id="+repair_order_id, {title: '返修单上传商业发票',width:'670px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function printWayBill(id,print_page)
	{
		 
		 var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+print_page+"?id="+id+"&type=R";
		
		 $.artDialog.open(uri, {title: '打印运单',width:'960px',height:'500px', lock: true,opacity: 0.3});
   	}
	function changeType(obj) {
		$('#declaration').attr('style','display:none');
		$('#clearance').attr('style','display:none');
		if($(obj).val()==1) {
			$('#clearance').attr('style','');
		}else if($(obj).val()==2) {
			$('#declaration').attr('style','');
		}
	}
	function promptCheck(v,m,f)
	{
		if (v=="y")
		{
			 if(f.content == "")
			 {
					alert("请填写适当备注");
					return false;
			 }
			 else
			 {
			 	if(f.content.length>300)
			 	{
			 		alert("内容太多，请简略些");
					return false;
			 	}
			 }
			 return true;
		}
	}
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
 	function deleteFileTable(_file_id){
  	   $.ajax({
 			url:'<%= deleteFileAction%>',
 			dataType:'json',
 			data:{table_name:'file',file_id:_file_id,folder:'',pk:'file_id'},
 			success:function(data){
 				if(data && data.flag === "success"){
 					window.location.reload();
 				}else{
 					showMessage("系统错误,请稍后重试","error");
 				}
 			},
 			error:function(){
 				showMessage("系统错误,请稍后重试","error");
 			}
 		})
  	}
	function goApplyFunds(id)
	{
		var id="'F"+id+"'";
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=searchby_index&search_mode=1&apply_id="+id);       
	}
</script>



<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:text-top;
		padding-top:2px;
	}
.set{
	border:2px #999999 solid;
	padding:2px;
	width:90%;
	word-break:break-all;
	margin-top:10px;
	margin-top:5px;
	line-height:18px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	margin-bottom: 10px;
}
	.create_order_button{background-attachment: fixed;background: url(../imgs/create_order.jpg);background-repeat: no-repeat;background-position: center center;height: 51px;width: 129px;color: #000000;border: 0px;}
	.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
	.zebraTable td {line-height:25px;height:25px;}
	.right-title{line-height:20px;height:20px;}
	span.spanBold{font-weight:bold;}
	span.fontGreen{color:green;}
	span.fontRed{color:red;}
	span.spanBlue{color:blue;}
</style>
<script>
function openApplyMoneyInsert() {
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/financial_management/apply_money_transport_insert.html?associationId=<%=repairOrder.getString("repair_order_id")%>";

	$.artDialog.open(uri, {title: '申请费用',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showTransit()
{
	var	uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_basic_update.html?isOutter=2&repair_order_id=<%=repairOrder.getString("repair_order_id")%>'; 
	$.artDialog.open(uri, {title: '返修单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function updateRepairAllProdures(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/repair/repair_wayout_update.html?isOutter=2&repair_order_id=<%=repairOrder.getString("repair_order_id")%>";
	$.artDialog.open(uri, {title: '修改各流程信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showFreight(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/repair/repair_freight_update.html?isOutter=2&repair_order_id=<%=repairOrder.getString("repair_order_id")%>";
	$.artDialog.open(uri, {title: '修改运单',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showFreightCost(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+'administrator/repair/setRepairFreight.html?isOutter=2&repair_order_id=<%=repairOrder.getString("repair_order_id")%>&fr_id=<%=repairOrder.getString("fr_id")%>&purchase_id=<%=repairOrder.get("purchase_id",0) %>';
	$.artDialog.open(uri, {title: '修改运费',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function applyFreigth(amount,applyTransferFreightTotal,purchase_id) {
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/apply_money_repair_insert.html?associationId=<%=repair_order_id%>&amount='+amount+'&subject=1&purchase_id='+purchase_id+"&applyTransferFreightTotal="+applyTransferFreightTotal;
	$.artDialog.open(url, {title: '申请运费',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function fileWithClassCertifycateChange(){
	var file_with_class = $("#file_with_class_certificate").val();
   	$("#tabsCertificate").tabs( "select" , file_with_class * 1-1 );
}
function deleteFileCommon(file_id , tableName , folder,pk){
	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:tableName,file_id:file_id,folder:folder,pk:pk},
		success:function(data){
			if(data && data.flag === "success"){
				window.location.reload();
			}else{
				showMessage("系统错误,请稍后重试","error");
			}
		},
		error:function(){
			showMessage("系统错误,请稍后重试","error");
		}
})
}
 
jQuery(function($){
	if($.trim('<%= fileFlag%>') === "1"){
		showMessage("上传文件出错","error");
	}
	if($.trim('<%= fileFlag%>') === "2"){
		showMessage("请选择正确的文件类型","error");
	}
})
function followUpCerficate(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_certificate_follow_up.html?repair_order_id="+<%= repair_order_id%>;
	$.artDialog.open(uri , {title: "单证跟进["+<%= repair_order_id%>+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
function productFileFollowUp(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_product_file_follow_up.html?repair_order_id="+<%= repair_order_id%>;
	$.artDialog.open(uri , {title: "实物图片跟进["+<%= repair_order_id%>+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});	
}
function showSingleLogs(repair_order_id,repair_type,title){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_show_single_log.html?repair_order_id="+<%= repair_order_id%>+"&repair_type="+repair_type;
	$.artDialog.open(uri , {title: title+"["+<%= repair_order_id%>+"]",width:'870px',height:'470px', lock: true,opacity: 0.3,fixed: true});	
}
function clearanceButton(repair_order_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_clearance.html?repair_order_id="+repair_order_id; 
	$.artDialog.open(uri , {title: "清关流程["+repair_order_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
function declarationButton(repair_order_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_declaration.html?repair_order_id="+repair_order_id;
	$.artDialog.open(uri , {title: "报关流程["+repair_order_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
function quality_inspectionKey(repair_order_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_quality_inspection.html?repair_order_id="+repair_order_id;
	$.artDialog.open(uri , {title: "质检流程["+repair_order_id+"]",width:'570px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}
function tag(repair_order_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_tag.html?repair_order_id="+repair_order_id;
	$.artDialog.open(uri , {title: "跟进制签["+repair_order_id+"]",width:'570px',height:'290px', lock: true,opacity: 0.3,fixed: true});
}
function stock_in_set(repair_order_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_stockinset.html?repair_order_id="+repair_order_id;
	$.artDialog.open(uri , {title: "运费流程["+repair_order_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,_target){
    var targetNode = $("#"+_target);
    $("input[name='file_names']",targetNode).val(fileNames);
    if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
        $("input[name='file_names']").val(fileNames);
        var myform = $("#"+_target);
        var file_with_class = $("#file_with_class").val();
			$("input[name='backurl']",targetNode).val("<%= backurl%>");
		  	myform.submit();
	}
}

function updateRepairVW(repair_order_id)
	{
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/updateRepairDetailVW.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:{repair_order_id:repair_order_id},
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date)
					{
						if(date.result=="ok")
						{
							window.location.reload();
						}
					}
				});
	}
</script>
</head>

<body onload="onLoadInitZebraTable();">
<div class="demo">
<div id="repairTabs">
	<ul>
		<li><a href="#repair_basic_info">基础信息<span> </span></a></li>
		<li><a href="#repair_info">运单信息<span> </span></a></li>
		<%if(repairOrder.get("stock_in_set",RepairStockInSetKey.SHIPPINGFEE_NOTSET) != RepairStockInSetKey.SHIPPINGFEE_NOTSET) {%>
			<%
	  						//计算颜色
	  						String stockInSetClass = "" ;
				int stockInSetInt = repairOrder.get("stock_in_set",RepairStockInSetKey.SHIPPINGFEE_NOTSET);
				if(stockInSetInt == RepairStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH){
  								stockInSetClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",repairOrder.get("repair_order_id",0l),FileWithTypeKey.REPAIR_STOCKINSET);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									stockInSetClass += " fontGreen";
  								}else{
  									stockInSetClass += " fontRed";
  								}
				}else if(stockInSetInt != RepairStockInSetKey.SHIPPINGFEE_NOTSET){
					stockInSetClass += "spanBold spanBlue";
  							}
  				 %>
			<li><a href="#repair_price_info"><span class="<%=stockInSetClass %>">运费信息</span></a></li>
		<%} %>
	 	<%if(repairOrder.get("declaration",RepairDeclarationKey.NODELARATION) != RepairDeclarationKey.NODELARATION) {%>
			<%
	  						//计算颜色
	  						String declarationKeyClass = "" ;
				int declarationInt = repairOrder.get("declaration",RepairDeclarationKey.NODELARATION);
				if(declarationInt == RepairDeclarationKey.FINISH){
  								declarationKeyClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",repairOrder.get("repair_order_id",0l),FileWithTypeKey.REPAIR_DECLARATION);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									declarationKeyClass += " fontGreen";
  								}else{
  									declarationKeyClass += " fontRed";
  								}
				}else if(declarationInt != RepairDeclarationKey.NODELARATION ){
					declarationKeyClass +=  "spanBold spanBlue";
  							}
	  	 %>
			<li><a href="#repair_out_info"><span class="<%=declarationKeyClass %>">出口报关 </span></a></li>
		<%} %>
		<%if(repairOrder.get("clearance",RepairClearanceKey.NOCLEARANCE) != RepairClearanceKey.NOCLEARANCE) {%>
			 <%
	  						//计算颜色
	  						String clearanceKeyClass = "" ;
				int clearanceInt = repairOrder.get("clearance",RepairClearanceKey.NOCLEARANCE);
				if( clearanceInt == RepairClearanceKey.FINISH){
  								clearanceKeyClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",repairOrder.get("repair_order_id",0l),FileWithTypeKey.REPAIR_CLEARANCE);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									clearanceKeyClass += " fontGreen";
  								}else{
  									clearanceKeyClass += " fontRed";
  								}
				}else if(clearanceInt != RepairClearanceKey.NOCLEARANCE){
					clearanceKeyClass +=  "spanBold spanBlue";
  							}
	  					%>
			<li><a href="#repair_in_info"><span class="<%= clearanceKeyClass%>">进口清关</span></a></li>
		<%} %>
	 				<%
	  						//计算颜色
	  						String tagClass = "" ;
  							int tagInt = repairOrder.get("tag",RepairTagKey.NOTAG);
  							if(tagInt == RepairTagKey.FINISH){
  								tagClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",repairOrder.get("repair_order_id",0l),FileWithTypeKey.REPAIR_TAG);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									tagClass += " fontGreen";
  								}else{
  									tagClass += " fontRed";
  								}
  							}else if(tagInt != RepairTagKey.NOTAG){
  								tagClass += "spanBold spanBlue";
  							}
	  					%>
		<li><a href="#repair_tag_make"><span class='<%= tagClass%>'>标签制作</span></a></li>
 
 		<%if(repairOrder.get("certificate",RepairCertificateKey.NOCERTIFICATE) != RepairCertificateKey.NOCERTIFICATE) {%>
  							<%
	  						//计算颜色
	  						String certificateClass = "" ;
  							int certificateInt = repairOrder.get("certificate",RepairCertificateKey.NOCERTIFICATE);
  							if(certificateInt == RepairCertificateKey.FINISH){
  								certificateClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",repairOrder.get("repair_order_id",0l),FileWithTypeKey.REPAIR_CERTIFICATE);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									certificateClass += " fontGreen";
  								}else{
  									certificateClass += " fontRed";
  								}
  							}else if(certificateInt != RepairCertificateKey.NOCERTIFICATE){
  								certificateClass += "spanBold spanBlue";
  							}
  			 %>
			<li><a href="#repair_order_prove"><span class='<%= certificateClass%>'>单证信息</span></a></li>
		<%} %>
		<%
	  						//计算颜色
	  						String productFileClass = "" ;
  							int productFileInt = repairOrder.get("product_file",RepairProductFileKey.NOPRODUCTFILE);
  							if(productFileInt == RepairProductFileKey.FINISH){
  								productFileClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("product_file",repairOrder.get("repair_order_id",0l),FileWithTypeKey.REPAIR_PRODUCT_FILE);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									productFileClass += " fontGreen";
  								}else{
  									productFileClass += " fontRed";
  								}
  							}else if(productFileInt != RepairProductFileKey.NOPRODUCTFILE){
  								productFileClass += "spanBold spanBlue";
  							}
  			 %>
			<li><a href="#product_file"><span class='<%= productFileClass%>'>实物图片</span></a></li>
 
		<%if(repairOrder.get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY) != RepairQualityInspectionKey.NO_NEED_QUALITY) {%>
						<%
	  						//计算颜色
	  						String qualityInspectionClass = "" ;
							int qualityInspectionInt = repairOrder.get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY);
							if(qualityInspectionInt == RepairQualityInspectionKey.FINISH){
  								qualityInspectionClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",repairOrder.get("repair_order_id",0l),FileWithTypeKey.REPAIR_QUALITYINSPECTION);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									qualityInspectionClass += " fontGreen";
  								}else{
  									qualityInspectionClass += " fontRed";
  								}
							}else if(qualityInspectionInt != RepairQualityInspectionKey.NO_NEED_QUALITY){
								qualityInspectionClass += "spanBold spanBlue";
  							}
  					 %>
			<li><a href="#quality_inspection"><span class='<%= qualityInspectionClass%>'>质检</span></a></li>
		<%} %>
	</ul>
	<div id="repair_basic_info">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="60%" valign="top">
					<table width="100%">
						<tr>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">返修单号：</font></td>
							<td align="left" width="16%">R<%=repair_order_id%></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">预计到达日期：</font></td>
							<td align="left" width="16%"><% 
								if(repairOrder.getString("repair_receive_date").trim().length() > 0){
									out.println(tDate.getFormateTime(repairOrder.getString("repair_receive_date")));
								}
 								%>
							</td>
							<td width="16%" align="right"height="25"><font style="font-family: 黑体; font-size: 14px;">创建人：</font></td>
							<td align="left" width="16%"><%=repairOrder.getString("create_account")%></td>
						</tr>
						<tr>
							<td align="right" width="16%" height="25"><font style="font-family: 黑体; font-size: 14px;">允许装箱：</font></td>
							<td align="left" width="16%">
								<% 
									DBRow packinger = adminMgr.getDetailAdmin(repairOrder.get("packing_account",0l));
									if(packinger!=null)
									{
										out.print(packinger.getString("employe_name"));
									}
								%>
							</td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">创建时间：</font></td>
							<td align="left" colspan="3"><%=tDate.getFormateTime(repairOrder.getString("repair_date"))%></td>
						</tr>
						<tr>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">货物状态：</font></td>
							<td align="left" width="16%"><%=repairOrderKey.getRepairOrderStatusById(repairOrder.get("repair_status",0)) %></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">出口报关：</font></td>
							<td align="left" width="16%"><%=repairDeclarationKey.getStatusById(repairOrder.get("declaration",01)) %></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">进口清关：</font></td>
							<td align="left" width="16%"><%=repairClearanceKey.getStatusById(repairOrder.get("clearance",01)) %></td>
						</tr>
						<tr>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">运费状态：</font></td>
							<td align="left" width="16%"><%=repairStockInSetKey.getStatusById(repairOrder.get("stock_in_set",RepairStockInSetKey.SHIPPINGFEE_NOTSET)) %></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">制签流程：</font></td>
							<td align="left" width="16%">
							<% 
								if(repairOrder.get("tag",RepairTagKey.NOTAG) == RepairTagKey.TAG){
									out.print("制签中");
								}else{
									out.print(repairTagKey.getRepairTagById(repairOrder.get("tag",RepairTagKey.NOTAG)));
								}
							 %>
							</td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">单证流程：</font></td>
							<td align="left" width="16%"><%=repairCertificateKey.getStatusById(repairOrder.get("certificate",RepairCertificateKey.NOCERTIFICATE)) %></td>
						</tr>
						<tr>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">实物图片：</font></td>
							<td align="left" width="16%"><%= repairProductFileKey.getStatusById(repairOrder.get("product_file",RepairProductFileKey.NOPRODUCTFILE)) %></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">质检：</font></td>
							<td align="left" width="16%"><%= repairQualityInspectionKey.getStatusById(repairOrder.get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY)+"") %></td>
							<td width="16%" align="right" height="25"></td>
							<td align="left" width="16%"></td>
						</tr>
					</table>
				</td>
				<td width="40%"></td>
			</tr>
			<tr><td colspan="2" height="15px"></td></tr>
			<tr>
				<td colspan="2" align="left"><br/>
					<input name="button" type="button" class="long-button" value="明细修改" onClick="showTransit()"/>
					<input name="button" type="button" class="long-button" value="修改各流程信息" onClick="updateRepairAllProdures()"/>
					<%
						if(repairOrder.get("repair_status",0)==RepairOrderKey.READY)
						{
					%>
						<input  type="button" class="long-button-upload" value="上传返修单" onclick="uploadRepairOrderDetail()"/>
					<%	
						}
					%>
					<input  type="button" class="long-button-next" value="下载返修单" onclick="downloadRepairOrder(<%=repair_order_id%>)"/>
			  		<input type="button" class="long-button-print" value="打印返修单" onclick="print()"/>
			  		<% 
						if(repairOrder.get("repair_status",0)==RepairOrderKey.READY&&repairOrder.get("purchase_id",0l)==0)
						{
					%>
							<input name="button" type="button" class="long-button" value="装箱" onClick="readyPacking(<%=repair_order_id%>)"/>
					<% 
						}
					%>
					<%
	  					if(repairOrder.get("repair_status",0)==RepairOrderKey.PACKING)
	  					{
	  				%>
	  					<tst:authentication bindAction="com.cwc.app.api.zyj.RepairOrderMgrZyj.rebackRepairDamage">
	  						<input type="button" value="停止装箱" class="lonrepairredtext" onclick="reBackRepair(<%=repairOrder.get("repair_order_id",0l)%>)"/>
	  					</tst:authentication>
	  				<%
	  					}
	  				%>
	  				<% 
					if(repairOrder.get("repair_status",0)==RepairOrderKey.PACKING||(repairOrder.get("purchase_id",0l)!=0l&&repairOrder.get("repair_status",0)==RepairOrderKey.READY))
					 	{
				  	%>
	 					 <input name="button" type="button" class="long-button" value="返修单出库" onClick="showStockOut(<%=repairOrder.get("repair_order_id",0l)%>)"/>
					<%
						}
					%>
					<%
	  					if(repairOrder.get("repair_status",0)==RepairOrderKey.INTRANSIT)
	  					{
	  				%>
	  					<tst:authentication bindAction="com.cwc.app.api.zyj.RepairOrderMgrZyj.reStorageRepair">
	  						<input type="button" value="中止运输" class="long-button-yellow" onclick="reStorageRepair(<%=repairOrder.get("repair_order_id",0l)%>)"/> 
	  					</tst:authentication>
	  				<%
	  					}
	  				%>
					<%
	  					if(RepairOrderKey.INTRANSIT == repairOrder.get("repair_status",0))
	  					{
	  				%>
		  				<input type="button" value="到货派送" class="long-button" onclick="goodsArriveDelivery(<%=repairOrder.get("repair_order_id",0l)%>)"/>
		  				<input type="button" value="到货并入库" class="long-button" onclick="showStockIn(<%=repairOrder.get("repair_order_id",0l)%>)"/>
	  				<%		
	  					}
	  				%>
	  				<%
	  					if(RepairOrderKey.AlREADYRECEIVE == repairOrder.get("repair_status",0))
	  					{
	  				%>
	  					<input type="button" value="入库" class="short-short-button" onclick="showStockIn(<%=repairOrder.get("repair_order_id",0l)%>)"/>
	  				<%		
	  					}
	  				%>
	  				<%
	  					if(RepairOrderKey.FINISH == repairOrder.get("repair_status",0)||RepairOrderKey.PART_REPAIR == repairOrder.get("repair_status",0))
	  					{
	  				%>
	  					<input type="button" value="处理货物" class="long-button" onclick="handleDamageProduct(<%=repairOrder.get("repair_order_id",0l)%>)"/>
	  				<%		
	  					}
	  				%>
				</td>
			</tr>
		</table>
	</div>
	<div id="repair_info">
		<table border="0" width="100%">
			<tr>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">货运公司：</font></td>
			  	<td width="40%" align="left" height="25"><%=repairOrder.getString("repair_waybill_name") %></td>
			  	<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">运单号：</font></td>
				<td width="40%" align="left" height="25"><%=repairOrder.getString("repair_waybill_number") %></td>
			</tr>
			<tr>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">运输方式：</font></td>
			  	<td width="40%" align="left" height="25"><%=repairWayKey.getStatusById(repairOrder.get("repairby",0)) %></td>
			  	<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">承运公司：</font></td>
				<td width="40%" align="left" height="25"><%=repairOrder.getString("carriers") %></td>
			</tr>
			<tr>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">发货港：</font></td>
				<td width="40%" align="left" height="25"><%=repairOrder.getString("repair_send_place")%></td>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">目的港：</font></td>
				<td width="40%" align="left" height="25"><%=repairOrder.getString("repair_receive_place")%></td>
			</tr>
			<tr><td colspan="4" height="15"></td></tr>
			<tr>
				<td style="width:10%;text-align:right;">
					<font style="font-family: 黑体; font-size: 14px;">&nbsp;</font>
				</td>
				<td style="width:90%;" colspan="3">
					<table style="width:100%;" class="addr_table">
						<tr>
							<td style="width:45%;">
								<table style="width:100%;border-collapse:collapse ;">
									<tr>
										<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">提货地址</font></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">提货仓库：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												if(repairOrder.get("purchase_id",0l)==0)
												{
													out.print(catalogMgr.getDetailProductStorageCatalogById(repairOrder.get("send_psid",0l)).getString("title"));
													session.setAttribute("billOfLading",catalogMgr.getDetailProductStorageCatalogById(repairOrder.get("send_psid",0l)).getString("title"));
												}
												else
												{
													out.print(supplierMgrTJH.getDetailSupplier(repairOrder.get("send_psid",0l)).getString("sup_name"));
												}
											%>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=repairOrder?repairOrder.getString("send_house_number"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=repairOrder?repairOrder.getString("send_street"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=repairOrder?repairOrder.getString("send_city"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=repairOrder?repairOrder.getString("send_zip_code"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
										<td style="border: 1px solid silver;width: 80%;">
			  <%
												long ps_id3 = null!=repairOrder?repairOrder.get("send_pro_id",0L):0L ;
												DBRow psIdRow3 = productMgr.getDetailProvinceByProId(ps_id3);
											%>
											<%=(psIdRow3 != null? psIdRow3.getString("pro_name"):repairOrder.getString("send_pro_input") ) %>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ccid3 = null!=repairOrder?repairOrder.get("send_ccid",0L):0L; 
												DBRow ccidRow3 = orderMgr.getDetailCountryCodeByCcid(ccid3);
			  %>
											<%= (ccidRow3 != null?ccidRow3.getString("c_country"):"" ) %>	
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=repairOrder?repairOrder.getString("send_name"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=repairOrder?repairOrder.getString("send_linkman_phone"):"" %></td>
									</tr>
								</table>
							</td>
							<td style="width:10%;text-align: center;font-size: 18px;">
								-->
							</td>
							<td style="width:45%;">
								<table style="width:100%;border-collapse:collapse ;" class="addr_table">
									<tr>
										<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">收货地址</font></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">收货仓库：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=catalogMgr.getDetailProductStorageCatalogById(repairOrder.get("receive_psid",0l)).getString("title")?catalogMgr.getDetailProductStorageCatalogById(repairOrder.get("receive_psid",0l)).getString("title"):""%></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=repairOrder.getString("deliver_house_number") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=repairOrder.getString("deliver_street") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=repairOrder.getString("deliver_city") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=repairOrder.getString("deliver_zip_code") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ps_id4 = null!=repairOrder?repairOrder.get("deliver_pro_id",0l):0L;
												DBRow psIdRow4 = productMgr.getDetailProvinceByProId(ps_id4);
											%>
											<%=(psIdRow4 != null? psIdRow4.getString("pro_name"):repairOrder.getString("deliver_pro_input") ) %>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ccid4 = null!=repairOrder?repairOrder.get("deliver_ccid",0l):0L; 
												DBRow ccidRow4 = orderMgr.getDetailCountryCodeByCcid(ccid4);
											%>
											<%= (ccidRow4 != null?ccidRow4.getString("c_country"):"" ) %>	
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=repairOrder.getString("repair_linkman")%></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=repairOrder.getString("repair_linkman_phone")%></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table><br/>
		<table width="100%">
			<tr>
				<td align="left"">
					<input name="button" type="button" value="运单修改" onClick="showFreight()" class="long-button"/>
					<input  type="button" class="long-button-upload" value="上传商业发票" onclick="uploadInvoice(<%=repair_order_id %>)"/>
					<%
						if(repairOrder.getString("invoice_path").length()>0)
						{
					%>
					<input type="button" value="下载商业发票" class="long-button-next" onclick="window.location.href='../../<%=repairOrder.getString("invoice_path")%>'"/>
					<%
						}
					%>
					<%
		  					if(repairOrder.get("repair_status",0)==RepairOrderKey.INTRANSIT)
		  					{
		  			%>
											<input name="button" type="button" value="生成快递单" onClick="createWaybill(<%=repairOrder.get("repair_order_id",0l)%>)" class="long-button"/>
					 <%
				  	  			DBRow shippingCompany = expressMgr.getDetailCompany(repairOrder.get("sc_id",0l));
				  	  			if(shippingCompany !=null)
				  	  			{
				  	 %>
							  	  <input type="button" class="long-button-print" value="打印快递单" onclick="printWayBill(<%=repairOrder.get("repair_order_id",0l)%>,'<%=shippingCompany.getString("internal_print_page")%>')"/>
				  	 <%	
				  	  			}
							}
					 %>
					</td>
				</tr>
			</table>
		 
							
							
							  	  		
	</div>
 <%
	//如果采购单ID为0，为返修单，否则为交货单
 	if(repairOrder.get("stock_in_set",RepairStockInSetKey.SHIPPINGFEE_NOTSET) != RepairStockInSetKey.SHIPPINGFEE_NOTSET)
 {%>
	<div id="repair_price_info">
	<p style="margin-bottom:5px;">
		 运费流程阶段:<span style="color:green;font-weight:bold;"><%=repairStockInSetKey.getStatusById(repairOrder.get("stock_in_set",RepairStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>
		  <input class="short-button" type="button" onclick='stock_in_set(<%=repair_order_id%>)' value="运费跟进"/>
			<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=repair_order_id %>',5,'运费日志')"/> 		
	</p>
						<%
					DBRow[] rows = repairOrderMgrZyj.getRepairFreightCostByRepairId(repairOrder.getString("repair_order_id"));
					DBRow repairOrderRow = repairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
				%>
		<table id="tables" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table">
			     <tr>
			       <th width="21%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>
			       <th width="13%" nowrap="nowrap" class="right-title"  style="text-align: center;">小计</th>	
			     </tr>
			     <%
		    	 float sum_price = 0;
			     if(rows!=null){
				     for(int i = 0;i<rows.length;i++)
				     {
				    	 DBRow row = rows[i];
				    	 long tfc_id = row.get("tfc_id",0l);
			    		 double tfc_unit_count = row.get("tfc_unit_count",0d);
			    		 String tfc_project_name = "";
			    		 String tfc_way = "";
			    		 String tfc_unit = "";
			    		 String tfc_currency = "";
			    		 double tfc_unit_price = 0;
			    		 double tfc_exchange_rate = 0;
				    	 if(tfc_id != 0) {//以前有数据
				    		 tfc_project_name = row.getString("tfc_project_name");
				    		 tfc_way = row.getString("tfc_way");
				    		 tfc_unit = row.getString("tfc_unit");
				    		 tfc_unit_price = row.get("tfc_unit_price",0d);
				    		 tfc_currency = row.getString("tfc_currency");
				    		 tfc_exchange_rate = row.get("tfc_exchange_rate",0d);
				    	 }
				    	 String bgCol = "#FFFFFF";
		     			if(1 == i%2){
		     				bgCol = "#F9F9F9";
		     			}
			      %>
	     <tr height="30px" style="padding-top:3px;background-color: <%=bgCol %>">
			        <td valign="middle" style="padding-top:3px;">
			        	<input type="hidden" name="tfc_id" id="tfc_id" value="<%=tfc_id %>"/>
			        	<input type="hidden" name="tfc_project_name" id="tfc_project_name" value="<%=tfc_project_name %>"/>
			        	<input type="hidden" name="tfc_way" id="tfc_way" value="<%=tfc_way %>"/>
			        	<input type="hidden" name="tfc_unit" id="tfc_unit" value="<%=tfc_unit %>"/>
			        	
						<%=tfc_project_name %>	
			        </td>
			         <td valign="middle" nowrap="nowrap" >       
			      		<%=tfc_way %>
			        </td>
			        <td align="left" valign="middle">
						<%=tfc_unit %>
			        </td>
			        <td >
						<%=tfc_unit_price %>
			        </td>
			        <td >
						<%=tfc_currency%>
			        </td>
			        <td >
						<%=tfc_exchange_rate %>
			        </td>
			        <td valign="middle" >       
						<%=tfc_unit_count %>
			        </td>
			        <td valign="middle" nowrap="nowrap">       
					<%=MoneyUtil.round(tfc_unit_price*tfc_exchange_rate*tfc_unit_count,2)%>
		        </td>
			     </tr>
			     <%
			     		sum_price += tfc_unit_price*tfc_exchange_rate*tfc_unit_count;
			    	 }
			     %>
				 <tr height="30px">
				 	<td colspan="2">
		 		<fieldset class="set" style="border-color:green;">
					<legend>运费:<%=repairOrderMgrZyj.getSumRepairFreightCost(repairOrder.getString("repair_order_id")).get("sum_price",0d) %>RMB</legend>
					<%
						if(repairOrder.get("stock_in_set",1)==1) {
							out.println("</b></font>");
						}
					%>
					<table style="width:100%" align="left">
					<%
						DBRow[] applyMoneys = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(10015,repairOrder.get("repair_order_id",0L),FinanceApplyTypeKey.REPAIR_ORDER);
						for(int j=0; j<applyMoneys.length; j++){
							String freightCostStr = "";
							if(!"RMB".equals(applyMoneys[j].getString("currency")))
							{
								freightCostStr = "/"+applyMoneys[j].get("standard_money",0f)+"RMB";
							}
					%>	
								<tr><td align='left' style="border: 0px;">
								<a href="javascript:void(0)" onClick='goApplyFunds(<%=applyMoneys[j].get("apply_id",0) %>)'>F<%=applyMoneys[j].get("apply_id",0) %></a>
								<%
									List imageListStock = applyMoneyMgrLL.getImageList(applyMoneys[j].getString("apply_id"),"1");
									String stockStatusName = "";
									//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
								 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneys[j].getString("status")) && imageListStock.size() < 1)
								 	{
								 		stockStatusName = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
								 	}else{
								 		stockStatusName = fundStatusKey.getFundStatusKeyNameById(applyMoneys[j].getString("status"));	
								 	}
								 	stockStatusName = "("+stockStatusName+")";
								%>
								<%=stockStatusName %>
								<%=applyMoneys[j].get("amount",0f) %><%=applyMoneys[j].getString("currency") %><%=freightCostStr %>
								</td></tr>
							<%
						 			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneys[j].getString("apply_id"));
						 				//applyMoneyMgrLL.getApplyTransferByBusiness(repairOrder.getString("repair_order_id"),6);
						 			if(applyTransferRows.length>0) {
						 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
						 					if(applyTransferRows[ii].get("apply_money_id",0) == applyMoneys[j].get("apply_id",0)) {
							  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
							  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
							  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
							  				int moneyStatus = applyTransferRows[ii].get("status",0);
							  				String transferMoneyStandardFreightStr = "";
							  				if(!"RMB".equals(currency))
							  				{
							  					transferMoneyStandardFreightStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
							  				}
							  				if(moneyStatus != 0 )
							  					out.println("<tr><td align='left' style='border: 0px;'><a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>已转账"+amount+" "+currency+transferMoneyStandardFreightStr+"</td></tr>");
							  				else
							  					out.println("<tr><td align='left' style='border: 0px;'><a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请转账"+amount+" "+currency+transferMoneyStandardFreightStr+"</td></tr>");
							  				}	
						 				}
						 			}
						 		}
						 	%>
				 	</table>
			 	</fieldset>
				 	</td>
				 	<td colspan="5" align="right">总计</td>
				 	<td nowrap="nowrap"><%=sum_price %></td>
				 </tr>
				 <%
			     }
			     %>
	     </table><br/>
	     <table style="border: 0; width: 98%">
			 <tr>
	     		<td colspan="10" align="left" style="border: 0">
				<%
					double applyTransferFreightTotal = applyFundsMgrZyj.getTransportFreightTotalApply(repairOrder.get("repair_order_id",0L),FinanceApplyTypeKey.REPAIR_ORDER);
				%>
	     			<input name="button" type="button" value="运费修改" onClick="showFreightCost()" class="long-button"/>
		     	<%
		     		if(repairOrder.get("stock_in_set",1)==2) 
		     		{
		     	%>
			        	<input name="button" type="button" value="申请运费" onClick="applyFreigth('<%=sum_price %>','<%=applyTransferFreightTotal %>','<%=repairOrder.get("purchase_id",0) %>')" class="long-button"/>
		     	<%
		     		}
		     	%>
	     		</td>
	     	</tr>
	 </table>
    </div>
<%} %>	
<%if(repairOrder.get("declaration",RepairDeclarationKey.NODELARATION) != RepairDeclarationKey.NODELARATION) {%>
	<div id="repair_out_info">
		<p style="margin-bottom:5px;">
			 出口报关阶段:
			 <span style="color:green;font-weight:bold;"><%=repairDeclarationKey.getStatusById(repairOrder.get("declaration",RepairDeclarationKey.NODELARATION)) %></span>
			 <input class="short-button" type="button" onclick='declarationButton(<%=repair_order_id%>)' value="报关跟进"/>
		</p>
		<table  width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" >
				<tr>
		       <th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
		       <th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
		     </tr>
		     <%
		    	int[] repair_type = {RepairLogTypeKey.ExportCustoms};
				DBRow [] repairRows = repairOrderMgrZyj.getRepairLogsByRepairIdAndType(repair_order_id,repair_type);
		     	if(null != repairRows && repairRows.length > 0){
		     		for(int i = repairRows.length - 1; i >=0 ; i--){
		     			DBRow transRow = repairRows[i];
		     			 
		     %>			
		     			 <tr height="30px">
					     	<td><%=null == adminMgrLL.getAdminById(transRow.getString("repairer_id"))?"":adminMgrLL.getAdminById(transRow.getString("repairer_id")).getString("employe_name") %></td>
					     	<td><%=transRow.getString("repair_content") %></td>
					     	<td><%=transRow.getString("time_complete") %></td>
					     	<td><%= tDate.getFormateTime(transRow.getString("repair_date")) %></td>
		  					 <td><%=followuptype.get(transRow.get("repair_type",0))%></td>
					     </tr>
		     <%		
		     		}
		     	}else{
		     %>		
		     	<tr>
		     		<td colspan="5" align="center" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;"> 无数据 </td>
		     	</tr>
		     <%		
		     	}
			%>
			</table>
	</div>
<%} %>	
<%if(repairOrder.get("clearance",RepairClearanceKey.NOCLEARANCE) != RepairClearanceKey.NOCLEARANCE) {%>
	<div id="repair_in_info">
	<p style="margin-bottom:5px;">
				 进口清关阶段:
				 <span style="color:green;font-weight:bold;"><%=repairClearanceKey.getStatusById(repairOrder.get("clearance",RepairClearanceKey.NOCLEARANCE)) %></span>
				<input class="short-button" type="button" onclick='clearanceButton(<%=repair_order_id%>)' value="清关跟进"/>
			</p>
		<table id="tableLog" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable"  >
			<tr>
		       <th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
		       <th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
		     </tr>
		     <%
		    	int[] repair_type_clearance = {RepairLogTypeKey.ImportCustoms};
				DBRow [] repairRowsClearance = repairOrderMgrZyj.getRepairLogsByRepairIdAndType(repair_order_id,repair_type_clearance);
		     	if(null != repairRowsClearance && repairRowsClearance.length > 0){
		     		for(int i =( repairRowsClearance.length-1); i >=0 ; i-- ){
		     			DBRow transRow = repairRowsClearance[i];
		     %>			
		     			 <tr height="30px"  >
					     	<td><%=null == adminMgrLL.getAdminById(transRow.getString("repairer_id"))?"":adminMgrLL.getAdminById(transRow.getString("repairer_id")).getString("employe_name") %></td>
					     	<td><%=transRow.getString("repair_content") %></td>
					     	<td><%=transRow.getString("time_complete") %></td>
					     	<td><%=transRow.getString("repair_date") %></td>
		  					 <td><%=followuptype.get(transRow.get("repair_type",0))%></td>
				</tr>
		     <%		
		     		}
		     	}else{
		     %>		
		     	<tr>
		     		<td colspan="5" align="center" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
		     	</tr>
		     <%		
		     	}
			%>
			</table>
	</div>
<%} %>	
 
	<div id="repair_tag_make">
		 <p style="margin-bottom:5px;">
		 	制签流程阶段:<span style="color:green;font-weight:bold;"><%= repairTagKey.getRepairTagById(repairOrder.get("tag",RepairTagKey.NOTAG)) %></span>
			<input type="button" class="long-button-print" value="制作唛头" onclick="shippingMark()"/>
			<input type="button" class="long-button-print" value="制作标签" onclick="printLabel()"/>
			<input type="button" class="long-button-print" value="跟进制签" onclick="tag('<%=repair_order_id %>')"/>
			<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=repair_order_id %>',8,'制签')"/> 
	  	 </p>
		<div id="tagTabs" style="margin-top:15px;">
   		<ul>
		<%
				 		if(selectedList != null && selectedList.size() > 0){
				 			for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
		%>	
				 				<li><a href="#repair_product_tag_<%=index %>"> <%=selectedList.get(index) %></a></li>
				 			<% 	
				 			}
				 		}
			 		%>
	   		 	</ul>
	   		 	<%
	   		 		for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
	   		 				List<DBRow> tempListRows = imageMap.get((index+1)+"");
	   		 			%>
	   		 			 <div id="repair_product_tag_<%= index%>">
	   		 			 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
	   		 			 			<tr> 
				  						<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
				  						<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
				  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
				  					</tr>
	   		 			 	<%
	   		 			 		if(tempListRows != null && tempListRows.size() > 0){
	   		 			 			 for(DBRow row : tempListRows){
	   		 			 			%>
	   		 			 				<tr style="height: 25px;">
	   		 			 					<td>
	   		 			 					<a href='<%= downLoadFileAction%>?file_name=<%= row.getString("file_name") %>&folder=<%=systemConfig.getStringConfigValue("file_path_product")%>'><%= row.getString("file_name") %></a>
	   		 			 					</td>
	   		 			 					<td><%=null != productMgr.getDetailProductByPcid(row.get("pc_id",0))?productMgr.getDetailProductByPcid(row.get("pc_id",0)).getString("p_name"):""  %></td>
	   		 			 					<td>
	   		 			 						<% if(!(repairOrder.get("nee_tag",0) == RepairTagKey.FINISH && repairOrder.get("need_tag",0.0d) > 0)){ %>
	   		 			 							<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
	   		 			 				 		<%} %>	
	   		 			 				 	</td>
	   		 			 				</tr>	
	   		 			 			<% 
	   		 			 		 }
	   		 			 		}else{
	   		 			 			%>
	 		 			 			<tr>
	 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	 								</tr>
	   		 			 			<%
	   		 			 		}
	   		 			 	%>
	   		 			 	 	</table>
			</div>
		<%	
			} 
		%>
	</div>
	</div>
 
 <%if(repairOrder.get("certificate",RepairCertificateKey.NOCERTIFICATE) != RepairCertificateKey.NOCERTIFICATE) {%>
	<div id="repair_order_prove">
	<p style="margin-bottom:5px;">
		单证流程阶段:<span style="color:green;font-weight:bold;"><%= repairCertificateKey.getStatusById(repairOrder.get("certificate",0)) %></span>
		 <input type="button" class="short-button" value="单证跟进" onclick="followUpCerficate();"/>
	 	 <input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=repair_order_id %>',9,'单证')"/> 
	</p>
			<table width="90%" border="0" align="center"   cellspacing="0" style="margin-left:18px;margin-top:-8px;">
		  <tr>
		    <td>
				 <fieldset style="border:2px #cccccc solid;padding:2px;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<legend style="font-size:15px;font-weight:normal;color:#999999;">上传文件</legend>
					<form name="uploadCertificateImageForm" id="uploadCertificateImageForm" value="uploadCertificateImageForm" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/repair/RepairCertificateUpFileAction.action" %>'   method="post">	
						<input type="hidden" name="backurl"  value=""/>
						<input type="hidden" name="file_with_id" value="<%=repair_order_id %>" />
						<input type="hidden" name="sn" id="sn" value="R_certificate"/>
						<input type="hidden" name="path" id="path" value="<%=systemConfig.getStringConfigValue("file_path_repair") %>"/>
						<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.REPAIR_CERTIFICATE %>"/>
						<input type="hidden" name="file_names" />
						<table width="90%" border="0">
						 	<td height="25" align="right"  style="width:120px;"  class="STYLE2" nowrap="nowrap">上传文件:</td>
						       <td>	 
						     	  &nbsp;<input type="button"  class="long-button" onclick="uploadFile('uploadCertificateImageForm');" value="选择文件" />
						 	   </td>
							 </tr>
							 <tr>
							 	<td height="25" align="right"  class="STYLE2" nowrap="nowrap">文件分类:</td>
							 	<td>
						        	&nbsp;<select name="file_with_class" id="file_with_class_certificate" onchange="fileWithClassCertifycateChange();">
		 								<%if(arraySelectedCertificate != null && arraySelectedCertificate.length > 0){
		 									for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
		 									String tempStr = arraySelectedCertificate[index];
		 									String tempValue = "" ;
		 									String tempHtml = "" ;
		 									if(tempStr.indexOf("=") != -1){
		 										String[] tempArray = tempStr.split("=");
		 										tempValue = tempArray[0];
		 										tempHtml = tempArray[1];
		 									}
		 								%>		
		 									<option value="<%=tempValue %>"><%=tempHtml %></option>
		 								<%	} 
		 								}
		 								%>
						        	</select>
						        	
							 	</td>
							 </tr>
						</table>
					</form>
				</fieldset>	
			</td>
		  </tr>
		</table>
			<div id="tabsCertificate" style="margin-top:10px;">
			 <ul>	
			 	<%
			 		if(selectedListCertificate != null && selectedListCertificate.size() > 0){
			 			for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ ){
			 			%>
			 				<li><a href="#repair_certificate_<%=index %>"> <%=selectedListCertificate.get(index) %></a></li>
			 			<% 	
			 			}
			 		}
			 	%>
			 </ul>
			 	<!-- 遍历出Div 然后显示出来里面的 数据-->
			 	<%
					for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ ){
						%>
							<div id="repair_certificate_<%=index %>">
			 					<%
			 						List<DBRow> arrayLisTemp = mapCertificate.get(""+(index+1));
			 						%>
			 					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
			 						<tr> 
				  						<th width="75%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
				  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
				  					</tr>
			 						<% 
			 						if(arrayLisTemp != null && arrayLisTemp.size() > 0 ){
			 							for(int listIndex = 0 , total = arrayLisTemp.size() ; listIndex < total ; listIndex++ ){
			 							 %>
			 								<tr>
			 									<td>
			 						<a href='<%= downLoadFileAction%>?file_name=<%=arrayLisTemp.get(listIndex).getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_repair")%>'><%=arrayLisTemp.get(listIndex).getString("file_name") %></a>
			 									</td><td>
			 										<!--  如果是整个的单据流程已经结束那么就是不应该有删除的文件的按钮的 -->
			 										<%
			 											if(!(repairOrder.get("certificate",0) == RepairCertificateKey.FINISH )){
			 												%>
			 												 <a href="javascript:deleteFileCommon('<%=arrayLisTemp.get(listIndex).get("file_id",0l)%>','file','<%=systemConfig.getStringConfigValue("file_path_repair") %>','file_id')">删除</a>
			 												<%  
			 											}
			 										%>	
	 		 									</td>
			 								</tr>
			 							 <%
			 							}	
			 						}else{
			 					 	%>
			 								<tr>
			 									<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
			 								</tr>
			 							<%
			 						}
			 					%>
			 					</table>	
							 </div>
						<%
					}
			 	%>
		</div>
	</div>
<%} %>	
	<div id="product_file">
		<p style="margin-bottom:5px;">
		 	  图片流程阶段:<span style="color:green;font-weight:bold;"><%= repairProductFileKey.getStatusById(repairOrder.get("product_file",0))%></span>
   		  <input type="button" value="图片跟进" class="short-button" onclick="productFileFollowUp();"/>
   		 	 <input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=repair_order_id %>',10,'实物图片')"/>
		</p>
		<div style="padding-top:5px;padding-bottom:5px;padding-left:10px;">
   			</div>		  
 			<div id="tabs">
  	            <ul>
					   <%if(arrayProductFileSelected != null && arrayProductFileSelected.length > 0){
 									for(int index = 0 , count = arrayProductFileSelected.length ; index < count ; index++ ){
 									String tempStr = arrayProductFileSelected[index];
 									String tempValue = "" ;
 									String tempHtml = "" ;
 									if(tempStr.indexOf("=") != -1){
 										String[] tempArray = tempStr.split("=");
 										tempValue = tempArray[0];
 										tempHtml = tempArray[1];
 									}
 								%>		
 				  <li><a href="repair_product_file_tright.html?repair_order_id=<%=repair_order_id %>&product_file_type=<%=tempValue %>&file_type_name=<%=tempHtml %>"><%=tempHtml %></a></li>
 								<%	} 
 								}
 						%>
			    </ul>
			  </div>
	</div>
 
 <%if(repairOrder.get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY) != RepairQualityInspectionKey.NO_NEED_QUALITY) {%>
	<div id="quality_inspection">
		<p style="margin-bottom:5px;">
			质检流程阶段:<span style="font-weight:bold;color:green;"><%=repairQualityInspectionKey.getStatusById(repairOrder.get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY)+"") %></span>
			 <input class="short-button" type="button" onclick='quality_inspectionKey(<%=repair_order_id%>)' value="质检跟进"/>
			 <input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=repair_order_id %>',11,'质检')"/>
		</p>
		<form  id="qualityInspectionForm"   method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/repair/RepairQualityInspectionAction.action" %>'>
		<!-- 读取质检报告的状态 -->
			<table>
				<tr>
					<td style="width:85px;"><%=repairQualityInspectionKey.getStatusById(repairOrder.get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY)+"") %>：</td>					
					<td style="width:700px;">
					 	<input type="hidden" name="backurl" value="<%=backurl %>" />
						<input type="hidden" name="sn" value="R_quality_inspection"/>
						<input type="hidden" name="quality_inspection" value='<%= repairOrder.get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY) %>' />
		 				<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.REPAIR_QUALITYINSPECTION %>" />
		 				<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_repair")%>" />
					 	<input type="hidden" name="file_names" />
					 	<input type="hidden" name="cmd" value="submit"/>	<!-- 表示页面提交 -->
						<input type="hidden" name="repair_order_id" value="<%=repair_order_id %>"/>
		 			 	<input type="button"  class="long-button" onclick="uploadFile('qualityInspectionForm');" value="选择文件" />
		 			</td>
		     </tr>
			</table>
						<table>
		     <%
							 DBRow[] fileRows = transportMgrZr.getFileByFilwWidthIdAndFileType("file",repair_order_id,FileWithTypeKey.REPAIR_QUALITYINSPECTION);
							 if(fileRows != null && fileRows.length > 0 ){
								 for(DBRow tempRow  : fileRows){
		     %>			
									<tr style="height: 25px;">
										<td width="100px" align="right">
											<%=null == adminMgrLL.getAdminById(tempRow.getString("upload_adid"))?"":adminMgrLL.getAdminById(tempRow.getString("upload_adid")).getString("employe_name") %>
										</td>
										<td width="100px;" align="center">
										<% if(!"".equals(tempRow.getString("upload_time"))){
												out.print(tDate.getFormateTime(tempRow.getString("upload_time")));
		     		}
		     %>		
										</td>
										<td width="60%" align="left">
												 <a href='<%= downLoadFileAction%>?file_name=<%=tempRow.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_repair")%>'><%=tempRow.getString("file_name") %></a>
										</td> 
										<!--  判断是不是可以删除 -->
										<% if(repairOrder.get("quality_inspection",RepairQualityInspectionKey.NO_NEED_QUALITY)!= RepairQualityInspectionKey.FINISH) {%> 
		     								 <td>
		     								 	<a  href="javascript:deleteFileTable('<%= tempRow.get("file_id",0l)%>')">删除</a>
		     								</td>
		     							<%} %>
		     	</tr>
		     <%		
								 }
									 
								 
								 
		     	}
			%>
						 </table>
				
			
			
		
		</form>
	</div>
<%} %>
</div>
<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
 	<tr>
 		<td align="left">
 			<input type='button' class='long-button' value='实物图片' onclick='addProductsPicture()'/> 
			<input type='button' class='long-button' value='第三方标签' onclick='addProductTagTypesFile()'/>"		
 		</td>
 		<td align="right">
 			<input type='button' class='long-button' value="更新重量与体积" onclick='updateRepairVW(<%=repair_order_id%>)'/>
 		</td>
 	</tr>
 </table>

<script>
	$("#repairTabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	 
	});
	$("#tagTabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	$("#tabsCertificate").tabs({
 		cache: true,
 		 
		select: function(event, ui){
			 $("#file_with_class_certificate option[value='"+(ui.index+1)+"']").attr("selected",true);
		} 
 	 });
	 // 首先获取 大的tabs,然后获取小的tabs。
	var bigIndex = ($("#repairTabs").tabs("option","selected")) * 1;
	if(bigIndex == 5){
		var selectedIndex = '<%= file_with_class%>' * 1 ;
		$("#file_with_class_certificate option[value='"+selectedIndex+"']").attr("selected",true);
	}
</script>
	<div id="detail" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table id="gridtest"></table>
	<div id="pager2"></div> 
	
	<script type="text/javascript">
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					sortable:true,
					url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/dataRepairOrderDetail.html',//dataDeliveryOrderDetails.html
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.98),
					height:255,
					autowidth: false,
					shrinkToFit:true,
					postData:{repair_order_id:<%=repair_order_id%>},
					jsonReader:{
				   			id:'repair_detail_id',
	                        repeatitems : false
	                	},
				   	colNames:['repair_detail_id','商品名','商品条码','返修数','销毁数','计划装箱数','实际装箱数','到达数量','体积(cm³)','重量(Kg)',<%=repairOrder.get("repair_status",0)==RepairOrderKey.FINISH?"'运费'":"'估算运费'"%>,'所在箱号','repair_order_id','repair_pc_id','已上传图片'], 
				   	colModel:[ 
				   		{name:'repair_detail_id',index:'repair_detail_id',hidden:true,sortable:false},
				   		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
				   		{name:'p_code',index:'p_code',align:'left'},
				   		{name:'repair_count',width:50,index:'repair_count',editrules:{required:true,number:true,minValue:0},sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'destroy_count',width:50,index:'destroy_count',editrules:{required:true,number:true,minValue:0},sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'repair_total_count',editable:<%=edit%>,width:50,index:'repair_total_count',editrules:{required:true,number:true,minValue:1},sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'repair_send_count',width:50,index:'repair_send_count',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},sortable:false,align:'center'},
				   		{name:'repair_reap_count',width:50,index:'repair_reap_count',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},sortable:false,align:'center'},
				   		{name:'repair_volume',width:50,index:'repair_volume',align:'center',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'repair_weight',index:'repair_weight',width:50,align:'center',width:60,formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","}},
				   		{name:'freight_cost',index:'freight_cost',width:50,align:'center',width:60,formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","},sortable:false},
				   		{name:'repair_box',width:50,index:'repair_box',editable:<%=edit%>},
				   		{name:'repair_order_id',index:'repair_order_id',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=repair_order_id%>},hidden:true,sortable:false},
				   		{name:'repair_pc_id',index:'repair_pc_id',hidden:true,sortable:false},
				   		{name:'button',index:'button',align:'left',sortable:false},
				   		], 
				   	rowNum:-1,//-1显示全部
				   	pgtext:false,
				   	pgbuttons:false,
				   	mtype: "GET",
				   	gridview: true, 
					rownumbers:true,
				   	pager:'#pager2',
				   	sortname: 'repair_detail_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
				   	multiselect: true,
				   	onSelectCell:function(id,name)
					{
	                		if(name=="button")
   					 		{
	                			var product_id = jQuery("#gridtest").jqGrid('getCell',id,'repair_pc_id');
   					 			addProductPicture(product_id);
   					 		}
					},
				   	afterEditCell:function(id,name,val,iRow,iCol)
				   				  { 
				   					if(name=='p_name') 
				   					{
				   						autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
				   					}
				   					select_iRow = iRow;
				   					select_iCol = iCol;
				   				  }, 
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				   				  { 
				   				  	if(name=='p_name'||name=='repair_delivery_count'||name=='repair_backup_count') 
				   					{
				   						ajaxModProductName(jQuery("#gridtest"),rowid);
				   						getRepairSumVWP(<%=repair_order_id%>);
				   				  	}
				   				  }, 
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/gridEditRepairDetail.action',
				   	errorCell:function(serverresponse, status)
				   				{
				   					alert(errorMessage(serverresponse.responseText));
				   				},
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/gridEditRepairDetail.action'
				   	}); 		   	
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
			   
				function getRepairSumVWP(repair_order_id)
				{
					$.ajax({
								url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/getRepairSumVWPJson.action',
								type: 'post',
								dataType: 'json',
								timeout: 60000,
								cache:false,
								data:{repair_order_id:repair_order_id},
								
								beforeSend:function(request){
								},
								
								error: function(e){
									alert(e);
									alert("提交失败，请重试！");
								},
								
								success: function(date)
								{
									$("#sum_volume").html("总体积:"+date.volume+" cm³");
									$("#sum_weight").html("总重量:"+date.weight+" Kg");
									$("#sum_price").html("总货款:"+date.send_price+" RMB");
								}
							});
				}
	</script>
		</td>
	</tr>
</table>
<table width="100%">
	<tr>
		<td colspan="2">
			<span id="sum_volume"></span>
			<span id="sum_price"></span>
			<span id="sum_weight"></span>
		</td>
	</tr>
</table>
	</div>
  </div>
</div>

<form name="download_form" method="post"></form>
<form action="" method="post" name="followup_form">
	<input type="hidden" name="repair_order_id"/>
	<input type="hidden" name="type" value="1"/>
	<input type="hidden" name="stage" value="1"/>
	<input type="hidden" name="repair_type" value="1"/>
	<input type="hidden" name="repair_content"/>
	<input type="hidden" name="expect_date"/>
</form>
<script type="text/javascript">
	//$("#eta").date_input();
	function readyPacking(repair_order_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_storage_show.html?repair_order_id='+repair_order_id;
		$.artDialog.open(url, {title: '返修缺货商品',width:'700px',height:'400px', lock: true,opacity: 0.3});
		//tb_show('返修缺货商品','repair_storage_show.html?repair_order_id='+repair_order_id+'&TB_iframe=true&height=400&width=700',false);
	}
	function reBackRepair(repair_order_id)
	{
		if(confirm("确定返修单R"+repair_order_id+"停止装箱？（将按照返修商品回退库存）"))
		{
			var para = "repair_order_id="+repair_order_id+"&is_need_notify_executer=true";
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/reBackRepairAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	function showStockOut(repair_order_id) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_stock_temp_out.html?repair_order_id='+repair_order_id;
		$.artDialog.open(url, {title: '返修单出库',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function reStorageRepair(repair_order_id)
	{
		if(confirm("确定返修单R"+repair_order_id+"中止运输？"))
		{
			var para = "repair_order_id="+repair_order_id+"&is_need_notify_executer=true";
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/reStorageRepairAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	function goodsArriveDelivery(repair_order_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_goods_arrive_delivery.html?repair_order_id='+repair_order_id;
		$.artDialog.open(url, {title: '到货派送['+repair_order_id+"]",width:'500px',height:'200px', lock: true,opacity: 0.3});
	}
	function showStockIn(repair_order_id) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_stock_temp_in.html?repair_order_id='+repair_order_id;
		$.artDialog.open(url, {title: '返修单入库',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function handleDamageProduct(repair_order_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_handle_damage_product.html?repair_order_id='+repair_order_id;
		$.artDialog.open(url, {title: '处理货物',width:'800px',height:'500px', lock: true,opacity: 0.3});
	}
	function addProductsPicture(){
		var repair_order_id = '<%= repair_order_id%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_product_picture_up.html?repair_order_id="+repair_order_id;
	 	var addUri =  getSelectedIdAndNames();
	 	if($.trim(addUri).length < 1 ){
			showMessage("请先选择商品!","alert");
			return ;
		}else{uri += addUri;}
		$.artDialog.open(uri , {title: "实物图片上传["+repair_order_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	function addProductPicture(pc_id){
		var repair_order_id = '<%= repair_order_id%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_product_picture_up.html?repair_order_id="+repair_order_id+"&pc_id="+pc_id;
		$.artDialog.open(uri , {title: "实物图片上传["+repair_order_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	function addProductTagTypesFile(){
		 //添加商品标签
		var repair_order_id = '<%= repair_order_id%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/repair/repair_product_tag_file.html?repair_order_id="+repair_order_id ;
		var addUri =  getSelectedIdAndNames();
	 	if($.trim(addUri).length < 1 ){
			showMessage("请先选择商品!","alert");
			return ;
		}else{uri += addUri;}
		 $.artDialog.open(uri , {title: "第三方标签上传["+repair_order_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	function getSelectedIdAndNames(){
		s = jQuery("#gridtest").jqGrid('getGridParam','selarrrow')+"";  
	  	 
	 	var array = s.split(",");
 		var strIds = "";
 		var strNames = "";
	 	for(var index = 0 , count = array.length ; index < count ; index++ ){
		   var number = array[index];
	 	   var thisid= $("#gridtest").getCell(number,"repair_pc_id");
		    var thisName =$("#gridtest").getCell(number,"p_name"); 
	 	  	strIds += (","+thisid);
	 	  	strNames += (","+thisName);
		}
		if(strIds.length > 1 ){
		    strIds = strIds.substr(1);
		    strNames = strNames.substr(1);
		}
		if(strIds+"" === "false"){return "";}
		return  "&pc_id="+strIds;
	}
	//点击弹出制作麦头的界面
	function shippingMark()
	{
	    var repair_order_id=<%=repair_order_id%>;
        var awb="<%=repairOrder.getString("repair_waybill_number")%>";
        var awbName="<%=repairOrder.getString("repair_waybill_name")%>";
	    var menpai="<%=repairOrder.getString("deliver_house_number")%>";
	    var jiedao="<%=repairOrder.getString("deliver_street") %>";
	    var chengshi="<%=repairOrder.getString("deliver_city") %>";
	    var youbian="<%=repairOrder.getString("deliver_zip_code") %>";
	    var shengfen="<%=ps_id4%>";
	    var guojia="<%= ccid4%>";
	    var lianxiren="<%=repairOrder.getString("repair_linkman")%>";
	    var dianhua="<%=repairOrder.getString("repair_linkman_phone")%>";
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/print_repair_shpping_mark.html?awbName='+awbName+"&dianhua="+dianhua+"&lianxiren="+lianxiren+"&guojia="+guojia+"&shengfen="+shengfen+"&youbian="+youbian+"&chengshi="+chengshi+"&jiedao="+jiedao+"&menpai="+menpai+"&repair_order_id="+repair_order_id+"&awb="+awb; 
		$.artDialog.open(uri , {title: '唛头标签打印',width:'600px',height:'420px', lock: true,opacity: 0.3,fixed: true});
	} 
	//点击制作标签后修改成弹出dialog
	function printLabel()
	{
         var supplierSid =<%=repairOrder.get("send_psid",0l) %>;
         var repair_order_id=<%=repair_order_id%>;
         var warehouse="<%=null!=catalogMgr.getDetailProductStorageCatalogById(repairOrder.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(repairOrder.get("receive_psid",0l)).getString("title"):""%>";
       	 var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/made_repair_internal_label.html?warehouse="+warehouse+"&repair_order_id="+repair_order_id;
		 $.artDialog.open(uri , {title: '返修单内部标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});		
	}
</script>
<script>
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	cookie: {expires: 30000},
	load: function(event, ui) {onLoadInitZebraTable();}	
});

getRepairSumVWP(<%=repair_order_id%>);
</script>	
<script type="text/javascript">
//stateBox 信息提示框
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
	 var  _self = $("body"),
	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
 }
}
</script>
</body>
</html>

