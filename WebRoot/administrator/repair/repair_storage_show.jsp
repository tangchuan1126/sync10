<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.exception.purchase.FileException"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.exception.transport.NoExistTransportDetailException"%>
<%@ include file="../../include.jsp"%> 
<%
	long repair_order_id = StringUtil.getLong(request,"repair_order_id");
	String inserted = StringUtil.getString(request, "inserted");
	boolean submit = true;
	boolean existDetail = true;
	DBRow[] rows;
	DBRow[] repairOrderDetails = repairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id);
	try
	{
		rows = repairOrderMgrZyj.checkStorage(repair_order_id);
	}
	catch(NoExistTransportDetailException e)
	{
		rows = null;
		existDetail = false;
	}
	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>准备装箱</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" cellpadding="0" cellspacing="0" height="100%">
  <tr>
  	<td valign="top">
		<table width="98%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;" class="zebraTable">
		  <% 
		  	if(rows!=null&&rows.length>0)
		  	{
		  		submit = false;
		  %>
		  	 <tr>
				<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">商品名</th>
				<th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">转运数量</th>
			 </tr>
			  <%
			  	for(int i=0;i<rows.length;i++)
			  	{
			  %>
			  	<tr>
			  		<td height="40"><%=rows[i].getString("p_name")%></td>
			  		<td align="center"><%=rows[i].get("repair_total_count",0f)%></td>
			  	</tr>
			  <%	
			  	}
			  %>
	  			<tr align="left" valign="middle">
					<th height="100%" align="left" style="font-size:xx-large;background-color: white;">库存缺货</th>
				</tr>
		  <%
		  	}
		  	else
		  	{
		  		if(existDetail)
		  		{
		  	%>
			  	 <tr>
					<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">商品名</th>
					<th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">转运数量</th>
				 </tr>
			  	 <%
				  	for(int i=0;i<repairOrderDetails.length;i++)
				  	{
				  %>
				  	<tr>
				  		<td height="40"><%=repairOrderDetails[i].getString("repair_p_name")%></td>
				  		<td align="center"><%=repairOrderDetails[i].get("repair_total_count",0.0)%></td>
				  	</tr>
				  <%	
				  	}
				  %>
				<tr height="5px"></tr>
		  		<tr align="left" valign="middle">
					<th height="100%" align="left" style="font-size:xx-large;background-color: white;">可以从该仓库转运发货</th>
				</tr>
		  	<%
		  		}
		  		else
		  		{
		  	%>
		  		<tr align="center" valign="middle">
					<th height="100%" align="center" style="padding-left:10px;font-size:xx-large;background-color: white;"><font color="red">没有转运明细，无法转运</font></th>
				</tr>
		  	<%
		  		}
		  %>
		  	
		  <%
		  	}
		  %>
		  </table>
	</td>
  </tr>
 <tr>
 	<td valign="bottom">
 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="left" valign="middle" class="win-bottom-line">&nbsp;
				</td>
 				<td align="right" valign="middle" class="win-bottom-line"> 
 					<%
 						if(submit&&existDetail)
 						{
 					%>
 					<input id="submit" type="button" name="Submit2" value="确定" class="normal-green" onClick="intransitDelivery(<%=repair_order_id%>)"/>
 					<%
 						}
 					%>
			      	
				  	<input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeDialog()"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
   
</table>
<form action='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/intransitRepair.action' name="subForm" id="subForm" method="post">
	<input type="hidden" name="repair_order_id" value='<%=repair_order_id%>'>
	<input type="hidden" name="backurl" value='<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_storage_show.html?repair_order_id=<%=repair_order_id %>&inserted=1'/>
	<input type="hidden" name="is_need_notify_executer" value="true"/>
</form>
<script type="text/javascript">
	function closeDialog(){
		$.artDialog && $.artDialog.close();
	}
	function ajaxSaveRepairDetail()
	{
			var tempfilename = $("#tempfilename").val();
			var repair_order_id = $("#repair_order_id").val();	
				
			var para = "tempfilename="+tempfilename+"&repair_order_id="+repair_order_id;
			
			$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true, 
			
				fadeOut:  2000
			};
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
			
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminitrrator/repair/saveRepairDetail.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){

					if (date["close"])
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">保存成功！</span>' });
						$.unblockUI();
						parent.closeWin();
					}
					else
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">'+date["error"]+'</span>' });
						alert();
					}
				}
			});
	}
	
	function intransitDelivery(repair_order_id,number)
	{
		$("#submit").attr("disabled","disabled");
		$("#subForm").submit();				
	}
	var inserted = false;
	<%
		if(inserted.equals("1")) {
	%>
			inserted = true;
	<%
		}
	%>
	
    function init() {
		if(inserted) {
			parent.location.reload();
			$.artDialog.close();
		}
	}

	$(document).ready(function(){
		init();
	});
</script>
<script type="text/javascript">
	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
  </script>

</body>
</html>



