<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@ include file="../../include.jsp"%> 
<%
DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
long transport_id = StringUtil.getLong(request,"transport_id");

DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<script>
	function modTransport()
	{
		if($("#send_psid").val() == "")
		{
			alert("请选择转运仓库");
		}
		else if($("#receive_psid").val()=="")
		{
			alert("请选择接收仓库");
		}
		else if($("#address").val()=="")
		{
			alert("请填写交货地址！");
		}
		else if($("#linkman").val()=="")
		{
			alert("请填写联系人！");
		}
		else if($("#linkman_number").val()=="")
		{
			alert("必须留下联系方式！");
		}
		else
		{
			document.mod_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/modTransport.action"
			document.mod_form.send_psid.value = $("#send_psid").val();
			document.mod_form.receive_psid.value = $("#receive_psid").val();		
			document.mod_form.transport_address.value = $("#address").val();
			document.mod_form.transport_linkman.value = $("#linkman").val();
			document.mod_form.linkman_phone.value = $("#linkman_number").val();
			document.mod_form.transport_waybill_name.value = $("#transport_waybill_name").val();
			document.mod_form.transport_waybill_number.value = $("#transport_waybill_number").val();
			document.mod_form.transport_receive_date.value = $("#eta").val();
			document.mod_form.transport_send_place.value = $("#transport_send_place").val();
			document.mod_form.transport_send_freight.value = $("#transport_send_freight").val();
			document.mod_form.transport_receive_place.value = $("#transport_receive_place").val();
			document.mod_form.transport_receive_freight.value = $("#transport_receive_freight").val();
			
			document.mod_form.submit();	
		}
	}

	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	function selectStorage()
	{
		var para = "ps_id="+$("#receive_psid").val();
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){

				},
				
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				
				success: function(data){
					$("#address").val(data.address);
					$("#linkman").val(data.contact);
					$("#linkman_number").val(data.phone);
				}
			});
	}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE4 {font-size: medium}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
			<form method="post" name="mod_form">
			<input type="hidden" name="transport_id" value="<%=transport_id%>"/>
			<input type="hidden" name="send_psid"/>
			<input type="hidden" name="receive_psid"/>
			<input type="hidden" name="transport_address"/>
			<input type="hidden" name="transport_linkman"/>
			<input type="hidden" name="linkman_phone"/>
			<input type="hidden" name="transport_waybill_name"/>
			<input type="hidden" name="transport_waybill_number"/>
			<input type="hidden" name="transport_receive_date"/>
			<input type="hidden" name="transport_send_place"/>
			<input type="hidden" name="transport_send_freight"/>
			<input type="hidden" name="transport_receive_place"/>
			<input type="hidden" name="transport_receive_freight"/>
			<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/.html"/>
			</form>
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top"><br>
		<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" >
						  <tr>
							<td align="left" style="font-family:'黑体'; font-size:25px;border-bottom:1px solid #999999;color:#000000;">
							请选择转运仓库与接收仓库
							</td>
						  </tr>
					  </table>
						<br/>
                        <br>
                        <table width="95%" align="center" cellpadding="0" cellspacing="0" style="padding-left:10px;">
							<tr valign="bottom">
							  <td align="left" class="STYLE1 STYLE2" style="padding:0px;"><p class="STYLE4">转运仓库：</p></td>
						      <td height="20%"  align="left" class="STYLE1 STYLE2" style="padding:0px;">
						      	<select name='select' id='send_psid' <%=transport.get("transport_status",0)!=TransportOrderKey.READY?"disabled='disabled'":""%>>
                                <option value="">请选择...</option>
                                <%
									  for ( int i=0; i<treeRows.length; i++ )
									  {
									%>
                                <option value='<%=treeRows[i].getString("id")%>' <%=transport.get("send_psid",0l)==treeRows[i].get("id",0l)?"selected":""%>><%=treeRows[i].getString("title")%></option>
                                <%
									}
									%>
                              </select>
						      </td>
						      <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">发货地(城市/港口)：<input id="transport_send_place" name="transport_send_place" type="text" style="width: 150px" value="<%=transport.getString("transport_send_place")%>"/></td>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">发货地代办：<input id="transport_send_freight" name="transport_send_freight" type="text" style="width: 150px" value="<%=transport.getString("transport_send_freight")%>"/></td>
							</tr>
							<tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" >&nbsp;</td>
						  </tr>
							<tr>
							  <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >目的仓库：</td>
						      <td align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" >
						      <select name='select' id='receive_psid' onChange="selectStorage()">
                                <option value="">请选择...</option>
                                <%
									  for ( int i=0; i<treeRows.length; i++ )
									  {
									%>
                                <option value='<%=treeRows[i].getString("id")%>' <%=transport.get("receive_psid",0l)==treeRows[i].get("id",0l)?"selected":""%>><%=treeRows[i].getString("title")%></option>
                                <%
									}
									%>
                              </select>
                              </td>
                              <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">目的地(城市/港口)：<input id="transport_receive_place" name="transport_receive_place" type="text" style="width: 150px" value="<%=transport.getString("transport_receive_place")%>"/></td>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">目的地代办：<input id="transport_receive_freight" name="transport_receive_freight" type="text" style="width: 150px" value="<%=transport.getString("transport_receive_freight")%>"/></td>
						  </tr>
						  	 <tr>
							  <td  align="left" colspan="4" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						  <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >交货地点：</td>
						      <td colspan="3" width="90%"  align="left" valign="middle" nowrap="nowrap"><input name="address" type="text" id="address" value="<%=transport.getString("transport_address")%>" style="width: 600px;"/></td>
						  </tr>
						  <tr>
							  <td  colspan="4" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						  <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >联系人：</td>
						      <td  colspan="3" width="90%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2"><input type="text" name="linkman" id="linkman" value="<%=transport.getString("transport_linkman")%>"/></td>
						  </tr>
						  <tr>
							  <td  colspan="4" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						  <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">联系电话：</td>
						      <td colspan="3" width="90%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" ><input type="text" name="linkman_number" id="linkman_number" value="<%=transport.getString("transport_linkman_phone")%>" width="200px"/></td>
						  </tr>
						  <tr>
							  <td colspan="4" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						   <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">货运公司：</td>
						      <td colspan="3" width="90%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" ><input type="text" name="transport_waybill_name" id="transport_waybill_name" value="<%=transport.getString("transport_waybill_name")%>" width="200px"/></td>
						  </tr>
						  <tr>
							  <td colspan="4" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						   <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">运单号：</td>
						      <td colspan="3" width="90%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" ><input type="text" name="transport_waybill_number" value="<%=transport.getString("transport_waybill_number")%>" id="transport_waybill_number" width="200px"/></td>
						  </tr>
						  <tr>
							  <td colspan="4" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						  <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">预计到达时间：</td>
						      <td colspan="3" width="90%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" ><strong><input type="text" id="eta" name="eta" value='<%=transport.getString("transport_receive_date").equals("")?DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay():transport.getString("transport_receive_date")%>' style="width: 70px;"></strong></td>
						  </tr>
						  <tr>
							  <td colspan="4" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
					  </table>
				
				    </td></tr>
				
				<tr>
					<td align="right" valign="bottom">				
						<table width="100%">
							<tr>
								<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
								  <input type="button" name="Submit2" value="保存" class="normal-green" onClick="modTransport();">
								  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>	
			<script type="text/javascript">
				$("#eta").date_input();
			</script>
</body>
</html>
