<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.ll.Utils,java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.RepairQualityInspectionKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.RepairClearanceKey"%>
<%@page import="com.cwc.app.key.RepairDeclarationKey"%>
<%@page import="com.cwc.app.key.RepairProductFileKey"%>
<%@page import="com.cwc.app.key.RepairTagKey"%>
<%@page import="com.cwc.app.key.RepairStockInSetKey"%>
<%@page import="com.cwc.app.key.RepairCertificateKey"%>
<%@page import="com.cwc.app.key.RepairQualityInspectionKey"%>
<jsp:useBean id="repairClearanceKey" class="com.cwc.app.key.RepairClearanceKey"/>
<jsp:useBean id="repairDeclarationKey" class="com.cwc.app.key.RepairDeclarationKey"/>
<jsp:useBean id="repairTagKey" class="com.cwc.app.key.RepairTagKey"/> 
<jsp:useBean id="repairCertificateKey" class="com.cwc.app.key.RepairCertificateKey"/> 
<jsp:useBean id="repairStockInSetKey" class="com.cwc.app.key.RepairStockInSetKey"/> 
<jsp:useBean id="repairProductFileKey" class="com.cwc.app.key.RepairProductFileKey"/> 
<jsp:useBean id="repairQualityInspectionKey" class="com.cwc.app.key.RepairQualityInspectionKey"/> 
<%@ include file="../../include.jsp"%>
<%
	String repair_order_id = StringUtil.getString(request,"repair_order_id").equals("")?"0":StringUtil.getString(request,"repair_order_id");
	DBRow row = repairOrderMgrZyj.getDetailRepairOrderById(Long.parseLong(repair_order_id));
	//如果发货地址与提货地址相同，清关和报关默认不需要
	int delieverCcid = row.get("deliver_ccid",0);
	int sendCcid	 = row.get("send_ccid",0);
	boolean ccidIsSame = delieverCcid==sendCcid?true:false;
	String repair_waybill_number = row.getValue("repair_waybill_number")==null?"0":row.getValue("repair_waybill_number").toString();
	String repair_waybill_name = row.getValue("repair_waybill_name")==null?"":row.getValue("repair_waybill_name").toString();
	String repairby = row.getValue("repairby")==null?"0":row.getValue("repairby").toString();
	String declaration = row.getValue("declaration")==null?"0":row.getValue("declaration").toString();
	String clearance = row.getValue("clearance")==null?"0":row.getValue("clearance").toString();
	String drawback = row.getValue("drawback")==null?"1":row.getValue("drawback").toString();
	String invoice = row.getValue("invoice")==null?"1":row.getValue("invoice").toString();
	String repair_send_country = row.getValue("repair_send_country")==null?"1":row.getValue("repair_send_country").toString();
	String repair_receive_country = row.getValue("repair_receive_country")==null?"1":row.getValue("repair_receive_country").toString();
	String repair_send_place = row.getValue("repair_send_place")==null?"":row.getValue("repair_send_place").toString();
	String repair_receive_place = row.getValue("repair_receive_place")==null?"":row.getValue("repair_receive_place").toString();
	String repair_linkman = row.getValue("repair_linkman")==null?"":row.getValue("repair_linkman").toString();
	String repair_linkman_phone = row.getValue("repair_linkman_phone")==null?"":row.getValue("repair_linkman_phone").toString();
	String repair_address = row.getValue("repair_address")==null?"":row.getValue("repair_address").toString();
	String remark = row.getValue("remark")==null?"":row.getValue("remark").toString();
	String repair_receive_date = row.getValue("repair_receive_date")==null?"":row.getValue("repair_receive_date").toString();
	String fr_id = row.getValue("fr_id")==null?"":row.getValue("fr_id").toString();
	int repair_status = row.get("repair_status",0);
	int finished = StringUtil.getInt(request,"finished",0);
	int product_file = row.get("product_file",0);
	int tag = row.get("tag",0);
	int stock_in_set = row.get("stock_in_set",0);
	int certificate = row.get("certificate",0);
	int quality_inspection = row.get("quality_inspection",0);
	
	//查询各流程任务的负责人
	String adminUserIdsTransport		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 4);
	String adminUserNamesTransport		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 4);
	String adminUserIdsDeclaration		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 7);
	String adminUserNamesDeclaration	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 7);
	String adminUserIdsClearance		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 6);
	String adminUserNamesClearance		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 6);
	String adminUserIdsProductFile		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 10);
	String adminUserNamesProductFile	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 10);
	String adminUserIdsTag				= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 8);
	String adminUserNamesTag			= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 8);
	String adminUserIdsStockInSet		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 5);
	String adminUserNamesStockInSet		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 5);
	String adminUserIdsCertificate		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 9);
	String adminUserNamesCertificate	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 9);
	String adminUserIdsQualityInspection	= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 11);
	String adminUserNamesQualityInspection	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id, ModuleKey.REPAIR_ORDER, 11);
	
	int isOutter = StringUtil.getInt(request, "isOutter");
	String isSubmitSuccess = StringUtil.getString(request, "isSubmitSuccess");
	
	String previousUrl = ConfigBean.getStringValue("systenFolder")+"administrator/repair/repair_basic_update.html?repair_order_id="+repair_order_id;
	if(row.get("purchase_id",0)!=0)
	{
		previousUrl = ConfigBean.getStringValue("systenFolder")+"administrator/repair/repair_purchase_basic_update.html?repair_order_id="+repair_order_id;
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改返修单的各流程信息</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script>
	var updated = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated").equals("1")) {
	%>
			updated = true;
	<%
		}
	%>
	function init() {
		if(updated) {
			parent.location.reload();
			closeWindow();
		}
	}
	
	$(document).ready(function(){
		init();
		$("input:radio[name^=radio_]").click(
				function(){
					var hasThisCheck = $(this).val();
					var hasThisName = $(this).attr("name");
					var presonTrName = hasThisName.split("_")[1]+"PersonTr";
					if(2 == hasThisCheck){
						$("#"+presonTrName).attr("style","");
					}else{
						$("#"+presonTrName).attr("style","display:none");
					}
				}
		);
		$("input:radio[name=stock_in_set_radio]").click(
				function(){
					var hasThisCheck = $(this).val();
					if(2 == hasThisCheck){
						$("#stockInSetPersonTr").attr("style","");
					}else{
						$("#stockInSetPersonTr").attr("style","display:none");
					}
				}
		);
		
		if('2' == '<%=isSubmitSuccess%>'){
			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
		}
		
	});

	function adminUserTransport(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsTransport").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTransport'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTransport(user_ids , user_names){
		$("#adminUserIdsTransport").val(user_ids);
		$("#adminUserNamesTransport").val(user_names);
	}
	function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);};
	function adminUserDeclaration(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsDeclaration").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowDeclaration'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowDeclaration(user_ids , user_names){
		$("#adminUserIdsDeclaration").val(user_ids);
		$("#adminUserNamesDeclaration").val(user_names);
	}

	function adminUserClearance(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsClearance").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowClearance'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowClearance(user_ids , user_names){
		$("#adminUserIdsClearance").val(user_ids);
		$("#adminUserNamesClearance").val(user_names);
	}

	function adminUserProductFile(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsProductFile").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowProductFile'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowProductFile(user_ids , user_names){
		$("#adminUserIdsProductFile").val(user_ids);
		$("#adminUserNamesProductFile").val(user_names);
	}

	function adminUserTag(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsTag").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTag'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTag(user_ids , user_names){
		$("#adminUserIdsTag").val(user_ids);
		$("#adminUserNamesTag").val(user_names);
	}

	function adminUserStockInSet(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsStockInSet").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowStockInSet'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowStockInSet(user_ids , user_names){
		$("#adminUserIdsStockInSet").val(user_ids);
		$("#adminUserNamesStockInSet").val(user_names);
	}

	function adminUserCertificate(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsCertificate").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowCertificate'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowCertificate(user_ids , user_names){
		$("#adminUserIdsCertificate").val(user_ids);
		$("#adminUserNamesCertificate").val(user_names);
	}

	function adminUserQualityInspection(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsQualityInspection").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowQualityInspection'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowQualityInspection(user_ids , user_names){
		$("#adminUserIdsQualityInspection").val(user_ids);
		$("#adminUserNamesQualityInspection").val(user_names);
	};
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="apply_money_form" id="apply_money_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/RepairAllProduresUpdateAction.action">
<input type="hidden" name="repair_order_id" value="<%=repair_order_id%>"/>
<input type="hidden" name="freight_changed" id="freight_changed" value="0"/>
<input name="finished" id="finished" type="hidden">
<input type="hidden" name="isOutter" value="<%=isOutter %>"/>

<!-- 各流程 -->
<input type="hidden" name="declaration" value="<%=declaration %>"/>
<input type="hidden" name="clearance" value="<%=clearance %>"/>
<input type="hidden" name="product_file" value="<%=product_file %>"/>
<input type="hidden" name="tag" value="<%=tag %>"/>
<input type="hidden" name="stock_in_set" value="<%=stock_in_set %>"/>
<input type="hidden" name="certificate" value="<%=certificate %>"/>
<input type="hidden" name="quality_inspection" value="<%=quality_inspection %>"/>

<!-- 负责人的Ids -->
<input type="hidden" name="adminUserIdsTransport" id="adminUserIdsTransport" value='<%=adminUserIdsTransport %>'/>
<input type="hidden" name="adminUserIdsDeclaration" id="adminUserIdsDeclaration" value='<%=adminUserIdsDeclaration %>'/>
<input type="hidden" name="adminUserIdsClearance" id="adminUserIdsClearance" value='<%=adminUserIdsClearance %>'/>
<input type='hidden' name="adminUserIdsProductFile" id="adminUserIdsProductFile" value='<%=adminUserIdsProductFile %>'/>
<input type="hidden" name="adminUserIdsTag" id="adminUserIdsTag" value='<%=adminUserIdsTag %>'/>
<input type="hidden" name="adminUserIdsStockInSet" id="adminUserIdsStockInSet" value='<%=adminUserIdsStockInSet %>'/>
<input type="hidden" name="adminUserIdsCertificate" id="adminUserIdsCertificate" value='<%=adminUserIdsCertificate %>'/>
<input type="hidden" name="adminUserIdsQualityInspection" id="adminUserIdsQualityInspection" value='<%=adminUserIdsQualityInspection %>'/>


<!-- 各流程，是否发各类通知 -->
<input type="hidden" name="needMailTransport"/>
<input type="hidden" name="needMessageTransport"/>
<input type="hidden" name="needPageTransport"/>

<input type="hidden" name="needMailDeclaration"/>
<input type="hidden" name="needMessageDeclaration"/>
<input type="hidden" name="needPageDeclaration"/>

<input type="hidden" name="needMailClearance"/>
<input type="hidden" name="needMessageClearance"/>
<input type="hidden" name="needPageClearance"/>

<input type="hidden" name="needMailProductFile"/>
<input type="hidden" name="needMessageProductFile"/>
<input type="hidden" name="needPageProductFile"/>

<input type="hidden" name="needMailTag"/>
<input type="hidden" name="needMessageTag"/>
<input type="hidden" name="needPageTag"/>

<input type="hidden" name="needMailStockInSet"/>
<input type="hidden" name="needMessageStockInSet"/>
<input type="hidden" name="needPageStockInSet"/>

<input type="hidden" name="needMailCertificate"/>
<input type="hidden" name="needMessageCertificate"/>
<input type="hidden" name="needPageCertificate"/>

<input type="hidden" name="needMailQualityInspection"/>
<input type="hidden" name="needMessageQualityInspection"/>
<input type="hidden" name="needPageQualityInspection"/>

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">返修单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
	        <tr height="25px">
	          <td width="13%" align="right" class="STYLE2">返修单号:</td>
	          <td width="87%" align="left" valign="middle">
	          	<%="R"+repair_order_id%>
	          </td>
	        </tr>
	        <tr height="25px" id="transportPersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">运输负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input type="text" id="adminUserNamesTransport" name="adminUserNamesTransport" style="width:180px;" onclick="adminUserTransport()" value='<%=adminUserNamesTransport %>'/>
			    	通知：
			   		<%
			    		DBRow scheduleTransport	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(repair_order_id), ModuleKey.REPAIR_ORDER, 4);
				    	int mailTransport			= 2;//1提醒
			    		int messageTransport		= 2;
			    		int pageTransport			= 2;	
			   		 	if(null != scheduleTransport){
				   		 	mailTransport			= scheduleTransport.get("sms_email_notify",2);//1提醒
				    		messageTransport		= scheduleTransport.get("sms_short_notify",2);
				    		pageTransport			= scheduleTransport.get("is_need_replay",2);
			    		}
			    	%>
			   		<input type="checkbox" name="isMailTransport" <%= (1 == mailTransport || 2 == mailTransport) ? "checked":""%>/>邮件
			   		<input type="checkbox" name="isMessageTransport" <%if(1 == messageTransport || 2 == messageTransport){out.print("checked='checked'");}%>/>短信
			   		<input type="checkbox" name="isPageTransport" <%if(1 == pageTransport || 2 == pageTransport){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	        <tr height="25px">
	        	   <td width="13%" align="right" class="STYLE2">出口报关:</td>
	         		<td width="87%" align="left" valign="middle" id="declarationTr">
	         		<%
	         			//需要，不需要，或者null时，显示radio，否则输出当前报关的阶段
	         			if(declaration.equals(RepairDeclarationKey.DELARATION+"") || declaration.equals(RepairDeclarationKey.NODELARATION+"") || declaration.equals("0")){
	         		%>
	         		<input type="radio" name="radio_declaration" value="1" <%if(declaration.equals("1") || (ccidIsSame && declaration.equals("0"))){out.print("checked='checked'");}%>/> <%= repairDeclarationKey.getStatusById(1) %>
	          		<input type="radio" name="radio_declaration" value="2" <%if((!ccidIsSame && declaration.equals("0")) || declaration.equals("2")){out.print("checked='checked'");}%>/> <%= repairDeclarationKey.getStatusById(2) %>
	         		<%
	         			}else{
	         				out.print(repairDeclarationKey.getStatusById(Integer.parseInt(declaration)));
	         			}
	         		%>
	      			</td>
	      	</tr>
			 <%
		   	 	String declarationPersonTrStyle = "";
		   	 	if((ccidIsSame && declaration.equals("0")) || declaration.equals("1")){
		   	 		declarationPersonTrStyle = "display:none";
		    	}
			 %>
			<tr height="25px" style='<%=declarationPersonTrStyle %>' id="declarationPersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input type="text" id="adminUserNamesDeclaration" name="adminUserNamesDeclaration" value='<%=adminUserNamesDeclaration %>' style="width:180px;" onclick="adminUserDeclaration()"/>
			    	通知：
			    	<%
			    		DBRow scheduleDeclaration	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(repair_order_id), ModuleKey.REPAIR_ORDER, 7);
				    	int mailDeclaration			= 2;//1提醒
			    		int messageDeclaration		= 2;
			    		int pageDeclaration			= 2;	
			   		 	if(null != scheduleDeclaration){
				   		 	mailDeclaration			= scheduleDeclaration.get("sms_email_notify",2);//1提醒
				    		messageDeclaration		= scheduleDeclaration.get("sms_short_notify",2);
				    		pageDeclaration			= scheduleDeclaration.get("is_need_replay",2);
			    		}
			   		 	//System.out.println(mailDeclaration+"---"+messageDeclaration+"---"+pageDeclaration);
			    		
			    	%>
			   		<input type="checkbox" name="isMailDeclaration" <%= (1 == mailDeclaration || 2 == mailDeclaration) ? "checked":""%>/>邮件
			   		<input type="checkbox" name="isMessageDeclaration" <%if(1 == messageDeclaration || 2 == messageDeclaration){out.print("checked='checked'");}%>/>短信
			   		<input type="checkbox" name="isPageDeclaration" <%if(1 == pageDeclaration || 2 == pageDeclaration){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
			<tr height="25px">
        		<td width="13%" align="right" class="STYLE2">进口清关:</td>
		        <td width="87%" align="left" valign="middle" id="clearanceTr">
		       <%
		        	if(clearance.equals(RepairClearanceKey.CLEARANCE+"") || clearance.equals(RepairClearanceKey.NOCLEARANCE+"") || clearance.equals("0")){
		       %>
		         <input type="radio" name="radio_clearance" value="1" <%if(clearance.equals("1") || (ccidIsSame && clearance.equals("0"))){out.print("checked='checked'");}%>/> <%= repairClearanceKey.getStatusById(1) %>
		         <input type="radio" name="radio_clearance" value="2" <%if((!ccidIsSame && clearance.equals("0")) || clearance.equals("2")){out.print("checked='checked'");}%>/> <%= repairClearanceKey.getStatusById(2) %>		
		       <% 		
		        	}else{
		        		out.print(repairClearanceKey.getStatusById(Integer.parseInt(clearance)));
		        	}
		       %>
				</td>
	        </tr>
	         <%
			 	String clearancePersonTrStyle = "";
		   	 	if((ccidIsSame && clearance.equals("0")) || clearance.equals("1")){
		   	 		clearancePersonTrStyle = "display:none";
		    	}
			 %>
	        <tr height="25px" style='<%=clearancePersonTrStyle %>' id="clearancePersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input type="text" id="adminUserNamesClearance" name="adminUserNamesClearance" value='<%=adminUserNamesClearance %>' style="width:180px;" onclick="adminUserClearance()"/>
			    	通知：
			    	<%
			    		DBRow scheduleClearance	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(repair_order_id), ModuleKey.REPAIR_ORDER, 6);
				    	int mailClearance			= 2;//1提醒
			    		int messageClearance		= 2;
			    		int pageClearance			= 2;	
			   		 	if(null != scheduleClearance){
				   		 	mailClearance			= scheduleClearance.get("sms_email_notify",2);//1提醒
				    		messageClearance		= scheduleClearance.get("sms_short_notify",2);
				    		pageClearance			= scheduleClearance.get("is_need_replay",2);
				    		
			    		}
			   		 //System.out.println(mailClearance+"---"+messageClearance+"---"+pageClearance);
			    		
			    	%>
			   		<input type="checkbox" name="isMailClearance" <%if(1 == mailClearance || 2 == mailClearance){out.print("checked='checked'");}%>/>邮件
			   		<input type="checkbox" name="isMessageClearance"  <%if(1 == messageClearance || 2 == messageClearance){out.print("checked='checked'");}%>/>短信
			   		<input type="checkbox" name="isPageClearance"  <%if(1 == pageClearance || 2 == pageClearance){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	        <tr height="25px">
	          <td width="13%" align="right" class="STYLE2">图片流程:</td>
	          <td width="87%">
	          <%
	          	if(RepairProductFileKey.NOPRODUCTFILE == product_file || RepairProductFileKey.PRODUCTFILE == product_file || 0 == product_file){
	          %>		
	          	 <input type="radio" name="radio_productFile" value="1" <%if(1 == product_file){out.print("checked='checked'");}%>/> <%= repairProductFileKey.getStatusById(1)%>
	          	 <input type="radio" name="radio_productFile" value="2" <%if(0 == product_file || 2 == product_file){out.print("checked='checked'");}%>/> 需要实物图片流程
	          <%	
	          	}else{
	          		out.print(repairProductFileKey.getStatusById(product_file));
	          	}
	          %>
	          </td>
	        </tr>
	          <%
			 	String productFilePersonTrStyle = "display:none";
		   	 	if(1 != product_file){
		   	 		productFilePersonTrStyle = "";
		    	}
			 %>
	        <tr height="25px" style="<%=productFilePersonTrStyle %>" id="productFilePersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input type="text" id="adminUserNamesProductFile" name="adminUserNamesProductFile" value='<%=adminUserNamesProductFile %>' style="width:180px;" onclick="adminUserProductFile()"/>
			    	通知：
			    	<%
			    		DBRow scheduleProductFile	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(repair_order_id), ModuleKey.REPAIR_ORDER, 10);
				    	int mailProductFile			= 2;//1提醒
			    		int messageProductFile		= 2;
			    		int pageProductFile			= 2;	
			   		 	if(null != scheduleProductFile){
				   		 	mailProductFile			= scheduleProductFile.get("sms_email_notify",2);//1提醒
				    		messageProductFile		= scheduleProductFile.get("sms_short_notify",2);
				    		pageProductFile			= scheduleProductFile.get("is_need_replay",2);
			    		}
			   		// System.out.println(mailProductFile+"---"+messageProductFile+"---"+pageProductFile);
			    		
			    	%>
			   		<input type="checkbox" name="isMailProductFile" <%if(1 == mailProductFile || 2 == mailProductFile){out.print("checked='checked'");}%>/>邮件
			   		<input type="checkbox" name="isMessageProductFile" <%if(1 == messageProductFile || 2 == messageProductFile){out.print("checked='checked'");}%>/>短信
			   		<input type="checkbox" name="isPageProductFile" <%if(1 == pageProductFile || 2 == pageProductFile){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	        <tr height="25px">
        	  <td width="13%" align="right" class="STYLE2">制签:</td>
	          <td width="87%" align="left" valign="middle" id="transportTr">
	          <%
	          	if(RepairTagKey.NOTAG == tag || RepairTagKey.TAG == tag || 0 == tag){
	          %>	
	          	<input type="radio" name="radio_tag" value="1" <%if(1 == tag){out.print("checked='checked'");}%>/> <%= repairTagKey.getRepairTagById(1) %>
	          	<input type="radio" name="radio_tag" value="2" <%if(0 == tag || 2 == tag){out.print("checked='checked'");}%>/> <%= repairTagKey.getRepairTagById(2) %>
	          <%	
	          	}else{
	          		out.print(repairTagKey.getRepairTagById(tag));
	          	}
	          %>
	          </td>
	        </tr>
	         <%
			 	String tagPersonTrStyle = "display:none";
		   	 	if(1 != tag){
		   	 		tagPersonTrStyle = "";
		    	}
			 %>
	        <tr height="25px" style="<%=tagPersonTrStyle %>" id="tagPersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input type="text" id="adminUserNamesTag" name="adminUserNamesTag" value='<%=adminUserNamesTag %>' style="width:180px;" onclick="adminUserTag()"/>
			    	通知：
			    	<%
			    		DBRow scheduleTag	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(repair_order_id), ModuleKey.REPAIR_ORDER, 8);
				    	int mailTag			= 2;//1提醒
			    		int messageTag		= 2;
			    		int pageTag			= 2;	
			   		 	if(null != scheduleTag){
				   		 	mailTag			= scheduleTag.get("sms_email_notify",2);//1提醒
				    		messageTag		= scheduleTag.get("sms_short_notify",2);
				    		pageTag			= scheduleTag.get("is_need_replay",2);
			    		}
			   		// System.out.println(mailTag+"---"+messageTag+"---"+pageTag);
			    	%>
			   		<input type="checkbox" name="isMailTag" <%if(1 == mailTag || 2 == mailTag){out.print("checked='checked'");}%>/>邮件
			   		<input type="checkbox" name="isMessageTag" <%if(1 == messageTag || 2 == messageTag){out.print("checked='checked'");}%>/>短信
			   		<input type="checkbox" name="isPageTag" <%if(1 == pageTag || 2 == pageTag){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	        <tr height="25px">
	       	  <td width="13%" align="right" class="STYLE2">运费流程:</td>
	          <td width="87%" align="left" valign="middle" id="stockInSetTr">
	          <%
	          	if(RepairStockInSetKey.SHIPPINGFEE_NOTSET == stock_in_set || RepairStockInSetKey.SHIPPINGFEE_SET == stock_in_set || 0 == stock_in_set){
	          %>
	          	 <input type="radio" name="stock_in_set_radio" value="1" <%if(1 == stock_in_set){out.print("checked='checked'");}%>/> <%= repairStockInSetKey.getStatusById(1) %>
	          	 <input type="radio" name="stock_in_set_radio" value="2" <%if(0 == stock_in_set || 2 == stock_in_set){out.print("checked='checked'");}%>/> <%= repairStockInSetKey.getStatusById(2) %>	
	          <%		
	          	}else{
	          		out.print(repairStockInSetKey.getStatusById(stock_in_set));
	          	}
	          %>
	          </td>
	        </tr>
	          <%
			 	String stockInSetPersonTrStyle = "display:none";
		   	 	if(1 != stock_in_set){
		   	 	stockInSetPersonTrStyle = "";
		    	}
			 %>
	        <tr height="25px" style="<%=stockInSetPersonTrStyle %>" id="stockInSetPersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input type="text" id="adminUserNamesStockInSet" name="adminUserNamesStockInSet" value='<%=adminUserNamesStockInSet %>' style="width:180px;" onclick="adminUserStockInSet()"/>
			    	通知：
			    	<%
			    		DBRow scheduleStockInSet	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(repair_order_id), ModuleKey.REPAIR_ORDER, 5);
				    	int mailStockInSet			= 2;//1提醒
			    		int messageStockInSet		= 2;
			    		int pageStockInSet			= 2;	
			   		 	if(null != scheduleStockInSet){
				   		 	mailStockInSet			= scheduleStockInSet.get("sms_email_notify",2);//1提醒
				    		messageStockInSet		= scheduleStockInSet.get("sms_short_notify",2);
				    		pageStockInSet			= scheduleStockInSet.get("is_need_replay",2);
			    		}
			   		 //System.out.println(mailStockInSet+"---"+messageStockInSet+"---"+pageStockInSet);
			    	%>
			   		<input type="checkbox" name="isMailStockInSet" <%if(1 == mailStockInSet || 2 == mailStockInSet){out.print("checked='checked'");}%>/>邮件
			   		<input type="checkbox" name="isMessageStockInSet" <%if(1 == messageStockInSet || 2 == messageStockInSet){out.print("checked='checked'");}%>/>短信
			   		<input type="checkbox" name="isPageStockInSet" <%if(1 == pageStockInSet || 2 == pageStockInSet){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	        <tr height="25px">
	          <td width="13%" align="right" class="STYLE2">单证:</td>
	          <td width="87%" align="left" valign="middle" id="certificateTr">
	          <%
	          	if(RepairCertificateKey.NOCERTIFICATE == certificate || RepairCertificateKey.CERTIFICATE == certificate || 0 == certificate){
	          %>
	          	<input type="radio" name="radio_certificate" value="1" <%if(1 == certificate){out.print("checked='checked'");}%>/> <%= repairCertificateKey.getStatusById(1) %>
	          	<input type="radio" name="radio_certificate" value="2" <%if(0 == certificate || 2 == certificate){out.print("checked='checked'");}%>/> 需要单证
	          <%		
	          	}else{
	          		out.print(repairCertificateKey.getStatusById(certificate));
	          	}
	          %>
	          </td>
	        </tr>
	         <%
			 	String certificatePersonTrStyle = "display:none";
		   	 	if(1 != certificate){
		   	 		certificatePersonTrStyle = "";
		    	}
			 %>
	        <tr height="25px" style="<%=certificatePersonTrStyle %>" id="certificatePersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input type="text" id="adminUserNamesCertificate" name="adminUserNamesCertificate" value='<%=adminUserNamesCertificate %>' style="width:180px;" onclick="adminUserCertificate()"/>
			    	通知：
			    	<%
			    		DBRow scheduleCertificate	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(repair_order_id), ModuleKey.REPAIR_ORDER, 9);
				    	int mailCertificate			= 2;//1提醒
			    		int messageCertificate		= 2;
			    		int pageCertificate			= 2;	
			   		 	if(null != scheduleCertificate){
				   		 	mailCertificate			= scheduleCertificate.get("sms_email_notify",2);//1提醒
				    		messageCertificate		= scheduleCertificate.get("sms_short_notify",2);
				    		pageCertificate			= scheduleCertificate.get("is_need_replay",2);
			    		}
			   		 //System.out.println(mailCertificate+"---"+messageCertificate+"---"+pageCertificate);
			    	%>
			   		<input type="checkbox" name="isMailCertificate" <%if(1 == mailCertificate || 2 == mailCertificate){out.print("checked='checked'");}%>/>邮件
			   		<input type="checkbox" name="isMessageCertificate" <%if(1 == messageCertificate || 2 == messageCertificate){out.print("checked='checked'");}%>/>短信
			   		<input type="checkbox" name="isPageCertificate" <%if(1 == pageCertificate || 2 == pageCertificate){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	        <tr height="25px">
	          <td width="13%" align="right" class="STYLE2">质检流程:</td>
	          <td width="87%" align="left" valign="middle" id="qualityInspectionTr">
	          <%
	          	if(RepairQualityInspectionKey.NO_NEED_QUALITY == quality_inspection || RepairQualityInspectionKey.NEED_QUALITY == quality_inspection || 0 == quality_inspection){
	          %>
	          <input type="radio" name="radio_qualityInspection" value="1" <%if(1 == quality_inspection){out.print("checked='checked'");}%>/> 无需质检报告
	          <input type="radio" name="radio_qualityInspection" value="2" <%if(0 == quality_inspection || 2 == quality_inspection){out.print("checked='checked'");}%>/>	 需要质检报告
	          <%
	          	}else{
	          		out.print(repairQualityInspectionKey.getStatusById(quality_inspection+""));
	          	}
	          %>
	          </td>
	        </tr>
	        <%
			 	String qualityInspectionPersonTrStyle = "display:none";
		   	 	if(1 != quality_inspection){
		   	 		qualityInspectionPersonTrStyle = "";
		    	}
			 %>
	        <tr height="25px;" style="<%=qualityInspectionPersonTrStyle %>" id="qualityInspectionPersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input type="text" id="adminUserNamesQualityInspection" name="adminUserNamesQualityInspection" value='<%=adminUserNamesQualityInspection %>' style="width:180px;" onclick="adminUserQualityInspection()"/>
			    	通知：
			    	<%
			    		DBRow scheduleQualityInspection	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(repair_order_id), ModuleKey.REPAIR_ORDER, 11);
				    	int mailQualityInspection			= 2;//1提醒
			    		int messageQualityInspection		= 2;
			    		int pageQualityInspection			= 2;	
			   		 	if(null != scheduleQualityInspection){
				   		 	mailQualityInspection			= scheduleQualityInspection.get("sms_email_notify",2);//1提醒
				    		messageQualityInspection		= scheduleQualityInspection.get("sms_short_notify",2);
				    		pageQualityInspection			= scheduleQualityInspection.get("is_need_replay",2);
			    		}
			   		 //System.out.println(mailQualityInspection+"---"+messageQualityInspection+"---"+pageQualityInspection);
			    	%>
			   		<input type="checkbox" name="isMailQualityInspection" <%if(1 == mailQualityInspection || 2 == mailQualityInspection){out.print("checked='checked'");}%>/>邮件
			   		<input type="checkbox" name="isMessageQualityInspection" <%if(1 == messageQualityInspection || 2 == messageQualityInspection){out.print("checked='checked'");}%>/>短信
			   		<input type="checkbox" name="isPageQualityInspection" <%if(1 == pageQualityInspection || 2 == pageQualityInspection){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
		</table>
	</fieldset>	
	</td>
  </tr>
</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
    <%
    	if(2 != isOutter){
    %>
    	 <input name="insert" type="button" class="normal-green-long" onclick="previousStep()" value="上一步" >
    	 <input name="insert" type="button" class="normal-green-long" onclick="submitApply(1)" value="下一步" >
    <%
    	}else{
    %>		
    	<input name="insert" type="button" class="normal-green-long" onclick="submitApply(1)" value="完成" >
    <%
    	}
    %>
   
    <%
    //	if(finished == 1) {
    %>
      		
    <%
    //	}else {
    %>
<%--    		<input name="insert" type="button" class="normal-green-long" onclick="submitApply(1)" value="完成" >--%>
    <%
    //	}
    %>
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
</form>
<form name="previousStepForm" id="previousStepForm" action="<%=previousUrl%>" method="post">
</form>
<script type="text/javascript">
<!--
	function submitApply(finished)
	{
		$("#finished").val(finished);
		document.apply_money_form.declaration.value = $("input:radio[name=radio_declaration]:checked").val();
		document.apply_money_form.clearance.value = $("input:radio[name=radio_clearance]:checked").val();
		document.apply_money_form.product_file.value = $("input:radio[name=radio_productFile]:checked").val();
		document.apply_money_form.tag.value = $("input:radio[name=radio_tag]:checked").val();
		document.apply_money_form.stock_in_set.value = $("input:radio[name=stock_in_set_radio]:checked").val();
		//alert(document.apply_money_form.stock_in_set.value);
		document.apply_money_form.certificate.value = $("input:radio[name=radio_certificate]:checked").val();
		document.apply_money_form.quality_inspection.value = $("input:radio[name=radio_qualityInspection]:checked").val();

		if($("input:checkbox[name=isMailTransport]").attr("checked")){
			document.apply_money_form.needMailTransport.value = 2;
		}
		if($("input:checkbox[name=isMessageTransport]").attr("checked")){
			document.apply_money_form.needMessageTransport.value = 2;
		}
		if($("input:checkbox[name=isPageTransport]").attr("checked")){
			document.apply_money_form.needPageTransport.value = 2;
		}
		
		if($("input:checkbox[name=isMailDeclaration]").attr("checked")){
			document.apply_money_form.needMailDeclaration.value = 2;
		}
		if($("input:checkbox[name=isMessageDeclaration]").attr("checked")){
			document.apply_money_form.needMessageDeclaration.value = 2;
		}
		if($("input:checkbox[name=isPageDeclaration]").attr("checked")){
			document.apply_money_form.needPageDeclaration.value = 2;
		}

		if($("input:checkbox[name=isMailClearance]").attr("checked")){
			document.apply_money_form.needMailClearance.value = 2;
		}
		if($("input:checkbox[name=isMessageClearance]").attr("checked")){
			document.apply_money_form.needMessageClearance.value = 2;
		}
		if($("input:checkbox[name=isPageClearance]").attr("checked")){
			document.apply_money_form.needPageClearance.value = 2;
		}

		if($("input:checkbox[name=isMailProductFile]").attr("checked")){
			document.apply_money_form.needMailProductFile.value = 2;
		}
		if($("input:checkbox[name=isMessageProductFile]").attr("checked")){
			document.apply_money_form.needMessageProductFile.value = 2;
		}
		if($("input:checkbox[name=isPageProductFile]").attr("checked")){
			document.apply_money_form.needPageProductFile.value = 2;
		}

		if($("input:checkbox[name=isMailTag]").attr("checked")){
			document.apply_money_form.needMailTag.value = 2;
		}
		if($("input:checkbox[name=isMessageTag]").attr("checked")){
			document.apply_money_form.needMessageTag.value = 2;
		}
		if($("input:checkbox[name=isPageTag]").attr("checked")){
			document.apply_money_form.needPageTag.value = 2;
		}

		if($("input:checkbox[name=isMailStockInSet]").attr("checked")){
			document.apply_money_form.needMailStockInSet.value = 2;
		}
		if($("input:checkbox[name=isMessageStockInSet]").attr("checked")){
			document.apply_money_form.needMessageStockInSet.value = 2;
		}
		if($("input:checkbox[name=isPageStockInSet]").attr("checked")){
			document.apply_money_form.needPageStockInSet.value = 2;
		}

		if($("input:checkbox[name=isMailCertificate]").attr("checked")){
			document.apply_money_form.needMailCertificate.value = 2;
		}
		if($("input:checkbox[name=isMessageCertificate]").attr("checked")){
			document.apply_money_form.needMessageCertificate.value = 2;
		}
		if($("input:checkbox[name=isPageCertificate]").attr("checked")){
			document.apply_money_form.needPageCertificate.value = 2;
		}

		if($("input:checkbox[name=isMailQualityInspection]").attr("checked")){
			document.apply_money_form.needMailQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isMessageQualityInspection]").attr("checked")){
			document.apply_money_form.needMessageQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isPageQualityInspection]").attr("checked")){
			document.apply_money_form.needPageQualityInspection.value = 2;
		}
		
		if($("input:radio[name=radio_declaration]:checked") && $("input:radio[name=radio_declaration]:checked").val() && '<%=RepairDeclarationKey.DELARATION %>' == $("input:radio[name=radio_declaration]:checked").val() && $("#adminUserNamesDeclaration").val() == '')
		{
			alert("出口报关负责人不能为空");
		}
		else if($("input:radio[name=radio_clearance]:checked") && $("input:radio[name=radio_clearance]:checked").val() && '<%=RepairClearanceKey.CLEARANCE%>' == $("input:radio[name=radio_clearance]:checked").val() && $("#adminUserNamesClearance").val() == '')
		{
			alert("进口清关负责人不能为空");
		}
		else if($("input:radio[name=radio_productFile]:checked") && $("input:radio[name=radio_productFile]:checked").val() && '<%=RepairProductFileKey.PRODUCTFILE %>' == $("input:radio[name=radio_productFile]:checked").val() && $("#adminUserNamesProductFile").val() == '')
		{
			alert("实物图片负责人不能为空");
		}
		else if($("input:radio[name=radio_tag]:checked") && $("input:radio[name=radio_tag]:checked").val() && '<%=RepairTagKey.TAG %>' == $("input:radio[name=radio_tag]:checked").val() && $("#adminUserNamesTag").val() == '')
		{
			alert("制签负责人不能为空");
		}
		else if($("input:radio[name=stock_in_set_radio]:checked") && $("input:radio[name=stock_in_set_radio]:checked").val() && '<%=RepairStockInSetKey.SHIPPINGFEE_SET %>' == $("input:radio[name=stock_in_set_radio]:checked").val() && $("#adminUserNamesStockInSet").val() == '')
		{
			alert("运费流程负责人不能为空");
		}
		else if($("input:radio[name=radio_certificate]:checked") && $("input:radio[name=radio_certificate]:checked").val() && '<%=RepairCertificateKey.CERTIFICATE %>' == $("input:radio[name=radio_certificate]:checked").val() && $("#adminUserNamesCertificate").val() == '')
		{
			alert("单证流程负责人不能为空");
		}
		else if($("input:radio[name=radio_qualityInspection]:checked") && $("input:radio[name=radio_qualityInspection]:checked").val() && '<%=RepairQualityInspectionKey.NEED_QUALITY %>' == $("input:radio[name=radio_qualityInspection]:checked").val() && $("#adminUserNamesQualityInspection").val() == '')
		{
			alert("质检流程负责人不能为空");
		}
		else
		{
		document.apply_money_form.submit();
		}
		
	}
	function closeWindow(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	}
	function previousStep(){
		document.previousStepForm.submit();
	};
//-->
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 

</script>
</body>
</html>

