<%@ page language="java"  pageEncoding="utf-8"%>

<%@ include file="../../include.jsp"%>
<%
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(20);
	String repair_order_id = StringUtil.getString(request,"repair_order_id");
	long purchase_id	= StringUtil.getLong(request, "purchase_id");
	String fr_id = StringUtil.getString(request,"fr_id");
	DBRow[] rows = repairOrderMgrZyj.getRepairFreightCostByRepairId(repair_order_id);//更新数据
	int isOutter = StringUtil.getInt(request, "isOutter");
%>
<html>
<head>
<title>修改运费</title>
	<link href="../comm.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" src="../../common.js"></script>

	<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	<script type="text/javascript" src="../js/select.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	
<script>
	var updated = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated").equals("1")) {
	%>
			updated = true;
	<%
		}
	%>
	function init() {
		if(updated) {
			parent.location.reload();
			closeWindow();
		}
	}
	
	$(document).ready(function(){
		init();
	});

	function setFreightCost(fc_id,fc_project_name,fc_way,fc_unit) {
		addRow(fc_id,fc_project_name,fc_way,fc_unit);
		$('#changed').val('2');
	}
	
	function setRate(obj) {
		if(obj.value == 'RMB') {
			document.getElementsByName("tfc_exchange_rate")[$(obj).attr('index')].value = "1.0";
		}else {
			document.getElementsByName("tfc_exchange_rate")[$(obj).attr('index')].value = "0.0";
		}
	}
	
	function addRow(fc_id,fc_project_name,fc_way,fc_unit) {
		var index = document.getElementById("tables").rows.length;
    	var row = document.getElementById("tables").insertRow(index);
    	var add1=row.insertCell(0).innerHTML = "<input type='hidden' name='tfc_id' id='tfc_id'>";
    	add1=row.insertCell(0).innerHTML += "<input type='hidden' name='tfc_project_name' id='tfc_project_name' value='"+fc_project_name+"'>";
    	add1=row.insertCell(0).innerHTML += "<input type='hidden' name='tfc_way' id='tfc_way' value='"+fc_way+"'>";
    	add1=row.insertCell(0).innerHTML += "<input type='hidden' name='tfc_unit' id='tfc_unit' value='"+fc_unit+"'>";
    	add1=row.insertCell(0).innerHTML += fc_project_name;
		var add2=row.insertCell(1).innerHTML = fc_way;
		var add3=row.insertCell(2).innerHTML = fc_unit;
		var add4=row.insertCell(3).innerHTML = "<input type='text' size='8' name='tfc_unit_price' id='tfc_unit_price' value='0.0'>";
		var add5=row.insertCell(4).innerHTML = "<select id='tfc_currency' index='"+(index-2)+"' name='tfc_currency' onchange='setRate(this)'>"+createCurrency(fc_unit)+"</select>";
		var add6=row.insertCell(5).innerHTML = "<input type='text' size='8' name='tfc_exchange_rate' id='tfc_exchange_rate' value='1.0'>";
		var add7=row.insertCell(6).innerHTML = "<input type='text' size='8' name='tfc_unit_count' id='tfc_unit_count' value='0.0'>";
		var add8=row.insertCell(7).innerHTML = "<input type='button' value='删除' onclick='deleteRow(this)'/>";
		//让回来的币种都默认选中
	}
	function createCurrency(fc_unit){
		var array = ['RMB','USD','HKD'];
		var options = "" ;
	 	for(var index = 0 , count = array.length  ; index < count ; index++ ){
			var selected = "" ;
		 	if(fc_unit.toUpperCase() === array[index]){
		 	   selected = "selected";
			}
			options += "<option "+selected+" value='"+array[index]+"'>"+array[index]+"</option>";
		}
		return  options;
    }

    function deleteRow(input){  
        var s=input.parentNode.parentNode.rowIndex;
        document.getElementById("tables").deleteRow(s); 
        $('#changed').val('2');
        //var num=document.getElementById("tables").rows.length;  
	}

    function selectFreigthCost() {
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_cost/freight_cost_select.html";
		freightSelectWindow = $.artDialog.open(url, {title: '运输资源选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function setFreigth() {
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/repairSetFreightAction.action',
			data:$("#freight_form").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag == "success"){
					showMessage("修改成功","success");
					setTimeout("windowClose()", 1000);
				}else{
					showMessage("修改失败,修改后的总额不能小于已申请总额:"+data.apply_freight,"error");
				}
			},
			error:function(){
				showMessage("系统错误","error");
			}
		})	
	}

	function setFreigthSubmit() {
		//$('#stock_in_set').val('3');
		freight_form.submit();
	}
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function cancelFreigthSubmit() {
		//$('#stock_in_set').val('2');
		freight_form.submit();
	}
</script>
</head>

<body onload="onLoadInitZebraTable()">
<form name="freight_form" id="freight_form" action="">
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/setRepairFreight.html?repair_order_id=<%=repair_order_id%>&updated=1"/>
	<input type="hidden" name="repair_order_id" id="repair_order_id" value="<%=repair_order_id %>">
	<input type="hidden" name="purchase_id" value='<%=purchase_id %>'/>
	<input type="hidden" name="isOutter" id="isOutter" value="<%=isOutter%>"/>
	<input type="hidden" name="changed" id="changed" value="1">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			<legend style="font-size:15px;font-weight:normal;color:#999999;">货运费用信息</legend>
			<table id="tables" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" style="padding-top: 17px" class="zebraTable" id="stat_table">
			 <tr>
		     	<td align="right" colspan="8" height="30px"><input type="button" class="long-button" name="selectFreightCost" value="选择运费项目" onclick="selectFreigthCost()"></td>
		     </tr>
		     <tr>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>
		       <th width="25%" nowrap="nowrap" class="right-title"  style="text-align: center;"></th>	
		     </tr>
		     <%
		     if(rows!=null){
			     for(int i = 0;i<rows.length;i++)
			     {
			    	 DBRow row = rows[i];
			    	 long tfc_id = row.get("tfc_id",0l);
		    		 double tfc_unit_count = row.get("tfc_unit_count",0d);
		    		 String tfc_project_name = "";
		    		 String tfc_way = "";
		    		 String tfc_unit = "";
		    		 String tfc_currency = "";
		    		 double tfc_unit_price = 0;
		    		 double tfc_exchange_rate = 0;
			    	 if(tfc_id != 0) {//以前有数据
			    		 tfc_project_name = row.getString("tfc_project_name");
			    		 tfc_way = row.getString("tfc_way");
			    		 tfc_unit = row.getString("tfc_unit");
			    		 tfc_unit_price = row.get("tfc_unit_price",0d);
			    		 tfc_currency = row.getString("tfc_currency");
			    		 tfc_exchange_rate = row.get("tfc_exchange_rate",0d);
			    	 }
		      %>
		     <tr height="30px" style="padding-top:3px;">
		        <td valign="middle" style="padding-top:3px;">
		        	<input type="hidden" name="tfc_id" id="tfc_id" value="<%=tfc_id %>">
		        	<input type="hidden" name="tfc_project_name" id="tfc_project_name" value="<%=tfc_project_name %>">
		        	<input type="hidden" name="tfc_way" id="tfc_way" value="<%=tfc_way %>">
		        	<input type="hidden" name="tfc_unit" id="tfc_unit" value="<%=tfc_unit %>">
		        	
					<%=tfc_project_name %>	
		        </td>
		         <td valign="middle" nowrap="nowrap" >       
		      		<%=tfc_way %>
		        </td>
		        <td align="left" valign="middle">
					<%=tfc_unit %>
		        </td>
		        <td >
					<input type="text" size="8" name="tfc_unit_price" id="tfc_unit_price" value="<%=tfc_unit_price %>">
		        </td>
		        <td >
					<select id="tfc_currency" name="tfc_currency" index="<%=i %>" onclick="setRate(this)">
						<option value="RMB" <%=tfc_currency.equals("RMB")?"selected":""%>>RMB</option>
						<option value="USD" <%=tfc_currency.equals("USD")?"selected":""%>>USD</option>
						<option value="HKD" <%=tfc_currency.equals("HKD")?"selected":""%>>HKD</option>
					</select>
		        </td>
		        <td >
					<input type="text" size="8" name="tfc_exchange_rate" id="tfc_exchange_rate" value="<%=tfc_exchange_rate %>">
		        </td>
		        <td valign="middle" >       
					<input type="text" size="8" name="tfc_unit_count" id="tfc_unit_count" value="<%=tfc_unit_count %>">
		        </td>
				<td>
		     		<input type="button" value="删除" onclick="deleteRow(this)"/>
		     	</td>
		     </tr>
		     <%
		    	 }
		     }
		     %>
		  </table>
		  <br>
<%--		  <input type="hidden" name="stock_in_set" id="stock_in_set" value="1">--%>
		</fieldset>	
	</td>
  </tr>
</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
		<input name="insert" type="button" class="normal-green-long" onclick="setFreigth()" value="保存" >
		<input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
</form>
<form name="previousStepForm" id="previousStepForm" action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_freight_update.html?repair_order_id=<%=repair_order_id %>" method="post">
</form>
<script type="text/javascript">
<!--
	function closeWindow(){
		$.artDialog.close();
		//self.close();
	}
	function previousStep(){
		document.previousStepForm.submit();
	};
//-->
</script>
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>

