<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.RepairDeclarationKey"%>
<%@page import="com.cwc.app.key.RepairClearanceKey"%>
<%@page import="com.cwc.app.key.RepairTagKey"%>
<%@page import="com.cwc.app.key.RepairCertificateKey"%>
<%@page import="com.cwc.app.key.RepairQualityInspectionKey"%>
<%@page import="com.cwc.app.key.RepairStockInSetKey"%>
<%@page import="com.cwc.app.key.RepairProductFileKey"%>

<jsp:useBean id="repairDeclarationKey" class="com.cwc.app.key.RepairDeclarationKey"/>
<jsp:useBean id="repairClearanceKey" class="com.cwc.app.key.RepairClearanceKey"/>
<jsp:useBean id="repairTagKey" class="com.cwc.app.key.RepairTagKey"/>
<jsp:useBean id="repairCertificateKey" class="com.cwc.app.key.RepairCertificateKey"/>
<jsp:useBean id="repairQualityInspectionKey" class="com.cwc.app.key.RepairQualityInspectionKey"/>
<jsp:useBean id="repairStockInSetKey" class="com.cwc.app.key.RepairStockInSetKey"/>
<jsp:useBean id="repairProductFileKey" class="com.cwc.app.key.RepairProductFileKey"/>

<%
	DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
	DBRow countrycode[] = orderMgr.getAllCountryCode();
									
	String selectBg="#ffffff";
	String preLetter="";
	
	String repair_order_id_last = repairOrderMgrZyj.getRepairLastTimeCreateByAdid(request);
	
	
	//查询各流程任务的负责人
	String adminUserIdsTransport		= "";
	String adminUserNamesTransport		= "";
	String adminUserIdsDeclaration		= "";
	String adminUserNamesDeclaration	= "";
	String adminUserIdsClearance		= "";
	String adminUserNamesClearance		= "";
	String adminUserIdsProductFile		= "";
	String adminUserNamesProductFile	= "";
	String adminUserIdsTag				= "";
	String adminUserNamesTag			= "";
	String adminUserIdsStockInSet		= "";
	String adminUserNamesStockInSet		= "";
	String adminUserIdsCertificate		= "";
	String adminUserNamesCertificate	= "";
	String adminUserIdsQualityInspection	= "";
	String adminUserNamesQualityInspection	= "";
	
	if(!"".equals(repair_order_id_last))
	{
		adminUserIdsTransport		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 4);
		adminUserNamesTransport		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 4);
		adminUserIdsDeclaration		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 7);
		adminUserNamesDeclaration	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 7);
		adminUserIdsClearance		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 6);
		adminUserNamesClearance		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 6);
		adminUserIdsProductFile		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 10);
		adminUserNamesProductFile	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 10);
		adminUserIdsTag				= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 8);
		adminUserNamesTag			= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 8);
		adminUserIdsStockInSet		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 5);
		adminUserNamesStockInSet	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 5);
		adminUserIdsCertificate		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 9);
		adminUserNamesCertificate	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 9);
		adminUserIdsQualityInspection	= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 11);
		adminUserNamesQualityInspection	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(repair_order_id_last, ModuleKey.REPAIR_ORDER, 11);
	}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增返修单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<!-- dialog -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
	jQuery(function($){
		$('#repair_receive_date').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
		});
		$("#ui-datepicker-div").css("display","none");
	});
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	function stateDiv(pro_id)
	{
		if(pro_id==-1)
		{
			$("#state_div").css("display","inline");
		}
		else
		{
			$("#state_div").css("display","none");
		}
	}
	
	//国家地区切换
function getStorageProvinceByCcid(ccid,pro_id,id)
{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#"+id).clearAll();
					$("#"+id).addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#"+id).addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#"+id).addOption("手工输入","-1");
					}
				});
				
				if (pro_id!=""&&pro_id!=0)
				{
					$("#"+id).setSelectedValue(pro_id);
					stateDiv(pro_id);
				}
}

	function selectDeliveryStorage(ps_id)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{ps_id:ps_id},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data){
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					//$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.city);
					$("#deliver_ccid").val(data["deliver_nation"]);
					
					getStorageProvinceByCcid($("#deliver_ccid").val(),data["deliver_pro_id"],"deliver_pro_id");
					
					$("#deliver_name").val(data["deliver_contact"]);
					$("#deliver_linkman_phone").val(data["deliver_phone"]);
				}
			});
	}
	
	function selectSendStorage(ps_id)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					ps_id:ps_id
				},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data)
				{
					$("#send_house_number").val(data["send_house_number"]);
					$("#send_street").val(data["send_street"]);
					//$("#send_address3").val(data.deliver_address3);
					$("#send_zip_code").val(data.send_zip_code);
					$("#send_name").val(data.send_contact);
					$("#send_linkman_phone").val(data.send_phone);
					$("#send_city").val(data.send_city);
					$("#send_ccid").val(data["send_nation"]);
					
					getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id");
				}
			});
	}
	
	//检查提货地址
	function checkSend()
	{
		if($("#psid").val()==0)
		{
			alert("请选择提货仓库");
			return false;
		}
		else if($("#send_ccid").val()==0)
		{
			alert("请选择提货国家");
			return false;
			
		}
		else if($("#send_city").val()=="")
		{
			alert("请填写提货城市");
			$("#send_city").focus();
			return false;
		}
		else if($("#send_house_number").val()==""||$("#send_street").val()=="")
		{
			alert("请填写提货地址");
			return false;
		}
		else if($("#send_zip_code").val()=="")
		{
			alert("请填写提货地邮编");
			$("#send_zip_code").focus();
			return false;
		}
		else if($("#send_name").val()=="")
		{
			alert("请填写提货联系人");
			$("#send_name").focus();
			return false;
		}
	}
	
	//检查交货地址
	function checkReceive()
	{
		if($("#receive_psid").val()==0)
		{
			alert("请选择收货仓库");
			return false;
		}
		else if($("#deliver_ccid").val()==0)
		{
			alert("请选择收货国家");
			return false;
		}
		else if($("#deliver_city").val()=="")
		{
			alert("请填写收货城市");
			$("#deliver_city").focus();
			return false;
		}
		else if($("#deliver_house_number").val()==""||$("#deliver_street").val()=="")
		{
			alert("请填写收货地址");
			return false;
		}
		else if($("#deliver_zip_code").val()=="")
		{
			alert("交货地址邮编");
			$("#deliver_zip_code").focus();
			return false;
		}
		else if($("#deliver_name").val()=="")
		{
			alert("请填写交货联系人");
			$("#deliver_name").focus();
			return false;
		}
	}
	
	//检查流程
	function checkProcedures()
	{
		document.add_form.declaration.value = $("input:radio[name=radio_declaration]:checked").val();
		document.add_form.clearance.value = $("input:radio[name=radio_clearance]:checked").val();
		document.add_form.product_file.value = $("input:radio[name=radio_productFile]:checked").val();
		document.add_form.tag.value = $("input:radio[name=radio_tag]:checked").val();
		document.add_form.stock_in_set.value = $("input:radio[name=stock_in_set_radio]:checked").val();

		document.add_form.certificate.value = $("input:radio[name=radio_certificate]:checked").val();
		document.add_form.quality_inspection.value = $("input:radio[name=radio_qualityInspection]:checked").val();

		if($("input:checkbox[name=isMailTransport]").attr("checked")){
			document.add_form.needMailTransport.value = 2;
		}
		if($("input:checkbox[name=isMessageTransport]").attr("checked")){
			document.add_form.needMessageTransport.value = 2;
		}
		if($("input:checkbox[name=isPageTransport]").attr("checked")){
			document.add_form.needPageTransport.value = 2;
		}
		
		if($("input:checkbox[name=isMailDeclaration]").attr("checked")){
			document.add_form.needMailDeclaration.value = 2;
		}
		if($("input:checkbox[name=isMessageDeclaration]").attr("checked")){
			document.add_form.needMessageDeclaration.value = 2;
		}
		if($("input:checkbox[name=isPageDeclaration]").attr("checked")){
			document.add_form.needPageDeclaration.value = 2;
		}

		if($("input:checkbox[name=isMailClearance]").attr("checked")){
			document.add_form.needMailClearance.value = 2;
		}
		if($("input:checkbox[name=isMessageClearance]").attr("checked")){
			document.add_form.needMessageClearance.value = 2;
		}
		if($("input:checkbox[name=isPageClearance]").attr("checked")){
			document.add_form.needPageClearance.value = 2;
		}

		if($("input:checkbox[name=isMailProductFile]").attr("checked")){
			document.add_form.needMailProductFile.value = 2;
		}
		if($("input:checkbox[name=isMessageProductFile]").attr("checked")){
			document.add_form.needMessageProductFile.value = 2;
		}
		if($("input:checkbox[name=isPageProductFile]").attr("checked")){
			document.add_form.needPageProductFile.value = 2;
		}

		if($("input:checkbox[name=isMailTag]").attr("checked")){
			document.add_form.needMailTag.value = 2;
		}
		if($("input:checkbox[name=isMessageTag]").attr("checked")){
			document.add_form.needMessageTag.value = 2;
		}
		if($("input:checkbox[name=isPageTag]").attr("checked")){
			document.add_form.needPageTag.value = 2;
		}

		if($("input:checkbox[name=isMailStockInSet]").attr("checked")){
			document.add_form.needMailStockInSet.value = 2;
		}
		if($("input:checkbox[name=isMessageStockInSet]").attr("checked")){
			document.add_form.needMessageStockInSet.value = 2;
		}
		if($("input:checkbox[name=isPageStockInSet]").attr("checked")){
			document.add_form.needPageStockInSet.value = 2;
		}

		if($("input:checkbox[name=isMailCertificate]").attr("checked")){
			document.add_form.needMailCertificate.value = 2;
		}
		if($("input:checkbox[name=isMessageCertificate]").attr("checked")){
			document.add_form.needMessageCertificate.value = 2;
		}
		if($("input:checkbox[name=isPageCertificate]").attr("checked")){
			document.add_form.needPageCertificate.value = 2;
		}

		if($("input:checkbox[name=isMailQualityInspection]").attr("checked")){
			document.add_form.needMailQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isMessageQualityInspection]").attr("checked")){
			document.add_form.needMessageQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isPageQualityInspection]").attr("checked")){
			document.add_form.needPageQualityInspection.value = 2;
		}
		if($("#adminUserNamesTransport").val() == '')
		{
			alert("运输负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_declaration]:checked") && $("input:radio[name=radio_declaration]:checked").val() && '<%=RepairDeclarationKey.DELARATION %>' == $("input:radio[name=radio_declaration]:checked").val() && $("#adminUserNamesDeclaration").val() == '')
		{
			alert("出口报关负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_clearance]:checked") && $("input:radio[name=radio_clearance]:checked").val() && '<%=RepairClearanceKey.CLEARANCE%>' == $("input:radio[name=radio_clearance]:checked").val() && $("#adminUserNamesClearance").val() == '')
		{
			alert("进口清关负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_productFile]:checked") && $("input:radio[name=radio_productFile]:checked").val() && '<%=RepairProductFileKey.PRODUCTFILE %>' == $("input:radio[name=radio_productFile]:checked").val() && $("#adminUserNamesProductFile").val() == '')
		{
			alert("实物图片负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_tag]:checked") && $("input:radio[name=radio_tag]:checked").val() && '<%=RepairTagKey.TAG %>' == $("input:radio[name=radio_tag]:checked").val() && $("#adminUserNamesTag").val() == '')
		{
			alert("制签负责人不能为空");
			return false;
		}
		else if($("input:radio[name=stock_in_set_radio]:checked") && $("input:radio[name=stock_in_set_radio]:checked").val() && '<%=RepairStockInSetKey.SHIPPINGFEE_SET %>' == $("input:radio[name=stock_in_set_radio]:checked").val() && $("#adminUserNamesStockInSet").val() == '')
		{
			alert("运费流程负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_certificate]:checked") && $("input:radio[name=radio_certificate]:checked").val() && '<%=RepairCertificateKey.CERTIFICATE %>' == $("input:radio[name=radio_certificate]:checked").val() && $("#adminUserNamesCertificate").val() == '')
		{
			alert("单证流程负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_qualityInspection]:checked") && $("input:radio[name=radio_qualityInspection]:checked").val() && '<%=RepairQualityInspectionKey.NEED_QUALITY %>' == $("input:radio[name=radio_qualityInspection]:checked").val() && $("#adminUserNamesQualityInspection").val() == '')
		{
			alert("质检流程负责人不能为空");
			return false;
		}
	}
	
	$(document).ready(function()
	{
		$("input:radio[name^=radio_]").click(
				function(){
					var hasThisCheck = $(this).val();
					var hasThisName = $(this).attr("name");
					var presonTrName = hasThisName.split("_")[1]+"PersonTr";
					if(2 == hasThisCheck){
						$("#"+presonTrName).attr("style","");
					}else{
						$("#"+presonTrName).attr("style","display:none");
					}
				}
		);
		$("input:radio[name=stock_in_set_radio]").click(
				function(){
					var hasThisCheck = $(this).val();
					if(2 == hasThisCheck){
						$("#stockInSetPersonTr").attr("style","");
					}else{
						$("#stockInSetPersonTr").attr("style","display:none");
					}
				}
		);		
	});

	function adminUserTransport(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsTransport").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTransport'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTransport(user_ids , user_names){
		$("#adminUserIdsTransport").val(user_ids);
		$("#adminUserNamesTransport").val(user_names);
	}
	function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);};
	function adminUserDeclaration(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsDeclaration").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowDeclaration'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowDeclaration(user_ids , user_names){
		$("#adminUserIdsDeclaration").val(user_ids);
		$("#adminUserNamesDeclaration").val(user_names);
	}

	function adminUserClearance(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsClearance").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowClearance'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowClearance(user_ids , user_names){
		$("#adminUserIdsClearance").val(user_ids);
		$("#adminUserNamesClearance").val(user_names);
	}

	function adminUserProductFile(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsProductFile").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowProductFile'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowProductFile(user_ids , user_names){
		$("#adminUserIdsProductFile").val(user_ids);
		$("#adminUserNamesProductFile").val(user_names);
	}

	function adminUserTag(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsTag").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTag'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTag(user_ids , user_names){
		$("#adminUserIdsTag").val(user_ids);
		$("#adminUserNamesTag").val(user_names);
	}

	function adminUserStockInSet(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsStockInSet").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowStockInSet'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowStockInSet(user_ids , user_names){
		$("#adminUserIdsStockInSet").val(user_ids);
		$("#adminUserNamesStockInSet").val(user_names);
	}

	function adminUserCertificate(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsCertificate").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowCertificate'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowCertificate(user_ids , user_names){
		$("#adminUserIdsCertificate").val(user_ids);
		$("#adminUserNamesCertificate").val(user_names);
	}

	function adminUserQualityInspection(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsQualityInspection").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowQualityInspection'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowQualityInspection(user_ids , user_names){
		$("#adminUserIdsQualityInspection").val(user_ids);
		$("#adminUserNamesQualityInspection").val(user_names);
	};
	
	function openFreightSelect() 
	{
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_resources/freight_resources_select.html";
		freightSelectWindow = $.artDialog.open(url, {title: '运输资源选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function setFreight(fr_id,waybill_name,carriers,repairby,repairby_name, begin_country, begin_country_name, end_country, end_country_name, begin_port, end_port) 
	{
		$("#fr_id").val(fr_id);
		$("#repair_waybill_name").val(waybill_name);
		$("#carriers").val(carriers);	
		$("#repairby").val(repairby);
		$("#repairby_name").val(repairby_name);
		$("#repair_send_country").val(begin_country);
		$("#repair_send_country_name").val(begin_country_name);
		$("#repair_receive_country").val(end_country);
		$("#repair_receive_country_name").val(end_country_name);
		$("#repair_send_place").val(begin_port);
		$("#repair_receive_place").val(end_port);
	}
	
	function setFreightCost(fc_id,fc_project_name,fc_way,fc_unit) {
		addFreigthCost(fc_id,fc_project_name,fc_way,fc_unit);
		$('#changed').val('2');
	}
	
	function setRate(obj) {
		if(obj.value == 'RMB') {
			document.getElementsByName("tfc_exchange_rate")[$(obj).attr('index')].value = "1.0";
		}else {
			document.getElementsByName("tfc_exchange_rate")[$(obj).attr('index')].value = "0.0";
		}
	}
	
	function addFreigthCost(fc_id,fc_project_name,fc_way,fc_unit) 
	{
		var index = document.getElementById("freight_cost").rows.length;
    	var row = document.getElementById("freight_cost").insertRow(index);
    	row.insertCell(0).innerHTML = "<input type='hidden' name='tfc_id' id='tfc_id'>"
							    	 +"<input type='hidden' name='tfc_project_name' id='tfc_project_name' value='"+fc_project_name+"'>"
							    	 +"<input type='hidden' name='tfc_way' id='tfc_way' value='"+fc_way+"'>"
							    	 +"<input type='hidden' name='tfc_unit' id='tfc_unit' value='"+fc_unit+"'>"
							    	 +fc_project_name;
		
		row.insertCell(1).innerHTML = fc_way;
		row.insertCell(2).innerHTML = fc_unit;
		row.insertCell(3).innerHTML = "<input type='text' size='8' name='tfc_unit_price' id='tfc_unit_price' value='0.0'>";
		row.insertCell(4).innerHTML = "<select id='tfc_currency' index='"+(index-2)+"' name='tfc_currency' onchange='setRate(this)'>"+createCurrency(fc_unit)+"</select>";
		row.insertCell(5).innerHTML = "<input type='text' size='8' name='tfc_exchange_rate' id='tfc_exchange_rate' value='1.0'>";
		row.insertCell(6).innerHTML = "<input type='text' size='8' name='tfc_unit_count' id='tfc_unit_count' value='0.0'>";
		row.insertCell(7).innerHTML = "<input type='button' value='删除' onclick='deleteFreigthCost(this)'/>";
		//让回来的币种都默认选中
	}
	function createCurrency(fc_unit){
		var array = ['RMB','USD','HKD'];
		var options = "" ;
	 	for(var index = 0 , count = array.length  ; index < count ; index++ ){
			var selected = "" ;
		 	if(fc_unit.toUpperCase() === array[index]){
		 	   selected = "selected";
			}
			options += "<option "+selected+" value='"+array[index]+"'>"+array[index]+"</option>";
		}
		return  options;
    }

    function deleteFreigthCost(input){  
        var s=input.parentNode.parentNode.rowIndex;
        document.getElementById("freight_cost").deleteRow(s); 
        $('#changed').val('2');
        //var num=document.getElementById("tables").rows.length;  
	}

    function selectFreigthCost() {
    	if($("input:radio[name=stock_in_set_radio]:checked").val()==2)
		{
    		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_cost/freight_cost_select.html";
    		$.artDialog.open(url, {title: '运费项目选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
		} 
    	else
    	{
			alert("请确认在流程指派中是否选择了需要运费");
    	}
	}
	
	function uploadFile(_target)
	{
	    var obj  = {
		     reg:"xls",
		     limitSize:2,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		
		$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
				 //close:function()
				 //{
						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 //	 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
				 //}
			 });
	}
	
	function prefillDetailByFile(file_name)
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_detail_prefill.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{file_name:file_name},
			
			beforeSend:function(request){
			},
			
			error: function(){
			},
			
			success: function(html)
			{
				$("#details").html(html);
				
				addAutoComplete($("#p_name"),
					"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
					"merge_info",
					"p_name");
			}
		});
	}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target)
{
    prefillDetailByFile(fileNames);
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
<form name="add_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/repair/addRepairOrderAction.action">
<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/repair/repair_order_detail.html"/>
<!-- 各流程 -->
<input type="hidden" name="declaration"/>
<input type="hidden" name="clearance"/>
<input type="hidden" name="product_file"/>
<input type="hidden" name="tag"/>
<input type="hidden" name="stock_in_set"/>
<input type="hidden" name="certificate"/>
<input type="hidden" name="quality_inspection"/>

<!-- 负责人的Ids -->
<input type="hidden" name="adminUserIdsTransport" id="adminUserIdsTransport" value='<%=adminUserIdsTransport %>'/>
<input type="hidden" name="adminUserIdsDeclaration" id="adminUserIdsDeclaration" value='<%=adminUserIdsDeclaration %>'/>
<input type="hidden" name="adminUserIdsClearance" id="adminUserIdsClearance" value='<%=adminUserIdsClearance %>'/>
<input type='hidden' name="adminUserIdsProductFile" id="adminUserIdsProductFile" value='<%=adminUserIdsProductFile %>'/>
<input type="hidden" name="adminUserIdsTag" id="adminUserIdsTag" value='<%=adminUserIdsTag %>'/>
<input type="hidden" name="adminUserIdsStockInSet" id="adminUserIdsStockInSet" value='<%=adminUserIdsStockInSet %>'/>
<input type="hidden" name="adminUserIdsCertificate" id="adminUserIdsCertificate" value='<%=adminUserIdsCertificate %>'/>
<input type="hidden" name="adminUserIdsQualityInspection" id="adminUserIdsQualityInspection" value='<%=adminUserIdsQualityInspection %>'/>


<!-- 各流程，是否发各类通知 -->
<input type="hidden" name="needMailTransport"/>
<input type="hidden" name="needMessageTransport"/>
<input type="hidden" name="needPageTransport"/>

<input type="hidden" name="needMailDeclaration"/>
<input type="hidden" name="needMessageDeclaration"/>
<input type="hidden" name="needPageDeclaration"/>

<input type="hidden" name="needMailClearance"/>
<input type="hidden" name="needMessageClearance"/>
<input type="hidden" name="needPageClearance"/>

<input type="hidden" name="needMailProductFile"/>
<input type="hidden" name="needMessageProductFile"/>
<input type="hidden" name="needPageProductFile"/>

<input type="hidden" name="needMailTag"/>
<input type="hidden" name="needMessageTag"/>
<input type="hidden" name="needPageTag"/>

<input type="hidden" name="needMailStockInSet"/>
<input type="hidden" name="needMessageStockInSet"/>
<input type="hidden" name="needPageStockInSet"/>

<input type="hidden" name="needMailCertificate"/>
<input type="hidden" name="needMessageCertificate"/>
<input type="hidden" name="needPageCertificate"/>

<input type="hidden" name="needMailQualityInspection"/>
<input type="hidden" name="needMessageQualityInspection"/>
<input type="hidden" name="needPageQualityInspection"/>

<!-- 运费项 -->
<input type="hidden" name="changed" id="changed" value="1">
	
	<div class="demo" align="center">
	  	 <div id="tabs" style="width: 98%">
	  	    <ul>
	  	      <li><a href="#send" >提货地址</a></li>
	  	      <li><a href="#receive">收货地址</a></li>
			  <li><a href="#other">其他</a></li>
			  <li><a href="#details">货物列表</a></li>
			  <li><a href="#procedures">流程指派</a></li>
			  <li><a href="#freight">运输设置</a></li>
			  <li><a href="#freightcost">运费设置</a></li>
			</ul>
			<div id="send">
			<table width="95%" align="center" cellpadding="2" cellspacing="5">
						 	<tr>
						 		<td align="right">提货仓库</td>
						 		<td>
						 				<select id="psid" name="send_psid" onchange="selectSendStorage(this.value)">
							     			<option value="0">请选择...</option>
		                                <%
											for ( int i=0; i<treeRows.length; i++ )
											{
										%>
		                                	<option value='<%=treeRows[i].getString("id")%>'><%=treeRows[i].getString("title")%></option>
		                                <%
											}
										%>
							     		</select>
							    </td>
						 	</tr>
						 	<tr>
						 		<td align="right">提货国家</td>
						 		<td>
							      <select name="send_ccid" id="send_ccid" onChange="getStorageProvinceByCcid(this.value,0,'send_pro_id');">
								  <option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++)
								  {
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
									{
										if (selectBg.equals("#eeeeee"))
										{
											selectBg = "#ffffff";
										}
										else
										{
											selectBg = "#eeeeee";
										}
									}  	
									
									preLetter = countrycode[i].getString("c_country").substring(0,1);
								  %>
								    <option style="background:<%=selectBg%>;"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								  <%
								  }
								  %>
							      </select>		
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">提货省份</td>
						 		<td>
						 			<select name="send_pro_id" id="send_pro_id">
								    </select>		
								      	<div style="padding-top: 10px;display:none;" id="state_div">
											<input type="text" name="address_state" id="address_state"/>
										</div>
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">提货地址门牌号</td>
						 		<td><input style="width: 300px;" type="text" name="send_street" id="send_street"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">提货地址街道</td>
						 		<td><input style="width: 300px;" type="text" name="send_house_number" id="send_house_number"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">提货城市</td>
						 		<td><input style="width: 300px;" type="text" name="send_city" id="send_city"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">提货地址邮编</td>
						 		<td><input style="width: 300px;" type="text" name="send_zip_code" id="send_zip_code"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">提货联系人</td>
						 		<td><input style="width: 300px;" type="text" name="send_name" id="send_name"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">提货联系人电话</td>
						 		<td><input style="width: 300px;" type="text" name="send_linkman_phone" id="send_linkman_phone"/></td>
						 	</tr>
						 </table>
		</div>
		<div id="receive">
			<table width="95%" align="center" cellpadding="2" cellspacing="5">
						 	<tr valign="bottom">
									  <td align="right">收货仓库</td>
								      <td>
								      	<select name='receive_psid' id='receive_psid' onchange="selectDeliveryStorage(this.value)">
		                                <option value="0">请选择...</option>
		                                <%
											  for ( int i=0; i<treeRows.length; i++ )
											  {
											%>
		                                <option value='<%=treeRows[i].getString("id")%>'><%=treeRows[i].getString("title")%></option>
		                                <%
											}
											%>
		                              </select>
								      </td>
							</tr>
						 	<tr>
						 		<td align="right">收货国家</td>
						 		<td>
							      <select name="deliver_ccid" id="deliver_ccid" onChange="getStorageProvinceByCcid(this.value,0,'deliver_pro_id');">
								  <option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++)
								  {
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
									{
										if (selectBg.equals("#eeeeee"))
										{
											selectBg = "#ffffff";
										}
										else
										{
											selectBg = "#eeeeee";
										}
									}  	
									
									preLetter = countrycode[i].getString("c_country").substring(0,1);
								  %>
								    <option style="background:<%=selectBg%>;"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								  <%
								  }
								  %>
							      </select>		
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">收货省份</td>
						 		<td>
						 			<select name="deliver_pro_id" id="deliver_pro_id">
								    </select>		
								      	<div style="padding-top: 10px;display:none;" id="state_div">
											<input type="text" name="address_state" id="address_state"/>
										</div>
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">收货地址门牌</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_house_number" id="deliver_house_number"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">收货地址街道</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_street" id="deliver_street"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">收货城市</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_city" id="deliver_city"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">收货地址邮编</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_zip_code" id="deliver_zip_code"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">收货联系人</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_name" id="deliver_name"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">收货联系人电话</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_linkman_phone" id="deliver_linkman_phone"/></td>
						 	</tr>
						 </table>
		</div>
		<div id="other">
			<table width="95%" align="center" cellpadding="2" cellspacing="5">
				<tr>
			 		<td align="right" width="20%">预计到达时间</td>
			 		<td>
				 		<input width="80%" type="text" name="repair_receive_date" id="repair_receive_date" value="">
					</td>
				</tr>
				<tr>
					<td width="20%" align="right">备注</td>
					<td width="80%">
				 		<textarea rows="2" cols="70" name="remark" id="remark"></textarea>
					</td>
				</tr>
			</table>
		</div>
		
		<div id="details">
			参考模板:<span class="STYLE12"><a href="../repair/repair_template.xls">下载</a></span>
			<input type="button" class="long-button" onclick="uploadFile('');" value="上传货物列表"/>
		</div>
		<div id="procedures">
			<table width="100%" border="0" cellspacing="5" cellpadding="0">
				<tr height="25px" id="transportPersonTr">
						<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">运输负责人：</td>
					    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
					    	<input type="text" id="adminUserNamesTransport" name="adminUserNamesTransport" style="width:180px;" onclick="adminUserTransport()" value='<%=adminUserNamesTransport %>'/>
					    	通知：
					   		<input type="checkbox" name="isMailTransport" checked="checked"/>邮件
					   		<input type="checkbox" name="isMessageTransport" checked="checked"/>短信
					   		<input type="checkbox" name="isPageTransport" checked="checked"/>页面
						</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
		        	   <td width="13%" align="right" class="STYLE2">出口报关:</td>
		         		<td width="87%" align="left" valign="middle" id="declarationTr">
		         			<input type="radio" name="radio_declaration" value="1" checked="checked"/> <%=repairDeclarationKey.getStatusById(1)%>
		          			<input type="radio" name="radio_declaration" value="2"/> <%= repairDeclarationKey.getStatusById(2) %>
		      			</td>
		      	</tr>
				<tr height="25px" style="display:none" id="declarationPersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesDeclaration" name="adminUserNamesDeclaration" style="width:180px;" onclick="adminUserDeclaration()" value='<%=adminUserNamesDeclaration %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailDeclaration" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageDeclaration" checked="checked"/>短信
				   		<input type="checkbox" name="isPageDeclaration" checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
				<tr height="25px">
	        		<td width="13%" align="right" class="STYLE2">进口清关:</td>
			        <td width="87%" align="left" valign="middle" id="clearanceTr">
			         <input type="radio" name="radio_clearance" value="1" checked="checked"/> <%= repairClearanceKey.getStatusById(1) %>
			         <input type="radio" name="radio_clearance" value="2"/> <%= repairClearanceKey.getStatusById(2) %>					      
					</td>
		        </tr>
		        
		        <tr height="25px" style="display:none" id="clearancePersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesClearance" name="adminUserNamesClearance" style="width:180px;" onclick="adminUserClearance()" value='<%=adminUserNamesClearance %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailClearance" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageClearance"  checked="checked"/>短信
				   		<input type="checkbox" name="isPageClearance"  checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
		          <td width="13%" align="right" class="STYLE2">图片流程:</td>
		          <td width="87%">
		          	 <input type="radio" name="radio_productFile" value="1" checked="checked"/> <%= repairProductFileKey.getStatusById(1)%>
		          	 <input type="radio" name="radio_productFile" value="2"/> 需要实物图片流程
		          </td>
		        </tr>
		         
		        <tr height="25px" style="display:none" id="productFilePersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesProductFile" name="adminUserNamesProductFile" style="width:180px;" onclick="adminUserProductFile()" value='<%=adminUserNamesProductFile %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailProductFile" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageProductFile" checked="checked"/>短信
				   		<input type="checkbox" name="isPageProductFile" checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
	        	  <td width="13%" align="right" class="STYLE2">制签:</td>
		          <td width="87%" align="left" valign="middle" id="transportTr">
		   
		          	<input type="radio" name="radio_tag" value="1" checked="checked"/> <%= repairTagKey.getRepairTagById(1) %>
		          	<input type="radio" name="radio_tag" value="2"/> <%= repairTagKey.getRepairTagById(2) %>
		          </td>
		        </tr>
		       
		        <tr height="25px" style="display:none" id="tagPersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesTag" name="adminUserNamesTag" style="width:180px;" onclick="adminUserTag()" value='<%=adminUserNamesTag %>'/>
				    	通知：
				    	
				   		<input type="checkbox" name="isMailTag" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageTag" checked="checked"/>短信
				   		<input type="checkbox" name="isPageTag" checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
		       	  <td width="13%" align="right" class="STYLE2">运费流程:</td>
		          <td width="87%" align="left" valign="middle" id="stockInSetTr">
		          	 <input type="radio" name="stock_in_set_radio" value="1" checked="checked"/> <%= repairStockInSetKey.getStatusById(1) %>
		          	 <input type="radio" name="stock_in_set_radio" value="2"/> <%= repairStockInSetKey.getStatusById(2) %>	
		          </td>
		        </tr>
		        <tr height="25px" style="display:none" id="stockInSetPersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesStockInSet" name="adminUserNamesStockInSet" style="width:180px;" onclick="adminUserStockInSet()" value='<%=adminUserNamesStockInSet %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailStockInSet" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageStockInSet" checked="checked"/>短信
				   		<input type="checkbox" name="isPageStockInSet" checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
		          <td width="13%" align="right" class="STYLE2">单证:</td>
		          <td width="87%" align="left" valign="middle" id="certificateTr">
		          	<input type="radio" name="radio_certificate" value="1" checked="checked"/> <%=repairCertificateKey.getStatusById(1)%>
		          	<input type="radio" name="radio_certificate" value="2"/> 需要单证
		          </td>
		        </tr>
		        <tr height="25px" style="display:none" id="certificatePersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesCertificate" name="adminUserNamesCertificate" style="width:180px;" onclick="adminUserCertificate()" value='<%=adminUserNamesCertificate %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailCertificate" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageCertificate" checked="checked"/>短信
				   		<input type="checkbox" name="isPageCertificate" checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
		          <td width="13%" align="right" class="STYLE2">质检流程:</td>
		          <td width="87%" align="left" valign="middle" id="qualityInspectionTr">
		          <input type="radio" name="radio_qualityInspection" value="1" checked="checked"/> 无需质检报告
		          <input type="radio" name="radio_qualityInspection" value="2"/>需要质检报告
		          </td>
		        </tr>
		        <tr height="25px;" style="display:none" id="qualityInspectionPersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesQualityInspection" name="adminUserNamesQualityInspection" style="width:180px;" onclick="adminUserQualityInspection()" value='<%=adminUserNamesQualityInspection %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailQualityInspection" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageQualityInspection" checked="checked"/>短信
				   		<input type="checkbox" name="isPageQualityInspection" checked="checked"/>页面
					</td>
				</tr>
			</table>
		</div>
		<div id="freight">
			<input type="hidden" name="fr_id" id="fr_id"/>
			<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr height="29">
		          <td align="right" class="STYLE2">货运公司:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="repair_waybill_name" id="repair_waybill_name" readonly="readonly"><br/>
		          	<input type="button" class="long-button" name="waybillSelect" id="waybillSelect" value="选择运输资源" onclick="openFreightSelect()">
		          </td>
		          <td align="right" class="STYLE2">运单号</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="repair_waybill_number" id="repair_waybill_number">
		          </td>
		        </tr>
		        <tr height="29">
		          <td align="right" class="STYLE2">运输方式:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="hidden" id="repairby" name="repairby">
		          	<input type="text" name="repairby_name" id="repairby_name" readonly="readonly">
		          </td>
		          <td align="right" class="STYLE2">承运公司:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="carriers" id="carriers" readonly="readonly">
		          </td>
		        </tr>
		        <tr height="29" id="tr1" name="tr1">
		          <td align="right" class="STYLE2">始发港:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="repair_send_place" id="repair_send_place" readonly="readonly">
		          </td>
		          <td align="right" class="STYLE2">收货港:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="repair_receive_place" id="repair_receive_place" readonly="readonly">
		          </td>
		        </tr>
			</table>
		</div>
		<div id="freightcost">
			<table id="freight_cost" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" style="padding-top: 17px" class="zebraTable">
			 <tr>
		     	<td align="right" colspan="8" height="30px"><input id="select_freight_cost" type="button" class="long-button" name="selectFreightCost" value="选择运费项目" onclick="selectFreigthCost()"></td>
		     </tr>
		     <tr>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>
		       <th width="25%" nowrap="nowrap" class="right-title"  style="text-align: center;"></th>	
		     </tr>
		    </table>
		</div>
	</div>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top:10px">
		<tr>
			<td align="right">
				<input id="back_button" type="button" style="font-weight: bold" class="normal-green" onClick="back()" value="上一步"/>
				&nbsp;&nbsp;&nbsp;
				<input id="next_button" type="button" style="font-weight: bold" class="normal-green" onClick="next()" value="下一步"/>
				&nbsp;&nbsp;&nbsp;
				<input type="button" style="font-weight: bold" class="normal-green" onClick="submitCheck()" value="提交"/>
			</td>
		</tr>
	</table>					
	</div>
      </form>
      <script type="text/javascript">
		
		var tabsIndex = 0;//当前tabs的Index
		var tab = $("#tabs").tabs({
		cache: false,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		load: function(event, ui) {onLoadInitZebraTable();}	,
		selected:0,
		show:function(event,ui)
			 {
			 	if(ui.index==1)
			 	{
			 		if(checkSend()==false)
			 		{
			 			tabSelect(0);
			 		}
			 	}
			 	if(ui.index==2)
			 	{
			 		if(checkReceive()==false)
			 		{
			 			tabSelect(1);
			 		}
			 	}
			 	if(ui.index==5)
			 	{
			 		if(checkProcedures()==false)
			 		{
			 			tabSelect(4)
			 		}
			 	}
			 	
			 	tabsIndex = ui.index;
			 	if(tabsIndex==0)
			 	{
			 		$("#back_button").attr("disabled",true);
			 	}
			 	else
			 	{
			 		$("#back_button").removeAttr("disabled");
			 	}
			 }
		});
		
		function countCheck(obj)
		{
			var count = obj.value;
			var countFloat = parseFloat(count);
			
			if(countFloat<0||countFloat!=count)
			{
				alert("数量填写错误");
				$(obj).css("backgroundColor","red");
				return false;
			}
			else
			{
				$(obj).css("backgroundColor","");
			}
		}
		
		function submitCheck()
		{
			if(checkSend()==false)
			{
			 	tabSelect(0);
			}
			else if(checkReceive()==false)
			{
			 	tabSelect(1);
			}
			else if(checkProcedures()==false)
			{
			 	tabSelect(4)
			}
			else
			{
				var isSubmit = true;
				
				$("[name$='_count']").each(function()
				{
			 		if(countCheck(this)==false)
			 		{
			 			isSubmit = false;
			 			return false;
			 		}
					
				});
				
				if(isSubmit==true)
				{
					document.add_form.submit();
				}
				else
				{
					tabSelect(3)
				}
				
			}
		}
		
		function next()
		{
			var index = tabsIndex+1;
			tabSelect(index);
		}
		
		function back()
		{
			var index = tabsIndex-1;
			tabSelect(index);
		}
		
		function tabSelect(index)
		{
			$("#tabs").tabs("select",index);
		}
	</script>
</body>
</html>
