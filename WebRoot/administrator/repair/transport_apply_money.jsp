<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
	String purchase_id = StringUtil.getString(request,"purchase_id");
	long transport_id = StringUtil.getLong(request,"transport_id");
	DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id);
	DBRow supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(purchase.getString("supplier")));
 
	 
	DBRow applyMoneyCategoryRow = applyFundsCategoryMgrZZZ.getApplyMoneyCategoryByCategoryId(request) ;
	String transportApplyMoneyAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/transport/TransportApplyMoneyAction.action";
	//1.申请的金额首先根据在详细上的列表去进行计算
 
	//2.在提交的时候检查提交的金额与改采购单(本身的金额) - 所有的申请的金额交货型转运单金额比较 >= 0 即可
	//3.提交的时候是在一个误差的范围内的
	double transportDetailMoney = MoneyUtil.round(transportMgrZJ.getTransportSendPrice(transport_id),2);
	double usdCurrency =Double.parseDouble(systemConfig.getStringConfigValue("USD")) ;
	double usdMoney = MoneyUtil.round(transportDetailMoney / usdCurrency,2);
	double maxRMBMoney = MoneyUtil.round(transportDetailMoney + transportDetailMoney * 0.01 * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation")) ,2);
	double maxUSDMoney = MoneyUtil.round((transportDetailMoney + transportDetailMoney * 0.01 *Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))) / usdCurrency ,2);
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>申请货款</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 时间控件 -->
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script>
    	  
var va = ['一','二','三','四','五','六','七','八','九'];
 
function addUSD(){
	//r_name,r_account,swift,r_address,r_bank_name,r_bank_address
	var len = $(".usd").length;	 
	var table =  "<table class='receiver usd next'>";
	table += "<tr><td rowspan='7' style='"+"width:150px;text-align:center;"+"'>外币收款信息"+va[len]+"</td>";
	table += "<td class='left'>收款人户名:</td><td><input type='text' class='noborder' name='r_name'/></td>";
	table += "<td rowspan='7' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";
	table += "<tr><td class='left'>收款人账号:</td><td><input type='text' class='noborder' name='r_account'/></td></tr>";
	table +="<tr><td class='left'>SWIFT:</td><td><input type='text' class='noborder' name='swift'/></td></tr>";
	table +="<tr><td class='left'>收款人地址:</td><td><input type='text' class='noborder' name='r_address'></td></tr>";
	table +="<tr><td class='left'>收款行名称:</td><td><input type='text' class='noborder' name='r_bank_name'></td></tr>" ;
	table +="<tr><td class='left'>收款行地址:</td><td><input type='text' class='noborder' name='r_bank_address'></td></tr></table>" ;

 	//	
	var addBar = $(".receiver").last();
	addBar.after($(table));
}
function addRMB(){
	var len  = $(".rmb").length ;
	var table =  "<table class='receiver rmb next'>";
	table += "<tr><td rowspan='4' style='"+"width:150px;text-align:center;"+"'>人民币收款信息"+va[len]+"</td>";
	table += "<td class='left'>收款人户名:</td>";
	table += "<td><input type='text' name='r_name' class='noborder'/></td>";
	table += "<td rowspan='4' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";

	table += "<tr><td class='left'>收款人账号:</td>";
	table += "<td><input type='text' name='r_account' class='noborder'/></td></tr>";
	table +="<tr><td class='left'>收款开户行:</td><td><input type='text' name='r_bank' class='noborder'/></td></tr>";
	table +="<tr><td class='left'>收款人电话:</td><td><input type='text' name='r_phone' class='noborder'/></td></tr></table>";
 	// 获取最后一个RMB的table 然后添加到他的后面。如果是没有的话,那么就选取H1标签然后添加到他的后面
	var addBar = $(".receiver").last();
	addBar.after($(table));
}
function changeValue(){
    var amount = $("#amount").val();
	var currency = $("select[name='currency']").val();
	 
	if($.trim(amount).length > 0){
	 	if(currency === "USD"){
	 	  var tempValue = parseFloat(amount) / parseFloat('<%= usdCurrency%>');
	 	  $("#amount").val(decimal(tempValue,2));
	 	}else{
	 	   var tempValue = parseFloat(amount) * parseFloat('<%= usdCurrency%>');
	  	  $("#amount").val(decimal(tempValue,2));
	 	}
	}
}
function decimal(num,v){
	var vv = Math.pow(10,v);
	return Math.round(num*vv)/vv;
} 
//文件上传
function uploadFile(_target){
 
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames){
    $("#file_up_td").html("");
	if($.trim(fileNames).length > 0 ){
	    $("input[name='file_names']").val(fileNames);
	    //将要上传的文件回显出来
	    var arrayFile = fileNames.split(",");
	    var as = "";
	    for(var index = 0 , count = arrayFile.length ; index < count ; index++ ){
			as += createA(arrayFile[index]);
		}
	    $("#file_up_td").html(as);
	}
	
}
function createA(fileName){
   var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
   var fixedFileName = $("#sn").val()+"_"+ fileName;
   var a = "<a href='"+uri+"'  >"+fixedFileName+"</a><br />"; 
   return a ;
}
 
</script>
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
table.receiver td {border:1px solid silver;}
table.receiver td input.noborder{border-bottom:1px solid silver;width:200px;}
table.receiver td.left{width:120px;text-align:right;}
table.next{margin-top:5px;}
table.receiver{border-collapse:collapse ;}
span._hand{cursor:pointer;}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">资金申请单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr>
		          <td align="right" class="STYLE2">资金类型:</td>
		          <td>&nbsp;</td>
		          <td>		      
		         	<%= (applyMoneyCategoryRow != null ? applyMoneyCategoryRow.getString("category_name"):"") %>
		          </td>
		        </tr>
		    <tr> 
			
		    <td width="12%" align="right"><span class="STYLE2">关联:</span></td>
		      <td width="2%">&nbsp;</td>
		      <td>
		       	转运单T<%=transport_id%>
			   </td>
		    </tr>
		   <tr>
		  	 	<td width="12%" align="right"><span class="STYLE2">允许申请金额:</span></td>
		  	 	<td width="2%">&nbsp;</td>
		  	 	<td>
		  	 			<span id="rmbValue"><%=transportDetailMoney %></span>  <span style="color:#f60;font-weight:bold;">RMB</span> ,<span id="usdValue"><%=usdMoney %></span>  <span style="color:#f60;font-weight:bold;">USD</span>
		  	 	</td>
		 	 </tr>
		      <td align="right" class="STYLE2">金额:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<input name="amount" type="text" id="amount" size="15" value="<%=transportDetailMoney %>">
		     	<select name="currency" onchange="changeValue()">
		      		<option value="RMB">RMB</option>
		      		<option value="USD">USD</option>
		      	</select>
		      </td>
		    </tr>
				  
		    <tr> 
		      <td align="right" class="STYLE2">收款人:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<input type="text" style="width:300px;" id="payee" name="payee" value='<%=supplier.getString("sup_name")%>'/>
		      </td>
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2">最迟转款时间:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<input name="st" type="text" id="st_date"  value=""  style="border:1px #CCCCCC solid;width:70px;"/>
		      </td>
		    </tr>			 
		    <tr> 
		      <td align="right" class="STYLE2">付款信息:</td>
		      <td>&nbsp;</td>
		      <td>		      		    		     
		      		<h1 id="addBar"> 
		      			<input type="button" value="添加人民币收款" onclick="addRMB();"/>
		      			<input type="button" value="添加外币收款" onclick="addUSD();"/>
		      		</h1>
		      		<table class="receiver rmb next">
		      			<tr>
		      				<td rowspan="4" style='width:150px;text-align:center;'>人民币收款信息一</td>
		      				<td class="left">收款人户名:</td>
		      				<td><input type="text" name="r_name" class="noborder"/></td>
		      				<td rowspan="4" style="width:80px;text-align:center;"><span onclick="deletePaymentInfo(this)" class="_hand">删除</span></td>
		      			</tr>
		      			<tr>
		      				<td class="left">收款人账号:</td>
		      				<td><input type="text" name="r_account" class="noborder"/></td>
		      			</tr>
		      			<tr>
		      				<td class="left">收款开户行:</td>
		      				<td><input type="text" name="r_bank" class="noborder"/></td>
		      			</tr>
		      			<tr>
		      				<td class="left">收款人电话:</td>
		      				<td><input type="text" name="r_phone" class="noborder"/></td>
		      			</tr>
		      		</table>
		       
		          <p>
		          		<span style='font-weight:bold;'>参考账号信息: </span>
		      	<%=supplier.getString("bank_information")%>
		          </p>
		      </td>
		    </tr>
		    <tr>
		      <td class="STYLE2" align="right">负责人:</td>
		      <td>&nbsp;</td>
		      <td>
		      <input type="text" id="adminUserNamesInvoice" name="adminUserNamesInvoice" style="width:180px;" onclick="adminUserInvoice()"/>
		      <input type="hidden" id="adminUserIdsInvoice" value=""/>
		            通知：
	   		<input type="checkbox"  name="isMailInvoice" id="isMailInvoice"/>邮件
	   		<input type="checkbox"  name="isMessageInvoice" id="isMessageInvoice" />短信
	   		<input type="checkbox"  name="isPageInvoice" id="isPageInvoice" />页面
		      </td>
		    </tr>					    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="remark" rows="4" cols="80"></textarea></td>
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2">凭证:</td>
		      <td>&nbsp;</td>
		      <td><input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件"/></td>
		    </tr>
		    <tr>
		    	<td align="right" class="STYLE2"></td>
		    	<td>&nbsp;</td>
		    	<td id="file_up_td">
		    	
		    	</td>
		    </tr>
		</table>
			<input type="hidden" name="folder" id="folder" value="<%=systemConfig.getStringConfigValue("file_path_financial") %>"/>
	 		<input type="hidden" name="sn" value="F_applyMoneny" id="sn"/>
	 		<input type="hidden" name="file_names" id="file_names"/>
		
	</fieldset>	
	</td>
  </tr>
  <!--  
	  <tr>
	    <td>
	    	 
	       <iframe width="98%"  frameborder="0" scrolling="no" name="upFile" id="upFile" src="<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/upload_purchase_image.html">
	
	       </iframe>
	    </td>
	  </tr>
  -->
</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onclick="submitApply()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="$.artDialog && $.artDialog.close()" value="取消" >
    </td>
  </tr>
</table>
<script type="text/javascript">
 
function setParentUserShow(ids,names,  methodName)
{
	eval(methodName)(ids,names);
}
function setParentUserShowPurchaser(user_ids , user_names){
	$("#adminUserIdsPurchaser").val(user_ids);
	$("#adminUserNamesInvoice").val(user_names);
	$("#adminUserIdsInvoice").val(user_ids);
}

function adminUserInvoice(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsInvoice").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowPurchaser'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}

	function submitApply()
	{
		 var pageMessage=false;
		 var mail=false;
		 var shortMessage=false;
           //判断 页面  邮件 短信
		  if($("#isMailInvoice").attr("checked")){
		     mail=true;
		  }
		  if($("#isMessageInvoice").attr("checked")){
			 shortMessage=true;
		  }
		  if($("#isPageInvoice").attr("checked")){
			 pageMessage=true;
		  }  
		  
			var str  = ""
			var rmbInfo = $(".rmb");
			if(rmbInfo.length > 0 ){
				for(var index = 0 , count = rmbInfo.length ; index < count ; index++ ){
					var _table = $(rmbInfo.get(index));
					var r_name =  $("input[name='r_name']",_table).val();
					var r_account = $("input[name='r_account']",_table).val();
					var r_bank = $("input[name='r_bank']",_table).val();
					var r_phone = $("input[name='r_phone']",_table).val();
					str +="\n";
					if($.trim(r_name).length > 0 ){
						str += "收款人户名:"+r_name+"\n";
					}else{
						alert("输入收款人户名!");
						return ;
					}
					if($.trim(r_account).length > 0 ){
						str += "收款人账号:"+r_account+"\n";
					}else{
						alert("输入收款人账号!");
						return ;
					}
					if($.trim(r_bank).length > 0 ){
						str += "收款开户行:"+r_bank+"\n";
					}
					if($.trim(r_phone).length > 0 ){
						str += "联系电话:"+r_phone+"\n";
					}	
				}
			}
			if($.trim($("#st_date").val()).length < 1){
				alert("请先选择最迟转款时间");
				return ;
			}
			 str += getUsdPayMentInfo();
		 	 // 计算组织出所有收款信息
		 	 if(str.length <= 4){
				alert("请先输入收款人信息");
				return ;
			 }
		 	 if($("#payee").val().trim().length < 1){
		 		$("#payee").focus();
		 		alert("输入供应商的名称!");
					return ;
			 }
			 if($.trim($("#amount").val().length) < 1||$("#amount").val()==null)
			 {
			     $("#amount").focus();
			     alert("金额不能为空！");
			 }
			 else if(isNaN($("#amount").val().trim()))
		 	 {
			     $("#amount").focus();
			     alert("金额必须为数字！");
		 	 }
			 else if($("#amount").val().trim()<=0)
			 {
			 	 $("#amount").focus();
			     alert("金额必须大于零！");
			 }
			 else if($("#remark").val().trim()==""||$("#remark").val()==null)
			 {
			     $("#remark").focus();
			     alert("请填写备注！");
			 }
			 else if($("#remark").val().trim().length>200)
			 {
			     $("#remark").focus();
			     alert("备注字数不能超过200！");
			 }
			 else
			 {
				// 如果是有file_names ，那么读取子页面的file_names
				
					var _file_names = $("input[name='file_names']").val() ;
			  		// 提交的时候检查申请可以在一个误差范围以内
			  		if(!validate()){return ;}
				var o = {
						remark:"资金申请:"+$("#remark").val(),
						amount:$("#amount").val(),
						supplier_id:<%=supplier.get("id",0l)%>,
						payee: $("#payee").val(),
						payment_information:str,
						last_time:$("#st_date").val(),
						admin_id:$("#adminUserIdsInvoice").val(),
						mail:mail,
						shortMessage:shortMessage,
						pageMessage:pageMessage	,
						folder:$("#folder").val(),
						sn:$("#sn").val(),
						file_names:_file_names,
						transport_id:'<%= transport_id%>',
						purchase_id:'<%= purchase_id%>',
						currency:$("select[name='currency']").val()
			            }
				ajaxSubmit(o);
			 }
	
	}
	function validate(){
		var currency = $("select[name='currency']").val();
		var amount = parseFloat($("#amount").val())  ;
		if(currency.trim() === "USD"){
			if(amount > '<%= maxUSDMoney%>' * 1){
				showMessage("申请金额错误","alert");
				return false ;
			}
		}
		if(currency.trim() === "RMB"){
		    if(amount > '<%= maxRMBMoney%>' * 1){
				showMessage("申请金额错误","alert");
				return false ;
			}
		}
		return true ;
	}
		function ajaxSubmit(o){
			$.ajax({
				url:'<%= transportApplyMoneyAction%>',
				data:jQuery.param(o),
				dataType:'json',
				success:function(data){
					 
					if(data && data.flag == "success"){
						if(data.money_flag && data.money_flag == "notallow"){
							alert("申请资金大于最大申请金额!(对应采购单创建了多个交货单)");
						}else{
					   		 $.artDialog && $.artDialog.close();
							$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();	
						}
					}else{
					    showMessage("系统错误","error");
					}
				},
				error:function(){
				    showMessage("系统错误","error");
				}
			})
		}
			function getUsdPayMentInfo(){
				var str  = "";
				var usdInfo = $(".usd");
				if(usdInfo.length > 0 ){
					for(var index = 0 , count = usdInfo.length ; index < count ; index++ ){
					//	r_name,r_account,swift,r_address,r_bank_name,r_bank_address
					//	收款人户名,收款人账号,SWIFT,收款人地址,收款行名称,收款行地址
						var _table = $(usdInfo.get(index));
						var r_name =  $("input[name='r_name']",_table).val();
						var r_account = $("input[name='r_account']",_table).val();
						var swift = $("input[name='swift']",_table).val();
						var r_address = $("input[name='r_address']",_table).val();
						var r_bank_name = $("input[name='r_bank_name']",_table).val();
						var r_bank_address = $("input[name='r_bank_address']",_table).val();
						str +="\n";
						if($.trim(r_name).length > 0 ){
							str += "收款人户名:"+r_name+"\n";
						}else{
							alert("输入收款人户名!");
							return ;
						}
						if($.trim(r_account).length > 0 ){
							str += "收款人账号:"+r_account+"\n";
						}else{
							alert("输入收款人账号!");
							return ;
						}
						if($.trim(swift).length > 0 ){
							str += "SWIFT:"+swift+"\n";
						}
						if($.trim(r_address).length > 0 ){
							str += "收款人地址:"+r_address+"\n";
						}
						if($.trim(r_bank_name).length > 0 ){
							str += "收款行名称:"+r_bank_name+"\n";
						}
						if($.trim(r_bank_address).length > 0 ){
							str += "收款行地址:"+r_bank_address+"\n";
						}
					}
					return str;
				}
				return "" ;
			}
			function deletePaymentInfo(_this){
				var parentNode = $(_this).parent().parent().parent().parent();
				if($(".receiver").length == 1){
					alert("至少包含一个收款信息");
					return ;
				}
 				 parentNode.remove();
			}
			function showMessage(_content,_state){
				var o =  {
					state:_state || "succeed" ,
					content:_content,
					corner: true
				 };
			 
				 var  _self = $("body"),
				_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
				_self.append(_stateBox);	
				 
				if(o.corner){
					_stateBox.addClass("ui-corner-all");
				}
				if(o.state === "succeed"){
					_stateBox.addClass("ui-stateBox-succeed");
					setTimeout(removeBox,1500);
				}else if(o.state === "alert"){
					_stateBox.addClass("ui-stateBox-alert");
					setTimeout(removeBox,2000);
				}else if(o.state === "error"){
					_stateBox.addClass("ui-stateBox-error");
					setTimeout(removeBox,2800);
				}
				_stateBox.fadeIn("fast");
				function removeBox(){
					_stateBox.fadeOut("fast").remove();
			 }
			}
</script>
<script type="text/javascript">
	$("#st_date").date_input();
</script>
</body>
</html>
