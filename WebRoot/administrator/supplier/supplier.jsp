<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.AccountKey"%>
<%@ include file="../../include.jsp"%> 
<%
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(10);
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
String province_id = null;
String coutry = null;
DBRow [] suppliers = null;
if(cmd.equals("search")){
	suppliers = supplierGZY.getSearchSupplier(key,pc);

}else if(cmd.equals("filter")){
	suppliers = supplierGZY.getSupplierInfoListByCondition(request,pc);
	 coutry = StringUtil.getString(request,"country");
	  province_id = StringUtil.getString(request,"province_id");

}
else{
	suppliers = supplierGZY.getSupplierInfoList(pc);
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script language="javascript"><!--

	jQuery(function($){
		var coutry = "<%=coutry %>";
		var provinceid = "<%=province_id%>";
		if(coutry != "null"){
			getProvinceByCountry(coutry , provinceid);
		}

	})
	
	function createSupplier(){
		var uri	= '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/supplier/add_supplierGZY.html";
		$.artDialog.open(uri,{title:'创建供应商', height:450, width:750, lock:true,opacity:0.3,fixed:true});
		//tb_show('创建供应商','add_supplierGZY.html?TB_iframe=true&height=450&width=750',false);
	}
	function refreshWindow(){
		window.location.reload();	
	};
	function delSupplier(id){
		if(confirm("确认要删除吗")){
			document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/delSupplier.action";
			document.del_form.s_id.value = id;
			document.del_form.submit();
		}	
	}
	
	function modSupplier(id){
		var uri	= '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/supplier/mod_supplierGZY.html?sid="+id;
		$.artDialog.open(uri,{title:'修改供应商信息', height:450, width:750, lock:true,opacity:0.3,fixed:true});
		//tb_show('修改供应商信息','mod_supplierGZY.html?sid='+id+'&TB_iframe=true&height=400&width=750',false);
	}
	
	function upload_qualification(id){
		tb_show('上传供应商资质文件','supplier_upload.html?sid='+id+'&TB_iframe=true&height=400&width=700',false);
	}
	
	function modSupplierPwd(id){
		tb_show('修改供应商密码','mod_supplier_pwd.html?sid='+id+'&TB_iframe=true&height=400&width=750',false);
	}
	function closeWinRefresh(){
		window.location.reload();
		tb_remove();
	}
	
	function closeWin(){
		tb_remove();
	}
	function searchSupplier(){
		document.filter_search_form.cmd.value = "search";		
		document.filter_search_form.key.value = $("#search_key").val();		
		document.filter_search_form.submit();
	}
	function getProvinceByCountry(nationid,provinceid){
	
		var nation = "country_id="+nationid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/GetProvinceByCountryJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:nation,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("数据读取出错！");
				},
				
				success: function(data){
					$("#province_id").clearAll();
					$("#province_id").addOption("请选择...","");
					if(data != ""){
						$.each(data,function(i){
							$("#province_id").addOption(data[i].pro_name,data[i].pro_id);
						});
						if(provinceid){
							$("#province_id option[value = '"+provinceid+"']").attr("selected",true);
						}
					}
				}
			});
	}
--></script>

<style>
form
{
	padding:0px;
	margin:0px;
}
.set {
	    border: 2px solid #999999;
	    font-weight: normal;
	    line-height: 18px;
	    margin-bottom: 10px;
	    margin-top: 10px;
	    padding: 7px;
	    width: 250px;
	    word-break: break-all;
	}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
	
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<%--<br>--%>
<%--<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">--%>
<%--  <tr>--%>
<%--    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购应用 &raquo; 供应商维护</td>--%>
<%--  </tr>--%>
<%--</table>--%>
<%--<br>--%>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
 <tr>
    <td>
	
	<div class="demo" >

	<div id="tabs">
<ul>
		<li><a href="#usual_tools">常用工具</a></li>
		<li><a href="#advan_search">高级搜索</a></li>		 
</ul>
     
		<div id="usual_tools">
			<input type="hidden" id="cmd" name="cmd" value="search"/>
			<div id="easy_search_father">
				<div id="easy_search"><a href="javascript:searchSupplier()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
			</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="50%">
						<div  class="search_shadow_bg">
							<input type="text" name="searchKey" id="searchKey" value="" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" onkeydown=""/>
						</div>
					</td>
					<td width="50%" align="right"">
						供应商名称:
						<input name="search_key" type="text" style="width: 150px" id="search_key" value="<%=key %>" onKeyDown="if(event.keyCode==13)searchSupplier()" /> 
						<input name="Submit" type="button" class="button_long_search" value="搜索" onClick="searchSupplier()"/>
				        <input name="Submit3" type="button" class="long-long-button-add" value="创建供应商" onClick="createSupplier()">
					</td>
				</tr>
			</table>
		</div>
	<div id="advan_search">
		<form name="filterForm" action="supplier.html" method="post">
		<input type="hidden" id="cmd" name="cmd" value="filter"/>
          <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td width="5%" align="right" style="padding-top:3px;" nowrap="nowrap" >所在国家:&nbsp;</td>
             <td width="10%" align="left" style="padding-top:3px;padding-left:5px">
		      <%
		DBRow countrycode[] = orderMgr.getAllCountryCode();
		String selectBg="#ffffff";
		String preLetter="";
		%>
      <select style="width:200px" name="country" id="country" onChange="getProvinceByCountry(this.value)">
	  <option value="0">请选择...</option>
	  <%
	  for (int i=0; i<countrycode.length; i++){
	  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
			if (selectBg.equals("#eeeeee")){
				selectBg = "#ffffff";
			}
			else{
				selectBg = "#eeeeee";
			}
		}
		
		preLetter = countrycode[i].getString("c_country").substring(0,1);
		
	if(StringUtil.getString(request,"country").equals(countrycode[i].getString("ccid"))){
	%>
	<option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>" selected="selected"><%=countrycode[i].getString("c_country")%></option>
	<%
	}else{
	  %>
	    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
	  <%
	  }
	}
	  %>
	 
      </select> 
			</td>
			<td width="5%" align="right" style="padding-top:3px;" nowrap="nowrap" >&nbsp;&nbsp;省份、洲:&nbsp;</td>
            <td width="10%" align="left" style="padding-top:3px;padding-left:5px">
			<select style="width:150px" name="province_id" id="province_id">
      		<option value="0">请选择...</option>
      </select> 
       </td>
       
			<td width="5%" align="right" style="padding-top:3px;" nowrap="nowrap" >&nbsp;&nbsp;所在产品线:&nbsp;</td>
              <td width="46%" height="35" align="left" valign="middle" nowrap="nowrap">
		      	<select style="width:150px" name="product_line_id" id="product_line_id">
		      	<option value="0">请选择...</option>
		      	<%
		      		DBRow[] productline =  productLineMgrTJH.getAllProductLine();
		      		for(int i=0;i<productline.length;i++){
		      		if(StringUtil.getString(request,"product_line_id").equals(productline[i].getString("id"))){
		      		%>
		      		<option value="<%=productline[i].getString("id") %>" selected="selected"><%=productline[i].getString("name") %></option>
		      		<%
		      		}else{
		      	%>
		      	<option value="<%=productline[i].getString("id") %>"><%=productline[i].getString("name") %></option>
		      	<%
		      	 }
		      		}
		      	%>
		      </select>
		      &nbsp;&nbsp;         
			  </td>
			  
              <td>
              	<input type="submit" id="seachByCondition" name="seachByCondition" class="button_long_refresh" value="过    滤" />
              </td>
            </tr>
          </table>
		  </form> 
	</div>
  </div>
  </div>
  </td>
  </tr>
</table>


 <form  method="post" name="update_form" />
    <input type="hidden" name="sup_name" />
    <input type="hidden" name="nation" />
    <input type="hidden" name="province" />
    <input type="hidden" name="linkman" />
    <input type="hidden" name="address" />
    <input type="hidden" name="legal_name" />
    <input type="hidden" name="phone" />
    <input type="hidden" name="other_contacts" />
    <input type="hidden" name="bank_information" />
    <input type="hidden" name="sid" />
    <input type="hidden" name="password" />
    <input type="hidden" name="product_line_id" />
    <input type="hidden" name="address_houme_number" />
    <input type="hidden" name="address_street" />
    <input type="hidden" name="city_input" />
    <input type="hidden" name="zip_code" />
    <input type="hidden" name="address_pro_input" />
  </form>
  
<form name="filter_search_form" method="get"  action="supplier.html">
<input type="hidden" name="cmd" >
<input type="hidden" name="key" >

</form>
 
  <form method="post" name="del_form">
  		<input type="hidden" name="s_id">
  </form><br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
    	<th width="10%" nowrap="nowrap" style="vertical-align: center;text-align: center;" align="center" class="left-title">供应商信息</th>
		<th width="29%" nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">地址信息</th>
        <th width="20%" nowrap="nowrap" style="vertical-align: center;text-align: center;" class="right-title" >联系信息</th>
		<th width="26%" nowrap="nowrap" class="right-title" style="vertical-align:center; text-align:center">银行信息</th>
        <th width="10%" nowrap="nowrap" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
    </tr>

<%
for (int i=0; i<suppliers.length; i++)
{
%>

    <tr>
      <td>
      	<fieldset class="set" style="border-color:green;">
      		<legend><%=suppliers[i].getString("id") %></legend>
      		<%=suppliers[i].getString("sup_name")%><br/>
      		<%
			  	DBRow productLine = productLineMgrTJH.getProductLineById(suppliers[i].get("product_line_id",0l));
			  	if(null!=productLine){
			%>
				<%=productLine.getString("name") %>
			<%
			  	}
			%><br/>
			<%=suppliers[i].getString("legal_name") %>
		</fieldset>
      </td>
	  <td align="left" valign="middle" style='word-break:break-all;' >
	  
		<%=suppliers[i].getString("address_houme_number")  %><br/>
		<%=suppliers[i].getString("address_street")  %><br/>
		<%=suppliers[i].getString("city_input")  %><br/>
		<%=suppliers[i].getString("zip_code")  %><br/>
		<%
			if(-1 != suppliers[i].get("province_id",0) && 0 != suppliers[i].get("province_id",0)){
				if(null == storageCatalogMgrZyj.getProviceInfoById(suppliers[i].get("province_id",0))){
		%>
					<br/>
		<%						
				}else{
		%>
				<%=storageCatalogMgrZyj.getProviceInfoById(suppliers[i].get("province_id",0)).getString("pro_name")  %><br/>	
		<%
				}
			}else{
		%>		
			<%=suppliers[i].getString("address_pro_input")  %><br/>
		<%		
			}
		%>
		<%
			if(0 == suppliers[i].get("nation_id",0l) || null == orderMgr.getDetailCountryCodeByCcid(suppliers[i].get("nation_id",0l))){
		%>		
			<br/>	
		<%		
			}else{
		%>		
				<%=orderMgr.getDetailCountryCodeByCcid(suppliers[i].get("nation_id",0l)).getString("c_country")%><br/>
		<%		
			}
		%>
        <%=suppliers[i].getString("address") %>
      </td>
      <td align="left" valign="middle" style="word-break:break-all;">
      	<%=suppliers[i].getString("linkman")%><br/>
		<%=suppliers[i].getString("phone")%><br/>
	  	<%
	  		String contacts = suppliers[i].getString("other_contacts");
	  		contacts=contacts.replaceAll("\n","<br>");
	  		out.print(contacts);
	  	 %>
	  </td>
	  <td align="left" valign="middle" style="width: 30px">
	  	<!-- 查询account表中,如果有了数据那么就是要显示Account表中的数据 -->	
	  	<!-- 如果没有那么就是要显示字段里面的东西 -->
	  	<!-- 兼容以前 -->
	  	<%
	  		DBRow[] accountInfos = accountMgrZr.getAccountByAccountIdAndAccountWithType(suppliers[i].get("id",0l),AccountKey.SUPPLIER);
	  		if(accountInfos != null && accountInfos.length > 0){
	  			for(DBRow tempAccount :  accountInfos){
	  	%>
	  					 
	  					<p>收款人户名:<%=tempAccount.getString("account_name") %></p>
	  					<p>收款人银行:<%=tempAccount.getString("account_blank") %></p>
	  					<p>收款人账号:<%=tempAccount.getString("account_number") %></p>
	  					<%if(tempAccount.getString("account_phone").length() > 0 ){ %>
	  						<p>收款人电话:<%=tempAccount.getString("account_phone") %></p>	
	  					<%} %>
	  					 <%if(tempAccount.getString("account_swift").length() > 0 ){ %>
	  						<p>SWIFT Code:<%=tempAccount.getString("account_swift") %></p>	
	  					<%} %> 
	  					<%if(tempAccount.getString("account_address").length() > 0 ){ %>
	  						<p>收款人地址:<%=tempAccount.getString("account_address") %></p>	
	  					<%} %>
	  					<%if(tempAccount.getString("account_blank_address").length() > 0 ){ %>
	  						<p>开户行地址:<%=tempAccount.getString("account_blank_address") %></p>	
	  					<%} %>
	  					<br />
	  					
	  				<%
	  			}
	  		}else{
	  	 
	  		String bank = suppliers[i].getString("bank_information");
	  		bank = bank.replaceAll("\n","<br>");
	  		out.print(bank);
	  		}
	  	 %>
	  </td>
      <td nowrap="nowrap" align="center" valign="middle" >
	   <input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="modSupplier(<%=suppliers[i].getString("id") %>)">
        &nbsp;&nbsp;&nbsp;
        <input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delSupplier(<%=suppliers[i].getString("id") %>)">
		<br>
		<br>
        <input name="Submit3" type="button" class="long-button" value="上传资质文件" onClick="upload_qualification(<%=suppliers[i].getString("id") %>)">
      </td>
    </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
 <form name="dataForm" action="supplier.html">    
<input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="key" value="<%=key%>">
<input type="hidden" name="country" value="<%=StringUtil.getString(request,"country") %>"/>
<input type="hidden" name="province_id" value="<%=StringUtil.getString(request,"province_id") %>"/>
<input type="hidden" name="product_line_id" value="<%=StringUtil.getString(request,"product_line_id") %>"/>
 </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>

<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>
</body>
</html>
