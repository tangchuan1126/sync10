<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	int refresh = StringUtil.getInt(request,"refresh");
	long sid = StringUtil.getLong(request,"sid");
	DBRow supplier = supplierMgrTJH.getDetailSupplier(sid);
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>上传资质文件</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
function ajaxSaveSupplier()
{
	var maxsize = 1024*1024;
	if($("#file").val() == "")
	{
		alert("请选择文件上传");
		
	}
	else
	{
		var filename=$("#file").val().split(".");
		if(filename[filename.length-1]=="xls" || filename[filename.length-1]=="docx" || filename[filename.length-1]=="doc" || filename[filename.length-1]=="gif" || filename[filename.length-1]=="jpg" || filename[filename.length-1]=="png" || filename[filename.length-1]=="bmp" || filename[filename.length-1]=="zip" || filename[filename.length-1]=="rar" || filename[filename.length-1]=="txt")
		{
	 		document.upload_form.submit();
	 	}
	 	else
	 	{
	 		alert("只能上传xls、doc、docx、jpg、gif、rar、zip和txt格式的文件");
	 	}
	 }	
}

function delAtitude(ap_id)
{
	if(confirm("确认要删除<%=supplier.getString("sup_name")%>的资质文件吗"))
		{
			document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/delSupplierOfAptitude.action";
			document.del_form.ap_id.value = ap_id;
			document.del_form.submit();
		}
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<form method="post" name="del_form">
	<input type="hidden" name="ap_id">
</form>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/saveImportSupplier.action" name="upload_form" enctype="multipart/form-data" method="post">
	
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="left" valign="top">
					<table width="95%" align="center" cellpadding="5" cellspacing="0" >
						<tr style=" padding-bottom:15px; padding-left:15px;">
							<td width="9%" align="left" valign="middle" nowrap="nowrap" class="STYLE3" >上传资质文件</td>
							<td width="91%" align="left" valign="middle" >
								 <input type="file" name="file" id="file">
								 <input type="hidden" name="sid" value="<%=sid%>">
							</td>
						</tr>
				  </table>
					<br/>
					
					<table width="95%" align="center" cellpadding="5" cellspacing="0" >
						
						<tr> 
					        <th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">供应商名称</th>
					        <th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">资质名称</th>
					        <th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">操作类型</th>
					  	</tr>
					<%
						DBRow [] rows = supplierMgrTJH.getAptitudeBySupId(sid);
						if(rows.length != 0)
						{
							for(int i=0;i<rows.length;i++)
							{
					 %>
						<tr  style=" padding-bottom:15px; padding-left:15px;">
						<td><%=supplier.getString("sup_name") %></td>
						<td width="9%" align="left" valign="top" nowrap="nowrap">
							<a href="" onclick="window.open('<%="supplier/upload_supplier/"+rows[i].getString("ap_name") %>')" style="text-align: 9px;color: gray;text-decoration: none"><%=rows[i].getString("ap_name") %></a>
							
						</td>
						<td width="91"align="left" valign="top" >
							<input name="Submit4" type="button" class="short-button" value="下载" onclick="window.open('supplier/upload_supplier/<%=rows[i].getString("ap_name") %>')"/>&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delAtitude(<%=rows[i].getString("id") %>)">
						</td>
					  </tr>
					  <%
					  	    }
					  	}
					  	else
					  	{
					   %>
					   <tr>
					   		<td colspan="2">暂时没有上传该供应商的资质文件</td>
					   </tr>
					   <%
					   	}
					    %>
				  </table>
				</td></tr>
              <tr>
                <td  align="right" valign="middle" class="win-bottom-line">				
				  <input type="button" name="Submit2" value="确定" class="normal-green" onClick="ajaxSaveSupplier()">
			
			    <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();"></td>
              </tr>
            </table>
</form>
</body>
</html>
