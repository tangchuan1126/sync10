<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.AccountKey"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>创建供应商</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<%--<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>--%>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
	div.cssDivschedule{
			width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:9999;display:none;
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
			background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
			background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
	
</style>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script>
var va = ['一','二','三','四','五','六','七','八','九'];
function addAssets(){
	var f = document.add_supplier_form;
	if (f.proName.value == ""){
		alert("请填写供应商名称");
		return false;
	 }
	 if(f.legal_name.value == ""){
	 	alert("请输入公司企业法人");
	 	return false;
	 }if(f.country.value == 0){
	 	alert("请选择国家");
	 	return false;
	 }if(f.product_line_id.value == 0){
	 	alert("请选择所在产品线");
	 	return false;
	 }
	 if(f.proLinkman.value == ""){
	 	alert("请输入联系人信息");
	 	return false;
	 }
	 if(f.proPhone.value == ""){
	 	alert("请输入联系电话");
	 	return false;
	 }
		document.update_form.sup_name.value = f.proName.value;
		document.update_form.product_line_id.value = f.product_line_id.value;
		document.update_form.nation.value = f.country.value;
		document.update_form.province.value = f.province_id.value;
		//document.update_form.address.value = f.proAddress.value;
		document.update_form.phone.value = f.proPhone.value;
		document.update_form.linkman.value = f.proLinkman.value;
		document.update_form.legal_name.value = f.legal_name.value;
		document.update_form.other_contacts.value = f.proOtherContacts.value;
		//document.update_form.bank_information.value = f.proBankInformation.value;
		document.update_form.address_houme_number.value = f.address_houme_number.value;//加的地址
		document.update_form.address_street.value = f.address_street.value;
		document.update_form.city_input.value = f.city_input.value;
		document.update_form.zip_code.value = f.zip_code.value;
		if(-1 == f.province_id.value){
			document.update_form.address_pro_input.value = f.address_pro_input.value;
		}
		return true;
}
function submitSupplier(bankInfo){
	document.update_form.bank_information.value = bankInfo;
	 if(document.update_form.bank_information.value == 'undefined'){
			document.update_form.bank_information.value = "";
	 }
	 
	if(addAssets()){
		 
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/addSupplierGZY.action',
			data:$("#add_form").serialize() + "&" + $("#account_form").serialize(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
				if(data && data.flag == "true"){
				    $.unblockUI();
					setTimeout("windowClose()", 1000);
				}else{
					$(".cssDivschedule").css("display","none");
					showMessage("提交失败","error");
				}
			},
			error:function(){alert("系统错误"); $.unblockUI();}
		})
	}
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};

function getProvinceByCountry(nation_id){
		var nation = "country_id="+nation_id;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/GetProvinceByCountryJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:nation,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("数据读取出错！");
				},
				
				success: function(data){
					$("#province_id").clearAll();
					$("#province_id").addOption("请选择...","");
					if(data != ""){
						$.each(data,function(i){
							$("#province_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}else{
						$("#province_id").addOption("手工输入",-1);
					}
				}
			});
	}
function handleProInput(){
 	var value = $("#province_id").val();
 	if(-1 == value){
 		$("#province_tr").after("<tr id='provice_input'><td align='right' valign='middle' class='STYLE3' colspan='1'>输入省份</td><td align='left' valign='middle' colspan='3'><input type='text' name='address_pro_input' id='address_pro_input' style='width: 250px'/></td></tr>");
 	}else{
 		removeAfterProIdInput();
 	}
 };
 function removeAfterProIdInput(){$("#provice_input").remove();}
 function addUSD(){
		//r_name,r_account,swift,r_address,r_bank_name,r_bank_address
		var len = $(".usd").length;	 
		var table =  "<table class='receiver usd next'>";
		table += "<tr><td rowspan='7' style='"+"width:150px;text-align:center;"+"'>外币收款信息"+va[len]+"</td>";
		table += "<td class='left'>收款人户名:</td><td><input type='text' class='noborder account_name' name='account_name'/></td>";
		table += "<td rowspan='7' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";
		table += "<tr><td class='left'>收款人账号:</td><td><input type='text' class='noborder account_number' name='account_number'/></td></tr>";
		table +="<tr><td class='left'>SWIFT:</td><td><input type='text' class='noborder account_swift' name='account_swift'/></td></tr>";
		table +="<tr><td class='left'>收款人地址:</td><td><input type='text' class='noborder account_address' name='account_address' /></td></tr>";
		table +="<tr><td class='left'>收款行名称:</td><td><input type='text' class='noborder account_blank' name='account_blank' /></td></tr>" ;
		table +="<tr><td class='left'>收款行地址:</td><td><input type='text' class='noborder account_blank_address' name='account_blank_address' /><input type='hidden' name='account_currency' value='usd'/></td></tr>" ;
		table +="<tr><td class='left'>收款人电话:</td><td><input type='text' class='noborder account_phone' name='account_phone'/></td></tr></table>" ;
	 	//	
		var addBar = $(".receiver").last();
		addBar.after($(table));
	}
	function addRMB(){
		var len  = $(".rmb").length ;
		var table =  "<table class='receiver rmb next'>";
		table += "<tr><td rowspan='4' style='"+"width:150px;text-align:center;"+"'>人民币收款信息"+va[len]+"</td>";
		table += "<td class='left'>收款人户名:</td>";
		table += "<td><input type='text' name='account_name' class='noborder account_name'/></td>";
		table += "<td rowspan='4' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";

		table += "<tr><td class='left'>收款人账号:</td>";
		table += "<td><input type='text' name='account_number' class='noborder account_number'/></td></tr>";
		table +="<tr><td class='left'>收款开户行:</td><td><input type='text' name='account_blank' class='noborder account_blank'/></td></tr>";
		table +="<tr><td class='left'>收款人电话:</td><td><input type='text' name='account_phone' class='noborder account_phone'/><input type='hidden' name='account_currency' value='rmb'/></td></tr></table>";
	 	// 获取最后一个RMB的table 然后添加到他的后面。如果是没有的话,那么就选取H1标签然后添加到他的后面
		var addBar = $(".receiver").last();
		addBar.after($(table));
	}
</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}
.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
table.receiver td {border:1px solid silver;}
table.receiver td input.noborder{border-bottom:1px solid silver;width:200px;}
table.receiver td.left{width:120px;text-align:right;}
table.next{margin-top:5px;}
table.receiver{border-collapse:collapse ;}
span._hand{cursor:pointer;}
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="">
<div class="cssDivschedule"></div>
<form  method="post" name="update_form" id="add_form"/>
    <input type="hidden" name="sup_name" />
    <input type="hidden" name="nation" />
    <input type="hidden" name="province" />
    <input type="hidden" name="linkman" />
    <input type="hidden" name="address" />
    <input type="hidden" name="legal_name" />
    <input type="hidden" name="phone" />
    <input type="hidden" name="other_contacts" />
    <input type="hidden" name="bank_information" />
    <input type="hidden" name="sid" />
    <input type="hidden" name="password" />
    <input type="hidden" name="product_line_id" />
    <input type="hidden" name="address_houme_number" />
    <input type="hidden" name="address_street" />
    <input type="hidden" name="city_input" />
    <input type="hidden" name="zip_code" />
    <input type="hidden" name="address_pro_input" />
  </form>		
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">
	    <table width="98%" border="0" cellspacing="0" cellpadding="0">
	      <tr>
	        <td>
				<form name="add_supplier_form" method="post" action="" >
				  <table width="98%" border="0" cellspacing="5" cellpadding="2">
				  	<tr>
				  		 <td align="right" valign="middle" class="STYLE3" >所在产品线</td>
					      <td align="left" valign="middle">
						      <select style="width:200px" name="product_line_id" id="product_line_id">
						      	<option value="0">请选择...</option>
						      	<%
						      		DBRow[] productLine =  productLineMgrTJH.getAllProductLine();
						      		for(int i=0;i<productLine.length;i++){
						      	%>
						      	<option value="<%=productLine[i].getString("id") %>"><%=productLine[i].getString("name") %></option>
						      	<%
						      		}
						      	%>
						      </select>
					      </td>
				  	</tr>
				    <tr>
				      <td align="right" valign="middle" class="STYLE1 STYLE2" >供应商名称</td>
				      <td align="left" valign="middle"><input name="proName" type="text" style="width: 250px" id="proName" ></td>
				   	</tr>
				    <tr>  
				      <td align="right" valign="middle" class="STYLE3" >企业法人</td>
				      <td align="left" valign="middle" ><input name="legal_name" type="text" id="legal_name"  class="input-line" style="width: 250px"></td>
				    </tr>
				    <tr>
				      <td align="right" valign="middle" class="STYLE3" >门牌号</td>
					  <td align="left" valign="middle" ><input name="address_houme_number" type="text" class="input-line" id="address_houme_number" style="width:400px;"></td>
				    </tr>
				    <tr>
					 <td align="right" valign="middle" class="STYLE3" >街道</td>
					 <td align="left" valign="middle" ><input name="address_street" type="text" class="input-line" id="address_street" style="width:400px;"></td>
				    </tr> 
				    <tr>
					 <td align="right" valign="middle" class="STYLE3" >城市</td>
					 <td align="left" valign="middle" ><input name="city_input" type="text" class="input-line" id="city_input" style="width:400px;"></td>
				    </tr> 
				    <tr>
					 <td align="right" valign="middle" class="STYLE3" >邮编</td>
					 <td align="left" valign="middle" ><input name="zip_code" type="text" class="input-line" id="zip_code" style="width: 250px"></td>
				    </tr> 
					<tr id="province_tr">
				      <td align="right" valign="middle" class="STYLE3" colspan="1">省份、洲</td>
				      <td align="left" valign="middle" colspan="3">
				      <select style="width:250px" name="province_id" id="province_id" onchange="handleProInput()">
				      	<option value="0">请选择...</option>
				      </select>      
				      </td>
				    </tr>
				    <tr>
				    	<td align="right" valign="middle" class="STYLE3" colspan="1">所在国家</td>
				      <td align="left" valign="middle" colspan="1">
				        <%
						DBRow countrycode[] = orderMgr.getAllCountryCode();
						String selectBg="#ffffff";
						String preLetter="";
						%>
				      <select style="width:250px" name="country" id="country" onChange="getProvinceByCountry(this.value)">
						  <option value="0">请选择...</option>
						  <%
						  for (int i=0; i<countrycode.length; i++){
						  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
								if (selectBg.equals("#eeeeee")){
									selectBg = "#ffffff";
								}
								else{
									selectBg = "#eeeeee";
								}
							}
							
							preLetter = countrycode[i].getString("c_country").substring(0,1);
						  %>
						    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
						  <%
						  }
						  %>
				      </select>
				      </td>
				    </tr>
				    <tr>
				      <td align="right" valign="middle" class="STYLE3" >联系人</td>
				      <td align="left" valign="middle" ><input style="width:250px" name="proLinkman" type="text" id="proLinkman" class="input-line">
				      </td>
				    </tr>
				    <tr>
				      <td align="right" valign="middle" class="STYLE3" >联系电话</td>
				      <td align="left" valign="middle" ><input name="proPhone" type="text" id="proPhone" class="input-line" style="width: 250px"></td>
				    </tr>
				    <tr>
				    	<td align="right" valign="middle" class="STYLE3" >其他联系方式</td>
					    <td align="left" valign="middle" >
					      	<textarea name="proOtherContacts" class="input-line" id="proOtherContacts" style="width:250px"></textarea>      
					    </td>
				    </tr>
				    
				  </table>
				</form>		
			</td>
	      </tr>
	    </table>
    </td>
  </tr>
  </table >
  <form id="account_form" name="account_form">
  		<input type="hidden" name="account_with_type" value='<%=AccountKey.SUPPLIER %>'/>
	 <table width="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			    <td colspan="2" align="center" valign="top">
				  <table width="98%" border="0" cellpadding="0" cellspacing="0">
				  	 <tr>
					      <td align="right" valign="middle" class="STYLE3" style="width:127px;">银行信息&nbsp;</td>
					      		<td>		      		    		     
							      		<h1 id="addBar"> 
							      			<input type="button" value="添加人民币收款" onclick="addRMB();"/>
							      			<input type="button" value="添加外币收款" onclick="addUSD();"/>
							      		</h1>
							      		<table class="receiver rmb next">
							      			<tr>
							      				<td rowspan="4" style='width:150px;text-align:center;'>人民币收款信息一</td>
							      				<td class="left">收款人户名:</td>
							      				<td><input type="text" name="account_name" class="noborder account_name"/></td>
							      				<td rowspan="4" style="width:80px;text-align:center;"><span onclick="deletePaymentInfo(this)" class="_hand">删除</span></td>
							      			</tr>
							      			<tr>
							      				<td class="left">收款人账号:</td>
							      				<td><input type="text" name="account_number" class="noborder account_number"/></td>
							      			</tr>
							      			<tr>
							      				<td class="left">收款开户行:</td>
							      				<td><input type="text" name="account_blank" class="noborder account_blank"/></td>
							      			</tr>
							      			<tr>
							      				<td class="left">收款人电话:</td>
							      				<td>
							      					<input type="text" name="account_phone" class="noborder account_phone"/>
							      				 	<input type="hidden" name="account_currency" value='rmb' />
							      				</td>
							      			</tr>
							      		</table>
							   </td>	
					    </tr>
				  </table>
				</td>
		  </tr>
	</table>
  </form>
  <table width="100%" height="30px" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="增加" class="normal-green" onclick="submitApply();">
	</td>
  </tr>
</table> 
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
  <script type="text/javascript">
	function submitApply()
	{
			var str  = "";
			var rmbInfo = $(".rmb");
			if(rmbInfo.length > 0 ){
				for(var index = 0 , count = rmbInfo.length ; index < count ; index++ ){
					var _table = $(rmbInfo.get(index));
					var fixIndex =  index+1;
					var r_name =  $("input.account_name",_table).val();
					var r_account = $("input.account_number",_table).val();
					var r_bank = $("input.account_blank",_table).val();
					var r_phone = $("input.account_phone",_table).val();
					str +="\n";
					if($.trim(r_name).length > 0 ){
						str += "收款人户名:"+r_name+"\n";
					}else{
					 
						alert("输入收款人户名!");
						return;
					}
					if($.trim(r_account).length > 0 ){
						str += "收款人账号:"+r_account+"\n";
					}else{
						alert("输入收款人账号!");
						return;
					}
					if($.trim(r_bank).length > 0 ){
						str += "收款开户行:"+r_bank+"\n";
					}
					if($.trim(r_phone).length > 0 ){
						str += "联系电话:"+r_phone+"\n";
					}	
				}
			}
			var str1  = "";
			var usdInfo = $(".usd");
		 
			if(usdInfo.length > 0 ){
				for(var index = 0 , count = usdInfo.length ; index < count ; index++ ){
				//	r_name,r_account,swift,r_address,r_bank_name,r_bank_address
				//	收款人户名,收款人账号,SWIFT,收款人地址,收款行名称,收款行地址
					var _table = $(usdInfo.get(index));
					 
					var r_name =  $("input.account_name",_table).val();
					var r_account = $("input.account_number",_table).val();
					var swift = $("input.account_swift",_table).val();
					var r_address = $("input.account_address",_table).val();
					var r_bank_name = $("input.account_blank",_table).val();
					var r_bank_address = $("input.account_blank_address",_table).val();
					str1 +="\n";
					if($.trim(r_name).length > 0 ){
						str1 += "收款人户名:"+r_name+"\n";
					}else{
						alert("输入收款人户名!");
						return;
					}
					if($.trim(r_account).length > 0 ){
						str1 += "收款人账号:"+r_account+"\n";
					}else{
						alert("输入收款人账号!");
						return;
					}
					if($.trim(swift).length > 0 ){
						str1 += "SWIFT:"+swift+"\n";
					}
					if($.trim(r_address).length > 0 ){
						str1 += "收款人地址:"+r_address+"\n";
					}
					if($.trim(r_bank_name).length > 0 ){
						str1 += "收款行名称:"+r_bank_name+"\n";
					}
					if($.trim(r_bank_address).length > 0 ){
						str1 += "收款行地址:"+r_bank_address+"\n";
					}
				}
			}
			 str += str1;
			 if(str.length <= 4){
					alert("请先输入收款人信息");
					return ;
			 }else{
			     fixAccountInfo();
				 submitSupplier(str);
				 
			 }
	}
	//重新梳理账号信息
	function fixAccountInfo(){
		$(".receiver").each(function(index){
			//将里面的name 都重新加上数字
			var fixIndex = index + 1 ;
			 
			var node = $(this);
			var inputs = $("input",node);
		 	for(var inputIndex = 0 , count = inputs.length ; inputIndex < count ; inputIndex++ ){
				var nodeInput = $(inputs[inputIndex]);
				var name = nodeInput.attr("name");
				if(!(isCanChangeName(name))){
					nodeInput.attr("name",name+"_"+fixIndex);
				}
				
		    }

		})
	}
 
	function isCanChangeName(name) {
	    var r = /[0-9]$/;
	    return r.test(name);
	}
			 
			function deletePaymentInfo(_this){
 
				var parentNode = $(_this).parent().parent().parent().parent();
				if($(".receiver").length == 1){
					alert("至少包含一个收款信息");
					return ;
				}
 				 parentNode.remove();
			}
</script>
</body>
</html>
