<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
//StringUtil.getLong(request,"sid");
DBRow row = adminMgr.getAdminLoginBean(session).getAttach();//supplierMgrTJH.getDetailSupplier();

long sid = row.get("id",0l);//adminMgr.getAdminLoginBean(session);

row = supplierMgrTJH.getDetailSupplier(sid);

long pro_id = row.get("province_id",0l);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>创建供应商</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<script>
function updateSupplier()
{
	var f = document.mod_supplier_form;
	
	if (f.proName.value == "")
	 {
		alert("请填写供应商名称");
		return false;
	 }
	 if(f.legal_name.value == "")
	 {
	 	alert("请输入公司企业法人");
	 	return false;
	 }
	 if(f.proLinkman.value == "")
	 {
	 	alert("请输入联系人信息");
	 	return false;
	 }
	 if(f.proPhone.value == "")
	 {
	 	alert("请输入联系电话");
	 	return false;
	 }
	 else
	 {
		document.update_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/modSupplier.action";
		document.update_form.sup_name.value = f.proName.value;		
		document.update_form.nation.value = f.country.value;
		document.update_form.province.value = f.province_id.value;
		document.update_form.address.value = f.proAddress.value;
		document.update_form.phone.value = f.proPhone.value;
		document.update_form.linkman.value = f.proLinkman.value;
		document.update_form.legal_name.value = f.legal_name.value;
		document.update_form.other_contacts.value = f.proOtherContacts.value;
		document.update_form.bank_information.value = f.proBankInformation.value;
		document.update_form.sid.value = <%=sid%>;
		document.update_form.submit();	
	 }
}

function getProvinceByCountry(nation_id)
{
		var nation = "country_id="+nation_id;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/GetProvinceByCountryJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:nation,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("数据读取出错！");
				},
				
				success: function(data){
					$("#province_id").clearAll();
					$("#province_id").addOption("请选择省、州...","");
					if(data != "")
					{
						$.each(data,function(i){
							$("#province_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					var province_id = "<%=pro_id%>";
					if (province_id!="")
					{
						$("#province_id").setSelectedValue(province_id);
					}
				}
			});
	}
	
</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
</style>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="getProvinceByCountry(document.mod_supplier_form.country.value)">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><br/>
		
<form name="mod_supplier_form" method="post" action="" >
  <table width="98%" height="70%" border="0" cellspacing="5" cellpadding="2">
    <tr>
      <td align="right" valign="middle" class="STYLE1 STYLE2" >供应商名称</td>
      <td align="left" valign="middle" ><input name="proName" type="text" style="width: 250px" id="proName" value="<%=row.getString("sup_name") %>"></td>
       <td align="right" valign="middle" class="STYLE3" >企业法人</td>
       <td align="left" valign="middle" ><input name="legal_name" type="text" id="legal_name" value="<%=row.getString("legal_name") %>"  class="input-line"></td>
    </tr>
    <tr>
      <td align="right" valign="middle" class="STYLE3" colspan="1">所在国家</td>
      <td align="left" valign="middle" colspan="3">
      <%
	DBRow countrycode[] = orderMgr.getAllCountryCode();
	String selectBg="#ffffff";
	String preLetter="";
	%>
      <select  name="country" id="country" onChange="getProvinceByCountry(this.value)">
	  <option value="0">选择国家...</option>
	  <%
	  for (int i=0; i<countrycode.length; i++)
	  {
	  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
		{
			if (selectBg.equals("#eeeeee"))
			{
				selectBg = "#ffffff";
			}
			else
			{
				selectBg = "#eeeeee";
			}
		}  	
		
		preLetter = countrycode[i].getString("c_country").substring(0,1);
	  %>
	    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>" <%=countrycode[i].get("ccid",0l) == row.get("nation_id",0l)?"selected":"" %>><%=countrycode[i].getString("c_country")%></option>
	  <%
	  }
	  %>
      </select>
      </td>
    </tr>
	<tr>
	 <td align="right" valign="middle" class="STYLE3" colspan="1">省份、洲</td>
      <td align="left" valign="middle" colspan="3">
	      <select name="province_id" id="province_id">
	      	<option value="0">请选择省、州</option>
	      </select>     
      </td>
	</tr>
    
    <tr>
      <td align="right" valign="middle" class="STYLE3" >联系人</td>
      <td align="left" valign="middle" ><input name="proLinkman" type="text" id="proLinkman" value="<%=row.getString("linkman") %>" class="input-line">
      </td>
      
      <td align="right" valign="middle" class="STYLE3" >联系电话</td>
      <td align="left" valign="middle" ><input name="proPhone" type="text" id="proPhone" value="<%=row.getString("phone") %>" class="input-line">      </td>
    </tr>
    <tr>
      <td align="right" valign="middle" class="STYLE3" >所在地址</td>
      <td align="left" valign="middle" ><textarea name="proAddress" id="proAddress" style="width:250px"><%=row.getString("address") %></textarea></td>
     
      <td align="right" valign="middle" class="STYLE3" >其他联系方式</td>
      <td align="left" valign="middle" ><textarea name="proOtherContacts" class="input-line" id="proOtherContacts"><%=row.getString("other_contacts") %></textarea></td>
    </tr>
    <tr>
      <td align="right" valign="middle" class="STYLE3" >银行信息</td>
      <td align="left" valign="middle" colspan="3"><textarea  name="proBankInformation" rows="50" id="proBankInformation" style='width:650px;height:270px;'><%=row.getString("bank_information") %></textarea>      </td>
    </tr>
  </table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存修改" class="normal-green" onClick="updateSupplier();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="window.history.go(-1);">
	</td>
  </tr>
</table> 
<form  method="post" name="update_form" >
    <input type="hidden" name="sup_name">
    <input type="hidden" name="nation">
    <input type="hidden" name="province">
    <input type="hidden" name="linkman">
    <input type="hidden" name="address">
    <input type="hidden" name="legal_name">
    <input type="hidden" name="phone" />
    <input type="hidden" name="other_contacts" />
    <input type="hidden" name="bank_information">
    <input type="hidden" name="sid">
  </form>
</body>
</html>
