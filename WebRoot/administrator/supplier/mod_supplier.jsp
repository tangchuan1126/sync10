<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.iface.zr.AccountMgrIfaceZr"%>
<%@page import="com.cwc.app.key.AccountKey"%>
<%@ include file="../../include.jsp"%> 
<%


long sid = StringUtil.getLong(request,"sid");
DBRow row = supplierMgrTJH.getDetailSupplier(sid);
DBRow[] accounts = accountMgrZr.getAccountByAccountIdAndAccountWithType(sid,AccountKey.SUPPLIER);
long pro_id = row.get("province_id",0l);
String deleteAccountAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/account/DeleteAccountAction.action";
String updateAccountUrl =  ConfigBean.getStringValue("systenFolder") + "administrator/admin/ebay_account_update.html";
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改供应商</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script>
 
function updateSupplier()
{
	var f = document.mod_supplier_form;
	
	if (f.proName.value == ""){
		alert("请填写供应商名称");
		return false;
	 }
	 if(f.legal_name.value == ""){
	 	alert("请输入公司企业法人");
	 	return false;
	 }if(f.country.value == 0){
	 	alert("请选择国家");
	 	return false;
	 }
	 if(f.product_line_id.value == 0){
	 	alert("请选择所在产品线");
	 	return false;
	 }
	 if(f.proLinkman.value == ""){
	 	alert("请输入联系人信息");
	 	return false;
	 }
	 if(f.proPhone.value == ""){
	 	alert("请输入联系电话");
	 	return false;
	 }
 
		document.update_form.sup_name.value = f.proName.value;		
		document.update_form.nation.value = f.country.value;
		document.update_form.province.value = f.province_id.value;
		document.update_form.product_line_id.value = f.product_line_id.value;
		//document.update_form.address.value = f.proAddress.value;
		document.update_form.phone.value = f.proPhone.value;
		document.update_form.linkman.value = f.proLinkman.value;
		document.update_form.legal_name.value = f.legal_name.value;
		document.update_form.other_contacts.value = f.proOtherContacts.value;
		if(f.proBankInformation){
			document.update_form.bank_information.value = f.proBankInformation.value;
		}
		document.update_form.sid.value = <%=sid%>;
		document.update_form.password.value = f.pwd.value;
		document.update_form.address_houme_number.value = f.address_houme_number.value;//加的地址
		document.update_form.address_street.value = f.address_street.value;
		document.update_form.city_input.value = f.city_input.value;
		document.update_form.zip_code.value = f.zip_code.value;
		if(-1 == f.province_id.value){
	 
			document.update_form.address_pro_input.value = f.address_pro_input.value;
		 
		}
 		return true;
}
function submitSupplier(){
 	if(updateSupplier()){
 		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/modSupplierGZY.action',
			data:$("#add_form").serialize(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
		     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
				if(data && data.flag == "true"){
				    $.unblockUI();
					setTimeout("windowClose()", 1000);
				}else{
					alert(11);
					showMessage("提交失败","error");
				}
			},
			error:function(){alert("系统错误"); $.unblockUI();}
		})
	}
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function getProvinceByCountry(nation_id)
{
		var nation = "country_id="+nation_id;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/GetProvinceByCountryJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:nation,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("数据读取出错！");
				},
				
				success: function(data){
					$("#province_id").clearAll();
					$("#province_id").addOption("请选择...","");
					if(data != "")
					{
						$.each(data,function(i){
							$("#province_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}else{
						$("#province_id").addOption("手工输入",-1);
					}
					var province_id = "<%=pro_id%>";
					if (province_id!="")
					{
						$("#province_id").setSelectedValue(province_id);
						if(-1 == province_id){
							$("#province_tr").after("<tr id='provice_input'><td align='right' valign='middle' class='STYLE3' colspan='1'>输入省份</td><td align='left' valign='middle' colspan='3'><input type='text' name='address_pro_input' id='address_pro_input' value='<%=row.getString("address_pro_input") %>' style='width: 250px'/></td></tr>");
						}
					}
				}
			});
	}
function handleProInput(){
 	var value = $("#province_id").val();
 	if(-1 == value){
 		$("#province_tr").after("<tr id='provice_input'><td align='right' valign='middle' class='STYLE3' colspan='1'>输入省份</td><td align='left' valign='middle' colspan='3'><input type='text' name='address_pro_input' id='address_pro_input' style='width: 250px'/></td></tr>");
 	}else{
 		removeAfterProIdInput();
 	}
 };
 function removeAfterProIdInput(){$("#provice_input").remove();}
 function deleteAccount(account_id){
     	$.artDialog.confirm('确定删除联该系账号?', function () {
	     	$.ajax({
					url:'<%= deleteAccountAction%>' + "?account_id="+ account_id ,
					dataType:'json',
					beforeSend:function(request){
					    $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
					},
					error:function(){alert("系统错误!");$.unblockUI();},
					success:function(data){
					    $.unblockUI();
					    if(data && data.flag === "success"){
							window.location.reload();
						}
					}
			})
		}, function () {
	  		  
		});
	
 }
 function updateAccount(account_id){
     var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/account/account_update.html?account_id="+ account_id; 
	 $.artDialog.open(uri , {title: '修改账号信息',width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
 }
 function refreshWindow(){window.location.reload();}
 function addAccount(){
     var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/account/account_add.html?account_with_id="+ '<%=row.getString("id")%>' + "&account_with_type=" + '<%= AccountKey.SUPPLIER%>'; 
	 $.artDialog.open(uri , {title: '添加账号信息',width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
 }
</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
td.left{width:75px;text-align:right;}
td.right{text-align:left ;font-weight:bold;color: #666666;width:250px;}
table.account_table{border:1px solid silver;border-collapse:collapse;}
table.account_table td{border:1px solid silver;}
</style>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="getProvinceByCountry(document.mod_supplier_form.country.value)">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
 <form  method="post" name="update_form" id="add_form"/>
    <input type="hidden" name="sup_name" />
    <input type="hidden" name="nation" />
    <input type="hidden" name="province" />
    <input type="hidden" name="linkman" />
    <input type="hidden" name="address" />
    <input type="hidden" name="legal_name" />
    <input type="hidden" name="phone" />
    <input type="hidden" name="other_contacts" />
    <input type="hidden" name="bank_information" />
    <input type="hidden" name="sid" />
    <input type="hidden" name="password" />
    <input type="hidden" name="product_line_id" />
    <input type="hidden" name="address_houme_number" />
    <input type="hidden" name="address_street" />
    <input type="hidden" name="city_input" />
    <input type="hidden" name="zip_code" />
    <input type="hidden" name="address_pro_input" />
</form>			
<form name="mod_supplier_form" method="post" action="" >
  <table width="98%" border="0" cellspacing="5" cellpadding="2">
  	<tr>
  		<td align="right" valign="middle" class="STYLE3" >所在产品线</td>
      <td align="left" valign="middle">
	      <select style="width:200px" name="product_line_id" id="product_line_id">
	      	<option value="0">请选择...</option>
	      	<%
	      		DBRow[] productLine =  productLineMgrTJH.getAllProductLine();
	      		for(int i=0;i<productLine.length;i++){
	      		if(productLine[i].getString("id").equals(row.getString("product_line_id"))){
	      	%>
	      	<option value="<%=productLine[i].getString("id") %>" selected="selected"><%=productLine[i].getString("name") %></option>
	      	<%
	      		}else{
	      	%>
	      	<option value="<%=productLine[i].getString("id") %>"><%=productLine[i].getString("name") %></option>
	      	<%
	      		}
	      			}
	      	%>
	      </select>
  	 </td>
  	</tr>
    <tr>
      <td align="right" valign="middle" class="STYLE1 STYLE2" >供应商名称</td>
      <td align="left" valign="middle" ><input name="proName" type="text" style="width: 250px" id="proName" value="<%=row.getString("sup_name") %>"></td>
    </tr>
    <tr>
       <td align="right" valign="middle" class="STYLE3" >企业法人</td>
       <td align="left" valign="middle" ><input name="legal_name" type="text" id="legal_name" value="<%=row.getString("legal_name") %>"  class="input-line" style="width: 250px"></td>
    </tr>
    <tr>
      <td align="right" valign="middle" class="STYLE3" >门牌号</td>
	  <td align="left" valign="middle" ><input name="address_houme_number" type="text" class="input-line" id="address_houme_number" value="<%=row.getString("address_houme_number") %>" style="width:400px;"></td>
    </tr>
    <tr>
	 <td align="right" valign="middle" class="STYLE3" >街道</td>
	 <td align="left" valign="middle" ><input name="address_street" type="text" class="input-line" id="address_street" value="<%=row.getString("address_street") %>" style="width:400px;"></td>
    </tr> 
    <tr>
	 <td align="right" valign="middle" class="STYLE3" >城市</td>
	 <td align="left" valign="middle" ><input name="city_input" type="text" class="input-line" id="city_input" value="<%=row.getString("city_input") %>" style="width:400px;"></td>
    </tr> 
    <tr>
	 <td align="right" valign="middle" class="STYLE3" >邮编</td>
	 <td align="left" valign="middle" ><input name="zip_code" type="text" class="input-line" id="zip_code" value="<%=row.getString("zip_code") %>" style="width: 250px"></td>
    </tr> 
    <tr id="province_tr">
	 <td align="right" valign="middle" class="STYLE3" colspan="1">省份、洲</td>
      <td align="left" valign="middle" colspan="3">
	      <select style="width:250px" name="province_id" id="province_id" onchange="handleProInput()">
	      	<option value="0">请选择</option>
	      </select>     
      </td>
	</tr>
    <tr>
      <td align="right" valign="middle" class="STYLE3" colspan="1">所在国家</td>
      <td align="left" valign="middle" colspan="1">
      <%
	DBRow countrycode[] = orderMgr.getAllCountryCode();
	String selectBg="#ffffff";
	String preLetter="";
	%>
      <select style="width:250px" name="country" id="country" onChange="getProvinceByCountry(this.value)">
	  <option value="0">选择国家...</option>
	  <%
	  for (int i=0; i<countrycode.length; i++)
	  {
	  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
		{
			if (selectBg.equals("#eeeeee"))
			{
				selectBg = "#ffffff";
			}
			else
			{
				selectBg = "#eeeeee";
			}
		}  	
		
		preLetter = countrycode[i].getString("c_country").substring(0,1);
	  %>
	    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>" <%=countrycode[i].get("ccid",0l) == row.get("nation_id",0l)?"selected":"" %>><%=countrycode[i].getString("c_country")%></option>
	  <%
	  }
	  %>
      </select>
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle" class="STYLE3" >联系人</td>
      <td align="left" valign="middle" ><input style="width:250px" name="proLinkman" type="text" id="proLinkman" value="<%=row.getString("linkman") %>" class="input-line">
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle" class="STYLE3" >联系电话</td>
      <td align="left" valign="middle" ><input name="proPhone" type="text" id="proPhone" value="<%=row.getString("phone") %>" class="input-line" style="width: 250px"></td>
    </tr>
<%--    <tr>--%>
<%--      <td align="right" valign="middle" class="STYLE3" >所在地址</td>--%>
<%--      <td align="left" valign="middle" ><textarea name="proAddress" id="proAddress" style="width:250px"><%=row.getString("address") %></textarea></td>--%>
<%--    </tr>--%>
    <tr>
      <td align="right" valign="middle" class="STYLE3" >其他联系方式</td>
      <td align="left" valign="middle" ><textarea name="proOtherContacts" class="input-line" id="proOtherContacts" style="width:250px"><%=row.getString("other_contacts") %></textarea></td>
    </tr>
    <tr>
      <td align="right" valign="middle" class="STYLE3" >银行信息</td>
      <td align="left" valign="middle">
      	<input type="button" value="添加收款信息" onclick="addAccount()" class="long-button"/>  
      	<br />
      	<!-- 增加收款信息 -->
      	<!-- 修改收款信息 -->
      	<!-- 查询account表如果是有记录的那么就读取出来显示。可以修改,可以新添加.如果是没有就按照以前的显示在一个textarea里面 -->
      	<%
      		if(accounts != null && accounts.length > 0 ){
				//读取信息显示出来
				String[] indexStr = new String[]{"一","二","三","四","五","六","七","八","九"};
				for(int index = 0 , count = accounts.length ; index < count ;index++ ){	
					boolean isOtherCurrency = !accounts[index].getString("account_currency").toUpperCase().equals("RMB");
					int rowNumber = isOtherCurrency?8:5;
				%>
					<table class="account_table">
						<tr>
							<td  rowspan="<%=rowNumber %>">账号<%=indexStr[index] %></td>
							<td class="left">货币类型:</td>
							<td class="right"><%=accounts[index].getString("account_currency").toUpperCase() %></td>
							<td rowspan="<%=rowNumber %>">	
								<a href="javascript:void(0)" onclick="deleteAccount('<%=accounts[index].getString("account_id") %>')">删除</a>
								<a href="javascript:void(0)" onclick="updateAccount('<%=accounts[index].getString("account_id") %>')">编辑</a>
							</td>
						</tr>
					
						 <tr>
							<td class="left">账号户名:</td>
							<td class="right"><%=accounts[index].getString("account_name").toUpperCase() %></td>
						</tr>
						<tr>
							<td class="left">账号:</td>
							<td class="right"><%= accounts[index].getString("account_number") %></td>
						</tr>
						<tr>
							<td class="left">联系电话:</td>
							<td class="right"><%= accounts[index].getString("account_phone") %></td>
						</tr>
						<tr>
							<td class="left">开户行:</td>
							<td class="right"><%= accounts[index].getString("account_blank") %></td>
						</tr>
						
						<%if(isOtherCurrency){ %>
							 <tr>
							 	<td class="left">SWIFT:</td>
							 	<td class="right"><%= accounts[index].getString("account_swift") %></td>
							 </tr>		
							 <tr>
								<td class="left">户名地址:</td>
								<td class="right"><%= accounts[index].getString("account_address") %></td>
							</tr>
							<tr>
								<td class="left">银行地址:</td>
								<td class="right"><%= accounts[index].getString("account_blank_address") %></td>
							</tr>
						<%} %>
					</table>
					 <br />	 
				 
				<% 
				}
      		}else{
      	%>
      		<textarea style='width:350px;height:100px;' id="proBankInformation"  name="proBankInformation"><%=row.getString("bank_information") %></textarea>   
     	<%}%>
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle" class="STYLE3" >密码</td>
      <td align="left" valign="middle" >
      	<input name="pwd" type="password" id="pwd">（留空不修改）
	  </td>
    </tr>
  </table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存修改" class="normal-green" onClick="submitSupplier();">

<%--	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">--%>
	</td>
  </tr>
</table> 
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>
