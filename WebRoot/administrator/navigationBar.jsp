<%@ page contentType="text/html;charset=UTF-8"%><!DOCTYPE html>
<html lang="cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Visionari Micro ERP v2 ™ </title>
    <!-- mCustomScrollbar -->
    <!-- <link href="/Sync10-ui/lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"> -->
    <!-- Bootstrap Core CSS -->
    <link href="/Sync10-ui/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="/Sync10-ui/pages/Examples/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- Timeline CSS -->
    <link href="/Sync10-ui/pages/Examples/css/plugins/timeline.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/Sync10-ui/pages/Examples/css/sb-admin-2.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="/Sync10-ui/pages/Examples/css/plugins/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="/Sync10-ui/bower_components/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
     <!-- 自定义 CSS -->
    <link href="/Sync10-ui/pages/Examples/css/VisiStyle.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      html,body{overflow: hidden;padding: 0px}   
      
      /*body .sidebar{position:static;width:100%;display: block;margin:0px;}*/
      /* body .navbar-default{background: #fff;} */
      /*body .nav>li>a:hover,body .side-menubox .a_select{font-weight: bold;}*/
      /*body .sidebar ul li{border-bottom-color: #2A2A2A}*/
      
      #Compond_navbar{padding: 0px;border-width: 0px}
      body .sidebar{position:static;width:100%;display: block;margin:0px;}
      body .navbar-default{background: #fff;}
      body .nav>li>a{color: #5E5E5E}
      body .nav>li>a:hover,body .side-menubox .a_select{font-weight: bold;color: #5E5E5E}
      
      
      .side-menubox .mCSB_scrollTools {
		  right: -8px !important;
		}
      
      
    </style>

</head>
<body>
<div class="navbar-default sidebar" role="navigation">
<div id="Compond_navbar" class="sidebar-nav navbar-collapse"></div>
</div>    
</body>
<script type="text/javascript" data-main="./leftMenu" src="/Sync10-ui/bower_components/requirejs/require.js"></script> 
</html>