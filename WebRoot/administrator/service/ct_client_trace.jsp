<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.api.ClientServiceKey"%>
<%@ include file="../../include.jsp"%> 
<%
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(25);

long cid = StringUtil.getLong(request,"cid");

DBRow rows[]=clientMgr.getClientTracesByCid(cid,pc);

DBRow detailClient = clientMgr.getDetailClientByCid(cid);

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td style="font-size:15px;font-weight:bold;color:#CC0000">客户：<%=detailClient.getString("email")%></td>
  </tr>
</table>
<br>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="thetable">
  <form method="post"  name="listForm">
<input type="hidden" name="summary">
<input type="hidden" name="cid">
<input type="hidden" name="note">
	  <thead>
    <tr> 
        <td width="18%" align="center" valign="middle" class="left-title">日期</td>
        <td width="16%" align="center" valign="middle" class="right-title">客服</td>
        <td width="17%" align="center" valign="middle" class="right-title">服务类型</td>
        <td width="49%" align="left" valign="middle" class="right-title">服务内容</td>
    </tr>
	</thead>
    <%
String className;
String serviceColor = "#FF6600";
for ( int i=0; i<rows.length; i++ )
{
	if ( i%2==0 )
	{
		className = "row-line1";
	}
	else
	{
		className = "row-line2";
	}
	
	if (rows[i].get("clients_service",0)==88)
	{
		serviceColor = "#FF6600";
	}
	else
	{
		serviceColor = "#000000";
	}
%>
    <tr  > 
      <td height="60" align="center" valign="middle" class="<%=className%>"  style='word-break:break-all;' ><%=rows[i].getString("post_date")%></td>
      <td align="center" valign="middle"  class="<%=className%>" style='word-break:break-all;' ><%=rows[i].getString("account")%></td>
      <td align="center" valign="middle" class="<%=className%>"  style='word-break:break-all;' >
	  <span style="color:<%=serviceColor%>">
	  <%=ClientServiceKey.getNameByVal(rows[i].get("clients_service",0))%>
	  </span>
	  </td>
      <td align="left" valign="middle"  class="<%=className%>" style='word-break:break-all;padding-top:10px;padding-bottom:10px;line-height:20px;' ><%=StringUtil.ascii2Html(rows[i].getString("note"))%></td>
    </tr>
    <%
}
%>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
          <input type="hidden" name="p">
		  <input type="hidden" name="cid" value="<%=cid%>">


		
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
</body>
</html>
