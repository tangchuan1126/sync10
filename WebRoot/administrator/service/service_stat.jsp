<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.api.ClientServiceKey"%>
<%@ include file="../../include.jsp"%> 
<%
 	String cmd = StringUtil.getString(request,"cmd");
 String st = StringUtil.getString(request,"st");
 String en = StringUtil.getString(request,"en");
 String account = StringUtil.getString(request,"account");


 TDate tDate = new TDate();
 tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

 String input_st_date,input_en_date;
 if ( st.equals("") )
 {	
 	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
 }
 else
 {	 
 	input_st_date = st;
 }


 if ( en.equals("") )
 {	
 	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
 }
 else
 {	
 	input_en_date = en;
 }

 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 if (account.equals("")&&cmd.equals(""))
 {
 	account = adminLoggerBean.getAccount();
 }

 ArrayList inServiceAl = new ArrayList();
 String inService[] = request.getParameterValues("inService");//需要统计的服务类型
 DBRow serviceStatResult = new DBRow();

 for (int i=0; inService!=null&&i<inService.length;i++)
 {
 	inServiceAl.add(inService[i]);
 	serviceStatResult.add("service_"+inService[i],clientMgr.getClientsServiceStat(input_st_date,input_en_date,account,StringUtil.getInt(inService[i]) ));	
 }
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">


<script language="javascript">
<!--

function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.st.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.en.value = date;
}

function selectOneElement()
{
		for (i=0; i<document.filterForm.inService.length; i++)
		{
			if(document.filterForm.inService[i].checked)
			{
				return(true);
			}
		}
		return(false);	
}

function checkStat()
{
	if (!selectOneElement())
	{
		alert("至少选择一个统计项目");
		return(false);
	}
	else
	{
		return(true);
	}
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 客户追踪 »   客户服务统计</td>
  </tr>
</table>
<br>

<table width="98%" border="0" align="center" cellpadding="8" cellspacing="0" bgcolor="#E3F2E3">
  
  <tr>
    <td width="100%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="79%">
		<form name="filterForm" method="post" action="service_stat.html" onSubmit="return checkStat()">
		<input type="hidden" name="cmd" value="stat">


		开始 
              <input name="st" type="text" id="st" size="9" value="<%=input_st_date%>" readonly> 
              <script language="JavaScript" type="text/javascript" >
    <!--
    	stfooCalendar = new dynCalendar('stfooCalendar', 'stcalendarCallback', '../js/images/');
    //-->
    </script> 
              &nbsp; &nbsp; &nbsp;结束 
              <input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  readonly> 
              <script language="JavaScript" type="text/javascript">
    <!--
    	enfooCalendar = new dynCalendar('enfooCalendar', 'encalendarCallback', '../js/images/');
    //-->
	</script>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
	客服
		
		<select name="account">
						   <option value="" <%=account.equals("")?"selected":""%>>全部</option>
                <%
DBRow adminGroup[] = adminMgr.getAllAdmin(null);
for (int i=0; i<adminGroup.length; i++)
{
%>
                <option value="<%=adminGroup[i].getString("account")%>" <%=account.equals(adminGroup[i].getString("account"))?"selected":""%>> 
                <%=adminGroup[i].getString("account")%>                </option>
                <%
}
%>
              </select>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label>
<input name="Submit8" type="submit" class="button_long_refresh" value=" 统 计 ">
</label>
<br>



<table width="100%" border="0" cellspacing="0" cellpadding="0">

	       <tr >
        <td colspan="2" style="padding-top:10px;">包含 
		

		
			                <%
DBRow clientService[] = ClientServiceKey.getAllKeyVal();
for (int ii=0; ii<clientService.length; ii++)
{
%>

                            <input type="checkbox" name="inService" value="<%=clientService[ii].getString("val")%>" <%=inServiceAl.contains(clientService[ii].getString("val"))?"checked":""%>>  <%=clientService[ii].getString("name")%>
<%
}
%></td>
      </tr>
	  
	          <td colspan="2" style="display:none">排除含有
			  

			   
			                <%

for (int ii=0; ii<clientService.length; ii++)
{
%>
<input type="checkbox" name="outService" value="<%=clientService[ii].getString("val")%>">  <%=clientService[ii].getString("name")%>

<%
}
%></td>
      </tr>
</table>
          </form>		</td>
      </tr>

	  

	  

	  
	  

	  
	  
	  
    </table></td>
  </tr>
</table>
<%
if (cmd.equals("stat"))
{

int data_len = 150;
int service_len = 105;
int table_len = data_len + service_len*inService.length;

DBRow serviceStatDate[] = clientMgr.getClientsServiceStatDate(input_st_date,input_en_date,account);
%>
<br>
<br>
<table width="<%=table_len%>" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" >
  <tr>
    <th width="<%=data_len%>" style="vertical-align: center;text-align: center;"  class="left-title"><strong>日期</strong></th>
	<%
	for (int colNum=0; colNum<inService.length; colNum++)
	{
	%>
    <th  class="right-title"  width="<%=service_len%>" style="vertical-align: center;text-align: center;"><strong><%=ClientServiceKey.getNameByVal(StringUtil.getInt(inService[colNum]))%></strong></th>
	<%
	}
	%>
  </tr>
  
<%
int tmpFieldName;

for (int i=0; i<serviceStatDate.length; i++)
{

%> 
  
  <tr>
  	<td align="center"  height=50 style="font-size:13px;font-family: Verdana"  ><%=serviceStatDate[i].getString("post_date").substring(0,10)%></td>
 	<%
	for (int colNum=0; colNum<inService.length; colNum++)
	{
		//获得一行结果集合，是用日期作为 key，从dbrow获得
		tmpFieldName = ( (DBRow)serviceStatResult.get("service_"+inService[colNum],new Object()) ).get(serviceStatDate[i].getString("post_date").substring(0,10),0);
	%>
    <td align="center" bgcolor="#FFFFFF"    style="font-size:13px;font-family: Verdana" ><%=tmpFieldName%>
	
	<%
	if (inService[colNum].equals("88"))
	{
		out.println("/<font color=red> "+ MoneyUtil.round(clientMgr.getClientsServiceOrderMcGrossStat(serviceStatDate[i].getString("post_date"),account),2) +"</font>");
	}
	%>
	
	</td>
	<%
	}
	%>
  </tr>
<%
}
%>

</table><br>
<br>
<br>

<%
}
%>
</body>
</html>
