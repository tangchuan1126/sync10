<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.api.ClientServiceKey"%>
<%@ include file="../../include.jsp"%> 
<%
 	//long stt = System.currentTimeMillis();

 long cid = StringUtil.getLong(request,"cid");
 String search_key = StringUtil.getString(request,"search_key");
 String cmd = StringUtil.getString(request,"cmd");

 String st = StringUtil.getString(request,"st");
 String en = StringUtil.getString(request,"en");
 String account = StringUtil.getString(request,"account");

 long st_cid = StringUtil.getLong(request,"st_cid");
 long en_cid = StringUtil.getLong(request,"en_cid"); 

 int sort = StringUtil.getInt(request,"sort");


 ArrayList inServiceAl = new ArrayList();
 String inService[] = request.getParameterValues("inService");
 //这里很奇怪，get方式提交，参数会自动重复增加！！，所以这里先做参数过滤，把重复的参数去掉
 ArrayList inServiceFilter = new ArrayList();
 for (int i=0; inService!=null&&i<inService.length;i++)
 {
 	if (inServiceFilter.contains(inService[i])==false)
 	{
 		inServiceFilter.add(inService[i]);
 	}
 }
 inService = (String[])inServiceFilter.toArray(new String[0]);
 ///////////////////////////////

 for (int i=0; inService!=null&&i<inService.length;i++)
 {
 	inServiceAl.add(inService[i]);
 }


 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 if (account.equals("")&&!cmd.equals("filter")&&sort==0)
 {
 	account = adminLoggerBean.getAccount();
 }

 TDate tDate = new TDate();
 tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

 String input_st_date,input_en_date;
 if ( st.equals("") )
 {	
 	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
 }
 else
 {	
 	input_st_date = st;
 }


 if ( en.equals("") )
 {	
 	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
 }
 else
 {	
 	input_en_date = en;
 }


 PageCtrl pc = new PageCtrl();
 pc.setPageNo(StringUtil.getInt(request,"p"));
 pc.setPageSize(20);



 DBRow rows[]=null;

 if ( cmd.equals("search") )
 {
 	rows = clientMgr.getSearchClients(search_key,pc);
 }
 else if ( cmd.equals("search_range") )
 {
 	rows = clientMgr.getClientsByCidRange(st_cid,en_cid,pc);
 }
 else if ( cmd.equals("filter") )
 {
 	
 	rows = clientMgr.getSearchServiceByFilter(inService,st,en, account, pc);
 }
 else
 {
 	if (sort==1)
 	{
 		rows = clientMgr.getClientsSortBySumMcGross(input_st_date,input_en_date, account,pc);		
 	}
 	else if (sort==11)
 	{
 		rows = clientMgr.getClientsSortBySumMcGrossDown(input_st_date,input_en_date, account,pc);
 	}
 	else if (sort==2)
 	{
 		rows = clientMgr.getClientsSortByModDate(input_st_date,input_en_date, account,pc);
 	}
 	else if (sort==22)
 	{
 		rows = clientMgr.getClientsSortByModDateDown(input_st_date,input_en_date, account,pc);
 	}
 	else
 	{
 		rows = clientMgr.getSearchServiceByFilter(inService,input_st_date,input_en_date, account, pc);
 	}
 }
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

		
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript">
<!--
//创建客户
function addClient() 
{
	$.prompt(
	
	"<div id='title'>创建客户</div><br>邮件地址：<br><input name='proEmail' type='text' id='proEmail' style='width:300px;'><br>客户信息：<br><textarea name='proSummary' id='proSummary' style='width:300px;height:100px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if(f.proEmail=="")
						{
							alert("请填写邮件");
							return(false);
						}
						else if(f.proEmail.indexOf("@")==-1)
						{
							alert("邮件格式不正确");
							return(false);
						}
						else if (f.proSummary=="")
						{
							alert("请填写客户信息");
							return(false);
						}
	
						 return true;
					}
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_client_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service/addClients.action";
						document.add_client_form.email.value = f.proEmail;		
						document.add_client_form.summary.value = f.proSummary;			
						document.add_client_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


//增加咨询 
function addAsk() 
{

<%
StringBuffer askTypeSB = new StringBuffer("");
askTypeSB.append("<select name='proAskType' id='proAskType'>");
askTypeSB.append("<option value='0'>请选择</option>");

String ask_type[] = systemConfig.getStringConfigValue("ask_type").split(",");
for (int kk=0; kk<ask_type.length; kk++)
{
askTypeSB.append("<option value='"+ask_type[kk]+"'>");
askTypeSB.append(ask_type[kk]);
askTypeSB.append("</option>");
}
askTypeSB.append("</select>");
%>

              


	$.prompt(
	
	"<div id='title'>客户咨询</div><br />咨询类型：<%=askTypeSB.toString()%><br><br>邮件地址：<input name='proEmail' type='text' id='proEmail' style='width:300px;'>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.proAskType==0)
						{
							alert("请选择咨询类型");
							return(false);
						}
						else if(f.proEmail=="")
						{
							alert("请填写邮件");
							return(false);
						}
						else if(f.proEmail.indexOf("@")==-1)
						{
							alert("邮件格式不正确");
							return(false);
						}

	
						 return true;
					}
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_ask_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service/addAsk.action";
						document.add_ask_form.email.value = f.proEmail;		
						document.add_ask_form.ask_type.value = f.proAskType;			
						document.add_ask_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}






//增加服务
function addNote(cid,email) 
{

<%
StringBuffer askNoteSB = new StringBuffer("");
askNoteSB.append("<select name='proClientService' id='proClientService'>");
askNoteSB.append("<option value='0'>请选择</option>");

DBRow clientService[] = ClientServiceKey.getAllKeyVal();
for (int kk=0; kk<clientService.length; kk++)
{
	if ( clientService[kk].getString("val").equals("88")||clientService[kk].getString("val").equals("89")||clientService[kk].getString("val").equals("90")  )
	{
		continue;
	}
	
	askNoteSB.append("<option value='"+clientService[kk].getString("val")+"'>");
	askNoteSB.append(clientService[kk].getString("name"));
	askNoteSB.append("</option>");
}
askNoteSB.append("</select>");
%>


	$.prompt(
	
	"<div id='title'>为 "+email+" 服务</div><br />服务类型：<%=askNoteSB.toString()%><br>服务内容：<textarea name='proNote' id='proNote' style='width:350px;height:90px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if(f.proClientService==0)
						{
							alert("请选择服务类型");
							return(false);
						}
						else if (f.proNote=="")
						{
							alert("请填写服务内容");
							return(false);
						}
	
						 return true;
					}
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service/addClientTrace.action";
						document.listForm.cid.value = cid;		
						document.listForm.note.value = f.proNote;	
						document.listForm.clients_service.value = f.proClientService;			
						document.listForm.submit();
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}




//成单
function doneOrder(cid,email) 
{
	$.prompt(
	
	"<div id='title'>为 "+email+" 成单</div><br />订单号：<input name='proOid' type='text' id='proOid' style='width:300px;'>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if(f.proOid=="")
						{
							alert("请填写订单号");
							return(false);
						}
	
						 return true;
					}
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service/addClientTrace.action";
						document.listForm.cid.value = cid;		
						document.listForm.note.value = "单号："+f.proOid+" 金额：";	
						document.listForm.oid.value = f.proOid;
						document.listForm.clients_service.value = "<%=ClientServiceKey.Doneorder%>";
						document.listForm.submit();									
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}







function modSummary(cid,mnObj)
{
	noteObj = document.getElementById("summary_"+cid);

	if ( mnObj.value=="修改" )
	{
		noteObj.readOnly = false;
		noteObj.focus();
		noteObj.style.background="#CAFFDF";
		mnObj.value="提交修改";
	}
	else
	{
		document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service/modClient.action";
		document.listForm.cid.value = cid;
		document.listForm.summary.value = noteObj.value;
		document.listForm.backurl.value = "<%=StringUtil.getCurrentURL(request)%>";
		document.listForm.submit();
	}	
}


function allTrace(cid)
{
	tb_show('服务记录','ct_client_trace.html?cid='+cid+'&TB_iframe=true&height=550&width=900',false);
}


function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.st.value = date;

}

function encalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.filterForm.en.value = date;
}

function showSearchAddClientRowQQ()
{
	document.getElementById("add_client_search_row").style.display = "";
}

function hiddenSearchAddClientRowQQ()
{
	document.getElementById("add_client_search_row").style.display = "none";
}

function checkSearchAddClient(theForm)
{
	if(theForm.email.value=="")
	{
		alert("请填写邮件");
		return(false);
	}

	return(true);
}






function ajaxSearchClients() 
{ 
	if (document.getElementById("input_search_key").value=="")
	{
		alert("请填写关键字");
	}
	else
	{
		//先用ajax搜索一次，如果有搜索结果，再进行表单搜索
	
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service/getSearchClientResultCount.action'; 
		var pars = 'search_key=' + document.getElementById("input_search_key").value;
		
		$.ajax({                                              
			type: "POST",                                     
			url: url,                                       
			data: pars, 
			error:function(XMLHttpRequest, textStatus, errorThrown)
			{
				alert(XMLHttpRequest.getResponseHeader);
			}
			,
			success: function(originalRequest)
			{                 
			
					if (originalRequest*1>0)
					{
							document.listForm11.search_key.value = document.getElementById("input_search_key").value;
							document.listForm11.submit();
					}
					else
					{
							if (document.getElementById("input_search_key").value.indexOf("@")==-1)
							{
								$.prompt('没有搜索到相关结果',
								{
									overlayspeed:"fast",
									timeout:1500
								});
							}
							else
							{
								add_search_client();
							}
					}			
			} 
		}); 

	}
} 



function add_search_client()
{
	$.prompt("该客户不存在，是否创建？<br />		  <input type='text' id='prompt_input_email'  name='prompt_input_email' value='' size=50/>",
	{
	      submit: promptCheckEmail,
   		  loaded:promptOnload,
		  callback: addSearchClientCallback,
		  overlayspeed:"fast",
		  buttons: { 创建: "y", 取消: "n" }
	});
	
}

		
function addSearchClientCallback(v,m,f)
{
	if (v=="y")
	{
		document.add_client_search_form.email.value = f.prompt_input_email;
		document.add_client_search_form.submit();
	}
}
	
function promptOnload(v,m,f)
{
	document.getElementById("prompt_input_email").value = document.getElementById("input_search_key").value;
}

function promptCheckEmail(v,m,f)
{
	if (v=="y")
	{
      if(f.prompt_input_email == ""||f.prompt_input_email.indexOf("@")==-1)
	  {
           alert("请正确填写邮件地址");
		   
            return false;
      }
      return true;
	  }
}

function delClient(cid,email)
{
	if (confirm("确认删除 "+email+"？"))
	{
		document.del_client_form.cid.value = cid;
		document.del_client_form.submit();
	}
}


//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 客户追踪 »   客户列表 </td>
  </tr>
</table>
<br>
          <form name="add_client_form" method="post" >
		  <input type="hidden" name="clients_service" value="<%=ClientServiceKey.CreateClient%>">
		  <input type="hidden" name="note" value="创建客户：">
			<input name="email" type="hidden" id="email" >
			<input name="summary" type="hidden" id="summary">
          </form>
		  
<form name="add_client_search_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service/addClients.action" onSubmit="return checkSearchAddClient(this)">
		  		  <input type="hidden" name="clients_service" value="<%=ClientServiceKey.CreateClient%>">
		  <input type="hidden" name="note" value="搜索创建客户：">
		    <input type="hidden" name="email" id="email">
</form>

<form name="add_ask_form" method="post" >
<input type="hidden" name="clients_service" value="<%=ClientServiceKey.Ask%>">
<input name="email" type="hidden" id="email" >
<input name="ask_type" type="hidden" id="ask_type" >
</form>	



<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="80%" height="70" align="left" bgcolor="#E3F2E3" style="padding-left:10px;">
		
	<form name="filterForm" method="get" action="ct_clients.html">
		<input type="hidden" name="cmd" value="filter">


		开始 
              <input name="st" type="text" id="st" size="9" value="<%=input_st_date%>" readonly> 
              <script language="JavaScript" type="text/javascript" >
    <!--
    	stfooCalendar = new dynCalendar('stfooCalendar', 'stcalendarCallback', '../js/images/');
    //-->
    </script> 
              &nbsp; &nbsp; &nbsp;结束 
              <input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  readonly> 
              <script language="JavaScript" type="text/javascript">
    <!--
    	enfooCalendar = new dynCalendar('enfooCalendar', 'encalendarCallback', '../js/images/');
    //-->
	</script>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
	客服
		
		<select name="account">
						   <option value="" <%=account.equals("")?"selected":""%>>全部</option>
						   <option value="-1" <%=account.equals("-1")?"selected":""%>>未处理</option>
                <%
DBRow adminGroup[] = adminMgr.getAllAdmin(null);
for (int i=0; i<adminGroup.length; i++)
{
%>
                <option value="<%=adminGroup[i].getString("account")%>" <%=account.equals(adminGroup[i].getString("account"))?"selected":""%>> 
                <%=adminGroup[i].getString("account")%>
                </option>
                <%
}
%>
              </select>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label>
<input name="Submit8" type="submit" class="button_long_refresh" value=" 过 滤 ">
</label>
<br>



<table width="100%" border="0" cellspacing="0" cellpadding="0">

	       <tr >
        <td colspan="2" style="padding-top:10px;">包含 
		

		
			                <%
for (int ii=0; ii<clientService.length; ii++)
{
%>

                            <input type="checkbox" name="inService" value="<%=clientService[ii].getString("val")%>" <%=inServiceAl.contains(clientService[ii].getString("val"))?"checked":""%>>  <%=clientService[ii].getString("name")%>
<%
}
%>	  
</td>
      </tr>
	  
	        
</table>





	        </form>
		
</td>
        <td width="20%" align="center" bgcolor="#E3F2E3"  style="padding-right:10px;">


		
          <input name="showAddClientRowQQ" type="button" class="long-button-add" id="showAddClientRowQQ" onClick="addClient()" value="创建客户">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;        
  <input name="showAskRow" type="button" class="long-button-add"  id="showAskRow" onClick="addAsk()" value=" 咨询 ">


		</td>
      </tr>
  
    </table></td>
  </tr>
  <tr>
    <td width="4%" height="50" align="center" bgcolor="#CEDEFF">搜索</td>
    <td width="44%" bgcolor="#CEDEFF"> 
	<form name="listForm11" method="post" action="ct_clients.html">
	 <input type="hidden" name="cmd" value="search">
	  <input type="hidden" name="search_key">
	 </form>
	<label>
      <input type="text" name="input_search_key" id="input_search_key" value="<%=search_key%>" style="width:300px;">
      <input name="Submit" type="button" class="button_long_search" onClick="ajaxSearchClients()" value=" 搜 索 ">
    </label> 	</td>
    <td width="52%" bgcolor="#CEDEFF">
	<form name="listForm12" method="get" action="ct_clients.html">
	<input type="hidden" name="cmd" value="search_range">
	<label>顾客编号
        <input type="text" name="st_cid" style="width:100px;" value="<%=st_cid%>">
    - 
    <input type="text" name="en_cid" style="width:100px;" value="<%=en_cid%>">
    <input name="Submit5" type="submit" class="button_long_search" value=" 搜 索 ">
    </label>
    </form>	</td>
  </tr>
</table>
<br>


<%
if (rows.length>0)
{
%>


<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form method="post"  name="del_client_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/service/delClient.action">
<input type="hidden" name="cid">
</form>


  <form method="post"  name="listForm">
<input type="hidden" name="summary">
<input type="hidden" name="cid">
<input type="hidden" name="note">
<input type="hidden" name="clients_service">
<input type="hidden" name="oid">
<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")+"administrator/service/"%>ct_clients.html">


    <tr> 
        <th width="130"  style="vertical-align: center;text-align: center;"class="left-title">客户编号</th>
        <th width="147"  style="vertical-align: center;text-align: center;" class="right-title">客户信息</th>
        <th width="206"  style="vertical-align: center;text-align: center;" class="right-title">联络资料</th>
        <th width="200"  style="vertical-align: center;text-align: center;" class="right-title"> <a href="?sort=<%=sort==1?"11":"1"%>&st=<%=input_st_date%>&en=<%=input_en_date%>&account=<%=account%>" style="color:#000000">订单情况</a></th>
        <th width="154"  style="vertical-align: center;text-align: center;" class="right-title"><a href="?sort=<%=sort==2?"22":"2"%>&st=<%=input_st_date%>&en=<%=input_en_date%>&account=<%=account.equals("-1")?"":account%>" style="color:#000000">更新时间</a></th>
        <th width="421"  style="vertical-align: center;text-align: center;" class="right-title">服务记录</th>
        </tr>

    <%

for ( int i=0; i<rows.length; i++ )
{

%>
    <tr > 
      <td    height="100" align="center" valign="middle" style='word-break:break-all;' ><%=rows[i].getString("cid")%><br>
	  <%
if (adminLoggerBean.getAdgid()==10000l)
{
%>
      <br>
      <input name="Submit6" type="button" class="short-short-button-del" onClick="delClient(<%=rows[i].getString("cid")%>,'<%=rows[i].getString("email")%>')" value="删除">
<%
}
%>
	  </td>
      <td    align="center" valign="middle" style='word-break:break-all;' ><%=rows[i].getString("email")%></td>
      <td    align="left" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;'>
  
        <textarea name="summary_<%=rows[i].getString("cid")%>" id="summary_<%=rows[i].getString("cid")%>" style="width:200px;height:100px;" readOnly><%=rows[i].getString("summary")%></textarea>
        <br>
        <input name="Submit2" type="button" class="long-button-mod" onClick="modSummary(<%=rows[i].getString("cid")%>,this)" value="修改">
        <br>
      </td>
      <td    align="center" valign="middle" ><span style="word-break:break-all;">订单数：
	  <%
	  int orderCount = orderMgr.getOrderCountByClientId(rows[i].getString("email"));
	  out.println(orderCount);
	  
	  if (orderCount>0)
	  {
	  		out.println("&nbsp;<a href='../order/ct_order_auto_reflush.html?val="+rows[i].getString("email")+"&cmd=search' target='_blank' style='color:#0000FF'>[搜索]</a>");
	  }
	  %>
         
		 
		<br>
       订单金额：
      <%=MoneyUtil.round( orderMgr.getSumMcGrossByClientId(rows[i].getString("email")),2 )%></span></td>
      <td    align="center" valign="middle" ><%=rows[i].getString("mod_date")%></td>
      <td    align="left" valign="middle" style="word-wrap:break-word;padding-top:10px;padding-bottom:10px;line-height:20px;"  >
	  
	  
	  
	  <%
	  String serviceColor = "#0099FF";

	  DBRow client_trace[] = clientMgr.getClientTraceByCount(StringUtil.getLong(rows[i].getString("cid")),5);
	  for (int j=0; j<client_trace.length; j++)
	  {
	  		if (client_trace[j].get("clients_service",0)==88)
			{
				serviceColor = "#FF6600";
			}
			else
			{
				serviceColor = "#000000";
			}
	  %>
	  <span style="color:#ffffff;background:#00B588"><%=client_trace[j].getString("post_date")%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="font-weight:bold">[<%=client_trace[j].getString("account")%>]</span></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [<span style="color:<%=serviceColor%>;font-weight:bold"><%=ClientServiceKey.getNameByVal(client_trace[j].get("clients_service",0))%></span>] </span> <br> 
	  <%=StringUtil.ascii2Html( client_trace[j].getString("note") )%><br>
	  
	  <%
	  }
	  %>

	  <br>


	  <input name="Submit3" type="button" class="short-button" id="botton_<%=rows[i].getString("cid")%>" onClick="addNote(<%=rows[i].getString("cid")%>,'<%=rows[i].getString("email")%>')" value="  服务  ">
&nbsp;&nbsp;&nbsp;&nbsp;
<input name="Submit33" type="button" class="short-button" id="doneorder_button_<%=rows[i].getString("cid")%>" onClick="doneOrder(<%=rows[i].getString("cid")%>,'<%=rows[i].getString("email")%>')" value="  成单  ">
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <input name="Submit32" type="button" class="long-long-button" id="Submit3" onClick="allTrace(<%=rows[i].getString("cid")%>)" value="全部记录(<%=clientMgr.getClientTraceCountByCid(rows[i].get("cid",0l))%>)">


	  </td>
    </tr>
    <%
}
%>
  </form>
</table>

<br>
  <form name="dataForm">
          <input type="hidden" name="p">
		  <input type="hidden" name="cid" value="<%=cid%>">
		<input type="hidden" name="cmd" value="<%=cmd%>">
		<input type="hidden" name="st_cid" value="<%=st_cid%>">
		<input type="hidden" name="en_cid" value="<%=en_cid%>">
		<input type="hidden" name="sort" value="<%=sort%>">
		<input type="hidden" name="search_key" value="<%=search_key%>">
		<input type="hidden" name="st" value="<%=st%>">
		<input type="hidden" name="en" value="<%=en%>">
		<input type="hidden" name="account" value="<%=account%>">		
		
<%
for (int i=0; inService!=null&&i<inService.length;i++)
{
	inServiceAl.add(inService[i]);
%>
<input type="hidden" name="inService" value="<%=inService[i]%>">
<%	
}
%>
		
  </form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>

<%
}

%>



<%
//long ent = System.currentTimeMillis();
//System.out.println("CtClients Page time:"+(ent-stt));
%>

</body>
</html>
