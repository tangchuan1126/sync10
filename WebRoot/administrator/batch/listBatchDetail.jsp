<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.BatchTypeKey"%>
<%
	long batch_id = StringUtil.getLong(request,"batch_id");
	long ps_id = StringUtil.getLong(request,"ps_id");
	String pc_name = StringUtil.getString(request,"pc_name");
	long batch_detail_id = StringUtil.getLong(request,"batch_detail_id");
	String cmd = StringUtil.getString(request,"cmd");
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(10);

	DBRow[] batch_detail_rows = null;
	
	if(!cmd.equals("")&&batch_detail_id !=0)
		batch_detail_rows = batchMgrLL.getBatchchildDetails(batch_detail_id,null);
	else if(cmd.equals("") && batch_detail_id !=0)
		batch_detail_rows = batchMgrLL.getBatchDetailByBatchDetailId(batch_detail_id,null);
	else
		batch_detail_rows = batchMgrLL.getBatchDetailByBatchId(batch_id, ps_id, pc_name, pc);
	
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
	<script language="javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
	
	<script type="text/javascript" src="../js/select.js"></script>
	
	<link href="../comm.css" rel="stylesheet" type="text/css"/>
	
	<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<style type="text/css" media="all">
	@import "../js/thickbox/global.css";
	@import "../js/thickbox/thickbox.css";
	</style>
	<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
	<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
	<script src="../js/thickbox/global.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
	
	<script>
	function openBatchRelation(batch_id, batch_child_detail_id) {
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/batch/listBatchRelation.html?batch_id=" + batch_id + "&batch_child_detail_id="+batch_child_detail_id;
		batchRelationWindow = $.artDialog.open(url, {title: '批次关系',width:'800px',height:'500px', lock: true,opacity: 0.3});
	}

	function openBatchDetailUpdate(batch_id,ps_id,p_name) {
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/batch/batchDetailUpdate.html?batch_id="+batch_id+"&p_name="+p_name+"&ps_id="+ps_id;
		batchRelationWindow = $.artDialog.open(url, {title: '批次关系',width:'800px',height:'500px', lock: true,opacity: 0.3});
	}

	function openBatchDetail(batch_detail_id) {
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/batch/listBatchDetail.html?cmd=child&batch_detail_id="+batch_detail_id;
		batchChildDetailWindow = $.artDialog.open(url, {title: '批次明细',width:'800px',height:'500px', lock: true,opacity: 0.3});
	}
	</script>
  </head>
  
  <body>
  <div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#delivery_search">常用工具</a></li>
		</ul>
		<div id="delivery_search">
			 <table width="100%"  border="0" cellpadding="0" cellspacing="0">
			 <form action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/batch/listBatchDetail.html">
	            <tr>
	              <td nowrap="nowrap" width="30%" style="padding-top:3px;">
	              	<input type="hidden" name="batch_id" id="batch_id" value="<%=batch_id%>">
	              	产品编号
	              	<input type="text" name="pc_name" id="pc_name" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" value="<%=pc_name %>">
			    	<select id="ps_id" name="ps_id">
							<option value="0">目的仓库</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("id",0l)%>" <%=ps_id==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
							<%
								}
							%>
						</select>
			    	&nbsp;&nbsp;
			    	
			    		<label>
			  				<input name="Submit" type="submit" class="button_long_search" value="查询" onclick="search()"/>
			  			</label>
				   </td>
	            </tr>
	            </form>
	          </table>
		</div>
	</div>
	</div>
	<br>
	<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
    	<th width="4%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">批次明细编号</th>
        <th width="4%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">批次号</th>
        <th width="8%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">仓库</th>
        <th width="8%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">产品名称</th>
        <th width="10%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">入库数量</th>
        <th width="10%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">出库数量</th>
        <th width="10%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">成本</th>
        <th width="10%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">累加运费</th>
        <th width="10%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">本次运费</th>
		<th width="4%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">操作</th>
  </tr>
  <%
  	for(int i=0; i<batch_detail_rows.length; i++) {
  %>
  <tr height="30">
  	<td nowrap="nowrap" align="center"><a href="#" onclick="openBatchRelation(<%=batch_detail_rows[i].get("batch_id",0l)%>,<%=batch_detail_rows[i].get("batch_detail_id",0l)%>)"><%=batch_detail_rows[i].get("batch_detail_id",0l) %></a></td>
  	<td nowrap="nowrap" align="center">
  		<%=batch_detail_rows[i].get("batch_id",0l) %><br/>
  		<%
  			DBRow batch = batchMgrLL.getBatchByBatchId(batch_detail_rows[i].get("batch_id",0l));
  			BatchTypeKey batchTypeKey = new BatchTypeKey();
  		%>
  		<%
  			if(batch.get("business_type",0l) == batchTypeKey.DELIVERY) {
  				DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrder(batch.get("business_id",0l));
  		%>
  				<%=delivery_order.getString("delivery_order_number") %>
  		<%
  			}else {
  		%>
	  			<%=batch.get("business_id",0l) %>
  		<%
  			}
  		%><br/>
  		<%=batchTypeKey.getStatusById(batch.get("business_type",0)) %>
  	</td>
  	<td nowrap="nowrap" align="center"><%=batch_detail_rows[i].getString("title") %></td>
  	<td nowrap="nowrap" align="center"><%=batch_detail_rows[i].getString("p_name") %></td>
  	<td nowrap="nowrap" align="center"><%=batch_detail_rows[i].get("batch_count",0f) %></td>
  	<td nowrap="nowrap" align="center"><%=batch_detail_rows[i].get("batch_stockout_count",0f) %></td>
  	<td nowrap="nowrap" align="center"><%=batch_detail_rows[i].get("batch_cost",0f) %></td>
  	<td nowrap="nowrap" align="center"><%=batch_detail_rows[i].get("batch_freight_cost",0f) %></td>
  	<td nowrap="nowrap" align="center"><%=batch_detail_rows[i].get("batch_current_freight_cost",0f) %></td>
  	<td nowrap="nowrap" align="center">
  		<a href="#" onclick="openBatchDetailUpdate(<%=batch_detail_rows[i].get("batch_id",0l)%>, <%=batch_detail_rows[i].get("cid",0l) %>, '<%=batch_detail_rows[i].getString("p_name") %>')">修改</a>
  	</td>
  </tr>
  <%
  	}
  %>
</table>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onclick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>
<form name="dataForm" method="post">
        <input type="hidden" name="p"/>
        <input type="hidden" name="batch_id" value="<%=batch_id%>"/>
</form>
<script>
	$("#st").date_input();
	$("#en").date_input();
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});

	function refreshWindow() {
		window.location.reload();
	}
</script>
  </body>
</html>
