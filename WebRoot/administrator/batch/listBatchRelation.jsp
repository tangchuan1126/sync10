<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.BatchOperationTypeKey"%>
<%
	long batch_id = StringUtil.getLong(request,"batch_id");
	long batch_child_detail_id = StringUtil.getLong(request,"batch_child_detail_id");
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(10);

	DBRow[] batch_relation_rows = batchMgrLL.getBatchRelationByBatchId(batch_id, batch_child_detail_id, pc);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
	<script language="javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
	
	<script type="text/javascript" src="../js/select.js"></script>
	
	<link href="../comm.css" rel="stylesheet" type="text/css"/>
	
	<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<style type="text/css" media="all">
	@import "../js/thickbox/global.css";
	@import "../js/thickbox/thickbox.css";
	</style>
	<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
	<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
	<script src="../js/thickbox/global.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
	
	<script>
	function openBatchRelationUpdate(relation_id) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/batch/batchRelationUpdate.html?relation_id='+relation_id;
		batchRelationWindow = $.artDialog.open(url, {title: '批次关系',width:'800px',height:'500px', lock: true,opacity: 0.3});
	}

	function refreshWindow() {
		window.location.reload();
	}

	function openBatchDetail(batch_detail_id) {
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/batch/listBatchDetail.html?batch_detail_id="+batch_detail_id;
		batchDetailWindow = $.artDialog.open(url, {title: '批次明细',width:'800px',height:'500px', lock: true,opacity: 0.3});
	}
	</script>
  </head>
  
  <body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="4%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">批次号</th>
        <th width="8%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">产品名称</th>
        <th width="10%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">出入库数量</th>
        <th width="8%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">依赖批次产品名称</th>
        <th width="10%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">依赖批次出入库数量</th>
        <th width="10%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">转换类型</th>
        <th width="4%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">操作</th>
  </tr>
  <%
  	BatchOperationTypeKey batchOperationTypeKey = new BatchOperationTypeKey();
  	for(int i=0; i<batch_relation_rows.length; i++) {
  %>
  <tr height="30">
  	<td nowrap="nowrap" align="center">
  		<%=batch_relation_rows[i].get("batch_id",0l) %>
  	</td>
  	<td nowrap="nowrap" align="center"><%=batch_relation_rows[i].getString("child_name") %></td>
  	<td nowrap="nowrap" align="center"><%=batch_relation_rows[i].get("batch_child_stockout_count",0f) %></td>
  	<td nowrap="nowrap" align="center"><a href="#" onclick="openBatchDetail('<%=batch_relation_rows[i].get("batch_parent_detail_id",0l)%>')"><%=batch_relation_rows[i].getString("parent_name") %></a></td>
  	<td nowrap="nowrap" align="center"><%=batch_relation_rows[i].get("batch_parent_stockout_count",0f) %></td>
  	<td nowrap="nowrap" align="center"><%=batchOperationTypeKey.getBatchOperationTypeById(batch_relation_rows[i].get("batch_split_type",0)) %></td>
  	<td nowrap="nowrap" align="center"><a href="#" onclick="openBatchRelationUpdate('<%=batch_relation_rows[i].get("batch_relation_id",0l) %>')">修改</a></td>
  </tr>
  <%
  	}
  %>
</table>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onclick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>
<form name="dataForm" method="post">
        <input type="hidden" name="p"/>
        <input type="hidden" name="batch_id" value="<%=batch_id%>"/>
</form>
  </body>
</html>
