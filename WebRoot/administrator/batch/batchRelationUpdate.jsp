<%@page contentType="text/html;charset=UTF-8"%>

<%@ include file="../../include.jsp"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	long relation_id = StringUtil.getLong(request,"relation_id");
	//System.out.println(relation_id);
	DBRow row = batchMgrLL.getBatchRelationByRelationId(relation_id);
	DBRow child_product_row = productMgr.getDetailProductByPcid(row.get("batch_child_pcid",0l));
	DBRow parent_product_row = productMgr.getDetailProductByPcid(row.get("batch_parent_pcid",0l));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script>
	var updated = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated").equals("1")) {
	%>
			updated = true;
	<%
		}
	%>
	function init() {
		if(updated) {
			//parent.location.reload();
			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			closeWindow();
		}
	}
	
	$(document).ready(function(){
		init();
	});
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">

  <tr>
    <td align="center" valign="top">
<form method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batch/batchRelationUpdateAction.action" name="form1">
<input type="hidden" name="storage_batch_relation.batch_relation_id" value="<%=relation_id %>">
<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/batch/batchRelationUpdate.html?relation_id=<%=relation_id %>&updated=1"/>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">交货单货运信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
	        <tr height="29">
	          <td align="right" class="STYLE2">产品编号:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<%=row.get("batch_child_pcid",0l)%>
	          </td>
	          <td align="right" class="STYLE2">产品名称:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<%=child_product_row.getString("p_name")%>
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">出入库数量:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="storage_batch_relation.batch_child_stockout_count" id="batch_child_stockout_count" value="<%=row.get("batch_child_stockout_count",0f)%>"/>
	          </td>
	          <td align="right" class="STYLE2">&nbsp;</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	&nbsp;
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">依赖批次产品编号:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<%=row.get("batch_parent_pcid",0l)%>
	          </td>
	          <td align="right" class="STYLE2">依赖批次产品名称:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<%=parent_product_row.getString("p_name")%>
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">依赖批次出入库数量:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="storage_batch_relation.batch_parent_stockout_count" id="batch_parent_stockout_count" value="<%=row.get("batch_parent_stockout_count",0f)%>"/>
	          </td>
	          <td align="right" class="STYLE2">&nbsp;</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	&nbsp;
	          </td>
	        </tr>
		</table>
	</fieldset>	
	</td>
  </tr>
</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">

		<input name="insert" type="button" class="normal-green-long" onclick="save()" value="保存" >

		<input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
  </form>
<script type="text/javascript">
<!--
	function save() {
		form1.submit();
	}

	function closeWindow(){
		$.artDialog.close();
		//self.close();
	}
//-->
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 

</script>
</body>
</html>

