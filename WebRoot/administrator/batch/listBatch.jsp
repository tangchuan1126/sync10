<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="batchTypeKey" class="com.cwc.app.key.BatchTypeKey"/>
<%
	long batch_id = StringUtil.getLong(request,"batch_id");
	String search_key = StringUtil.getString(request,"search_key");
	int business_type = StringUtil.getInt(request,"business_type");
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);

	DBRow[] batch_rows = batchMgrLL.getBatchByBatchId(batch_id,search_key,business_type,pc);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
	<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
	<script language="javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
	
	<script type="text/javascript" src="../js/select.js"></script>
	
	<link href="../comm.css" rel="stylesheet" type="text/css"/>
	
	<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<style type="text/css" media="all">
	@import "../js/thickbox/global.css";
	@import "../js/thickbox/thickbox.css";
	</style>
	<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
	<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
	<script src="../js/thickbox/global.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
	<script>
	function openBatchDetail(batch_id) {
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/batch/listBatchDetailSum.html?batch_id="+batch_id;
		batchDetailWindow = $.artDialog.open(url, {title: '批次明细',width:'800px',height:'500px', lock: true,opacity: 0.3});
	}
	</script>
  </head>
  
  <body onload="onLoadInitZebraTable()">
    <div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#delivery_search">常用工具</a></li>
		</ul>
		<div id="delivery_search">
			 <table width="100%"  border="0" cellpadding="0" cellspacing="0">
			 <form action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/batch/listBatch.html">
	            <tr>
	              <td nowrap="nowrap" width="30%" style="padding-top:3px;">
			    	<font style="font-size:12px">批次号和业务单据：</font>
			    	<input name="search_key" type="text" class="input-line" id="search_key" onkeydown="if(event.keyCode==13) 1=1" value="" />
			    	&nbsp;&nbsp;
			    	<select name="business_type" id="business_type">
			    		<option value="0">所有业务类型</option>
			    		<option value="1" <%=business_type==1?"selected":"" %>><%=batchTypeKey.getStatusById(1) %></option>
			    		<option value="2" <%=business_type==2?"selected":"" %>><%=batchTypeKey.getStatusById(2) %></option>
			    		<option value="3" <%=business_type==3?"selected":"" %>><%=batchTypeKey.getStatusById(3) %></option>
			    		
			    		<option value="5" <%=business_type==5?"selected":"" %>><%=batchTypeKey.getStatusById(5) %></option>
			    		<option value="6" <%=business_type==6?"selected":"" %>><%=batchTypeKey.getStatusById(6) %></option>
			    		<option value="7" <%=business_type==7?"selected":"" %>><%=batchTypeKey.getStatusById(7) %></option>
			    		<option value="8" <%=business_type==8?"selected":"" %>><%=batchTypeKey.getStatusById(8) %></option>
			    		<option value="9" <%=business_type==9?"selected":"" %>><%=batchTypeKey.getStatusById(9) %></option>
			    		<option value="10" <%=business_type==10?"selected":"" %>><%=batchTypeKey.getStatusById(10) %></option>
			    		<option value="11" <%=business_type==11?"selected":"" %>><%=batchTypeKey.getStatusById(11) %></option>
			    		<option value="12" <%=business_type==12?"selected":"" %>><%=batchTypeKey.getStatusById(12) %></option>
			    		<option value="13" <%=business_type==13?"selected":"" %>><%=batchTypeKey.getStatusById(13) %></option>
			    	</select>
			    		<label>
			  				<input name="Submit" type="submit" class="button_long_search" value="查询" onclick="search()"/>
			  			</label>
				   </td>
	            </tr>
	            </form>
	          </table>
		</div>
	</div>
	</div>
	<br>
	<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="4%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">批次号</th>
        <th width="8%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">相关业务</th>
        <th width="17%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">业务单号</th>
        <th width="10%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">创建时间</th>
        <th width="7%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">创建人</th>
  </tr>
  <tr height="30">
  	<td nowrap="nowrap" align="center">0</td>
  	<td nowrap="nowrap" align="center"><a href="#" onclick="openBatchDetail(0)">所有批次</a></td>
  	<td nowrap="nowrap" align="center">&nbsp;</td>
  	<td nowrap="nowrap" align="center">&nbsp;</td>
  	<td nowrap="nowrap" align="center">&nbsp;</td>
  </tr>
  <%
  	for(int i=0; i<batch_rows.length; i++) {
  %>
  <tr height="30">
  	<td nowrap="nowrap" align="center"><%=batch_rows[i].get("batch_id",0l) %></td>
  	<td nowrap="nowrap" align="center"><a href="#" onclick="openBatchDetail(<%=batch_rows[i].get("batch_id",0l)%>)"><%=batchTypeKey.getStatusById(batch_rows[i].get("business_type",0)) %></a></td>
  	<td nowrap="nowrap" align="center">
  		<%
  			if(batch_rows[i].get("business_type",0l) == batchTypeKey.DELIVERY) {
  				DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrder(batch_rows[i].get("business_id",0l));
  		%>
  				<%=delivery_order.getString("delivery_order_number") %>
  		<%
  			}else {
  		%>
	  			<%=batch_rows[i].get("business_id",0l) %>
  		<%
  			}
  		%>
  	</td>
  	<td nowrap="nowrap" align="center"><%=DateUtil.FormatDatetime(batch_rows[i].getString("create_datetime"))%></td>
  	<td nowrap="nowrap" align="center"><%=batch_rows[i].getString("employe_name") %></td>
  </tr>
  <%
  	}
  %>
</table>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onclick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>
<form name="dataForm" method="post">
        <input type="hidden" name="p"/>
        <input type="hidden" name="batch_id" value="<%=batch_id%>"/>
</form>
<script>
	$("#st").date_input();
	$("#en").date_input();
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>
  </body>
</html>
