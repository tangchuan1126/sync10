<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@ include file="../../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Picture Online Show</title>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<link type="text/css" href="../js/picture_online_show/css/prettyPhoto.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/thickbox.css";
</style>
<script type="text/javascript" src="../js/picture_online_show/jquery.opacityrollover.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.smoothZoom.min.js"></script>
<%
	//根据传入的参数去查询对应的文件
	//只显示一张图片
	//显示多个图片()
	//如果是有多个图片的时候那么点击的那个。就应该让他选中
	String cmd = StringUtil.getString(request,"cmd");
	String table = StringUtil.getString(request,"table");
	String basepath = StringUtil.getString(request,"base_path");
	String currentName = StringUtil.getString(request,"current_name");
	long currentFileId = 0l ;
 	DBRow[] rows = null;
 	//apply_image表中查询出的图片的所有信息
 	List rowApply=null;
 	//判断当前点击的是哪个图片
 	String currentFileIdApply="";
	
	if(cmd.equals("show_only")){
		rows = new DBRow[1];
		String imgPath = StringUtil.getString(request,"image_path");
		String imgName = StringUtil.getString(request,"image_name");
		DBRow temp = new DBRow();
		temp.add("image_path",imgPath);
		temp.add("img_name",imgName);
		rows[0] = (temp);		
	}
	if(table.equals("file")){
		long fileWithId = StringUtil.getLong(request,"file_with_id");
		int fileWithType = StringUtil.getInt(request,"file_with_type");
		int fileWithClass = StringUtil.getInt(request,"file_with_class");
		//过滤掉不是图片的文件
		if(cmd.equals("multiFile")){
			rows = 	fileMgrZr.getFilesByFileWithIdAndFileWithType(fileWithId,fileWithType);
		}else if(cmd.equals("sortMultiFile"))
		{
			rows = fileMgrZr.getFilesByFileWithIdAndFileWithTypeAndFileWithClass(fileWithId,fileWithType,fileWithClass);
		}
		List<DBRow> rowList = new ArrayList<DBRow>();
		if(rows != null  && rows.length > 0 ){
			for(DBRow fileRow :  rows){
				
				if(StringUtil.isPictureFile(fileRow.getString("file_name"))){
					if(currentName.equals(fileRow.getString("file_name"))){
				 		 currentFileId = fileRow.get("file_id",0l);
				 	 }
					rowList.add(fileRow);
				}	 
				
			}
		}
		rows = rowList.toArray(new DBRow[rowList.size()]);
	}
	//添加临时图片的显示
 	//table == temp 
	//basepath == ''
	if(table.equals("temp")){
		String fileNames = StringUtil.getString(request,"fileNames");
 		if(!StringUtil.isNull(fileNames)){
 			String[] namesArray = fileNames.split(",");
 		    rows = new DBRow[namesArray.length];
 		    for(int index = 0 , count = namesArray.length ; index < count ; index++ ){
 		    	DBRow row = new DBRow();
 		    	row.add("file_name",namesArray[index]);
 		    	rows[index] = row;
 		    }
 		}
	}
	if(table.equals("product_file")){
		long fileWithId = StringUtil.getLong(request,"file_with_id");
		int fileWithType = StringUtil.getInt(request,"file_with_type");
		int product_file_type=StringUtil.getInt(request,"product_file_type");
 		String pcId = StringUtil.getString(request,"pc_Id");
		//PageCtrl pc = new PageCtrl();
		//pc.setPageNo(1);
		//pc.setPageSize(100);
		//过滤掉不是图片的文件
		rows = 	fileMgrZr.getProductFileByFileTypeAndWithIdAndPcId(fileWithId,fileWithType,product_file_type,pcId);
		List<DBRow> rowList = new ArrayList<DBRow>();
		if(rows != null  && rows.length > 0 ){
			for(DBRow fileRow :  rows){
				if(StringUtil.isPictureFile(fileRow.getString("file_name"))){
					if(currentName.equals(fileRow.getString("file_name"))){
				 		 currentFileId = fileRow.get("pf_id",0l);
				 	 }
					rowList.add(fileRow);
				}	 
			}
		}
		rows = rowList.toArray(new DBRow[rowList.size()]);
	}	
	//apply_images表中的图片在线显示
	if(table.equals("apply_images")){
		String associationId = StringUtil.getString(request,"association_id");
		String associationType=StringUtil.getString(request,"association_type");
		rowApply = applyMoneyMgrLL.getImageList(associationId,associationType);
		List  row=new ArrayList();
		if(rowApply!=null &&rowApply.size()>0){
			for(int i=0;i<rowApply.size();i++){
				HashMap imageMap = (HashMap)rowApply.get(i);
				if(StringUtil.isPictureFile(imageMap.get("path").toString())){
					if(currentName.equals(imageMap.get("path").toString())){
						currentFileIdApply = imageMap.get("id").toString();
				 	 }
				row.add(rowApply.get(i));}
			}
			rowApply=row;	
		}		
	}	
	
	
	//构造一个List<DBRow[]>,设置四个DBRow[] 的数据
	int countCols = 4 ;
	List<DBRow[]> arrayListRows = new ArrayList<DBRow[]>();
	List<DBRow> rowOne = new ArrayList<DBRow>();
	List<DBRow> rowTwo = new ArrayList<DBRow>();
	List<DBRow> rowThree = new ArrayList<DBRow>();
	List<DBRow> rowFour = new ArrayList<DBRow>();
	if(rows!=null){
	for(int index = 0  , count = rows.length ; index < count ; index+= 4){
		if(index < count){rowOne.add(rows[index]);}
		if((index + 1) < count){rowTwo.add(rows[index+1]);}
		if((index + 2) < count){rowThree.add(rows[index+2]);}
		if((index + 3) < count){rowFour.add(rows[index+3]);}
	}
	arrayListRows.add(rowOne.toArray(new DBRow[rowOne.size()]));
	arrayListRows.add(rowTwo.toArray(new DBRow[rowTwo.size()]));
	arrayListRows.add(rowThree.toArray(new DBRow[rowThree.size()]));
	arrayListRows.add(rowFour.toArray(new DBRow[rowFour.size()]));
	}
%>
<style type="text/css">
			*{margin:0px padding:0px;font-size:12px;}
			div.content{margin:0px auto;}
			body{background-image:url("../js/picture_online_show/images/bg.jpg");background-repeat:repeat;height:100%;}
			ul.ul_float{width:240px;float:left;margin:0px;padding:0px;margin-left:10px;}
			ul.ul_float li{list-style-type:none;display:block;padding:5px;background:white;box-shadow: 0 1px 3px #BBBBBB;margin-top:10px;}
			ul.ul_float li.active{box-shadow: 0 1px 3px #f60;}
			div.image_div{width:230px;margin:0px auto;}
			div.image_div img{width:230px}
			div.image_div:hover{cursor:pointer;}
		 
			a.abs{ color: #222222;overflow: hidden;padding: 0 0 6px;vertical-align: baseline;overflow: hidden;padding: 0 0 6px;display: block;font-size: 12px;line-height: 14px;text-decoration: none;}
			div.info{padding:12px 6px 5px;}
			.smooth_zoom_preloader {background-image: url("../js/picture_online_show/images/preloader.gif");}	
			.smooth_zoom_icons {background-image: url("../js/picture_online_show/images/icons.png");}
			span.size_info{color:#f60;} 
			.pp_description{line-height:25px;}
	  
</style>
<script type="text/javascript">
	function imageLoad(_this){
		var node = $(_this);
		var image = new Image();
		image.src = node.attr("src");
		image.onload = function(){
			var imageHeight =  image.height ;
			var imageWidth = image.width;
			 
			var changeHeight = imageHeight * (230 /imageWidth );
			node.attr("height",changeHeight+"px");
			var liNode = (node.parent().parent().parent());
			var spanSize = $(".size_info",liNode).html(imageWidth+"x"+imageHeight);
		};
	}	
	
</script>
</head>
 <body>
  	<div class="content">
  		<%
  		  if(arrayListRows.size() > 0 ){ 
  			for(DBRow[] arrayRows : arrayListRows){
  		%>
  			<ul class="ul_float">
  				 <%if(arrayRows != null && rows.length > 0){ %>
  				 <% for(DBRow rowTemp : arrayRows){ 
  				 	 String title = rowTemp.getString("file_name");
  				 	 String picturePath = basepath+"/"+ rowTemp.getString("file_name");
  				 	 String pName = rowTemp.getString("p_name");
	 			 	 String productFileType = rowTemp.getString("product_file_type");
	 			 	 String certificateFileType = rowTemp.getString("file_with_class");
	 			 	 boolean isActiveLi ;
	 			 	 //判断是file表中的图片还是product_file表中的图片
	 			 	 if(rowTemp.get("file_id",0l)!=0){
	 			 		isActiveLi = (currentFileId == rowTemp.get("file_id",0l) );
	 			 	 }else{
	 			 		 isActiveLi = (currentFileId == rowTemp.get("pf_id",0l) );
	 			 	 }
	 			 	 //让
 	 			 	 if(currentName.equalsIgnoreCase(title)){
	 			 		isActiveLi = true ;
	 			 	 }else{
	 			 		isActiveLi = false ;
	 			 	 }
   	 			 	String tempHtml="";
	 			 	Map<Integer,String> map = null ; 
	 				if(rowTemp.get("file_with_type",0l)== FileWithTypeKey.product_file || rowTemp.get("file_with_type",0l)== FileWithTypeKey.PURCHASE_PRODUCT_FILE || rowTemp.get("file_with_type",0l)== FileWithTypeKey.PRODUCT_SELF_FILE){
	 					
	 					map = StringUtil.getConfigMap(systemConfig.getStringConfigValue("transport_product_file"));
	 					tempHtml = map.get(Integer.parseInt(productFileType));
 	 					
	 				}else if(rowTemp.get("file_with_type",0l)== FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE || rowTemp.get("file_with_type",0l)== FileWithTypeKey.PRODUCT_TAG_FILE){
	 					map = StringUtil.getConfigMap(systemConfig.getStringConfigValue("transport_tag_types"));
	 					tempHtml = map.get(Integer.parseInt(productFileType));
	 					
	 				}else if(rowTemp.get("file_with_type",0l)== FileWithTypeKey.transport_certificate){
	 					map = StringUtil.getConfigMap(systemConfig.getStringConfigValue("transport_certificate"));
	 					tempHtml = map.get(Integer.parseInt(certificateFileType));
	 				}
 		
  				 %>
				<li class='<%=isActiveLi?"active":"" %>'>
				 <%
				    if(pName.equals("")&&pName.length()<=0){
				    	if(!tempHtml.equals("") && tempHtml.length()>0){
				   %>
							<div class="image_div">
								<a class="image_a" href='<%=picturePath %>' rel="prettyPhoto[gallery2]" title ='文件类型：<%=tempHtml %> , 文件名：<%=title %>'>
									<img src='<%=picturePath %>' onload="imageLoad(this)" width="230px"/>
								</a>
							</div>
							<div class="info">
								 <a class="abs" href="javascript:void(0)">文件类型：<%=tempHtml %> <br/> 文件名：<%=title %></a>
								 <span class="size_info"></span>
							</div>
					 <%}else{ %>
					 		<div class="image_div">
								<a class="image_a" href='<%=picturePath %>' rel="prettyPhoto[gallery2]" title ='<%=title %>'>
									<img src='<%=picturePath %>' onload="imageLoad(this)" width="230px"/>
								</a>
							</div>
							<div class="info">
								 <a class="abs" href="javascript:void(0)"><%=title %></a>
								 <span class="size_info"></span>
							</div>
					 <%	}
					 }else{
		 		       %>
		 		           <div class="image_div">
								<a class="image_a" href='<%=picturePath %>' rel="prettyPhoto[gallery2]" title ='关联商品：<%=pName %> , 文件类型：<%=tempHtml %> , 文件名：<%=title %>'>
									<img src='<%=picturePath %>' onload="imageLoad(this)" width="230px"/>
								</a>
							</div>
							<div class="info">
								 <a class="abs" href="javascript:void(0)">关联商品：<%=pName %> <br/> 文件类型：<%=tempHtml %><br/>文件名：<%=title %></a>
								 <span class="size_info"></span>
				            </div>
				            <%} %>
					 </li>
					<%}  
  				 }%>
  				</ul>
  				
  				<% 
  			}
  		} //申请资金页面的图片在线显示    apply_images 表
  		 else if(rowApply!=null&&rowApply.size()>0){
		 		for(int index = 0; index < rowApply.size() ; index++ ){
		 			HashMap imageMap = (HashMap)rowApply.get(index);
		 			boolean isActiveLi = (currentFileIdApply .equals(imageMap.get("id").toString() ));
	 			 	String picturePath = basepath + "/" + imageMap.get("path");
		 		%>
		 		 <ul class="ul_float">
		 	  		<li class='<%=isActiveLi?"active":"" %>'>
		 		            <div class="image_div">
								<a class="image_a" href='<%=picturePath %>' rel="prettyPhoto[gallery2]" title ='<%=imageMap.get("path") %>'>
									<img src='<%=picturePath %>' onload="imageLoad(this)" width="230px"/>
								</a>
							</div>
							<div class="info">
								 <a class="abs" href="javascript:void(0)"><%=imageMap.get("path") %></a>
								 <span class="size_info"></span>
							</div>
			  		</li>
			    </ul>
		 		<%} %>
		   <%} else{
		 		String picturePath = basepath + "/" + currentName;
		 	%>
		 	<ul class="ul_float">
		 	<li>
		 		 <div class="image_div">
								<a class="image_a" href='<%=picturePath %>' rel="prettyPhoto[gallery2]" title ='<%=currentName %>'>
									<img src='<%=picturePath %>' onload="imageLoad(this)" width="230px"/>
								</a>
							</div>
							<div class="info">
								 <a class="abs" href="javascript:void(0)"><%=currentName %></a>
								 <span class="size_info"></span>
							</div>
			  </li>
			 </ul>
		 	<%} %>
		<div style="clear:both;"></div>
  	</div>
  	<script type="text/javascript">
  	jQuery(function($){
		var onMouseOutOpacity = 0.67;
		$('.image_div').opacityrollover({
			mouseOutOpacity:   1.0,
			mouseOverOpacity:  onMouseOutOpacity,
			fadeSpeed:         'fast' 
		});
 
	var canvasWidth =  806;
	var canvasHeight = 447;
 	if($(window).width()* 1 < 1000){
 	   canvasWidth = 600;
 	  canvasHeight = 347;
 	}
	$("a.image_a").prettyPhoto({
		default_width: canvasWidth,
		default_height: canvasHeight,	
		 slideshow:false, /* false OR interval time in ms */
		autoplay_slideshow: false, /* true/false */
		opacity: 0.50, /* opacity of background black */
		theme: 'facebook', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
		modal: true, /* If set to true, only the close button will close the window */	
		overlay_gallery: false,
		changepicturecallback: setZoom,
		callback: closeZoom,
		social_tools: false,
		image_markup: '<div style="width:'+canvasWidth+'px; height:'+canvasHeight+'px;"><div id="fullResImage"  ><img id="rotateImg"  src="{path}" /></div></div>',
		fixed_size: true,
		
		/******************************************
		Enable Responsive settings below if needed.
		Max width and height values are optional.
		******************************************/
		responsive: false,
		responsive_maintain_ratio: true,
		max_WIDTH: '',
		max_HEIGHT: ''
	});		
	//让点取的选中
	$("html,body").animate({scrollTop: $("li.active").offset().top}, 1000);
	});
	function setZoom (){
  		var imgObj = new Image();
  	    imgObj.src = $("#rotateImg").attr('src');
  	    var sizeImg = imgObj.width>imgObj.height?imgObj.width:imgObj.height;
  	    $('#fullResImage').css({
  	    "width":sizeImg+"px",
  	    "height":sizeImg+"px",
  	    });
  	    if(imgObj.width<imgObj.height){				//高图需要进行横向居中。正好相等则什么也不做 
  	    	 $("#rotateImg").css({
  				"position" : "absolute",
  				"left" : "25%"
  			});
  	    }else if(imgObj.width>imgObj.height){		//宽图需要进行纵向居中
  	    	var topsize = Math.floor((1-imgObj.height/imgObj.width)/2*100);//计算应距离上边距百分之多少，并给过宽的图片加行top属性
  		   $("#rotateImg").css({
  				"position" : "absolute",
  				"top" :""+topsize+"%",
  			});
  		}
		$('#fullResImage').smoothZoom('destroy').smoothZoom();
	}

	function closeZoom (){
		$('#fullResImage').smoothZoom('destroy');
	}
	//图片旋转
	var indexZ = 0;
	function turnLeft()
	{
		if (indexZ == 360 | indexZ == -360)
			indexZ = 0;
		$("#rotateImg").css({
			"-moz-transform" : "rotate(" + (indexZ -= 90) + "deg)",
			"-webkit-transform" : "rotate(" + (indexZ) + "deg)"
		});

	} 
	function turnRight()
	{
 		if (indexZ == 360 | indexZ == -360)
			indexZ = 0;
		$("#rotateImg").css({
			"-moz-transform" : "rotate(" + (indexZ += 90) + "deg)"
		});
		$("#rotateImg").css({
			"-webkit-transform" : "rotate(" + (indexZ) + "deg)"
		});
	}
  		
  	</script>
 </bod