<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML>
 
<html lang="en">
<head>
 <%
  	String jqueryFileUpAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/file_up/JqueryFileUpSubmitFormAction.action";
  	String getJqueryFileAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/ReadJqueryFileAction.action";
 	String fileName = StringUtil.getString(request,"file_names");
 	String target = StringUtil.getString(request,"target");
 	String reg = StringUtil.getString(request,"reg").length() < 1 ?"all":StringUtil.getString(request,"reg");
 	String limitSize  = StringUtil.getString(request,"limitSize").length() < 1 ? "3" : StringUtil.getString(request,"limitSize") ;
  	String limitNum = StringUtil.getString(request,"limitNum");
  	limitNum = limitNum.trim().length() < 1 ? "100":limitNum;
  	String questionId=StringUtil.getString(request,"questionId");
  %>
  <meta charset="utf-8">
<title>jQuery File Upload Demo</title>
<meta name="description" content="File Upload widget with multiple file selection, drag&amp;drop support, progress bar and preview images for jQuery. Supports cross-domain, chunked and resumable file uploads. Works with any server-side platform (Google App Engine, PHP, Python, Ruby on Rails, Java, etc.) that supports standard HTML form file uploads.">
<meta name="viewport" content="width=device-width">
 <link rel="stylesheet" href="../js/file-upload/css/bootstrap.min.css">
  
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 <link rel="stylesheet" href="../js/file-upload/css/bootstrap-responsive.min.css">
  <link rel="stylesheet" href="../js/file-upload/css/bootstrap-image-gallery.min.css">
 <link rel="stylesheet" href="../js/file-upload/css/jquery.fileupload-ui.css">
 <!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/file-upload/js/locale.js"></script>
<style type="text/css">
	div.note{
	background-color: #F5F5F5;
    border: 1px solid #E3E3E3;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05) inset;
    min-height: 20px;
    padding:5px;
  
    }
    div.note ul {list-style-type:none;}
 	.table{width:99%;}
</style>
 <script type="text/javascript">
 
 	function showFiles(_isSelf){
		var arrayATag = $("td.name a");
		var file_names =  "";
		if(arrayATag && arrayATag.length > 0 ){
			for(var index = 0 , count = arrayATag.length ; index < count ; index++ ){
				var _this = $(arrayATag[index]);
		 
				file_names += ","+_this.html();
			}
		}
	
		file_names = file_names.length > 1 ? file_names.substr(1):"";
		$.artDialog.opener.uploadFileCallBack  && $.artDialog.opener.uploadFileCallBack(file_names,'<%= target%>',<%= questionId%>);	
		if(_isSelf){
			$.artDialog && $.artDialog.close();
		}
 	}
 	 
 	var limitSize = '<%= limitSize%>' * 1
 	var reg = '<%= reg%>';
 	var limitNum = '<%= limitNum%>' * 1;
 	jQuery(function($){
 	   $('#fileupload').fileupload({
 	      	 formAcceptCharset:'utf-8',
 	      	 acceptFileTypes:regObject[reg]["reg"],
 	      	 maxFileSize:limitSize * 1000000,
 	      	 maxNumberOfFiles:limitNum 
	 	 });	//初始化
	 	 $("#file_up_type").html(regObject[reg]["alertText"]);
 	   var fileName = $.trim('<%= fileName%>');
 	   if(fileName.length > 0 ){
			// 要回显出已经上传的文件
 	       $.ajax({
				url:'<%= getJqueryFileAction%>' + "?file_names="+fileName,
				dataType:'json',
				success:function(data){
					if(data && data.length > 0 ){
					    $("#fileupload").fileupload('option', 'done')
			                    .call($("#fileupload"), null, {result: data});
					}
				}
	 	   })
	   }
 	})
 </script>
  </head>
 
<body>
 
<div class="container" style="width:97%;padding-left:10px;">

    <form id="fileupload" action="<%=jqueryFileUpAction %>" method="POST" enctype="multipart/form-data">
         <div class="row fileupload-buttonbar">
            <div class="span7 alert alert-error" style="margin:1px;padding:4px;width:98%;">
                 <span class="btn btn-success fileinput-button">
                    <i class="icon-plus icon-white"></i>
                  	Add
                     <input type="file" name="files[]" multiple />
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="icon-upload icon-white"></i>
                   		  Upload
                 </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="icon-ban-circle icon-white"></i>
                    	Cancel
                 </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="icon-trash icon-white"></i>
                		    Delete
                 </button>
                  <button type="button" class="btn btn-info delete" onclick="showFiles('true');"  title="Confirm">
                    <i class="icon-ok icon-white"></i>
                    Confirm
                 </button>
                <input type="checkbox" class="toggle">
            </div>
            <div class="note" style="clear:both;">
            	<ul >
            		<li>Limit on Single File Size<span style="font-weight:bold;"><%=limitSize %>M</span></li>
            		<li>Upload File Type<span id="file_up_type" style="font-weight:bold;"></span></li>
            	</ul>
            </div>
              
        </div>
         <div class="fileupload-loading"></div>
        <br>
         <table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
    </form>
 
</div>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
            <td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
            <td>
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
            </td>
            <td class="start">{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary">
                    <i class="icon-upload icon-white"></i>
                    <span>{%=locale.fileupload.start%}</span>
                </button>
            {% } %}</td>
        {% } else { %}
            <td colspan="2"></td>
        {% } %}
        <td class="cancel">{% if (!i) { %}
            <button class="btn btn-warning">
                <i class="icon-ban-circle icon-white"></i>
                <span>{%=locale.fileupload.cancel%}</span>
            </button>
        {% } %}</td>
    </tr>
{% } %}
</script>
<!-- 上传过后回显出来的数据 -->
 <script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
            <td></td>
            <td class="name"><span>{%=file.name%}</span></td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
        {% } else { %}
            <td class="preview">{% if (file.thumbnail_url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
            <td class="name">
                <a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
            </td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td colspan="2"></td>
        {% } %}
        <td class="delete">
            <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}">
                <i class="icon-trash icon-white"></i>
                <span>{%=locale.fileupload.destroy%}</span>
            </button>
 <input type="checkbox" name="delete" value="1">
        </td>
    </tr>
{% } %}
</script>
  


 <script src="../js/file-upload/js/vendor/jquery.ui.widget.js"></script>
 <script src="../js/file-upload/js/tmpl.min.js"></script>
 <script src="../js/file-upload/js/load-image.min.js"></script>
 <script src="../js/file-upload/js/canvas-to-blob.min.js"></script>
 <script src="../js/file-upload/js/bootstrap.min.js"></script>
<script src="../js/file-upload/js/bootstrap-image-gallery.min.js"></script>
 <script src="../js/file-upload/js/jquery.iframe-transport.js"></script>
 <script src="../js/file-upload/js/jquery.fileupload.js"></script>
 <script src="../js/file-upload/js/jquery.fileupload-fp.js"></script>
 <script src="../js/file-upload/js/jquery.fileupload-ui.js"></script>

 
 </body> 
</html>
 