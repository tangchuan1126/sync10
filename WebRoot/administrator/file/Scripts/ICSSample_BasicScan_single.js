﻿var gImageCapture;
_imageCaptureParam.width = 1 ;
_imageCaptureParam.height = 1;
 
(function() {
	gImageCapture = new Dynamsoft.ImageCapture(_imageCaptureParam);
})();
var seed;
var isHasImageSpeed ;
function onPageLoad() {
     seed = setInterval(initControl, 500);
 }

function initControl() {
     var DWObject = gImageCapture.getInstance();
     if (DWObject) {
    	   if (DWObject.ErrorCode == 0) {
               clearInterval(seed);
               DWObject.BrokerProcessType = 1;

               var vDWTSource = document.getElementById("source");
               if (vDWTSource) {
                   vDWTSource.options.length = 0;
                   // fill in the source items.
                   for (var i = 0; i < DWObject.SourceCount; i++) {
                       vDWTSource.options.add(new Option(DWObject.GetSourceNameItems(i), i));
                   }
                   if (DWObject.SourceCount > 0) {
                       source_onchange();
                   }
               }
               var vResolution = document.getElementById("Resolution");
               if (vResolution) {
                   vResolution.options.length = 0;
                   vResolution.options.add(new Option("100", 100));
                   vResolution.options.add(new Option("150", 150));
                   vResolution.options.add(new Option("200", 200));
                   vResolution.options.add(new Option("300", 300));
                }
               vResolution.selectedIndex = 3 ;
               notifyIsOpenCapture();
           }
     }
 }
function notifyIsOpenCapture(){
	//如果是只有一个Twain的那么就是直接弹开Twain的
	var source = $("#source");
	var options = $("#source option");
	var items = "" ;
	var hasTwainCount = 0 ;
	var hasWainIndex = [] ;
	if(options.length == 1){	//如果是只有一个，那么直接弹开
		acquireImage();
		return ;
	}
}
function source_onchange() {
    var DWObject = gImageCapture.getInstance();
    if (DWObject) {
        var vDWTSource = document.getElementById("source");
        if (vDWTSource) {

            if (vDWTSource)
                DWObject.SelectSourceByIndex(vDWTSource.selectedIndex);
            else
                DWObject.SelectSource();
        }

        DWObject.CloseSource();
    }
}
function isHasPhoto(){
    var DWObject = gImageCapture.getInstance();

 	var flag = DWObject &&  DWObject.HowManyImagesInBuffer > 0;
 	if(flag){
 		clearInterval(isHasImageSpeed);
  		uploadImages();
 	}
}
function acquireImage(isSelectSouce) {

    var DWObject = gImageCapture.getInstance();
    if (DWObject) {
        if (DWObject.SourceCount > 0) {

            DWObject.IfShowUI = true ;
            DWObject.CloseSource();
            DWObject.OpenSource();

        	if(isSelectSouce){
                DWObject.SelectSource();
        	}else{
	        	var vDWTSource = document.getElementById("source");
	            if (vDWTSource) {
	                if (vDWTSource)
	                    DWObject.SelectSourceByIndex(vDWTSource.selectedIndex);
	                else
	                    DWObject.SelectSource();
	            }
        	}
        	var vResolution = document.getElementById("Resolution");
        	DWObject.Resolution = vResolution.value;

 			DWObject.SetViewMode(3,3);
 			DWObject.IfDisableSourceAfterAcquire = true;
		    DWObject.IfFeederEnabled  = false ;
			DWObject.IfDuplexEnabled  = true ;
			DWObject.MaxImagesInBuffer = 1;
			DWObject.AcquireImage();
			//同时开启检查是否有图片的东西
        	isHasImageSpeed = setInterval(isHasPhoto, 500);
         }
        else
            alert("No webcams or TWAIN compatible drivers detected.");
    }
}

function Dynamsoft_OnTopImageInTheViewChanged(index) {
  var DWObject = gImageCapture.getInstance();
  if (DWObject) {
      DWObject.CurrentImageIndexInBuffer = index;
  }
}
 
