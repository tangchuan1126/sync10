<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
    <title>OnLine Scan</title>
    
     <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
    <script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
 	
 	 <link rel="stylesheet" href="Styles/simple_scan.css">
    <%
    	String target = StringUtil.getString(request,"target");
    %>
</head>
<body onload="onPageLoad();">
		 	 		<div class="divTableStyle" style="margin-top:10px;">
 						<label for="source" style="font-size:12px;font-family:arial,helvetica,sans-serif;line-height:20px;">
							<b>Select Source:</b>
							<select id="source"  onchange="source_onchange()" style="position:relative;width: 217px;margin-left:29px;" size="1"></select>
						</label>
						<div style="margin-top:5px;">
				  		 	<label for="Resolution" style="font-size:12px;font-family:arial,helvetica,sans-serif;line-height:20px;">
								<b>Resolution:</b>
								<select id="Resolution" size="1">
								</select>
								DPI
							</label>
						</div>
 				  		  <input class="DWTScanButton btn" type="button" style="margin-top:10px;" value="Single Scan " onclick="acquireImage();" />
 				    	</div>
 	  					<div id="dwtcontrolContainer" class="DWTContainer"></div>
     					 <script src="Scripts/dynamsoft.imagecapturesuite.initiate.js"></script>
     					 <script src="Scripts/product_info.js"></script>
      					 <script src="Scripts/ICSSample_BasicScan_single.js"></script>
      					 <script type="text/javascript">
      					function uploadImages(){
      					    		var DWObject = gImageCapture.getInstance();
      					 		
      								//这里执行上传的文件.
      								//和jquery file up 一样都是走同样接口。和回调方法
      								//这里的上传是先上传到temp那个文件夹下。然后后面的操作执行回调函数的(需要返回file_names ,target)
      								 
      								var CurrentPathName = unescape(location.pathname);	// get current PathName in plain ASCII	
      								var CurrentPath = CurrentPathName.substring(0, CurrentPathName.lastIndexOf("/") + 1);
      								var  strActionPage = '<%=ConfigBean.getStringValue("systenFolder") %>'+"action/file/FileOnLineScannerUploadAction.action";
      								var count =  DWObject.HowManyImagesInBuffer;
      								var successUploadCount = 0 ;
      								var filenames = [];
      								for(var index = 0 ; index < count ; index++ ){
      								    var Digital = new Date();
      								     
      										var CurrentTime = Digital.getTime();
      										strFileName = CurrentTime+".jpg";
      										var strHostIP = location.hostname;	
      										DWObject.HTTPPort = $.trim(location.port==""?80:location.port);	
      								 		DWObject.HTTPUploadThroughPost($.trim(strHostIP),index,strActionPage,$.trim(strFileName));
      								 		if (DWObject.ErrorCode != 0) {
      											alert(DWObject.ErrorString);
      											if (DWObject.ErrorString == "HTTP process error."){
      											}
      										}
      										else{
      										    successUploadCount++;
      										    filenames.push(strFileName);
      										}	  
      								}
      								if(successUploadCount * 1 > 0){
      									//调用回调函数
      									var file_names = filenames.join(",");
      								    $.artDialog.opener.uploadFileCallBack  && $.artDialog.opener.uploadFileCallBack(file_names,'<%= target%>');
      								    $.artDialog && $.artDialog.close();
      								}
      							    
      							}
      					 </script>
   
</body>
</html>
