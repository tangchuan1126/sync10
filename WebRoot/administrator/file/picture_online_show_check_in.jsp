<!DOCTYPE html>
<%@page import="com.cwc.util.StringUtil"%>
<%@page import="com.cwc.app.util.ConfigBean"%>
<html>
<head>

<%
	long dlo_id = StringUtil.getLong(request, "file_with_id");
%>
<title>Picture Online Show</title>
<link href="Styles/photo.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<style type="text/css">
		
			.smooth_zoom_preloader {background-image: url("../js/picture_online_show/images/preloader.gif");}	
			.smooth_zoom_icons {background-image: url("../js/picture_online_show/images/icons.png");}
			li a{
				position: relative;
			}
		.count{
			    background: none repeat scroll 0 0 #2894ff;
			    border-radius: 125px;
			    color: #d2e9ff;
			    font-weight: 700;
			    height: 20px;
			    line-height: 21px;
			    position: absolute;
			    right: 28px;
			    top: 7px;
			    width: 20px;
		}
	  
</style>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.smoothZoom.min.js"></script>

<script type="text/javascript" src="../js/picture_online_show/jquery.prettyPhoto.js"></script>
<link type="text/css" href="../js/picture_online_show/css/prettyPhoto.css" rel="stylesheet" />


<script type="text/javascript">
	$(function(){
		var base_path = '<%=ConfigBean.getStringValue("systenFolder")%>';
		//遮罩 
		$.blockUI.defaults = {
				 css: { 
				  padding:        '8px',
				  margin:         0,
				  width:          '170px', 
				  top:            '45%', 
				  left:           '40%', 
				  textAlign:      'center', 
				  color:          '#000', 
				  border:         '3px solid #999999',
				  backgroundColor:'#ffffff',
				  '-webkit-border-radius': '10px',
				  '-moz-border-radius':    '10px',
				  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
				  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
				 },
				 //设置遮罩层的样式
				 overlayCSS:  { 
				  backgroundColor:'#000', 
				  opacity:        '0.6' 
				 },
				 
				 baseZ: 99999, 
				 centerX: true,
				 centerY: true, 
				 fadeOut:  1000,
				 showOverlay: true
				};
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/getPicturesByMainId.action';
		$.ajax({
	  		type:'post',
	  		url:uri,
	  		dataType:'json',
	  		data:{"entry_id":<%=dlo_id%>},
	  		beforeSend:function(request){
			      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
		 	success:function(data){
		 		$.unblockUI();       //遮罩关闭
	 			var folder = "";
	 			var pictures = "";
		 		if(data!=null&&data.length>0){
		 			for(var i=0;i<data.length;i++){
		 			//	console.log("the "+i+ " folder"+data[i].pictures.length);
		 		
		 				//home 文件夹
		 				folder += "<li>";
		 				folder += "<a href='javascript:void(0)'><img alt='"+data[i].dir_name+"' value="+data[i].file_with_class+" src='images/folder.PNG'>";
		 				folder += "<span class='text'>"+data[i].dir_name+"</span>";
		 				folder += "<div class='count'>"+data[i].dir_photo_count+"</div></a>";
		 				folder += "</li>";
		 				//图片
		 				pictures += "<div class='photo_item'>";
		 				pictures += "<span class='ttl'>"+data[i].dir_name+"</span>";
		 				
		 				
		 				pictures += "<ul>";
		 				
		 				if(data[i].photos.length>0){
		 					for(var j=0;j<data[i].photos.length;j++){
				 				pictures += "<li class='photo_container'>";
				 				pictures += "<div  class='image_div'>";
				 				pictures += "<a class='image' href='<%=ConfigBean.getStringValue("systenFolder")%>"+data[i].photos[j].URI+"'  rel='prettyPhoto[gallery"+i+"]'  href='javascript:void(0)' title='"+data[i].photos[j].FILE_NAME+"'>";
				 				pictures += "<img id='loadImage' src='<%=ConfigBean.getStringValue("systenFolder")%>"+data[i].photos[j].URI+"' >"; 
				 				pictures += "</a>";
				 				pictures += "</div>";
				 				
				 				pictures += "<div  class='image_div'>";
				 				pictures += "<a href='javascript:void(0)'>"+data[i].photos[j].FILE_NAME+"</a>";
				 				pictures += "</div>";
				 				pictures += "</li>";
		 				
		 					}
		 				}
		 				pictures += "</ul>";
		 				pictures += "</div>";
			 			
		 			}
		 			
					 		
				}
	 			$("#picture_folder").html(folder);
	 			
	 			$("#photo_item").html(pictures); 
	 			
	 			
	 			
	 			
	 			$(".module-toolbar .view").click(function(){
	 				$(".folder_show_toolbar").html("All File");
	 				$("#show_photo .photo_folder").show().siblings().hide().end().find("ul").show().nextAll().remove();
	 			});
	 			$(".module-toolbar .list").click(function(){
	 				$(".folder_show_toolbar").html("All File");
	 				$("#photo_item").show().siblings().hide();
	 			});
	 			$(".photo_folder ul li a").click(function(){
	 				$(".folder_show_toolbar").html("<a href='javascript:void(0)'>Back</a> |  <a href='javascript:void(0)'>All File</a> > "+$(this).find(".text").text());
	 				var index = $(".photo_folder ul li a").index(this);
	 				$(".photo_folder ul").hide().parent(".photo_folder").append($("#photo_item").children().eq(index).children("ul").clone(true));
	 				$("#pic_count").val($("#photo_item").children().eq(index).children("ul li").length);
	 			});
	 			
	 			$(".folder_show_toolbar a").live('click',function(){
	 				$(".folder_show_toolbar").html("All File");
	 			
	 				$("#picture_folder").show().nextAll().remove();
	 				$("#pic_count").val("");
	 			});
	 			
	 			var canvasWidth =  806;
	 			var canvasHeight = 447;
	 			
	 			//图片查看
	 			if($(window).width()* 1 < 1000){
	 		 	   	canvasWidth = 600;
	 		 		canvasHeight = 347;
	 		 	}
	 			$(".image").prettyPhoto({
	 				default_width: canvasWidth,
	 				default_height: canvasHeight,	
	 				slideshow:false, /* false OR interval time in ms */
	 				autoplay_slideshow: false, /* true/false */
	 				opacity: 0.50, /* opacity of background black */
	 				theme: 'facebook', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
	 				modal: false, /* If set to true, only the close button will close the window */	
	 				allow_resize: false, /* Resize the photos bigger than viewport. true/false */
	 				overlay_gallery: false,
	 				changepicturecallback: setZoom,
	 				callback: closeZoom,
	 				social_tools: false,
	 				image_markup: '<div style="width:'+canvasWidth+'px; height:'+canvasHeight+'px;"><div id="fullResImage"  ><img id="rotateImg"  src="{path}" /></div></div>',
	 				fixed_size: true,
	 				
	 				/******************************************
	 				Enable Responsive settings below if needed.
	 				Max width and height values are optional.
	 				******************************************/
	 				responsive: false,
	 				responsive_maintain_ratio: true,
	 				max_WIDTH: '',
	 				max_HEIGHT: ''
	 			});		
				
	 	//		console.log(($(window)));
		//		console.log("-----height-----"+($(window).height()));
		//		$("#photo_item").height($(window).height()-60);
		 	},
		 	error:function(){
		 		$.unblockUI();       //遮罩关闭
				alert("System error >_<!!!");			 		
		 	}
		 });
	})
	
	
	function setZoom (){
  		var imgObj = new Image();
  	    imgObj.src = $("#rotateImg").attr('src');
  	    var sizeImg = imgObj.width>imgObj.height?imgObj.width:imgObj.height;
  	    $('#fullResImage').css({
  	    "width":sizeImg+"px",
  	    "height":sizeImg+"px",
  	    });
  	    if(imgObj.width<imgObj.height){				//高图需要进行横向居中。正好相等则什么也不做 
  	    	var leftsize = Math.floor((1-imgObj.width/imgObj.height)/2*100);//计算应距离左边边距百分之多少，并给过高的图片加行left属性
  	    	 $("#rotateImg").css({
  				"position" : "absolute",
  				"left" : leftsize+"%"
  			});
  	    }else if(imgObj.width>imgObj.height){		//宽图需要进行纵向居中
  	    	var topsize = Math.floor((1-imgObj.height/imgObj.width)/2*100);//计算应距离上边距百分之多少，并给过宽的图片加行top属性
  		   $("#rotateImg").css({
  				"position" : "absolute",
  				"top" :""+topsize+"%",
  			});
  		}
		$('#fullResImage').smoothZoom('destroy').smoothZoom();
	}

	function closeZoom (){
		$('#fullResImage').smoothZoom('destroy');
	}
	//图片旋转
	var indexZ = 0;
	function turnLeft()
	{
		if (indexZ == 360 | indexZ == -360)
			indexZ = 0;
		$("#rotateImg").css({
			"-moz-transform" : "rotate(" + (indexZ -= 90) + "deg)",
			"-webkit-transform" : "rotate(" + (indexZ) + "deg)"
		});

	} 
	function turnRight()
	{
 		if (indexZ == 360 | indexZ == -360)
			indexZ = 0;
		$("#rotateImg").css({
			"-moz-transform" : "rotate(" + (indexZ += 90) + "deg)"
		});
		$("#rotateImg").css({
			"-webkit-transform" : "rotate(" + (indexZ) + "deg)"
		});
	}
	
</script>
</head>
<body>
	<div class="content">
		
		<div class="module-toolbar">
			<span class="folder_show_toolbar">All File</span>
			<a title="Home" class="view" href="javascript:void(0)" >Home</a>
			<a title="List" class="list" href="javascript:void(0)" >List</a>
		</div>
		<div id="show_photo">
			<div class="photo_folder">
				
				<span style="float:right;margin-right:50px;" id="pic_count"></span>
				<ul class="ul_folder" id="picture_folder">
					
				</ul>
				<div style="clear:both;"></div>
			</div>
			<div id="photo_item">
				
			</div>
		</div>
  	</div>
</body>
</html>