<%@ page language="java" pageEncoding="utf-8"%>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<%@ include file="../../include.jsp"%> 

<%
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(20);
	String company = StringUtil.getString(request,"company");
	DBRow[] companys = freightMgrLL.getCompanys();
	DBRow[] rows = freightMgrLL.getFreightResourcesByCompany(company,pc);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>运输资源选择</title>    
    
	<link href="../comm.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" src="../../common.js"></script>

	<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	<script type="text/javascript" src="../js/select.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	function closeWindow(){
		$.artDialog.close();
		//self.close();
	}
	</script>
  </head>
  
  <body onload="onLoadInitZebraTable()">
  <table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" style="padding-top: 17px" class="zebraTable" id="stat_table">
     <tr>
     	<td>
	     	<form action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_resources/freight_resources_select.html">
	     		<select name="company" id="company">
	     			<option value="">全部货运资源</option>
	     			<%
	     				for(int i=0; i<companys.length; i++) {
							out.println("<option value='"+companys[i].getString("fr_company")+"' "+(company.equals(companys[i].getString("fr_company"))?"selected":"")+">"+companys[i].getString("fr_company")+"</option>");
	     				}
	     			%>
	     		</select>
	     		<input type="submit" value="过滤">
	     	</form>
     	</td>
     </tr>
  </table>
    <div id="applyMoneyList">
  <table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" style="padding-top: 17px" class="zebraTable" id="stat_table">
     <tr>
       <th width="6%" nowrap="nowrap" class="right-title"  style="text-align: center;">货运公司</th>
       <th width="5%" nowrap="nowrap" class="right-title"  style="text-align: center;">运输方式</th>
       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">承运公司</th>
       <th width="6%" nowrap="nowrap" class="right-title"  style="text-align: center;">始发国/港口</th>
       <th width="6%" nowrap="nowrap" class="right-title"  style="text-align: center;">目的国/港口</th>
       <th width="6%" nowrap="nowrap" class="right-title" style="text-align: center;">&nbsp;</th>

     </tr>
     <%
     if(rows!=null){
	     for(int i = 0;i<rows.length;i++)
	     {
	    	 DBRow row = rows[i];
	    	 String fr_id = row.getString("fr_id");
	    	 String fr_company = row.getString("fr_company");
	    	 int fr_way = row.get("fr_way",0);
	    	 String fr_undertake_company = row.getString("fr_undertake_company");
	    	 long fr_from_country = row.get("fr_from_country",0l);
	    	 long fr_to_country = row.get("fr_to_country",0l);
	    	 String from_country = freightMgrLL.getCountryById(Long.toString(fr_from_country)).getString("c_country");
	    	 String to_country = freightMgrLL.getCountryById(Long.toString(fr_to_country)).getString("c_country");
	    	 String fr_from_port = row.getString("fr_from_port");
	    	 String fr_to_port = row.getString("fr_to_port");
      %>
     <tr height="80px" style="padding-top:3px;">
        <td align="center" valign="middle" style="padding-top:3px;">
			<%=fr_company %>	
        </td>
         <td align="center" valign="middle" nowrap="nowrap" >       
      		<%=transportWayKey.getStatusById(fr_way) %>
        </td>
        <td align="left" valign="middle">
			<%=fr_undertake_company %>
        </td>
        <td align="center" >
			<%=from_country+"<br/>"+fr_from_port %>	          
        </td>
        <td align="left" valign="middle" >       
			<%=to_country+"<br/>"+fr_to_port %>    	
        </td>
     	   <td align="left" valign="middle" style="padding-top: 5px;padding-bottom: 5px" nowrap="nowrap">
     	   <input type="button" class="short-short-button-mod" onclick="$.artDialog.opener.setFreight(<%=fr_id %>,'<%=fr_company %>','<%=fr_undertake_company %>',<%=fr_way %>,'<%=transportWayKey.getStatusById(fr_way) %>',<%=fr_from_country %>,'<%=from_country %>',<%=fr_to_country %>,'<%=to_country %>','<%=fr_from_port %>','<%=fr_to_port %>');closeWindow();" value="选择"/><br/><br/>
     	   </td>
     </tr>
     <%
    	 }
     }
     %>
  </table>
  <br>
  <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
    <input type="hidden" name="p">
    <input type="hidden" id="company" name="company" value="<%=company %>"/>
  </form>
  <tr>
    <td height="28" align="right" valign="middle">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
</div>
  </body>
</html>
