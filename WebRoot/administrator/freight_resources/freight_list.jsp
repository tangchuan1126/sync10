<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.ccc.FreightResourcesMgrCCC"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<%@ include file="../../include.jsp"%>
<%
	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(20);
 	
 	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 	

 	String cmd = StringUtil.getString(request,"cmd");
 	String company = StringUtil.getString(request,"company");
 	String fr_from_country = StringUtil.getString(request,"fr_from_country"); 	
 	String fr_to_country = StringUtil.getString(request,"fr_to_country"); 	
 	int fr_way = StringUtil.getInt(request,"fr_way",0);
 	String fr_from_port = StringUtil.getString(request,"fr_from_port");
 	String fr_to_port = StringUtil.getString(request,"fr_to_port");
 	
 	
 	DBRow freightr [] = null;
 	freightr = freightResourcesMgrCCC.getFreightResourcesByCondition(request,pc);
	
	DBRow[] countyRows = countryMgrLY.getAllCountry();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script language="javascript">
function goFreightPost(){
	window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_cost/listFreightCostAction.html";
}
function closeWinNotRefresh()
{
   tb_remove();
}

function changeColorBlack(){

	if($("#company").val()=="*货运公司或承运公司")
	{
 	 	 
 	 	 $("#company").val("");
 	 	 $("#company").removeAttr("class");
 	}
 	
}

function changeColorGray()
{
	 
	 if($("#company").val().trim()=="")
	 {
		  $("#company").val("*货运公司或承运公司");
		  $("#company").attr("class","searchbarGray");
	 }
}

function checkForm()
{
    if($("#company").val().trim()==""||$("#company").val().trim()=="*货运公司或承运公司"||$("#company").val()==null)
    {
       alert("请输入查询信息！");
       $("#company").focus();
       return false;
    }
    
    return true;
}

function delFreight(id)
{
   if (confirm("确定完成操作吗？"))
	 {
	 		$("#delfr_id").val(id);
	 		$("#deleteFreightForm").submit();
	 }
}
</script>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />

<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<style>




.zebraTable tr td a:visited {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:hover {
	color: #000000;
	text-decoration: underline;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:active {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
 .searchbarGray{
        color:#c4c4c4;font-family: Arial
   }
</style>

</head>

<body onLoad="onLoadInitZebraTable()">
			
<div align="left"><br> 
</div>
<form id="deleteFreightForm" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/freight_resource/deleteFreightResources.action">
<input type="hidden" name="fr_id" id="delfr_id"/>
</form>


<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td width="80%" class="page-title" style="border-bottom:0px;" align="left"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle"/>&nbsp;&nbsp; 采购管理 »   货运资源列表 </td>
  </tr>
</table>
<br/>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	
	<div class="demo" >

	<div id="tabs">
<ul>
	<li><a href="#usual_tools">常用工具</a></li>
	<li><a href="#advan_search">高级搜索</a></li>		 
</ul>

		<div id="usual_tools">
		<form method="post" name="search" id="search" action="listFreightResourcesAction.html" onSubmit="return checkForm()" >
		<input type="hidden" id="cmd" name="cmd" value="search"/>
          <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
            <tr>
	            <td width="80%" align="left" style="padding-top:3px;padding-left: 5px" nowrap="nowrap">
				 <input type="text" onblur="changeColorGray()" onclick="changeColorBlack()" size="48" id="company" name="company" value="<%=(company.equals("")?"*货运公司或承运公司":company) %>" />
             	&nbsp; &nbsp;<input type="submit" class="button_long_search" id="btnSubmit" name="btnSubmit" value="搜    索" /> 
				</td>	           

                <td width="20%" style="padding-right:5px" align="right" valign="middle" nowrap="nowrap">
                	<input type="button" class="long-long-button-add" value="创建货运资源"  onclick="$.artDialog.open('addFreightResourcesAction.html', {title: '创建货运资源',width:'700px',height:'450px', lock: true,opacity: 0.3})"/>
                	<input type="button" class="short-button" onClick="goFreightPost()"  value="货运运费" />
                </td>
            </tr>
          </table>
		  </form> 
		</div>

		
	  <%
TDate tDate = new TDate();
tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

String input_st_date,input_en_date;
if ( st.equals("") )
{	
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}
else
{
	input_st_date = st;
}

if ( en.equals("") )
{	
	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
}else
{
	input_en_date = en;
}

%>
 
 <div id="advan_search">

	<form name="filterForm" action="listFreightResourcesAction.html" method="post">
	<input type="hidden" id="cmd" name="cmd" value="filter"/>
	   <table width="100%" height="30" border="0" cellpadding="1" cellspacing="0">
		  
	<tr height="29">
	
	 <td>运输方式</td>
		      <td>		      		    		     
		      	<div style="float:left;">
		      	<select name="fr_way" id="fr_way" data-placeholder="请选择分类" class="chzn-select1" style="width:100px;">
		      		<option value='0'>任意</option>
		      		<%	
	          			ArrayList statuses4 = transportWayKey.getStatuses();
		      				for(int i=0; i<statuses4.size(); i++) {
		      					int statuse = Integer.parseInt(statuses4.get(i).toString());
		          				String key = transportWayKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"' " + (statuse==fr_way?"selected":"") + ">"+key+"</option>");
		      			}	          			
	          		%>
				</select>
				</div>
		      </td>
	 <td>始发国</td>
		     
		      <td>
			      <div style="float:left;">
			      	<select name="fr_from_country" id="fr_from_country" data-placeholder="请选择分类" class="chzn-select1" style="width:100px;" >
			      		<option value='0' >任意</option>
						<%
						
	          			for(int i=0;i<countyRows.length;i++) {
	          				if(countyRows[i].get("ccid","").equals(fr_from_country)){
	          					String countryStr = freightResourcesMgrCCC.getCountryById(fr_from_country).getValue("c_country").toString();
								out.println("<option value='"+fr_from_country+"' selected>"+countryStr+"</option>");
	          				}else{
	          					out.println("<option value='"+countyRows[i].get("ccid",0)+"'>"+countyRows[i].getString("c_country")+"</option>");
	          				}
	          				
	          			}
	          		%>
					</select>
				</div>	

				</td>
	 <td>始发港
	 <input name="fr_from_port" type="text" id="fr_from_port" size="9"  style="border:1px #CCCCCC solid;width:85px;" value="<%=fr_from_port %>"/>
	 </td>
	 <td>目的国</td>
		     
		      <td>
			      <div style="float:left;">
			      	<select name="fr_to_country" id="fr_to_country" data-placeholder="请选择分类" class="chzn-select1" style="width:100px;" >
			      		<option value='0' >任意</option>
						<%
	          			for(int i=0;i<countyRows.length;i++) {
	          				if(countyRows[i].get("ccid","").equals(fr_to_country)){
	          					String countryStr = freightResourcesMgrCCC.getCountryById(fr_to_country).getValue("c_country").toString();
								out.println("<option value='"+fr_to_country+"' selected>"+countryStr+"</option>");
	          				}else{
	          					out.println("<option value='"+countyRows[i].get("ccid",0)+"'>"+countyRows[i].getString("c_country")+"</option>");
	          				}
	          			}
	          		%>
					</select>
				</div>	

				</td>
	 <td>目的港
	 <input name="fr_to_port" type="text" id="fr_to_port" size="9"  style="border:1px #CCCCCC solid;width:85px;" value="<%=fr_to_port %>"/>
	 </td>
		<td align="left" valign="center">
		      	<input type="submit" id="seachByCondition" name="seachByCondition" class="button_long_refresh" value="过    滤" />
		</td>
	</tr>
		</table>
	</form>
 </div>

	</div>	
	</div>	
	</td>
  </tr>
</table>
<br/>
<script>
	$("#st").date_input();
    $("#en").date_input();
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>

<div id="applyMoneyList">
  <table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" style="padding-top: 17px" class="zebraTable" id="stat_table">
     <tr>
       <th width="12%" nowrap="nowrap" class="right-title"  style="text-align: center;">货运公司</th>
       <th width="5%" nowrap="nowrap" class="right-title"  style="text-align: center;">运输方式</th>
       <th width="13%" nowrap="nowrap" class="right-title"  style="text-align: center;">承运公司</th>
       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">始发国/港口</th>
       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">目的国/港口</th>
       <th width="8%" nowrap="nowrap" class="right-title"  style="text-align: center;">联系人</th>
       <th width="8%" nowrap="nowrap" class="right-title"  style="text-align: center;">电话</th>
       <th width="15" nowrap="nowrap" class="right-title"  style="text-align: center;">交货地址</th>
       <th width="8%" nowrap="nowrap" class="right-title"  style="text-align: center;">备注</th>
       <th width="6%" nowrap="nowrap" class="right-title" style="text-align: center;">&nbsp;</th>

     </tr>
     <%
     if(freightr!=null){
	     for(int i = 0;i<freightr.length;i++)
	     {
      %>
     <tr height="80px" style="padding-top:3px;">
        <td align="center" valign="middle" style="padding-top:3px;">
           <%=freightr[i].getString("fr_company") %>
        </td>
        <td align="center" valign="middle">
        <%=freightr[i].get("fr_way",0)!=0?transportWayKey.getStatusById(freightr[i].get("fr_way",0)):"-"%>
           
        </td>
        <td align="center" valign="middle">
			&nbsp;<%=freightr[i].getString("fr_undertake_company") %>
        </td>
        
        <td align="center" >
          &nbsp;<%=freightr[i].getString("fr_from_country")!=null?freightResourcesMgrCCC.getCountryById(freightr[i].getString("fr_from_country")).getValue("c_country"):"-" %>/&nbsp;<%=freightr[i].getString("fr_from_port") %>
        </td>
        
        <td align="center" >
          &nbsp;<%=freightr[i].getString("fr_to_country")!=null?freightResourcesMgrCCC.getCountryById(freightr[i].getString("fr_to_country")).getValue("c_country"):"-" %>/&nbsp;<%=freightr[i].getString("fr_to_port") %>
        </td>
        <td align="left" valign="middle" >       
           &nbsp;<%=freightr[i].getString("fr_contact") %>
        </td>
        <td align="left" valign="middle">&nbsp;
          &nbsp;<%=freightr[i].getString("fr_contact_tel") %>
        </td>
        <td align="left" valign="middle">&nbsp;
          &nbsp;<%=freightr[i].getString("fr_delivery_address") %>
        </td>
        <td align="left" valign="middle">
		&nbsp;<%=freightr[i].getString("fr_remark") %>
        </td>
        
      
        	<td align="left" valign="middle">
        	<!-- <input type="button" class="short-short-button-merge" onClick="goFundsTransferListPage()"  value="查看" />
        	<br/><input type="button" class="short-button" onClick="goFreightPost(<%=freightr[i].getString("fr_id") %>)"  value="货运运费" /> -->
        	<br/><br/><input type="button" class="short-short-button-cancel" value="删除" onClick="delFreight('<%=freightr[i].get("fr_id",0l) %>')" />
     	   <br/><br/><input type="button" class="short-short-button-mod" onClick="tb_show('修改申请','updateFreightResourcesAction.html?fr_id=<%=freightr[i].getString("fr_id") %>&TB_iframe=true&height=400&width=700',false);" value="修改" />
     	   <br/><br/>
        	</td>	
        	
      
     </tr>
     <%
    	 }
     }
     %>
  </table>
  <br>
  <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
    <input type="hidden" name="p">
    <input type="hidden" id="cmd" name="cmd" value="<%=StringUtil.getString(request,"cmd") %>"/>
    <input type="hidden" id="center_account_id" name="center_account_id" value="<%=StringUtil.getLong(request,"center_account_id") %>" />
    <input type="hidden" id="product_line_id" name="product_line_id" value="<%=StringUtil.getLong(request,"product_line_id") %>" />
    <input type="hidden" id="association_type_id" name="association_type_id" value="<%=StringUtil.getLong(request,"association_type_id") %>" />
    <input name="st" type="hidden" id="st" size="9" value="<%=StringUtil.getString(request,"st")%>"/>
    <input name="en" type="hidden" id="en" size="9" value="<%=StringUtil.getString(request,"en")%>"/>
    <input type="hidden" name="types" value="<%=StringUtil.getInt(request,"filter_acid",-1) %>" />
    <input type="hidden" id="status" name="status" value="<%=StringUtil.getInt(request,"status",0) %>"/>
  </form>
  <tr>
    <td height="28" align="right" valign="middle">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
</div>

</body>
</html>