<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<%@ include file="../../include.jsp"%> 

<%
	DBRow[] countyRows = deliveryMgrLL.getCounty();
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>


<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>

<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 

<script type="text/javascript">

var updated = false;
	
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"added")=="1"||"1".equals(StringUtil.getString(request,"added"))) {
	%>
			updated = true;
	<%
		}
	%>
	
	function init() {
		if(updated) {
			parent.location.reload();
			closeWindow();
		}
	}
	
	$(document).ready(function(){
			init();
			//parent.location.reload();
			
			$(".chzn-select").chosen(); 
			$(".chzn-select1").chosen();
	})
	
 function closeWindow(){
				$.artDialog.close();
				//self.close();
			}
<!--
	function submitFreight()
	{
			 if($('input[name=fr_company]').val().trim()==""||$('input[name=fr_company]').val()==null)
			 {
			     $('input[name=fr_company]').focus();
			     alert("货运公司不能为空！");
			 }
			 
			 else if($('input[name=fr_undertake_company]').val().trim()==""||$('input[name=fr_undertake_company]').val()==null)
			 {
			 	 $('input[name=fr_undertake_company]').focus();
			 	 alert("承运公司不能为空！");
			 }
			 else if($("#fr_contact").val().trim()==""||$("#fr_contact").val()==null)
			 {
			 	 $("#fr_contact").focus();
			 	 alert("联系人不能为空！");
			 } 
			 else if($("#fr_contact_tel").val().trim()==""||$("#fr_contact_tel").val()==null)
			 {
			 	 $("#fr_contact_tel").focus();
			 	 alert("联系电话不能为空！");
			 }			 
			 else if($("#fr_delivery_address").val().trim()==""||$("#fr_delivery_address").val()==null)
			 {
			     $("#fr_delivery_address").focus();
			     alert("交货地址不能为空！");
			 }
			 else if($("#fr_remark").val().trim().length>=200)
			 {
			     $("#fr_remark").focus();
			     alert("备注字数不能超过200！");
			 }
			 else
			 {
				document.apply_freight_form.submit();
				//parent.location.reload();
				//closeWindow();
			 }	
	}
//-->
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="apply_freight_form" id="apply_freight_form" action="<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/freight_resource/addFreightResources.action">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">货运资源信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr height="29">
		          <td align="right" class="STYLE2" width="10%" nowrap="nowrap">货运公司</td>
		          <td align="left" valign="middle" width="40%">
	  				<input type="text" name="fr_company" id="fr_company" value="" />
		          </td>
		      <td align="right" class="STYLE2" nowrap="nowrap" width="10%">运输方式</td>
		      <td width="40%">		      		    		     
		      	<div style="float:left;">
		      	<select name="fr_way" id="fr_way" data-placeholder="请选择分类" class="chzn-select1" style="width:150px;">
					<%	
	          			ArrayList statuses4 = transportWayKey.getStatuses();
	          			for(int i=0;i<4;i++) {
	          				int statuse = Integer.parseInt(statuses4.get(i).toString());
	          				String key = transportWayKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"'>"+key+"</option>");
	          			}
	          		%>
				</select>
				</div>
		      </td>
		   
		    </tr>
			
		  <tr> 
		      <td align="right" class="STYLE2" nowrap="nowrap">承运公司</td>
		      <td colspan="3"><input name="fr_undertake_company" type="text" id="fr_undertake_company" value=""></td>
		    </tr>
				  
		   
		    <tr> 
		      <td align="right" class="STYLE2" nowrap="nowrap">始发国</td>
		     
		      <td >
			      <div style="float:left;">
			      	<select name="fr_from_country" id="fr_from_country" data-placeholder="请选择分类" class="chzn-select" style="width:150px;">
						<%
	          			for(int i=0;i<countyRows.length;i++) {
	          				out.println("<option value='"+countyRows[i].get("ccid",0)+"'>"+countyRows[i].getString("c_country")+"</option>");
	          			}
	          		%>
					</select>
				</div>	

				</td>
				<td align="right" class="STYLE2" nowrap="nowrap">始发港口</td>
		     
		      <td>
			    <input name="fr_from_port" id="fr_from_port" type="text" value="" />	

				</td>
		   
		    </tr>	
		     <tr> 
		      <td align="right" class="STYLE2" nowrap="nowrap">目的国</td>
		     
		      <td>
			      <div style="float:left;">
			      	<select name="fr_to_country" id="fr_to_country" data-placeholder="请选择分类" class="chzn-select1" style="width:150px;">
						<%
	          			for(int i=0;i<countyRows.length;i++) {
	          				out.println("<option value='"+countyRows[i].get("ccid",0)+"'>"+countyRows[i].getString("c_country")+"</option>");
	          			}
	          		%>
					</select>
				</div>	

				</td>
				<td align="right" class="STYLE2" nowrap="nowrap">目的港口</td>
		     
		      <td>
			    <input name="fr_to_port" id="fr_to_port" type="text" value="" />	

				</td>
		   
		    </tr>	
		     <tr> 
		      <td align="right" class="STYLE2" nowrap="nowrap">联系人</td>
		      <td><input name="fr_contact" id="fr_contact" type="text" value="" /></td>
		      <td align="right" class="STYLE2" nowrap="nowrap">联系电话</td>
		      <td>		      		    		     
		      	<input name="fr_contact_tel" id="fr_contact_tel" type="text" value="" />
		      </td>
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2" nowrap="nowrap">交货地址</td>
		      <td colspan="3">		      		    		     
		      	<input name="fr_delivery_address" id="fr_delivery_address" type="text" value="" style="width:510px"/>
		      </td>
		    </tr>	    
		    <tr> 
		      <td align="right" class="STYLE2" nowrap="nowrap">备注:</td>
		      <td colspan="3"><textarea id="fr_remark" name="fr_remark" rows="4" cols="80"></textarea></td>
		    </tr>
		</table>
		
	</fieldset>	
	</td>
  </tr>

</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onclick="submitFreight()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>
</table>
</form>

<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
</body>
</html>
