<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@ include file="../../include.jsp"%> 
<%
	long b2b_oid = StringUtil.getLong(request,"b2b_oid");
	long transport_id = transportMgrZJ.getTransportIdsByB2BOid(b2b_oid)[0];
	DBRow[] rows = productStoreLocationMgrZJ.getOutListDetails(transport_id,ProductStoreBillKey.TRANSPORT_ORDER);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
.profile {
	background-attachment: scroll; 
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
</style>
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../common.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

</head>

<body >
<form name="transfer_form" method="post">
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
	<input type="hidden" name="transfer_type"/>
	<input type="hidden" name="money_status"/>
</form>

<form action="" method="post" name="followup_form">
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
</form>
	
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="10%"  class="left-title " style="vertical-align: center;text-align: center;">位置</th>
        <th width="35%"  class="left-title " style="vertical-align: center;text-align: center;">商品名</th>
        <th width="13%"  class="right-title " style="vertical-align: center;text-align: center;">目标容器</th>
        <th width="10%"  class="right-title " style="vertical-align: center;text-align: center;">需求容器</th>
  	</tr>
  	<% 
  		for(int i=0;i<rows.length;i++)
  		{
  	%>
  		<tr align="center" valign="middle">
		    <td height="30"><%=adminMgrLL.getAdminById(rows[i].getString("b2ber_id")).getString("employe_name") %></td>
		    <td align="left">
		    	<%
		    		out.print(rows[i].getString("b2b_content"));
		    	%>	      
		    </td>
		    <td><%=tdate.getFormateTime(rows[i].getString("b2b_date")) %></td>
		    <td><%=followuptype.get(rows[i].get("b2b_type",0))%></td>
		    <td><%=  rows[i].getString("time_complete")%>&nbsp;</td>
	  	</tr>
  	<%
  		}
  	%>
 	
</table>
</body>
</html>



