<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.SimulationTransportKey"%>
<%@ include file="../../include.jsp"%>

<%
	long transport_id = StringUtil.getLong(request,"transport_id");
	String inserted = StringUtil.getString(request,"inserted");
	DBRow[] rows = transportOutboundMgrZJ.getTransportOutboundByTransportId(transport_id);
%>
<html>
  <head>
    <title>到货</title>
		<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
		<link href="../comm.css" rel="stylesheet" type="text/css">
		<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type='text/javascript' src='../js/jquery.form.js'></script>
		<script type="text/javascript" src="../js/select.js"></script>
		<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
		
		<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
		
		<%-- Frank ****************** --%>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
		<%-- ***********************  --%>
		
		<%-- Frank ****************** --%>
		<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
		<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
		<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
		<%-- ***********************  --%>
		

		<script type="text/javascript">
			$().ready(function() {	
				<%-- Frank ****************** --%>
				addAutoComplete($("#product_name"),
						"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
						"merge_info",
						"p_name");
				<%-- ***********************  --%>	
			});

			function closeWindow(){
				$.artDialog.close();
			}

			function deleteRow(input){  
		          var s=input.parentNode.parentNode.rowIndex;
		          document.getElementById("tables").deleteRow(s); 
		          //var num=document.getElementById("tables").rows.length;  
		         
		    }

		    function addRow() {
			    if($('#product_name').val()=="") {
				    alert('请输入商品!');
				    return;
			    }
			    if($('#stockIn_count').val()==""||$('#stockIn_count').val()=="0") {
				    alert('请输入实到数量!');
				    return;
			    }
			    
		    	var row = document.getElementById("tables").insertRow(document.getElementById("tables").rows.length);
		    	var add1=row.insertCell(0).innerHTML = "<input type='hidden' name='product_name' value='"+document.getElementById("product_name").value+"'/>"+document.getElementById("product_name").value;
				var add2=row.insertCell(1).innerHTML = "0";
				var add3=row.insertCell(2).innerHTML = "<input type='text' name='stockIn_count' value='"+document.getElementById("stockIn_count").value+"'/>";
				var add4=row.insertCell(3).innerHTML = "<input type='button' value='删除' onclick='deleteRow(this)'/>";
				$('#product_name').val('');
				$('#stockIn_count').val('');

		    }

		    function submitStockTempIn()
		    {
			  $('#stockInFrm').submit();
		    }

		    var inserted = false;
			<%
				if(inserted.equals("1")) {
			%>
					inserted = true;
			<%
				}
			%>
			
		    function init() {
				if(inserted) {
					parent.location.reload();
					closeWindow();
				}
			}

			$(document).ready(function(){
				init();
			});
			
			function uploadFile(_target)
			{
			    var obj  = {
				     reg:"xls",
				     limitSize:2,
				     target:_target
				 }
			    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
				uri += jQuery.param(obj);
				
				$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
						 //close:function()
						 //{
								//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
						 //	 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
						 //}
					 });
			}
			
			function simulationOutIn(file_name)
			{
				$.ajax({
					url: 'transport_simulation_out_in.html',
					type: 'post',
					dataType: 'html',
					timeout: 60000,
					cache:false,
					data:{
						file_name:file_name,
						transport_id:<%=transport_id%>,
						simulation_type:<%=SimulationTransportKey.In%>
					},
					
					beforeSend:function(request){
					},
					
					error: function(){
					},
					
					success: function(html)
					{
						$("#details").html(html);
						
						$("#submitButton").css("display","");
					}
				});
			}
			
			function uploadFileCallBack(fileNames,target)
			{
			   simulationOutIn(fileNames);
			}
		</script>
  </head>
  
  <body>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
	<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">转运单到货&nbsp;&nbsp;<input name="button" type="button" class="long-button" onclick="uploadFile('')" value="上传收货文件"/></legend>
		<form id="stockInFrm" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/transportStockTempInAction.action">
		<input type="hidden" name="backurl" value='<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_stock_temp_in.html?transport_id=<%=transport_id %>&inserted=1'/>
		<input type="hidden" id="transport_id" name="transport_id" value="<%=transport_id %>">
		<input type="hidden" name="is_need_notify_executer" value="true"/>
		<div id="details">
		<table id="tables" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td width="30%" align="left">商品名称</td>
				<td width="15%" align="center">应到数量</td>
				<td width="15%" align="center">实到数量</td>
				<td width="20%" align="center">序列号</td>
				<td width="20%" align="center">托盘</td>
			</tr>
			<%
				for(int i=0; rows!=null && i<rows.length; i++) {
					String transport_p_name = rows[i].getString("to_p_name");
					String transport_send_count = rows[i].getString("to_count");
					String transport_product_serial_number = rows[i].getString("to_serial_number");
					long transport_lp_id = rows[i].get("to_lp_id",0l);
			%>
			<tr>
				<td align="left">
					<input type="hidden" name="product_name" value="<%=transport_p_name %>"/>
					<%=transport_p_name %>
				</td>
				<td align="center">
					<%=transport_send_count %>
				</td>
				<td align="center">
					<%=transport_send_count%>
				</td>
				<td align="left">
					<input type="hidden" name="product_serial_number" value="<%=transport_product_serial_number %>"/>
					<%=transport_product_serial_number%>
				</td>
				<td align="left">
					<%
						DBRow container = containerMgrZYZ.getDetailContainer(transport_lp_id);
						if(container !=null)
						{
							out.print(container.getString("container"));
						}
					%>
				</td>
			</tr>
			<%
				}
			%>
			</table>
			</div>
		</form>
	</fieldset>	

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="insert" type="button" class="normal-green-long" onclick="submitStockTempIn()" value="确定" >
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>
</table>
  </body>
</html>
