<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long transport_id = StringUtil.getLong(request,"transport_id");
	
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
	
	if(transport ==null)
	{
		transport = new DBRow();
	}
	
	float weight = transportMgrZJ.getTransportDetailsSendWeight(transport_id);
	
	DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
	function stateDiv(pro_id,divid)
	{
		var id = "";
		if(divid==="send_pro_id")
		{
			id = "send";
		}
		else
		{
			id = "deliver";
		}
		
		if(pro_id==-1)
		{
			$("#"+id+"_state_div").css("display","inline");
		}
		else
		{
			$("#"+id+"state_div").css("display","none");
		}
	}
	
	//国家地区切换
function getStorageProvinceByCcid(ccid,pro_id,id)
{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#"+id).clearAll();
					$("#"+id).addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#"+id).addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#"+id).addOption("手工输入","-1");
					}
				});
				
				if (pro_id!=""&&pro_id!=0)
				{
					$("#"+id).setSelectedValue(pro_id);
					stateDiv(pro_id,id);
				}
}

	function selectStorage(ps_id)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					ps_id:ps_id
				},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data)
				{
					$("#send_house_number").val(data.send_house_number);
					$("#send_street").val(data.send_street);
					//$("#send_address3").val(data.deliver_address3);
					$("#send_zip_code").val(data.deliver_zip_code);
					$("#send_name").val(data.contact);
					$("#send_linkman_phone").val(data.phone);
					$("#send_city").val(data.city);
					
					$("#send_ccid").val(data["send_nation"]);
					
					getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id");
				}
			});
	}
	
	function selectDeliveryStorage(ps_id)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{ps_id:ps_id},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data){
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					//$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.city);
					$("#deliver_ccid").val(data["deliver_nation"]);
					
					getStorageProvinceByCcid($("#deliver_ccid").val(),data["deliver_pro_id"],"deliver_pro_id");
					
					$("#deliver_name").val(data["deliver_contact"]);
					$("#deliver_linkman_phone").val(data["deliver_phone"]);
				}
			});
	}

$().ready(function() 
{
	getStorageProvinceByCcid(<%=transport.get("deliver_ccid",0l)%>,<%=transport.get("deliver_pro_id",0l)%>,"deliver_pro_id");//初始化地区
	
	getStorageProvinceByCcid(<%=transport.get("send_ccid",0l)%>,<%=transport.get("send_pro_id",0l)%>,"send_pro_id");	
});
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/internal_print_center.html">
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="left" valign="top"><br>
            	  <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;">
						转运单号：T<%=transport.getString("transport_id")%>
						<input type="hidden" name="type" value="T"/>
						<input type="hidden" name="id" value="<%=transport_id%>"/>
						</td>
					  </tr>
				  </table>
				  	<fieldset class="set" style="padding-bottom:4px;text-align: left;">
		  				<legend>
							<span style="" class="title">
								快递隶属仓库	
							</span>
						</legend>
							<table width="95%" align="center" cellpadding="2" cellspacing="0">
								<tr>
									<td align="center">
										<select id="psid" name="ps_id">
							     			<option value="0">选择仓库以计算运费</option>
							     			 <%
							     			 	DBRow[] storageRows = catalogMgr.getProductDevStorageCatalogTree();
												String qx;
												
												for ( int i=0; i<storageRows.length; i++ )
												{
													if ( storageRows[i].get("parentid",0) != 0 )
													 {
													 	qx = "├ ";
													 }
													 else
													 {
													 	qx = "";
													 }
											%>
						          			<option <%=transport.get("send_psid",0l)==storageRows[i].get("id",0l)?"selected=\"selected\"":""%> value="<%=storageRows[i].get("id",0l)%>"> 
												     <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageRows[i].get("level",0))%>
												     <%=qx%>
												     <%=storageRows[i].getString("title")%>
											</option>
											<%
												}
											%>
							     	</select>
									</td>
								</tr>
							</table>
				</fieldset>
				  <br/>
				 <fieldset class="set" style="padding-bottom:4px;text-align: left;">
	  				<legend>
						<span style="" class="title">
							提货地址	
						</span>
					</legend>
				 	<table width="95%" align="center" cellpadding="2" cellspacing="0">
				 	<tr>
				 		<td align="right">提货仓库</td>
				 		<td>
				 				<select onchange="selectStorage(this.value)">
					     			<option value="0">选择仓库以计算运费</option>
					     			 <%
										
										for ( int i=0; i<storageRows.length; i++ )
										{
											if ( storageRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
									%>
				          			<option <%=transport.get("send_psid",0l)==storageRows[i].get("id",0l)?"selected=\"selected\"":""%> value="<%=storageRows[i].get("id",0l)%>"> 
										     <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageRows[i].get("level",0))%>
										     <%=qx%>
										     <%=storageRows[i].getString("title")%>
									</option>
									<%
										}
									%>
					     			 </select>
					    </td>
				 	</tr>
				 	<tr>
						 <td align="right">提货国家</td>
						 <td>
						 	<%
							DBRow countrycode[] = orderMgr.getAllCountryCode();
							String selectBg="#ffffff";
							String preLetter="";
							%>
							 <select name="send_ccid" id="send_ccid" onChange="getStorageProvinceByCcid(this.value,0,'send_pro_id');">
								<option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++)
								  {
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
									{
										if (selectBg.equals("#eeeeee"))
										{
											selectBg = "#ffffff";
										}
										else
										{
											selectBg = "#eeeeee";
										}
									}  	
									
									preLetter = countrycode[i].getString("c_country").substring(0,1);
								  %>
								    <option style="background:<%=selectBg%>;" <%=transport.get("send_ccid",0l)==countrycode[i].get("ccid",0l)?"selected=\"selected\"":""%> value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								  <%
								  }
								  %>
							      </select>		
						 		</td>
						 	</tr>
					<tr>
						 <td align="right">提货省份</td>
						 		<td>
						 			<select name="send_pro_id" id="send_pro_id">
								    </select>		
								      	<div style="padding-top: 10px;display:none;" id="send_state_div">
											<input type="text" name="send_address_state" id="send_address_state"/>
										</div>
						 		</td>
					</tr>
				 	<tr>
				 		<td align="right">提货地址门牌</td>
				 		<td><input style="width: 300px;" type="text" name="send_house_number" id="send_house_number" value="<%=transport.getString("send_house_number")%>"/></td>
				 	</tr>
				 	<tr>
				 		<td align="right">提货地址街道</td>
				 		<td><input style="width: 300px;" type="text" name="send_street" id="send_street" value="<%=transport.getString("send_street")%>"/></td>
				 	</tr>
				 	<tr>
				 		<td align="right">提货城市</td>
				 		<td><input style="width: 300px;" type="text" name="send_city" id="send_city" value="<%=transport.getString("send_city")%>"/></td>
				 	</tr>
				 	<tr>
				 		<td align="right">提货地址邮编</td>
				 		<td><input style="width: 300px;" type="text" name="send_zip_code" id="send_zip_code" value="<%=transport.getString("send_zip_code")%>"/></td>
				 	</tr>
				 	<tr>
				 		<td align="right">提货联系人</td>
				 		<td><input style="width: 300px;" type="text" name="send_name" id="send_name" value="<%=transport.getString("send_name")%>"/></td>
				 	</tr>
				 	<tr>
				 		<td align="right">提货联系人电话</td>
				 		<td><input style="width: 300px;" type="text" name="send_linkman_phone" id="send_linkman_phone" value="<%=transport.getString("send_linkman_phone")%>"/></td>
				 	</tr>
				 	
				 </table>
				 </fieldset>
				 <br/>
				  <fieldset class="set" style="padding-bottom:4px;text-align: left;">
	  				<legend>
						<span style="" class="title">
							收货地址	
						</span>
					</legend>
					<table width="95%" align="center" cellpadding="2" cellspacing="0">
							<tr valign="bottom">
									  <td align="right">收货仓库</td>
								      <td>
								      		<select name='receive_psid' id='receive_psid' onchange="selectDeliveryStorage(this.value)">
				                                <option value="0">请选择...</option>
				                                <%
													  for ( int i=0; i<treeRows.length; i++ )
													  {
												%>
				                                <option value='<%=treeRows[i].getString("id")%>' <%=transport.get("receive_psid",0l)==treeRows[i].get("id",0l)?"selected=\"selected\"":""%> ><%=treeRows[i].getString("title")%></option>
				                                <%
													}
												%>
		                              		</select>
								      </td>
							</tr>
				 	<tr>
				 		<td align="right">收货国家</td>
				 		<td>
					      <select name="deliver_ccid" id="deliver_ccid" onChange="getStorageProvinceByCcid(this.value,0,'deliver_pro_id');">
						  <option value="0">请选择...</option>
						  <%
						  for (int i=0; i<countrycode.length; i++)
						  {
						  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
							{
								if (selectBg.equals("#eeeeee"))
								{
									selectBg = "#ffffff";
								}
								else
								{
									selectBg = "#eeeeee";
								}
							}  	
							
							preLetter = countrycode[i].getString("c_country").substring(0,1);
						  %>
						    <option style="background:<%=selectBg%>;" <%=transport.get("deliver_ccid",0l)==countrycode[i].get("ccid",0l)?"selected=\"selected\"":""%> value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
						  <%
						  }
						  %>
					      </select>		
				 		</td>
				 	</tr>
				 	<tr>
				 		<td align="right">收货省份</td>
				 		<td>
				 			<select name="deliver_pro_id" id="deliver_pro_id">
						    </select>		
						      	<div style="padding-top: 10px;display:none;" id="deliver_state_div">
									<input type="text" name="deliver_address_state" id="deliver_address_state"/>
								</div>
				 		</td>
				 	</tr>
				 	
				 	<tr>
				 		<td align="right">收货地址门牌号</td>
				 		<td><input style="width: 300px;" type="text" id="deliver_house_number" name="deliver_house_number" value="<%=transport.getString("deliver_house_number")%>"/></td>
				 	</tr>
				 	<tr>
				 		<td align="right">收货地址街道</td>
				 		<td><input style="width: 300px;" type="text" id="deliver_street" name="deliver_street" value="<%=transport.getString("deliver_street")%>"/></td>
				 	</tr>
				 	<tr>
				 		<td align="right">收货城市</td>
				 		<td><input style="width: 300px;" type="text" id="deliver_city" name="deliver_city" value="<%=transport.getString("deliver_city")%>"/></td>
				 	</tr>
				 	<tr>
				 		<td align="right">收货地址邮编</td>
				 		<td><input style="width: 300px;" type="text" id="deliver_zip_code" name="deliver_zip_code" value="<%=transport.getString("deliver_zip_code")%>"/></td>
				 	</tr>
				 	<tr>
				 		<td align="right">收货联系人</td>
				 		<td><input style="width: 300px;" type="text" id="deliver_name" name="deliver_name" value="<%=transport.getString("transport_linkman")%>"/></td>
				 	</tr>
				 	<tr>
				 		<td align="right">收货联系人电话</td>
				 		<td><input style="width: 300px;" type="text" id="deliver_linkman_phone" name="deliver_linkman_phone" value="<%=transport.getString("transport_linkman_phone")%>"/></td>
				 	</tr>
				 	</table>
				 </fieldset>
				  <fieldset class="set" style="padding-bottom:4px;text-align:center;">
	  				<legend>
						<span style="" class="title">
							估算重量
						</span>
					</legend>
				 	<input style="width: 300px;" type="text" name="weight" id="weight" value="<%=weight%>"/>Kg
				 </fieldset>
				</td>
			 </tr>
             <tr>
                <td  align="right" valign="middle" class="win-bottom-line">				
				 	<input type="submit" name="Submit2" value="下一步" class="normal-green">
			
			    	<input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close();"></td>
             </tr>
            </table>
      </form>
</body>
</html>
