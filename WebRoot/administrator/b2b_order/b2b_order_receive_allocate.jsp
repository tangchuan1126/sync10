<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
	cartWaybillB2BMgrZJ.cleanCart(request);
	
	long b2b_oid = StringUtil.getLong(request, "b2b_oid");
	DBRow[] showReceiveAllocate = b2BOrderMgrZyj.showReceiveAllocate(b2b_oid);
%>
<head>
	
<style type="text/css">
td.topBorder {
	border-top: 1px solid silver;
}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script src="/Sync10-ui/pages/b2b_order/b2b_order_allocate_page.js" type="text/javascript"/>
</head>
<body>
			<table width="100%" cellpadding="0" cellspacing="0"
				style="border: 1px solid silver;">
				<tr>
					<td width="40%" valign="top">
						<div id="receive_allocate_view">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<%
									for (int i = 0; i < showReceiveAllocate.length; i++) 
									{
										String backgroundColor = (i % 2 == 1 ? "#f9f9f9" : "white");
								%>
								<tr	style="height:22px;line-height:22px;background:<%=backgroundColor%>">
									<td colspan="5" align="center">
										<fieldset class="set">
							  				<legend>
							  					<input  id="<%=showReceiveAllocate[i].get("b2b_detail_id",0l)%>" type="checkbox" onclick="checkBoxClick(this.value)" value="<%=showReceiveAllocate[i].get("b2b_detail_id",0l)%>" data-pname="<%=showReceiveAllocate[i].getString("p_name")%>">
							  					<%=showReceiveAllocate[i].getString("p_name")%>
							  					<input rel="all" id="<%=showReceiveAllocate[i].get("b2b_detail_id",0l)%>_allocate_count" type="text" style="width:40px;" value="<%=showReceiveAllocate[i].get("all_count",0)%>" data-clpname="<%=showReceiveAllocate[i].getString("clp_name")%>" data-blpname="<%=showReceiveAllocate[i].getString("blp_name")%>" data-clptotalpiece="<%=showReceiveAllocate[i].get("clp_total_piece",0)%>" data-blptotalpiece="<%=showReceiveAllocate[i].get("blp_total_piece",0)%>" data-b2bOrderdetailid="<%=showReceiveAllocate[i].get("b2b_detail_id",0l)%>" />
							  				</legend>
							  				<input id="<%=showReceiveAllocate[i].get("b2b_detail_id",0l)%>_all_count" type="hidden" style="width:40px;" value="<%=showReceiveAllocate[i].get("all_count",0)%>"/>
							  				<div id="<%=showReceiveAllocate[i].get("b2b_detail_id",0l)%>_show">
							  					<table>
								  					<%
								  						String isShow = "style=\"display:none;\"";
								  						if(showReceiveAllocate[i].get("needCLP",0)>0)
								  						{
								  							isShow = "";
								  						}
								  					%>
								  						<tr <%=isShow%>>
								  							<td nowrap="nowrap" style="font-size:12px;">CLP:<font color="blue"><%=showReceiveAllocate[i].getString("clp_name")%></font></td>
								  							<td align="right"><input rel="clp" id="<%=showReceiveAllocate[i].get("b2b_detail_id",0l)%>_need_clp_count" style="width:40px;" value="<%=showReceiveAllocate[i].get("needCLP",0)%>"/></td>
								  						</tr>
								  					
								  					<%
								  						if(showReceiveAllocate[i].get("needBLP",0)>0)
								  						{
								  							isShow = "";
								  						}
								  						else
								  						{
								  							isShow = "style=\"display:none;\"";
								  						}
								  					%>
								  						<tr <%=isShow%>>
								  							<td nowrap="nowrap" style="font-size:12px;">BLP:<font color="blue"><%=showReceiveAllocate[i].getString("blp_name")%></font></td>
								  							<td align="right"><input rel="blp" id="<%=showReceiveAllocate[i].get("b2b_detail_id",0l)%>_need_blp_count" style="width:40px;" value="<%=showReceiveAllocate[i].get("needBLP",0)%>"/></td>
								  						</tr>
								  					<%
								  						if(showReceiveAllocate[i].get("needILP",0)>0)
								  						{
								  							isShow = "";
								  						}
								  						else
								  						{
								  							isShow = "style=\"display:none;\"";
								  						}
								  					%>
								  						<tr <%=isShow%>>
								  							<td nowrap="nowrap" style="font-size:12px;">ILP:<%=showReceiveAllocate[i].getString("ilp_name")%></td>
								  							<td align="right"><input id="<%=showReceiveAllocate[i].get("b2b_detail_id",0l)%>_need_ilp_count" style="width:40px;" value="<%=showReceiveAllocate[i].get("needILP",0)%>"/></td>
								  						</tr>								  					
								  					<%
								  						if(showReceiveAllocate[i].get("needOriginal",0)>0)
								  						{
								  							isShow = "";
								  						}
								  						else
								  						{
								  							isShow = "style=\"display:none;\"";
								  						}
								  					%>
								  						<tr <%=isShow%>>
								  							<td>Original:</td>
								  							<td><input rel="Orig" id="<%=showReceiveAllocate[i].get("b2b_detail_id",0l)%>_need_orignial_count" style="width:40px;" value="<%=showReceiveAllocate[i].get("needOriginal",0)%>"/></td>
								  						</tr>
								  				</table>
							  				</div>
							  			</fieldset>
									</td>
								</tr>
								<%
									}
								%>
							</table>
						</div>
					</td>
					<td
						style="width: 5%; border-left: 1px solid silver; border-right: 1px solid silver;">
						&nbsp;&nbsp;
					</td>
					<td align="center" style="width: 45%;">
						<div id="pre_calcu_order_page"></div>
						<div id="pre_calcu_order_info"></div>
					</td>
				</tr>
				<tr>
					<td colspan="3" class="topBorder">
					</td>
				</tr>
			</table>
			<table width="100%" cellpadding="0" cellspacing="0"
				style="padding-top: 5px;">
				<tr>
					<td></td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
			</table>

	<script type="text/javascript">
		function preCalcuOrder(b2b_oid)
		{
			$.ajax({
				url: 'b2b_order_pre_calcu.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:{
					b2b_oid:b2b_oid
				},
				
				beforeSend:function(request){
					$("#pre_calcu_order_info").text("正在计算......");
				},
				
				error: function(){
					$("#pre_calcu_order_info").text("计算失败！");
				},
				
				success: function(html){
					$("#pre_calcu_order_page").html(html);
					//console.log("DEBUG");
					$("#pre_calcu_order_info").text("");
					Sealingcalculation.selectcheckbox();
				}
			});
		}
		
		function checkBoxClick(b2b_detail_id)
		{
			if($("#"+b2b_detail_id).attr("checked")=="checked")
			{
				putToB2BWaybillForItem(b2b_detail_id);
			}
			else
			{
				removeToB2BWaybillForItem(b2b_detail_id);
			}
		}
		
		function putToB2BWaybillForItem(b2b_detail_id)
		{
			var clp_count = $("#"+b2b_detail_id+"_need_clp_count").val();
			var blp_count = $("#"+b2b_detail_id+"_need_blp_count").val();
			var ilp_count = $("#"+b2b_detail_id+"_need_ilp_count").val();
			var count = $("#"+b2b_detail_id+"_need_orignial_count").val();
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybillB2B/putToWaybillB2B.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				async:false,
				data:{
					b2b_detail_id:b2b_detail_id,
					clp_count:clp_count,
					blp_count:blp_count,
					ilp_count:ilp_count,
					count:count
				},
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(msg){
					if(msg=="ok")
					{
						preCalcuOrder(<%=b2b_oid%>)
					}
				}
			});	
		}
		function removeToB2BWaybillForItem(b2b_detail_id)
		{
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybillB2B/reomveToWaybillB2B.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				async:false,
				data:{
					b2b_detail_id:b2b_detail_id
				},
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(msg){
					if(msg=="ok")
					{
						preCalcuOrder(<%=b2b_oid%>)
					}
				}
			});	
		}
		preCalcuOrder(<%=b2b_oid%>);
	</script>
</body>