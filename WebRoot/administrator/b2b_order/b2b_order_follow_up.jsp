<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.B2BOrderTagKey"%>
<%@page import="com.cwc.app.key.B2BOrderCertificateKey"%>
<%@page import="com.cwc.app.key.B2BOrderProductFileKey"%>
<%@page import="com.cwc.app.key.B2BOrderLogTypeKey"%>
 
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.ArrayList" %>

<jsp:useBean id="b2BOrderClearanceKey" class="com.cwc.app.key.B2BOrderClearanceKey"/>
<jsp:useBean id="b2BOrderDeclarationKey" class="com.cwc.app.key.B2BOrderDeclarationKey"/>
<jsp:useBean id="b2BOrderKey" class="com.cwc.app.key.B2BOrderKey"/>
<jsp:useBean id="b2BOrderCertificateKey" class="com.cwc.app.key.B2BOrderCertificateKey"/>
<jsp:useBean id="b2BOrderTagKey" class="com.cwc.app.key.B2BOrderTagKey"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>运单日志跟进</title>

<!--  基本样式和javascript -->
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
 
<style type="text/css">
*{margin:0px;padding:0px;}
div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .title{ background: url("../js/popmenu/pro_title.jpg") no-repeat scroll left center transparent;color: #FFFFFF;display: table;font-size: 14px; height: 27px;padding-left: 5px; padding-top: 3px;width: 356px;line-height:27px;}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:80px;text-align:center;border:1px solid silver;margin:0px auto;width:300px;height:30px;background:white;line-height:30px;font-size:14px;}	
	#ui-datepicker-div{z-index:9999;display:none;}
</style>
<style type="text/css">
 	select.key{display:none;}
</style>
<%
 
	long b2b_oid = StringUtil.getLong(request,"b2b_oid");
	
	DBRow row = b2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);

	
	int declaration = row.get("declaration",0);
	int clearance = row.get("clearance",0);
	int stock_in_set =  row.get("stock_in_set",0); 
	int b2b_order_status = row.get("b2b_order_status",0);
	int tag = row.get("tag",B2BOrderTagKey.NOTAG);
	int certificate = row.get("certificate",B2BOrderCertificateKey.NOCERTIFICATE);
	int product_file = row.get("product_file",B2BOrderProductFileKey.NOPRODUCTFILE);
	 // 在页面上要分别根据all_over(运输流程) , declaration_over(报关完成时间)
	 String b2BOrderLogAddAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/b2b_order/B2BOrderAddLogAction.action";
	  
	TDate tdate = new TDate();
 %>
 <script type="text/javascript">
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
jQuery(function($){
	$('#eta').datepicker({
		dateFormat:"yy-mm-dd",
		changeMonth: true,
		changeYear: true,
	 
		onSelect:function(dateText, inst){

			setContentHtml(dateText);
			
		}
		
	});
	//changeType(true);
	changeTypeStatus(true);
})	
function setContentHtml(dateText){
	// 获取type ，和显示的select 的html 然后组成语句
	if(!dateText){
		dateText = $("#eta").val();
	}
	 
	var contentNode = $("#content");
	var contentNodeValue = contentNode.val();
	 
	var option = $("#type option:selected");
	var html ="["+ option.html() + "]流程";
	//var process = option.attr("target");
	//var processHtml = $.trim($("option:selected",$("#"+process)).html());
	var processHtml = $("#b2b_order_activity_name").text();
	html += "["+(processHtml)+"]阶段预计";
	html += dateText+"完成:";
	if(contentNodeValue.indexOf("完成:") != -1){
		var index = contentNodeValue.indexOf("完成:")
		html += contentNodeValue.substr(index +3);
		$("#content").val(html);
	}else{
		$("#content").val(html + contentNodeValue);
	}
}
function changeType(isFirst){
	var selected = $("#type option:selected");
 	var target = (selected.attr("target"));
 	if(target.length > 0 ){
		$(".key").css("display","none");
		// 如果在转运单中已经有了的状态，那么就要回显出来
		$("#"+target).css("display","inline-block");
		 
		 if(target === "b2b_order_status"){
			 $("#"+target+" option[value='<%=b2b_order_status %>']").attr("selected",true);
		 }
		 if(target === "stock_in_set"){
			 $("#"+target+" option[value='<%=stock_in_set %>']").attr("selected",true);
		 }
		 if(target === "b2BOrderClearanceKey"){
			 $("#"+target+" option[value='<%= clearance%>']").attr("selected",true);
			 
		 }
		 if(target === "b2BOrderDeclarationKey"){
			 $("#"+target+" option[value='<%= declaration%>']").attr("selected",true);
			 
		 }
		 if(target === "b2BOrderTagKey"){
			 $("#"+target+" option[value='<%= tag%>']").attr("selected",true);
		 }
		 if(target === "b2BOrderCertificateKey"){
			 $("#"+target+" option[value='<%= certificate%>']").attr("selected",true);
		 }
	 	$("#type_name").html("当前阶段:");
	 	if(!isFirst){setContentHtml()};
 	}
}
function changeTypeStatus(isFirst){
	var typeSelVal = $("#type").val();
	$("#type_name").html("当前阶段:");
	$("#b2b_order_activity_name").html("");
	if(<%=B2BOrderLogTypeKey.Goods%> == typeSelVal){//货物
		$("#b2b_order_activity_name").html("<%=b2BOrderKey.getB2BOrderKeyById(b2b_order_status)%>");
		$("#activity_id").val(<%=b2b_order_status%>);
	}
	if(!isFirst){setContentHtml()};
	
}
// b2b_oid ， b2b_order_content(跟进内容),eta(具体内容的完成时间),b2b_order_type 跟进类型主Key,stage
function submitForm(){
	if(!validateForm()){return ;}
	var subform =  "#submitForm";
	 $("input[name='b2b_type']",$(subform)).val($("#type").val());
	 $("input[name='eta']",$(subform)).val($("#eta").val());
		var target = $("#type option:selected").attr("target");
	 
	 $("input[name='stage']",$(subform)).val($("#activity_id").val());
	 $("input[name='b2b_content']",$(subform)).val($("#content").val());
	
	 
	$.ajax({
		url:'<%= b2BOrderLogAddAction%>',
		dataType:'json',
		data:$(subform).serialize(),
		beforeSend:function(request){
 			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
			$.unblockUI();
			if(data && data.flag === "success"){
				$.artDialog && $.artDialog.close();
				// 父页面调用go()方法;
				$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			}else{
				showMessage("系统错误,请稍后重试","error");
			}
			
		},
		error:function(){
			showMessage("系统错误,请稍后重试","error");
		}
	})
}
function validateForm(){
	if($.trim($("#content").val()).length < 1){
		showMessage("请输入跟进内容","alert");
		return false;
	}
	if($.trim($("#eta").val()).length < 1){
		showMessage("请输入阶段完成时间","alert");
		return false;
	}
	return true;
}
function cancel(){
	$.artDialog && $.artDialog.close();
}
</script>
</head>
<body>
	 <form id="submitForm">
	 	<input type="hidden" value="<%=b2b_oid %>" name="b2b_oid"/>
	 	<input type="hidden"  name="b2b_type"/>
	 	<input type="hidden"  name="eta" />
	 	<input type="hidden" name="stage" />
	 	<input type="hidden" name="b2b_content" />
	 	<input type="hidden" id="activity_id" name="activity_id"/>
	 </form>
	 
		<!-- 遮盖层 -->
  <div class="cssDivschedule">
 		<div class="innerDiv">
 			请稍等.......
 		</div>
 </div>
 <br/><br/>
 	<form id="dataForm">
 	
 		<table>
 			<tr>
 				 
 				<td style="width: 13%"> 跟进流程:</td>
 				<td style="width: 13%">
 					<select id='type' name='b2b_type' onchange="changeTypeStatus();">
 	  					 <%
 	  						 out.print("<option value='"+B2BOrderLogTypeKey.Goods+"' target='b2b_order_status'>货物状态</option>");
 	  					 
 	  					 %>
 	  				</select>
 	  			    </td>
 	  				<td style="width: 13%"><span id="type_name"></span></td>
 	  				<td style="width: 13%"><span id="b2b_order_activity_name"></span></td>
 
 	  				<td style="width: 23%">预计本阶段完成时间:</td>
 	  				 
 	  				<td style="width: 25%"><input type="text" id="eta" name="eta" value="<%= tdate.getYYMMDDOfNow() %>"/></td>
 			</tr>
 			 
 			<tr>
 				<td style="text-align:right;width: 13%">备注:</td>
 				<td colspan="5"><textarea style='width: 503px; height: 155px' id='content' name='followup_content'/></textarea></td>
 			</tr>
 		</table>
 	  	
 	  	</form>	
 	  		
	  		
	  		
 	  		 <div class="buttonDiv" style="margin-top:5px;"> 
	 	 	<input type="button" id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil"  onclick="submitForm()" value="提交"> 
			<button id="jqi_state0_button取消" value="n" class="jqidefaultbutton specl" name="jqi_state0_button取消" onclick="cancel();">取消</button>
	 	 </div>
	 
</body>
</html>