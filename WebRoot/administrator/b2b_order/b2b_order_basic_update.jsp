<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.B2BOrderKey"%>
<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@page import="java.util.List"%>
<%@ include file="../../include.jsp"%> 
<%
	DBRow treeRows[]	= catalogMgr.getProductStorageCatalogTree();
	String b2b_oid = StringUtil.getString(request, "b2b_oid");
	DBRow b2BOrderRow	= null,storageCatalog=null;
	DBRow[] shipTos = shipToMgrZJ.getAllShipTo();//仓库类型客户仓库的  二级仓库
	DBRow[] ships = null;
	if(!"".equals(b2b_oid)){
		b2BOrderRow	= b2BOrderMgrZyj.getDetailB2BOrderById(Long.parseLong(b2b_oid));
		
	}


	long countryId = 0L;//目的国家
	if(null != b2BOrderRow){
		countryId = b2BOrderRow.get("deliver_ccid",0L);
		
	}
	Long deliver_pro_id = 0L;
	String address_state_deliver = "";
	if(null != b2BOrderRow){
		deliver_pro_id = b2BOrderRow.get("deliver_pro_id",0L);
		address_state_deliver = b2BOrderRow.getString("address_state_deliver");
		storageCatalog=catalogMgr.getDetailProductStorageCatalogById(Long.valueOf(b2BOrderRow.getString("receive_psid")));
		if(storageCatalog.get("ship_to_id")!=null){
			ships=shipToMgrZJ.GetAllShipToCustomer(Long.valueOf(storageCatalog.getString("storage_type")), Long.valueOf(storageCatalog.getString("ship_to_id")));
		}
		
	}
	DBRow[] storageCatalogs = productStorageMgrZJ.getProductStorageCatalogsByTypeShipTo(Integer.valueOf(storageCatalog.getString("storage_type")).intValue(),0);////仓库类型不为客户仓库的  二级仓库
	Long sendCountryId = 0L;
	String address_state_send = "";
	if(null != b2BOrderRow){
		sendCountryId = b2BOrderRow.get("send_ccid",0L);
		address_state_send = b2BOrderRow.getString("address_state_send");
	}
	Long send_pro_id = 0L;
	if(null != b2BOrderRow){
		send_pro_id = b2BOrderRow.get("send_pro_id",0L);
	}
	int isOutter = StringUtil.getInt(request, "isOutter");
	String isSubmitSuccess = StringUtil.getString(request, "isSubmitSuccess");
	DBRow[] titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true, 0L, "", null, request);  //客户登录列出自己的title
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增转运单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 带搜索的下拉框 -->
    <link rel="stylesheet" href="chosen.css">
    <script type="text/javascript" src="chosen.jquery.min.js"></script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">

	jQuery(function($){
	//下拉框初始化
	  $(".chzn-select").chosen();
	  //不显二级仓库
		   if(<%=StorageTypeKey.CUSTOMER!=Integer.valueOf(storageCatalog.getString("storage_type"))%>){
	            $("#ship_to_customer_list_chzn").css("display","none");
	      }
	
			$('#b2b_order_out_date,#b2b_order_receive_date').datepicker({
				dateFormat:"yy-mm-dd",
				changeMonth: true,
				changeYear: true,
			});
			$("#ui-datepicker-div").css("display","none");
			if('0' != '<%=countryId %>'){
				getStorageProvinceByCcid('<%=countryId %>','<%=deliver_pro_id %>','deliver_pro_id', '<%=address_state_deliver%>' );
			}
			if('0' != '<%=sendCountryId %>'){
				getStorageProvinceByCcid('<%=sendCountryId %>','<%=send_pro_id %>','send_pro_id', '<%=address_state_send%>');
			}
			if('2' == '<%=isSubmitSuccess%>'){
				//cancel();
				$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			}
			if(1==$("#receive_psid option:selected").attr("stroageType"))
			{
				$("#b2BOrderArrrivalDateTr").css("display","");
			}
			else
			{
				$("#b2BOrderArrrivalDateTr").css("display","none");
			}
			if(1==$("#psid option:selected").attr("stroageType"))
			{
				$("#b2BOrderOutDateTr").css("display","");
			}
			else
			{
				$("#b2BOrderOutDateTr").css("display","none");
			}
			var stroage_type_receive = $("#receive_psid option:selected").attr("stroageType");
			$("#receive_ps_type").val(stroage_type_receive);

			var stroage_type_send = $("#psid option:selected").attr("stroageType");
			$("#send_ps_type").val(stroage_type_send);

			/*if('<%=StorageTypeKey.SELF%>'*1==stroage_type_receive*1)
			{
				$("#b2BOrderArrrivalDateTr").css("display","");
			}
			else
			{
				$("#b2BOrderArrrivalDateTr").css("display","none");
				$("#b2b_order_receive_date").val("");
			}
			if('<%=StorageTypeKey.SELF%>'*1==stroage_type_send*1)
			{
				$("#b2BOrderOutDateTr").css("display","");
			}
			else
			{
				$("#b2BOrderOutDateTr").css("display","none");
				$("#b2b_order_out_date").val("");
			}*/

			
	});
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	function stateDivSend(pro_id, pro_input)
	{
		if(pro_id==-1)
		{
			$("#state_div_send").css("display","inline");
			$("#address_state_send").val(pro_input);
		}
		else
		{
			$("#state_div_send").css("display","none");
			$("#address_state_send").val("");
		}
	}
	function stateDivDeliver(pro_id, pro_input)
	{
		if(pro_id==-1)
		{
			$("#state_div_deliver").css("display","inline");
			$("#address_state_deliver").val(pro_input);
		}
		else
		{
			$("#state_div_deliver").css("display","none");
			$("#address_state_deliver").val("");
		}
	}
	
	//国家地区切换
function getStorageProvinceByCcid(ccid,pro_id,id, pro_input)
{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#"+id).clearAll();
					$("#"+id).addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#"+id).addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#"+id).addOption("手工输入","-1");
					}
				});
				if (pro_id!=""&&pro_id!=0)
				{
					$("#"+id).setSelectedValue(pro_id);
					if(id=="deliver_pro_id")
					{
						stateDivDeliver(pro_id, pro_input);
					}
					else
					{
						stateDivSend(pro_id, pro_input);
					}
				}
					$("#"+id).chosen();
			   	$("#"+id).trigger("liszt:updated");	
}
	//当仓库类型为客户仓库，存在二级仓库，更改一级仓库，联动二级仓库
   function changeShipTo(obj) {
      var shipToId=obj;
     $.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminsitrator/shipTo/getAllShipToCustomerJson.action?typeId='+typeId+'&shipToId='+shipToId,
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			beforeSend:function(request){
			},
			error: function(){
			},
			
			success: function(data){
				if (data!="")
				{    $("#ship_to_customer_list").clearAll();
				     $("#ship_to_customer_list").addOption("请选择......",-1);
					$.each(data,function(i){
						$("#ship_to_customer_list").addOption(data[i].title,data[i].id);
					});
				}
				$("#send_house_number").val("");
				$("#send_street").val("");
				$("#send_zip_code").val("");
				$("#send_name").val("");
				$("#send_linkman_phone").val("");
				$("#send_city").val("");
				$("#send_ccid").val("");
				$("#send_pro_id").val("");
				$("#state_div_send").css("display","none");
				$("#address_state_send").val("");
				$("#ship_to_customer_list").chosen();
			    $("#ship_to_customer_list").trigger("liszt:updated");	
			}
		});
   
     
   }
    //改变客户的二级仓库，带出仓库属性
   function changeCutomerStorage(obj){
   var ps_id = obj.value;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{ps_id:ps_id},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败");
				},
				success: function(data){
				$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					//$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.deliver_city);
					$("#deliver_ccid").val(data["deliver_nation"]);
					
					getStorageProvinceByCcid($("#deliver_ccid").val(),data["deliver_pro_id"],"deliver_pro_id", data.deliver_pro_input);
					
					$("#deliver_name").val(data["deliver_contact"]);
					$("#deliver_linkman_phone").val(data["deliver_phone"]);
					$("#re_psid").val(ps_id);
					$("#deliver_ccid").trigger("liszt:updated");
				    $("#deliver_pro_id").trigger("liszt:updated");
				
				}
			});

   }

	function selectDeliveryStorage(obj)
	{
	
	
	  	if(typeId==<%=StorageTypeKey.CUSTOMER%>){
	  	changeShipTo(obj);
	  	
	  	}else{
		var ps_id = obj;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{ps_id:ps_id},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data){
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					//$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.deliver_city);
					$("#deliver_ccid").val(data["deliver_nation"]);
					
					getStorageProvinceByCcid($("#deliver_ccid").val(),data["deliver_pro_id"],"deliver_pro_id", data.deliver_pro_input);
					$("#re_psid").val(ps_id);
					$("#deliver_name").val(data["deliver_contact"]);
					$("#deliver_linkman_phone").val(data["deliver_phone"]);
				    $("#deliver_ccid").trigger("liszt:updated");
				    $("#deliver_pro_id").trigger("liszt:updated");
				}
			});
			}
		/*var stroage_type = $("option:selected", obj).attr("stroageType");
		if(1==stroage_type)
		{
			$("#b2BOrderArrrivalDateTr").css("display","");
		}
		else
		{
			$("#b2BOrderArrrivalDateTr").css("display","none");
		}*/
	}
	
	function selectSendStorage(obj)
	{
		var ps_id = obj.value;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					ps_id:ps_id
				},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data)
				{
					$("#send_house_number").val(data["send_house_number"]);
					$("#send_street").val(data["send_street"]);
					//$("#send_address3").val(data.deliver_address3);
					$("#send_zip_code").val(data.send_zip_code);
					$("#send_name").val(data.send_contact);
					$("#send_linkman_phone").val(data.send_phone);
					$("#send_city").val(data.send_city);
					$("#send_ccid").val(data["send_nation"]);
					
					getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id",data.send_pro_input);
				}
			});
		/*var stroage_type = $("option:selected", obj).attr("stroageType");
		if(1==stroage_type)
		{
			$("#b2BOrderOutDateTr").css("display","");
		}
		else
		{
			$("#b2BOrderOutDateTr").css("display","none");
		}*/
	}
	function checkSendAddress()
	{
		if($("#psid").val()!=0)
		{
			if($("#send_ccid").val()==0)
			{
				alert("请选择发货国家");
			}
			else if($("#send_pro_id").val()==0)
			{
				alert("请选择发货省份");
			}
			else if($("#send_pro_id").val()==-1 && ''==$("#address_state_send").val())
			{
				alert("请填写发货省份");
			}
			else if($("#send_city").val()=="")
			{
				alert("请填写发货城市");
				$("#send_city").focus();
			}
			else if($("#send_house_number").val()=="")
			{
				alert("请填写发货地址门牌号");
			}
			else if($("#send_street").val()=="")
			{
				alert("请填写发货地址街道");
			}
			else if($("#send_zip_code").val()=="")
			{
				alert("请填写发货地邮编");
				$("#send_zip_code").focus();
			}
			else if($("#send_name").val()=="")
			{
				alert("请填写发货联系人");
				$("#send_name").focus();
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	
	function submitCheck()
	{
		if(checkSendAddress())
		{
			if(-1 == $("#title_id").val())
			{
				alert("请选择title");
				tabSelect(2);
			}
			else
			{
				 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				document.add_form.submit();
			}
		} 
	}
	function closeWindow(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	}
	function changeSendStorageType(obj)
	{
		typeId = obj.value;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/GetStorageCatalogsByTypeAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:{typeId:typeId},
			beforeSend:function(request){
			},
			error: function(){
			},
			
			success: function(data)
			{
				$("#psid").clearAll();
				$("#psid").addOption("请选择","0");
				if (data!="")
				{
					$.each(data,function(i){
						$("#psid").addOption(data[i].title,data[i].id);
					});
				}
				$("#send_house_number").val("");
				$("#send_street").val("");
				$("#send_zip_code").val("");
				$("#send_name").val("");
				$("#send_linkman_phone").val("");
				$("#send_city").val("");
				$("#send_ccid").val("");
				$("#send_pro_id").val("");
				$("#state_div_send").css("display","none");
				$("#address_state_send").val("");
				//getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id");
			}
		});
		if('<%=StorageTypeKey.SELF%>'*1==typeId*1)
		{
			$("#b2BOrderOutDateTr").css("display","");
		}
		else
		{
			$("#b2BOrderOutDateTr").css("display","none");
			$("#b2b_order_out_date").val("");
		}
		
	}
	function fillShipTo()
{


	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminsitrator/shipTo/getAllShipToJson.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			beforeSend:function(request){
			},
			error: function(){
			},
			
			success: function(data)
			{
			
				if (data!="")
				{
				
	
				$("#receive_psid").clearAll();
					$.each(data,function(i){
						$("#receive_psid").addOption(data[i].ship_to_name,data[i].ship_to_id);
					});
				}
				$("#deliver_house_number").val("");
				$("#deliver_street").val("");
				$("#deliver_zip_code").val("");
				$("#deliver_name").val("");
				$("#deliver_linkman_phone").val("");
				$("#deliver_city").val("");
				$("#deliver_ccid").val("");
				$("#deliver_pro_id").val("");
				$("#state_div_deliver").css("display","none");
				$("#address_state_deliver").val("");
			    $("#receive_psid").chosen();
				$("#receive_psid").trigger("liszt:updated");
				$("#deliver_ccid").trigger("liszt:updated");
				$("#deliver_pro_id").trigger("liszt:updated");
			
				
			}
		});
}
var typeId=<%=storageCatalog.getString("storage_type")%>;
	function changeReceiveStorageType(obj,ship_to_id)
	{
		 typeId = obj;
		

     $("#ship_to_customer_list").clearAll();
     $("#ship_to_customer_list").chosen();
	$("#ship_to_customer_list").trigger("liszt:updated");
	//客户仓库

	if(typeId==<%=StorageTypeKey.CUSTOMER%>)
	{
         $("#ship_to_customer_list_chzn").css("display","");
		fillShipTo();
		//显示门店仓库
	
	}else{
           $("#ship_to_customer_list_chzn").css("display","none")
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>/action/adminsitrator/product_storage/getStorageCatalogJsonByTypeShipTo.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:{storage_type:typeId,
			ship_to_id:ship_to_id},
			beforeSend:function(request){
			},
			error: function(){
			},
			
			success: function(data)
			{
		
				$("#receive_psid").clearAll();
				$("#receive_psid").addOption("请选择","0");
			
				if (data!="")
				{
					$.each(data,function(i){
						$("#receive_psid").addOption(data[i].title,data[i].id);
					});
				}
				$("#deliver_house_number").val("");
				$("#deliver_street").val("");
				$("#deliver_zip_code").val("");
				$("#deliver_name").val("");
				$("#deliver_linkman_phone").val("");
				$("#deliver_city").val("");
				$("#deliver_ccid").val("");
				$("#deliver_pro_id").val("");
				$("#state_div_deliver").css("display","none");
				$("#address_state_deliver").val("");
			    $("#receive_psid").chosen();
				$("#receive_psid").trigger("liszt:updated");
				$("#deliver_ccid").trigger("liszt:updated");
				$("#deliver_pro_id").trigger("liszt:updated");	
			}
		});
		}
		
		if('<%=StorageTypeKey.SELF%>'*1==typeId*1)
		{
			$("#b2BOrderArrrivalDateTr").css("display","");
		}
		else
		{
			$("#b2BOrderArrrivalDateTr").css("display","none");
			$("#b2b_order_receive_date").val("");
		}
		
	}
	function changeProIdDeliver(obj)
	{
		stateDivDeliver(obj.value);
	}
	function changeProIdSend(obj)
	{
		stateDivSend(obj.value);
	}
 
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
<form name="add_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/B2BOrderUpdateBasicAction.action">
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_basic_update.html"/>
	<input type="hidden" name="b2b_oid" value='<%=b2b_oid %>'/>
	<input type="hidden" name="isOutter" value="<%=isOutter%>"/>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="left" valign="top"><br>
            	  <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;"> 
						请选择提货仓库与接收仓库 
						</td>
					  </tr>
				  </table>
				  
				  <fieldset class="set" style="padding-bottom:4px;text-align: left;">
	  				<legend>
						<span style="" class="title"> 
							接收地址	 
						</span>
					</legend>
						 <table width="95%" align="center" cellpadding="2" cellspacing="0">
						 	<tr valign="bottom">
									<td width="25%" align="right">TITLE</td>
									<td width="75%">
										<select name="title_id" id="title_id" class="chzn-select" style="width: 150px;">
											<option value="-1">请选择</option>
											<%
												for(int i = 0; i < titles.length; i ++)
												{
													String isSeled = "";
													if(null!=b2BOrderRow && titles[i].get("title_id", 0L) == b2BOrderRow.get("title_id", 0L))
													{
														isSeled = "selected = 'selected'";
													}
											%>
												<option value='<%=titles[i].get("title_id", 0L) %>' <%=isSeled %>><%=titles[i].getString("title_name") %></option>
											<%
												}
											%>
										</select>
									</td>
								</tr>
						 	<tr valign="bottom">
									  <td align="right" nowrap="nowrap">目的仓库</td>
								      <td nowrap="nowrap">
								      <select id="receive_ps_type" onchange="changeReceiveStorageType(this.value,0)" class="chzn-select" style="width: 120px;" >
						 					<option value="-1">请选择</option>
								      		<%
									      		StorageTypeKey storageTypeKey = new StorageTypeKey();
								      			List<String> storageTypes = storageTypeKey.getStorageTypeKeys();
								      			for(int i = 0; i < storageTypes.size(); i ++)
								      			{
								      				String select="";
								      				if(storageTypes.get(i).equals(storageCatalog.getString("storage_type"))){
								      					select="selected";
								      				}
								      				
								      		%>
								      			<option value='<%=storageTypes.get(i) %>'  <%=select %> ><%=storageTypeKey.getStorageTypeKeyName(storageTypes.get(i)) %></option>
								      		<%		
								      			}
								      		%>
								      	</select>
								      	<input type="hidden" id="re_psid" name="re_psid" value="<%=b2BOrderRow.get("receive_psid").toString()%>">
								      	<select name='receive_psid' id='receive_psid' onchange="selectDeliveryStorage(this.value)" style="width: 150px;" class="chzn-select">
								      	
		                                <option value="0">请选择...</option >
		                                <%
		                                 String display="";
		                                 if(StorageTypeKey.CUSTOMER==Integer.valueOf(storageCatalog.getString("storage_type"))){//客户仓库
		                                	 display="block"; 
		                                	 String select="";
		                                		 
		                                	 
		                                	 for(DBRow ship: shipTos){
		                                		 if(ship.get("ship_to_id").toString().equals(storageCatalog.get("ship_to_id").toString())){
		                                				select="selected";
			                                	 }	
		                                	 
		                                	 %>
		                                	 
		                                		 <option value='<%=ship.get("ship_to_id") %>'  <%=select %> ><%=ship.get("ship_to_name") %></option>
		                                		 
		                                	<%}
		                                	 
		                                 }else{//不是客户仓库
		                                	 display="none";
		                                     String select="";
		                                	 for(int i=0;i<storageCatalogs.length;i++){
		                                		 DBRow catalog=storageCatalogs[i];
		                                	
		                                		 if(catalog.get("id").toString().equals(b2BOrderRow.get("receive_psid").toString())){
		                                			 select="selected"; 
		                                		 }
		                                		 %>
			                                	 
		                                		 <option value='<%=catalog.get("id") %>'  <%=select %> ><%=catalog.get("title") %></option>
		                                		 
		                                	<%
		                                		 
		                                	 }
		                                	 
		                                 }
		                                
		                                
		                                
		                                %>
		                      
		                              </select>
		                                    <!-- 仓库类型为客户仓库的二级仓库 -->
					                          	<select  id="ship_to_customer_list" style="display:none;width: 200px;" name="ship_to_customer_list"  class="chzn-select"   onchange="changeCutomerStorage(this)">
					                          	    <option value="-1" >请选择</option>	
					                          	    <%
					                          	    if(ships!=null){
					                          	         String select="";
					                          	 
					                          	       for(DBRow shipsss:ships){
					                          	     	 if(shipsss.get("id").toString().equals(b2BOrderRow.get("receive_psid").toString())){
				                                			 select="selected"; 
				                                		 }
					                          	    	   %>
					                          	              <option value='<%=shipsss.getString("id") %>' <%=select %>><%=shipsss.getString("title") %></option>  
					                          	    	  
					                          	       <% }
					                          	       }%>
					                          	</select>
								      </td>
									</tr>
						 	<tr>
						 		<td align="right">目的国家</td>
						 		<td>
						 			<%
									DBRow countrycode[] = orderMgr.getAllCountryCode();
									String selectBg="#ffffff";
									String preLetter="";
									%>
							      <select name="deliver_ccid" id="deliver_ccid" onChange="getStorageProvinceByCcid(this.value,0,'deliver_pro_id');" class="chzn-select">
								  <option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++)
								  {
								 	
									preLetter = countrycode[i].getString("c_country").substring(0,1);
									if(countrycode[i].getString("ccid").equals(countryId+"")){
								 %>
								    <option   value="<%=countrycode[i].getString("ccid")%>" selected="selected"><%=countrycode[i].getString("c_country")%></option>
								 <%		
									}else{
								 %>
								    <option   value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								 <%		
									}
								  }
								  %>
							      </select>		
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">目的省份</td>
						 		<td>
						 			<select name="deliver_pro_id" id="deliver_pro_id" onchange="changeProIdDeliver(this)" class="chzn-select" style="width: 200px;">
								    </select>		
								      <div style="padding-top: 10px;display:none;" id="state_div_deliver">
											<input type="text" name="address_state_deliver" id="address_state_deliver" value='<%=null!=b2BOrderRow?b2BOrderRow.getString("address_state_deliver"):"" %>'/>
									  </div>
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址门牌号</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_house_number" id="deliver_house_number" value='<%=null!=b2BOrderRow?b2BOrderRow.getString("deliver_house_number"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址街道</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_street" id="deliver_street" value='<%=null!=b2BOrderRow?b2BOrderRow.getString("deliver_street"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">目的城市</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_city" id="deliver_city" value='<%=null!=b2BOrderRow?b2BOrderRow.getString("deliver_city"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址邮编</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_zip_code" id="deliver_zip_code" value='<%=null!=b2BOrderRow?b2BOrderRow.getString("deliver_zip_code"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货联系人</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_name" id="deliver_name" value='<%=null!=b2BOrderRow?b2BOrderRow.getString("b2b_order_linkman"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货联系人电话</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_linkman_phone" id="deliver_linkman_phone" value='<%=null!=b2BOrderRow?b2BOrderRow.getString("b2b_order_linkman_phone"):"" %>'/></td>
						 	</tr>
						 	<tr id="b2BOrderArrrivalDateTr">
						 		<td align="right">ETA</td>
						 		<td><input width="80%" type="text" name="b2b_order_receive_date" id="b2b_order_receive_date" value='<%=!"".equals(b2BOrderRow.getString("b2b_order_receive_date"))?new TDate(b2BOrderRow.getString("b2b_order_receive_date")).formatDate("YYYY-MM-dd"):""%>'></td>
						 	</tr>
						 </table>
						</fieldset>
						<br/>
						<%
							if(b2BOrderRow.get("b2b_order_status",0)==B2BOrderKey.READY)
							{
				  		%>
					
						<%
							}
						%>
						<fieldset class="set" style="padding-bottom:4px;text-align: left;">
			  				<legend>
								<span style="" class="title">
									其他信息
								</span>
							</legend>
							<table width="95%" align="center" cellpadding="2" cellspacing="0">
							 	<tr>
							 		<td width="25%" align="right">备注</td>
							 		<td width="75%">
							 		<textarea rows="2" cols="70" name="remark" id="remark"><%=null!=b2BOrderRow?b2BOrderRow.getString("remark"):"" %></textarea>
							 		</td>
							 	</tr>
							 
						 	</table>
						</fieldset>
				</td>
			 </tr>
             <tr>
                <td  align="right" valign="middle" class="win-bottom-line">	
                 <%
			    	if(2 != isOutter){
			    %>
			    	<input type="button" name="Submit2" value="下一步" class="normal-green" onclick="submitCheck()">
			    <%
			    	}else{
			    %>		
			    	<input type="button" name="Submit2" value="完成" class="normal-green" onclick="submitCheck()">
			    <%
			    	}
			    %>			
			    	<input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWindow();"></td>
             </tr>
            </table>
      </form>
</body>
</html>
