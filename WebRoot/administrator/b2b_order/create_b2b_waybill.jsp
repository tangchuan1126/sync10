<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.B2BOrderLogTypeKey"%>
<%@page import="com.cwc.app.key.B2BOrderStatusKey"%>
<%@page import="com.cwc.app.key.B2BOrderConfigChangeKey"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.ClearanceKey"%>
<%@page import="com.cwc.app.key.DeclarationKey"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="com.cwc.app.key.QualityInspectionKey"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/> 
<jsp:useBean id="transportCertificateKey" class="com.cwc.app.key.TransportCertificateKey"/> 
<jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey"/> 
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<jsp:useBean id="transportQualityInspectionKeyKey" class="com.cwc.app.key.TransportQualityInspectionKey"/> 
<%
	long b2b_oid = StringUtil.getLong(request,"b2b_oid");
	DBRow b2bOrder = b2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
	long title_id = b2bOrder.get("title_id",0l);
	
	String selectBg="#ffffff";
	String preLetter="";
		
	String adminUserIdsB2BOrder		= "";
	String adminUserNamesB2BOrder		= "";
	String adminUserIdsDeclaration		= "";
	String adminUserNamesDeclaration	= "";
	String adminUserIdsClearance		= "";
	String adminUserNamesClearance		= "";
	String adminUserIdsProductFile		= "";
	String adminUserNamesProductFile	= "";
	String adminUserIdsTag				= "";
	String adminUserNamesTag			= "";
	String adminUserIdsStockInSet		= "";	
	String adminUserNamesStockInSet		= "";
	String adminUserIdsCertificate		= "";
	String adminUserNamesCertificate	= "";
	String adminUserIdsQualityInspection = ""; 
	String adminUserNamesQualityInspection	= "";
	String adminUserIdsTagThird			= "";
	String adminUserNamesTagThird		= "";


	//查询各流程任务的负责人
	adminUserIdsB2BOrder		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Goods);
	adminUserNamesB2BOrder		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Goods);
	adminUserIdsDeclaration		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ExportCustoms);
	adminUserNamesDeclaration	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ExportCustoms);
	adminUserIdsClearance		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ImportCustoms);
	adminUserNamesClearance		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ImportCustoms);
	adminUserIdsProductFile		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ProductShot);
	adminUserNamesProductFile	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ProductShot);
	adminUserIdsTag				= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Label);
	adminUserNamesTag			= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Label);
	adminUserIdsStockInSet		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Freight);
	adminUserNamesStockInSet	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Freight);
	adminUserIdsCertificate		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Document);
	adminUserNamesCertificate	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Document);
	adminUserIdsQualityInspection	= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.QualityControl);
	adminUserNamesQualityInspection	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.QualityControl);
	adminUserIdsTagThird		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.THIRD_TAG);
	adminUserNamesTagThird		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(b2b_oid), ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.THIRD_TAG);
	
	int delieverCcid = b2bOrder.get("deliver_ccid",0);
	int sendCcid	 = b2bOrder.get("send_ccid",0);
	boolean ccidIsSame = delieverCcid==sendCcid?true:false;
	String declaration = b2bOrder.getValue("declaration")==null?"0":b2bOrder.getValue("declaration").toString();
	String clearance = b2bOrder.getValue("clearance")==null?"0":b2bOrder.getValue("clearance").toString();
	String drawback = b2bOrder.getValue("drawback")==null?"1":b2bOrder.getValue("drawback").toString();
	String invoice = b2bOrder.getValue("invoice")==null?"1":b2bOrder.getValue("invoice").toString();
	int product_file = b2bOrder.get("product_file",0);
	int tag = b2bOrder.get("tag",0);
	int tag_third = b2bOrder.get("tag_third",0);
	int stock_in_set = b2bOrder.get("stock_in_set",0);
	int certificate = b2bOrder.get("certificate",0);
	int quality_inspection = b2bOrder.get("quality_inspection",0);
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增转运单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<script type="text/javascript" src="/Sync10-ui/pages/b2b_order/b2b_order_allocate_page.js"/>

<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<!-- dialog -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	
	//检查流程
	function checkProcedures()
	{
		document.add_form.declaration.value = $("input:radio[name=radio_declaration]:checked").val();
		document.add_form.clearance.value = $("input:radio[name=radio_clearance]:checked").val();
		document.add_form.tag.value = $("input:radio[name=radio_tag]:checked").val();
		document.add_form.tag_third.value = $("input:radio[name=radio_tagThird]:checked").val();
		document.add_form.stock_in_set.value = $("input:radio[name=stock_in_set_radio]:checked").val();
		document.add_form.certificate.value = $("input:radio[name=radio_certificate]:checked").val();
		document.add_form.quality_inspection.value = $("input:radio[name=radio_qualityInspection]:checked").val();

		if($("input:checkbox[name=isMailTransport]").attr("checked")){
			document.add_form.needMailTransport.value = 2;
		}
		if($("input:checkbox[name=isMessageTransport]").attr("checked")){
			document.add_form.needMessageTransport.value = 2;
		}
		if($("input:checkbox[name=isPageTransport]").attr("checked")){
			document.add_form.needPageTransport.value = 2;
		}
		
		if($("input:checkbox[name=isMailDeclaration]").attr("checked")){
			document.add_form.needMailDeclaration.value = 2;
		}
		if($("input:checkbox[name=isMessageDeclaration]").attr("checked")){
			document.add_form.needMessageDeclaration.value = 2;
		}
		if($("input:checkbox[name=isPageDeclaration]").attr("checked")){
			document.add_form.needPageDeclaration.value = 2;
		}

		if($("input:checkbox[name=isMailClearance]").attr("checked")){
			document.add_form.needMailClearance.value = 2;
		}
		if($("input:checkbox[name=isMessageClearance]").attr("checked")){
			document.add_form.needMessageClearance.value = 2;
		}
		if($("input:checkbox[name=isPageClearance]").attr("checked")){
			document.add_form.needPageClearance.value = 2;
		}

		if($("input:checkbox[name=isMailProductFile]").attr("checked")){
			document.add_form.needMailProductFile.value = 2;
		}
		if($("input:checkbox[name=isMessageProductFile]").attr("checked")){
			document.add_form.needMessageProductFile.value = 2;
		}
		if($("input:checkbox[name=isPageProductFile]").attr("checked")){
			document.add_form.needPageProductFile.value = 2;
		}

		if($("input:checkbox[name=isMailTag]").attr("checked")){
			document.add_form.needMailTag.value = 2;
		}
		if($("input:checkbox[name=isMessageTag]").attr("checked")){
			document.add_form.needMessageTag.value = 2;
		}
		if($("input:checkbox[name=isPageTag]").attr("checked")){
			document.add_form.needPageTag.value = 2;
		}

		if($("input:checkbox[name=isMailTagThird]").attr("checked")){
			document.add_form.needMailTagThird.value = 2;
		}
		if($("input:checkbox[name=isMessageTagThird]").attr("checked")){
			document.add_form.needMessageTagThird.value = 2;
		}
		if($("input:checkbox[name=isPageTagThird]").attr("checked")){
			document.add_form.needPageTagThird.value = 2;
		}

		if($("input:checkbox[name=isMailStockInSet]").attr("checked")){
			document.add_form.needMailStockInSet.value = 2;
		}
		if($("input:checkbox[name=isMessageStockInSet]").attr("checked")){
			document.add_form.needMessageStockInSet.value = 2;
		}
		if($("input:checkbox[name=isPageStockInSet]").attr("checked")){
			document.add_form.needPageStockInSet.value = 2;
		}

		if($("input:checkbox[name=isMailCertificate]").attr("checked")){
			document.add_form.needMailCertificate.value = 2;
		}
		if($("input:checkbox[name=isMessageCertificate]").attr("checked")){
			document.add_form.needMessageCertificate.value = 2;
		}
		if($("input:checkbox[name=isPageCertificate]").attr("checked")){
			document.add_form.needPageCertificate.value = 2;
		}

		if($("input:checkbox[name=isMailQualityInspection]").attr("checked")){
			document.add_form.needMailQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isMessageQualityInspection]").attr("checked")){
			document.add_form.needMessageQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isPageQualityInspection]").attr("checked")){
			document.add_form.needPageQualityInspection.value = 2;
		}
		if($("#adminUserNamesB2BOrder").val() == '')
		{
			alert("运输负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_declaration]:checked") && $("input:radio[name=radio_declaration]:checked").val() && '<%=DeclarationKey.DELARATION %>' == $("input:radio[name=radio_declaration]:checked").val() && $("#adminUserNamesDeclaration").val() == '')
		{
			alert("出口报关负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_clearance]:checked") && $("input:radio[name=radio_clearance]:checked").val() && '<%=ClearanceKey.CLEARANCE%>' == $("input:radio[name=radio_clearance]:checked").val() && $("#adminUserNamesClearance").val() == '')
		{
			alert("进口清关负责人不能为空");
			return false;
		}
		else if($("#adminUserNamesProductFile").val() == '')
		{
			alert("实物图片负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_tag]:checked") && $("input:radio[name=radio_tag]:checked").val() && '<%=TransportTagKey.TAG %>' == $("input:radio[name=radio_tag]:checked").val() && $("#adminUserNamesTag").val() == '')
		{
			alert("内部标签负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_tagThird]:checked") && $("input:radio[name=radio_tagThird]:checked").val() && '<%=TransportTagKey.TAG %>' == $("input:radio[name=radio_tagThird]:checked").val() && $("#adminUserNamesTagThird").val() == '')
		{
			alert("第三方标签负责人不能为空");
			return false;
		}
		else if($("input:radio[name=stock_in_set_radio]:checked") && $("input:radio[name=stock_in_set_radio]:checked").val() && '<%=TransportStockInSetKey.SHIPPINGFEE_SET %>' == $("input:radio[name=stock_in_set_radio]:checked").val() && $("#adminUserNamesStockInSet").val() == '')
		{
			alert("运费流程负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_certificate]:checked") && $("input:radio[name=radio_certificate]:checked").val() && '<%=TransportCertificateKey.CERTIFICATE %>' == $("input:radio[name=radio_certificate]:checked").val() && $("#adminUserNamesCertificate").val() == '')
		{
			alert("单证流程负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_qualityInspection]:checked") && $("input:radio[name=radio_qualityInspection]:checked").val() && '<%=TransportQualityInspectionKey.NEED_QUALITY %>' == $("input:radio[name=radio_qualityInspection]:checked").val() && $("#adminUserNamesQualityInspection").val() == '')
		{
			alert("质检流程负责人不能为空");
			return false;
		}
	}
	
	$(document).ready(function()
	{
		$("input:radio[name^=radio_]").click(
				function(){
					var hasThisCheck = $(this).val();
					var hasThisName = $(this).attr("name");
					var presonTrName = hasThisName.split("_")[1]+"PersonTr";
					if(2 == hasThisCheck){
						$("#"+presonTrName).attr("style","");
					}else{
						$("#"+presonTrName).attr("style","display:none");
					}
				}
		);
		$("input:radio[name=stock_in_set_radio]").click(
				function(){
					var hasThisCheck = $(this).val();
					if(2 == hasThisCheck){
						$("#stockInSetPersonTr").attr("style","");
					}else{
						$("#stockInSetPersonTr").attr("style","display:none");
					}
				}
		);		
	});

	function adminUserTransport(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsB2BOrder").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTransport'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTransport(user_ids , user_names){
		$("#adminUserIdsB2BOrder").val(user_ids);
		$("#adminUserNamesB2BOrder").val(user_names);
	}
	function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);};
	function adminUserDeclaration(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsDeclaration").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowDeclaration'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowDeclaration(user_ids , user_names){
		$("#adminUserIdsDeclaration").val(user_ids);
		$("#adminUserNamesDeclaration").val(user_names);
	}

	function adminUserClearance(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsClearance").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowClearance'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowClearance(user_ids , user_names){
		$("#adminUserIdsClearance").val(user_ids);
		$("#adminUserNamesClearance").val(user_names);
	}

	function adminUserProductFile(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsProductFile").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowProductFile'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowProductFile(user_ids , user_names){
		$("#adminUserIdsProductFile").val(user_ids);
		$("#adminUserNamesProductFile").val(user_names);
	}

	function adminUserTag(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsTag").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTag'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTag(user_ids , user_names){
		$("#adminUserIdsTag").val(user_ids);
		$("#adminUserNamesTag").val(user_names);
	}
	function adminUserTagThird(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsTagThird").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTagThird'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTagThird(user_ids , user_names){
		$("#adminUserIdsTagThird").val(user_ids);
		$("#adminUserNamesTagThird").val(user_names);
	}
	function adminUserStockInSet(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsStockInSet").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowStockInSet'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowStockInSet(user_ids , user_names){
		$("#adminUserIdsStockInSet").val(user_ids);
		$("#adminUserNamesStockInSet").val(user_names);
	}

	function adminUserCertificate(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsCertificate").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowCertificate'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowCertificate(user_ids , user_names){
		$("#adminUserIdsCertificate").val(user_ids);
		$("#adminUserNamesCertificate").val(user_names);
	}

	function adminUserQualityInspection(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsQualityInspection").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowQualityInspection'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowQualityInspection(user_ids , user_names){
		$("#adminUserIdsQualityInspection").val(user_ids);
		$("#adminUserNamesQualityInspection").val(user_names);
	};
	
	function openFreightSelect() 
	{
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_resources/freight_resources_select.html";
		freightSelectWindow = $.artDialog.open(url, {title: '运输资源选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function setFreightCost(fc_id,fc_project_name,fc_way,fc_unit) {
		addFreigthCost(fc_id,fc_project_name,fc_way,fc_unit);
		$('#changed').val('2');
	}
	
	function setRate(obj) {
		if(obj.value == 'RMB') {
			document.getElementsByName("tfc_exchange_rate")[$(obj).attr('index')].value = "1.0";
		}else {
			document.getElementsByName("tfc_exchange_rate")[$(obj).attr('index')].value = "0.0";
		}
	}
	
	function addFreigthCost(fc_id,fc_project_name,fc_way,fc_unit) 
	{
		var index = document.getElementById("freight_cost").rows.length;
    	var row = document.getElementById("freight_cost").insertRow(index);
    	row.insertCell(0).innerHTML = "<input type='hidden' name='tfc_id' id='tfc_id'>"
							    	 +"<input type='hidden' name='tfc_project_name' id='tfc_project_name' value='"+fc_project_name+"'>"
							    	 +"<input type='hidden' name='tfc_way' id='tfc_way' value='"+fc_way+"'>"
							    	 +"<input type='hidden' name='tfc_unit' id='tfc_unit' value='"+fc_unit+"'>"
							    	 +fc_project_name;
		
		row.insertCell(1).innerHTML = fc_way;
		row.insertCell(2).innerHTML = fc_unit;
		row.insertCell(3).innerHTML = "<input type='text' size='8' name='tfc_unit_price' id='tfc_unit_price' value='0.0'>";
		row.insertCell(4).innerHTML = "<select id='tfc_currency' index='"+(index-2)+"' name='tfc_currency' onchange='setRate(this)'>"+createCurrency(fc_unit)+"</select>";
		row.insertCell(5).innerHTML = "<input type='text' size='8' name='tfc_exchange_rate' id='tfc_exchange_rate' value='1.0'>";
		row.insertCell(6).innerHTML = "<input type='text' size='8' name='tfc_unit_count' id='tfc_unit_count' value='0.0'>";
		row.insertCell(7).innerHTML = "<input type='button' value='删除' onclick='deleteFreigthCost(this)'/>";
		//让回来的币种都默认选中
	}
	function createCurrency(fc_unit){
		var array = ['RMB','USD','HKD'];
		var options = "" ;
	 	for(var index = 0 , count = array.length  ; index < count ; index++ ){
			var selected = "" ;
		 	if(fc_unit.toUpperCase() === array[index]){
		 	   selected = "selected";
			}
			options += "<option "+selected+" value='"+array[index]+"'>"+array[index]+"</option>";
		}
		return  options;
    }

    function deleteFreigthCost(input){  
        var s=input.parentNode.parentNode.rowIndex;
        document.getElementById("freight_cost").deleteRow(s); 
        $('#changed').val('2');
        //var num=document.getElementById("tables").rows.length;  
	}

    function selectFreigthCost() {
    	if($("input:radio[name=stock_in_set_radio]:checked").val()==2)
		{
    		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_cost/freight_cost_select.html";
    		$.artDialog.open(url, {title: '运费项目选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
		}
		else
		{
			alert("请确认在流程指派中是否选择了需要运费");
		} 
	}
	
	function uploadFile(_target)
	{
	    var obj  = {
		     reg:"xls",
		     limitSize:2,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		
		$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
				 //close:function()
				 //{
						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 //	 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
				 //}
			 });
	}
	
	function prefillDetailByFile(file_name)
	{
		$.ajax({
			url: 'transport_detail_upload_show.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{file_name:file_name},
			
			beforeSend:function(request){
			},
			
			error: function(){
			},
			
			success: function(html)
			{
				$("#details").html(html);
				
				addAutoComplete($("#p_name"),
					"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
					"merge_info",
					"p_name");
			}
		});
	}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target)
{
	if('' != fileNames)
	{
  	  prefillDetailByFile(fileNames);
	}
}

function setFreight(fr_id,waybill_name,carriers,transportby,transportby_name, begin_country, begin_country_name, end_country, end_country_name, begin_port, end_port) 
{
	$("#fr_id").val(fr_id);
	$("#transport_waybill_name").val(waybill_name);
	$("#carriers").val(carriers);	
	$("#transportby").val(transportby);
	$("#transportby_name").val(transportby_name);
	$("#transport_send_country").val(begin_country);
	$("#transport_send_country_name").val(begin_country_name);
	$("#transport_receive_country").val(end_country);
	$("#transport_receive_country_name").val(end_country_name);
	$("#transport_send_place").val(begin_port);
	$("#transport_receive_place").val(end_port);
}

function loadWaybill()
{
	$.ajax({
		url: 'b2b_order_receive_allocate.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:{
			b2b_oid:<%=b2b_oid%>
		},
		async:false,
		
		
		beforeSend:function(request){
		},
		
		error: function(respon)
		{
			alert(respon);
		},
		
		success: function(html){
			
			$("#create_b2b_waybill").html(html);
			Sealingcalculation._int();
		}
	});
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive();">
<form name="add_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/B2BOrderAddTransportAction.action">
<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_order_detail.html"/>
<input type="hidden" name="b2b_oid" value='<%=b2b_oid %>'>
<!-- 各流程 -->
<input type="hidden" name="declaration"/>
<input type="hidden" name="clearance"/>
<input type="hidden" name="product_file"/>
<input type="hidden" name="tag"/>
<input type="hidden" name="tag_third"/>
<input type="hidden" name="stock_in_set"/>
<input type="hidden" name="certificate"/>
<input type="hidden" name="quality_inspection"/>

<!-- 负责人的Ids -->
<input type="hidden" name="adminUserIdsB2BOrder" id="adminUserIdsB2BOrder" value='<%=adminUserIdsB2BOrder %>'/>
<input type="hidden" name="adminUserIdsDeclaration" id="adminUserIdsDeclaration" value='<%=adminUserIdsDeclaration %>'/>
<input type="hidden" name="adminUserIdsClearance" id="adminUserIdsClearance" value='<%=adminUserIdsClearance %>'/>
<input type='hidden' name="adminUserIdsProductFile" id="adminUserIdsProductFile" value='<%=adminUserIdsProductFile %>'/>
<input type="hidden" name="adminUserIdsTag" id="adminUserIdsTag" value='<%=adminUserIdsTag %>'/>
<input type="hidden" name="adminUserIdsTagThird" id="adminUserIdsTagThird" value='<%=adminUserIdsTagThird %>'/>
<input type="hidden" name="adminUserIdsStockInSet" id="adminUserIdsStockInSet" value='<%=adminUserIdsStockInSet %>'/>
<input type="hidden" name="adminUserIdsCertificate" id="adminUserIdsCertificate" value='<%=adminUserIdsCertificate %>'/>
<input type="hidden" name="adminUserIdsQualityInspection" id="adminUserIdsQualityInspection" value='<%=adminUserIdsQualityInspection %>'/>


<!-- 各流程，是否发各类通知 -->
<input type="hidden" name="needMailTransport"/>
<input type="hidden" name="needMessageTransport"/>
<input type="hidden" name="needPageTransport"/>

<input type="hidden" name="needMailDeclaration"/>
<input type="hidden" name="needMessageDeclaration"/>
<input type="hidden" name="needPageDeclaration"/>

<input type="hidden" name="needMailClearance"/>
<input type="hidden" name="needMessageClearance"/>
<input type="hidden" name="needPageClearance"/>

<input type="hidden" name="needMailProductFile"/>
<input type="hidden" name="needMessageProductFile"/>
<input type="hidden" name="needPageProductFile"/>

<input type="hidden" name="needMailTag"/>
<input type="hidden" name="needMessageTag"/>
<input type="hidden" name="needPageTag"/>

<input type="hidden" name="needMailTagThird"/>
<input type="hidden" name="needMessageTagThird"/>
<input type="hidden" name="needPageTagThird"/>

<input type="hidden" name="needMailStockInSet"/>
<input type="hidden" name="needMessageStockInSet"/>
<input type="hidden" name="needPageStockInSet"/>

<input type="hidden" name="needMailCertificate"/>
<input type="hidden" name="needMessageCertificate"/>
<input type="hidden" name="needPageCertificate"/>

<input type="hidden" name="needMailQualityInspection"/>
<input type="hidden" name="needMessageQualityInspection"/>
<input type="hidden" name="needPageQualityInspection"/>

<!-- 运费项 -->
<input type="hidden" name="changed" id="changed" value="1">
	
	<div class="demo" align="center">
	  	 <div id="tabs" style="width: 98%">
	  	    <ul>
			  <li><a href="#procedures">流程指派</a></li>
			  <li><a href="#freight">运输设置</a></li>
			  <li><a href="#freightcost">运费设置</a></li>
			  <li><a href="#receive_allocate">保留库存</a></li>
			</ul>
		<div id="procedures">
			<table width="100%" border="0" cellspacing="5" cellpadding="0">
				<tr height="25px" id="transportPersonTr">
						<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">运输负责人：</td>
					    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
					    	<input type="text" id="adminUserNamesB2BOrder" name="adminUserNamesB2BOrder" style="width:180px;" onclick="adminUserTransport()" value='<%=adminUserNamesB2BOrder %>'/>
					    	通知：
					   		<input type="checkbox" name="isMailTransport" checked="checked"/>邮件
					   		<input type="checkbox" name="isMessageTransport" checked="checked"/>短信
					   		<input type="checkbox" name="isPageTransport" checked="checked"/>页面
						</td>
				</tr>
				<tr height="25px" id="transportPersonTr">
						<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">注：</td>
					    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
					    	请选择产品经理或海运负责人
						</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
		          <td width="13%" align="right" class="STYLE2">实物图片:</td>
		          <td width="87%">
		          	 	<input type="text" id="adminUserNamesProductFile" name="adminUserNamesProductFile" style="width:180px;" onclick="adminUserProductFile()" value='<%=adminUserNamesProductFile %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailProductFile" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageProductFile" checked="checked"/>短信
				   		<input type="checkbox" name="isPageProductFile" checked="checked"/>页面
		          </td>
		        </tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
	        	  <td width="13%" align="right" class="STYLE2">内部标签:</td>
		          <td width="87%" align="left" valign="middle" id="transportTr">
		          	<input type="radio" name="radio_tag" value="1" <%if(1 == tag){out.print("checked='checked'");}%>/> <%= transportTagKey.getTransportTagById(1) %>
    	          	<input type="radio" name="radio_tag" value="2" <%if(0 == tag || 2 == tag){out.print("checked='checked'");}%>/> <%= transportTagKey.getTransportTagById(2) %>
		          </td>
		        </tr>
		         <%
			 	String tagPersonTrStyle = "display:none";
		   	 	if(1 != tag){
		   	 		tagPersonTrStyle = "";
		    	}
				 %>
	       		<tr height="25px" style="<%=tagPersonTrStyle %>" id="tagPersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesTag" name="adminUserNamesTag" style="width:180px;" onclick="adminUserTag()" value='<%=adminUserNamesTag %>'/>
				    	通知：
				    	
				   		<input type="checkbox" name="isMailTag" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageTag" checked="checked"/>短信
				   		<input type="checkbox" name="isPageTag" checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
		          <td width="13%" align="right" class="STYLE2">质检流程:</td>
		          <td width="87%" align="left" valign="middle" id="qualityInspectionTr">
		          <input type="radio" name="radio_qualityInspection" value="1" <%if(1 == quality_inspection){out.print("checked='checked'");}%>/>无需质检报告
  	         	  <input type="radio" name="radio_qualityInspection" value="2" <%if(0 == quality_inspection || 2 == quality_inspection){out.print("checked='checked'");}%>/>需要质检报告
		          </td>
		        </tr>
		         <%
				 	String qualityInspectionPersonTrStyle = "display:none";
			   	 	if(1 != quality_inspection){
			   	 		qualityInspectionPersonTrStyle = "";
			    	}
				 %>
		        <tr height="25px;" style="<%=qualityInspectionPersonTrStyle %>" id="qualityInspectionPersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesQualityInspection" name="adminUserNamesQualityInspection" style="width:180px;" onclick="adminUserQualityInspection()" value='<%=adminUserNamesQualityInspection %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailQualityInspection" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageQualityInspection" checked="checked"/>短信
				   		<input type="checkbox" name="isPageQualityInspection" checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
		        	   <td width="13%" align="right" class="STYLE2">出口报关:</td>
		         	   <td width="87%" align="left" valign="middle" id="declarationTr">
	          				<input type="radio" name="radio_declaration" value="1" <%if(declaration.equals("1") || (ccidIsSame && declaration.equals("0"))){out.print("checked='checked'");}%>/> <%=declarationKey.getStatusById(1) %>
	          				<input type="radio" name="radio_declaration" value="2" <%if((!ccidIsSame && declaration.equals("0")) || declaration.equals("2")){out.print("checked='checked'");}%>/> <%=declarationKey.getStatusById(2) %>
		      		  </td>
		      	</tr>
		      	<%
			   	 	String declarationPersonTrStyle = "";
			   	 	if((ccidIsSame && declaration.equals("0")) || declaration.equals("1")){
			   	 		declarationPersonTrStyle = "display:none";
			    	}
				 %>
				<tr height="25px" style='<%=declarationPersonTrStyle %>' id="declarationPersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesDeclaration" name="adminUserNamesDeclaration" style="width:180px;" onclick="adminUserDeclaration()" value='<%=adminUserNamesDeclaration %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailDeclaration" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageDeclaration" checked="checked"/>短信
				   		<input type="checkbox" name="isPageDeclaration" checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
				<tr height="25px">
	        		<td width="13%" align="right" class="STYLE2">进口清关:</td>
			        <td width="87%" align="left" valign="middle" id="clearanceTr">
		        	 <input type="radio" name="radio_clearance" value="1" <%if(clearance.equals("1") || (ccidIsSame && clearance.equals("0"))){out.print("checked='checked'");}%>/> <%= clearanceKey.getStatusById(1) %>
		         	 <input type="radio" name="radio_clearance" value="2" <%if((!ccidIsSame && clearance.equals("0")) || clearance.equals("2")){out.print("checked='checked'");}%>/> <%= clearanceKey.getStatusById(2) %>			      
					</td>
		        </tr>
		         <%
				 	String clearancePersonTrStyle = "";
			   	 	if((ccidIsSame && clearance.equals("0")) || clearance.equals("1")){
			   	 		clearancePersonTrStyle = "display:none";
			    	}
				 %>
		        <tr height="25px" style='<%=clearancePersonTrStyle %>' id="clearancePersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesClearance" name="adminUserNamesClearance" style="width:180px;" onclick="adminUserClearance()" value='<%=adminUserNamesClearance %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailClearance" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageClearance"  checked="checked"/>短信
				   		<input type="checkbox" name="isPageClearance"  checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
		       	  <td width="13%" align="right" class="STYLE2">运费流程:</td>
		          <td width="87%" align="left" valign="middle" id="stockInSetTr">
		          	 <input type="radio" name="stock_in_set_radio" value="1" <%if(1 == stock_in_set){out.print("checked='checked'");}%>/> <%= transportStockInSetKey.getStatusById(1) %>
	          		 <input type="radio" name="stock_in_set_radio" value="2" <%if(0 == stock_in_set || 2 == stock_in_set){out.print("checked='checked'");}%>/> <%= transportStockInSetKey.getStatusById(2) %>
		          </td>
		        </tr>
		        <%
				 	String stockInSetPersonTrStyle = "display:none";
			   	 	if(1 != stock_in_set){
			   	 	stockInSetPersonTrStyle = "";
			    	}
				 %>
		        <tr height="25px" style="<%=stockInSetPersonTrStyle %>" id="stockInSetPersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesStockInSet" name="adminUserNamesStockInSet" style="width:180px;" onclick="adminUserStockInSet()" value='<%=adminUserNamesStockInSet %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailStockInSet" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageStockInSet" checked="checked"/>短信
				   		<input type="checkbox" name="isPageStockInSet" checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
		          <td width="13%" align="right" class="STYLE2">单证:</td>
		          <td width="87%" align="left" valign="middle" id="certificateTr">
		          	<input type="radio" name="radio_certificate" value="1" <%if(1 == certificate){out.print("checked='checked'");}%>/> <%= transportCertificateKey.getStatusById(1) %>
	          		<input type="radio" name="radio_certificate" value="2" <%if(0 == certificate || 2 == certificate){out.print("checked='checked'");}%>/> 需要单证
		          </td>
		        </tr>
		         <%
				 	String certificatePersonTrStyle = "display:none";
			   	 	if(1 != certificate){
			   	 		certificatePersonTrStyle = "";
			    	}
				 %>
		        <tr height="25px" style="<%=certificatePersonTrStyle %>" id="certificatePersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesCertificate" name="adminUserNamesCertificate" style="width:180px;" onclick="adminUserCertificate()" value='<%=adminUserNamesCertificate %>'/>
				    	通知：
				   		<input type="checkbox" name="isMailCertificate" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageCertificate" checked="checked"/>短信
				   		<input type="checkbox" name="isPageCertificate" checked="checked"/>页面
					</td>
				</tr>
				<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		        <tr height="25px">
	        	  <td width="13%" align="right" class="STYLE2">第三方标签:</td>
		          <td width="87%" align="left" valign="middle" id="transportTrThird">
		          	<input type="radio" name="radio_tagThird" value="1" <%if(1 == tag_third){out.print("checked='checked'");}%>/> <%= transportTagKey.getTransportTagById(1) %>
	          		<input type="radio" name="radio_tagThird" value="2" <%if(0 == tag_third || 2 == tag_third){out.print("checked='checked'");}%>/> <%= transportTagKey.getTransportTagById(2) %>
		          </td>
		        </tr>
		        <%
				 	String tagThirdPersonTrStyle = "display:none";
			   	 	if(1 != tag_third){
			   	 	tagThirdPersonTrStyle = "";
			    	}
				 %>
		        <tr height="25px" style="<%=tagThirdPersonTrStyle %>" id="tagThirdPersonTr">
					<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
				    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
				    	<input type="text" id="adminUserNamesTagThird" name="adminUserNamesTagThird" style="width:180px;" onclick="adminUserTagThird()" value='<%=adminUserNamesTagThird %>'/>
				    	通知：
				    	
				   		<input type="checkbox" name="isMailTagThird" checked="checked"/>邮件
				   		<input type="checkbox" name="isMessageTagThird" checked="checked"/>短信
				   		<input type="checkbox" name="isPageTagThird" checked="checked"/>页面
					</td>
				</tr>
			</table>
		</div>
		<div id="freight">
			<input type="hidden" name="fr_id" id="fr_id"/>
			<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr height="29">
		          <td align="right" class="STYLE2">货运公司:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="transport_waybill_name" id="transport_waybill_name" readonly="readonly"><br/>
		          	<input type="button" class="long-button" name="waybillSelect" id="waybillSelect" value="选择运输资源" onclick="openFreightSelect()">
		          </td>
		          <td align="right" class="STYLE2">运单号</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="transport_waybill_number" id="transport_waybill_number">
		          </td>
		        </tr>
		        <tr height="29">
		          <td align="right" class="STYLE2">运输方式:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="hidden" name="transportby" id="transportby">
		          	<input type="text" name="transportby_name" id="transportby_name" readonly="readonly">
		          </td>
		          <td align="right" class="STYLE2">承运公司:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="carriers" id="carriers" readonly="readonly">
		          </td>
		        </tr>
		        <tr height="29" id="tr1" name="tr1">
		          <td align="right" class="STYLE2">始发港:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="transport_send_place" id="transport_send_place" readonly="readonly">
		          </td>
		          <td align="right" class="STYLE2">收货港:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="transport_receive_place" id="transport_receive_place" readonly="readonly">
		          </td>
		        </tr>
			</table>
		</div>
		<div id="freightcost">
			<table id="freight_cost" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" style="padding-top: 17px" class="zebraTable">
			 <tr>
		     	<td align="right" colspan="8" height="30px"><input id="select_freight_cost" type="button" class="long-button" name="selectFreightCost" value="选择运费项目" onclick="selectFreigthCost()"></td>
		     </tr>
		     <tr>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>
		       <th width="25%" nowrap="nowrap" class="right-title"  style="text-align: center;"></th>	
		     </tr>
		    </table>
		</div>
		<div id="receive_allocate">
			<div id="create_b2b_waybill"></div>
		</div>
		
	</div>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top:10px">
		<tr>
			<td align="right">
				<input id="back_button" type="button" style="font-weight: bold" class="normal-green" onClick="back()" value="上一步"/>
				&nbsp;&nbsp;&nbsp;
				<input id="next_button" type="button" style="font-weight: bold" class="normal-green" onClick="next()" value="下一步"/>
				&nbsp;&nbsp;&nbsp;
				<div style="display:none;" id="submitbutton">
					<input type="button" style="font-weight: bold" class="normal-green" onClick="submitCheck()" value="提交"/>
				</div>
				
			</td>
		</tr>
	</table>					
	</div>
      </form>
      <script type="text/javascript">
		
		var tabsIndex = 0;//当前tabs的Index
		var tab = $("#tabs").tabs({
		cache: false,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		load: function(event, ui) {onLoadInitZebraTable();}	,
		selected:0,
		show:function(event,ui)
			 {
			 	if(ui.index==1)
			 	{
			 		checkProcedures();
			 	}
			 	if(ui.index==2)
			 	{
			 		
			 	}
			 	if(ui.index==3)
			 	{
			 		loadWaybill();
			 	}
			 	if(ui.index==4)
			 	{
			 		
				}
			 	
			 	tabsIndex = ui.index;
			 	if(tabsIndex==0)
			 	{
			 		$("#back_button").hide();
			 		$("#next_button").show();
			 	}
			 	else if(tabsIndex==3)
			 	{
			 		$("#next_button").hide();
			 		$("#back_button").show();
			 	}
			 	else
			 	{
			 		$("#back_button").show();
			 		$("#next_button").show();
				}
			 }
		});
		
		function countCheck(obj)
		{
			var count = obj.value;
			var countFloat = parseFloat(count);
			
			if(countFloat<0||countFloat!=count)
			{
				alert("数量填写错误");
				$(obj).css("backgroundColor","red");
				return false;
			}
			else
			{
				$(obj).css("backgroundColor","");
			}
		}
		
		function submitCheck(order_status,config_change_status)
		{
			if(checkProcedures()==false)
			{
			 	tabSelect(0)
			}
			else
			{
				if(order_status==<%=B2BOrderStatusKey.Enough%>)
				{
					document.add_form.submit();
				}
				else if(order_status==<%=B2BOrderStatusKey.ConfigNotEnough%>&&config_change_status==<%=B2BOrderConfigChangeKey.Can%>)
				{
					if(confirm("确定申请配置更改？"))
					{
						document.add_form.submit();
					}
				}
			}
		}
		
		function next()
		{
			var index = tabsIndex+1;
			tabSelect(index);
		}
		
		function back()
		{
			var index = tabsIndex-1;
			tabSelect(index);
		}
		
		function tabSelect(index)
		{
			$("#tabs").tabs("select",index);
		}
	</script>
</body>
</html>
