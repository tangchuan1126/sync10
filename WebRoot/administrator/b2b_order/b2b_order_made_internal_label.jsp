<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<%
String supplierName=StringUtil.getString(request,"supplierName");
long supplierSid=StringUtil.getLong(request,"supplierSid");
//订单号
long b2b_oid=StringUtil.getLong(request,"b2b_oid");
//收货仓库
String warehouse=StringUtil.getString(request,"warehouse");

long pcid = StringUtil.getLong(request,"pcid");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

long cid = StringUtil.getLong(request,"cid");
int type = StringUtil.getInt(request,"type");
String name = StringUtil.getString(request,"name");
String cmd = StringUtil.getString(request,"cmd");


DBRow[] rows;

if(cmd.equals("search"))
{
	rows = b2BOrderMgrZyj.getB2BOrderProductByName(b2b_oid,name);
}
else
{
	rows = b2BOrderMgrZyj.getB2BOrderProductByName(b2b_oid, null);
}

DBRow[] options = lableTemplateMgrZJ.getAllLableTemplate(null);

Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>标签打印</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script language="javascript">

function search()
{
	
	document.search_form.name.value = $("#filter_name").val();	
	
	if($("#filter_name").val()!="")
	{
		document.search_form.cmd.value = "search";
	}
	else
	{
		document.search_form.cmd.value = "";	
	}
	
	document.search_form.submit();
}


function noNumbers(e)
	{
		var keynum
		var keychar
		var numcheck
		if(e.keyCode==8||e.keyCode==46||e.keyCode==37||e.keyCode==38||e.keyCode==39||e.keyCode==40)
		{
			return true;
		}
		else if(window.event) // IE
		{
		  keynum = e.keyCode
		}
		else if(e.which) // Netscape/Firefox/Opera
		{
		  keynum = e.which
		}
		keychar = String.fromCharCode(keynum)
		numcheck = /[^\d]/
		return !numcheck.test(keychar);
	}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 


function closeWin()
{
	tb_remove();
}

$().ready(function() {

	function log(event, data, formatted) {
		
	}

	addAutoComplete($("#filter_name"),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");
});
//-->

	function printProductLabel(pc_id,width,height)
	{
		printer = "LabelPrinter";
		paper = width+"X"+height;
		print_range_width = width;
		print_range_height = height;
		template_path = "lable_template/productbarcode.html";
													
							
		var counts = document.getElementById("productCount_"+pc_id).value;
		document.print_product_label.pc_id.value = pc_id;
		if(parseInt(document.getElementById("productprintcount_"+pc_id).value)!=document.getElementById("productprintcount_"+pc_id).value)
		{
			alert("打印数量有问题！");
		}
		else
		{
			document.print_product_label.copies.value = $("#productprintcount_"+pc_id).val();
			document.print_product_label.printer.value= printer;
			document.print_product_label.paper.value = paper;
			document.print_product_label.print_range_width.value = print_range_width;
			document.print_product_label.print_range_height.value = print_range_height;
			document.print_product_label.counts.value = counts;
							
			document.print_product_label.action = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+template_path;
			document.print_product_label.submit();
		}
	}
	
	function printBoxLabel(pc_id)
	{
		printer = "LabelPrinter";
		paper = "100X50";
		print_range_width = 100;
		print_range_height = 50;
		template_path = "lable_template/productOutSize.html";
													
							
		var counts = document.getElementById("productCount_"+pc_id).value;
		
		if(parseFloat(document.getElementById("productCount_"+pc_id).value)!=document.getElementById("productCount_"+pc_id).value)
		{
			alert("装箱数量有误！");
		}
		else if((parseInt(document.getElementById("boxprintcount_"+pc_id).value))!=document.getElementById("boxprintcount_"+pc_id).value)
		{
			alert("打印数量有误！");
		}
		else
		{
			document.print_box_label.pc_id.value = pc_id
			document.print_box_label.copies.value = $("#boxprintcount_"+pc_id).val();
			document.print_box_label.printer.value= printer;
			document.print_box_label.paper.value = paper;
			document.print_box_label.print_range_width.value = print_range_width;
			document.print_box_label.print_range_height.value = print_range_height;
			document.print_box_label.counts.value = counts;
						
			document.print_box_label.action = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+template_path;
			document.print_box_label.submit();
		}
	}

	//打开标签 列表
	function selectLabel(product_id,factory_type,num)
	{
		var warehouse="<%=warehouse%>";
		var uri = '../b2b_order/b2b_order_lable_template_show.html?warehouse='+warehouse+'&pc_id='+product_id+'&factory_type='+factory_type+'&supplierSid='+<%=supplierSid%>+'&num='+num+'&b2b_oid=<%=b2b_oid%>'; 
		$.artDialog.open(uri , {title: '转运单标签列表',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	
	
	}
	
	function selectLabels()
	{
		var warehouse="<%=warehouse%>";
		var uri = '../b2b_order/b2b_order_lable_internal_template_show.html?warehouse='+warehouse+'&b2b_oid='+<%=b2b_oid%>+'&supplierSid='+<%=supplierSid%>+'&cmd=products&b2b_oid=<%=b2b_oid%>';  
		$.artDialog.open(uri , {title: '转运单批量标签列表',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	    	
	}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<div align="center">
<table width="98%" height="40px;" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title">
         <span style=" font-size: 18px;">提货仓库:<%=session.getAttribute("billOfLading")%></span>
         <span style=" font-size: 18px;">收货仓库:<%=warehouse %></span>
    </td>
  </tr>
</table>
<br>
	<form  method="get" name="search_form">		
		  <input type="hidden" name="supplierName" value="<%=supplierName%>" />
		  <input type="hidden" name="supplierSid" value="<%=supplierSid %>"/>
		  <input type="hidden" name="b2b_oid" value="<%=b2b_oid %>" />
		  <input type="hidden" name="pcid" value="<%=pcid%>" />   
		  <input type="hidden" name="type" value="<%=type%>" />
		  <input type="hidden" name="name"  />
		  <input type="hidden" name="cmd" value="search" /> 	
		  <input type="hidden" name="cid" value="<%=cid%>" /> 
		  <input type="hidden" name="warehouse" value="<%=warehouse %>" />     
	</form>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
          <td width="54%" height="33" >
		  <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">
	 
		  <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td valign="middle"" width="340px;">
                <input type="text" name="filter_name" id="filter_name" value="<%=name%>" style="width: 200px;" onkeydown="if(event.keyCode==13)search()">&nbsp;&nbsp;&nbsp;&nbsp; 
        		<input type="button" name="Submit3" class="button_long_search" value="搜索" onclick="search()"> 
        	  </td>
        	  <td align="center" valign="middle"> 
        	    <input type="button" onclick="selectLabels()" class="long-button-print" value="批量商品签">
        	  </td>
            </tr>
          </table>

			</div>
		  </td>
    <td width="46%" align="right" style="padding-right:10px;"><br><br>
    </td>
  </tr>
</table>
	<br>
<br>
  <form name="listForm" method="post">
		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" >
    <tr>
        <th width="179px" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
		<th width="180px;"  style="vertical-align: center;text-align: center;" class="right-title">商品ID</th>		
        <th width="246px"  style="vertical-align: center;text-align: center;" class="right-title">制作商品标签 </th>
    </tr>

    <% 
		String delPCID = ""; 
		DBRow proGroup[]; 
		for ( int i=0; i<rows.length; i++ ) { 
		delPCID += rows[i].getString("pc_id")+","; 
		%>
    <tr> 
      <td height="100" valign="middle"   style="font-size:14px;">
          <%=rows[i].getString("b2b_p_name")%><br>  
      </td>
	  <td align="center" valign="middle">
      	  <%=rows[i].getString("b2b_pc_id") %><br>
      </td>
    
      <td align="center" valign="middle" nowrap="nowrap">
      	<input type="button" onclick="selectLabel(<%=rows[i].get("b2b_pc_id",0l)%>,'',0)" class="long-button-print" value="商品标签"><br/><br/> 
      	<input type="button" onclick="selectLabel(<%=rows[i].get("b2b_pc_id",0l)%>,'',1)" class="long-button-print" value="外箱标签">      	
	  </td>
	  </tr>
    <% 
	} 
	%>
</table>
		
  </form>
          
<form target="_blank" method="get" name="print_product_label">
	<input type="hidden" id="pc_id" name="pc_id"/>
	<input type="hidden" id="copies" name="copies"/>
	<input type="hidden" id="counts" name="counts"/>
	<input type="hidden" id="print_range_width" name="print_range_width"/>
	<input type="hidden" id="print_range_height" name="print_range_height"/>
	<input type="hidden" id="printer" name="printer"/>
	<input type="hidden" id="paper" name="paper"/>
</form>

<form target="_blank" method="get" name="print_box_label">
	<input type="hidden" id="pc_id" name="pc_id"/>
	<input type="hidden" id="copies" name="copies"/>
	<input type="hidden" id="counts" name="counts"/>
	<input type="hidden" id="print_range_width" name="print_range_width"/>
	<input type="hidden" id="print_range_height" name="print_range_height"/>
	<input type="hidden" id="printer" name="printer"/>
	<input type="hidden" id="paper" name="paper"/>
</form>
</div>
</body>
</html>
