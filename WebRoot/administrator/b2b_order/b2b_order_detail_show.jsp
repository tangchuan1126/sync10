<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.exception.purchase.FileException"%>
<%@page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%

 	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(30);
 	String[] file=new String[2];
 	DBRow[] rows=null;
 	try
 	{
 		file = transportMgrZJ.uploadTransportDetail(request);
 		rows = transportMgrZJ.excelshow(file[0]);
 	}
 	catch(FileTypeException e)
 	{
 		out.print("<script>alert('只可上传.xls文件');window.history.go(-1)</script>");
 	}
 	catch(FileException e)
 	{
 		out.print("<script>alert('上传失败，请重试');window.history.go(-1)</script>");
 	}
 	
 	boolean submit = true;
 	String msg = "";
 	HashMap pName = new HashMap();
 	
 %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" cellpadding="0" cellspacing="0" height="100%">
  <tr>
  	<td valign="top">
		<table width="98%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;">
		  <tr>
			<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">商品名</th>
			<th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">交货数、转运数</th>
			<th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">备件数</th>
			<th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">箱号</th>
		  </tr>
			 <%
				if(rows!=null)
				{	
					DBRow transport = transportMgrZJ.getDetailTransportById(Long.parseLong(file[1]));
					for(int i=0;i<rows.length;i++)
					{
						String q = rows[i].getString("p_name");
						DBRow product = productMgr.getDetailProductByPname(rows[i].getString("p_name"));
						boolean colorchange = false;//增加有问题行变色
						if(product==null)
						{
							colorchange = true;
							submit = false;
							msg = "我们无法识别这个商品";
						}
						if((!rows[i].getString("prefill_deliver_count").matches("^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d|[1-9]\\d*$"))||rows[i].getString("prefill_deliver_count").trim().equals(""))
						{
							colorchange = true;
							submit = false;
							msg = "转运(交货)数量异常";
						}
						if((!rows[i].getString("prefill_backup_count").matches("^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d|[1-9]\\d*|0$"))||rows[i].getString("prefill_backup_count").trim().equals(""))
						{
							colorchange = true;
							submit = false;
							msg = "备件数量异常";
						}
						if(pName.containsKey(rows[i].getString("p_name")))
						{
							colorchange = true;
							submit = false;
							msg = "文件内有重复商品";
						}
						else
						{
							pName.put(rows[i].getString("p_name"),rows[i].getString("p_name"));
						}
			  %>
				  <tr align="center" valign="middle" <%=colorchange==true?" bgcolor='#FFC1C1'":""%>>
					<td height="40" align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("p_name") %>&nbsp;</td>
					<td align="center" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("prefill_deliver_count") %>&nbsp;</td>
					<td align="center" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("prefill_backup_count") %>&nbsp;</td>
					<td style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("prefill_box") %>&nbsp;</td>
				  </tr>
			  <%
					}
				}
			  %>
	  </table>
	</td>
  </tr>
 <tr>
 	<td valign="bottom">
 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="left" valign="middle" class="win-bottom-line">
					<%
						if(!msg.equals(""))
						{
					%>
						<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:red"><%=msg%></span>
					<%
						}
						else
						{
					%>
						&nbsp;
					<%
						}
					%></td>
 				<td align="right" valign="middle" class="win-bottom-line"> 
			      <%  
			      	if(submit)
			      	{
			      %>
			      	<input type="button" name="Submit2" value="确定" class="normal-green" onClick="ajaxSaveTransportDetail()"/>
			      <%
			      	}
			      %>
				  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
   
</table>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/saveTransportDetail.action" name="savePurchasedetail_form">
	<input type="hidden" name="tempfilename" id="tempfilename" value="<%=file[0]%>"/>
	<input type="hidden" name="transport_id" id="transport_id" value="<%=file[1]%>"/>
</form>
<script type="text/javascript">
	function ajaxSaveTransportDetail()
	{
		if(<%=submit%>)
		{
			var tempfilename = $("#tempfilename").val();
			var transport_id = $("#transport_id").val();	
				
			var para = "tempfilename="+tempfilename+"&transport_id="+transport_id;
			
			$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true, 
			
				fadeOut:  2000
			};
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
			
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminitrrator/transport/saveTransportDetail.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){

					if (date["close"])
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">保存成功！</span>' });
						$.unblockUI();
						parent.closeWin();
					}
					else
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">'+date["error"]+'</span>' });
					}
				}
			});
		}
		else
		{
			alert("<%=msg%>");
		}
	}
</script>

</body>
</html>



