<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@page import="org.directwebremoting.json.types.JsonArray"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<title>b2b需求分析</title>

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/select.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>

<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<!-- 带搜索的下拉框 -->
    <link rel="stylesheet" href="chosen.css">
    <script type="text/javascript" src="chosen.jquery.min.js"></script>
      <script type="text/javascript" src="../js/areaSelect.js"></script>
<%
	String title_id = StringUtil.getString(request, "title_id");
String pro_line_id = StringUtil.getString(request,"pro_line_id");
String pcid = StringUtil.getString(request,"pcid");
String search_key = StringUtil.getString(request,"search_key");
int union_flag = StringUtil.getInt(request,"union_flag");


AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adminId=adminLoggerBean.getAdid();
//根据登录帐号判断是否为客户
Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
//如果是用户登录 查询用户下的产品
DBRow[] titles=mgrZwb.selectAllTitle();

TDate tDate = new TDate();
String end_time = StringUtil.getString(request,"end_time",tDate.formatDate("yyyy-MM-dd"));
tDate.addDay(-30);
String start_time = StringUtil.getString(request,"start_time",tDate.formatDate("yyyy-MM-dd"));
%>
<script type="text/javascript">
var title_id = '<%=title_id%>';
var pro_line_id = '<%=pro_line_id%>';
var pcid = '<%=pcid%>';
var search_key = '<%=search_key%>';
$(function(){

	$.blockUI.defaults = {
			 css: { 
			  padding:        '8px',
			  margin:         0,
			  width:          '170px', 
			  top:            '45%', 
			  left:           '40%', 
			  textAlign:      'center', 
			  color:          '#000', 
			  border:         '3px solid #999999',
			  backgroundColor:'#ffffff',
			  '-webkit-border-radius': '10px',
			  '-moz-border-radius':    '10px',
			  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			 },
			 //设置遮罩层的样式
			 overlayCSS:  { 
			  backgroundColor:'#000', 
			  opacity:        '0.6' 
			 },		
			 baseZ: 99999, 
			 centerX: true,
			 centerY: true, 
			 fadeOut:  1000,
			 showOverlay: true
			};
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_demand_analyze_show.html',
		type: 'post',
		dataType: 'html',
		data: 'start_time='+$("#start_time").val()+'&end_time='+$("#end_time").val()+"&type="+$('input[name="groupWith"]:checked').val(),
		async:false,
		success: function(html){
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	

	<%-- Frank ****************** --%>
	addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
	<%-- ***********************  --%>
	
	$("#search_key").keydown(function(event){
		if (event.keyCode==13)
		{
			search();
		}
	});
	
});
</script>
<script type="text/javascript">

function loadData(){
    
    $(".chzn-select").chosen();

	var titles = <%=new JsonObject(titles).toString()%> ;
	var item=[];
	for(var i=0;i<titles.length;i++){
		var op={};       
		op.id=titles[i].title_id;                                                                                                                                                                                                                                                                                                                                                                                                      
		op.name=titles[i].title_name;
		item.push(op);
	}
	var data=[{
		key:'TITLE：',
		type:'title',
		son_key:'产品线：',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadProductLineAction.action',
		
		son_key2:'一级分类：',
		url2:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadFirstProductCatalogAction.action',
		
		son_key3:'二级分类：',
		url3:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadSecondProductCatalogAction.action',	
				
		son_key4:'三级分类：',
		url4:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadThirdProductCatalogAction.action',

		array:item
	 },
 	 ];
	initializtion(data, 1);  //初始化
}	
//点击查询条件
function custom_seach(){
	    
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_demand_analyze_show.html',
		type: 'post',
		dataType: 'html',
		data:searchAllConditions(),
		async:false,
		success: function(html){
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	
}

function go(number){
	
	var para = searchAllConditions()+'&p='+number;
    //alert("go:"+para);
    
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_demand_analyze_show.html',
			type: 'post',
			dataType: 'html',
			data:para,
			async:false,
			success: function(html){
				$("#showList").html(html);
				onLoadInitZebraTable(); //重新调用斑马线样式
			}
		});	
}
function refreshWindow(){
	go($("#jump_p2").val());
}

function search()
{
	if ($("#search_key").val()=="")
	{
		alert("请填写关键字");
		pcid = '';
		search_key = '';
		pro_line_id = '';
		title_id = '';
		return(false);
	}
	else
	{
		pcid = $("#filter_pcid").val();
		search_key = $("#search_key").val();
		pro_line_id = '';
		title_id = $("#title_id").val();
	}
}
function searchAllConditions()
{
	var array=getSeachValue();
	title_id=0;
	pro_line_id=0;
	pcid=0;
	for(var i=0;i<array.length;i++){
		 if(array[i].type=='title'){
			   title_id=array[i].val;
		 }
		 if(array[i].type=='title_son'){
			 pro_line_id=array[i].val;
		 }
		 if(array[i].type=='title_son_son'){
			 pcid=array[i].val;
		 }else if(array[i].type=='title_son_son_son'){
			 pcid=array[i].val;
		 }else if(array[i].type=='title_son_son_son_son'){
			 pcid=array[i].val;
		 }
	}
	 var para='filter_productLine='+pro_line_id+'&filter_pcid='+pcid+"&title_id="+title_id+"&search_key="+$("#search_key").val();
     para += '&start_time='+$("#start_time").val()+'&end_time='+$("#end_time").val();
     para += '&ca_id='+$("#sale_area").val()+'&ccid='+$("#ccid_hidden").val()+'&pro_id='+$("#pro_id").val();
     para += '&cid='+$("#cid").val()+"&type="+$('input[name="groupWith"]:checked').val();
     return para;
}
function exportB2BOrderAnalysis()
{
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/AjaxDownB2BOrderDemandAnalyzeAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:searchAllConditions(),
		cache:false,
		success: function(msg){
			if(msg && msg.flag == "true"){
				window.location="../../"+msg["fileurl"];
				$.unblockUI();
			}else{
				$.unblockUI();
	 		}
		}
	});
}
</script>
</head>
<body onload="loadData()">   
	     
<div id="tabs" >
	<ul>
		<li><a href="#av2">条件搜索</a></li>
	</ul>
	<div id="av2">
		<div id="av"></div>
			<input type="hidden" id="atomicBomb" value=""/>
			<input type="hidden" id="title" />
		<br/><br/>
		发货仓库：<select id="cid" class="chzn-select" style="width: 250px;">
	     			 <option value="0">全部仓库</option>
	     			 <%
	     			 	DBRow[] storageRows = catalogMgr.getProductStorageCatalogTree();
						String qx;
						
						for ( int i=0; i<storageRows.length; i++ )
						{
							if ( storageRows[i].get("parentid",0) != 0 )
							 {
							 	qx = "├ ";
							 }
							 else
							 {
							 	qx = "";
							 }
					%>
          <option value="<%=storageRows[i].getString("id")%>"> 
						     <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageRows[i].get("level",0))%>
						     <%=qx%>
						     <%=storageRows[i].getString("title")%>          </option>
						<%
						}
						%>
    			 </select>
    			 <input name="groupWith" type="radio" id="groupWith" checked="checked" value="1"/>求和统计
    			 <input type="radio" id="groupWith" name="groupWith" value="2"/>分散统计
    			 &nbsp;&nbsp;
    			  <select id="sale_area" onchange="getAreaCountryByCaId(this.value)">
						  		 <option value="0">全部区域</option>
						  		<%
						  			DBRow[] areas = productMgr.getAllCountryAreas(null);
						  			for(int i = 0;i<areas.length;i++)
						  			{
								%>
									<option value="<%=areas[i].get("ca_id",0l)%>"><%=areas[i].getString("name")%></option>
								<%
						  			}
						  		%>
						  	</select>
								&nbsp;&nbsp;&nbsp;
						      <select name="ccid_hidden"  id="ccid_hidden" onChange="getStorageProvinceByCcid(this.value);"
							  <option value="0">全部国家</option>
						      </select>
						      &nbsp;&nbsp;&nbsp;
						      <select name="pro_id" id="pro_id" >
						      	<option value="0">全部区域</option>
						      </select> 
	    <br/><br/>
	    
		<div id="easy_search_father">
				<div id="easy_search" style="margin-top:-2px;">
					<a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a>
				</div>
		    </div>
	           <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="418">
						<div  class="search_shadow_bg">
						 <input name="search_key" id="search_key" value=""  type="text" class="search_input" style="font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)search()"/>
						</div>
					</td>
					<td>
						发货开始时间：<input name="start_time" type="text" id="start_time" size="10" value="<%=start_time%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
				    	结束时间：<input name="end_time" type="text" id="end_time" size="10" value="<%=end_time%>" readonly="readonly" /> &nbsp;&nbsp;&nbsp;
				    	&nbsp;&nbsp;&nbsp;<input type="button" onclick="custom_seach()" class="button_long_search" value="搜索">
				    	&nbsp;&nbsp;&nbsp;<input type="button" onclick="exportB2BOrderAnalysis('')" class="long-button-export" value="导出">
					</td>
				</tr>
			  </table>	
	</div>
</div>
</div>
<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	$("#start_time").date_input();
	$("#end_time").date_input();
</script>
<div id="showList">1213231</div>
	  
</body>
</html>

