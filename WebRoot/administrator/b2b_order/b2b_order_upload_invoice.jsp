<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long b2b_oid = StringUtil.getLong(request,"b2b_oid");
	
	DBRow b2BOrderRow = b2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
	
	if(b2BOrderRow ==null)
	{
		b2BOrderRow = new DBRow();
	}
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>上传商业发票</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script>
function uploadDeliveryOrderDetail()
{
	 if($("#file").val() == "")
	{
		alert("请选择文件上传");
		
	}
	else
	{	
		var filename=$("#file").val().split(".");
		if(filename[filename.length-1]=="xls"||filename[filename.length-1]=="doc"||filename[filename.length-1]=="xlsx"||filename[filename.length-1]=="docx")
		{
			document.upload_form.submit();
		}
		else
		{
			alert("请上传Excel或Word文件");
		}	
	}
}

</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/B2BOrderUploadInvoiceAction.action" name="upload_form" enctype="multipart/form-data" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
            <tr>
              <td align="left" valign="top"><br>
         		 <table width="95%" align="center" cellpadding="0" cellspacing="0" >
			  <tr>
				<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;">
				转运单单号：B<%=b2b_oid%>
				</td>
			  </tr>
		  </table>
		  <br/><br/>
		  <table width="95%" align="center" cellpadding="5" cellspacing="0" >
			<tr  style=" padding-bottom:15px; padding-left:15px;">
				<td width="9%" align="left" valign="top" nowrap="nowrap" class="STYLE3" >上传发票文件</td>
				<td width="91"align="left" valign="top" >
					<input type="file" name="file" id="file">
				</td>
			</tr>
		  </table>
		</td>
	  </tr>
      <tr>
         <td  align="right" valign="middle" class="win-bottom-line">				
	  		<input type="button" name="Submit2" value="确定" class="normal-green" onClick="uploadDeliveryOrderDetail();">
    		<input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close();">
    	 </td>
      </tr>
	</table>
<input type="hidden" name="b2b_oid" id="b2b_oid" value="<%=b2b_oid%>"/>
</form>
</body>
</html>
