<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="java.util.Date"%>
<%@ include file="../../include.jsp"%> 
<%
	long b2b_oid = StringUtil.getLong(request,"b2b_oid");
	
	DBRow b2BOrderRow = b2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
	
	DBRow[] b2BOrderDetails = b2BOrderMgrZyj.getB2BOrderItemByB2BOrderId(b2b_oid,null,null,null,null);
	
	double sum = 0;
%>
<style type="text/css">
<!--
.STYLE1 {font-family: Arial, Helvetica, sans-serif}
-->
</style>

<table width="720px" border="0" align="left" cellpadding="0" cellspacing="0">
<thead>
  <tr>
  	<td colspan="6">
		<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2" align="center" valign="middle" style="font-family: Arial Black;font-size: 19px; padding-bottom:10px;">Visionari LLC</td>
				<td width="45%" align="center" valign="middle" nowrap="nowrap"  style="font-family: 黑体;font-size: large;padding-bottom:10px;">微尘大业订单</td>
				<td width="29%" colspan="2" align="left" style="font-family:黑体;font-size: 15px;padding-bottom:10px;">
				<br />
				订单号：<%="B"+b2b_oid%><br/>
			 <!--  <span style="font-family: C39HrP48DmTt;font-size:40px;">*<%=b2b_oid%>*</span> -->
			  <img src="/barbecue/barcode?data=<%=b2b_oid%>&width=1&height=35&type=code39" />
			  </td>
		  </tr>
			   <tr>
			   	<td colspan="5" style=" padding-bottom:5px; padding-top:5px;">
					<hr/>
					<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<table width="100%">
						<tr>
							<td align="right" style="font-family:黑体;font-size: 16px;" nowrap="nowrap">货运公司：</td>
							<td align="left"><%=b2BOrderRow.getString("b2b_order_waybill_name") %></td>
							<td nowrap="nowrap" style="font-family:黑体;font-size: 16px;" align="right">运单号：</td>
							<td align="left"><%=b2BOrderRow.getString("b2b_order_waybill_number")%></td>
						</tr>
					</table>
					</div>
					<hr/>
				</td>
			  </tr>
		 </table>
	</td>
  </tr>
    <tr>
		<th nowrap="nowrap"  align="left"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">商品名</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">应发数量</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">实发数量</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;border-right: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">所在箱号</th>
	</tr>
  </tr>
  </thead>
  	<%
		for(int i = 0;i<b2BOrderDetails.length;i++)
		{
	%>
		<tr>
		<td align="left" height="30" style="font-family:黑体;font-size: 15px;border-left: 2px #000000 solid;border-bottom: 1px #000000 solid;padding-left:3px;"><%=b2BOrderDetails[i].getString("p_name")%></td>
		<td align="center" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;border-bottom: 1px #000000 solid;"><%=b2BOrderDetails[i].get("b2b_count",0f) %></td>
		<td align="center" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;border-bottom: 1px #000000 solid;font-style:oblique"><%=b2BOrderDetails[i].get("b2b_send_count",0f) %></td>
		<td align="center" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;;border-bottom: 1px #000000 solid;border-right: 2px #000000 solid;padding-left:3px;"><%=b2BOrderDetails[i].getString("b2b_box")%></td>
		</tr>
		<tr>
			<td colspan="2" align="center" valign="middle" style="border-left: 2px #000000 solid;border-bottom: 1px #000000 solid; padding-top:5px; padding-bottom:5px;">
			<img src="/barbecue/barcode?data=<%=b2BOrderDetails[i].getString("p_code")%>&width=1&height=35&type=code39" />

			</td>
			<td colspan="4" style="border-right: 2px #000000 solid;border-bottom: 1px #000000 solid;"></td>
		</tr>
	<%
		}
	%>
	<tfoot>
		<tr>
			<td colspan="6" style="padding-top:15px;">
				<hr/>
				<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;">
				<table width="100%">
				  <tr>
				<td style="width:100%;" colspan="2">
					<table style="width:100%;" class="addr_table">
						<tr>
							<td style="width:48%;">
								<table style="width:100%;border-collapse:collapse ;">
									<tr>
										<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 16px;">提货地址</font></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">提货仓库</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												out.print(catalogMgr.getDetailProductStorageCatalogById(b2BOrderRow.get("send_psid",0l)).getString("title"));
												session.setAttribute("billOfLading",catalogMgr.getDetailProductStorageCatalogById(b2BOrderRow.get("send_psid",0l)).getString("title"));
											%>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">门牌号</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=b2BOrderRow?b2BOrderRow.getString("send_house_number"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;height: 60px;font-size: 14px;">街道</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=b2BOrderRow?b2BOrderRow.getString("send_street"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">城市</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=b2BOrderRow?b2BOrderRow.getString("send_city"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">邮编</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=b2BOrderRow?b2BOrderRow.getString("send_zip_code"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">省份</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ps_id = null!=b2BOrderRow?b2BOrderRow.get("send_pro_id",0L):0L ;
												DBRow psIdRow = productMgr.getDetailProvinceByProId(ps_id);
											%>
											<%=(psIdRow != null? psIdRow.getString("pro_name"):b2BOrderRow.getString("address_state_send") ) %>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">国家</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ccid = null!=b2BOrderRow?b2BOrderRow.get("send_ccid",0L):0L; 
												DBRow ccidRow = orderMgr.getDetailCountryCodeByCcid(ccid);
											%>
											<%= (ccidRow != null?ccidRow.getString("c_country"):"" ) %>	
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">联系人</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=b2BOrderRow?b2BOrderRow.getString("send_name"):"" %></td>
				  </tr>
				  <tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">联系电话</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=b2BOrderRow?b2BOrderRow.getString("send_linkman_phone"):"" %></td>
									</tr>
								</table>
							</td>
							<td style="width:4%;text-align: center;font-size: 18px;">
								-->
							</td>
							<td style="width:48%;">
								<table style="width:100%;border-collapse:collapse ;" class="addr_table">
									<tr>
										<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 16px;">收货地址</font></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">收货仓库</td>
										<td style="border: 1px solid silver;width: 80%;"><%=catalogMgr.getDetailProductStorageCatalogById(b2BOrderRow.get("receive_psid",0l)).getString("title")%></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">门牌号</td>
										<td style="border: 1px solid silver;width: 80%;"><%=b2BOrderRow.getString("deliver_house_number") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;height: 60px;font-size: 14px;">街道</td>
										<td style="border: 1px solid silver;width: 80%;"><%=b2BOrderRow.getString("deliver_street") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">城市</td>
										<td style="border: 1px solid silver;width: 80%;"><%=b2BOrderRow.getString("deliver_city") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">邮编</td>
										<td style="border: 1px solid silver;width: 80%;"><%=b2BOrderRow.getString("deliver_zip_code") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">省份</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ps_id1 = null!=b2BOrderRow?b2BOrderRow.get("deliver_pro_id",0l):0L;
												DBRow psIdRow1 = productMgr.getDetailProvinceByProId(ps_id1);
											%>
											<%=(psIdRow1 != null? psIdRow1.getString("pro_name"):b2BOrderRow.getString("address_state_deliver") ) %>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">国家</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ccid1 = null!=b2BOrderRow?b2BOrderRow.get("deliver_ccid",0l):0L; 
												DBRow ccidRow1 = orderMgr.getDetailCountryCodeByCcid(ccid1);
											%>
											<%= (ccidRow1 != null?ccidRow1.getString("c_country"):"" ) %>	
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">联系人</td>
										<td style="border: 1px solid silver;width: 80%;"><%=b2BOrderRow.getString("b2b_order_linkman")%></td>
				  </tr>
				  <tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;font-size: 14px;">联系电话</td>
										<td style="border: 1px solid silver;width: 80%;"><%=b2BOrderRow.getString("b2b_order_linkman_phone")%></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				  </tr>
				</table>
				</div>
				<hr/>
			</td>
		</tr>
		
	</tfoot>
</table>



