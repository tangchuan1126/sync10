<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%> 

<%
String titleId = StringUtil.getString(request, "title_id");
String filter_productLine = StringUtil.getString(request,"filter_productLine");
String pc_name = StringUtil.getString(request,"search_key");
String filter_categoryId = StringUtil.getString(request, "filter_pcid");
long ccid = StringUtil.getLong(request, "ccid");
long pro_id = StringUtil.getLong(request, "pro_id");
String start_time = StringUtil.getString(request, "start_time");
String end_time = StringUtil.getString(request, "end_time");
long ca_id = StringUtil.getLong(request, "ca_id");
long cid = StringUtil.getLong(request, "cid");
int type = StringUtil.getInt(request, "type");
int p = StringUtil.getInt(request,"p");
PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(10);
int begin = 0;
int length = 2;
DBRow[] dates = ordersProcessMgrQLL.getDate(start_time,end_time);
DBRow[] rows = b2BOrderMgrZyj.getB2BOrderOrProductCountAnalyze(start_time, end_time, filter_categoryId, filter_productLine,
		pc_name, ca_id, ccid, pro_id, cid, type,titleId, pc);
%>

<html>
<head>
<title>b2b需求分析列表</title>


<script type="text/javascript">

</script>

<style type="text/css">
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:413px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
table.zebraTable tr{height: 30;}
.set{
padding:2px;
width:90%;
word-break:break-all;
margin-top:10px;
margin-top:5px;
line-height:18px;
-webkit-border-radius:5px;
-moz-border-radius:5px;
 margin-bottom: 10px;
 border: 2px solid silver;}
ul.myulAdmin, ul.myulPcLine, ul.myulPcCata{list-style-type:none;}
ul.myulAdmin li{float:left;width:150px;line-height:25px;height:25px;text-align: left;}
ul.myulPcLine li{float:left;width:100%;line-height:25px;height:25px;text-align: left;}
ul.myulPcCata li{float:left;width:100%;line-height:25px;height:25px;text-align: left;}
.searchbarGray{
       color:#c4c4c4;font-family: Arial;
 }
</style>
</head>
  
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<br/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
    		<th width="10%" class="right-title" style="vertical-align: center;text-align: center;" nowrap="nowrap">&nbsp;所属产品线&nbsp;</th>
    		<th width="10%" class="right-title" style="vertical-align: center;text-align: center;" nowrap="nowrap">&nbsp;产品分类&nbsp;</th>
	        <th width="10%" class="right-title" style="vertical-align: center;text-align: center;" nowrap="nowrap">&nbsp;商品名称&nbsp;</th>
	        <th width="10%" class="right-title" style="vertical-align: center;text-align: center;" nowrap="nowrap">&nbsp;仓库&nbsp;</th>
	        <th width="10%" class="right-title" style="vertical-align: center;text-align: center;" nowrap="nowrap">&nbsp;区域&nbsp;</th>
	         <%
			  	if(ca_id>0)
			  	{
			 %>
	        <th width="10%" class="right-title" style="vertical-align: center;text-align: center;" nowrap="nowrap">&nbsp;国家&nbsp;</th>
	        <%} %>
	        <%
			  	if(ccid>0)
			  	{
			 %>
	        <th width="10%" class="right-title" style="vertical-align: center;text-align: center;" nowrap="nowrap">&nbsp;地区&nbsp;</th>
	        <% } %>
	        <th width="10%" class="right-title" style="vertical-align: center;text-align: center;" nowrap="nowrap">&nbsp;需求总数&nbsp;</th>
	        <%for(int i = 0; i < dates.length; i ++){ %>
	      	  <th width="10%" class="left-title" style="vertical-align: center;text-align: center;"><%=dates[i].getString("date") %></th>
	        <%} %>
		</tr>
		<%if(rows.length > 0)
		{
			for(int j = 0; j < rows.length; j ++)
			{
		%>
			<tr>
				<td width="10%" nowrap="nowrap">
					<%=rows[j].getString("pl_name") %>
				</td>
				<td width="10%" nowrap="nowrap">
					<%=rows[j].getString("title") %>
				</td>
				<td width="10%" nowrap="nowrap">
					<%=rows[j].getString("p_name") %>
				</td>
				<td width="10%" nowrap="nowrap">
					<%=(0==cid)?"ALL":rows[j].getString("storage_title") %>
				</td>
				<td width="10%" nowrap="nowrap">
					<%=(0==ca_id&&1==type)?"ALL":rows[j].getString("area_name") %>
				</td>
				<% 
				 	if(ca_id>0)
				 	{
				 %>
				<td width="10%" nowrap="nowrap">
					<%=(ccid==0&&type==1)?"ALL":rows[j].getString("c_country") %>
				</td>
				<%} %>
				 <% 
				 	if(ccid>0)
				 	{
				 %>
				<td width="10%" nowrap="nowrap">
					<%=(pro_id==0&&type==1)?"ALL":rows[j].getString("pro_name") %>
				</td>
				<%} %>
				<td width="10%" nowrap="nowrap">
					<%=rows[j].getString("sum_quantity") %>
				</td>
				<%for(int i = 0; i < dates.length; i ++)
				{
				%>
	      	  	<td width="10%" nowrap="nowrap"><%=rows[j].getString("date_"+i) %></td>
	     	   <%} %>
			</tr>
		
		<%	}
		}else{%>
		<tr>
			<td colspan="<%=dates.length+6 %>" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
		</tr>
		<%}%>
	</table>
    <br/>
<form action="" id="pageForm" method="post">
		<input type="hidden" name="p" id="pageCount" value="<%=p %>"/>
		<input type="hidden" name="cmd" value=""/>
		<input type="hidden" name="filter_productLine" id="filter_productLine" value="<%=filter_productLine %>"/>
		<input type="hidden" name="filter_categoryId" id="filter_categoryId" value="<%=filter_categoryId %>"/>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle">
	<%
		int pre = pc.getPageNo() - 1;
		int next = pc.getPageNo() + 1;
		out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
		out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
		out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
		out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
		out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
          跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<form action="" name="download_form" id="download_form">
</form>

</body>
</html>
