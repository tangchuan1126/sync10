<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
 <%@ page import="com.cwc.app.key.FileWithTypeKey"%>
   <%@ page import="com.cwc.app.key.B2BOrderProductFileKey"%>
 
<%@ page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%
	long b2b_oid = StringUtil.getLong(request,"b2b_oid");
	DBRow b2BOrderRow = b2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
	int product_file_type = StringUtil.getInt(request,"product_file_type");
	TDate tdate = new TDate();
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(10);
	String fileTypeName = StringUtil.getString(request,"file_type_name");
	
	String descriptionByType = "";
 	String product_photo_description = systemConfig.getStringConfigValue("product_photo_description");
 	if(!"".equals(product_photo_description))
 	{
 		String[] product_photo_description_arr = product_photo_description.split("\n");
 		if(product_file_type-1 < product_photo_description_arr.length)
 		{
 			descriptionByType = "注:" + product_photo_description_arr[product_file_type-1] + "</td>";
 		}
 	}
	
	DBRow[] rows =  transportMgrZr.getProductFileByFileTypeAndWithId(  b2b_oid ,FileWithTypeKey.B2B_ORDER_PRODUCT_FILE ,  product_file_type ,  pc);
	
	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script language="javascript" src="../../common.js"></script>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
 

 <script type="text/javascript">
 function deleteFile(file_id){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'product_file',file_id:file_id,folder:'product',pk:'pf_id'},
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}else{
					showMessage("系统错误,请稍后重试","error");
				}
			},
			error:function(){
				showMessage("系统错误,请稍后重试","error");
			}
		})
	}
 //图片在线显示  	
	 
	 function showPictrueOnline(fileWithType,fileWithId , currentName){
var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		product_file_type:<%=product_file_type%>,
		cmd:"multiFile",
		table:'product_file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_product")%>'
}
if(window.top && window.top.openPictureOnlineShow){
	window.top.openPictureOnlineShow(obj);
}else{
    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
}
}
 </script>

</head>

<body >
 
	
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	<tr style="background: #FFFFFF;">
		<td colspan="6">
			<%=descriptionByType %>
		</td>
	</tr>
    <tr> 
        <th width="10%"  class="left-title " style="vertical-align: center;text-align: center;">操作员</th>
        <th width="30%"  class="left-title " style="vertical-align: center;text-align: center;">商品名称</th>
        <th width="25%"  class="right-title " style="vertical-align: center;text-align: center;">文件名称</th>
        <th width="14%"  class="right-title " style="vertical-align: center;text-align: center;">上传时间</th>
        <th width="10%"  class="right-title " style="vertical-align: center;text-align: center;">文件类型</th>
        <th width="10%"  class="right-title " style="vertical-align: center;text-align: center;">操作</th>
  	</tr>
  	 <%
		if(rows != null && rows.length > 0){
			for(DBRow row : rows){
				%>
				<tr>
					<td style="height:25px;line-height:25px;"><%= row.getString("employe_name") %></td>
					<td><%= row.getString("p_name") %></td>
					<td>
					     <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	 	<%if(StringUtil.isPictureFile(row.getString("file_name"))){ %>
				 			 	 <p>
				 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.B2B_ORDER_PRODUCT_FILE %>','<%=b2b_oid %>','<%=row.getString("file_name") %>');"><%=row.getString("file_name") %></a>
				 			 	 </p>
			 			 	 <%} 
			 			 	 else{ %>
 		 			 	 	  		<p><a href='<%= downLoadFileAction%>?file_name=<%=row.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_product")%>'><%=row.getString("file_name") %></a></p> 
 		 			 	 	  <%} %>
					    
 					</td>
					<td><%= tdate.getFormateTime(row.getString("upload_time")) %></td>
		 			<td><%= fileTypeName %></td>
		 			<td>
		 			
		 				<% if(b2BOrderRow.get("product_file",0) != B2BOrderProductFileKey.FINISH){ %>
   		 			 			 <a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
   		 			 	 <%}else{out.print("&nbsp;");} %>	
		 			</td>
				</tr>
				<% 
			}
 
		}else{
  	 %>
  	 	<tr>
	 		 <td colspan="6" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	   </tr>	
 	<%} %>	
</table>
<form  id="dataForm" name="dataForm">
		<input type="hidden" name="p" id="pageCount"/>
		<input type="hidden" name="b2b_oid" value="<%=b2b_oid %>"/>
		<input type="hidden" name="product_file_type" value="<%=product_file_type %>"/>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  
	  <tr>
	    <td height="28" align="right" valign="middle"><%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
	      跳转到
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
	        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
	    </td>
	  </tr>
	</table>
</body>
</html>



