<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.B2BOrderLogTypeKey"%>
<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@page import="java.util.List"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.B2BOrderQualityInspectionKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.B2BOrderClearanceKey"%>
<%@page import="com.cwc.app.key.B2BOrderDeclarationKey"%>
<%@page import="com.cwc.app.key.B2BOrderProductFileKey"%>
<%@page import="com.cwc.app.key.B2BOrderTagKey"%>
<%@page import="com.cwc.app.key.B2BOrderStockInSetKey"%>
<%@page import="com.cwc.app.key.B2BOrderCertificateKey"%>
<%@page import="com.cwc.app.key.B2BOrderQualityInspectionKey"%>
<jsp:useBean id="b2BOrderClearanceKey" class="com.cwc.app.key.B2BOrderClearanceKey"/>
<jsp:useBean id="b2BOrderDeclarationKey" class="com.cwc.app.key.B2BOrderDeclarationKey"/>
<jsp:useBean id="b2BOrderWayKey" class="com.cwc.app.key.B2BOrderWayKey"/>
<jsp:useBean id="b2BOrderTagKey" class="com.cwc.app.key.B2BOrderTagKey"/> 
<jsp:useBean id="b2BOrderCertificateKey" class="com.cwc.app.key.B2BOrderCertificateKey"/> 
<jsp:useBean id="b2BOrderStockInSetKey" class="com.cwc.app.key.B2BOrderStockInSetKey"/> 
<jsp:useBean id="b2BOrderProductFileKey" class="com.cwc.app.key.B2BOrderProductFileKey"/> 
<jsp:useBean id="b2BOrderQualityInspectionKeyKey" class="com.cwc.app.key.B2BOrderQualityInspectionKey"/> 
<%
	DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
	DBRow countrycode[] = orderMgr.getAllCountryCode();
									
	String selectBg="#ffffff";
	String preLetter="";
	
	String b2b_oid_last = b2BOrderMgrZyj.getB2BOrderLastTimeCreateByAdid(request);
	
	
	String adminUserIdsB2BOrder		= "";
	String adminUserNamesB2BOrder		= "";
	String adminUserIdsDeclaration		= "";
	String adminUserNamesDeclaration	= "";
	String adminUserIdsClearance		= "";
	String adminUserNamesClearance		= "";
	String adminUserIdsProductFile		= "";
	String adminUserNamesProductFile	= "";
	String adminUserIdsTag				= "";
	String adminUserNamesTag			= "";
	String adminUserIdsStockInSet		= "";	
	String adminUserNamesStockInSet		= "";
	String adminUserIdsCertificate		= "";
	String adminUserNamesCertificate	= "";
	String adminUserIdsQualityInspection	= ""; 
	String adminUserNamesQualityInspection	= "";
	String adminUserIdsTagThird			= "";
	String adminUserNamesTagThird		= "";

	if(!"".equals(b2b_oid_last))
	{
		//查询各流程任务的负责人
		adminUserIdsB2BOrder		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Goods);
		adminUserNamesB2BOrder		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Goods);
		adminUserIdsDeclaration		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ExportCustoms);
		adminUserNamesDeclaration	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ExportCustoms);
		adminUserIdsClearance		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ImportCustoms);
		adminUserNamesClearance		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ImportCustoms);
		adminUserIdsProductFile		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ProductShot);
		adminUserNamesProductFile	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ProductShot);
		adminUserIdsTag				= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Label);
		adminUserNamesTag			= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Label);
		adminUserIdsStockInSet		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Freight);
		adminUserNamesStockInSet	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Freight);
		adminUserIdsCertificate		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Document);
		adminUserNamesCertificate	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Document);
		adminUserIdsQualityInspection	= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.QualityControl);
		adminUserNamesQualityInspection	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.QualityControl);
		adminUserIdsTagThird		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.THIRD_TAG);
		adminUserNamesTagThird		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.THIRD_TAG);
	}		
	
	DBRow[] titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true, 0L, "", null, request);  //客户登录列出自己的title
	StorageTypeKey storageTypeKey = new StorageTypeKey();
	List<String> storageTypes = storageTypeKey.getStorageTypeKeys();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增订单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<!-- dialog -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<!-- 多选下拉框 -->
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.filter.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.filter.min.js"></script>	
    <link rel="stylesheet" href="chosen.css">
    <script type="text/javascript" src="chosen.jquery.min.js"></script>

<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
	jQuery(function($){
		$('#b2b_order_out_date,#b2b_order_receive_date').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
		});
		$("#ui-datepicker-div").css("display","none");
	});
	
	
	$.fn.serializeObject = function()    
	{    
	   var o = {};    
	   var a = this.serializeArray();    
	   $.each(a, function() 
			   {    
	       if (o[this.name]) 
	       {    
	           if (!o[this.name].push) 
	           {    
	               o[this.name] = [o[this.name]];    
	           }    
	           o[this.name].push(this.value || '');    
	       } 
	       else 
	       {    
	           o[this.name] = this.value || '';    
	       }    
	   });    
	   return o;    
	};  
	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	function stateDivSend(pro_id, pro_input)
	{
		if(pro_id==-1)
		{
			$("#state_div_send").css("display","inline");
			$("#address_state_send").val(pro_input);
		}
		else
		{
			$("#state_div_send").css("display","none");
			$("#address_state_send").val("");
		}
	}
	function stateDivDeliver(pro_id, pro_input)
	{
		if(pro_id==-1)
		{
			$("#state_div_deliver").css("display","inline");
			$("#address_state_deliver").val(pro_input);
		}
		else
		{
			$("#state_div_deliver").css("display","none");
			$("#address_state_deliver").val("");
		}
	}
	//国家地区切换
function getStorageProvinceByCcid(ccid,pro_id,id, pro_input)
{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#"+id).clearAll();
					$("#"+id).addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#"+id).addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#"+id).addOption("手工输入","-1");
					}
				});
				if (pro_id!=""&&pro_id!=0)
				{
					$("#"+id).setSelectedValue(pro_id);
					if(id=="deliver_pro_id")
					{
						stateDivDeliver(pro_id, pro_input);
					}
					else
					{
						stateDivSend(pro_id, pro_input);
					}
				}
					$("#"+id).trigger("liszt:updated");
}

	function selectDeliveryStorage(obj)
	{
	
	
     if(typeId==<%=StorageTypeKey.CUSTOMER%>){
 

              changeShipTo(obj);


     }
	else{

		var ps_id = obj.value;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{ps_id:ps_id},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败");
				},
				success: function(data){
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					//$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.deliver_city);
					$("#deliver_ccid").val(data["deliver_nation"]);

					getStorageProvinceByCcid($("#deliver_ccid").val(),data["deliver_pro_id"],"deliver_pro_id", data.deliver_pro_input);
					
					$("#deliver_name").val(data["deliver_contact"]);
					$("#deliver_linkman_phone").val(data["deliver_phone"]);
					$("#receive_psid").val($("#receive_psid_list").val());
					   $("#deliver_pro_id").trigger("liszt:updated");	
			   	 $("#deliver_ccid").trigger("liszt:updated");
			   	    $("#deliver_pro_id").trigger("liszt:updated");	
			   	 $("#deliver_ccid").trigger("liszt:updated");		
				}
			});
			/*var stroage_type = $("option:selected", obj).attr("stroageType");
			if(1==stroage_type)
			{
				$("#transportArrrivalDateTr").css("display","");
			}
			else
			{
				$("#transportArrrivalDateTr").css("display","none");
			}*/
	}
	}
	
	function selectSendStorage(obj)
	{
		var ps_id = obj.value;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					ps_id:ps_id
				},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data)
				{
					$("#send_house_number").val(data["send_house_number"]);
					$("#send_street").val(data["send_street"]);
					//$("#send_address3").val(data.deliver_address3);
					$("#send_zip_code").val(data.send_zip_code);
					$("#send_name").val(data.send_contact);
					$("#send_linkman_phone").val(data.send_phone);
					$("#send_city").val(data.send_city);
					$("#send_ccid").val(data["send_nation"]);

					getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id",data.send_pro_input);
				}
			});
		/*var stroage_type = $("option:selected", obj).attr("stroageType");
		if(1==stroage_type)
		{
			$("#transportOutDateTr").css("display","");
		}
		else
		{
			$("#transportOutDateTr").css("display","none");
		}*/
	}
	
	//检查提货地址
	function checkSend()
	{
		if($("#psid").val()==0)
		{
			alert("请选择提货仓库");
			return false;
		}
		else if($("#send_ccid").val()==0)
		{
			alert("请选择提货国家");
			return false;
		}
		else if($("#send_pro_id").val()==0)
		{
			alert("请选择提货省份");
			return false;
		}
		else if($("#send_pro_id").val()==-1 && ''==$("#address_state_send").val())
		{
			alert("请填写提货省份");
			$("#address_state_send").focus();
			return false;
		}
		else if($("#send_city").val()=="")
		{
			alert("请填写提货城市");
			$("#send_city").focus();
			return false;
		}
		else if($("#send_house_number").val()=="")
		{
			alert("请填写提货地址门牌号");
			$("#send_house_number").focus();
			return false;
		}
		else if($("#send_street").val()=="")
		{
			alert("请填写提货地址街道");
			$("#send_street").focus();
			return false;
		}
		else if($("#send_zip_code").val()=="")
		{
			alert("请填写提货地邮编");
			$("#send_zip_code").focus();
			return false;
		}
		else if($("#send_name").val()=="")
		{
			alert("请填写提货联系人");
			$("#send_name").focus();
			return false;
		}
	}
	
	//检查交货地址
	function checkReceive()
	{
		if($("#receive_psid").val()==0)
		{
			alert("请选择收货仓库");
			return false;
		}
		else if($("#deliver_ccid").val()==0)
		{
			alert("请选择收货国家");
			return false;
		}
		else if($("#deliver_pro_id").val()==0)
		{
			alert("请选择收货省份");
			return false;
		}
		else if($("#deliver_pro_id").val()==-1 && ''==$("#address_state_deliver").val())
		{
			alert("请填写收货省份");
			$("#address_state_deliver").focus();
			return false;
		}
		else if($("#deliver_city").val()=="")
		{
			alert("请填写收货城市");
			$("#deliver_city").focus();
			return false;
		}
		else if($("#deliver_house_number").val()=="")
		{
			alert("请填写收货地址门牌号");
			$("#deliver_house_number").focus();
			return false;
		}
		else if($("#deliver_street").val()=="")
		{
			alert("请填写收货地址街道");
			$("#deliver_street").focus();
			return false;
		}
		else if($("#deliver_zip_code").val()=="")
		{
			alert("交货地址邮编");
			$("#deliver_zip_code").focus();
			return false;
		}
		else if($("#deliver_name").val()=="")
		{
			alert("请填写交货联系人");
			$("#deliver_name").focus();
			return false;
		}
	}
	
	//检查流程
	function checkProcedures()
	{
		document.add_form.declaration.value = $("input:radio[name=radio_declaration]:checked").val();
		document.add_form.clearance.value = $("input:radio[name=radio_clearance]:checked").val();
		document.add_form.tag.value = $("input:radio[name=radio_tag]:checked").val();
		document.add_form.tag_third.value = $("input:radio[name=radio_tagThird]:checked").val();
		document.add_form.stock_in_set.value = $("input:radio[name=stock_in_set_radio]:checked").val();
		document.add_form.certificate.value = $("input:radio[name=radio_certificate]:checked").val();
		document.add_form.quality_inspection.value = $("input:radio[name=radio_qualityInspection]:checked").val();

		if($("input:checkbox[name=isMailB2BOrder]").attr("checked")){
			document.add_form.needMailB2BOrder.value = 2;
		}
		if($("input:checkbox[name=isMessageB2BOrder]").attr("checked")){
			document.add_form.needMessageB2BOrder.value = 2;
		}
		if($("input:checkbox[name=isPageB2BOrder]").attr("checked")){
			document.add_form.needPageB2BOrder.value = 2;
		}
		
		if($("input:checkbox[name=isMailDeclaration]").attr("checked")){
			document.add_form.needMailDeclaration.value = 2;
		}
		if($("input:checkbox[name=isMessageDeclaration]").attr("checked")){
			document.add_form.needMessageDeclaration.value = 2;
		}
		if($("input:checkbox[name=isPageDeclaration]").attr("checked")){
			document.add_form.needPageDeclaration.value = 2;
		}

		if($("input:checkbox[name=isMailClearance]").attr("checked")){
			document.add_form.needMailClearance.value = 2;
		}
		if($("input:checkbox[name=isMessageClearance]").attr("checked")){
			document.add_form.needMessageClearance.value = 2;
		}
		if($("input:checkbox[name=isPageClearance]").attr("checked")){
			document.add_form.needPageClearance.value = 2;
		}

		if($("input:checkbox[name=isMailProductFile]").attr("checked")){
			document.add_form.needMailProductFile.value = 2;
		}
		if($("input:checkbox[name=isMessageProductFile]").attr("checked")){
			document.add_form.needMessageProductFile.value = 2;
		}
		if($("input:checkbox[name=isPageProductFile]").attr("checked")){
			document.add_form.needPageProductFile.value = 2;
		}

		if($("input:checkbox[name=isMailTag]").attr("checked")){
			document.add_form.needMailTag.value = 2;
		}
		if($("input:checkbox[name=isMessageTag]").attr("checked")){
			document.add_form.needMessageTag.value = 2;
		}
		if($("input:checkbox[name=isPageTag]").attr("checked")){
			document.add_form.needPageTag.value = 2;
		}

		if($("input:checkbox[name=isMailTagThird]").attr("checked")){
			document.add_form.needMailTagThird.value = 2;
		}
		if($("input:checkbox[name=isMessageTagThird]").attr("checked")){
			document.add_form.needMessageTagThird.value = 2;
		}
		if($("input:checkbox[name=isPageTagThird]").attr("checked")){
			document.add_form.needPageTagThird.value = 2;
		}

		if($("input:checkbox[name=isMailStockInSet]").attr("checked")){
			document.add_form.needMailStockInSet.value = 2;
		}
		if($("input:checkbox[name=isMessageStockInSet]").attr("checked")){
			document.add_form.needMessageStockInSet.value = 2;
		}
		if($("input:checkbox[name=isPageStockInSet]").attr("checked")){
			document.add_form.needPageStockInSet.value = 2;
		}

		if($("input:checkbox[name=isMailCertificate]").attr("checked")){
			document.add_form.needMailCertificate.value = 2;
		}
		if($("input:checkbox[name=isMessageCertificate]").attr("checked")){
			document.add_form.needMessageCertificate.value = 2;
		}
		if($("input:checkbox[name=isPageCertificate]").attr("checked")){
			document.add_form.needPageCertificate.value = 2;
		}

		if($("input:checkbox[name=isMailQualityInspection]").attr("checked")){
			document.add_form.needMailQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isMessageQualityInspection]").attr("checked")){
			document.add_form.needMessageQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isPageQualityInspection]").attr("checked")){
			document.add_form.needPageQualityInspection.value = 2;
		}
<%-- 		if($("#adminUserNamesB2BOrder").val() == '')
		{
			alert("运输负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_declaration]:checked") && $("input:radio[name=radio_declaration]:checked").val() && '<%=B2BOrderDeclarationKey.DELARATION %>' == $("input:radio[name=radio_declaration]:checked").val() && $("#adminUserNamesDeclaration").val() == '')
		{
			alert("出口报关负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_clearance]:checked") && $("input:radio[name=radio_clearance]:checked").val() && '<%=B2BOrderClearanceKey.CLEARANCE%>' == $("input:radio[name=radio_clearance]:checked").val() && $("#adminUserNamesClearance").val() == '')
		{
			alert("进口清关负责人不能为空");
			return false;
		}
		else if($("#adminUserNamesProductFile").val() == '')
		{
			alert("实物图片负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_tag]:checked") && $("input:radio[name=radio_tag]:checked").val() && '<%=B2BOrderTagKey.TAG %>' == $("input:radio[name=radio_tag]:checked").val() && $("#adminUserNamesTag").val() == '')
		{
			alert("内部标签负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_tagThird]:checked") && $("input:radio[name=radio_tagThird]:checked").val() && '<%=B2BOrderTagKey.TAG %>' == $("input:radio[name=radio_tagThird]:checked").val() && $("#adminUserNamesTagThird").val() == '')
		{
			alert("第三方标签负责人不能为空");
			return false;
		}
		else if($("input:radio[name=stock_in_set_radio]:checked") && $("input:radio[name=stock_in_set_radio]:checked").val() && '<%=B2BOrderStockInSetKey.SHIPPINGFEE_SET %>' == $("input:radio[name=stock_in_set_radio]:checked").val() && $("#adminUserNamesStockInSet").val() == '')
		{
			alert("运费流程负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_certificate]:checked") && $("input:radio[name=radio_certificate]:checked").val() && '<%=B2BOrderCertificateKey.CERTIFICATE %>' == $("input:radio[name=radio_certificate]:checked").val() && $("#adminUserNamesCertificate").val() == '')
		{
			alert("单证流程负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_qualityInspection]:checked") && $("input:radio[name=radio_qualityInspection]:checked").val() && '<%=B2BOrderQualityInspectionKey.NEED_QUALITY %>' == $("input:radio[name=radio_qualityInspection]:checked").val() && $("#adminUserNamesQualityInspection").val() == '')
		{
			alert("质检流程负责人不能为空");
			return false;
		} --%>
	}
	
	$(document).ready(function()
	{
		$("input:radio[name^=radio_]").click(
				function(){
					var hasThisCheck = $(this).val();
					var hasThisName = $(this).attr("name");
					var presonTrName = hasThisName.split("_")[1]+"PersonTr";
					if(2 == hasThisCheck){
						$("#"+presonTrName).attr("style","");
					}else{
						$("#"+presonTrName).attr("style","display:none");
					}
				}
		);
		$("input:radio[name=stock_in_set_radio]").click(
				function(){
					var hasThisCheck = $(this).val();
					if(2 == hasThisCheck){
						$("#stockInSetPersonTr").attr("style","");
					}else{
						$("#stockInSetPersonTr").attr("style","display:none");
					}
				}
		);		
	});

	function adminUserB2BOrder(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsB2BOrder").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowB2BOrder'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowB2BOrder(user_ids , user_names){
		$("#adminUserIdsB2BOrder").val(user_ids);
		$("#adminUserNamesB2BOrder").val(user_names);
	}
	function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);};
	function adminUserDeclaration(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsDeclaration").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowDeclaration'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowDeclaration(user_ids , user_names){
		$("#adminUserIdsDeclaration").val(user_ids);
		$("#adminUserNamesDeclaration").val(user_names);
	}

	function adminUserClearance(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsClearance").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowClearance'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowClearance(user_ids , user_names){
		$("#adminUserIdsClearance").val(user_ids);
		$("#adminUserNamesClearance").val(user_names);
	}

	function adminUserProductFile(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsProductFile").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowProductFile'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowProductFile(user_ids , user_names){
		$("#adminUserIdsProductFile").val(user_ids);
		$("#adminUserNamesProductFile").val(user_names);
	}

	function adminUserTag(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsTag").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTag'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTag(user_ids , user_names){
		$("#adminUserIdsTag").val(user_ids);
		$("#adminUserNamesTag").val(user_names);
	}
	function adminUserTagThird(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsTagThird").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTagThird'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTagThird(user_ids , user_names){
		$("#adminUserIdsTagThird").val(user_ids);
		$("#adminUserNamesTagThird").val(user_names);
	}
	function adminUserStockInSet(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsStockInSet").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowStockInSet'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowStockInSet(user_ids , user_names){
		$("#adminUserIdsStockInSet").val(user_ids);
		$("#adminUserNamesStockInSet").val(user_names);
	}

	function adminUserCertificate(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsCertificate").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowCertificate'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowCertificate(user_ids , user_names){
		$("#adminUserIdsCertificate").val(user_ids);
		$("#adminUserNamesCertificate").val(user_names);
	}

	function adminUserQualityInspection(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsQualityInspection").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowQualityInspection'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowQualityInspection(user_ids , user_names){
		$("#adminUserIdsQualityInspection").val(user_ids);
		$("#adminUserNamesQualityInspection").val(user_names);
	};
	
	function openFreightSelect() 
	{
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_resources/freight_resources_select.html";
		freightSelectWindow = $.artDialog.open(url, {title: '运输资源选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function setFreight(fr_id,waybill_name,carriers,b2b_orderby,b2b_orderby_name, begin_country, begin_country_name, end_country, end_country_name, begin_port, end_port) 
	{
		$("#fr_id").val(fr_id);
		$("#b2b_order_waybill_name").val(waybill_name);
		$("#carriers").val(carriers);	
		$("#b2b_orderby").val(b2b_orderby);
		$("#b2b_orderby_name").val(b2b_orderby_name);
		$("#b2b_order_send_country").val(begin_country);
		$("#b2b_order_send_country_name").val(begin_country_name);
		$("#b2b_order_receive_country").val(end_country);
		$("#b2b_order_receive_country_name").val(end_country_name);
		$("#b2b_order_send_place").val(begin_port);
		$("#b2b_order_receive_place").val(end_port);
	}
	
	function setFreightCost(fc_id,fc_project_name,fc_way,fc_unit) {
		addFreigthCost(fc_id,fc_project_name,fc_way,fc_unit);
		$('#changed').val('2');
	}
	
	function setRate(obj) {
		if(obj.value == 'RMB') {
			document.getElementsByName("tfc_exchange_rate")[$(obj).attr('index')].value = "1.0";
		}else {
			document.getElementsByName("tfc_exchange_rate")[$(obj).attr('index')].value = "0.0";
		}
	}
	
	function addFreigthCost(fc_id,fc_project_name,fc_way,fc_unit) 
	{
		var index = document.getElementById("freight_cost").rows.length;
    	var row = document.getElementById("freight_cost").insertRow(index);
    	row.insertCell(0).innerHTML = "<input type='hidden' name='tfc_id' id='tfc_id'>"
							    	 +"<input type='hidden' name='tfc_project_name' id='tfc_project_name' value='"+fc_project_name+"'>"
							    	 +"<input type='hidden' name='tfc_way' id='tfc_way' value='"+fc_way+"'>"
							    	 +"<input type='hidden' name='tfc_unit' id='tfc_unit' value='"+fc_unit+"'>"
							    	 +fc_project_name;
		
		row.insertCell(1).innerHTML = fc_way;
		row.insertCell(2).innerHTML = fc_unit;
		row.insertCell(3).innerHTML = "<input type='text' size='8' name='tfc_unit_price' id='tfc_unit_price' value='0.0'>";
		row.insertCell(4).innerHTML = "<select id='tfc_currency' index='"+(index-2)+"' name='tfc_currency' onchange='setRate(this)'>"+createCurrency(fc_unit)+"</select>";
		row.insertCell(5).innerHTML = "<input type='text' size='8' name='tfc_exchange_rate' id='tfc_exchange_rate' value='1.0'>";
		row.insertCell(6).innerHTML = "<input type='text' size='8' name='tfc_unit_count' id='tfc_unit_count' value='0.0'>";
		row.insertCell(7).innerHTML = "<input type='button' value='删除' onclick='deleteFreigthCost(this)'/>";
		//让回来的币种都默认选中
	}
	function createCurrency(fc_unit){
		var array = ['RMB','USD','HKD'];
		var options = "" ;
	 	for(var index = 0 , count = array.length  ; index < count ; index++ ){
			var selected = "" ;
		 	if(fc_unit.toUpperCase() === array[index]){
		 	   selected = "selected";
			}
			options += "<option "+selected+" value='"+array[index]+"'>"+array[index]+"</option>";
		}
		return  options;
    }

    function deleteFreigthCost(input){  
        var s=input.parentNode.parentNode.rowIndex;
        document.getElementById("freight_cost").deleteRow(s); 
        $('#changed').val('2');
        //var num=document.getElementById("tables").rows.length;  
	}

    function selectFreigthCost() {
    	if($("input:radio[name=stock_in_set_radio]:checked").val()==2)
		{
    		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_cost/freight_cost_select.html";
    		$.artDialog.open(url, {title: '运费项目选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
		}
		else
		{
			alert("请确认在流程指派中是否选择了需要运费");
		} 
	}
	
	function uploadFile(_target)
	{
	    var obj  = {
		     reg:"xls",
		     limitSize:2,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		
		$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
				 //close:function()
				 //{
						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 //	 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
				 //}
			 });
	}
	
	function prefillDetailByFile(file_name)
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_detail_upload_show.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{file_name:file_name},
			
			beforeSend:function(request){
			},
			
			error: function(){
			},
			
			success: function(html)
			{
				$("#details").html(html);
				
				addAutoComplete($("#p_name"),
					"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
					"merge_info",
					"p_name");
			}
		});
	}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target)
{
	if('' != fileNames)
	{
    	prefillDetailByFile(fileNames);
	}
}
function changeSendStorageType(obj)
{
	var typeId = obj.value;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/GetStorageCatalogsByTypeAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:{typeId:typeId},
		beforeSend:function(request){
		},
		error: function(){
		},
		
		success: function(data)
		{
			$("#psid").clearAll();
			
			
			if (data!="")
			{
				$.each(data,function(i){
					$("#psid").addOption(data[i].title,data[i].id);
				});
			}
			$("#send_house_number").val("");
			$("#send_street").val("");
			$("#send_zip_code").val("");
			$("#send_name").val("");
			$("#send_linkman_phone").val("");
			$("#send_city").val("");
			$("#send_ccid").val("");
			$("#send_pro_id").val("");
			$("#state_div_send").css("display","none");
			$("#address_state_send").val("");
			//getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id");
			
			$("#ps_list").clearAll();
				var $receiveSecice = $("#ps_list").append($("#psid").html())
			$receiveSecice.multiselect('refresh');
			
		}
	});
	if('<%=StorageTypeKey.SELF%>'*1==typeId*1)
	{
		$("#transportOutDateTr").css("display","");
	}
	else
	{
		$("#transportOutDateTr").css("display","none");
		$("#b2b_order_out_date").val("");
	}
	
}
//更改收货仓库类型
var typeId=0;//收货仓库类型
function changeReceiveStorageType(obj)
{

	 typeId = obj.value;



	//客户仓库
	if(typeId==<%=StorageTypeKey.CUSTOMER%>)
	{

		fillShipTo();
		//显示门店仓库
	
	}
	else
	{
	//不显示门店仓库
	   if($("#ship_to_customer_list_chzn")!=null){
	   $("#ship_to_customer_list_chzn").css("display","none");
	   }

	
		fillReceiveStore(typeId,0);
		
	}
	
	if('<%=StorageTypeKey.SELF%>'*1==typeId*1)
	{
		$("#transportArrrivalDateTr").css("display","");
	}
	else
	{
		$("#transportArrrivalDateTr").css("display","none");
		$("#b2b_order_receive_date").val("");
	}
	
}


function changeProIdDeliver(obj)
{
	stateDivDeliver(obj.value, "");
}
function changeProIdSend(obj)
{
	stateDivSend(obj.value, "");
}

function fillShipTo()
{


	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminsitrator/shipTo/getAllShipToJson.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			beforeSend:function(request){
			},
			error: function(){
			},
			
			success: function(data)
			{
			
				if (data!="")
				{
				
	
				$("#receive_psid_list").clearAll();
					$.each(data,function(i){
						$("#receive_psid_list").addOption(data[i].ship_to_name,data[i].ship_to_id);
					});
				}
				$("#deliver_house_number").val("");
				$("#deliver_street").val("");
				$("#deliver_zip_code").val("");
				$("#deliver_name").val("");
				$("#deliver_linkman_phone").val("");
				$("#deliver_city").val("");
				$("#deliver_ccid").val("");
				$("#deliver_pro_id").val("");
				$("#state_div_deliver").css("display","none");
				$("#address_state_deliver").val("");
			    $("#receive_psid_list").chosen();
				$("#receive_psid_list").trigger("liszt:updated");
				   $("#deliver_pro_id").trigger("liszt:updated");	
			   	 $("#deliver_ccid").trigger("liszt:updated");	
			
				
			}
		});
		
}

function fillReceiveStore(storage_type,ship_to_id)
{
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>/action/adminsitrator/product_storage/getStorageCatalogJsonByTypeShipTo.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:{
				storage_type:storage_type,
				ship_to_id:ship_to_id
			},
			beforeSend:function(request){
			},
			error: function(){
			},
			
			success: function(data)
			{
				$("#receive_psid_list").clearAll();
				$("#receive_psid_list").addOption("请选择......",-1);
				if (data!="")
				{
				
					$.each(data,function(i){
						$("#receive_psid_list").addOption(data[i].title,data[i].id);
					});
				}
				$("#deliver_house_number").val("");
				$("#deliver_street").val("");
				$("#deliver_zip_code").val("");
				$("#deliver_name").val("");
				$("#deliver_linkman_phone").val("");
				$("#deliver_city").val("");
				$("#deliver_ccid").val("");
				$("#deliver_pro_id").val("");
				$("#state_div_deliver").css("display","none");
				$("#address_state_deliver").val("");
				$("#receive_psid_list").chosen();
				$("#receive_psid_list").trigger("liszt:updated");
			    $("#deliver_pro_id").trigger("liszt:updated");	
			   	 $("#deliver_ccid").trigger("liszt:updated");	
				
		
			}
		});
}



	

	//当仓库类型为客户仓库，存在二级仓库，更改一级仓库，联动二级仓库
   function changeShipTo(obj) {
      var shipToId=obj.value;
     $.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminsitrator/shipTo/getAllShipToCustomerJson.action?typeId='+typeId+'&shipToId='+shipToId,
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			beforeSend:function(request){
			},
			error: function(){
			},
			
			success: function(data)
			{
				if (data!="")
				{    $("#ship_to_customer_list").clearAll();
				     $("#ship_to_customer_list").addOption("请选择......",-1);
					$.each(data,function(i){
						$("#ship_to_customer_list").addOption(data[i].title,data[i].id);
					});
				}
					$("#deliver_house_number").val("");
				$("#deliver_street").val("");
				$("#deliver_zip_code").val("");
				$("#deliver_name").val("");
				$("#deliver_linkman_phone").val("");
				$("#deliver_city").val("");
				$("#deliver_ccid").val("");
				$("#deliver_pro_id").val("");
				$("#state_div_deliver").css("display","none");
				$("#address_state_deliver").val("");
				$("#ship_to_customer_list_chzn").css("display","");
				$("#ship_to_customer_list").chosen();
			    $("#ship_to_customer_list").trigger("liszt:updated");	
			    $("#deliver_pro_id").trigger("liszt:updated");	
			   	$("#deliver_ccid").trigger("liszt:updated");	
			    
			}
		});
   
     
   }
   //改变客户的二级仓库，带出仓库属性
   function changeCutomerStorage(obj){
   var ps_id = obj.value;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{ps_id:ps_id},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败");
				},
				success: function(data){
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					//$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.deliver_city);

					$("#deliver_ccid").val(data["deliver_nation"]);

					getStorageProvinceByCcid($("#deliver_ccid").val(),data["deliver_pro_id"],"deliver_pro_id", data.deliver_pro_input);
					
					$("#deliver_name").val(data["deliver_contact"]);
					$("#deliver_linkman_phone").val(data["deliver_phone"]);
					$("#receive_psid").val($("#ship_to_customer_list").val());
					$("#deliver_pro_id").trigger("liszt:updated");	
			     	$("#deliver_ccid").trigger("liszt:updated");	
				}
			});

   }

	


   $(function ($) {
             $("#deliver_ccid").chosen();//收货国家
             $("#storagetype").chosen();//收货仓库类型
             $("#deliver_pro_id").chosen();//收货省份
             $("#title_id").chosen();
    
    // 改变仓库类型
       $("#storagetype").change(function(){
      
          changeReceiveStorageType(this);
        });
        //改变一级仓库
         $("#receive_psid_list").change(function(){
          selectDeliveryStorage(this);
        });
         //改变收货国家
         $("#deliver_ccid").change(function(){
          getStorageProvinceByCcid(this.value,0,'deliver_pro_id');
        });
    }); 

</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive();">
<!--form name="add_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/B2BOrderAddAction.action"-->

<form name="add_form" id="add_form"  action="<%=ConfigBean.getStringValue("systenFolder")%>app/b2bOrder" method="post">
<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_detail.html"/>
<!-- 各流程 -->
<input type="hidden" name="declaration"/>
<input type="hidden" name="clearance"/>
<input type="hidden" name="product_file"/>
<input type="hidden" name="tag"/>
<input type="hidden" name="tag_third"/>
<input type="hidden" name="stock_in_set"/>
<input type="hidden" name="certificate"/>
<input type="hidden" name="quality_inspection"/>

<!-- 负责人的Ids -->
<input type="hidden" name="adminUserIdsB2BOrder" id="adminUserIdsB2BOrder" value='<%=adminUserIdsB2BOrder %>'/>
<input type="hidden" name="adminUserIdsDeclaration" id="adminUserIdsDeclaration" value='<%=adminUserIdsDeclaration %>'/>
<input type="hidden" name="adminUserIdsClearance" id="adminUserIdsClearance" value='<%=adminUserIdsClearance %>'/>
<input type='hidden' name="adminUserIdsProductFile" id="adminUserIdsProductFile" value='<%=adminUserIdsProductFile %>'/>
<input type="hidden" name="adminUserIdsTag" id="adminUserIdsTag" value='<%=adminUserIdsTag %>'/>
<input type="hidden" name="adminUserIdsTagThird" id="adminUserIdsTagThird" value='<%=adminUserIdsTagThird %>'/>
<input type="hidden" name="adminUserIdsStockInSet" id="adminUserIdsStockInSet" value='<%=adminUserIdsStockInSet %>'/>
<input type="hidden" name="adminUserIdsCertificate" id="adminUserIdsCertificate" value='<%=adminUserIdsCertificate %>'/>
<input type="hidden" name="adminUserIdsQualityInspection" id="adminUserIdsQualityInspection" value='<%=adminUserIdsQualityInspection %>'/>


<!-- 各流程，是否发各类通知 -->
<input type="hidden" name="needMailB2BOrder"/>
<input type="hidden" name="needMessageB2BOrder"/>
<input type="hidden" name="needPageB2BOrder"/>

<input type="hidden" name="needMailDeclaration"/>
<input type="hidden" name="needMessageDeclaration"/>
<input type="hidden" name="needPageDeclaration"/>

<input type="hidden" name="needMailClearance"/>
<input type="hidden" name="needMessageClearance"/>
<input type="hidden" name="needPageClearance"/>

<input type="hidden" name="needMailProductFile"/>
<input type="hidden" name="needMessageProductFile"/>
<input type="hidden" name="needPageProductFile"/>

<input type="hidden" name="needMailTag"/>
<input type="hidden" name="needMessageTag"/>
<input type="hidden" name="needPageTag"/>

<input type="hidden" name="needMailTagThird"/>
<input type="hidden" name="needMessageTagThird"/>
<input type="hidden" name="needPageTagThird"/>

<input type="hidden" name="needMailStockInSet"/>
<input type="hidden" name="needMessageStockInSet"/>
<input type="hidden" name="needPageStockInSet"/>

<input type="hidden" name="needMailCertificate"/>
<input type="hidden" name="needMessageCertificate"/>
<input type="hidden" name="needPageCertificate"/>

<input type="hidden" name="needMailQualityInspection"/>
<input type="hidden" name="needMessageQualityInspection"/>
<input type="hidden" name="needPageQualityInspection"/>
<input  type="hidden" name="receive_psid" id="receive_psid">
<!-- 运费项 -->
<input type="hidden" name="changed" id="changed" value="1">
	
	<div class="demo" align="center" style="height:100%;">
		<table width="100%" style="height:100%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td valign="top">
					 <div id="tabs" style="width: 98%">
				  	    <ul>
				  	      <li><a href="#receive">收货地址</a></li>
						  <li><a href="#other">其他</a></li>
						  <li><a href="#details">货物列表</a></li>
						  <li><a href="#procedures">流程指派</a></li>
						  <li><a href="#freight">运输设置</a></li>
						  <li><a href="#freightcost">运费设置</a></li>
						</ul>
					<div id="receive">
						<table width="95%" align="center" cellpadding="2" cellspacing="5">
							<tr>
								<td nowrap width="20%" align="right">title</td>
								<td width="80%">
									<select name="title_id" id="title_id" class="chzn-select" style="width: 150px;">
										<option value="-1">请选择</option>
										<%
											for(int i = 0; i < titles.length; i ++)
											{
										%>
											<option value='<%=titles[i].get("title_id", 0L) %>'><%=titles[i].getString("title_name") %></option>
										<%
										}
										%>
									</select>
								</td>
							</tr>
									 	<tr valign="bottom">
												  <td nowrap align="right">收货仓库</td>
											      <td nowrap>
											      <!-- 仓库类型 -->
											      <select  id="storagetype" name="storage_type"  class="chzn-select" style="width: 100px;">
									 					<option value="-1">请选择</option>
											      		<%
											      			for(int i = 0; i < storageTypes.size(); i ++)
											      			{
											      		%>
											      			<option value='<%=storageTypes.get(i) %>'><%=storageTypeKey.getStorageTypeKeyName(storageTypes.get(i)) %></option>
											      		<%		
											      			}
											      		%>
											      	</select>
											
					                              <!-- 一级仓库 -->
											      	<select  id="receive_psid_list" style="display:none;width: 120px;" name="receive_ps_list"   class="chzn-select"  onchange="selectDeliveryStorage(this)"></select>
					                             <!-- 仓库类型为客户仓库的二级仓库 -->
					                          	<select  id="ship_to_customer_list" style="display:none;width: 300px;" name="ship_to_customer_list"  class="chzn-select"   onchange="changeCutomerStorage(this)">
					                          	    <option value="-1" selected="selected">请选择</option>					                         	
					                          	</select>
											      </td>
										</tr>
									 	<tr>
									 		<td nowrap align="right">收货国家</td>
									 		<td>
										      <select name="deliver_ccid" id="deliver_ccid" class="chzn-select" >
											  <option value="0">请选择...</option>
											  <%
											  for (int i=0; i<countrycode.length; i++)
											  {
											  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
												{
											
												}  	
												
												preLetter = countrycode[i].getString("c_country").substring(0,1);
											  %>
											    <option   value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
											  <%
											  }
											  %>
										      </select>		
									 		</td>
									 	</tr>
									 	<tr>
									 		<td nowrap align="right">收货省份</td>
									 		<td>
									 			<select name="deliver_pro_id" id="deliver_pro_id" class="chzn-select" style="width: 200px" onchange="changeProIdDeliver(this)">
											    </select>		
											      	<div style="padding-top: 10px;display:none;" id="state_div_deliver">
														<input type="text" name="address_state_deliver" id="address_state_deliver"/>
													</div>
									 		</td>
									 	</tr>
									 	<tr>
									 		<td  nowrap align="right">收货地址门牌号</td>
									 		<td><input style="width: 300px;" type="text" name="deliver_house_number" id="deliver_house_number"/></td>
									 	</tr>
									 	<tr>
									 		<td  nowrap align="right">收货地址街道</td>
									 		<td><input style="width: 300px;" type="text" name="deliver_street" id="deliver_street"/></td>
									 	</tr>
									 	<tr>
									 		<td nowrap align="right">收货城市</td>
									 		<td><input style="width: 300px;" type="text" name="deliver_city" id="deliver_city"/></td>
									 	</tr>
									 	<tr>
									 		<td nowrap align="right">收货地址邮编</td>
									 		<td><input style="width: 300px;" type="text" name="deliver_zip_code" id="deliver_zip_code"/></td>
									 	</tr>
									 	<tr>
									 		<td nowrap align="right">收货联系人</td>
									 		<td><input style="width: 300px;" type="text" name="deliver_name" id="deliver_name"/></td>
									 	</tr>
									 	<tr>
									 		<td nowrap align="right">收货联系人电话</td>
									 		<td><input style="width: 300px;" type="text" name="deliver_linkman_phone" id="deliver_linkman_phone"/></td>
									 	</tr>
									 	<tr id="transportArrrivalDateTr" style="display:none;">
									 		<td align="right">ETA</td>
									 		<td><input width="80%" type="text" name="b2b_order_receive_date" id="b2b_order_receive_date" value=""></td>
									 	</tr>
									 </table>
					</div>
					<div id="other">
						<table width="95%" align="center" cellpadding="2" cellspacing="5">
							<tr>
								<td width="20%" align="right">备注</td>
								<td width="80%">
							 		<textarea rows="2" cols="70" name="remark" id="remark"></textarea>
								</td>
							</tr>
						
						</table>
					</div>
					
					<div id="details">
						参考模板:<span class="STYLE12"><a href="b2b_order_template.xls">下载</a></span>
						<input type="button" class="long-button" onclick="uploadFile('');" value="上传货物列表"/>
						<input type="hidden" id="import_submit" value="true"/>
					</div>
					<div id="procedures">
						<table width="100%" border="0" cellspacing="5" cellpadding="0">
							<tr height="25px" id="transportPersonTr">
									<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">运输负责人：</td>
								    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
								    	<input type="text" id="adminUserNamesB2BOrder" name="adminUserNamesB2BOrder" style="width:180px;" onclick="adminUserB2BOrder()" value='<%=adminUserNamesB2BOrder %>'/>
								    	通知：
								   		<input type="checkbox" name="isMailB2BOrder" checked="checked"/>邮件
								   		<input type="checkbox" name="isMessageB2BOrder" checked="checked"/>短信
								   		<input type="checkbox" name="isPageB2BOrder" checked="checked"/>页面
									</td>
							</tr>
							<tr height="25px" id="transportPersonTr">
									<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">注：</td>
								    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
								    	请选择产品经理或海运负责人
									</td>
							</tr>
							<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
					        <tr height="25px">
					          <td width="13%" align="right" class="STYLE2">实物图片:</td>
					          <td width="87%">
					          	 	<input type="text" id="adminUserNamesProductFile" name="adminUserNamesProductFile" style="width:180px;" onclick="adminUserProductFile()" value='<%=adminUserNamesProductFile %>'/>
							    	通知：
							   		<input type="checkbox" name="isMailProductFile" checked="checked"/>邮件
							   		<input type="checkbox" name="isMessageProductFile" checked="checked"/>短信
							   		<input type="checkbox" name="isPageProductFile" checked="checked"/>页面
					          </td>
					        </tr>
							<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
					        <tr height="25px">
				        	  <td width="13%" align="right" class="STYLE2">内部标签:</td>
					          <td width="87%" align="left" valign="changeReceiveStorageType(this)transportTr">
					   
					          	<input type="radio" name="radio_tag" value="1"/> <%= b2BOrderTagKey.getB2BOrderTagById(1) %>
					          	<input type="radio" name="radio_tag" value="2" checked="checked"/> <%= b2BOrderTagKey.getB2BOrderTagById(2) %>
					          </td>
					        </tr>
					       
					        <tr height="25px" id="tagPersonTr">
								<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
							    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
							    	<input type="text" id="adminUserNamesTag" name="adminUserNamesTag" style="width:180px;" onclick="adminUserTag()" value='<%=adminUserNamesTag %>'/>
							    	通知：
							    	
							   		<input type="checkbox" name="isMailTag" checked="checked"/>邮件
							   		<input type="checkbox" name="isMessageTag" checked="checked"/>短信
							   		<input type="checkbox" name="isPageTag" checked="checked"/>页面
								</td>
							</tr>
							<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
					        <tr height="25px">
					          <td width="13%" align="right" class="STYLE2">质检流程:</td>
					          <td width="87%" align="left" valign="middle" id="qualityInspectionTr">
					          <input type="radio" name="radio_qualityInspection" value="1"/> 无需质检报告
					          <input type="radio" name="radio_qualityInspection" value="2" checked="checked"/>需要质检报告
					          </td>
					        </tr>
					        <tr height="25px;" id="qualityInspectionPersonTr">
								<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
							    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
							    	<input type="text" id="adminUserNamesQualityInspection" name="adminUserNamesQualityInspection" style="width:180px;" onclick="adminUserQualityInspection()" value='<%=adminUserNamesQualityInspection %>'/>
							    	通知：
							   		<input type="checkbox" name="isMailQualityInspection" checked="checked"/>邮件
							   		<input type="checkbox" name="isMessageQualityInspection" checked="checked"/>短信
							   		<input type="checkbox" name="isPageQualityInspection" checked="checked"/>页面
								</td>
							</tr>
							<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
					        <tr height="25px">
					        	   <td width="13%" align="right" class="STYLE2">出口报关:</td>
					         	   <td width="87%" align="left" valign="middle" id="declarationTr">
					         			<input type="radio" name="radio_declaration" value="1"/> <%=b2BOrderDeclarationKey.getB2BOrderDeclarationKeyById(1)%>
					          			<input type="radio" name="radio_declaration" value="2" checked="checked"/> <%= b2BOrderDeclarationKey.getB2BOrderDeclarationKeyById(2) %>
					      		  </td>
					      	</tr>
							<tr height="25px" id="declarationPersonTr">
								<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
							    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
							    	<input type="text" id="adminUserNamesDeclaration" name="adminUserNamesDeclaration" style="width:180px;" onclick="adminUserDeclaration()" value='<%=adminUserNamesDeclaration %>'/>
							    	通知：
							   		<input type="checkbox" name="isMailDeclaration" checked="checked"/>邮件
							   		<input type="checkbox" name="isMessageDeclaration" checked="checked"/>短信
							   		<input type="checkbox" name="isPageDeclaration" checked="checked"/>页面
								</td>
							</tr>
							<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
							<tr height="25px">
				        		<td width="13%" align="right" class="STYLE2">进口清关:</td>
						        <td width="87%" align="left" valign="middle" id="clearanceTr">
						         <input type="radio" name="radio_clearance" value="1" checked="checked"/> <%= b2BOrderClearanceKey.getB2BOrderClearanceKeyById(1) %>
						         <input type="radio" name="radio_clearance" value="2" checked="checked"/> <%= b2BOrderClearanceKey.getB2BOrderClearanceKeyById(2) %>					      
								</td>
					        </tr>
					        
					        <tr height="25px" id="clearancePersonTr">
								<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
							    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
							    	<input type="text" id="adminUserNamesClearance" name="adminUserNamesClearance" style="width:180px;" onclick="adminUserClearance()" value='<%=adminUserNamesClearance %>'/>
							    	通知：
							   		<input type="checkbox" name="isMailClearance" checked="checked"/>邮件
							   		<input type="checkbox" name="isMessageClearance"  checked="checked"/>短信
							   		<input type="checkbox" name="isPageClearance"  checked="checked"/>页面
								</td>
							</tr>
							<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
					        <tr height="25px">
					       	  <td width="13%" align="right" class="STYLE2">运费流程:</td>
					          <td width="87%" align="left" valign="middle" id="stockInSetTr">
					          	 <input type="radio" name="stock_in_set_radio" value="1"/> <%= b2BOrderStockInSetKey.getB2BStockInSetKeyById(1) %>
					          	 <input type="radio" name="stock_in_set_radio" value="2" checked="checked"/> <%= b2BOrderStockInSetKey.getB2BStockInSetKeyById(2) %>	
					          </td>
					        </tr>
					        <tr height="25px" id="stockInSetPersonTr">
								<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
							    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
							    	<input type="text" id="adminUserNamesStockInSet" name="adminUserNamesStockInSet" style="width:180px;" onclick="adminUserStockInSet()" value='<%=adminUserNamesStockInSet %>'/>
							    	通知：
							   		<input type="checkbox" name="isMailStockInSet" checked="checked"/>邮件
							   		<input type="checkbox" name="isMessageStockInSet" checked="checked"/>短信
							   		<input type="checkbox" name="isPageStockInSet" checked="checked"/>页面
								</td>
							</tr>
							<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
					        <tr height="25px">
					          <td width="13%" align="right" class="STYLE2">单证:</td>
					          <td width="87%" align="left" valign="middle" id="certificateTr">
					          	<input type="radio" name="radio_certificate" value="1"/> <%=b2BOrderCertificateKey.getB2BOrderCertificateKeyById(1)%>
					          	<input type="radio" name="radio_certificate" value="2" checked="checked"/> 需要单证
					          </td>
					        </tr>
					        <tr height="25px" id="certificatePersonTr">
								<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
							    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
							    	<input type="text" id="adminUserNamesCertificate" name="adminUserNamesCertificate" style="width:180px;" onclick="adminUserCertificate()" value='<%=adminUserNamesCertificate %>'/>
							    	通知：
							   		<input type="checkbox" name="isMailCertificate" checked="checked"/>邮件
							   		<input type="checkbox" name="isMessageCertificate" checked="checked"/>短信
							   		<input type="checkbox" name="isPageCertificate" checked="checked"/>页面
								</td>
							</tr>
							<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
					        <tr height="25px">
				        	  <td width="13%" align="right" class="STYLE2">第三方标签:</td>
					          <td width="87%" align="left" valign="middle" id="transportTrThird">
					          	<input type="radio" name="radio_tagThird" value="1"/> <%= b2BOrderTagKey.getB2BOrderTagById(1) %>
					          	<input type="radio" name="radio_tagThird" value="2" checked="checked"/> <%= b2BOrderTagKey.getB2BOrderTagById(2) %>
					          </td>
					        </tr>
					       
					        <tr height="25px" id="tagThirdPersonTr">
								<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
							    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
							    	<input type="text" id="adminUserNamesTagThird" name="adminUserNamesTagThird" style="width:180px;" onclick="adminUserTagThird()" value='<%=adminUserNamesTagThird %>'/>
							    	通知：
							    	
							   		<input type="checkbox" name="isMailTagThird" checked="checked"/>邮件
							   		<input type="checkbox" name="isMessageTagThird" checked="checked"/>短信
							   		<input type="checkbox" name="isPageTagThird" checked="checked"/>页面
								</td>
							</tr>
						</table>
					</div>
					<div id="freight">
						<input type="hidden" name="fr_id" id="fr_id"/>
						<table width="100%" border="0" cellspacing="5" cellpadding="0">
					        <tr height="29">
					          <td align="right" class="STYLE2">货运公司:</td>
					          <td>&nbsp;</td>
					          <td align="left" valign="middle">
					          	<input type="text" name="b2b_order_waybill_name" id="b2b_order_waybill_name" readonly="readonly"><br/>
					          	<input type="button" class="long-button" name="waybillSelect" id="waybillSelect" value="选择运输资源" onclick="openFreightSelect()">
					          </td>
					          <td align="right" class="STYLE2">运单号</td>
					          <td>&nbsp;</td>
					          <td align="left" valign="middle">
					          	<input type="text" name="b2b_order_waybill_number" id="b2b_order_waybill_number">
					          </td>
					        </tr>
					        <tr height="29">
					          <td align="right" class="STYLE2">运输方式:</td>
					          <td>&nbsp;</td>
					          <td align="left" valign="middle">
					          	<input type="hidden" name="b2b_orderby" id="b2b_orderby">
					          	<input type="text" name="b2b_orderby_name" id="b2b_orderby_name" readonly="readonly">
					          </td>
					          <td align="right" class="STYLE2">承运公司:</td>
					          <td>&nbsp;</td>
					          <td align="left" valign="middle">
					          	<input type="text" name="carriers" id="carriers" readonly="readonly">
					          </td>
					        </tr>
					        <tr height="29" id="tr1" name="tr1">
					          <td align="right" class="STYLE2">始发港:</td>
					          <td>&nbsp;</td>
					          <td align="left" valign="middle">
					          	<input type="text" name="b2b_order_send_place" id="b2b_order_send_place" readonly="readonly">
					          </td>
					          <td align="right" class="STYLE2">收货港:</td>
					          <td>&nbsp;</td>
					          <td align="left" valign="middle">
					          	<input type="text" name="b2b_order_receive_place" id="b2b_order_receive_place" readonly="readonly">
					          </td>
					        </tr>
						</table>
					</div>
					<div id="freightcost">
						<table id="freight_cost" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" style="padding-top: 17px" class="zebraTable">
						 <tr>
					     	<td align="right" colspan="8" height="30px"><input id="select_freight_cost" type="button" class="long-button" name="selectFreightCost" value="选择运费项目" onclick="selectFreigthCost()"></td>
					     </tr>
					     <tr>
					       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
					       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
					       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
					       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
					       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
					       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
					       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>
					       <th width="25%" nowrap="nowrap" class="right-title"  style="text-align: center;"></th>	
					     </tr>
					    </table>
					</div>
				</div>
				</td>
			</tr>
			<tr>
				<td >
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top:10px">
						<tr >
							<td align="right" >
								<input id="back_button" type="button" style="font-weight: bold" class="normal-green" onClick="back()" value="上一步"/>
								&nbsp;&nbsp;&nbsp;
								<input id="next_button" type="button" style="font-weight: bold" class="normal-green" onClick="next()" value="下一步"/>
								&nbsp;&nbsp;&nbsp;
								<input type="button" style="font-weight: bold" class="normal-green" onClick="submitCheck()" value="提交"/>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
					</table>	
				</td>
			</tr>
				
		</table>					
	</div>
      </form>
      <script type="text/javascript">
		
		var tabsIndex = 0;//当前tabs的Index
		var tab = $("#tabs").tabs({
		cache: false,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		load: function(event, ui) {onLoadInitZebraTable();}	,
		selected:0,
		show:function(event,ui)
			 {
			 	if(ui.index==1)
			 	{
				 	if(0 != $("#psid").val())
				 	{
				 		if(checkSend()==false)
				 		{
				 			tabSelect(0);
				 		}
				 	}
			 		
			 	}
			 	if(ui.index==2)
			 	{
			 		if(checkReceive()==false)
			 		{
			 			tabSelect(1);
			 		}
			 	}
			 	if(ui.index==3)
			 	{
			 		if(-1 == $("#title_id").val())
					{
						alert("请选择title");
						tabSelect(2);
					}
			 	}
			 	
			 	if(ui.index==5)
			 	{
			 		if(checkProcedures()==false)
			 		{
			 			tabSelect(4)
			 		}
			 	}
			 	
			 	tabsIndex = ui.index;
			 	if(tabsIndex==0)
			 	{
			 		$("#back_button").attr("disabled",true);
			 	}
			 	else
			 	{
			 		$("#back_button").removeAttr("disabled");
			 	}
			 }
		});
		
		function countCheck(obj)
		{
			var count = obj.value;
			var countFloat = parseFloat(count);
			
			if(countFloat<0||countFloat!=count)
			{
				alert("数量填写错误");
				$(obj).css("backgroundColor","red");
				return false;
			}
			else
			{
				$(obj).css("backgroundColor","");
			}
		}
		
		function submitCheck()
		{
		    	if('<%=StorageTypeKey.SELF%>'*1==typeId*1)
		    	
	             {
	         
	                if($("#b2b_order_receive_date").val().length<=0){
	                    alert("ETA不能为空");
	                    return false;
	                }
	             
	              }
			
			if(0 != $("#psid").val() && checkSend()==false)
			{
			 	tabSelect(0);
			}
			else if(checkReceive()==false)
			{
			 	tabSelect(1);
			}
			else if(checkProcedures()==false)
			{
			 	tabSelect(4)
			}
			else if(-1 == $("#title_id").val())
			{
				alert("请选择title");
				tabSelect(2);
			}
			else
			{
				var isSubmit = $("#import_submit").val();
				
				if(isSubmit=="true")
				{
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
					//var dataformJson =JSON.stringify($('#add_form').serializeObject());  
					//var dataform = $('#add_form').serializeObject();
					//console.log(dataformJson);
					//ajaxB2BOrder(dataformJson);
					if (confirm("ajax or form"))
				    {
						var dataformJson =JSON.stringify($('#add_form').serializeObject());  
						var dataform = $('#add_form').serializeObject();
						//console.log(dataformJson);
						ajaxB2BOrder(dataformJson);
				    }
				  	else
				    {
				  		document.add_form.submit();
				    }
				}
				else
				{
					tabSelect(3)
				}
				
			}
		}
		
		function next()
		{
			var index = tabsIndex+1;
			tabSelect(index);
		}
		
		function back()
		{
			var index = tabsIndex-1;
			tabSelect(index);
		}
		
		function ajaxB2BOrder(dataForm)
		{
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>app/b2bOrder',
				type: 'post',
				contentType: "application/json",
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:dataForm,
				beforeSend:function(request)
				{
				},
				error: function(e){
					alert("请求失败");
				},
				success: function(data)
				{
					$.artDialog.close();
				}
			});
		}
		
		function tabSelect(index)
		{
			$("#tabs").tabs("select",index);
		}
	</script>

</body>
</html>
