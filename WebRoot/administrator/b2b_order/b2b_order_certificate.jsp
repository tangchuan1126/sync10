<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.B2BOrderCertificateKey"%>
 <%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>单证流程处理</title>
<%
	String valueCertificate = systemConfig.getStringConfigValue("b2b_order_certificate");
	String[] arraySelectedCertificate = valueCertificate.split("\n");
	//将arraySelected组成一个List
	ArrayList<String> selectedListCertificate= new ArrayList<String>();
	for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
			String tempStr = arraySelectedCertificate[index];
			if(tempStr.indexOf("=") != -1){
				String[] tempArray = tempStr.split("=");
				String tempHtml = tempArray[1];
				selectedListCertificate.add(tempHtml);
			}
	}
	long b2b_oid = StringUtil.getLong(request,"b2b_oid"); 
	DBRow b2BOrderRow = b2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
 
	String file_with_class = StringUtil.getString(request,"file_with_class");
	DBRow[] imageListCertificate = fileMgrZJ.getFileByWithIdAndType(b2b_oid, FileWithTypeKey.B2B_ORDER_CERTIFICATE);

 	Map<String,List<DBRow>> mapCertificate = new HashMap<String,List<DBRow>>();
 	 
 	 
	if(imageListCertificate != null && imageListCertificate.length > 0){
		//map<string,List<DBRow>>
		for(int index = 0 , count = imageListCertificate.length ; index < count ;index++ ){
			DBRow temp = imageListCertificate[index];
			List<DBRow> tempListRow = mapCertificate.get(temp.getString("file_with_class"));
			if(tempListRow == null){
				tempListRow = new ArrayList<DBRow>();
			}
			tempListRow.add(temp);
			mapCertificate.put(temp.getString("file_with_class"),tempListRow);
		}
	}
	 
	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	B2BOrderCertificateKey b2BOrderCertificateKey = new B2BOrderCertificateKey();
	String updateTransportAction =   ConfigBean.getStringValue("systenFolder") +"action/administrator/b2b_order/B2BOrderCertificateCompleteAction.action";
	String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/b2b_order/b2b_order_certificate.html?b2b_oid="+b2b_oid ;
	String b2BOrderCertificateFollowUp =   ConfigBean.getStringValue("systenFolder") +"action/administrator/b2b_order/B2BOrderCertificateFollowUpAction.action";
	TDate tdate = new TDate();
%>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
 
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
 <!-- 文件预览  -->
 <script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<script type="text/javascript">
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
jQuery(function($){
	 
	//选择下面的tab上面也跟着切换
	$("#tabs").tabs({
 		cache: true,
		cookie: { expires: 30000 } ,
		select: function(event, ui){
			 $("#file_with_class option[value='"+(ui.index+1)+"']").attr("selected",true);
		}
 	 });
	 if('<%= file_with_class%>'.length > 0){
		 $("#tabs").tabs( "select" , '<%= file_with_class%>' * 1-1 );
		 $("#file_with_class option[value='"+'<%= file_with_class%>'+"']").attr("selected",true);
	}
	 fileWithClassChange();
})
function deleteFile(file_id , tableName , folder,pk){
	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:tableName,file_id:file_id,folder:folder,pk:pk},
		beforeSend:function(request){
     		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
			$.unblockUI();
			if(data && data.flag === "success"){
				window.location.reload();
			}else{
				showMessage("系统错误,请稍后重试","error");
			}
		},
		error:function(){
			showMessage("系统错误,请稍后重试","error");
		}
})
}
function downLoad(fileName , tableName , folder){
	 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ folder);
}
function cerficateCompelte(){
	$.artDialog.confirm('单证流程完成后文件只能上传不能删除,确认继续吗？', function(){
		$.ajax({
			url:'<%= updateTransportAction%>',
			data:{b2b_oid:'<%= b2b_oid%>',b2b_order_date:'<%= b2BOrderRow.getString("b2b_order_date")%>'},
			dataType:'json',
			beforeSend:function(request){
	     		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
				$.unblockUI();
				if(data && data.flag == "success"){
					window.location.reload();
				}else{showMessage("系统错误,请稍后重试","alert");}
			},
			error:function(){
			  showMessage("系统错误,请稍后重试","alert");
			}
		})
	}, function(){
	});
}
function fileWithClassChange(){
   	var file_with_class = $("#file_with_class").val();
   	$("#tabs").tabs( "select" , file_with_class * 1-1 );
}
function followUpCerficate(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/b2b_order/b2b_order_certificate_follow_up.html?b2b_oid="+<%= b2b_oid%>;
	$.artDialog.open(uri , {title: "单证跟进["+<%= b2b_oid%>+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
function refreshWindow(){window.location.reload();}
function showSingleLogs(b2b_oid,b2b_type){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/b2b_order/b2b_order_show_single_log.html?b2b_oid="+<%= b2b_oid%>+"&b2b_type="+b2b_type;
	$.artDialog.open(uri , {title: "实物图片日志["+<%= b2b_oid%>+"]",width:'870px',height:'470px', lock: true,opacity: 0.3,fixed: true});	
}
//文件上传
//做成文件上传后,然后页面刷新提交数据如果是有添加文件
  function uploadFile(_target){
      var targetNode = $("#"+_target);
      var fileNames = $("input[name='file_names']").val();
      var obj  = {
  	     reg:"all",
  	     limitSize:2,
  	     target:_target
  	 }
      var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
  	uri += jQuery.param(obj);
  	 if(fileNames && fileNames.length > 0 ){
  		uri += "&file_names=" + fileNames;
  	}
  	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
      		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
     		 }});
  }
//jquery file up 回调函数
  function uploadFileCallBack(fileNames,target){
      $("input[name='file_names']").val(fileNames);
      if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
          $("input[name='file_names']").val(fileNames);
          var myform = $("#uploadImageForm");
          var file_with_class = $("#file_with_class").val();
			$("#backurl").val("<%= backurl%>" + "&file_with_class="+file_with_class);
		  	myform.submit();
  	}
  }
  jQuery(function($){
		var certificate = $("#certificate");
		$("#certificate option[value='<%= b2BOrderRow.get("certificate",B2BOrderCertificateKey.CERTIFICATE)%>']").attr("selected",true);
		$('#eta').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
		 
			onSelect:function(dateText, inst){

				setContentHtml(dateText);
				
			}
			
		});

		changeCertificateType();
		$("#ui-datepicker-div").css("display","none");
	})
	function changeCertificateType(){
		var certificate = $("#certificate").val();
		if(certificate * 1 == '<%= B2BOrderCertificateKey.FINISH %>' * 1){
		   var  notfinish_span = $("#notfinish_span");
		   notfinish_span.css("display","none");
		}else{
		    var  notfinish_span = $("#notfinish_span");
			notfinish_span.attr("style","");
		}
		setContentHtml();
	}
	function setContentHtml(){
		var certificateHTML = $.trim($("#certificate option:selected").html());
		var html = "";
		if($("#certificate").val() * 1 == '<%= B2BOrderCertificateKey.FINISH%>'){
			html = "[单证采集完成]完成:"
		}else{
			html = "["+certificateHTML+"]阶段预计"+$("#eta").val()+"完成:";
		}
		 
		var contentNode = $("#context");
		var contentNodeValue = contentNode.val();
		 
		if(contentNodeValue.indexOf("完成:") != -1){
			var index = contentNodeValue.indexOf("完成:")
			html += contentNodeValue.substr(index +3);
			$("#context").val(html);
		}else{
			$("#context").val(html + contentNodeValue);
		}
	}
	function submitForm(){
	 
		var myform = $("#myform");
		$.ajax({
			url:'<%= b2BOrderCertificateFollowUp%>',
			dataType:'json',
			data:$("#myform").serialize(),
			beforeSend:function(request){
	 		  $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
			success:function(data){
				$.unblockUI();
				if(data && data.flag === "success"){
					showMessage("提交成功","success");
					$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
				}
			},
			error:function(){
				showMessage("系统错误!","error");
			}
		})
	}
	//在线预览图片
	function showPictrueOnline(fileWithType,fileWithId , currentName){
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			file_with_class:$("#file_with_class").val(),
			cmd:"sortMultiFile",
			table:'file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_b2b_order")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}
	function onlineScanner(){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}
</script>
  <style type="text/css">
	<!--
	.create_order_button{background-attachment: fixed;background: url(../imgs/create_order.jpg);background-repeat: no-repeat;background-position: center center;height: 51px;width: 129px;color: #000000;border: 0px;}
	.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
	.zebraTable td {line-height:25px;height:25px;}
	.right-title{line-height:20px;height:20px;}
	div.buttonDiv{border: 1px solid #FFFFFF;padding: 5px 0;text-align: right;}
	input.buttonSpecil {background-color: #BF5E26;}
 	.jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
	.specl:hover{background-color: #728a8c;}
	-->
	</style>
</head>

<body onload = "onLoadInitZebraTable()">
<table width="95%" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left:10px;margin-right:10px;margin-top:10px;">
	<tr>
		<td>
			<fieldset style="border:2px #cccccc solid;-webkit-border-radius:2px;-moz-border-radius:2px;margin-bottom: 3px;">
			<legend style="font-size:15px;font-weight:normal;color:#999999;">单证跟进</legend>
			<form id="myform" >
				<input type="hidden" name="b2b_oid" value="<%=b2b_oid %>"/>
				 <%
				 	if(b2BOrderRow.get("certificate",B2BOrderCertificateKey.CERTIFICATE) == B2BOrderCertificateKey.FINISH){
		 		%>
		 			<input type="hidden" name="certificate" value='<%=b2BOrderRow.get("certificate",B2BOrderCertificateKey.CERTIFICATE) %>'/>
		 		<%
				 	}
				 %>
				 <input type="hidden" name="b2b_order_date" value='<%=b2BOrderRow.getString("b2b_order_date") %>' />
				 <table>
				 	<tr>
				 		<td style="text-align:right;">单证阶段:</td>
				 		<td>
				 			<select name="certificate" id="certificate" <%=(b2BOrderRow.get("certificate",B2BOrderCertificateKey.NOCERTIFICATE) == B2BOrderCertificateKey.FINISH ? "disabled":"" ) %> onchange="changeCertificateType();">
				 				 <option value="<%=B2BOrderCertificateKey.CERTIFICATE %>"><%= b2BOrderCertificateKey.getB2BOrderCertificateKeyById(B2BOrderCertificateKey.CERTIFICATE) %></option>
				 				 <option value="<%=B2BOrderCertificateKey.FINISH %>"><%= b2BOrderCertificateKey.getB2BOrderCertificateKeyById(B2BOrderCertificateKey.FINISH )%></option>
				 			</select>
				 			<span id="notfinish_span" style=''>
				 		 		预计本阶段完成时间:<input type="text" id="eta" name="eta" value="<%= tdate.getYYMMDDOfNow() %>"/></span>
				 			</span>
				 			<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=b2b_oid %>',9)"/>
				 		</td>
				 	</tr>
				 	<tr>
				 		<td style="text-align:right;">备注</td>
				 		<td>
				 			<textarea style="width:500px;height:130px;" id="context" name="context"></textarea>
				 		</td>
				 	</tr>
				 </table>
	 	  </form>		
	   <div class="buttonDiv" style="margin-top:5px;"> 
	 	 	<input type="button" id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil"  onclick="submitForm()" value="提交"> 
			<button id="jqi_state0_button取消" value="n" class="jqidefaultbutton specl" name="jqi_state0_button取消" onclick="cancel();">取消</button>
	  </div>
			
			
			</fieldset>
		
			
		</td>
	</tr>
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;-webkit-border-radius:2px;-moz-border-radius:2px;margin-top: 3px;">
			<legend style="font-size:15px;font-weight:normal;color:#999999;">上传文件</legend>
			<form name="uploadImageForm" id="uploadImageForm" value="uploadImageForm" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/transport/TransportCertificateUpFileAction.action" %>'   method="post">	
				<input type="hidden" name="backurl" id="backurl" value=""/>
				<input type="hidden" name="file_names" id="file_names" />
				<input type="hidden" name="file_with_id" value="<%=b2b_oid %>">
				<input type="hidden" name="sn" id="sn" value="B_certificate"/>
				<input type="hidden" name="path" id="path" value="<%=systemConfig.getStringConfigValue("file_path_b2b_order") %>"/>
				<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.B2B_ORDER_CERTIFICATE%>"/>
				<table width="90%" border="0">
					 <tr>
					 	<td height="25" align="left" class="STYLE2" nowrap="nowrap">
				        	文件分类:&nbsp;<select name="file_with_class" id="file_with_class" onchange="fileWithClassChange();">
 								<%if(arraySelectedCertificate != null && arraySelectedCertificate.length > 0){
 									for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
 									String tempStr = arraySelectedCertificate[index];
 									String tempValue = "" ;
 									String tempHtml = "" ;
 									 
 									if(tempStr.indexOf("=") != -1){
 										String[] tempArray = tempStr.split("=");
 										tempValue = tempArray[0];
 										tempHtml = tempArray[1];
 									}
 								%>		
 									<option value="<%=tempValue %>"><%=tempHtml %></option>
 
 								<%	} 
 								}
 								%>
				        	</select>
				        	&nbsp;<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件" />
				        	&nbsp;<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
					 	</td>
					 </tr>
				</table>
			</form>
			<div id="tabs" style="margin-top:10px;">
		 <ul>	
		 	<%
		 		if(selectedListCertificate != null && selectedListCertificate.size() > 0){
		 			for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ ){
		 			%>
		 				<li><a href="#transport_certificate_<%=index %>"> <%=selectedListCertificate.get(index) %></a></li>
		 			<% 	
		 			}
		 		}
		 	%>
		 </ul>
		 	<!-- 遍历出Div 然后显示出来里面的 数据-->
		 	<%
				for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ ){
					%>
						<div id="transport_certificate_<%=index %>">
		 					<%
		 						List<DBRow> arrayLisTemp = mapCertificate.get(""+(index+1));
		 						%>
		 					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
		 						<tr> 
			  						<th width="75%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
			  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
			  					</tr>
		 					
		 						<% 
		 						if(arrayLisTemp != null && arrayLisTemp.size() > 0 ){
		 							for(int listIndex = 0 , total = arrayLisTemp.size() ; listIndex < total ; listIndex++ ){
		 							 %>
		 								<tr>
		 									<td>
		 									<!-- 如果是图片文件那么就是要调用在线预览的图片 -->
						 			 	 	<!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
						 			 		<!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
						 			 	 	<%if(StringUtil.isPictureFile(arrayLisTemp.get(listIndex).getString("file_name"))){ %>
								 			 	 <p>
								 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.B2B_ORDER_CERTIFICATE %>','<%=b2BOrderRow.get("b2b_oid",0l) %>','<%=arrayLisTemp.get(listIndex).getString("file_name") %>');"><%=arrayLisTemp.get(listIndex).getString("file_name") %></a>
								 			 	 </p>
							 			 	 <%} else if(StringUtil.isOfficeFile(arrayLisTemp.get(listIndex).getString("file_name"))){%>
							 			 	 		<p>
							 			 	 			<a href="javascript:void(0)"  file_id='<%=arrayLisTemp.get(listIndex).get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("file_path_b2b_order")%>')" file_is_convert='<%=arrayLisTemp.get(listIndex).get("file_is_convert",0) %>'><%=arrayLisTemp.get(listIndex).getString("file_name") %></a>
							 			 	 		</p>
							 			 	 <%}else{ %>
		 						            <a href='<%= downLoadFileAction%>?file_name=<%=arrayLisTemp.get(listIndex).getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_b2b_order")%>'><%=arrayLisTemp.get(listIndex).getString("file_name") %></a>
		 								 <%} %>
		 								 </td>
		 									<td>
		 										<!--  如果是整个的单据流程已经结束那么就是不应该有删除的文件的按钮的 -->
		 										<%
		 											if(!(b2BOrderRow.get("certificate",0) == B2BOrderCertificateKey.FINISH )){
		 												%>
		 												 <a href="javascript:deleteFile('<%=arrayLisTemp.get(listIndex).get("file_id",0l)%>','file','<%=systemConfig.getStringConfigValue("file_path_b2b_order") %>','file_id')">删除</a>
		 												<%  
		 											}
		 										%>	
		 										 
		 									</td>
		 								</tr>
		 							 <%
		 							}	
		 						}else{
		 					 	%>
		 								<tr>
		 									<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
		 								</tr>
		 							<%
		 						}
		 					%>
		 					</table>	
						 </div>
					<%
				}
		 	%>
		  
		 
		 
	</div>
		</fieldset>	
	</td>
  </tr>
  <!--  
  	<tr>
		<td style="padding-top: 20px;" align="right"><input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="退出"></td>
	</tr>
	-->
</table>
	
<script type="text/javascript">

//stateBox 信息提示框
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
 
	 var  _self = $("body"),
	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	 
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
 }
}
</script>
</body>
</html>