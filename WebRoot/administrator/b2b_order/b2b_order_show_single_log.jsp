<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%
	long b2b_oid = StringUtil.getLong(request,"b2b_oid");
	int b2b_type = StringUtil.getInt(request,"b2b_type");
	TDate tdate = new TDate();
	int[] arrayInt = new int[]{b2b_type};
	DBRow [] rows = b2BOrderMgrZyj.getB2BOrderLogsByB2BOrderIdAndType(b2b_oid,arrayInt);
		
	HashMap followuptype = new HashMap();
	//操作日志类型:1表示进度跟进2:表示财务跟进;3修改日志;1货物状态;5运费流程;6进口清关;7出口报关8标签流程;9:单证流程,10实物图片流程,11质检流程
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改转运单详细记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	followuptype.put(13,"到货通知仓库");
	followuptype.put(14,"第三方标签");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>日志</title>
<style type="text/css">
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
</style>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
 
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
 

</head>

<body onload="onLoadInitZebraTable();">
 <div id="tabs">
 	<ul>
 			<li><a href="#li_show"> <%= followuptype.get(b2b_type) %></a></li>
 	</ul>
 	<div id="li_show">
 		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		    <tr> 
		        <th width="10%"  class="left-title " style="vertical-align: center;text-align: center;">操作员</th>
		        <th width="35%"  class="left-title " style="vertical-align: center;text-align: center;">记录备注</th>
		        <th width="13%"  class="right-title " style="vertical-align: center;text-align: center;">记录时间</th>
		        <th width="10%"  class="right-title " style="vertical-align: center;text-align: center;">操作类型</th>
		        <th width="10%"  class="right-title " style="vertical-align: center;text-align: center;">预计完成</th>
		  	</tr>
		  	<% 
		  	 for(int i = (rows.length-1) ; i >= 0 ; i--) 
		  		{
		  	%>
		  		<tr align="center" valign="middle">
				    <td height="30"><%=adminMgrLL.getAdminById(rows[i].getString("b2ber_id")).getString("employe_name") %></td>
				    <td align="left">
				    	<%
				    		out.print(rows[i].getString("b2b_content"));
				    	%>	      
				    </td>
				    <td><%=tdate.getFormateTime(rows[i].getString("b2b_date")) %></td>
				    <td><%=followuptype.get(rows[i].get("b2b_type",0))%></td>
				    <td><%=  rows[i].getString("time_complete")%>&nbsp;</td>
			  	</tr>
		  	<%
		  		}
		  	%>
		 	
		</table>
 	</div>
 </div>
	  <script type="text/javascript">
			  $("#tabs").tabs({
					spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
					cookie: { expires: 30000 }  
				});
			  </script>

</body>
</html>



