<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.key.ImportTransportDetailErrorKey"%>
<%@ include file="../../include.jsp"%> 
<%
	long transport_id = StringUtil.getLong(request,"transport_id");
	int simulation_type = StringUtil.getInt(request,"simulation_type");
	
	String fileName = StringUtil.getString(request,"file_name");
	
	String systemTimeFile = transportMgrZJ.simulationOutIn(transport_id,fileName,simulation_type);
	DBRow[] rows = new DBRow[0];
	if(!fileName.equals(""))
	{
		 rows = transportMgrZJ.simulationOutInError(systemTimeFile,simulation_type);
	}
	
 	boolean submit = true;
 	String msg = "";
 	ImportTransportDetailErrorKey importTransportDetailErrorKey = new ImportTransportDetailErrorKey();
 %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>

<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="onLoadInitZebraTable()" >
<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
  	<td valign="top" width="98%" align="center">
			<div id="error_import" align="center">
				<table width="100%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;" class="zebraTable">
					 <% 
						if(rows!=null&&rows.length>0)
						{	
							submit = false;
					%>
						<tr>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">商品名</th>
							<th width="10%" class="right-title "style="vertical-align: center;text-align: left;">数量</th>
							<th width="10%" class="right-title "style="vertical-align: center;text-align: left;">序列号</th>
							<th width="10%" class="right-title "style="vertical-align: center;text-align: left;">托盘</th>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">错误原因</th>
						</tr>
					<%
						for(int i=0;i<rows.length;i++)
						{
					%>
						  <tr align="center" valign="middle">
							<td height="40" align="left" nowrap="nowrap" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("p_name")%>&nbsp;</td>
							<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("count") %>&nbsp;</td>
							<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("serial_number") %>&nbsp;</td>
							<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("lp") %>&nbsp;</td>
							<td style="border-bottom: 1px solid #dddddd;padding-left:10px;" align="left" nowrap="nowrap">
								<% 
									out.print("<font color='red'>位置:商品表内第"+rows[i].getString("errorProductRow")+"行</font><br/>");
									String[] errorP = rows[i].getString("errorMessage").split(",");
									for(int j = 0;j<errorP.length;j++)
									{
										out.println("<font color='red'>"+(j+1)+".</font>"+importTransportDetailErrorKey.getErrorMessageById(errorP[j].toString()));
										if(j<errorP.length-1)
										{
											out.print("<br/>");
										}
									}
								%>
							</td>
						  </tr>
					  <%
							}
						}
						else
						{
					  %>
						<tr align="center" valign="middle">
							<th height="100%" align="center" style="padding-left:10px;font-size:xx-large;background-color: white;">上传商品检查无误</th>
						</tr>
					  <%
					  	}
					  %>
			  </table>
			</div>
	</td>
  </tr>
</table>
<input type="hidden" id="import_submit" value="<%=submit%>"/>
<input type="hidden" id="import_name" name="import_name" value="<%=systemTimeFile%>"/>
</body>
</html>



