<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
	long transport_id	= StringUtil.getLong(request, "transport_id");
	DBRow transportRow	= transportMgrZJ.getDetailTransportById(transport_id);
	String deliveryed_date = transportRow.getString("deliveryed_date");
	deliveryed_date		= deliveryed_date.length() > 10 ?deliveryed_date.substring(0,10): deliveryed_date;
	long deliveryer_id	= transportRow.get("deliveryer_id",0L);
	String deliveryer_name = null!=adminMgr.getDetailAdmin(deliveryer_id)?adminMgr.getDetailAdmin(deliveryer_id).getString("employe_name"):"";
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>到货派送</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
	div.cssDivschedule{
			width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
			background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
			background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
</style>
<script type="text/javascript">
$(function(){
	$('#deliveryed_date').datepicker({
		dateFormat:"yy-mm-dd",
		changeMonth: true,
		changeYear: true,
	});
	$("#ui-datepicker-div").css("display","none");
});
function submitData(){
	if(volidate()){
	$(".cssDivschedule").css("display","block");
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/TransportGoodsArriveDeliveryAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag == "success"){
					showMessage("操作成功","success");
					setTimeout("windowClose()", 1000);
				}else{
					$(".cssDivschedule").css("display","none");
					showMessage("操作失败","error");
				}
			},
			error:function(){
				$(".cssDivschedule").css("display","none");
				showMessage("系统错误","error");
			}
		})	
	}
};
function volidate(){
	if($("#deliveryed_date").val() == ""){
		alert("送至时间不能为空");
		return false;
	}
	if($("#deliveryer_id").val() == ""){
		alert("仓库收货人不能为空");
		return false;
	}
	return true;
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function selectDeliveryerPerson()
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:1, 					// 1表示的 单选
			 user_ids:$("#deliveryer_id").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setSelectDeliveryerPerson'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setSelectDeliveryerPerson(user_ids , user_names){
	$("#deliveryer_id").val(user_ids);
	$("#deliveryer_name").val(user_names);
}
function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);}
</script>
</head>
<body>
<div class="cssDivschedule" style=""></div>
	<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			到货派送T<%=transport_id %>
		</legend>	
		<form action="" id="subForm">
		<input type="hidden" name="is_need_notify_executer" value="true"/>
		<input type="hidden" name="transport_id" value="<%=transport_id %>" >
			<table>
				<tr>
					<td>送至时间</td>
					<td>
						<input name="deliveryed_date" id="deliveryed_date" value="<%=deliveryed_date %>"/>
					</td>
				</tr>
				<tr>
					<td>仓库收货人</td>
					<td>
						<input type="hidden" name="deliveryer_id" id="deliveryer_id" value="<%=deliveryer_id %>"/>
						<input name="deliveryer_name" id="deliveryer_name" value="<%=deliveryer_name %>" onclick="selectDeliveryerPerson()"/>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<table align="right">
		<tr align="right">
			<td colspan="2"><input type="button" class="normal-green" value="提交" onclick="submitData();"/></td>
		</tr>
	</table>
	<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>