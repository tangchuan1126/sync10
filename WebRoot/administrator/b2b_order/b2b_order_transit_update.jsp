<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.ll.Utils,java.text.SimpleDateFormat,com.cwc.app.key.TransportOrderKey"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<%@ include file="../../include.jsp"%>
<%
	String transport_id = StringUtil.getString(request,"transport_id").equals("")?"0":StringUtil.getString(request,"transport_id");
	DBRow row = transportMgrLL.getTransportById(transport_id);
	
	if(row == null)
	{
		row = new DBRow();
	}
	
	String transport_waybill_number = row.getValue("transport_waybill_number")==null?"0":row.getValue("transport_waybill_number").toString();
	String transport_waybill_name = row.getValue("transport_waybill_name")==null?"":row.getValue("transport_waybill_name").toString();
	String transportby = row.getValue("transportby")==null?"0":row.getValue("transportby").toString();
	String declaration = row.getValue("declaration")==null?"1":row.getValue("declaration").toString();
	String clearance = row.getValue("clearance")==null?"1":row.getValue("clearance").toString();
	String drawback = row.getValue("drawback")==null?"1":row.getValue("drawback").toString();
	String invoice = row.getValue("invoice")==null?"1":row.getValue("invoice").toString();
	String transport_send_country = row.getValue("transport_send_country")==null?"1":row.getValue("transport_send_country").toString();
	String transport_receive_country = row.getValue("transport_receive_country")==null?"1":row.getValue("transport_receive_country").toString();
	String transport_send_place = row.getValue("transport_send_place")==null?"":row.getValue("transport_send_place").toString();
	String transport_receive_place = row.getValue("transport_receive_place")==null?"":row.getValue("transport_receive_place").toString();
	String transport_linkman = row.getValue("transport_linkman")==null?"":row.getValue("transport_linkman").toString();
	String transport_linkman_phone = row.getValue("transport_linkman_phone")==null?"":row.getValue("transport_linkman_phone").toString();
	String transport_address = row.getValue("transport_address")==null?"":row.getValue("transport_address").toString();
	String remark = row.getValue("remark")==null?"":row.getValue("remark").toString();
	String transport_receive_date = row.getValue("transport_receive_date")==null?"":row.getValue("transport_receive_date").toString();
	String fr_id = row.getValue("fr_id")==null?"":row.getValue("fr_id").toString();
	int transport_status = row.get("transport_status",0);
	int finished = StringUtil.getInt(request,"finished",0);
	
	String deliver_house_number = row.getString("deliver_house_number");
	String deliver_street = row.getString("deliver_street");
	String deliver_address3 = row.getString("deliver_address3");
	String deliver_zip_code = row.getString("deliver_zip_code");
	String deliver_city = row.getString("deliver_city");
	
	DBRow[] countyRows = transportMgrLL.getCounty();
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script>
	var updated = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated").equals("1")) {
	%>
			updated = true;
	<%
		}
	%>
	function init() {
		if(updated) {
			parent.location.reload();
			closeWindow();
		}
	}
	
	$(document).ready(function(){
		init();
		
		getStorageProvinceByCcid(<%=row.get("deliver_ccid",0l)%>,<%=row.get("deliver_pro_id",0l)%>);
	});

	function getStorageProvinceByCcid(ccid,pro_id)
	{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#deliver_pro_id").clearAll();
					$("#deliver_pro_id").addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#deliver_pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#deliver_pro_id").addOption("手工输入","-1");
					}
				});
				
				if (pro_id!=""&&pro_id!=0)
				{
					$("#deliver_pro_id").setSelectedValue(pro_id);
				}
	}
	
	function selectStorage()
	{
		var para = "ps_id="+$("#receive_psid").val();
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){

				},
				
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				
				success: function(data){
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.city);
					$("#ccid").val(data["native"]);
					
					getStorageProvinceByCcid($("#deliver_ccid").val(),data.pro_id);
					
					$("#transport_linkman").val(data.contact);
					$("#transport_linkman_phone").val(data.phone);
				}
			});
	}
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="apply_money_form" id="apply_money_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/transportTransitUpdateAction.action">
<input type="hidden" name="transport.transport_id" value="<%=transport_id%>"/>
<input type="hidden" name="freight_changed" id="freight_changed" value="0"/>
<input name="finished" id="finished" type="hidden">

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">转运单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
	        <tr height="29">
	          <td align="right" class="STYLE2">转运单号:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<%="T"+transport_id%>
	          </td>
	          <td align="right" class="STYLE2"></td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">联系人:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="transport.transport_linkman" id="transport_linkman" value="<%=transport_linkman %>">
	          </td>
	          <td align="right" class="STYLE2">联系电话:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="transport.transport_linkman_phone" id="transport_linkman_phone" value="<%=transport_linkman_phone %>">
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2" nowrap="nowrap">预计到达日期:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="transport.transport_receive_date" id="transport_receive_date" value="<%=transport_receive_date.equals("")?DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay():transport_receive_date%>">
	          </td> 
	          <td align="right" class="STYLE2">目的仓库</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" >
	          	<input type="hidden" value="0" name="transport.sc_id"/>
				<%
	          	if(row.get("transport_status",0l)!=TransportOrderKey.FINISH) {
	          %>
	          	<select name="transport.receive_psid" id="receive_psid" onchange="selectStorage()">          		
	          		<% 
	          			for(int i=0;i<ps.length;i++) {
	          		%>
	          				<option value="<%=ps[i].get("id",0l)%>" <%=row.get("receive_psid",0l)==ps[i].get("id",0l)?"selected":"" %>><%=ps[i].getString("title")%></option>
	          		<%
	          			}
	          		%>
	          	</select>
	          <%
	          	}else{
	          %>
	          		<% 
	          			for(int i=0;i<ps.length;i++) {
	          		%>
	          				<%=row.get("receive_psid",0l)==ps[i].get("id",0l)?ps[i].getString("title"):"" %>
	          				<input type="hidden" name="transport.receive_psid" id="receive_psid" value="<%=row.get("receive_psid",0l) %>">
	          		<%
	          			}
	          		%>
	          <%
	          	}
	          %>
			  </td>
	        </tr>
	        <tr>
			    <td align="right" valign="middle" class="STYLE2" >所属国家:</td>
			    <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
				<%
				DBRow countrycode[] = orderMgr.getAllCountryCode();
				String selectBg="#ffffff";
				String preLetter="";
				%>
			      <select name="transport.deliver_ccid" id="deliver_ccid" onchange="getStorageProvinceByCcid(this.value)">
				  <option value="0">选择国家...</option>
				  <%
				  for (int i=0; i<countrycode.length; i++)
				  {
				  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
					{
						if (selectBg.equals("#eeeeee"))
						{
							selectBg = "#ffffff";
						}
						else
						{
							selectBg = "#eeeeee";
						}
					}  	
					
					preLetter = countrycode[i].getString("c_country").substring(0,1);
					
			
				  %>
				    <option style="background:<%=selectBg%>;" <%=row.get("deliver_ccid",0l)==countrycode[i].get("ccid",0l)?"selected":""%> value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
				  <%
				  }
				  %>
			      </select> 
				</td>
			  </tr>
			  <tr>
			  	<td align="right" valign="middle" class="STYLE2" >所属省份:</td>
			    <td>&nbsp;</td>
	          	<td align="left" valign="middle" colspan="4">
	          		<select name="transport.deliver_pro_id" id="deliver_pro_id"></select>
	          	</td>
			  </tr>
			 <tr height="29">
	          <td align="right" class="STYLE2">所在城市:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<input type="text" name="transport.deliver_city" id="deliver_city" value="<%=deliver_city%>" style="width:510">
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">交货地址门牌:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<input type="text" name="transport.deliver_house_number" id="deliver_house_number" value="<%=deliver_house_number%>" style="width:510">
	          </td>
	        </tr>
	         <tr height="29">
	          <td align="right" class="STYLE2">交货地址街道:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<input type="text" name="transport.deliver_street" id="deliver_street" value="<%=deliver_street%>" style="width:510">
	          </td>
	        </tr>
	         
	        <tr height="29">
	          <td align="right" class="STYLE2">邮政编码</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<input type="text" name="transport.deliver_zip_code" id="deliver_zip_code" value="<%=deliver_zip_code%>" style="width:510">
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">备注:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<textarea rows="2" cols="80" name="transport.remark" id="remark"><%=remark %></textarea>
	          </td>
	        </tr>
		</table>
	</fieldset>	
	</td>
  </tr>
</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
    <%
    	if(finished == 1) {
    %>
      		<input name="insert" type="button" class="normal-green-long" onclick="submitApply(1)" value="下一步" >
    <%
    	}else {
    %>
    		<input name="insert" type="button" class="normal-green-long" onclick="submitApply(0)" value="完成" >
    <%
    	}
    %>
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
  </form>
<script type="text/javascript">
<!--
	function submitApply(finished)
	{
		if($("#remark").val().trim().length>500)
		{
		     $("#remark").focus();
		     alert("备注字数不能超过500！");
		}else {
			$("#finished").val(finished);
			document.apply_money_form.submit();
		}
	}
	function closeWindow(){
		$.artDialog.close();
		//self.close();
	}
//-->
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 

</script>
</body>
</html>

