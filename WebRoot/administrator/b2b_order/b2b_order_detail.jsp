<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.B2BOrderKey"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>

<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.B2BOrderTagKey"%>
<%@page import="com.cwc.app.key.B2BOrderCertificateKey"%>
<%@page import="com.cwc.app.key.B2BOrderQualityInspectionKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.B2BOrderProductFileKey"%>
<%@page import="com.cwc.app.key.B2BOrderStockInSetKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.B2BOrderLogTypeKey"%>
<%@page import="com.cwc.app.key.B2BOrderClearanceKey"%>
<%@page import="com.cwc.app.key.B2BOrderDeclarationKey"%>
<%@page import="com.cwc.app.key.B2BOrderWayKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="b2BOrderClearanceKey"
	class="com.cwc.app.key.B2BOrderClearanceKey" />
<jsp:useBean id="b2BOrderDeclarationKey"
	class="com.cwc.app.key.B2BOrderDeclarationKey" />
<jsp:useBean id="b2BOrderWayKey" class="com.cwc.app.key.B2BOrderWayKey" />
<jsp:useBean id="b2BOrderKey" class="com.cwc.app.key.B2BOrderKey" />
<jsp:useBean id="b2BOrderStockInSetKey"
	class="com.cwc.app.key.B2BOrderStockInSetKey"></jsp:useBean>
<jsp:useBean id="b2BOrderProductFileKey"
	class="com.cwc.app.key.B2BOrderProductFileKey" />
<jsp:useBean id="b2BOrderTagKey" class="com.cwc.app.key.B2BOrderTagKey" />
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="b2BOrderQualityInspectionKey"
	class="com.cwc.app.key.B2BOrderQualityInspectionKey" />
<%
TransportOrderKey transportOrderKey = new TransportOrderKey();
	long b2b_oid = StringUtil.getLong(request,"b2b_oid");
 	TDate tDate = new TDate();
	//fileFlag
	String fileFlag= StringUtil.getString(request,"flag");
	DBRow b2BOrder = b2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
	int isPrint = StringUtil.getInt(request,"isPrint",0);
	
	int number = 0;
	boolean edit = false;
	//根据订单id获取运单信息
	List<List<DBRow>>  transportDBrows=b2BOrderMgrZyj.getTransportByOderId(b2b_oid);
	//title信息
	DBRow titleDBrow=proprietaryMgrZyj.getDetailTitleByTitleId(Long.valueOf(b2BOrder.getString("title_id")));
	//目的仓库  b2BOrder.getString("receive_psid")
	 DBRow storageCatalog=catalogMgr.getDetailProductStorageCatalogById(Long.valueOf(b2BOrder.getString("receive_psid")));
	// 目的国家b2BOrder.getString("deliver_ccid")
	 DBRow    country=orderMgr.getDetailCountryCodeByCcid(Long.valueOf(b2BOrder.getString("deliver_ccid")));
	//目的省份   b2BOrder.getString("deliver_pro_id")
	//准备中的订单,并且没有货款申请,订单明细可修改
	if(b2BOrder.get("b2b_order_status",0)==B2BOrderKey.READY&&applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,b2b_oid,FinanceApplyTypeKey.DELIVERY_ORDER).length==0)
	{
		edit = true;
	}
	HashMap followuptype = new HashMap();
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改订单详细记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	String updateB2BOrderCertificateAction =   ConfigBean.getStringValue("systenFolder") +"action/administrator/b2b_order/B2BOrderCertificateCompleteAction.action";
	//读取配置文件中的purchase_tag_types
		String tagType = systemConfig.getStringConfigValue("b2b_order_tag_types");
		String[] arraySelected = tagType.split("\n");
		//将arraySelected组成一个List
		ArrayList<String> selectedList= new ArrayList<String>();
		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
		String tempStr = arraySelected[index];
		if(tempStr.indexOf("=") != -1){
			String[] tempArray = tempStr.split("=");
			String tempHtml = tempArray[1];
			selectedList.add(tempHtml);
		}
		}
		// 获取所有的关联图片 然后在页面上分类
		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
		int[] productFileTyps = {FileWithTypeKey.B2B_ORDER_PRODUCT_TAG_FILE};
		DBRow[] imagesRows = purchaseMgrZyj.getAllProductTagFilesByPcId(b2b_oid,productFileTyps );//临时拷贝样式，使用方法为采购单，后续开发请注意
		if(imagesRows != null && imagesRows.length > 0 )
		{
	for(int index = 0 , count = imagesRows.length ; index < count ; index++ )
	{
		DBRow tempRow = imagesRows[index];
		String product_file_type = tempRow.getString("product_file_type");
		List<DBRow> tempListRow = imageMap.get(product_file_type);
		if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1))
		{
			tempListRow = new ArrayList<DBRow>();
		}
		tempListRow.add(tempRow);
		imageMap.put(product_file_type,tempListRow);
	}
		}
		// 单证
		String valueCertificate = systemConfig.getStringConfigValue("b2b_order_certificate");
	String[] arraySelectedCertificate = valueCertificate.split("\n");
	//将arraySelected组成一个List
	ArrayList<String> selectedListCertificate= new ArrayList<String>();
	for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
	String tempStr = arraySelectedCertificate[index];
	if(tempStr.indexOf("=") != -1){
		String[] tempArray = tempStr.split("=");
		String tempHtml = tempArray[1];
		selectedListCertificate.add(tempHtml);
	}
	}
	int file_with_type = StringUtil.getInt(request,"file_with_type");
	String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
	String file_with_class = StringUtil.getString(request,"file_with_class");
	DBRow[] imageListCertificate = fileMgrZJ.getFileByWithIdAndType(b2b_oid,FileWithTypeKey.B2B_ORDER_CERTIFICATE);
 	Map<String,List<DBRow>> mapCertificate = new HashMap<String,List<DBRow>>();
 	B2BOrderCertificateKey certificateKey = new B2BOrderCertificateKey();
	if(imageListCertificate != null && imageListCertificate.length > 0){
		//map<string,List<DBRow>>
		for(int index = 0 , count = imageListCertificate.length ; index < count ;index++ ){
	DBRow temp = imageListCertificate[index];
	List<DBRow> tempListRow = mapCertificate.get(temp.getString("file_with_class"));
	if(tempListRow == null){
		tempListRow = new ArrayList<DBRow>();
	}
	tempListRow.add(temp);
	mapCertificate.put(temp.getString("file_with_class"),tempListRow);
		}
	}
	// 实物图片
	String updateB2BOrderAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/b2b_order/B2BOrderProductFileCompleteAction.action";
	String valueProductFile = systemConfig.getStringConfigValue("b2b_order_product_file");
	String[] arrayProductFileSelected = valueProductFile.split("\n");
 	String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/b2b_order/b2b_order_detail.html?b2b_oid="+b2b_oid;
 	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>订单</title>

<link href="../comm.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.tabs.js"></script>


<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/icon.css" />

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript"
	src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript'
	src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript'
	src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript'
	src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript'
	src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript"
	src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript"
	src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css"
	type="text/css" />

<link rel="stylesheet"
	href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css"
	type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";

@import "../js/thickbox/thickbox.css";
</style>

<style type="text/css">
body {
	text-align: left;
}

ol.fundsOl {
	list-style-type: none;
}

ol.fundsOl li {
	width: 350px;
	height: 200px;
	float: left;
	line-height: 25px;
}

.set {
	padding: 2px;
	width: 90%;
	word-break: break-all;
	margin-top: 10px;
	margin-top: 5px;
	line-height: 18px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	margin-bottom: 10px;
	border: 2px solid blue;
}

.receive {
	padding: 2px;
	width: 90%;
	word-break: break-all;
	margin-top: 10px;
	margin-top: 5px;
	line-height: 18px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	margin-bottom: 10px;
	border: 2px solid blue;
}
</style>

<link rel="alternate stylesheet" type="text/css"
	href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript"
	src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css"
	href="../js/jgrowl/jquery.jgrowl.css" />
<link type="text/css"
	href="../js/mcdropdown/css/jquery.order.mcdropdown.css"
	rel="stylesheet" />

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jqgrid 引用 -->
<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css"
	rel="stylesheet" />
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css"
	rel="stylesheet" />
<!-- 在线阅读 -->
<script type="text/javascript"
	src="../js/office_file_online/officeFileOnline.js"></script>

<script type="text/javascript">
	var select_iRow;
	var select_iCol;				
	
	function print()
	{
		visionariPrinter.PRINT_INIT("订单");
			
			visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../b2b_order/print_b2b_order_detail.html?b2b_oid=<%=b2b_oid%>");
				
			visionariPrinter.PREVIEW();
	}
	
	function uploadB2BOrderOrderDetail()
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_upload_excel.html?b2b_oid=<%=b2b_oid%>';
		$.artDialog.open(url, {title: '上传订单',width:'800px',height:'500px', lock: true,opacity: 0.3});
		//tb_show('上传订单','b2b_order_upload_excel.html?b2b_oid=<%=b2b_oid%>&TB_iframe=true&height=500&width=800',false);
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
			 
	function downloadB2BOrderOrder(b2b_oid)
	{
		var para = "b2b_oid="+b2b_oid;
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/B2BOrderDownloadAction.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
	}
	
	function saveDeliveryOrder()
	{
			if(confirm("确定保存对此交货单的修改吗？"))
			{
				jQuery("#gridtest").jqGrid('saveCell',select_iRow,select_iCol);
				<%if(b2b_oid !=0)
					{%>
						document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/saveDeliveryOrder.action";
				<%}
					else
					{%>
					
					document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/addDeliveryOrder.action";
				<%}%>
				
				document.save_deliveryOrder_form.waybill_number.value =  $("#waybill_id").val();
				document.save_deliveryOrder_form.waybill_name.value = $("#waybill").val();
				document.save_deliveryOrder_form.arrival_eta.value = $("#eta").val();
				
				document.save_deliveryOrder_form.submit();
			}
	}
	
	function autoComplete(obj)
	{
		addAutoComplete(obj,
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
	}
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	function refreshWindow(){
		window.location.reload();
	}
	
	function ajaxModProductName(obj,rowid,col)
	{
		var unit_name;//单字段修改商品名时更换单位专用
		var para ="b2b_detail_id="+rowid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/B2BOrderDetailGetJSONAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				async:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}
	function showStockOut(b2b_oid) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_stock_temp_out.html?b2b_oid='+b2b_oid;
		$.artDialog.open(url, {title: '订单出库',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function goodsArriveDelivery(b2b_oid)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_goods_arrive_delivery.html?b2b_oid='+b2b_oid;
		$.artDialog.open(url, {title: '到货派送['+b2b_oid+"]",width:'500px',height:'200px', lock: true,opacity: 0.3});
	}
	function showStockIn(b2b_oid) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_stock_temp_in.html?b2b_oid='+b2b_oid;
		$.artDialog.open(url, {title: '订单入库',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}
	
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
	
	function createWaybill(b2b_oid)
	{
		
		$.artDialog.open("b2b_order_waybill.html?b2b_oid="+b2b_oid, {title: '订单创建运单',width:'670px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function uploadInvoice(b2b_oid)
	{
		$.artDialog.open("b2b_order_upload_invoice.html?b2b_oid="+b2b_oid, {title: '订单上传商业发票',width:'670px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function printWayBill(id,print_page)
	{
		 
		 var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+print_page+"?id="+id+"&type=T";
		
		 $.artDialog.open(uri, {title: '打印运单',width:'960px',height:'500px', lock: true,opacity: 0.3});
   	}
	function changeType(obj) {
		$('#declaration').attr('style','display:none');
		$('#clearance').attr('style','display:none');
		if($(obj).val()==1) {
			$('#clearance').attr('style','');
		}else if($(obj).val()==2) {
			$('#declaration').attr('style','');
		}
	}
	function promptCheck(v,m,f)
	{
		if (v=="y")
		{
			 if(f.content == "")
			 {
					alert("请填写适当备注");
					return false;
			 }
			 else
			 {
			 	if(f.content.length>300)
			 	{
			 		alert("内容太多，请简略些");
					return false;
			 	}
			 }
			 return true;
		}
	}
	function followup(b2b_oid,declaration,clearance)
	{
		if((declaration>1&&declaration!=4)||(clearance>1&&clearance!=4)) {
			var text = "<div id='title'>订单跟进[单号:"+b2b_oid+"]</div><br />";
			text += "跟进范围:<select id='type' name='type' onchange='changeType(this)'>";
			text += declaration>1&&declaration!=4?"<option value='2'>出口报关</option>":"";
			text += clearance>1&&clearance!=4?"<option value='1'>进口清关</option>":"";
			text += "</select>&nbsp;&nbsp;";
			text += "跟进阶段:";
			var b = false;		
			b1 = declaration>1&&declaration!=4;
	  		text += "<select name='declaration' id='declaration' "+(!b?(b1?"":"style='display:none'"):"style='display:none'")+">";
	  		<%ArrayList statuses2 = b2BOrderDeclarationKey.getB2BOrderDeclarationKeys();
	  			for(int i=2;i<statuses2.size();i++) {
	  				int statuse = Integer.parseInt(statuses2.get(i).toString());
	  				String key1 = b2BOrderDeclarationKey.getB2BOrderDeclarationKeyById(statuse);%>
			text += "<option value='<%=statuse%>'><%=key1%></option>";
			<%}%>
	  		text += "</select>";
			b = b?b:b1;  		
			b1 = clearance>1&&clearance!=4;
	  		text += "<select name='clearance' id='clearance' "+(!b?(b1?"":"style='display:none'"):"style='display:none'")+">";
	  		<%ArrayList statuses3 = b2BOrderClearanceKey.getB2BOrderClearanceKeys();
	  			for(int i=2;i<statuses3.size();i++) {
	  				int statuse = Integer.parseInt(statuses3.get(i).toString());
	  				String key1 = b2BOrderClearanceKey.getB2BOrderClearanceKeyById(statuse);%>
			text += "<option value='<%=statuse%>'><%=key1%></option>";
			<%}%>
	  		text += "</select>";
			b = b?b:b1;
			text += '<br/>ETA:<input type="text" value="" id="eta" name="eta" />';
			$('#eta').datepicker({
				dateFormat:"yy-mm-dd",
				changeMonth: true,
				changeYear: true,
				onSelect:function(dateText, inst){
					var content = $("#content").val();
					 if(content.indexOf("预计时间") != -1){
						 content = content.replace("预计时间","").substr(11);
					}
					 $("#content").val("预计时间" + dateText + ":"+content);
				}
			});
			text += "<br/>备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>";
			$.prompt(text,
			{
				  submit:promptCheck,
		   		  loaded:
						function ()
						{
						}
				  ,
				  callback: 
						function (v,m,f)
						{
							if (v=="y")
							{
									document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/B2BOrderLogsInsertAction.action"
									document.followup_form.b2b_oid.value = b2b_oid;
									document.followup_form.type.value = f.type;
									if(f.type==1) {
										document.followup_form.stage.value = f.clearance;
									}else if(f.type==2) {
										document.followup_form.stage.value = f.declaration;
									}
									document.followup_form.b2b_order_content.value = f.content;
									document.followup_form.b2b_order_type.value = 1;
									document.followup_form.expect_date.value = f.eta;
									document.followup_form.submit();	
							}
						}
				  ,
				  overlayspeed:"fast",
				  buttons: { 提交: "y", 取消: "n" }
			});
		}else {
			alert("没有跟进项目");
		}
	}
	function followup_clearance_declaration(b2b_oid,declaration,clearance){
		if((declaration>1&&declaration!=4)||(clearance>1&&clearance!=4)){
		 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/b2b_order/b2b_order_log_declaration_clear_add.html"; 
				uri += "?b2b_oid="+b2b_oid+"&declaration="+declaration+"&clearance="+clearance;
	 		$.artDialog.open(uri , {title: "订单跟进["+b2b_oid+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
		}else{
			alert("无跟进项目!");
		}
	}
	function followupB2BOrder(b2b_oid)
	{
	 	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' +"administrator/b2b_order/b2b_order_log_declaration_clear_add.html?b2b_oid="+b2b_oid;
		 $.artDialog.open(uri , {title: "日志跟进["+b2b_oid+"]",width:'450px',height:'320px', lock: true,opacity: 0.3,fixed: true});
	}
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
 	function deleteFileTable(_file_id){
  	   $.ajax({
 			url:'<%=deleteFileAction%>',
 			dataType:'json',
 			data:{table_name:'file',file_id:_file_id,folder:'',pk:'file_id'},
 			success:function(data){
 				if(data && data.flag === "success"){
 					window.location.reload();
 				}else{
 					showMessage("系统错误,请稍后重试","error");
 				}
 			},
 			error:function(){
 				showMessage("系统错误,请稍后重试","error");
 			}
 		})
  	}
	function goApplyFunds(id)
	{
		var id="'F"+id+"'";
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=searchby_index&search_mode=1&apply_id="+id);       
	}

	function onlineScanner(target){
		var _target = "";
		if(target == "uploadCertificateImageForm"){
			_target = "uploadCertificateImageForm";
		}else if(target == "qualityInspectionForm" ){
			_target = "qualityInspectionForm";
		}
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target="+_target; 
		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}

	//file表中的图片在线显示
    function showPictrueOnlinef(fileWithType,fileWithId , currentName){
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			cmd:"multiFile",
			table:'file',
			base_path:'<%=ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%=systemConfig.getStringConfigValue("file_path_b2b_order")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%=ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}

    //file表中的  单证信息的图片在线显示
    function showPictrueOnlinecer(fileWithType,fileWithId , currentName){
    	var file_with_class = $("#file_with_class_certificate").val();
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			file_with_class :file_with_class,
			cmd:"sortMultiFile",
			table:'file',
			base_path:'<%=ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%=systemConfig.getStringConfigValue("file_path_b2b_order")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%=ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}

	
	//product_file表中的图片在线显示
	function showPictrueOnline1(fileWithType,fileWithId , currentName,product_file_type){
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			product_file_type:product_file_type,
			cmd:"multiFile",
			table:'product_file',
			base_path:'<%=ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%=systemConfig.getStringConfigValue("file_path_product")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%=ConfigBean.getStringValue("systenFolder")%>');
		}
	}

	function getBLPSelect(pc_id,receive_id)
	{
		var selectString = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/LPType/getBLPSelectForJqgrid.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			async:false,
			data:{pc_id:pc_id,to_ps_id:receive_id},
			
			beforeSend:function(request){
			},
			
			error: function(){
				alert("提交失败，请重试！");
			},
			
			success: function(data)
			{
				selectString = data;
			}
		});
		return selectString;
	}

	function getCLPSelect(pc_id)
	{
		var selectString = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/LPType/getCLPSelectForJqgrid.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			async:false,
			data:{
				pc_id:pc_id,
				to_ps_id:<%=b2BOrder.get("receive_psid",0l)%>,
				title_id:<%=b2BOrder.get("title_id",0l)%>
			},
			
			beforeSend:function(request){
			},
			
			error: function(){
				alert("提交失败，请重试！");
			},
			
			success: function(data)
			{
				selectString = data;
			}
		});
		return selectString;
	}
</script>



<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size: 12px;
}

a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size: 12px;
}

a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size: 12px;
}

a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size: 12px;
}

a.hard:link {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size: 14px;
}

a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size: 14px;
}

a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size: 14px;
}

a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size: 14px;
}

body {
	font-size: 12px;
}

.STYLE1 {
	color: #FFFFFF
}

.input-line {
	width: 200px;
	font-size: 12px;
}

.text-line {
	font-size: 12px;
}

a.logout:link {
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:hover {
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:active {
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

.ui-datepicker {
	z-index: 1200;
}

.rotate { /* for Safari */
	-webkit-transform: rotate(-90deg);
	/* for Firefox */
	-moz-transform: rotate(-90deg);
	/* for Internet Explorer */
	filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3 );
}

.ui-jqgrid tr.jqgrow td {
	white-space: normal !important;
	height: auto;
	vertical-align: text-top;
	padding-top: 2px;
}

.send {
	padding: 2px;
	width: 90%;
	word-break: break-all;
	margin-top: 10px;
	margin-top: 5px;
	line-height: 18px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	margin-bottom: 10px;
	border: 2px solid blue;
}

.recive {
	padding: 2px;
	width: 80%;
	word-break: break-all;
	margin-top: 10px;
	margin-top: 5px;
	line-height: 18px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	margin-bottom: 10px;
	border: 2px solid green;
}

.create_order_button {
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
}

.STYLE2 {
	color: #0066FF;
	font-weight: bold;
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif;
}

.zebraTable td {
	line-height: 25px;
	height: 25px;
}

.right-title {
	line-height: 20px;
	height: 20px;
}

span.spanBold {
	font-weight: bold;
}

span.fontGreen {
	color: green;
}

span.fontRed {
	color: red;
}

span.spanBlue {
	color: blue;
}

div.tabdown {
	background-attachment: fixed;
	background: url(../imgs/tabdown.png);
	background-repeat: no-repeat;
	background-position: center center;
	height: 15px;
	width: 79px;
	color: #000000;
	border: 0px;
	position: relative;
}

div.tabup {
	background-attachment: fixed;
	background: url(../imgs/tabup.png);
	background-repeat: no-repeat;
	background-position: center center;
	height: 15px;
	width: 79px;
	color: #000000;
	border: 0px;
	position: relative;
}
</style>
<script>
function openApplyMoneyInsert() {
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/financial_management/apply_money_b2b_order_insert.html?associationId=<%=b2b_oid%>";

	$.artDialog.open(uri, {title: '申请费用',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showTransit()
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+'administrator/b2b_order/b2b_order_basic_update.html?isOutter=2&b2b_oid=<%=b2b_oid%>';
	$.artDialog.open(uri, {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function updateB2BOrderAllProdures()
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+'administrator/b2b_order/b2b_order_wayout_update.html?isOutter=2&b2b_oid=<%=b2b_oid%>';
	$.artDialog.open(uri, {title: '修改各流程信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showFreight(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+'administrator/b2b_order/b2b_order_freight_update.html?isOutter=2&b2b_oid=<%=b2b_oid%>';
	$.artDialog.open(uri, {title: '订单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showFreightCost(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+'administrator/b2b_order/b2b_order_set_freight.html?isOutter=2&b2b_oid=<%=b2b_oid%>&fr_id=<%=b2BOrder.getString("fr_id")%>';
	$.artDialog.open(uri, {title: '修改运费',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showWayout(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+'administrator/b2b_order/b2b_order_wayout_update.html?b2b_oid=<%=b2b_oid%>';
	$.artDialog.open(uri, {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showSetDrawback(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/b2b_order/b2bOrderSetDrawback.html?b2b_oid=<%=b2b_oid%>";
	$.artDialog.open(uri, {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}

function fileWithClassCertifycateChange(){
	var file_with_class = $("#file_with_class_certificate").val();
   	$("#tabsCertificate").tabs( "select" , file_with_class * 1-1 );
}
function deleteFileCommon(file_id , tableName , folder,pk){
	$.ajax({
		url:'<%=deleteFileAction%>',
		dataType:'json',
		data:{table_name:tableName,file_id:file_id,folder:folder,pk:pk},
		success:function(data){
			if(data && data.flag === "success"){
				window.location.reload();
			}else{
				showMessage("系统错误,请稍后重试","error");
			}
		},
		error:function(){
			showMessage("系统错误,请稍后重试","error");
		}
})
}
 
jQuery(function($){
	if($.trim('<%=fileFlag%>') === "1"){
		showMessage("上传文件出错","error");
	}
	if($.trim('<%=fileFlag%>') === "2"){
		showMessage("请选择正确的文件类型","error");
	}
})
function cerficateCompelte(){
	$.artDialog.confirm('单证流程完成后文件只能上传不能删除,确认继续吗？', function(){
		$.ajax({
			url:'<%=updateB2BOrderCertificateAction%>',
			data:{b2b_oid:'<%=b2b_oid%>',b2b_order_date:'<%=b2BOrder.getString("b2b_order_date")%>'},
			dataType:'json',
			success:function(data){
				if(data && data.flag == "success"){
					window.location.reload();
				}else{showMessage("系统错误,请稍后重试","alert");}
			},
			error:function(){
			  showMessage("系统错误,请稍后重试","alert");
			}
		})
	}, function(){
	});
}
function productFileCompelte(){
	var isFlag =  window.confirm("所有商品相关文件都上传了吗？");
	 if(isFlag){
		 $.ajax({
	    			url:'<%=updateB2BOrderAction%>',
	    			data:{b2b_oid:'<%=b2b_oid%>',b2b_order_date:'<%=b2BOrder.getString("b2b_order_date")%>'},
	    			dataType:'json',
	    			success:function(data){
	    				if(data && data.flag == "success"){
	    					window.location.reload();
	    				}else{showMessage("系统错误,请稍后重试","alert");}
	    			},
	    			error:function(){
	    			  showMessage("系统错误,请稍后重试","alert");
	    			}
	    		})
		   }
}
function showSingleLogs(b2b_oid,b2b_order_type,title){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/b2b_order/b2b_order_show_single_log.html?b2b_oid="+<%=b2b_oid%>+"&b2b_type="+b2b_order_type;
	$.artDialog.open(uri , {title: title+"["+<%=b2b_oid%>+"]",width:'870px',height:'470px', lock: true,opacity: 0.3,fixed: true});	
}
function b2bOrderApplyMoney(_type){

	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/CheckB2BOrderApplyPromptAction.action',
		data:'b2b_oid=<%=b2b_oid%>',
		dataType:'json',
		type:'post',
		success:function(data){
			if(data && data.result)
			{
				alert(data.result);
			}
			else
			{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/b2b_order/b2b_order_apply_money.html?id="+_type+"&b2b_oid="+<%=b2b_oid%>;
				$.artDialog.open(uri , {title: "交货单["+'<%=b2b_oid%>'+"]申请货款",width:'900px',height:'570px', lock: true,opacity: 0.3,fixed: true});
			}
		},
		error:function(){
			showMessage("系统错误","error");
		}
	});
    
}
 
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,_target){
    var targetNode = $("#"+_target);
    $("input[name='file_names']",targetNode).val(fileNames);
    if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
        $("input[name='file_names']").val(fileNames);
        var myform = $("#"+_target);
        var file_with_class = $("#file_with_class").val();
			$("input[name='backurl']",targetNode).val("<%=backurl%>" + "&file_with_class="+file_with_class);
		  	myform.submit();
	}
}

function updateB2BOrderVW(b2b_oid)
	{
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/B2BOrderUpdateItemsVW.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:{b2b_oid:b2b_oid},
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date)
					{
						if(date.result=="ok")
						{
							window.location.reload();
						}
					}
				});
	}
function b2BOrderProductFileFinishToNeed()
{
	if(confirm("您确定要修改实物图片流程从完成到需要阶段吗？"))
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/b2b_order/b2b_order_product_file_finish_to_need.html?b2b_oid=<%=b2b_oid%>";
		$.artDialog.open(uri, {title: '修改实物图片完成-->需要['+<%=b2b_oid%>+']',width:'570px',height:'280px', lock: true,opacity: 0.3});

	}
}
</script>
</head>

<body onload="onLoadInitZebraTable();">
	<div class="demo">
		<table>
			<tr>
				<td colspan="2" align="left"><br /> <%
 	if(b2BOrder.get("b2b_order_status",0)!=B2BOrderKey.AlREADYARRIAL&&b2BOrder.get("b2b_order_status",0)!=B2BOrderKey.APPROVEING&&b2BOrder.get("b2b_order_status",0)!=B2BOrderKey.FINISH)
 				{
 %> <input name="button" type="button" class="long-button"
					value="明细修改" onClick="showTransit()" /> <%
 	}
 %> <input name="button" type="button" class="long-button"
					value="修改各流程信息" onClick="updateB2BOrderAllProdures()" /> <%
 	if(b2BOrder.get("b2b_order_status",0)==B2BOrderKey.READY)
 				{
 %> <input type="button" class="long-button-upload" value="上传订单"
					onclick="uploadB2BOrderOrderDetail()" /> <%
 	}
 %> <input type="button" class="long-button-next" value="下载订单"
					onclick="downloadB2BOrderOrder(<%=b2b_oid%>)" /> <input
					type="button" class="long-button-print" value="打印订单"
					onclick="print()" /> <%
 	if(b2BOrder.get("b2b_order_status",0)==B2BOrderKey.READY|| b2BOrder.get("b2b_order_status",0)==B2BOrderKey.PARTALLOCATE)
 	  			{
 %> <input name="button" type="button" class="long-button"
					value="保留库存" onClick="receiveAllocate(<%=b2b_oid%>)" /> <%
 	}
 %>
				</td>
			</tr>

		</table>
		<div id="b2BOrderTabs">

			<ul>
				<li><a href="#b2b_order_basic_info" ondblclick="funfff()">基础信息<span> </span>
				</a>
				</li>
				<li><a href="#b2b_order_info" ondblclick="funfff()">运单信息<span> </span>
				</a>
				</li>
				<%
					if(b2BOrder.get("stock_in_set",B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET) != B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET) {
				%>
				<%
					//计算颜色
					  						String stockInSetClass = "" ;
						int stockInSetInt = b2BOrder.get("stock_in_set",B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET);
						if(stockInSetInt == B2BOrderStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH){
				  								stockInSetClass += "spanBold";
				  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",b2BOrder.get("b2b_oid",0l),FileWithTypeKey.B2B_ORDER_STOCKINSET);
				  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
				  									stockInSetClass += " fontGreen";
				  								}else{
				  									stockInSetClass += " fontRed";
				  								}
						}else if(stockInSetInt != B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET){
							stockInSetClass += "spanBold spanBlue";
				  							}
				%>

				<%
					}
				%>
				<%
					if(b2BOrder.get("declaration",B2BOrderDeclarationKey.NODELARATION) != B2BOrderDeclarationKey.NODELARATION) {
				%>
				<%
					//计算颜色
					  						String declarationKeyClass = "" ;
						int declarationInt = b2BOrder.get("declaration",B2BOrderDeclarationKey.NODELARATION);
						if(declarationInt == B2BOrderDeclarationKey.FINISH){
				  								declarationKeyClass += "spanBold";
				  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",b2BOrder.get("b2b_oid",0l),FileWithTypeKey.B2B_ORDER_DECLARATION);
				  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
				  									declarationKeyClass += " fontGreen";
				  								}else{
				  									declarationKeyClass += " fontRed";
				  								}
						}else if(declarationInt != B2BOrderDeclarationKey.NODELARATION ){
							declarationKeyClass +=  "spanBold spanBlue";
				  							}
				%>

				<%
					}
				%>
				<%
					if(b2BOrder.get("clearance",B2BOrderClearanceKey.NOCLEARANCE) != B2BOrderClearanceKey.NOCLEARANCE) {
				%>
				<%
					//计算颜色
					  						String clearanceKeyClass = "" ;
						int clearanceInt = b2BOrder.get("clearance",B2BOrderClearanceKey.NOCLEARANCE);
						if( clearanceInt == B2BOrderClearanceKey.FINISH){
				  								clearanceKeyClass += "spanBold";
				  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",b2BOrder.get("b2b_oid",0l),FileWithTypeKey.B2B_ORDER_CLEARANCE);
				  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
				  									clearanceKeyClass += " fontGreen";
				  								}else{
				  									clearanceKeyClass += " fontRed";
				  								}
						}else if(clearanceInt != B2BOrderClearanceKey.NOCLEARANCE){
							clearanceKeyClass +=  "spanBold spanBlue";
				  							}
				%>

				<%
					}
				%>
				<%
					//计算颜色
					  						String tagClass = "" ;
				  							int tagInt = b2BOrder.get("tag",B2BOrderTagKey.NOTAG);
				  							if(tagInt == B2BOrderTagKey.FINISH){
				  								tagClass += "spanBold";
				  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",b2BOrder.get("b2b_oid",0l),FileWithTypeKey.B2B_ORDER_TAG);
				  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
				  									tagClass += " fontGreen";
				  								}else{
				  									tagClass += " fontRed";
				  								}
				  							}else if(tagInt != B2BOrderTagKey.NOTAG){
				  								tagClass += "spanBold spanBlue";
				  							}
				%>



				<%
					int tagIntThird = b2BOrder.get("tag_third",B2BOrderTagKey.NOTAG);
						if(tagIntThird != B2BOrderTagKey.NOTAG)
						{
					//计算颜色
					String tagClassThird = "" ;
					
					if(tagIntThird == B2BOrderTagKey.FINISH){
						tagClassThird += "spanBold";
						int[] fileType	= {FileWithTypeKey.B2B_ORDER_PRODUCT_TAG_FILE};
						  		if(purchaseMgrZyj.checkPurchaseThirdTagFileFinish(b2b_oid,fileType,"b2b_order_tag_types"))
						  		{
							tagClassThird += " fontGreen";
						}else{
							tagClassThird += " fontRed";
						}
					}else if(tagIntThird != B2BOrderTagKey.NOTAG){
						tagClassThird += "spanBold spanBlue";
					}
				%>

				<%
					}
				%>
				<%
					if(b2BOrder.get("certificate",certificateKey.NOCERTIFICATE) != certificateKey.NOCERTIFICATE) {
				%>
				<%
					//计算颜色
					  						String certificateClass = "" ;
				  							int certificateInt = b2BOrder.get("certificate",B2BOrderCertificateKey.NOCERTIFICATE);
				  							if(certificateInt == B2BOrderCertificateKey.FINISH){
				  								certificateClass += "spanBold";
				  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",b2BOrder.get("b2b_oid",0l),FileWithTypeKey.B2B_ORDER_CERTIFICATE);
				  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
				  									certificateClass += " fontGreen";
				  								}else{
				  									certificateClass += " fontRed";
				  								}
				  							}else if(certificateInt != B2BOrderCertificateKey.NOCERTIFICATE){
				  								certificateClass += "spanBold spanBlue";
				  							}
				%>

				<%
					}
				%>
				<%
					//计算颜色
					  						String productFileClass = "" ;
				  							int productFileInt = b2BOrder.get("product_file",B2BOrderProductFileKey.NOPRODUCTFILE);
				  							if(productFileInt == B2BOrderProductFileKey.FINISH){
				  								productFileClass += "spanBold";
				  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("product_file",b2BOrder.get("b2b_oid",0l),FileWithTypeKey.B2B_ORDER_PRODUCT_FILE);
				  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
				  									productFileClass += " fontGreen";
				  								}else{
				  									productFileClass += " fontRed";
				  								}
				  							}else if(productFileInt != B2BOrderProductFileKey.NOPRODUCTFILE){
				  								productFileClass += "spanBold spanBlue";
				  							}
				%>


				<%
					if(b2BOrder.get("quality_inspection",B2BOrderQualityInspectionKey.NO_NEED_QUALITY) != B2BOrderQualityInspectionKey.NO_NEED_QUALITY) {
				%>
				<%
					//计算颜色
					  						String qualityInspectionClass = "" ;
									int qualityInspectionInt = b2BOrder.get("quality_inspection",B2BOrderQualityInspectionKey.NO_NEED_QUALITY);
									if(qualityInspectionInt == B2BOrderQualityInspectionKey.FINISH){
				  								qualityInspectionClass += "spanBold";
				  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",b2BOrder.get("b2b_oid",0l),FileWithTypeKey.B2B_ORDER_QUALITYINSPECTION);
				  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
				  									qualityInspectionClass += " fontGreen";
				  								}else{
				  									qualityInspectionClass += " fontRed";
				  								}
									}else if(qualityInspectionInt != B2BOrderQualityInspectionKey.NO_NEED_QUALITY){
										qualityInspectionClass += "spanBold spanBlue";
				  							}
				%>

				<%
					}
				%>
			</ul>
			<div>
				<div id="cc" style="display: none;">
					<div id="b2b_order_basic_info">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top" width="25%">
									<fieldset style="border-width: 2px;">
										<legend>运单基础信息</legend>
										<table>
											<tr>
												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">订单号：</font>
												</td>
												<td align="left" width="16%">B<%=b2b_oid%></td>
											</tr>
											<tr>
												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">TITLE：</font>
												</td>
												<td align="left" colspan="3">
													<%
														DBRow titleRow22 = proprietaryMgrZyj.findProprietaryByTitleId(b2BOrder.get("title_id", 0L));
															  						if(null != titleRow22)
															  						{  							
																				out.println(titleRow22.getString("title_name"));
															  						}
															  						else
															  						{
															  							out.println("&nbsp;");
															  						}
													%>
												</td>

											</tr>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">预计到达日期：</font>
												</td>
												<td align="left" width="16%">
													<%
														if(b2BOrder.getString("b2b_order_receive_date").trim().length() > 0){
																				out.println(tDate.getFormateTime(b2BOrder.getString("b2b_order_receive_date"), "yyyy-MM-dd"));
																			}
													%>
												</td>


											</tr>
											<tr>
												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">创建人：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrder.getString("create_account")%></td>
											</tr>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">创建时间：</font>
												</td>
												<td align="left"><%=tDate.getFormateTime(b2BOrder.getString("b2b_order_date"))%></td>

											</tr>




											<tr>
												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">货物状态：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrderKey.getB2BOrderKeyById(b2BOrder.get("b2b_order_status",0))%></td>
											</tr>
										</table>
									</fieldset></td>


								<td width="5%" ></td>
								<td width="25%" valign="top">
									<fieldset style="border-width: 2px;">
										<legend>流程基础信息</legend>
										<table>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">质检：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrderQualityInspectionKey.getB2BQualityInspectionKeyById(b2BOrder.get("quality_inspection",B2BOrderQualityInspectionKey.NO_NEED_QUALITY)+"")%></td>


											</tr>
											<tr>
												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">出口报关：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrderDeclarationKey.getB2BOrderDeclarationKeyById(b2BOrder.get("declaration",01))%></td>



											</tr>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">进口清关：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrderClearanceKey.getB2BOrderClearanceKeyById(b2BOrder.get("clearance",01))%></td>


											</tr>

											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">单证流程：</font>
												</td>
												<td align="left" width="16%"><%=certificateKey.getB2BOrderCertificateKeyById(b2BOrder.get("certificate",certificateKey.NOCERTIFICATE))%></td>
											</tr>
											<tr>


												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">制签流程：</font>
												</td>
												<td align="left" width="16%">
													<%
														if(b2BOrder.get("tag",B2BOrderTagKey.NOTAG) == B2BOrderTagKey.TAG){
																				out.print("制签中");
																			}else{
																				out.print(b2BOrderTagKey.getB2BOrderTagById(b2BOrder.get("tag",B2BOrderTagKey.NOTAG)));
																			}
													%>
												</td>

											</tr>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">运费状态：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrderStockInSetKey.getB2BStockInSetKeyById(b2BOrder.get("stock_in_set",B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET))%></td>



											</tr>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">实物图片：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrderProductFileKey.getB2BOrderProductFileKeyById(b2BOrder.get("product_file",B2BOrderProductFileKey.NOPRODUCTFILE))%></td>
											</tr>

										</table>
									</fieldset></td>
								<td width="5%"></td>
								<td width="25%">
								
									<fieldset style="border-width: 2px;">
										<legend>接受地址</legend>
										<table>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">TITLE：</font>
												</td>
												<td align="left" width="16%"><%=titleDBrow.getString("title_name") %></td>


											</tr>
											<tr>
												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">目的仓库：</font>
												</td>
												<td align="left" width="16%"><%
												if(storageCatalog!=null){
											
												out.print(	storageCatalog.getString("title"));
												}%></td>



											</tr>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">目的国家：</font>
												</td>
												<td align="left" width="16%"><%=country.getString("c_country") %></td>


											</tr>

											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">目的省份：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrder.getString("address_state_deliver") %></td>
											</tr>
											<tr>


												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">交货地址门牌号：</font>
												</td>
												<td align="left" width="16%">
											<%=b2BOrder.getString("deliver_house_number") %>
												</td>

											</tr>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">交货地址街道：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrder.getString("deliver_street") %></td>



											</tr>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">交货地址邮编：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrder.getString("deliver_zip_code") %></td>



											</tr>
											<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">交货联系人：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrder.getString("b2b_order_linkman") %></td>
											</tr>
												<tr>

												<td width="16%" align="right" height="25"><font
													style="font-family: 黑体; font-size: 14px;">交货联系人电话：</font>
												</td>
												<td align="left" width="16%"><%=b2BOrder.getString("b2b_order_linkman_phone") %></td>
											</tr>

										</table>
									</fieldset>
								
								
								
								</td>

							</tr>
							<tr>
								<td colspan="2" height="15px"></td>
							</tr>

						</table>

					</div>
					<div id="b2b_order_info">

						<table border="0" width="100%">
					<%
  		for(List<DBRow> tt:transportDBrows){
  	                                       %>
							<tr>
							<% for(DBRow d:tt){ %>
								<td width="25%">
							<% if(d!=null){ %>
									<fieldset class="set">
										<legend>
<a target="_blank"
								href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_order_in_detail.html?transport_id=<%=d.getString("transport_id")%>">T<%=d.getString("transport_id")%></a>
							&nbsp;<%=transportOrderKey.getTransportOrderStatusById(d.get("transport_status",0))%>
                                        </legend>
                                        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td width="70%">
										<p style="text-align: left;clear: both; " >
											<span class="stateName">创建人:</span> 
											<span class="stateValue">	
											<% 
											DBRow createAdmin = adminMgr.getDetailAdmin(d.get("create_account_id",0l));
 			  						             	if(createAdmin!=null)
 			  						 	         {
 			  						 		         out.print(createAdmin.getString("employe_name"));
 			  						 	          }%>
 			  						 	          </span>
										</p>
										<p style="text-align: left;clear: both;">
											<span class="stateName">TITLE:</span> <span class="stateValue">
											<%
 	                        DBRow titleRow = proprietaryMgrZyj.findProprietaryByTitleId(d.get("title_id", 0L));
 			  						if(null != titleRow)
 			  						{  							
 				  						out.println(titleRow.getString("title_name"));
 			  						}
                            %>
											&nbsp;</span>
										</p>
											<p style="text-align: left;clear: both;">
											<span class="stateName">允许装箱:</span> <span class="stateValue">
											<%
														DBRow packingAdmin = adminMgr.getDetailAdmin(d.get("packing_account",0l));
																  						 	if(packingAdmin!=null)
																  						 	{
																  						 		out.print(packingAdmin.getString("employe_name"));
																  						 	}
													%>
&nbsp;</span>
										</p>
										<p style="text-align: left;clear: both;">
											<span class="stateName">创建时间:</span> <span class="stateValue"><%=tDate.getFormateTime(d.getString("transport_date"), "dd/MM/yyyy hh:mm")
										%>&nbsp;</span>
										</p>
										<p style="text-align: left;clear: both;">
											<span class="stateName">更新时间:</span> <span class="stateValue"><%=tDate.getFormateTime(d.getString("updatedate"),"dd/MM/yyyy hh:mm")%>&nbsp;</span>
										</p>
										<p style="text-align: left;clear: both;">
											<span class="stateName">运输方式:</span> <span class="stateValue"><%=b2BOrderWayKey.getB2BOrderWayKeyById(d.get("transportby",0))%>&nbsp;</span>
										</p>
										<p style="text-align: left;clear: both;">
											<span class="stateName">承运公司:</span> <span class="stateValue"><%=d.getString("carriers")%>&nbsp;</span>
										</p>
										<p style="text-align: left;clear: both;">
											<span class="stateName">发货港:</span> <span class="stateValue"><%=d.getString("transport_send_place")%>&nbsp;</span>
										</p>
										<p style="text-align: left;clear: both;">
											<span class="stateName">目的港:</span> <span class="stateValue"><%=d.getString("transport_receive_place")%></span>
										</p>
										</td>
										<td style="border-left:1px dashed silver;" nowrap align="left" valign="middle" style="padding-left:0">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<col width="30%">
										<col width="70%">
										<tr>
											<td align="right" nowrap style="padding-left:10px;">总容器:</td>
											<td align="left" nowrap style="padding-left:2px;"><%= outboundOrderMgrZJ.getOutListContainerCountBySystemBill(ProductStoreBillKey.TRANSPORT_ORDER, d.get("transport_id",0l)) %></td>
										</tr>
									</table>
								</td>
								</tr>
								</table>
									</fieldset>
									<%} %>
									</td>
								</td>
								<%} %>
							</tr>

                       <%} %>
							<%
								long ps_id4 = null!=b2BOrder?b2BOrder.get("deliver_pro_id",0l):0L;
																	DBRow psIdRow4 = productMgr.getDetailProvinceByProId(ps_id4);
							%>
							<%
								long ccid4 = null!=b2BOrder?b2BOrder.get("deliver_ccid",0l):0L; 
																	DBRow ccidRow4 = orderMgr.getDetailCountryCodeByCcid(ccid4);
							%>
							
							</table>
							<table width="100%">
								<tr>
									<td align="left""> <%
 	if(b2BOrder.getString("invoice_path").length()>0)
 				{
 %> <input type="button" value="下载商业发票" class="long-button-next"
										onclick="window.location.href='../../<%=b2BOrder.getString("invoice_path")%>'" />
										<%
											}
										%> <%
 	if(b2BOrder.get("b2b_order_status",0)==B2BOrderKey.INTRANSIT)
   					{
 %> <%--									&nbsp;&nbsp;<a href="#" onClick="createWaybill(<%=b2BOrder.get("b2b_oid",0l)%>)">生成快递单</a>--%>
										<input name="button" type="button" value="生成快递单"
										onClick="createWaybill(<%=b2BOrder.get("b2b_oid",0l)%>)"
										class="long-button" /> <%
 	}
 %> <%
 	DBRow shippingCompany = expressMgr.getDetailCompany(b2BOrder.get("sc_id",0l));
 		  	  			if(shippingCompany !=null)
 		  	  			{
 %> <%--					  	  				&nbsp;&nbsp;<a href="#" onClick="printWayBill(<%=b2BOrder.get("b2b_oid",0l)%>,'<%=shippingCompany.getString("internal_print_page")%>')">打印快递单</a>--%>
										<input type="button" class="long-button-print" value="打印快递单"
										onclick="printWayBill(<%=b2BOrder.get("b2b_oid",0l)%>,'<%=shippingCompany.getString("internal_print_page")%>')" />
										<%
											}
										%>
									</td>
								</tr>
							</table>
					</div>
					<%
						//如果采购单ID为0，为订单，否则为交货单
					 	if(b2BOrder.get("stock_in_set",B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET) != B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET)
					 {
					%>

					<%
						}
					%>
					<%
						if(b2BOrder.get("declaration",B2BOrderDeclarationKey.NODELARATION) != B2BOrderDeclarationKey.NODELARATION) {
					%>

					<%
						}
					%>
					<%
						if(b2BOrder.get("clearance",B2BOrderClearanceKey.NOCLEARANCE) != B2BOrderClearanceKey.NOCLEARANCE) {
					%>

					<%
						}
					%>


					<%
						if(tagIntThird != B2BOrderTagKey.NOTAG)
					{
					%>

					<%
						}
					%>
					<%
						if(b2BOrder.get("certificate",certificateKey.NOCERTIFICATE) != certificateKey.NOCERTIFICATE) {
					%>

					<%
						}
					%>


					<%
						if(b2BOrder.get("quality_inspection",B2BOrderQualityInspectionKey.NO_NEED_QUALITY) != B2BOrderQualityInspectionKey.NO_NEED_QUALITY) {
					%>

					<%
						}
					%>

				</div>
			</div>
			<div id="title">
				<table width="100%">
					<tr>
						<td width="16%" align="right" height="25"><font
							style="font-family: 黑体; font-size: 14px;">订单号：</font>
						</td>
						<td align="left" width="16%">B<%=b2b_oid%></td>

						<td width="16%" align="right" height="25"><font
							style="font-family: 黑体; font-size: 14px;">创建人：</font>
						</td>
						<td align="left" width="16%"><%=b2BOrder.getString("create_account")%></td>
						<td width="16%" align="right" height="25"><font
							style="font-family: 黑体; font-size: 14px;">创建时间：</font>
						</td>
						<td align="left"><%=tDate.getFormateTime(b2BOrder.getString("b2b_order_date"))%></td>
					</tr>
				</table>
			</div>
		</div>

		<div id="tabbar" class="tabdown"
			style="height:15px;padding-top:0px;text-align:center;margin:0 auto;padding-left:90%; float:inherit ;cursor:pointer;"
			title="更多" onclick="sildeShow()"></div>

		<table style="margin-top:4px;margin-bottom:4px;" width="98%"
			border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left"><input type='button' class='long-button'
					value='实物图片' onclick='addProductsPicture()' /> <input type='button'
					class='long-long-button' value='第三方标签打印文件'
					onclick='addProductTagTypesFile()' />"</td>
				<td align="right"><input type='button' class='long-button'
					value="更新重量与体积" onclick='updateB2BOrderVW(<%=b2b_oid%>)' /></td>
			</tr>
		</table>

		<script>
	$("#b2BOrderTabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	 
	});
	$("#tagTabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	$("#tabsCertificate").tabs({
 		cache: true,
 		 
		select: function(event, ui){
			 $("#file_with_class_certificate option[value='"+(ui.index+1)+"']").attr("selected",true);
		} 
 	 });
	 // 首先获取 大的tabs,然后获取小的tabs。
	var bigIndex = ($("#b2BOrderTabs").tabs("option","selected")) * 1;
	if(bigIndex == 5){
		var selectedIndex = '<%=file_with_class%>' * 1 ;
		$("#file_with_class_certificate option[value='"+selectedIndex+"']").attr("selected",true);
	}

	function format(id)
	{
		var product_id = jQuery("#gridtest").jqGrid('getCell',id,'b2b_pc_id');
		var clp_type_id = jQuery("#gridtest").jqGrid('getCell',id,'clp_type_id');
		var serial_number = jQuery("#gridtest").jqGrid('getCell',id,'b2b_product_serial_number');

		if(serial_number.length>0)
		{
			$("#gridtest").jqGrid('setColProp','blp_type',{editable:false});
			$("#gridtest").jqGrid('setColProp','clp_type',{editable:false});
		}
		else
		{
			$("#gridtest").jqGrid('setColProp','clp_type',{editType:"select",editoptions:{value:getCLPSelect(product_id)}});
			if (clp_type_id!=0)
			{
				$("#gridtest").jqGrid('setColProp','blp_type',{editable:false});
			}
			else
			{
				$("#gridtest").jqGrid('setColProp','blp_type',{editable:true,editType:"select",editoptions:{value:getBLPSelect(product_id,<%=b2BOrder.get("receive_psid",0l)%>)}});
			}
		}
	}
</script>
		<div id="detail" align="left"
			style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			<table width="98%" border="0" align="center" cellpadding="0"
				cellspacing="0">
				<tr>
					<td>
						<table id="gridtest"></table>
						<div id="pager2"></div> <script type="text/javascript">
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					sortable:true,
					url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/data_d2b_order_item.html',
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.98),
					height:255,
					autowidth: false,
					shrinkToFit:true,
					postData:{b2b_oid:<%=b2b_oid%>},
					jsonReader:{
				   			id:'b2b_detail_id',
	                        repeatitems : false
	                	},
	                	colNames:['b2b_detail_id','商品名','商品条码','货物数','备件数','计划装箱数','V(cm³),W(Kg),<%=b2BOrder.get("b2b_order_status",0)==B2BOrderKey.FINISH?"运费":"估算运费"%>','所在箱号','商品序列号','使用CLP','使用BLP','b2b_oid','b2b_pc_id','clp_type_id','blp_type_id','批次','已上传图片'], 
				   	colModel:[ 
				   		{name:'b2b_detail_id',index:'b2b_detail_id',hidden:true,sortable:false},
				   		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
				   		{name:'p_code',index:'p_code',align:'left'},
				   		{name:'b2b_delivery_count',width:50,index:'b2b_delivery_count',editrules:{required:true,number:true,minValue:0,integer:true},editable:<%=edit%>,sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},
				   		{name:'b2b_backup_count',width:50,index:'b2b_backup_count',editrules:{required:true,number:true,minValue:0,integer:true},editable:<%=edit%>,sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},
				   		{name:'b2b_count',width:50,index:'b2b_count',editrules:{required:true,number:true,minValue:1},sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},
						{name:'volume_weight_freight',width:80,index:'volume_weight_freight',align:'left',sortable:false},
				   		{name:'b2b_box',width:50,index:'b2b_box',editable:false},
				   		{name:'b2b_product_serial_number',width:50,index:'b2b_product_serial_number',editable:false},
				   		{name:'clp_type',width:30,index:'clp_type',editable:<%=edit%>,edittype:'select',sortable:false},
				   		{name:'blp_type',width:30,index:'blp_type',edittype:'select',sortable:false},
				   		{name:'b2b_oid',index:'b2b_oid',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=b2b_oid%>},hidden:true,sortable:false},
				   		{name:'b2b_pc_id',index:'b2b_pc_id',hidden:true,sortable:false},
				   		{name:'clp_type_id',index:'clp_type_id',hidden:true,sortable:false},
				   		{name:'blp_type_id',index:'blp_type_id',hidden:true,sortable:false},
				   		{name:'lot_number',width:60,index:'lot_number',hidden:false,editable:<%=edit%>},
				   		{name:'button',index:'button',align:'left',sortable:false},
				   		], 
				   	rowNum:-1,//-1显示全部
				   	pgtext:false,
				   	pgbuttons:false,
				   	mtype: "GET",
				   	gridview: true, 
					rownumbers:true,
				   	pager:'#pager2',
				   	sortname: 'b2b_detail_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
				   	multiselect: true,
				   	onSelectCell:function(id,name,val)
					{
	                	if(name=="button")
   					 	{
	                		var product_id = jQuery("#gridtest").jqGrid('getCell',id,'b2b_pc_id');
   					 		addProductPicture(product_id);
   					 	}
	                	format(id);
					},
					formatCell:function(id,name,val,iRow,iCol)
					{
						format(id);
					},
				   	afterEditCell:function(id,name,val,iRow,iCol)
				   	{ 
				   		if(name=='p_name') 
				   		{
				   			autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
				   		}
				   		select_iRow = iRow;
				   		select_iCol = iCol;
				    }, 
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				 	{ 
				   		if(name=='p_name'||name=='b2b_delivery_count'||name=='b2b_backup_count') 
				   		{
				   			ajaxModProductName(jQuery("#gridtest"),rowid);
				   			
				   	  	}
				   	  	if(name=='clp_type'||name=='blp_type')
				   	  	{
				   	  		ajaxModProductName(jQuery("#gridtest"),rowid);
				   	  		format(rowid);
						}

				   	}, 
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/GridEditB2BOrderAction.action',
				   	errorCell:function(serverresponse, status)
				   	{
				   		alert(errorMessage(serverresponse.responseText));
				   	},
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/GridEditB2BOrderAction.action'
				}); 		   	
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
			   
			   
				function getB2bOrderSumVWP(b2b_oid)
				{
					$.ajax({
								url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/GetB2BOrderSumVWPJsonAction.action',
								type: 'post',
								dataType: 'json',
								timeout: 60000,
								cache:false,
								data:{b2b_oid:b2b_oid},
								
								beforeSend:function(request){
								},
								
								error: function(e){
									alert(e);
									alert("提交失败，请重试！");
								},
								
								success: function(date)
								{
									$("#sum_volume").html("总体积:"+date.volume+" cm³");
									$("#sum_weight").html("总重量:"+date.weight+" Kg");
									$("#sum_price").html("总货款:"+date.send_price+" RMB");
								}
							});
				}
	</script></td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td colspan="2"><span id="sum_volume"></span> <span
						id="sum_price"></span> <span id="sum_weight"></span></td>
				</tr>
			</table>
		</div>
	</div>


	<form name="download_form" method="post"></form>

	<form action="" method="post" name="applicationApprove_form">
		<input type="hidden" name="b2b_oid" />
	</form>
	<form action="" method="post" name="followup_form">
		<input type="hidden" name="b2b_oid" /> <input type="hidden"
			name="type" value="1" /> <input type="hidden" name="stage" value="1" />
		<input type="hidden" name="b2b_order_type" value="1" /> <input
			type="hidden" name="b2b_order_content" /> <input type="hidden"
			name="expect_date" />
	</form>
	<script type="text/javascript">
	function readyPacking(b2b_oid)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/b2b_order_storage_show.html?b2b_oid='+b2b_oid;
		$.artDialog.open(url, {title: '转运缺货商品',width:'700px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function receiveAllocate(b2b_oid)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/b2b_order/create_b2b_waybill_new.html?b2b_oid='+b2b_oid;
		$.artDialog.open(url, {title: '预保留库存',width:'870px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function applicationApprove(b2b_oid)
	{
		if(confirm("确定申请T"+b2b_oid+"订单完成?"))
		{
			document.applicationApprove_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/addB2bOrderApprove.action";
			document.applicationApprove_form.b2b_oid.value = b2b_oid;
			document.applicationApprove_form.submit();
		}
	}
	function addProductsPicture(){
		var b2b_oid = '<%=b2b_oid%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/b2b_order/b2b_order_product_picture_up.html?b2b_oid="+b2b_oid;
	 	var addUri =  getSelectedIdAndNames();
	 	if($.trim(addUri).length < 1 ){
			showMessage("请先选择商品!","alert");
			return ;
		}else{uri += addUri;}
		$.artDialog.open(uri , {title: "实物图片上传["+b2b_oid+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	function addProductPicture(pc_id){
		 //添加商品图片
		var b2b_oid = '<%=b2b_oid%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/b2b_order/b2b_order_product_picture_up.html?b2b_oid="+b2b_oid+"&pc_id="+pc_id;
		$.artDialog.open(uri , {title: "商品范例上传["+b2b_oid+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,close:function(){window.location.reload();},fixed: true});
	}
	function addProductTagTypesFile(){
		 //添加商品标签
		var b2b_oid = '<%=b2b_oid%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/b2b_order/b2b_order_product_tag_file.html?b2b_oid="+b2b_oid ;
		var addUri =  getSelectedIdAndNames();
	 	if($.trim(addUri).length < 1 ){
			showMessage("请先选择商品!","alert");
			return ;
		}else{uri += addUri;}
		 $.artDialog.open(uri , {title: "第三方标签上传["+b2b_oid+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	function getSelectedIdAndNames(){
		s = jQuery("#gridtest").jqGrid('getGridParam','selarrrow')+"";  
	  	 
	 	var array = s.split(",");
 		var strIds = "";
 		var strNames = "";
	 	for(var index = 0 , count = array.length ; index < count ; index++ ){
		   var number = array[index];
	 	   var thisid= $("#gridtest").getCell(number,"b2b_pc_id");
		    var thisName =$("#gridtest").getCell(number,"p_name"); 
	 	  	strIds += (","+thisid);
	 	  	strNames += (","+thisName);
		}
		if(strIds.length > 1 ){
		    strIds = strIds.substr(1);
		    strNames = strNames.substr(1);
		}
		if(strIds+"" === "false"){return "";}
		return  "&pc_id="+strIds;
	}
	//点击弹出制作麦头的界面
	function shippingMark()
	{
		//如果不是0就是交货型订单
	    var b2b_oid=<%=b2b_oid%>;
        var awb="<%=b2BOrder.getString("b2b_order_waybill_number")%>";
        var awbName="<%=b2BOrder.getString("b2b_order_waybill_name")%>";
	    var menpai="<%=b2BOrder.getString("deliver_house_number")%>";
	    var jiedao="<%=b2BOrder.getString("deliver_street")%>";
	    var chengshi="<%=b2BOrder.getString("deliver_city")%>";
	    var youbian="<%=b2BOrder.getString("deliver_zip_code")%>";
	    var shengfen="<%=ps_id4%>";
	    var guojia="<%=ccid4%>";
	    var lianxiren="<%=b2BOrder.getString("b2b_order_linkman")%>";
	    var dianhua="<%=b2BOrder.getString("b2b_order_linkman_phone")%>";
	    var receive_psid = "<%=b2BOrder.get("receive_psid",0l)%>";
	    var address_state_deliver = '<%=b2BOrder.getString("address_state_deliver")%>';
	    var uri = "../b2b_order/b2b_order_print_shipping_mark.html?awbName="+awbName+"&dianhua="+dianhua+"&lianxiren="+lianxiren+"&guojia="+guojia+"&address_state_deliver="+address_state_deliver
	    +"&shengfen="+shengfen+"&youbian="+youbian+"&chengshi="+chengshi+"&jiedao="+jiedao+"&menpai="+menpai+"&b2b_oid="+b2b_oid+"&awb="+awb+"&receive_psid="+receive_psid; 
		$.artDialog.open(uri , {title: '唛头标签打印',width:'600px',height:'420px', lock: true,opacity: 0.3,fixed: true});
	} 
	//点击制作标签后修改成弹出dialog
	function printLabel()
	{
		//如果不是0就是交货型订单
        var supplierSid =<%=b2BOrder.get("send_psid",0l)%>;
        var b2b_oid=<%=b2b_oid%>;
        var warehouse="<%=null!=catalogMgr.getDetailProductStorageCatalogById(b2BOrder.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(b2BOrder.get("receive_psid",0l)).getString("title"):""%>";
       	var uri = "../b2b_order/b2b_order_made_internal_label.html?warehouse="+warehouse+"&b2b_oid="+b2b_oid+"&supplierName="+""+"&supplierSid="+supplierSid; 
		$.artDialog.open(uri , {title: '订单内部标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});		
	}
	
	function showDifferent(selectIndex)
	{
		$.artDialog.open('b2b_order_different_show.html?b2b_oid=<%=b2b_oid%>&select_index='+selectIndex, {title: "订单差异",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	
</script>
	<script>
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	cookie: {expires: 30000},
	load: function(event, ui) {onLoadInitZebraTable();}	
});

  $("#tabbar").bind({
  click: function() {
        if($("#tabbar").attr("class")=="tabup"){
         $("#title").slideDown();
            $("#cc").slideUp();
           $("#tabbar").attr("class", "tabdown");
        }else{
           $("#cc").slideDown();
              $("#title").slideUp();
           $("#tabbar").attr("class", "tabup");
        }
  }
  
  });
 function funfff(){
 $("#tabbar").click();
 
 }
</script>
</body>
</html>

