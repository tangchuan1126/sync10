<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%
	long b2b_oid = StringUtil.getLong(request,"b2b_oid");
	int b2b_type = StringUtil.getInt(request,"b2b_type");
	TDate tdate = new TDate();
	
	DBRow [] rows;
	int[] b2b_types = {b2b_type};
	rows = b2BOrderMgrZyj.getB2BOrderLogsByB2BOrderIdAndType(b2b_oid, b2b_types);
		//transportMgrLL.getTransportLogsByType(b2b_oid,b2b_type);
	HashMap followuptype = new HashMap();
	//操作日志类型:1表示进度跟进2:表示财务跟进;3修改日志;1货物状态;5运费流程;6进口清关;7出口报关8标签流程;9:单证流程
	followuptype.put(1,"Create"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"Update");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"内部标签");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	followuptype.put(13,"到货通知仓库");
	followuptype.put(14,"第三方标签");
	followuptype.put(15,"缷货司机签到");
	followuptype.put(16, "装货司机签到");
	/*
	followuptype.put(1,"创建记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"内部标签");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	followuptype.put(13,"到货通知仓库");
	followuptype.put(14,"第三方标签");
	followuptype.put(15,"缷货司机签到");
	followuptype.put(16, "装货司机签到");
	*/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>

</head>

<body >
<form name="transfer_form" method="post">
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
	<input type="hidden" name="transfer_type"/>
	<input type="hidden" name="money_status"/>
</form>

<form action="" method="post" name="followup_form">
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
</form>
	
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="10%"  class="left-title " style="vertical-align: center;text-align: center;">Operator</th>
        <th width="35%"  class="left-title " style="vertical-align: center;text-align: center;">Remarks</th>
        <th width="13%"  class="right-title " style="vertical-align: center;text-align: center;">Operation Time</th>
        <th width="10%"  class="right-title " style="vertical-align: center;text-align: center;">Operate</th>
        <!-- th width="10%"  class="right-title " style="vertical-align: center;text-align: center;">预计完成</th -->
  	</tr>
  	<% 
  		for(int i=0;i<rows.length;i++)
  		{
  	%>
  		<tr align="center" valign="middle">
		    <td height="30">
		    <%  DBRow admin= adminMgrLL.getAdminById(rows[i].get("b2ber_id", ""));
		    if(admin!=null){
		    	out.print(admin.get("employe_name",""));
		    }
		    
		    
		    %>
		    
		    
		    </td>
		    <td align="left">
		    	<%
		    		out.print(rows[i].get("b2b_content",""));
		    	%>	      
		    </td>
		    <td><%=tdate.getEnglishFormateTime(rows[i].getString("b2b_date")) %></td>
		    <td><%=followuptype.get(rows[i].get("b2b_type",0))%></td>
		    <!-- td><%=  rows[i].get("time_complete","")%>&nbsp;</td -->
	  	</tr>
  	<%
  		}
  	%>
 	
</table>
</body>
</html>



