<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="org.directwebremoting.json.types.JsonArray"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.ChangeStatusKey"%>
<%@ include file="../../include.jsp"%> 
<%
 	ChangeStatusKey changeStatusKey = new ChangeStatusKey();

 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 long adminId=adminLoggerBean.getAdid();
 //根据登录帐号判断是否为客户
 Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
 //如果是用户登录 查询用户下的产品
 DBRow[] titles;
 long number=0;
 if(bl){
 	number=1;
 	titles=adminMgrZwb.getProprietaryByadminId(adminLoggerBean.getAdid());
 }else{
 	number=0;
 	titles=mgrZwb.selectAllTitle();
 }

 String storageStringForNewSearch = productStorageMgrZJ.loadProductStorageForNexSearch();
 %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>商品信息</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<%-- ***********************  --%>
<%-- 此版本的jquery ui与新版autocomplete不兼容
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-ui-1.8.14.custom.min.js"></script>
--%>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<%-- Frank ****************** --%>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<%-- ***********************  --%>

<%-- 此版本的jquery ui与新版autocomplete不兼容
<link type="text/css" href="../js/jqGrid-4.1.1/js/jquery-ui-1.8.1.custom.css" rel="stylesheet"/>
--%>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />


<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<style type="text/css">
	td.categorymenu_td div{margin-top:-3px;}
</style>
<script language="javascript">

	function loadData(){
		
		var storages = <%=storageStringForNewSearch%>;
		var titles = <%=new JsonObject(titles).toString()%> ;
		var item=[];
		for(var i=0;i<titles.length;i++){
			var op={};       
			op.id=titles[i].title_id;                                                                                                                                                                                                                                                                                                                                                                                                      
			op.name=titles[i].title_name;
			item.push(op);
		}
		var data=[
		 {key:'TITLE：',type:'title',array:item},
		 {key:'仓库：',select:'true',type:'cc_ps',array:storages},	 
		 {key:'状态：',select:'true',type:'cc_status',array:[
		 		{id:<%=ChangeStatusKey.Readying%>,name:'<%=changeStatusKey.getChangeStatusKeyById(ChangeStatusKey.Readying)%>'},
		 		{id:<%=ChangeStatusKey.Allocate%>,name:'<%=changeStatusKey.getChangeStatusKeyById(ChangeStatusKey.Allocate)%>'},
		 		{id:<%=ChangeStatusKey.Picking%>,name:'<%=changeStatusKey.getChangeStatusKeyById(ChangeStatusKey.Picking)%>'},
		 		{id:<%=ChangeStatusKey.Finish%>,name:'<%=changeStatusKey.getChangeStatusKeyById(ChangeStatusKey.Finish)%>'}
		 	]
		 }
	 	 ];
		initializtion(data);  //初始化
	}	
	
	//点击查询条件
	function custom_seach()
	{
		var array=getSeachValue();
		var title_id = 0;
		var ps_id = 0;
		var cc_status = 0;
		for(var i=0;i<array.length;i++)
		{
			 if(array[i].type=='title')
			 {
				   title_id = array[i].val;
			 }
			 if(array[i].type=='cc_ps')
			 {
				 ps_id = array[i].val;
			 }
			 if(array[i].type=='cc_status')
			 {
				 cc_status = array[i].val;
			 }
		}
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/config_change/config_change_list.html',
			type: 'post',
			dataType: 'html',
			data:
			{
				ps_id:ps_id,
				title_id:title_id,
				cc_status:cc_status
			},
			async:false,
			success: function(html){
				$.unblockUI();   //遮罩关闭
				$("#showList").html(html);
				onLoadInitZebraTable(); //重新调用斑马线样式
				
			}
		});	
	}
	
	function addConfigChange()
	{
		var  uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/config_change/add_config_change.html"; 
	    $.artDialog.open(uri , {title: "创建配置修改",width:'520px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:middle;
		padding:3px;
	}
	<style type="text/css">
<!--
.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:29px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="loadData()">
<br/>
	
	<div id="tabs">
		<ul>
			<li><a href="#common">常用工具</a></li>
			<li><a href="#av1">条件搜索</a></li>		 
		</ul>
		<div id="common">
			<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="30%" style="padding-top:3px;">
			   <div id="easy_search_father">
				   <div id="easy_search">
				   		<a href=""><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"></a>
				   	</div>
			   </div>
				<table width="485" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				    <td width="418">
						<div  class="search_shadow_bg">
							  <input onclick="" type="text" name="search_key" id="search_key" value="" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333">
						</div>
					</td>
				    <td width="67">
					 <a href="javascript:void(0)" id="search_help" style="padding:0px;color: #000000;text-decoration: none;"><img src="../imgs/help.gif" border="0" align="absmiddle" border="0"></a>
					</td>
				  </tr>
				</table>
			</td>
              <td width="45%" ></td>
              <td width="12%" align="right" valign="middle">
				<!--  
			 	 <a href="javascript:tb_show('手工创建订单','add.html?TB_iframe=true&height=500&width=850',false);"><img src="../imgs/create_order.jpg" border="0"></a>	
			  	-->	  
			  	<a href="javascript:addConfigChange();"><img src="../imgs/create_order.jpg" border="0"></a>
			 </td>
			 </tr>
          </table>
		</div>
		<div id="av1">
			<!-- 条件搜索 -->
			<div id="av"></div>
			<input type="hidden" id="atomicBomb" value=""/>
			<input type="hidden" id="title" />
		</div>
	</div>
	<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,  
		cookie: { expires: 30000 } ,
	});
	</script>
<br/>
<div id="showList"></div>
</body>
</html>
<script>

(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();
</script>
