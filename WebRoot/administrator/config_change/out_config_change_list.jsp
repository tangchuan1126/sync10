<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@page import="com.cwc.app.key.ContainerTypeKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	long out_id = StringUtil.getLong(request,"out_id");  			 //运单号
	DBRow[] areaRow=transportMgrZwb.findArea(out_id);                //区域
	//DBRow[] slcRow=transportMgrZwb.findSlcId(out_id);                //位置
	
	DBRow[] pickDetail = transportMgrZwb.selectOutDetail(out_id);    //捡货的详细
	DBRow outRow= waybillMgrZwb.selectOutOrder(out_id);
	DBRow[] doorOrLocation=waybillMgrZwb.getStorageOrDoorByOutId(ProductStoreBillKey.PICK_UP_ORDER,out_id);
%>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0"  style="margin-top:5px;">
	<tr>
		<td>
			<div>
				<span style="font-size:19px; font-weight: bold;">
					<%=catalogMgr.getDetailProductStorageCatalogById(outRow.get("ps_id",0l)).getString("title")%>仓库<%=out_id %>号拣货单
				</span>
			</div>
			<div align="right">
				<span><%= outRow.getString("create_time").substring(5,16)%></span>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
				<tr>
					<td style="background-color:#eeeeee;width:200px; border-left:2px solid #000000;border-right:2px solid #000000;border-top:2px solid #000000">DOOR</td>
					<td style="background-color:#eeeeee;border-top:2px solid #000000;border-right:2px solid #000000;">
					 	<%for(int a=0;a<doorOrLocation.length;a++){ %>
					 	    <%=doorOrLocation[a].getString("door")%>
					 	<%} %>
					</td>
				</tr>
				<tr>
					<td style="background-color:#eeeeee; border-left:2px solid #000000;border-right:2px solid #000000;border-top:2px solid #000000;">LOCATION</td>
					<td style="background-color:#eeeeee;border-top:2px solid #000000;border-right:2px solid #000000;">
						<%for(int b=0;b<doorOrLocation.length;b++){ %>
					 	    <%=doorOrLocation[b].getString("location")%>
					 	<%} %>
					</td>
				</tr>		
			</table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
				<tr>
					<td colspan="4" style="border:2px #000000 solid" >&nbsp;</td>
				</tr>
				<%for(int a=0;a<areaRow.length;a++){ %>
				   <tr>
			  			<td colspan="4" align="left" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;border-right: 2px #000000 solid;background-color:#eeeeee">
			  				<%DBRow qy= waybillMgrZwb.findStorageLocationAreaById(areaRow[a].get("out_list_area_id",0l));%>
			  				<span style="color:blue;">区域：<%=qy.getString("area_name") %></span>
			  			</td>
			  	   </tr>
			  	   <%DBRow[] slcIdRow= transportMgrZwb.findSlcByAreaId(areaRow[a].get("out_list_area_id",0l),out_id); %>
			  	   <%for(int i=0;i<slcIdRow.length;i++){ %>
				  	   <tr>
				  	  	   <td colspan="4" align="left" style="padding-left:20px;border-left:2px #000000 solid;border-bottom: 2px #000000 solid;border-right:2px #000000 solid;font-family:Arial;font-size: 16px;">
				  	       		<%DBRow weizhi= waybillMgrZwb.findStorageLocationCatalogById(slcIdRow[i].get("out_list_slc_id",0l));%>
				  	       		<span style="color:red">
				  	       			位置：<%=weizhi.getString("slc_position_all") %>
				  	       			<%DBRow isTwoD=transportMgrZwb.findCollocationDetail(weizhi.getString("slc_position_all")); %>
				  	       			<%if(isTwoD.get("is_three_dimensional",0l)==0){%>
				  	       			  [2D]
				  	       			<%}else{%>
				  	       			  [3D]
				  	       			<%}%>
				  	       		</span>
				  	       		<%for(int k=0;k<pickDetail.length;k++){ %>
				  	       			<%if(pickDetail[k].get("out_list_area_id",0l)==areaRow[a].get("out_list_area_id",0l) && pickDetail[k].get("out_list_slc_id",0l)==slcIdRow[i].get("out_list_slc_id",0l)){ %>
				  	       				<table width="100%" border="0" cellspacing="0" cellpadding="0">
										  <tr>
										    <td align="left" width="180px" style="padding-left:20px; border-bottom:1px dashed #999">
										        <%if(pickDetail[k].get("from_con_id",0l)!=0){ %>
										    		<span style="font-size:15px;">F-<%= new ContainerTypeKey().getContainerTypeKeyValue(pickDetail[k].get("from_container_type",0))%>#<%=pickDetail[k].get("from_con_id",0l) %></span>
										  	    <%}else if(pickDetail[k].get("from_container_type_id",0l)!=0){ %>
							  	       				<span style="font-size:15px;">F-<%= new ContainerTypeKey().getContainerTypeKeyValue(pickDetail[k].get("from_container_type",0))%>-TYPE#<%=pickDetail[k].get("from_container_type_id",0l) %></span>
										        <%}else if(pickDetail[k].get("from_container_type",0l)!=0){%>
										            <span style="font-size:15px;">F-<%= new ContainerTypeKey().getContainerTypeKeyValue(pickDetail[k].get("from_container_type",0))%></span>
										        <%}%>								 
										    </td>
										    <td align="left" width="250px;" style="padding-left:20px; border-bottom:1px dashed #999">
										    	
							  	       			<%if(pickDetail[k].get("pick_container_type_id",0l)!=0 && pickDetail[k].get("pick_con_id",0l)==0){ %>
							  	       				<span style="font-size:15px;">P-<%= new ContainerTypeKey().getContainerTypeKeyValue(pickDetail[k].get("pick_container_type",0))%>-TYPE#<%=pickDetail[k].get("pick_container_type_id",0l) %></span>
							  	       				&nbsp;&nbsp;&nbsp;&nbsp;
							  	       			<%}else if(pickDetail[k].get("pick_container_type",0l)!=0 && pickDetail[k].get("pick_container_type_id",0l)==0 && pickDetail[k].get("pick_con_id",0l)==0){%>
							  	       				<span style="font-size:15px;">P-<%= new ContainerTypeKey().getContainerTypeKeyValue(pickDetail[k].get("pick_container_type",0))%></span>
							  	       				&nbsp;&nbsp;&nbsp;&nbsp;
							  	       			<%} %>
										    
										    	<%if(pickDetail[k].get("pick_con_id",0l)!=0){ %>
										    		<span style="font-size:15px;">P-<%= new ContainerTypeKey().getContainerTypeKeyValue(pickDetail[k].get("pick_container_type",0))%>#<%=pickDetail[k].get("pick_con_id",0l) %></span>
										    	<%}else if(pickDetail[k].get("pick_container_type",0l)!=0){ %>
										    		
										    	<%}else{ %>
										    		<span style="font-size:15px;">P-O#<%=pickDetail[k].get("out_list_pc_id",0l) %></span>
										    	<%} %>
										    	
										    </td>
										    <td align="left" style="padding-left:20px; border-bottom:1px dashed #999">
										    <%if(pickDetail[k].get("pick_con_id",0l)!=0 || pickDetail[k].get("pick_container_type",0l)!=0){ %>
				  	       						<span style="font-size:15px;">QTY:<%=pickDetail[k].get("pick_container_quantity",0d) %></span>
				  	       					<%}else{ %>
				  	       						<span style="font-size:15px;">QTY:<%=pickDetail[k].get("pick_up_quantity",0d) %></span>
				  	       					<%} %>	
										    </td>
										  </tr>
										</table>
									<%}%>				  	       
				  	       		<%}%>			  	 		  	   
				  	       </td>				  	   	    
				  	   </tr>
			  	   <%} %>
				<%} %>
			</table>
		</td>
	</tr>
</table>
