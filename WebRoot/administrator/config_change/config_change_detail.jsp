<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.B2BOrderKey"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.ChangeStatusKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 

<% 
	long cc_id = StringUtil.getLong(request,"cc_id");
	DBRow configChange = configChangeMgrZJ.getDetailConfigChangeByCcid(cc_id);
	boolean edit = false;
	if(configChange.get("cc_status",0)==ChangeStatusKey.Readying)
	{
		edit = true;
	}
	
	String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/config_change/config_change_detail.html?cc_id="+cc_id;
	String LPSelect = LPTypeMgrZJ.getLPTypeSelectForJqgrid();
	ChangeStatusKey changeStatusKey = new ChangeStatusKey();
	ProductStoreBillKey productStoreBillKey = new ProductStoreBillKey();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>配置更改</title>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<style type="text/css">
	body{text-align:left;}
	ol.fundsOl{
		list-style-type: none;	
	}
	ol.fundsOl li{
		width: 350px;
		height: 200px;
		float: left;
		line-height: 25px;
	}
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<!-- jqgrid 引用 -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>
<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>

<script type="text/javascript">
	var select_iRow;
	var select_iCol;				
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
	
	function autoComplete(obj)
	{
		addAutoComplete(obj,
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
	}
	
	function onLPChange(obj,union_id)
	{
		obj.attr("onChange","cciTypeIdLoad(this.value,$('#p_name').val(),'"+union_id+"')");
	}
	
	function cciTypeIdLoad(lpType,p_name,union_id)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/LPType/getLPSelectForJqgrid.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				async:false,
				data:{
					lp_type:lpType,
					p_name:p_name,
					title_id:<%=configChange.get("cc_title",0l)%>
				},
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					$("#"+union_id).clearAll();
					$.each(data,function(i){
							$("#"+union_id).addOption(data[i].text,data[i].value);
						});
				}
			});
	}
	
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#p_name"));
		onLPChange($("#cci_type"),"cci_type_id");
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#p_name"));
		onLPChange($("#cci_type"),"cci_type_id");
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	function refreshWindow(){
		window.location.reload();
	}
	
	function ajaxModProductName(obj,rowid,col)
	{
		var unit_name;//单字段修改商品名时更换单位专用
		var para ="cci_id="+rowid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/b2b_order/B2BOrderDetailGetJSONAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				async:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}

	function getBLPSelect(pc_id,receive_id)
	{
		var selectString = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/LPType/getBLPSelectForJqgrid.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			async:false,
			data:{pc_id:pc_id,to_ps_id:receive_id},
			
			beforeSend:function(request){
			},
			
			error: function(){
				alert("提交失败，请重试！");
			},
			
			success: function(data)
			{
				selectString = data;
			}
		});
		return selectString;
	}

</script>



<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:text-top;
		padding-top:2px;
	}
.set{
	border:2px #999999 solid;
	padding:2px;
	width:90%;
	word-break:break-all;
	margin-top:10px;
	margin-top:5px;
	line-height:18px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	margin-bottom: 10px;
}
	.create_order_button{background-attachment: fixed;background: url(../imgs/create_order.jpg);background-repeat: no-repeat;background-position: center center;height: 51px;width: 129px;color: #000000;border: 0px;}
	.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
	.zebraTable td {line-height:25px;height:25px;}
	.right-title{line-height:20px;height:20px;}
	span.spanBold{font-weight:bold;}
	span.fontGreen{color:green;}
	span.fontRed{color:red;}
	span.spanBlue{color:blue;}
</style>
<script>
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,_target){
    var targetNode = $("#"+_target);
    $("input[name='file_names']",targetNode).val(fileNames);
    if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
        $("input[name='file_names']").val(fileNames);
        var myform = $("#"+_target);
        var file_with_class = $("#file_with_class").val();
			$("input[name='backurl']",targetNode).val("<%= backurl%>" + "&file_with_class="+file_with_class);
		  	myform.submit();
	}
}

function receiveAllocate(cc_id)
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/config_change/config_change_allocate.html?cc_id='+cc_id;
	$.artDialog.open(url, {title: '转运缺货商品',width:'700px',height:'400px', lock: true,opacity: 0.3});
}

function uploadCongifChangeItem(cc_id)
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/config_change/config_change_upload_excel.html?cc_id='+cc_id;
	$.artDialog.open(url, {title: '上传配置变更明细',width:'700px',height:'400px', lock: true,opacity: 0.3});
}

function createOutStoreBill()
{
	document.create_outStorebill_form.submit();
}

function showOutStoreBill()
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/config_change/out_config_change_list.html?out_id=<%=configChange.get("cc_out_id",0l)%>';
	$.artDialog.open(url, {title: '上传配置变更明细',width:'700px',height:'400px', lock: true,opacity: 0.3});
}
</script>
</head>

<body onload="onLoadInitZebraTable();">
<div class="demo">
<div id="configChangeTabs">
	<ul>
		<li><a href="#config_change_info">基础信息<span> </span></a></li>
	</ul>
	<div id="b2b_order_basic_info">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="60%" valign="top">
					<table width="100%" height="100%">
						<tr>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">配置单号：</font></td>
							<td align="left" width="16%">CC<%=cc_id%></td>
							<td align="right" nowrap="nowrap"><font style="font-family: 黑体; font-size: 14px;">状态:</font></td>
							<td align="left" nowrap="nowrap"><%=changeStatusKey.getChangeStatusKeyById(configChange.get("cc_status",0))%></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">创建日期：</font></td>
							<td align="left" width="16%">
							<%=configChange.getString("cc_create_time").substring(2,19)%>
							</td>
							<td width="16%" align="right"height="25"><font style="font-family: 黑体; font-size: 14px;">创建人：</font></td>
							<td align="left" width="16%"><%=adminMgr.getDetailAdmin(configChange.get("cc_create_adid",0l)).getString("employe_name")%></td>
						</tr>
						<tr>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">关联单据：</font></td>
							<td colspan="5">
							<%=productStoreBillKey.getProductStoreBillAcronymId(configChange.get("cc_system_type",0))+configChange.get("cc_system_id",0l)%>
							</td>
						</tr>
						<tr>
							<td height="25" valign="bottom"><input name="button" type="button" class="long-button" value="保留库存" onClick="receiveAllocate(<%=cc_id%>)"/></td>
							<td  align="left" height="25" valign="bottom"><input name="button" type="button" class="long-button" value="上传明细" onClick="uploadCongifChangeItem(<%=cc_id%>)"/></td>
							<td  align="left" height="25" valign="bottom">
								<%
									if(configChange.get("cc_status",0)==ChangeStatusKey.Allocate)
									{
								%>
									<input name="button" type="button" class="long-button" value="生成拣货单" onClick="createOutStoreBill()"/>	
								<%
									}
								%>
								<%
									if(configChange.get("cc_status",0)==ChangeStatusKey.Picking)
									{
								%>
									<input name="button" type="button" class="long-button" value="查看拣货单" onClick="showOutStoreBill()"/>	
								<%
									}
								%>
							</td>
							<td colspan="3">
							
							</td>
						</tr>
					</table>
				</td>
				<td width="40%">
				</td>
			</tr>
			<tr>
				<td colspan="2" height="15px"></td>
			</tr>
			<tr>
				<td colspan="2" align="left"><br/>
					
				</td>
			</tr>
		</table>
	</div>
</div>


<script>
	$("#configChangeTabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	 
	});
	
	
	function format(id)
	{
		var product_id = jQuery("#gridtest").jqGrid('getCell',id,'cci_pc_id');
		var cci_type = jQuery("#gridtest").jqGrid('getCell',id,'cci_type');

		if(serial_number.length>0)
		{
			$("#gridtest").jqGrid('setColProp','blp_type',{editable:false});
			$("#gridtest").jqGrid('setColProp','clp_type',{editable:false});
		}
		else
		{
			$("#gridtest").jqGrid('setColProp','clp_type',{editType:"select",editoptions:{value:getCLPSelect(product_id)}});
			if (clp_type_id!=0)
			{
				$("#gridtest").jqGrid('setColProp','blp_type',{editable:false});
			}
			else
			{
				$("#gridtest").jqGrid('setColProp','blp_type',{editable:true,editType:"select",editoptions:{value:getBLPSelect(product_id,0)}});
			}
		}
	}
	
	function ajaxLoadConfigChangeItem(obj,rowid,col)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/config_change/ajaxLoadConfigChange.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				async:false,
				data:{
					cci_id:rowid
				},
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
</script>
	<div id="detail" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table id="gridtest"></table>
	<div id="pager2"></div> 
	
	<script type="text/javascript">
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					sortable:true,
					url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/config_change/data_config_change_item.html',
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.98),
					height:255,
					autowidth: false,
					shrinkToFit:true,
					postData:{cc_id:<%=cc_id%>},
					jsonReader:{
				   			id:'cci_id',
	                        repeatitems : false
	                	},
	                	colNames:['cci_id','商品名','商品条码','LPType','LPType_ID','组装数','cci_pc_id','cci_cc_id'], 
				   	colModel:[ 
				   		{name:'cci_id',index:'cci_id',hidden:true,sortable:false},
				   		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
				   		{name:'p_code',index:'p_code',align:'left'},
				   		{name:'cci_type',index:'cci_type',width:30,editable:<%=edit%>,edittype:'select',editoptions:{value:'<%=LPSelect%>'},sortable:false},
				   		{name:'cci_type_id',index:'cci_type_id',editable:<%=edit%>,edittype:'select',editoptions:{value:'-1:请选择'},sortable:false},
				   		{name:'cci_count',index:'cci_count',editrules:{required:true,number:true,minValue:0,integer:true},editable:<%=edit%>,sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},
				   		{name:'cci_pc_id',index:'cci_pc_id',hidden:true,sortable:false},
				   		{name:'cci_cc_id',index:'cci_cc_id',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=cc_id%>},hidden:true,sortable:false},
				   		], 
				   	rowNum:-1,//-1显示全部
				   	pgtext:false,
				   	pgbuttons:false,
				   	mtype: "GET",
				   	gridview: true, 
					rownumbers:true,
				   	pager:'#pager2',
				   	sortname: 'cci_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
				   	multiselect: true,
				   	onSelectCell:function(id,name,val)
					{
					},
					formatCell:function(id,name,val,iRow,iCol)
					{
					
					},
				   	afterEditCell:function(id,name,val,iRow,iCol)
				   	{ 
				   		if(name=='p_name') 
				   		{
				   			autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
				   		}
				   		
				   		if(name=="cci_type_id")
				   		{
				   			var cci_type = jQuery("#gridtest").jqGrid('getCell',id,'cci_type');
				   			
				   			if(cci_type=='CLP')
				   			{
				   				cci_type = 1;
				   			}
				   			else if(cci_type =='BLP')
				   			{
				   				cci_type = 2;
				   			}
				   			else if(cci_type=='ILP')
				   			{
				   				cci_type = 4;
				   			}
				   			else if(cci_type=='Orignail')
				   			{
				   				cci_type = 0;
				   			}
				   			
				   			var p_name = jQuery("#gridtest").jqGrid('getCell',id,'p_name');
				   			cciTypeIdLoad(cci_type,p_name,iRow+"_cci_type_id");
				   		}
				   		
				   		select_iRow = iRow;
				   		select_iCol = iCol;
				    }, 
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				 	{ 
				 		if(name=="p_name"||name=="cci_type")
				 		{
				 			ajaxLoadConfigChangeItem(jQuery("#gridtest"),rowid);
				 		}
				   		
				   	}, 
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/config_change/gridEditConfigChangeItem.action',
				   	errorCell:function(serverresponse, status)
				   	{
				   		alert(errorMessage(serverresponse.responseText));
				   	},
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/config_change/gridEditConfigChangeItem.action'
				}); 		   	
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
			   
			   
	</script>
		</td>
	</tr>
</table>

	</div>
  </div>
</div>

<form name="download_form" method="post"></form>
<form name="create_outStorebill_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/outbound/createOutStoreBillForConfigChange.action">
	<input type="hidden" name="cc_id" value="<%=cc_id%>"/>
</form>
</body>
</html>

