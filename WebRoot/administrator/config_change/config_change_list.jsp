<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="javax.mail.Transport"%>
<%@page import="com.cwc.app.key.ChangeStatusKey"%>
<%@include file="../../include.jsp"%> 
<%@page import="com.cwc.app.api.zr.BoxTypeMgrZr"%>
<%@page import="com.cwc.app.beans.AdminLoginBean"%>
<%@taglib uri="/turboshop-tag" prefix="tst" %>

<%
long ps_id = StringUtil.getLong(request,"ps_id");
long title_id = StringUtil.getLong(request,"title_id");
int cc_status = StringUtil.getInt(request,"cc_status");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

DBRow[] configChangeRows = configChangeMgrZJ.getSearchConfigChange(ps_id,title_id,cc_status,pc);
ChangeStatusKey changeStatusKey = new ChangeStatusKey();
%>
<html>
<head>
<title>商品相关数据管理查询结果显示页面</title>
<style type="text/css">
ul.myulPcTitles{list-style-type: none;}
</style>
</head>
<body>
<table width="98%" border="0" isBottom="true" cellpadding="0" cellspacing="0" class="zebraTable" >
	<tr> 
		<th width="18%" class="right-title"  style="vertical-align: center;text-align: center;">ConfigChange</th>
		<th width="27%" align="center" class="right-title" style="vertical-align: center;text-align: cenleftter;">仓库</th>
	 	<th width="18%"  style="vertical-align: center;text-align: center;" class="right-title">TITLE</th>
	 	<th width="18%" class="right-title"  style="vertical-align: center;text-align: center;">状态</th>
		<th width="18%" class="right-title"  style="vertical-align: center;text-align: center;">创建时间</th>
	</tr>
	<%
		for(int i=0;i<configChangeRows.length;i++)
		{
	%>
	<tr>
		<td align="center"><a style="text-decoration:none" href="config_change_detail.html?cc_id=<%=configChangeRows[i].get("cc_id",0l)%>"><%=configChangeRows[i].get("cc_id",0l)%></a></td>
		<td><%=catalogMgr.getDetailProductStorageCatalogById(configChangeRows[i].get("cc_psid",0l)).getString("title")%></td>
		<td><%=proprietaryMgrZyj.getDetailTitleByTitleId(configChangeRows[i].get("cc_title",0l)).getString("title_name")%></td>
		<td align="center"><%=changeStatusKey.getChangeStatusKeyById(configChangeRows[i].get("cc_status",0))%></td>
		<td><%=configChangeRows[i].getString("cc_create_time").substring(0,19)%></td>
	</tr>
	<%
		}
	%>
</table>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>       
    <td height="28" align="right" valign="middle">
      <%
		int pre = pc.getPageNo() - 1;
		int next = pc.getPageNo() + 1;
		out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
		out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
		out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
		out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
		out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	 %>
    	  跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input type="hidden" id="page_ps" value="<%=ps_id%>"/>
      <input type="hidden" id="page_title" value="<%=title_id%>"/>
      <input type="hidden" id="page_status" value="<%=cc_status%>"/>
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
  </tr>
</table>
</body>
</html>