<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.beans.PreCalcuConfigChangeBean"%>
<%@page import="com.cwc.app.key.ContainerTypeKey"%>
<%@page import="com.cwc.app.key.ConfigChangeStoreStatusKey"%>
<%@page import="com.cwc.app.key.ChangeStatusKey"%>
<%@ include file="../../include.jsp"%> 

<%
	long cc_id = StringUtil.getLong(request,"cc_id");
	
	PreCalcuConfigChangeBean preCalcuConfigChangeBean = configChangeMgrZJ.preCalcuConfigChange(cc_id);
	
	DBRow[] result = preCalcuConfigChangeBean.getResult();
	
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	ConfigChangeStoreStatusKey configChangeStoreStatusKey = new ConfigChangeStoreStatusKey();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>ConfigChangeAllocate</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<!-- dialog -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive();">
<form name="add_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/config_change/allocateStoreConfigChange.action">
<input type="hidden" name="cc_id" value='<%=cc_id%>'>
	
	<div class="demo" align="center">
	  	 <div id="tabs" style="width: 98%">
	  	    <ul>
			  <li><a href="#receive_allocate">保留库存</a></li>
			</ul>
		<div id="receive_allocate">
			<table width="98%" border="0" isBottom="true" cellpadding="0" cellspacing="0" class="zebraTable">
				<tr>
					<td>组装类型</td>
					<td>组装数量</td>
					<td>内装类型</td>
					<td>内装数量</td>
					<td>库存状态</td>
				</tr>
				<%
					for(int i = 0; i<result.length; i++)
					{
				%>
				<tr>
					<td><%=containerTypeKey.getContainerTypeKeyValue(result[i].get("cci_type",0))+result[i].get("cci_type_id",0l)%></td>
					<td><%=result[i].get("cci_count",0)%></td>
					<td>
						<%
							String inner = "";
							if(result[i].get("inner_type",0)==ContainerTypeKey.Original)
							{
								inner = containerTypeKey.getContainerTypeKeyValue(result[i].get("inner_type",0));
							}
							else
							{
								inner = containerTypeKey.getContainerTypeKeyValue(result[i].get("inner_type",0))+result[i].get("inner_type_id",0l);
							}
							out.print(inner);
						%>
						</td>
					<td><%=result[i].get("inner_need_count",0)%></td>
					<td><%=configChangeStoreStatusKey.getConfigChangeStoreStatusKeyById(result[i].get("item_status",0))%></td>
				</tr>
				<%
					}
				%>
			</table>
			<br>
			<%
				if(preCalcuConfigChangeBean.getStoreStatus()==ConfigChangeStoreStatusKey.Enough)
				{
			%>
				<div align="right">
					<input type="submit" style="font-weight: bold" class="normal-green" value="保留"/>
				</div>
			<%		
				}
			%>
			
		</div>
	</div>
</div>
      </form>
      <script type="text/javascript">
		
		var tabsIndex = 0;//当前tabs的Index
		var tab = $("#tabs").tabs({
		cache: false,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		load: function(event, ui) {onLoadInitZebraTable();}	,
		selected:0,
		show:function(event,ui)
			 {
			 
			 }
		});
		
		function next()
		{
			var index = tabsIndex+1;
			tabSelect(index);
		}
		
		function back()
		{
			var index = tabsIndex-1;
			tabSelect(index);
		}
		
		function tabSelect(index)
		{
			$("#tabs").tabs("select",index);
		}
	</script>
</body>
</html>
