<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.B2BOrderLogTypeKey"%>
<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@page import="java.util.List"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.B2BOrderQualityInspectionKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.B2BOrderClearanceKey"%>
<%@page import="com.cwc.app.key.B2BOrderDeclarationKey"%>
<%@page import="com.cwc.app.key.B2BOrderProductFileKey"%>
<%@page import="com.cwc.app.key.B2BOrderTagKey"%>
<%@page import="com.cwc.app.key.B2BOrderStockInSetKey"%>
<%@page import="com.cwc.app.key.B2BOrderCertificateKey"%>
<%@page import="com.cwc.app.key.B2BOrderQualityInspectionKey"%>
<jsp:useBean id="b2BOrderClearanceKey" class="com.cwc.app.key.B2BOrderClearanceKey"/>
<jsp:useBean id="b2BOrderDeclarationKey" class="com.cwc.app.key.B2BOrderDeclarationKey"/>
<jsp:useBean id="b2BOrderWayKey" class="com.cwc.app.key.B2BOrderWayKey"/>
<jsp:useBean id="b2BOrderTagKey" class="com.cwc.app.key.B2BOrderTagKey"/> 
<jsp:useBean id="b2BOrderCertificateKey" class="com.cwc.app.key.B2BOrderCertificateKey"/> 
<jsp:useBean id="b2BOrderStockInSetKey" class="com.cwc.app.key.B2BOrderStockInSetKey"/> 
<jsp:useBean id="b2BOrderProductFileKey" class="com.cwc.app.key.B2BOrderProductFileKey"/> 
<jsp:useBean id="b2BOrderQualityInspectionKeyKey" class="com.cwc.app.key.B2BOrderQualityInspectionKey"/> 
<%
	DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
	DBRow countrycode[] = orderMgr.getAllCountryCode();
									
	String selectBg="#ffffff";
	String preLetter="";
	
	String b2b_oid_last = b2BOrderMgrZyj.getB2BOrderLastTimeCreateByAdid(request);
	
	String adminUserIdsB2BOrder		= "";
	String adminUserNamesB2BOrder		= "";
	String adminUserIdsDeclaration		= "";
	String adminUserNamesDeclaration	= "";
	String adminUserIdsClearance		= "";
	String adminUserNamesClearance		= "";
	String adminUserIdsProductFile		= "";
	String adminUserNamesProductFile	= "";
	String adminUserIdsTag				= "";
	String adminUserNamesTag			= "";
	String adminUserIdsStockInSet		= "";	
	String adminUserNamesStockInSet		= "";
	String adminUserIdsCertificate		= "";
	String adminUserNamesCertificate	= "";
	String adminUserIdsQualityInspection	= ""; 
	String adminUserNamesQualityInspection	= "";
	String adminUserIdsTagThird			= "";
	String adminUserNamesTagThird		= "";

	if(!"".equals(b2b_oid_last))
	{
		//查询各流程任务的负责人
		adminUserIdsB2BOrder		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Goods);
		adminUserNamesB2BOrder		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Goods);
		adminUserIdsDeclaration		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ExportCustoms);
		adminUserNamesDeclaration	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ExportCustoms);
		adminUserIdsClearance		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ImportCustoms);
		adminUserNamesClearance		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ImportCustoms);
		adminUserIdsProductFile		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ProductShot);
		adminUserNamesProductFile	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ProductShot);
		adminUserIdsTag				= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Label);
		adminUserNamesTag			= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Label);
		adminUserIdsStockInSet		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Freight);
		adminUserNamesStockInSet	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Freight);
		adminUserIdsCertificate		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Document);
		adminUserNamesCertificate	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Document);
		adminUserIdsQualityInspection	= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.QualityControl);
		adminUserNamesQualityInspection	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.QualityControl);
		adminUserIdsTagThird		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.THIRD_TAG);
		adminUserNamesTagThird		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(b2b_oid_last, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.THIRD_TAG);
	}		
	
	DBRow[] titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true, 0L, "", null, request);  //客户登录列出自己的title
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增订单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<!-- dialog -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
	jQuery(function($){
		$('#b2b_order_out_date,#b2b_order_receive_date').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
		});
		$("#ui-datepicker-div").css("display","none");
	});
	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	function stateDivSend(pro_id, pro_input)
	{
		if(pro_id==-1)
		{
			$("#state_div_send").css("display","inline");
			$("#address_state_send").val(pro_input);
		}
		else
		{
			$("#state_div_send").css("display","none");
			$("#address_state_send").val("");
		}
	}
	function stateDivDeliver(pro_id, pro_input)
	{
		if(pro_id==-1)
		{
			$("#state_div_deliver").css("display","inline");
			$("#address_state_deliver").val(pro_input);
		}
		else
		{
			$("#state_div_deliver").css("display","none");
			$("#address_state_deliver").val("");
		}
	}
	//国家地区切换
function getStorageProvinceByCcid(ccid,pro_id,id, pro_input)
{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#"+id).clearAll();
					$("#"+id).addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#"+id).addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#"+id).addOption("手工输入","-1");
					}
				});
				if (pro_id!=""&&pro_id!=0)
				{
					$("#"+id).setSelectedValue(pro_id);
					if(id=="deliver_pro_id")
					{
						stateDivDeliver(pro_id, pro_input);
					}
					else
					{
						stateDivSend(pro_id, pro_input);
					}
				}
}

	function selectDeliveryStorage(obj)
	{
		var ps_id = obj.value;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{ps_id:ps_id},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data){
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					//$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.deliver_city);
					$("#deliver_ccid").val(data["deliver_nation"]);

					getStorageProvinceByCcid($("#deliver_ccid").val(),data["deliver_pro_id"],"deliver_pro_id", data.deliver_pro_input);
					
					$("#deliver_name").val(data["deliver_contact"]);
					$("#deliver_linkman_phone").val(data["deliver_phone"]);
				}
			});
			/*var stroage_type = $("option:selected", obj).attr("stroageType");
			if(1==stroage_type)
			{
				$("#transportArrrivalDateTr").css("display","");
			}
			else
			{
				$("#transportArrrivalDateTr").css("display","none");
			}*/
	}
	
	function selectSendStorage(obj)
	{
		var ps_id = obj.value;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					ps_id:ps_id
				},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data)
				{
					$("#send_house_number").val(data["send_house_number"]);
					$("#send_street").val(data["send_street"]);
					//$("#send_address3").val(data.deliver_address3);
					$("#send_zip_code").val(data.send_zip_code);
					$("#send_name").val(data.send_contact);
					$("#send_linkman_phone").val(data.send_phone);
					$("#send_city").val(data.send_city);
					$("#send_ccid").val(data["send_nation"]);

					getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id",data.send_pro_input);
				}
			});
		/*var stroage_type = $("option:selected", obj).attr("stroageType");
		if(1==stroage_type)
		{
			$("#transportOutDateTr").css("display","");
		}
		else
		{
			$("#transportOutDateTr").css("display","none");
		}*/
	}
	
	//检查提货地址
	function checkSend()
	{
		if($("#psid").val()==0)
		{
			alert("请选择提货仓库");
			return false;
		}
		else if($("#send_ccid").val()==0)
		{
			alert("请选择提货国家");
			return false;
		}
		else if($("#send_pro_id").val()==0)
		{
			alert("请选择提货省份");
			return false;
		}
		else if($("#send_pro_id").val()==-1 && ''==$("#address_state_send").val())
		{
			alert("请填写提货省份");
			$("#address_state_send").focus();
			return false;
		}
		else if($("#send_city").val()=="")
		{
			alert("请填写提货城市");
			$("#send_city").focus();
			return false;
		}
		else if($("#send_house_number").val()=="")
		{
			alert("请填写提货地址门牌号");
			$("#send_house_number").focus();
			return false;
		}
		else if($("#send_street").val()=="")
		{
			alert("请填写提货地址街道");
			$("#send_street").focus();
			return false;
		}
		else if($("#send_zip_code").val()=="")
		{
			alert("请填写提货地邮编");
			$("#send_zip_code").focus();
			return false;
		}
		else if($("#send_name").val()=="")
		{
			alert("请填写提货联系人");
			$("#send_name").focus();
			return false;
		}
	}
	
	//检查交货地址
	function checkReceive()
	{
		if($("#receive_psid").val()==0)
		{
			alert("请选择收货仓库");
			return false;
		}
		else if($("#deliver_ccid").val()==0)
		{
			alert("请选择收货国家");
			return false;
		}
		else if($("#deliver_pro_id").val()==0)
		{
			alert("请选择收货省份");
			return false;
		}
		else if($("#deliver_pro_id").val()==-1 && ''==$("#address_state_deliver").val())
		{
			alert("请填写收货省份");
			$("#address_state_deliver").focus();
			return false;
		}
		else if($("#deliver_city").val()=="")
		{
			alert("请填写收货城市");
			$("#deliver_city").focus();
			return false;
		}
		else if($("#deliver_house_number").val()=="")
		{
			alert("请填写收货地址门牌号");
			$("#deliver_house_number").focus();
			return false;
		}
		else if($("#deliver_street").val()=="")
		{
			alert("请填写收货地址街道");
			$("#deliver_street").focus();
			return false;
		}
		else if($("#deliver_zip_code").val()=="")
		{
			alert("交货地址邮编");
			$("#deliver_zip_code").focus();
			return false;
		}
		else if($("#deliver_name").val()=="")
		{
			alert("请填写交货联系人");
			$("#deliver_name").focus();
			return false;
		}
	}
	
	//检查流程
	function checkProcedures()
	{
		document.add_form.declaration.value = $("input:radio[name=radio_declaration]:checked").val();
		document.add_form.clearance.value = $("input:radio[name=radio_clearance]:checked").val();
		document.add_form.tag.value = $("input:radio[name=radio_tag]:checked").val();
		document.add_form.tag_third.value = $("input:radio[name=radio_tagThird]:checked").val();
		document.add_form.stock_in_set.value = $("input:radio[name=stock_in_set_radio]:checked").val();
		document.add_form.certificate.value = $("input:radio[name=radio_certificate]:checked").val();
		document.add_form.quality_inspection.value = $("input:radio[name=radio_qualityInspection]:checked").val();

		if($("input:checkbox[name=isMailB2BOrder]").attr("checked")){
			document.add_form.needMailB2BOrder.value = 2;
		}
		if($("input:checkbox[name=isMessageB2BOrder]").attr("checked")){
			document.add_form.needMessageB2BOrder.value = 2;
		}
		if($("input:checkbox[name=isPageB2BOrder]").attr("checked")){
			document.add_form.needPageB2BOrder.value = 2;
		}
		
		if($("input:checkbox[name=isMailDeclaration]").attr("checked")){
			document.add_form.needMailDeclaration.value = 2;
		}
		if($("input:checkbox[name=isMessageDeclaration]").attr("checked")){
			document.add_form.needMessageDeclaration.value = 2;
		}
		if($("input:checkbox[name=isPageDeclaration]").attr("checked")){
			document.add_form.needPageDeclaration.value = 2;
		}

		if($("input:checkbox[name=isMailClearance]").attr("checked")){
			document.add_form.needMailClearance.value = 2;
		}
		if($("input:checkbox[name=isMessageClearance]").attr("checked")){
			document.add_form.needMessageClearance.value = 2;
		}
		if($("input:checkbox[name=isPageClearance]").attr("checked")){
			document.add_form.needPageClearance.value = 2;
		}

		if($("input:checkbox[name=isMailProductFile]").attr("checked")){
			document.add_form.needMailProductFile.value = 2;
		}
		if($("input:checkbox[name=isMessageProductFile]").attr("checked")){
			document.add_form.needMessageProductFile.value = 2;
		}
		if($("input:checkbox[name=isPageProductFile]").attr("checked")){
			document.add_form.needPageProductFile.value = 2;
		}

		if($("input:checkbox[name=isMailTag]").attr("checked")){
			document.add_form.needMailTag.value = 2;
		}
		if($("input:checkbox[name=isMessageTag]").attr("checked")){
			document.add_form.needMessageTag.value = 2;
		}
		if($("input:checkbox[name=isPageTag]").attr("checked")){
			document.add_form.needPageTag.value = 2;
		}

		if($("input:checkbox[name=isMailTagThird]").attr("checked")){
			document.add_form.needMailTagThird.value = 2;
		}
		if($("input:checkbox[name=isMessageTagThird]").attr("checked")){
			document.add_form.needMessageTagThird.value = 2;
		}
		if($("input:checkbox[name=isPageTagThird]").attr("checked")){
			document.add_form.needPageTagThird.value = 2;
		}

		if($("input:checkbox[name=isMailStockInSet]").attr("checked")){
			document.add_form.needMailStockInSet.value = 2;
		}
		if($("input:checkbox[name=isMessageStockInSet]").attr("checked")){
			document.add_form.needMessageStockInSet.value = 2;
		}
		if($("input:checkbox[name=isPageStockInSet]").attr("checked")){
			document.add_form.needPageStockInSet.value = 2;
		}

		if($("input:checkbox[name=isMailCertificate]").attr("checked")){
			document.add_form.needMailCertificate.value = 2;
		}
		if($("input:checkbox[name=isMessageCertificate]").attr("checked")){
			document.add_form.needMessageCertificate.value = 2;
		}
		if($("input:checkbox[name=isPageCertificate]").attr("checked")){
			document.add_form.needPageCertificate.value = 2;
		}

		if($("input:checkbox[name=isMailQualityInspection]").attr("checked")){
			document.add_form.needMailQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isMessageQualityInspection]").attr("checked")){
			document.add_form.needMessageQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isPageQualityInspection]").attr("checked")){
			document.add_form.needPageQualityInspection.value = 2;
		}
		if($("#adminUserNamesB2BOrder").val() == '')
		{
			alert("运输负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_declaration]:checked") && $("input:radio[name=radio_declaration]:checked").val() && '<%=B2BOrderDeclarationKey.DELARATION %>' == $("input:radio[name=radio_declaration]:checked").val() && $("#adminUserNamesDeclaration").val() == '')
		{
			alert("出口报关负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_clearance]:checked") && $("input:radio[name=radio_clearance]:checked").val() && '<%=B2BOrderClearanceKey.CLEARANCE%>' == $("input:radio[name=radio_clearance]:checked").val() && $("#adminUserNamesClearance").val() == '')
		{
			alert("进口清关负责人不能为空");
			return false;
		}
		else if($("#adminUserNamesProductFile").val() == '')
		{
			alert("实物图片负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_tag]:checked") && $("input:radio[name=radio_tag]:checked").val() && '<%=B2BOrderTagKey.TAG %>' == $("input:radio[name=radio_tag]:checked").val() && $("#adminUserNamesTag").val() == '')
		{
			alert("内部标签负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_tagThird]:checked") && $("input:radio[name=radio_tagThird]:checked").val() && '<%=B2BOrderTagKey.TAG %>' == $("input:radio[name=radio_tagThird]:checked").val() && $("#adminUserNamesTagThird").val() == '')
		{
			alert("第三方标签负责人不能为空");
			return false;
		}
		else if($("input:radio[name=stock_in_set_radio]:checked") && $("input:radio[name=stock_in_set_radio]:checked").val() && '<%=B2BOrderStockInSetKey.SHIPPINGFEE_SET %>' == $("input:radio[name=stock_in_set_radio]:checked").val() && $("#adminUserNamesStockInSet").val() == '')
		{
			alert("运费流程负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_certificate]:checked") && $("input:radio[name=radio_certificate]:checked").val() && '<%=B2BOrderCertificateKey.CERTIFICATE %>' == $("input:radio[name=radio_certificate]:checked").val() && $("#adminUserNamesCertificate").val() == '')
		{
			alert("单证流程负责人不能为空");
			return false;
		}
		else if($("input:radio[name=radio_qualityInspection]:checked") && $("input:radio[name=radio_qualityInspection]:checked").val() && '<%=B2BOrderQualityInspectionKey.NEED_QUALITY %>' == $("input:radio[name=radio_qualityInspection]:checked").val() && $("#adminUserNamesQualityInspection").val() == '')
		{
			alert("质检流程负责人不能为空");
			return false;
		}
	}
	
	$(document).ready(function()
	{
		$("input:radio[name^=radio_]").click(
				function(){
					var hasThisCheck = $(this).val();
					var hasThisName = $(this).attr("name");
					var presonTrName = hasThisName.split("_")[1]+"PersonTr";
					if(2 == hasThisCheck){
						$("#"+presonTrName).attr("style","");
					}else{
						$("#"+presonTrName).attr("style","display:none");
					}
				}
		);
		$("input:radio[name=stock_in_set_radio]").click(
				function(){
					var hasThisCheck = $(this).val();
					if(2 == hasThisCheck){
						$("#stockInSetPersonTr").attr("style","");
					}else{
						$("#stockInSetPersonTr").attr("style","display:none");
					}
				}
		);		
	});

	function adminUserB2BOrder(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsB2BOrder").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowB2BOrder'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowB2BOrder(user_ids , user_names){
		$("#adminUserIdsB2BOrder").val(user_ids);
		$("#adminUserNamesB2BOrder").val(user_names);
	}
	function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);};
	function adminUserDeclaration(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsDeclaration").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowDeclaration'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowDeclaration(user_ids , user_names){
		$("#adminUserIdsDeclaration").val(user_ids);
		$("#adminUserNamesDeclaration").val(user_names);
	}

	function adminUserClearance(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsClearance").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowClearance'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowClearance(user_ids , user_names){
		$("#adminUserIdsClearance").val(user_ids);
		$("#adminUserNamesClearance").val(user_names);
	}

	function adminUserProductFile(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsProductFile").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowProductFile'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowProductFile(user_ids , user_names){
		$("#adminUserIdsProductFile").val(user_ids);
		$("#adminUserNamesProductFile").val(user_names);
	}

	function adminUserTag(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsTag").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTag'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTag(user_ids , user_names){
		$("#adminUserIdsTag").val(user_ids);
		$("#adminUserNamesTag").val(user_names);
	}
	function adminUserTagThird(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsTagThird").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowTagThird'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowTagThird(user_ids , user_names){
		$("#adminUserIdsTagThird").val(user_ids);
		$("#adminUserNamesTagThird").val(user_names);
	}
	function adminUserStockInSet(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsStockInSet").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowStockInSet'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowStockInSet(user_ids , user_names){
		$("#adminUserIdsStockInSet").val(user_ids);
		$("#adminUserNamesStockInSet").val(user_names);
	}

	function adminUserCertificate(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsCertificate").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowCertificate'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowCertificate(user_ids , user_names){
		$("#adminUserIdsCertificate").val(user_ids);
		$("#adminUserNamesCertificate").val(user_names);
	}

	function adminUserQualityInspection(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 	 var option = {
	 			 single_check:0, 					// 1表示的 单选
	 			 user_ids:$("#adminUserIdsQualityInspection").val(), //需要回显的UserId
	 			 not_check_user:"",					//某些人不 会被选中的
	 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
	 			 ps_id:'0',						//所属仓库
	 			 handle_method:'setParentUserShowQualityInspection'
	 	 };
	 	 uri  = uri+"?"+jQuery.param(option);
	 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	};
	function setParentUserShowQualityInspection(user_ids , user_names){
		$("#adminUserIdsQualityInspection").val(user_ids);
		$("#adminUserNamesQualityInspection").val(user_names);
	};
	
	function openFreightSelect() 
	{
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_resources/freight_resources_select.html";
		freightSelectWindow = $.artDialog.open(url, {title: '运输资源选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function setFreight(fr_id,waybill_name,carriers,b2b_orderby,b2b_orderby_name, begin_country, begin_country_name, end_country, end_country_name, begin_port, end_port) 
	{
		$("#fr_id").val(fr_id);
		$("#b2b_order_waybill_name").val(waybill_name);
		$("#carriers").val(carriers);	
		$("#b2b_orderby").val(b2b_orderby);
		$("#b2b_orderby_name").val(b2b_orderby_name);
		$("#b2b_order_send_country").val(begin_country);
		$("#b2b_order_send_country_name").val(begin_country_name);
		$("#b2b_order_receive_country").val(end_country);
		$("#b2b_order_receive_country_name").val(end_country_name);
		$("#b2b_order_send_place").val(begin_port);
		$("#b2b_order_receive_place").val(end_port);
	}
	
	function setFreightCost(fc_id,fc_project_name,fc_way,fc_unit) {
		addFreigthCost(fc_id,fc_project_name,fc_way,fc_unit);
		$('#changed').val('2');
	}
	
	function setRate(obj) {
		if(obj.value == 'RMB') {
			document.getElementsByName("tfc_exchange_rate")[$(obj).attr('index')].value = "1.0";
		}else {
			document.getElementsByName("tfc_exchange_rate")[$(obj).attr('index')].value = "0.0";
		}
	}
	
	function addFreigthCost(fc_id,fc_project_name,fc_way,fc_unit) 
	{
		var index = document.getElementById("freight_cost").rows.length;
    	var row = document.getElementById("freight_cost").insertRow(index);
    	row.insertCell(0).innerHTML = "<input type='hidden' name='tfc_id' id='tfc_id'>"
							    	 +"<input type='hidden' name='tfc_project_name' id='tfc_project_name' value='"+fc_project_name+"'>"
							    	 +"<input type='hidden' name='tfc_way' id='tfc_way' value='"+fc_way+"'>"
							    	 +"<input type='hidden' name='tfc_unit' id='tfc_unit' value='"+fc_unit+"'>"
							    	 +fc_project_name;
		
		row.insertCell(1).innerHTML = fc_way;
		row.insertCell(2).innerHTML = fc_unit;
		row.insertCell(3).innerHTML = "<input type='text' size='8' name='tfc_unit_price' id='tfc_unit_price' value='0.0'>";
		row.insertCell(4).innerHTML = "<select id='tfc_currency' index='"+(index-2)+"' name='tfc_currency' onchange='setRate(this)'>"+createCurrency(fc_unit)+"</select>";
		row.insertCell(5).innerHTML = "<input type='text' size='8' name='tfc_exchange_rate' id='tfc_exchange_rate' value='1.0'>";
		row.insertCell(6).innerHTML = "<input type='text' size='8' name='tfc_unit_count' id='tfc_unit_count' value='0.0'>";
		row.insertCell(7).innerHTML = "<input type='button' value='删除' onclick='deleteFreigthCost(this)'/>";
		//让回来的币种都默认选中
	}
	function createCurrency(fc_unit){
		var array = ['RMB','USD','HKD'];
		var options = "" ;
	 	for(var index = 0 , count = array.length  ; index < count ; index++ ){
			var selected = "" ;
		 	if(fc_unit.toUpperCase() === array[index]){
		 	   selected = "selected";
			}
			options += "<option "+selected+" value='"+array[index]+"'>"+array[index]+"</option>";
		}
		return  options;
    }

    function deleteFreigthCost(input){  
        var s=input.parentNode.parentNode.rowIndex;
        document.getElementById("freight_cost").deleteRow(s); 
        $('#changed').val('2');
        //var num=document.getElementById("tables").rows.length;  
	}

    function selectFreigthCost() {
    	if($("input:radio[name=stock_in_set_radio]:checked").val()==2)
		{
    		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_cost/freight_cost_select.html";
    		$.artDialog.open(url, {title: '运费项目选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
		}
		else
		{
			alert("请确认在流程指派中是否选择了需要运费");
		} 
	}
	
	function uploadFile(_target)
	{
	    var obj  = {
		     reg:"xls",
		     limitSize:2,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		
		$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
				 //close:function()
				 //{
						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 //	 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
				 //}
			 });
	}
	
	function prefillDetailByFile(file_name)
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminsitrator/config_change/ajaxLoadUpLoadExcel.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:{file_name:file_name},
			
			beforeSend:function(request){
			},
			
			error: function(){
			},
			
			success: function(data)
			{
				var html = "商品名<br/>";
				for(i = 0;i<data.length;i++)
				{
					html +=data[i].p_name+"<br/>";
				}
				$("#show").html(html);
				$("#submitButton").css("display","");
			}
		});
	}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target)
{
	if('' != fileNames)
	{
    	prefillDetailByFile(fileNames);
    	$("#file_name").val(fileNames);
	}
}
function changeSendStorageType(obj)
{
	var typeId = obj.value;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/GetStorageCatalogsByTypeAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:{typeId:typeId},
		beforeSend:function(request){
		},
		error: function(){
		},
		
		success: function(data)
		{
			$("#psid").clearAll();
			$("#psid").addOption("请选择","0");
			if (data!="")
			{
				$.each(data,function(i){
					$("#psid").addOption(data[i].title,data[i].id);
				});
			}
			$("#send_house_number").val("");
			$("#send_street").val("");
			$("#send_zip_code").val("");
			$("#send_name").val("");
			$("#send_linkman_phone").val("");
			$("#send_city").val("");
			$("#send_ccid").val("");
			$("#send_pro_id").val("");
			$("#state_div_send").css("display","none");
			$("#address_state_send").val("");
			//getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id");
		}
	});
	if('<%=StorageTypeKey.SELF%>'*1==typeId*1)
	{
		$("#transportOutDateTr").css("display","");
	}
	else
	{
		$("#transportOutDateTr").css("display","none");
		$("#b2b_order_out_date").val("");
	}
	
}
function changeReceiveStorageType(obj)
{
	var typeId = obj.value;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/GetStorageCatalogsByTypeAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:{typeId:typeId},
		beforeSend:function(request){
		},
		error: function(){
		},
		
		success: function(data)
		{
			$("#receive_psid").clearAll();
			$("#receive_psid").addOption("请选择","0");
			if (data!="")
			{
				$.each(data,function(i){
					$("#receive_psid").addOption(data[i].title,data[i].id);
				});
			}
			$("#deliver_house_number").val("");
			$("#deliver_street").val("");
			$("#deliver_zip_code").val("");
			$("#deliver_name").val("");
			$("#deliver_linkman_phone").val("");
			$("#deliver_city").val("");
			$("#deliver_ccid").val("");
			$("#deliver_pro_id").val("");
			$("#state_div_deliver").css("display","none");
			$("#address_state_deliver").val("");
		}
	});
	
	if('<%=StorageTypeKey.SELF%>'*1==typeId*1)
	{
		$("#transportArrrivalDateTr").css("display","");
	}
	else
	{
		$("#transportArrrivalDateTr").css("display","none");
		$("#b2b_order_receive_date").val("");
	}
	
}
function changeProIdDeliver(obj)
{
	stateDivDeliver(obj.value, "");
}
function changeProIdSend(obj)
{
	stateDivSend(obj.value, "");
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive();">
<form name="add_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/config_change/addConfigChange.action">
<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/config_change/config_change_detail.html"/>
	<div class="demo" align="center">
	  	 <div id="tabs" style="width: 98%">
	  	    <ul>
	  	      <li><a href="#storage" >执行仓库</a></li>
			  <li><a href="#details">货物列表</a></li>
			</ul>
			<div id="storage">
			<table width="95%" align="center" cellpadding="2" cellspacing="5">
						 	<tr>
						 		<td align="right">提货仓库</td>
						 		<td>
						 				<select onchange="changeSendStorageType(this)">
						 					<option value="-1">请选择</option>
								      		<%
								      			StorageTypeKey storageTypeKey = new StorageTypeKey();
								      			List<String> storageTypes = storageTypeKey.getStorageTypeKeys();
								      			for(int i = 0; i < storageTypes.size(); i ++)
								      			{
								      		%>
								      			<option value='<%=storageTypes.get(i) %>'><%=storageTypeKey.getStorageTypeKeyName(storageTypes.get(i)) %></option>
								      		<%		
								      			}
								      		%>
								      	</select>
						 				<select id="psid" name="ps_id" onchange="selectSendStorage(this)">
							     			<option value="0">请选择...</option>
		                                <%
		                                if(1==2){
											for ( int i=0; i<treeRows.length; i++ )
											{
										%>
		                                	<option value='<%=treeRows[i].getString("id")%>' stroageType='<%=treeRows[i].get("storage_type",0) %>'><%=treeRows[i].getString("title")%></option>
		                                <%
											}
		                                }
										%>
							     		</select>
							     		
							    </td>
						 	</tr>
						 	<tr>
						 		<td align="right">Title:</td>
						 		<td>
						 			<select name="title_id" id="title_id">
										<option value="-1">请选择</option>
										<%
											for(int i = 0; i < titles.length; i ++)
											{
										%>
											<option value='<%=titles[i].get("title_id", 0L) %>'><%=titles[i].getString("title_name") %></option>
										<%
										}
										%>
									</select>
						 		</td>
						 	</tr>
						 </table>
		</div>
		
		<div id="details">
			参考模板:<span class="STYLE12"><a href="b2b_order_template.xls">下载</a></span>
			<input type="button" class="long-button" onclick="uploadFile('');" value="上传货物列表"/>
			<div id="show"></div>
			<input type="hidden" id="import_submit" value="true"/>
			<input type="hidden" id="file_name" name="file_name"/>
		</div>
	</div>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top:10px">
		<tr>
			<td align="right">
				<input id="back_button" type="button" style="font-weight: bold" class="normal-green" onClick="back()" value="上一步"/>
				&nbsp;&nbsp;&nbsp;
				<input id="next_button" type="button" style="font-weight: bold" class="normal-green" onClick="next()" value="下一步"/>
				&nbsp;&nbsp;&nbsp;
				<input type="button" style="font-weight: bold" class="normal-green" onClick="submitCheck()" value="提交"/>
			</td>
		</tr>
	</table>					
	</div>
</form>
      <script type="text/javascript">
		
		var tabsIndex = 0;//当前tabs的Index
		var tab = $("#tabs").tabs({
			cache: false,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			load: function(event, ui) {onLoadInitZebraTable();}	,
			selected:0
		});
		
		function countCheck(obj)
		{
			var count = obj.value;
			var countFloat = parseFloat(count);
			
			if(countFloat<0||countFloat!=count)
			{
				alert("数量填写错误");
				$(obj).css("backgroundColor","red");
				return false;
			}
			else
			{
				$(obj).css("backgroundColor","");
			}
		}
		
		function submitCheck()
		{
			
			if($("#psid").val()==0)
			{
			 	alert("请选择执行仓库");
			}
			
			else if(-1 == $("#title_id").val())
			{
				alert("请选择title");
			}
			else
			{
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				document.add_form.submit();
			}
		}
		
		function next()
		{
			var index = tabsIndex+1;
			tabSelect(index);
		}
		
		function back()
		{
			var index = tabsIndex-1;
			tabSelect(index);
		}
		
		function tabSelect(index)
		{
			$("#tabs").tabs("select",index);
		}
	</script>

</body>
</html>
