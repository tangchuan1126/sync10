<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long tid = StringUtil.getLong(request,"tid");
DBRow detail = emailTemplateMgr.getDetailTemplate(tid);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>编辑邮件模板</title>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script>
function modVelocityTemplate()
{
	 if ($("#email_template_editor").val() == "")
	 {
	 	
		alert("模板内容不能为空");
	 }
	 else
	 {
		document.modForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/email/ModVelocityTemplate.action";
		document.modForm.email_template.value = $("#email_template_editor").val();
		document.modForm.submit();
		
	 }
}

function preViewTemplate()
{
	document.preViewForm.email_template.value = $("#email_template_editor").val();
	document.preViewForm.submit();
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="modForm" method="post">
<input type="hidden" name="tid" value="<%=tid%>">
<input type="hidden" name="email_template">
<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/email/email_template_list.html">
</form>

<form name="preViewForm" method="post" action="view.html" target="_blank">
<input type="hidden" name="email_template">
</form>

<br>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="4" >
  <tr>
    <td width="11%" align="right"  ><strong>模板名</strong>&nbsp;&nbsp;&nbsp;</td>
    <td width="89%" align="left" valign="top" ><%=detail.getString("template_name")%></td>
  </tr>
  <tr>
    <td align="right"  ><strong>应用场景</strong>&nbsp;&nbsp;&nbsp;</td>
    <td align="left" valign="top" >
	<%
	  Tree tree = new Tree(ConfigBean.getStringValue("email_scene"));
	  DBRow allFather[] = tree.getAllFather(detail.get("scene_id",0l));
	  
	  int jj=0;
	  for (; jj<allFather.length-1; jj++)
	  {
	 	out.println(allFather[jj].getString("sname")+" » ");
	  }
	  out.println(allFather[jj].getString("sname"));
%>	</td>
  </tr>
  <tr>
    <td align="right"  >&nbsp;</td>
    <td align="left" valign="top" >&nbsp;</td>
  </tr>
  
  <tr>
    <td align="right"  ><strong>邮件内容</strong>&nbsp;&nbsp;&nbsp;</td>
    <td align="left" valign="top" >
	    <textarea  name="email_template_editor" id="email_template_editor" style="width:700px;height:400px;"><%=detail.getString("email_template")%></textarea>			</td>
  </tr>
  <tr>
    <td align="right"  >&nbsp;</td>
    <td align="left" valign="top" >&nbsp; </td>
  </tr>
  <tr>
    <td align="right"  >&nbsp;</td>
    <td align="left" valign="top" >
		 <input type="button" name="Submit2" value="修改" class="normal-green" onClick="modVelocityTemplate();" id="saveButton">
	&nbsp;&nbsp;&nbsp;
	  <input type="button" name="Submit2" value="预览" class="normal-white" onClick="preViewTemplate()">	
	  
	  &nbsp;&nbsp;&nbsp;
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="window.location.href='email_template_list.html'">	</td>
	  </td>
  </tr>
  <tr>
    <td align="right"  >&nbsp;</td>
    <td align="left" valign="top" >&nbsp;</td>
  </tr>
</table>
</body>
</html>
