<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long tid = StringUtil.getLong(request,"tid");
long cid = StringUtil.getLong(request,"cid");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加资产</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<script>
function modScene()
{
	if ($("#scene_id").val() == 0)
	 {
		alert("请选择一个应用场景！");
	 }
     else
	 { 
		parent.document.mod_scene_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/email/modScene.action";	
		parent.document.mod_scene_form.template_id.value = <%=tid%>; 
		parent.document.mod_scene_form.scene_id.value = $("#scene_id").val(); 
		parent.document.mod_scene_form.submit();	
	 }
}


function onLoadInit()
{

}

</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInit();">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_assets_form" method="post" action="" >

<table width="98%" border="0" cellspacing="5" cellpadding="2">

  <tr>
    <td align="left" valign="middle" class="text-line"><table width="100%" border="0" cellpadding="5" cellspacing="0" >
      <tr>
        <td width="96%" style="color:#666666;font-size:20px;font-weight:bold;font-family:Arial, 宋体">
  <%
				 	  Tree tree = new Tree(ConfigBean.getStringValue("email_scene"));
	  DBRow allFather[] = tree.getAllFather(cid);
	  int jj=0;
	  for (; jj<allFather.length-1; jj++)
	  {
	 	out.println(allFather[jj].getString("sname")+" » ");
	  }
		out.println(allFather[jj].getString("sname"));
	  %>			</td>
      </tr>
      <tr>
        <td style="color:#666666;font-size:13px;font-family:宋体">修改为......</td>
      </tr>
    </table></td>
    </tr>
  
  <tr>
  	<td align="left" valign="middle" colspan="3">
    		 	<ul id="scene_menu" class="mcdropdown_menu">
								  <li rel="0">所有场景</li>
								  <%
								  DBRow c1[] = emailSceneMgr.getSceneChildren(0);;
								  for (int i=0; i<c1.length; i++)
								  {
										out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("sname"));
							
										 DBRow c2[] = emailSceneMgr.getSceneChildren(c1[i].get("id",0l));
										  if (c2.length>0)
									     {
										  		out.println("<ul>");	
										  }
										  for (int ii=0; ii<c2.length; ii++)
										  {
												out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("sname"));		
														
														DBRow c3[] = emailSceneMgr.getSceneChildren(c2[ii].get("id",0l));
														  if (c3.length>0)
														  {
																out.println("<ul>");	
														  }
															for (int iii=0; iii<c3.length; iii++)
															{
																	out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("sname"));
																	out.println("</li>");
															}
														  if (c3.length>0)
														  {
																out.println("</ul>");
														  }
														
													
													  
												out.println("</li>");				
										  }
										  if (c2.length>0)
										  {
										  		out.println("</ul>");	
										  }
										  
										out.println("</li>");
								  }
								  %>
							</ul>
							
					  <input type="text" name="scene_id" id="scene_id" value="" />
					  <input type="hidden" name="filter_lid" id="filter_lid" value=""  />
					  
				
				<script>
				$("#scene_id").mcDropdown("#scene_menu",{
						allowParentSelect:true,
						  select: 
						  
								function (id,name)
								{
									$("#filter_lid").val(id);
								}
				
				});
				
				<%
				if (cid > 0)
				{
				%>
				$("#scene_id").mcDropdown("#scene_menu").setValue(<%=cid%>);
				<%
				}
				else
				{
				%>
				$("#scene_id").mcDropdown("#scene_menu").setValue(0);
				<%
				}
				%> 
				
				</script>      </td>
  </tr>
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="修改" class="normal-green" onClick="modScene();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
	</td>
  </tr>
</table>

</body>
</html>
