<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
		
	
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
		<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
		
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script  language="JavaScript1.2">

function addScene(parentId)
{
	$.prompt(
	
	 "<div id='title'>增加子类</div><br />所属分类：<input name='parentname' type='text' id='parentname' style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br>场景名称：<input name='sname' type='text' id='sname' style='width:300px;'><br>",
	
	{
	      submit: checkAddScene,
   		  loaded:
		  
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/email/getDetailEmailSceneActionJson.action",
							{id:parentId},
							function callback(data)
							{
								$("#parentname").val(data.sname);
							}
					);
				}
		  
		  ,
		  callback: 
		  		 
				function (v,m,f)
				{
				 if (v=="y")
					{
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/email/addEmailSceneAction.action";
						document.add_form.parentid.value = parentId;
						document.add_form.sname.value = f.sname;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}
 

function checkAddScene(v,m,f)
{

	if (v=="y")
	{
		 if(f.sname == "")
		 {
				alert("请填写新建场景名称");
				return false;
		 }
		 
		 return true;
	}

}
function checkModScene(v,m,f)
{

	if (v=="y")
	{
		 if(f.sname == "")
		 {
				alert("请填写场景名称！");
				return false;
		 }
		 
		 return true;
	}

}



function delScene(sid,len){
	if (confirm("确认要进行删除操作吗？"))
		{
			document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/email/delEmailSceneAction.action";
			document.del_form.sid.value = sid;
			document.del_form.submit();
		}

}

function updateScene(sid){
	$.prompt(
		
		  "<div id='title'>修改分类</div><br />原场景名称：<input name='yname' type='text' id='yname'   style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br />名称修改为：<input name='sname' type='text' id='sname' style='width:300px;'><br> ",
		{
			submit: checkModScene,
			loaded:
		  
				function ()
				{
					 $.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/email/getDetailEmailSceneActionJson.action",
							{id:sid},
							function callback(data)
							{
								$("#yname").val(data.sname);
							}
					);
				}
		  ,
		   
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/email/modEmailSceneAction.action";
						document.mod_form.sid.value = sid;
						document.mod_form.sname.value = f.sname;		
						document.mod_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 邮件场景管理 »   场景类别</td>
  </tr>
</table>
<br>

<form method="post" name="del_form">
	<input type="hidden" name="sid" >
</form>

<form method="post" name="mod_form">
	<input type="hidden" name="sid" >
	<input type="hidden" name="sname" >
</form>

<form method="post" name="add_form">
	<input type="hidden" name="parentid">
	<input type="hidden" name="sname">
</form>
 <script type="text/javascript">
	 	function showDiv(showDivId)
	 	{
			if(document.getElementById("parent_"+showDivId).style.display=="none")
			{
				document.getElementById("parent_"+showDivId).style.display="block";
				document.getElementById("imgShow_"+showDivId).src = "../email/img/minus.gif";
				document.getElementById("folder_"+showDivId).src = "img/folderopen.gif";
			}else{
				document.getElementById("parent_"+showDivId).style.display="none";
				document.getElementById("imgShow_"+showDivId).src = "../email/img/plus.gif";
				document.getElementById("folder_"+showDivId).src = "img/folder.gif";
			}
	 	}
    	
	 </script>
 


<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
		      <tr> 
		        <th width="45%" class="left-title" colspan="2">场景名称</th>
		        <th class="right-title" width="20%" style="vertical-align: center;text-align: center;">编号</th>
		        <th width="30%" class="right-title">&nbsp;</th>
		      </tr>
			
			<tr>
				<td colspan="5">
					<script type="text/javascript">
						d = new dTree('d');
						d.add(0,-1,'场景类别</td><td align="center" valign="middle" width="20%">&nbsp;</td><td align="center" valign="middle" width="8%">&nbsp;</td><td align="center" valign="middle" width="30%"><input name="Submit" type="button" class="long-button-add" onClick="addScene(0)" value=" 增加子类"></td></tr></table>');
						<%
							 DBRow [] categories = emailSceneMgr.getSceneTree();
							for(int i=0;i<categories.length;i++)
							{
								 DBRow [] c1 = emailSceneMgr.getSceneChildren(categories[i].get("id",0l));
								 %> 
								 d.add(<%=categories[i].get("id",0l)%>,0,'<%=categories[i].getString("sname")%></td> <td align="center" valign="middle" width="8%"><%=categories[i].getString("id")%></td><td align="center" valign="middle" width="40%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delScene(<%=categories[i].get("id",0l)%>,<%=c1.length%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateScene(<%=categories[i].get("id",0l)%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addScene(<%=categories[i].get("id",0l)%>)" value=" 增加子类"></td></tr></table>','');
								
								<%if(c1.length != 0)
								{
										for(int j=0;j<c1.length;j++)
										{
									 %>	
									 	d.add(<%=c1[j].getString("id")%>,<%=categories[i].get("id",0l)%>,'<%=c1[j].getString("sname")%></td><td align="center" valign="middle" width="8%"><%=c1[j].getString("id")%></td><td align="center" valign="middle" width="40%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delScene(<%=c1[j].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateScene(<%=c1[j].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>','');
									 <%   
										}
								}
							}
						 %>
				
						document.write(d);
				
					</script>
				</td>
			</tr>
	</table>
</form>

</body>
</html>
