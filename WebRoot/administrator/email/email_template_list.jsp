<%@ page contentType="text/html;charset=UTF-8" import="com.cwc.app.beans.ebay.qll.*"%>
<%@ include file="../../include.jsp"%> 
<%
long acid = StringUtil.getLong(request,"acid");
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
String receiver = StringUtil.getString(request,"receiver");
String business = StringUtil.getString(request,"business");
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);


DBRow rows[];
if (cmd.equals("filter"))
{
	rows = emailTemplateMgr.getTemplateById(acid,receiver,pc);
}
else
{
	rows = emailTemplateMgr.getAllTemplate(pc);
}	

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

		
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript">

$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#search_key").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/email/getSearchTemplateActionJson.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		selectFirst: false,
		updownSelect:false,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},
		
				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].a_name,
    				value:data[i].a_name+"["+data[i].a_barcode+"]",
        			result:data[i].a_name
				};    
			  }   
			
		  	 return rows;   
			 },
		
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	$("#search_key").keydown(function(event){
			if(event.keyCode == 13)
			{
				search();
			}
		})
});



function filter()
{
		 
		var receiver= document.search.receiver.value;
		document.filter_search_form.cmd.value = "filter";		
		document.filter_search_form.acid.value = $("#filter_acid").val();
		document.filter_search_form.receiver.value = receiver;	
		document.filter_search_form.business.value = receiver;
		document.filter_search_form.submit();
}

function search()
{
	if ($("#search_key").val()=="")
	{
		alert("请填写关键字");
		return(false);
	}
	else
	{
		document.filter_search_form.cmd.value = "search";		
		document.filter_search_form.key.value = $("#search_key").val();		
		document.filter_search_form.submit();
	}
}

function modTemplate(template_id,cid)
{
	tb_show('修改模板信息','mod_email_template.html?template_id='+template_id+'&cid='+cid+'&TB_iframe=true&height=300&width=600',false);

}
 
function delTemplate(name,template_id)
{
	if (confirm("确定删除 "+name+"？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/email/delEmailTemplateAction.action";
		document.del_form.template_id.value = template_id;
		document.del_form.submit();	
	}
}

 
 

function addTemplate()
{
	
	var id = $("#filter_acid").val();
	if (id==0)
	{
		alert("请选择一个场景类别");
		openCatalogMenu();
	}
	else
	{	
		tb_show('增加邮件模板','add_email_template.html?category_id='+id+'&TB_iframe=true&height=300&width=600',false);
	}
}

function closeWin()
{
	tb_remove();
}

function openCatalogMenu()
{
	  
	$("#category").mcDropdown("#categorymenu").openMenu();
}

function print(aid)
{
	document.print_form.aid.value = aid; 
	document.print_form.submit();
}

function modScene(tid,cid)
{
	tb_show('修改应用场景','mod_scene.html?tid='+tid+'&cid='+cid+'&TB_iframe=true&height=350&width=700',false);
}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 邮件模板  »    模板清单</td>
  </tr>
</table>
<br>

<form name="mod_scene_form" method="post">
<input type="hidden" name="template_id">
<input type="hidden" name="scene_id">
</form>
 
<form name="del_form" method="post">
<input type="hidden" name="template_id">
</form>

<form name="mod_form" method="post">
<input type="hidden" name="a_id">
<input type="hidden" name="a_name">
<input type="hidden" name="category_id">
<input type="hidden" name="a_barcode">

<input type="hidden" name="unit_name">
<input type="hidden" name="unit_price">
<input type="hidden" name="standard">
<input type="hidden" name="purchase_Date">
<input type="hidden" name="suppliers">
<input type="hidden" name="location">
<input type="hidden" name="requisitioned">
<input type="hidden" name="state">

</form>
<form name="parentForm" method="post" action="" >
   <input name="template_id" type="hidden" id="template_id"    >
   <input name="template_name" type="hidden" id="template_name"    > 

   <input name="smpt_server" type="hidden" id="smpt_server"  />
   <input name="smpt_port" type="hidden" id="smpt_port"  >
   <input name="send_id" type="hidden" id="send_id" >
   <input name="send_pwd" type="hidden" id="send_pwd"  >
   <input name="send_name" type="hidden" id="send_name"  >
   <input name="reply_id" type="hidden" id="reply_id"  >
   <input name="receiver" type="hidden" id="receiver" >
	<input type="hidden" name="scene_id" id="scene_id">
</form>		

<form name="filter_search_form" method="get"  action="email_template_list.html">
<input type="hidden" name="cmd" value="<%=cmd %>">
<input type="hidden" name="acid" value="<%=acid %>" >
<input type="hidden" name="business"  id="business" value="<%=business %>" >
<input type="hidden" name="key" value="<%=key %>">
<input type="hidden" name="receiver" value="<%=receiver %>" >
</form>
<form action="" name="search" method="get">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="70%">
	
	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;padding-left:15px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
	
	<table width="98%" border="0" align="center" cellpadding="5" cellspacing="0">
		<tr>
			<td colspan="2">
					<div style="padding-bottom:5px;color:#666666;font-size:12px;">
					可以选择一个场景类别：
					</div>
			</td>	
		</tr>
  		<tr>
          <td width="423"  align="left" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">
				
							 	<ul id="categorymenu" class="mcdropdown_menu">
								  <li rel="0">所有场景</li>
								  <%
								  DBRow c1[] = emailSceneMgr.getSceneChildren(0);;
								  for (int i=0; i<c1.length; i++)
								  {
										out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("sname"));
							
										 DBRow c2[] = emailSceneMgr.getSceneChildren(c1[i].get("id",0l));
										  if (c2.length>0)
									     {
										  		out.println("<ul>");	
										  }
										  for (int ii=0; ii<c2.length; ii++)
										  {
												out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("sname"));		
														
														DBRow c3[] = emailSceneMgr.getSceneChildren(c2[ii].get("id",0l));
														  if (c3.length>0)
														  {
																out.println("<ul>");	
														  }
															for (int iii=0; iii<c3.length; iii++)
															{
																	out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("sname"));
																	out.println("</li>");
															}
														  if (c3.length>0)
														  {
																out.println("</ul>");
														  }
														
													
													  
												out.println("</li>");				
										  }
										  if (c2.length>0)
										  {
										  		out.println("</ul>");	
										  }
										  
										out.println("</li>");
								  }
								  %>
							</ul>
						  <input type="text" name="category" id="category" value="" />
						  <input type="hidden" name="filter_acid" id="filter_acid" value="0"  />

						<script>
						$("#category").mcDropdown("#categorymenu",{
								allowParentSelect:true,
								  select: 
								  
										function (id,name)
										{
											$("#filter_acid").val(id);
										}
						
						});
						<%
						
						if (acid>0)
						{
						%>
						$("#category").mcDropdown("#categorymenu").setValue(<%=acid%>);
						<%
						}
						else
						{
						%>
						$("#category").mcDropdown("#categorymenu").setValue(0);
						<%
						}
						%> 
						
						</script>	
			  </td>

			   	 <td width="416" ><input name="Submit2" type="button" class="button_long_refresh" value="过滤" onClick="filter()">
		      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 		<input name="Submit" type="button" class="long-button-add" value="增加模板" onClick="addTemplate()">
			  </td>
 		    </tr>
  			<tr>
			   	 <td  width="423"   align="left" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">	 
				 		<select name="receiver" style="border:1px #CCCCCC solid; width:415px;"  >
				          <option value="">全部收款人</option>
				          <%
								String businessl[] = systemConfig.getStringConfigValue("business").split(",");
								for (int i=0; i<businessl.length; i++)
								{
								  %>
									 <option value="<%=businessl[i]%>" <%=(business.equals(businessl[i]))?"selected":""%>> <%=businessl[i]%> </option>
								  <%
						        }
						  %>
				         
				        </select>
       		    </td>
       		    
			    <td height="29" bgcolor="#eeeeee">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 				</td>
 				
  			</tr> 
		</table>
 </div>
	 </td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>	</form>
<br>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	    <tr> 
          	<th width="61" class="right-title" style="vertical-align: center;text-align: center;">ID</th>
	        <th width="242" align="center"  class="left-title" style="vertical-align: center;text-align: left;">模板名</th>
	        <th width="222"  class="right-title" style="vertical-align: center;text-align: center;">应用场景</th>
	        <th width="207"  class="right-title" style="vertical-align: center;text-align: center;">收款账号</th>
	        <th width="501" align="center"  class="right-title" style="vertical-align: center;text-align: center;">邮件发送配置</th>
	        <th width="181"  class="right-title" style="vertical-align: center;text-align: center;">操作</th>
	    </tr>
	
	    <%
	for ( int i=0; i<rows.length; i++ )
	{
	
	%>
	    <tr  > 
	       <td style='word-break:break-all;' align="center">
		  	<%=rows[i].getString("template_id") %>		  </td>
	      <td height="80" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
	
	      <%=rows[i].getString("template_name")%>		 <br>
	      <br>
	      <input name="Submit33" type="button" class="short-short-button-search-email" value="预览" onClick="window.open('view-<%=rows[i].getString("template_id")%>.html')"></td>
	      
	      <td align="center" valign="middle" style="line-height:25px;">
<%
	  Tree tree = new Tree(ConfigBean.getStringValue("email_scene"));
	  DBRow allFather[] = tree.getAllFather(rows[i].get("scene_id",0l));
	  
	  int jj=0;
	  for (; jj<allFather.length-1; jj++)
	  {
	 	  out.println("<a href='?cmd=filter&acid="+allFather[jj].getString("id")+"'>"+allFather[jj].getString("sname")+"</a> »");
	  }
	    out.println("<a href='?cmd=filter&acid="+allFather[jj].getString("id")+"'>"+allFather[jj].getString("sname")+"</a>");
%>
		   &nbsp;&nbsp;&nbsp;   <a href="javascript:modScene(<%=rows[i].getString("template_id")%>,<%=rows[i].getString("scene_id")%>)"><img src="../imgs/modify.gif" width="12" height="12" border="0" align="absmiddle"></a></td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("receiver").equals("")?"无":rows[i].getString("receiver")%> </td>
<td style="vertical-align: center;text-align: center;padding:0px;padding-top:8px;padding-bottom:8px;" ><table width="98%" border="0" cellspacing="5" cellpadding="0">
  <tr>
    <td width="28%" align="right">SMPT服务器</td>
    <td width="72%"><%=rows[i].getString("smpt_server")%></td>
  </tr>
  <tr>
    <td align="right">SMPT服务端口</td>
    <td><%=rows[i].getString("smpt_port")%></td>
  </tr>
  <tr>
    <td align="right">发送邮件账号</td>
    <td> <%=rows[i].getString("send_id")%></td>
  </tr>
  <tr>
    <td align="right">发送邮件密码</td>
    <td><%=rows[i].getString("send_pwd")%></td>
  </tr>
  <tr>
    <td align="right">邮件发送人名称</td>
    <td><%=rows[i].getString("send_name")%></td>
  </tr>
  <tr>
    <td align="right">默认回复邮件账号</td>
    <td><%=rows[i].getString("reply_id")%></td>
  </tr>
</table></td>
	      <td align="center" valign="middle" nowrap="nowrap">

	        <input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="modTemplate(<%=rows[i].getString("template_id")%>,<%=rows[i].get("scene_id",0l)%>)">
	        &nbsp;&nbsp;
	        <input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delTemplate('<%=rows[i].getString("template_name") %>',<%=rows[i].getString("template_id") %>)"><br>

			<br>
	        <input name="Submit3" type="button" class="long-button-mod" value="编辑邮件" onClick="window.location='mod_velocity_template-<%=rows[i].getString("template_id")%>.html'"></td>
	    </tr>
	    <%
	}
	%>
	</table>
</form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="email_template_list.html">
    <input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="acid" value="<%=acid%>">
<input type="hidden" name="key" value="<%=key%>">

  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/printTemplate100_50.html" method="get" target="_blank" name="print_form">
	<input type="hidden" name="aid" id="aid"/>
	<input type="hidden" id="print_range_width" name="print_range_width" value="80"/>
	<input type="hidden" id="print_range_height" name="print_range_height" value="35"/>
	<input type="hidden" id="printer" name="printer" value="LabelPrinter"/>
	<input type="hidden" id="paper" name="paper" value="100X50"/>
</form>
<p>&nbsp;</p>
</body>
</html>
