<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long tid = StringUtil.getLong(request,"tid");
DBRow detail = emailTemplateMgr.getDetailTemplate(tid);
String email_template;
if (detail==null)
{
	email_template = StringUtil.getString(request,"email_template");
}
else
{
	email_template = detail.getString("email_template");
}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>预览邮件模板</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script>

</script>
<style type="text/css">

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<br>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" >
  <tr>
    <td bgcolor="#FFFFFF" ><%=email_template%></td>
  </tr>
</table>
</body>
</html>
