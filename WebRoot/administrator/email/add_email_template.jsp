 <%@ page contentType="text/html;charset=UTF-8"  %>
<%@ include file="../../include.jsp" %> 
<%
long category_id = StringUtil.getLong(request,"category_id");

DBRow sonCatalog[] = emailSceneMgr.getSceneChildren(category_id);
 DBRow catalog = emailSceneMgr.getDetailScene(category_id);
 String template_file_name = StringUtil.getString(request,"template_file_name");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加邮件模板</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script language="javaScript">

function addTemplate()
{
	var f = document.addTemplateForm;
	 
	if (f.template_name.value == "")
	 {
		alert("请填写模板名称！");
	 } 
	  else if (f.smpt_server.value == "")
	 {
		alert("请填写SMPT服务器！");
	 }
	  else if (f.smpt_port.value == "")
	 {
		alert("请填写SMPT服务端口！");
	 }
	  else if (f.send_id.value == "")
	 {
		alert("请填写发送邮件账号！");
	 }
	 else if (f.send_pwd.value == "")
	 {
		alert("请填写发送邮件密码！");
	 }
	   else if (f.send_name.value == "")
	 {
		alert("请填写邮件发送人名称！");
	 } else if (f.reply_id.value == "")
	 {
		alert("请填写回复邮件账号！");
	 }
	  

	   else
	 {
		parent.document.parentForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/email/addEmailTemplateAction.action";	
		parent.document.parentForm.template_name.value = f.template_name.value;
		parent.document.parentForm.smpt_server.value = f.smpt_server.value;
		parent.document.parentForm.smpt_port.value = f.smpt_port.value;
		parent.document.parentForm.send_id.value = f.send_id.value;
		parent.document.parentForm.send_name.value = f.send_name.value;
		parent.document.parentForm.send_pwd.value = f.send_pwd.value;
		parent.document.parentForm.reply_id.value = f.reply_id.value;
		parent.document.parentForm.receiver.value = f.receiver.value;
		parent.document.parentForm.scene_id.value = <%=category_id%>;
		parent.document.parentForm.submit();	
	 }
}

</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="addTemplateForm" method="post" action="" >
  <table width="98%" border="0" cellspacing="9" cellpadding="2">
    
    
    <tr>
      <td colspan="4" style="border-bottom:1px #cccccc solid">
	  <table width="100%" height="57" border="0" cellpadding="5" cellspacing="0" >
          <tr>
            <td width="96%" style="color:#666666;font-size:20px;font-weight:bold;font-family:Arial, 宋体"><%
		 
	  Tree tree = new Tree(ConfigBean.getStringValue("email_scene"));
	  DBRow allFather[] = tree.getAllFather(category_id);
	  int jj=0;
	  for (; jj<allFather.length-1; jj++)
	  {
	 	out.println(allFather[jj].getString("sname")+" » ");
	  }

	  if (catalog!=null)
	  {
	  	out.println(catalog.getString("sname"));
	  }
	  %>
                </td>
          </tr>
          <tr>
            <td colspan="4" style="color:#999999;font-size:12px;border:0px;">场景下增加邮件模板......</td>
          </tr>
      </table>	  </td>
      </tr>
    <tr>
      <td  >&nbsp;</td>
      <td colspan="3"  align="left"  >&nbsp;</td>
    </tr>
    <tr>
      <td  >模板名</td>
      <td colspan="3"  align="left"  ><input name="template_name" type="text" id="template_name"   style="width:480px;"></td>
      </tr>
    <tr>
      <td   >SMPT服务器</td>
      <td  align="left" ><input name="smpt_server" type="text" id="smpt_server"  style="width:180px;" value="smtp.c2.corpease.net" ></td>
      <td  >SMPT服务端口</td>
      <td><input name="smpt_port" type="text" id="smpt_port"  style="width:180px;" value="25"></td>
    </tr>
    <tr>
      <td >发送邮件账号</td>
      <td  align="left"   ><input name="send_id" type="text" id="send_id"  style="width:180px;" value="sales@vvme.com" ></td>
      <td  >发送邮件密码</td>
      <td  ><input name="send_pwd" type="text" id="send_pwd"  style="width:180px;" value="hanfe1980" ></td>
    </tr>
    <tr>
      <td >邮件发送人名称</td>
      <td   align="left"  ><input name="send_name" type="text" id="send_name"  style="width:180px;" value="Visionari LLC" ></td>
      <td  >默认回复账号</td>
      <td   ><input name="reply_id" type="text" id="reply_id"  style="width:180px;" value="service@vvme.com" ></td>
    </tr>
    <tr>
      <td >收款账号(可选)</td>
      <td   align="left"  >
	  <select name="receiver" style="width:180px;"  >
          <option value="">请选择...</option>
          <%
								String businessl[] = systemConfig.getStringConfigValue("business").split(",");
								for (int i=0; i<businessl.length; i++)
								{
								  %>
          <option value="<%=businessl[i]%>"  > <%=businessl[i]%> </option>
          <%
						        }
						  %>
        </select>	  </td>
      <td  >&nbsp;</td>
      <td   >&nbsp;</td>
    </tr>
  </table>
</form></td>
  </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line"><img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#999999">必须把模板增加到最底层分类</span>         </td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="增加" class="normal-green" onClick="addTemplate();">
	 <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
	</td>
  </tr>
</table> 
</body>
</html>
