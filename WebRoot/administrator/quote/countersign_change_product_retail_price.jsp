<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="org.jbpm.api.task.Task"%>
<%@ page import="java.util.List,org.jbpm.api.*,org.jbpm.api.task.Task,org.jbpm.api.history.*,com.cwc.app.api.JbpmMgr,com.cwc.app.jbpm.CountersignActivity"%>
<%
long task_id = StringUtil.getLong(request,"task_id");
long rp_id = jbpmMgr.getTaskLongVaviable(String.valueOf(task_id),"id");
DBRow retailPriceProducts[] = quoteMgr.getRetailPriceItemsByRpId(rp_id,null);
DBRow detailRetailPriceProduct = quoteMgr.getDetailRetailPriceByRpId(rp_id);
String ware_house = catalogMgr.getDetailProductStorageCatalogById(detailRetailPriceProduct.get("ps_id",0l)).getString("title");
String country_area = productMgr.getDetailCountryAreaByCaId(detailRetailPriceProduct.get("ca_id",0l)).getString("name");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script>

function countersign_nopass(task_id)
{
	$.prompt(
	
	"<div id='title'>请填写否决原因</div><br><textarea name='proReason' id='proReason' style='width:250px;height:90px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{

						  if(f.proReason == "")
						  {
							   alert("请填写否决原因");
							   
								return false;
						  }

						  
						  return true;
					}
				}
		  ,
   		  loaded:
				function ()
				{

				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						parent.document.countersign_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/CounterSignChangeRetailPrice.action";
						parent.document.countersign_form.task_id.value = task_id;
						parent.document.countersign_form.reject_reasons.value = f.proReason;
						parent.document.countersign_form.countersign_name.value = "<%=CountersignActivity.COUNTERSIGN_NOPASS%>";
						parent.document.countersign_form.submit();
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function countersign_pass(task_id)
{
	if ( confirm("确认同意？") )
	{
		parent.document.countersign_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/CounterSignChangeRetailPrice.action";
		parent.document.countersign_form.task_id.value = task_id;
		parent.document.countersign_form.countersign_name.value = "<%=CountersignActivity.COUNTERSIGN_PASS%>";
		parent.document.countersign_form.submit();
	}
} 

function countersign_abstain(task_id)
{
	if ( confirm("确认弃权？") )
	{
		parent.document.countersign_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/CounterSignChangeRetailPrice.action";
		parent.document.countersign_form.task_id.value = task_id;
		parent.document.countersign_form.countersign_name.value = "<%=CountersignActivity.COUNTERSIGN_ABSTAIN%>";
		parent.document.countersign_form.submit();
	}
} 
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><br>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="middle" style="font-size:20px;font-weight:bold">调整商品价格审批</td>
      </tr>
      <tr>
        <td><%=ware_house%> -&gt; <%=country_area%>  (<%=detailRetailPriceProduct.getString("reasons")%>) <br>
        <table width="100%" border="0" align="center"  cellpadding="5" cellspacing="0"  class="zebraTable">
  <thead>
  <tr>
    <th width="28%" align="left"  class="right-title"  style="text-align:left;" >商品名称</th>
    <th width="16%" align="left"  class="right-title"  style="text-align:center;" >成本</th>
    <th width="17%" align="left"  class="right-title"  style="text-align:center;" >运费</th>
    <th width="17%" align="left"  class="right-title"  style="text-align:center;" >旧价格</th>
      <th width="22%"  class="left-title" style="text-align: center;">新价格</th>
      </tr>
  </thead>
  <%
for (int i=0; i<retailPriceProducts.length; i++)
{
%>
          <tr>
            <td align="left" valign="middle"  ><%=retailPriceProducts[i].getString("name")%></td>
      <td align="center" valign="middle"  ><%=retailPriceProducts[i].getString("price")%></td>
      <td align="center" valign="middle"  ><%=retailPriceProducts[i].getString("shipping_fee")%></td>
      <td align="center" valign="middle"  ><%=retailPriceProducts[i].getString("old_retail_price")%></td>
      <td align="center" valign="middle" ><%=retailPriceProducts[i].getString("new_retail_price")%></td>
    </tr>
  <%
}
%>
  </table>
		  
		</td></tr>
    </table>
	</td>
  </tr>
  <tr>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="同意" class="normal-green" onClick="countersign_pass(<%=task_id%>);">
	 &nbsp;&nbsp;
<input type="button" name="Submit2" value="否决" class="normal-green" onClick="countersign_nopass(<%=task_id%>);">
&nbsp;&nbsp;
<input type="button" name="Submit2" value="弃权" class="normal-green" onClick="countersign_abstain(<%=task_id%>);">

&nbsp;&nbsp;
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">	</td>
  </tr>
</table> 
</body>

</html>
