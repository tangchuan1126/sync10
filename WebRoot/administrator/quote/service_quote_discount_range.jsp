<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
DBRow discountRange[] = quoteMgr.getServiceQuoteDiscountRange(100006l);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>创建折扣优惠</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script>
function saveModify()
{
	

	var discount_range_str = "";
	var errorNum;
	var errorNumFlag = false;
	$("input[name='discount_range']").each(  
		function()
		{  
			if ( checkNum($(this).val())==false )
			{
				errorNumFlag = true;
				errorNum = $(this).val();
			}
			discount_range_str += "discount_range="+$(this).val()+"&";
		}   
	 ) 
	 
	 if (errorNumFlag)
	 {
	 	alert(errorNum+" 格式错误！\n只能输入两位整数");
	 }
	 else
	 {

	var adid_str = "";
	$("input[name='adid']").each(  
		function()
		{  
			adid_str += "adid="+$(this).val()+"&";
		}   
	 ) 
	 
			var para = discount_range_str+adid_str;
		
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/BatchModServiceQuoteDiscountRange.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(html){

					if (html=="true")
					{
						alert("保存成功！");
						parent.closeTBWin();
					}
					else
					{
						alert("保存失败！");
					}
				}
			});
			
			}

}

//验证两位小数
function checkNum(val)
{
	var reg = /^(\d){1,2}(\.(\d){1,2})*$/;
	return(reg.test(val));
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="padding:20px;"  onLoad="onLoadInitZebraTable();">
<table width="100%" border="0" cellspacing="1" cellpadding="5" class="zebraTable">
<thead>
  <tr>
    <td width="51%" bgcolor="#eeeeee" class="right-title"><strong>客服</strong></td>
    <td width="49%" align="center" bgcolor="#eeeeee" class="right-title" style="text-align: center;"><strong>可下调折扣幅度</strong></td>
  </tr>
  </thead>
<%
for (int i=0; i<discountRange.length; i++)
{

%>
  <tr>
    <td style="border-bottom:1px solid #eeeeee"><%=discountRange[i].getString("account")%></td>
    <td align="center" style="border-bottom:1px solid #eeeeee">
	 <input type="hidden" name="adid" id="adid<%=i%>" value="<%=discountRange[i].get("adid",0l)%>" >
      <input type="text" name="discount_range" id="discount_range<%=i%>" value="<%=MoneyUtil.round(discountRange[i].get("discount_range",0f)*100, 0)%>" style="width:80px;">
    %   </td>
  </tr>
<%
}  
%>
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <input name="Submit" type="button" class="long-button" value="保存修改" onClick="saveModify()">
	  &nbsp;&nbsp;&nbsp;&nbsp;
      <input name="Submit2" type="button" class="short-button" value="取消" onClick="parent.closeTBWin();">
    </td>
  </tr> 
</table>
<br>
</body>
</html>
