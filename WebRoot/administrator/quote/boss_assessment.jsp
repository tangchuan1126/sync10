<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.RetailPriceResultTypeKey" %>
 <%
long task_id = 0l;
long rp_id =  StringUtil.getLong(request,"rp_id");
long schedule_id = StringUtil.getLong(request,"schedule_id");
DBRow schduleRow = scheduleMgrZR.getScheduleById(schedule_id);
long schedule_sub_id = StringUtil.getLong(request,"schedule_sub_id");
String cmd = StringUtil.getString(request,"cmd");
DBRow retailPriceProducts[] = quoteMgr.getRetailPriceItemsByRpId(rp_id,null);
DBRow detailRetailPriceProduct = quoteMgr.getDetailRetailPriceByRpId(rp_id);
String ware_house = catalogMgr.getDetailProductStorageCatalogById(detailRetailPriceProduct.get("ps_id",0l)).getString("title");
String country_area = productMgr.getDetailCountryAreaByCaId(detailRetailPriceProduct.get("ca_id",0l)).getString("name");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
 
<script>
function agree(_create_adid)
{
	 var obj = {
		 	rp_id:'<%= rp_id%>',
		 	schedule_id:'<%= schedule_id%>',
		 	schedule_sub_id:'<%= schedule_sub_id%>',
		 	create_adid:_create_adid
	 	}
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/retail_price/AgreeRetailPriceAction.action'
	commonAjax(uri,obj);
} 
//json 放回结果为success的适用
function commonAjax(uri,dataJson){
    $.ajax({
	    url:uri,
	    data:dataJson,
	    dataType:'json',
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
	    	if(data && data.flag == "success"){
	    		cancel();
	    		$.artDialog.opener.refreshWindow && $.artDialog.opener.refreshWindow();
		   	}else{
		   		showMessage("系统错误.","error");
			}
    	},
    	error:function(){$.unblockUI();showMessage("系统错误.","error");}
	})
}
function notAgree(_create_adid)
{
	$.prompt(
	
	"<div id='title'>请填写否决原因</div><br><textarea name='proReason' id='proReason' style='width:250px;height:90px;'></textarea>",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{

						  if(f.proReason == "")
						  {
							   alert("请填写否决原因");
							   
								return false;
						  }

						  
						  return true;
					}
				}
		  ,
   		  loaded:
				function ()
				{

				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{			
						var obj = {
							rp_id:'<%= rp_id%>',
						 	schedule_id:'<%= schedule_id%>',
						 	schedule_sub_id:'<%= schedule_sub_id%>',
						 	create_adid:_create_adid,
						 	result_text:f.proReason
						}		
						var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/retail_price/NotAgreeRetailPriceAction.action'
						commonAjax(uri,obj);
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" >
  <tr>
    <td align="center" valign="top"><br>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="middle" style="font-size:20px;font-weight:bold">调整商品价格审批</td>
      </tr>
      <tr>
        <td><%=ware_house%> -&gt; <%=country_area%> <br>
        <table width="100%" border="0" align="center"  cellpadding="5" cellspacing="0"   class="zebraTable">
  <thead>
  <tr>
    <th width="28%" align="left"  class="right-title"  style="text-align:left;" >商品名称</th>
    <th width="16%" align="left"  class="right-title"  style="text-align:center;" >成本</th>
    <th width="17%" align="left"  class="right-title"  style="text-align:center;" >运费</th>
    <th width="17%" align="left"  class="right-title"  style="text-align:center;" >旧价格</th>
      <th width="22%"  class="left-title" style="text-align: center;">新价格</th>
      </tr>
  </thead>
  <%
for (int i=0; i<retailPriceProducts.length; i++)
{
%>
          <tr>
            <td align="left" valign="middle"  ><%=retailPriceProducts[i].getString("name")%></td>
      <td align="center" valign="middle"  ><%=retailPriceProducts[i].getString("price")%></td>
      <td align="center" valign="middle"  ><%=retailPriceProducts[i].getString("shipping_fee")%></td>
      <td align="center" valign="middle"  ><%=retailPriceProducts[i].getString("old_retail_price")%></td>
      <td align="center" valign="middle" ><%=retailPriceProducts[i].getString("new_retail_price")%></td>
    </tr>
  <%
}
  if(cmd.equals("show") &&detailRetailPriceProduct.get("result_type",0) == RetailPriceResultTypeKey.NOT_AGREE ){
	  %>
	  	<tr>
	  		<td colspan="5" style="padding-top:10px;">拒绝理由:<%=detailRetailPriceProduct.getString("result_text") %></td>
	  	</tr>
	  <% 
  }
%>
  </table>
		  
		</td></tr>
    </table></td>
  </tr>
   
  <tr>
    <td width="49%" align="right" class="win-bottom-line">
    <%if(cmd.equals("execute")){ %>
	 <input type="button" name="Submit2" value="同意" class="normal-green" onClick="agree('<%=schduleRow.get("assign_user_id",0l) %>');">
		 &nbsp;&nbsp;
	<input type="button" name="Submit2" value="不同意" class="normal-green" onClick="notAgree('<%=schduleRow.get("assign_user_id",0l) %>');">
	&nbsp;&nbsp;
<%} %>
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="cancel();">	</td>
  </tr>
</table> 
<script type="text/javascript">
function cancel(){
	$.artDialog && $.artDialog.close();
}
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
</body>

</html>
