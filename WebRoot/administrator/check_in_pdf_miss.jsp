<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="../../include.jsp" %>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<%@page import="com.cwc.app.key.ModuleKey" %>
<%@page import="com.cwc.app.key.ProcessKey" %>
<%@page import="com.cwc.app.key.OccupyTypeKey" %>
<%@page import="com.cwc.app.key.YesOrNotKey" %>
<jsp:useBean id="moduleKey" class="com.cwc.app.key.ModuleKey"/>

<html>
<head>
    <!--  基本样式和javascript -->
    <link type="text/css" href="../comm.css" rel="stylesheet"/>
    <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
    <script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
    <!-- table 斑马线 -->
    <script src="../js/zebra/zebra.js" type="text/javascript"></script>
    <link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
    <!-- 引入Art -->
    <link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css"/>
    <script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
    <script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
    <script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
    <!-- 遮罩 -->
    <script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
    <!-- 时间 -->
    <script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>
    <script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
    <link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
    <!-- 提示信息 -->
    <script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
    <link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
    <!-- 选项卡 -->
    <script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
    <script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css"/>
    <link type="text/css" href="../js/tabs/demos.css" rel="stylesheet"/>
    <!-- select2 -->
    <link href="../js/select2-3.5.2/select2.css" rel="stylesheet"/>
    <script src="../js/select2-3.5.2/select2.js"></script>

    <!-- 自动匹配 -->
    <script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
    <script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
    <script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
    <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css"/>
    <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css"/>
    <link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css"/>
    <script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

    <%
        String serverName = request.getServerName();
        AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
        boolean isadministrator = adminLoggerBean.isAdministrator();  //判断当前登录帐号是不是admin.如果是返回true
        long psId = StringUtil.getLong(request, "ps");//当前账号所属仓库id
        if (psId == 0l) {
            psId = adminLoggerBean.getPs_id();
        }
        DBRow[] ps = customerPDFDownload.findAllStorage();//查询仓库

        String carrier = StringUtil.getString(request, "carrier");
        String customer = StringUtil.getString(request, "customer");
        String loadno = StringUtil.getString(request, "loadno");
        String start_time = StringUtil.getString(request, "start_time");
        String end_time = StringUtil.getString(request, "end_time");
        String title = StringUtil.getString(request, "title");
        String cmd = StringUtil.getString(request, "cmd");
        String dn = StringUtil.getString(request, "dn");
        DBRow[] rows = printLabelMgrGql.getDnNameBySearchInfo(request, null);
        DBRow[] customers = customerPDFDownload.getAllCustomer();
        DBRow[] titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(request);

        int tabSelect = 0;
        if ("load".equals(cmd)) {
            tabSelect = 1;//选中第二个查询
        }
        int totalnum = 0, creatednum = 0;

    %>

    <title>pdf batch download</title>
    <style type="text/css">
        .demo {
            margin-bottom: 5px;
        }

        .zebraTable td {
            border-bottom: 1px solid #dddddd;
            padding-left: 0px;
            line-height: 30px;
        }

        .but_input {
            float: left;
            padding-right: 10px;
        }

        .tilte_right {
            width: 8%;
            text-align: right;
        }

        /* 调整 refresh 位置 */
        .adjustRefresh {
            background: url(../check_in/imgs/button_long_refresh_2.png) no-repeat 0 0;
            width: 130px;
            height: 42px;
        }

        .search_input {
            background: url(../imgs/search_bg.jpg) repeat 0 0;
            width: 400px;
            height: 24px;
            font-weight: bold;
            border: 1px #bdbdbd solid;

        }

        .search_shadow_bg {
            background: url(../imgs/search_shadow_bg2.jpg) no-repeat 1px 4px;
            width: 420px;
            height: 43px;
            padding-top: 3px;
            padding-left: 3px;
            margin-bottom: 5px;
        }

        #easy_search_father {
            position: absolute;
            width: 0px;
            height: 0px;
            z-index: 1;
        }

        #easy_search {
            position: absolute;
            left: 318px;
            top: -18px;
            width: 55px;
            height: 30px;
            z-index: 9999;
            visibility: visible;
        }

        .input_index {
            margin-right: 22%;
        }

        .w150 {
            width: 150px;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            //遮罩
            $.blockUI.defaults = {
                css: {
                    padding: '8px',
                    margin: 0,
                    width: '170px',
                    top: '45%',
                    left: '40%',
                    textAlign: 'center',
                    color: '#000',
                    border: '3px solid #999999',
                    backgroundColor: '#eeeeee',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
                    '-webkit-box-shadow': '0 1px 12px rgba(0,0,0,0.5)'
                },
                //设置遮罩层的样式
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: '0.6'
                },
                baseZ: 99999,
                centerX: true,
                centerY: true,
                fadeOut: 1000,
                showOverlay: true
            };

            var originCustomers = [];
            $("#sel_customer option").each(function () {
                originCustomers.push(this.value);
            });

            function updateCustomer(customer_val) {
                $("#sel_customer option").remove();


                $("#sel_customer").append($("<option></option>").attr("value", "").text(""));
                var storage = $.trim($("#ps").select2("data").text);
                if (storage == "Valley") {
                    if ($.inArray("VIZIO", originCustomers) > 0) {
                        $("#sel_customer").append($("<option></option>").attr("value", "VIZIO").text("VIZIO"));
                    }
                    if ($.inArray("VZB", originCustomers) > 0) {
                        $("#sel_customer").append($("<option></option>").attr("value", "VZB").text("VZB"));
                    }
                } else if (storage == "Elwood") {
                    if ($.inArray("VZO", originCustomers) > 0) {
                        $("#sel_customer").append($("<option></option>").attr("value", "VZO").text("VZO"));
                    }
                } else if (storage == "Dallas") {
                    if ($.inArray("VIZIO3", originCustomers) > 0) {
                        $("#sel_customer").append($("<option></option>").attr("value", "VIZIO3").text("VIZIO3"));
                    }
                } else if (storage == "DallasWildlife") {
                    if ($.inArray("VIZIO2", originCustomers) > 0) {
                        $("#sel_customer").append($("<option></option>").attr("value", "VIZIO2").text("VIZIO2"));
                    }
                } else if (storage == "Portage") {
                    if ($.inArray("VIZIO1", originCustomers) > 0) {
                        $("#sel_customer").append($("<option></option>").attr("value", "VIZIO1").text("VIZIO1"));
                    }
                }

                if (customer_val) {
                    $("#sel_customer").select2('val', customer_val);
                } else {
                    $("#sel_customer").select2({
                        placeholder: "Select...",
                        allowClear: true
                    });
                }
            }

            $("#ps").select2({
                placeholder: "Select...",
                allowClear: true
            }).on("change", function (e) {
                updateCustomer();
            });
            $("#title").select2({
                placeholder: "Select...",
                allowClear: true
            });
            $("#sel_customer").select2({
                placeholder: "Select...",
                allowClear: true
            });

            updateCustomer($('#sel_customer').val());

            addAutoComplete($("#loadno"),
                    "<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchPdfLoadJSONAction.action",
                    "loadno", "loadno");
            $("#loadno").autocomplete({minLength: 1});

            addAutoComplete($("#dn"),
                    "<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchPdfLoadJSONAction.action?type=dn",
                    "dn", "dn");
            $("#dn").autocomplete({minLength: 1});

            //为carrier绑定
            clickAutoComplete($("#loadno"));

            //选项卡
            $("#tabs").tabs({
                spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
            });
            $("#tabs").tabs("select", <%=tabSelect%>);

            $("#start_time").datetimepicker({
                dateFormat: "mm/dd/y",
                changeYear: true,
                changeMonth: true,
                onClose: function (selectedDate) {
                    var currentDate = $("#start_time").datepicker("getDate");
                    if (currentDate) {
                        currentDate.setDate(currentDate.getDate() + 2);
                        $("#end_time").datetimepicker("option", "minDate", selectedDate).datetimepicker("option", "maxDate", currentDate);
                    }
                }
            });
            $("#end_time").datetimepicker({
                dateFormat: "mm/dd/y",
                changeYear: true,
                changeMonth: true,
                onClose: function (selectedDate) {
                    var currentDate = $("#end_time").datepicker("getDate");
                    if (currentDate) {
                        currentDate.setDate(currentDate.getDate() - 2);
                        $("#start_time").datetimepicker("option", "maxDate", selectedDate).datetimepicker("option", "minDate", currentDate);
                    }
                }
            });

            $("#ui-datepicker-div").hide();

            //all_select全选或者取消选中
            $("input:checkbox[id=all_select]").click(function () {
                if ($(this).attr("checked")) {
                    $("input:checkbox[name^=checkbox]").attr("checked", true);
                }
                else {
                    $("input:checkbox[name^=checkbox]").attr("checked", false);
                }
            });

            //checkbox单个取消时，取消全选
            $("input:checkbox[name^=checkbox]").click(function () {
                if (!$(this).attr("checked") && $("input:checkbox[id=all_select]").attr("checked")) {
                    $("input:checkbox[id=all_select]").attr("checked", false);
                }
            });

        });
    </script>

</head>
<body>
<div id="pdf_"></div>
<div class="demo" style="width:100%">
    <div id="tabs">
        <ul>
            <li><a href="#search">Advanced Search</a></li>
            <li><a href="#load_search">Common Tools</a></li>
        </ul>
        <div id="search">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr style="line-height: 1px">
                    <td class="tilte_right">
                        Start Time :&nbsp;
                    </td>
                    <td width="15%">
                        <input maxlength="100" value="<%=start_time %>" id="start_time" class="w150"/>
                    </td>
                    <td class="tilte_right">
                        End Time:&nbsp;
                    </td>
                    <td width="15%">
                        <input maxlength="100" value="<%=end_time %>" id="end_time" class="w150"/>&nbsp; &nbsp; &nbsp;
                    </td>
                    <td class="tilte_right">
                        WHSE:&nbsp;
                    </td>
                    <td width="15%">
                        <select id="ps" name="ps" class="w150">
                            <option value=""></option>
                            <%
                                for (int j = 0; j < ps.length; j++) {
                            %>
                            <option value="<%=ps[j].get("ps_id",0l)%>"  <%=psId == ps[j].get("ps_id", 0l) ? "selected" : "" %>><%=ps[j].getString("ps_name")%>
                            </option>
                            <%
                                }
                            %>
                        </select>&nbsp; &nbsp;
                    </td>
                    <td class="tilte_right">
                        Title:&nbsp;
                    </td>
                    <td width="14%">

                        <select id="title" name="title" class="w150">
                            <option value="-1">All Title</option>
                            <%
                                for (int j = 0; j < titles.length; j++) {
                            %>
                            <option value="<%=titles[j].get("title_id","")%>"  <%=titles[j].get("title_id", "").equals(title) ? "selected" : "" %>><%=titles[j].getString("title_name")%>
                            </option>
                            <%
                                }
                            %>
                        </select>&nbsp; &nbsp;
                    </td>
                    <td>
                        <div class="but_input">
                            <img src="imgs/green_search.png" style="margin-left: 5px;cursor: pointer;cursor: pointer;"
                                 onclick="search('search');">
                        </div>
                        <div>
                            <img src="imgs/zipdownload.png" style="cursor: pointer;cursor: pointer;"
                                 onclick="download();">
                        </div>
                    </td>
                </tr>
                <tr style="line-height: 1px">
                    <td class="tilte_right">
                        Customer :&nbsp;
                    </td>
                    <td width="15%">
                        <select id="sel_customer" name="customer" style="height:26px;" class="w150">
                            <option value=""></option>
                            <%for (int i = 0; i < customers.length; i++) { %>
                            <option value="<%=customers[i].getString("customer_id")%>"
                                    <%if (!"".equals(customer) && customers[i].getString("customer_id").equals(customer)) {%>
                                    selected="selected"
                                    <%} %>
                                    ><%=customers[i].getString("customer_id")%>
                            </option>
                            <% }%>
                        </select>
                    </td>
                     <td class="tilte_right">DN :&nbsp;</td>
                     <td ><input type="text" id="dn" style="width:150px;" value="<%=dn%>"></td>
                    <td colspan="4">&nbsp;</td>
                    <td id="num_search" colspan="1">
                    </td>
                </tr>
            </table>
        </div>

        <div id="load_search">
            <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="30%" style="padding-top:3px;">
                        <div id="easy_search_father">
                            <div id="easy_search"><a href="javascript:search()"><img id="eso_search"
                                                                                     src="../imgs/easy_search_2.png"
                                                                                     width="70" height="60" border="0"/></a>
                            </div>
                        </div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <div class="search_shadow_bg">
                                        <input id="loadno" name="loadno" type="text" placeholder="Load NO./Order NO."
                                               class="search_input"
                                               style="font-size:17px;font-family:Arial;color:#333333;height:30px;"
                                               onkeydown="if(event.keyCode==13)search('load')" value="<%=loadno %>"/>
                                    </div>
                                </td>
                                <td id="num_load">
                                    <img src="imgs/zipdownload.png"
                                         style="margin-left: 5px;cursor: pointer;cursor: pointer;"
                                         onclick="download();">
                                </td>
                                <td width="67" align="center">
                                    <input type="button" class="button_long_refresh adjustRefresh" onclick="search();"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

    </div>
</div>
<form action="" method="post" name="search_form">
    <input type="hidden" name="cmd" value="search"/>
    <input type="hidden" name="customer" value=""/>
    <input type="hidden" name="carrier" value=""/>
    <input type="hidden" name="shipto" value=""/>
    <input type="hidden" name="ps" value=""/>
    <input type="hidden" name="title" value=""/>
    <input type="hidden" name="start_time" value=""/>
    <input type="hidden" name="end_time" value=""/>
     <input type="hidden" name="dn" value=""/>
</form>
<form action="" method="post" name="load_form">
    <input type="hidden" name="cmd" value="load"/>
    <input type="hidden" name="loadno" value=""/>
</form>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
    <tr>
        <th width="5%" class="left-title" style="vertical-align:center;text-align:center;">
            <input type="checkbox" name="all_select" id="all_select" value=""/>
        </th>
        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Entry_ID</th>
        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">WHSE</th>
        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Title</th>
        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Customer</th>
        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Ship to</th>
        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Carrier</th>
        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">Load/Order/PO</th>
        <th width="10%" class="left-title" style="vertical-align:center;text-align:center;">DN</th>
        <th width="15%" class="left-title" style="vertical-align:center;text-align:center;">PDF</th>
    </tr>
    <%
        if (rows != null && rows.length > 0) {
            for (DBRow row : rows) {
                if (psId == -1 || psId == row.get("ps_id", 0l)) {
                    totalnum++;
    %>

    <tr class="download">
        <td width="5%" align="center">
            <div class="input_index">
                <%=(totalnum < 10 ? "0" + totalnum : totalnum)%>
                <input type="checkbox" name="checkbox" value="<%=row.getString("file_id")%>"/>
                <input type="hidden" name="dn" value="<%=row.getString("dn")%>"/>
            </div>

        </td>
        <td width="10%" align="center">
            <span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;">&nbsp;<%=row.getString("entry_id")%></span>
        </td>
        <td width="10%" align="center">
            <span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;">&nbsp;<%=row.getString("wsname")%></span>
        </td>
        <td width="10%" align="center">
            <span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;">&nbsp;<%=row.getString("title")%></span>
        </td>
        <td width="10%" align="center">
            <span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;">&nbsp;<%=row.getString("customer_id")%></span>
        </td>
        <td width="10%" align="center">
            <span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;">&nbsp;<%=row.getString("account_id")%></span>
        </td>
        <td width="10%" align="center">
            <span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;">&nbsp;<%=row.getString("company_name")%></span>
        </td>

        <td width="10%" align="center">
		  			<span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;">&nbsp;
		  			<%=StringUtil.isBlank(row.get("load_no", null)) ? row.get("orderno", null) : row.get("load_no", null)%>
		  			</span>
        </td>
        <td width="10%" align="center">
            <span style="font-size: 12px;color:green;font-weight: bold;font-family:Arial;">&nbsp;<%=row.getString("dn")%></span>
        </td>

        <td width="15%" align="center">
            <%
                if (!StringUtil.isBlank(row.getString("file_id"))) {
                    creatednum++;
            %>
            <img src="imgs/pdf.png" style="margin-left:30px;margin-bottom:5px;margin-top:10px; float: left;"
                 width="25px;" height="25px;">&nbsp;
            <a href='http://<%=serverName%>/Sync10/_fileserv_print/download/<%=row.getString("file_id")%>.pdf'
               style="color:blue">
                <span style="float: left;margin-top: 10px;">&nbsp;&nbsp;<%=row.getString("dn") + ".pdf"%></span>
                <%} %>
            </a>
        </td>
    </tr>
    <% }
    } %>
</table>
<%
} else {
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
    <tr>
        <td style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;"
            colspan="3">NO PDF
        </td>
    </tr>
</table>
<%}%>
</body>
<div style="display:none" ;><img src="../js/thickbox/loadingAnimation6.gif"></div>
</html>
<script type="text/javascript">
    var totalnum =<%=totalnum%>;
    var creatednum =<%=creatednum %>;


    $("#num_search").append("<div style='margin-left:50px;' >" + creatednum + "/" + totalnum + "<div/>");
    $("#num_load").append("<div style='margin-left:5px' >" + creatednum + "/" + totalnum + "<div/>");
    //鼠标滑动换图片
    function mouseOver(obj) {
        $(obj).attr("src", "imgs/down_load_2.png");
    }
    function mouseOut(obj) {
        $(obj).attr("src", "imgs/down_load_1.png");
    }

    //当点击文本框时触发事件
    function clickAutoComplete(jqObj) {
        jqObj.bind('click', function () {
            var val = $.trim($(this).val());
            if (val != "") {
                jqObj.autocomplete("search");
            }
        });
    }

    //提供回车事件支持
    function autoCMEnter(field, event) {
        var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

        if (keyCode == 13) {
            var i;
            var arr = $('input');
            for (i = 0; i < arr.length; i++)
                if (field == arr[i])
                    break;
            i = (i + 1) % arr.length;
            arr[i].focus();
        }
    }

    //按条件搜索
    function search(flag) {
        if (flag == "load") {
            var load_no = $("#loadno").val();
            if (load_no.length <= 0) {
                showMessage("please input load#", "alert");
            } else {
                $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
                document.load_form.loadno.value = load_no;
                document.load_form.submit();
                window.onload = function () {
                    $.unblockUI();
                }
            }
        } else if (flag == "search") {
            var sel_customer = $("#sel_customer").val();
            var ps = $("#ps").val();
            var title = $("#title").val();
            var start_time = $("#start_time").val();
            var end_time = $("#end_time").val();
            var dn =$("#dn").val();
            if ((start_time.length <= 0 || end_time.length <= 0) && dn=="") {
                showMessage("please choose start time and end time", "alert");
            } else {
                $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});//添加遮罩
                document.search_form.customer.value = sel_customer;
                document.search_form.ps.value = ps;
                document.search_form.title.value = title;
                document.search_form.start_time.value = start_time;
                document.search_form.end_time.value = end_time;
                document.search_form.dn.value = dn;


                document.search_form.submit();
                window.onload = function () {
                    $.unblockUI();
                }
            }
        } else {
            var load_no = $("#loadno").val();
            if (load_no.length <= 0) {
                showMessage("please input load#", "alert");
            } else {
                $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
                document.load_form.loadno.value = load_no;
                document.load_form.submit();
                window.onload = function () {
                    $.unblockUI();
                }
            }
        }

    }


    //下载文件
    function download() {
        var fileChecked = $("input:checkbox[name^=checkbox][checked]").length;

        if (fileChecked > 100) {
            if (confirm("Too many files selected, whether to choose ?")) {
                batchDownload();
                return;
            } else {
                batchDownload();
            }
        } else if (fileChecked != 0) {
            batchDownload();
        } else {
            showMessage("Without the selected file.", "alert");
        }
    }
    function batchDownload() {
        var file_id = "";
        //选择时，下载选中的文件
        $("input:checkbox[name^=checkbox][checked]").each(
                function (i) {
                    file_id = $(this).val().trim();
                    if (file_id) {
                        var ifr = document.createElement('iframe');

                        var pdffile = "http://<%=serverName%>/Sync10/_fileserv_print/download/" + file_id + ".pdf";
                        ifr.style.display = 'none';
                        ifr.src = pdffile;
                        ifr.onload = function () {
                        }
                        $("#pdf_").append(ifr);
                    }
                }
        );
    }

    function dowmloadZip() {
        $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
        var file_id = "";
        var dn = "";
        var fileArray = [];
        var file_ids = "";
        //选择时，下载选中的文件
        $("input:checkbox[name^=checkbox][checked]").each(
                function (i) {
                    file_id = $(this).val().trim();
                    dn = $(this).next("input[name='dn']").val().trim();
                    var file = {};
                    file.dn = dn;
                    file.file_id = file_id;
                    fileArray.push(file);
                }
        );
        var pdffile = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/batch/BatchCreatePdfAction.action?file_ids=' + jQuery.fn.toJSON(fileArray);

        var ifr = document.createElement('iframe');

        ifr.style.display = 'none';
        ifr.src = pdffile;
        ifr.onload = function () {
            $.unblockUI();
        }
        $("#pdf_").append(ifr);
    }
</script>
