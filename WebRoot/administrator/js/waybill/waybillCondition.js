var url;
url =$('#uri').val();

(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

//不同tab 回车不同功能
$(function(){
	document.onkeydown = function(e){
	    var ev = document.all ? window.event : e;
	    if(ev.keyCode==13) {
	    	var suoyin=$("#tabs").tabs("option","selected");
	    	if(suoyin==1){
	    		findOutList();
	    	}else if(suoyin==3){
	    		findOutTransportList();
	    	}
	    }
	}
}); 

//选项卡
jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
        $("#tabs").tabs("select",0);  //默认选中第一个
});

//过滤出库单
function findOutList(){
	var ps_id=$('#cid').val();
	var out_id=$('#findWaybillId').val();
	var out_for_type=$('#out_for_type').val();
	if(out_id=="*出库单号查询"){
		out_id=0;
	}
	add_out_list(out_id,ps_id,out_for_type,1);
}
//过滤包含转运单的出库单
function findOutTransportList(){
	var ps_id=$('#cidTransport').val();
	var out_id=$('#findTransportId').val();
	var out_for_type=$('#out_for_transport').val();
	if(out_id=="*出库单号查询"){
		out_id=0;
	}
	find_out_transport_list(out_id,ps_id,out_for_type,1);
}

function inputIn(){
 	$("#findWaybillId").val("").css("color","black");
}
function outInput(){
	var  st = $("#findWaybillId").val();
	if($.trim(st).length < 1) {
		$("#findWaybillId").val("*出库单号查询").css("color","silver");
	}
}
function inputInTransport(){
 	$("#findTransportId").val("").css("color","black");
}
function outInputTransport(){
	var  st = $("#findTransportId").val();
	if($.trim(st).length < 1) {
		$("#findTransportId").val("*出库单号查询").css("color","silver");
	}
}

//加载出库单列表
function add_out_list(out_id,ps_id,out_for_type,num){
	$.ajax({
	 	url:url+'administrator/waybill/out_list_new.html?ps_id='+ps_id+'&out_id='+out_id+'&p='+num+'&out_for_type='+out_for_type,
	 	dataType:'html',
	 	type:'post',
	 	success:function(data){
	 		$("#outbound_list").html(data);
	 	}
	});
}
//加载包含转运单出库单列表
function find_out_transport_list(out_id,ps_id,out_for_type,num){
	$.ajax({
	 	url:url+'administrator/waybill/out_transport_list_new.html?ps_id='+ps_id+'&out_id='+out_id+'&p='+num+'&out_for_type='+out_for_type,
	 	dataType:'html',
	 	type:'post',
	 	success:function(data){			
	 		$("#out_transport_list").html(data);
	 	}
	});
}

//加载转运单列表
function add_transport_list(send_psid,receive_psid){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var sum_count=0;
	$.ajax({
	 	url:url+'administrator/waybill/transport_list_new.html?send_psid='+send_psid+'&receive_psid='+receive_psid,
	 	dataType:'html',
	 	async:false,
	 	type:'post',
	 	success:function(data){
			$.unblockUI();       //遮罩关闭
	 		$("#transport_list").html(data);
	 		//总条数
	 		sum_count=$('#transport_list_count').val();
	 	}
	});
	return sum_count;
}

//条件转运单
function addTransport(id,name,type){
	var ty='';
	var send_psid=0;
	var sumCount=0;
	if(type=='chukucangku'){
	   ty='出库仓库：';
	   $('#chukucangku').css('display','none'); 
	   $('#shouhuocangku').css('display','block'); 
	   sumCount=add_transport_list(id,0);
	}
	if(type=='shouhuocangku'){
		ty='收货仓库：';
		$('#shouhuocangku').css('display','none'); 
		
		if($("div[type='chukucangku']",$('#transportCondition')).html()!=null){
			send_psid=$("div[type='chukucangku']",$('#transportCondition')).attr("zhi");
		}
		sumCount=add_transport_list(send_psid,id);
	}
	$('#transportTiaoshu').html('共（'+sumCount+'）条');
	var html='<div id="'+type+''+id+'" type="'+type+'" title="'+name+'" zhi="'+id+'" style="border:1px #090 solid;float:left;min-width;font-size:13px;cursor:pointer;margin-left:10px;padding-left:2px;">'+ty+'<span style="color:red;font-weight: bold;">'+name+'&nbsp;×<span></div>';
	var avt=$(html).appendTo($('#transportCondition')); //添加到页面
	$('#transportTiaojian').css('display','block');      //显示条件
	//删除条件
	$(avt).click(function(){     //绑定删除事件
		if($(this).attr("type")=='shouhuocangku'){
			$('#shouhuocangku').css("display","block");
		}
		if($(this).attr("type")=='chukucangku'){
			$('#shouhuocangku').css("display","block");
		}
		$('#'+this.id).remove();           //删除当前条件
		
		if($('#transportCondition').html()==''){ 
			$('#transportTiaojian').css('display','none'); 
		   $('#chukucangku').css("display","block");
		   $('#shouhuocangku').css("display","none");
		   $("#transport_list").html('');
		}else{
			var chuku=0;
			var shouhuo=0;
			if($("div[type='chukucangku']",$('#transportCondition')).html()!=null){
				chuku=$("div[type='chukucangku']",$('#transportCondition')).attr("zhi");
			}
			if($("div[type='shouhuocangku']",$('#transportCondition')).html()!=null){
				shouhuo=$("div[type='shouhuocangku']",$('#transportCondition')).attr("zhi");
			}
			add_transport_list(chuku,shouhuo);
		}
	});	
}

//条件运单方法
function addCondition(id,name,type){
	
	//判断 点击进来的是否为 仓库条件 如果不是 判断 条件里是否包含了仓库 
	if(type!='cangku'){
		if($("div[type='cangku']",$('#condition')).html()==null){
			alert("请选择仓库");
			return false;
		}
	}

	var sumCount=seach(id,type,1);              //查询运单详细
	ajaxSeachNum(id,type);                      //点击查询包含数量
	ajaxSeachLine(id,type);                     //点击条件时 加载产品线
	$('#tiaojian').css('display','block');      //条件显示
	var ty='';
	if(type=='line'){
	   $('#line').css("display","none");
	   ty='产品线：';
	}else if(type=='cangku'){
	   ty='仓库：';	
	   $('#cangku').css("display","none");
	   $('#kuaidi').css("display","block");
	   $('#line').css("display","block");
	   $('#zhongliang').css("display","block");
	   $('#baohan').css("display","block");
	   ajaxGetShipCompany(id);                  //ajax加载 快递
	}else if(type=='kuaidi'){
		ty='快递：';
	   $('#kuaidi').css("display","none");     
	}else if(type=='baohan'){
		ty='包含件数：';
	   $('#baohan').css("display","none");	
	}else if(type=='zhongliang'){
		ty='重量：';
	   $('#zhongliang').css("display","none");
	}
	//总条数
	$('#tiaoshu').html('共（'+sumCount+'）条');
	var html='<div id="'+type+''+id+'" type="'+type+'" title="'+name+'" zhi="'+id+'" style="border:1px #090 solid;float:left;min-width;font-size:13px;cursor:pointer;margin-left:10px;padding-left:2px;">'+ty+'<span style="color:red;font-weight: bold;">'+name+'&nbsp;×<span></div>';
	var av=$(html).appendTo($('#condition')); //添加到页面
	
	$(av).click(function(){     //绑定删除事件
		if($(this).attr("type")=='line'){
			$('#line').css("display","block");
		}else if($(this).attr("type")=='cangku'){
			$('#cangku').css("display","block");
			$('#kuaidi').css("display","none");    //去掉仓库条件后隐藏快递       
	        $("div[type='kuaidi']",$('#condition')).remove();//去掉仓库条件 同时去掉快递
		}else if($(this).attr("type")=='kuaidi'){
            $('#kuaidi').css("display","block"); 
		}else if($(this).attr('type')=='baohan'){
 			$('#baohan').css("display","block");
		}else if($(this).attr('type')=='zhongliang'){
			$('#zhongliang').css("display","block");
		}
		
		$('#'+this.id).remove();           //删除当前条件
		
		if($('#condition').html()==''){    //判断如果条件都没 隐藏条件
			$('#tiaojian').css('display','none');
			ajaxLineCount();               //无条件查询产品线下数量
			ajaxBaohanNo();                //无条件包含数量
			$('#dataList').html("");       //如果没有条件 清空运单详细列表
			$('#line').css("display","none");
			$('#zhongliang').css("display","none");
			$('#baohan').css("display","none");
		}else{
			ajaxDetConditionLine();        //去掉条件查询产品线下数量
			ajaxDetPkcount();              //去掉条件时 按剩下条件查询
			var zongshu=detConditionSeach(1);          //减条件时 查询运单详细
			$('#tiaoshu').html('共（'+zongshu+'）条');
		}          
	});	
}

//无条件检索包含
function ajaxBaohanNo(){
	 $.ajax({
		    url:url+'action/administrator/waybill/AjaxWaybillPkcountConditionAction.action',
			dataType:'json',
			success:function(data){
				var html='';
				for(var i=0;i<data.length;i++){
					html+='<div style="width:200px;float:left;margin-left:10px;">'+
							'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'baohan'"+')" style="color:#069;text-decoration:none;outline:none;">'+
								data[i].name+'件('+data[i].count+')'+
							'</a>'+
					     '</div>';		
				}
				 $('#baohanCondition').html(html);
		    } 
	    });
}

//根据仓库检索出快递
function ajaxGetShipCompany(ps_id){
  	$.ajax({
		url:url+'action/administrator/order/getExpressCompanysByPsIdJSON.action',
		dataType:'json',
		data:{ps_id:ps_id},
		success:function(data){
			if (data!=""){
				var html='';
				$.each(data,function(i){					
					html+='<div style="width:200px;float:left;margin-left:10px;">'+
				            '<a href="javascript:void(0)" onclick="addCondition('+data[i].sc_id+','+"'"+data[i].name+"'"+','+"'kuaidi'"+')" style="color:#069;text-decoration:none;outline:none;">'+data[i].name+'</a>'+
				          '</div>';
				});
				$('#kuaidivalue').html(html);
			}
	 	}
	});
}

//包含件数点击事件
function baohanClick(){
	var value=$('#baohanvalue').val();
	if(value==""|| value==' '){
       alert("请输入包含件数");
       return false;
	}
	addCondition(value,value,'baohan');
}

//减条件的时候查询运单详细
function detConditionSeach(num){
	var scId=0;
	var psId=0;
	var allWeight=0;
	var line=0;
	var pkcount=0;
	var sumCount=0;
	if($("div[type='cangku']",$('#condition')).html()!=null){
		psId=$("div[type='cangku']",$('#condition')).attr("zhi");
	}
	if($("div[type='kuaidi']",$('#condition')).html()!=null){
		scId=$("div[type='kuaidi']",$('#condition')).attr("zhi");
	} 
	if($("div[type='line']",$('#condition')).html()!=null){
	    line=$("div[type='line']",$('#condition')).attr("zhi");
	} 
	if($("div[type='zhongliang']",$('#condition')).html()!=null){
		allWeight=$("div[type='zhongliang']",$('#condition')).attr("zhi");
	} 
	if($("div[type='baohan']",$('#condition')).html()!=null){
		pkcount=$("div[type='baohan']",$('#condition')).attr("zhi");
	}
	var params = {sc_id:scId,ps_id:psId,allWeight:allWeight,line:line,pkcount:pkcount,p:num};
	var str = jQuery.param(params);
	$.ajax({
	 	 	url:url+'administrator/waybill/waybill_list_new.html',
	 	 	dataType:'html',
	 	 	type:'post',
	 		async:false,
	 	 	data:str,
	 	 	success:function(data){
	 	 		$("#dataList").html(data);
	 	 		sumCount=$('#sumCount').val();
	 	 	}
	 });
	return sumCount;
}

//根据条件查询运单详细
function seach(id,type,num){
	var scId=0;
	var psId=0;
	var allWeight=0;
	var line=0;
	var pkcount=0;
	var sumCount=0;
	if($("div[type='cangku']",$('#condition')).html()!=null){
		psId=$("div[type='cangku']",$('#condition')).attr("zhi");
	}
	if($("div[type='kuaidi']",$('#condition')).html()!=null){
		scId=$("div[type='kuaidi']",$('#condition')).attr("zhi");
	} 
	if($("div[type='line']",$('#condition')).html()!=null){
	    line=$("div[type='line']",$('#condition')).attr("zhi");
	} 
	if($("div[type='zhongliang']",$('#condition')).html()!=null){
		allWeight=$("div[type='zhongliang']",$('#condition')).attr("zhi");
	} 
	if($("div[type='baohan']",$('#condition')).html()!=null){
		pkcount=$("div[type='baohan']",$('#condition')).attr("zhi");
	}
	if(type=='line'){
		  line=id;
	}else if(type=='cangku'){
	      psId=id;       
	}else if(type=='kuaidi'){
		  scId=id;
	}else if(type=='baohan'){
		  pkcount=id;
	}else if(type=='zhongliang'){
		  allWeight=id;
	} 
	var params = {sc_id:scId,ps_id:psId,allWeight:allWeight,line:line,pkcount:pkcount,p:num};
	var str = jQuery.param(params);
	$.ajax({
	 	 	url:url+'administrator/waybill/waybill_list_new.html',
	 	 	dataType:'html',
	 	 	type:'post',
	 	 	async:false,
	 	 	data:str,
	 	 	success:function(data){
	 	 		$("#dataList").html(data);
	 	 		sumCount=$('#sumCount').val();
	 	 	}
	 });
	return sumCount;
}

//无条件查询产品线下运单
function ajaxLineCount(){
	$.ajax({
	    url:url+'action/administrator/waybill/AjaxWaybillCountLineAlwaysAction.action',
		dataType:'json',
		success:function(data){
			var html='';
			for(var i=0;i<data.length;i++){
				html+='<div style="width:200px;float:left;margin-left:10px;">'+
						'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'line'"+')" style="color:#069;text-decoration:none;outline:none;">'+
							data[i].name+'('+data[i].count+')'+
						'</a>'+
				     '</div>';		
			}
			$('#lineCondition').html(html);
	    } 
    });
}
//去条件时查询产品线下运单数量
function ajaxDetConditionLine(){
	var scId=0;
	var psId=0;
	var allWeight=0;
	var line=0;
	var pkcount=0;
	if($("div[type='cangku']",$('#condition')).html()!=null){
		psId=$("div[type='cangku']",$('#condition')).attr("zhi");
	}
	if($("div[type='kuaidi']",$('#condition')).html()!=null){
		scId=$("div[type='kuaidi']",$('#condition')).attr("zhi");
	} 
	if($("div[type='line']",$('#condition')).html()!=null){
	    line=$("div[type='line']",$('#condition')).attr("zhi");
	} 
	if($("div[type='zhongliang']",$('#condition')).html()!=null){
		allWeight=$("div[type='zhongliang']",$('#condition')).attr("zhi");
	} 
	if($("div[type='baohan']",$('#condition')).html()!=null){
		pkcount=$("div[type='baohan']",$('#condition')).attr("zhi");
	} 
	var params = {sc_id:scId,ps_id:psId,allWeight:allWeight,line:line,pkcount:pkcount};
	var str = jQuery.param(params);
    $.ajax({
	    url:url+'action/administrator/waybill/AjaxWaybillCountLineAction.action',
		dataType:'json',
		data:str,
		success:function(data){
			var html='';
			for(var i=0;i<data.length;i++){
				html+='<div style="width:200px;float:left;margin-left:10px;">'+
						'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'line'"+')" style="color:#069;text-decoration:none;outline:none;">'+
							data[i].name+'('+data[i].count+')'+
						'</a>'+
				     '</div>';		
			}
			$('#lineCondition').html(html);
	    } 
    });
}
//根据条件查产品线下数量
//scId 	   快递id
//psId 	   仓库id
//allWeight 运单总重量
//line       产品线
//pkcount    包含数量
function ajaxSeachLine(id,type){
	var scId=0;
	var psId=0;
	var allWeight=0;
	var line=0;
	var pkcount=0;
	if($("div[type='cangku']",$('#condition')).html()!=null){
		psId=$("div[type='cangku']",$('#condition')).attr("zhi");
	}
	if($("div[type='kuaidi']",$('#condition')).html()!=null){
		scId=$("div[type='kuaidi']",$('#condition')).attr("zhi");
	} 
	if($("div[type='line']",$('#condition')).html()!=null){
	    line=$("div[type='line']",$('#condition')).attr("zhi");
	} 
	if($("div[type='zhongliang']",$('#condition')).html()!=null){
		allWeight=$("div[type='zhongliang']",$('#condition')).attr("zhi");
	} 
	if($("div[type='baohan']",$('#condition')).html()!=null){
		pkcount=$("div[type='baohan']",$('#condition')).attr("zhi");
	} 
	if(type=='line'){
		  line=id;
	}else if(type=='cangku'){
	      psId=id;       
	}else if(type=='kuaidi'){
		  scId=id;
	}else if(type=='baohan'){
		  pkcount=id;
	}else if(type=='zhongliang'){
		  allWeight=id;
	}
	var params = {sc_id:scId,ps_id:psId,allWeight:allWeight,line:line,pkcount:pkcount};
	var str = jQuery.param(params);
    $.ajax({
	    url:url+'action/administrator/waybill/AjaxWaybillCountLineAction.action',
		dataType:'json',
		data:str,
		success:function(data){
			var html='';
			for(var i=0;i<data.length;i++){
				html+='<div style="width:200px;float:left;margin-left:10px;">'+
						'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'line'"+')" style="color:#069;text-decoration:none;outline:none;">'+
							data[i].name+'('+data[i].count+')'+
						'</a>'+
				     '</div>';		
			}
			$('#lineCondition').html(html);
	    } 
    });
}

//去掉条件时 按条件查询包含数量
function ajaxDetPkcount(){
	var scId=0;
	var psId=0;
	var allWeight=0;
	var line=0;
	var pkcount=0;
	if($("div[type='cangku']",$('#condition')).html()!=null){
		psId=$("div[type='cangku']",$('#condition')).attr("zhi");
	}
	if($("div[type='kuaidi']",$('#condition')).html()!=null){
		scId=$("div[type='kuaidi']",$('#condition')).attr("zhi");
	} 
	if($("div[type='line']",$('#condition')).html()!=null){
	    line=$("div[type='line']",$('#condition')).attr("zhi");
	} 
	if($("div[type='zhongliang']",$('#condition')).html()!=null){
		allWeight=$("div[type='zhongliang']",$('#condition')).attr("zhi");
	} 
	if($("div[type='baohan']",$('#condition')).html()!=null){
		pkcount=$("div[type='baohan']",$('#condition')).attr("zhi");
	} 
	var params = {sc_id:scId,ps_id:psId,allWeight:allWeight,line:line,pkcount:pkcount};
	var str = jQuery.param(params);
    $.ajax({
	    url:url+'action/administrator/waybill/AjaxWaybillCountPkcountAction.action',
		dataType:'json',
		data:str,
		success:function(data){
			var html='';
			for(var i=0;i<data.length;i++){
				html+='<div style="width:200px;float:left;margin-left:10px;">'+
						'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'baohan'"+')" style="color:#069;text-decoration:none;outline:none;">'+
							data[i].name+'件('+data[i].count+')'+
						'</a>'+
				     '</div>';		
			}
			 $('#baohanCondition').html(html);
	    } 
    });
}


//根据条件查包含下数量
function ajaxSeachNum(id,type){
	var scId=0;
	var psId=0;
	var allWeight=0;
	var line=0;
	var pkcount=0;
	if($("div[type='cangku']",$('#condition')).html()!=null){
		psId=$("div[type='cangku']",$('#condition')).attr("zhi");
	}
	if($("div[type='kuaidi']",$('#condition')).html()!=null){
		scId=$("div[type='kuaidi']",$('#condition')).attr("zhi");
	} 
	if($("div[type='line']",$('#condition')).html()!=null){
	    line=$("div[type='line']",$('#condition')).attr("zhi");
	} 
	if($("div[type='zhongliang']",$('#condition')).html()!=null){
		allWeight=$("div[type='zhongliang']",$('#condition')).attr("zhi");
	} 
	if($("div[type='baohan']",$('#condition')).html()!=null){
		pkcount=$("div[type='baohan']",$('#condition')).attr("zhi");
	} 
	if(type=='line'){
		  line=id;
	}else if(type=='cangku'){
	      psId=id;       
	}else if(type=='kuaidi'){
		  scId=id;
	}else if(type=='baohan'){
		  pkcount=id;
	}else if(type=='zhongliang'){
		  allWeight=id;
	}
	var params = {sc_id:scId,ps_id:psId,allWeight:allWeight,line:line,pkcount:pkcount};
	var str = jQuery.param(params);
    $.ajax({
	    url:url+'action/administrator/waybill/AjaxWaybillCountPkcountAction.action',
		dataType:'json',
		data:str,
		success:function(data){
			var html='';
			for(var i=0;i<data.length;i++){
				html+='<div style="width:200px;float:left;margin-left:10px;">'+
						'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'baohan'"+')" style="color:#069;text-decoration:none;outline:none;">'+
							data[i].name+'件('+data[i].count+')'+
						'</a>'+
				     '</div>';		
			}
			 $('#baohanCondition').html(html);
	    } 
    });
}

var outOrderDialog;
//创建拣货单
function addOutOrder(){
	var scId=0;
	var psId=0;
	var allWeight=0;
	var line=0;
	var pkcount=0;
	if($("div[type='cangku']",$('#condition')).html()!=null){
		psId=$("div[type='cangku']",$('#condition')).attr("zhi");
	}
	if($("div[type='kuaidi']",$('#condition')).html()!=null){
		scId=$("div[type='kuaidi']",$('#condition')).attr("zhi");
	} 
	if($("div[type='line']",$('#condition')).html()!=null){
	    line=$("div[type='line']",$('#condition')).attr("zhi");
	} 
	if($("div[type='zhongliang']",$('#condition')).html()!=null){
		allWeight=$("div[type='zhongliang']",$('#condition')).attr("zhi");
	} 
	if($("div[type='baohan']",$('#condition')).html()!=null){
		pkcount=$("div[type='baohan']",$('#condition')).attr("zhi");
	} 
	var params = {sc_id:scId,ps_id:psId,allWeight:allWeight,line:line,pkcount:pkcount};
	var str = jQuery.param(params);
	
	var ur=url+'administrator/transport/book_way_out_door_or_location.html?scId='+scId+'&psId='+psId+'&allWeight='+allWeight+'&line='+line+'&pkcount='+pkcount;
	outOrderDialog=$.artDialog.open(ur, {title: '选择门和位置',width:'900px',height:'500px', lock: true});
	
	
	  
	/*
	//创建拣货单 ，更新当前运单到这张拣货单
    $.ajax({
	    url:url+'action/administrator/waybill/AjaxAddOutOrderAction.action',
		dataType:'json',
		data:str,
		success:function(data){
    	   if(data.out_id!=undefined){
    		//	var ur=url+'administrator/order/print/dialog_out_order_list.html?out_id='+data.out_id+'&out_for_type=1&create_time='+data.create_time;
    		//	$.artDialog.open(ur, {title: '出库单',width:'900px',height:'500px', lock: true});
    		 //选择门和仓库位置
     		  var ur=url+'administrator/transport/book_out_door_or_location.html?out_id='+data.out_id+'&ps_id='+data.ps_id+'&out_for_type='+data.out_for_type+'&create_time='+data.create_time;
     		  outOrderDialog=$.artDialog.open(ur, {title: '选择门和位置',width:'900px',height:'500px', lock: true});
    	   } 
	    } 
   });
   */
}
function changeOutDialogTitle(){
	outOrderDialog.title("拣货单");
}


var transportDialog;
//转运单生成拣货单
function addOutOrderByTransport(){
	var se=$("input[name='choose']");
	var str='';
	var zhi='';
	var chuku=0;
	//出货仓库
	if($("div[type='chukucangku']",$('#transportCondition')).html()!=null){
		chuku=$("div[type='chukucangku']",$('#transportCondition')).attr("zhi");
	}
	for(var i=0;i<se.length;i++){
		if(se[i].checked){
			var value=$(se[i]).val();
			str+=value+',';
		}
	}
	if(str==''){
	   alert('请选择转运单');
	   return false;
	}
	zhi = str.substring(0, str.lastIndexOf(','));
	
	//选择门和仓库位置
	  var ur=url+'administrator/transport/book_out_door_or_location.html?ids='+zhi+'&send_psid='+chuku;
	  transportDialog=$.artDialog.open(ur, {title: '选择门和位置',width:'900px',height:'500px', lock: true});
}
	
function changeDialogTitle(){
	transportDialog.title("拣货单");
}


