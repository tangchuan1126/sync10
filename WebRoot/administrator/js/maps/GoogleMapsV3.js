var MAP_PROVIDER_NAME = "GoogleMapsV3"; 

var MapsEvent = google.maps.event;
/**
*** Create Map
**/
function jsNewMap(element){
	var options = {
			center:jsNewLatLng(DEFAULT_CENTER.lat, DEFAULT_CENTER.lon),
			zoom:DEFAULT_ZOOM,
			disableDoubleClickZoom:true,
			panControl:false
	};
	return new google.maps.Map(element,options);     
}
/**
 *** Create Map 无任何组件的地图
 **/
function jsNewMapClear(element){
	var options = {
			center:jsNewLatLng(DEFAULT_CENTER.lat, DEFAULT_CENTER.lon),
			zoom:DEFAULT_ZOOM,
			disableDoubleClickZoom:true,
			scrollwheel:true,
			mapTypeControl:false,
			overviewMapControl:false,
			panControl:false,
			rotateControl:false,
			scaleControl:false,
			streetViewControl:false,
			zoomControl:false
	};
	return new google.maps.Map(element,options);     
}

/**
*** Create LatLng
**/
function jsNewLatLng(lat, lng){
    return new google.maps.LatLng(lat, lng);
};

/**
*** Create LatLngBounds
**/
function jsNewLatLngBounds(){
    return new google.maps.LatLngBounds();
};

/**
*** Create Size
**/
function jsNewSize(W, H) 
{
    return new google.maps.Size(W, H);
};

/**
*** Create Point
**/
function jsNewPoint(X, Y){
    return new google.maps.Point(X, Y);
};

/**
*** Create Polyline
**/
function jsNewPolyline(path, strokeColor, strokeWeight, strokeOpacity){
    return new google.maps.Polyline({
    	path:path,
    	map:jsmap.getMap(),
    	strokeColor: strokeColor,
    	strokeWeight: strokeWeight,
    	strokeOpacity :strokeOpacity
    });
};
google.maps.Polyline.prototype.putData = function(data){
	this.data = data;
};
google.maps.Polyline.prototype.getBounds = function(){
	var path = this.getPath().getArray();
	var bounds = new google.maps.LatLngBounds();
	for(var i=0; i<path.length; i++){
		bounds.extend(path[i]);
	}
	return bounds;
};
//获取路径坐标字符串表示 ,格式：lng,lat;lng,lat...
google.maps.Polyline.prototype.getPointStr = function(){
	var path = this.getPath().getArray();
	var str = "";
	for(var i=0; i<path.length; i++){
		str += path[i].lng()+","+path[i].lat()+";";
	}
	return str.substring(0, str.length-1);
};
google.maps.Polyline.prototype.getCenter = function(){
	return this.getBounds().getCenter();
}
/**
*** Create Polygon
**/
function jsNewPolygon(paths, strokeColor, strokeWeight, strokeOpacity, fillColor, fillOpacity){
    return new google.maps.Polygon({
    	paths:paths, 
    	strokeColor:strokeColor, 
    	strokeWeight:strokeWeight, 
    	strokeOpacity:strokeOpacity, 
    	fillColor:fillColor, 
    	fillOpacity:fillOpacity,
    	zIndex:1,
    	map:jsmap.getMap()
    });
};
google.maps.Polygon.prototype.putData = function(data){
	this.data = data;
};
google.maps.Polygon.prototype.getBounds = function(){
	var paths = this.getPaths().getArray();
	var bounds = new google.maps.LatLngBounds();
	for(var i=0; i<paths.length; i++){
		var path = paths[i].getArray();
		for(var j=0; j<path.length; j++){
			bounds.extend(path[j]);
		}
	}
	return bounds;
};
google.maps.Polygon.prototype.getCenter = function(){
	return this.getBounds().getCenter();
}
//获取第一条路径坐标字符串表示 ,格式：lng,lat;lng,lat...
google.maps.Polygon.prototype.getPointStr = function(){
	var path = this.getPath().getArray();
	var str = "";
	for(var i=0; i<path.length; i++){
		str += path[i].lng()+","+path[i].lat()+";";
	}
	return str.substring(0, str.length-1);
};
/**
 * Creat Sector
 */
function jsNewSector(pathOption, strokeColor, strokeWeight, strokeOpacity, fillColor, fillOpacity){
	var paths = computeSectorPath(pathOption);//计算扇形的轨迹。
	return new google.maps.Polygon({
    	paths:paths, 
    	strokeColor:strokeColor, 
    	strokeWeight:strokeWeight, 
    	strokeOpacity:strokeOpacity, 
    	fillColor:fillColor, 
    	fillOpacity:fillOpacity,
    	map:jsmap.getMap()
    });
}
/*function jsNewStretchPolygon(paths, strokeColor, strokeWeight, strokeOpacity, fillColor, fillOpacity){
	 this.paths=paths;
	 this.strokeColor=strokeColor;
	 this.strokeWeight=strokeWeight;
	 this.strokeOpacity=strokeOpacity;
	 this.fillColor=fillColor;
	 this.fillOpacity=fillOpacity;
}
jsNewStretchPolygon.prototype =new  jsNewPolygon();
jsNewStretchPolygon.prototype.stretch=function() {
	var path=this.getPath();
	for(var i=0;i<path.length;i++){
		var latlng=jsNewLatLng(path[i]);
		var maker=jsNewMarker(latlng,"../imgs/maps/dragCircle.png");
	}
}
*/
/**
 * Creat Rectangle
 */

function jsNewRectangle(bounds, strokeColor, strokeWeight, strokeOpacity, fillColor, fillOpacity){
	return new google.maps.Rectangle({
		bounds:bounds,
		strokeColor:strokeColor, 
    	strokeWeight:strokeWeight, 
    	strokeOpacity:strokeOpacity, 
    	fillColor:fillColor, 
    	fillOpacity:fillOpacity,
    	map:jsmap.getMap()
    });
}
//获取矩形范围字符串表示,格式：lng(sw),lat(sw),lng(ne),lat(ne)
google.maps.Rectangle.prototype.getPointStr = function(){
	var bounds = this.getBounds();
	var sw = bounds.getSouthWest();
	var ne = bounds.getNorthEast();
	var str = sw.lng()+","+sw.lat()+","+ne.lng()+","+ne.lat();
	return str;
};
/**
 * Creat Circle
 */
function jsNewCircle(center, radius, strokeColor, strokeWeight, strokeOpacity, fillColor, fillOpacity){
	return new google.maps.Circle({
		center:center,
		radius:radius,
		strokeColor:strokeColor, 
    	strokeWeight:strokeWeight, 
    	strokeOpacity:strokeOpacity, 
    	fillColor:fillColor, 
    	fillOpacity:fillOpacity,
    	map:jsmap.getMap()
	});
}
//获取圆坐标字符串表示,格式：lng(sw),lat(sw),lng(ne),lat(ne)
google.maps.Circle.prototype.getPointStr = function(){
	var center = this.getCenter();
	var radius = this.getRadius();
	var str = center.lng()+","+center.lat()+","+radius;
	return str;
};
/**
 * Create Marker
 */
function jsNewMarker(position,icon){
	var marker = new google.maps.Marker({
		animation: MARKER_ANIMATION ? MARKER_ANIMATION : 0 ,
		position:position,
		visible:true,
		cursor:"pointer",
		icon:icon,
		map:jsmap.getMap()
	});
	MapsEvent.addListener(marker,"mouseover",function(){
		marker.setZIndex(marker.getZIndex()+1);
	});
	MapsEvent.addListener(marker,"mouseout",function(){
		marker.setZIndex(marker.getZIndex()-1);
	});
	/*MapsEvent.addListener(marker,"click",function(){
		setFloatWindowContent("<b>Current Position:</b>\n"+position.toUrlValue());
	});*/
	return marker;
}
//获取坐标字符串表示 ,格式：lng,lat
google.maps.Marker.prototype.getPointStr = function(){
	var latlng = this.getPosition();
	return latlng.lng()+","+latlng.lat();
};
google.maps.Marker.prototype.putData = function(data){
	this.data = $.extend({},this.data,data);
};
/**
 * Create MarkerImage
 */

function jsNewMarkerImege(url, size, origin, anchor, scaledSize){
	return new google.maps.MarkerImage(url, size, origin, anchor, scaledSize);
}
/**
 * Create InfoWindow
 */
function jsNewInfoWindow(content){
	return new google.maps.InfoWindow({
		content:content
	});
}
/**
 * Creat heatmapLayer
 */
function jsNewHeatmapLayer(latLngArray){
	return new google.maps.visualization.HeatmapLayer({
		data:latLngArray,
		map:jsmap.getMap()
	});
}
function jsNewFusionTablesLayer(query,styles){
	return new google.maps.FusionTablesLayer({
		query: query,
		styles: styles,
		map:jsmap.getMap()
    });
}

function jsNewKmlLayer(url,fun,map,fitBounds){
	var kmlLayer = new google.maps.KmlLayer({
		url:url,
		options:{
			map: (map ? map : jsmap.getMap()),
			suppressInfoWindows:true,
			preserveViewport : !fitBounds
		}
	});
	fun ? MapsEvent.addListener(kmlLayer,"click",fun):null;
	return kmlLayer;
}

// ----------------------------------------------------------------------------
//var toolObject = null; //polyline线或polygon多边形
var debugIndex = 0;
function JSMap(element,simpleMap){
	this.googleMap =  simpleMap ? jsNewMapClear(element) : jsNewMap(element);
    this.centerBounds = jsNewLatLngBounds();
    this.zoom = this.googleMap.getZoom();
    this.mouseLatlng = null;
    this.flag = {
    		"drawLocation" : true
    };
    
    this.historyBounds = null;
    this.currentMarker = null; //历史轨迹  当前播放点
    this.latLngList = [];      //all lat/lng of this.markerList
    this.markerList = [];  //all markers of this.googleMap
    this.iconList = [];
    this.polyline = null;  
    this.replayIndex = 0;  //for replay,the current marker index in this.markerList
    
    this.markerCurrent = {};  //实时位置
    this.labelCurrent = {};
    this.infoWindowCurrent = {};
    
    this.heatmap = null;
    
    this.fusionTablesLayer = null;
    
    this.regionBoundaries = {};  //边界坐标数据
    this.regionPolygon = {};	//国家、省份区域
    this.mapControls = {};  //自定义控件
    
    this.mapTool = "move_map";  //地图工具状态    move_map:移动  measure_distance：测量距离   measure_area:测量面积
    this.mapToolPositionList = [];  //需要测量的经纬度数组
    this.mapToolObject = null; //polyline线或polygon多边形
    this.mapToolLabel = null;  //测量值overlay
    
    this.overlay = new google.maps.OverlayView();
    this.overlay.draw = function(){};
    this.overlay.setMap(this.googleMap);
    
    this.storageKml = {};   //仓库KML图层
    this.storageBounds = {};  //仓库内对象(door parking area staging  等)坐标数据
    this.storageLights = {};  //仓库内对象(light)坐标数据
    this.storageObjPolygon = {};  //door parking
    this.storageObjsBounds = {};  //包含仓库内对象(door parking等)的仓库视口
    this.storageCatalogPolygon = {};  //库存位置，单独存放，方便清理图层
    this.storageAreaPolygon = {};  //区域，用于位置的分块处理 隐藏area
    this.storageLocationPolygon = {};  //位置，单独存放，方便按area加载
    this.storageLayerPolygon = {};  //仓库各图层位置，包括area  staging warehouse(HouseBase area)
    this.storageWebcamMarker={};//仓库摄像头图层
    this.storagePrinterMarker={};//仓库打印机图层
    this.storageResourceMarker={};//仓库打印机图层
    this.storageDemoLayer={};//仓库图层包括可以拖拽的printer webcam  拖拽进去的矩形
    this.storageProvenLayer={};//仓库图层
    this.storageZoneDocksLayer={};//仓库图层
    this.storageZonePersonLayer={};//仓库图层
    this.geoFencing = {};  //围栏线路关键点数据
    this.geoFencingLabel = {};  //围栏线路关键点标签
    this.mapToolBackFun = null;  //回调函数
    this.storageRoadPolyline = {}; //仓库内线路
    this.routePathPolyLine = {};  //路径规划结果
    this.storageCoordinateSysPolyline = {}; //仓库坐标系
    
    this.areaTitle = {}; //area和title的关系，{ps_id:{area_name:[titles]}}
    this.areaDock = {}; //area和door的关系，{ps_id:{area_name:[docks]}}
    
    this.tempBoundData = null; //边界临时数据
    
	//地图层级改变
	MapsEvent.addListener(this.googleMap,"zoom_changed",function(){
		jsmap.zoom = this.zoom;
		if(document.getElementById("map_zoom")){
			document.getElementById("map_zoom").innerHTML = "地图层级:" + this.zoom;
		}
		//改变区域边界显示
		//changeRegionBoundariesDisplay();
		//改变仓库标签显示
		changeStorageObjLabelDisplay();
		//改变marker显示
		changeStorageMarkerDisplay();
	});
    //移动鼠标 地图
	MapsEvent.addListener(this.googleMap,"mousemove",function(e){
		jsmap.mouseLatlng = e.latLng;
		/*var latLngAry = e.latLng.toUrlValue().split(",");
		var mousePosition = document.getElementById("mouse_position");
		if(mousePosition){
			mousePosition.innerHTML = "经纬度:" + latLngAry[1]+","+latLngAry[0];
		}*/
	}); 
	//鼠标右击 地图
	MapsEvent.addListener(this.googleMap,"rightclick",function(e){
		//AREA内Location处理
		for(var key in jsmap.storageAreaPolygon){
			var area = jsmap.storageAreaPolygon[key];
			if(area && containsLocation(e.latLng,area)){
				if(storageKmlRightClick){
					var position = jsmap.overlay.getProjection().fromLatLngToContainerPixel(e.latLng);
					storageKmlRightClick(area.data,position, e.latLng);
					jsmap.storageProvenLayer[area.data.key]=jsmap.storageLayerPolygon[key];
				}
				break;
			}
		}
	});
	//鼠标左击 地图
	MapsEvent.addListener(this.googleMap,"click",function(e){
		
	});
	//移动鼠标 div
	MapsEvent.addDomListener(element, 'mousemove', function(e){
		var position = jsmap.fromContainerPixelToLatLng(e.clientX-this.offsetLeft,e.clientY-this.offsetTop);
		if(jsmap.mapToolPositionList.length > 0){
			var tool = jsmap.mapTool;
			//测距、测面积
			if(tool=="measure_distance" 
					|| tool=="measure_area"
					|| tool=="line"
					|| tool=="polygon"){
				jsmap.mapToolObject.setPath(jsmap.mapToolPositionList.concat([position]));
			}else if(tool=="rect"){//绘制矩形
				var bounds = jsNewLatLngBounds().extend(jsmap.mapToolPositionList[0]).extend(position);
				jsmap.mapToolObject.setBounds(bounds);
			}else if(tool=="circle"){//绘制圆
				var radius = computeLength([jsmap.mapToolPositionList[0],position]);
				jsmap.mapToolObject.setRadius(radius);
			}else if(tool=="road"){
				var len = jsmap.mapToolPositionList.length;
				jsmap.mapToolObject.setPath([jsmap.mapToolPositionList[len-1],position]);
			}
		}
		
		//AREA内Location处理
		if(jsmap.flag["drawLocation"]){
			for(var key in jsmap.storageAreaPolygon){
				var area = jsmap.storageAreaPolygon[key];
				if(area && !area.data.draw && containsLocation(position,area)){
					drawLocation(area.data);
					//area.data.draw = true;
					break;
				}
			}
		}else{
			for(var key in jsmap.storageAreaPolygon){
				var area = jsmap.storageAreaPolygon[key];
				if(area && !area.data.draw && containsLocation(position,area)){
					$("#jsmap").data("curr_poly_data",area.data)
					break;
				}
			}
		}
	});
	//鼠标左击 div
	MapsEvent.addDomListener(element, 'click', function(e){
		var tool = jsmap.mapTool;
		
		if(tool!=null && tool!="" && tool!="move_map"){
			var position = jsmap.overlay.getProjection().fromContainerPixelToLatLng(jsNewPoint(e.clientX-this.offsetLeft,e.clientY-this.offsetTop));
			//关键点直接回填结果
			if(tool == "point"){
				if(jsmap.mapToolObject != null){
					jsmap.mapToolObject.setMap(null);
				}
				jsmap.mapToolObject = jsNewMarker(position, null);
				backfillDrawResult();  //回填绘制结果
				return null;
			}
			jsmap.mapToolPositionList.push(position);
			if(jsmap.mapToolPositionList.length == 1){
				if(jsmap.mapToolObject != null){
					jsmap.mapToolObject.setMap(null);
				}
				jsmap.mapToolObject = creatMapToolObject(tool);
				if(tool=="measure_distance" || tool=="measure_area"){
					jsmap.mapToolLabel = new LabelOverlay(position,"","right",null,true);
				}
			}else{
				if(tool=="measure_distance" || tool=="measure_area"){//测距、测面积
					jsmap.mapToolObject.getPath().pop();
					jsmap.mapToolObject.getPath().push(position);
					var valueType = tool=="measure_distance"?"length":"area";
					jsmap.mapToolLabel.setHtml(getLabelValue(valueType,jsmap.mapToolPositionList));
					jsmap.mapToolLabel.setPosition(position);
				}else if(tool=="line" || tool=="polygon"){  //绘制线、面，双击结束
					jsmap.mapToolObject.getPath().pop();
					jsmap.mapToolObject.getPath().push(position);
				}else if(tool=="rect"){	//绘制矩形，单击结束
					var bounds = jsNewLatLngBounds().extend(jsmap.mapToolPositionList[0]).extend(position);
					jsmap.mapToolObject.setBounds(bounds);
					jsmap.mapToolPositionList = [];
					backfillDrawResult();  //回填绘制结果
				}else if(tool=="circle"){//绘制圆，单击结束
					var radius = computeLength([jsmap.mapToolPositionList[0],position]);
					jsmap.mapToolObject.setRadius(radius);
					jsmap.mapToolPositionList = [];
					backfillDrawResult();  //回填绘制结果
				}else if(tool=="road"){
					jsmap.mapToolObject.getPath().pop();
					jsmap.mapToolObject.getPath().push(position);
					backfillDrawResult();  //回填绘制结果
				}
			}
		}
		
		//AREA内Location处理
		for(var key in jsmap.storageAreaPolygon){
			var area = jsmap.storageAreaPolygon[key];
			
			var latLng=jsmap.overlay.getProjection().fromContainerPixelToLatLng(jsNewPoint(e.clientX-this.offsetLeft,e.clientY-this.offsetTop));
			if(area && containsLocation(latLng,area)){
				prescribedRoute_(this,e,area);
				break;
			}
		}
	});
	//鼠标双击 div
	MapsEvent.addDomListener(element, 'dblclick', function(e){
		var tool = jsmap.mapTool;
		//var position = jsmap.overlay.getProjection().fromContainerPixelToLatLng(jsNewPoint(e.clientX-this.offsetLeft,e.clientY-this.offsetTop));
		//结束测距、测面积
		if(tool=="measure_distance" || tool=="measure_area"){
			jsmap.clearMapToolData();
		}else if(tool=="line" || tool=="polygon"){  //绘制线、面
			jsmap.mapToolPositionList = [];
			jsmap.mapToolObject.getPath().pop(); //清除双击产生的重复点
			backfillDrawResult();  //回填绘制结果
		}else if(tool=="rect"){//绘制矩形
			
		}else if(tool=="circle"){//绘制圆
			
		}else if(tool=="road"){
			var fun = jsmap.mapToolBackFun;
			jsmap.clearMapToolData();
			jsmap.mapToolBackFun = fun; //支持连续绘制
		}
	});
}
//清除地图
JSMap.prototype.clearLayers = function(){
	//清除工具数据
	this.clearMapToolData();
	//清除历史轨迹
	this.clearHistory();
	//清除围栏
	this.clearGeofencing();
	//清除库存位置
	this.clearStorageCatalog();
	//清除仓库位置
	this.clearLocation();
	//清除边界图层
	this.clearRegion();
	//清除摄像头图层
	this.clearWebcam();
	//清除打印机图层
	this.clearPrinter();
	//清除线路图层
	this.clearRoad();
    //清除路径规划
	this.clearRoutePath();
	//清除仓库坐标系
	this.clearStorageCoordinateSys();
};
//清除围栏、线路、关键点
JSMap.prototype.clearGeofencing = function(){
	for(var key in this.geoFencing){
		this.geoFencing[key].setMap(null);
	}
	for(var key in this.geoFencingLabel){
		this.geoFencingLabel[key].setMap(null);
		this.geoFencingLabel[key]= null;
	}
};
//清除历史轨迹
JSMap.prototype.clearHistory = function(){
	this.removeAllMarker();  
	this.hidePolyline();
	if(this.currentMarker){
		this.currentMarker.setMap(null);
		removeFloatWindow();
	}
};
//像素坐标转经纬度坐标
JSMap.prototype.fromContainerPixelToLatLng = function(x,y){
	var latlng = null;
	if(this.overlay.getProjection()){
		latlng = this.overlay.getProjection().fromContainerPixelToLatLng(jsNewPoint(x,y));
	}
	return latlng;
}
//经纬度转像素坐标
JSMap.prototype.fromLatLngToContainerPixel = function(lat,lng){
	var pixel = null;
	if(this.overlay.getProjection()){
		pixel = this.overlay.getProjection().fromLatLngToContainerPixel(jsNewLatLng(lat,lng));
	}
	return pixel;
}
//创建测量对象
function creatMapToolObject(tool){
	//测距、绘制路线
	if(tool=="measure_distance" || tool=="line"){
		return jsNewPolyline(jsmap.mapToolPositionList, "#DD0000",2,1);
	}else if(tool=="measure_area" || tool=="polygon"){//测面积、绘制面
		return jsNewPolygon(jsmap.mapToolPositionList, "#DD0000",2,0.9,"#FF0000",0.2);
	}else if(tool=="rect"){   //绘制矩形 
		var bounds = jsNewLatLngBounds().extend(jsmap.mapToolPositionList[0]);
		return jsNewRectangle(bounds, "#DD0000",2,0.9,"#FF0000",0.2)
	}else if(tool=="circle"){
		return jsNewCircle(jsmap.mapToolPositionList[0], 0, "#DD0000",2,0.9,"#FF0000",0.2)
	}else if(tool=="road"){
		return jsNewPolyline(jsmap.mapToolPositionList, "#FF0000",4,1);  //91dafe
	}
}
//计算测量结果
function getLabelValue(type,path){
	var value = 0;
	var unit = "";
	if(type == "length"){
		value = computeLength(path);
		if(value >= 1000){
			value = (value/1000).toFixed(1);
			unit = "km";
		}else{
			value = value.toFixed(1);
			unit = "m";
		}
	}else if(type == "area"){
		value = computeArea(path);
		if(value == 0){
			value = "";
		}else{
			if(value >= 1000000){
				value = (value/1000000).toFixed(1);
				unit = "km²";
			}else{
				value = value.toFixed(1);
				unit = "m²";
			}
		}
	}
	return value + unit;
}
//回填绘图结果
function backfillDrawResult(){
	if(jsmap.mapToolBackFun != null){  //页面存在回填方法时
		var obj = jsmap.mapToolObject;
		var tool = jsmap.mapTool;
		if(tool=="road"){
			var path = obj.getPath().getArray();
			var len = path.length;
			var newPath = [path[len-2],path[len-1]];
			jsmap.mapToolBackFun(newPath);
		}else{
			jsmap.mapToolBackFun(obj.getPointStr());
		}
	}
}
JSMap.prototype.getMap = function(){
	return this.googleMap;
};
JSMap.prototype.setZoom = function(zoom){
	this.googleMap.setZoom(zoom);
};
JSMap.prototype.setOptions = function(options){
	this.googleMap.setOptions(options);
}
//设置地图工具状态
JSMap.prototype.setMapTool = function(tool){
	if(tool){
		this.clearMapToolData();
		this.mapTool = tool;
		if(tool != "move_map"){
			this.setOptions({
				draggableCursor:"crosshair",
				draggingCursor:"crosshair"
			});
		}else{
			this.setOptions({
				draggableCursor:"",
				draggingCursor:""
			});
		}
	}
}
//当前地图视图是否包含object，  true包含      false不包含      ___有交集(待定)
JSMap.prototype.contains = function(obj,objType){
	var mapBounds = this.googleMap.getBounds();
	if(objType == "position"){
		return mapBounds.contains(obj);
	}
}
//清除测距、测面积数据
JSMap.prototype.clearMapToolData = function(){
	this.mapToolPositionList = [];
	//this.mapToolValue = [];
	if(this.mapToolObject != null){
		this.mapToolObject.setMap(null);
		this.mapToolObject = null;
	}
	if(this.mapToolLabel != null){
		this.mapToolLabel.setMap(null);
		this.mapToolLabel = null;
	}
	this.mapToolBackFun = null;
}
//打点
JSMap.prototype.drawPushpins = function(pushpins,visible){
	var count = 0;
	if(pushpins && pushpins.length>0){
		this.removeAllMarker();  //清空历史记录
		this.markerList = [];  
		if(this.polyline != null){
			this.polyline.setMap(null);
			this.polyline = null;
		}
		this.latLngList = [];
		this.iconList = [];
		
		this.historyBounds = jsNewLatLngBounds();
		var lastPosition = null;
		for(var i=0; i<pushpins.length; i++){
			//连续2个相同的点算作无效数据
			var pushpin = pushpins[i];
			if(lastPosition != null){
				var eq = false;
				eq = (lastPosition.lat() == pushpin.position.lat()) && (lastPosition.lng() == pushpin.position.lng());
				if(eq){continue;}
			}
			
			lastPosition = pushpin.position;
			this.historyBounds.extend(pushpin.position);
			this.latLngList.push(pushpin.position);
			this.iconList.push(pushpin.icon);
			count++;
		}
		this.currentMarker = jsNewMarker(pushpins[0].position, pushpins[0].icon);  //历史轨迹第一个点
		this.currentMarker.setOptions({
			visible: visible
		});
		//this.panToBounds(this.historyBounds);	//set view
	}
	return count;
};
//画线
JSMap.prototype.drawPolyline = function(latLngList){
	if(this.polyline == null){
		this.polyline = jsNewPolyline(latLngList, "red", 2, 1);
	}else{
		this.polyline.setPath(latLngList);
	}
};
//历史轨迹视口调整
JSMap.prototype.fitHistoryBounds = function(force){
	/**
	 * force: true 强制调整
	 */
	var hisBounds = this.historyBounds;
	if(hisBounds && !hisBounds.isEmpty()){
		if(force){
			this.fitBounds(this.historyBounds);
		}else{
			var mapBound = this.googleMap.getBounds();
			if(mapBound.intersects(this.historyBounds)){	//有交集
				//if(!mapBound.contains(this.latLngList[0])){		//不包含第一点
				this.panToBounds(hisBounds);
				//}
			}else{	//无交集
				this.fitBounds(hisBounds);
			}
		}
	}
};

//设置地图中心
JSMap.prototype.setCenter = function(latLng){
	this.googleMap.setCenter(latLng);
};
//设置地图包含视图
JSMap.prototype.fitBounds = function(bounds){
	this.googleMap.fitBounds(bounds);
};
//返回原视图
JSMap.prototype.backToOriginalView = function(){
	this.googleMap.fitBounds(this.centerBounds);
}
//设置地图视图
JSMap.prototype.panToBounds = function(bounds){
	this.googleMap.panToBounds(bounds);
};
//隐藏所有点
JSMap.prototype.hideAllMarker = function(){
	if(this.markerList.length>0){
		for(var i=0; i<this.markerList.length; i++){
			this.markerList[i].setOptions({
				visible: false
			});
		}
	}
};
//显示所有点
JSMap.prototype.showAllMarker = function(){
	if(this.markerList.length>0){
		for(var i=0; i<this.markerList.length; i++){
			this.markerList[i].setOptions({
				visible: true,
				animation: MARKER_ANIMATION
			});
		}
	}
};
//隐藏单个点
JSMap.prototype.hideMarker = function(marker){
	if(marker){
		marker.setOptions({
			visible: false
		});
	}
};
//显示单个点
JSMap.prototype.showMarker = function(marker){
	if(marker){
		marker.setOptions({
			visible: true,
			animation: MARKER_ANIMATION
		});
	}
};
//移除所有点
JSMap.prototype.removeAllMarker = function(){
	if(this.markerList.length>0){
		for(var i=0; i<this.markerList.length; i++){
			this.markerList[i].setMap(null);
		}
	}
};
//回放行驶线路
JSMap.prototype.showCurrentMarker = function(){
	if(this.latLngList.length>0 && this.replayIndex<this.latLngList.length){
		this.currentMarker.setOptions({
			visible: true,
			animation: MARKER_ANIMATION,
			icon:this.iconList[this.replayIndex],
			position:this.latLngList[this.replayIndex]
		});
		return true;
	}else{
		return false; 
	}
};
//隐藏线路
JSMap.prototype.hidePolyline = function(){
	if(this.polyline){
		this.polyline.setMap(null);
	}
};
//显示线路
JSMap.prototype.showPolyline = function(){
	if(this.polyline){
		this.polyline.setMap(this.googleMap);
	}else{
		this.polyline = jsNewPolyline(this.latLngList, "red", 2, 1);
	}
};
//显示/隐藏热图
JSMap.prototype.showOrHideHeatmap = function(){
	if(this.heatmap){
		if(this.heatmap.getMap()){
			this.heatmap.setMap(null);
			return false;
		}else{
			this.heatmap.setMap(this.googleMap);
		}
	}else{
		if(this.latLngList.length>0){
			this.heatmap = jsNewHeatmapLayer(this.latLngList);
		}
	}
	return true;
};
//创建FusionTables
JSMap.prototype.creatFusionTablesLayer = function(data){
	if(this.fusionTablesLayer){
		this.fusionTablesLayer.setMap(this.googleMap);
	}else{
		var query = {};
		var styles = {};
		
		this.fusionTablesLayer = jsNewFusionTablesLayer(query,styles);
	}
};
//隐藏热图
JSMap.prototype.deleteFusionTablesLayer = function(){
	if(this.fusionTablesLayer){
		this.fusionTablesLayer.setMap(null);
	}
};
//显示地界 
JSMap.prototype.showRegionBoundary = function(data,fun){
	var regionType = fun ? "country" : "province";
	var regionIds = "";
	var bounds = this.regionBoundaries;
	for(var i=0; i<data.length; i++){
		if(bounds[regionType+"_"+data[i].regionId] == null){
			regionIds += data[i].regionId+",";
		}
	}
	if(regionIds){
		regionIds = regionIds.substring(0, regionIds.length-1);
		getRegionBoundariesAjax(data,fun,regionType,regionIds);
	}else{
		drawRegionBoundary(data,fun);
	}
};

//获取地图边界
var tempBoundData = null;
function getRegionBoundariesAjax(data,fun,regionType,regionIds){
	var _data = data;
	var _fun = fun;
	var _type = regionType;
	$.ajax({
		url:systenFolder+'action/administrator/maps/GetGoogleMapsBoundariesAction.action',
		data:'type='+regionType+'&regionIds='+regionIds,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	    },
		success:function(data){
	    	if(data && data.boundary){
	    		var bounds = jsmap.regionBoundaries;
	    		var b = data.boundary;
	    		var info = "";
    			for(var i=0; i<b.length; i++){
    				if(b[i].boundary && b[i].boundary != ""){
    					bounds[_type+"_"+b[i].region_id] = b[i];
    				}else{
    					info += b[i].region_name+",";
    				}
    			}
    			if(info != ""){
    				showMessage("Boundary data Deficient...","alert");
    			}
    			drawRegionBoundary(_data,_fun);
	    	}
		},
		error:function(data){
		}
	});
}
//绘制边界
function drawRegionBoundary(data,fun){
	//jsmap.clearRegion();
	jsmap.clearLayers();
	var bounds = jsmap.regionBoundaries;
	var regionType = fun ? "country" : "province";
	var mapBound = jsNewLatLngBounds();
	for(var i=0; i<data.length; i++){
		var key = regionType + "_" +data[i].regionId;
		if(bounds[key]){
			var bound = bounds[key];
			var pointStr = bound.boundary
			var boundaryStr = pointStr.split(";");
			var paths = [];
			var regionBound = jsNewLatLngBounds();
			for(var j=0; j<boundaryStr.length; j++){
				var points = boundaryStr[j].split(" ");
				var path = [];
				for(var k=0; k<points.length; k++){
					var point = points[k].split(",");
					var latLng = jsNewLatLng(point[1], point[0]);
					path.push(latLng);
					regionBound.extend(latLng);
				}
				paths.push(path);
			}
			mapBound.union(regionBound);
			var polygon = jsNewPolygon(paths, data[i].color, 2, 1, data[i].color, 1);
			var content = "<table style='width:160px; height:60px'>";
			content += "	<tr>";
			content += "		<td align='right'>Region：</td>";
			content += "		<td>"+bound.region_name+"</td>";
			content += "	</tr>";
			content += "	<tr>";
			content += "		<td align='right'>Count：</td>";
			content += "		<td>"+data[i].count+"</td>";
			content += "	</tr>";
			content += "</table>";
			var infoWindow = jsNewInfoWindow(content);
			infoWindow.setOptions({
				position : regionBound.getCenter(),
				disableAutoPan : true
			});
			polygon.putData({
				infoWindow : infoWindow,
				regionId : data[i].regionId,
				color : data[i].color,
				fun : fun
			});
			//鼠标进入时高亮
			MapsEvent.addListener(polygon,"mouseover",function(e){
				this.setOptions({
					//fillColor:HIGH_LIGHT_COLOR,
					strokeColor : "#FFFFFF"
				});
				//this.data.infoWindow.setPosition(e.latLng);
				this.data.infoWindow.open(this.getMap());
			});
			//鼠标离开时恢复
			MapsEvent.addListener(polygon,"mouseout",function(e){
				this.setOptions({
					fillColor : this.data.color,
					strokeColor : this.data.color
				});
				if(this.data.infoWindow){
					this.data.infoWindow.close();
				}
			});
			//鼠标移动
			/*MapsEvent.addListener(polygon,"mousemove",function(e){
				this.data.infoWindow.setPosition(e.latLng);
				this.data.infoWindow.open(this.getMap());
			});*/
			//鼠标点击
			MapsEvent.addListener(polygon,"click",function(e){
				if(this.data.fun){
					this.data.fun(this.data.regionId);
				}
			});
			jsmap.regionPolygon[key] = polygon;
		}
	}
	jsmap.fitBounds(mapBound);
}
//清除边界图层
JSMap.prototype.clearRegion = function(){
	var objs = this.regionPolygon;
	for(var key in objs){
		if(objs[key]){
			//objs[key].data.label.setMap(null);
			//objs[key].data.label = null;
			objs[key].data.infoWindow.close();
			objs[key].data.infoWindow.setMap(null);
			objs[key].data.infoWindow = null;
			objs[key].setMap(null);
			objs[key] = null;
		}
	}
};
//显示parking docks占用情况
var tempData = {};
JSMap.prototype.showParkingDocksOccupancy = function(psId,objAry){
	if($.isEmptyObject(this.storageBounds[psId])){
		tempData["parkAndDoor"] = objAry;
		tempData["kml"] = psId;  //仓库对应kml文件
		
		getStorageBoundsAjax(psId,"kml_storage");
	}else{
		drawParkingDocks(psId,objAry);
	}
}
var parkingDocksLayer = "";
function drawParkingDocks(psId,objAry){
	var storageBounds = jsmap.storageBounds[psId];
	if(objAry == null){
		objAry = tempData["parkAndDoor"];
	}
	var fillColor = {
			"parking":"#93FBBF",
			"docks":"#93FBBF"
	};
	var strokeColor = {
			"parking":"#69D46E",
			"docks":"#69D46E",
			"unable":"#9acbaf" //无效门     灰绿色
	};
	jsmap.storageObjsBounds[psId + "_door_parking"] = jsNewLatLngBounds();
	var objsBounds = jsmap.storageObjsBounds[psId + "_door_parking"]; //kml视口,包含所有门和停车位
	for(var i=0; i<objAry.length; i++){
		var obj = objAry[i];
		if(parkingDocksLayer.indexOf(obj.obj_type)<0){
			continue;
		}
		//统一门和停车位状态
		var name = (obj.obj_type + "_" + obj.obj_name).toLowerCase();
		var colorSt = strokeColor[obj.obj_type];  //边框颜色
		var color = "";    //填充颜色
		var rel_type=obj.rel_type?obj.rel_type:"";
		var child_status=obj.child_status?obj.child_status:"";
		var colorKey="";
		
		/** obj_status
		 * 数据库定义：
		 *        parking     dock
		 *  空闲		1			3
		 *  保留		-			1
		 *  占用		2			2
		 * 	
		 */
		/**rel_type 定义:
		 *          parking     docks
		 * delivery  1            1
		 * pick up   2            2
		 */
		/**child_status 定义:
		 *          parking     docks
		 * open      0            0
		 * close     1            1
		 */
		if(obj.obj_type=="docks"){
			if(obj.obj_status==3){
				colorKey="100";//表示 free
			}else if(obj.obj_status==2||obj.obj_status==1){
				if(obj.dm_rel_type == 3||obj.dm_rel_type==2||obj.dm_rel_type==1){ 
					colorKey =obj.obj_status+rel_type+child_status;//
				}else if(obj.rel_type=4){
					colorKey='000'; //表示none
				}
			}
			
		}
		if(obj.obj_type=="parking"){
			if(obj.obj_status==1){
				colorKey="300";//表示 free
			}else if(obj.obj_status==2){
				if(obj.dm_rel_type == 3||obj.dm_rel_type==2||obj.dm_rel_type==1){ 
					colorKey =obj.obj_status+rel_type+child_status;//
				}else if(obj.rel_type=4){
					colorKey='000'; //表示none
				}
			}
		}
		//状态颜色
		if(obj.obj_type=="docks"||obj.obj_type=="parking"){
			//color = PACKING_OCCU_COLOR[obj.main_status];
			color = PACKING_OCCU_COLOR[colorKey]?PACKING_OCCU_COLOR[colorKey]:fillColor[obj.obj_type];
		}
		//不可用的门
		if(obj.available_status == "1"&&obj.obj_type=="docks"){
			color = PACKING_OCCU_COLOR["-1"];
			colorSt = strokeColor["unable"];
		}
		var labelContent = "<span style='color: #A129E6'><p align='center'>"+obj.obj_name+"</p>" 
							+ (obj.gate_container_no ? "CTN#"+obj.gate_container_no : (obj.gate_liscense_plate ? obj.gate_liscense_plate: "")) 
							+ "</span>";
		if(jsmap.storageObjPolygon[psId+"_"+name]){
			var polygon = jsmap.storageObjPolygon[psId+"_"+name];
			polygon.setOptions({
				fillColor:color,
				strokeColor:colorSt,
				map : jsmap.getMap()
			});	
			if(polygon.data){
				polygon.data.labelContent = labelContent;
				polygon.data.strokeColor = colorSt;
				polygon.data.type = obj.obj_type;
				polygon.data.id = obj.obj_id;
				polygon.data.areaId = obj.area_id;
				polygon.data.name = obj.obj_name;
				polygon.data.status = obj.obj_status || 0;  //状态不存在时默认为0
				polygon.data.availableStatus = obj.obj_available_status;
				polygon.data.ctn = obj.gate_container_no;
				polygon.data.car = obj.gate_liscense_plate;
				//polygon.data.label.setHtml(labelContent);
			}
		}else{
			if(storageBounds[name]){
				var bounds = storageBounds[name];
				var points = bounds.split(" ");
				var path = [];
				var bound = jsNewLatLngBounds();
				for(var j=0; j<points.length; j++){
					var point = points[j].split(",");
					var latLng = jsNewLatLng(point[1], point[0]);
					path.push(latLng);
					bound.extend(latLng);
				}
				objsBounds.union(bound);
				var polygon = jsNewPolygon(path, colorSt, 2, 1, color, 1);
				
				var position = bound.getCenter();
				/*var labelContent = "<span style='color: #A129E6'><p align='center'>"+obj.obj_name+"</p>" 
									+ (obj.gate_container_no ? "CTN#"+obj.gate_container_no : (obj.gate_liscense_plate ? obj.gate_liscense_plate: "")) 
									+ "</span>";*/
				var label = new LabelOverlay(position,obj.obj_name,"center");
				label.zoomRange = [20,21];
				
				polygon.putData({
					key : name,
					labelContent : labelContent,
					strokeColor : colorSt,
					type : obj.obj_type,
					id : obj.obj_id,
					areaId : obj.area_id,
					name : obj.obj_name,
					status : obj.obj_status || 0,
					availableStatus : obj.obj_available_status,
					ctn : obj.gate_container_no, 
					car : obj.gate_liscense_plate,
					label : label,
					psId : psId,
					path:path,
					state:STORAGE_POSITION_TYPE[obj.obj_type.toUpperCase()] //  区分点击事件点击的是packing 还是localtion
					
				});
				setStorageObjEvent(polygon);
				jsmap.storageObjPolygon[psId+"_"+name] = polygon;
			}
		}
	}
	//jsmap.storageObjsBounds[psId + "_door_parking"] = objsBounds;
	tempData["parkAndDoor"] = null;
}
function drawParkingDocks(psId,objAry){
	var storageBounds = jsmap.storageBounds[psId];
	if(objAry == null){
		objAry = tempData["parkAndDoor"];
	}
	var fillColor = {
			"parking":"#93FBBF",
			"docks":"#93FBBF"
	};
	var strokeColor = {
			"parking":"#69D46E",
			"docks":"#69D46E",
			"unable":"#9acbaf" //无效门     灰绿色
	};
	jsmap.storageObjsBounds[psId + "_door_parking"] = jsNewLatLngBounds();
	var objsBounds = jsmap.storageObjsBounds[psId + "_door_parking"]; //kml视口,包含所有门和停车位
	for(var i=0; i<objAry.length; i++){
		var obj = objAry[i];
		if(parkingDocksLayer.indexOf(obj.obj_type)<0){
			continue;
		}
		//统一门和停车位状态
		var name = (obj.obj_type + "_" + obj.obj_name).toLowerCase();
		var colorSt = strokeColor[obj.obj_type];  //边框颜色
		var color = "";    //填充颜色
		var rel_type=obj.rel_type?obj.rel_type:"";
		var child_status=obj.child_status?obj.child_status:"";
		var colorKey="";
		
		/** obj_status
		 * 数据库定义：
		 *        parking     dock
		 *  空闲		1			3
		 *  保留		-			1
		 *  占用		2			2
		 * 	
		 */
		/**rel_type 定义:
		 *          parking     docks
		 * delivery  1            1
		 * pick up   2            2
		 */
		/**child_status 定义:
		 *          parking     docks
		 * open      0            0
		 * close     1            1
		 */
		if(obj.obj_type=="docks"){
			if(obj.obj_status==3){
				colorKey="100";//表示 free
			}else if(obj.obj_status==2||obj.obj_status==1){
				if(obj.dm_rel_type){
					if(obj.dm_rel_type==2||obj.dm_rel_type==1){ //delivery或者pick up
						colorKey =obj.obj_status+obj.dm_rel_type+child_status;//
					}else if(obj.dm_rel_type == 3){ //both
						colorKey =obj.obj_status+rel_type+child_status;//
					}else if(obj.dm_rel_type==4){//none
						colorKey='000'; //表示none
					}
				}
			}
			
		}
		if(obj.obj_type=="parking"){
			if(obj.obj_status==1){
				colorKey="300";//表示 free
			}else if(obj.obj_status==2){
				if(obj.dm_rel_type){
					if(obj.dm_rel_type==2||obj.dm_rel_type==1){ //delivery或者pick up
						colorKey =obj.obj_status+obj.dm_rel_type+child_status;//
					}else if(obj.dm_rel_type == 3){ //both
						colorKey =obj.obj_status+rel_type+child_status;//
					}else if(obj.dm_rel_type==4){//none
						colorKey='000'; //表示none
					}
				}
			}
		}

		//状态颜色
		if(obj.obj_type=="docks"||obj.obj_type=="parking"){
			//color = PACKING_OCCU_COLOR[obj.main_status];
			color = PACKING_OCCU_COLOR[colorKey]?PACKING_OCCU_COLOR[colorKey]:fillColor[obj.obj_type];
		}
		//不可用的门
		if(obj.available_status == "1"&&obj.obj_type=="docks"){
			color = PACKING_OCCU_COLOR["-1"];
			colorSt = strokeColor["unable"];
		}
		var labelContent = "<span style='color: #A129E6'><p align='center'>"+obj.obj_name+"</p>" 
							+ (obj.gate_container_no ? "CTN#"+obj.gate_container_no : (obj.gate_liscense_plate ? obj.gate_liscense_plate: "")) 
							+ "</span>";
		if(jsmap.storageObjPolygon[psId+"_"+name]){
			var polygon = jsmap.storageObjPolygon[psId+"_"+name];
			polygon.setOptions({
				fillColor:color,
				strokeColor:colorSt,
				map : jsmap.getMap()
			});	
			if(polygon.data){
				polygon.data.labelContent = labelContent;
				polygon.data.strokeColor = colorSt;
				polygon.data.type = obj.obj_type;
				polygon.data.id = obj.obj_id;
				polygon.data.areaId = obj.area_id;
				polygon.data.name = obj.obj_name;
				polygon.data.status = obj.obj_status;  //状态不存在时默认为0
				polygon.data.availableStatus = obj.available_status;
				polygon.data.ctn = obj.gate_container_no;
				polygon.data.car = obj.gate_liscense_plate;
				//polygon.data.label.setHtml(labelContent);
			}
		}else{
			if(storageBounds[name]){
				var bounds = storageBounds[name];
				var points = bounds.split(" ");
				var path = [];
				var bound = jsNewLatLngBounds();
				for(var j=0; j<points.length; j++){
					var point = points[j].split(",");
					var latLng = jsNewLatLng(point[1], point[0]);
					path.push(latLng);
					bound.extend(latLng);
				}
				objsBounds.union(bound);
				var polygon = jsNewPolygon(path, colorSt, 2, 1, color, 1);
				
				var position = bound.getCenter();
				/*var labelContent = "<span style='color: #A129E6'><p align='center'>"+obj.obj_name+"</p>" 
									+ (obj.gate_container_no ? "CTN#"+obj.gate_container_no : (obj.gate_liscense_plate ? obj.gate_liscense_plate: "")) 
									+ "</span>";*/
				var label = new LabelOverlay(position,obj.obj_name,"center");
				label.zoomRange = [20,21];
				
				polygon.putData({
					key : name,
					labelContent : labelContent,
					strokeColor : colorSt,
					type : obj.obj_type,
					id : obj.obj_id,
					areaId : obj.area_id,
					name : obj.obj_name,
					status : obj.obj_status,
					availableStatus : obj.available_status,
					ctn : obj.gate_container_no, 
					car : obj.gate_liscense_plate,
					label : label,
					psId : psId,
					path:path,
					state:STORAGE_POSITION_TYPE[obj.obj_type.toUpperCase()] //  区分点击事件点击的是packing 还是localtion
					
				});
				setStorageObjEvent(polygon);
				jsmap.storageObjPolygon[psId+"_"+name] = polygon;
			}
		}
	}
	//jsmap.storageObjsBounds[psId + "_door_parking"] = objsBounds;
	tempData["parkAndDoor"] = null;
}

var loading = {}
function getStorageBoundsAjax(psId,type,layer){
	var _layer = layer ? layer : "";
	var _boudsName = layer ? (layer+"_"+psId) : psId;
	var _psId = psId;
	if(loading[_boudsName]){
		return false;
	}
	$.ajax({
		url:systenFolder+'action/administrator/maps/GetGoogleMapsBoundariesAction.action',
		data:'type='+type+'&psId='+psId+"&layer="+_layer,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
			loading[_boudsName] = true;
	    },
		success:function(data){
	    	if(data){
	    		jsmap.storageBounds[_boudsName] = data;
	    		loading[_boudsName] = false;
	    		if(_layer == ""){
	    			drawArea(_psId);
	    			if(tempData["parkAndDoor"] != null){
	    				drawParkingDocks(_psId,null); 
	    			}
	    			if(tempData["storageLayer"] != null){
	    				jsmap.loadStorageLayer(_psId,null);
	    			}
	    		}else{
	    			if(tempData["storageCatalog"] != null){
	    				drawStorageCatalog(_psId,null);
	    			}
	    			if(tempData["areaData"] != null){
	    				drawLocation(null);
	    			}
	    		}
	    	}
		},
		error:function(){
			loading[_boudsName] = false;
		}
	});
}
//显示库存位置
JSMap.prototype.showStorageCatalogLocation = function(psId,catalogData){
	this.clearStorageCatalog();
	if(!this.storageBounds["loc_"+psId]){
		tempData["storageCatalog"] = catalogData;
		getStorageBoundsAjax(psId,"kml_storage","loc");
	}else{
		drawStorageCatalog(psId,catalogData);
	}
}
function drawStorageCatalog(psId,catalogData){
	if(catalogData == null){
		catalogData = tempData["storageCatalog"];
	}
	var storageBounds = jsmap.storageBounds["loc_"+psId];
	var infoWindows = jsmap.infoWindowCurrent;
	for(var i=0; i<catalogData.length; i++){
		var cata = catalogData[i];
		var pos_name = cata.position;
		pos_name = pos_name.substr(pos_name.indexOf(cata.area_name)+cata.area_name.length);
		var key = ("location["+ cata.area_name+"]_"+cata.position+"_"+pos_name).toLowerCase();
		if(storageBounds[key]){
			var bounds = storageBounds[key];
			var points = bounds.split(" ");
			var path = [];
			var bound = jsNewLatLngBounds();
			for(var j=0; j<points.length; j++){
				var point = points[j].split(",");
				var latLng = jsNewLatLng(point[1], point[0]);
				path.push(latLng);
				bound.extend(latLng);
			}
			var polygon = jsNewPolygon(path, "#795046", 1, 1, "#ff0000", 0.5);
			
			var position = bound.getCenter();
			var html =  "<div style='width: 180px'>"+
						"	<table style='width: 100%'>"+
						"		<tr>"+
						"			<td style='width: 35%' align='right'>位置：</td>"+
						"			<td style='width: 65%'>"+cata.position+"</td>"+
						"		</tr>"+
						"		<tr>"+
						"			<td style='width: 35%' align='right'>物理库存：</td>"+
						"			<td style='width: 65%'>"+cata.physical_quantity_count+"</td>"+
						"		</tr>"+
						"		<tr>"+
						"			<td style='width: 35%' align='right'>理论库存：</td>"+
						"			<td style='width: 65%'>"+cata.theoretical_quantity_count +"</td>"+
						"		</tr>"+
						"	</table>"+
						"</div>";
			var label = new LabelOverlay(position,cata.position,"center");
			label.zoomRange = [22,23];
			polygon.setOptions({zIndex : 5});
			polygon.putData({
				key : key,
				labelContent : "<span style='color: #A129E6'><p align='center'>"+pos_name+"</p></span>",
				strokeColor : "#795046",
				name : pos_name,
				name_full : cata.position,
				label : label,
				psId : psId,
				path:path,
				area : jsmap.storageAreaPolygon[psId+"_area_"+cata.area_name.toLowerCase()].data,
				state : STORAGE_POSITION_TYPE.LOCATION
			});
			setStorageObjEvent(polygon);
			if(infoWindows[key]){
				infoWindows[key].setContent(html);
				infoWindows[key].setMap(null);
			}
			infoWindows[key] = addInfoWindowToAttach(polygon,html,position);
			jsmap.storageCatalogPolygon[key] = polygon;
		}
	}
	tempData["storageCatalog"] = null;
}
//绘制仓库内area   不可见，用于绘制area内locatin
function drawArea(psId){
	var storageBounds = jsmap.storageBounds[psId];
	for(var key in storageBounds){
		if(key.indexOf("area") == 0){
			if(!jsmap.storageAreaPolygon[key]){
				var name = key.substr(key.indexOf("_")+1);
				var bounds = storageBounds[key];
				var points = bounds.split(" ");
				var path = [];
				var bound = jsNewLatLngBounds();
				for(var j=0; j<points.length; j++){
					var point = points[j].split(",");
					var latLng = jsNewLatLng(point[1], point[0]);
					path.push(latLng);
					bound.extend(latLng);
				}
				var polygon = jsNewPolygon(path, "#ff0000", 1, 0, "#ff0000", 0);
				polygon.setMap(null);
				polygon.putData({
					key : key,
					name : name,
					psId : psId,
					path:path,
					state : STORAGE_POSITION_TYPE.AREA,
					draw : false
				});
				setStorageObjEvent(polygon);
				jsmap.storageAreaPolygon[psId+"_"+key] = polygon;
				
			}
		}
	}
}
//绘制area内locatin
function drawLocation(data){
	jsmap.clearLocation();
	if(data == null){
		data = tempData["areaData"];
	}else{
		tempData["areaData"] = data;
	}
	var psId = data.psId;
	var index = "location["+data.name.toLowerCase()+"]";
	var storageBounds = jsmap.storageBounds["loc_"+data.psId];
	if(!storageBounds){
		getStorageBoundsAjax(psId,"kml_storage","loc");
		return;
	}
	tempData["areaData"] = null;
	for(var key in storageBounds){
		if(key.indexOf(index) == 0){
			var i1 = key.indexOf("_");
			var i2 = key.lastIndexOf("_");
			var name = key.substr(i2+1).toUpperCase();
			var name_full = key.substr(i1+1,i2-i1-1).toUpperCase();
			var bounds = storageBounds[key];
			var points = bounds.split(" ");
			var path = [];
			var bound = jsNewLatLngBounds();
			for(var j=0; j<points.length; j++){
				var point = points[j].split(",");
				var latLng = jsNewLatLng(point[1], point[0]);
				path.push(latLng);
				bound.extend(latLng);
			}
			var polygon = jsNewPolygon(path, "#795046", 1, 1, "#ff0000", 0);
			var position = bound.getCenter();
			var labelContent = "<span style='color: #A129E6'><p align='center'>"+name+"</p></span>";
			var label = new LabelOverlay(position,name,"center");
			label.zoomRange = [22,23];
			polygon.putData({
				key : key,
				labelContent : labelContent,
				strokeColor : "#795046",
				name : name,
				name_full : name_full,
				label : label,
				psId:psId,
				path:path,
				area : data,
				state :STORAGE_POSITION_TYPE.LOCATION //  区分点击事件点击的是packing 还是localtion
			});
			setStorageObjEvent(polygon);
			jsmap.storageLocationPolygon[key] = polygon;
		}
	}
	data.draw = true;
}
function reLoadWebcam(psId,camparameterId,flag){
	var key =psId+"_webcam";
	if(camparameterId){
		jsmap.clearWebcam(camparameterId);
	}
	if(!flag){
		var camparameter_data=jsmap.storageBounds[key];
		if(camparameter_data){
			if(camparameter_data.length==1){
				delete jsmap.storageBounds[key];
			}else{
				for(var i=0;i<camparameter_data.length;i++){
					if(camparameter_data[i].id==camparameterId){
						camparameter_data.splice(i,1);
					}
				}
			}
		}
	}
	//jsmap.storageBounds[key]={};
	drawWebcam(psId);
};
function reSetObjectDraggable(type,psId,id){
	jsmap.flag["drawWebcam"]=false;
	jsmap.flag["drawLocation"]=tempData["drawLocation"];
	jsmap.setOptions({draggable:true,
		scrollwheel:true});
		var key="";
		if(type ==6){
		key =psId+"_"+id;
		var marker =  jsmap.storageWebcamMarker[key]?jsmap.storageWebcamMarker[key]:false;
			if(marker){
				if(marker.getDraggable()){
				marker.setDraggable(false);
				return ;
				}
			}
	}else if(type ==7){
		key =psId+"_"+id;
		var printer =  jsmap.storagePrinterMarker[key]?jsmap.storagePrinterMarker[key]:false;
			if(printer){
					if(printer.getDraggable()){
						printer.setDraggable(false);
						return ;
					}
			}
	}else if(type==9){
		key =psId+"_"+id;
		var light =  jsmap.storageLights[key]?jsmap.storageLights[key]:false;
			if(light){
					if(light.getDraggable()){
						light.setDraggable(false);
						return ;
					}
			}	
	}
	
}
//仓库webcam绘制
function drawWebcam(psId){
	
	var key =psId+"_webcam";
	if($.isEmptyObject(jsmap.storageBounds[key])){
		//jsmap.storageBounds[key]=null;
		getStorageLayerAjax(6,psId);//ajax请求获取webcam数据 并存到全局数组storageBounds{}中
	}
	var bounds =jsmap.storageBounds[key]?jsmap.storageBounds[key]:false;
	var obj =jsmap.storageWebcamMarker;
	if(bounds&&bounds.length>0){
    for(var i=0;i<bounds.length;i++){
    	var marker ="";
    	 if(obj && jsmap.storageWebcamMarker[psId+"_"+bounds[i].id]){
    		 continue ;
    	 }
    	var webcamName=bounds[i].name?bounds[i].name:bounds[i].port;//如果摄像头有name 这显示name否则显示port
    	var lng =bounds[i].latlng.split(",")[0];
    	var lat =bounds[i].latlng.split(",")[1];
    	var latlng=new jsNewLatLng(lat, lng);
    	marker =jsNewMarker(latlng, "../imgs/maps/webcam.png");
    	var inner_radius=bounds[i].inner_radius;
    	var outer_radius=bounds[i].outer_radius;
    	var s_degree=bounds[i].s_degree;
    	var e_degree=bounds[i].e_degree;
    	var labelContent = "<span style='color: #A129E6'><p align='center'>"+webcamName+"</p></span>";
       var pathOption={
    		   latlng:latlng,
    		   inner_radius:inner_radius,
    		   outer_radius:outer_radius,
    		   s_degree:s_degree,
    		   e_degree:e_degree,
    		   sides:50
       };       
       var strokeColor=STORAGE_POSITION_COLOR.WEBCAM.strokeColor;
       var fillColor=STORAGE_POSITION_COLOR.WEBCAM.fillColor;
       var sector =  jsNewSector(pathOption, strokeColor, 1, 0.2, fillColor, 0.3);
       sector.setOptions({zIndex:-1});
       var 	label = new LabelOverlay(latlng,webcamName,"bottom");
	     label.zoomRange = [18,21];
		marker.putData({
			key : key,
			labelContent : labelContent,
			name : webcamName,
			sector:sector,
			psId:psId,
			label:label,
			pathOption:pathOption,
			webcamData : bounds[i],
			state :STORAGE_POSITION_TYPE.WEBCAM 
		});
		setStorageObjEvent(marker);
        jsmap.storageWebcamMarker[psId+"_"+bounds[i].id]=marker;
    	}
	}
}

//绘制打印机图层
function drawPrinter(psId){
	var key =psId+"_"+"printer";
	if($.isEmptyObject(jsmap.storageBounds[key])){
		//jsmap.storageBounds[key]=null;
		getStorageLayerAjax(7,psId);//ajax请求获取printer数据 并存到全局数组storageBounds{}中
	}
	var bounds =jsmap.storageBounds[key]?jsmap.storageBounds[key]:false;
	var obj =jsmap.storagePrinterMarker;
	if(bounds&&!$.isEmptyObject(bounds)){
    for(var i=0;i<bounds.length;i++){
		 if(obj && jsmap.storagePrinterMarker[psId+"_"+bounds[i].p_id]){
    		 continue ;
    	 }
		var point=bounds[i].latlng;
		var latlng =strToLatlng(point);
		var name=bounds[i].name;
		var servers =bounds[i].servers?bounds[i].servers:"";
		var type=parseInt(bounds[i].type);
		var type_name="";
		var color ='';
		var servers_name =bounds[i].servers_name?bounds[i].servers_name:"";
		switch(type){
		case 0:
			type_name="label";
			color ='C6EF8C';
			break;
		case 1:
			type_name="letter/A4";
			color ='2AB5FA';
			break;
		}
		var map_icon="";
		if(servers){
			map_icon = {
					url: 'http://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=computer|bb|'+servers_name+'+'+name+'+'+type_name+'|'+color+'|000000',
					anchor: new google.maps.Point(0, 42)
			};
		}else {
			map_icon = {
					url: 'http://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=glyphish_photos|bb|'+name+'+'+type_name+'|'+color+'|000000',
					anchor: new google.maps.Point(0, 42)
			};
		}
		var printer=jsNewMarker(latlng,	 map_icon);
		 printer.putData({
				 printerData:bounds[i],
				 state :STORAGE_POSITION_TYPE.PRINTER ,
				 name:name,
				 key:key,
				 psId:psId
		 });
			MapsEvent.addListener(printer,"rightclick",function(e){
			dragStorageLayerObject(this.data.state,this.data.psId,this.data.printerData.p_id);	
			openPinterWindow(this.data.printerData);
			});
		 jsmap.storagePrinterMarker[psId+"_"+bounds[i].p_id] =printer;
	}
	}
}
function reLoadPrinter(psId,printerId,flag){
	var key =psId+"_printer";
	if(printerId){
		jsmap.clearPrinter(printerId);
	}
	
	if(!flag){
		var printer_data=jsmap.storageBounds[key];
		if(printer_data){
			if(printer_data.length==1){
				delete jsmap.storageBounds[key];
			}else if(printer_data.length>1){
				for(var i=0;i<printer_data.length;i++){
					if(printer_data[i].p_id==printerId){
						printer_data.splice(i,1);
					}
				}
			}
		}
	}
	drawPrinter(psId);
}
//设置图层对象为可拖拽
function dragStorageLayerObject(type,psId,id){
		jsmap.flag["drawLocation"]=false;
		jsmap.clearLocation();
	if(type==6){
		jsmap.setOptions({
			draggable:false,
			scrollwheel:false
			});
		var key =psId+"_"+id
		var marker=jsmap.storageWebcamMarker[key];
		var map =marker.getMap();
			if(!marker.getDraggable()){
			marker.setDraggable(true);
			var sector=marker.data.sector;
			var zindex=marker.getZIndex();
			sector.setOptions({zIndex:zindex+3});
			MapsEvent.addListener(marker,"dragstart",function(e){
			});
			MapsEvent.addListener(marker,"drag",function(e){
				var position =marker.getPosition();
				var 	pathOption 	=marker.data.pathOption;
						pathOption.latlng=position;
				var paths = computeSectorPath(pathOption);//计算扇形的轨迹。
						sector.setPaths(paths);
						marker.data.label.setPosition(position);
						var  lat =position.lat();
						var  lng =position.lng();
							if($.artDialog({id: 'webcam'})){
								$.artDialog({id: 'webcam'}).iframe.contentWindow.convertCoordinateAjax(psId,lat,lng);
							}
			});
			MapsEvent.addListener(marker,"dragend",function(e){
			});
			sectorEdit(marker);
			}
	}
	if(type ==7){
		//jsmap.setOptions({draggable:false});
		var key =psId+"_"+id;
		var printer=jsmap.storagePrinterMarker[key];
		var map =printer.getMap();
			if(!printer.getDraggable()){
				printer.setDraggable(true);
			MapsEvent.addListener(printer,"dragstart",function(e){
			});
			MapsEvent.addListener(printer,"drag",function(e){
				var position =printer.getPosition();
				var lat =position.lat();
				var lng =position.lng();
				if($.artDialog({id: 'printer'})){
				$.artDialog({id: 'printer'}).iframe.contentWindow.convertCoordinateAjax(psId,lat,lng);
				}
			});
			MapsEvent.addListener(printer,"dragend",function(e){
			});
		    }
	}
	if(type ==9){
		var key =psId+"_"+id;
		var light=jsmap.storageLights[key];
		var map =light.getMap();
		if(!light.getDraggable()){
			light.setDraggable(true);
			MapsEvent.addListener(light,"dragstart",function(e){
			});
			MapsEvent.addListener(light,"drag",function(e){
				var position =light.getPosition();
				var lat =position.lat();
				var lng =position.lng();
				if($.artDialog({id: 'light'})){
					$.artDialog({id: 'light'}).iframe.contentWindow.convertCoordinateAjax(psId,lat,lng);
				}
			});
			MapsEvent.addListener(light,"dragend",function(e){
			});
		}
	}
}
function createLayerDemo (type,psId ,lat,lng){
		if(type==6||type==7||type==9){
			tempData["drawLocation"]=jsmap.flag["drawLocation"];
			jsmap.flag["drawLocation"]=false;
			jsmap.clearLocation();
			jsmap.setOptions({draggable:false,scrollwheel:false});
		}
	       var latlng=new jsNewLatLng(lat,lng);
     if(type==6){
    	    	var marker ="";
    	    	var webcamName="demo";//如果摄像头有name 这显示name否则显示port
    	    	marker =jsNewMarker(latlng, "../imgs/maps/webcam.png");
    	    	marker.setDraggable(true);
    	    	var inner_radius=0;
    	    	var outer_radius=100;
    	    	var s_degree=0;
    	    	var e_degree=150;
    	    	var labelContent = "<span style='color: #A129E6'><p align='center'>"+webcamName+"</p></span>";
    	       var pathOption={
    	    		   latlng:latlng,
    	    		   inner_radius:inner_radius,
    	    		   outer_radius:outer_radius,
    	    		   s_degree:s_degree,
    	    		   e_degree:e_degree,
    	    		   sides:50
    	       };       
    	       var strokeColor=STORAGE_POSITION_COLOR.WEBCAM.strokeColor;
    	       var fillColor=STORAGE_POSITION_COLOR.WEBCAM.fillColor;
    	       var sector =  jsNewSector(pathOption, strokeColor, 1, 0.7, fillColor, 0.7);
    	       sector.setOptions({zIndex:-1});
    	       var 	label = new LabelOverlay(latlng,webcamName,"bottom");
    		     label.zoomRange = [18,21];
    			marker.putData({
    				labelContent : labelContent,
    				name : webcamName,
    				sector:sector,
    				psId:psId,
    				label:label,
    				pathOption:pathOption,
    				state :STORAGE_POSITION_TYPE.WEBCAM 
    			});
    		//	setStorageObjEvent(marker);
    			MapsEvent.addListener(marker,"drag",function(e){
    				var position =marker.getPosition();
    						pathOption.latlng=position;
    				var paths = computeSectorPath(pathOption);//计算扇形的轨迹。
    						sector.setPaths(paths);
    						marker.data.label.setPosition(position);
    						var  lat =position.lat();
    						var  lng =position.lng();
    						$.artDialog({id: 'webcam'}).iframe.contentWindow.convertCoordinateAjax(psId,lat,lng);
    			});
    			 sectorEdit(marker);
    			 jsmap.storageWebcamMarker[psId+"_demo" ]=marker;
     }
	if(type==7){
			var map_icon = {
					url: 'http://chart.googleapis.com/chart?chst=d_bubble_icon_text_small&chld=glyphish_photos|bb|Printer|C6EF8C|000000',
					anchor: new google.maps.Point(0, 42)
				};
			var printer=jsNewMarker(latlng,	 map_icon);
			printer.setDraggable(true);
			 printer.putData({
					 state :STORAGE_POSITION_TYPE.PRINTER ,
					 name:name,
					 psId:psId
			 });
			/*	MapsEvent.addListener(printer,"rightclick",function(e){
				dragStorageLayerObject(this.data.state,this.data.psId,this.data.printerData.p_id);	
				openPinterWindow(this.data.printerData);
				});
				*/
				MapsEvent.addListener(printer,"drag",function(e){
					var position =printer.getPosition();
					var lat =position.lat();
					var lng =position.lng();
					$.artDialog({id: 'printer'}).iframe.contentWindow.convertCoordinateAjax(psId,lat,lng);
				});
			 jsmap.storagePrinterMarker[psId+"_demo"] =printer;
	}
	if(type==9){
		var map_icon = "../imgs/maps/light_off.png";
		var light=jsNewMarker(latlng,map_icon);
		light.setDraggable(true);
		light.putData({
			state :STORAGE_POSITION_TYPE.LIGHT ,
			name:name,
			psId:psId
		});
		/*
		 * 	MapsEvent.addListener(light,"rightclick",function(e){
			dragStorageLayerObject(this.data.state,this.data.psId,this.data.dbData.id);	
			modifyLight(this.data.dbData);
		});
		*/
		MapsEvent.addListener(light,"drag",function(e){
			var position =light.getPosition();
			var lat =position.lat();
			var lng =position.lng();
			$.artDialog({id: 'light'}).iframe.contentWindow.convertCoordinateAjax(psId,lat,lng);
		});
		jsmap.storageLights[psId+"_demo"] =light;
	}
		if( type=='x'){
				var point=convertCoordinate(psId,lat,lng);
				var x=point.split(",")[0];
				var y=point.split(",")[1];
				var heigth=10;
				var width=100;
				var initAngle=0;
				var path_1=rectanglePaths(psId,x,y,heigth,width,initAngle);
				drawDraggableLayer(path_1,'layer',psId,initAngle);
		}
}
function dragStorageLayer(poly,dialogId,initAngle){
	var path=poly.getPath();
	var psId=poly.data.psId;
	drawDraggableLayer(path,dialogId,psId,initAngle);
}

function layerSetMap(key){
	var obj=jsmap.storageProvenLayer[key];
	obj.setVisible(true);
}
function drawDraggableLayer(path,dialogId,psId,initAngle){
				var rectangle= new  jsNewPolygon(path, "#7EE0F9", 1, 0.5,"#7EE0F9",0.5);
					var circles=[];
					map_icon = {
							url: "../imgs/maps/dragCircle.png",
							anchor: new google.maps.Point(5, 5)
					};
					var path_2=rectangle.getPath().getArray();
					for(var i=0;i<path_2.length-1;i++){
						var circle=jsNewMarker(	path_2[i],map_icon);
						circle.setDraggable(true);
						circle.setTitle(i+"A");
						circles.push(circle);
					}
				 	rectangle.putData({
		        		paths:rectangle.getPath(),
		        		circles:circles
					});
					var circle_0=circles[0];
					var circle_1=circles[1];
					var circle_2=circles[2];
					var circle_3=circles[3];
					var  _angle= computeHeading(circle_0.getPosition(),circle_1.getPosition());
					var  oc= computeHeading(circle_0.getPosition(),circle_3.getPosition());
					var pointLatLng1;
					var changePath;
				MapsEvent.addListener(circle_0,"drag",function(e){
					var circle1=circles[3];
					var circle2=circles[1];
					var circle3=circles[2];
					var latlng1=	circle1.getPosition();
					var latlng2=	circle2.getPosition();
					var latlng3=	circle3.getPosition();
					var x1=jsmap.fromLatLngToContainerPixel(latlng3.lat(),latlng3.lng()).x;
					var y1=jsmap.fromLatLngToContainerPixel(latlng3.lat(),latlng3.lng()).y;
					var x2=jsmap.fromLatLngToContainerPixel(latlng1.lat(),latlng1.lng()).x;
					var y2=jsmap.fromLatLngToContainerPixel(latlng1.lat(),latlng1.lng()).y;
					var _latlng=	circle_0.getPosition();
					var m=jsmap.fromLatLngToContainerPixel(_latlng.lat(),_latlng.lng()).x;
					var  n=jsmap.fromLatLngToContainerPixel(_latlng.lat(),_latlng.lng()).y;
					var  point1=calculatePointToLine(x1,y1,x2,y2,m,n);
					var	path1 =jsmap.fromContainerPixelToLatLng(parseFloat(point1.split(",")[0]),parseFloat(point1.split(",")[1]));
					var	path2 =jsmap.fromContainerPixelToLatLng(parseFloat(point1.split(",")[2]),parseFloat(point1.split(",")[3]));
					var _oa=	computeHeading(_latlng,path2);
					var _oc=	computeHeading(_latlng,path1);
					var path =[];
					circle1.setPosition(path1);
					circle2.setPosition(path2);
					//顺序不能变
					path.push(_latlng);
					path.push(path2);
					path.push(latlng3);''
					path.push(path1);
					path.push(_latlng);
					rectangle.setOptions({path:path});
					var height=	(computeDistanceBetween(_latlng,path2)*3.28083).toFixed(2);
					var width=	(computeDistanceBetween(_latlng,path1)*3.28083).toFixed(2);
					if(_oa-_angle>170||_oa-_angle<-170){
						height=height*(-1);
					}
					if(_oc-oc>170||_oc-oc<-170){
						width=width*(-1);
					}
				$.artDialog({id: dialogId}).iframe.contentWindow.fillValue(width,height);
				$.artDialog({id: dialogId}).iframe.contentWindow.convertCoordinateAjax(psId,_latlng.lat(),_latlng.lng());
			});
			MapsEvent.addListener(circle_1,"drag",function(e){
				var circle1=circles[0];
				var circle2=circles[2];
				var circle3=circles[3];
				var latlng1=	circle1.getPosition();
				var latlng2=	circle2.getPosition();
				var latlng3=	circle3.getPosition();
				var _latlng=	circle_1.getPosition();
				var x1=jsmap.fromLatLngToContainerPixel(latlng3.lat(),latlng3.lng()).x;
				var y1=jsmap.fromLatLngToContainerPixel(latlng3.lat(),latlng3.lng()).y;
				var x2=jsmap.fromLatLngToContainerPixel(latlng1.lat(),latlng1.lng()).x;
				var y2=jsmap.fromLatLngToContainerPixel(latlng1.lat(),latlng1.lng()).y;
				var m=jsmap.fromLatLngToContainerPixel(_latlng.lat(),_latlng.lng()).x;
				var n=jsmap.fromLatLngToContainerPixel(_latlng.lat(),_latlng.lng()).y;
				var point1=calculatePointToLine(x1,y1,x2,y2,m,n);
				var	path1 =jsmap.fromContainerPixelToLatLng(parseFloat(point1.split(",")[0]),parseFloat(point1.split(",")[1]));
				var	path2 =jsmap.fromContainerPixelToLatLng(parseFloat(point1.split(",")[2]),parseFloat(point1.split(",")[3]));
				var _oa=	computeHeading(path1,_latlng);
				var _oc=	computeHeading(_latlng,path2);
					circle1.setPosition(path1);
					circle2.setPosition(path2);
					var path =[];
					path.push(path1);
					path.push(_latlng);
					path.push(path2);
					path.push(latlng3);
					path.push(path1);
					rectangle.setOptions({path:path});
					var height=(computeDistanceBetween(latlng1,_latlng)*3.28083).toFixed(2)	;//x
					var width=	(computeDistanceBetween(latlng1,latlng3)*3.28083).toFixed(2);//y
					if(_oa-_angle>170||_oa-_angle<-170){
						height=height*(-1);
					}
					if(_oc-oc>170||_oc-oc<-170){
						width=width*(-1);
					}
					$.artDialog({id: dialogId}).iframe.contentWindow.convertCoordinateAjax(psId,path1.lat(),path1.lng());
					$.artDialog({id: dialogId}).iframe.contentWindow.fillValue(width,height);
			});
			MapsEvent.addListener(circle_2,"drag",function(e){
				var circle1=circles[1];
				var circle2=circles[3];
				var circle3=circles[0];
				var latlng1=	circle1.getPosition();
				var latlng2=	circle2.getPosition();
				var latlng3=	circle3.getPosition();
				var x1=jsmap.fromLatLngToContainerPixel(latlng3.lat(),latlng3.lng()).x;
				var y1=jsmap.fromLatLngToContainerPixel(latlng3.lat(),latlng3.lng()).y;
				var x2=jsmap.fromLatLngToContainerPixel(latlng1.lat(),latlng1.lng()).x;
				var y2=jsmap.fromLatLngToContainerPixel(latlng1.lat(),latlng1.lng()).y;
				var _latlng=	circle_2.getPosition();
				var m=jsmap.fromLatLngToContainerPixel(_latlng.lat(),_latlng.lng()).x;
				var n=jsmap.fromLatLngToContainerPixel(_latlng.lat(),_latlng.lng()).y;
				var point1=calculatePointToLine(x1,y1,x2,y2,m,n);
				var	path1 =jsmap.fromContainerPixelToLatLng(parseFloat(point1.split(",")[0]),parseFloat(point1.split(",")[1]));
				var	path2 =jsmap.fromContainerPixelToLatLng(parseFloat(point1.split(",")[2]),parseFloat(point1.split(",")[3]));
				var _oa=computeHeading(latlng3,path1);
				var _oc=computeHeading(latlng3,path2);
				circle1.setPosition(path1);
				circle2.setPosition(path2);
				var path =[];
				path.push(latlng3);
				path.push(path1);
				path.push(_latlng);
				path.push(path2);
				path.push(latlng3);
				rectangle.setOptions({path:path});
				var height=(computeDistanceBetween(latlng3,path1)*3.28083).toFixed(2);
				var width=	(computeDistanceBetween(latlng3,path2)*3.28083).toFixed(2);
				if(_oa-_angle>170||_oa-_angle<-170){
					height=height*(-1);
				}
				if(_oc-oc>170||_oc-oc<-170){
					width=width*(-1);
				}
				$.artDialog({id: dialogId}).iframe.contentWindow.convertCoordinateAjax(psId,latlng3.lat(),latlng3.lng());
				$.artDialog({id: dialogId}).iframe.contentWindow.fillValue(width,height);
			});
			MapsEvent.addListener(circle_3,"drag",function(e){
				var circle1=circles[2];
				var circle2=circles[0];
				var circle3=circles[1];
				var latlng1=	circle1.getPosition();
				var latlng2=	circle2.getPosition();
				var latlng3=	circle3.getPosition();
				var x1=jsmap.fromLatLngToContainerPixel(latlng3.lat(),latlng3.lng()).x;
				var y1=jsmap.fromLatLngToContainerPixel(latlng3.lat(),latlng3.lng()).y;
				var x2=jsmap.fromLatLngToContainerPixel(latlng1.lat(),latlng1.lng()).x;
				var y2=jsmap.fromLatLngToContainerPixel(latlng1.lat(),latlng1.lng()).y;
				var _latlng=	circle_3.getPosition();
				var m=jsmap.fromLatLngToContainerPixel(_latlng.lat(),_latlng.lng()).x;
				var  n=jsmap.fromLatLngToContainerPixel(_latlng.lat(),_latlng.lng()).y;
				var  point1=calculatePointToLine(x1,y1,x2,y2,m,n);
				var	path1 =jsmap.fromContainerPixelToLatLng(parseFloat(point1.split(",")[0]),parseFloat(point1.split(",")[1]));
				var	path2 =jsmap.fromContainerPixelToLatLng(parseFloat(point1.split(",")[2]),parseFloat(point1.split(",")[3]));
				var _oa=	computeHeading(path2,latlng3);
				var _oc=	computeHeading(path2,_latlng);
				circle1.setPosition(path1);
				circle2.setPosition(path2);
				var path_3 =[];
				path_3.push(latlng2);
				path_3.push(latlng3);
				path_3.push(path1);
				path_3.push(_latlng);
				path_3.push(latlng2);
				rectangle.setOptions({path:path_3});
				var height=	(computeDistanceBetween(latlng2,latlng3)*3.28083).toFixed(2);
				var width=	(computeDistanceBetween(latlng2,_latlng)*3.28083).toFixed(2);
				if(_oa-_angle>170||_oa-_angle<-170){
					height=height*(-1);
				}
				if(_oc-oc>170||_oc-oc<-170){
					width=width*(-1);
				}
				$.artDialog({id: dialogId}).iframe.contentWindow.convertCoordinateAjax(psId,path2.lat(),path2.lng());
				$.artDialog({id: dialogId}).iframe.contentWindow.fillValue(width,height);
				});
		
			var point=rectangle.getCenter();
        setStorageObjEvent(rectangle);
      	MapsEvent.addListener(rectangle,"mousedown",function(e){
  			jsmap.setOptions({draggable:false});
  			var centre;
  			if(pointLatLng1){
  				centre=pointLatLng1;
  				pointLatLng1=undefined;
  			}else{
  				centre=circle_0.getPosition();
  			}
      		var mousePoint=e.latLng;
      		var path;
      		if(changePath){
      			if(changePath.length>0){
          			for(var i=0;i<circles.length;i++){
          				circles[i].setPosition(changePath[i]);
          			}
          			rectangle.setOptions({path:changePath});
          			changePath=[];
          		}
      		}
      		
      		path=rectangle.getPath().getArray();
      		var lastMousemoveLatLng;
				MapsEvent.addDomListener(rectangle,"mousemove",function(e){
				  	var event=null;
			    	for(var v in e){
			    		if(e[v] && e[v].type=="mousemove"){
			    			event = e[v];
			    		}
			    	}
      			var path_4=[];
							if(1==event.which&&event.altKey){
							lastMousemoveLatLng=e.latLng;
			  				jsmap.setOptions({draggable:false});
							var d1 = computeHeading(centre,mousePoint);
							var d2= computeHeading(centre,e.latLng);
							var _d=d1-d2;
							for(var i=0;i<path.length;i++){
								var distance = computeDistanceBetween(centre,path[i]);
								var d3=computeHeading(centre,path[i]);
								var d=d3-_d;
								if(i!=4||i!=1){
								var _path= computeOffsetDestination(centre,distance,d);
								}else{
									path_4.push(centre);
								}
								path_4.push(_path);
								if(i<4){
									circles[i].setPosition(_path);
								}
							}
							var  _angle1= computeHeading(circles[0].getPosition(),circles[1].getPosition());
							var changeAngle=_angle-_angle1;
							var endAngle=(parseFloat(changeAngle)+parseFloat(initAngle)).toFixed(2);
							endAngle=endAngle%360;
							if(endAngle>180){
								endAngle-=360;
							}
							if(endAngle<-180){
								endAngle+=360;
							}
							$.artDialog({id:dialogId}).iframe.contentWindow.fillAngle(endAngle);		
							rectangle.setOptions({path:path_4});
			  			}
					if(1==event.which&&!event.altKey){
						if(lastMousemoveLatLng){
							mousePoint=lastMousemoveLatLng;
							lastMousemoveLatLng=undefined;
							path=rectangle.getPath().getArray();
						}
      				var angle=computeHeading(mousePoint,e.latLng);
      				var _distance = computeDistanceBetween(mousePoint,e.latLng);
      				for(var i=0;i<path.length;i++){
      					var _path=computeOffsetDestination(path[i],_distance,angle);
      					path_4.push(_path);
      					if(i<4){
    						circles[i].setPosition(_path);
    					}
      					if(i==0){
							$.artDialog({id: dialogId}).iframe.contentWindow.convertCoordinateAjax(psId,_path.lat(),_path.lng());
      					}
      				}
      				rectangle.setOptions({path:path_4});
      			}
      		});
				
      	});
      	MapsEvent.addListener(rectangle,"mouseup",function(e){
      		jsmap.setOptions({draggable:true});
     	   MapsEvent.clearListeners(this, "mousemove");
      		
      	});
      	MapsEvent.addListener(rectangle,"mouseout",function(e){
      		jsmap.setOptions({draggable:true});
     	   MapsEvent.clearListeners(this, "mousemove");
      	});
			jsmap.storageDemoLayer["drag"]=rectangle;
}
JSMap.prototype.clearDemoLayer = function(){
	var objs = this.storageDemoLayer;
	for(var key in objs){
		if(objs[key]){
			objs[key].setMap(null);
			if(key=="drag"){
			var circles =objs[key].data.circles;
				for(var i in circles){
					circles[i].setMap(null);
					circles[i]=null;
				}
				circles={};
				objs[key] = null;
			}
		}
	}
	this.storageDemoLayer = {};
}
/**
 *  坐标计算经纬度
 * @param psId
 * @param x
 * @param y
 * @returns {String}
 */

function calculatePointToLatlng(psId,x,y){
			var result="";
			$.ajax({
				url:systenFolder+'action/administrator/gis/calculatePointToLatlng.action',
				data:{"psId":psId,"x":x,"y":y },
				dataType:'json',
				type:'post',
				async:false, 
				beforeSend:function(request){
			    },
				success:function(data){
					if(data&&data.flag=="true"){
						result=strToLatlng(data.data);
					}
				},
			    error:function (){
			    	showMessage("System error","error");
			    }
		});
			return result;
}
/*
  function getLightDataAjax(psId){
	var result="";
	$.ajax({
		url:systenFolder+'action/administrator/gis/getLightData.action',
		data:{"ps_id":psId},
		dataType:'json',
		type:'post',
		async:false, 
		beforeSend:function(request){
		},
		success:function(data){
			if(data&&data.flag=="true"){
				result=data.data;
			}
		},
		error:function (){
			showMessage("System error","error");
		}
	});
	return result;
}
*/
/**
 * 经纬度计算坐标
 * @param psId
 * @param lat
 * @param lng
 * @returns {String}
 */
function convertCoordinate(psId,lat,lng){
	var result="";
	$.ajax({
		url:systenFolder+'action/administrator/gis/convertCoordinate.action',
		data:'psId='+psId+'&lat='+lat+'&lng='+lng,
		dataType:'json',
		type:'post',
		async:false, 
		beforeSend:function(request){
	    },
		success:function(data){
	    	if(data&&data.flag=="true"){
	        result =data.x+","+data.y;
	    	}
		},
		error:function(){
		}
	});
	return  result;
}
/**
 *  仓库坐标计算经纬度
 * @param psId
 * @param x
 * @param y
 * @param heigth
 * @param width
 * @param angle
 * @returns {Array}
 */
function rectanglePaths(psId,x,y,heigth,width,angle){
	var path=[];
	$.ajax({
		url:systenFolder+'action/administrator/gis/RectanglePaths.action',
		data:{"psId":psId,"x":x,"y":y,"xPosition":heigth,"yPosition":width,"angle":angle },
		dataType:'json',
		type:'post',
		async:false, 
		beforeSend:function(request){
	    },
		success:function(data){
			if(data&&data.flag=="true"){
				var points=data.data.split(" ");
				for(var j=0; j<points.length; j++){
					var latLng=strToLatlng(points[j]);
					path.push(latLng);
				}
			}else{
					path=[];
					showMessage("Calculate paths error","error");
			}
		},
	    error:function (){
	    	showMessage("System error","error");
	    }
});
	return path;
}
/**
 * 计算点到直线最近的点的坐标
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 * @param m
 * @param n
 * @returns {String}
 */
function calculatePointToLine(_a,_b,_c,_d,_m,_n){
	var a=parseFloat(_a);
	var b=parseFloat(_b);
	var c=parseFloat(_c);
	var d=parseFloat(_d);
	var m=parseFloat(_m);
	var n=parseFloat(_n);
	if(b-d==0){
		return m+","+b+","+a+","+n;
	}
	if(a-c==0){
		return a+","+n+","+m+","+b;
	}
		var n1=n+(a-c)*m/(b-d);
		var k=(b-d)/(a-c);
		var n2=(c*b-a*d)/(c-a);
		var x1=k*(n1-n2)/(k*k+1);
		var y1=(k*k*n1+n2)/(k*k+1);
		var n3=n-(b-d)*m/(a-c);
		var n4=b+(a-c)*a/(b-d);
		var x2=-(n3-n4)*k/(k*k+1);
		var y2=(n4*k*k+n3)/(k*k+1);
	return x1+","+y1+","+x2+","+y2;
}
function sectorEdit(marker){
	jsmap.flag["drawWebcam"] = true;
	   var sector=marker.data.sector;
	   var sL="";
	       var eL="";
	       var seL="";
	       var sPoint="";
	       var e_degree="";
	       var s_degree="";
	       MapsEvent.addListener(sector,"mouseover",function(event){
	       MapsEvent.addListener(sector,"mousedown",function(event){
	    	   			sPoint =event.latLng;
		                sL=computeDistanceBetween(marker.getPosition(),sPoint);
		                 e_degree=parseInt(marker.data.pathOption.e_degree);
		                 s_degree=parseInt(marker.data.pathOption.s_degree);
					    MapsEvent.addListener(sector,"mousemove",function(event){
					    	var e=null;
					    	for(var v in event){
					    		if(event[v] && event[v].type=="mousemove"){
					    			e = event[v];
					    		}
					    	}
					    				var d1 = computeHeading(marker.getPosition(),sPoint);
					    				var d2 = computeHeading(marker.getPosition(),event.latLng);
					    			   var angle = (d2 - d1)%360;
			              	   if("1"==e.which&&!e.altKey){
					              		   e_degree += parseInt(angle);
					              		   s_degree += parseInt(angle);
					              	   }
			             	   if("1"==e.which&&e.altKey){
					             			e_degree += parseInt(angle);
					             	   }
					             	 e_degree%=360;
				              		 s_degree%=360;
				              		 marker.data.pathOption.e_degree=e_degree;
				              		 marker.data. pathOption.s_degree=s_degree;
				              		 sector.setPaths(computeSectorPath( marker.data. pathOption));
					              		 if( $.artDialog({id: 'webcam'})){
					              			 $.artDialog({id: 'webcam'}).iframe.contentWindow.fillAngle( marker.data.pathOption.s_degree, marker.data.pathOption.e_degree); 
					              		 }
					              	 sPoint =event.latLng;
				       });
	       });
	       var scrollFunc = function(ev){
	    	   if(!jsmap.flag["drawWebcam"]){
	    		   return;
	    	   }
	    	   ev=ev || window.event;
	    		if (ev&&ev.preventDefault ){  
	    			ev.preventDefault();  
	    			} 
	    	   var roll = 0;
	    	   if(ev.wheelDelta){//IE/Opera/Chrome
	    		   roll=ev.wheelDelta;
	    	   }else if(ev.detail){//Firefox
	    		   roll=ev.detail;
	    	   }
	    	   var  inner_radius =parseInt(marker.data.pathOption.inner_radius);
	    	   var  outer_radius =parseInt(marker.data.pathOption.outer_radius);
	    	   if(!ev.altKey){
		    	   if(roll>0){
		    		   outer_radius+=1;
		    	   }else{
		    		   outer_radius-=1;
		    		   if(outer_radius<0){
		    			   outer_radius=0;
		    		   }
		    		   if(outer_radius<=inner_radius){
		    			   outer_radius=inner_radius;
		    		   }
		    	   }
	    	   }else{
		       	   if(roll>0){
		    		   inner_radius+=1;
		    		   if(inner_radius>=outer_radius){
		    			  inner_radius=outer_radius;
		    		   }
		    	   }else{
		    		   inner_radius-=1;
		    		   if(inner_radius<0){
		    			   inner_radius=0;
		    		   }
		    	   }
	       }
	    	   marker.data. pathOption.inner_radius=inner_radius;
	    	   marker.data. pathOption.outer_radius=outer_radius;
	    	   sector.setPaths(computeSectorPath( marker.data. pathOption));
		       if($.artDialog({id: 'webcam'})){
		    	   $.artDialog({id: 'webcam'}).iframe.contentWindow.fillRadius( marker.data.pathOption.inner_radius, marker.data.pathOption.outer_radius); 
		       }
	       }
	       if(document.addEventListener){
	    	   document.addEventListener('DOMMouseScroll',scrollFunc,false);
	       }else{
	       }
	       document.onmousewheel=scrollFunc;//IE/Opera/Chrome/Safari
	       MapsEvent.addListener(sector,"mouseup",function(event){
	    	   MapsEvent.clearListeners(this, "mousemove");
	       });
	       });
	       MapsEvent.addListener(sector,"mouseout",function(event){
	    	   MapsEvent.clearListeners(this, "mousemove");
	       });
}

function autoCreatLocaiton(psId,x,y,xLenght,yLength,angle,h,v,loc_name,h_interval,v_interval){
	var obj=jsmap.storageDemoLayer;
	var flag =false;
		for(var key in obj){
			if(key.indexOf("autoLocal")>-1&&!$.isEmptyObject(obj[key])){
				flag=true;
				return;
			}
		}
	var k=3.28083;//英寸转换成米的系数
	var rectangle= obj["drag"];
		rectangle.setVisible(false);//隐藏原有的矩形
		var 	circles=rectangle.data.circles;//隐藏原有的矩形
		for (var key in circles){
			circles[key].setVisible(false);
		}
		if($.isEmptyObject(obj["drag_line"])){
		var strokeColor ="#000000" ;
		var strokeWeight =1 ;
		var strokeOpacity =1 ;
		var polyline = jsNewPolyline(rectangle.getPath(), strokeColor, strokeWeight, strokeOpacity);
		obj["drag_line"]=polyline;
		}
		var paths=rectanglePaths(psId,x,y,xLenght,yLength,angle);
		var point_0=jsmap.fromLatLngToContainerPixel(paths[0].lat(),paths[0].lng());
		var point_1=jsmap.fromLatLngToContainerPixel(paths[1].lat(),paths[1].lng());
		var point_3=jsmap.fromLatLngToContainerPixel(paths[3].lat(),paths[3].lng());
		var width =computeDistanceBetween(paths[0],paths[1]);
		var h_angle=computeHeading(paths[0],paths[1]);
		var height =computeDistanceBetween(paths[0],paths[3]);
		var z_angle=computeHeading(paths[0],paths[3]);
		var _path = new Array();
		var h=parseInt(h);
		var v=parseInt(v);
		var h_interval=parseFloat(h_interval);
		var v_interval=parseFloat(v_interval);
		var hl=(width+h_interval)/h-h_interval ;
		var vl=(height+v_interval)/v-v_interval;
		//计算存在误差 横向给定误差系数0.99 纵向给定误差系数0.985
		for(var i=0;i<2*h;i+=2){
			_path[i]=new Array(); 
			_path[i+1]=new Array(); 
			var point1 =computeOffsetDestination(paths[0],((width+h_interval)/h)*i*0.99/2,h_angle);
			var point2 =computeOffsetDestination(paths[0],((width+h_interval)/h)*i*0.99/2+hl,h_angle);
			for(var j=0;j<2*v;j+=2){
				var point3 =computeOffsetDestination(point1,((height+v_interval)/v)*j*0.985/2,z_angle);
				var point4 =computeOffsetDestination(point1,((height+v_interval)/v)*j*0.985/2+vl,z_angle);
				var point5 =computeOffsetDestination(point2,((height+v_interval)/v)*j*0.985/2,z_angle);
				var point6 =computeOffsetDestination(point2,((height+v_interval)/v)*j*0.985/2+vl,z_angle);
				_path[i][j]=point3;
				_path[i][j+1]=point4;
				_path[i+1][j]=point5;
				_path[i+1][j+1]=point6;
			}
			}
		var _paths =[];
		for (var i =0;i<_path.length;i+=2){
			for (var j =0;j<_path[i].length;j+=2){
				var path=[];
				var path0=_path[i][j];
				var path1=_path[i+1][j];
				var path2=_path[i+1][j+1];
				var path3=_path[i][j+1];
				path.push(path0);
				path.push(path1);
				path.push(path2);
				path.push(path3);
				path.push(path0);
				_paths.push(path);
			}
		}
		if(_paths.length>0){
			var strokeColor=STORAGE_POSITION_COLOR.LOCATION.strokeColor;
			var fillColor=STORAGE_POSITION_COLOR.LOCATION.fillColor;
			for(var i=0;i<_paths.length;i++){
				var polygon = jsNewPolygon(_paths[i], strokeColor, 1, 0.5,fillColor,0.5)	;
				var position = polygon.getCenter();
				var pos_name=loc_name+i;
				var label = new LabelOverlay(position,pos_name,"center");
				var key ="autoLocal"+i;
				label.zoomRange = [22,23];
				polygon.setOptions({zIndex : 5});
				polygon.putData({
					labelContent : "<span style='color: #A129E6'><p align='center'>"+pos_name+"</p></span>",
					strokeColor : "#795046",
					name : pos_name,
					label : label,
					height:hl*k.toFixed(2),
					width:vl*k.toFixed(2),
					point:_paths[i][0],
					psId : psId,
					path:_paths[i],
					angle:angle,
					area :obj["drag"]
				});
				setAotuLocalEvent(polygon);
				jsmap.storageDemoLayer[key]=polygon;
			}
		}
}
function DrawLightLayer(psId){
	var key =psId+"_light";
	var bounds =jsmap.storageBounds[key];
	var obj =jsmap.storageLights;
	if($.isEmptyObject(bounds)){
		getStorageLayerAjax(STORAGE_POSITION_TYPE.LIGHT,psId);
		bounds =jsmap.storageBounds[key];
	}
	if(bounds&&bounds.length>0){
	    for(var i=0;i<bounds.length;i++){
	    	
			 if(obj&&obj[psId+"_"+bounds[i].id]){
	    		 continue ;
	    	 }
			 var point;
			 var latlng;
			if(bounds[i].latlng){
				point=bounds[i].latlng;
				latlng =strToLatlng(point);
			}
			var name=bounds[i].name;
			var status=parseInt(bounds[i].status);
			var map_icon ="";
			switch(status){
			case 0:
				map_icon="../imgs/maps/light_off.png";
				break;
			case 1:
				map_icon="../imgs/maps/light_on.png";
				break;
			}
			var light=jsNewMarker(latlng,map_icon);
			light.setTitle(name);
			light.putData({
					 dbData:bounds[i],
					 status :bounds[i].status,
					 name:name,
					 key:psId+"_"+i,
					 psId:psId,
					 state:STORAGE_POSITION_TYPE.LIGHT
			 });
				MapsEvent.addListener(light,"rightclick",function(e){
				dragStorageLayerObject(this.data.state,this.data.psId,this.data.dbData.id);	
				modifyLight(this.data.dbData);
				});
			 jsmap.storageLights[psId+"_"+bounds[i].id] =light;
		}
		
	}
	
}
//重新加载light图层

JSMap.prototype.reLoadLightLayer=function(psId,lightId,flag){
	var key =psId+"_light";
	if(lightId){
		jsmap.clearLight(lightId);
	}
	if(!flag){
		var light_data=jsmap.storageBounds[key];
		if(light_data){
			if(light_data.length==1){
				delete jsmap.storageBounds[key];
			}else if(light_data.length>1){
				for(var i=0;i<light_data.length;i++){
					if(light_data[i].id==lightId){
						light_data.splice(i,1);
					}
				}
			}
		}
		
	}
	DrawLightLayer(psId);
}
//清除light图层
JSMap.prototype.clearLight=function(lightId){
	var obj =jsmap.storageLights;
	if(!$.isEmptyObject(obj)){
		for(var key in obj){
			if(key.split('_')[1]==lightId){
				obj[key].setMap(null);
				delete obj[key];
			}else if(!lightId){
				obj[key].setMap(null);
				delete obj[key];
			}
			if(lightId=='all'){
				obj[key].setMap(null);
				delete obj[key];
			}
		}
		
	}
}
//清除light图层
JSMap.prototype.clearDomeLight=function(psId){
	var obj =jsmap.storageLights;
	var key =psId+"_demo";
	obj[key].setMap(null);
	obj[key]=null;
}
function clearAutoLocation(){
	var obj =jsmap.storageDemoLayer;
		for(var key in obj){
			if(key.indexOf("autoLocal")>-1){
				obj[key].setMap(null);
				obj[key]=null;
			}
		}
}
function clearStroageBounds(psId){
	jsmap.storageBounds[psId]={};
}
function clearStroageBoundsSingle(psId,key){
	var _key =key.toLowerCase();
	delete  jsmap.storageBounds[psId][_key];
}
function clearStroageBoundsSingle(psId,key){
	var _key =key.toLowerCase();
	delete  jsmap.storageBounds[psId][_key];
}

//加载仓库图层
JSMap.prototype.loadStorageLayer = function(psId,layers){
	var storageBounds = this.storageBounds[psId];
	if($.isEmptyObject(storageBounds)){
		tempData["storageLayer"] = layers;
		getStorageBoundsAjax(psId,"kml_storage");
		return;
	}else{
		layers = layers ? layers : tempData["storageLayer"];
		var showTitle = layers.indexOf("zone-title")>-1;  //控制area图层显示名称为title
		if(showTitle && $.isEmptyObject(this.areaTitle[psId])){  //需先加载title和zone的关系，否则zone-title图层无法显示
			tempData["storageLayer"] = layers;
			getAreaTitleAjax(psId);
			return;
		}
		var showDock = layers.indexOf("zone-dock")>-1;  //控制area图层显示名称为dock
		if(showDock && !this.areaDock[psId]){  //需先加载dock和zone的关系，否则zone-dock图层无法显示
			tempData["storageLayer"] = layers;
			getAreaDockAjax(psId);
			return;
		}
		for(var key in storageBounds){
			var layer = key == "base" ? key : key.substring(0,key.indexOf("_"));
			if(layers.indexOf(layer) >= 0){
				var polygon = this.storageLayerPolygon[psId+"_"+key];
				if(!polygon){
					var strokeColor = STORAGE_POSITION_COLOR[layer.toUpperCase()].strokeColor;
					var strokeWeight = STORAGE_POSITION_COLOR[layer.toUpperCase()].strokeWeight;
					var strokeOpacity = STORAGE_POSITION_COLOR[layer.toUpperCase()].strokeOpacity;
					var fillColor = STORAGE_POSITION_COLOR[layer.toUpperCase()].fillColor;
					var fillOpacity = STORAGE_POSITION_COLOR[layer.toUpperCase()].fillOpacity;
					var name = key.substr(key.indexOf("_")+1).toUpperCase();
					var bounds = storageBounds[key];
					var points = bounds.split(" ");
					var path = [];
					var bound = jsNewLatLngBounds();
					for(var j=0; j<points.length; j++){
						var point = points[j].split(",");
						var latLng = jsNewLatLng(point[1], point[0]);
						path.push(latLng);
						bound.extend(latLng);
					}
					var position = bound.getCenter();
					var labelContent = "<span style='color: #A129E6'><p align='center'>"+name+"</p></span>";
					var label = null;

					var geometry = null;
					if(layer == "base"){
						geometry = jsNewPolyline(path, strokeColor, strokeWeight, strokeOpacity);
						this.googleMap.fitBounds(bound);
						var zoom = this.googleMap.zoom;
						if(zoom < 21){
							this.setZoom(zoom+1);
						}
					}else if(layer == "area"){
						if(showTitle){
							var html = "";
							var titles = getTitleByAreaName(psId,name);
							var ts = titles.split(",");
							for(var i=0; i<ts.length; i++){
								html += "<p align='center'>"+ts[i]+"</p>"
							}
							name = html;
							labelContent = "<span style='color: #A129E6'>"+name+"</span>";
						}
						geometry = jsNewPolyline(path, strokeColor, strokeWeight, strokeOpacity);
						label = new LabelOverlay(position,name,"center");
						label.zoomRange = [18,21];
					}else{
						geometry = jsNewPolygon(path, strokeColor, strokeWeight, strokeOpacity, fillColor, fillOpacity);
						label = new LabelOverlay(position,name,"center");
						label.zoomRange = [22,23];
					}
					geometry.putData({
						key : key,
						labelContent : labelContent,
						strokeColor : strokeColor,
						name : name, //显示title图层时次值为area对应的title
						label : label,
						psId:psId,
						state : STORAGE_POSITION_TYPE[layer.toUpperCase()]
					});
					setStorageObjEvent(geometry);
					this.storageLayerPolygon[psId+"_"+key] = geometry;
				}else{
					if(layer == "base"){
						this.googleMap.fitBounds(polygon.getBounds());
						var zoom = this.googleMap.zoom;
						if(zoom < 21){
							this.setZoom(zoom+1);
						}
					}
					if(layer == "area"){
						var name = key.substr(key.indexOf("_")+1).toUpperCase();
						var html = "";
						if(showTitle){
							var titles = getTitleByAreaName(psId,name);
							var ts = titles.split(",");
							for(var i=0; i<ts.length; i++){
								html += "<p align='center'>"+ts[i]+"</p>"
							}
						}else if(showDock){
							html = name;
						}else{
							html = name;
						}
						polygon.data.labelContent = "<span style='color: #A129E6'><p align='center'>"+html+"</p></span>";
						polygon.data.name = html;
						if(polygon.data.label){
							polygon.data.label.setHtml(polygon.data.name);
							polygon.data.label.autoDisplay();
						}
					}
				}
			}
		}
		tempData["storageLayer"] = null;
	}
};
function showStagingPlate(datas,filter_id){
	for(var i=0;i<datas.length; i++ ){
		var data=datas[i];
		var filterId={"filter_id":filter_id};
		data = $.extend({},data,filterId);
		var _key =data.ps_id+"_staging_"+data.name;
		_key=_key.toLowerCase();
		var staging=jsmap.storageLayerPolygon[_key];
		staging.setOptions({fillColor:'#BDD9FF'});
		staging.data = $.extend({},staging.data,data);
		MapsEvent.addListener(staging,"click",function(even){
			openContainerWin(this.data);
		});
	}
	
}

function initStagingState(psId){
	var obj=jsmap.storageLayerPolygon;
	for(var key in obj){
		if(key.indexOf(psId+"_staging_")!=-1){
			if(obj[key]){
				obj[key].setOptions({fillColor:'#FFFFB4'});
				MapsEvent.clearListeners(obj[key], "click");
			}
		}
	}
}
JSMap.prototype.loadZoneDocks=function(psId){
	var resource=	getAreaDockCountsAjax(psId);
	if(!$.isEmptyObject(resource)){
		for(var i=0;i<resource.length;i++){
			if(resource[i].counts!="0"){
			var key =psId+"_dock_"+resource[i].area_id;
			if(!this.storageZoneDocksLayer[key]){
			var points=resource[i].latlng.split(" ");
			var bound = jsNewLatLngBounds();
			for(var j=0; j<points.length; j++){
				var point = points[j].split(",");
				var latLng = jsNewLatLng(point[1], point[0]);
				bound.extend(latLng);
			}
		var 	position =bound.getCenter();
		var resourceMarker=jsNewMarker(position ,"../imgs/maps/area_door.png");
		resourceMarker.putData({
			dbase: resource[i]
		});
		MapsEvent.addListener(resourceMarker,"click",function(e){
			var area_id =this.data.dbase.area_id;
			var area_name =this.data.dbase.area_name;
			showZonedocks(psId,area_id ,area_name);
		});
		 this.storageZoneDocksLayer[key]=resourceMarker;
				}
			}
		}
	}
}

JSMap.prototype.loadZonePerson=function(psId){
	var resource=	getAreaPersionCountsAjax(psId);
	if(!$.isEmptyObject(resource)){
		for(var i=0;i<resource.length;i++){
			if(resource[i].counts!="0"){
				var key =psId+"_person_"+resource[i].area_id;
				if(!this.storageZonePersonLayer[key]){
				var points=resource[i].latlng.split(" ");
				var bound = jsNewLatLngBounds();
				for(var j=0; j<points.length; j++){
					var point = points[j].split(",");
					var latLng = jsNewLatLng(point[1], point[0]);
					bound.extend(latLng);
				}
				var 	position =bound.getCenter();
				var personMarker=jsNewMarker(position ,"../imgs/maps/area_person.png");
				personMarker.putData({
					dbase: resource[i]
				});
				MapsEvent.addListener(personMarker,"click",function(e){
					var area_id =this.data.dbase.area_id;
					var area_name =this.data.dbase.area_name;
					showZonePerson(psId,area_id ,area_name);
				});
				this.storageZonePersonLayer[key]=personMarker;
				}
			}
		}
	}
}
//加载道路图层
JSMap.prototype.loadRoadLayer = function(psId,layer){
	var _storageBounds = this.storageBounds["road_"+psId];
	if(!_storageBounds || 
			layer["main"]&&!_storageBounds["main"] || 
			layer["entery"]&&!_storageBounds["loc_entery"]){
		tempData["loadRoadLayer"] = true;
		getStorageRoadAjax(psId,layer);
		return;
	}
	var storageBounds = this.storageBounds["road_"+psId];
	if(layer["main"]){
		var mainRoad = storageBounds.main;
		for(var i=0; i<mainRoad.length; i++){
			var r = mainRoad[i];
			var id = r.r_id;
			var name = r.name;
			var key = "main_"+id;
			if(!this.storageRoadPolyline[key] && r.geom){
				var path = wktToPath(r.geom)[0];
				var polyline = jsNewPolyline(path, "#91dafe", 3, 1);
				polyline.setOptions({zIndex : 2});
				
				/*
			var labelContent = "<span style='color: #A129E6'>"+id+"</span>";
			var label = new LabelOverlay(polyline.getCenter(),id,"center");
			label.zoomRange = [17,21];
			polyline.putData({
				key : key,
				labelContent : labelContent,
				strokeColor : "#91dafe",
				name : id, 
				label : label,
				psId:psId,
				state : STORAGE_POSITION_TYPE.ROAD
			});
			setStorageObjEvent(polyline);
				 */
				
				this.storageRoadPolyline[key] = polyline;
			}
		}
	}
	if(layer["entery"]){
		var locEntery = storageBounds.loc_entery;
		for(var i=0; i<locEntery.length; i++){
			var r = locEntery[i];
			var id = r.slc_id;
			var key = "loc_entery_"+id;
			if(!this.storageRoadPolyline[key] && r.entery_point && r.road_point){
				var path = [strToLatlng(r.entery_point),strToLatlng(r.road_point)];
				var polyline = jsNewPolyline(path, "#91dafe", 3, 1);
				polyline.setOptions({zIndex : 1});
				this.storageRoadPolyline[key] = polyline;
			}
		}
	}
}
//仓库对象鼠标事件
function setStorageObjEvent(polygon){
	MapsEvent.addListener(polygon,"mouseover",function(e){
		//显示边框、名称
		this.setOptions({	
			strokeColor:"#FFFFFF",
			zIndex:2
		});
		if(this.data.label){
			this.data.label.setHtml(this.data.labelContent);
			this.data.label.show();
		}
		if(this.data.sector){
			this.data.sector.setOptions({strokeOpacity:0.7,fillOpacity:0.7});
		}
		
	});
	MapsEvent.addListener(polygon,"mouseout",function(e){
		//隐藏边框、名称
		this.setOptions({
			strokeColor:this.data.strokeColor,
			zIndex:1
		});
		if(this.data.label){
			this.data.label.setHtml(this.data.name);
			this.data.label.autoDisplay();
		}
		if(this.data.sector){
			this.data.sector.setOptions({strokeOpacity:0.2,fillOpacity:0.2});
		}
	});
	MapsEvent.addListener(polygon,"click",function(e){
    		if(storageKmlClick){
				storageKmlClick(this.data,e);
				
			}
	});
	MapsEvent.addListener(polygon,"rightclick",function(e){
		if(storageKmlRightClick){
			var position = jsmap.overlay.getProjection().fromLatLngToContainerPixel(e.latLng);
			storageKmlRightClick(this.data,position, e.latLng);
			jsmap.storageProvenLayer[polygon.data.key]=polygon;
		}
	});
}
function setAotuLocalEvent(polygon){
	MapsEvent.addListener(polygon,"mouseover",function(e){
		//显示边框、名称
		this.setOptions({	
			strokeColor:"#FFFFFF",
			zIndex:2
		});
		if(this.data.label){
			this.data.label.setHtml(this.data.labelContent);
			this.data.label.show();
		}
		if(this.data.sector){
			this.data.sector.setOptions({strokeOpacity:0.7,fillOpacity:0.7});
		}
	});
	MapsEvent.addListener(polygon,"mouseout",function(e){
		//隐藏边框、名称
		this.setOptions({
			strokeColor:this.data.strokeColor,
			zIndex:1
		});
		if(this.data.label){
			this.data.label.setHtml(this.data.name);
			this.data.label.autoDisplay();
		}
		if(this.data.sector){
			this.data.sector.setOptions({strokeOpacity:0.2,fillOpacity:0.2});
		}
	});
}
//加载area和title的关系
function getAreaTitleAjax(psId){
	var _psId = psId;
	if(loading["areaTitle_"+_psId]){
		return false;
	}
	$.ajax({
		url:systenFolder+'action/administrator/maps/GetAreaTitleAction.action',
		data:'ps_id='+_psId,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
			loading["areaTitle_"+_psId] = true;
	    },
		success:function(data){
	    	if(data){
	    		jsmap.areaTitle[_psId] = data;
	    		loading["areaTitle_"+_psId] = false;
    			if(tempData["storageLayer"] != null){
    				jsmap.loadStorageLayer(_psId,null);
    			}
	    	}
		},
		error:function(){
			loading["areaTitle_"+_psId] = false;
		}
	});
}
/**
 * 图层数据来源数据表
 */
function getStorageLayerAjax(type ,psId){
    var 	typeName=null;
    var loadType =psId+"_"+type;
    if(loading[loadType]){
    	return;
    }
    var key =null;
	switch (type){
		case 1:
			typeName="location";
			break;
		case 2:
			typeName="staging";
			break;
		case 3:
			typeName="docks";
			break;
		case 4:
			typeName="parking";
			break;
		case 5:
			typeName="area";
			break;
		case 6:
			typeName="webcam";
			break;
		case 7:
			typeName="printer";
			break;
		case 8:
			typeName="road";
			break;
		case 9:
			typeName="light";
			break;
	}
	key =psId+"_"+typeName;
	$.ajax({
			url:systenFolder+'action/administrator/gis/getStorageLayer.action',
			data:{"type":type,"ps_id":psId },
			dataType:'json',
			type:'post',
			async:false, 
			beforeSend:function(request){
				loading[loadType] = true;
		    },
			success:function(data){
				if(data&&data.length>0){
					jsmap.storageBounds[key]=data;
					loading[loadType] = false;
					switch (type){
					case 6:
						drawWebcam(psId);
						break;
					case 7:
						drawPrinter(psId);
						break;
					case 9:
						DrawLightLayer(psId);
						break;
					}
				}else{
					
				}
			},
		    error:function (){
		    	loading[loadType] = false;
		    }
       })
}
//加载area和dock的关系
function getAreaDockAjax(psId){
	var _psId = psId;
	if(loading['dock_'+psId]){
		return false;
	}
	$.ajax({
		url:systenFolder+'action/administrator/maps/GetAreaDoorAction.action',
		data:'ps_id='+_psId,
		dataType:'json',
		type:'post',
		async:false,
		beforeSend:function(request){
			loading['dock_'+psId]=true;
		},
		success:function(data){
			if(data){
				jsmap.areaDock[_psId] = data;
				if(tempData["storageLayer"] != null){
					jsmap.loadStorageLayer(_psId,null);
					loading['dock_'+psId]=false;
				}
			}
			}
	});
}

//加载area和dock的关系对应的dock 的counts
function getAreaDockCountsAjax(psId){
	var areaDocks="";
	var _psId = psId;
	$.ajax({
		url:systenFolder+'action/administrator/gis/getAreaDoorCounts.action',
		data:'ps_id='+_psId,
		dataType:'json',
		type:'post',
		async:false,
		beforeSend:function(request){
		},
		success:function(data){
			if(data){
				areaDocks= data;
			}
		}
	});
	return areaDocks;
}
//通过经纬度计算坐标
function convertLatlngToCoordinateAjax(psId,lat,lng){
	var point="";
	var _psId = psId;
	$.ajax({
		url:systenFolder+'action/administrator/gis/convertCoordinate.action',
		data:'psId='+_psId+'&lat='+lat+'&lng='+lng,
		dataType:'json',
		type:'post',
		async:false,
		beforeSend:function(request){
		},
		success:function(data){
			if(data.flag=="true"){
				point= jsNewPoint(data.x, data.y);
			}
		}
	});
	return point;
}
//加载area和Person的关系对应的Person 的counts
function getAreaPersionCountsAjax(psId){
	var areaPerson="";
	var _psId = psId;
	$.ajax({
		url:systenFolder+'action/administrator/gis/getAreaPersonCounts.action',
		data:'ps_id='+_psId,
		dataType:'json',
		type:'post',
		async:false,
		beforeSend:function(request){
		},
		success:function(data){
			if(data){
				areaPerson= data;
			}
		}
	});
	return areaPerson;
}
//加载路
function getStorageRoadAjax(psId,layer){
	var _psId = psId;
	var _layer = layer;
	if(loading["road_"+_psId]){
		return false;
	}
	var para = 'ps_id='+_psId;
	if(layer["main"]){ //主路
		para += "&main=1";
	}
	if(layer["entery"]){ //location入口道路
		para += "&entery=1"
	}
	if(layer["point"]){ //路口
		para += "&point=1"
	}
	$.ajax({
		url:systenFolder+'action/administrator/maps/GetStorageRoadAction.action',
		data:para,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
			loading["road_"+_psId] = true;
		},
		success:function(data){
			if(data && data.flag=="true"){
				jsmap.storageBounds["road_"+_psId] = $.extend({},jsmap.storageBounds["road_"+_psId],data.road);
				loading["road_"+_psId] = false;
				if(tempData["loadRoadLayer"]){
					loadRoadLayer(_psId,_layer);
					tempData["loadRoadLayer"] = false;
				}
			}
		},
		error:function(){
			loading["road_"+_psId] = false;
		}
	});
}
//根据areaName获取title
function getTitleByAreaName(psId,areaName){
	var titles = "";
	var areaTitle = jsmap.areaTitle[psId];
	if(areaTitle){
		for(var i=0; i<areaTitle.length; i++){
			var t = areaTitle[i];
			if(t.area_name == areaName){
				titles += t.title_name+",";
			}
		}
	}
	if(titles != ""){
		titles = titles.substr(0,titles.length-1);
	}
	return titles;
}
//根据areaName获取dock
function getDoorByAreaName(psId,areaName){
	var docks = "";
	var areaDock = jsmap.areaDock[psId];
	if(areaDock){
		for(var i=0; i<areaDock.length; i++){
			var t = areaDock[i];
			if(t.area_name == areaName){
				docks += t.doorid+",";
			}
		}
	}
	if(docks != ""){
		docks = docks.substr(0,docks.length-1);
	}
	return docks;
}
function getDoorByAreaNameFormat(psId,areaName){
	var re = "";
	var docks = getDoorByAreaName(psId,areaName);
	var ds = docks.split(",");
	var len = ds.length;
	if(len>0){
		for(var i=0; i<len; i+=3){
			re +=  ds[i+0] +" ";
			if(i+1 < len){
				re +=  ds[i+1] +" ";
			}
			if(i+2 < len){
				re +=  ds[i+2] +",";
			}
		}
	}
	return re;
}
//修改位置路径
function modifyPositionLatlng(psId,oldName,newName,latlngs,type){
	var typeName = "";
	if(type == 5){
		//modifyAreaLatlng(psId,oldName,newName,latlngs);
		typeName = "_area_";
	}else if(type == 4){
		typeName = "_parking_";
	}else if(type == 3){
		typeName = "_docks_";
	}else if(type == 2){
		typeName = "_staging_";
	}else if(type == 1){
		typeName = "_location_";
	}
	//重新加载location图层
	getStorageBoundsAjax(psId,"kml_storage","loc");
	getStorageBoundsAjax(psId,"kml_storage");
	var key = psId + typeName + oldName.toLowerCase();
	var newKey = psId + typeName + newName.toLowerCase();
	if(latlngs != ""){
		var points = latlngs.split(" ");
		var path = [];
		for(var j=0; j<points.length; j++){
			var point = points[j].split(",");
			var latLng = jsNewLatLng(point[1], point[0]);
			path.push(latLng);
		}
		//area  staging
		//修改图层区域area  staging
		if(jsmap.storageLayerPolygon[key]){
			var polygon = jsmap.storageLayerPolygon[key];
			polygon.setPath(path);
			polygon.data.label.setPosition(polygon.getCenter());
		}
		//修改隐藏区域area
		if(jsmap.storageAreaPolygon[key]){
			jsmap.storageAreaPolygon[key].setPath(path);
		}
		//修改图层区域parking  door
		if(jsmap.storageObjPolygon[key]){
			var polygon = jsmap.storageObjPolygon[key];
			polygon.setPath(path);
			polygon.data.label.setPosition(polygon.getCenter());
		}
	}
	if(oldName&&oldName != newName){
		//area  staging
		//修改图层区域area  staging
		if(jsmap.storageLayerPolygon[key]){
			jsmap.storageLayerPolygon[newKey] = jsmap.storageLayerPolygon[key];
			var data = jsmap.storageLayerPolygon[newKey].data;
			data.name = newName.toUpperCase();
			data.labelContent = data.labelContent.replace(oldName.toUpperCase(),newName.toUpperCase());
			data.label.setHtml(newName);
			data.label.setPosition(jsmap.storageLayerPolygon[newKey].getCenter());
			jsmap.storageLayerPolygon[key] = null;
		}
		//修改隐藏区域area
		if(jsmap.storageAreaPolygon[key]){
			jsmap.storageAreaPolygon[newKey] = jsmap.storageAreaPolygon[key];
			var data = jsmap.storageAreaPolygon[newKey].data;
			data.key = "area_"+newName.toLowerCase();
			data.name = newName.toUpperCase();
			data.draw = false;
			jsmap.storageAreaPolygon[key] =null;
		}
		//修改图层区域parking door
		if(jsmap.storageObjPolygon[key]){
			jsmap.storageObjPolygon[newKey] = jsmap.storageObjPolygon[key];
			var data = jsmap.storageObjPolygon[newKey].data;
			data.name = newName.toUpperCase();
			data.labelContent = data.labelContent.replace(oldName.toUpperCase(),newName.toUpperCase());
			data.label.setHtml(newName);
			data.label.setPosition(jsmap.storageObjPolygon[newKey].getCenter());
			jsmap.storageObjPolygon[key] = null;
		}
	}
}
//修改location经纬度路径
function modifyLocationLatlng(psId,oldName,newName,latlngs){
	//重新加载location图层
	getStorageBoundsAjax(psId,"kml_storage","loc");
	
}
//修改area经纬度路径
function modifyAreaLatlng(psId,oldName,newName,latlngs){
	//重新加载location图层
	getStorageBoundsAjax(psId,"kml_storage","loc");
	var key = psId + "_area_" + oldName.toLowerCase();
	var newKey = psId + "_area_" + newName.toLowerCase();
	if(oldName != newName){
		//修改图层区域
		if(jsmap.storageLayerPolygon[key]){
			jsmap.storageLayerPolygon[newKey] = jsmap.storageLayerPolygon[key];
			var data = jsmap.storageLayerPolygon[newKey].data;
			data.name = newName.toUpperCase();
			data.labelContent = data.labelContent.replace(oldName.toUpperCase(),newName.toUpperCase());
			data.label.setHtml(newName);
			jsmap.storageLayerPolygon[key] = null;
		}
		//修改隐藏区域
		if(jsmap.storageAreaPolygon[key]){
			jsmap.storageAreaPolygon[newKey] = jsmap.storageAreaPolygon[key];
			var data = jsmap.storageAreaPolygon[newKey].data;
			data.key = "area_"+newName.toLowerCase();
			data.name = newName.toUpperCase();
			data.draw = false;
			jsmap.storageAreaPolygon[key] =null;
		}
	}
	if(latlngs != ""){
		var points = latlngs.split(" ");
		var path = [];
		for(var j=0; j<points.length; j++){
			var point = points[j].split(",");
			var latLng = jsNewLatLng(point[1], point[0]);
			path.push(latLng);
		}
		//修改图层区域
		if(jsmap.storageLayerPolygon[newKey]){
			jsmap.storageLayerPolygon[newKey].setPath(path);
		}
		//修改隐藏区域
		if(jsmap.storageAreaPolygon[newKey]){
			jsmap.storageAreaPolygon[newKey].setPath(path);
		}
	}
}
//显示路径规划结果
JSMap.prototype.drawRoutePath = function(paths){
	var path = null;
	var length = 100000;
	for(var i=0; i<paths.length; i++){
		var p = stringToPath(paths[i].path,",");
		var l = computeLength(p);
		if(l<length){
			path = p;
			length = l;
		}
	}
	var icon = {
	      icon: {path: 'M 0,-2 0,0',
	    	    strokeOpacity: 1,
	    	    scale: 3
	    	  	},
	      offset: '0',
	      repeat: '15px'
	    };
	var poly = this.routePathPolyLine;
	var fromPath = [poly["from"].getPosition(),path[0]]; 
	var toPath = [path[path.length-1],poly["to"].getPosition()]; 
	if(poly["line"]){
		poly["line"].setPath(path);
		poly["fromline"].setPath(fromPath);
		poly["toline"].setPath(toPath);
	}else{
		poly["line"] = jsNewPolyline(path, "#0066FF", 3, 1);
		poly["line"].setOptions({zIndex : 3});
		poly["fromline"] = jsNewPolyline(fromPath, "#0066FF", 1, 0);
		poly["fromline"].setOptions({zIndex : 3, icons: [icon]});
		poly["toline"] = jsNewPolyline(toPath, "#0066FF", 1, 0);
		poly["toline"].setOptions({zIndex : 3, icons: [icon]});
	}
}
//添加路径规划起始点标记
JSMap.prototype.addRouteMarker = function(latlng, type){
	var name = "A";
	if(type.toLowerCase() == "to"){
		name = "B";
	}
	var icon = "https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld="+name+"|0099EE|000000";
	if(this.routePathPolyLine[type]){
		this.routePathPolyLine[type].setPosition(latlng);
	}else{
		this.routePathPolyLine[type] = jsNewMarker(latlng, icon);
		this.routePathPolyLine[type].setDraggable(true);
		
		this.routePathPolyLine[type].putData({"type":type});
		
		MapsEvent.addListener( this.routePathPolyLine[type],'mouseup',function(){
			//var latlng = $("#jsmap").data("latlng",this.getPosition);
			var poly = $("#jsmap").data("curr_poly_data");
			var nav = $("#navFrom").data("nav");
			nav[this.data.type] = {"type":poly.state, "name":poly.name, "latlng":this.getPosition()};
			getRoutePathAjax();
			//routePlan(this.data.type);
		})
	}
	
}
//显示仓库坐标系
JSMap.prototype.showStorageCoordinateSys =function(psId,data){
	var d = this.storageCoordinateSysPolyline;
	var _psId = d.psId;
	this.clearStorageCoordinateSys();
	if(_psId == psId){
		return;
	}
	var len = data.length;
	var xPath = [];
	var yPath = [];
	var x = parseInt(data[1].coor.split(" ")[0]);
	var y = parseInt(data[len-1].coor.split(" ")[1]);
	var xn = x;
	var yn = y;
	
	xPath.push(jsNewLatLng(data[0].lat, data[0].lng));
	xPath.push(jsNewLatLng(data[1].lat, data[1].lng));
	yPath.push(jsNewLatLng(data[0].lat, data[0].lng));
	yPath.push(jsNewLatLng(data[len-1].lat, data[len-1].lng));
	
	for (var i=2; i<len-1; i++) {
		var coor = data[i].coor.split(" ");
		if(parseInt(coor[0])>xn){
			xn = parseInt(coor[0]);
		}
		if(parseInt(coor[1])>yn){
			yn = parseInt(coor[1]);
		}
	}
	
	var h = computeHeading(xPath[0], xPath[1]);
	var dis = computeDistanceBetween(xPath[0], xPath[1]);
	var begin = computeOffsetOrigin(xPath[0], dis*0.1*xn/x, h);
	var end = computeOffsetDestination(xPath[0], dis*1.1*xn/x, h);
	xPath = [begin,end];
	
	h = computeHeading(yPath[0], yPath[1]);
	dis = computeDistanceBetween(yPath[0], yPath[1]);
	begin = computeOffsetOrigin(yPath[0], dis*0.1*yn/y, h);
	end = computeOffsetDestination(yPath[0], dis*1.1*yn/y, h);
	yPath = [begin,end];
	
	var x = jsNewPolyline(xPath, "red", 2, 1);
	x.setOptions({zIndex:5,
				  icons: [{
					      icon: {path: google.maps.SymbolPath.FORWARD_OPEN_ARROW},
					      offset: '100%'
				  		  },
				  		  {
				  		   icon: {path: "M -4,-3 l 8,-4 m -8,0 l 8,4"},
						   offset: '100%'
				  		  },
				  		  {
				  		   icon: {path: "M 2,2 m 0,1 l 0,1 l 1,1 l 4,0 l 1,-1 l 0,-1 l -1,-1 l -4,0 l -1,1"},
						   offset: '8%'
				  		  }
		  		  ]
	});
	var y = jsNewPolyline(yPath, "red", 2, 1);
	y.setOptions({zIndex:5,
				  icons: [{icon: {path: google.maps.SymbolPath.FORWARD_OPEN_ARROW},
					       offset: '100%'
				  		  },
				  		  {
				  		   icon: {path: "M 0,-3 l 0,-5 l -2,-3 m 2,3 l 2,-3"},
						   offset: '100%'
				  		  }
				  ]
	});
	this.storageCoordinateSysPolyline.psId = psId;
	this.storageCoordinateSysPolyline.line = {"x" : x, "y" : y}
}
//清除停车位、门图层
JSMap.prototype.clearStorageObj = function(layer){
	var objs = this.storageObjPolygon;
	for(var key in objs){
		var clear = layer ? (key.indexOf(layer)==key.indexOf("_")+1) : true;
		if(objs[key] && clear){
			objs[key].data.label.setMap(null);
			objs[key].data.label = null;
			objs[key].setMap(null);
			objs[key] = null;
		}
	}
	//this.storageObjPolygon = {};
}
//清除库存位置
JSMap.prototype.clearStorageCatalog = function(){
	var objs = this.storageCatalogPolygon;
	for(var key in objs){
		if(objs[key]){
			objs[key].data.label.setMap(null);
			objs[key].data.label = null;
			objs[key].setMap(null);
			objs[key] = null;
		}
	}
	this.storageCatalogPolygon = {};
}
//清除area内位置
JSMap.prototype.clearLocation = function(){
	var objs = this.storageLocationPolygon;
	var areaKey = null;
	for(var key in objs){
		if(objs[key]){
			var psId = objs[key].data.psId;
			var begin = key.indexOf("[")+1;
			var count = key.indexOf("]") - begin;
			areaKey = psId+"_area_"+key.substr(begin,count);
			
			objs[key].data.label.setMap(null);
			objs[key].data.label = null;
			objs[key].setMap(null);
			objs[key] = null;
		}
	}
	if(this.storageAreaPolygon[areaKey]){
		this.storageAreaPolygon[areaKey].data.draw = false;
	}
	this.storageLocationPolygon = {};
}
//清除仓库图层
JSMap.prototype.clearStorageLayer = function(psId,layers){
	var objs = this.storageLayerPolygon;
	var layer = layers.split(",");
	for(var key in objs){
		var clear = false;
		for(var i=0; i<layer.length; i++){
			if(key.indexOf(psId+"_"+layer[i])==0){
				clear = true;
				break;
			}
		}
		if(objs[key] && clear){
			objs[key].data.label.setMap(null);
			objs[key].data.label = null;
			objs[key].setMap(null);
			objs[key] = null;
		}
	}
}
//清除Zone-person图层
JSMap.prototype.clearZonePerson=function(){
	 var objs =this.storageZonePersonLayer;
	 if(!$.isEmptyObject(objs)){
	 for(var key in objs){
			 if(objs[key]){
			 objs[key].setMap(null);
			 objs[key]=null;
			 }
	 }
	 this.storageZonePersonLayer={};
	 }
}
//清除Zone-dock图层
JSMap.prototype.clearZoneDocks=function(){
	var objs =this.storageZoneDocksLayer;
	if(!$.isEmptyObject(objs)){
	for(var key in objs){
			if(objs[key]){
			 objs[key].setMap(null);
			objs[key]=null;
			}
	}
	this.storageZoneDocksLayer={};
	}
}
//清除摄像头图层
JSMap.prototype.clearWebcam = function(camparameterId){
	var objs = this.storageWebcamMarker;
	for(var key in objs){
		if(objs[key]){
			if(key.split('_')[1]==camparameterId){
				objs[key].data.label.setMap(null);
				objs[key].data.label = null;
				objs[key].data.sector.setMap(null);
				objs[key].data.sector = null;
				objs[key].setMap(null);
				objs[key] = null;
			}else if(!camparameterId){
				objs[key].data.label.setMap(null);
				//objs[key].data.label = null;
				objs[key].data.sector.setMap(null);
				//objs[key].data.sector = null;
				objs[key].setMap(null);
				delete objs[key];
			}
			
		}
	}
	//this.storageWebcamMarker = {};
}
//清除摄像头图层
JSMap.prototype.clearDomeWebcam = function(psId){
	var objs = this.storageWebcamMarker;
	var key =psId+"_demo";
	objs[key].setMap(null);
	objs[key].data.label.setMap(null);
	objs[key].data.label = null;
	objs[key].data.sector.setMap(null);
	objs[key].data.sector = null;
	objs[psId+"_demo"]=null;
}
//清除打印机图层
JSMap.prototype.clearPrinter = function(printerId){
	var objs = this.storagePrinterMarker;
	for(var key in objs){
		if(objs[key]){
			if(printerId==key.split('_')[1]){
				objs[key].setMap(null);
				delete objs[key] ;
			}else if(!printerId){
				objs[key].setMap(null);
				delete objs[key] ;
			}
			
		}
	}
	//this.storagePrinterMarker = {};
}
//清除打印机图层
JSMap.prototype.clearDomePrinter = function(psId){
	var objs = this.storagePrinterMarker;
	objs[psId+"_demo"].setMap(null);
	objs[psId+"_demo"]=null;
}
//清除路线图层
JSMap.prototype.clearRoad = function(){
	var objs = this.storageRoadPolyline;
	for(var key in objs){
		if(objs[key]){
			if(objs[key].data && objs[key].data.label){
				objs[key].data.label.setMap(null);
				objs[key].data.label = null;
			}
			objs[key].setMap(null);
			objs[key] = null;
		}
	}
	this.storageRoadPolyline = {};
}
//清除规划线路
JSMap.prototype.clearRoutePath = function(){
	var objs = this.routePathPolyLine;
	for(var key in objs){
		if(objs[key]){
			objs[key].setMap(null);
			objs[key] = null;
		}
	}
	this.routePathPolyLine = {};
}
//清除仓库坐标系
JSMap.prototype.clearStorageCoordinateSys = function(){
	var obj = this.storageCoordinateSysPolyline.line;
	if(obj){
		for(var key in obj){
			if(obj[key]){
				obj[key].setMap(null);
				obj[key] = null;
			}
		}
	}
	this.storageCoordinateSysPolyline = {};
}
//在锚点(anchor)上添加infowindow
function addInfoWindowToAttach(anchor,content,latlng){
	var infowindow = jsNewInfoWindow(content);
	infowindow.setOptions({
					disableAutoPan : true
				});
	MapsEvent.addListener(anchor, 'mouseover', function() {
		if(latlng && latlng!=null){  //infoWindow加到LatLng上
			infowindow.setPosition(latlng);
			infowindow.open(jsmap.getMap());
		}else{
			infowindow.open(jsmap.getMap(), anchor);
		}
	});
	MapsEvent.addListener(anchor, 'mouseout', function() {
		infowindow.close();
	});
	return infowindow;
}


//地图上添加文字标记
function LabelOverlay(position,label,align,offset,hasBorder){
	this.zoomRange = [0,21];  //在此范围内标记显示    默认始终显示
	this.label = label;
	this.position = position;  //经纬度
	this.align = align;  //值：right left center top bottom ,label相对于position的显示位置 ，默认right
	this.offset = offset ? parseInt(offset) : 0;  //偏移像素点
	this.div = null;
	this.map = jsmap.getMap();
	this.hasBorder = hasBorder;   //是否显示边框  boolean类型
	
	this.setMap(this.map);
}
LabelOverlay.prototype = new google.maps.OverlayView();
LabelOverlay.prototype.onAdd = function (){
	var div = null;
	if(this.div){
		div = this.div;
	}else{
		div = document.createElement('div');
	}
	div.style.border = "none";
	div.style.borderWidth = "0px";
	div.style.position = "absolute";
	div.style.textShadow = "1px 1px 0px white, -1px -1px 0px white, 1px -1px 0px white, -1px 1px 0px white ";  //文字阴影
	div.style.fontSize = "12px";
	div.style.fontWeight = "600";
	div.style.zIndex = "800";
	if(this.hasBorder){
		div.style.border = "solid";
		div.style.borderWidth = "1px";
		div.style.borderColor = "#CCCCCC";
		div.style.backgroundColor = "#EEEEEE";
	}
	div.innerHTML = this.label;
	 
	if(this.div!=null && this.div.parentNode!=null){
		this.div.parentNode.removeChild(this.div);
	}
	this.div = div;

	var panes = this.getPanes();
	panes.overlayLayer.appendChild(div);
}
LabelOverlay.prototype.draw = function() {
	  var overlayProjection = this.getProjection();
	  var point = overlayProjection.fromLatLngToDivPixel(this.position);
	  var div = this.div;
	  
	  var left = point.x;
	  var top = point.y;
	  if(this.align=="right"){
		  left += this.offset;
		  top -= $(div).height()/2;
	  }else if(this.align=="left"){
		  left -= $(div).width()+this.offset;
		  top -= $(div).height()/2;
	  }else if(this.align=="center"){
		  left -= $(div).width()/2;
		  top -= $(div).height()/2;
	  }else if(this.align=="top"){
		  left -= $(div).width()/2;
		  top -= $(div).height()+this.offset;
	  }else if(this.align=="bottom"){
		  left -= $(div).width()/2;
		  top += this.offset;
	  }
	  div.style.left = left +"px";
	  div.style.top = top +"px";
	  
	  this.autoDisplay();
}
LabelOverlay.prototype.onRemove = function() {
	if(this.div!=null && this.div.parentNode!=null){
		this.div.parentNode.removeChild(this.div);
	}
	this.div = null;
};
LabelOverlay.prototype.setPosition = function(position) {
	this.position = position;
	this.draw();
};
LabelOverlay.prototype.setHtml = function(html){
	this.label = html;
	this.onRemove()
	this.onAdd();
	this.draw();
};
LabelOverlay.prototype.hide = function() {
	$(this.div).hide();
};
LabelOverlay.prototype.show = function() {
	$(this.div).show();
};
LabelOverlay.prototype.autoDisplay = function() {
	var range = this.zoomRange;
	if(range && range.length > 1){
		var zoom = jsmap.zoom;
		if(zoom>=range[0] && zoom<=range[1]){
			this.show();
		}else{
			this.hide();
		}
	}
};
var lvTEST = 1;
//取权重颜色
function getLevelColor(sumLv,lv){
	lv =  (lvTEST++)%5+1;
	var from = COLOR_LEVEL.from;
	var to = COLOR_LEVEL.to;
	if(sumLv>1 && lv>0 && sumLv>=lv){
		sumLv--;
		lv--;
		var fromR = parseInt(from/0x10000);
		var fromG = parseInt(from%0x10000/0x100);
		var fromB = parseInt(from%0x100);
		
		var toR = parseInt(to/0x10000);
		var toG = parseInt(to%0x10000/0x100);
		var toB = parseInt(to%0x100);
		
		var newR = parseInt(fromR-(fromR-toR)/sumLv*lv);
		var newG = parseInt(fromG-(fromG-toG)/sumLv*lv);
		var newB = parseInt(fromB-(fromB-toB)/sumLv*lv);
		
		var rel = "#" + 
					(newR<0x10?("0"+newR.toString(16)):newR.toString(16)) + 
					(newG<0x10?("0"+newG.toString(16)):newG.toString(16)) + 
					(newB<0x10?("0"+newB.toString(16)):newB.toString(16));
		return rel;
	}else if(sumLv==1){
		var toR = parseInt(to/0x10000);
		var toG = parseInt(to%0x10000/0x100);
		var toB = parseInt(to%0x100);
		
		var rel = "#" + 
					(toR<0x10?("0"+toR.toString(16)):toR.toString(16)) + 
					(toG<0x10?("0"+toG.toString(16)):toG.toString(16)) + 
					(toB<0x10?("0"+toB.toString(16)):toB.toString(16));
		
		document.getElementById("debugInfo").value = "color:"+rel;
		return rel;
	}
	return null;
}
//新增控件
JSMap.prototype.addMapControl = function(position,eventType,func,name,imgUrl){
	if(!this.mapControls[name]){
		this.mapControls[name] = new mapControl(position,eventType,func,name,imgUrl);
	}else{
		this.showMapControl(name);
	}
};
//显示控件
JSMap.prototype.showMapControl = function(name){
	if(this.mapControls[name]){
		var ctrl = this.mapControls[name];
		ctrl.div.style.display = "inline";
	}
};
//隐藏控件
JSMap.prototype.hideMapControl = function(name){
	if(this.mapControls[name]){
		var ctrl = this.mapControls[name];
		ctrl.div.style.display = "none";
	}
};
//加载KML
JSMap.prototype.loadKml = function(url,fun,map,fitBounds){
	if(this.storageKml[url]){
		this.storageKml[url].setOptions({
			preserveViewport : !fitBounds
		});
		if(this.storageKml[url].getMap() == null){
			this.storageKml[url].setMap(jsmap.getMap());
		}
		if(fitBounds && this.storageKml[url].getDefaultViewport()){
			this.fitBounds(this.storageKml[url].getDefaultViewport());
		}
	}else{
		var kml = jsNewKmlLayer(url,fun,map,fitBounds);
		this.storageKml[url] = kml;
	}
	jsmap.flag["drawLocation"]=true;
};
//移除kml
JSMap.prototype.unLoadKml = function(url){
	if(this.storageKml[url]){
		this.storageKml[url].setMap(null);
	}
	jsmap.flag["drawLocation"]=false;
	jsmap.clearLocation();
}
//自定义控件
function mapControl(position,eventType,func,name,imgUrl){
	var controlUI = document.createElement('div');
	this.div = controlUI;
	this.name = name;
	this.position = controlPosition(position);
	
	controlUI.style.margin = '5px 5px 5px 5px';
	controlUI.style.cursor = 'pointer';
	
	if(imgUrl && imgUrl!=null){
		controlUI.innerHTML = '<img alt="'+name+'" src="'+imgUrl+'">';
	}else{
		var p = name.substring(0, 1).toUpperCase()+name.substring(1,name.length);
		controlUI.innerHTML = '<p>'+p+'</p>';
		
	controlUI.style.backgroundColor = 'white';
	controlUI.style.borderStyle = 'solid';
	controlUI.style.borderWidth = '1px';
		controlUI.style.borderColor = '#AAAAAA';
	controlUI.style.textAlign = 'center';
		controlUI.style.fontFamily = 'Arial,sans-serif';
		controlUI.style.fontSize = '13px';
		controlUI.style.padding = '0px 6px 0px 6px';
		controlUI.setAttribute("onmouseover", "this.style.backgroundColor = '#EEEEEE'");
	controlUI.setAttribute("onmouseout", "this.style.backgroundColor = 'white'");
	}
	MapsEvent.addDomListener(controlUI, eventType, func);
	jsmap.getMap().controls[this.position].push(controlUI);
	//jsmap.getMap().controls[this.position].insertAt(0,controlUI);
}
//控件位置
function controlPosition(position){
	position = position.toUpperCase();
	switch(position){
		case 'TOP_RIGHT' : return google.maps.ControlPosition.TOP_RIGHT ;
		case 'TOP_LEFT' : return google.maps.ControlPosition.TOP_LEFT ;
		case 'TOP_CENTER' : return google.maps.ControlPosition.TOP_CENTER ;
		case 'RIGHT_TOP' : return google.maps.ControlPosition.RIGHT_TOP ;
		case 'RIGHT_CENTER' : return google.maps.ControlPosition.RIGHT_CENTER;
		case 'RIGHT_BOTTOM' : return google.maps.ControlPosition.RIGHT_BOTTOM ;
		case 'LEFT_TOP' : return google.maps.ControlPosition.LEFT_TOP ;
		case 'LEFT_CENTER' : return google.maps.ControlPosition.LEFT_CENTER ;
		case 'LEFT_BOTTOM' : return google.maps.ControlPosition.LEFT_BOTTOM ;
		case 'BOTTOM_RIGHT' : return google.maps.ControlPosition.BOTTOM_RIGHT ;
		case 'BOTTOM_LEFT' : return google.maps.ControlPosition.BOTTOM_LEFT ;
		case 'BOTTOM_CENTER' : return google.maps.ControlPosition.BOTTOM_CENTER ;
		default : return google.maps.ControlPosition.RIGHT_TOP ;
	}
}

//地界随地图层级变化显示或隐藏
function changeRegionBoundariesDisplay(){
	var boundaries = jsmap.regionBoundaries;
	if(boundaries.length == 0){
		return;
	}
	var zoom = jsmap.zoom;
	if(zoom <= 3){    //显示国家级区域，隐藏省级区域
		for(var i=0; i<boundaries.length; i++){
			var polygon = boundaries[i].polygon;
			if(boundaries[i].type == '1' && polygon.getMap() == null){
				polygon.setMap(jsmap.getMap());
			}else if(boundaries[i].type == '2' && polygon.getMap() != null){
				polygon.setMap(null);
			}
		}
	}else{  //显示省级区域，隐藏国家级区域
		for(var i=0; i<boundaries.length; i++){
			var polygon = boundaries[i].polygon;
			if(boundaries[i].type == '1' && polygon.getMap() != null){
				polygon.setMap(null);
			}else if(boundaries[i].type == '2' && polygon.getMap() == null){
				polygon.setMap(jsmap.getMap());
			}
		}
	}
}
//标签随地图层级变化显示或隐藏
function changeStorageObjLabelDisplay(){
	var zoom = jsmap.zoom;
	var polygon = jsmap.storageObjPolygon;
	for(var key in polygon){
		if(polygon[key]){
			var lab = polygon[key].data.label;
			if(lab && lab != null){
				var range = lab.zoomRange;
				if(zoom>=range[0] && zoom<=range[1]){
					lab.show();
				}else{
					lab.hide();
				}
			}
		}
	}
}
//marker随地图层级变化显示或隐藏
function changeStorageMarkerDisplay(){
	var zoom = jsmap.zoom;
	var obj = $.extend({},
					   jsmap.storageWebcamMarker,//仓库摄像头图层
			           jsmap.storagePrinterMarker,//仓库打印机图层
			           jsmap.storageResourceMarker,//仓库打印机图层
			           jsmap.storageZoneDocksLayer,//仓库图层
			           jsmap.storageZonePersonLayer//仓库图层
			           );
	for(var key in obj){
		var marker = obj[key];
		if(marker.data && marker.data.range){
			var range = marker.data.range;
			if(zoom>=range[0] && zoom<=range[1]){
				marker.setVisible(true);
			}else{
				marker.setVisible(false);
			}
		}
	}
}
//获取kml图层信息
function getInfoFromKmlEvent(e){
	var desc = e.featureData.description;
	var d = desc.split("_");
	var name = e.featureData.name;
	var info = {
			psid : d[0],
			kmlName : d[1],
			areaName : desc.substr(d[0].length+d[1].length+2),
			objType : name.split("_")[0],
			objName : name.substr(name.indexOf("_")+1),
			latlng : e.latLng
	}
	return info;
}
//计算扇形路径
function computeSectorPath(option){
	var points = [];
	var point = option.latlng;
	var inRad = parseFloat(option.inner_radius)*0.3048;
	var outRad = parseFloat(option.outer_radius)*0.3048;
	var sDeg = parseInt(option.s_degree)%360;
	var eDeg = parseInt(option.e_degree)%360;
	var sides = parseInt(option.sides); //边数
	var p = "";
	var deg = eDeg - sDeg;
	if(inRad != 0){
		var s = parseInt(sides*inRad/outRad);
		if(s<1){
			s = 1;
		}
		for(var i=0; i<=s; i++){
			var d = sDeg + deg*i/s;
			p = computeOffsetDestination(point, inRad, d);
			points.push(p);
		}
	}else{
		points.push(point);
	}
	for(var i=sides; i>=0; i--){
		var d = sDeg + deg*i/sides;
		p = computeOffsetDestination(point, outRad, d);
		points.push(p);
	}
	points.push(points[0]);
	return points;
}
//重新加载图层 
function reloadLayer(psId,layers){
		clearStroageBounds(psId);//清除除location以外的 所以图层数据库查询数据
	if(layers.indexOf("parking")>-1||layers.indexOf("docks")>-1){
		clearStorageObj(layers.split(",")[1]);
		parkingDocksOccupancy();	
	}else{
		getStorageBoundsAjax(psId,"kml_storage","loc");
		getStorageBoundsAjax(psId,"kml_storage");
		//jsmap.clearStorageLayer(psId, layers);
		jsmap.loadStorageLayer(psId,layers);
	}
}
//================================= Util ================================
//计算面积
function computeArea(loop){
	return google.maps.geometry.spherical.computeArea(loop);
}
//计算路线长度
function computeLength(path){
	return google.maps.geometry.spherical.computeLength(path);
}
//计算方向
function computeHeading(from, to){
	return google.maps.geometry.spherical.computeHeading(from,to);  
}
//计算两点间距离(经纬度latlng)
function computeDistanceBetween(from, to){
	return google.maps.geometry.spherical.computeDistanceBetween(from,to);  
}
//计算两点间距离(像素点point)
function computeDistanceBetweenByPixel(from, to){
	var from = jsmap.fromContainerPixelToLatLng(from.x,from.y);
	var to = jsmap.fromContainerPixelToLatLng(to.x,to.y);
	return google.maps.geometry.spherical.computeDistanceBetween(from,to);  
}
//计算两像素点的平方根（距离）
function computeSquareRootByPixel(from, to){
	return Math.sqrt(Math.pow((to.x-from.x),2)+Math.pow((to.y-from.y),2));  
}
//计算起始点
function computeOffsetOrigin(to, distance, heading){
	return google.maps.geometry.spherical.computeOffsetOrigin(to, distance, heading);
}
//计算终止点
function computeOffsetDestination(from, distance, heading){
	if(heading < 0){
		heading +=180;
	}else{
		heading -=180;
	}
	return google.maps.geometry.spherical.computeOffsetOrigin(from, distance, heading);
}
//计算基于像素坐标是的终止点
function computePixelDestination(from, distance, heading){
	if(heading < 0){
		heading +=180;
	}else{
		heading -=180;
	}
	var x =from.x+distance*Math.sin(heading);
	var y =from.y+distance*Math.cos(heading);
	return jsNewPoint(x, y);
	
}
//计算像素点到另一像素点基于像素坐标系的方向
function computePixelHeading(from, to){
	return Math.asin((to.x-from.x)/(Math.sqrt(Math.pow((to.x-from.x),2)+Math.pow((to.y-from.y),2))))*180/Math.PI;
}
//判断latLng是否在polygon内
function containsLocation(latLng, polygon){
	return google.maps.geometry.poly.containsLocation(latLng, polygon);
}
//计算两条线路的交点latlng
function computeIntersection(path1,path2){
	var len1 = path1.length;
	var len2 = path2.length;
	if(len1<2 || len2<2){
		return null;
	}
	for(var i=0; i<len1-1; i++){
		for(var j=0; i<len2-1; i++){
			var x = [];
			var y = [];
			x.push(path1[i].lng());
			x.push(path1[i+1].lng());
			x.push(path2[j].lng());
			x.push(path2[j+1].lng());
			y.push(path1[i].lat());
			y.push(path1[i+1].lat());
			y.push(path2[j].lat());
			y.push(path2[j+1].lat());
			
			var k1 = (y[1]-y[0])/(x[1]-x[0]);
			var c1 = y[0]-k1.x[0];
			var k2 = (y[3]-y[2])/(x[3]-x[2]);
			var c2 = y[2]-k1.x[2];
			
			//直线交点
			var _x = (c1-c2)/(k2-k1);
			var _y = k1*_x + c1;
			
			if(((_x<=x[0] && _x>=x[1]) || (_x>=x[0] && _x<=x[1])) && 
					((_y<=y[0] && _y>=y[1]) || (_x>=y[0] && _y<=y[1])) &&
					((_x<=x[2] && _x>=x[3]) || (_x>=x[2] && _x<=x[3])) && 
					((_y<=y[2] && _y>=y[3]) || (_x>=y[2] && _y<=y[3]))){
				return jsNewLatLng(_y, _x);
			}
		}
	}
	return null;
}
//设置图标位置
function setMarkerPosition(marker,latlng){
	marker.setPosition(latlng);
}
//字符串转经纬度Latlng对象
function strToLatlng(str){
	var separator = ",";
	if(str.indexOf(",") == -1){
		separator = " ";
	}
	var lat = str.split(separator)[1];
	var lng = str.split(separator)[0];
	return jsNewLatLng(lat, lng);
}
//string路径转latlng路径
function stringToPath(str,separator){
	if(!separator){
		separator = " ";
	}
	var path = [];
	var ps = str.split(separator);
	for(var i=0; i<ps.length; i++){
		var latlng = strToLatlng(ps[i]);
		path.push(latlng);
	}
	return path;
}
//WKT转换为Googlemap路径
function wktToPath(geom){
	if(typeof geom == "object"){
		var temp = "";
		for(var i=0; i<geom.length; i++){
			temp += String.fromCharCode(geom[i]);
		}
		geom = temp;
	}
	var begin = geom.indexOf("(");
	var end = geom.lastIndexOf(")");
	var type = geom.substring(0, begin).toLowerCase();
	if(type == "multilinestring"){
		var paths = [];
		var ls = geom.substring(begin+1,end);
		while(ls){
			var path = [];
			var b = ls.indexOf("(");
			var e = ls.indexOf(")");
			var ps = ls.substring(b+1,e).split(",");
			for(var i=0; i<ps.length; i++){
				path.push(strToLatlng(ps[i]));
			}
			paths.push(path);
			if((e+1) == ls.length){
				ls = "";
			}else{
				ls = ls.substr(e);
			}
		}
		return paths;
	}else if(type == "point"){
		var pStr = geom.substring(begin+1,end);
		return strToLatlng(pStr);
	}
}
function reloadPerson(psId){
	var key =psId+"_person";
	jsmap.storageBounds[key]={};
	jsmap.clearZonePerson();
	jsmap.loadZonePerson(psId);
}
function reloadDock(psId){
	jsmap.clearZoneDocks();
	jsmap.loadZoneDocks(psId);
}