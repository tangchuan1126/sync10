/**
 * eleId 为验证元素的Id，eleId1为对勾的img ID， eleId2 为叉叉img的id reg正则表达式规则，url涉及后台数据重复性验证ajax路径，data Ajax请求数据格式为Json格式
 * @param eleId
 * @param eleId1
 * @param eleId2
 * @param reg
 * @param url
 * @param data
 * @return 1表示验证通过，0表示验证未通过，2表示验证失败！
 */  
function verify(eleId,eleId1,eleId2,reg,url,data,vMsg){
	var v_flag;
	var _eleId1=eleId1;
	var _eleId2=eleId2;
	  if($("#"+eleId).val()==""){
		  $("#"+eleId1).hide();
			  $("#"+eleId2).show();
			  $("#"+eleId2).attr("title",vMsg);
			 return 0;
	  }
	  if(!reg&&!url){
		  $("#"+_eleId2).show();
		  $("#"+_eleId1).hide();
		  return 2;
	  }
	  if(reg){
		  if(!reg.test($("#"+eleId).val())){
			  $("#"+_eleId2).show();
			  $("#"+_eleId1).hide();
			  if(vMsg){
				  $("#"+eleId2).attr("title",vMsg);
			  }else{
				  $("#"+eleId2).attr("title","Name is Error!");
			  }
			 
			  return 0;
		  }else {
			  $("#"+_eleId2).hide();
			  $("#"+_eleId1).show();
			  return 1;
		  }
	  }
	  
	  if(url){
	  $.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'post',
			async:false, 
			beforeSend:function(request){
		    },
			success:function(data){
				if(data.flag=="true"){
				 $("#"+_eleId2).hide();
				 $("#"+_eleId1).show();
				 v_flag= 1;
				}else{
				 $("#"+_eleId2).show();
				 $("#"+_eleId1).hide();
				 $("#"+eleId2).attr("title","Name is Repeat!");
				 v_flag= 0;
				}
			},
			error:function(){
				 $("#"+_eleId2).show();
				 $("#"+_eleId1).hide();
				 $("#"+eleId2).attr("title","Verify is Failed!");
				 v_flag= 2;
			}
		});
	  
	  }
	  return v_flag;
  }