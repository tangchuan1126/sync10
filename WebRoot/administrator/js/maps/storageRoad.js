var editRoad = {};
//编辑路径初始化
function initEditRoad(psId){
	if(!psId){
		alert("Please choose storage First!");
		return;
	}
	//只显示area图层
	layerSelectStatus(["area"],true,true,true);
	//加载主路
	loadRoadLayer(psId,{"main":true});
	//复制路线数据
	editRoad["road"] = $.extend(true,{},jsmap.storageBounds["road_"+psId]["main"]);
	editRoad["point"] = $.extend(true,{},jsmap.storageBounds["road_"+psId]["point"]);
	//初始化路径
	initRoadPath();
	//启动画图模式
	drawOnMap("road",onCreateRoad);
	//初始化新数据id
	editRoad["index_point"] = 0;
	editRoad["index_road"] = 0;
}
//初始化线路latlng路径
function initRoadPath(){
	var road = editRoad.road;
	for(var key in road){
		if(road[key] && road[key].geom){
			var path = wktToPath(road[key].geom);
			road[key].path = path[0];
		}
	}
	var point = editRoad.point;
	for(var key in point){
		if(point[key] && point[key].geom){
			var path = wktToPath(point[key].geom);
			point[key].path = path;
		}
	}
}
//画图
function onCreateRoad(path){
	var road = editRoad.road;
	var point = editRoad.point;
	
	//起始点在路口处理
	nearToExistsRoadPoint(path,10);
	
	var r = jsNewPolyline(path, "#91dafe",3,1);
	jsmap.mapToolObject.setPath(path);
}
//计算起始点是否在已有road point上
function nearToExistsRoadPoint(path,pix){
	var _pix = pix;
	if(pix==null && pix==undefined){
		_pix = 10;//默认10像素
	}
	//pix像素点在地图上的距离
	var l = computeDistanceBetweenByPixel(jsNewPoint(1, 1),jsNewPoint(1, _pix+1));
	var newpath = [];
	var point = editRoad.point;
	for(var i=0; i<path.length; i++){
		var isNew = true;
		for(var key in point){
			if(point[key] && point[key].path){
				var len = computeLength([path[i],point[key].path]);
				//pix像素以内算作同一point
				if(len < l){
					path[i] = point[key].path;
					isNew = false;
					break;
				}
			}
		}
		if(isNew){
			//新添加点   key以n开头表示new
			point["n_"+editRoad.index_point] = {"path" : path[i]};
			editRoad.index_point += 1;
		}
	}
	//return result;
}
//线路相交处理
function crossExistRoad(path){
	
}




