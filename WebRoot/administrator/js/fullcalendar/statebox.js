(function($,undefined){
	 
	$.widget("ui.stateBox",
	 /** @lends stateBox.prototype */	 
	{
		options:{
      
		state:"succeed",
	
		content:"�ύ�ɹ���",
	
		corner: false
		},
		_create:function(){
		},
		_init:function(){
			 
			var o = this.options,
				_self = this.element,
				_stateBox = $("<div />").addClass("ui-stateBox").html(o.content);
				_self.append(_stateBox);	
				_self.append("<sc"+"ript>removeBox();</scr"+"ipt>");
				if(o.corner){
					_stateBox.addClass("ui-corner-all");
				}
				if(o.state === "succeed"){
					_stateBox.addClass("ui-stateBox-succeed");
					setTimeout(removeBox,1500);
				}else if(o.state === "alert"){
					_stateBox.addClass("ui-stateBox-alert");
					setTimeout(removeBox,2000);
				}else if(o.state === "error"){
					_stateBox.addClass("ui-stateBox-error");
					setTimeout(removeBox,2800);
				}
				_stateBox.fadeIn("fast");
				function removeBox(){
					_stateBox.fadeOut("fast").remove();
				}
		},
		destroy:function(){
			
		}
	});
	
$.extend($.ui.stateBox, {
	version: "1.0"
});

})(jQuery);

 