function assembleTemplate(data, content){  
      var target;
      //if(typeof(data) == 'undefined' || data == null || data == ""){
      if(!data || !content){
        return content;
      }
      for(var i in data){    
        i = i.toUpperCase();
        var placeHolder = ["{"+i+"}", "$"+i+"$"];
        for(var t in placeHolder){
          var index = content.indexOf(placeHolder[t]);
          //在content中发现data中的key值
          if(index >= 0){        
            target = data[i];      
            content = content.replace(placeHolder[t], target);
          }
        }        
      }
      return content;
    }

/*
 * obj：         div等有html方法的jquery对象
 * length： 字体为20的内容最大长度
 */
function changFontSize(obj,length){
	var $obj = obj;
	var val = $obj.html().replace(/&nbsp;/g,"").trim();
	var size = 20;
	var count = ((val.length-length)/2)-0.5;
	for(var i=0;i<count;i++){
		size = size-1;
	}
//	console.log(size);
	if(val.length>0){
		$obj.html(val);
	}
	$obj.css("font-size",size+"px");
}


//设置clp打印的样式
function setClpPrintAll(obj){
	var $obj = obj;
	var html = $obj.html();
	
	setDisplay($("input[name='input_date']",$obj), $("span[name='dateShow']",$obj));//设置date，input隐藏div显示
	setDisplay($(".select2",$obj), "");//设置下拉框隐藏
	setDisplay("", $("div[name='show_title']",$obj));//设置title，select隐藏div显示
	setDisplay("", $("div[name='show_customer']",$obj));//设置customer，select隐藏div显示
		
	changFontSize($("div[name='show_lot']",$obj).find("center"),30);//修改lot字体大小
	changFontSize($("div[name='show_customer']",$obj).find("center"),10);//修改customer打印字体大小
	changFontSize($("div[name='show_title']",$obj).find("center"),10);//修改title打印字体大小
	
}


//设置tlp打印的样式
function setTlpPrintAll(obj){
	var $obj = obj;
	
	setDisplay($("input[name='input_date']",$obj), $("span[name='dateShow']",$obj));//设置date，input隐藏div显示
	setDisplay($("input[name='pcodeInput']",$obj), $("div[name='pcodeText']",$obj));//设置pcode，div显示，输入框隐藏
	setDisplay($(".select2",$obj), "");//设置下拉框隐藏
	setDisplay("", $("div[name='show_title']",$obj));//设置title，select隐藏div显示
	setDisplay("", $("div[name='show_customer']",$obj));//设置customer，select隐藏div显示
	
	changFontSize($("div[name='show_lot']",$obj).find("center"),25);//修改lot字体大小
	changFontSize($("div[name='show_customer']",$obj).find("center"),10);//修改customer打印字体大小
	changFontSize($("div[name='show_title']",$obj).find("center"),10);//修改title打印字体大小
	changFontSize($("div[name='show_totpdt']",$obj).find("center"),10);//修改Total-Products打印字体大小
	changFontSize($("div[name='show_totpkg']",$obj).find("center"),10);//修改Total-Packages打印字体大小
	
}



//设置显示和隐藏的样式，改变打印内容；打印时使用，设置打印内容 gql 2015/04/27
function setDisplay(hid, show){
	if(hid&&hid!=""){
		hid.css("display","none");
	}
	if(show&&show!=""){
		show.css("display","");
	}
}

/*
 * 调整替换值后条形码的位置,只调整60mmX30mm和80mmX40mm的标签
 */
function setImg(obj, code, lable_type){
	var $obj = obj;
	var $allImg = $obj.find("img");
	var limit_length = 0;
	var init_count = 0;
	if(lable_type.trim()=="80mmX40mm"){
		limit_length = 21;
		init_count = 3;
	}else if(lable_type.trim()=="60mmX30mm"){
		limit_length = 15;
		init_count = 6;
	}
//	console.log("lable_type="+lable_type);
	if(code.length<limit_length&&code.length>0){
		var count = limit_length-code.length;
		var m_left = count*6+init_count;
//		console.log(m_left);
		$allImg.each(function(i, elem){
//			console.log($(this));
			$(this).css("margin-left",m_left);
		});
	}else{
		$allImg.each(function(i, elem){
			$(this).css("margin-left",0);
		});
	}
	
	return $obj.html();
}