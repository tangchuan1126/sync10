(function($){
//
function init(_165)
{
	$(_165).appendTo("body");
	$(_165).addClass("menu-top");
	var _166=[];
	_167($(_165));

	for(var i=0;i<_166.length;i++)
	{
		var menu=_166[i];
		_168(menu);
		
		menu.find(">div.menu-item").each(function(){
			//alert("haha");
		_169($(this));
		});

		menu.find("div.menu-item").click(function(){
		if(!this.submenu){
		_170(_165);
		}
		return false;
		});
	}

	function _167(menu)
	{
		_166.push(menu);
		menu.find(">div").each(function(){
			var item=$(this);
			var _16a=item.find(">div");
			if(_16a.length){
				_16a.insertAfter(_165);
				item[0].submenu=_16a;
				_167(_16a);
			}
		});
	};

	function _169(item){
		item.hover(function(){
			item.siblings().each(function(){
				if(this.submenu){
					_172(this.submenu);
				}
				$(this).removeClass("menu-active");
			});
			item.addClass("menu-active");
			var _16b=item[0].submenu;
			if(_16b){
				var left=item.offset().left+item.outerWidth()-2;
				if(left+_16b.outerWidth()>$(window).width()){
					left=item.offset().left-_16b.outerWidth()+2;
				}
				_175(_16b,{left:left,top:item.offset().top-3});
			}
		},function(e){
		item.removeClass("menu-active");
		var _16c=item[0].submenu;
		
		if(_16c){
			if(e.pageX>=parseInt(_16c.css("left"))){
				item.addClass("menu-active");
			}
			else{
				_172(_16c);
			}
		}else{
			item.removeClass("menu-active");
		}
		});
	};


function _168(menu){
menu.addClass("menu").find(">div").each(function(){
var item=$(this);
if(item.hasClass("menu-sep")){
item.html("&nbsp;");
}else{
var text=item.addClass("menu-item").html();
item.empty().append($("<div class=\"menu-text\"></div>").html(text));
var icon=item.attr("icon");
if(icon){
$("<div class=\"menu-icon\"></div>").addClass(icon).appendTo(item);
}
if(item[0].submenu){
$("<div class=\"menu-rightarrow\"></div>").appendTo(item);
}
if($.boxModel==true){
var _16d=item.height();
item.height(_16d-(item.outerHeight()-item.height()));
}
}
});
menu.hide();
};
};
function _16e(e){
var _16f=e.data;
_170(_16f);
return false;
};
function _170(_171){
var opts=$.data(_171,"menu").options;
_172($(_171));
$(document).unbind(".menu");
opts.onHide.call(_171);
return false;
};

function _173(_174,pos){
var opts=$.data(_174,"menu").options;

if(pos){
opts.left=pos.left;
opts.top=pos.top;
}
_175($(_174),{left:opts.left,top:opts.top},function(){
$(document).bind("click.menu",_174,_16e);
opts.onShow.call(_174);
});
};
function _175(menu,pos,_176){
if(!menu){
return;
}
if(pos){
menu.css(pos);
}
menu.show(1,function(){
if(!menu[0].shadow){
menu[0].shadow=$("<div class=\"menu-shadow\"></div>").insertAfter(menu);
}
menu[0].shadow.css({display:"block",zIndex:$.fn.menu.defaults.zIndex++,left:menu.css("left"),top:menu.css("top"),width:menu.outerWidth(),height:menu.outerHeight()});
menu.css("z-index",$.fn.menu.defaults.zIndex++);
if(_176){
_176();
}
});
};
function _172(menu){
if(!menu){
return;
}
_177(menu);
menu.find("div.menu-item").each(function(){
	
if(this.submenu){
_172(this.submenu);
}
$(this).removeClass("menu-active");
});
function _177(m){
if(m[0].shadow){
m[0].shadow.hide();
}
m.hide();
};
};

$.fn.menu=function(_178,_179){
	
	if(typeof _178=="string"){
		switch(_178){
			case "show":
				return this.each(function(){
				_173(this,_179);
				});

			case "hide":
				return this.each(function(){
				_170(this);
				});
		}
	}

_178=_178||{};

return this.each(function(){
var _17a=$.data(this,"menu");

	if(_17a){
		$.extend(_17a.options,_178);

	}else{
		_17a=$.data(this,"menu",{options:$.extend({},$.fn.menu.defaults,_178)});
		init(this);

	}

	$(this).css({left:_17a.options.left,top:_17a.options.top});
});
};

$.fn.menu.defaults={zIndex:110000,left:0,top:0,onShow:function(){
},onHide:function(){
}};
})(jQuery);





(function($){
$.parser={parse:function(_1e9){
if($.parser.defaults.auto){
var r;
r=$(".easyui-menu",_1e9);
if(r.length){
r.menu();
}

}
}};
$.parser.defaults={auto:true};
$(function(){
$.parser.parse();
});
})(jQuery);







