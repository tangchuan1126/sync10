function highlight(match, keywords) {
	//keywords = keywords.split(' ').join('|');
	//keywords = keywords.split(/\s+|\*+|\/+/).join('|');
	
	keywords = keywords.replace("\"","");
	keywords = keywords.replace("\"","");
	
	return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
}

function addAutoComplete(jqObj,jsonUrl,labelKey,valueKey){  //NOTE: jqObj must be a jQuery object
	
	jqObj.autocomplete({ 
		source: function( request, response ) {
			
			$.ajax({
				url: jsonUrl,
				dataType: "json",
				data: {
					q:jqObj.attr("value") ? jqObj.attr("value") : jqObj.val()
				},
				success: function( data ) {
					
				
					response( $.map( data, function( item ) {
						return {
							label:highlight(item[labelKey],jqObj.attr("value") ? jqObj.attr("value") : jqObj.val()),
							value: item[valueKey ? valueKey: labelKey ]
						};
					}));
				}
			});
		},
		dataType: 'json', 
		html:true
	}).autocomplete( "instance" )._renderItem = function(ul,item) {
		return $( "<li>" ).html(item.label).appendTo( ul );
 	};
}
