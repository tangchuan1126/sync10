 /**
 * 拿到数据过后的处理
 * 0.回来的数据，先添加到bufferData当中.当可以添加到data中的时候在添加进去
 * 1.生成HTML，隐藏起来，如果查看页面是打开的，那么添加显示，如果没有那么就显示一个【待打印的数量】
 * 2.如果想立即的添加显示出来，那么必须当前没有在打印的中，在打印这条单据完成过后，在把数据填充进去，
 * 正在打印的时候，是不允许重新刷新数据的
 */
function PrintTask(_showTitle,_basePath,window_){
	this.data = [] ;
	this.bufferData =  [] ;
	this.printFailedData = [] ;
	this.printIndex =  0;
	this.isInPrint = false ;
	this.isPrintOver = true ;
	this.showTitle = _showTitle ;
	this.basePath = _basePath ;
	this.w = window_ ;
 

	//判断是否有数据在buffer中
	this.isDataInBuffer = function(){
		return this.bufferData && this.bufferData.length > 0 ;
	}
	//
	this.appendData = function(appendData){
		this.bufferData.push(appendData);
 		if(this.isPrintOver){				//如果已经结束打印了,那么开启打印.
			this.initData(this.bufferData);
		}
	}
	this.initData = function(refreshData){
 
  		if(refreshData && refreshData.length> 0){
  			this.data =  refreshData ;
  			this.bufferData =  [] ;
  			this.printIndex = 0 ;
  			this.isPrintOver = false ;
  			this.showInitHtml();
  			this.getPrintHtml();
		}else{
			this.isPrintOver = true ;
		}
	}
	/**
	 * 把data里面的数据展示到Html
	 */
	this.showInitHtml = function(){
		var $node = $("#print");
		if(this.data.length > 0 ){
			$node.html("");
			for(var index = 0 , count = this.data.length ; index < count ; index++ ){
				var task = this.data[index];
				var copy = $("#will_print_template").clone() ;
				$node.append(this.formateWillPrintTaks(task, copy).css("display","block"));
			}
		}else{
			$node.html("<p style='padding:10px;border:1px dashed silver'>No Records</p>");
		}

	}
	this.formateWillPrintTaks = function(task,node){
		node.attr("id","will_print_template_"+task["print_task_id"]);
 		for(var attr in task){
			if(attr === "state"){ //state 另外的处理显示的值
				$(".task_"+attr,node).html("WillPrint");
			}else{
				$(".task_"+attr,node).html(task[attr]);
			}
		}
 		$(".start",node).attr("task_print_task_id",task["print_task_id"])
		return node ;
	}
	
	this.setShowTitleHtml = function(){
		var showValue = this.data.length - this.printIndex ;
 		this.showTitle.html(showValue >= 0 ? showValue+"":"0");
	}
	
	this.getPrintData = function(){
		
		if(this.isDataInBuffer()){
			for(var index = 0 , count = this.bufferData.length ; index < this.bufferData ; index++ ){
				this.data.push(this.bufferData[index]);
			}
			this.bufferData = [] ;
		}
 		this.setShowTitleHtml();
		if(this.data.length > 0 && this.printIndex < this.data.length){
			this.isInPrint = true ;
			return this.data[this.printIndex];
		} 
		
		this.isPrintOver = true ;
		this.isInPrint = false ;
 		$("#print").html("<p style='padding:10px;border:1px dashed silver'>No Records</p>");
		return undefined ;
	}
	this.printHtml = function(html){
		$('#androidPrint').html(html);
		var printData = this.getPrintData();
		$("#url_path").val(printData["url"]);
		//alert($("#url_path").val() +".............................")
		 var flag = supportAndroidprint();
 		 if(flag){
			this.updatePrintTaskState(1);
		 }else{
			this.updatePrintTaskState(0);
		 }
		 $('#androidPrint').html("");
	}
	this.updatePrintTaskState = function(isSuccess){
		var _this =  this ;
		 var task = this.getPrintData();
		 if(task){
			 var parentNode = $("#will_print_template_"+task["print_task_id"]);
			 $(".task_state",parentNode).html(isSuccess == 1 ? "Success" : "Fail");
	 		 $.ajax({
				 url:_this.basePath+'action/checkin/PrintTaskOperAction.action',
				 data:'isSuccess='+isSuccess+"&task_id="+task["print_task_id"]+"&Method=print",
				 dataType:'json',
				 success:function(data){
					 if(data && data.ret * 1 == 1){
						 if(isSuccess == 0){ 	//添加到打印失败的列表中
							 _this.appendFailedData(task);
						 }
					 }
				 },
	 			 complete:function(XHR, TS){
	   				 _this.printIndex++ ;
					 _this.isInPrint =  false ;
					  parentNode.slideToggle("slow",function(){this.remove();});
					  _this.w.clearInterval(task.timeId);
					 _this.getPrintHtml(); 			//继续打印
	 			}
			 })
		 }
	}
	this.handSuccess = function(){
		var printObj = this.data[this.printIndex];
		if(printObj && printObj.html && printObj.html.indexOf("page_error") == -1 && printObj.html.indexOf("zhangrui_self") == -1 ){
 			this.printHtml(printObj.html);
			return ;
		}
		this.updatePrintTaskState(0);
  	}
	 
	//改变当前的printTask, 显示出来他正在打印当中
	this.printTaskInPrint = function(printTask){
		var node = $("#will_print_template_"+printTask["print_task_id"]);
		$(".state_div",node).effect("shake",{direction:'left',distance:10,times:10});
		$(".task_state",node).html("Printing...");
	}
	this.startPrintLabel = function(printLabel){
		printLabel.printTime = 0 ;	//设置当前打印，花费的时间
		_this = this ;
		printLabel.timeId = this.w.setInterval(function(_this){
 			printLabel.printTime ++ ;
 			if(printLabel.printTime > 30){	//如果一个任务打印了20s，没有成功那么就是任务打印失败。让后面的任务先
 				staticPrintFailed();
			}	
		},1000);
	}
	//打印当前数据中的data,发起Ajax请求去请求一个Html，回来然后打印
	this.getPrintHtml = function(){
		var _this = this ;
		var _printData = this.getPrintData();
	
 		if(_printData){
 
 			this.startPrintLabel(_printData) ;
 			this.printTaskInPrint(_printData);
  			$.ajax({
				url:_printData["url"],
 				dataType:'html',
				success:function(html){
					 _printData.html = html ;
				},
				complete:function(XHR, TS){
				   _this.handSuccess();
 				}
				
			})
		}
		
	} 
	/**
	 * 根据print_task_id 去获取打印的obj
	 */
	this.getPrintTaskById = function(id){
		if(this.data && this.data.length > 0 ){
			for(var index = 0 , count = this.data.length ; index < count ; index++ ){
				if(this.data[index]["print_task_id"] * 1 == id){
					return this.data[index] ;
				}
			}
		}
		return undefined ;
	}
	this.appendFailedData = function(obj){
		this.printFailedData.unshift(obj);
		var $node = $("#failed");
		var copy = $("#will_print_failed_template").clone() ;
 
		$node.prepend(this.formatePrintFailedTask(obj, copy).slideToggle("slow"));
		
		this.appendFailedNumber(1);
	}
	/**
	 * 可以是 -1
	 */
	this.appendFailedNumber = function(appendNumber){
		var ora = $("#print_failed_count").html() * 1;
		var total = ora+appendNumber * 1 ;
		$("#print_failed_count").html(total+"");
		if(total < 1){
			$("#failed").html("<p style='padding:10px;border:1px dashed silver'>No Records</p>");
		}
	}
	this.setFailedNumber = function(number){
		$("#print_failed_count").html(number * 1);
		var fixNumber = number * 1 ;
		if(fixNumber < 1){
			$("#failed").html("<p style='padding:10px;border:1px dashed silver'>No Records</p>");
		}
	}
	/**
	 * 初始化打印失败的数据
	 * 1.在打印的时候，失败的任务会放到打印失败的列表当中
	 */
	this.initFailedData = function(failedData){
 
 		  this.printFailedData = failedData ;
		  this.setFailedNumber(failedData.length * 1)
		  var $node = $("#failed");
		  $node.html("");
		  for(var index = 0 , count = this.printFailedData.length ; index < count ; index++ ){
				var task = this.printFailedData[index];
				var copy = $("#will_print_failed_template").clone() ;
 				$node.append(this.formatePrintFailedTask(task, copy).css("display","block"));
		  }
	 
	}
	this.formatePrintFailedTask = function(task,node){
		node.attr("id","will_print_failed_template_"+task["print_task_id"]);
 		for(var attr in task){
 			if(attr === "state"){ //state 另外的处理显示的值
				$(".task_"+attr,node).html("Print Fail");
			}else{
				$(".task_"+attr,node).html(task[attr]);
			}
 		}
 		$(".start",node).attr("task_print_task_id",task["print_task_id"])
		return node ;
	}
	this.getPrintFailedTaskById = function(print_task_id){
		if(this.printFailedData && this.printFailedData.length > 0 ){
			for(var index = 0 , count = this.printFailedData.length ; index < count ; index++ ){
				if(this.printFailedData[index]["print_task_id"] * 1 == print_task_id){
					return this.printFailedData[index] ;
				}
			}
		}
		return undefined ;
	}
	/**
	 * 打印失败的 重新打印的
	 * 1.获取数据出来，然后加入到待打印的列表
	 * 2.同时removePrintFailedTask
	 */
	this.printFailedReprint = function(print_task_id){
		var task = this.getPrintFailedTaskById(print_task_id);
		if(task){
			this.appendData(task);
			this.removePrintFailedTask(print_task_id);
		}
	}
	/**
	 * 移除一个打印失败的 同时 数量 -1
	 * 同时在printFailed里面的数据 
	 */
	this.removePrintFailedTask = function(print_task_id){
		 var repeateArray = [] ;
		 for(var index = 0  , count = this.printFailedData.length ; index < count ; index++ ){
			 var obj = this.printFailedData[index];
			 if(obj["print_task_id"] * 1 !=  print_task_id * 1){
				 repeateArray.push(obj);
			 }
		 }
		 this.printFailedData = repeateArray ;
		 var parentNode = $("#will_print_failed_template_"+print_task_id);
		 parentNode.slideToggle("slow");
		 this.appendFailedNumber(-1);
	}
}