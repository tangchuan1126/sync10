 /**
 * 拿到数据过后的处理
 * 0.回来的数据，先添加到bufferData当中.当可以添加到data中的时候在添加进去
 * 1.生成HTML，隐藏起来，如果查看页面是打开的，那么添加显示，如果没有那么就显示一个【待打印的数量】
 * 2.如果想立即的添加显示出来，那么必须当前没有在打印的中，在打印这条单据完成过后，在把数据填充进去，
 * 正在打印的时候，是不允许重新刷新数据的
 */
function PrintTask(_showTitle,_basePath){
	this.data = [] ;
	this.bufferData =  [] ;
	this.printIndex =  0;
	this.isInPrint = false ;
	this.isPrintOver = true ;
	this.showTitle = _showTitle ;
	this.basePath = _basePath ;
	 
	
  
 
	this.isDataInBuffer = function(){
		return this.bufferData && this.bufferData.length > 0 ;
	}
	 
	this.appendData = function(appendData){
		this.bufferData.push(appendData);
		if(this.isPrintOver){				//如果已经结束打印了,那么开启打印.
			this.initData(this.bufferData);
		}
	}
	this.initData = function(refreshData){
  		if(refreshData && refreshData.length> 0){
  			this.data =  refreshData ;
  			this.bufferData =  [] ;
  			this.printIndex = 0 ;
  			this.isPrintOver = false ;
  			this.getPrintHtml();
		}else{
			this.isPrintOver = true ;
		}
	}
	this.setShowTitleHtml = function(){
 		this.showTitle.html(this.data.length - this.printIndex);
	}
	
	this.getPrintData = function(){
		
		if(this.isDataInBuffer()){
			for(var index = 0 , count = this.bufferData.length ; index < this.bufferData ; index++ ){
				this.data.push(this.bufferData[index]);
			}
			this.bufferData = [] ;
		}
 		this.setShowTitleHtml();
		if(this.data.length > 0 && this.printIndex < this.data.length){
			this.isInPrint = true ;
			return this.data[this.printIndex];
		} 
		
		this.isPrintOver = true ;
		this.isInPrint = false ;
		return undefined ;
	}
	this.printHtml = function(html){
		$('#androidPrint').html(html);
		 var flag = supportAndroidprint();     //调页面打印方法
		 if(flag){
			this.updatePrintTaskState(1);
		 }else{
			this.updatePrintTaskState(0);
		 }
		 $('#androidPrint').html("");
	}
	this.updatePrintTaskState = function(isSuccess){
		var _this =  this ;
		 var task = this.getPrintData();
 		 $.ajax({
			 url:_this.basePath+'action/checkin/PrintTaskOperAction.action',
			 data:'isSuccess='+isSuccess+"&task_id="+task["print_task_id"]+"&Method=print",
			 dataType:'json',
 			 complete:function(XHR, TS){
 				 _this.printIndex++ ;
				 _this.isInPrint =  false ;
				 _this.getPrintHtml(); 			//继续打印
 			}
		 })
	}
	this.handSuccess = function(){
		var printObj = this.data[this.printIndex];
		if(printObj && printObj.html && printObj.html.indexOf("page_error") == -1 ){
 			this.printHtml(printObj.html);
			return ;
		}
		this.updatePrintTaskState(0);
  	}
	 
	//打印当前数据中的data,发起Ajax请求去请求一个Html，回来然后打印
	this.getPrintHtml = function(){
		var _this = this ;
		var _printData = this.getPrintData();
 		if(_printData){
  			$.ajax({
				url:_printData["url"],
 				dataType:'html',
				success:function(html){
					_printData.html = html ;
				},
				complete:function(XHR, TS){
				   _this.handSuccess();
 				}
				
			})
		}
		
	} 
}