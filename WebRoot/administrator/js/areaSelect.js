function getAreaCountryByCaId(ca_id,systenFolder)
{

		$.getJSON("/Sync10/action/administrator/product/getAreaCountrysJSON.action",
				{ca_id:ca_id},
				function callback(data)
				{ 
					$("#ccid_hidden").clearAll();
					$("#ccid_hidden").addOption("全部国家","0");
					
					if (data!="")
					{
						$("#ccid_hidden_chzn").css("display","");
					    $("#pro_id_chzn").css("display","none");
						$("#pro_id").clearAll();
						
						$.each(data,function(i){
							$("#ccid_hidden").addOption(data[i].c_country,data[i].ccid);
						});
						$("#ccid_hidden").trigger("liszt:updated");//同步数据
						
					}
					else
					{
					
					 
						$("#ccid_hidden_chzn").css("display","none");
						$("#pro_id_chzn").css("display","none");
						
					}
				}
		);
}

//国家地区切换
function getStorageProvinceByCcid(ccid)
{


		$.getJSON("/Sync10/action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("全部区域","0");
					
					if (data!="")
					{
					$("#pro_id_chzn").css("display","");
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					else
					{
						$("#pro_id_chzn").css("display","none");
					}
					$("#pro_id").trigger("liszt:updated");//同步数据
						
					
					if (ccid>0)
					{
						$("#pro_id").addOption("Others","-1");
					}
				}
		);
}
jQuery(function($){
	$("#ccid_hidden").addClass("chzn-select");
	$("#pro_id").addClass("chzn-select");
	$("#sale_area").addClass("chzn-select");
	$("#sale_area").css("width","150px");
	$("#ccid_hidden").css("width","180px");
	$("#pro_id").css("width","180px");
	$("#sale_area").chosen();
	$("#ccid_hidden").chosen();
	$("#pro_id").chosen();
    $("#ccid_hidden_chzn").css("display","none");
    $("#pro_id_chzn").css("display","none");

});