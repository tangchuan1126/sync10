
function openOfficeFileOnline(_this,ul,configPath){
    var node = $(_this);
    var file_id = node.attr("file_id");
    var file_is_convert = node.attr("file_is_convert");
    if(file_is_convert * 1 == 1){
	    if(window.top && window.top.openOfficeFileOnlineShow){
			window.top.openOfficeFileOnlineShow(file_id);
		}else{
			openArtOfficeOnlineShow(file_id,ul);
		}
	}else{
	    ajaxConvertFile(file_id,node,ul,configPath);
	}
}
function ajaxConvertFile(file_id,node,ul,configPath){
    var o = {
	    file_id:file_id,
	    table_name:'file',
	    path:configPath ,
	}
    $.ajax({
		url:ul + 'action/administrator/file/ConvertFileAction.action',
		data:jQuery.param(o),
		dataType:'json',
		beforeSend:function(request){
     		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
				if(data && data.flag === "success"){
				    node.attr("file_is_convert","1");
				    
				    if(window.top && window.top.openOfficeFileOnlineShow){
						window.top.openOfficeFileOnlineShow(file_id);
					}else{
						openArtOfficeOnlineShow(file_id,ul);
					}
				}else{
					if(data.message && data.message.length > 0 ){alert(data.message);}
					else{alert("转换文件失败");}
				}
		},
		error:function(){
		    $.unblockUI();
		    alert("系统错误");
		}
	})
}
function openArtOfficeOnlineShow(file_id,ul){
    var uri = ul + "administrator/customerservice_qa/show_file_online.html?file_id="+file_id; 
	 $.artDialog.open(uri , {title: '文件查看',width:'800px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function openArtPictureOnlineShow(obj,ul){
    if(!obj) {return ;}
    var param = jQuery.param(obj);
    var fileWithType = obj.file_with_type;
    var uri = ul + "administrator/file/picture_online_show.html?" +param; 
    if(fileWithType==52){  //如果是checkIn模块使用新的图片查看页面 ----wfh
    	uri = ul + "administrator/file/picture_online_show_check_in.html?" +param; 
    }
    $.artDialog.open(uri , {title: 'Photo View',width:'1100px',height:'600px', lock: true,opacity: 0.3,fixed: true});
}




function openOfficeFileOnlineReturnFile(_this,ul,configPath){
    var node = $(_this);
    var file_id = node.attr("file_id");
    var file_is_convert = node.attr("file_is_convert");
    if(file_is_convert * 1 == 1){
	    if(window.top && window.top.openOfficeFileOnlineShow){
			window.top.openOfficeFileOnlineShow(file_id);
		}else{
			openArtOfficeOnlineShow(file_id,ul);
		}
	}else{
		ajaxConvertFileReturnFile(file_id,node,ul,configPath);
	}
}
function ajaxConvertFileReturnFile(file_id,node,ul,configPath){
    var o = {
	    file_id:file_id,
	    table_name:'file',
	    path:configPath ,
	}
    $.ajax({
		url:ul + 'action/administrator/return_order/ConvertReturnOrderFileAction.action',
		data:jQuery.param(o),
		dataType:'json',
		beforeSend:function(request){
     		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
				if(data && data.flag === "success"){
				    node.attr("file_is_convert","1");
				    
				    if(window.top && window.top.openOfficeFileOnlineShow){
						window.top.openOfficeFileOnlineShow(file_id);
					}else{
						openArtOfficeOnlineShow(file_id,ul);
					}
				}else{
					if(data.message && data.message.length > 0 ){alert(data.message);}
					else{alert("转换文件失败");}
				}
		},
		error:function(){
		    $.unblockUI();
		    alert("系统错误");
		}
	})
}
