//
// global variables
//
var isMozilla;
var objDiv = null;
var originalDivHTML = "";
var DivID = "drilledDownWindow";
var over = false;
var mouseX =0;
var mouseY =0;
var interval1=null;

var mouseDrillX , mouseDrillY;
var winIX, winIY, winL,winT,winW,winH, winIW, winIH, count;
//
// dinamically add a div to 
// dim all the page
//
var setWT=97, setWL=17, setWW=960, setWH=455;

function setWindow(t,l,w,h){
	setWT=t;//97;
	setWL=l;//17;
	setWW=w;//960;
	setWH=h;//455;
}

function zoomWindow(){
	mouseDrillX = mouseX;
	mouseDrillY = mouseY;
	winL = parseInt(mouseX);
	winT = parseInt(mouseY);
	winIX = (winL-15)/10;
	winIY = (winT-100)/10;
	count=0;
	//winIW =960/10;
	//winIH =460/10;
	winIW =setWW/10;
	winIH =(setWH)/10;
	winW =0;
	winH =0;
	interval1 = window.setInterval(zoomInterval,70);
	
	
}


function zoomInterval(){
	if (count<9){
		winL -= winIX;
		winT -= winIY;
		winW += winIW;
		winH += winIH;
		
		document.getElementById(DivID).style.position = "absolute";
		document.getElementById(DivID).style.left = winL+"px";
		document.getElementById(DivID).style.top = winT+"px";
		document.getElementById(DivID).style.width = winW+"px";
		document.getElementById(DivID).style.height = winH+"px";
		document.getElementById(DivID).style.visibility = "visible";
		document.getElementById(DivID).style.zIndex = "10002";
		document.getElementById("dimmer").style.filter="progid:DXImageTransform.Microsoft.Alpha(opacity="+(count*6)+")";
		document.getElementById("dimmer").style.MozOpacity =count/10;
		document.getElementById("dimmer").style.opacity =count/10;
		
	}
	else
	{
		window.clearInterval(interval1);
		interval1=null;
		document.getElementById(DivID).style.position = "absolute";
		document.getElementById(DivID).style.left = setWL+"px";//"15px";
		document.getElementById(DivID).style.top = setWT+"px";//"100px";
		document.getElementById(DivID).style.width = setWW+"px";//"960px";
		document.getElementById(DivID).style.height = setWH+"px";//"455px";

		if(isMozilla || isNetscape){
			document.getElementById("pmIdUSDrillDown").style.MozOpacity="1";
			document.getElementById("pmIdUSDrillDown").style.opacity="1";
			document.getElementById("pmIdUSDrillDown").style.width = 760+"px";
			document.getElementById("pmIdUSDrillDown").style.height = 433+"px";

		}

		
	}
	count++;

}



//
//
function displayFloatingDiv(divId,dimmerTop, dimmerLeft, dimmerWidth, dimmerHeight) 
{
	DivID = divId;

	document.getElementById('dimmer').style.position = "absolute";
	document.getElementById('dimmer').style.left =dimmerLeft+"px";// "9px";
	document.getElementById('dimmer').style.top = dimmerTop+"px";//"98px";
	document.getElementById('dimmer').style.width = dimmerWidth+"px";//"975px";
	document.getElementById('dimmer').style.height = dimmerHeight+"px";//"430px";
	document.getElementById('dimmer').style.visibility = "visible";
	document.getElementById('dimmer').style.zIndex = "10000";

	var addHeader;
	
	if (originalDivHTML == "")
	    originalDivHTML = document.getElementById(divId).innerHTML;
	
	addHeader = '<table style="width:100%;" class="floatingHeader textBoldLight" >'+
       '<tr>'+
	   	'<td style="height:22px;cursor:move;" onDblClick="void(0);" onMouseOver="over=true;" onMouseOut="over=false;" valign="middle"><div id="pmDrillDownTitle" style="height:16px;overflow:hidden;cursor:move;" ></div></td>'+
        '<td style="width:14px;" align="left" valign="middle">'+
			'<a href="javascript:closeWindow(\''+divId+'\');void(0);">'+
			   '<img alt="Close" title="Close" src="FW/close.gif" border="0">'+
			 '</a>'+
		'</td></tr></table>';

    // add to your div an header
	
	document.getElementById(divId).innerHTML = addHeader + originalDivHTML;
	
	
}


//

function zoomOutInterval(){
	if (count>1){

		winL += winIX;
		winT += winIY;
		winW -= winIW;
		winH -= winIH;
		
		document.getElementById(DivID).style.position = "absolute";
		document.getElementById(DivID).style.width = winW+"px";
		document.getElementById(DivID).style.height = winH+"px";
		document.getElementById(DivID).style.left = winL+"px";
		document.getElementById(DivID).style.top = winT+"px";
		document.getElementById("dimmer").style.filter="progid:DXImageTransform.Microsoft.Alpha(opacity="+(count*6)+")";

		document.getElementById("dimmer").style.MozOpacity =(count)/10;
		document.getElementById("dimmer").style.opacity =(count)/10;
	}
	else{
		window.clearInterval(interval1);
		interval1=null;
		document.getElementById(DivID).innerHTML = originalDivHTML;
		document.getElementById(DivID).style.position = "absolute";
		document.getElementById(DivID).style.width = "1px";
		document.getElementById(DivID).style.height = "1px";
		document.getElementById(DivID).style.left = "1000px";
		document.getElementById(DivID).style.top = "1px";
		document.getElementById(DivID).style.visibility="hidden";
	
		document.getElementById('dimmer').style.position = "absolute";
		document.getElementById('dimmer').style.width = "1px";
		document.getElementById('dimmer').style.height = "1px";
		document.getElementById('dimmer').style.left = "1000px";
		document.getElementById('dimmer').style.top = "1px";
		document.getElementById('dimmer').style.visibility = "hidden";
		
		DivID = "";
	}
	count--;

}


//
//
function closeWindow(divId) 
{
	if(isMozilla || isNetscape){
		document.getElementById("pmIdUSDrillDown").style.MozOpacity="0";
		document.getElementById("dimmer").style.opacity="0";
		document.getElementById("pmIdUSDrillDown").style.width = 1+"px";
		document.getElementById("pmIdUSDrillDown").style.height = 1+"px";
		document.getElementById("pmIdUSDrillDown").style.visibility ="hidden";
	}

	if(interval1==null){
		interval1 = window.setInterval(zoomOutInterval,70);
	
		
	}
}

//
//
//
function MouseDown(e) 
{
    if (over)
    {
        if (isMozilla) {
            objDiv = document.getElementById(DivID);
            X = e.layerX;
            Y = e.layerY;
            return false;
        }
        else {
            objDiv = document.getElementById(DivID);
            objDiv = objDiv.style;
            X = event.offsetX;
            Y = event.offsetY;
        }
    }
}


//
//
//
function MouseMove(e) 
{
    
		if (isMozilla) {
               	mouseX = e.pageX;
            	mouseY = e.pageY;
        }
        else {
	           	mouseX = event.clientX;
    	       	mouseY = event.clientY;
		}
		
		
		
	if (objDiv) {
        if (isMozilla) {
            objDiv.style.top = (e.pageY-Y) + 'px';
            objDiv.style.left = (e.pageX-X) + 'px';
            return false;
        }
        else 
        {
            objDiv.pixelLeft = event.clientX-X + document.body.scrollLeft;
            objDiv.pixelTop = event.clientY-Y + document.body.scrollTop;
            return false;
        }
    }
}

//
//
//
function MouseUp() 
{
    objDiv = null;
}


//
//
//
function init()
{
    // check browser
    isMozilla = (document.all) ? 0 : 1;


    if (isMozilla) 
    {
        document.captureEvents(Event.MOUSEDOWN | Event.MOUSEMOVE | Event.MOUSEUP);
    }

    document.onmousedown = MouseDown;
    document.onmousemove = MouseMove;
    document.onmouseup = MouseUp;

    // add the div used to dim the main map


}

// call init
init();
