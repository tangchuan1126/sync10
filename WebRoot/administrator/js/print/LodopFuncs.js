﻿function getLodop(oOBJECT,oEMBED){
/**************************
  本函数根据浏览器类型决定采用哪个对象作为控件实例：
  IE系列、IE内核系列的浏览器采用oOBJECT，
  其它浏览器(Firefox系列、Chrome系列、Opera系列、Safari系列等)采用oEMBED。
**************************/
        var strHtml1="<br><font color='#FF00FF'>打印控件未安装!点击这里<a href='/install_lodop.exe'>执行安装</a>,安装后请刷新页面或重新进入。</font>";
        var strHtml2="<br><font color='#FF00FF'>打印控件需要升级!点击这里<a href='/install_lodop.exe'>执行升级</a>,升级后请重新进入。</font>";
        var strHtml3="<br><br><font color='#FF00FF'>(注：如曾安装过Lodop旧版附件npActiveXPLugin,请在【工具】->【附加组件】->【扩展】中先卸载它)</font>";
        var LODOP=oEMBED;	
        //alert(strHtml1);
        
        var html='<div style="width:100%; height:80px; background-image:url(/Sync10/administrator/js/print/lodop_bg.jpg); position:fixed; top:0; left:0;" onclick="closeControl(this)" align="center">'+
		         	'<table  border="0" cellspacing="0" cellpadding="0">'+
				        '<tr>'+
				          '<td> '+
				          '<font color="#FF00FF" style="font-family:Verdana;">'+
				          	'The print control is not installed!<br />'+
				           	'After installation, please refresh the page or enter.'+
				                '</font>'+
				          '</td>'+
				          '<td align="center" width="150px;">'+
				            '<font color="#FF00FF" style="font-family:Verdana;">'+
				          	 '<a href="/install_lodop32.exe" style="text-decoration:none;outline:none;" ><img src="/Sync10/administrator/js/print/installation.jpg"  /></a><br />'+
				                'Installation'+
				             '</font>'+    
				          '</td>'+
				            '<td align="center">'+
				            '<font color="#FF00FF" style="font-family:Verdana;">'+
				               '<a href="/install_lodop32.exe" style="text-decoration:none;outline:none;"><img src="/Sync10/administrator/js/print/uninstall.jpg" /></a><br />'+
				                'Uninstall'+
				             '</font>'+    
				          '</td>'+
				        '</tr>'+
				      '</table>'+
				   '</div>';
      
	try{		 

		console.log(LODOP);
		console.log(typeof(LODOP.VERSION));
		
	     if (navigator.appVersion.indexOf("MSIE")>=0) LODOP=oOBJECT;
	  
	     if ((LODOP==null)||(typeof(LODOP.VERSION)=="undefined")) {
	    	 
			 if (navigator.userAgent.indexOf('Firefox')>=0){    //判断是否为火狐
				 if($('#lodopIsShow').val()!=1){    			//判断是否安装了控件
			    	   $("body").append(html);
			     }
			 }
			 
			 if(navigator.userAgent.indexOf('Chrome')>=0){      //谷歌浏览器
				 if($('#lodopIsShow').val()!=1){
			    	   $("body").append(html);
			     }
			 }
			 
			 if (navigator.appVersion.indexOf("MSIE")>=0){     //IE浏览器
				 if($('#lodopIsShow').val()!=1){
			    	   $("body").append(html);
			     }
			 }
			 
			 
	  	        // document.documentElement.innerHTML=strHtml3+document.documentElement.innerHTML;
//			 if (navigator.appVersion.indexOf("MSIE")>=0){ 
//				 alert("22");
//				// document.write(strHtml1); 
//				 if($('#lodopIsShow').val()!=1){
//			    	   $("body").append(html);
//			      }
//			 }else{
//				// document.documentElement.innerHTML=strHtml1+document.documentElement.innerHTML;
//				 alert("333");
//				 if($('#lodopIsShow').val()!=1){
//			    	   $("body").append(html);
//			       }
//			 }	 
////			 if(navigator.userAgent.indexOf('Chrome')>=0){
////				 if($('#lodopIsShow').val()!=1){
////			    	   $("body").append(html);
////			       }
////			 }
			 return LODOP;
	     }else if (LODOP.VERSION<"6.0.2.4") {                //判断版本的
//			 if (navigator.appVersion.indexOf("MSIE")>=0){ 
//				// document.write(strHtml2); 
//			 }else{
//				 
//			 }
				
			 // document.documentElement.innerHTML=strHtml2+document.documentElement.innerHTML;
	     }
	     
	     
	     //*****如下空白位置适合调用统一功能:*********
	     LODOP.SET_LICENSES("北京微尘嘉业科技有限公司","BC1442938EE17DECE4254F4CA683CA08","北京微塵嘉業科技有限公司","A5696C79F6C63B7B292F5C87FC1D40C9");
	     LODOP.SET_LICENSES("THIRD LICENSE","","Beijing JIAYE Dust Technology Co. Ltd.","C6A22411DA7656EA2EE9A19CA9CECC3F");
	     //LODOP.SET_LICENSES("","695941044795108109561289003863","688858710010010811411756128900","642504549495252565612890038639"); 
	     //*******************************************
	     return LODOP; 
	}catch(err){
	     document.documentElement.innerHTML="Error:"+strHtml1+document.documentElement.innerHTML;
	     return LODOP; 
	}
}

function closeControl(own){
	$(own).remove();
}
