﻿$().ready(function()
{
	visionariPrinter = getLodop(document.getElementById('LODOP'),document.getElementById('LODOP_EM'));  
	keepAlive();
	
	try
	{
		initPrint();
	}
	catch (e)
	{
	}
});

//var visionariPrinter; //声明为全局变量 
var mm_pix=3.78;
 
//打印容器内容定位
function setComponentPos(name,x,y,weight,height)
{
	var obj = document.getElementById(name);
	obj.style.position = "absolute";

	obj.style.left = x * mm_pix;
	obj.style.top = y * mm_pix;

	if (weight>0)
	{
		obj.style.weight = weight * mm_pix;
	}

	if (height>0)
	{
		obj.style.height = height * mm_pix;
	}


}

//保持页面session不过期
var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}

//创建打印容器
function createContainer(name,left,top,width,height)
{
	width = width*mm_pix;
	height = height*mm_pix;
	top = top*mm_pix;
	left = left*mm_pix;

	$("#"+name).css({ width: width,height: height,left:left,top:top,position:"absolute",border:"1px #999999 solid","-moz-box-shadow":"2px 2px 10px #909090","-webkit-box-shadow":"2px 2px 10px #909090","box-shadow":"2px 2px 10px #909090"}); 
}

function getPrintMmPx(mm)
{
	return(mm*mm_pix);
}


document.write('<object id="LODOP" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0> ');
document.write('<embed id="LODOP_EM" type="application/x-print-lodop" width=0 height=0 pluginspage="/install_lodop.exe"></embed>');
document.write('</object>');