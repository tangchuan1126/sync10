
//初始化
var overallSituationData;
var memoryData=[];
var ajaxMemoryData=[];
var lv;
function initializtion(data){
	overallSituationData=data;
	var conditionHtml='<div id="tiaojian" style="border-bottom:1px #CCC dashed; display:none;">'+
						'<table width="100%" cellspacing="0" cellpadding="0" border="0">'+
							'<tr>'+
								'<td width="10%" height="30px;" align="right">'+
									'<span style="font-weight:bold;font-size:12px;">Condition：</span>'+
								'</td>'+
								'<td width="90%" valign="middle" >'+
									'<div id="condition" style="min-width; float:left"></div>'+
										'<div style="float:left">'+							    	 
											'<a value="Search" style="margin-left:10px" class="buttons primary" onclick="custom_seach()"><i class="icon-search"></i>&nbsp;Search</a>'+
										'</div>'+
								'</td>'+
							'</tr>'+
						 '</table>'+
					   '</div>';
	$('#av').append(conditionHtml);	  //添加到页面			   
	for(var i=0;i<data.length;i++){
		var key=data[i].key;
		var type=data[i].type;
		var array=data[i].array;
		var url=data[i].url;
		var url2=data[i].url2;
		var url3=data[i].url3;
		var url4=data[i].url4;
		var son_key2=data[i].son_key2;
		var son_key3=data[i].son_key3;
		var son_key4=data[i].son_key4;
		var son_key=data[i].son_key;
		var level=data[i].level;
		var select=data[i].select;
		if(level!= undefined){
			lv=level;
		}
		//alert(level);
		var htmlTop='<div id="'+type+'" style="border-bottom:1px #CCC dashed; display:block;">'+
						'<table width="100%" cellspacing="0" cellpadding="0" border="0">'+
							'<tr>'+
								'<td width="10%" height="30px;" align="right">'+
									'<span style="font-weight:bold;font-size:12px;">'+key+'</span>'+
								'</td>'+
								'<td width="80%" valign="middle">'+
									 '<div style="overflow:auto" id="'+type+'Condition" >';
									 
		var htmlCenter='';				
		for(var a=0;a<array.length;a++){
			var name=array[a].name;
			var id=array[a].id;
			
			htmlCenter+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<a href="javascript:void(0)" onclick="addCondition(\''+id+'\','+"'"+name+"'"+','+"'"+type+"'"+','+"'"+key+"'"+','+"'"+url+"'"+','+"'"+url2+"'"+','+"'"+url3+"'"+','+"'"+url4+"'"+','+"'"+son_key+"'"+','+"'"+son_key2+"'"+','+"'"+son_key3+"'"+','+"'"+son_key4+"'"+',\''+type+'\')" style="font-size:13px;color:#069;text-decoration:none;outline:none;">'+
								 name+							 	  	 						
							'</a>'+
						'</div>';
		}
		var htmlBottom='';
		if(select=='true' && select!='undefined'){
			htmlBottom=	'</div>'+
						  '</td>'+
						'<td style="padding-left:10px">&nbsp;</td>'+
					'</tr>'+
				'</table>'+
			'</div>';
		}else{
			htmlBottom=	'</div>'+
						  '</td>'+
						'<td style="padding-left:10px"><a id="'+type+'Select" class="buttons" value="Multi" onclick="multiSelect(\''+type+'\','+"'"+type+"Condition'"+',this)"><i class="icon-filter">&nbsp; </i>Multi</a></td>'+
					'</tr>'+
				'</table>'+
			'</div>';				
		}
			

		$('#av').append(htmlTop+htmlCenter+htmlBottom);//添加到页面  
		//调计算高度方法超高时 出现滚动条
	    computeHeight(type+'Condition');
	}
}

//点击查询条件
function addCondition(id,name,type,key,url,url2,url3,url4,son_key,son_key2,son_key3,son_key4,son_type){
	$('#tiaojian').css('display','block');      //条件显示
	$('#'+type).css('display','none');    //隐藏当前点击的
	
	//判断如果是type 是title 把title id保存
	if(type=='title'){
		$('#title').val(id);
		ajaxLotNumber(lv,id);
	}
	
	var str=son_type.split('_son');
	if(str.length==1 && url!=''&& url!='undefined'){
		
		ajaxData(id,url,url2,url3,url4,son_key,son_key2,son_key3,son_key4,son_type,type);   //ajax加载查询值
	}else if(str.length==2 && url2!='' && url2!='undefined'){
		
		ajaxData(id,url,url2,url3,url4,son_key,son_key2,son_key3,son_key4,son_type,type);   //ajax加载查询值
	}else if(str.length==3 && url3!='' && url3!='undefined'){
		
		ajaxData(id,url,url2,url3,url4,son_key,son_key2,son_key3,son_key4,son_type,type);   //ajax加载查询值
	}else if(str.length==4 && url4!='' && url4!='undefined'){
		
		ajaxData(id,url,url2,url3,url4,son_key,son_key2,son_key3,son_key4,son_type,type);   //ajax加载查询值
	}

	
	var html='<div id="'+type+''+id+'" type="'+type+'" title="'+name+'" zhi="'+id+'" class="'+type+'" style="border:1px #090 solid;float:left;min-width;font-size:13px;cursor:pointer;margin-left:10px;padding-left:2px;line-height:25px;">'+key+'<span style="color:red;font-weight: bold;">'+name+'&nbsp;×<span></div>';
	var av=$(html).appendTo($('#condition'));   //添加到页面
	
	$(av).click(function(){   //绑定删除事件
		$(this).remove();  //删除当前条件
		$('#'+$(this).attr('type')).css('display','block');  //显示查询值
		
		
		if($(this).attr('type')=='title'){
			$("#condition").children("."+'pici').remove();
			$('#pici').remove();
		}
		
		
		if(url!=''){
			$('#'+$(this).attr('type')+'_son').remove();   //删除子集选择值
			$('#'+$(this).attr('type')+'_son_son').remove();   //删除子集选择值
			$('#'+$(this).attr('type')+'_son_son_son').remove();   //删除子集选择值
			$('#'+$(this).attr('type')+'_son_son_son_son').remove();   //删除子集选择值
			
			$("#condition").children("."+$(this).attr('type')+'_son').remove();
			$("#condition").children("."+$(this).attr('type')+'_son_son').remove();
			$("#condition").children("."+$(this).attr('type')+'_son_son_son').remove();
			$("#condition").children("."+$(this).attr('type')+'_son_son_son_son').remove();
			
		}
		
		if($('#condition').html()==''){  //如果条件里为空自动隐藏
			$('#tiaojian').css('display','none');
			customRefresh();    //用户需求 无条件时刷新页面
		} 
	});
}




//计算高度 超过出现滚动条
function clertHeight(id){
	 $('#'+id).css("height","");
}
function computeHeight(id){	
   var $av=$('#'+id);
   var height=$av.height();	
   if(height>=60){
	  $av.css("height","60px");
   }
}

function multiSelect(type,divId,button){
	var nan={};
	nan.id=divId;
	nan.data=$('#'+divId).html();
	memoryData.push(nan);
	var html='';
	var htmlBottom='';
	for(var a=0;a<overallSituationData.length;a++){
		var op=overallSituationData[a];
		if(op.type==type){
			var array=op.array;
			var ty=op.type;
			var key=op.key;
			var url=op.url;
			var url2=op.url2;
			var url3=op.url3;
			var url4=op.url4;
			var son_key2=op.son_key2;
			var son_key3=op.son_key3;
			var son_key4=op.son_key4;
			var son_key=op.son_key;
			for(var i=0;i<array.length;i++){
				var name=array[i].name;
				var id=array[i].id;
				html+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<input name="'+ty+'Se" type="checkbox" value="'+id+'" textName="'+name+'" />'+
							'<a href="javascript:void(0)" onclick="setCheckBoxSe(\''+ty+'Se\',\''+id+'\')" style="font-size:13px;color:#069;text-decoration:none;outline:none;">'+
									 name+							 	  	 						
							'</a>'+
					  '</div>';
			}
			htmlBottom='<div style="clear:left;background-color:#F0F0F0;height:26px;padding-top:2px" align="center">'+
							'<div class="buttons-group minor-group"><a value="Sure" class="buttons" onclick="addMultiCondition(\''+ty+'Se\',\''+ty+'\',\''+key+'\',\''+url+'\',\''+url2+'\',\''+url3+'\',\''+url4+'\',\''+son_key+'\',\''+son_key2+'\',\''+son_key3+'\',\''+son_key4+'\')"><i class="icon-ok icon-green"></i>&nbsp;Sure</a>&nbsp;&nbsp;'+
							'<a value="Cancel" class="buttons" onclick="singleSelection(\''+type+'\','+"'"+type+"Condition'"+')"><i class="icon-ban-circle icon-red"></i>&nbsp;Cancel</a></div>'+
					   '</div>';
		}
	}
	$(button).css('display','none');
	clertHeight(divId);       //清除高度
	//$('#'+divId).css("border-left","1px solid #E6E6E6");
	$('#'+divId).html(html+htmlBottom);
}


					  

function singleSelection(ty,divId){
	var html='';
	for(var i=0;i<memoryData.length;i++){
		if(memoryData[i].id==divId){
			html=memoryData[i].data;
		}
	}

    $('#'+ty+'Select').css('display','inline-block');
	$('#'+divId).html(html);
	computeHeight(divId);    //计算高度
}

//复选框 点击文字复选框选中
function setCheckBoxSe(seId,id){
	var se=$('input:checkbox[name='+seId+']');
	for(var i=0;i<se.length;i++){
		var val=$(se[i]).val();
		if(val==id){
			if($(se[i]).attr("checked")){
				$(se[i]).removeAttr("checked");
			}else{
				$(se[i]).attr("checked","checked");
			}
		}
	}
}

//多选
function addMultiCondition(seId,type,key,url,url2,url3,url4,son_key,son_key2,son_key3,son_key4){
	$('#tiaojian').css('display','block');      //条件显示
	$('#'+type).css('display','none');
	var id='';
	var name='';
	var se=$('input:checkbox[name='+seId+']:checked');
	for(var i=0;i<se.length;i++){
		var val=$(se[i]).val();
		var textName=$(se[i]).attr('textName');
		if(i==se.length-1){
			id+=val;
			name+=textName;
		}else{
			id+=val+',';
			name+=textName+',';
		}
	}
	
	if(type=='title'){
	    $('#title').val(id);	
	}
	
	ajaxData(id,url,url2,url3,url4,son_key,son_key2,son_key3,son_key4,type,type);
	
	html='<div id="'+type+''+id+'" type="'+type+'" class="'+type+'" title="'+name+'" zhi="'+id+'" style="border:1px #090 solid;float:left;min-width;font-size:13px;cursor:pointer;margin-left:10px;padding-left:2px;line-height:25px; ">'+key+'<span style="color:red;font-weight: bold;">'+name+'&nbsp;×<span></div>';

	var av=$(html).appendTo($('#condition'));   //添加到页面
	$(av).click(function(){
		$(this).remove();  //删除当前条件
		$('#'+$(this).attr('type')).css('display','block');  //显示查询值
		
		$('#'+$(this).attr('type')+'_son').remove();   //删除子集选择值
		$('#'+$(this).attr('type')+'_son_son').remove();   //删除子集选择值
		$('#'+$(this).attr('type')+'_son_son_son').remove();   //删除子集选择值
		$('#'+$(this).attr('type')+'_son_son_son_son').remove();   //删除子集选择值
		
		$("#condition").children("."+$(this).attr('type')+'_son').remove();
		$("#condition").children("."+$(this).attr('type')+'_son_son').remove();
		$("#condition").children("."+$(this).attr('type')+'_son_son_son').remove();
		$("#condition").children("."+$(this).attr('type')+'_son_son_son_son').remove();
		
		if($('#condition').html()==''){  //如果条件里为空自动隐藏
			$('#tiaojian').css('display','none');
		} 					 
	});
}

//获取查询值
function getSeachValue(){
	 var con=$("#condition > div"); 
	 var array=[];
	 for(var i=0;i<con.length;i++){
		var op={};
		var key=$(con[i]).attr('type');
	    var val=$(con[i]).attr('zhi');
	    op.type=key;
		op.val=val;
		array.push(op);
	 }
	 return array;
}

//加载批次
function ajaxLotNumber(ur,titleId){
	var url='';
	var url2='';
	var url3='';
	var url4='';
	var son_key2='';
	var son_key3='';
	var son_key4='';
	var son_key='';
	$.ajax({
		url:ur,
		type:'post',
		dataType:'json',
		data:"titleId="+titleId,
		success:function(data){  
		var htmlCenter='';
		var htmlTop='<div id="pici" style="border-bottom:1px #CCC dashed; display:block;">'+
					'<table width="100%" cellspacing="0" cellpadding="0" border="0">'+
						'<tr>'+
							'<td width="10%" height="30px;" align="right">'+
								'<span style="font-weight:bold;font-size:12px;">批次：</span>'+
							'</td>'+
							'<td width="80%" valign="middle">'+
								 '<div style="overflow:auto" id="piciCondition">';
		for(var i=0;i<data.length;i++){
		htmlCenter+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
						'<a href="javascript:void(0)" onclick="addCondition(\''+data[i].id+'\','+"'"+data[i].name+"'"+',\''+"pici"+'\',\''+"批次："+'\','+"'"+url+"'"+','+"'"+url2+"'"+','+"'"+url3+"'"+','+"'"+url4+"'"+','+"'"+son_key+"'"+','+"'"+son_key2+"'"+','+"'"+son_key3+"'"+','+"'"+son_key4+"'"+',\''+"pici"+'\')" style="font-size:13px;color:#069;text-decoration:none;outline:none;">'+
							data[i].name+
						'</a>'+
					'</div>';	
		}
		var htmlBottom=	'</div>'+
					'</td>'+
				'<td style="padding-left:10px">&nbsp;</td>'+
			'</tr>'+
			'</table>'+
		'</div>';
		var html=htmlTop+htmlCenter+htmlBottom;		
		$('#av').append(html);
	    }
	});
}


//ajax加载
function ajaxData(id,url,url2,url3,url4,son_key,son_key2,son_key3,son_key4,son_type,type){
	var title_id=$('#title').val();
	var atomicBomb=$('#atomicBomb').val();
	var para='id='+id+'&atomicBomb='+atomicBomb+'&title_id='+title_id;
	var uu='';
	var key='';
	var str=son_type.split('_son');
	if(str.length==1 && url!=''&& url!='undefined'){
		uu=url;
		key=son_key;
	}else if(str.length==2 && url2!='' && url2!='undefined'){
		uu=url2;
		key=son_key2;
	}else if(str.length==3 && url3!='' && url3!='undefined'){
		uu=url3;
		key=son_key3;
	}else if(str.length==4 && url4!='' && url4!='undefined'){
		uu=url4;
		key=son_key4;
	}

	var sonType=son_type+'_son';
	$.ajax({
		url: uu,
		type:'post',
		dataType:'json',
		data:para,
		async:false,
		success:function(data){
		    var zwb={};
		    zwb.data=data;
			if(data.length>0){
				zwb.id=sonType;
				var htmlCenter='';
				var htmlTop='<div id="'+sonType+'" style="border-bottom:1px #CCC dashed; display:block;">'+
								'<table width="100%" cellspacing="0" cellpadding="0" border="0">'+
									'<tr>'+
										'<td width="10%" height="30px;" align="right">'+
											'<span style="font-weight:bold;font-size:12px;">'+key+'</span>'+
										'</td>'+
										'<td width="80%" valign="middle">'+
											 '<div style="overflow:auto" id="'+sonType+'Condition">';
				for(var i=0;i<data.length;i++){
					htmlCenter+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
									'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'"+sonType+"'"+','+"'"+key+"'"+','+"'"+url+"'"+','+"'"+url2+"'"+','+"'"+url3+"'"+','+"'"+url4+"'"+','+"'"+son_key+"'"+','+"'"+son_key2+"'"+','+"'"+son_key3+"'"+','+"'"+son_key4+"'"+','+"'"+sonType+"'"+')" style="font-size:13px;color:#069;text-decoration:none;outline:none;">'+
										data[i].name+
									'</a>'+
								 '</div>';	
				}
					var htmlBottom=	'</div>'+
								'</td>'+
							'<td style="padding-left:10px"><a id="'+sonType+'Select" value="Multi" onclick="ajaxMultiSelect(\''+sonType+'\',\''+sonType+'Condition\',this,\''+key+'\','+"'"+url+"'"+','+"'"+url2+"'"+','+"'"+url3+"'"+','+"'"+url4+"'"+','+"'"+son_key+"'"+','+"'"+son_key2+"'"+','+"'"+son_key3+"'"+','+"'"+son_key4+"'"+')" class="buttons" ><i class="icon-filter"></i>&nbsp; Multi</a></td>'+
						'</tr>'+
					'</table>'+
				'</div>'; 
				var html=htmlTop+htmlCenter+htmlBottom;
				zwb.html=htmlCenter;
				ajaxMemoryData.push(zwb);
				$('#av').append(html);
				$('#'+type).css('display','none');    //隐藏当前点击的
			}
		}
	});
	//调计算高度方法超高时 出现滚动条
    computeHeight(sonType+'Condition');
}

function ajaxMultiSelect(type,divId,button,key,url,url2,url3,url4,son_key,son_key2,son_key3,son_key4){
	for(var i=0;i<ajaxMemoryData.length;i++){
		if(ajaxMemoryData[i].id==type){
			var html='';
			var data=ajaxMemoryData[i].data;
			for(var a=0;a<data.length;a++){
                var id=data[a].id;
                var name=data[a].name;
				html+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<input name="'+type+'Se" type="checkbox" value="'+id+'" textName="'+name+'" />'+
							'<a href="javascript:void(0)" onclick="setCheckBoxSe(\''+type+'Se\','+id+')" style="font-size:13px;color:#069;text-decoration:none;outline:none;">'+
									 name+							 	  	 						
							'</a>'+
					  '</div>';
			}
			htmlBottom='<div style="clear:left;background-color:#F0F0F0;height:26px;padding-top:2px" align="center">'+
							'<input type="button" value="Sure" class="short-button-ok" onclick="addMultiCondition(\''+type+'Se\',\''+type+'\',\''+key+'\',\''+url+'\',\''+url2+'\',\''+url3+'\',\''+url4+'\',\''+son_key+'\',\''+son_key2+'\',\''+son_key3+'\',\''+son_key4+'\')"/>&nbsp;&nbsp;'+
							'<input type="button" value="Cancel" class="short-button-del" onclick="ajaxSingleSelection(\''+type+'\','+"'"+type+"Condition'"+')"/>'+
					   '</div>';
			
			$(button).css('display','none');
			clertHeight(divId);       //清除高度
		
			$('#'+divId).html(html+htmlBottom);
			
			 
		}
	}
}

function ajaxSingleSelection(type,divId){
	var html='';
	for(var i=0;i<ajaxMemoryData.length;i++){
		if(ajaxMemoryData[i].id==type){
			html=ajaxMemoryData[i].html;
		}
	}
    $('#'+type+'Select').css('display','inline-block');  
	$('#'+divId).html(html);
	computeHeight(divId);    //计算高度
}
