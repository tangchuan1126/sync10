/**
 * 
 */


(function($) {
    //扩展这个方法到jquery
    $.fn.extend({
        //插件名字
    	
        showPicture: function() {
        	var data = arguments[0];
        	var arg1 = arguments[1];
        	
        	var _element = this;
        	
        	var fn = {
        		'appendPicture' : function(){	//////*********方法回调 添加图片
        			
        			//**************
        			return _element.each(function() {
                		var pictureUrl = "_fileserv/file";
               		 	var element = this;
        	        	var html = '';
        	        	var base_path = $(element).find("input[name='base_path']").val();
        	        	data = arg1;
        				if(data.length>0){
        					//***过滤相同的Id的图片 相同则不添加
        					var fileIds = $(element).find("input[name='file_names']").val().split(",");
        					
        					var _data = [];
        					for(var i=0;i<data.length;i++){
        						_data[i] = data[i].file_id;
        					}
        					for(var i=0;i<fileIds.length;i++){
	        					if($.inArray(fileIds[i],_data)!=-1){
	        						return;
	        					}
        					}
        					//************* 过滤  end
        				
        					var fileId = "";
        					for(var i=0; i<data.length; i++){
        						fileId += data[i].file_id+",";  
        						html += '<li value="'+data[i].original_file_name+'" fileWithId="'+data[i].file_with_id+'" fileid="'+data[i].file_id+'">';
        						var imgType = data[i].original_file_name;
            					imgType = imgType.substring(imgType.lastIndexOf(".")+1).toLowerCase();
            					if(imgType == "jpg" || imgType == "png" || imgType == "bmp" || imgType == "gif"){
            						html += '<img class="local_show" value="'+data[i].file_id+'" src="'+base_path+pictureUrl+'/'+data[i].file_id+'" title="'+data[i].original_file_name+'" >';
            					}else if(imgType == "xls"||imgType == "xlsx"){
            						html += ' <img class="down" value="'+data[i].file_id+'" src="../js/picture/images/xls.png" fileId="'+data[i].file_id+'" title="'+data[i].original_file_name+'">';
            					}else if(imgType == "doc"||imgType == "docx"){
            						html += ' <img class="down" value="'+data[i].file_id+'" src="../js/picture/images/doc.png" fileId="'+data[i].file_id+'" title="'+data[i].original_file_name+'">';
            					}else if(imgType == "pdf"){
            						html += ' <img class="down" value="'+data[i].file_id+'" src="../js/picture/images/pdf.png" fileId="'+data[i].file_id+'" title="'+data[i].original_file_name+'">';
            					}else{
            						html += ' <img class="down" value="'+data[i].file_id+'" src="../js/picture/images/none.png" fileId="'+data[i].file_id+'" title="'+data[i].original_file_name+'">';
            					} 
        						html += '<span is_local="false"></span>';
        						html += '<div >local</div> </li>';
        					}
        					fileId = $(element).find("input[name='file_names']").val() + fileId;
        					$(element).find("input[name='file_names']").val(fileId);
        				}
        				$(element).find("div.showPicture_overflow ul").prepend(html);
        				$(element).find("li img.local_show").unbind();
        				$(element).find("li span[is_local='false']").unbind();
        				
        				//视图移到最左边   能看到刚添加的文件
        				var pic = $(element).find(".showPicture_overflow #pictureWidth");
        				var left = pic.offset().left;
        				var left_el = $(element).find(".showPicture_overflow .move_left");
        				var over_view = left_el.offset().left + left_el.width() - left;
        				left = left + over_view -left_el.offset().left - pic.find(".fill_space").width();
        				
        				pic.animate({
        						left: left
        				  	}, 
        				  	1000 );
        				//视图移到最左边  end
        				
        				
        				/*
        				$(element).find("li img.down").on("click",function(){			//其他文件点击下载
            				window.location.href=(base_path+"_fileserv/download/"+$(this).parent().attr("fileId"));
            			});
        				*/
        				$(element).find("li img.local_show").on("click",function(){		//绑定local文件查看
        					var throughBox = $.artDialog.through;
        					var path = $(this).attr("src");
        					var title = $(this).attr("title");
        					throughBox({
        					    content: '<img  src="'+path+'" title="'+title+'" width="100%" height="100%" style=" border: 1px solid #aaa;">',
        					    lock: true,
        					    title:title
        					});
        				});
            			/*
            			$(element).find("li img.server_show").on("click",function(){	//绑定server查看事件
            				var currentName = $(this).parent().val();
        					var file_with_id = $(this).parent().attr("fileWithId")*1;
        					var file_with_type = "";
        					var obj = {
        						  		file_with_type:file_with_type,
        						  		file_with_id : file_with_id,
        						  		current_name : currentName ,
        						  		cmd:"multiFile",
        						  		table:'file',
        						  		base_path:base_path + "_fileserv/file"
        							}
							openArtPictureOnlineShow(obj,base_path);
            			});
            			*/
        				$(element).find("li span[is_local='false']").on("click",function(){	//绑定删除事件
	                       	var _this = this;
	                       	var id = $(_this).parent().attr("fileid");
                          	$.artDialog({
           					    content: '<span style="font-size:14px;">Confirm delete this photo?</span>',
           					    icon: 'question',
           					    lock: true,	
           					    width: 240,
           					    height: 70,
           					    title:'tips',
           					    okVal: 'Yes',
           					    ok: function () {
           					    	//无论文件服务是否删除成功都移除img
           					    	var array = $(element).find("input[name='file_names']").val().split(",");
		                          	var lastFile = "";
			                       	for(var index = 0 ; index < array.length ; index++ ){
		                       			if(id==array[index]){
		                       				array[index] = "";
		                       				continue;
		                       			}
		                       		
		                       			lastFile += array[index];
		                       			if(index!=array.length-1 && array[index]!=""){
		                       				lastFile+=",";
		                       			}
			                       	 }
			                       
			                       	 $(element).find("input[name='file_names']").val(lastFile);
			                       	 $(_this).parent().remove();
           					    	//删除文件服务器记录
           					    	$.ajax({
    									url: base_path + "_fileserv/file/" + id,
    									dataType: 'json',
    									type:'delete',
    									success: function (data) {
    									},
    									error: function () {
    									}
    								});
           					    },
           					    cancelVal: 'No',
           					    cancel: function(){
           					    	
           						}
           					});	
        				});	//删除 end
               	 });
               },
               'getUploadPicture':function(){
            	//   console.log(_element[0]);
            	   var value = "";
            	    _element.each(function() {
            	    	
            		  value =  $(this).find("input[name='file_names']").val();
            		  if(value.length > 0){
            			  value = value.substring(0,value.length-1);
            		  }
            	   });
            	    return value;
               },
               'removeLocal':function(){
            	   return _element.each(function(){
            		   var  ids = $(this).find("input[name='file_names']").val("");
            		   $(this).find("ul li span").parent().remove();
            	   });
            	   
               },
               'hasNewFile':function(){
            	   var flag = false;
            	   _element.each(function(){
            		   if($(this).find("input[name='file_names']").val()!=""){
            			   flag = true;
            		   }
            	   });
            	   return flag;
               },
               'hasFile':function(){
            	   var flag = false;
            	   _element.each(function(){
            		 //  console.log($(this).find("ul li").length>0);
            		   if($(this).find("ul li").length>0){
            			   flag = true;
            		   }
            	   });
            	   return flag;
               }
               
        	};
        	if(typeof data == 'string'){
        		//fn
        		
        		return fn[data](arg1);
        	}
        	
        	if(typeof data !== 'object'){
        		//
        		alert("arguments is error !!");
        		return;
        	}
        	return this.each(function() {
        		var element = this;
        		
        		//*********查询条件
            	var file_with_id = data.file_with_id;
            	var file_with_class = data.file_with_class;
            	var file_with_type = data.file_with_type;
            	
            	//*********一些配置参数
            	var base_path = data.base_path;
            	var target = this.id;
            	var limitSize = data.limitSize;
            	var multipleBtn = data.multiple_button==null?true:data.multiple_button;
            	var singleBtn = data.single_button==null?true:data.single_button;
            	var uploadBtn = data.upload_button==null?true:data.upload_button;
            	var width = data.width==null?0:data.width;
            	//**************根据配置参数生成页面
            	var div = '<div class="showPicture_overflow" >'+
            					'<div class="btn"></div>'+
            					'<div class="move_left"></div>'+
            					'<div id="pictureWidth" style="">'+
	            					'<div class="fill_space"></div>'+
            						'<ul ></ul>'+
            					'</div>'+
            					'<div class="move_right"></div>'+
            				'</div>';
            	$(element).append(div);
            	if(width>0){
            		$(element).find("div.showPicture").width(width);
            	}
            	
            	$(element).find(".move_left").on("click",function(){	//绑定图片滑动事件   右滑
            		var pic = $(element).find(".showPicture_overflow #pictureWidth");
    				var left = pic.offset().left;
    				var width = pic.width();
    				//可见区域宽度
    				var space = $(element).width() - $(element).find(".showPicture_overflow .btn").width() - $(element).find(".showPicture_overflow .move_left").width() - $(element).find(".showPicture_overflow .move_right").width();
    				//图片能全部展示
    				if(space > width-pic.find(".fill_space").width()){
    					return;
    				}
    				var step = space * 0.8;
    				var left_el = $(element).find(".showPicture_overflow .move_left");
    				var over_view = left_el.offset().left + left_el.width() - left;
    				if(over_view < step){
    					step = over_view;
    				}
    				left = left + step -left_el.offset().left - pic.find(".fill_space").width();
    				pic.animate({ 
        				    left: left
    				  	}, 1000 );
    			});
    			$(element).find(".move_right").on("click",function(){	//绑定图片滑动事件  左滑
    				var pic = $(element).find(".showPicture_overflow #pictureWidth");
    				var left = pic.offset().left;
    				var width = pic.width();
    				//可见区域宽度
    				var space = $(element).width() - $(element).find(".showPicture_overflow .btn").width() - $(element).find(".showPicture_overflow .move_left").width() - $(element).find(".showPicture_overflow .move_right").width();
    				//图片能全部展示
    				if(space > width-pic.find(".fill_space").width()){
    					return;
    				}
    				var step = space * 0.8;
    				var left_el = $(element).find(".showPicture_overflow .move_left");
    				var over_view = width + left - left_el.offset().left - left_el.width() - space;
    				if(over_view < step){
    					step = over_view;
    				}
    				left = left - step - left_el.offset().left - pic.find(".fill_space").width();
    				pic.animate({ 
        				    left: left
    				  	}, 1000 );	
    			});
    			
            	if(multipleBtn){
            		var btn = '<span><input type="button" id="mutiple" class="multiple-button" title="Multiple Photos" tabindex="8"/></span>';
            		$(element).find(".btn").append(btn);
            		$(element).find(".btn #mutiple").on("click",function(){
	            		var uri = base_path + "administrator/file_serv/picture_online_scanner.html?target="+target; 
	            		$.artDialog.open(uri , {title: 'Online Mutiple',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
            		});
            		
            	}
            	if(singleBtn){
            		var btn = '<span><input type="button" id="single" class="single-button" title="Single Photos"  tabindex="9"/></span>';
            		$(element).find(".btn").append(btn);
            		$(element).find(".btn #single").on("click",function(){
	            		//	onlineSingleScanner('onlineScanner');
            		    var uri = base_path + "administrator/file_serv/picture_online_single_scanner.html?target="+target; 
            			$.artDialog.open(uri , {title: 'Online Single',width:'300px',height:'170px', lock: true,opacity: 0.3,fixed: true});
            		});
            		
            	}
            	if(uploadBtn){
            		var btn = '<span><input type="button" id="upload" class="upload-button" title="Upload Photos" /></span>';
            		$(element).find(".btn").append(btn);
            		$(element).find(".btn #upload").on("click",function(){
            		//	uploadFile('jquery_file_up');
            			
        			    var fileNames = $("input[name='file_names']").val();
        			    var obj  = {
        				     reg:"picture_office",
        				     'limitSize':limitSize,
        				     'target':target
        				 }
        			    var uri = base_path + "administrator/file_serv/jquery_file_up.html?"; 
        				uri += jQuery.param(obj);
        				 if(fileNames && fileNames.length > 0 ){
        					uri += "&file_names=" + fileNames;
        				}
        			//	 $.artDialog.data("files",fileMap.get(_target));
        				$.artDialog.open(uri , {id:'file_up',title: 'Upload Photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
        			    	 close:function(){
        						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
        						this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
        			   	}});
            			
            		});
            	}
            	$(element).append('<input type="hidden" name="file_names" id="file_names"/>');	//添加数据临时存放隐藏域
            	$(element).append('<input type="hidden" name="base_path" id="base_path"/>');	//添加数据临时存放隐藏域
            	$(element).find("input[name='base_path']").val(base_path);
            	$(element).append('<input type="hidden" name="is_in_file_serv" id="is_in_file_serv" value="1"/>');	//标记文件是否存在文件服务器
            	//**************查询 将图片展示在页面
            	$(element).width($(element).parent().width());		// width  set
            	$(element).css({"margin-left":"auto","margin-right":"auto"});		// width  set
            	if(file_with_id * 1 > 0 ){
                	var uri = base_path+"action/administrator/file_up/getFilesAction.action";
                	var pictureUrl = "_fileserv/file";
                	$.ajax({
                		url:uri,
                		data:{'file_with_class':file_with_class,'file_with_type':file_with_type,'file_with_id':file_with_id},
                		dataType:'json',
                		type:'post',
                		success:function(data){
                			var html = '';
                			if(data.length>0){
                				$(element).find("#pictureWidths").css("width",data.legnth*64);	//设置宽度 看滚动条是否出现
                				for(var i=0; i<data.length; i++){
                					var imgType = data[i].file_path;
                					if(imgType == undefined || imgType == null || imgType == ""){
                						continue;
                					}
                					html += ' <li value="'+data[i].original_file_name+'" fileWithId="'+data[i].file_with_id+'" fileId="'+data[i].file_id+'">';
                					imgType = imgType.substring(imgType.lastIndexOf(".")+1).toLowerCase();
                					if(imgType == "jpg" || imgType == "jpeg" || imgType == "png" || imgType == "gif"){ 
                						var _type = data[i].expire_in_secs>0?"local_show":"server_show";
                						html += ' <img class="'+_type+'" src="'+base_path+pictureUrl+'/'+data[i].file_id+'" value="'+data[i].file_id+'" style="background:url(\'../js/picture/images/load.gif\')" title="'+data[i].original_file_name+'">';
                					}else if(imgType == "xls"||imgType == "xlsx"){
                						html += ' <img class="down" value="'+data[i].file_id+'" src="../js/picture/images/xls.png" fileId="'+data[i].file_id+'" title="'+data[i].original_file_name+'">';
                					}else if(imgType == "doc"||imgType == "docx"){
                						html += ' <img class="down" value="'+data[i].file_id+'" src="../js/picture/images/doc.png" fileId="'+data[i].file_id+'" title="'+data[i].original_file_name+'">';
                					}else if(imgType == "pdf"){
                						html += ' <img class="down" value="'+data[i].file_id+'" src="../js/picture/images/pdf.png" fileId="'+data[i].file_id+'" title="'+data[i].original_file_name+'">';
                					}else{
                						html += ' <img class="down" value="'+data[i].file_id+'" src="../js/picture/images/none.png" fileId="'+data[i].file_id+'" title="'+data[i].original_file_name+'">';
                					}
                					//服务器图片支持删除   ---BY CHENCHEN
                					//if(data[i].expire_in_secs>0){
                					html += '<span></span>';
                					//}
                					var __type =data[i].expire_in_secs>0?"local":"server";
                					html += '<div >'+__type+' </div> </li>';
                				}
                			}
                			$(element).find("div.showPicture_overflow ul").append(html);
                			$(element).find("li img.down").on("click",function(){			//其他文件点击下载
                				window.location.href=(base_path+"_fileserv/download/"+$(this).attr("fileId"));
                				
                			});
                		//	console.log($(element).find("li img.local_show,img.server_show"))
                			//取消lazyload方式，src直接加载图片
            				/*$(element).find("li img.local_show,img.server_show").lazyload({ 
            					event: "scrollstop",
            					 effect : "fadeIn"
            				});*/
                //			console.log(2)
                			$(element).find("li img.local_show").on("click",function(){		//绑定local文件查看
            					var throughBox = $.artDialog.through;
            					var path = $(this).attr("src");
            					var title = $(this).attr("title");
            					throughBox({
            					    content: '<img  src="'+path+'" title="'+title+'">',
            					    lock: true,
            					    title:title
            					   });
            				});
                			$(element).find("li img.server_show").on("click",function(){	//绑定server查看事件
                				
                				var throughBox = $.artDialog.through;
            					var path = $(this).attr("src");
            					var title = $(this).attr("title");
            					throughBox({
	        					    content: '<img  src="'+path+'" title="'+title+'">',
	        					    lock: true,
	        					    title:title
	        					});
                				
                				
                				/*
                				var currentName = $(this).parent().val();
            					var file_with_id = $(this).parent().attr("fileWithId")*1;
            					
            					 var obj = {
            						  		file_with_type:file_with_type,
            						  		file_with_id : file_with_id,
            						  		current_name : currentName ,
            						  		cmd:"multiFile",
            						  		table:'file',
            						  		base_path:base_path + "_fileserv/file/"
            							}
    							 openArtPictureOnlineShow(obj,base_path);
    							 */
                			});
                			$(element).find("li span").on("click",function(){	//绑定serv删除事件
	           					var id = $(this).parent().attr("fileid");
	           					var _ele = $(this).parent();
	           					$.artDialog({
	           					    content: '<span style="font-size:14px;">Confirm delete this photo?</span>',
	           					    icon: 'question',
	           					    lock: true,	
	           					    width: 240,
	           					    height: 70,
	           					    title:'tips',
	           					    okVal: 'Yes',
	           					    ok: function () {
	           					    	$.ajax({
	    									url: base_path + "_fileserv/file/" + id,
	    									dataType: 'json',
	    									type:'delete',
	    									success: function (data) {
	    										if(data && data.length>0) {
	    											_ele.remove();
	    										}
	    									},
	    									error: function () {
	    										showMessage("System error,please try later", "error");
	    									}
	    								});
	           					    },
	           					    cancelVal: 'No',
	           					    cancel: function(){
	           					    	
	           						}
	           					});	
	           				});
                		},
                		error:function(){
                			alert('System error');
                		}
                	});
            	}
            });
        }
    });
 //传递jQuery到方法中，这样我们可以使用任何javascript中的变量来代替"$"      
})(jQuery); 
