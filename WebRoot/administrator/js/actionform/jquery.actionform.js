/*******************************************************************************
 * 
 * Description: 快速操作页面上的Radio button,Drop list ，checkbook 的Jquery的插件
 * 
 * @since 2010-1-18
 * @author monlyu
 * @version 0.1
 * @project hone : http://code.google.com/p/actionform/
 * @see jquery >1.4
 * 
 ******************************************************************************/
(function($) {
	// append the message after the selector
	$.apd = function(selc, av) {
		return selc.selector + av;
	};
	$.apdObj = function(selc, av) {
		return $($.apd(selc, av));
	};
	$.fn.radio = function(val) {
		var obj = $(this);
		if ($.isEmpty(val)) {
			return $($.apd(obj, ':checked')).val();
		} else {
			if ($.isArray(val))
				return;
			$.getByVal(obj, val).attr('checked', 'checked');
		}
	};
	$.fn.checkbox = function(val) {
		var obj = $(this);
		if ($.isEmpty(val)) {
			var ary = [], c = 0;
			$($.apd($(this), ':checked')).each(function() {
				ary[c++] = $(this).val();
			});
			return ary;
		} else {
			if($.isArray(val)){
				for(var opt in val){
					$.getByVal(obj, val[opt]).attr('checked', 'checked');
				}
			}else{
				$.getByVal(obj, val).attr('checked', 'checked');
			}
		}
	};
	// 反选
	$.fn.inverse = function() {
		$(this).each(function() {
			if ($(this).attr('checked')) {
				$(this).removeAttr('checked');
			} else {
				$(this).attr('checked', 'true');
			}
		});
	};
	// 全选
	$.fn.checkAll = function() {
		$(this).each(function() {
			if (!$(this).attr('checked')) {
				$(this).attr('checked', 'true');
			}
		});
	};
	// 设置和获取选中值
	$.fn.select = function(val) {
		var obj = $(this);
		if ($.isEmpty(val)) {
			return $(this).val();
		} else {
			if ($.isArray(val)) {
				val = val[0];
			}
			$(this).get(0).value = val;
		}
	};
	// 获取选中的文本值
	$.fn.sltTxt = function() {
		return $.apdObj($(this), ' option:selected').text();
	};
	// 获取选择的Options的数量j1.4
	$.fn.optsCount = function() {
		return $.apdObj($(this), ' option').toArray().length;
	};
	// 添加数量
	$.fn.addOpt = function(txt, val) {
		if ($(this).isExistVal(val)) {
			return;
		}
		$(this).get(0).options.add(new Option(txt, val));
	};
	$.fn.sltValAry = function() {
		var ary = [], c = 0;
		$.apdObj($(this), ' option').each(function() {
			ary[c++] = $(this).val();
		});
		return ary;
	};
	// 清空所有的option
	$.fn.empSelect = function() {
		$(this).html('');
	};
	// 查看列表是否包含某个值
	$.fn.isExistVal = function(val) {
		var valAry = $(this).sltValAry();
		if ($.isArray(val)) {
			for ( var int in val) {
				if (!$(this).isExistVal(val[int])) {
					return false;
					break;
				}
			}
			return true;
		} else {
			for ( var opt in valAry) {
				if (valAry[opt] == val) {
					return true;
					break;
				}
			}
			return false;
		}
	};
	// 获取当前选中项为第几的项
	$.fn.sltIndex = function() {
		return $(this).get(0).selectedIndex;
	};
	$.fn.disabled = function(vals) {
		var obj = $(this);
		if ($.isEmpty(vals)) {
			return $.eachVal($(this), false);
		} else {
			if($.isArray(vals)){
				for ( var opt in vals) {
					$.getByVal(obj, vals[opt]).attr('disabled', 'disabled')
				}
			}else{
				$.getByVal(obj, vals).attr('disabled', 'disabled')
			}
		}
	};
	$.fn.enabled = function(vals) {
		var obj = $(this);
		if ($.isEmpty(vals)) {
			return $.eachVal($(this), true);
		} else {
			if($.isArray(vals)){
			 for ( var opt in vals) {
					$.getByVal(obj, vals[opt]).removeAttr('disabled');
				}
			}else{
				$.getByVal(obj, vals).removeAttr('disabled');
			}
		}
	};
	$.getByVal = function(obj, val) {
		return $($.apd(obj, '[value=' + val + ']'));
	};
	$.eachVal = function(selc, isEnable) {
		var ary = [], c = 0;
		selc.each(function() {
			if (isEnable) {
				if (!$(this).attr('disabled')) {
					ary[c++] = $(this).val();
				}
			} else {
				if ($(this).attr('disabled')) {
					ary[c++] = $(this).val();
				}
			}
		});
		return ary;
	};
	$.isEmpty = function(input) {
		return (input == null || input == '' || input == undefined);
	};
	$.isArray = function(input) {
		return Object.prototype.toString.apply(input) === '[object Array]';
	};
	$.getType = function(selc) {
		var type = selc.attr('type');
		if ($.isEmpty(type)) {
			return;
		}
		return type;
	};
})(jQuery);
