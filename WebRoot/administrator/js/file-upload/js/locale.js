/*
 * jQuery File Upload Plugin Localization Example 6.5.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2012, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*global window */

window.locale = {
    "fileupload": {
        "errors": {
            "maxFileSize": "Felie is too big",
            "minFileSize": "File is too small",
            "acceptFileTypes": "File format can't be supported",
            "maxNumberOfFiles": "File quantity exceed limit",
            "uploadedBytes": "Uploaded bytes exceed file size",
            "emptyResult": "Empty file upload result"
        },
        "error": "Error",
        "start": "Upload",
        "cancel": "Cancel",
        "destroy": "Delete"
    }
};
var regObject = {
	all:{reg:/(\.|\/)(\w+)$/i,alertText:"All Type"},
	picture:{reg:/(\.|\/)(gif|jpe?g|png)$/i,alertText:"(gif,jpg,jpeg,png)"},
	office:{reg:/(\.|\/)(doc|docx|xls|pdf)$/i,alertText:"(doc,docx,xls,pdf)"},
	question:{reg:/(\.|\/)(doc|ppt|xls|pdf)$/i,alertText:"(doc,ppt,xls,pdf)"},
	xls:{reg:/(\.|\/)(xls)$/i,alertText:"(xls)"},
	xlsx:{reg:/(\.|\/)(xlsx)$/i,alertText:"(xlsx)"},
	xlsm:{reg:/(\.|\/)(xlsm)$/i,alertText:"(xlsm)"},
	kml:{reg:/(\.|\/)(kml)$/i,alertText:"(kml)"},
	picture_office:{reg:/(\.|\/)(gif|jpe?g|png|doc|docx|xls|xlsx|pdf)$/i,alertText:"(gif,jpg,jpeg,png,doc,docx,xls,xlsx,pdf)"}
} 
