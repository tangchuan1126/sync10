//激活的产品线
function pLineClick(lineName, catalogName, productLineId, catalogId, event,
		num, catalogTitle, ul) {
	var $addQuestion = $("#rightAddQuestion");
	var $addChild = $("#rightAddChild");
	var $addQuestionCatalog = $("#rightAddQuestionCatalog");
	var $uqdateQuestionCatalog = $("#rightUpdateQuestionCatalogName");
	var $rightList = $("#rightList");

	closeOrdRightList();// 屏蔽原有鼠标右键
	var sro = document.body.scrollTop // 获取滚动条距离上
	var evt = event ? event : (window.event ? window.event : null);
	var x = evt.pageX
			|| (evt.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft));
	var y = evt.pageY
			|| (evt.clientY + (document.documentElement.scrollTop || document.body.scrollTop));
	x = x - 13;
	y = y - sro - 13;
	if (evt.button == 2) { // 判断是鼠标右键
		if (num == 0) { // 如果为0 从产品线点进来的 部分功能限时
			var productLineId = productLineId;
			var productLineName = catalogName;
			$addQuestion
					.click( function() {
						
						uri = ul
								+ 'administrator/customerservice_qa/add_question_classify.html?productLineId='
								+ productLineId + '&productLineName='
								+ productLineName;
						$.artDialog.open(uri, {
							title : "添加问题分类",
							width : '800px',
							height : '450px',
							lock : true,
							opacity : 0.3,
							fixed : true
						});
						$rightList.css("display", "none");
					});
			$addQuestion.mouseout( function() {
				$addQuestion.css( {
					backgroundColor : "#FFF",
					color : "#000"
				});
			});
			$addQuestion.mouseover( function() {
				$addQuestion.css( {
					backgroundColor : "#06C",
					color : "#FFF"
				});
			});
			$addChild
					.click( function() {
						uri = ul
								+ "administrator/customerservice_qa/add_question_catalog_child_node.html?productLineId="
								+ productLineId + "&productLineName="
								+ productLineName;
						$.artDialog.open(uri, {
							title : "添加子分类",
							width : '500px',
							height : '200px',
							lock : true,
							opacity : 0.3,
							fixed : true
						});
					});
			$addChild.mouseout( function() {
				$addChild.css( {
					backgroundColor : "#FFF",
					color : "#000"
				});
			});
			$addChild.mouseover( function() {
				$addChild.css( {
					backgroundColor : "#06C",
					color : "#FFF"
				});
			});
			// 同级和修改功能限制 文字灰色
			$addQuestionCatalog.css("color", "#999");
			$uqdateQuestionCatalog.css("color", "#999");
		} else {
			$addQuestionCatalog.css("color", "#000");
			$uqdateQuestionCatalog.css("color", "#000");
			$addQuestion.click( function() {
				// 返回所有父类名字
					$.ajax( {
							type : 'post',
							url : ul + 'action/administrator/customerservice_qa/AjaxQuestionCatalogTitleAction.action',
							dataType : 'text',
							data : 'catalogId=' + catalogId,
							success : function(text) {
								uri = ul
										+ "administrator/customerservice_qa/add_question_classify_small.html?parentName="
										+ text + "&catalogId=" + catalogId
										+ "&productLineId=" + productLineId
										+ "&productLineName="
										+ lineName + "&catalogName="
										+ catalogName;
								$.artDialog.open(uri, {
									title : "添加问题子分类",
									width : '800px',
									height : '450px',
									lock : true,
									opacity : 0.3,
									fixed : true
								});
							}
						});
				});
			$addQuestion.mouseout( function() {
				$addQuestion.css( {
					backgroundColor : "#FFF",
					color : "#000"
				});
			});
			$addQuestion.mouseover( function() {
				$addQuestion.css( {
					backgroundColor : "#06C",
					color : "#FFF"
				});
			});
			$addChild
					.click( function() {
						uri = ul
								+ "administrator/customerservice_qa/add_question_catalog_child_node2.html?catalogId="
								+ catalogId + "&productLineId=" + productLineId
								+ "&productLineName=" + lineName
								+ "&catalogName=" + catalogName;
						$.artDialog.open(uri, {
							title : "添加子分类",
							width : '500px',
							height : '200px',
							lock : true,
							opacity : 0.3,
							fixed : true
						});
					});
			$addChild.mouseout( function() {
				$addChild.css( {
					backgroundColor : "#FFF",
					color : "#000"
				});
			});
			$addChild.mouseover( function() {
				$addChild.css( {
					backgroundColor : "#06C",
					color : "#FFF"
				});
			});
			$addQuestionCatalog
					.click( function() {
						uri = ul
								+ "administrator/customerservice_qa/add_question_catalog.html?catalogId="
								+ catalogId + "&productLineId=" + productLineId
								+ "&productLineName=" + lineName
								+ "&catalogName=" + catalogName;
						$.artDialog.open(uri, {
							title : "添加同级分类",
							width : '500px',
							height : '200px',
							lock : true,
							opacity : 0.3,
							fixed : true
						});
					});
			$addQuestionCatalog.mouseout( function() {
				$addQuestionCatalog.css( {
					backgroundColor : "#FFF",
					color : "#000"
				});
			});
			$addQuestionCatalog.mouseover( function() {
				$addQuestionCatalog.css( {
					backgroundColor : "#06C",
					color : "#FFF"
				});
			});
			$uqdateQuestionCatalog
					.click( function() {
						uri = ul
								+ "administrator/customerservice_qa/update_question_catalog.html?catalogId="
								+ catalogId + "&productLineId=" + productLineId
								+ "&productLineName=" + lineName
								+ "&catalogName=" + catalogName;
						$.artDialog.open(uri, {
							title : "添加同级分类",
							width : '500px',
							height : '200px',
							lock : true,
							opacity : 0.3,
							fixed : true
						});
					});
			$uqdateQuestionCatalog.mouseout( function() {
				$uqdateQuestionCatalog.css( {
					backgroundColor : "#FFF",
					color : "#000"
				});
			});
			$uqdateQuestionCatalog.mouseover( function() {
				$uqdateQuestionCatalog.css( {
					backgroundColor : "#06C",
					color : "#FFF"
				});
			});
		}
		if(y>400){
			y=y-100;
		}
		$rightList.css({
			display : "block",
			marginLeft : x,
			marginTop : y
		});
		$rightList.mouseleave( function() {// 鼠标移开 隐藏右键菜单
					$rightList.css("display", "none");
					$addQuestion.unbind();// 移除点击事件
					$addChild.unbind();
					$addQuestionCatalog.unbind();
					$uqdateQuestionCatalog.unbind();

					openOrdRightList(); // 打开原有鼠标右键
				});
	} else { // 否则是鼠标左键
		var str = lineName + " >> " + catalogName;
		if (lineName == 1) {
			$("#pLineClick").html(catalogName);
		} else {
			$("#pLineClick").html(str);
		}
		// 返填商品分类下拉列表控件
		ajaxLoadCatalogMenuPage(productLineId);
		// 调查询各分类下的所有问题
		findQuestion(catalogId, productLineId,"question_create_time",1);
	}
}

// 无限子级右键
function rightClickItems(productLineName, catalogName, productLineId,
		catalogId, event, catalogTitle, ul, catalogParentId) {
	var $addQuestion = $("#rightAddQuestion");
	var $addChild = $("#rightAddChild");
	var $addQuestionCatalog = $("#rightAddQuestionCatalog");
	var $uqdateQuestionCatalog = $("#rightUpdateQuestionCatalogName");
	var $rightList = $("#rightList");

	closeOrdRightList();// 屏蔽原有鼠标右键
	var sro = document.body.scrollTop // 获取滚动条距离上
	var evt = event ? event : (window.event ? window.event : null);
	var x = evt.pageX
			|| (evt.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft));
	var y = evt.pageY
			|| (evt.clientY + (document.documentElement.scrollTop || document.body.scrollTop));
	x = x - 13;
	y = y - sro - 13;
	if (evt.button == 2) {

		$addQuestionCatalog.css("color", "#000");
		$uqdateQuestionCatalog.css("color", "#000");
		$addQuestion
				.click( function() {
					$
							.ajax( {
								type : 'post',
								url : ul + 'action/administrator/customerservice_qa/AjaxQuestionCatalogTitleAction.action',
								dataType : 'text',
								data : 'catalogId=' + catalogId,
								success : function(text) {
									uri = ul
											+ "administrator/customerservice_qa/add_question_classify_very_small.html?parentName="
											+ text + "&catalogParentId="
											+ catalogParentId + "&catalogId="
											+ catalogId + "&productLineId="
											+ productLineId
											+ "&productLineName="
											+ productLineName + "&catalogName="
											+ catalogName;
									$.artDialog.open(uri, {
										title : "添加问题",
										width : '800px',
										height : '450px',
										lock : true,
										opacity : 0.3,
										fixed : true
									});
								}
							});
				});
		$addQuestion.mouseout( function() {
			$addQuestion.css( {
				backgroundColor : "#FFF",
				color : "#000"
			});
		});
		$addQuestion.mouseover( function() {
			$addQuestion.css( {
				backgroundColor : "#06C",
				color : "#FFF"
			});
		});
		$addChild
				.click( function() {
					uri = ul
							+ "administrator/customerservice_qa/add_question_catalog_child_node3.html?catalogId="
							+ catalogId + "&productLineId=" + productLineId
							+ "&productLineName=" + productLineName;
					$.artDialog.open(uri, {
						title : "添加子分类",
						width : '500px',
						height : '200px',
						lock : true,
						opacity : 0.3,
						fixed : true
					});
				});
		$addChild.mouseout( function() {
			$addChild.css( {
				backgroundColor : "#FFF",
				color : "#000"
			});
		});
		$addChild.mouseover( function() {
			$addChild.css( {
				backgroundColor : "#06C",
				color : "#FFF"
			});
		});
		$addQuestionCatalog
				.click( function() {
					uri = ul
							+ "administrator/customerservice_qa/add_question_catalog2.html?productLineId="
							+ productLineId + "&productLineName="
							+ productLineName + "&catalogParentId="
							+ catalogParentId;
					$.artDialog.open(uri, {
						title : "添加同级分类",
						width : '500px',
						height : '200px',
						lock : true,
						opacity : 0.3,
						fixed : true
					});
				});
		$addQuestionCatalog.mouseout( function() {
			$addQuestionCatalog.css( {
				backgroundColor : "#FFF",
				color : "#000"
			});
		});
		$addQuestionCatalog.mouseover( function() {
			$addQuestionCatalog.css( {
				backgroundColor : "#06C",
				color : "#FFF"
			});
		});
		$uqdateQuestionCatalog
				.click( function() {
					uri = ul
							+ "administrator/customerservice_qa/update_question_catalog2.html?catalogId="
							+ catalogId + "&catalogName=" + catalogName;
					$.artDialog.open(uri, {
						title : "添加同级分类",
						width : '500px',
						height : '200px',
						lock : true,
						opacity : 0.3,
						fixed : true
					});
				});
		$uqdateQuestionCatalog.mouseout( function() {
			$uqdateQuestionCatalog.css( {
				backgroundColor : "#FFF",
				color : "#000"
			});
		});
		$uqdateQuestionCatalog.mouseover( function() {
			$uqdateQuestionCatalog.css( {
				backgroundColor : "#06C",
				color : "#FFF"
			});
		});
		if(y>400){
			y=y-100;
		}
		$rightList.css( {
			display : "block",
			marginLeft : x,
			marginTop : y
		});
		$rightList.mouseleave( function() {// 鼠标移开 隐藏右键菜单
					$rightList.css("display", "none");
					$addQuestion.unbind();// 移除点击事件
					$addChild.unbind();
					$addQuestionCatalog.unbind();
					$uqdateQuestionCatalog.unbind();
					openOrdRightList(); // 打开原有鼠标右键
				});
	} else {
		// 返填商品分类下拉列表控件
		ajaxLoadCatalogMenuPage(productLineId);
		$
				.ajax( {
					type : 'post',
					url : ul + 'action/administrator/customerservice_qa/AjaxQuestionCatalogTitleAction.action',
					dataType : 'text',
					data : 'catalogId=' + catalogId,
					success : function(text) {
						$("#pLineClick").html(text);
					}
				});
		// 调查询各分类下的所有问题
		findQuestion(catalogId, productLineId,"question_create_time",1);
	}

}

// 屏蔽原有右键菜单
function closeOrdRightList() {
	document.oncontextmenu = function() {
		return false;
	}// 屏蔽右键
}
// 打开原有右键菜单
function openOrdRightList() {
	document.oncontextmenu = function() {
		return true;
	}// 打开右键
}
