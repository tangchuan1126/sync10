<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
		<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
		<script type="text/javascript" src="../js/select.js"></script>

	
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="JavaScript1.2">

function addAssetsCategory(parentId)
{
	$.prompt(
	
	"<div id='title'>增加子类</div><br />父类：<input name='ParentName' type='text' id='ParentName' style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br>新建类别<br>类别名称：<input name='categoryName' type='text' id='categoryName' style='width:300px;'><br>类型冻结：<input type='checkbox' id='cannotUpdate' name='cannotUpdate' value='1'>",
	
	{
	      submit: checkAddAssetsCategory,
   		  loaded:
		  
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/getApplyFundsCategoryPrecessAction.action",
							{id:parentId},
							function callback(data)
							{
								
								$("#ParentName").val(data.category_name);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/addApplyFundsCategoryPrecessAction.action";
						document.add_form.parentId.value = parentId;
						document.add_form.categoryName.value = f.categoryName;	
						document.add_form.categoryStatus.value=f.cannotUpdate;				
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function checkAddAssetsCategory(v,m,f)
{
	if (v=="y")
	{
		 if(f.categoryName == "")
		 {
				alert("请填写类别名称");
				return false;
		 }
		 if(f.categoryName.length>20)
		 {
		 	alert("类别名称不能超过20个字符！");
		 	return false;
		 }				 
		 return true;
	}

}

function delAssetsCategory(categoryId){
	if (confirm("确认操作吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/deleteApplyFundsCategoryPrecessAction.action";
		document.del_form.id.value = categoryId;
		document.del_form.submit();
	}
}

function updateAssetsCategory(categoryId){
	var category_status="";
	$.prompt(
		"<div id='title'>修改分类</div><br />中文名称：<input name='categoryName' type='text' id='categoryName' style='width:300px;'><br>类型冻结：<input type='checkbox' id='cannotUpdate' name='cannotUpdate' value='1'>",
		{
			submit: checkAddAssetsCategory,
			loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/getApplyFundsCategoryPrecessAction.action",
							{id:categoryId},
							function callback(data)
							{							
								$("#categoryName").val(data.category_name);	
								category_status=data.category_status;							
							}
					);
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						if(category_status!="1"&&category_status!=1)
						{
							document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/updateApplyFundsCategoryPrecessAction.action";
							document.mod_form.id.value = categoryId;
							document.mod_form.categoryName.value = f.categoryName;		
							document.mod_form.categoryStatus.value = f.cannotUpdate;						
							document.mod_form.submit();
						}
						else
						{
							alert("该子类为状态为冻结，不可操作！");
						}
						
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" } 	
	});
}

</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 财务管理 »   资金申请类别</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="id" >

</form>

<form method="post" name="mod_form">
<input type="hidden" name="id" >
<input type="hidden" name="categoryName" >
<input type="hidden" name="categoryStatus"/>
</form>

<form method="post" name="add_form">
<input type="hidden" name="parentId">
<input type="hidden" name="categoryName">
<input type="hidden" name="categoryStatus"/>
</form>


<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
		      <tr> 
		        <th width="45%" class="left-title" colspan="2">资金申请类型名称</th>
		        <th width="8%" class="right-title" style="vertical-align: center;text-align: center;">编号</th>
		        <th width="30%" class="right-title">&nbsp;</th>
		      </tr>
			
			<tr>
				<td colspan="5" >
					<script type="text/javascript">
					<!--
						d = new dTree('d');
						d.add(0,-1,'资金申请类别</td><td align="center" valign="middle" width="20%">&nbsp;</td><td align="center" valign="middle" width="8%">&nbsp;</td><td style="padding-right:10px;" valign="middle" width="30%"><input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(0)" value=" 增加子类"></td></tr></table>');
						<%
						DBRow [] parentCategory = applyFundsCategoryMgrZZZ.getAllApplyMoneyCategory();
						if(parentCategory.length>0)
						{
							for(int i = 0; i<parentCategory.length;i++)
							{
								if("0".equals(parentCategory[i].getString("prent_id"))||"".equals(parentCategory[i].getString("prent_id")))
								{
								%>
							d.add(<%=parentCategory[i].getString("category_id")%>,<%=parentCategory[i].getString("prent_id")%>,'<%=parentCategory[i].getString("category_name")%></td><td align="center" valign="middle" width="8%"><%=parentCategory[i].getString("category_id")%></td><td style="padding-right:10px;" valign="middle" width="30%"><input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=parentCategory[i].getString("category_id")%>)" value=" 增加子类">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateAssetsCategory(<%=parentCategory[i].getString("category_id")%>)" value="修改"> </td></tr></table>','');
								<%
								}
								else
								{
									if(parentCategory[i].get("category_status",0)==1){
								
								%>
							d.add(<%=parentCategory[i].getString("category_id")%>,<%=parentCategory[i].getString("prent_id")%>,'<%=parentCategory[i].getString("category_name")%></td><td align="center" valign="middle" width="8%"><%=parentCategory[i].getString("category_id")%></td><td style="padding-right:10px;" valign="middle" width="30%"> <input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=parentCategory[i].getString("prent_id")%>)" value=" 增加子类"></td></tr></table>','');
								<%
									}
									else
									{
										%>
							d.add(<%=parentCategory[i].getString("category_id")%>,<%=parentCategory[i].getString("prent_id")%>,'<%=parentCategory[i].getString("category_name")%></td><td align="center" valign="middle" width="8%"><%=parentCategory[i].getString("category_id")%></td><td style="padding-right:10px;" valign="middle" width="30%"><input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=parentCategory[i].getString("prent_id")%>)" value=" 增加子类">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateAssetsCategory(<%=parentCategory[i].getString("category_id")%>)" value="修改"> </td></tr></table>','');
										<%
									}
								}
							}
						}
						%>
						
						document.write(d);
					//-->
					</script>
				</td>
			</tr>
	</table>
</form>

<br>
<br>
<br>
</body>
</html>
