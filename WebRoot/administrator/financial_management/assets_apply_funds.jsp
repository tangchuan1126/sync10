<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@ page import="com.cwc.app.key.FinanceApplyTypeKey" %>
<%@page import="com.cwc.app.key.AccountKey"%>
<%@ include file="../../include.jsp"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	long assets_id = StringUtil.getLong(request,"assets_id");
	int add = StringUtil.getInt(request,"add",0);
	int type = StringUtil.getInt(request,"type",0);
	int payee_type_id = StringUtil.getInt(request,"payee_type_id",0);
	int payee_type1_id = StringUtil.getInt(request,"payee_type1_id",0);
	long payee_id = StringUtil.getLong(request,"payee_id",0);
	
	long product_line_id = StringUtil.getLong(request,"product_line_id",0);
	long center_account_id = StringUtil.getLong(request,"center_account_id",0);
	long center_account_type_id = StringUtil.getLong(request,"center_account_type_id",0);
	long center_account_type1_id = StringUtil.getLong(request,"center_account_type1_id",0);
	
	DBRow assets = assetsMgr.getDetailAssets(assets_id);
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
%>
<html>
<script type="text/javascript">

var va = ['一','二','三','四','五','六','七','八','九'];
function setParentUserShow(ids,names,  methodName)
{
	eval(methodName)(ids,names);
}
function setParentUserShowPurchaser(user_ids , user_names){
	$("#adminUserIdsPurchaser").val(user_ids);
	$("#adminUserNamesInvoice").val(user_names);
	$("#adminUserIdsInvoice").val(user_ids);
}

function adminUserInvoice(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsInvoice").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowPurchaser'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function addUSD(){
	//r_name,r_account,swift,r_address,r_bank_name,r_bank_address
	var len = $(".usd").length;	 
	var table =  "<table class='receiver usd next'>";
	table += "<tr><td rowspan='7' style='"+"width:150px;text-align:center;"+"'>外币收款信息"+va[len]+"</td>";
	table += "<td class='left'>收款人户名:</td><td><input type='text' class='noborder account_name' name='account_name'/></td>";
	table += "<td rowspan='7' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";
	table += "<tr><td class='left'>收款人账号:</td><td><input type='text' class='noborder account_number' name='account_number'/></td></tr>";
	table +="<tr><td class='left'>SWIFT:</td><td><input type='text' class='noborder account_swift' name='account_swift'/></td></tr>";
	table +="<tr><td class='left'>收款人地址:</td><td><input type='text' class='noborder account_address' name='account_address' /></td></tr>";
	table +="<tr><td class='left'>收款行名称:</td><td><input type='text' class='noborder account_blank' name='account_blank' /></td></tr>" ;
	table +="<tr><td class='left'>收款行地址:</td><td><input type='text' class='noborder account_blank_address' name='account_blank_address' /><input type='hidden' name='account_currency' value='usd'/></td></tr>" ;
	table +="<tr><td class='left'>收款人电话:</td><td><input type='text' class='noborder account_phone' name='account_phone'/></td></tr></table>" ;
 	//	
	var addBar = $(".receiver").last();
	addBar.after($(table));
}
function addRMB(){
	var len  = $(".rmb").length ;
	var table =  "<table class='receiver rmb next'>";
	table += "<tr><td rowspan='4' style='"+"width:150px;text-align:center;"+"'>人民币收款信息"+va[len]+"</td>";
	table += "<td class='left'>收款人户名:</td>";
	table += "<td><input type='text' name='account_name' class='noborder account_name'/></td>";
	table += "<td rowspan='4' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";

	table += "<tr><td class='left'>收款人账号:</td>";
	table += "<td><input type='text' name='account_number' class='noborder account_number'/></td></tr>";
	table +="<tr><td class='left'>收款开户行:</td><td><input type='text' name='account_blank' class='noborder account_blank'/></td></tr>";
	table +="<tr><td class='left'>收款人电话:</td><td><input type='text' name='account_phone' class='noborder account_phone'/><input type='hidden' name='account_currency' value='rmb'/></td></tr></table>";
 	// 获取最后一个RMB的table 然后添加到他的后面。如果是没有的话,那么就选取H1标签然后添加到他的后面
	var addBar = $(".receiver").last();
	addBar.after($(table));
}

function deletePaymentInfo(_this){
	var parentNode = $(_this).parent().parent().parent().parent();
	if($(".receiver").length == 1){
		alert("至少包含一个收款信息");
		return ;
	}
		 parentNode.remove();
}
//重新梳理账号信息
//重新梳理账号信息
	function fixAccountInfo(){
		$(".receiver").each(function(index){
			//将里面的name 都重新加上数字
			var fixIndex = index + 1 ;
			 
			var node = $(this);
			var inputs = $("input",node);
		 	for(var inputIndex = 0 , count = inputs.length ; inputIndex < count ; inputIndex++ ){
				var nodeInput = $(inputs[inputIndex]);
				var name = nodeInput.attr("name");
				if(!(isCanChangeName(name))){
					nodeInput.attr("name",name+"_"+fixIndex);
				}
				
		    }

		})
	}
 
	function isCanChangeName(name) {
	    var r = /[0-9]$/;
	    return r.test(name);
	}
function getUsdPayMentInfo(){
	var str  = "";
	var usdInfo = $(".usd");
	if(usdInfo.length > 0 ){
		for(var index = 0 , count = usdInfo.length ; index < count ; index++ ){
		//	r_name,r_account,swift,r_address,r_bank_name,r_bank_address
		//	收款人户名,收款人账号,SWIFT,收款人地址,收款行名称,收款行地址
			var _table = $(usdInfo.get(index));
			var r_name =  $("input.account_name",_table).val();
			var r_account = $("input.account_number",_table).val();
			var swift = $("input.account_swift",_table).val();
			var r_address = $("input.account_address",_table).val();
			var r_bank_name = $("input.account_blank",_table).val();
			var r_bank_address = $("input.account_blank_address",_table).val();
			str +="\n";
			if($.trim(r_name).length > 0 ){
				str += "收款人户名:"+r_name+"\n";
			}else{
				alert("输入收款人户名!");
				return;
			}
			if($.trim(r_account).length > 0 ){
				str += "收款人账号:"+r_account+"\n";
			}else{
				alert("输入收款人账号!");
				return;
			}
			if($.trim(swift).length > 0 ){
				str += "SWIFT:"+swift+"\n";
			}
			if($.trim(r_address).length > 0 ){
				str += "收款人地址:"+r_address+"\n";
			}
			if($.trim(r_bank_name).length > 0 ){
				str += "收款行名称:"+r_bank_name+"\n";
			}
			if($.trim(r_bank_address).length > 0 ){
				str += "收款行地址:"+r_bank_address+"\n";
			}
		}
		return str;
	}
	return "" ;
}
<!--

function submitApply()
{

	 var pageMessage=false;
	 var mail=false;
	 var shortMessage=false;
      //判断 页面  邮件 短信
	  if($("#isMailInvoice").attr("checked")){
	     mail=true;
	  }
	  if($("#isMessageInvoice").attr("checked")){
		 shortMessage=true;
	  }
	  if($("#isPageInvoice").attr("checked")){
		 pageMessage=true;
	  }     
   
		var str  = ""
		var rmbInfo = $(".rmb");
		if(rmbInfo.length > 0 ){
			for(var index = 0 , count = rmbInfo.length ; index < count ; index++ ){
				var _table = $(rmbInfo.get(index));
				var r_name =  $("input.account_name",_table).val();
				var r_account = $("input.account_number",_table).val();
				var r_bank = $("input.account_blank",_table).val();
				var r_phone = $("input.account_phone",_table).val();
				str +="\n";
				if($.trim(r_name).length > 0 ){
					str += "收款人户名:"+r_name+"\n";
				}else{
				 
					alert("输入收款人户名!");
					return;
				}
				if($.trim(r_account).length > 0 ){
					str += "收款人账号:"+r_account+"\n";
				}else{
					alert("输入收款人账号!");
					return;
				}
				if($.trim(r_bank).length > 0 ){
					str += "收款开户行:"+r_bank+"\n";
				}
				if($.trim(r_phone).length > 0 ){
					str += "联系电话:"+r_phone+"\n";
				}	
			}
		}
		str += getUsdPayMentInfo();
		 // 计算组织出所有收款信息
		 if(str.length <= 4){
				alert("请先输入收款人信息");
				return ;
		 }else{
			 fixAccountInfo();
	
	 if($("#categoryId").val().trim()==0 || $("#categoryId").val().trim()=="0")
	 {
 		alert("请选择资金类型！");
	 }
	 else if($("#amount").val().trim()==""||$("#amount").val()==null)
	 {
	     $("#amount").focus();
	     alert("请填写金额！");
	 }
	 else if(isNaN($("#amount").val().trim()))
 	 {
	     $("#amount").focus();
	     alert("金额必须为数字！");
 	 }
	 else if($("#amount").val().trim()<=0)
	 {
	 	 $("#amount").focus();
	     alert("金额必须大于零！");
	 }
	 else if($("#payee").val().trim() == "")
	 {
	 	 $("#payee").focus();
	 	 alert("请填写收款人！");
	 }
	 else if($("#last_time").val().trim()=="" || $("#last_time").val()==null)
	 {
	     $("#last_time").focus();
	     alert("请填写预计转款时间！");
	 }
	 else
	 {
		// 如果是有file_names ，那么读取子页面的file_names
		 
		 	apply_funds_form1.mail.value = mail;
		 	apply_funds_form1.shortMessage.value = shortMessage;
		 	apply_funds_form1.pageMessage.value = pageMessage;
			<%
		 	if(type==1)
			 	out.println("apply_funds_form1.association_type_id.value = '2'");
		 	else
		 		out.println("apply_funds_form1.association_type_id.value = '3'");
			%>
		    apply_funds_form1.add.value = "1";
			
			apply_funds_form1.submit();
			
	}
 }
}
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_name']",targetNode).val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
// 回显要求看上去已经是上传了的文件。所以上传是要加上前缀名的sn
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_name']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			 
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
		
	 	//判断是不是有了
	 	if($("a",$("#over_file_td")).length > 0){
			var td = $("#over_file_td");
	
			td.append(lis);
		}else{
			$("#file_up_tr").attr("style","");
			$("#jquery_file_up").append(lis);
		}
	} 
}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+"_"+'<%= assets_id%>'+"_" + fileName;
    var  a = "<p class='new'><a href="+uri+">"+showName+"</a>&nbsp;&nbsp;<span onclick='deleteFile(this)' class='_hand'>删除</span></p>";
    return a ;
  
}
function deleteFile(_this){
	var node = $(_this);
	node.parent().remove();	
}
function onlineScanner(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
//-->
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>资金申请</title>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>


<!-- 时间控件 -->
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />


<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
table.receiver td {border:1px solid silver;}
table.receiver td input.noborder{border-bottom:1px solid silver;width:200px;}
table.receiver td.left{width:120px;text-align:right;}
table.next{margin-top:5px;}
table.receiver{border-collapse:collapse ;}
span._hand{cursor:pointer;}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form id="apply_funds_form1" name="apply_funds_form1" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/AddApplyFundsPrecessAction.action" >
<input type="hidden" name="association_type_id" id="association_type_id"/>
<input type="hidden" name="mail" id="mail"/>
<input type="hidden" name="shortMessage" id="shortMessage"/>
<input type="hidden" name="pageMessage" id="pageMessage"/>
<input type="hidden" name="add" id="add"/>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:15px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="width:100%;border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">资金申请单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr>
		          <td width="15%" align="right" class="STYLE2" nowrap="nowrap"></td>
		          <td>&nbsp;</td>
		          <td>		 
		          	<%
		          		
		          		if(type == 1)
		          			out.println("<input type='hidden' name='categoryId' id='categoryId' value='100003'>");
		          		else
		          			out.println("<input type='hidden' name='categoryId' id='categoryId' value='10014'>");
		          	%>         
		          	<input type="hidden" name="" value="<%=adminLoggerBean.getAdid() %>"/>
		          </td>
		        </tr>
		        <tr> 			
		    <td width="15%" align="right"><span class="STYLE2">关联单号:</span></td>
		      <td width="2%">&nbsp;</td>
		      <td>
		       <%=assets_id%><input type="hidden" id="associationId" name="associationId" value="<%=assets_id%>" />
			   </td>
		      <td>
		     
			   </td>
		    </tr>
		  <tr> 
		      <td align="right" class="STYLE2">金额:</td>
		      <td>&nbsp;</td>
		      <td><input name="amount" type="text" id="amount" size="15" value="<%=assets==null?"":assets.get("unit_price",0d)%>">		     
		      	<select id="currency" name="currency">
		      	<%
		      		if(null != assets && assets.getString("currency").equals("RMB")){
		      	%>
    			<option value="RMB" selected="selected">RMB</option>
    			<%}else{ %>
    			<option value="RMB">RMB</option>
    			<%} 
		      		if(null != assets && assets.getString("currency").equals("USD")){
		      	%>
    			<option value="USD" selected="selected">USD</option>
    			<%}else{ %>
    			<option value="USD">USD</option>
    			<%} %>
    	</select>
		     </td>
		    </tr>				  
		    <tr> 
		      <td align="right" class="STYLE2">收款人:</td>
		      <td>&nbsp;</td>
		      	<td><input type="text" size="30" id="payee" name="payee" value="<%=assets==null?"":assets.getString("suppliers")%>"  ></td>
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2" >预计转款时间:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<input name="last_time" type="text" id="last_time"  value=""  style="border:1px #CCCCCC solid;width:70px;"/>
		      </td>
		    </tr>			 
		    <tr> 
		      <td align="right" class="STYLE2">付款信息:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<h1 id="addBar"> 
							      			<input type="button" value="添加人民币收款" onclick="addRMB();"/>
							      			<input type="button" value="添加外币收款" onclick="addUSD();"/>
							      		</h1>
							      		<table class="receiver rmb next">
							      			<tr>
							      				<td rowspan="4" style='width:150px;text-align:center;'>人民币收款信息一</td>
							      				<td class="left">收款人户名:</td>
							      				<td><input type="text" name="account_name" class="noborder account_name"/></td>
							      				<td rowspan="4" style="width:80px;text-align:center;"><span onclick="deletePaymentInfo(this)" class="_hand">删除</span></td>
							      			</tr>
							      			<tr>
							      				<td class="left">收款人账号:</td>
							      				<td><input type="text" name="account_number" class="noborder account_number"/></td>
							      			</tr>
							      			<tr>
							      				<td class="left">收款开户行:</td>
							      				<td><input type="text" name="account_blank" class="noborder account_blank"/></td>
							      			</tr>
							      			<tr>
							      				<td class="left">收款人电话:</td>
							      				<td>
							      					<input type="text" name="account_phone" class="noborder account_phone"/>
							      				 	<input type="hidden" name="account_currency" value='rmb' />
							      				</td>
							      			</tr>
							      		</table> 
							      		<input type="hidden" name="account_with_type" value='<%=AccountKey.APPLI_MONEY %>'/>   
		      </td>
		    </tr>
		    <tr>
		      <td class="STYLE2" align="right">资金负责人:</td>
		      <td>&nbsp;</td>
		      <td>
		      <input type="text" id="adminUserNamesInvoice" name="adminUserNamesInvoice" style="width:180px;" onclick="adminUserInvoice()"/>
		      <input type="hidden" name="adminUserIdsInvoice" id="adminUserIdsInvoice" value=""/>
		            通知：
	   		<input type="checkbox"  name="isMailInvoice" id="isMailInvoice"/>邮件
	   		<input type="checkbox"  name="isMessageInvoice" id="isMessageInvoice" />短信
	   		<input type="checkbox"  name="isPageInvoice" id="isPageInvoice" />页面
		      </td>
		    </tr>			    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="remark" rows="4" cols="80"></textarea></td>
		    </tr>
		     <tr>
		    	<td align="right" class="STYLE2">凭证</td>
		    	<td>&nbsp;</td>
		    	<td>
		    	<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件" />
		    	<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />	
		    	</td>
		    </tr>
		    <tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		    	<td>
		    		<div>	
							 	<input type="hidden" id="sn" name="sn" value="F_applyAssetsMoneny"/>
		 						<input type="hidden" id="association_type" name="association_type" value="<%=FinanceApplyTypeKey.NO_ASOCIATE %>" />
		 						<input type="hidden" id="path" name="path" value="<%= systemConfig.getStringConfigValue("file_path_financial")%>" />
		 						
			            </div>
		    				<div id="jquery_file_up">	
			              			<input type="hidden" id="file_name" name="file_name" value=""/>				 	
			            	</div> 
		    	</td>
		    </tr>
		</table>
		</fieldset>	
	</td>
  </tr>
  </table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">  
   		  <input name="Submit" type="button" class="normal-green-long" onclick="submitApply()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="WinClose();" value="取消" >
    </td>
  </tr>
</table>
</form>
<form id="apply_funds_form" name="apply_funds_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/AddApplyFundsPrecessAction.action">
<input type="hidden" id="add" name="add" value="0">
<input type="hidden" id="categoryId" name="categoryId"/>
<input type="hidden" id="associationId" name="associationId" />
<input type="hidden" id="amount" name="amount" />
<input type="hidden" id="payee" name="payee"/>
<input type="hidden" id="paymentInfo" name="paymentInfo" />
<input type="hidden" id="remark" name="remark"/>
<input type="hidden" id="center_account_id" name="center_account_id" value="<%=center_account_id %>"/>
<input type="hidden" id="center_account_type_id" name="center_account_type_id" value="<%=center_account_type_id %>"/>
<input type="hidden" id="center_account_type1_id" name="center_account_type1_id" value="<%=center_account_type1_id %>"/>
<input type="hidden" id="product_line_id" name="product_line_id" value="<%=product_line_id %>"/>
<input type="hidden" id="association_type_id" name="association_type_id"/>
<input type="hidden" id="payee_type_id" name="payee_type_id" value="<%=payee_type_id %>"/>
<input type="hidden" id="payee_type1_id" name="payee_type1_id" value="<%=payee_type1_id %>"/>
<input type="hidden" id="payee_id" name="payee_id" value="<%=payee_id %>"/>
<input type="hidden" name="currency"/>
<input type="hidden" id="adid" name="adid" value="<%=adminLoggerBean.getAdid()%>" />
<input type="hidden" id="file_name" name="file_name"/>
<input type="hidden" id="last_time" name="last_time"/> 
<input type="hidden" id="mail" name="mail"/>
<input type="hidden" id="pageMessage" name="pageMessage"/>
<input type="hidden" id="shortMessage" name="shortMessage"/>
<input type="hidden" id="admin_id" name="admin_id"/>
<input type="hidden" name="sn"/>
<input type="hidden" name="association_type"/>
<input type="hidden" name="path"/>



</form>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> $(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({allow_single_deselect:true}); </script>

<script type="text/javascript">
	$("#last_time").date_input();

	function WinClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.closeWinRefresh && $.artDialog.opener.closeWinRefresh();		 
	}
	
</script>
</body>
</html>
