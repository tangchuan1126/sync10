<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long lid = StringUtil.getLong(request,"lid");
long category_id = StringUtil.getLong(request,"category_id");
DBRow sonCatalog[] = assetsCategoryMgr.getAssetsCategoryChildren(category_id);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加资产</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


 <script type="text/javascript" src="../js/fg.menu/fg.menu.js"></script>
    
 <link type="text/css" href="../js/fg.menu/fg.menu.css" media="screen" rel="stylesheet" />
 <link type="text/css" href="../js/fg.menu/theme/ui.all.css" media="screen" rel="stylesheet" />
<script>
<!--
$(document).ready(function (){
	$('.fg-button').hover(
    		function(){ $(this).removeClass('ui-state-default').addClass('ui-state-focus'); },
    		function(){ $(this).removeClass('ui-state-focus').addClass('ui-state-default'); }
    	);
    $.get('menu.html', 
           function(data){ // grab content from another page
				$('#flyout').menu({ content: data, 
									 flyOut: true
				});
		});
});
 
function checkCategory()
{
	var flag=true;
	  $.ajax({
	  type: "POST",
	  dataType: "json",
	  async:false,
	  url: "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/GetAssetsCategoryChildrenJSONPrecessAction.action",
	  data:"categoryId="+$("#categoryId").val(),
	  success: function(msg){
		 if(msg.isok=="ok")
		{
			flag= false;
		}
      }
	  
	});	
	return flag;
}
function addAssets()
{
	var f = document.add_assets_form;
	
	
	if($("#categoryId").val()==0 || $("#categoryId").val()=="0")
	{
		alert("请选择资产类别！");
		$("#category").mcDropdown("#categorymenu").openMenu();
		return false;
	}

	if(!checkCategory())
   	{
		alert("资产必须选择到最底层类分，请重新选择");
		$("#category").mcDropdown("#categorymenu").openMenu();
		return false;
   	}

	if (f.a_name.value.trim() == "")
	 {
		alert("请填写资产名称");
		return false;
	 }
	if (f.unit_price.value.trim() == "")
	 {
		alert("请填写采购价格");
		return false;
	 }
	 else if(parseFloat(f.unit_price.value) != f.unit_price.value)
	 {
	 	alert("采购价格只能是数字");
	 	return false;
	 }
	else if(f.unit_price.value.match(/^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/))
	 {
	 	alert("采购价格不能为负数");
	 	return false;
	 }
	 if (f.location.value == 0){
	 	alert("请选择资产存放位置");
	 	return false;
	 }
	
	$("#add_assets_form").submit();
}

function catagory(id)
{	
	
}
//-->
</script>
<style type="text/css">
	body { font-size:62.5%; margin:0; padding:0; }
	#menuLog { font-size:1.4em; margin:20px; }
	.hidden { position:absolute; top:0; left:-9999px; width:1px; height:1px; overflow:hidden; }
	
	.fg-button { clear:left; margin:0 4px 40px 20px; padding: .4em 1em; text-decoration:none !important; cursor:pointer; position: relative; text-align: center; zoom: 1; }
	.fg-button .ui-icon { position: absolute; top: 50%; margin-top: -8px; left: 50%; margin-left: -8px; }
	a.fg-button { float:left;  }
	button.fg-button { width:auto; overflow:visible; }  /*removes extra button width in IE */
	
	.fg-button-icon-left { padding-left: 2.1em; }
	.fg-button-icon-right { padding-right: 2.1em; }
	.fg-button-icon-left .ui-icon { right: auto; left: .2em; margin-left: 0; }
	.fg-button-icon-right .ui-icon { left: auto; right: .2em; margin-left: 0; }
	.fg-button-icon-solo { display:block; width:8px; text-indent: -9999px; }	 /* solo icon buttons must have block properties for the text-indent to work */	
	
	.fg-button.ui-state-loading .ui-icon { background: url(spinner_bar.gif) no-repeat 0 0; }
	</style>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_assets_form"  id="add_assets_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/AddAssetsPrecessAction.action" >

<table width="98%" border="0" cellspacing="5" cellpadding="2">

  <tr >
    <td align="left" valign="top" style="padding-top: 30px"  nowrap="nowrap" class="STYLE1 STYLE2"> 产品类别:
    </td>
     <td  align="left" style="padding-top: 20px; ">
     <a href="menu.html" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-all" id="flyout"><span class="ui-icon ui-icon-triangle-1-s"></span>全部类别</a>
     <font class="STYLE3">你的选择:<span id="menuSelection"></span></font>
      <input onmouseup="" type="hidden" name="category_id" id="categoryId" value="0"  />	
    </td>
    </tr>
  
    

  <tr>
    <td align="left" valign="middle" class="STYLE1 STYLE2" >名称</td>
    <td align="left" valign="middle" ><input name="a_name" type="text" class="input-line" id="a_name" ></td>
    <td align="left" valign="middle" class="STYLE3" nowrap="nowrap">采购价格</td>
    <td align="left" valign="middle"><input name="unit_price" type="text" id="unit_price"   style="width:100px;"></td>
  </tr>
  <tr>
  	 <td align="left" valign="middle" class="STYLE3" >单位</td>
    <td align="left" valign="middle" >
    	<input name="unit_name" type="text" id="unit"  style="width:100px;" >    </td>

 <td align="left" valign="middle" class="STYLE3" >规格</td>
    <td align="left" valign="middle" >
    <input name="standard" type="text" id="standard" class="input-line">    </td>
  </tr>
  
  <tr>
  	<td align="left" valign="middle" class="STYLE3" colspan="1">存放地点</td>
    	<td align="left" valign="middle" colspan="3">
    		<ul id="locationmenu" class="mcdropdown_menu">
					  <li rel="0">所有分类</li>
					  <%
					  DBRow c1[] = officeLocationMgr.getChildOfficeLocation(0);
					  for (int i=0; i<c1.length; i++)
					  {
							out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("office_name"));
				
							  DBRow c2[] = officeLocationMgr.getChildOfficeLocation(c1[i].get("id",0l));
							  if (c2.length>0)
							  {
							  		out.println("<ul>");	
							  }
							  for (int ii=0; ii<c2.length; ii++)
							  {
									out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("office_name"));		
									
										DBRow c3[] = officeLocationMgr.getChildOfficeLocation(c2[ii].get("id",0l));
										  if (c3.length>0)
										  {
												out.println("<ul>");	
										  }
											for (int iii=0; iii<c3.length; iii++)
											{
													out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("office_name"));
													out.println("</li>");
											}
										  if (c3.length>0)
										  {
												out.println("</ul>");
										  }
										  
									out.println("</li>");				
							  }
							  if (c2.length>0)
							  {
							  		out.println("</ul>");	
							  }
							  
							out.println("</li>");
					  }
					  %>
				</ul>
					  <input type="text" name="location" id="location" value="" />
					  <input type="hidden" name="filter_lid" id="filter_lid" value=""  />
					  
				
				<script>
				$("#location").mcDropdown("#locationmenu",{
						allowParentSelect:true,
						  select: 
						  
								function (id,name)
								{
									$("#filter_lid").val(id);
								}
				
				});								
				$("#location").mcDropdown("#locationmenu").setValue(0);
				
				
				</script>      </td>
  </tr>
  
  <tr>
  	 <td align="left" valign="middle" class="STYLE3" ></td>
    <td align="left" valign="middle" >	</td>
   <td align="left" valign="middle" class="STYLE3" ></td>
    <td align="left" valign="middle" >    </td>
  </tr>
 
 <tr>
    <td align="left" valign="middle" class="STYLE3" colspan="1">供应商</td>
    <td align="left" valign="middle" colspan="3"><input name="suppliers" type="text" id="suppliers"  class="input-line">
    <input type="hidden" name="category_id" id="category_id" value="<%=category_id %>"/>
    <input type="hidden" name="state" id="state" value="4"/>      </td>
  </tr>
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line"><img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#999999">必须把资产增加到最底层分类</span>         </td>
    <td width="49%" align="right" class="win-bottom-line">    
	 <input type="button" name="Submit2" value="下一步" class="normal-green" onClick="addAssets();">	
	 &nbsp;&nbsp;
	 <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWinNotRefresh();">
	</td>
  </tr>
</table>
 
</body>
</html>
