<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.GoodsStatusKey"%>
<%@page import="com.cwc.app.key.GoodsCaseKey"%>
<%@page import="com.cwc.app.key.GoodsApplyStatusKey"%> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 long lid = StringUtil.getLong(request,"lid");
 long category_id = StringUtil.getLong(request,"category_id");
 String goodsstate = StringUtil.getString(request,"goodsstate");
 String state = StringUtil.getString(request,"state");
 DBRow sonCatalog[] = assetsCategoryMgr.getAssetsCategoryChildren(category_id);
 int type = 1;
 DBRow[] accounts = applyMoneyMgrLL.getAllByReadView("accountReadViewLL");
 DBRow[] accountCategorys = applyMoneyMgrLL.getAllByTable("accountCategoryBeanLL");
 DBRow[] adminGroups = applyMoneyMgrLL.getAllByTable("adminGroupBeanLL");
 DBRow[] productLineDefines = applyMoneyMgrLL.getAllByTable("productLineDefineBeanLL");
 DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();

 GoodsStatusKey goodsStatusKey = new GoodsStatusKey();
 GoodsCaseKey goodsCaseKey = new GoodsCaseKey();
 GoodsApplyStatusKey applyStatusKey = new GoodsApplyStatusKey();
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>创建固定资产</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.bgiframe.js"></script></head>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
 
  <script type="text/javascript" src="../js/fg.menu/fg.menu.js"></script>
    
 <link type="text/css" href="../js/fg.menu/fg.menu.css" media="screen" rel="stylesheet" />
 <link type="text/css" href="../js/fg.menu/theme/ui.all.css" media="screen" rel="stylesheet" />
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script>
<!--
//加载控件

$(document).ready(function (){
	$('.fg-button').hover(
    		function(){ $(this).removeClass('ui-state-default').addClass('ui-state-focus'); },
    		function(){ $(this).removeClass('ui-state-focus').addClass('ui-state-default'); }
    	);
    $.get('menu.html?type=<%=type%>', 
           function(data){ // grab content from another page
				$('#flyout').menu({ content: data, 
									 flyOut: true
				});
		});

});

function checkCategory()
{
	var flag=true;
	  $.ajax({
	  type: "POST",
	  dataType: "json",
	  async:false,
	  url: "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/GetAssetsCategoryChildrenJSONPrecessAction.action",
	  data:"categoryId="+$("#categoryId").val(),
	  success: function(msg){
		 if(msg.isok=="ok")
		{
			flag= false;
		}
      }
	  
	});	
	return flag;
}

//追加商品信息
var va = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15'];
function addName(){	
	var len  = $(".name").length ;
	var table =  "<table class='receiver name next'>";
	table += "<tr><td style='"+"width:120px;text-align:center;"+"' class='STYLE2'>商品名"+va[len]+":</td>";
	table += "<td align='left' valign='middle' width='20%'><input name='aname' type='text' class='input-line' id='aname' >";
	table += "<td style='width:120px;text-align:right' class='STYLE2'>型号"+va[len]+":</td>";
	table += "<td><input type='text' name='model' class='noborder'/></td>";
	table += "<td style='width:80px;text-align:left;'><img alt='删除' src='../imgs/del.gif' onclick='deleteGoodsInfo(this)' /></td></tr></table>";
 	// 获取最后一个name的table 然后添加到他的后面。如果是没有的话,那么就选取H1标签然后添加到他的后面
	var addBar = $(".receiver").last();
	addBar.after($(table));
}
function deleteGoodsInfo(_this){
	var parentNode = $(_this).parent().parent().parent().parent();
	if($(".receiver").length == 1){
		alert("至少包含一个商品信息");
		return ;
	}
		 parentNode.remove();
}
function addAssets()
{
	var f = document.add_assets_form;	
	var validateMobileCode = /^0?(13[0-9]|15[012356789]|18[0236789]|14[57])[0-9]{8}$/;
	if(type==1){
	if($("#categoryId").val()==0 || $("#categoryId").val()=="0")
	{
		alert("请选择资产类别！");
		$("#category").mcDropdown("#categorymenu").openMenu();
		return false;
	}

	if(!checkCategory())
   	{
		alert("资产必须选择到最底层类分，请重新选择");
		$("#category").mcDropdown("#categorymenu").openMenu();
		return false;
   	}
	
	document.add_assets_form.category_id.value = $("#categoryId").val();
	}
	var str  = "";
	var goodsInfo = $(".name");
	if(goodsInfo.length > 0 ){
		for(var index = 0 , count = goodsInfo.length ; index < count ; index++ ){
		//	aname,model	商品名、型号
			var _table = $(goodsInfo.get(index));
			var aname = $("input[name='aname']",_table).val();
			var model =  $("input[name='model']",_table).val();
			if($.trim(aname).length > 0 ){
				str +=aname+"/";
			}else{
				alert("输入商品名称!");
				return ;
			}
			if($.trim(model).length > 0 ){
				str += model+",";
			}else{
				alert("输入商品型号!");
				return ;
			}
		}
	}	
	if (f.unit_price.value.trim() == "")
	 {
		alert("请填写采购价格");
		return false;
	 }
	 else if(parseFloat(f.unit_price.value) != f.unit_price.value)
	 {
	 	alert("采购价格只能是数字");
	 	return false;
	 }
	else if(f.unit_price.value.match(/^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/))
	 {
	 	alert("采购价格不能为负数");
	 	return false;
	 }
	 if (f.location.value == 0){
	 	alert("请选择资产存放位置");
	 	return false;
	 }else if (f.consignee.value == ""){
			alert("请填写收货人");
			return false;
		}
		else if (f.phone.value == ""){
			alert("必须留下联系方式");
			return false;
		}
		else if (!validateMobileCode.test(f.phone.value)){
			alert("请正确填写联系方式");
			return false;
		}
		else if($("#ps_id").val()==0){
			alert("请选择存储仓库");
			return false;
		}else if($("#applystate").val()=="<%=applyStatusKey.norequired_apply%>"){
			document.add_assets_form.cmd.value = "noapply";
			document.add_assets_form.a_name.value = str;
			$("#add_assets_form").submit();
		}else if($("#applystate").val()=="<%=applyStatusKey.no_apply%>"){
			document.add_assets_form.cmd.value = "apply";
			document.add_assets_form.a_name.value = str;
			$("#add_assets_form").submit();
		}
}


	$(document).ready(function(){
		getLevelSelect(0, nameArray, widthArray, centerAccounts, null,vname);
		getLevelSelect(0, nameArray1, widthArray1, pays, null,vname1);
	});
	
<%//level_id,value,name
	String str = "var pays = new Array(";
	str += "new Array('01',0,'任意')";
	str += ",new Array('01.0',0,'任意')";
	str += ",new Array('01.0.0',0,'任意')";
	str += ",new Array('02',1,'公司账户')";
	str += ",new Array('02.0',0,'公司账户')";
	str += ",new Array('02.0.0',0,'选择部门')";
	for(int i=0;i<accounts.length;i++) {
		DBRow account = accounts[i];
		if(account.get("account_parent_id",0)==0 && account.get("account_category_id",0)==1)//部门列表
			str += ",new Array('02.0."+(i+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
	}	
	str += ",new Array('03',2,'职员账户')";
	str += ",new Array('03.0',0,'选择部门')";
	str += ",new Array('03.0.0',0,'选择职员')";
	for(int i=0;i<adminGroups.length;i++) {//部门
		DBRow adminGroup = adminGroups[i];
		str += ",new Array('03."+(i+1)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
		str += ",new Array('03."+(i+1)+".0',0,'选择职员')";
		for(int ii=0;ii<accounts.length;ii++) {
			DBRow account = accounts[ii];
			if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
				str += ",new Array('03."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
		}
	} 
	str += ",new Array('04',3,'供应商')";
	str += ",new Array('04.0',0,'选择生产线')";
	str += ",new Array('04.0.0',0,'选择供应商')";
	for(int i=0;i<productLineDefines.length;i++) {
		DBRow productLineDefine = productLineDefines[i];
		str += ",new Array('04."+(i+1)+"',"+productLineDefine.get("id",0)+",'"+productLineDefine.getString("name")+"')";
		str += ",new Array('04."+(i+1)+".0',0,'选择供应商')";
		for(int ii=0;ii<accounts.length;ii++) {
			DBRow account = accounts[ii];
			if(account.get("account_parent_id",0)==productLineDefine.get("id",0) && account.get("account_category_id",0)==3)//供应商列表
				str += ",new Array('04."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
		}
	}
	
	str+= ");";
	out.println(str);
%>
<%//level_id,value,name
str = "var centerAccounts = new Array(";
str += "new Array('01',0,'任意')";
str += ",new Array('01.0',0,'任意')";
str += ",new Array('01.0.0',0,'任意')";
str += ",new Array('02',1,'公司账户')";
str += ",new Array('02.0',0,'公司账户')";
str += ",new Array('02.0.0',0,'选择部门')";
for(int i=0;i<accounts.length;i++) {
	DBRow account = accounts[i];
	if(account.get("account_parent_id",0)==0 && account.get("account_category_id",0)==1)//部门列表
		str += ",new Array('02.0."+(i+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
}	
str += ",new Array('03',2,'职员账户')";
str += ",new Array('03.0',0,'选择部门')";
str += ",new Array('03.0.0',0,'选择职员')";
for(int i=0;i<adminGroups.length;i++) {//部门
	DBRow adminGroup = adminGroups[i];
	str += ",new Array('03."+(i+1)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
	str += ",new Array('03."+(i+1)+".0',0,'选择职员')";
	for(int ii=0;ii<accounts.length;ii++) {
		DBRow account = accounts[ii];
		if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
			str += ",new Array('03."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
	}
}
	str+= ");";
	out.println(str);
%>

var nameArray = new Array('center_account_type_id','center_account_type1_id','center_account_id');
var widthArray = new Array(120,120,200);
var nameArray1 = new Array('payee_type_id','payee_type1_id','payee_id');
var widthArray1 = new Array(120,120,200);
var vname = new Array('nameArray','widthArray','pays','vname');
var vname1 = new Array('nameArray1','widthArray1','pays','vname1');

function getLevelSelect(level,nameArray, widthArray, selectArray,o,vnames,value) {
		if(level<nameArray.length) {
			var name = nameArray[level];
			var width = widthArray[level];
			var levelId = o==null?"":$("option:selected",o).attr('levelId');
			var onchangeStr = "";
			if(level==nameArray.length-1)
				onchangeStr = "exec(this)";
			else
				onchangeStr = "getLevelSelect("+(level+1)+","+vnames[0]+","+vnames[1]+","+vnames[2]+",this,"+vnames[3]+")";

			var selectHtml = "<select name='"+name+"' id='"+name+"' style='width:"+width+"px;' onchange="+onchangeStr+"> ";
			for(var i=0;i<selectArray.length;i++) {
				if(levelId!="") {
					var levelIdChange = selectArray[i][0].replace(levelId+".");
					var levelIds = levelIdChange.split(".");	
					
					if(levelIdChange!=selectArray[i][0]&&levelIds.length==1){
						//alert(levelId+",value="+selectArray[i][0]+",change='"+levelIdChange+"',"+levelIds.length);
						selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
					}
				}
				else {
					var levelIdChange = selectArray[i][0];
					var levelIds = levelIdChange.split(".");
					if(levelIds.length==1){
						//alert(levelId+","+selectArray[i][0]+levelId1);
						selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
					}
				}
				
			}
			selectHtml += "</select>";
			$('#'+name+'_div').html('');
			$('#'+name+'_div').append(selectHtml);
			$('#'+name).val(value);
		    
			$("#"+name).chosen();
			$("#"+name).chosen({no_results_text: "没有该项!"});
	
			getLevelSelect(level+1,nameArray,widthArray,pays,$('#'+name),vnames);

			//修改输入框,破坏结构
			if($(o).attr("id")=="payee_type_id"||$(o).attr("id")=="payee_type1_id")
				$('#suppliers').val('');
		}
}
//收货地址
function removeAfterProIdInput(){$("#address_state_input").remove();}
function setPro_id(deliver_proId, deliver_proInput){
	removeAfterProIdInput();
	var node = $("#ccid_hidden");
	var value = node.val();
 	$("#pro_id").empty();
	 	  $.ajax({
				url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+value,
				dataType:'json',
				type:'post',
				success:function(data){
					if("" != data){
						var provinces = eval(data);
						$("#pro_id").attr("disabled",false);
						if(deliver_proId == -1){
							$("#pro_id").attr("disabled",false);
							for(var i = 0; i < data.length; i ++){
								$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
							}
							$("#pro_id").append("<option value=-1 selected='selected'>手动输入</option>");
							$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input' value="+deliver_proInput+"/>");
						}else{
							$("#pro_id").append("<option value=0>请选择......</option>");
							for(var i = 0; i < data.length; i ++){
								if(deliver_proId == data[i].pro_id){
									$("#pro_id").append("<option value="+data[i].pro_id+" selected='selected'>"+data[i].pro_name+"</option>");
								}else{
									$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
								}
							}
							$("#pro_id").append("<option value=-1>手动输入</option>");
						}
						
					}else{
						$("#pro_id").attr("disabled",false);
						$("#pro_id").append("<option value=0>请选择......</option>");
						$("#pro_id").append("<option value=-1>手工输入</option>");
					}
				},
				error:function(){
				}
		  })
 };
 function handleProInput(){
 	var value = $("#pro_id").val();
 	if(-1 == value){
 		$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input'/>");
 	}else{
 		removeAfterProIdInput();
 	}
 };

function selectStorage(){
	var para = "ps_id="+$("#ps_id").val();
	$("#deliver_house_number").val("");
	$("#deliver_street").val("");
	$("#deliver_zip_code").val("");
	$("#deliver_city").val("");
	$("#ccid_hidden option").attr("selected", false);
	$("#ccid_hidden").change();
	//$("#linkman").val("");
	//$("#linkman_number").val("");
	
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_catalog/GetStorageCatalogDetailDeliver.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:para,
			beforeSend:function(request){
				
			},		
			error: function(e){
				alert(e);
				alert("请求失败")
			},
			
			success: function(data){
				$("#deliver_house_number").val(data.deliver_house_number);
				$("#deliver_street").val(data.deliver_street);
				$("#deliver_zip_code").val(data.deliver_zip_code);
				$("#deliver_city").val(data.deliver_city);
				//$("#deliver_consignee")
				//$("#linkman").val(data.deliver_contact);
				//$("#linkman_number").val(data.deliver_phone);
				var ccid_selected = "#ccid_hidden option[value="+data.deliver_nation+"]";
				$(ccid_selected).attr("selected", "selected");
				setPro_id(data.deliver_pro_id,data.deliver_pro_input);
				//$("#ccid_hidden").change(data.deliver_provice);
			}
		});
}
function exec(o) {
	if($(o).attr("id")=="payee_id") {
		if($("option:selected",o).val()!=0)
				$('#suppliers').val($("option:selected",o).attr('displayName'));
		else
				$('#suppliers').val('');
	}
}
//-->
</script>
<style type="text/css">
	body { font-size:62.5%; margin:0; padding:0; }
	#menuLog { font-size:1.4em; margin:20px; }
	.hidden { position:absolute; top:0; left:-9999px; width:1px; height:1px; overflow:hidden; }
	
	.fg-button { clear:left; margin:0 4px 40px 20px; padding: .4em 1em; text-decoration:none !important; cursor:pointer; position: relative; text-align: center; zoom: 1; }
	.fg-button .ui-icon { position: absolute; top: 50%; margin-top: -8px; left: 50%; margin-left: -8px; }
	a.fg-button { float:left;  }
	button.fg-button { width:auto; overflow:visible; }  /*removes extra button width in IE */
	
	.fg-button-icon-left { padding-left: 2.1em; }
	.fg-button-icon-right { padding-right: 2.1em; }
	.fg-button-icon-left .ui-icon { right: auto; left: .2em; margin-left: 0; }
	.fg-button-icon-right .ui-icon { left: auto; right: .2em; margin-left: 0; }
	.fg-button-icon-solo { display:block; width:8px; text-indent: -9999px; }	 /* solo icon buttons must have block properties for the text-indent to work */	
	
	.fg-button.ui-state-loading .ui-icon { background: url(spinner_bar.gif) no-repeat 0 0; }
	</style>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
table.receiver td {border:0px;}
table.receiver td input.noborder{border-bottom:1px solid silver;width:200px;}
table.receiver td.left{width:150px;text-align:right;}
table.receiver{border-collapse:collapse ;}
span._hand{cursor:pointer;}
</style>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_assets_form"  id="add_assets_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/AddAssetsPrecessAction.action?type=<%=type%>" >

<table width="98%" border="0" cellspacing="5" cellpadding="2">
					
    <%
    	if(type==1) {
    %>
  <tr >
    <td align="left" valign="top" style="padding-top: 30px"  nowrap="nowrap" class="STYLE1 STYLE2"> 
    	<%
    		out.print("固定资产:");
    	%>
    </td>
     <td  align="left" style="padding-top: 20px; " colspan="4">
    		<ul id="categorymenu" class="mcdropdown_menu">
					  <%
					  if(1==1) {
					  	  DBRow ca1[] = assetsCategoryMgr.getAssetsCategoryChildren(0,Integer.toString(type));
						  for (int i=0; i<ca1.length; i++)
						  {
								out.println("<li rel='"+ca1[i].get("id",0l)+"'> "+ca1[i].getString("chName"));
								  DBRow[] ca2 = assetsCategoryMgr.getAssetsCategoryChildren(ca1[i].get("id",0l),"1");
								  if (ca2.length>0)
								  {
								  		out.println("<ul>");
								  }
								  for (int ii=0; ii<ca2.length; ii++)
								  {
										out.println("<li rel='"+ca2[ii].get("id",0l)+"'> "+ca2[ii].getString("chName"));		
										//System.out.println(ca2[ii].getString("chName"));
										
										out.println("</li>");				
									  
								  }
								  if (ca2.length>0)
								  {
								  		out.println("</ul>");	
								  }
								out.println("</li>");
						  }
					  }
					  %>
				</ul>
					<input type="hidden" id="adid" name="adid" value="<%=adminLoggerBean.getAdid()%>" />
					  <input type="text" name="category" id="category" value="" />
					  <input type="hidden" name="categoryId" id="categoryId" value="0" />

       <script>
       $("#category").mcDropdown("#categorymenu",{width:200,allowParentSelect:true,targetColumnSize:1});
  $("#category").mcDropdown("#categorymenu",{
						allowParentSelect:true,
						  select: 
						  
								function (id,name)
								{
									$("#categoryId").val(id);
									//alert($("#filter_lid1").val());
								}
				
				});								
				$("#category").mcDropdown("#categorymenu").setValue(0);
  </script>	
    </td>
    </tr>
        <%} %>
					  <input type="hidden" name="productLine" id="productLine" value="" />
					  <input type="hidden" name="product_line_id" id="product_line_id" value="0" />

  
  <tr>
  <td align="right" valign="middle" class="STYLE3" nowrap="nowrap">商品信息:</td>
  <input type="hidden" name="a_name" id="a_name" />
  <td colspan="3">
    		<h1 id="addBar"> </h1>
    	
    	<table class="receiver name next">
		      			<tr>
		      				<td style="width:120px;text-align:center;" class="STYLE2">商品名1:</td>
		      				<td align="left" valign="middle" width="20%"><input name="aname" type="text" class="input-line" id="aname" ></td>
		      				<td style="width:120px;text-align:right;" class="STYLE2">型号1:</td>
		      				<td align="left" valign="middle" >
   							 <input name="model" type="text" id="model" class="input-line">    </td>
		      				<td style="width:80px;text-align:left;">
		      				<img alt="删除" src="../imgs/del.gif" onclick="deleteGoodsInfo(this)" />
		      				<img alt="添加" src="../imgs/add.gif" onclick="addName();">
		      				</td>
		      			</tr>
		      	</table>		 
    
    </td>
  </tr>
    <td align="right" valign="middle" class="STYLE3" nowrap="nowrap">采购价格:</td>
     <td align="left" valign="middle" colspan="3"><input name="unit_price" type="text" id="unit_price"   style="width:100px;">   
    	<select id="currency" name="currency">
    		<option value="RMB">RMB</option>
    		<option value="USD">USD</option>
    	</select>
    	/
    	<select id="unit" name="unit_name">
    		<option value="0">--单位--</option>
    		<option value="套">套</option>
    		<option value="个">个</option>
    		<option value="只">只</option>
    		<option value="对">对</option>
    		<option value="台">台</option>
    		<option value="件">件</option>
    		<option value="把">把</option>
    	</select>
    	</td>
  </tr>
  <tr>
	    <td align="right" valign="middle" class="STYLE3">生产公司:</td>
	    <td align="left" colspan="3">
	    	<input name="suppliers" type="text" id="suppliers"  class="input-line">
	    </td>
    </tr> 
    <tr>
  	<td align="left" valign="middle" class="STYLE3" colspan="1">存放地点</td>
    	<td align="left" valign="middle" colspan="3">
    		<ul id="locationmenu" class="mcdropdown_menu">
					  <li rel="0">所有分类</li>
					  <%
					  DBRow c1[] = officeLocationMgr.getChildOfficeLocation(0);
					  for (int i=0; i<c1.length; i++)
					  {
							out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("office_name"));
				
							  DBRow c2[] = officeLocationMgr.getChildOfficeLocation(c1[i].get("id",0l));
							  if (c2.length>0)
							  {
							  		out.println("<ul>");	
							  }
							  for (int ii=0; ii<c2.length; ii++)
							  {
									out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("office_name"));		
									
										DBRow c3[] = officeLocationMgr.getChildOfficeLocation(c2[ii].get("id",0l));
										  if (c3.length>0)
										  {
												out.println("<ul>");	
										  }
											for (int iii=0; iii<c3.length; iii++)
											{
													out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("office_name"));
													out.println("</li>");
											}
										  if (c3.length>0)
										  {
												out.println("</ul>");
										  }
										  
									out.println("</li>");				
							  }
							  if (c2.length>0)
							  {
							  		out.println("</ul>");	
							  }
							  
							out.println("</li>");
					  }
					  %>
				</ul>
					  <input type="text" name="location" id="location" value="" />
					  <input type="hidden" name="filter_lid" id="filter_lid" value=""  />
				
				<script>
				
				$("#location").mcDropdown("#locationmenu",{
						allowParentSelect:true,
						  select: 
						  
								function (id,name)
								{
									$("#filter_lid").val(id);
								}
				
				});								
				$("#location").mcDropdown("#locationmenu").setValue(0);
				
				</script>      </td>
  </tr>
      
  <tr> 
		      <td align="left" valign="middle" class="STYLE3" height="29">成本中心:</td>
		      <td colspan="3">
		      	<div style="float:left;" id="center_account_type_id_div" name="center_account_type_id_div">		      	
				</div>
				<div id='center_account_type1_id_div' name='center_account_type1_id_div' style="float:left;">
				</div>
				<div id='center_account_id_div' name='center_account_id_div' style="float:left;">
		        </div>
		      </td>
	</tr>
				<tr>
				<td align="right" valign="middle" class="STYLE3">好坏情况:</td>
				<td  align="left" valign="middle" nowrap="nowrap" colspan="3">
						      	<select id="state" name="state">
						      		<option value="<%=goodsCaseKey.intact %>"<%=state==goodsCaseKey.intact?"selected":"" %>><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.intact) %></option>
						      		<option value="<%=goodsCaseKey.function_damage %>"<%=state==goodsCaseKey.function_damage?"selected":"" %>><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.function_damage) %></option>
						      		<option value="<%=goodsCaseKey.facade_damage %>"<%=state==goodsCaseKey.facade_damage?"selected":"" %>><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.facade_damage) %></option>
						      		<option value="<%=goodsCaseKey.scrap %>"<%=state==goodsCaseKey.scrap?"selected":"" %>><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.scrap) %></option>
						      	</select>
						      	&nbsp;&nbsp;
						      <span class="STYLE3">货品状态:</span>
						      	<select id="goodsstate" name="goodsstate">
							<option value="<%=goodsStatusKey.no_arrival%>" <%=goodsstate==goodsStatusKey.no_arrival?"selected":"" %>><%=goodsStatusKey.getGoodsStatusById(goodsStatusKey.no_arrival)%></option>
							<option value="<%=goodsStatusKey.arrival%>" <%=goodsstate==goodsStatusKey.arrival?"selected":"" %>><%=goodsStatusKey.getGoodsStatusById(goodsStatusKey.arrival)%></option>						
						</select>
						&nbsp;&nbsp;
						 <span class="STYLE3">资金申请:</span>
						<select name='applystate' id='applystate'>
	                                <option value="<%=applyStatusKey.norequired_apply%>">不需要</option>
	                                 <option value="<%=applyStatusKey.no_apply%>">需要</option>
	                               
	               </select>
			</td>
			</tr>
  <tr>
  	<td align="left" valign="middle" class="STYLE3" ></td>
    <td align="left" valign="middle" ></td>
   <td align="left" valign="middle" class="STYLE3" ></td>
    <td align="left" valign="middle" >    </td>
  </tr>
 
	  <tr>
	    <td align="right" valign="middle" class="STYLE3">收货人:</td>
	    <td align="left" width="20%">
	    	<input name="consignee" type="text" id="consignee"  class="input-line">
	    </td>
	    <td valign="middle" align="right" class="STYLE3">联系电话:</td>
	    <td align="left">
	    	<input name="phone" type="text" id="phone"  style="width:100px;">
	    </td>
    </tr>  
    
    <tr>
				  <td width="10%"  align="right" valign="middle" class="STYLE3">收货地址:</td>
			      <td width="90%"  align="left" valign="middle" nowrap="nowrap" colspan="3">
							      <select name='select' id='ps_id' onChange="selectStorage()">
	                                <option value="0">请选择...</option>
	                                <%
										  for ( int i=0; i<treeRows.length; i++ )
										  {
										%>
	                                <option value='<%=treeRows[i].getString("id")%>'><%=treeRows[i].getString("title")%></option>
	                                <%
										}
										%>
	                              </select>
                              </td>
							</tr>
						  <tr>
			  	 <td width="10%"  align="right" valign="middle" class="STYLE3">门牌号:</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap" colspan="3"><input name="deliver_house_number" type="text" id="deliver_house_number" style="width:400px;"/></td>
						  </tr>
						  <tr>
			  	 <td width="10%"  align="right" valign="middle" class="STYLE3">街道:</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap" colspan="3"><input name="deliver_street" type="text" id="deliver_street" style="width:400px;"/></td>
						  </tr>
						  <tr>
			  	 <td width="10%"  align="right" valign="middle" class="STYLE3">城市:</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap" colspan="3"><input name="deliver_city" type="text" id="deliver_city" style="width:400px;"/></td>
						  </tr>
						  <tr>
			  	 <td width="10%"  align="right" valign="middle" class="STYLE3">邮编:</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap" colspan="3"><input name="deliver_zip_code" type="text" id="deliver_zip_code" style="width:400px;"/></td>
						  </tr>
						  <tr>
			  	 <td width="10%"  align="right" valign="middle" class="STYLE3">省份:</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap" colspan="3">
						      	<span id="addBillProSpan">
									<select disabled="disabled" id="pro_id" name="pro_id" onchange="handleProInput()" style="margin-right:5px;"></select>
								</span>
						      </td>
						  </tr>
						  <tr>
			  	 <td width="10%"  align="right" valign="middle" class="STYLE3">国家:</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap" colspan="3">
									<%
										DBRow countrycode[] = orderMgr.getAllCountryCode();
										String selectBg="#ffffff";
										String preLetter="";
									%>
							      	 <select  id="ccid_hidden"  name="ccid" onChange="removeAfterProIdInput();setPro_id();" >
								 	 	<option value="0">请选择...</option>
										  <%
										  for (int i=0; i<countrycode.length; i++){
										  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
												if (selectBg.equals("#eeeeee")){
													selectBg = "#ffffff";
												}else{
													selectBg = "#eeeeee";
												}
											}  	
											preLetter = countrycode[i].getString("c_country").substring(0,1);
										  %>
								  		  <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
										  <%
										  }
										%>
						   			 </select>
							  </td>
						  </tr>
																															
 <tr>
	<td>
	<input type="hidden" name="goodsstate" id="goodsstate" value="1"/>
    <input type="hidden" name="category_id" id="category_id" value="<%=category_id %>"/>
    <input type="hidden" name="type" id="type" value="<%=type %>"/>
    <input type="hidden" name="state" id="state" value="4"/>      
    <input type="hidden" name="requisitioned" id="requisitioned" value=""/>
    <input type="hidden" name="requisitioned_id" id="requisitioned_id" value="0"/>
    <input type="hidden" name="cmd" id="cmd">
    <input type="hidden" name="applystate" id = "applystate">
    </td>
  </tr>

</table>
</form></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line"><img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#999999">必须把资产增加到最底层分类</span>         </td>
    <td width="49%" align="right" class="win-bottom-line">
   <input type="button" name="Submit2" id="Submit2" value="确认" class="normal-green-long" onClick="addAssets();">	
	 &nbsp;&nbsp;
	 <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWindow();">
	</td>
  </tr>
</table>
<script type="text/javascript">
function closeWindow(){
	$.artDialog.close();
}
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 
	$(".chzn-select").chosen(); 
	$(".chzn-select1").chosen(); 
	$(".chzn-select2").chosen(); 
</script>
</body>
</html>
