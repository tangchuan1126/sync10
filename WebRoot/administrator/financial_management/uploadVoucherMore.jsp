<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@page import="com.cwc.app.key.AccountKey"%>
<%@ include file="../../include.jsp"%> 
<% 

	long transferId=StringUtil.getLong(request,"aid",0l);
	long applyMoneyId=StringUtil.getLong(request,"applyMoneyId");
	String transfer_ids = StringUtil.getString(request,"transfer_ids");
	
	ApplyTypeKey typeKey = new ApplyTypeKey();
	DBRow applyTransfer[] = null;
	DBRow applyMoney[]=null;
	applyTransfer = applyMoneyMgrLL.getApplyTransferByIds(transfer_ids,null); 
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script>
<!--

function btnSubmit()
{
   $("#add_handle_form").attr("action","<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/uploadFileAction.action");
   var flag=checkForm();
   if(flag)
   {
      $("#add_handle_form").submit();

   }
}

function checkForm()
{
      var voucher=$("#file_names").val();
      
      if($("#payment_information").val().trim()==""||$("#payment_information").val()==null)
 	 {
 	     $("#payment_information").focus();
 	     alert("付款信息不能为空！");
 	     return false;
 	 }
 	 else
 	 {
 	 	 if($("#payment_information").val().trim().length>600)
 	 	 {
 	 	 	 $("#payment_information").focus();
  	 		 alert("付款信息已超过字数上限600，请调整字数！");
  	 		 return false;
 	 	 }
 	 }

      var transfer_id = $("#transfer_ids").val().split(",");
      for(i=0;i<transfer_id.length;i++) {
	      if($("#money"+transfer_id[i]).val().trim()==""||$("#money"+transfer_id[i]).val()==null)
	      {
	      	 $("#money"+transfer_id[i]).focus();
	      	 alert("请输入实际金额！");
	         return false;
	      }
	      else
	      {
		      if(isNaN($("#money"+transfer_id[i]).val().trim()))
		      {
		      	  $("#money"+transfer_id[i]).focus();
			      alert("实际金额必须为数字！");
			      return false;
		      }
	      }
      }
      
      if(voucher.trim()==""||voucher==null)
      {
         alert("请选择您要上传的图片！");
         return false;
      }
      
        
      //voucher=voucher.substring(voucher.length-3,voucher.length);
      //if(voucher!="jpg" && voucher!="gif" && voucher!="bmp")
      //{
      //	 alert("图片格式不正确，请检查！");
      //   return false;
      //}
      
      
      return true;
   
}
//文件上传
function uploadFile(_target){
    var fixWidth = "770px" , fixHeight = "530px" ;
    
    if(window.screen.availWidth  < 1400){
		fixWidth = "650px";
		fixHeight = "400px";
	}
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:2,
	     target:_target,
	     limitNum:1
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:fixWidth,height:fixHeight, lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
// 回显要求看上去已经是上传了的文件。所以上传是要加上前缀名的sn
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_names']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
	 	//判断是不是有了
	 	if($("a",$("#over_file_td")).length > 0){
			var td = $("#over_file_td");
	
			td.append(lis);
		}else{
			$("#file_up_tr").attr("style","");
			$("#jquery_file_up").append(lis);
		}
	} 
}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+ "_" + fileName;
    var  a = "<p class='new'><a href="+uri+">"+showName+"</a></p>";
    return a ;
  
}
function closeDialogCancel(){
	$.artDialog && $.artDialog.close();
}
function onlineScanner(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
//-->
</script>
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="add_handle_form" id="add_handle_form" method="post">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top:10px;margin-left:22px;">
  <tr>
    <td align="center">
<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">上传凭证</legend>
<table width="100%" height="21%" border="0" cellpadding="0" cellspacing="0">		    		    
    <tr>
      <td align="right" class="STYLE2" width="14%">付款账号:</td>
      <td>&nbsp;</td>
      <td>
       <input type="text" id="payment_information" name="payment_information" value="622208 0200010209172"/>
	  </td>
	  <td width="60%"></td>
    </tr>
    <tr> 			
      <td align="right"><span class="STYLE2">收款账号:</span></td>
      <td>&nbsp;</td>
      <td>
       <input type="text" id="receiver_account" name="receiver_account"/>
       <input type="hidden" id="transfer_ids" name="transfer_ids" value="<%=transfer_ids %>">
       <input type="hidden" id="amount_sum" name="amount_sum">
       
	  </td>
	  <td></td>
    </tr>
</table>

<%
	float total_amount = 0;
	for(int i=0;i<applyTransfer.length;i++) {
		applyMoney=applyMoneyMgrZZZ.getApplyMoneyById(applyTransfer[i].getString("apply_money_id"));
%>
<hr size="1" width="90%">
<table width="100%" height="21%" border="0" cellpadding="0" cellspacing="0">
			<tr>
	          <td width="14%" align="right" class="STYLE2">付款人:</td>
	          <td width="1%">&nbsp;</td>
	          <td width="80%">		          
	          <%=applyTransfer[i].getString("payer") %>
	          </td>
	        </tr>		    		    
		    
		    <tr>
	          <td align="right" class="STYLE2">收款人:</td>
	          <td>&nbsp;</td>
	          <td>
	          	<%=applyTransfer[i].getString("receiver") %>
	          </td>
	        </tr>
		    <tr> 			
		      <td align="right"  valign="top"><span class="STYLE2">收款信息:</span></td>
		      <td>&nbsp;</td>
		      <td>
		      	    <!-- 查询account表中,如果有了数据那么就是要显示Account表中的数据 -->	
				  	<!-- 如果没有那么就是要显示字段里面的东西 -->
				  	<!-- 兼容以前 -->
				  	<%
				  		DBRow[] accountInfos = accountMgrZr.getAccountByAccountIdAndAccountWithType(applyTransfer[i].get("transfer_id",0l),AccountKey.APPLY_TRANSFER);
				  		if(accountInfos != null && accountInfos.length > 0){
				  			for(DBRow tempAccount :  accountInfos){
				  	%>
				  					 
				  					<p>收款人户名:<%=tempAccount.getString("account_name") %></p>
				  					<p>收款人银行:<%=tempAccount.getString("account_blank") %></p>
				  					<p>收款人账号:<%=tempAccount.getString("account_number") %></p>
				  					<%if(tempAccount.getString("account_phone").length() > 0 ){ %>
				  						<p>收款人电话:<%=tempAccount.getString("account_phone") %></p>	
				  					<%} %>
				  					 <%if(tempAccount.getString("account_swift").length() > 0 ){ %>
				  						<p>SWIFT Code:<%=tempAccount.getString("account_swift") %></p>	
				  					<%} %> 
				  					<%if(tempAccount.getString("account_address").length() > 0 ){ %>
				  						<p>收款人地址:<%=tempAccount.getString("account_address") %></p>	
				  					<%} %>
				  					<%if(tempAccount.getString("account_blank_address").length() > 0 ){ %>
				  						<p>开户行地址:<%=tempAccount.getString("account_blank_address") %></p>	
				  					<%} 
				  			}
				  		}else{
				  			String receiver_information = applyTransfer[i].getString("receiver_information");
				  			receiver_information = receiver_information.replaceAll("\n","<br>");
			           		out.print(receiver_information);
				  		}
			           %>
			  </td>
		    </tr>
	<tr>
	  <td height="25" align="right" class="STYLE2" nowrap="nowrap">类型:</td>
	  <td>&nbsp;</td>
	  <td align="left">	
	    <%  
	    	if(applyMoney!=null)
		    {
		    	typeKey.getApplyTypeStatusById(applyMoney[0].getString("types")); 
		    } 
	    %>	  
	          
	    <%  if(applyTransfer!=null)
		    {
		    	applyTransfer[i].get("amount",0d);
		    	total_amount = total_amount + applyTransfer[i].get("amount",0f);
		    } 
	    %>  
	    <%  if(applyTransfer!=null)
		    {
		    	applyTransfer[i].getString("currency");
		    } 
	    %>
	  </td>
	</tr>
	<tr>
	  <td height="25" align="right"  class="STYLE2" nowrap="nowrap">实际金额:</td>
	  <td>&nbsp;</td>
	  <td align="left">		          
	  	<input name="money<%=applyTransfer[i].getString("transfer_id") %>" id="money<%=applyTransfer[i].getString("transfer_id") %>" value="<%=applyTransfer[i].get("amount",0d) %>"/>
        <input type="hidden" id="applyMoneyId" name="applyMoneyId" value="<%=applyTransfer[i].get("apply_money_id",0d) %>"/>
        <input type="hidden" id="applyTransferId" name="applyTransferId" value="<%=applyTransfer[i].get("transfer_if",0d) %>" />
	  </td>
	</tr>
</table>
<%
	}
%>
<hr size="1" width="90%">
<table width="100%" height="21%" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td height="25" align="right"  class="STYLE2" nowrap="nowrap" width="14%" >总计:</td>
	  <td width="5">&nbsp;</td>
       <td align="left">
		<%=total_amount %>
	   </td>
	 </tr>	 
</table>
<hr size="1" width="90%">
<table width="100%" height="21%" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td height="25" align="right"  class="STYLE2" nowrap="nowrap" width="14%" >上传凭证:</td>
	  <td>&nbsp;</td>
       <td >
     	  	<span id="file_up_span">
				<input type="hidden" id="sn" name="sn" value="W_transfer"/>
				<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_financial")%>" />
				<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="上传凭证" />
				<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
		 	</span>
<%--        	<input type="file" id="voucher" name="voucher" />--%>
	   </td>
	   <td align="left" nowrap="nowrap"><font style="color: red"></font></td>	
	 </tr>	
	 <tr id="file_up_tr" style='display:none;'>
		 	<td align="right" class="STYLE2">完成凭证:</td>
		 	<td>&nbsp;</td>
		 	<td>
             	<div id="jquery_file_up">	
             		<input type="hidden" name="file_names" id="file_names" value=""/>
             	</div>
		 	</td>
 	</tr>	  
</table>
	</fieldset>	
	</td>
  </tr>

</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onClick="btnSubmit()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onClick="closeDialogCancel();" value="取消" >
    </td>
  </tr>
</table>
</form>
</body>
</html>
