<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey;import com.cwc.app.api.ll.Utils"%>
<%@ include file="../../include.jsp"%>
<%
	com.cwc.app.iface.ll.ApplyMoneyIFace applyMoneyAPI = (com.cwc.app.iface.ll.ApplyMoneyIFace)MvcUtil.getBeanFromContainer("applyMoneyAPI"); 

	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	DBRow row =Utils.getDBRowByName("apply_money", request);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>

<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="apply_money_form" id="apply_money_form" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">资金申请单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr height="29">
		          <td align="right" class="STYLE2">相关单据号:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle" bgcolor="#eeeeee">
	  				<input type="hidden" name="apply_money.types" id="apply_money.types" value="10015" />
		          </td>
		        </tr> 
		    	<input type="hidden" name="apply_money.center_account_id" id="apply_money.center_account_id" value="<%=row.getValue("center_account_id")%>">		
		  <tr> 
		      <td align="right" class="STYLE2">金额:</td>
		      <td>&nbsp;</td>
		      <td><input name="apply_money.amount" id="apply_money.amount" type="text" size="15" value="<%=row.getValue("amount")%>"></td>
		    </tr>
				  
		    <tr> 
		      <td align="right" class="STYLE2">收款人:</td>
		      <td>&nbsp;</td>
		     
		      <td><input name="apply_money.payee" id="apply_money.payee" /></td>
		   
		    </tr>		 
		    <tr> 
		      <td align="right" class="STYLE2">付款信息:</td>
		      <td>&nbsp;</td>
		      <td>		      		    		     
		      	<textarea rows="4" cols="40" name="apply_money.payment_information" id="payment_information" ><%=row.getValue("payment_information")%></textarea>
		      </td>
		    </tr>
		     		    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="apply_money.remark" rows="4" cols="40"><%=row.getValue("remark")%></textarea></td>
		    </tr>
		</table>
		<input id="apply_money.apply_id" name="apply_money.apply_id" type="hidden" value="<%=row.getValue("apply_id")%>"/>
		<input id="apply_money.associationId" name="apply_money.associationId" type="hidden" value="<%=row.getValue("associationId")%>"/>
		<input id="apply_money.adid" name="apply_money.adid" type="hidden" value="<%=adminLoggerBean.getAdid() %>"/>
		
	</fieldset>	
	</td>
  </tr>

</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onclick="submitApply()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="parent.closeWinNotRefresh()" value="取消" >
    </td>
  </tr>

</table>
  </form>
<script type="text/javascript">
<!--
	function submitApply()
	{
			 if($('input[name=apply_money.amount]').val().trim()==""||$('input[name=apply_money.amount]').val()==null)
			 {
			     $('input[name=apply_money.amount]').focus();
			     alert("金额不能为空！");
			 }
			 else if(isNaN($('input[name=apply_money.amount]').val().trim()))
		 	 {
			    $('input[name=apply_money.amount]').focus();
			     alert("金额必须为数字！");
		 	 }
			 else if($('input[name=apply_money.amount]').val().trim()<=0)
			 {
			 	 $('input[name=apply_money.amount]').focus();
			     alert("金额必须大于零！");
			 }
			 else if($('input[name=apply_money.payee]').val().trim()==""||$('input[name=apply_money.payee]').val()==null)
			 {
			 	 $('input[name=apply_money.payee]').focus();
			 	 alert("请填写收款人！");
			 }
			 else if($("#payment_information").val().trim()==""||$("#payment_information").val()==null)
			 {
			 	 $("#payment_information").focus();
			 	 alert("请填写付款信息！");
			 } 
			 else if($("#payment_information").val().trim().length>600)
			 {
			 	 $("#payment_information").focus();
			 	 alert("付款信息不能超过600字符！");
			 }			 
			 else if($("#remark").val().trim().length>=200)
			 {
			     $("#remark").focus();
			     alert("备注字数不能超过200！");
			 }
			 else
			 {
				document.apply_money_form.submit();
			 }	
	}
//-->
</script>
</body>
</html>

