<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.util.StringUtil"%>
<%@ include file="../../include.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <%
    	long transfer_apply_id	= StringUtil.getLong(request, "transfer_apply_id");
    	String last_time_old	= StringUtil.getString(request, "last_time_old");
    	
    %>
    <title>修改最迟转款时间</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
	<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
	<link href="../comm.css" rel="stylesheet" type="text/css">
		<!-- 引入date -->
	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
	
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	
	<script type="text/javascript">
	function submitData(){
		if(volidate()){
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/UpdateTransferMoneyLastTimeAction.action',
				data:$("#subForm").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "success"){
						showMessage("修改成功","success");
						setTimeout("windowClose()", 1000);
					}else{
						showMessage("修改失败","error");
					}
				},
				error:function(){
					showMessage("系统错误","error");
				}
			})	
		}
	};
	function volidate(){
		if($("#last_time").val() == ""){
			alert("最迟转款时间不能为空");
			return false;
		}
		return true;
	};
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	</script>
	<style type="text/css">
		#lastTimeTable tr td{height: 30px;line-height: 27px;font-size: 13px;}
	</style>
  </head>
  
  <body>
	<form action="" id="subForm" name="subForm" method="post">
	<input type="hidden" id="transfer_apply_id" name="transfer_apply_id" value='<%=transfer_apply_id %>'/>
		<table align="left" width="100%" height="100%">
			<tr width="100%" height="80%" valign="top">
				<td valign="top">
					<table id="lastTimeTable">
						<tr>
							<td align="right">原最迟转款时间:</td>
							<td align="left">
								<%=last_time_old %>
							</td>
						</tr>
						<tr>
							<td align="right">最迟转款时间:</td>
							<td align="left">
								<input name="last_time" id="last_time" value='<%=last_time_old %>'/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr width="100%" height="20%">
				<td width="100%" height="100%">
					<table width="100%" height="100%">
						<tr align="right">
							<td align="right"><input type="button" class="normal-green" value="提交" onclick="submitData();"/></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
	</form>
	<script type="text/javascript">
		$("#last_time").date_input();
	</script>
		<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
  </body>
</html>
