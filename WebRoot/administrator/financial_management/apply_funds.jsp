<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.zzz.ApplyMoneyMgrZZZ"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@page import="com.cwc.app.key.AccountKey"%>
<%@page import="com.cwc.app.lucene.zr.ApplyMoneyIndexMgr"%>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="financeApplyTypeKey" class="com.cwc.app.key.FinanceApplyTypeKey"></jsp:useBean>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%>
<%
	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(systemConfig.getIntConfigValue("order_page_count"));
 	
 	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	long center_account_id = StringUtil.getLong(request,"center_account_id");
	long center_account_type_id = StringUtil.getLong(request,"center_account_type_id");
	long center_account_type1_id = StringUtil.getLong(request,"center_account_type1_id");
	long add = StringUtil.getLong(request,"add");
	String selectSortTime=StringUtil.getString(request,"selectSortTime");
	
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 	
 	String id=StringUtil.getString(request,"apply_id","0");

 	String cmd = StringUtil.getString(request,"cmd");
 	int search_mode = StringUtil.getInt(request,"search_mode");
 	DBRow applyMoney[]= null;
 	if(cmd.equals("filter"))
 	{
		applyMoney = applyMoneyMgrZZZ.getApplyMoneyByCondition(request,pc);
 	}
	else if(cmd.equals("search"))
 	{
 	    applyMoney = applyMoneyMgrZZZ.getApplyMoneyByInfo(id,pc);
 	}
	else if(cmd.equals("query"))
	{
	  	applyMoney = applyMoneyMgrZZZ.getApplyMoneyById(id);
	}
 	else if(cmd.equals("searchby_index"))
 	{
 		 applyMoney = applyMoneyMgrZZZ.getApplyMonyByIndexMgr(id,search_mode,pc);
	}
 	else if(cmd.equals("last_time"))
 	{
 		applyMoney = applyMoneyMgrZZZ.getApplyMoneyLastTimeSort(pc);
 	}
 	else if(cmd.equals("create_time"))
 	{
 		applyMoney = applyMoneyMgrZZZ.getApplyMoneyCreateTimeSort(pc);
 	}
	else
 	{
 		applyMoney = applyMoneyMgrZZZ.getAllApplyMoney(pc);
 	}

	DBRow[] associationType = applyMoneyMgrLL.getMoneyAssociationTypeAll();
	DBRow[] accounts = applyMoneyMgrLL.getAllByReadView("accountReadViewLL");
	DBRow[] accountCategorys = applyMoneyMgrLL.getAllByTable("accountCategoryBeanLL");
	DBRow[] adminGroups = applyMoneyMgrLL.getAllByTable("adminGroupBeanLL");
	DBRow[] productLineDefines = applyMoneyMgrLL.getAllByTable("productLineDefineBeanLL");
	if(id.equals("0")){
		id= "";
	}
	//文件转换的action路径
	String fileConvertAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/file/ConvertFileAction.action";
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
%>
<%!private String getCenterAccountName(DBRow[] accounts,long center_account_type_id,long center_account_id){
		String account_name = "";

		for(int i=0;i<accounts.length;i++) {			
			if(accounts[i].get("account_category_id",0)==center_account_type_id&&accounts[i].get("acid",0)==center_account_id) {
				account_name = accounts[i].getString("account_name");
				break;
			}
		}
		return account_name;
	}%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>资金申请列表</title>
<script language="javascript">

<!--
//加载控件
<%
	String v = "var accounts = new Array(";
	for(int i=0;i<accounts.length;i++) {
		DBRow account = accounts[i];
		if(i==0)
			v += "new Array("+account.get("acid",0)+","+account.get("account_category_id",0)+",'"+account.getString("account_name")+"',"+account.getString("account_parent_id")+")";
		else
			v += ",new Array("+account.get("acid",0)+","+account.get("account_category_id",0)+",'"+account.getString("account_name")+"',"+account.getString("account_parent_id")+")";
	} 
	v+= ");";
	out.println(v);
%>
<%
	String v1 = "var adminGroups = new Array(";
	for(int i=0;i<adminGroups.length;i++) {
		DBRow adminGroup = adminGroups[i];
		if(i==0)
			v1 += "new Array("+adminGroup.get("adgid",0)+",2,'"+adminGroup.getString("name")+"')";
		else
			v1 += ",new Array("+adminGroup.get("adgid",0)+",2,'"+adminGroup.getString("name")+"')";
	} 
	v1 += ");";
	out.println(v1);
%>
<%
	String v2 = "var productLineDefines = new Array(";
	for(int i=0;i<productLineDefines.length;i++) {
		DBRow productLineDefine = productLineDefines[i];
		if(i==0)
			v2 += "new Array("+productLineDefine.get("id",0)+",3,'"+productLineDefine.getString("name")+"')";
		else
			v2 += ",new Array("+productLineDefine.get("id",0)+",3,'"+productLineDefine.getString("name")+"')";
	} 
	v2 += ");";
	out.println(v2);
%>
function selectClass(select) {
	var classId = 0;
	
	for(var i=0; i<select.options.length; i++) {
		if(select.options[i].selected==true) {
			classId = select.options[i].value;
		}
	}
	
	if(classId == 0) {
		getAccountSelect1('center_account_type1_id','chzn-select2','',classId,0);
		getAccountSelect('center_account_id','chzn-select',accounts,-1,-1,0);
	}
	else if(classId == 1) {
		
		getAccountSelect1('center_account_type1_id','chzn-select2','',classId,0);
		
		getAccountSelect('center_account_id','chzn-select',accounts,classId,0,0);
	}
	else if(classId == 2) {
		getAccountSelect1('center_account_type1_id','chzn-select2',adminGroups,classId,0);
		getAccountSelect('center_account_id','chzn-select',accounts,-1,-1,0);
	}
	else if(classId == 3) {
		getAccountSelect1('center_account_type1_id','chzn-select2',productLineDefines,classId,0);
		getAccountSelect('center_account_id','chzn-select',accounts,-1,-1,0);
	}
	
}

function selectClass1(select) {
	var classId = 0;
	var parentId = 0;
	
	for(var i=0; i<select.options.length; i++) {
		if(select.options[i].selected==true) {
			parentId = select.options[i].value;
			classId = $(select.options[i]).attr('rel');
			break;	
		}
	}
	
	getAccountSelect('center_account_id','chzn-select',accounts,classId,parentId);
}

function getAccountSelect2(name,className,classId) {	
	var selectHtml = "<select name='"+name+"' id='"+name+"' class='"+className+"' style='width:100px;' onchange='selectClass(this)'>";

	selectHtml += "<option value='0'>任意</option> ";
	selectHtml += "<option value='1' "+(classId==1?"selected":"")+">公司账户</option>";
	selectHtml += "<option value='2' "+(classId==2?"selected":"")+">职员账户</option>";
	selectHtml += "<option value='3' "+(classId==3?"selected":"")+">供应商</option>";
	
	selectHtml += "</select>";

	$('#'+name+'_Div').html('');
	$('#'+name+'_Div').append(selectHtml);
}

function getAccountSelect1(name,className,parentArray,classId,selectValue) {	
	var selectHtml = "<select name='"+name+"' id='"+name+"' class='"+className+"' style='width:120px;' onchange='selectClass1(this)'>";
	if(classId==0) {//任意
		selectHtml += "<option value='0' rel='0'>任意</option> ";
	}
	else if(classId==1){//部门
		selectHtml += "<option value='0' rel='1'>选择成员</option> ";
	}
	else if(classId == 2) {//人员，先显示部门
		selectHtml += "<option value='-1' rel='-1'>选择成员</option> ";
		for(var i=0; i<parentArray.length; i++) {
			selectHtml += "<option value='"+parentArray[i][0]+"' "+(selectValue==parentArray[i][0]?"selected":"")+" rel='" + parentArray[i][1] + "'>"+parentArray[i][2]+"</option>";
		}
	}
	else if(classId == 3) {//供应商,先显示产品线
		selectHtml += "<option value='-1' rel='-1'>选择成员</option> ";
		for(var i=0; i<parentArray.length; i++) {
			selectHtml += "<option value='"+parentArray[i][0]+"' "+(selectValue==parentArray[i][0]?"selected":"")+" rel='" + parentArray[i][1] + "'>"+parentArray[i][2]+"</option>";
		}
	}
	
	selectHtml += "</select>";
	
	$('#'+name+'_Div').html('');
	$('#'+name+'_Div').append(selectHtml);
}

function getAccountSelect(name,className,accountArray,classId,parentId,selectValue) {	
	
	var selectHtml = "<select name='"+name+"' id='"+name+"' class='"+className+"' style='width:160px;' onchange='selectEmp()'>";
	selectHtml += "<option value='0' rel='0'>选择成员</option> ";
	
	for(var i=0; i<accountArray.length; i++) {
		if(classId==0) {
			selectHtml += "<option value='"+accountArray[i][0]+"' rel='" + accountArray[i][1] + "'>"+accountArray[i][2]+"</option>";
		}else if(classId == accountArray[i][1] && parentId == accountArray[i][3]){	
			selectHtml += "<option value='"+accountArray[i][0]+"' "+(selectValue==accountArray[i][0]?"selected":"")+" rel='" + accountArray[i][1] + "'>"+accountArray[i][2]+"</option>";
		}
	}
	selectHtml += "</select>";
	
	$('#'+name+'_Div').html('');
	$('#'+name+'_Div').append(selectHtml);
}
function closeWinRefresh(){window.location.reload();}

//图片在线显示
function showPictrueOnline(associationId ,associationType,currentName){
    var obj = {
		association_id : associationId,
		association_type:associationType,
		current_name : currentName ,
		cmd:"multiFile",
		table:'apply_images',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_financial")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}

//-->
</script>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script language="javascript">
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")


</script>
<script language="javascript">
function selectAccount() {
	var account_select = filterForm.account_select;	     
	var center_account_id = filterForm.center_account_id;
	var account_category_id = 0;
	
	for(var i=0; i<account_select.options.length; i++) {
		if(account_select.options[i].selected==true) {
			account_category_id = account_select.options[i].value;
		}
	}
	
	for(var i=0; i<center_account_id.options.length; i++) {
		if(account_category_id == 0) {
			center_account_id.options[i].style.display = '';
		}
		else if((account_category_id != $(center_account_id.options[i]).attr("rel")) && $(center_account_id.options[i]).attr("rel")!=0) {
			center_account_id.options[i].style.display = 'none';

		} else {
			center_account_id.options[i].style.display = '';
		}
	}
	
	center_account_id.options[0].selected=true;
}
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
 <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 文件查看  -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<style>
.ui-tabs .ui-tabs-panel{padding-right:0em;};
body
{
	font-size:12px;
}

 
form
{
	padding:0px;
	margin:0px;
}
 
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
 .searchbarGray{
        color:#c4c4c4;font-family: Arial
   }
 
</style>

<script type="text/javascript">
<!--
  $(document).ready(function(){
  init(); 
  $("#category").mcDropdown("#categorymenu",{
		allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{					
					$("#filter_acid").val(id);
				}
});

$("#category").mcDropdown("#categorymenu").setValue(0);
  <%
  long categoryId=StringUtil.getLong(request,"category");//filter_acid
  long productLineId=StringUtil.getLong(request,"productLine");
  String status=StringUtil.getString(request,"status");
  long association_type_id=StringUtil.getLong(request,"association_type_id");
  
  if(cmd.equals("filter"))
 	{
		%>
		  $("#st").val("<%=st%>");
		  $("#en").val("<%=en%>");	 
		  $("#status option[value='<%=status%>']").removeAttr("selected");
		  $("#status option[value='<%=status%>']").attr("selected","selected");
		  $("#association_type_id option[value='<%=association_type_id%>']").removeAttr("selected");
		  $("#association_type_id option[value='<%=association_type_id%>']").attr("selected","selected");
  	<%
	if (categoryId!=0l)
	{
	%>
	$("#category").mcDropdown("#categorymenu").setValue(<%=categoryId%>);
	<%
	}
	else
	{
	%>
	$("#category").mcDropdown("#categorymenu").setValue(0);
	<%
	}
	%> 	  
		<%
		
 	    cmd="";
 	}
 	else if(cmd.equals("search"))
 	{
 	    %>
 	    $("#apply_id").val("<%=id%>");
		<%
 	    cmd="";
 	}
  
  %>
  
    init(); 
  });
  
  
  function goFundsTransferListByFundId(id)
  {
      window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id;      
  }
  
  function checkForm()
  {
      if($("#apply_id1").val().trim()==""||$("#apply_id1").val().trim()=="*申请单号、收款人、关联ID、付款信息、创建人"||$("#apply_id1").val()==null)
      {
         alert("请输入查询信息！");
         $("#apply_id1").focus();
         return false;
      }
      
      return true;
  }
  function query()
  {
	  var val = $("#apply_id1").val();  
      if(val.trim()==""||$("#apply_id1").val().trim()=="*申请单号、收款人、关联ID、付款信息、创建人"||$("#apply_id1").val()==null)
      {
         alert("请输入查询信息！");
         $("#apply_id1").focus();
      }
      else
      {
      	val = val.replace(/\'/g,'');
      	var val_search = "\'"+val+"\'";
		$("#apply_id1").val(val_search);
    	$("#search_mode").val(1);
    	$("#search").submit();  
      }
  }
  
  function queryRightButton()
  {
	  var val = $("#apply_id1").val();  
      if(val.trim()==""||$("#apply_id1").val().trim()=="*申请单号、收款人、关联ID、付款信息、创建人"||$("#apply_id1").val()==null)
      {
         alert("请输入查询信息！");
         $("#apply_id1").focus();
      }
      else
      {
      	val = val.replace(/\'/g,'');
		$("#apply_id1").val(val);
    	$("#search_mode").val(2);
    	$("#search").submit();  
      }
  }
  
  function eso_button_even()
  {
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
			
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
	       	queryRightButton();
	   	}  
	}  
  }
  
  function closeWinNotRefresh()
  {
     tb_remove();
  }
  
  function updateApplyMoneyStatus(id)
  {
  	if (confirm("确定完成操作吗？"))
  	{
  		$("#applyId").val(id);  		
  		$("#updateApplyMoneyStatus").submit();
  	}
  }
  
  function checkApplyTransfer(id)
  {
	var flag=true;
	  $.ajax({
	  type: "POST",
	  dataType: "json",
	  async:false,
	  url: "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/GetApplyTransferPrecessAction.action",
	  data:"applyId="+id,
	  success: function(msg){
		 if(msg.flag=="y")
		{
			flag= false;
		}
      }
	  
	});	
	return flag;
  }

  function delApplyMoney(id)
  {
     if (confirm("确定删除资金单:F"+id+"吗？"))
  	 {
  	 	if(checkApplyTransfer(id))
  	 	{
  	 		$("#aid").val(id);
  	 		$("#deleteApplyTransfer").submit();
  	 	}
  	 	else
  	 	{
  	 		alert("该资金申请下有转账申请，无法删除！");
  	 	}
  	 }
  }
  
  function changeColorBlack(){

	if($("#apply_id1").val()=="*申请单号、收款人、关联ID、付款信息、创建人")
	{
 	 	 
 	 	 $("#apply_id1").val("");
 	 	 $("#apply_id1").removeAttr("class");
 	}
 	
}

function changeColorGray()
{
	 
	 if($("#apply_id1").val().trim()=="")
	 {
		  $("#apply_id1").val("*申请单号、收款人、关联ID、付款信息、 创建人");
		  $("#apply_id1").attr("class","searchbarGray");
	 }
}
  
function init()
{
 	if($("#apply_id1").val()=="*申请单号、收款人、关联ID、付款信息、 创建人")
	{
	 	 $("#apply_id1").attr("class","searchbarGray");
	}
	else
	{
		 $("#apply_id1").removeAttr("class");
	}

 	var int_center_account_type_id = <%=center_account_type_id%>;
	var int_center_account_type1_id = <%=center_account_type1_id%>;
	var int_center_account_id = <%=center_account_id%>;
	var classId = int_center_account_type_id;

	getAccountSelect2('center_account_type_id','chzn-select1',int_center_account_type_id)	
	
	if(classId == 0) {
		getAccountSelect1('center_account_type1_id','chzn-select2','',classId,int_center_account_type1_id);
		getAccountSelect('center_account_id','chzn-select',accounts,-1,-1,int_center_account_id);
	}
	else if(classId == 1) {
		getAccountSelect1('center_account_type1_id','chzn-select2','',classId,int_center_account_type1_id);
		getAccountSelect('center_account_id','chzn-select',accounts,classId,int_center_account_type1_id,int_center_account_id);
	}
	else if(classId == 2) {
		getAccountSelect1('center_account_type1_id','chzn-select2',adminGroups,classId,int_center_account_type1_id);
		getAccountSelect('center_account_id','chzn-select',accounts,classId,int_center_account_type1_id,int_center_account_id);
	}
	else if(classId == 3) {
		getAccountSelect1('center_account_type1_id','chzn-select2',productLineDefines,classId,int_center_account_type1_id);
		getAccountSelect('center_account_id','chzn-select',accounts,classId,int_center_account_type1_id,int_center_account_id);
	}
}
function promptCheck(v,m,f)
{
	if (v=="y")
	{
		 if(f.content == "")
		 {
				alert("请填写适当备注");
				return false;
		 }
		 else
		 {
		 	if(f.content.length>300)
		 	{
		 		alert("内容太多，请简略些");
				return false;
		 	}
		 }
		 return true;
	}
}
function remark(apply_id)
{
	$.prompt(
	"<div id='title'>备注</div><br />备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
	{
		  submit:promptCheck,
   		  loaded:
				function ()
				{
				}
		  ,
		  callback: 
				function (v,m,f)
				{
					if (v=="y")
					{
							document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/remarkAction.action"
							document.followup_form.apply_id.value = apply_id;
							document.followup_form.followup_content.value = f.content;		
							document.followup_form.submit();	
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}
function goFundsTransferListPage(id)
{
	window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
}
jQuery(function($){
	addAutoComplete($("#apply_id1"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/applyMoney/GetSearchApplyMoneyJSONAction.action",
			"merge_field","apply_id");
})
function refreshWindow(){
	window.location.reload();
}
//-->
</script>
</head>

<body onLoad="onLoadInitZebraTable()">
			
<div align="left"><br> 
</div><form id="deleteApplyTransfer" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/DeleteApplyMoneyPrecessAction.action">
<input type="hidden" name="applyId" id="aid"/>
</form>
<form id="updateApplyMoneyStatus" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/UpdateApplyMoneyStatusPrecessAction.action">
<input type="hidden" name="applyId" id="applyId"/>
</form>

<form id="funds_transfer" name="funds_transfer" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/action/administrator/financial_management/addApplyTransferAction.action">
<input type="hidden" id="payer" name="payer" />
<input type="hidden" id="payment_information" name="payment_information" />
<input type="hidden" id="amount" name="amount" />
<input type="hidden" id="apply_money_id" name="apply_money_id" />
<input type="hidden" id="remark" name="remark" />
</form>

<form id="apply_funds_form" name="apply_funds_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/AddApplyFundsPrecessAction.action?add=0">
<input type="hidden" id="last_time" name="last_time"/>
<input type="hidden" id="categoryId" name="categoryId"/>
<input type="hidden" id="associationId" name="associationId" />
<input type="hidden" id="amount" name="amount" />
<input type="hidden" id="payee" name="payee"/>
<input type="hidden" id="paymentInfo" name="paymentInfo" />
<input type="hidden" id="remark" name="remark"/>
<input type="hidden" id="center_account_id" name="center_account_id"/>
<input type="hidden" id="center_account_type_id" name="center_account_type_id"/>
<input type="hidden" id="center_account_type1_id" name="center_account_type1_id"/>
<input type="hidden" id="product_line_id" name="product_line_id"/>
<input type="hidden" id="payee_type_id" name="payee_type_id">
<input type="hidden" id="payee_type1_id" name="payee_type1_id"/>
<input type="hidden" id="payee_id" name="payee_id"/>
<input type="hidden" id="currency" name="currency"/>
<input type="hidden" id="adid" name="adid" value="<%=adminLoggerBean.getAdid()%>" />
<input type="hidden" id="add" name="add"/>
</form>

<form id="apply_funds_mod_form" name="apply_funds_mod_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/UpdateApplyFundsPrecessAction.action">
<input type="hidden" id="apply_id" name="apply_id"/>
<input type="hidden" id="categoryId" name="categoryId"/>
<input type="hidden" id="associationId" name="associationId" />
<input type="hidden" id="association_type_id" name="association_type_id" />
<input type="hidden" id="amount" name="amount" />
<input type="hidden" id="payee" name="payee"/>
<input type="hidden" id="paymentInfo" name="paymentInfo" />
<input type="hidden" id="remark" name="remark"/>
<input type="hidden" id="center_account_id" name="center_account_id"/>
<input type="hidden" id="center_account_type_id" name="center_account_type_id"/>
<input type="hidden" id="center_account_type1_id" name="center_account_type1_id"/>
<input type="hidden" id="product_line_id" name="product_line_id"/>
<input type="hidden" id="payee_type_id" name="payee_type_id">
<input type="hidden" id="payee_type1_id" name="payee_type1_id"/>
<input type="hidden" id="payee_id" name="payee_id"/>
<input type="hidden" id="currency" name="currency"/>
<input type="hidden" id="amount_transfer" name="amount_transfer"/>
<input type="hidden" id="adid" name="adid" value="<%=adminLoggerBean.getAdid()%>" />
<input type="hidden" id="adName" name="adName" value="<%=adminLoggerBean.getEmploye_name()%>" />
<input type="hidden" name="add" value="0"/>
<input type="hidden" id="states" name="states"/>
<input type="hidden" id="isNeedMail" name="isNeedMail"/>
<input type="hidden" id="isNeedMessage" name="isNeedMessage"/>
<input type="hidden" id="isNeedPage" name="isNeedPage"/>
<input type="hidden" id="adminUserIds" name="adminUserIds"/>
<input type="hidden" id="old_category_id" name="old_category_id"/>
</form>

<form id="messageForm" name="messageForm" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/SendMessageToFinanciPrecessAction.action">
<input type="hidden" id="applyMoneyId" name="applyId" />
<input type="hidden" name="message" id="message"/>
<input type="hidden" id="adid" name="adid" value="<%=adminLoggerBean.getAdid()%>"/>
</form>
<form action="" method="post" name="followup_form">
	<input type="hidden" name="apply_id"/>
	<input type="hidden" name="followup_content"/>
</form>
 
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	
	<div class="demo" >

	<div id="tabs">
<ul>
	<li><a href="#usual_tools">常用工具</a></li>
	<li><a href="#advan_search">高级搜索</a></li>		 
</ul>

		<div id="usual_tools">
		<form method="post" name="search" id="search" action="apply_funds.html" >
			<input type="hidden" id="search_mode" name="search_mode" />
			<input type="hidden" id="cmd" name="cmd" value="searchby_index"/>
          <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
            <tr>
	            <td width="80%" align="left" style="padding-top:3px;padding-left: 5px" nowrap="nowrap">
	            	 <div id="easy_search_father">
								<div id="easy_search"><a href="javascript:query()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
					  </div>
	            		<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
									 <input name="apply_id" id="apply_id1" value="<%=id%>" type="text" class="search_input" style="font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)query()"/>
									</div>
								</td>
								<td width="67">
								</td>
							</tr>
						</table>
						<script>eso_button_even();</script>
				</td>	           

                <td width="20%" style="padding-right:5px" align="right" valign="middle" nowrap="nowrap">
<%--                	<input type="button" class="long-long-button-add" value="申请固定资产资金"  onclick="$.artDialog.open('add_assets.html', {title: '申请固定资产资金',width:'700px',height:'300px', lock: true,opacity: 0.3})"/>--%>
<%--                	<input type="button" class="long-long-button-add" value="申请样品资金"  onclick="$.artDialog.open('add_sample.html', {title: '申请样品资金',width:'700px',height:'300px', lock: true,opacity: 0.3})"/>--%>
					<input type="button" class="long-long-button-add" value="申请资金" onClick="$.artDialog.open('apply_funds_detail.html', {title: '申请资金',width:'750px',height:'400px', lock: true,opacity: 0.3})" />
			  </td>
            </tr>
          </table>
		  </form> 
		</div>

		
	  <%
TDate tDate = new TDate();
tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

String input_st_date,input_en_date;
if ( st.equals("") )
{	
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}
else
{
	input_st_date = st;
}

if ( en.equals("") )
{	
	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
}else
{
	input_en_date = en;
}

%>
 
 <div id="advan_search">

	<form name="filterForm" action="apply_funds.html" method="post">
	<input type="hidden" id="cmd" name="cmd" value="filter"/>
	   <table width="100%" height="30" border="0" cellpadding="1" cellspacing="0">
		  
	<tr height="29">
	<td width="120px;">
		开始
		<input name="st" type="text" id="st" size="9" value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
	</td>
	<td align="left" width="420">
	<div>
	<ul id="categorymenu" name="categorymenu" class="mcdropdown_menu">
	  <li rel="0">所有费用</li>
	  <%
	  DBRow c1[] = applyFundsCategoryMgrZZZ.getApplyFundsCategoryChildren(0l);
	  for (int i=0; i<c1.length; i++)
	  {
			out.println("<li rel='"+c1[i].get("category_id",0l)+"'> "+c1[i].getString("category_name"));

			  DBRow c2[] = applyFundsCategoryMgrZZZ.getApplyFundsCategoryChildren(c1[i].get("category_id",1l));
			  if (c2.length>0)
			  {
			  		out.println("<ul>");
			  }
			  for (int ii=0; ii<c2.length; ii++)
			  {
					out.println("<li rel='"+c2[ii].get("category_id",0l)+"'> "+c2[ii].getString("category_name"));					
					out.println("</li>");				
			  }
			  if (c2.length>0)
			  {
			  		out.println("</ul>");
			  }
			  
			out.println("</li>");
	  }
	  %>
</ul>
	  <input type="text" name="category" id="category" value="" />
	  <input type="hidden" name="filter_acid1" id="filter_acid1" value="0"  />
	  </div>
	 </td>
	 
	 <td>
			<div id='center_account_type_id_Div' name='center_account_type_id_Div' style="float:left;">
		      	<select name="center_account_type_id" id="center_account_type_id" data-placeholder="请选择分类" class="chzn-select1" style="width:100px;" onchange="selectClass(this)">
					<option value="0">任意</option>
					<option value="1">公司账户</option>
					<option value="2">职员账户</option>
					<option value="3">供应商</option>
				</select>
				</div>
				<div id='center_account_type1_id_Div' name='center_account_type1_id_Div' style="float:left;">
					<select name="center_account_type1_id" id="center_account_type1_id" data-placeholder="请选择分类" class="chzn-select2" style="width:120px;" onchange="selectClass(this)">
						<option value="0">任意</option>
					</select>
				</div>
				<div id='center_account_id_Div' name='center_account_id_Div' style="float:left;">
		      	<select name="center_account_id" id="center_account_id" data-placeholder="请选择账户"  style="width:150px;" onchange="">
		          <option value='0' rel='0'>选择成员</option>
		        </select>
		        </div>
		        </div>
		        		       	
		        
	 </td>
	</tr>
	<tr>
		<td width="120px;">
		 		结束
		        <input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
		        
		</td>
		<td align="left" width="420">
			    <ul id="productLinemenu" class="mcdropdown_menu">
    			<li rel="0">所有产品线</li>
					  <%
		
					  	  DBRow ca1[] = assetsCategoryMgr.getAssetsCategoryChildren(0,"2");
						  for (int i=0; i<ca1.length; i++)
						  {
								//out.println("<li onmousemove='catagory("+ca1[i].get("id",0l)+")' value='"+ca1[i].get("id",0l)+"'><a href='"+ca1[i].get("id",0l)+"'> "+ca1[i].getString("chName")+" </a>");
								  DBRow[] ca2 = null;
								  	ca2 = assetsCategoryMgr.getAssetsCategoryChildren(ca1[i].get("id",0l),"3");
								 
								  for (int ii=0; ii<ca2.length; ii++)
								  {
										out.println("<li rel='"+ca2[ii].get("id",0l)+"'> "+ca2[ii].getString("chName"));		
										
										out.println("</li>");				
								  }
							
					  }
					  %>
				</ul>
					  <input type="text" name="productLine" id="productLine" value="" />
					  <input type="hidden" name="product_line_id" id="product_line_id" value="0" />

       <script>
  			$("#productLine").mcDropdown("#productLinemenu",{
						allowParentSelect:true,
						  select: 
						  
								function (id,name)
								{
									$("#product_line_id").val(id);
									//alert($("#product_line_id").val());
								}
				
				});
				$("#productLine").mcDropdown("#productLinemenu").setValue(0);				
				<%
				if (productLineId!=0)
				{
				%>
				$("#productLine").mcDropdown("#productLinemenu").setValue(<%=productLineId%>);
				<%
				}
				else
				{
				%>
				$("#productLine").mcDropdown("#productLinemenu").setValue(0);
				<%
				}
				%>
  		</script>	
		</td>
		
		<td align="left" valign="center">
		      <select name="status" id="status"  style="margin-left:0px;">
		        <option value="-1">任意付款类型</option>
		        <option value="<%=FundStatusKey.NO_APPLY_TRANSFER %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.NO_APPLY_TRANSFER+"")%></option>
		        <option value="<%=FundStatusKey.HAD_APPLY_TRANSFER %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.HAD_APPLY_TRANSFER+"")%></option>
		        <option value="<%=FundStatusKey.PART_PAYMENT %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.PART_PAYMENT+"")%></option>
		        <option value="<%=FundStatusKey.FINISH_PAYMENT %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.FINISH_PAYMENT+"")%></option>
		        <option value="<%=FundStatusKey.CANCEL %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANCEL+"")%></option>
		      </select>
		 
		     	<select name="association_type_id" id="association_type_id" style="margin-left:-2px;">
		     		<option value="-1">任意关联单据</option>
		      		<%
		      			for(int i=0;i<associationType.length;i++) {
		      				String atid  = associationType[i].getValue("id").toString();
		      				String name  = associationType[i].getValue("name").toString();
		      				out.println("<option value='"+atid+"'>"+name+"</option>");
		      			}
		      		%>
		      	</select>
		      	
		      	<select name="selectSortTime"  style="margin-left:-2px;">	   
						     <option value="create_time">创建时间排序排序</option>
					<option value="end_time" <%if(selectSortTime.equals("end_time")){%> selected="selected" <%} %>  >完成时间排序</option>
				    <option value="last_time" <%if(selectSortTime.equals("last_time")){%> selected="selected" <%} %> >最迟转款时间排序</option>
				</select>
		      	<input type="submit" id="seachByCondition" name="seachByCondition"  class="short-button" value="过    滤" />	
		</td>
	</tr>	      
		</table>
	</form>
 </div>

	</div>	
	</div>	
	</td>
  </tr>
</table>
<br/>
<form id="timeForm">
    <input type="hidden" value="" id="sortCmd" name="cmd" />
</form>
<script>
	$("#st").date_input();
    $("#en").date_input();
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>

<div id="applyMoneyList">
  <!-- 修改样式 -->
     <table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"   class="zebraTable" style="margin-left:3px;margin-top:5px;" isNeed="true" isBottom="true" >
     <tr>
				       <th width="20%" nowrap="nowrap" class="right-title"  style="text-align: center;">申请单号【类型/关联ID】</th>			
				       <th width="22%" nowrap="nowrap" class="right-title"  style="text-align: center;">申请金额</th>				   
				       <th width="25%" nowrap="nowrap" class="right-title"  style="text-align: center;">收款信息</th>
				       <th width="20%" nowrap="nowrap" class="right-title"  style="text-align: center;">跟进</th>
				       <th width="15%" class="right-title"  style="text-align: center;">凭证</th>				

     </tr>
		      <tbody>
     <%
     if(applyMoney!=null){
	     for(int i = 0;i<applyMoney.length;i++)
	     {
      %>
			 	<tr>
			 		<td>
			 		     <fieldset class="set" style="padding-bottom:4px;border:2px solid #993300; width:97%">
								<legend>
								<span class="title">
								  <span style="font-weight:bold;">
								    <a href="javascript:void(0)" onClick="goFundsTransferListPage('F<%=applyMoney[i].get("apply_id",0l) %>')">F<%=applyMoney[i].get("apply_id",0l) %></a>
								  </span>
								  <!-- 在状态等于0（未申请转账），如果未上传凭证，为不可申请转账 -->
								 <%
								 	List imageList = applyMoneyMgrLL.getImageList(applyMoney[i].getString("apply_id"),"1");
								 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoney[i].getString("status")) && imageList.size() < 1)
								 	{
						 		 %>
								 	<%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"")%>	
								 <%	 		
								 	}else{
						 		 %>
								 	<%=fundStatusKey.getFundStatusKeyNameById(applyMoney[i].getString("status"))%>	
								 <%				
								 	}
								 %>
								</span>
								</legend>
								<p style="padding-left:20%;">
								    <span class="alert-text stateName">
									申请人：
									</span>
									<%
										DBRow creater = applyMoneyMgrLL.getApplyMoneyCreate(applyMoney[i].getString("creater_id"));
										if(creater!=null)
										{
											out.print(creater.getString("employe_name"));
										}
									%>								
								</p>
								<p style="padding-left:15%;">
									<span class="alert-text stateName">
									创建时间：
									</span>
                                     <%=applyMoney[i].getString("create_time").equals("")?"":DateUtil.FormatDatetime("MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(applyMoney[i].getString("create_time")))%>        	
								</p>
								<p style="padding-left:5%;">
									<span class="alert-text stateName">
									最迟转款时间：
									</span>								
									<%=applyMoney[i].getString("last_time").equals("")?"":DateUtil.FormatDatetime("MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(applyMoney[i].getString("last_time")))%> 
								</p>
								<p style="padding-left:20%;">
								    <span class="alert-text stateName">
									完成人：
									</span>
									<%=applyMoney[i].getString("operating_personnel")%>							
								</p>
								<p style="padding-left:15%;">
									<span class="alert-text stateName">
									完成时间：
									</span>								
									<%=applyMoney[i].getString("end_time").equals("")?"":DateUtil.FormatDatetime("MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(applyMoney[i].getString("end_time")))%> 
								</p>
								<p style="padding-left:26%;">
									<span class="alert-text stateName">项目：</span>
									<%=applyMoney[i].getString("categoryName")%>
								</p>							
								<p style="padding-left:26%;">
      		<%
      			for(int ii=0;ii<associationType.length;ii++) {
      				String atid  = associationType[ii].getValue("id").toString();
      				String name  = associationType[ii].getValue("name").toString();
      				if(atid.equals(applyMoney[i].getString("association_type_id")))
      					out.println("");
      			}
      		%>

           <%
						          // if(1==1) {
										long association_id = applyMoney[i].get("association_id",0l);
										String associationTypeId = applyMoney[i].getString("association_type_id");
										String associateTypeName = "<font color='blue'>"+financeApplyTypeKey.getFinanceApplyTypeKeyName(associationTypeId)+"</font>";
										if(applyMoney[i].get("association_type_id",0l)==FinanceApplyTypeKey.FIXED_ASSETS)//固定资产
										{
							           		out.println("<span class='alert-text stateName'>关联：</span>"+associateTypeName+association_id);
										}
							           	else if(applyMoney[i].get("association_type_id",0l)==FinanceApplyTypeKey.SAMPLE)//样品
							           	{
							        		out.println("<span class='alert-text stateName'>关联：</span>"+associateTypeName+association_id);
							           	}
							           	else if(applyMoney[i].get("association_type_id",0l)==FinanceApplyTypeKey.PURCHASE_ORDER)//采购单
							           	{
							        		out.println("<span class='alert-text stateName'>关联：</span>"+associateTypeName+"<a target='_blank' href='"+ConfigBean.getStringValue("systenFolder")+"administrator/purchase/purchase_detail.html?purchase_id="+association_id+"'>P"+association_id+"</a>");
							           	}
							           	else if(applyMoney[i].get("association_type_id",0l)==FinanceApplyTypeKey.DELIVERY_ORDER) {//交货单
							           		//DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrder(association_id);
							           	//delivery_order.getString("delivery_order_number")+
							           		out.println("<span class='alert-text stateName'>关联：</span>"+associateTypeName+"<a target='_blank' href='"+ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_order_detail.html?transport_id="+association_id+"'>T"+association_id+"</a>");
	           	}
							           	else if(applyMoney[i].get("association_type_id",0l)==FinanceApplyTypeKey.TRANSPORT_ORDER)//转运单
							           	{
							        		out.println("<span class='alert-text stateName'>关联：</span>"+associateTypeName+"<a target='_blank' href='"+ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_order_detail.html?transport_id="+association_id+"'>T"+association_id+"</a>");
							           	}
							        	else if(applyMoney[i].get("association_type_id",0l)==FinanceApplyTypeKey.REPAIR_ORDER)//转运单
							           	{
							        		out.println("<span class='alert-text stateName'>关联：</span>"+associateTypeName+"<a target='_blank' href='"+ConfigBean.getStringValue("systenFolder")+"administrator/repair/repair_order_detail.html?transport_id="+association_id+"'>R"+association_id+"</a>");
							           	}
						        //   }
           %>
								</p>
								<p style="padding-left:20.5%;">
								    <span class="alert-text stateName">
									    产品线：
									</span>
									&nbsp;<%=applyMoney[i].getString("productLineName") %>
								</p>
								<p style="padding-left:15.5%;">
								    <span class="alert-text stateName">
									    成本中心：
									</span>
									&nbsp;<%=getCenterAccountName(accounts,applyMoney[i].get("center_account_type_id",0),applyMoney[i].get("center_account_id",0)) %>
								</p>
							</fieldset>
        </td>
        <td align="left" valign="middle">
			 		     <fieldset class="set" style="padding-bottom:4px;border:2px solid #00F; width:97%">
								<legend>
								<span class="title">
								<span style="font-weight:bold;">
								  <%if(applyMoney[i].getString("currency").equals("USD")){ %>
								       <%=applyMoney[i].get("amount",0d)%><%=applyMoney[i].getString("currency")%>/<%=applyMoney[i].get("standard_money",0d)%>RMB     
							      <%}else{%>    
								                  申请:<%=applyMoney[i].get("amount",0d) %> <%=applyMoney[i].getString("currency") %>
								  <%} %>
								</span>
								</span>
								</legend>
								<p style="padding-left:5%;">
        	<%
        			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoney[i].getString("apply_id"));
		  			for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
		  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
		  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
		  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
								  				String standard_money = applyTransferRows[ii].getString("standard_money")==null?"":applyTransferRows[ii].getString("standard_money");
		  				int moneyStatus = applyTransferRows[ii].get("status",0);
							  					if(currency.equals("USD")){
							  						if(moneyStatus != 0 ){
									  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+"/"+standard_money+"RMB<br/>");
								  					}else{
									  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+"/"+standard_money+"RMB<br/>");
								  					}
							  					}else{
							  						if(moneyStatus != 0 ){
		  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+"<br/>");
								  					}else{
		  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+"<br/>");
		  			}
							  					}
							  				}
		  	%>
           
								</p>	
						</fieldset>
						<br>
						<fieldset class="set" style="padding-bottom:4px;border:2px solid #00F; width:97%">
								<legend>
								<span class="title">
								<span style="font-weight:bold;">
								        备注
								</span>
								</span>
								</legend>
								<p style="padding-left:5%;">
								   <%=StringUtil.ascii2Html(applyMoney[i].getString("remark")) %>
								</p>	
						</fieldset>
        
			           
        </td>
        <td align="left" valign="middle" >       
           	收款人：<%=applyMoney[i].getString("payee")%><br>
           	<!-- 查询account表中,如果有了数据那么就是要显示Account表中的数据 -->	
	  	<!-- 如果没有那么就是要显示字段里面的东西 -->
	  	<!-- 兼容以前 -->
	  	<%
	  		DBRow[] accountInfos = accountMgrZr.getAccountByAccountIdAndAccountWithType(applyMoney[i].get("apply_id",0l),AccountKey.APPLI_MONEY);
	  		if(accountInfos != null && accountInfos.length > 0){
	  			for(DBRow tempAccount :  accountInfos){
	  	%>
	  					 
	  					<p>收款人户名:<%=tempAccount.getString("account_name") %></p>
	  					<p>收款人银行:<%=tempAccount.getString("account_blank") %></p>
	  					<p>收款人账号:<%=tempAccount.getString("account_number") %></p>
	  					<%if(tempAccount.getString("account_phone").length() > 0 ){ %>
	  						<p>收款人电话:<%=tempAccount.getString("account_phone") %></p>	
	  					<%} %>
	  					 <%if(tempAccount.getString("account_swift").length() > 0 ){ %>
	  						<p>SWIFT Code:<%=tempAccount.getString("account_swift") %></p>	
	  					<%} %> 
	  					<%if(tempAccount.getString("account_address").length() > 0 ){ %>
	  						<p>收款人地址:<%=tempAccount.getString("account_address") %></p>	
	  					<%} %>
	  					<%if(tempAccount.getString("account_blank_address").length() > 0 ){ %>
	  						<p>开户行地址:<%=tempAccount.getString("account_blank_address") %></p>	
	  					<%} %>
	  					<br />
	  					
	  				<%
	  			}
	  		}else{
	  			String paymentInfo = applyMoney[i].getString("payment_information");
	  			paymentInfo = paymentInfo.replaceAll("\n","<br>");
           		out.print(paymentInfo);
	  		}
           %>
           
        </td>
			 		<td align="left" valign="middle">		                             
			             <% 
			                long applyId = applyMoney[i].get("apply_id",0l);
			 		        DBRow ro[] = null;
			                ro = applyMoneyLogsMgrZZZ.getApplyMoneyLogsByApplyId(applyId,null); 
			                if(ro!=null)
			    	    	{    
			                	for(int j = 0;j<ro.length;j++)
			    	    		{
			             %>
					             <div style="border-bottom:#CCC 1px dashed;">
					                <span style="font-weight:bold;" ><%=ro[j].getString("userName") %></span>
					                <span style="color:#F60;" ><%=ro[j].getString("context") %></span><br>
					                <span style="color:#999;" >
					                	<%= tDate.getFormateTime(ro[j].getString("createDate")) %>
					                </span><br>
					             </div>
			             <%  
			                        if(j>2){
			             %>     			                       		             
			                          <a href="javascript:openDialog('apply_money_logs.html?applyId=<%=applyMoney[i].get("apply_id",0l) %>','全部','700px','300px')" >更多.......</a>
			             <%           break;
			                        }
			    	    		}
			    	    	}
			             %>			                            
        </td>
			 		<td align="center" valign="middle" nowrap="nowrap">
        	<%
        	for(int ii=0; ii<imageList.size(); ii++) {
				HashMap imageMap = (HashMap)imageList.get(ii);
			%>
							
							 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
		 			 	 	<%if(StringUtil.isPictureFile(imageMap.get("path").toString())){ %>
				 			 	 <p>
				 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=imageMap.get("association_id") %>','<%=imageMap.get("association_type") %>','<%=imageMap.get("path").toString() %>');">凭证<%=ii+1 %></a>
				 			 	 </p><br/>
			 			 	 <%} else{ %>
 		 			 	 	  		<p><a href='<%= downLoadFileAction%>?file_name=<%=imageMap.get("path").toString() %>&folder=<%= systemConfig.getStringConfigValue("file_path_financial")%>'>凭证<%=ii+1 %></a></p> <br/>
 		 			 	 	  <%} %>
							
			<%
			}
        	%>
        </td>
			 	</tr>
			 	<tr class="split" >
				    <td colspan="5" align="right" height="30px;">
        <%
        	if(applyMoney[i].getString("status").trim().equals("2"))
        	{
        		
        	%>
				        	<input type="button" class="short-short-button-mod" onclick='followup(<%=applyMoney[i].get("apply_id",0L) %>,<%=applyMoney[i].get("status",0) %>,<%=associationTypeId %>,<%=association_id %>,<%=applyMoney[i].getString("categoryId")%>)' value="跟进"/>
				        	<!--  
				        	<input type="button" class="short-short-button-mod" onclick="remark(<%=applyMoney[i].getString("apply_id")%>)" value="备注"/>
				        	<input type="button" class="short-short-button-merge" onClick="goFundsTransferListPage('<%=applyMoney[i].get("apply_id",0l) %>')"  value="查看" />				        	
        	<input type="button" class="short-short-button-merge" onclick="tb_show('日志','apply_money_logs.html?applyId=<%=applyMoney[i].get("apply_id",0l) %>&TB_iframe=true&height=300&width=700',false);" value="日志" />
				        	-->
				     	    <input type="button" class="short-button" onClick="openDialog('<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds_update.html?aid=<%=applyMoney[i].get("apply_id",0l) %>','修改申请','750px','400px');" value="修改" />
				        		
        	<% 
        	
        	}
        	else if(applyMoney[i].getString("status").trim().equals("3"))
        	{
        	 %>
				     	   <input type="button" class="short-short-button-mod" onclick='followup(<%=applyMoney[i].get("apply_id",0L) %>,<%=applyMoney[i].get("status",0) %>,<%=associationTypeId %>,<%=association_id %>,<%=applyMoney[i].getString("categoryId")%>)' value="跟进"/>
				     	   <!-- 
				     	   <input type="button" class="short-short-button-mod" onclick="remark(<%=applyMoney[i].getString("apply_id")%>)" value="备注"/>		     	    
     	   <input type="button" class="short-short-button-merge" onclick="tb_show('日志','apply_money_logs.html?applyId=<%=applyMoney[i].get("apply_id",0l) %>&TB_iframe=true&height=300&width=700',false);" value="日志" />
				     	   -->
				     	   <%=fundStatusKey.getFundStatusKeyNameById(applyMoney[i].getString("status").trim()) %>
				     	   <input type="button" class="short-button" onClick="openDialog('<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds_update.html?aid=<%=applyMoney[i].get("apply_id",0l) %>','修改申请','750px','400px');" value="修改" />
				     	   
        <%
        	}
        	else
        	{
         %>
				     	   <input type="button" class="short-short-button-mod" onclick='followup(<%=applyMoney[i].get("apply_id",0L) %>,<%=applyMoney[i].get("status",0) %>,<%=associationTypeId %>,<%=association_id %>,<%=applyMoney[i].getString("categoryId")%>)' value="跟进"/>
				     	   <!-- 
				     	   <input type="button" class="short-short-button-mod" onclick="remark(<%=applyMoney[i].getString("apply_id")%>)" value="备注"/>			     	   
				     	   <input type="button" class="short-short-button-merge" onclick="tb_show('日志','apply_money_logs.html?applyId=<%=applyMoney[i].get("apply_id",0l) %>&TB_iframe=true&height=300&width=700',false);" value="日志" />
				     	   -->
				     	   <input type="button" class="short-short-button-mod" onclick="openDialog('<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds_message.html?applyId=<%=applyMoney[i].get("apply_id",0l) %>','通知','370px','190px');" value="通知" />				     	   
     	   				   <%
     	   						DBRow[] applyTransfers = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoney[i].getString("apply_id"));
     	   				   		if(0 == applyTransfers.length)
     	   				   		{
     	   				   %>
	     	   				   <tst:authentication bindAction="com.com.cwc.app.api.zzz.ApplyMoneyMgrZZZ.deleteApplyMoneyByApplyId">
	     	   				   		<input type="button" class="short-short-button-cancel" value="删除" onClick="delApplyMoney('<%=applyMoney[i].get("apply_id",0l) %>')" />
	     	   				   </tst:authentication>
     	   				   <%			
     	   				   		}
     	   				   %>
     	   				   <tst:authentication bindAction="com.com.cwc.app.api.zzz.ApplyMoneyMgrZZZ.updateApplyMoneyStatusByApplyId">
     	   				   		<input type="button" class="short-short-button-mod" onclick="updateApplyMoneyStatus('<%=applyMoney[i].get("apply_id",0l) %>')" value="完成"/>
				     	   </tst:authentication>
				     	   <input type="button" class="short-button" onClick="openDialog('<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds_update.html?aid=<%=applyMoney[i].get("apply_id",0l) %>','修改申请','750px','400px');" value="修改" />
				     	   
     	 					 <%
				     	 		if(imageList.size() > 0) 
				     	 		{
     	  					 %>
     	  					<tst:authentication bindAction="com.cwc.app.api.zzz.ApplyTransferMgrZZZ.addApplyTransfer">
				     	  		<input type="button" class="short-button" onClick="openDialog('<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer.html?aid=<%=applyMoney[i].get("apply_id",0l) %>&association_type_id=<%=applyMoney[i].get("association_type_id",0l) %>&types=<%=applyMoney[i].get("types",0l) %>','申请转账','660px','500px');" value="申请转账" />
					     	 </tst:authentication>
					     	  <%
					     	 	}
					     	  %>
									     	  
					        <%
					        	}
					         %>
				    </td>
     </tr>
     <%
    	 }
     }
     %>
		      </tbody>
  </table>
<!-- 样式结束 -->
  <br>
  <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
    <input type="hidden" name="p">
    <input type="hidden" id="cmd" name="cmd" value="<%=StringUtil.getString(request,"cmd") %>"/>
    <input type="hidden" id="apply_id" name="apply_id" value="<%=StringUtil.getString(request,"apply_id") %>" />
    <input type="hidden" id="center_account_id" name="center_account_id" value="<%=StringUtil.getLong(request,"center_account_id") %>" />
    <input type="hidden" id="productLine" name="productLine" value="<%=StringUtil.getLong(request,"productLine") %>" />
    <input type="hidden" id="association_type_id" name="association_type_id" value="<%=StringUtil.getLong(request,"association_type_id") %>" />
    <input name="st" type="hidden" id="st" size="9" value="<%=StringUtil.getString(request,"st")%>"/>
    <input name="en" type="hidden" id="en" size="9" value="<%=StringUtil.getString(request,"en")%>"/>
    <input type="hidden" id="category" name="category" value="<%=StringUtil.getLong(request,"category")%>"/>
    <input type="hidden" id="status" name="status" value="<%=StringUtil.getInt(request,"status",0) %>"/>
    <input type="hidden" id="selectSortTime" name="selectSortTime" value="<%= StringUtil.getString(request,"selectSortTime") %>"/>
  </form>
  <tr>
    <td height="28" align="right" valign="middle">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
</div>

<script type="text/javascript">
	function followup(applyMoneyId, applyMoneyStatus,associationTypeId,association_id,categoryId)
	{  
	 	var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_apply_log_add.html?apply_money_id="+applyMoneyId+"&apply_money_status="+applyMoneyStatus+"&association_type_id="+associationTypeId+"&association_id="+association_id+"&categoryId="+categoryId;
		$.artDialog.open(uri , {title: "日志跟进["+applyMoneyId+"]",width:'450px',height:'320px', lock: true,opacity: 0.3,fixed: true});	  
	}
	function openDialog(uri,title,width,height){
	     $.artDialog.open(uri , {title: title,width:width,height:height, lock: true,opacity: 0.3,fixed: true});	  
		 return ;
	  }
<%
	if(add == 1) {
%>
		tb_show('申请固定资产资金','add_assets_and_sample.html?TB_iframe=true&height=300&width=700',false);
<%
	}else if(add == 2) {
%>
		tb_show('申请样品资金','add_assets_and_sample1.html?TB_iframe=true&height=300&width=700',false);
<%
	}
%>
</script>

</body>
</html>