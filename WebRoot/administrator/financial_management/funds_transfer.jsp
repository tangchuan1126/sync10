<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.iface.zr.AccountMgrIfaceZr"%>
<%@page import="com.cwc.app.key.AccountKey"%>
<%@ include file="../../include.jsp"%> 
<%
long id = StringUtil.getLong(request,"aid",0l);
//判断是采购单定金
int association_type_id=StringUtil.getInt(request,"association_type_id");
int types = StringUtil.getInt(request, "types");
DBRow row[] = null;
//if(id!=0l)
//{
   row = applyMoneyMgrZZZ.getApplyMoneyById(String.valueOf(id));
   double standard_money		= row[0].get("standard_money",0.00);
   double total_apply_transfer	= applyFundsMgrZyj.getTotalApplyTransferByApplyId(id).get("totalApplyTransfer",0.00);
   double maxMoney				= MoneyUtil.round((standard_money - total_apply_transfer),2) ;
   maxMoney						= maxMoney<0?0.00:maxMoney;
   double currencyEx			= Double.parseDouble(systemConfig.getStringConfigValue("USD")) ;
   double canApplyMaxRMBMoney	= MoneyUtil.round(maxMoney + maxMoney * 0.01 * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation")),2);
   double canApplyMaxUSDMoney	= MoneyUtil.round((maxMoney + maxMoney * 0.01 * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))) /currencyEx ,2);
//}else
//{
//  out.print("<script>alert('无法获取数据,请刷新后重试！');</script>");
//  out.print("<script>window.close();</script>");
//}
 //账号管理
	DBRow[] accountsRow = accountMgrZr.getAccountByAccountIdAndAccountWithType(id,AccountKey.APPLI_MONEY);
	String deleteAccountAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/account/DeleteAccountAction.action";
	String updateAccountUrl =  ConfigBean.getStringValue("systenFolder") + "administrator/admin/ebay_account_update.html";
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>创建转账申请</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 时间控件 -->
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
<!--
$(document).ready(function(){
	var currency = $("select[name='currency']").val();
	var showValue = "" ;
    if(currency === "USD"){
		showValue = parseFloat('<%= maxMoney/currencyEx%>');
	}else{
	    showValue = parseFloat('<%= maxMoney%>');
	}
	$("#amount").val(decimal(showValue,2));
});
function handleAmountByCurrency(){
	var amount = $("#amount").val();
	var currency = $("#currency").val();
	if($.trim(amount).length > 0){
	 	if(currency === "USD"){
	 	  var tempValue = parseFloat(amount) / parseFloat('<%= currencyEx%>');
	 	  $("#amount").val(decimal(tempValue,2));
	 	}else{
	 	   var tempValue = parseFloat(amount) * parseFloat('<%= currencyEx%>');
	  	  $("#amount").val(decimal(tempValue,2));
	 	}
	}
}
function decimal(num,v){
	var vv = Math.pow(10,v);
	return Math.round(num*vv)/vv;
} 
function getUsdPayMentInfo(){
	var str  = "";
	var usdInfo = $(".usd");
	if(usdInfo.length > 0 ){
		for(var index = 0 , count = usdInfo.length ; index < count ; index++ ){
		//	r_name,r_account,swift,r_address,r_bank_name,r_bank_address
		//	收款人户名,收款人账号,SWIFT,收款人地址,收款行名称,收款行地址
			var _table = $(usdInfo.get(index));
			var r_name =  $("input.account_name",_table).val();
			var r_account = $("input.account_number",_table).val();
			var swift = $("input.account_swift",_table).val();
			var r_address = $("input.account_address",_table).val();
			var r_bank_name = $("input.account_blank",_table).val();
			var r_bank_address = $("input.account_blank_address",_table).val();
			str +="\n";
			if($.trim(r_name).length > 0 ){
				str += "收款人户名:"+r_name+"\n";
			}else{
				alert("输入收款人户名!");
				return;
			}
			if($.trim(r_account).length > 0 ){
				str += "收款人账号:"+r_account+"\n";
			}else{
				alert("输入收款人账号!");
				return;
			}
			if($.trim(swift).length > 0 ){
				str += "SWIFT:"+swift+"\n";
			}
			if($.trim(r_address).length > 0 ){
				str += "收款人地址:"+r_address+"\n";
			}
			if($.trim(r_bank_name).length > 0 ){
				str += "收款行名称:"+r_bank_name+"\n";
			}
			if($.trim(r_bank_address).length > 0 ){
				str += "收款行地址:"+r_bank_address+"\n";
			}
		}
		return str;
	}
	return "" ;
}
function add()
{
   var ischeck=checkForm();
     var pageMessage=false;
	 var mail=false;
	 var shortMessage=false;
     //判断 页面  邮件 短信
	  if($("#isMailInvoice").attr("checked")){
	     mail=true;
	  }
	  if($("#isMessageInvoice").attr("checked")){
		 shortMessage=true;
	  }
	  if($("#isPageInvoice").attr("checked")){
		 pageMessage=true;
	  }     
	  var str  = ""
			var rmbInfo = $(".rmb");
			if(rmbInfo.length > 0 ){
				for(var index = 0 , count = rmbInfo.length ; index < count ; index++ ){
					var _table = $(rmbInfo.get(index));
					var r_name =  $("input.account_name",_table).val();
					var r_account = $("input.account_number",_table).val();
					var r_bank = $("input.account_blank",_table).val();
					var r_phone = $("input.account_phone",_table).val();
					str +="\n";
					if($.trim(r_name).length > 0 ){
						str += "收款人户名:"+r_name+"\n";
					}else{
						alert("输入收款人户名!");
						return;
					}
					if($.trim(r_account).length > 0 ){
						str += "收款人账号:"+r_account+"\n";
					}else{
						alert("输入收款人账号!");
						return;
					}
					if($.trim(r_bank).length > 0 ){
						str += "收款开户行:"+r_bank+"\n";
					}
					if($.trim(r_phone).length > 0 ){
						str += "联系电话:"+r_phone+"\n";
					}	
				}
			}
			str += getUsdPayMentInfo();
			 // 计算组织出所有收款信息
			 if(str.length <= 4){
					alert("请先输入收款人信息");
					return ;
			 }else{
				 fixAccountInfo();    
   if(ischeck)
   {  
					 
					   $.ajax({
							url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/addApplyTransferAction.action',
							data:$("#transfer_form").serialize() + "&mail=" + mail +"&shortMessage="+shortMessage +"&pageMessage="+pageMessage,
							dataType:'json',
							type:'post',
							beforeSend:function(request){
					     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
						   },
						   success:function(data){
								if(data && data.flag == "true"){
								    $.unblockUI();
		    	$.artDialog.opener.goFundsTransferListByFundId("F"+$("#apply_money_id").val());
		    }
						   },
							error:function(){alert("系统错误"); $.unblockUI();}
					   })
   }
}
}
function checkForm()
{
	 var amount = $("#amount").val();
	 var currency = $("#currency").val();
	// 判断金额是不是都正确
	if(currency === "USD" && parseFloat(amount) >  parseFloat ('<%=canApplyMaxUSDMoney%>')){
		 alert("申请金额不正确，最多只能申请"+parseFloat('<%=canApplyMaxUSDMoney%>')+" USD");
		 return false;
	}
	if(currency === "RMB" && parseFloat(amount) > parseFloat('<%=canApplyMaxRMBMoney%>')){
		 alert("申请金额不正确，最多只能申请"+parseFloat('<%=canApplyMaxRMBMoney%>')+" RMB");
		 return false;
	}
    if($("#payer").val().trim()==""||$("#payer").val()==null)
     {
         $("#payer").focus();
	     alert("付款人不能为空！");
	     return false;
     }
     if($.trim($("#lastTime").val()).length < 1){
		 alert("输入最迟转款时间");
			return ;
     }
     
	 if($("#amount").val().trim()==""||$("#amount").val()==null)
	 {
	     $("#amount").focus();
	     alert("金额不能为空！");
	     return false;
	 }
	 else
	 {
	    if(isNaN($("#amount").val().trim()))
	    {
      		$("#amount").focus();
	    	alert("金额必须为数字！");
	    	return false;
	    }
	    
	    if($("#amount").val().trim() <= 0)
	    {
	    	$("#amount").focus();
	    	alert("金额必须大于零！");
	    	return false;
	    }
	 }
	 
	 if($("#remark").val().trim() != "" && $("#remark").val() != null)
	 {
		 if($("#remark").val().trim().length>200)
		 {
		 	 $("#remark").focus();
		 	 alert("备注不能超过200字数！");
		 	 return false;
		 }
	 }
	 return true;
}

function adminUserInvoice(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#transferResponsiblePersonIds").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowPurchaser'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShow(ids,names,  methodName)
{
	eval(methodName)(ids,names);
}
function setParentUserShowPurchaser(user_ids , user_names){
	$("#transferResponsiblePersonNames").val(user_names);
	$("#transferResponsiblePersonIds").val(user_ids);
}
var va = ['一','二','三','四','五','六','七','八','九'];
function addUSD(){
    //r_name,r_account,swift,r_address,r_bank_name,r_bank_address
  	var len = $(".usd").length;	 
  	var table =  "<table class='receivers usd next'>";
  	table += "<tr><td rowspan='7' style='"+"width:150px;text-align:center;"+"'>外币收款信息"+va[len]+"</td>";
  	table += "<td class='left'>收款人户名:</td><td><input type='text' class='noborder account_name' name='account_name'/></td>";
  	table += "<td rowspan='7' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";
  	table += "<tr><td class='left'>收款人账号:</td><td><input type='text' class='noborder account_number' name='account_number'/></td></tr>";
  	table +="<tr><td class='left'>SWIFT:</td><td><input type='text' class='noborder account_swift' name='account_swift'/></td></tr>";
  	table +="<tr><td class='left'>收款人地址:</td><td><input type='text' class='noborder account_address' name='account_address' /></td></tr>";
  	table +="<tr><td class='left'>收款行名称:</td><td><input type='text' class='noborder account_blank' name='account_blank' /></td></tr>" ;
  	table +="<tr><td class='left'>收款行地址:</td><td><input type='text' class='noborder account_blank_address' name='account_blank_address' /><input type='hidden' name='account_currency' value='usd'/></td></tr>" ;
  	table +="<tr><td class='left'>收款人电话:</td><td><input type='text' class='noborder account_phone' name='account_phone'/></td></tr></table>" ;

  	//	
  	var addBar = $(".receivers").last();
  	addBar.after($(table));
  }
  function addRMB(){
      var len  = $(".rmb").length ;
  	var table =  "<table class='receivers rmb next'>";
  	table += "<tr><td rowspan='4' style='"+"width:150px;text-align:center;"+"'>人民币收款信息"+va[len]+"</td>";
  	table += "<td class='left'>收款人户名:</td>";
  	table += "<td><input type='text' name='account_name' class='noborder account_name'/></td>";
  	table += "<td rowspan='4' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";
  	table += "<tr><td class='left'>收款人账号:</td>";
  	table += "<td><input type='text' name='account_number' class='noborder account_number'/></td></tr>";
  	table +="<tr><td class='left'>收款开户行:</td><td><input type='text' name='account_blank' class='noborder account_blank'/></td></tr>";
  	table +="<tr><td class='left'>收款人电话:</td><td><input type='text' name='account_phone' class='noborder account_phone'/><input type='hidden' name='account_currency' value='rmb'/></td></tr></table>";
  	// 获取最后一个RMB的table 然后添加到他的后面。如果是没有的话,那么就选取H1标签然后添加到他的后面
  	var addBar = $(".receivers").last();
  	addBar.after($(table));
  }
function deletePaymentInfo(_this){
	var parentNode = $(_this).parent().parent().parent().parent();
	if($(".receivers").length == 1){
		alert("至少包含一个收款信息");
		return ;
	}
		 parentNode.remove();
}
//重新梳理账号信息
function fixAccountInfo(){
	$(".receivers").each(function(index){
		//将里面的name 都重新加上数字
		var fixIndex = index + 1 ;
		var node = $(this);
		var inputs = $("input",node);
	 	for(var inputIndex = 0 , count = inputs.length ; inputIndex < count ; inputIndex++ ){
			var nodeInput = $(inputs[inputIndex]);
			var name = nodeInput.attr("name");
			nodeInput.attr("name",name+"_"+fixIndex);
	    }
	})
}
//-->
</script>
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
td.left{width:75px;text-align:right;}
td.right{text-align:left ;font-weight:bold;color: #666666;width:250px;}
table.receivers td {border:1px solid silver;}
table.receivers td input.noborder{border-bottom:1px solid silver;width:200px;}
table.receivers td.left{width:120px;text-align:right;}
table.next{margin-top:5px;}
table.receivers{border-collapse:collapse ;}
span._hand{cursor:pointer;}
table.account_table{border:1px solid silver;border-collapse:collapse;}
table.account_table td{border:1px solid silver;}
-->
</style>
<style type="text/css">
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:9999999;display:none;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<form name="transfer_form" id="transfer_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/addApplyTransferAction.action">
<div class="cssDivschedule" style=""></div>
<input type="hidden" id="association_type_id" name="association_type_id" value="<%=association_type_id%>" />
<input type="hidden" id="types" name="types" value="<%=types%>" />
<!-- 
<input type="hidden" name="mail" id="mail"/>
<input type="hidden" name="shortMessage" id="shortMessage"/>
<input type="hidden" name="pageMessage" id="pageMessage"/> -->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" align="center" valign="top">
	
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:15px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">转账申请信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">		
	        <tr>
	          <td align="right" class="STYLE2">付款人:</td>
	          <td>&nbsp;</td>
	          <td>	
	          <input type="hidden" id="apply_money_id" name="apply_money_id" value="<%=id %>" />	          
	          <input id="payer" name="payer" value="" size="20"/>
	          </td>
	        </tr>		    		    
		     <tr> 			
		      <td align="right" class="STYLE2">最迟转款时间:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<input name="lastTime" type="text" id="lastTime"  value=""  style="border:1px #CCCCCC solid;width:70px;"/>
		      </td>
		    </tr>		    		    
		     <tr> 			
	          <td align="right" class="STYLE2">收款人:</td>
	          <td>&nbsp;</td>
	          <td>
	          <%=row[0].getString("payee")%>
	          <input type="hidden" id="receiver" name="receiver" value="<%=row[0].getString("payee") %>" />
	          </td>
	        </tr>
		     <tr> 			
		      <td align="right"><span class="STYLE2">收款信息:</span></td>
		      <td width="2%">&nbsp;</td>
		      <td>
		      		<h1 id="addBar"> 
		      			<input type="button" value="添加人民币收款" onclick="addRMB();"/>
		      			<input type="button" value="添加外币收款" onclick="addUSD();"/>
		      		</h1>
		      		<input type="hidden" name="account_with_type" value='<%=AccountKey.APPLY_TRANSFER %>'/>
		      	<%if(accountsRow != null && accountsRow.length > 0 ){
		      			for(DBRow tempAccount : accountsRow){
		      				int index = 0;
		      				char[] indexChar = new char[]{'一','二','三','四','五','六','七','八','九','十'};
		      				boolean isOutCurrency = !tempAccount.getString("account_currency").toUpperCase().equals("RMB");
		      				int colLength = isOutCurrency ?7:4 ;
		      				%>
		      					<table class="receivers <%=(isOutCurrency?"usd":"rmb") %> next">
					      			<tr>
					      				<td rowspan="<%=colLength %>" style='width:150px;text-align:center;'><%= isOutCurrency?"外币":"人民币" %>收款信息<%=indexChar[index] %></td>
					      				<td class="left">收款人户名:</td>
					      				<td><input type="text" name="account_name" class="noborder account_name" value='<%=tempAccount.getString("account_name") %>'/></td>
					      				<td rowspan="<%=colLength %>" style="width:80px;text-align:center;"><span onclick="deletePaymentInfo(this)" class="_hand">删除</span></td>
					      			</tr>
					      			<tr>
					      				<td class="left">收款人账号:</td>
					      				<td><input type="text" name="account_number" class="noborder account_number" value='<%=tempAccount.getString("account_number") %>'/></td>
					      			</tr>
					      			<%if(isOutCurrency){ %>
						      			<tr>
						      				<td  class="left">SWITH:</td>
						      				<td><input type="text" name="account_swift" class="noborder account_swift" value='<%=tempAccount.getString("account_swift") %>'></td>
						      			</tr>
						      			<tr>
						      				<td class="left">收款人地址</td>
						      				<td><input type="text" name="account_address" class="noborder account_address" value='<%=tempAccount.getString("account_address") %>' /></td>
						      			</tr>
						      			<tr>
						      				<td class="left">开户行地址</td>
						      				<td><input type="text" name="account_blank_address" class="noborder account_blank_address" value='<%=tempAccount.getString("account_blank_address") %>' /></td>
						      			</tr>
					      			<%
					      			} %>
					      			<tr>
					      				<td class="left">收款开户行:</td>
					      				<td><input type="text" name="account_blank" class="noborder account_blank" value='<%=tempAccount.getString("account_blank") %>'/></td>
					      			</tr>
					      			<tr>
					      				<td class="left">收款人电话:</td>
					      				<td><input type="text" name="account_phone" class="noborder account_phone" value='<%=tempAccount.getString("account_phone") %>'/><input type='hidden' name='account_currency' value='<%=tempAccount.getString("account_currency") %>'/></td>
					      			</tr>
					      		</table>
		      				<% 
		      				index++;
		      			}
		      		%>
		      		<%} else{%>
		      				<table  class="receivers rmb next">
					      			<tr>
					      				<td rowspan="4" style='width:150px;text-align:center;'>人民币收款信息 一</td>
					      				<td class="left">收款人户名:</td>
					      				<td><input type="text" name="account_name" class="noborder account_name"/></td>
					      				<td rowspan="4" style="width:80px;text-align:center;"><span onclick="deletePaymentInfo(this)" class="_hand">删除</span></td>
					      			</tr>
					      			<tr>
					      				<td class="left">收款人账号:</td>
					      				<td><input type="text" name="account_number" class="noborder account_number"/></td>
					      			</tr>
					      			<tr>
					      				<td class="left">收款开户行:</td>
					      				<td><input type="text" name="account_blank" class="noborder account_blank"/></td>
					      			</tr>
					      			<tr>
					      				<td class="left">收款人电话:</td>
					      				<td><input type="text" name="account_phone" class="noborder account_phone"/><input type='hidden' name='account_currency' value='rmb'/></td>
					      			</tr>
					       </table>
		       		<%} %>
			  </td>
		    </tr>
		    <tr>
			    <td valign="middle" class="STYLE2" align="right" nowrap="nowrap">货币种类:</td>
			    <td>&nbsp;</td>
			    <td align="left" valign="middle">
			    	<select id="currency" name="currency" onchange="handleAmountByCurrency();">
			    		<option value="RMB" <%=row[0].getString("currency").equals("RMB")?"selected":""%>>RMB</option>
			    		<option value="USD" <%=row[0].getString("currency").equals("USD")?"selected":""%>>USD</option>
			    	</select>
			    </td>
			</tr>
		  	<tr> 
		      <td align="right" class="STYLE2">金额:</td>
		      <td>&nbsp;</td>
		      <td><input name="amount" value="" id="amount" size="20"></td>
		    </tr>
				  		    
		    		    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea name="remark"  id="remark" rows="4" cols="40"></textarea>  </td>
		    </tr>
		    <tr>
		       <td align="right" class="STYLE2" width="85px;">转账负责人:</td>
		       <td>&nbsp;</td>
		       <td>
		           <input type="text" id="transferResponsiblePersonNames" name="transferResponsiblePersonNames" style="width:180px;" onclick="adminUserInvoice()"/>
		           <input type="hidden" id="transferResponsiblePersonIds" name="transferResponsiblePersonIds" value=""/>
		       </td>
		    </tr>
		    <tr>
		      <td align="right" class="STYLE2">通知:</td>
		      <td>&nbsp;</td>
		      <td>         
			   		<input type="checkbox"  name="isMailInvoice" id="isMailInvoice"/>邮件
			   		<input type="checkbox"  name="isMessageInvoice" id="isMessageInvoice" />短信
			   		<input type="checkbox"  name="isPageInvoice" id="isPageInvoice" />页面
			  </td>
		    </tr>
		</table>
		
	</fieldset>	
	</td>
  </tr>

</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onclick="add()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="$.artDialog && $.artDialog.close()" value="取消" >
    </td>
  </tr>
</table>
</form>
<script type="text/javascript">
	$("#lastTime").date_input();
</script>
</body>
</html>
