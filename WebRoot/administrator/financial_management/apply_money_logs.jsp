<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	
	long applyId = StringUtil.getLong(request,"applyId");
	DBRow row[] = null;
	
	row = applyMoneyLogsMgrZZZ.getApplyMoneyLogsByApplyId(applyId,null);
	TDate tDate = new TDate();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>资金申请日志列表</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll; 
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>



<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />



<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	
<link href="../comm.css" rel="stylesheet" type="text/css"/>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>


<script src="../js/zebra/zebra.js" type="text/javascript"></script>

<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
</style>

</head>

<body onLoad="onLoadInitZebraTable();">
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <tr>
  	<td valign="top" style="padding:15px;">
  		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center">
				<span style="font-family:'黑体'; font-size:20px;">资金申请单：<%=applyId %></span><br/>
            </td>
          </tr>
        </table>
	    
	  <br>
  	  <table width="98%"   border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable">
  	    <tr>
  	     <th class="right-title"  style="vertical-align: center;text-align: center;">
  	        操作人
		  </th>
		 <th class="right-title"  style="vertical-align: center;text-align: center;">
  	        事件记录
		  </th>
		 <th class="right-title"  style="vertical-align: center;text-align: center;">
  	   记录时间     
		  </th>
	    </tr>
	    <%
	    	if(row!=null)
	    	{
	    		for(int i = 0;i<row.length;i++)
	    		{
	    		%>
	    	<tr align="center" valign="middle" height="30px">
	    		<td align="center" valign="middle" >
	    			<%=row[i].getString("userName") %>
	    		</td>
	    		<td align="left" valign="middle" >
	    			<%=row[i].getString("context") %>
	    		</td>
	    		<td align="center" valign="middle" >
	    		    <%= tDate.getFormateTime(row[i].getString("createDate")) %>
	    		</td>
	 	   </tr>
	    		<%
	    		}
	    	}
	     %>
	    
      </table>
  </td></tr>
 <tr>
 	<td align="right" class="win-bottom-line">
     <input type="button" name="Submit2" value="关闭" class="normal-white" onClick="parent.closeWinNotRefresh()">	</td>
 </tr>
</table>
</body>
</html>

