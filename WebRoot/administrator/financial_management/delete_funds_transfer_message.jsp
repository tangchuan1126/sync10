<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
	long applyId=StringUtil.getLong(request,"applyMoneyId",0l);
	long transferId = StringUtil.getLong(request,"transferId",0l);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
 <!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td align="center" valign="middle">
		 <fieldset style="border:2px #cccccc solid;padding:4px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	<legend style="font-size:15px;font-weight:normal;color:#999999;">转账申请单删除原因</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
		    <tr> 
		      <td align="right" class="STYLE2">原因:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="remark" rows="4" cols="40"></textarea></td>
		    </tr>
		</table>
	</fieldset>	
	</td>
  </tr>

</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onclick="submitApply()" value="确定" >
      <input name="cancel" type="button" class="normal-white" onclick="$.artDialog && $.artDialog.close()" value="取消" >
    </td>
  </tr>
</table>
<script type="text/javascript">
<!--
	

	function submitApply()
	{
			 if($("#remark").val().trim()=="" || $("#remark").val()==null)
			 {
			 	 $("#remark").focus();
			     alert("请填写删除原因！");
			 }
			 else if($("#remark").val().trim().length>=200)
			 {
			 	 $("#remark").focus();
			     alert("字符长度不得超过200！");
			 }
			 else
			 {
			 	parent.document.deleteFundsTransferForm.applyId.value = "<%=applyId%>";
			 	parent.document.deleteFundsTransferForm.reason.value = $("#remark").val();
			 	parent.document.deleteFundsTransferForm.transferId.value="<%=transferId%>";
				parent.document.deleteFundsTransferForm.submit();
			 }	
	}
//-->
</script>
</body>
</html>
