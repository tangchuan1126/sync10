<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@page import="com.cwc.app.iface.zr.AccountMgrIfaceZr"%>
<%@page import="com.cwc.app.key.AccountKey"%>
<%@ include file="../../include.jsp"%> 
<%@page import="java.util.*"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	
	long aid=StringUtil.getLong(request,"aid",0l);
	
	DBRow row = preparePurchaseMgrZwb.getByApplyMoney(aid);
	
	DBRow[] associationType = applyMoneyMgrLL.getMoneyAssociationTypeAll();
	DBRow[] accounts = applyMoneyMgrLL.getAllByReadView("accountReadViewLL");
	DBRow[] accountCategorys = applyMoneyMgrLL.getAllByTable("accountCategoryBeanLL");
	DBRow[] adminGroups = applyMoneyMgrLL.getAllByTable("adminGroupBeanLL");
	DBRow[] productLineDefines = applyMoneyMgrLL.getAllByTable("productLineDefineBeanLL");
	//账号管理
	DBRow[] accountsRow = accountMgrZr.getAccountByAccountIdAndAccountWithType(aid,AccountKey.PRE_APPLY_MONEY);
	String deleteAccountAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/account/DeleteAccountAction.action";
	String updateAccountUrl =  ConfigBean.getStringValue("systenFolder") + "administrator/admin/ebay_account_update.html";
	String deleteImageAction = ConfigBean.getStringValue("systenFolder")+ "action/administrator/financial_management/applyDeleteImageAction.action";
	String uploadImageAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/financial_management/applyUploadImageAction.action";
	List imageList = applyMoneyMgrLL.getImageList(aid+"",1+"");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改预申请信息</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
 
 <!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script> 
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
 
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<script language="javascript">
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target){
	if($.trim(fileNames).length > 0 ){
	    $("input[name='file_names']").val(fileNames);
	    var  o = {
		    file_names:$("input[name='file_names']").val(),
		    sn:$("input[name='sn']").val(),
		    path:$("input[name='path']").val(),
		    backurl:$("input[name='backurl']").val(),
		    association_id:$("input[name='association_id']").val(),
		    association_type:$("input[name='association_type']").val()
		}
		$.ajax({
			url:'<%=uploadImageAction %>',
			dataType:'json',
			data:$.param(o),
			success:function(data){
				if(data && data.flag == "success"){
					window.location.reload();
				}else{
					alert("上传文件出错");
				}
			},
			error:function(){
				alert("系统错误!请稍后再试");
			}
		})
	}
	
}
//在线获取
function onlineScanner(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function deleteImage(_id){
	$.ajax({
		url:'<%= deleteImageAction%>' + "?id=" +_id,
		dataType:'json',
		success:function(data){
			if(data && data.flag == "success"){
				window.location.reload();
			}else{
				alert("系统错误!请稍后重试");
			}
		},
		error:function(){alert("系统错误!请稍后重试");}
	})
}
<!--
//加载控件
function init() {
}
$(document).ready(function(){
	getLevelSelect(0, nameArray, widthArray, centerAccounts, null,vname,<%=row.get("center_account_type_id",0)%>);
	getLevelSelect(1, nameArray, widthArray, centerAccounts, $("#center_account_type_id"),vname,<%=row.get("center_account_type1_id",0)%>);
	getLevelSelect(2, nameArray, widthArray, centerAccounts, $("#center_account_type1_id"),vname,<%=row.get("center_account_id",0)%>);
	getLevelSelect(0, nameArray1, widthArray1, pays, null,vname1,<%=row.get("payee_type_id",0)%>);
	getLevelSelect(1, nameArray1, widthArray1, pays, $("#payee_type_id"),vname1,<%=row.get("payee_type1_id",0)%>);
	getLevelSelect(2, nameArray1, widthArray1, pays, $("#payee_type1_id"),vname1,<%=row.get("payee_id",0)%>);
	$('#payee').val('<%=row.getString("payee")%>');
});

<%//level_id,value,name
	String str = "var pays = new Array(";
	str += "new Array('01',0,'任意')";
	str += ",new Array('01.0',0,'任意')";
	str += ",new Array('01.0.0',0,'任意')";
	str += ",new Array('02',1,'公司账户')";
	str += ",new Array('02.0',0,'公司账户')";
	str += ",new Array('02.0.0',0,'选择部门')";
	for(int i=0;i<accounts.length;i++) {
		DBRow account = accounts[i];
		if(account.get("account_parent_id",0)==0 && account.get("account_category_id",0)==1)//部门列表
			str += ",new Array('02.0."+(i+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
	}	
	str += ",new Array('03',2,'职员账户')";
	str += ",new Array('03.0',0,'选择部门')";
	str += ",new Array('03.0.0',0,'选择职员')";
	for(int i=0;i<adminGroups.length;i++) {//部门
		DBRow adminGroup = adminGroups[i];
		str += ",new Array('03."+(i+1)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
		str += ",new Array('03."+(i+1)+".0',0,'选择职员')";
		for(int ii=0;ii<accounts.length;ii++) {
			DBRow account = accounts[ii];
			if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
				str += ",new Array('03."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
		}
	} 
	str += ",new Array('04',3,'供应商')";
	str += ",new Array('04.0',0,'选择生产线')";
	str += ",new Array('04.0.0',0,'选择供应商')";
	for(int i=0;i<productLineDefines.length;i++) {
		DBRow productLineDefine = productLineDefines[i];
		str += ",new Array('04."+(i+1)+"',"+productLineDefine.get("id",0)+",'"+productLineDefine.getString("name")+"')";
		str += ",new Array('04."+(i+1)+".0',0,'选择供应商')";
		for(int ii=0;ii<accounts.length;ii++) {
			DBRow account = accounts[ii];
			if(account.get("account_parent_id",0)==productLineDefine.get("id",0) && account.get("account_category_id",0)==3)//供应商列表
				str += ",new Array('04."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
		}
	}
	
	str+= ");";
	out.println(str);
%>
<%//level_id,value,name
str = "var centerAccounts = new Array(";
str += "new Array('01',0,'任意')";
str += ",new Array('01.0',0,'任意')";
str += ",new Array('01.0.0',0,'任意')";
str += ",new Array('02',1,'公司账户')";
str += ",new Array('02.0',0,'公司账户')";
str += ",new Array('02.0.0',0,'选择部门')";
for(int i=0;i<accounts.length;i++) {
	DBRow account = accounts[i];
	if(account.get("account_parent_id",0)==0 && account.get("account_category_id",0)==1)//部门列表
		str += ",new Array('02.0."+(i+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
}	
str += ",new Array('03',2,'职员账户')";
str += ",new Array('03.0',0,'选择部门')";
str += ",new Array('03.0.0',0,'选择职员')";
for(int i=0;i<adminGroups.length;i++) {//部门
	DBRow adminGroup = adminGroups[i];
	str += ",new Array('03."+(i+1)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
	str += ",new Array('03."+(i+1)+".0',0,'选择职员')";
	for(int ii=0;ii<accounts.length;ii++) {
		DBRow account = accounts[ii];
		if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
			str += ",new Array('03."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
	}
}
	str+= ");";
	out.println(str);
%>

var nameArray = new Array('center_account_type_id','center_account_type1_id','center_account_id');
var widthArray = new Array(120,120,200);
var nameArray1 = new Array('payee_type_id','payee_type1_id','payee_id');
var widthArray1 = new Array(120,120,200);
var vname = new Array('nameArray','widthArray','pays','vname');
var vname1 = new Array('nameArray1','widthArray1','pays','vname1');

function getLevelSelect(level,nameArray, widthArray, selectArray,o,vnames,value) {
		if(level<nameArray.length) {
			var name = nameArray[level];
			var width = widthArray[level];
			var levelId = o==null?"":$("option:selected",o).attr('levelId');
			var onchangeStr = "";
			if(level==nameArray.length-1)
				onchangeStr = "exec(this)";
			else
				onchangeStr = "getLevelSelect("+(level+1)+","+vnames[0]+","+vnames[1]+","+vnames[2]+",this,"+vnames[3]+")";

			var selectHtml = "<select name='"+name+"' id='"+name+"' style='width:"+width+"px;' onchange="+onchangeStr+"> ";
			for(var i=0;i<selectArray.length;i++) {
				if(levelId!="") {
					var levelIdChange = selectArray[i][0].replace(levelId+".");
					var levelIds = levelIdChange.split(".");	
					
					if(levelIdChange!=selectArray[i][0]&&levelIds.length==1){
						//alert(levelId+",value="+selectArray[i][0]+",change='"+levelIdChange+"',"+levelIds.length);
						selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
					}
				}
				else {
					var levelIdChange = selectArray[i][0];
					var levelIds = levelIdChange.split(".");
					if(levelIds.length==1){
						//alert(levelId+","+selectArray[i][0]+levelId1);
						selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
					}
				}
				
			}
			selectHtml += "</select>";
			$('#'+name+'_div').html('');
			$('#'+name+'_div').append(selectHtml);
			//alert(value);
			$('#'+name).val(value);
		    
			$("#"+name).chosen();
			$("#"+name).chosen({no_results_text: "没有该项!"});
	
			getLevelSelect(level+1,nameArray,widthArray,vnames[2],$('#'+name),vnames);

			//修改输入框,破坏结构
			if($(o).attr("id")=="payee_type_id"||$(o).attr("id")=="payee_type1_id")
				$('#payee').val('');
		}
}

function exec(o) {
	if($(o).attr("id")=="payee_id") {
		if($("option:selected",o).val()!=0)
				$('#payee').val($("option:selected",o).attr('displayName'));
		else
				$('#payee').val('');
	}
}

function addAccount(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/account/account_add.html?account_with_id="+ '<%=row.get("apply_id",0)%>' + "&account_with_type=" + '<%= AccountKey.PRE_APPLY_MONEY%>'; 
	 $.artDialog.open(uri , {title: '添加账号信息',width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}
function deleteAccount(account_id){
 	$.artDialog.confirm('确定删除联该账号?', function () {
     	$.ajax({
				url:'<%= deleteAccountAction%>' + "?account_id="+ account_id ,
				dataType:'json',
				beforeSend:function(request){
				    $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				error:function(){alert("系统错误!");$.unblockUI();},
				success:function(data){
				    $.unblockUI();
				    if(data && data.flag === "success"){
						window.location.reload();
					}
				}
		})
	}, function () {
  		  
	});

}
function refreshWindow(){window.location.reload();}
function updateAccount(account_id){
 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/account/account_update.html?account_id="+ account_id; 
 $.artDialog.open(uri , {title: '修改账号信息',width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}
//-->
</script>

<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
td.left{width:75px;text-align:right;}
td.right{text-align:left ;font-weight:bold;color: #666666;width:250px;}
table.account_table{border:1px solid silver;border-collapse:collapse;}
table.account_table td{border:1px solid silver;}
</style>

<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 

<script type="text/javascript">
<!--
	function checkCategory()
	{
		var flag=true;
		  $.ajax({
		  type: "POST",
		  dataType: "json",
		  async:false,
		  url: "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/GetApplyFundsCategoryChildrenJsonPrecessAction.action",
		  data:"categoryId="+$("#filter_acid").val(),
		  success: function(msg){
			 if(msg.isok=="ok")
			{
				flag= false;
			}
	      }
		  
		});	
		return flag;
	}

function getClassNameBy() {
	if($("#association_type_id").val()==1) {
		return "";
	}else if($("#association_type_id").val()==2) {
		return "assetsBeanLL";
	}else if($("#association_type_id").val()==3) {
		return "assetsBeanLL";
	}else if($("#association_type_id").val()==4) {
		return "purchaseBeanLL";
	}else if($("#association_type_id").val()==5) {
		return "deliveryOrderBeanLL";
	}else if($("#association_type_id").val()==6) {
		return "transportBeanLL";
	}
}

function associationTypeCheck() {
	var flag=false;
	$.ajax({
		  type: "POST",
		  dataType:"json",
		  async:false,
		  url: "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/associationTypeCheckJSONAction.action",
		  data:"className="+getClassNameBy()+"&id="+$("#associationId").val()+"&type="+$("#association_type_id").val(),
		  async:false,
		  success: function(msg)
		  {
			 if(msg.isok=="true")
				 flag = true; 
	      }
	});
	return flag;
}
	
	function submitApply()
	{
			 if($("#filter_acid").val().trim()==0)
			 {
			 	alert("请选择资金申请类型！");
			 	$("#category").mcDropdown("#categorymenu").openMenu();
			 }
			 else if(!associationTypeCheck()) 
			 {
				 alert("填写单据不存在");
				 $("#associationId").focus()
			 }
			 else if(!checkCategory())
			 {			 	
			 	alert("资产必须增加到最底层类分，请重新选择");
				$("#category").mcDropdown("#categorymenu").openMenu();
			 }
			 else if($("#amount").val().trim()==""||$("#amount").val()==null)
			 {
			     $("#amount").focus();
			     alert("金额不能为空！");
			 }
			 else if(isNaN($("#amount").val().trim()))
		 	 {
			     $("#amount").focus();
			     alert("金额必须为数字！");
		 	 }
			 else if($("#amount").val().trim()<=0)
			 {
			 	 $("#amount").focus();
			     alert("金额必须大于零！");
			 }
			 else if($("#payee").val().trim()==""||$("#payee").val()==null)
			 {
			 	 $("#payee").focus();
			 	 alert("请填写收款人！");
			 }
			 else if($("#remark").val().trim().length>=200)
			 {
			     $("#remark").focus();
			     alert("备注字数不能超过200！");
			 }
			 else
			 {
			 	parent.document.apply_funds_mod_form.apply_id.value = <%=StringUtil.getLong(request,"aid",0l)%>;
			 	parent.document.apply_funds_mod_form.categoryId.value = $("#filter_acid").val();
			 	parent.document.apply_funds_mod_form.associationId.value = $("#associationId").val();
			 	parent.document.apply_funds_mod_form.association_type_id.value = $("#association_type_id").val();
			 	parent.document.apply_funds_mod_form.payee.value = $("#payee").val();
				if(myform.paymentInfo){
			 	 parent.document.apply_funds_mod_form.paymentInfo.value = $("#paymentInfo").val();
				}
				parent.document.apply_funds_mod_form.amount.value = $("#amount").val();
			 	parent.document.apply_funds_mod_form.remark.value = $("#remark").val();
			 	parent.document.apply_funds_mod_form.adid.value = $("#adid").val();			 	
			 	parent.document.apply_funds_mod_form.center_account_id.value = $("#center_account_id").val();
			 	parent.document.apply_funds_mod_form.center_account_type_id.value = $("#center_account_type_id").val();
			 	parent.document.apply_funds_mod_form.center_account_type1_id.value = $("#center_account_type1_id").val();
			 	parent.document.apply_funds_mod_form.product_line_id.value = $("#product_line_id").val();
			 	parent.document.apply_funds_mod_form.payee_type_id.value = $("#payee_type_id").val();			 	
			 	parent.document.apply_funds_mod_form.payee_type1_id.value = $("#payee_type1_id").val();
			 	parent.document.apply_funds_mod_form.payee_id.value = $("#payee_id").val();
			 	parent.document.apply_funds_mod_form.currency.value = $("#currency").val();
			 	parent.document.apply_funds_mod_form.amount_transfer.value = $("#amount_transfer").val();
				parent.document.apply_funds_mod_form.submit();
			 }	
	}
//-->
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="init()">
<form name="myform" method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">资金申请单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr height="29">
		          <td align="right" class="STYLE2">运营费用:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle" bgcolor="#eeeeee">

		          		          
		          <ul id="categorymenu" class="mcdropdown_menu">
	  <%
	  DBRow c1[] = applyFundsCategoryMgrZZZ.getApplyFundsCategoryChildren(0);
	  for (int i=0; i<c1.length; i++)
	  {
			out.println("<li rel='"+c1[i].get("category_id",0l)+"'> "+c1[i].getString("category_name"));

			  DBRow c2[] = applyFundsCategoryMgrZZZ.getApplyFundsCategoryChildren(c1[i].get("category_id",0l));
			  if (c2.length>0)
			  {
			  		out.println("<ul>");	
			  }
			  for (int ii=0; ii<c2.length; ii++)
			  {
					out.println("<li rel='"+c2[ii].get("category_id",0l)+"'> "+c2[ii].getString("category_name"));					
					out.println("</li>");				
			  }
			  if (c2.length>0)
			  {
			  		out.println("</ul>");	
			  }
			  
			out.println("</li>");
	  }
	  %>
</ul>
	  <input type="text" name="category" id="category" value="" />
	  <input type="hidden" name="filter_acid" id="filter_acid" value="0"  />
	  <script>
  		$("#category").mcDropdown("#categorymenu",{
  						width:200,
  						allowParentSelect:true,
  						targetColumnSize:1,
						allowParentSelect:true,
						  
						select:function (id,name){
									$("#filter_acid").val(id);
									//alert($("#filter_lid1").val());
								}
				
				});								
				$("#category").mcDropdown("#categorymenu").setValue(<%=row.get("types",0)%>);
  </script>	
	           
		          </td>
		        </tr>
		    <tr >
    <td  align="right" class="STYLE2"> 
    	<%		
		  	out.print("产品线:");
    	 %>
    </td>
    <td>&nbsp;</td>
     <td  align="left">
    		<ul id="productLinemenu" class="mcdropdown_menu">
    			<li rel="0">所有产品线</li>
					  <%
		
					  	  DBRow ca1[] = assetsCategoryMgr.getAssetsCategoryChildren(0,"2");
						  for (int i=0; i<ca1.length; i++)
						  {
								//out.println("<li onmousemove='catagory("+ca1[i].get("id",0l)+")' value='"+ca1[i].get("id",0l)+"'><a href='"+ca1[i].get("id",0l)+"'> "+ca1[i].getString("chName")+" </a>");
								  DBRow[] ca2 = null;
								  	ca2 = assetsCategoryMgr.getAssetsCategoryChildren(ca1[i].get("id",0l),"3");
								 
								  for (int ii=0; ii<ca2.length; ii++)
								  {
										out.println("<li rel='"+ca2[ii].get("id",0l)+"'> "+ca2[ii].getString("chName"));		
										
										out.println("</li>");				
								  }
							
					  }
					  %>
				</ul>
					  <input type="text" name="productLine" id="productLine" value="" />
					  <input type="hidden" name="product_line_id" id="product_line_id" value="0" />

       <script>
  $("#productLine").mcDropdown("#productLinemenu",{
						allowParentSelect:true,
						  select: 
						  
								function (id,name)
								{
									$("#product_line_id").val(id);
									//alert($("#filter_lid1").val());
								}
				
				});								
				$("#productLine").mcDropdown("#productLinemenu").setValue(<%=row.get("product_line_id",0)%>);
  </script>	
    </td>
    </tr>
		    
			<tr> 
		      <td align="right" class="STYLE2">关联单据类型:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<select name="association_type_id" id="association_type_id">
		      		<%
		      			for(int i=0;i<associationType.length;i++) {
		      				String id  = associationType[i].getValue("id").toString();
		      				String name  = associationType[i].getValue("name").toString();
		      				if(id.equals(row.getString("association_type_id")))
		      					out.println("<option value='"+id+"' selected>"+name+"</option>");
		      				else
		      					out.println("<option value='"+id+"'>"+name+"</option>");
		      			}
		      		%>
		      	</select>
		      	<input id="associationId" name="associationId" type="text" value="<%=row.getValue("association_id")%>"/>
		     </td>
		    </tr>
		    <tr>
		    <td align="right" class="STYLE2">货种</td>
		    <td>&nbsp;</td>
		    <td align="left" valign="middle">
		    	<%
		    		if(row.get("status",0)==0) {
		    	%>
		    	<select id="currency" name="currency">
		    		<option value="RMB" <%=row.getString("currency").equals("RMB")?"selected":""%>>RMB</option>
		    		<option value="USD" <%=row.getString("currency").equals("USD")?"selected":""%>>USD</option>
		    	</select>
		    	<%
		    		}else {
		    	%>
		    			<%=row.getString("currency") %>
		    			<input type="hidden" name="currency" id="currency" value="<%=row.getString("currency") %>">
		    	<%
		    		}
		    	%>
		    </td>
		  </tr>
		  <tr> 
		      <td align="right" class="STYLE2">金额</td>
		      <td>&nbsp;</td>
		      <td>
		      	<%
		      		if(row.get("status",0)==0) {
		    	%>
		      		<input name="amount" type="text" id="amount" size="15" value="<%=row.getValue("amount")%>"></td>
		      	<%
		    		}else {
		    			
		    	%>
		    			<%=row.getString("amount") %>
		    			<input type="hidden" name="amount" id="amount" value="<%=row.getString("amount") %>">
		    	<%
		    		}
		    	%>
		    </tr>
			<tr> 
		      <td align="right" class="STYLE2">成本中心</td>
		      <td>&nbsp;</td>
		      <td>		      		    		     
		      	<div style="float:left;" id="center_account_type_id_div" name="center_account_type_id_div">
				</div>
				<div id='center_account_type1_id_div' name='center_account_type1_id_div' style="float:left;">
				</div>
				<div id='center_account_id_div' name='center_account_id_div' style="float:left;">
		        </div>
		      </td>
	</tr>
		<tr>
	    <td align="right" class="STYLE2">收款人</td>
	    <td>&nbsp;</td>
	    <td>
	    	<div style="float:left;" id="payee_type_id_div" name="payee_type_id_div">		      	
			</div>
			<div id='payee_type1_id_div' name='payee_type1_id_div' style="float:left;">
			</div>
			<div id='payee_id_div' name='payee_id_div' style="float:left;">
	        </div>
	        <br/><br/>
	    	<input name="payee" type="text" id="payee"  class="input-line" value="<%=row.getString("payee") %>">
	    </td>
    </tr>		 
		    <tr> 
		      <td align="right" class="STYLE2">收款信息:</td>
		      <td>&nbsp;</td>
		      <td>	
		      <input type="button" value="添加收款信息" onclick="addAccount()" class="long-button"/>  
      	<br />
      	<!-- 增加收款信息 -->
      	<!-- 修改收款信息 -->
      	<!-- 查询account表如果是有记录的那么就读取出来显示。可以修改,可以新添加.如果是没有就按照以前的显示在一个textarea里面 -->
      	<%
      		if(accountsRow != null && accountsRow.length > 0 ){
				//读取信息显示出来
				String[] indexStr = new String[]{"一","二","三","四","五","六","七","八","九"};
				for(int index = 0 , count = accountsRow.length ; index < count ;index++ ){	
					boolean isOtherCurrency = !accountsRow[index].getString("account_currency").toUpperCase().equals("RMB");
					int rowNumber = isOtherCurrency?8:5;
				%>
					<table class="account_table">
						<tr>
							<td  rowspan="<%=rowNumber %>">账号<%=indexStr[index] %></td>
							<td class="left">货币类型:</td>
							<td class="right"><%=accountsRow[index].getString("account_currency").toUpperCase() %></td>
							<td rowspan="<%=rowNumber %>">	
								<a href="javascript:void(0)" onclick="deleteAccount('<%=accountsRow[index].getString("account_id") %>')">删除</a>
								<a href="javascript:void(0)" onclick="updateAccount('<%=accountsRow[index].getString("account_id") %>')">编辑</a>
							</td>
						</tr>
					
						 <tr>
							<td class="left">账号户名:</td>
							<td class="right"><%=accountsRow[index].getString("account_name").toUpperCase() %></td>
						</tr>
						<tr>
							<td class="left">账号:</td>
							<td class="right"><%= accountsRow[index].getString("account_number") %></td>
						</tr>
						<tr>
							<td class="left">联系电话:</td>
							<td class="right"><%= accountsRow[index].getString("account_phone") %></td>
						</tr>
						<tr>
							<td class="left">开户行:</td>
							<td class="right"><%= accountsRow[index].getString("account_blank") %></td>
						</tr>
						
						<%if(isOtherCurrency){ %>
							 <tr>
							 	<td class="left">SWIFT:</td>
							 	<td class="right"><%= accountsRow[index].getString("account_swift") %></td>
							 </tr>		
							 <tr>
								<td class="left">户名地址:</td>
								<td class="right"><%= accountsRow[index].getString("account_address") %></td>
							</tr>
							<tr>
								<td class="left">银行地址:</td>
								<td class="right"><%= accountsRow[index].getString("account_blank_address") %></td>
							</tr>
						<%} %>
					</table>
					 <br />	 
				 
				<% 
				}
      		}else{
      	%>		      		  	      		    		     
		      	<textarea rows="4" cols="40" name="paymentInfo" id="paymentInfo"><%=row.getString("payment_information") %></textarea>
		      <%} %>
		      </td>
		    </tr>
		     		    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="remark" rows="4" cols="40"><%=row.getString("remark") %></textarea></td>
		    </tr>
		    <tr>
		    	<td align="right" class="STYLE2">凭证</td>
		    	<td>&nbsp;</td>
		    	<td>
		    		<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件"/>
		    		<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
		    			<div id="jquery_file_up">	
			              		<input type="hidden" name="file_names" value=""/>
			              		<input type="hidden" name="sn" value="P_applyMoneny" />
			              		<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_financial")%>" />
							 	<input type="hidden" name="association_id" value='<%=row.get("apply_id",0) %>' />
							 	<input type="hidden" name="association_type" value="1"/>
							 	
			            </div>
		    	</td>
		    </tr>
		    <%if(imageList != null && imageList.size() > 0){ %>
   				<tr>
   					<td align="right" class="STYLE2"></td>
		    		<td>&nbsp;</td>
		    		<td>
					  
					 	<%
						for(int i=0; i<imageList.size(); i++) {
							HashMap imageMap = (HashMap)imageList.get(i);
					 	%>
					 		 <%= imageMap.get("path") %> <a  href="javascript:void(0)"  onclick="deleteImage('<%=imageMap.get("id") %>')">删除</a> <br />
					 	<%} %>
					  
					</td>
				</tr>
		<%} %>
		</table>
		<input id="adid" name="adid" type="hidden" value="<%=adminLoggerBean.getAdid() %>"/>
		<input id="amount_transfer" name="amount_transfer" type="hidden" value="<%=row.getString("amount_transfer") %>"/>
		
	</td>
  </tr>
  <!-- 
  <tr>
  	<td colspan="3"><iframe width="100%" height="100" frameborder=0 scrolling=auto src="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/upload_image.html?association_id=<%=row.get("apply_id",0) %>&association_type=1&update=1"></iframe></td>
  </tr>
   -->
	</fieldset>	
</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onclick="submitApply()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="$.artDialog && $.artDialog.close();" value="取消" >
    </td>
  </tr>
</table>
</form>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 
	$(".chzn-select").chosen(); 
	$(".chzn-select1").chosen(); 
	$(".chzn-select2").chosen(); 
</script>
</body>

</html>
