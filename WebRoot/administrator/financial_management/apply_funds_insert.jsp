<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 	
 	long aid=StringUtil.getLong(request,"aid",0l);
 	DBRow[]row = applyMoneyMgrZZZ.getApplyMoneyById(Long.toString(aid));
 	DBRow[] accounts = assetsMgr.getAccounts();
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<script language="javascript">

<!--
//加载控件
<%
	String v = "var account = new Array(";
	v += "new Array('0','0','0')";
	for(int i=0;i<accounts.length;i++) {
		DBRow account = accounts[i];
		
		v += ",new Array("+account.get("acid",0)+","+account.get("account_category_id",0)+",'"+account.getString("account_name")+"')";
	} 
	v+= ");";
	out.println(v);
%>

function selectClass() {
	var classId = 0;
	
	for(var i=0; i<account_select.options.length; i++) {
		if(account_select.options[i].selected==true) {
			classId = account_select.options[i].value;
		}
	}

	getAccountSelect('center_account_id','chzn-select',account,classId);
}

function getAccountSelect(name,className,accountArray,classId) {
	
	var selectHtml = "<select name='"+name+"' id='"+name+"' class='"+className+"' style='width:120px;' onchange='selectEmp()'>";
	selectHtml += "<option value='0' rel='0'>选择成员</option> ";
	for(var i=0; i<accountArray.length; i++) {

		if(classId==0) {
			selectHtml += "<option value='"+accountArray[i][0]+"' rel='" + accountArray[i][1] + "'>"+accountArray[i][2]+"</option>";
		}else if(classId == accountArray[i][1]){
			selectHtml += "<option value='"+accountArray[i][0]+"' rel='" + accountArray[i][1] + "'>"+accountArray[i][2]+"</option>";
		}
		
	}
	selectHtml += "</select>";
	$('#accountDiv').html('');
	$('#accountDiv').append(selectHtml);
	$("."+className).chosen();
	$("."+className).chosen({no_results_text: "没有该人:"});
	$("."+className).chosen({allow_single_deselect:true}); 

}
//-->
</script>

<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>

<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">资金申请单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr height="29">
		          <td align="right" class="STYLE2">运营费用:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle" bgcolor="#eeeeee">

		          		          
		          <ul id="categorymenu" class="mcdropdown_menu">
	  <%
	  DBRow c1[] = applyFundsCategoryMgrZZZ.getApplyFundsCategoryChildren(0l);
	  for (int i=0; i<c1.length; i++)
	  {
	  	if(c1[i].getString("category_name").equals("运营费用")) {
			//out.println("<li rel='"+c1[i].get("category_id",0l)+"'> "+c1[i].getString("category_name"));

			  DBRow c2[] = applyFundsCategoryMgrZZZ.getApplyFundsCategoryChildren(c1[i].get("category_id",1l));
			  /*if (c2.length>0)
			  {
			  		out.println("<ul>");	
			  }*/
			  for (int ii=0; ii<c2.length; ii++)
			  {
			  		if(!(c2[ii].getString("category_name").equals("运费")))
					out.println("<li rel='"+c2[ii].get("category_id",0l)+"'> "+c2[ii].getString("category_name"));					
					out.println("</li>");				
			  }
			  /*if (c2.length>0)
			  {
			  		out.println("</ul>");	
			  }*/
			  
			//out.println("</li>");
		}
	  }
	  %>
</ul>
	  <input type="text" name="category" id="category" value="" />
	  <input type="hidden" name="filter_acid" id="filter_acid" value="0"  />
	  <script>
  		$("#category").mcDropdown("#categorymenu",{
						allowParentSelect:true,
						  select: 
						  
								function (id,name)
								{
									$("#filter_acid").val(id);
									//alert($("#filter_lid1").val());
								}
				
				});								
				$("#category").mcDropdown("#categorymenu").setValue(<%=row[0].get("types",0)%>);
  </script>	
	           
		          </td>
		        </tr>
		    <tr> 
		      <td align="right" class="STYLE2" nowrap="nowrap">成本中心:</td>
		      <td>&nbsp;</td>
		      <td>		      		    		     
		      	<div style="float:left;">
		      	<select name="account_select" id="account_select" data-placeholder="请选择分类" class="chzn-select1" style="width:100px;" onchange="selectClass()">
					<option value="0">任意</option>
					<option value="1">公司部门</option>
					<option value="2">公司职员</option>
				</select>
				</div>
				<div id='accountDiv' name='accountDiv' style="float:left;">
		      	<select name="center_account_id" id="center_account_id" data-placeholder="请选择账户" class="chzn-select" style="width:100px;" onchange="">
		          <option value='0' rel='0'>成本中心</option>
		          <% 
		          	for(int i=0; i<accounts.length; i++) {
		          		long center_account_id = row[0].get("center_account_id",0);
		          		long acid = accounts[i].get("acid",01);
		          		String account_name = accounts[i].getString("account_name");
		          		if(center_account_id == acid)
		          			out.println("<option value='"+acid+"' selected>"+account_name+"</option>");
		          		else 
		          			out.println("<option value='"+acid+"'>"+account_name+"</option>");
		          	}
		          %>
		        </select>
		        </div>
		      </td>
		   
		    </tr>
			
		  <tr> 
		      <td align="right" class="STYLE2">金额:</td>
		      <td>&nbsp;</td>
		      <td><input name="amount" type="text" id="amount" size="15" value="<%=row[0].getValue("amount")%>"></td>
		    </tr>
				  
		    <tr> 
		      <td align="right" class="STYLE2">收款人:</td>
		      <td>&nbsp;</td>
		     
		      <td><input name="payee" id="payee" type="text" value="<%=row[0].getString("payee") %>" /></td>
		   
		    </tr>		 
		    <tr> 
		      <td align="right" class="STYLE2">付款信息:</td>
		      <td>&nbsp;</td>
		      <td>		      		    		     
		      	<textarea rows="4" cols="40" name="paymentInfo" id="paymentInfo"><%=row[0].getString("payment_information") %></textarea>
		      </td>
		    </tr>
		     		    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="remark" rows="4" cols="40"><%=row[0].getString("remark") %></textarea></td>
		    </tr>
		</table>
		<input id="associationId" name="associationId" type="hidden" value="0"/>
		<input id="adid" name="adid" type="hidden" value="<%=adminLoggerBean.getAdid() %>"/>
		
	</fieldset>	
	</td>
  </tr>

</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onclick="submitApply()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="parent.closeWinNotRefresh()" value="取消" >
    </td>
  </tr>
</table>
<script type="text/javascript">
<!--
	function checkCategory()
	{
		var flag=true;
		  $.ajax({
		  type: "POST",
		  dataType: "json",
		  async:false,
		  url: "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/GetApplyFundsCategoryChildrenJsonPrecessAction.action",
		  data:"categoryId="+$("#filter_acid").val(),
		  success: function(msg){
			 if(msg.isok=="ok")
			{
				flag= false;
			}
	      }
		  
		});	
		return flag;
	}

	function submitApply()
	{
			 if($("#filter_acid").val().trim()==0)
			 {
			 	alert("请选择资金申请类型！");
			 	$("#category").mcDropdown("#categorymenu").openMenu();
			 }
			 else if(!checkCategory())
			 {			 	
			 	alert("资产必须增加到最底层类分，请重新选择");
				$("#category").mcDropdown("#categorymenu").openMenu();
			 }
			 else if($("#amount").val().trim()==""||$("#amount").val()==null)
			 {
			     $("#amount").focus();
			     alert("金额不能为空！");
			 }
			 else if(isNaN($("#amount").val().trim()))
		 	 {
			     $("#amount").focus();
			     alert("金额必须为数字！");
		 	 }
			 else if($("#amount").val().trim()<=0)
			 {
			 	 $("#amount").focus();
			     alert("金额必须大于零！");
			 }
			 else if($("#payee").val().trim()==""||$("#payee").val()==null)
			 {
			 	 $("#payee").focus();
			 	 alert("请填写收款人！");
			 }
			 else if($("#paymentInfo").val().trim()==""||$("#paymentInfo").val()==null)
			 {
			 	 $("#paymentInfo").focus();
			 	 alert("请填写付款信息！");
			 } 
			 else if($("#paymentInfo").val().trim().length>600)
			 {
			 	 $("#paymentInfo").focus();
			 	 alert("付款信息不能超过600字符！");
			 }			 
			 else if($("#remark").val().trim().length>=200)
			 {
			     $("#remark").focus();
			     alert("备注字数不能超过200！");
			 }
			 else
			 {
			 	parent.document.apply_funds_mod_form.apply_id.value = <%=StringUtil.getLong(request,"aid",0l)%>;
			 	parent.document.apply_funds_mod_form.categoryId.value = $("#filter_acid").val();
			 	parent.document.apply_funds_mod_form.associationId.value = $("#associationId").val();
			 	parent.document.apply_funds_mod_form.payee.value = $("#payee").val();
			 	parent.document.apply_funds_mod_form.paymentInfo.value = $("#paymentInfo").val();;
				parent.document.apply_funds_mod_form.amount.value = $("#amount").val();
			 	parent.document.apply_funds_mod_form.remark.value = $("#remark").val();
			 	parent.document.apply_funds_mod_form.adid.value = $("#adid").val();			 	
			 	parent.document.apply_funds_mod_form.center_account_id.value = $("#center_account_id").val();

				parent.document.apply_funds_mod_form.submit();
			 }	
	}
//-->
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 
	$(".chzn-select").chosen(); 
	$(".chzn-select1").chosen(); 
</script>
</body>
</html>
