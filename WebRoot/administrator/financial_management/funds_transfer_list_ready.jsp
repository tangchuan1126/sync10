<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.zzz.ApplyMoneyMgrZZZ"%>
<%@page import="com.cwc.app.key.ApplyStatusKey"%>
<%@page import="com.cwc.app.lucene.zr.ApplyTransferIndexMgr"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
 	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(20);
 	
 	String st = StringUtil.getString(request,"st");
 	String en = StringUtil.getString(request,"en");
     long applyMoneyId = StringUtil.getLong(request,"apply_id",0l);
     String transferId = StringUtil.getString(request,"transfer_id","");
  	
  	String cmd = StringUtil.getString(request,"cmd");
  	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 	ApplyStatusKey statusKey = new ApplyStatusKey(); 
  	//System.out.println(cmd + " : " + transferId);
 	DBRow applyTransfer[] = null;
 	if(applyMoneyId!=0l)
 	{
 		applyTransfer = applyTransferMgrZZZ.getApplyTransferByApplyMoneyId(applyMoneyId,pc);
 	}
 	else if(cmd.equals("search")&&!"".equals(transferId))
  	{
  	    applyTransfer = applyTransferMgrZZZ.getApplyTransferById(transferId);
  	}
 	else if(cmd.equals("filter"))
 	{
 	  	applyTransfer = applyTransferMgrZZZ.getApplyTransferByCondition(request,pc);
 	}else if(cmd.equals("search_l")){
 		applyTransfer = applyTransferMgrZZZ.getApplyTransferByKey(transferId,pc);
 	}else
 	{
 		applyTransfer = applyTransferMgrZZZ.getAllApplyTransfer(pc);
 	}
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>


<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
 <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
 
 

 

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />

<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
 
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
 
 



	 
			
 
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

 .searchbarGray{
        color:#c4c4c4;font-family: Arial
   }
</style>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript">
<!--
  $(document).ready(function(){
  
  $("#transfer_id").attr("class","searchbarGray");
  <%
  String status=StringUtil.getString(request,"status");
  if(cmd.equals("filter"))
 	{
		%>
		  $("#st").val("<%=st%>");
		  $("#en").val("<%=en%>");
		  $("#status option[value='<%=status%>']").removeAttr("selected");
		  $("#status option[value='<%=status%>']").attr("selected","selected");
		  		  
		<%
 	    cmd="";
 	}
 	else if(cmd.equals("search"))
 	{
 	    %>
 	    $("#transfer_id").removeAttr("class");
 	    $("#transfer_id").val("<%=transferId%>");
		<%
 	    cmd="";
 	}
  
  %>
  });
  
  function query()
  {
  	 if($("#transfer_id").val().trim().length < 1)
  	 {
  	 	$("#transfer_id").focus();
  	  	alert("请输入查询条件");
  	  	return ;  	 
  	 }
 		 
	 $("#postValueForm").submit();
  }
  //文本框水印
  
function changeColorBlack(){
    var transfer_id=$("#transfer_id");
	if(transfer_id.val()=="*单号若为资金申请单号，必须前缀加单号查询")
	{
 	 	 
 	 	 transfer_id.val("");
 	 	 $("#transfer_id").removeAttr("class");
 	}
 	
}

function changeColorGray()
{
	 var transfer_id=$("#transfer_id");
	 if(transfer_id.val().trim()=="")	  
	 {
	  transfer_id.val("*单号若为资金申请单号，必须前缀加单号查询");
	  transfer_id.attr("class","searchbarGray");
	 }
}
  
  function reloadFundsTransfer(id)
  {
	  	 window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?apply_id="+id;
  }
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}

	function showUpdateVoucherMore(){
		var arr_transfer_ids = document.getElementsByName("transfer_ids");
		var str_transfer_ids = "";
		for(i=0;i<arr_transfer_ids.length;i++) {
			if(arr_transfer_ids[i].checked) {
				if(str_transfer_ids == "")
					str_transfer_ids = arr_transfer_ids[i].value;
				else
					str_transfer_ids += "," + arr_transfer_ids[i].value;
			}
		}
		if(str_transfer_ids=="") {
			alert("请选择上传凭证!");
			return;
		}
		tb_show('上传转账凭证','uploadVoucherMore.html?aid=100706&applyMoneyId=100458&transfer_ids='+str_transfer_ids+'&TB_iframe=true&height=450&width=550',false);
	}

	jQuery(function($){
		addAutoComplete($("#transfer_id"),
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/applyTransfer/GetSearchApplyTransferJSONAction.action",
				"merge_field","transfer_id");
		
	})
//-->
</script>
</head>

<body onLoad="onLoadInitZebraTable()">
<form id="deleteFundsTransferForm" name="deleteFundsTransferForm" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/DeleteFundsTransferPrecessAction.action">
<input type="hidden" id="reason" name="reason"/>
<input type="hidden" id="applyId" name="applyId" />
<input type="hidden" id="transferId" name="transferId"  />
<input type="hidden" id="userName" name="userName" value="<%=adminLoggerBean.getEmploye_name()%>"/>
<input type="hidden" id="userName_id" name="userName" value="<%=adminLoggerBean.getAdid()%>"/>
</form>			
 
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	
	<div class="demo" >

	<div id="tabs">
<ul>
		<li><a href="#usual_tools">常用工具</a></li>
		<li><a href="#advan_search">高级搜索</a></li>		 
</ul>

		<div id="usual_tools">
		<form method="post"  id="postValueForm"   >
		<input type="hidden" id="cmd" name="cmd" value="search_l"/>
          <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
            <tr>
            	 <td width="18%" align="left" style="padding-top:3px;padding-left: 5px" nowrap="nowrap">
	            	 <div id="easy_search_father">
								<div id="easy_search" style="margin-top:-2px;"><a href="javascript:query()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
					  </div>
	            		<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
									 <input name="transfer_id" id="transfer_id" value="<%=transferId %>"  type="text" class="search_input" style="font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)query()"/>
									</div>
								</td>
								<td width="67px">&nbsp;
								</td>
								<td width="67">
									<input type="button" class="button_long_search" id="btnSubmit1" name="btnSubmit1" value="多笔转账" onclick="showUpdateVoucherMore()"/>
								</td>
							</tr>
						</table>
				</td>	        
             
            </tr>
          </table>
		  </form> 
		</div>

		
	  <%
TDate tDate = new TDate();
tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

String input_st_date,input_en_date;
if ( st.equals("") )
{	
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}
else
{
	input_st_date = st;
}

if ( en.equals("") )
{	
	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
}else
{
	input_en_date = en;
}

%>
 
 <div id="advan_search">

	<form name="filterForm" action="funds_transfer_list.html" method="post">
	<input type="hidden" id="cmd" name="cmd" value="filter"/>
	   <table width="100%" height="30" border="0" cellpadding="1" cellspacing="0">
		  
		    <tr>
		      <td width="95%" height="30" >
		      
		     	   开始
		        <input name="st" type="text" id="st" size="9" value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
		        &nbsp;       
				  结束
		        <input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
		        &nbsp;
		        状态：
		      <select name="status" id="status" style="border:1px #CCCCCC solid">
		        <option value="-1">全部</option>
		        <option value="<%=ApplyStatusKey.NO_APPLY %>"><%=statusKey.getApplyTypeStatusById(ApplyStatusKey.NO_APPLY)%></option>
		        <option value="<%=ApplyStatusKey.FINISH %>"><%=statusKey.getApplyTypeStatusById(ApplyStatusKey.FINISH)%></option>
		      </select>
		       &nbsp; &nbsp;
		       <input type="submit" id="seachByCondition" name="seachByCondition" class="button_long_refresh" value="过    滤" />
			</td>
		      </tr>
		</table>
	</form>
 </div>

	</div>	
	</div>	
	</td>
  </tr>
</table>
<br/>
<script>
<!--
	$("#st").date_input();
    $("#en").date_input();
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
//-->
</script>
<div id="applyMoneyList">
  <table width="120%" border="0" align="center" style="padding-top: 17px" cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table">
     <tr>
       <th width="3%"  class="right-title"  style="text-align: center;" nowrap="nowrap">&nbsp;</th>
       <th width="4%"  class="right-title"  style="text-align: center;" nowrap="nowrap">申请单号资金单号</th>
       <th width="3%"  class="right-title"  style="text-align: center;" nowrap="nowrap">关联单据</th>
       <th width="10%"  class="right-title"  style="text-align: center;" nowrap="nowrap">付款信息</th> 
       <th width="20%"  class="right-title"  style="text-align: center;" nowrap="nowrap">收款信息</th>       
       <th width="5%"  class="right-title"  style="text-align: center;" nowrap="nowrap">金额</th>
       <th width="20%"  class="right-title"  style="text-align: center;" nowrap="nowrap">创建人及时间</th>
 
       <th width="20%"  class="right-title"  style="text-align: center;" nowrap="nowrap">备注</th>
       <th width="6%" class="right-title" style="text-align: center;" nowrap="nowrap"><br></th>
     </tr>
     <%
     if(applyTransfer!=null){
	     for(int i=0;i<applyTransfer.length;i++)
	     {
      %>
     <tr height="80px" style="padding-top:3px;">
        <td align="center" valign="middle">
         <%
          if(applyTransfer[i].getString("status").equals("0"))
          {
         %>
        	<input type="checkbox" name="transfer_ids" id="transfer_ids" value="<%=applyTransfer[i].get("transfer_id",0l) %>">
         <%
          }else{
        	  out.print("&nbsp;");
          }
         %>
        </td>
        <td align="center" valign="middle">
            W<%=applyTransfer[i].get("transfer_id",0l) %><br/>
            <a href="javascript:void(0)" onClick="goApplyFundsPage('<%=applyTransfer[i].get("apply_money_id",0l) %>')">F<%=applyTransfer[i].get("apply_money_id",0l) %></a><br />
       			  <%=statusKey.getApplyTypeStatusById(applyTransfer[i].getString("status")) %>
        </td>
        <td align="center" valign="middle" nowrap="nowrap">
        	<%
        		DBRow[] applyMoney = applyMoneyMgrZZZ.getApplyMoneyById(applyTransfer[i].getString("apply_money_id"));
        		out.println("资金类型:"+applyMoney[0].getString("categoryName")+"<br>");
        		DBRow[] associationType = applyMoneyMgrLL.getMoneyAssociationTypeAll();
        		
        		for(int j=0;j<applyMoney.length;j++) {
            		long association_type_id = applyMoney[j].get("association_type_id",0l);
            		long association_id = applyMoney[j].get("association_id",0l);
      		%>
            	<br>
            <%
	            	if(association_type_id==2)//固定资产
	            		out.println("固定资产:"+association_id);
	            	else if(association_type_id==3)//样品
		        		out.println("样品:"+association_id);
	            	else if(association_type_id==4)//采购单
		        		out.println("关联单据:<a href='"+ConfigBean.getStringValue("systenFolder")+"administrator/purchase/purchase_detail.html?purchase_id="+association_id+"'>P"+association_id+"</a>");
	            	else if(association_type_id==5) {//交货单
	            		DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrder(association_id);
	            		if(delivery_order != null){
	            			out.println("关联单据:<a href='"+ConfigBean.getStringValue("systenFolder")+"administrator/delivery/administrator_delivery_order_detail.html?delivery_order_id="+association_id+"'>"+delivery_order.getString("delivery_order_number")+"</a>");
	            		}
	            	 }
	            	else if(association_type_id==6)//转运单
	            		out.println("关联单据:<a href='"+ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_order_detail.html?transport_id="+association_id+"'>T"+association_id+"</a>");
        		}
        	%>
        </td>
        <td align="left" style="padding-top:10px;padding-bottom: 10px" valign="middle" nowrap="nowrap">
        	付款人:<%=applyTransfer[i].getString("payer") %><br>
        	付款账号:<%=StringUtil.ascii2Html(applyTransfer[i].getString("payment_information")) %>
        </td>   
        <td align="left" style="padding-top:10px;padding-bottom: 10px" valign="middle" nowrap="nowrap">
         	收款人：<%=applyTransfer[i].getString("receiver") %><br>
        	收款账号：<%=applyTransfer[i].getString("receiver_account") %><br>
        	收款信息:<%=StringUtil.ascii2Html(applyTransfer[i].getString("receiver_information")) %>
        </td>
        <td align="center" valign="middle" nowrap="nowrap" >
          <%=applyTransfer[i].get("amount",0d) %>
          <%=applyTransfer[i].getString("currency") %>
        </td>
                   
       
     
        <%
          if(applyTransfer[i].getString("status").equals("0"))
          {
         %>
          <td align="center" valign="middle">
           创建人: <%=applyTransfer[i].getString("creater") %><br />
         创建时间:<%= applyTransfer[i].getString("create_time").length() > 10 ? applyTransfer[i].getString("create_time").substring(0,10) : applyTransfer[i].getString("create_time")%>
        </td>
         
        <td align="left" stype="padding-top:10px;padding-bottom: 10px;" valign="middle">&nbsp;
          <%=StringUtil.ascii2Html(applyTransfer[i].getString("remark")) %>
        </td>
        <td align="left" valign="middle">
        <input type="button" class="short-short-button-cancel" value="删除" onClick="tb_show('删除原因','delete_funds_transfer_message.html?transferId=<%=applyTransfer[i].get("transfer_id",0l) %>&applyMoneyId=<%=applyTransfer[i].get("apply_money_id",0l) %>&TB_iframe=true&height=190&width=370',false);" />
        <br/><br/>
        <input type="button" class="short-button" onClick="tb_show('上传转账凭证','uploadVoucher.html?aid=<%=applyTransfer[i].get("transfer_id",0l) %>&applyMoneyId=<%=applyTransfer[i].get("apply_money_id",0l) %>&TB_iframe=true&height=450&width=550',false);" value="确认已转账" />
        </td>
        <%
         }
         else if(applyTransfer[i].getString("status").equals("2"))
         {
         %>
          <td align="center" valign="middle">
          创建人:<%=applyTransfer[i].getString("creater") %><br />
      创建时间:<%=applyTransfer[i].getString("create_time").length() > 10 ? applyTransfer[i].getString("create_time").substring(0,10):applyTransfer[i].getString("create_time")%><br />
    转账时间:<%=applyTransfer[i].getString("voucher_createTime").length() > 10 ? applyTransfer[i].getString("voucher_createTime").substring(0,10) :  applyTransfer[i].getString("voucher_createTime")%> 
        </td>
         
         <td align="left" stype="padding-top:10px;padding-bottom: 10px;" valign="middle">&nbsp;
          <%=StringUtil.ascii2Html(applyTransfer[i].getString("remark")) %>
        </td>
         <td align="left" valign="middle"> 
        <input type="button" class="short-button" onClick="$.artDialog.open('uploadImage/<%=applyTransfer[i].getString("voucher") %>', {title: '查看凭证',width:'800px',height:'1024px', lock: true,opacity: 0.3});" value="查看凭证" />
        </td>
         <%
         }
         else
         {
          %>
         <td align="center" valign="middle">&nbsp;<%=applyTransfer[i].getString("voucher_createTime") %></td>
         <td align="left" stype="padding-top:10px;padding-bottom: 10px;" valign="middle">&nbsp;
          <%=StringUtil.ascii2Html(applyTransfer[i].getString("remark")) %>
        </td>
         <td align="center" valign="middle"> 
         	&nbsp;
        </td>
         <%
         }
          %>
     </tr>
     <%      	
    	 }
     }
      %>
  </table>
  <br>
  <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" id="dataForm" action="funds_transfer_list.html">
    <input type="hidden" name="p">
    <input type="hidden" id="cmd" name="cmd" value="<%=StringUtil.getString(request,"cmd") %>"/>
    <input type="hidden" id="transfer_id" name="transfer_id" value="<%=StringUtil.getString(request,"transfer_id","") %>" />
    <input name="st" type="hidden" id="st" size="9" value="<%=StringUtil.getString(request,"st")%>"/>
    <input name="en" type="hidden" id="en" size="9" value="<%=StringUtil.getString(request,"en")%>"/>
    <input type="hidden" id="status" name="status" value="<%=StringUtil.getInt(request,"status",-1) %>"/>
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
</div>	
</body>
</html>