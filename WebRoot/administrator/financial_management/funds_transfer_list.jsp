<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.zzz.ApplyMoneyMgrZZZ"%>
<%@page import="com.cwc.app.key.TransferAccountsKey"%>
<jsp:useBean id="transferAccountsKey" class="com.cwc.app.key.TransferAccountsKey"></jsp:useBean>
<%@page import="com.cwc.app.lucene.zr.ApplyTransferIndexMgr"%> 
<%@page import="com.cwc.app.key.AccountKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
 	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(systemConfig.getIntConfigValue("order_page_count"));
 	
 	String st = StringUtil.getString(request,"st");
 	String en = StringUtil.getString(request,"en");
     long applyMoneyId = StringUtil.getLong(request,"apply_id",0l);
     String transferId = StringUtil.getString(request,"transfer_id","");
  	
  	String cmd = StringUtil.getString(request,"cmd");
  	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 	
 	int search_mode = StringUtil.getInt(request,"search_mode");
 	String selectSortTime=StringUtil.getString(request,"selectSortTime");
  
 	DBRow applyTransfer[] = null;
 	if(applyMoneyId!=0l)
 	{
 		applyTransfer = applyTransferMgrZZZ.getApplyTransferByApplyMoneyId(applyMoneyId,pc);
 	}
 	else if(cmd.equals("search")&&!"".equals(transferId))
  	{
  	    applyTransfer = applyTransferMgrZZZ.getApplyTransferById(transferId);
  	}
 	else if(cmd.equals("filter"))
 	{
 	  	applyTransfer = applyTransferMgrZZZ.getApplyTransferByCondition(request,pc);
 	}
 	else if(cmd.equals("search_l"))
 	{
 		applyTransfer = applyTransferMgrZZZ.getApplyTransferByKey(transferId,search_mode,pc);
 	}
 	else if(cmd.equals("end_time"))
 	{
 		applyTransfer = applyTransferMgrZZZ.getApplyTransferEndTimeSort(pc);
 	}
 	else if(cmd.equals("last_time"))
 	{
 		applyTransfer = applyTransferMgrZZZ.getApplyTransferLastTimeSort(pc);
 	}
 	else
 	{
 		applyTransfer = applyTransferMgrZZZ.getAllApplyTransfer(pc);
 	}
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>转账申请列表</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>


<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
 <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
 
 

 

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />

<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
 
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
<!-- 文件预览  -->
 <script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
 
 



	 
			
 
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

 .searchbarGray{color:#c4c4c4;font-family: Arial}
 .zebraTable td { border-bottom: 0px solid #DDDDDD; }
</style>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript">
// 遮罩 
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
<!--
  $(document).ready(function(){
  
  $("#transfer_id").attr("class","searchbarGray");
  <%
  String status=StringUtil.getString(request,"status");
  if(cmd.equals("filter"))
 	{
		%>
		  $("#st").val("<%=st%>");
		  $("#en").val("<%=en%>");
		  $("#status option[value='<%=status%>']").removeAttr("selected");
		  $("#status option[value='<%=status%>']").attr("selected","selected");
		  		  
		<%
 	    cmd="";
 	}
 	else if(cmd.equals("search"))
 	{
 	    %>
 	    $("#transfer_id").removeAttr("class");
 	    $("#transfer_id").val("<%=transferId%>");
		<%
 	    cmd="";
 	}
  
  %>
  });
  
  function query()
  {
  	 var val = $("#transfer_id").val();  
  	 if(val.trim().length < 1)
  	 {
  	 	$("#transfer_id").focus();
  	  	alert("请输入查询条件"); 
  	 }
  	 else
  	 {
  	 	val = val.replace(/\'/g,'');
  	 	var val_search = "\'"+val+"\'";
		$("#transfer_id").val(val_search);
    	$("#search_mode").val(1);
    	$("#postValueForm").submit();
  	 }
  }
  
  function queryRightButton()
  {
	 var val = $("#transfer_id").val();  
  	 if(val.trim().length < 1)
  	 {
  	 	$("#transfer_id").focus();
  	  	alert("请输入查询条件"); 
  	 }
  	 else
  	 {
  	 	val = val.replace(/\'/g,'');
		$("#transfer_id").val(val);
    	$("#search_mode").val(2);
    	$("#postValueForm").submit();
  	 }
  }
  
  function eso_button_even()
  {
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
			
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
	       	queryRightButton();
	   	}  
	}  
  }
  
  //文本框水印
  
function changeColorBlack(){
    var transfer_id=$("#transfer_id");
	if(transfer_id.val()=="*单号若为资金申请单号，必须前缀加单号查询")
	{
 	 	 
 	 	 transfer_id.val("");
 	 	 $("#transfer_id").removeAttr("class");
 	}
 	
}

function changeColorGray()
{
	 var transfer_id=$("#transfer_id");
	 if(transfer_id.val().trim()=="")	  
	 {
	  transfer_id.val("*单号若为资金申请单号，必须前缀加单号查询");
	  transfer_id.attr("class","searchbarGray");
	 }
}
  
  function reloadFundsTransfer(id)
  {
	  	 window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?apply_id="+id;
  }
	function goApplyFunds(id)
	{
		var id="'F"+id+"'";
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=searchby_index&search_mode=1&apply_id="+id);       
	}
	function showUpdateVoucherMore(){
		var arr_transfer_ids = document.getElementsByName("transfer_ids");
		var str_transfer_ids = "";
		for(i=0;i<arr_transfer_ids.length;i++) {
			if(arr_transfer_ids[i].checked) {
				if(str_transfer_ids == "")
					str_transfer_ids = arr_transfer_ids[i].value;
				else
					str_transfer_ids += "," + arr_transfer_ids[i].value;
			}
		}
		if(str_transfer_ids=="") {
			alert("请选择转账申请!");
			return;
		}
		var uri = 'uploadVoucherMore.html?aid=100706&applyMoneyId=100458&transfer_ids='+str_transfer_ids;
		$.artDialog.open(uri , {title: "多笔转账",width:'550px',height:'450px', lock: true,opacity: 0.3,fixed: true});
		//tb_show('上传转账凭证','uploadVoucherMore.html?aid=100706&applyMoneyId=100458&transfer_ids='+str_transfer_ids+'&TB_iframe=true&height=450&width=550',false);
	}

	jQuery(function($){
		addAutoComplete($("#transfer_id"),
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/applyTransfer/GetSearchApplyTransferJSONAction.action",
				"merge_field","transfer_id");

		//如果分辨率小
		 
		if($(window).width()*1 < 1200){
			$(".zebraTable").css("width","110%");
		}
		
		
	})
	$(window).resize(function(){
	    if($(window).width()*1 < 1200){
			$(".zebraTable").css("width","110%");
		}
	});
	function followup(applyTransferId,struts,association_type_id,association_id,apply_money_id,categoryId,apply_money_status)
	{  
	 	 var uri = "funds_transfer_log_add.html?apply_money_id="+apply_money_id+"&applyTransferId="+applyTransferId+"&struts="+struts+"&association_type_id="+association_type_id+"&association_id="+association_id+"&categoryId="+categoryId+"&apply_money_status="+apply_money_status;
		 $.artDialog.open(uri , {title: "日志跟进["+applyTransferId+"]",width:'450px',height:'320px', lock: true,opacity: 0.3,fixed: true});	  
	}
	function refreshWindow(){
		window.location.reload();
	}
	function openDialog(uri,title,width,height){
	     $.artDialog.open(uri , {title: title,width:width,height:height, lock: true,opacity: 0.3,fixed: true});	  
		 return ;
	}
	function modLastTime(transfer_id,last_time_old)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/mod_transfer_last_time.html?transfer_apply_id='+transfer_id+'&last_time_old='+last_time_old;
		$.artDialog.open(uri,{title: "转账单:["+transfer_id+"]",width:'400px',height:'250px', lock: true,opacity: 0.3,fixed: true});
	}
	
	//在线预览图片
	function showPictrueOnline(currentName){
	    var obj = {
			current_name : currentName ,
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_financial")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}
	
//-->
</script>
</head>

<body onLoad="onLoadInitZebraTable()">
<form id="deleteFundsTransferForm" name="deleteFundsTransferForm" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/DeleteFundsTransferPrecessAction.action">
<input type="hidden" id="reason" name="reason"/>
<input type="hidden" id="applyId" name="applyId" />
<input type="hidden" id="transferId" name="transferId"  />
<input type="hidden" id="userName" name="userName" value="<%=adminLoggerBean.getEmploye_name()%>"/>
<input type="hidden" id="userName_id" name="userName" value="<%=adminLoggerBean.getAdid()%>"/>
</form>			
 
<table id="outer_table" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	
	<div class="demo" >

	<div id="tabs">
<ul>
		<li><a href="#usual_tools">常用工具</a></li>
		<li><a href="#advan_search">高级搜索</a></li>		 
</ul>

		<div id="usual_tools">
		<form method="post"  id="postValueForm"   >
		<input type="hidden" id="cmd" name="cmd" value="search_l"/>
		<input type="hidden" id="search_mode" name="search_mode"/>
          <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
            <tr>
            	 <td width="18%" align="left" style="padding-top:3px;padding-left: 5px" nowrap="nowrap">
	            	 <div id="easy_search_father">
								<div id="easy_search" style="margin-top:-2px;"><a href="javascript:query()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
					  </div>
	            		<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
									 <input name="transfer_id" id="transfer_id" value="<%=transferId%>"  type="text" class="search_input" style="font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)query()"/>
									</div>
								</td>
								<td width="67px">&nbsp;
								</td>
								<td width="67">
									<input type="button" class="button_long_search" id="btnSubmit1" name="btnSubmit1" value="多笔转账" onclick="showUpdateVoucherMore()"/>
								</td>
							</tr>
						</table>
						<script>eso_button_even();</script>
				</td>	        
             
            </tr>
          </table>
		  </form> 
		</div>

		
	  <%
TDate tDate = new TDate();
tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

String input_st_date,input_en_date;
if ( st.equals("") )
{	
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}
else
{
	input_st_date = st;
}

if ( en.equals("") )
{	
	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
}else
{
	input_en_date = en;
}

%>
 
 <div id="advan_search">

	<form name="filterForm" action="funds_transfer_list.html" method="post">
	<input type="hidden" id="cmd" name="cmd" value="filter"/>
	   <table width="100%" height="30" border="0" cellpadding="1" cellspacing="0">
		  
		    <tr>
		      <td width="95%" height="30" >
			      <div style=" width:100%; float:left">		      
		     	   开始
		        <input name="st" type="text" id="st" size="9" value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
		        &nbsp;       
				  结束
		        <input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
		        &nbsp;
		        状态：
		      <select name="status" id="status" style="border:1px #CCCCCC solid">
		        <option value="-1">全部</option>
				<option value="<%=TransferAccountsKey.NOT_TRANSFER %>"><%=transferAccountsKey.getTransferAccountsKeyNameById(TransferAccountsKey.NOT_TRANSFER+"")%></option>
		        <option value="<%=TransferAccountsKey.FINISH_TRANSFER %>"><%=transferAccountsKey.getTransferAccountsKeyNameById(TransferAccountsKey.FINISH_TRANSFER+"") %></option>
		      </select>
		
		       <select name="selectSortTime" style="border:1px #CCCCCC solid" >	   
					<option value="create_time"  >创建时间排序排序</option>
					<option value="voucher_createTime" <%if(selectSortTime.equals("voucher_createTime")){%> selected="selected" <%} %>  >完成时间排序</option>
				    <option value="last_time" <%if(selectSortTime.equals("last_time")){%> selected="selected" <%} %> >最迟转款时间排序</option>
						 </select>
		       <input type="submit" id="seachByCondition" name="seachByCondition" class="button_long_refresh" value="过    滤" />
			</td>
		      </tr>
		</table>
	</form>
 </div>

	</div>	
	</div>	
	</td>
  </tr>
</table>
<form id="timeForm" method="post" action="">
    <input type="hidden" value="" id="sortCmd" name="cmd" />
</form>
<br/>
<script>

	$("#st").date_input();
    $("#en").date_input();
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});

	function openDialog(uri,title,width,height){
	     $.artDialog.open(uri , {title: title,width:width,height:height, lock: true,opacity: 0.3,fixed: true});	  
		 return ;
	  }
</script>
<div id="applyMoneyList">
<input type="hidden" name="test_time" id="test_time" />
  <table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"   class="zebraTable" style="margin-left:3px;margin-top:5px;" isNeed="true" isBottom="true" >
     <tr>
       <th width="3%"  class="right-title"  style="text-align: center;" nowrap="nowrap">&nbsp;</th>
       <th width="7%"  class="right-title"  style="text-align: center;" nowrap="nowrap">申请单号资金单号【关联单据】</th>
       <th width="18%"  class="right-title"  style="text-align: center;" nowrap="nowrap">付款信息</th> 
       <th width="25%"  class="right-title"  style="text-align: center;" nowrap="nowrap">收款信息</th>       
       <th width="19%"  class="right-title"  style="text-align: center;" nowrap="nowrap">跟进</th>
 
       <th width="10%" class="right-title" style="text-align: center;" nowrap="nowrap">凭证</th>
     </tr>
     <%
     if(applyTransfer!=null){
	     for(int i=0;i<applyTransfer.length;i++)
	     {
	    	 int categoryId = 0;
	    	 int apply_money_status = 0;
      %>
     <tr>
        <td align="center" valign="middle">
         <%
          if(applyTransfer[i].getString("status").equals("0"))
          {
         %>
        	<input type="checkbox" name="transfer_ids" id="transfer_ids" value="<%=applyTransfer[i].get("transfer_id",0l) %>">
         <%
          }else{
        	  out.print("&nbsp;");
          }
         %>
        </td>
        <td>
           <fieldset class="set" style="padding-bottom:4px;border:2px solid #993300; width:97%">
					<legend>
					<span class="title">
						 <span style="font-weight:bold;">
						    W<%=applyTransfer[i].get("transfer_id",0l) %>&nbsp;<%=transferAccountsKey.getTransferAccountsKeyNameById(applyTransfer[i].getString("status")) %>
						 </span>
					</span>
					</legend>
					<p style="padding-left:4%">
					   <a href="javascript:void(0)" onClick="goApplyFunds('<%=applyTransfer[i].get("apply_money_id",0l) %>')">
						     F<%=applyTransfer[i].get("apply_money_id",0l) %>
					   </a>
					</p>
					<p style="padding-left:20.5%;">
					    <span class="alert-text stateName">
						创建人：
						</span>
						<%=applyTransfer[i].getString("creater") %>									
					</p>
					<p style="padding-left:14%;">
						<span class="alert-text stateName">
						创建时间：
						</span>
                         <%= applyTransfer[i].getString("create_time").length() > 10 ? applyTransfer[i].getString("create_time").substring(0,10) : applyTransfer[i].getString("create_time")%> 		      	
					</p>
					<p style="padding-left:1%;">
						<span class="alert-text stateName">
						最迟转款时间：
						</span>
					   <%=applyTransfer[i].getString("last_time").length() > 10 ? applyTransfer[i].getString("last_time").substring(0,10):applyTransfer[i].getString("last_time")%>
						<img width="14" height="14" border="0" src="../imgs/application_edit.png" onclick="modLastTime(<%=applyTransfer[i].get("transfer_id",0L) %>,'<%=applyTransfer[i].getString("last_time").length() > 10 ? applyTransfer[i].getString("last_time").substring(0,10):applyTransfer[i].getString("last_time")%>')">		
<%--						<img width="14" height="14" border="0" src="../imgs/button_short_short_mod.gif">				    --%>
					</p>
					<p style="padding-left:27%;">
						<span class="alert-text stateName">项目：</span>
        	<%
        		DBRow[] applyMoney = applyMoneyMgrZZZ.getApplyMoneyById(applyTransfer[i].getString("apply_money_id"));
				        		out.println(applyMoney[0].getString("categoryName")+"<br>");
        		DBRow[] associationType = applyMoneyMgrLL.getMoneyAssociationTypeAll();
        		categoryId = applyMoney[0].get("categoryId",0);
        		apply_money_status = applyMoney[0].get("status",0);
				      		%>			            
					</p>							
					<p style="padding-left:27%;">
					    <span class="alert-text stateName">
						    关联：
						</span>
						<%
        		for(int j=0;j<applyMoney.length;j++) {
            		long association_type_id = applyMoney[j].get("association_type_id",0l);
            		long association_id = applyMoney[j].get("association_id",0l);
	            	if(association_type_id==2)//固定资产
					            		out.println(association_id);
	            	else if(association_type_id==3)//样品
						        		out.println(association_id);
	            	else if(association_type_id==4)//采购单
						        		out.println("<a target='_blank' href='"+ConfigBean.getStringValue("systenFolder")+"administrator/purchase/purchase_detail.html?purchase_id="+association_id+"'>P"+association_id+"</a>");
	            	else if(association_type_id==5) {//交货单
					            		out.println("<a target='_blank' href='"+ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_order_detail.html?transport_id="+association_id+"'>T"+association_id+"</a>");
	            	 }
	            	else if(association_type_id==6)//转运单
					            		out.println("<a target='_blank' href='"+ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_order_detail.html?transport_id="+association_id+"'>T"+association_id+"</a>");
	            	else if(association_type_id==7)//交货单
	            		out.println("<a target='_blank' href='"+ConfigBean.getStringValue("systenFolder")+"administrator/repair/repair_order_detail.html?repair_order_id="+association_id+"'>R"+association_id+"</a>");
        		}
        	%>
					</p>			
			</fieldset>
        </td>
        <td>   
             <fieldset class="set" style="padding-bottom:4px;border:2px solid #993300; width:97%">          
					<legend>
					<span class="title">
						 <span style="font-weight:bold;">
				                               付款人：<%=applyTransfer[i].getString("payer") %>
						 </span>
					</span>
					</legend>
                    <p style="padding-left:6%;">
					    <span class="alert-text stateName">
						确认人:
						</span>
						<%=applyTransfer[i].getString("operating_personnel") %>							
					</p>
					<p style="padding-left:1%;">
					    <span class="alert-text stateName">
						完成时间:
						</span>
                         <%=applyTransfer[i].getString("voucher_createTime").length() > 10 ? applyTransfer[i].getString("voucher_createTime").substring(0,10) :  applyTransfer[i].getString("voucher_createTime")%> 		      	
					</p>
					<p style="padding-left:1%;">
					    <span class="alert-text stateName">
						付款账号:
						</span>
					    <%=StringUtil.ascii2Html(applyTransfer[i].getString("payment_information")) %>					
					</p>	
					<p style="padding-left:11.5%;">
					    <span class="alert-text stateName">
						金额:
						</span>
						<%if(applyTransfer[i].getString("currency").equals("USD")){ %>
						   <%=applyTransfer[i].get("amount",0d)%><%=applyTransfer[i].getString("currency")%>/<%=applyTransfer[i].get("standard_money",0d)%>RMB
						<%}else{%>
						   <%=applyTransfer[i].get("amount",0d)%><%=applyTransfer[i].getString("currency")%>	
						<%}%>					      			
					</p>
															
			 </fieldset>
        </td>   
        <td align="left">
                                收款人:<%=applyTransfer[i].getString("receiver") %><br>
            <%
            	if(!applyTransfer[i].getString("receiver_account").equals("")&&applyTransfer[i].getString("receiver_account").length()>0){
            		out.print("收款账号:"+applyTransfer[i].getString("receiver_account")+"<br>");
            	}
            %>
        		<!-- 查询account表中,如果有了数据那么就是要显示Account表中的数据 -->	
	  	<!-- 如果没有那么就是要显示字段里面的东西 -->
	  	<!-- 兼容以前 -->
	  	<%
	  		DBRow[] accountInfos = accountMgrZr.getAccountByAccountIdAndAccountWithType(applyTransfer[i].get("transfer_id",0l),AccountKey.APPLY_TRANSFER);
	  		if(accountInfos != null && accountInfos.length > 0){
	  			for(DBRow tempAccount :  accountInfos){
	  	%>
	  					 
	  					<p>收款人户名:<%=tempAccount.getString("account_name") %></p>
	  					<p>收款人银行:<%=tempAccount.getString("account_blank") %></p>
	  					<p>收款人账号:<%=tempAccount.getString("account_number") %></p>
	  					<%if(tempAccount.getString("account_phone").length() > 0 ){ %>
	  						<p>收款人电话:<%=tempAccount.getString("account_phone") %></p>	
	  					<%} %>
	  					 <%if(tempAccount.getString("account_swift").length() > 0 ){ %>
	  						<p>SWIFT Code:<%=tempAccount.getString("account_swift") %></p>	
	  					<%} %> 
	  					<%if(tempAccount.getString("account_address").length() > 0 ){ %>
	  						<p>收款人地址:<%=tempAccount.getString("account_address") %></p>	
	  					<%} %>
	  					<%if(tempAccount.getString("account_blank_address").length() > 0 ){ %>
	  						<p>开户行地址:<%=tempAccount.getString("account_blank_address") %></p>	
	  					<%} %>
	  					<br />
	  					
	  				<%
	  			}
	  		}else{
	  			out.print("收款账号:"+applyTransfer[i].getString("receiver_account")+"<br>");
	  			String receiver_information = applyTransfer[i].getString("receiver_information");
	  			receiver_information = receiver_information.replaceAll("\n","<br>");
           		out.print(receiver_information);
	  		}
           %>
        </td>
        <td  align="left">
             <% DBRow[] transportLogs = transportMgrZwb.getTransferlogs(applyTransfer[i].get("transfer_id",0l));%>
             <% for(int k=0;k<transportLogs.length;k++){ %>
                <div>
                    <span style="font-weight: bold;"><%=k%><%=transportLogs[k].getString("user_name")%></span>
                    <span style=" color:#F90; font-size:12px;"><%=transportLogs[k].getString("context")%></span>
     
                </div>
                <div style=" color:#999; font-size:12px; border-bottom: #999 1px dashed">
                    <%= tDate.getFormateTime(transportLogs[k].getString("createDate")) %>
                </div>
                 <%if(k>1){ %>
                    <a href="javascript:openDialog('funds_transfer_logs.html?transferId=<%=applyTransfer[i].get("transfer_id",0l) %>','全部','700px','400px')" >更多.......</a>
                    <% break; 
         }
             } %>
            
        </td>
        <td  align="center">
	       <%if(applyTransfer[i].getString("status").equals("0")){%>
         
           <%}else if(applyTransfer[i].getString("status").equals("2")){%>
           	   <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 		    <%if(StringUtil.isPictureFile(applyTransfer[i].getString("voucher"))){ %>
		 		    
				 			<input type="button" class="short-button" onclick="showPictrueOnline('<%=applyTransfer[i].getString("voucher") %>');" value="查看凭证"/>
			 		
			 		 <%}else{ %>
                         <input type="button" class="short-button" onClick="$.artDialog.open('../../upload/financial_management/<%=applyTransfer[i].getString("voucher") %>', {title: '查看凭证',width:'800px',height:'1024px', lock: true,opacity: 0.3});" value="查看凭证" />
           			<%} %>
           <%}else{%>
           <%}%>
        </td>
     </tr>
     <tr class="split" >
        <td colspan="6" align="right">
           <%if(applyTransfer[i].getString("status").equals("0")){%>
               <tst:authentication bindAction="com.cwc.app.api.zzz.ApplyTransferMgrZZZ.deleteApplyTransferAndHandleApplyMoneyState">
               	<input type="button" class="short-short-button-cancel" value="删除" onClick="openDialog('delete_funds_transfer_message.html?transferId=<%=applyTransfer[i].get("transfer_id",0l) %>&applyMoneyId=<%=applyTransfer[i].get("apply_money_id",0l) %>','删除原因','370px','190px')" />
               </tst:authentication>
               <tst:authentication bindAction="com.cwc.app.api.zzz.ApplyTransferMgrZZZ.confirmTransferUploadVoucher">
               	<input type="button" class="short-button" onClick="$.artDialog.open('uploadVoucher.html?aid=<%=applyTransfer[i].get("transfer_id",0l) %>&applyMoneyId=<%=applyTransfer[i].get("apply_money_id",0l) %>',{title: '上传转账凭证',width:'750px',height:'350px', lock: true,opacity: 0.3});" value="确认已转账" />
               </tst:authentication>
		       
           <%}else if(applyTransfer[i].getString("status").equals("2")){%>
           <%}else{%>
           <%}%>
            <input type="button" class="short-short-button-mod" onclick="followup(<%=applyTransfer[i].get("transfer_id",0l) %>,<%=applyTransfer[i].get("status",0l)%>,<%=applyMoney[0].get("association_type_id",0l) %>,<%=applyMoney[0].get("association_id",0l) %>,<%=applyTransfer[i].get("apply_money_id",0l) %>,<%=categoryId %>,<%=apply_money_status %>)" value="跟进"/>
        </td>
     </tr>
     <%      	
   	 }}
      %>
  </table>
  <br>
  <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" id="dataForm" action="funds_transfer_list.html">
    <input type="hidden" name="p">
    <input type="hidden" id="cmd" name="cmd" value="<%=StringUtil.getString(request,"cmd") %>"/>
    <input type="hidden" id="transfer_id" name="transfer_id" value="<%=StringUtil.getString(request,"transfer_id","") %>" />
    <input name="st" type="hidden" id="st" size="9" value="<%=StringUtil.getString(request,"st")%>"/>
    <input name="en" type="hidden" id="en" size="9" value="<%=StringUtil.getString(request,"en")%>"/>
    <input type="hidden" id="status" name="status" value="<%=StringUtil.getInt(request,"status",-1) %>"/>
    <input type="hidden" id="selectSortTime" name="selectSortTime" value="<%=StringUtil.getString(request,"selectSortTime") %>">
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
</div>	
</body>
</html>