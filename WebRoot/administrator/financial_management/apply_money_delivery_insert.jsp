<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey,com.cwc.app.api.ll.Utils,java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	DBRow row =Utils.getDBRowByName("apply_money", request);
	float amount = row.getValue("amount")==null?StringUtil.getFloat(request,"amount",0f):row.get("amount",0f);
	String payee = row.getValue("payee")==null?"":row.getValue("payee").toString();
	String payment_information = row.getValue("payment_information")==null?"":row.getValue("payment_information").toString();
	String remark = row.getValue("remark")==null?"":row.getValue("remark").toString();
	String association_id = StringUtil.getString(request,"association_id").equals("")?row.getValue("association_id").toString():StringUtil.getString(request,"association_id");
	String apply_id = row.getValue("apply_id")==null?"0":row.getValue("apply_id").toString();
	String delivery_order_id = StringUtil.getString(request,"delivery_order_id");

	DBRow[] accounts = applyMoneyMgrLL.getAllByReadView("accountReadViewLL");
	DBRow[] accountCategorys = applyMoneyMgrLL.getAllByTable("accountCategoryBeanLL");
	DBRow[] adminGroups = applyMoneyMgrLL.getAllByTable("adminGroupBeanLL");
	DBRow[] productLineDefines = applyMoneyMgrLL.getAllByTable("productLineDefineBeanLL");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script>
	var inserted = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"inserted").equals("1")) {
	%>
			inserted = true;
	<%
		}
	%>
	function init() {
		if(inserted) {
			//apply_money_form.insert.style.display = "none";
			closeWindow();
		}
	}

	$(document).ready(function(){
		getLevelSelect(0, nameArray, widthArray, centerAccounts, null,vname,1);
		getLevelSelect(1, nameArray, widthArray, centerAccounts, $("#center_account_type_id"),vname,0);
		getLevelSelect(2, nameArray, widthArray, centerAccounts, $("#center_account_type1_id"),vname,100019);
		getLevelSelect(0, nameArray1, widthArray1, pays, null,vname1);
		$('#subject').val(<%=StringUtil.getString(request,"subject")%>);
		init();
	});
	
<%//level_id,value,name
	String str = "var pays = new Array(";
	str += "new Array('01',0,'任意')";
	str += ",new Array('01.0',0,'任意')";
	str += ",new Array('01.0.0',0,'任意')";
	str += ",new Array('02',1,'公司账户')";
	str += ",new Array('02.0',0,'公司账户')";
	str += ",new Array('02.0.0',0,'选择部门')";
	for(int i=0;i<accounts.length;i++) {
		DBRow account = accounts[i];
		if(account.get("account_parent_id",0)==0 && account.get("account_category_id",0)==1)//部门列表
			str += ",new Array('02.0."+(i+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
	}	
	str += ",new Array('03',2,'职员账户')";
	str += ",new Array('03.0',0,'选择部门')";
	str += ",new Array('03.0.0',0,'选择职员')";
	for(int i=0;i<adminGroups.length;i++) {//部门
		DBRow adminGroup = adminGroups[i];
		str += ",new Array('03."+(i+1)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
		str += ",new Array('03."+(i+1)+".0',0,'选择职员')";
		for(int ii=0;ii<accounts.length;ii++) {
			DBRow account = accounts[ii];
			if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
				str += ",new Array('03."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
		}
	} 
	str += ",new Array('04',3,'供应商')";
	str += ",new Array('04.0',0,'选择生产线')";
	str += ",new Array('04.0.0',0,'选择供应商')";
	for(int i=0;i<productLineDefines.length;i++) {
		DBRow productLineDefine = productLineDefines[i];
		str += ",new Array('04."+(i+1)+"',"+productLineDefine.get("id",0)+",'"+productLineDefine.getString("name")+"')";
		str += ",new Array('04."+(i+1)+".0',0,'选择供应商')";
		for(int ii=0;ii<accounts.length;ii++) {
			DBRow account = accounts[ii];
			if(account.get("account_parent_id",0)==productLineDefine.get("id",0) && account.get("account_category_id",0)==3)//供应商列表
				str += ",new Array('04."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
		}
	}

	str+= ");";
	out.println(str);
%>

<%//level_id,value,name
str = "var centerAccounts = new Array(";
str += "new Array('01',0,'任意')";
str += ",new Array('01.0',0,'任意')";
str += ",new Array('01.0.0',0,'任意')";
str += ",new Array('02',1,'公司账户')";
str += ",new Array('02.0',0,'公司账户')";
str += ",new Array('02.0.0',0,'选择部门')";
for(int i=0;i<accounts.length;i++) {
	DBRow account = accounts[i];
	if(account.get("account_parent_id",0)==0 && account.get("account_category_id",0)==1)//部门列表
		str += ",new Array('02.0."+(i+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
}	
str += ",new Array('03',2,'职员账户')";
str += ",new Array('03.0',0,'选择部门')";
str += ",new Array('03.0.0',0,'选择职员')";
for(int i=0;i<adminGroups.length;i++) {//部门
	DBRow adminGroup = adminGroups[i];
	str += ",new Array('03."+(i+1)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
	str += ",new Array('03."+(i+1)+".0',0,'选择职员')";
	for(int ii=0;ii<accounts.length;ii++) {
		DBRow account = accounts[ii];
		if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
			str += ",new Array('03."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
	}
}
str+= ");";
out.println(str);
%>

var nameArray = new Array('center_account_type_id','center_account_type1_id','center_account_id');
var widthArray = new Array(120,120,200);
var nameArray1 = new Array('payee_type_id','payee_type1_id','payee_id');
var widthArray1 = new Array(120,120,200);
var vname = new Array('nameArray','widthArray','centerAccounts','vname','apply_money');
var vname1 = new Array('nameArray1','widthArray1','pays','vname1','apply_money');

function getLevelSelect(level,nameArray, widthArray, selectArray,o,vnames,value) {
		if(level<nameArray.length) {
			var name = nameArray[level];
			var width = widthArray[level];
			var levelId = o==null?"":$("option:selected",o).attr('levelId');
			var onchangeStr = "";
			if(level==nameArray.length-1)
				onchangeStr = "exec(this)";
			else
				onchangeStr = "getLevelSelect("+(level+1)+","+vnames[0]+","+vnames[1]+","+vnames[2]+",this,"+vnames[3]+")";

			var selectHtml = "<select name='"+vnames[4]+"."+name+"' id='"+name+"' style='width:"+width+"px;' onchange="+onchangeStr+"> ";
			for(var i=0;i<selectArray.length;i++) {
				if(levelId!="") {
					var levelIdChange = selectArray[i][0].replace(levelId+".");
					var levelIds = levelIdChange.split(".");	
					
					if(levelIdChange!=selectArray[i][0]&&levelIds.length==1){
						//alert(levelId+",value="+selectArray[i][0]+",change='"+levelIdChange+"',"+levelIds.length);
						selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
					}
				}
				else {
					var levelIdChange = selectArray[i][0];
					var levelIds = levelIdChange.split(".");
					if(levelIds.length==1){
						//alert(levelId+","+selectArray[i][0]+levelId1);
						selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
					}
				}
				
			}
			selectHtml += "</select>";
			$('#'+name+'_div').html('');
			$('#'+name+'_div').append(selectHtml);
			$('#'+name).val(value);
		    
			$("#"+name).chosen();
			$("#"+name).chosen({no_results_text: "没有该项!"});
	
			getLevelSelect(level+1,nameArray,widthArray,selectArray,$('#'+name),vnames);

			//修改输入框,破坏结构
			if($(o).attr("id")=="payee_type_id"||$(o).attr("id")=="payee_type1_id")
				$('#payee').val('');
		}
}

function exec(o) {
	if($(o).attr("id")=="payee_id") {
		if($("option:selected",o).val()!=0)
				$('#payee').val($("option:selected",o).attr('displayName'));
		else
				$('#payee').val('');
	}
}

function setSubject(o) {
	if($(o).val()==4) {
		$("#tr1").attr("style","display:none");
	}else {
		$("#tr1").attr("style","display:none");
	}
}
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="apply_money_form" id="apply_money_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/applyMoneyDeliveryInsertAction.action" method="post">
<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_money_delivery_insert.html?association_id=<%=association_id%>&inserted=1"/>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">资金申请单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
	        <tr height="29">
	          <td align="right" class="STYLE2">交货单号:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" bgcolor="#eeeeee">
	          	<%=delivery_order_id%>
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">交货批次:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" bgcolor="#eeeeee">
	          	<%=association_id%>
	          </td>
	        </tr>
	        <tr> 
		      <td align="right" class="STYLE2">费用项目:</td>
		      <td>&nbsp;</td>
		      <td>
		      	运费
		      	<input type="hidden" name="subject" id="subject" value="1">
		      	<!--  
				<select name="subject" id="subject" onchange="setSubject(this)">
					<option value="1">运费</option>
					<option value="2">报关</option>
					<option value="3">清关</option>
					<option value="4">退税</option>
				</select>
				-->
			  </td>
		    </tr>
		    <tr id="tr1" name="tr1" style="display:none"> 
		      <td align="right" class="STYLE2">成本中心:</td>
		      <td>&nbsp;</td>
		      <td>
				<div id='center_account_type_id_div' name='center_account_type_id_div' style="float:left;">	      	
				</div>
				<div id='center_account_type1_id_div' name='center_account_type1_id_div' style="float:left;">
				</div>
				<div id='center_account_id_div' name='center_account_id_div' style="float:left;">
		        </div>
			  </td>
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2">币种:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<select id="currency" name="apply_money.currency">
		    		<option value="RMB" <%=row.getString("currency").equals("RMB")?"selected":""%>>RMB</option>
		    		<option value="USD" <%=row.getString("currency").equals("USD")?"selected":""%>>USD</option>
		    	</select>
		      </td>
		    </tr>
		  	<tr> 
		      <td align="right" class="STYLE2">金额:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<%
		      		float apply_money = batchMgrLL.getApplyMoneyFreightCost(5,Long.parseLong(delivery_order_id));
		      	%>
		      	<input name="apply_money.amount" id="amount" type="text" size="15" value="<%=(amount-apply_money)%>">
		      </td>
		    </tr>
				  
		    <tr>
		      <td align="right" class="STYLE2">收款人:</td>
		      <td>&nbsp;</td>
		     
		      <td>
		      	<div style="float:left;" id="payee_type_id_div" name="payee_type_id_div">		      	
				</div>
				<div id='payee_type1_id_div' name='payee_type1_id_div' style="float:left;">
				</div>
				<div id='payee_id_div' name='payee_id_div' style="float:left;">
		        </div>
		        <br/><br/>
		        <input name="apply_money.payee" id="payee" value="<%=payee%>"/>
	        </td>
		   
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2">收款信息:</td>
		      <td>&nbsp;</td>
		      <td>		      		    		     
		      	<textarea rows="4" cols="40" name="apply_money.payment_information" id="payment_information" ><%=payment_information%></textarea>
		      </td>
		    </tr>
		     		    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="apply_money.remark" rows="4" cols="40"><%=remark%></textarea></td>
		    </tr>
		</table>
		
		<input id="apply_money.apply_id" name="apply_money.apply_id" id="apply_id" type="hidden" value="<%=apply_id%>"/>
		<input id="apply_money.association_id" name="apply_money.association_id" id="association_id" type="hidden" value="<%=delivery_order_id%>"/>
		<input name="apply_money.creater_id" id="creater_id" type="hidden" value="<%=adminLoggerBean.getAdid() %>"/>
		<input name="apply_money.creater" id="creater" type="hidden" value="<%=adminLoggerBean.getEmploye_name() %>"/>
		<input name="apply_money.create_time" id="create_time" type="hidden" value="<%=(new SimpleDateFormat("yyyy-MM-dd")).format(new java.util.Date()) %>"/>
		<input type="hidden" name="apply_money.association_type_id" id="association_type_id" value="5" />
		<input type="hidden" name="apply_money.types" id="types" value="10015" />
		<input type="hidden" name="inserted" id="inserted" value="1" />
	</fieldset>	
	</td>
  </tr>

</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="insert" type="button" class="normal-green-long" onclick="submitApply()" value="下一步" >
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
  </form>
<script type="text/javascript">
<!--
	function submitApply()
	{
		if(!checkPayAcount()) {
			return;
		}
		
		 if($('#amount').val().trim()==""||$('#amount').val()==null)
		 {
		     $('#amount').focus();
		     alert("金额不能为空！");
		 }
		 else if(isNaN($('#amount').val().trim()))
	 	 {
		    $('#amount').focus();
		     alert("金额必须为数字！");
	 	 }
		 else if($('#amount').val().trim()<=0)
		 {
		 	 $('#amount').focus();
		     alert("金额必须大于零！");
		 }
		 else if($('#payee').val().trim()==""||$('#payee').val()==null)
		 {
		 	 $('#payee').focus();
		 	 alert("请填写收款人！");
		 }
		 else if($("#payment_information").val().trim()==""||$("#payment_information").val()==null)
		 {
		 	 $("#payment_information").focus();
		 	 alert("请填写付款信息！");
		 } 
		 else if($("#payment_information").val().trim().length>600)
		 {
		 	 $("#payment_information").focus();
		 	 alert("付款信息不能超过600字符！");
		 }			 
		 else if($("#remark").val().trim().length>=200)
		 {
		     $("#remark").focus();
		     alert("备注字数不能超过200！");
		 }
		 else
		 {	
			document.apply_money_form.submit();
		 }	
	}

	function checkPayAcount() {
		var amount = <%=amount%>;
		var apply_money = <%=apply_money%>;
		var currency = $("#currency").val();
		var currency_rate = 1;
		if(currency=="USD")
			currency_rate = <%=systemConfig.getStringConfigValue("USD")%>;

		if($("#amount").val()*currency_rate > (amount - apply_money)) {
			alert('申请金额不能大于实际运费');
			return false;
		}
		
		return true;
	}
	
	function closeWindow(){
		$.artDialog.close();
	}
//-->
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 
	$(".chzn-select").chosen(); 
	$(".chzn-select1").chosen(); 
	$(".chzn-select2").chosen(); 
</script>
</body>
</html>

