<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.QualityInspectionKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
 <jsp:useBean id="qualityInspectionKey" class="com.cwc.app.key.QualityInspectionKey"></jsp:useBean>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.ArrayList" %>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>运单日志跟进</title>

<!--  基本样式和javascript -->
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
 
 
<style type="text/css">
*{margin:0px;padding:0px;}
div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .title{ background: url("../js/popmenu/pro_title.jpg") no-repeat scroll left center transparent;color: #FFFFFF;display: table;font-size: 14px; height: 27px;padding-left: 5px; padding-top: 3px;width: 356px;line-height:27px;}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:80px;text-align:center;border:1px solid silver;margin:0px auto;width:300px;height:30px;background:white;line-height:30px;font-size:14px;}	
	#ui-datepicker-div{z-index:9999;display:none;}
</style>
<style type="text/css">
 	select.key{display:none;}
</style>
<%
 	long apply_money_id		= StringUtil.getLong(request, "apply_money_id");
	int apply_money_status	= StringUtil.getInt(request, "apply_money_status");
	int association_type_id	= StringUtil.getInt(request, "association_type_id");
	int categoryId			= StringUtil.getInt(request, "categoryId");
	long association_id		= StringUtil.getLong(request, "association_id");
	TDate tdate				= new TDate();
	String handleFundsLogs	= ConfigBean.getStringValue("systenFolder") + "action/administrator/financial_management/AddFundsApplyLogAction.action";
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	
 %>
<script type="text/javascript">
 $(function(){
	 	$('#eta').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
			onSelect:function(dateText, inst){
					setContentHtml();
			}
		});
	 	if(<%=FundStatusKey.FINISH_PAYMENT %> != '<%=apply_money_status %>'){
	 		setContentHtml();
	 	}else{
			$("#notfinish_span").attr("style","display:none");
		 }
});
function setContentHtml(){
	var fundStatusHTML = $("#fundsStateStr").html();
	if(fundStatusHTML.indexOf("font") != -1){
	  fundStatusHTML = $(fundStatusHTML).text();
	}
	var html = "";
	html = "[跟进资金]["+fundStatusHTML+"]预计"+$("#eta").val()+"完成:";
	var contentNode = $("#context");
	var contentNodeValue = contentNode.val();
	 
	if(contentNodeValue.indexOf("完成:") != -1){
		var index = contentNodeValue.indexOf("完成:")
		html += contentNodeValue.substr(index +3);
		$("#context").val(html);
	}else{
		$("#context").val(html + contentNodeValue);
	}
}
function submitForm(){
	if(!validate()){return ;}
	ajaxSubmit();
}
function ajaxSubmit(){
	$.ajax({
		url:'<%= handleFundsLogs %>',
		data:$("#myform").serialize(),
		success:function(data){
			$.artDialog && $.artDialog.close();
			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
		},
		error:function(){
			showMessage("系统错误","error");
		}
	})
}
function validate(){
	return true ;
}
function closeDialogNotRefresh(){
	$.artDialog && $.artDialog.close();
}
</script>
</head>
<body>
	 <form id="myform" method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/financial_management/AddFundsApplyLogAction.action" %>'>
		 <input type="hidden" name="backurl" id="backurl" value="administrator/financial_management/funds_apply_log_add.html"/>
		 <input type="hidden" name="apply_money_id" value="<%=apply_money_id %>"/> 
		 <input type="hidden" name="apply_money_status" value='<%=apply_money_status %>'/>
		 <input type="hidden" name="association_id" value='<%=association_id %>'/>
		 <input type="hidden" name="categoryId" value='<%=categoryId %>'/>
		 <input type="hidden" name="association_type_id" value='<%=association_type_id %>'/>
		 <table>
		 	<tr>
		 		<td style="text-align:right;">
		 			当前状态:
		 		</td>
		 		<td>
		 			<%
		 				String fundsStateString = "";
					 	List imageList = applyMoneyMgrLL.getImageList(String.valueOf(apply_money_id),"1");
					 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(String.valueOf(apply_money_status)) && imageList.size() < 1)
					 	{
					 		fundsStateString = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");	
					 	}else{
					 		fundsStateString = fundStatusKey.getFundStatusKeyNameById(String.valueOf(apply_money_status));
					 	}
					 %>
		 			<span id="fundsStateStr"><%=fundsStateString %></span>
		 			<span id="notfinish_span" style=''>
		 		 		预计本阶段完成时间:<input type="text" id="eta" name="eta" value="<%= tdate.getYYMMDDOfNow() %>"/></span>
		 			</span>
		 		</td>
		 	</tr>
		 	<tr>
		 		<td style="text-align:right;">备注</td>
		 		<td>
		 			<textarea style="width:350px;height:185px;" id="context" name="context"></textarea>
		 		</td>
		 	</tr>
		 </table>
 	 
 	  </form>		
	   <div class="buttonDiv" style="margin-top:5px;"> 
	 	 	<input type="button" id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil"  onclick="submitForm()" value="提交"> 
			<button id="jqi_state0_button取消" value="n" class="jqidefaultbutton specl" name="jqi_state0_button取消" onclick="closeDialogNotRefresh();">取消</button>
	  </div>
	  		
 	  		 
 	<script type="text/javascript">
 	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 		
 	</script> 
	 
</body>
</html>