<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey,com.cwc.app.api.ll.Utils,java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.AccountKey"%>
<%@ include file="../../include.jsp"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	DBRow row =Utils.getDBRowByName("apply_money", request);
	float amount = row.getValue("amount")==null?StringUtil.getFloat(request,"amount",0f):row.get("amount",0f);
	String payee = row.getValue("payee")==null?"":row.getValue("payee").toString();
	String payment_information = row.getValue("payment_information")==null?"":row.getValue("payment_information").toString();
	String remark = row.getValue("remark")==null?"":row.getValue("remark").toString();
	String apply_id = row.getValue("apply_id")==null?"0":row.getValue("apply_id").toString();
	String associationId = StringUtil.getString(request,"associationId");
	long purchase_id	= StringUtil.getLong(request, "purchase_id");
	//如果采购单的Id为0就是转运单，否则为交货单
	int association_type_id = (0 == purchase_id?6:5);
	DBRow[] accounts = applyMoneyMgrLL.getAllByReadView("accountReadViewLL");
	DBRow[] accountCategorys = applyMoneyMgrLL.getAllByTable("accountCategoryBeanLL");
	DBRow[] adminGroups = applyMoneyMgrLL.getAllByTable("adminGroupBeanLL");
	DBRow[] productLineDefines = applyMoneyMgrLL.getAllByTable("productLineDefineBeanLL");
	double applyTransferFreightTotal = StringUtil.getDouble(request, "applyTransferFreightTotal");
	double maxMoney				= MoneyUtil.round((amount - applyTransferFreightTotal),2) ;
    double currencyEx			= Double.parseDouble(systemConfig.getStringConfigValue("USD")) ;
    double canApplyMaxRMBMoney	= MoneyUtil.round(maxMoney + maxMoney * 0.01 * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation")),2);
    double canApplyMaxUSDMoney	= MoneyUtil.round((maxMoney + maxMoney * 0.01 * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))) /currencyEx ,2);
    String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
	String getAccountInfoAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/account/GetAccountByAction.action";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 时间控件 -->
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
table.receiver td {border:1px solid silver;}
table.receiver td input.noborder{border-bottom:1px solid silver;width:200px;}
table.receiver td.left{width:120px;text-align:right;}
table.next{margin-top:5px;}
table.receiver{border-collapse:collapse ;}
-->
</style>
<script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script>
	var inserted = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"inserted").equals("1")) {
	%>
			inserted = true;
	<%
		}
	%>
	function init() {
		if(inserted) {
			//apply_money_form.insert.style.display = "none";
			closeWindow();
		}
	}

	$(document).ready(function(){
		getLevelSelect(0, nameArray, widthArray, centerAccounts, null,vname,1);
		getLevelSelect(1, nameArray, widthArray, centerAccounts, $("#center_account_type_id"),vname,0);
		getLevelSelect(2, nameArray, widthArray, centerAccounts, $("#center_account_type1_id"),vname,100019);
		getLevelSelect(0, nameArray1, widthArray1, pays, null,vname1);
		//$('#subject').val(<%=StringUtil.getString(request,"subject")%>);
		init();
		var currency = $("select[name='currency']").val();
		var showValue = "" ;
	    if(currency === "USD"){
			showValue = parseFloat('<%= maxMoney/currencyEx%>');
		}else{
		    showValue = parseFloat('<%= maxMoney%>');
		}
		$("#amount").val(decimal(showValue,2));
	});
	function handleAmountByCurrency(){
		var amount = $("#amount").val();
		var currency = $("#currency").val();
		if($.trim(amount).length > 0){
		 	if(currency === "USD"){
		 	  var tempValue = parseFloat(amount) / parseFloat('<%= currencyEx%>');
		 	  $("#amount").val(decimal(tempValue,2));
		 	}else{
		 	   var tempValue = parseFloat(amount) * parseFloat('<%= currencyEx%>');
		  	  $("#amount").val(decimal(tempValue,2));
		 	}
		}
	}
	function decimal(num,v){
		var vv = Math.pow(10,v);
		return Math.round(num*vv)/vv;
	} 
	
<%//level_id,value,name
	String str = "var pays = new Array(";
	str += "new Array('01',0,'任意')";
	str += ",new Array('01.0',0,'任意')";
	str += ",new Array('01.0.0',0,'任意')";
	str += ",new Array('02',1,'公司账户')";
	str += ",new Array('02.0',0,'公司账户')";
	str += ",new Array('02.0.0',0,'选择部门')";
	for(int i=0;i<accounts.length;i++) {
		DBRow account = accounts[i];
		if(account.get("account_parent_id",0)==0 && account.get("account_category_id",0)==1)//部门列表
			str += ",new Array('02.0."+(i+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
	}	
	str += ",new Array('03',2,'职员账户')";
	str += ",new Array('03.0',0,'选择部门')";
	str += ",new Array('03.0.0',0,'选择职员')";
	for(int i=0;i<adminGroups.length;i++) {//部门
		DBRow adminGroup = adminGroups[i];
		str += ",new Array('03."+(i+1)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
		str += ",new Array('03."+(i+1)+".0',0,'选择职员')";
		for(int ii=0;ii<accounts.length;ii++) {
			DBRow account = accounts[ii];
			if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
				str += ",new Array('03."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
		}
	} 
	str += ",new Array('04',3,'供应商')";
	str += ",new Array('04.0',0,'选择生产线')";
	str += ",new Array('04.0.0',0,'选择供应商')";
	for(int i=0;i<productLineDefines.length;i++) {
		DBRow productLineDefine = productLineDefines[i];
		str += ",new Array('04."+(i+1)+"',"+productLineDefine.get("id",0)+",'"+productLineDefine.getString("name")+"')";
		str += ",new Array('04."+(i+1)+".0',0,'选择供应商')";
		for(int ii=0;ii<accounts.length;ii++) {
			DBRow account = accounts[ii];
			if(account.get("account_parent_id",0)==productLineDefine.get("id",0) && account.get("account_category_id",0)==3)//供应商列表
				str += ",new Array('04."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
		}
	}

	str+= ");";
	out.println(str);
%>

<%//level_id,value,name
str = "var centerAccounts = new Array(";
str += "new Array('01',0,'任意')";
str += ",new Array('01.0',0,'任意')";
str += ",new Array('01.0.0',0,'任意')";
str += ",new Array('02',1,'公司账户')";
str += ",new Array('02.0',0,'公司账户')";
str += ",new Array('02.0.0',0,'选择部门')";
for(int i=0;i<accounts.length;i++) {
	DBRow account = accounts[i];
	if(account.get("account_parent_id",0)==0 && account.get("account_category_id",0)==1)//部门列表
		str += ",new Array('02.0."+(i+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
}	
str += ",new Array('03',2,'职员账户')";
str += ",new Array('03.0',0,'选择部门')";
str += ",new Array('03.0.0',0,'选择职员')";
for(int i=0;i<adminGroups.length;i++) {//部门
	DBRow adminGroup = adminGroups[i];
	str += ",new Array('03."+(i+1)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
	str += ",new Array('03."+(i+1)+".0',0,'选择职员')";
	for(int ii=0;ii<accounts.length;ii++) {
		DBRow account = accounts[ii];
		if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
			str += ",new Array('03."+(i+1)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
	}
}
str+= ");";
out.println(str);
%>

var nameArray = new Array('center_account_type_id','center_account_type1_id','center_account_id');
var widthArray = new Array(120,120,200);
var nameArray1 = new Array('payee_type_id','payee_type1_id','payee_id');
var widthArray1 = new Array(120,120,200);
var vname = new Array('nameArray','widthArray','pays','vname');
var vname1 = new Array('nameArray1','widthArray1','pays','vname1');

function getLevelSelect(level,nameArray, widthArray, selectArray,o,vnames,value) {
		if(level<nameArray.length) {
			var name = nameArray[level];
			var width = widthArray[level];
			var levelId = o==null?"":$("option:selected",o).attr('levelId');
			var onchangeStr = "";
			if(level==nameArray.length-1)
				onchangeStr = "exec(this)";
			else
				onchangeStr = "getLevelSelect("+(level+1)+","+vnames[0]+","+vnames[1]+","+vnames[2]+",this,"+vnames[3]+")";

			var selectHtml = "<select name='"+name+"' id='"+name+"' style='width:"+width+"px;' onchange="+onchangeStr+"> ";
			for(var i=0;i<selectArray.length;i++) {
				if(levelId!="") {
					var levelIdChange = selectArray[i][0].replace(levelId+".");
					var levelIds = levelIdChange.split(".");	
					
					if(levelIdChange!=selectArray[i][0]&&levelIds.length==1){
						//alert(levelId+",value="+selectArray[i][0]+",change='"+levelIdChange+"',"+levelIds.length);
						selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
					}
				}
				else {
					var levelIdChange = selectArray[i][0];
					var levelIds = levelIdChange.split(".");
					if(levelIds.length==1){
						//alert(levelId+","+selectArray[i][0]+levelId1);
						selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
					}
				}
				
			}
			selectHtml += "</select>";
			$('#'+name+'_div').html('');
			$('#'+name+'_div').append(selectHtml);
			$('#'+name).val(value);
		    
			$("#"+name).chosen();
			$("#"+name).chosen({no_results_text: "没有该项!"});
	
			getLevelSelect(level+1,nameArray,widthArray,pays,$('#'+name),vnames);

			//修改输入框,破坏结构
			if($(o).attr("id")=="payee_type_id"||$(o).attr("id")=="payee_type1_id")
				$('#payee').val('');
		}
}

function exec(o) {
	if($(o).attr("id")=="payee_id") {

	  	
	  	
		if($("option:selected",o).val()!=0){
				$('#payee').val($("option:selected",o).attr('displayName'));
		}
		else{
				$('#payee').val('');
		}
		//如果是供应商 那么应该读取后台的Account表中的信息
		var payee_type_id = $("#payee_type_id").val();
		var payee_id = $("#payee_id").val();
		if(payee_type_id * 1  ==  3 && payee_id * 1  > 0 ){
		    ajaxGetAccountInfo(payee_id * 1 );
		}
	 
	}
}
function ajaxGetAccountInfo(account_with_id){
	var uri =  '<%= getAccountInfoAction%>' + '?account_with_type='+'<%=AccountKey.SUPPLIER%>' + '&account_with_id=' + account_with_id;
	$.ajax({
		url:uri,
		dataType:'json',
		beforeSend:function(request){$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});},
		success:function(data){
		    $.unblockUI();
		    if(data && data.length > 0 ){
			    $(".receiver").remove();
				for(var index = 0 , count = data.length ; index < count ; index++ ){
					//account_name,account_number,account_swift,account_address,account_blank,account_blank_address,account_phone
					if(data[index]["account_currency"].toUpperCase() === "RMB" ){
						addRMB(data[index]["account_name"],data[index]["account_number"],data[index]["account_swift"],data[index]["account_address"],data[index]["account_blank"],data[index]["account_blank_address"],data[index]["account_phone"],"fromajax");
					}else{
					    addUSD(data[index]["account_name"],data[index]["account_number"],data[index]["account_swift"],data[index]["account_address"],data[index]["account_blank"],data[index]["account_blank_address"],data[index]["account_phone"],"fromajax");
					}
				}
			}else{
				var tables = $(".fromajax");
				for(var index = 0 , count = tables.length ; index < count ; index++ ){
					var nodeTable = $(tables[index]);
					$("input[name!='account_currency']",nodeTable).val("");
				}
			}
		    
		},
		error:function(data){
		    alert("系统错误"); $.unblockUI();
		}
	})
    
}

function setSubject(o) {
	if($(o).val()==4) {
		$("#tr1").attr("style","style","display:none");
	}else {
		$("#tr1").attr("style","display:none");
	}
}
function setParentUserShow(ids,names,methodName)
{
	eval(methodName)(ids,names);
}
function setParentUserShow(user_ids , user_names){
	$("#adminUserIds").val(user_ids);
	$("#adminUserNames").val(user_names);
}
function adminUser(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIds").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShow'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
// 回显要求看上去已经是上传了的文件。所以上传是要加上前缀名的sn
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_names']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
	 	//判断是不是有了
	 	if($("a",$("#over_file_td")).length > 0){
			var td = $("#over_file_td");
	
			td.append(lis);
		}else{
			$("#file_up_tr").attr("style","");
			$("#jquery_file_up").append(lis);
		}
	} 
}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+"_" + fileName;
    var  a = "<p class='new'><a href="+uri+">"+showName+"</a>&nbsp;&nbsp;(New)</p>";
    return a ;
  
}
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="apply_money_form" id="apply_money_form" method="post">
<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_money_transport_insert.html?association_id=<%=associationId%>&inserted=1"/>
<input type="hidden" name="purchase_id" value='<%=purchase_id %>'/>
<input type="hidden" name="account_with_type" value='<%= AccountKey.APPLI_MONEY %>'/>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">资金申请单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
	        <tr height="29">
	          <td align="right" class="STYLE2">转运单号:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" bgcolor="#eeeeee">
	          	<%="T"+associationId%>
	          </td>
	        </tr>
	        <tr> 
		      <td align="right" class="STYLE2">费用项目:</td>
		      <td>&nbsp;</td>
		      <td>
		      	运费
		      	<input type="hidden" name="subject" id="subject" value="1">
		      	<!-- 
				<select name="subject" id="subject" onchange="setSubject(this)">
					<option value="1">运费</option>
					<option value="2">报关</option>
					<option value="3">清关</option>
					<option value="4">退税</option>
				</select>
				-->
			  </td>
		    </tr>
		    <tr id="tr1" name="tr1" style="display:none"> 
		      <td align="right" class="STYLE2">成本中心:</td>
		      <td>&nbsp;</td>
		      <td>
				<div id='center_account_type_id_div' name='center_account_type_id_div' style="float:left;">
				</div>
				<div id='center_account_type1_id_div' name='center_account_type1_id_div' style="float:left;">
				</div>
				<div id='center_account_id_div' name='center_account_id_div' style="float:left;">
		        </div>
			  </td>
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2">币种:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<select id="currency" name="currency" onchange="handleAmountByCurrency()">
		    		<option value="RMB" <%=row.getString("currency").equals("RMB")?"selected":""%>>RMB</option>
		    		<option value="USD" <%=row.getString("currency").equals("USD")?"selected":""%>>USD</option>
		    	</select>
		      </td>
		    </tr>
		  	<tr> 
		      <td align="right" class="STYLE2">金额:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<%
		      		float apply_money = 0; //TODO 待补逻辑:getApplyMoneyFreightCost(6,Long.parseLong(associationId));
		      	%>
		      	<input name="amount" id="amount" type="text" size="15" value="<%=(amount-apply_money)%>">
		      </td>
		    </tr>
			<tr> 
		      <td align="right" class="STYLE2">最迟转款时间:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<input type="text" name="last_time" id="st_date"  value=""  style="border:1px #CCCCCC solid;width:70px;"/>
		      </td>
		    </tr>		  
		    <tr>
		      <td align="right" class="STYLE2">收款人:</td>
		      <td>&nbsp;</td>
		     
		      <td>
		      	<div style="float:left;" id="payee_type_id_div" name="payee_type_id_div">		      	
				</div>
				<div id='payee_type1_id_div' name='payee_type1_id_div' style="float:left;">
				</div>
				<div id='payee_id_div' name='payee_id_div' style="float:left;">
		        </div>
		        <br/><br/>
		      	<input name="payee" id="payee" value="<%=payee%>"/>
		      </td>
		   
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2">收款信息:</td>
		      <td>&nbsp;</td>
		      <td>		  
		      <!-- 1.如果是选取的供应商那么应该重新加载出来。 -->
		          			   <h1 id="addBar"> 
							      			<input type="button" value="添加人民币收款" onclick="addRMB();"/>
							      			<input type="button" value="添加外币收款" onclick="addUSD();"/>
							      		</h1>
							      		<table class="receiver rmb next">
							      			<tr>
							      				<td rowspan="4" style='width:150px;text-align:center;'>人民币收款信息一</td>
							      				<td class="left">收款人户名:</td>
							      				<td><input type="text" name="account_name" class="noborder account_name"/></td>
							      				<td rowspan="4" style="width:80px;text-align:center;"><span onclick="deletePaymentInfo(this)" class="_hand">删除</span></td>
							      			</tr>
							      			<tr>
							      				<td class="left">收款人账号:</td>
							      				<td><input type="text" name="account_number" class="noborder account_number"/></td>
							      			</tr>
							      			<tr>
							      				<td class="left">收款开户行:</td>
							      				<td><input type="text" name="account_blank" class="noborder account_blank"/></td>
							      			</tr>
							      			<tr>
							      				<td class="left">收款人电话:</td>
							      				<td>
							      					<input type="text" name="account_phone" class="noborder account_phone"/>
							      				 	<input type="hidden" name="account_currency" value='rmb' />
							      				</td>
							      			</tr>
							      		</table> 		     
 		      </td>
		    </tr>
		    <tr>
			      <td class="STYLE2" align="right">负责人:</td>
			      <td>&nbsp;</td>
			      <td>
				     <input type="text" id="adminUserNames" name="adminUserNames" style="width:180px;" onclick="adminUser()"/>
				     <input type="hidden" id="adminUserIds" name="adminUserIds" value=""/>
				            通知：
			   		<input type="checkbox"  name="isMail" id="isMail"/>邮件
			   		<input type="checkbox"  name="isMessage" id="isMessage" />短信
			   		<input type="checkbox"  name="isPage" id="isPage" />页面
			   		<input type="hidden"  name="isNeedMail" id="isNeedMail"/>
			   		<input type="hidden"  name="isNeedMessage" id="isNeedMessage" />
			   		<input type="hidden"  name="isNeedPage" id="isNeedPage" />
		     	 </td>
		    </tr>			    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="remark" rows="4" cols="40"><%=remark%></textarea></td>
		    </tr>
		    <tr>
		    	<td align="right" class="STYLE2">凭证:</td>
		     	<td>&nbsp;</td>
		    	<td>
		    		<span id="file_up_span">
						<input type="hidden" id="sn" name="sn" value="F_applyMoney"/>
						<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_financial")%>" />
						<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="上传凭证" />
					</span>
		    	</td>
		    </tr>
		    <tr id="file_up_tr" style='display:none;'>
	 		 	<td align="right" class="STYLE2">完成凭证:</td>
	 		 	<td>&nbsp;</td>
	 		 	<td>
	              	<div id="jquery_file_up">	
	              		<input type="hidden" name="file_names" value=""/>
	              	</div>
	 		 	</td>
		 	</tr>
		</table>

		<input id="apply_id" name="apply_id" id="apply_id" type="hidden" value="<%=apply_id%>"/>
		<input id="association_id" name="association_id" id="association_id" type="hidden" value="<%=associationId%>"/>
		<input name="creater_id" id="creater_id" type="hidden" value="<%=adminLoggerBean.getAdid() %>"/>
		<input name="creater" id="creater" type="hidden" value="<%=adminLoggerBean.getEmploye_name() %>"/>
		<input name="create_time" id="create_time" type="hidden" value="<%=(new SimpleDateFormat("yyyy-MM-dd")).format(new java.util.Date()) %>"/>
		<input type="hidden" name="association_type_id" id="association_type_id" value='<%=association_type_id %>' />
		<input type="hidden" name="types" id="types" value="10015" />
		<input type="hidden" name="inserted" id="inserted" value="1" />
	</fieldset>	
	</td>
  </tr>

</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="insert" type="button" class="normal-green-long" onclick="submitApply()" value="<%=(!StringUtil.getString(request,"subject").equals("")?"确定":"下一步")%>">
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
  </form>
<script type="text/javascript">
<!--
	function submitApply()
	{
		if(!checkPayAcount()) {
			return;
		}
		if(!checkAccountInfo()){return ;}
		if($('#amount').val().trim()==""||$('#amount').val()==null)
		 {
		     $('#amount').focus();
		     alert("金额不能为空！");
		 }
		 else if(isNaN($('#amount').val().trim()))
	 	 {
		    $('#amount').focus();
		     alert("金额必须为数字！");
	 	 }
		 else if($('#amount').val().trim()<=0)
		 {
		 	 $('#amount').focus();
		     alert("金额必须大于零！");
		 }
		 else if($('#payee').val().trim()==""||$('#payee').val()==null)
		 {
		 	 $('#payee').focus();
		 	 alert("请填写收款人！");
		 }
	 		 
		 else if($("#remark").val().trim().length>=200)
		 {
		     $("#remark").focus();
		     alert("备注字数不能超过200！");
		 }
		 else if("" == $("#st_date").val() || 0 == $("#st_date").val().trim().length)
		 {
			$("#st_date").focus();
			alert("最迟转款时间不能为空");
		 }else if($("#adminUserIds").val().trim().length  <1){
		     $("#adminUserIds").focus();
		     alert("请选择负责人");
		}
		 else
		 {
		   	if($("input:checkbox[name=isMail]").attr("checked"))
		   	{
				$("#isNeedMail").val(2);
		   	}
		   	if($("input:checkbox[name=isMessage]").attr("checked"))
		   	{
				$("#isNeedMessage").val(2);
		   	}
		   	if($("input:checkbox[name=isPage]").attr("checked"))
		   	{
				$("#isNeedPage").val(2);
		   	}
		   	fixAccountInfo();
 
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/applyMoneyTransportFreightAction.action',
				data:$("#apply_money_form").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "success"){
						showMessage("申请成功","success");
						setTimeout("windowClose()", 1000);
					}else{
						showMessage("申请失败","error");
					}
				},
				error:function(){
					showMessage("系统错误","error");
				}
			})	
		 }	
	}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};

	function checkPayAcount() {
		 var amount = $("#amount").val();
		 var currency = $("#currency").val();
		// 判断金额是不是都正确
		if(currency === "USD" && parseFloat(amount) >  parseFloat ('<%=canApplyMaxUSDMoney%>')){
			 alert("申请金额不正确，最多只能申请"+parseFloat('<%=canApplyMaxUSDMoney%>')+" USD");
			 return false;
		}
		if(currency === "RMB" && parseFloat(amount) > parseFloat('<%=canApplyMaxRMBMoney%>')){
			 alert("申请金额不正确，最多只能申请"+parseFloat('<%=canApplyMaxRMBMoney%>')+" RMB");
			 return false;
		}
		return true;
	}

	function closeWindow(){
		$.artDialog.close();
	}
	function checkAccountInfo(){
		//rmb
	    var rmbInfo = $(".rmb");
		if(rmbInfo.length > 0 ){
			for(var index = 0 , count = rmbInfo.length ; index < count ; index++ ){
				var _table = $(rmbInfo.get(index));
				var fixIndex =  index+1;
				var r_name =  $("input.account_name",_table).val();
				var r_account = $("input.account_number",_table).val();
				var r_bank = $("input.account_blank",_table).val();
				var r_phone = $("input.account_phone",_table).val();
			 
				if($.trim(r_name).length < 1 ){
				    alert("输入收款人户名!");
					return false;
				} 
				if($.trim(r_account).length < 1 ){
				    alert("输入收款人账号!");
					return false;
				} 
				if($.trim(r_bank).length < 1 ){
				 	alert("输入开户行!");
				 	return false;
				}
				 
			}
		}
	//usd
		var usdInfo = $(".usd");
		 
		if(usdInfo.length > 0 ){
			for(var index = 0 , count = usdInfo.length ; index < count ; index++ ){
			//	r_name,r_account,swift,r_address,r_bank_name,r_bank_address
			//	收款人户名,收款人账号,SWIFT,收款人地址,收款行名称,收款行地址
				var _table = $(usdInfo.get(index));
				 
				var r_name =  $("input.account_name",_table).val();
				var r_account = $("input.account_number",_table).val();
				var swift = $("input.account_swift",_table).val();
				var r_address = $("input.account_address",_table).val();
				var r_bank_name = $("input.account_blank",_table).val();
				var r_bank_address = $("input.account_blank_address",_table).val();
			 
				if($.trim(r_name).length < 1 ){
				    alert("输入收款人户名!");
					return false;
				} 
				if($.trim(r_account).length < 1 ){
					alert("输入收款人账号!");
					return false;
				} 
				if($.trim(swift).length < 1 ){
					alert("输入SWIFT!") ;
					return false;
				}
			}
		}
		return true ;

		
	}
	
//-->
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 
	$(".chzn-select").chosen(); 
	$(".chzn-select1").chosen(); 
	$(".chzn-select2").chosen(); 
	$("#st_date").date_input();
	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 	}
	}
	 var va = ['一','二','三','四','五','六','七','八','九'];
	 function addUSD(account_name,account_number,account_swift,account_address,account_blank,account_blank_address,account_phone,where){
		//r_name,r_account,swift,r_address,r_bank_name,r_bank_address
		var len = $(".usd").length;	 
		var table =  "<table class='receiver usd next "+(where?where:"")+"'>";
		table += "<tr><td rowspan='7' style='"+"width:150px;text-align:center;"+"'>外币收款信息"+va[len]+"</td>";
		table += "<td class='left'>收款人户名:</td><td><input type='text' class='noborder account_name' name='account_name' value='"+(account_name?account_name:"")+"'/></td>";
		table += "<td rowspan='7' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";
		table += "<tr><td class='left'>收款人账号:</td><td><input type='text' class='noborder account_number' name='account_number' value='"+(account_number?account_number:"")+"'/></td></tr>";
		table +="<tr><td class='left'>SWIFT:</td><td><input type='text' class='noborder account_swift' name='account_swift' value='"+(account_swift?account_swift:"")+"'/></td></tr>";
		table +="<tr><td class='left'>收款人地址:</td><td><input type='text' class='noborder account_address' name='account_address' value='"+(account_address?account_address:"")+"'/></td></tr>";
		table +="<tr><td class='left'>收款行名称:</td><td><input type='text' class='noborder account_blank' name='account_blank' value='"+(account_blank?account_blank:"")+"'/></td></tr>" ;
		table +="<tr><td class='left'>收款行地址:</td><td><input type='text' class='noborder account_blank_address' name='account_blank_address' value='"+(account_blank_address?account_blank_address:"")+"' /><input type='hidden' name='account_currency' value='usd'/></td></tr></table>" ;

	 	//	
		if($(".receiver").length > 0 ){
			var addBar = $(".receiver").last();
			addBar.after($(table));
		}else{
			$("#addBar").after($(table));
		}
	}
	function addRMB(account_name,account_number,account_swift,account_address,account_blank,account_blank_address,account_phone,where){
		var len  = $(".rmb").length ;
		var table =  "<table class='receiver rmb next "+(where?where:"")+"'>";
		table += "<tr><td rowspan='4' style='"+"width:150px;text-align:center;"+"'>人民币收款信息"+va[len]+"</td>";
		table += "<td class='left'>收款人户名:</td>";
		table += "<td><input type='text' name='account_name' class='noborder account_name' value='"+(account_name?account_name:"")+"'/></td>";
		table += "<td rowspan='4' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";

		table += "<tr><td class='left'>收款人账号:</td>";
		table += "<td><input type='text' name='account_number' class='noborder account_number' value='"+(account_number?account_number:"")+"'/></td></tr>";
		table +="<tr><td class='left'>收款开户行:</td><td><input type='text' name='account_blank' class='noborder account_blank' value='"+(account_blank?account_blank:"")+"'/></td></tr>";
		table +="<tr><td class='left'>收款人电话:</td><td><input type='text' name='account_phone' class='noborder account_phone' value='"+(account_phone?account_phone:"")+"'/><input type='hidden' name='account_currency' value='rmb'/></td></tr></table>";
	 	// 获取最后一个RMB的table 然后添加到他的后面。如果是没有的话,那么就选取H1标签然后添加到他的后面
		if($(".receiver").length > 0 ){
			var addBar = $(".receiver").last();
			addBar.after($(table));
		}else{
			$("#addBar").after($(table));
		}
		
	}
	function deletePaymentInfo(_this){
	    
		var parentNode = $(_this).parent().parent().parent().parent();
		if($(".receiver").length == 1){
			alert("至少包含一个收款信息");
			return ;
		}
		 parentNode.remove();
	}
	//重新梳理账号信息
	function fixAccountInfo(){
		$(".receiver").each(function(index){
			//将里面的name 都重新加上数字
			var fixIndex = index + 1 ;
			 
			var node = $(this);
			var inputs = $("input",node);
		 	for(var inputIndex = 0 , count = inputs.length ; inputIndex < count ; inputIndex++ ){
				var nodeInput = $(inputs[inputIndex]);
				var name = nodeInput.attr("name");
				if(!(isCanChangeName(name))){
					nodeInput.attr("name",name+"_"+fixIndex);
				}
				
		    }

		})
	}
 
	function isCanChangeName(name) {
	    var r = /[0-9]$/;
	    return r.test(name);
	}
	 
</script>
</body>
</html>

