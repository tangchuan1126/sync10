<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
	long rp_id=StringUtil.getLong(request,"rp_id");
	DBRow returnProduct = productReturnOrderMgrZJ.getDetailReturnProduct(rp_id);
	long oid = returnProduct.get("oid",0l);
	DBRow[] files = fileMgrZJ.getFileByWithIdAndType(rp_id,FileWithTypeKey.ProductReturn);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<script>
function uploadPurchaseDetail()
{
	 if($("#file").val() == "")
	{
		alert("请选择文件上传");
		
	}
	else
	{	
		var filename=$("#file").val().split(".");
		if(filename[filename.length-1].toLowerCase()=="png"||filename[filename.length-1].toLowerCase()=="jpg"||filename[filename.length-1].toLowerCase().toLowerCase()=="gif"||filename[filename.length-1].toLowerCase()=="bmp"||filename[filename.length-1].toLowerCase()=="rar"||filename[filename.length-1].toLowerCase()=="jpeg"||filename[filename.length-1].toLowerCase()=="zip")
		{
			document.upload_form.submit();
		}
		else
		{
			alert("只可上传图片或压缩包");
		}	
	}
}

function refresh()
{
	$.artDialog.close();
}

function addNew()
{
	 var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/add_bill.html?rp_id=<%=rp_id%>&oid=<%=oid%>";
	 
	 //window.location.href = uri;
	 $.artDialog.open(uri,{title: '创建质保账单',width:'1100px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="left" valign="top"><br>
            <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;">
						退货单号：<%=rp_id%>
						</td>
					  </tr>
				  </table>
					<br/><br/>
					<form action="<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/warranty/returnProductByFile.action" name="upload_form" enctype="multipart/form-data" method="post">
						<table width="95%" align="center" cellpadding="5" cellspacing="0" >
							<tr  style=" padding-bottom:15px; padding-left:15px;">
							<td width="9%" align="left" valign="top" nowrap="nowrap" class="STYLE3" >上传文件</td>
							<td width="91"align="left" valign="top" >
								<input type="file" name="file" id="file">
								<input type="button" value="上传" onClick="uploadPurchaseDetail();"/>
							</td>
						  </tr>
					  	</table>
					  	<input type="hidden" name="rp_id" id="rp_id" value="<%=rp_id%>"/>
					</form>
				  	<br/><br/>
				  	<table width="95%" align="center" cellpadding="5" cellspacing="0" >
					<%
						if(files.length != 0)
						{
					%>
						<tr> 
					        
					        <th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">文件名</th>
					        <th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">操作类型</th>
					  	</tr>
					
					<%
							for(int i=0;i<files.length;i++)
							{
					 %>
						<tr  style=" padding-bottom:15px; padding-left:15px;">
						
						<td width="9%" align="left" valign="top" nowrap="nowrap">
							<a href="" onclick="window.open('<%=ConfigBean.getStringValue("systenFolder")+"returnImg/"+files[i].getString("file_name") %>')" style="text-align: 9px;color: gray;text-decoration: none"><%=files[i].getString("file_name")%></a>
							
						</td>
						<td width="91"align="left" valign="top" >
							<input name="Submit4" type="button" class="short-button" value="下载" onclick="window.open('<%=ConfigBean.getStringValue("systenFolder")+"returnImg/"+files[i].getString("file_name") %>')"/>&nbsp;&nbsp;
							<!-- 
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="">
							 -->
						</td>
					  </tr>
					  <%
					  	    }
					  	}
					  	else
					  	{
					   %>
					   <tr>
					   		<td colspan="2">暂时没有收到客户文件</td>
					   </tr>
					   <%
					   	}
					    %>
				  </table>
				</td>
				</tr>
              <tr>
                <td  align="right" valign="middle" class="win-bottom-line">				
				  <input type="button" name="Submit2" value="创建账单" class="normal-green" onclick="addNew()">
			
			    <input type="button" name="Submit2" value="关闭" class="normal-white" onClick="refresh();"></td>
              </tr>
            </table>
</body>
</html>
