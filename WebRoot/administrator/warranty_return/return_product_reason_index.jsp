<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%> 
<%
	
	String st = StringUtil.getString(request,"input_st_date");
	String en = StringUtil.getString(request,"input_en_date");
	
	String p_name = StringUtil.getString(request,"p_name");
	long pcid = StringUtil.getLong(request,"filter_pcid");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
	int warranty_type = StringUtil.getInt(request,"warranty_type");
	
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
	
	String input_st_date,input_en_date;
	if ( st.equals("") )
	{	
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		st = input_st_date;
	}
	else
	{	
		input_st_date = st;
	}
									
	if ( en.equals("") )
	{	
		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		en = input_en_date;
	}
	else
	{	
		input_en_date = en;
	}
	
	String cmd = StringUtil.getString(request,"cmd");
	String search_key = StringUtil.getString(request,"search_key");
	
	WarrantyTypeKey warrantyType = new WarrantyTypeKey();
	
	DBRow[] returnProductReasons;
	if(cmd.equals("search"))
	{
		returnProductReasons = productReturnOrderMgrZJ.searchReturnProductReasonByRpid(search_key,pc);
	}
	else
	{
		returnProductReasons = productReturnOrderMgrZJ.filterReturnProductReason(input_st_date,input_en_date,warranty_type,pcid,pro_line_id,p_name,pc);
	}
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<%-- ***********************  --%> 

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<%-- Frank ****************** --%>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<%-- ***********************  --%>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<style type="text/css">
	body{text-align:left;}
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	


<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>
<script>
	$().ready(function() {

	function log(event, data, formatted) {
		
	}

	
	addAutoComplete($("#p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"p_name");
	
	$("#p_name").keydown(function(event){
		if (event.keyCode==13)
		{
			
		}
	});

});

	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
	
	function searchReturnProductReason()
	{
		document.search_form.submit();
	}
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br/>
<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#usual_tools">服务单搜索</a></li>
			<li><a href="#return_reason_filter">高级搜索</a></li>
		</ul>
		<div id="usual_tools">
		<form action="return_product_reason_index.html" method="post" name="search_form">
          <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="30%" style="padding-top:3px;">
			   <div id="easy_search_father">
			   <div id="easy_search"><a href="javascript:searchReturnProductReason()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
			   </div>
				<table width="485" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				    <td width="418">
						<div  class="search_shadow_bg">
							  <input type="text" name="search_key" id="search_key" value="<%=search_key%>" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333"/>
						</div>
					</td>
				    <td width="67">
					 <a href="javascript:void(0)" id="search_help" style="padding:0px;color: #000000;text-decoration: none;"><img src="../imgs/help.gif" border="0" align="absmiddle" border="0"/></a>
					</td>
				  </tr>
				</table>
			</td>
              <td width="45%" ></td>
              <td width="12%" align="right" valign="middle">
				
			 </td>
            </tr>
          </table>
		   		<input type="hidden" name="cmd" id="cmd" value="search"/>
		   </form>
		</div>
		<div id="return_reason_filter">
			<form action="return_product_reason_index.html" method="post">
		    <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="180px" nowrap="nowrap" align="left">
				  		采样开始时间：<input name="input_st_date" type="text" id="input_st_date" size="10" value="<%=input_st_date%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
				  	</td>
					<td width="460px">
						<ul id="productLinemenu" class="mcdropdown_menu">
			             <li rel="0">所有产品线</li>
			             <%
							  DBRow p1[] = productLineMgrTJH.getAllProductLine();
							  for (int i=0; i<p1.length; i++)
							  {
									out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
									 
									out.println("</li>");
							  }
						  %>
			           </ul>
			           <input type="text" name="productLine" id="productLine" value=""/>
			           <input type="hidden" name="pro_line_id" id="pro_line_id" value="0"/>
					</td>
					<td>
						 <select id="warranty_type" name="warranty_type" style="display:inline;">
				           <option value="-1">质保类型</option>
				           <%
				           		ArrayList warrantyTypeList = warrantyType.getWarrantyType();
				           		for(int i = 0;i<warrantyTypeList.size();i++)
				           		{
				           	%>
				           		<option <%=warrantyTypeList.get(i).toString().equals(String.valueOf(warranty_type))?"selected=\"selected\"":""%> value="<%=warrantyTypeList.get(i)%>"><%=warrantyType.getWarrantyTypeById(warrantyTypeList.get(i).toString())%></option>
				           	<%
				           		}
				           %>
			           </select>
					</td>
					<td>
						<input type="submit" class="button_long_refresh" value="过滤"/>
					</td>
				</tr>
				<tr>
					<td align="left">
	            	采样结束时间：<input name="input_en_date" type="text" id="input_en_date" size="10" value="<%=input_en_date%>" readonly="readonly" />
	            	</td>
					<td  align="left" id="categorymenu_td"> 
					  	 <ul id="categorymenu" class="mcdropdown_menu">
						  <li rel="0">所有分类</li>
						  <%
							  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
							  for (int i=0; i<c1.length; i++)
							  {
									out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
						
									  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
									  if (c2.length>0)
									  {
									  		out.println("<ul>");	
									  }
									  for (int ii=0; ii<c2.length; ii++)
									  {
											out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
											
												DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
												  if (c3.length>0)
												  {
														out.println("<ul>");	
												  }
													for (int iii=0; iii<c3.length; iii++)
													{
															out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
															out.println("</li>");
													}
												  if (c3.length>0)
												  {
														out.println("</ul>");	
												  }
												  
											out.println("</li>");				
									  }
									  if (c2.length>0)
									  {
									  		out.println("</ul>");	
									  }
									  
									out.println("</li>");
							  }
							  %>
						</ul>
						<input type="text" name="category" id="category" value="" />
						<input type="hidden" name="filter_pcid" id="filter_pcid" value="0"/>
				  </td>
				  <td>
				  	<input type="text" name="p_name" id="p_name" value="<%=p_name%>" onclick="cleanProductLine('')" style="width:200px;"/>
				  </td>
				  <td></td>
				</tr>
			</table>
			<input type="hidden" name="cmd" value="filter"/>
			</form>
		</div>
	</div>
</div>
<script>
	$("#input_st_date").date_input();
	$("#input_en_date").date_input();
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	});
	</script>

	<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr>
    	<th  nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">服务单号</th> 
        <th  nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">商品名称</th>
        <th  nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">数量</th>
        <th  nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">所属产品线</th>
        <th nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">所属分类</th>
        <th  nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">质保类型</th>
        <th nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">不满意原因</th>
        <th nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">创建时间</th>
  	</tr>
  	<%
  		for(int i = 0;i<returnProductReasons.length;i++)
  		{
  			DBRow product = productMgr.getDetailProductByPcid(returnProductReasons[i].get("pc_id",0l));
  			DBRow catalog = catalogMgr.getDetailProductCatalogById(product.get("catalog_id",0l));
  	%>
  		<tr>
  			<td nowrap="nowrap" style="height: 30px;font-size: 10px;font-weight:bold;">
	  			R<%=returnProductReasons[i].getString("rp_id")%>
	  		</td>
	  		<td style="height: 30px;font-size: 12px;">
	  			<%=product.getString("p_name")%>
	  		</td>
	  		<td align="center"">
	  			<%=returnProductReasons[i].get("quantity",0f)%>
	  		</td>
	  		<td align="left">
	  			<%=productLineMgrTJH.getProductLineById(catalog.get("product_line_id",0l)).getString("name")%>
	  		</td>
	  		<td>
	  			<%=catalog.getString("title")%>
	  		</td>
	  		<td align="center"><%=warrantyType.getWarrantyTypeById(returnProductReasons[i].getString("warranty_type"))%></td>
	  		<td><%=returnProductReasons[i].getString("return_reason")%></td>
	  		<td align="center" nowrap="nowrap"><%=returnProductReasons[i].getString("create_time").substring(2,10)%></td>
	  	</tr>
  	<%
  		}
  	%>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>


<form name="dataForm" method="post">
    <input type="hidden" name="p"/>
    <input type="hidden" name="input_st_date" value="<%=input_st_date%>"/>
    <input type="hidden" name="input_en_date" value="<%=input_en_date%>"/>
    <input type="hidden" name="p_name" value="<%=p_name%>"/>
    <input type="hidden" name="pro_line_id" value="<%=pro_line_id%>"/>
    <input type="hidden" name="warranty_type" value="<%=warranty_type%>"/>
    <input type="hidden" name="cmd" value="<%=cmd%>"/>
    <input type="hidden" name="search_key" value="<%=search_key%>"/>
 </form>

<script>
	$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}
});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#pro_line_id").val(id);
					if(id==<%=pro_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"",<%=pcid%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
				}
});

<%
if (pro_line_id!=0)
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue('<%=pro_line_id%>');
<%
}
else
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue(0);
<%
}
%>
<%
if (pcid>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=pcid%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 

function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"p_name").val("");
}
</script>	

</body>
</html>



