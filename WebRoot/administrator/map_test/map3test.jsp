<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ include file="../../include.jsp"%>
  <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">    
  <html>    
      <head>  
      <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&sensor=false"></script>
      <script type="text/javascript" src="//www.google.com/jsapi"></script>
      <script type="text/javascript">
      		google.load('visualization', '1', {packages: ['table','geochart']}); //load google chart
      </script>
      
      <script src="../js/maps/GoogleMapsV3.js"></script> 
      <script src="../js/maps/jsmap.js"></script> 
   <script language="javascript" type="text/javascript">
var kmlLayer;
function load(){
	jsMapInit();
	kmlLayer = new google.maps.KmlLayer({
		url:'https://mapsengine.google.com/map/kml?mid=zRyYh9YiD1rg.krnyETIpCV20',
		options:{
			map:jsmap.getMap()
		}
	});
}
function changeKml(url){
	kmlLayer.setUrl(url);
}
</script>
      </head> 
      <body onload="load()">
      	  <div>
      	  仅供参考&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KML:
      	  <select onchange="changeKml(this.value)">
      	  	<option value="https://mapsengine.google.com/map/kml?mid=zRyYh9YiD1rg.krnyETIpCV20">KML 1</option>
      	  	<option value="https://mapsengine.google.com/map/kml?mid=zRyYh9YiD1rg.ksXsz6v6pzlY">KML 2</option>
      	  	<option value="https://mapsengine.google.com/map/kml?mid=zRyYh9YiD1rg.k19BMgFOP_7c">KML 3</option>
      	  </select>
      	  </div>
          <div id="jsmap" style="width: 750px; height: 500px"></div>
          <input type="text" id="lat_lng" style="width: 400px;"/>
          <input type="text" id="debugInfo" style="width: 300px;"/>
          <br/>
          <input type="text" id="lat_lng6" style="width: 400px;"/><br/>
          <br/>
         
      </body>
  </html> 