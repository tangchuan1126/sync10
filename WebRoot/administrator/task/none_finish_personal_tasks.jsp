<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.List"%>
 <%@ page import="com.cwc.app.key.WorkFlowTypeKey" %>
<%@ page import="com.cwc.app.key.RefundResultTypeKey" %>
<%@ page import="com.cwc.app.key.RetailPriceResultTypeKey" %>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@ include file="../../include.jsp"%> 
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css">
	table.zebraTable td{padding:6px;}
</style>
<script>
function next(task_id)
{
	if ( confirm("确认完成任务？") )
	{
		document.next_form.task_id.value = task_id;
		document.next_form.submit();
	}
}
function refreshWindow(){window.location.reload();}
function openWindow(url)
{
	window.open(url);
}
//订单任务
//openWindow('../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=556516')
function orderTask(oid){
    openWindow('../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val='+oid);
}
function finishTaskConfim(schedule_sub_id,schedule_id){
    $.artDialog.confirm('确认完成该任务?', function(){
		finishTask(schedule_sub_id,schedule_id);
	}, function(){
			
	});
}
function finishTask(schedule_sub_id,schedule_id){
    $.ajax({
	    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/schedule_management/WorkFlowFinishAction.action',
	    dataType:'json',
	    data:{schedule_sub_id:schedule_sub_id,schedule_id:schedule_id},
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
	    	if(data && data.flag === "success"){
				window.location.reload();
		    }else{
				showMessage("系统错误.","error");
		   }
	},
	error:function(){$.unblockUI();showMessage("系统错误.","error");}
	})
}
function refundTask(refund_id,schedule_id , schedule_sub_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/refund/handle_refund.html?refund_id="+ refund_id+"&schedule_id="+schedule_id+"&schedule_sub_id="+schedule_sub_id; 
	$.artDialog.open(uri , {title: '退款处理',width:'600px',height:'430px', lock: true,opacity: 0.3,fixed: true});   
}
//如果是驳回的执行任务,那么在点击这个按钮的时候就是要跳转另外的页面
function refundRejectTask(refund_id,schedule_id , schedule_sub_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/refund/handle_reject_refund.html?refund_id="+ refund_id+"&schedule_id="+schedule_id+"&schedule_sub_id="+schedule_sub_id; 
	$.artDialog.open(uri , {title: '退款处理',width:'600px',height:'430px', lock: true,opacity: 0.3,fixed: true});  
}
function showRefundTask(refund_id,schedule_id , schedule_sub_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/refund/show_refund.html?refund_id="+ refund_id+"&schedule_id="+schedule_id+"&schedule_sub_id="+schedule_sub_id; 
	$.artDialog.open(uri , {title: '退款处理',width:'600px',height:'430px', lock: true,opacity: 0.3,fixed: true}); 
}
function retailPrice(quote_id,schedule_id , schedule_sub_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/quote/boss_assessment.html?rp_id="+ quote_id+"&cmd=execute&schedule_id="+schedule_id+"&schedule_sub_id="+schedule_sub_id;
	$.artDialog.open(uri , {title: '商品价格审核',width:'800px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}
function showRetailPrice(quote_id,schedule_id , schedule_sub_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/quote/boss_assessment.html?rp_id="+ quote_id+"&cmd=show&schedule_id="+schedule_id+"&schedule_sub_id="+schedule_sub_id;
	$.artDialog.open(uri , {title: '查看商品价格审核',width:'800px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}
</script>
<%
	//根据当前登录人信息去查询任务的列表
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 	long adid = adminLoggerBean.getAdid(); 
 	DBRow[] rows = scheduleMgrZR.getScheduleWorkFlow(adid,0);
 	WorkFlowTypeKey workFlowType = new WorkFlowTypeKey();
 	RefundResultTypeKey refundResult = new RefundResultTypeKey();
 	RetailPriceResultTypeKey retailPricekey = new RetailPriceResultTypeKey();
%>
</head>
 
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload = "onLoadInitZebraTable()">
<table width="98%" border="0" align="center"  style="margin-top:5px;" cellpadding="0" cellspacing="0" isNeed="false" class="zebraTable" >
<thead> 
  <tr>
    <th width="40%" class="right-title" style="vertical-align: center;text-align: center;height:20px;">任务描述</th>
    <th width="11%" class="right-title" style="vertical-align: center;text-align: center;height:20px;">任务类型</th>
    <th width="14%" class="right-title" style="vertical-align: center;text-align: center;height:20px;">创建日期</th>
    <th width="35%" class="right-title" style="vertical-align: center;text-align: center;height:20px;">&nbsp;</th>
  </tr>
 </thead>
 <tbody>
  <%if(rows != null && rows.length > 0 ){ %>
  		<%for(DBRow row : rows){
  			DBRow refund = null ;
  			DBRow retailPrice = null ;
  			if(row.get("task_type",0) == WorkFlowTypeKey.DRWABACK){ 
				 refund = refundMgrZr.getRefundByRefundId(row.get("associate_id",0l));
  			}	 
  			if(row.get("task_type",0) == WorkFlowTypeKey.RETAIL_PRICE){
  				retailPrice = orderMgrZr.getDetailById(row.get("associate_id",0l),"retail_price","rp_id");
  			}
  		 %>
  			<tr>
  				<td> <%=row.getString("schedule_overview") %></td>
  				<td style="text-align:center;"> <%=workFlowType.getStatusById(row.get("task_type",0)) %> 
  					 <%if(row.get("task_type",0) == WorkFlowTypeKey.DRWABACK){ 
  								%>
  									<br />(<%= refundResult.getStatusById(refund.get("refund_result",0)) %>)
  								<%
  					   }
  					   if(row.get("task_type",0) == WorkFlowTypeKey.RETAIL_PRICE){
  						   %>
  						   		<br />(<%=retailPricekey.getStatusById(retailPrice.get("result_type",0)) %>)
  						   <% 
  					   }
  					 %>

  				</td>
  				<td><%= tDate.getFormateTime(row.getString("start_time")) %></td>
  				<td>
  					<%if(row.get("task_type",0) == workFlowType.ORDER_ASSESS){ %>
  						<input type="button" class="short-button" value="查看任务" onclick="orderTask('<%= row.get("associate_id",0l)%>')"/>
  						<input type="button" class="short-button-ok" value="完成" onclick="finishTaskConfim('<%=row.get("schedule_sub_id",0l) %>','<%=row.get("schedule_id",0l) %>')"/>
  						
  					<%} else if(row.get("task_type",0) == workFlowType.DRWABACK){%>
  							<!-- 如果是驳回的执行任务跳转到另外的页面 -->
  							<%if(refund.get("refund_result",0) ==refundResult.REJECT ){ %>
  								<input type="button" class="short-button" value="查看任务" onclick="refundRejectTask('<%=row.get("associate_id",0l) %>','<%=row.get("schedule_id",0l) %>','<%=row.get("schedule_sub_id",0l) %>')" />
  								<input type="button" class="short-button-ok" value="完成" onclick="finishTaskConfim('<%=row.get("schedule_sub_id",0l) %>','<%=row.get("schedule_id",0l) %>')"/>
  								
  							<%}else if(refund.get("refund_result",0) ==refundResult.AGREE ){%>
  							 	<input type="button" class="short-button" value="查看任务" onclick="showRefundTask('<%=row.get("associate_id",0l) %>','<%=row.get("schedule_id",0l) %>','<%=row.get("schedule_sub_id",0l) %>')" />
  								 <input type="button" class="short-button-ok" value="完成" onclick="finishTaskConfim('<%=row.get("schedule_sub_id",0l) %>','<%=row.get("schedule_id",0l) %>')"/>
  								
  							<%}else{ %>
  								<input type="button" class="short-button" value="执行任务" onclick="refundTask('<%=row.get("associate_id",0l) %>','<%=row.get("schedule_id",0l) %>','<%=row.get("schedule_sub_id",0l) %>')" />
  							<%} %>
  					<%}else if(row.get("task_type",0) == workFlowType.RETAIL_PRICE){ %>
  							<%if(retailPrice.get("result_type",0) == retailPricekey.APPLY){ %>
  								<input type="button" class="short-button" value="执行任务" onclick="retailPrice('<%=row.get("associate_id",0l) %>','<%=row.get("schedule_id",0l) %>','<%=row.get("schedule_sub_id",0l) %>')" />
  							<%}else{ %>
  							 	<input type="button" class="short-button" value="查看任务" onclick="showRetailPrice('<%=row.get("associate_id",0l) %>','<%=row.get("schedule_id",0l) %>','<%=row.get("schedule_sub_id",0l) %>')" />
  								<input type="button" class="short-button-ok" value="完成" onclick="finishTaskConfim('<%=row.get("schedule_sub_id",0l) %>','<%=row.get("schedule_id",0l) %>')"/>
  								
  							<%} %>
  					<%} %>
  				</td>
  			</tr>
  		
  		<%} %>
  <%}else{ %>
  		<tr>
	 			<td colspan="4" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	 	</tr>
  <%} %> 
  </tbody>
</table>
</body>
</html>

