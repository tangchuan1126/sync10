<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="com.cwc.app.key.WorkFlowTypeKey" %>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@ include file="../../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />

<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"></link>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"> </link>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript">
	//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<link href="../comm.css" rel="stylesheet" type="text/css"></link>
<style type="text/css">
	table.zebraTable td{padding:6px;}
</style>
<script>
function next(task_id)
{
	if ( confirm("确认完成任务？") )
	{
		document.next_form.task_id.value = task_id;
		document.next_form.submit();
	}
}

function openWindow(url)
{
	window.open(url);
}
//订单任务
//openWindow('../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=556516')
function orderTask(oid){
    openWindow('../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val='+oid);
}
function finishTaskConfim(schedule_sub_id,schedule_id){
    $.artDialog.confirm('确认完成该任务?', function(){
		finishTask(schedule_sub_id,schedule_id);
	}, function(){
			
	});
}
function finishTask(schedule_sub_id,schedule_id){
    $.ajax({
	    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/schedule_management/WorkFlowFinishAction.action',
	    dataType:'json',
	    data:{schedule_sub_id:schedule_sub_id,schedule_id:schedule_id},
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
	    	if(data && data.flag === "success"){
				window.location.reload();
		    }else{
				showMessage("系统错误.","error");
		   }
	},
	error:function(){$.unblockUI();showMessage("系统错误.","error");}
	})
}
</script>
<%
	//根据当前登录人信息去查询任务的列表
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 	long adid = adminLoggerBean.getAdid(); 
 
 	WorkFlowTypeKey workFlowType = new WorkFlowTypeKey();
 	DBRow countTaskFlow = scheduleMgrZR.getTaskFlowCount(adid);
%>
</head>
 

 
 
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload = "onLoadInitZebraTable()" style=" margin: 10px;">
 <div id="tabs">
	<ul>
		<li><a href="none_finish_personal_tasks.html"><img src="../imgs/phone_31.gif" align="absmiddle" border="0"/> 未完成任务(<%=countTaskFlow.get("not_finish_count",0) %>)<span> </span></a></li>
		<li><a href="finish_personal_tasks.html"><img src="../imgs/member.gif"  align="absmiddle" border="0"/>已完成任务(<%=countTaskFlow.get("finish_count",0) %>)<span> </span></a></li>
	</ul>
</div>
<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	 
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	</script>
</body>
</html>

