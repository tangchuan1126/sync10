<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.List,org.jbpm.api.*,org.jbpm.api.task.Task,org.jbpm.api.history.*"%>
<%@page import="com.cwc.app.api.JbpmMgr"%>
<%@ include file="../../include.jsp"%> 
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>


<script>
function next(task_id)
{
	if ( confirm("确认完成任务？") )
	{
		document.next_form.task_id.value = task_id;
		document.next_form.submit();
	}
}

function openWindow(url)
{
	window.open(url);
}



</script>

</head>
 
<body >
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/jbpm/Next.action" method="post" name="next_form">
<input type="hidden" name="task_id">
</form>

<form  method="post" name="countersign_form">
<input type="hidden" name="task_id">
<input type="hidden" name="countersign_name">
<input type="hidden" name="rp_id">
<input type="hidden" name="reject_reasons">
</form>

<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC" class="zebraTable">
  <tr>
    <th width="20%" align="center" valign="middle" bgcolor="#eeeeee" style="font-weight:bold">任务描述</th>
    <th width="11%" align="center" valign="middle" bgcolor="#eeeeee" style="font-weight:bold">当前任务</th>
    <th width="14%" align="center" valign="middle" bgcolor="#eeeeee">创建日期</th>
    <th width="55%" bgcolor="#eeeeee">&nbsp;</th>
  </tr>
  <%
List<Task> taskList = jbpmMgr.getTaskListByUser(String.valueOf(adminLoggerBean.getAdid()));


if (taskList.size()>0)
{
	for (Task task : taskList)
	{
%>
 
  <tr>
    <td height="35" align="center" bgcolor="#FFFFFF"><%=jbpmMgr.getTaskStringVaviable(task.getId(),"description")%>
    </td> 
    <td align="center" bgcolor="#FFFFFF"><%=task.getActivityName()%> 
    <br/>
   <%if(jbpmMgr.getTaskStringVaviable(task.getId(),"tran_desc")!=null && !jbpmMgr.getTaskStringVaviable(task.getId(),"tran_desc").equals("")&&!jbpmMgr.getTaskStringVaviable(task.getId(),"tran_desc").equals("null"))
    { 
		%>
		(<%=jbpmMgr.getTaskStringVaviable(task.getId(),"tran_desc") %>)
		<%	 
	}  %>
    </td>
    <td align="center" bgcolor="#FFFFFF"><%=task.getCreateTime()%></td>
    <td align="left" bgcolor="#FFFFFF" style="padding-left:15px;">
	<%
	if ( jbpmMgr.getTaskIntVaviable(task.getId(),"alert")==JbpmMgr.WORKFLOW_ALERT )
	{
	%>
 <input name="Submit" type="button" class="short-button" value="执行任务" onClick="openWindow('<%=StringUtil.replaceString(task.getFormResourceName(),"$",jbpmMgr.getTaskStringVaviable(task.getId(),"id"))%>')"/>
 
	&nbsp;&nbsp;&nbsp;&nbsp;
	<input name="Submit2" type="button" class="short-button-ok" value="完成" onClick="next(<%=task.getId()%>)"/>
	<%
	}
	else
	{
	%>
	<input name="Submit2" type="button" class="long-button" value="执行任务" onClick="openBox('','<%=StringUtil.replaceString(task.getFormResourceName(),"$",jbpmMgr.getTaskStringVaviable(task.getId(),"id"))%>&task_id=<%=task.getId()%>',800,400)"/>
	<%
	}
	%>	</td>
  </tr> 
  
  <%
	}
}
else
{
%>
  <tr>
    <td colspan="4" align="center" valign="middle" bgcolor="#FFFFFF">暂无任务</td>
  </tr> 
<%
}
%>
</table>
<form name="refundForm" method="post">
<input type="hidden" name="rejectNote" id="rejectNote"/>
<input id="userName" name="userName" type="hidden"  />  
<input id="userAccount" name="userAccount" type="hidden"  /> 
<input id="taskId" name="taskId" type="hidden" /> 
<input id="tranID" name="tranID" type="hidden" /> 
<input id="oid" name="oid" type="hidden" />
<input type="hidden" name="result" id="result"/>	
<input type="hidden" name="modifyAmount" id="modifyAmount"/>	
<input type="hidden" name="modifyRefundType" id="modifyRefundType"/>	
</form>
</body>
</html>

