<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.List,org.jbpm.api.*,org.jbpm.api.task.Task,org.jbpm.api.history.*"%>
<%@page import="com.cwc.app.api.JbpmMgr"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="org.jbpm.examples.task.*" %>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--

-->
</style>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script>
function openWindow(url)
{
	window.open(url);
}
</script>

</head> 

<body >
<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC" class="zebraTable">
  <tr>
    <th width="20%" align="center" valign="middle" bgcolor="#eeeeee" style="font-weight:bold">任务描述</th>
    <th width="11%" align="center" valign="middle" bgcolor="#eeeeee" style="font-weight:bold">当前任务</th>
    <th width="14%" align="center" valign="middle" bgcolor="#eeeeee">创建日期</th>
    <th width="55%" bgcolor="#eeeeee">&nbsp;</th>
  </tr>
  <%
List<Task> taskList = jbpmMgr.getTaskListByGroupUser(String.valueOf(adminLoggerBean.getAdid()));
if (taskList.size()>0)
{

	for (Task task : taskList)
	{
%>
  <tr>
    <td height="35" align="center" bgcolor="#FFFFFF"><%=jbpmMgr.getTaskStringVaviable(task.getId(),"description")%></td> 
    <td align="center" bgcolor="#FFFFFF"><%=task.getActivityName()%></td>
    <td align="center" bgcolor="#FFFFFF"><%=task.getCreateTime()%></td>
    <td align="left" bgcolor="#FFFFFF" style="padding-left:15px;">
	<%
	if ( jbpmMgr.getTaskIntVaviable(task.getId(),"alert")==JbpmMgr.WORKFLOW_ALERT )
	{
	%>
		  <input name="Submit" type="button" class="short-button" value="预览" onclick="openWindow('<%=StringUtil.replaceString(task.getFormResourceName(),"$",jbpmMgr.getTaskStringVaviable(task.getId(),"id"))%>')"/>
	<%
	}
	else
	{
	%>
		<input name="Submit" type="button" class="short-button" value="预览" onclick="check(<%=task.getId()%>,'<%=StringUtil.replaceString(task.getFormResourceName(),"$",jbpmMgr.getTaskStringVaviable(task.getId(),"id"))%>&task_id=<%=task.getId()%>&cmd=pre')"/>
	<%
	}
	%>	  
		  &nbsp;&nbsp;&nbsp;
		<input name="Submit" type="button" class="long-button" value="接受任务" onclick="checkAccept(<%=task.getId()%>,'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/task/AcceptTask.action?task_id=<%=task.getId()%>')"/></td>
  </tr> 

  <%
	}
} 
else
{
%>
  <tr>
    <td colspan="4" align="center" valign="middle" bgcolor="#FFFFFF">暂无任务</td>
  </tr>
<%
}
%>
</table>
</body>
</html>
<script>
	
 
function check(taskId,oUrl)
{
	var param ="taskId="+ taskId;
	$.ajax({
	url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/checkCompleteProcessAction.action',
	//url:dataUrl,
	type: 'post',
	dataType: 'json',
	timeout: 60000,
	cache:false,
	data:param,
	beforeSend:function(request){
	},
	
	error: function(data){
	},
	
	success: function(data){
			if(data["rs"]=="false")
			{
				alert("提醒：该任务已被他人接受并执行！");
				window.location.reload(); 
			}
			else
			{
				openBox('',oUrl,800,400);
			}
	} 
	});
 
}



 
function checkAccept(taskId,oUrl)
{
	var param ="taskId="+ taskId;
	$.ajax({
	url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/checkCompleteProcessAction.action',
	//url:dataUrl,
	type: 'post',
	dataType: 'json',
	timeout: 60000,
	cache:false,
	data:param,
	beforeSend:function(request){
	},
	
	error: function(data){
	},
	
	success: function(data){
			if(data["rs"]=="false")
			{
				alert("提醒：该任务已被他人接受并执行！");
				window.location.reload(); 
			}
			else
			{
				location=oUrl;
			}
	} 
	});
 
}
</script>
