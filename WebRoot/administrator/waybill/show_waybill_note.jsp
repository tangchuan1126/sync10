<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.ProductEditTypeKey"%>
<%@page import="com.cwc.app.key.ProductEditReasonKey"%>
<%@page import="com.cwc.app.key.WaybillInternalOperKey"%>
<%@page import="com.cwc.app.api.zj.WaybillLogMgrZJ"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.key.WaybillTrackeTypeKey"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@ include file="../../include.jsp"%>
<%
	long waybill_id = StringUtil.getLong(request,"waybill_id");
	DBRow[] rows = waybillMgrZR.getWayBillNoteBy(waybill_id,0);
	WaybillTrackeTypeKey typeKey = new WaybillTrackeTypeKey();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>运单日志</title>
    
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

 
<script type="text/javascript">
	 
</script>
<style type="text/css">
table.zebraTable{border-collapse:collapse ;}
table.zebraTable{line-height:28px;height:28px;}
</style>
</head>
  
  <body onload="onLoadInitZebraTable()">
  	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  		<tr>
  			<td>
  				 
  				<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="false">
  					<tr>
  						<th nowrap="nowrap"  class="left-title " style="width:15%;vertical-align: center;text-align: center;">日志类型</th>
  						<th nowrap="nowrap"  class="left-title " style="width:10%;vertical-align: center;text-align: center;">操作人</th>
  						<th nowrap="nowrap"  class="left-title " style="width:40%;vertical-align: center;text-align: center;">内容</th>
  						<th nowrap="nowrap"  class="left-title " style="width:28%;vertical-align: center;text-align: center;">操作时间</th>
  						 
  					</tr>
  					<tbody>
  						<%
  							if(rows != null && rows.length > 0 ){
  								for(int index = 0 , count = rows.length ; index < count ; index++ ){
  									DBRow tempNote = rows[index];
  								%>
  									<tr>
  										<td><%= typeKey.getWaybillTrackeType(tempNote.get("trace_type",0)+"") %></td>
  										<td><%= tempNote.getString("employe_name") %></td>
  										<td><%= tempNote.getString("note")%></td>
  										<td><%= tempNote.getString("post_date") %></td>
  									</tr>		
  								<% 	 									
  								}
  							}
  						%>
  						 
  					</tbody>
  				</table>
  				 
  			</td>
  		</tr>
  	</table>
	  	
  </body>
</html>
