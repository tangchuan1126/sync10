<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>


<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<%
	int[] status = new int[2];
	status[0] = WayBillOrderStatusKey.PERINTED;
	status[1] = WayBillOrderStatusKey.WAITPRINT;
	long out_id = StringUtil.getLong(request,"out_id");
	
	DBRow outbound = outboundOrderMgrZJ.getDetailOutboundById(out_id);
	DBRow[] outProducts = outboundOrderMgrZJ.getOutOrderProductItems(out_id,status);

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>打印出库单</title>


<script>
  function makeBq(productId,num){
   var purchase_id="";
   var factory_type="";
   var supplierSid="";
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/lable_template_show.html?purchase_id='+purchase_id+'&pc_id='+productId+'&factory_type='+factory_type+'&supplierSid='+supplierSid+'&num='+num; 
	$.artDialog.open(uri , {title: '标签列表',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
  }

  function showImg(pc_id){
	  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_file_upload.html?pc_id='+pc_id;
	  $.artDialog.open(uri , {title: '查看图片',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	}

</script>
</head>
<body>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table"  style="margin-top:5px;">
	<thead>
		<tr>
			<td colspan="6">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td style="font-family: Arial Black;font-size: 19px; padding-bottom:10px;"><%=catalogMgr.getDetailProductStorageCatalogById(outbound.get("ps_id",0l)).getString("title")%>第<%=out_id%>号出库单</td>
						<td nowrap="nowrap" align="left" style="font-family: 黑体;font-size:large;padding-bottom:10px;">
							<%=DateUtil.NowStr()%>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							包含运单:
							<%
								DBRow[] wayBillOrders = wayBillMgrZJ.filterWayBillOrder("","",null,0,0,"",0,0,0,0,out_id,null,-1,0,0,0);
								for(int q=0;q<wayBillOrders.length;q++)
								{
									out.print(wayBillOrders[q].get("waybill_id",0l));
									out.print(" ");
								}
							%>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">商品名称</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">操作</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">所在运单编号</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">出库数量</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">当前库存</th>
		    <th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;border-right: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">存放位置</th>
		</tr>
	</thead>
	<tbody id="tbody">
	  <%
	   	if(null != outProducts && outProducts.length > 0)
	   	{
	   		int[] compensationValueStatus = new int[1];
	   		compensationValueStatus[0] = WayBillOrderStatusKey.WAITPRINT;
	   		for(int i=0;i<outProducts.length;i++)
	   		{
	   			if(outProducts[i].get("pc_id",0l)==0)
	   			{
	   				break;
	   			}
	  %>
	  		<tr>
	  			<td align="left" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;"><%=outProducts[i].getString("p_name")%></td>
	  			<td align="left" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;">
	  				<input class="short-short-button-ok" type="button" value="图片" onclick="showImg(<%=outProducts[i].get("pc_id",0l) %>)"/>
	  				<input type="button" class="long-button-mod" value="产品标签" onclick="makeBq(<%=outProducts[i].get("pc_id",0l) %>,0)"/>
	  				<input type="button" class="long-button-mod" value="外箱标签" onclick="makeBq(<%=outProducts[i].get("pc_id",0l) %>,1)"/>
	  			</td>
	  			<td align="left" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;">
	  				<%
	  					DBRow[] waybills = outboundOrderMgrZJ.getWayBillIdsByOutIdAndP(out_id,outProducts[i].get("pc_id",0l),status);
	  					for(int j=0;j<waybills.length;j++)
	  					{
	  						out.println(waybills[j].get("waybill_id",0l));
	  						out.println("");
	  					}
	  				%>
	  			</td>
	  			<td align="left" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;"><%=outProducts[i].get("compensationValue",0f)%></td>
	  			<td align="left" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;">
	  				<%
	  					DBRow compensationValue = outboundOrderMgrZJ.compensationValueForPsAndP(outbound.get("ps_id",0l),compensationValueStatus,outProducts[i].get("pc_id",0l));
	  					
	  					if(compensationValue==null)
	  					{
	  						compensationValue = new DBRow();
	  					}
	  					
	  					DBRow storage = productMgr.getDetailProductProductStorageByPcid(outbound.get("ps_id",0l),outProducts[i].get("pc_id",0l));
	  					if(storage==null)
	  					{
	  						storage = new DBRow();
	  					}
	  					
	  					out.print(storage.get("store_count",0f)+compensationValue.get("compensationValue",0f));
	  				%>
	  			</td>
	  			<td align="left" nowrap="nowrap" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;border-right: 2px #000000 solid;font-size: 10px;">&nbsp;
	  				<%
	  				  //DBRow product = productMgr.getDetailProductByPcid(outProducts[i].get("pc_id",0l));
	  				  //if(product==null)
	  				  //{
	  				  //	product = new DBRow();
	  				  //}
					  DBRow locations[] = productMgr.getStorageLocationByBarcode(outbound.get("ps_id",0l),outProducts[i].get("pc_id",0l),null);
					  for (int j=0; j<locations.length; j++)
					  {
					%>
						<div style="padding:1px;color:#000000;font-size:13px;"><%=locations[j].getString("position")%> : <span style="color:#0000FF"><%=locations[j].get("quantity",0f)%></span></div>
					<%
					  }
					%>
	  			</td>
	  		</tr>
	  <%  
	   		}
	   	}
	   	else
	   	{
	  %>
	   		<tr style="background:#E6F3C5;">
	   			<td colspan="13" style="height:120px;">无数据</td>
	   		</tr>
	  <% 
	   	}
	  %>
	</tbody>
</table>
</body>
</html>