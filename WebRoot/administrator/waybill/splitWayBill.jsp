<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>


<script type="text/javascript" src="../js/poshytip/jquery-1.4.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<style type="text/css">
 	*{font-size:12px;padding:0px;margin:0px;}	
 	div.splitBox{width:750px;height:500px;margin:0px auto;border:1px solid silver;padding-top:10px;}
 	div.left{height:480px;border:1px solid silver;float:left;width:330px;margin-left:7px;overflow:auto;}
 	div.middle{height:480px;border:1px solid silver;float:left;width:55px;margin-right:5px;margin-left:5px;}
 	div.right{height:480px;border:1px solid silver;float:left;width:330px;overflow:auto;}
  	div.right h1,div.left h1{background:#D3EB9A;height:25px;text-indent:10px;line-height:25px;border-bottom:1px solid silver;}
 	input.txt{width:25px;}
 	 
 	ul.newSplit li, ul.items li {line-height:25px;height:25px;border-bottom:1px solid silver;}
 	ul.newSplit li.even,ul.items li.even{background:#F9F9F9}
 	span.goodName{display:block;float:left;border:0px solid silver;width:160px;overflow:hidden;height:25px;text-indent:15px;}}
 	span.info{margin-left:15px;display:block;float:right;margin-right:10px;border:1px solid red;text-indent:0px;}
 	span.fixBox{display:block;float:left;margin-top:5px;margin-left:6px;}
 	span.fixInfo{display:block;float:left;}
 	.changeColor{background:#E6F3C5;}
 	.deleteFlag{ background-image: url("../imgs/standard_msg_error.gif");background-repeat: no-repeat;border: 0px solid silver;cursor: pointer;display: block; float:right;margin-right:10px;height: 16px;margin-top: 4px;width: 16px;}
	div.splitDiv{margin-top:5px;}
	span.fixName{width:230px;}
</style>
<style type="text/css" media="all">
@import "../js/thickbox/thickbox.css";
</style>

<%
	long wayBillId = StringUtil.getLong(request,"way_bill_id");
	DBRow[] rows = waybillMgrZR.getAllOrderItemsInWayBillById(wayBillId);
	
	
  	// 查询出这个运单所拆分的所有的运单。以及运单下的商品 和数量
  	DBRow[] splitRows = waybillMgrZR.getSplitInfoByWayBillId(wayBillId);
  	//uri
  	String deleteSplitWayBillAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/DeleteSplitWayBillAction.action";
  	String splitAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/SplitWayBillAction.action";
   	String getWayBillInfoAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/GetWayBillInfoAction.action";
%>
	<script type="text/javascript">
		var stateValues = [];
		stateValues.push({
			state:'0',
			value:'待打印',
			show:true
		});
		stateValues.push({
			state:'1',
			value:'已打印',
			show:true
		});
		stateValues.push({
			state:'2',
			value:'已发货',
			show:false
		});
		stateValues.push({
			state:'3',
			value:'已取消',
			show:false
		});
		stateValues.push({
			state:'4',
			value:'拆分中',
			show:false
		});
		var splitRows = <%= new JsonObject(splitRows).toString()%>;
		var objects  = [] ;
		jQuery(function($){
		 	fixNumber();
		 	initSplitWayBill();
		})
		function initSplitWayBill(){
			if(splitRows.length > 0){
				for(var index = 0 , count = splitRows.length ; index < count ; index++ ){
					var flag = true;
					var a  ; 
					for(var j =  0 , l = objects.length ; j < l ; j++ ){
						if(splitRows[index].waybill_id == objects[j].id){
							flag = false;
							a = j;
							break;
						}
					}
					if(flag){
						// 新添加一条数据
						var array = [];
						array.push(splitRows[index]);
						var o = {
								id : splitRows[index].waybill_id,
								values : array,
								status:splitRows[index].status
						}
						objects.push(o);
					}else{
						// 在原来的基础上添加一条信息
						objects[a].values.push(splitRows[index]);
					}
				}
				showTheSplitWay(objects);
			}
		}
		function showTheSplitWay(objects){
			for(var index = 0 ; index <  objects.length ; index ++){
				var div = "<div>";
				var deleteFlag = true;
				var fixh = "";
				for(var j = 0 , count = stateValues.length ; j < count ; j++ ){
					if(stateValues[j].state   ===  objects[index].status){
						fixh = "  "+ stateValues[j].value;
						deleteFlag = stateValues[j].show;
						break;
					} 
				}
		 		if(deleteFlag){
					div += "<h1>["+objects[index].id+"] "+fixh+" <span class='deleteFlag' onclick=cencelSplitWayBill('"+objects[index].id+"')></span></h1>";
			 	}else{
					div += "<h1>["+objects[index].id+"] "+fixh+"</h1>";
			 	}
				div += "<ul class='newSplit'>";
				var str = (objects[index].id) + "\n";
				for(var j = 0 ,count = objects[index].values.length ; j < count ; j++ ){
					div += "<li onmouseover='changeToIn(this);' onmouseout='changeToOld(this)'>";
					div += "<span class='fixName goodName' title='"+objects[index].values[j].p_name+"'>"+objects[index].values[j].p_name+"</span>";
					div += "<span class='info'>";
					div += "<span style='display:block;float:left;'>拆分量<b style='color:green;'> "+objects[index].values[j].quantity * 1+" </b></span>";
					div += "</span></li>";
				}
				div += "</ul></div>";
				$(".right").append($(div));	
			};
		}
		function fixNumber(){
			 
			$(".buyNumber").each(function(index){
				var _this = $(this);
				var value = _this.html() * 1;
				_this.html(value);
			 	var node = _this.parent().parent().find(".txt");
			 	 node.val(value);
			})
		}
		function split(){
			// 读取所有checkbox勾选的。然后生成左边的一些新的运单。然后是这个运单的。所排列的顺序插入
			var newSplit = $(".newSplit");
			
			var boxs = $(".box:checked");
			 
			if(boxs.length < 1){
				showMessage("请选择拆分项","alert")
				return ;
			}
			$("li",newSplit).remove();
			//进行一个ajax的提交数据
			//读取checkbox为true的。并且数据不是为0的。
			var _ids = "";
			var _numbers = "";
			boxs.each(function(index){
				var _this = $(this);
				var li = ($("#"+_this.attr("id").replace("check_","old_")));
				var splitNumber = $(".txt",li).val();
				_ids += ","+li.attr("id").replace("old_","");
				_numbers += "," + splitNumber;
			})
			
			if(_ids.length <  1 || _numbers.length < 1){
				showMessage("请选择拆分项","alert");
				return ;
			}
			//在进入拆分之前,首先应该查询一次看当前的运单是不是已经  "待打印 " 了.只有在待打印的时候才可能拆分。 否则给一个提示
			var _wayBillId  = $("#wayBillIdInput").val();
   		 	 
   	 
			$.ajax({
				url:'<%= getWayBillInfoAction%>' + "?waybill_id=" + _wayBillId,
				dataType:'json',
				success:function(data){
				 
					if(data && data.status){
						 if(data.status+"" === "0" || data.status+"" === "4"){
							 var params = { ids:_ids.substr(1), numbers:_numbers.substr(1) ,way_bill_id:_wayBillId};
					   		 var str = jQuery.param(params);
					   		 var uri = "print_center.html?ids="+_ids.substr(1)+"&numbers="+_numbers.substr(1)+"&way_bill_id="+_wayBillId+"&TB_iframe=true&height=320&width=600";
					   		tb_show('选择快递',uri,false);
						 }else{
							var mess = ""	
							 for(var index = 0 , count = stateValues.length ; index < count ; index++ ){
									if(stateValues[index].state == data.status){
										mess = stateValues[index].value ; 
										break;
									}
							}
							 showMessage("运单" + mess,"error");
						}
					}else{
						showMessage("系统错误","error");
					}
				}
			})
		}
		// 创建一个右边的Item
		function createNewItemRight(index,newSplit,$li){
			var li = "<li onmouseover='changeToIn(this);' onmouseout='changeToOld(this)' id='"+$li.attr("id").replace("old_","new_")+"'>";
			li  += "<span class='goodName' title='"+$("#name_index_"+index).html()+"'>"+$("#name_index_"+index).html()+"</span>";
			li  += "<span class='info'>"
			li  += "<span style='display:block;float:left;'>拆分量<b style='color:green;'> "+$("#input_number_"+index).val()+"</b></span>";
			li	+= "</span></li>";
			newSplit.append($(li));
			 
		}
		// 清除有一个运单下的所有的item
		function  cencelSplitWayBill(wayBillId){
			$.ajax({
				url:'<%= deleteSplitWayBillAction%>'+"?way_bill_id=" +wayBillId + "&parent_bill_id=" +$("#wayBillIdInput").val() ,
				dataType:'text',
				success:function(data){
					if(data ==="success"){
						window.location.reload();
					}else{
						showMessage("系统错误","error");
					}
				}
			})	
			
		} 
	</script>
</head>
<body>
 	<div class="splitBox ui-corner-all">
 		<input id="wayBillIdInput" type="hidden" value="<%=wayBillId %>" />
 			<div class="left">
 				<h1><span>原始运单  [<%=wayBillId %>]</span> <span><input type="checkbox" id="selectAll" checked onclick="selectAll();" style="float:right;margin-top:5px;margin-right:14px;"/></span></h1>
 				
 				<ul class="items">
 			 
 					<%
 						if(rows != null && rows.length > 0){
 							for(int index = 0 ; index < rows.length ;index++ ){
 								  
 							 
 					%>
 				 <li  id="old_<%=rows[index].getString("waybill_order_item_id")  %>" class='<%= index%2 == 1 ? "even":"" %>' onmouseover="changeToIn(this);" onmouseout="changeToOld(this)">
 						<span class="goodName" id="name_index_<%=index %>" title='<%= rows[index].getString("p_name") %>'><%= rows[index].getString("p_name") %></span>
 						<span class="info">
 							<span style="display:block;float:left;width:70px;">待发量 <b style="color:green;" class="buyNumber"><%= rows[index].getString("quantity") %></b></span>
 							<% if(rows[index].get("quantity",0.0d) > 0 ){%>
 								 
 							<span style="display:block;float:left;">拆分量<input type="text" class="txt" id='input_number_<%=index %>' onkeyup="inputValue(this);"  value='<%= rows[index].getString("quantity") %>'/></span>
 							<span style="" class="fixBox"><input type="checkbox" class="box" id='check_<%=rows[index].getString("waybill_order_item_id")  %>' onclick="singleClick(this)" checked/></span>
 							<%} %>
 						</span>
 					</li>
 					<% 			
 							}
 						}
 					%>
 				</ul>
 				
 				
 			</div>
 			<div class="middle">
 				<input type="button" value="拆分" style="margin-top:150px; margin-left: 5px;" class="short-short-button-convert" onclick="split()"/>
 				 
 			</div>
 			<div class="right">
 			<!--   
	 			<div class="splitDiv" style="margin-top:0px;">
	 					<h1>新生成运单 </h1>
		 				<ul class="newSplit">
		 					<li onmouseover="changeToIn(this);" onmouseout="changeToOld(this)">
		 						<span class="goodName" title='商品名称萨的份上多发实得分'>CABLE/D2RSC</span>
		 						<span class="info">
		 							<span style="display:block;float:left;">拆分量<b style="color:green;"> 6.0</b></span>
		 						</span>
		 					</li>
		 					 <li class="even" onmouseover="changeToIn(this);" onmouseout="changeToOld(this)"> 
		 						<span class="goodName" title='商品名称萨的份上多发实得分'>商品名称</span>
		 						<span class="info">
		 							<span style="" class="fixInfo">拆分量 <b style="color:green;"> 6.0</b></span>
		 						</span>
		 					</li>
		 				</ul>
	 				</div>
 				
 				
		 			<div class="splitDiv">
		 					<h1>新生成运单 </h1>
			 				<ul class="newSplit">
			 					<li onmouseover="changeToIn(this);" onmouseout="changeToOld(this)">
			 						<span class="goodName" title='商品名称萨的份上多发实得分'>CABLE/D2RSC</span>
			 						<span class="info">
			 							<span style="display:block;float:left;">拆分量<b style="color:green;"> 6.0</b></span>
			 					 
			 						</span>
			 					</li>
			 					 <li class="even" onmouseover="changeToIn(this);" onmouseout="changeToOld(this)"> 
			 						<span class="goodName" title='商品名称萨的份上多发实得分'>商品名称</span>
			 						<span class="info">
			 							<span style="" class="fixInfo">拆分量 <b style="color:green;"> 6.0</b></span>
			 					 
			 						</span>
			 					</li>
			 				</ul>
		 			</div>
 				-->
 			</div>
 	</div>
 
<script type="text/javascript">
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
	 var  _self = $("body"),
	_stateBox = $("<div />").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
	 }
}
	var oldBack = "";
	function changeToIn(_this){
	 var node = $(_this);
	 oldBack = node.css("background");
	 node.css("background","#E6F3C5")
	}
	function changeToOld(_this){
		 var node = $(_this);
		 node.css("background",oldBack);
		 oldBack= "";
	}
	// checkAll checkBox
	function selectAll(){
		var selectAll = $("#selectAll");
		if(selectAll.attr("checked")){
			$(".box").each(function(){
				var node = $(this);
				var li = node.parent().parent();
				$(".txt",li).val($(".buyNumber",li).html() * 1);
				node.attr("checked",true);
			})
		}else{
			$(".box").attr("checked",false);
			$(".txt").val("0");
		}
	}
	function singleClick(_this){
		var node = $(_this);
		if(node.attr("checked")){
			$("#selectAll").attr("checked",checkIsAllChecked());
			var li = node.parent().parent();
			$(".txt",li).val($(".buyNumber",li).html() * 1);
		}else{
			$(".txt",node.parent().parent()).val(0);
			$("#selectAll").attr("checked",false);
		}
	}
	function checkIsAllChecked(){
		var boxs = $(".box");
		var flag = true;
		for(var index = 0 ,count = boxs.length; index < count ;index++ ){
			if(!$(boxs[index]).attr("checked")){
				flag = false;
				break;
			}
		} 
		return flag;
		
	}
	function inputValue(_this){
	 
		var node = $(_this);
		var value = node.val();
		var bValue = $(".buyNumber",node.parent().parent()).html() * 1;
	  
		value = (value.replace(/[^0-9]/g,''));
	 
		// 如果是0 那么就取消当前的勾选。
		if(value * 1 == 0){
			$(".box",node.parent().parent()).attr("checked",false); 
			node.val(0);
		}else{
			var li = node.parent().parent();
			 
			if( bValue < value *1 ){
				showMessage("输入值因该小于购买值","alert");
				node.val(bValue * 1);
				$(".box",li).attr("checked",true); 
			 
			}else{
				node.val(value * 1);
				$(".box",li).attr("checked",true); 
			}
		}
		$("#selectAll").attr("checked",checkIsAllChecked());
	}
	function closeWindow(){
		tb_remove();
	}
	function closeWindowAndRefresh(){
		tb_remove();
		window.location.reload();
	}
</script>
</body>
</html>