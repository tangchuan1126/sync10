<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	long out_id = StringUtil.getLong(request,"out_id");
	DBRow[] outProducts = transportMgrZwb.selectOutDetail(out_id);
	DBRow[] slcRow=transportMgrZwb.findSlcId(out_id);
	DBRow outRow= waybillMgrZwb.selectOutOrder(out_id);
	DBRow[] doorOrLocation=waybillMgrZwb.getStorageOrDoorByOutId(ProductStoreBillKey.PICK_UP_ORDER,out_id);
%>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0"  style="margin-top:5px;">
	<tr>
		<td>
			<div>
				<span style="font-size:19px; font-weight: bold;"><%=catalogMgr.getDetailProductStorageCatalogById(outRow.get("ps_id",0l)).getString("title")%>仓库<%=out_id %>号拣货单</span>
			</div>
			<div align="right">
				<span><%= outRow.getString("create_time").substring(5,16)%></span>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
				<tr>
					<td style="background-color:#eeeeee;width:200px; border-left:2px solid #000000;border-right:2px solid #000000;border-top:2px solid #000000">DOOR</td>
					<td style="background-color:#eeeeee;border-top:2px solid #000000;border-right:2px solid #000000;">
					 	<%for(int a=0;a<doorOrLocation.length;a++){ %>
					 	    <%=doorOrLocation[a].getString("door")%>
					 	<%} %>
					</td>
				</tr>
				<tr>
					<td style="background-color:#eeeeee; border-left:2px solid #000000;border-right:2px solid #000000;border-top:2px solid #000000;">LOCATION</td>
					<td style="background-color:#eeeeee;border-top:2px solid #000000;border-right:2px solid #000000;">
						<%for(int b=0;b<doorOrLocation.length;b++){ %>
					 	    <%=doorOrLocation[b].getString("location")%>
					 	<%} %>
					</td>
				</tr>		
			</table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" >
				<thead>
					<tr>
						<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">商品名称</th>
						<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">商品ID</th>			
					    <th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;border-right: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">出库数量</th>
					</tr>
				</thead>
				<tbody id="tbody">
				<%for(int a=0;a<slcRow.length;a++){ %>
				   <tr>
			  			<td colspan="4" align="left" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;border-right: 2px #000000 solid;background-color:#eeeeee">
			  				<%DBRow weizhi= waybillMgrZwb.findStorageLocationCatalogById(slcRow[a].get("slc_id",0l));%>
			  				仓库位置：<%=weizhi.getString("slc_position_all") %>
			  			</td>
			  	   </tr>
			  	   <%for(int i=0;i<outProducts.length;i++){ %>
			  	   	   <%if(outProducts[i].get("slc_id",0l)==slcRow[a].get("slc_id",0l)){ %>
				  	   <tr>
				  	  	   <td align="center" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;">
				  	       		<%DBRow pcRow=waybillMgrZwb.findProductById(outProducts[i].get("pc_id",0l)); %>
				  	       		<%=pcRow.getString("p_name") %>
				  	       </td>
				  	       <td align="center" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;">
				  	       		<%=outProducts[i].get("pc_id",0l) %>
				  	       </td>
				  	       <td align="center" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;border-right: 2px #000000 solid;">
				  	       		<%=outProducts[i].get("quantity",0d) %>
				  	       </td>	  	    
				  	   </tr>
				  	   <%} %>
			  	   <%} %>
				<%} %>
				</tbody>
			</table>
		</td>
	</tr>
</table>
