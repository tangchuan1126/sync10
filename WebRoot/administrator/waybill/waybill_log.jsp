<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.ProductEditTypeKey"%>
<%@page import="com.cwc.app.key.ProductEditReasonKey"%>
<%@page import="com.cwc.app.key.WaybillInternalOperKey"%>
<%@page import="com.cwc.app.api.zj.WaybillLogMgrZJ"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@ include file="../../include.jsp"%>
<%
	
	long adid = StringUtil.getLong(request,"operator_adid"); 
	int waybill_internal_status = StringUtil.getInt(request,"waybill_internal_status");
	int waybill_external_status = StringUtil.getInt(request,"waybill_external_status");
	String trackingNumber = StringUtil.getString(request,"trackingNumber");
	
	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
									
	String input_st_date,input_en_date;
	if ( st.equals("") )
	{	
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		st = input_st_date;
	}
	else
	{	
		input_st_date = st;
	}
									
	if ( en.equals("") )
	{	
		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		en = input_en_date;
	}
	else
	{	
		input_en_date = en;
	}
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	WaybillInternalOperKey waybillInternalOperKey = new WaybillInternalOperKey(); 
	WaybillInternalTrackingKey waybillInternalTrackingKey = new WaybillInternalTrackingKey();
	
	DBRow[] rows = waybillLogMgrZJ.filterWayBillLog(trackingNumber,input_st_date,input_en_date,waybill_internal_status,waybill_external_status,adid,pc);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>运单日志</title>
    
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


</head>
  
  <body>
  	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  		<tr>
  			<td>
  				<form action="waybill_log.html">
  				<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:950px;">
			 		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			 			<tr>
			 				<td>
			 					<table border="0" align="left" cellpadding="3" cellspacing="0">
			 						<tr>
			 							<td>
			 								外部运单号:<input type="text" id="trackingNumber" name="trackingNumber" value="<%=trackingNumber%>"/>
			 							</td>
						 				<td>
						 					<input name="st" type="text" id="statistics_order_st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
						 					至
						 					<input name="en" type="text" id="statistics_order_en"  value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
						 				</td>
						 				<td>
						 					<select name="operator_adid" id="operator_adid" style="border:1px #CCCCCC solid" >
									          <option value="0">选择操作人</option>
												<%
												DBRow admins[] = adminMgr.getAllAdmin(null);
												for (int i=0; i<admins.length; i++)
												{
												%>
												   <option value="<%=admins[i].getString("adid")%>" <%=admins[i].get("adid",0l)==adid?"selected":""%>> 
												      <%=admins[i].getString("employe_name")%>
												   </option>
												<%
												}
												%>
									        </select>
						 				</td>
						 				<td>
						 					<select name="waybill_internal_status" id="waybill_internal_status" style="border:1px #CCCCCC solid" >
									          <option value="0">选择操作</option>
												<%
						   							ArrayList waybillInternalStatus = waybillInternalOperKey.getWaybillInternalOper();
						   							
						   							for(int i = 0;i<waybillInternalStatus.size();i++)
						   							{
						   						%>
						   						<option <%=waybillInternalStatus.get(i).toString().equals(String.valueOf(waybill_internal_status))?"selected=\"selected\"":""%> value="<%=waybillInternalStatus.get(i)%>"><%=waybillInternalOperKey.getWaybillInternalOper(waybillInternalStatus.get(i).toString())%></option>
						   						<%
						   							}
						   						%>
									        </select>
						 				</td>
						 				<td>
						 					<select name="waybill_external_status" id="waybill_external_status" style="border:1px #CCCCCC solid" >
									          <option value="-1">追踪状态</option>
												<%
						   							ArrayList waybillInternalTrack = waybillInternalTrackingKey.getWaybillInternalTrack();
						   							
						   							for(int i = 0;i<waybillInternalTrack.size();i++)
						   							{
						   						%>
						   						<option <%=waybillInternalTrack.get(i).toString().equals(String.valueOf(waybill_external_status))?"selected=\"selected\"":""%> value="<%=waybillInternalTrack.get(i)%>"><%=waybillInternalTrackingKey.getWaybillInternalTrack(waybillInternalTrack.get(i).toString())%></option>
						   						<%
						   							}
						   						%>
									        </select>
						 				</td>
						 				<td>
						 					<input name="Submit" type="submit" class="button_long_refresh" value="过滤">
						 				</td>
						 			</tr>
							 		<script type="text/javascript">
							 			$("#statistics_order_st").date_input();
										$("#statistics_order_en").date_input();
							 		</script>
				 				</table>
			 				</td>
			 			</tr>
			 		</table>
				</div>
				</form>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<br/>
  				<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" class="zebraTable" >
  					<tr>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">内部运单号</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">外部运单号</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">操作</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">外部状态</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">操作人</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">操作时间</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">StatusCode</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">Definition</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">TrackingWeight</th>
  					</tr>
  					<%
  						for(int i = 0;i<rows.length;i++)
  						{
  					%>
	  					<tr>
	  						<td height="30"><%=rows[i].getString("waybill_id")%></td>
	  						<td><%=rows[i].getString("trackingNumber")%>&nbsp;</td>
	  						<td><%=waybillInternalOperKey.getWaybillInternalOper(rows[i].getString("waybill_internal_status"))%></td>
	  						<td>
	  							<%=waybillInternalTrackingKey.getWaybillInternalTrack(rows[i].getString("waybill_external_status"))%>&nbsp;
							</td>
	  						<td><%=adminMgr.getDetailAdmin(rows[i].get("operator_adid",0l)).getString("employe_name")%></td>
	  						<td><%=rows[i].getString("operator_time")%></td>
	  						<td><%=rows[i].getString("tracking_status_code")%>&nbsp;</td>
	  						<td><%=rows[i].getString("tracking_definition")%>&nbsp;</td>
	  						<td><%=rows[i].getString("tracking_weight")%>&nbsp;</td>
	  					</tr>
  					<%
  						}
  					%>
  				</table>
  				<br>
				<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				  <form name="dataForm" method="post">
				     <input type="hidden" name="p">
				     <input type="hidden" name="operator_adid" value="<%=adid%>">
				     <input type="hidden" name="waybill_internal_status" value="<%=waybill_internal_status%>">
				     <input type="hidden" name="st" value="<%=input_st_date%>">
				     <input type="hidden" name="en" value="<%=input_en_date%>">
				  </form>
				        <tr> 
				          
				    <td height="28" align="right" valign="middle"> 
				      <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
				%>
				      跳转到 
				      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
				      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
				    </td>
				        </tr>
				</table> 
  			</td>
  		</tr>
  	</table>
	  	
  </body>
</html>
