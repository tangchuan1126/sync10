<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	int[] status = new int[2];
	status[0] = WayBillOrderStatusKey.PERINTED;
	status[1] = WayBillOrderStatusKey.WAITPRINT;
	long out_id = StringUtil.getLong(request,"out_id");
	
	DBRow outbound = outboundOrderMgrZJ.getDetailOutboundById(out_id);
	DBRow[] outProducts = outboundOrderMgrZJ.getOutOrderProductItems(out_id,status);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>运单处理</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<link type="text/css" href="../comm.css" rel="stylesheet" />
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>


<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />


<style type="text/css">
div.dingdan1{height:100px; border: 1px solid #DDDDDD;border-radius: 7px 7px 7px 7px;padding: 5px 5px 5px 15px;width:1050px;margin-bottom:5px;}
table.fixTable td{height:30px;}
.zebraTable tr td{height:30px;}
.zebraTable tr td a:link {color:#000000;text-decoration: none;font-weight:bold;font-size:13px;}
.zebraTable tr td a:visited {color: #000000;text-decoration: none;font-weight:bold;font-size:13px;}
.zebraTable tr td a:hover {color: #000000;text-decoration: underline;font-weight:bold;font-size:13px;}
.zebraTable tr td a:active {color: #000000;text-decoration: none;font-weight:bold;font-size:13px;}
span.create{display:block;float:left;border:1px solid red;cursor:pointer;}
</style>

</head>
<body>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table"  style="margin-top:5px;">
	<thead>
		<tr>
			<th width="8%"   class="right-title"  style="text-align: center;">商品名称</th>
			<th width="8%"   class="right-title"  style="text-align: center;">出库数量</th>
			<th width="8%"   class="right-title"  style="text-align: center;">当前库存</th>
			<th width="8%"  class="left-title" style="text-align: center;">所属仓库</th>
		    <th width="8%"  class="right-title"  style="text-align: center;" >所属出库单</th>
		    <th width="8%"  class="right-title"  style="text-align: center;" >存放位置</th>
		</tr>
	</thead>
	<tbody id="tbody">
	  <%
	   	if(null != outProducts && outProducts.length > 0)
	   	{
	   		int[] compensationValueStatus = new int[1];
	   		compensationValueStatus[0] = WayBillOrderStatusKey.WAITPRINT;
	   		for(int i=0;i<outProducts.length;i++)
	   		{
	   			if(outProducts[i].get("pc_id",0l)==0)
	   			{
	   				break;
	   			}
	  %>
	  		<tr>
	  			<td><%=outProducts[i].getString("p_name")%></td>
	  			<td><%=outProducts[i].get("compensationValue",0f)%></td>
	  			<td>
	  				<%
	  					DBRow compensationValue = outboundOrderMgrZJ.compensationValueForPsAndP(outbound.get("ps_id",0l),compensationValueStatus,outProducts[i].get("pc_id",0l));
	  					
	  					if(compensationValue==null)
	  					{
	  						compensationValue = new DBRow();
	  					}
	  					
	  					DBRow storage = productMgr.getDetailProductProductStorageByPcid(outbound.get("ps_id",0l),outProducts[i].get("pc_id",0l));
	  					if(storage==null)
	  					{
	  						storage = new DBRow();
	  					}
	  					
	  					out.print(storage.get("store_count",0f)+compensationValue.get("compensationValue",0f));
	  				%>
	  			</td>
	  			<td><%=catalogMgr.getDetailProductStorageCatalogById(outbound.get("ps_id",0l)).getString("title")%></td>
	  			<td><%=out_id%></td>
	  			<td>
	  				<%
	  				  //DBRow product = productMgr.getDetailProductByPcid(outProducts[i].get("pc_id",0l));
	  				  //if(product==null)
	  				  //{
	  				  //	product = new DBRow();
	  				  //}
					  DBRow locations[] = productMgr.getStorageLocationByBarcode(outbound.get("ps_id",0l),outProducts[i].get("pc_id",0l),null);
					  for (int j=0; j<locations.length; j++)
					  {
					%>
						<div style="padding:3px;color:#000000;font-weight:bold;font-size:13px;"><%=locations[j].getString("position")%> : <span style="color:#0000FF"><%=locations[j].get("quantity",0f)%></span></div>
					<%
					  }
					%>
	  			</td>
	  		</tr>
	  <%  
	   		}
	   	}
	   	else
	   	{
	  %>
	   		<tr style="background:#E6F3C5;">
	   			<td colspan="13" style="height:120px;">无数据</td>
	   		</tr>
	  <% 
	   	}
	  %>
	</tbody>
	<tr>
		<td><input type="button" value="打印" onclick="print()"/></td>
	</tr>
</table>
<script>
	function printOutbound()
	{
		visionariPrinter.PRINT_INIT("出库单");
		visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
		visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../waybill/out_list_print.html?out_id=<%=out_id%>");
		if(visionariPrinter.PREVIEW()>0)
		{
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/outbound/closeOutbound.action',
					type: 'post',
					dataType: 'html',
					timeout: 30000,
					cache:false,
					data:"out_id=<%=out_id%>",
									
				beforeSend:function(request){
				},
									
				error: function(){
					alert("打印出库单失败，请重新打印");
				},
									
				success: function(msg){
					if (msg!="ok")
					{
						alert("打印出库单失败，请重新打印");
					}
				}
			});
		}
	}
</script>
</body>
</html>