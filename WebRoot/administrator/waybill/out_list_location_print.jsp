<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	int[] status = new int[2];
	status[0] = WayBillOrderStatusKey.PERINTED;
	status[1] = WayBillOrderStatusKey.WAITPRINT;
	long out_id = StringUtil.getLong(request,"out_id");
	
	DBRow outbound = outboundOrderMgrZJ.getDetailOutboundById(out_id);
	DBRow[] outProducts = outboundOrderMgrZJ.getOutOrderItemLocation(out_id,status);

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>打印出库单</title>
</head>
<body>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table"  style="margin-top:5px;">
	<thead>
		<tr>
			<td colspan="5">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td style="font-family: Arial Black;font-size: 19px; padding-bottom:10px;"><%=catalogMgr.getDetailProductStorageCatalogById(outbound.get("ps_id",0l)).getString("title")%>第<%=out_id%>号出库单</td>
						<td nowrap="nowrap" align="left" style="font-family: 黑体;font-size:large;padding-bottom:10px;">
							<%=DateUtil.NowStr()%>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							包含运单:
							<%
								DBRow[] wayBillOrders = wayBillMgrZJ.filterWayBillOrder("","",null,0,0,"",0,0,0,0,out_id,null,-1,0,0,0);
								for(int q=0;q<wayBillOrders.length;q++)
								{
									out.print(wayBillOrders[q].get("waybill_id",0l));
									out.print(" ");
								}
							%>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;border-right: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">存放位置</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">商品名称</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">位置数</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">出库数</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-right: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		</tr>
	</thead>
	<tbody id="tbody">
	<%
	   	if(null != outProducts && outProducts.length > 0)
	  	{
	  		for(int i = 0;i<outProducts.length;i++)
	  		{
	%>
		<tr>
	  		<td align="center" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 14px;"><%=outProducts[i].getString("position")%></td>
	  		<td align="left" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 14px;"><%=outProducts[i].getString("p_name")%></td>
	  		<td align="center" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size:14px;"><%=outProducts[i].get("quantity",0f)%></td>
	  		<td align="center" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size:14px;"><%=outProducts[i].get("compensationValue",0f)%></td>
	  		<td align="center" style="border-left: 2px #000000 solid;border-right: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size:14px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	  	</tr>
	  <%  
	  		}
	   	}
	   	else
	   	{
	  %>
	   		<tr style="background:#E6F3C5;">
	   			<td colspan="13" style="height:120px;">无数据</td>
	   		</tr>
	  <% 
	   	}
	  %>
	</tbody>
</table>
</body>
</html>