<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.api.OrderMgr"%>
<%@page import="com.cwc.app.key.WeightDifferentKey"%>
<%@page import="com.fedex.ship.stub.WeightUnits"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>运单处理</title>

 <%
 	String st = StringUtil.getString(request,"st");
 String en = StringUtil.getString(request,"en");
  String p_name = null;
  AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
  String order_source = "" ;
  long ps_id = 0l;
  long ca_id = 0l;
  long ccid = 0l;
  long pro_id = 0l;
  long out_id = 0l;
  long pcid = 0l;
  long pro_line_id =  0l;
  long adid =  0l;
  DBRow loginUserInfo = null ;
  int search_field = 0;
  String search_key = "";
  long psid = StringUtil.getLong(request,"ps_id",-1);
  DBRow[] store = null;
  String cmd = StringUtil.getString(request,"cmd");
  int status = StringUtil.getInt(request,"status");
  
 adid = adminLoggerBean.getAdid(); 
 loginUserInfo = adminMgr.getDetailAdmin(adid);

 psid = Long.parseLong(loginUserInfo.getString("ps_id"));

 //查询出该用户所在的仓库下的所有开放的出库单
 store =  waybillMgrZR.getAllStoreIsOpenAndPsId(psid);
  
 	  p_name = StringUtil.getString(request,"p_name");
 	  order_source = StringUtil.getString(request,"order_source");
 	  ps_id = StringUtil.getLong(request,"ps_id",adminLoggerBean.getPs_id());
 	  ca_id = StringUtil.getLong(request,"ca_id");
 	  ccid = StringUtil.getLong(request,"ccid");
 	  pro_id = StringUtil.getLong(request,"pro_id");
 	  out_id = StringUtil.getLong(request,"out_id");
 	  pcid = StringUtil.getLong(request,"pcid");
 	  pro_line_id = StringUtil.getLong(request,"pro_line_id");
 	  search_field = StringUtil.getInt(request,"search_field");
 	  search_key = StringUtil.getString(request,"search_key");
 	  
 int trace_type = StringUtil.getInt(request,"trace_type");
  
  
  
 // uri
 String addNewStoreAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/AddNewStoreAction.action";
 String assignStoreAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/AssignStoreAction.action";
 String getAllOpenStoreByPsId =  ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/GetAllOpenStoreByPsIdAction.action";
 String updateWallBillOutId = ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/UpdateWallBillOutIdAction.action";
 String ajaxGetWayBillInfoList = ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/AjaxGetWayBillInfoAction.action";
 String getWayBillInfoAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/GetWayBillInfoAction.action";

 TDate tDate = new TDate();
 tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

 String input_st_date,input_en_date;
 if ( st.equals("") )
 {	
 	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
 }
 else
 {	
 	input_st_date = st;
 }


 if ( en.equals("") )
 {	
 	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
 }
 else
 {	
 	input_en_date = en;
 }

 WaybillInternalTrackingKey  waybillInternalTrackingKey = new WaybillInternalTrackingKey();
 %>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script language="javascript" src="../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../js/print/m.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

 
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
 
 
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		

<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<!-- 右键弹出JS -->
<script src="../js/contextMenu/jquery.contextMenu.js" type="text/javascript"></script>
<link type="text/css" href="../js/contextMenu/jquery.contextMenu.css" rel="stylesheet"/>
 <style type="text/css">
 
table.fixTable td{height:30px;}
.zebraTable tr td{height:30px;}
.zebraTable tr td a:link {color:#000000;text-decoration: none;font-weight:bold;font-size:13px;}
.zebraTable tr td a:visited {color: #000000;text-decoration: none;font-weight:bold;font-size:13px;}
.zebraTable tr td a:hover {color: #000000;text-decoration: underline;font-weight:bold;font-size:13px;}
.zebraTable tr td a:active {color: #000000;text-decoration: none;font-weight:bold;font-size:13px;}
span.create{display:block;float:left;border:1px solid red;cursor:pointer;}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
	div.innerDiv{margin:0 auto; margin-top:200px;text-align:center;border:1px solid silver;width:400px;height:30px;background:white;line-height:30px;font-size:14px;}
 	body{text-align:left;}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:390px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:380px; 
	height:29px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>
 
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- popmenu -->
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

 

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<!-- tabs -->
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript">
var store = <%= new JsonObject(store).toString()%>;

</script>
 
 
<script type="text/javascript">
function selectTracking(tracking_status)
{
	if(tracking_status>0)
	{
		$("#waybill_status").val("<%=WayBillOrderStatusKey.SHIPPED%>");
	}
}
 //
 //主要是打印回来过后,然后根据判断是不是 filter 或者是  search 。然后选择一个方法去 ajaxGeiInfoList() 
 function refresh(){
	if($("#query").css("display") === "block"){
		if($("#r_cmd_value").val() === "query")
		{
			queryClick();
		}
		else if($("#r_cmd_value").val() === "waybill")
		{
			queryWayBill();
		}
		else if($("#r_cmd_value").val() === "waybillId")
		{
			queryWayBillId();
		}
 
	}
	if($("#filter").css("display") === "block"){
		filter();
	}
 }
 
 function printLacking()
 {
 	visionariPrinter.PRINT_INIT("出库单");
	visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
	visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../waybill/lacking_waybill_list_print.html?ps_id="+$("#cid").val());
	visionariPrinter.PREVIEW();
 }
 
$().ready(function() {

//	 var width = $(window).width();
//	 if(width < 1200){
//	   $(".dataList").css("width","1200px");
//	   $("#fixWidth").css("width","1200px");
//	 }

	function log(event, data, formatted) {
		
	}
	// autoComlete

	var s = addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",	  
			"merge_info",
			"p_name");
	addAutoComplete($("#oid"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/GetSearchWayBillJSONAction.action",	  
			"merge_field","waybill_id");
	
	$("#oid").keydown(function(event){
		if (event.keyCode==13)
		{
			queryWayBillId();
		}
	});

});



	var tag;
 	jQuery(function(){
		initTag();
		initModeForm();
 	})
 	function inTabs(){
 		$("#tabs_").tabs({});
 	}
 	function initModeForm(){
 	 	$("#storeList").live("change",function(){
 	 	 	$("#outId").val($(this).val());
 	 	 		
 	 	});
 	}
 	function create(array){
 		 //创建一个新的Tag
		 var str = "<li class='ui-corner-top ui-state-default' unselectable='on' style='-moz-user-select: none;' onmouseover='changeToHover(this);' onmouseout='changToDefault(this)' onclick='clickLi(this);'>";
			 str += "<a href='#' value='"+array[0]+"' style='color: black;'>第&nbsp;"+array[0]+"号出库单</a></li>";
	  	$("#stWidthChecker").append($(str));
	  	//然后在selectOutIds 中添加一条数据
	  	var selectOutIds = $("#selectOutIds");
	  	var option = "<option value="+array[0]+">第&nbsp;"+array[0]+"&nbsp;出库单</option>";
	  	selectOutIds.append($(option));
 	}
 	
 	

 	//1.如果是在这个仓库下没有出库单那么新创建(开放的)一个。
 	//2.那么就把新选择的运单放在这个出库单中(那么这个时候就是要在上面的tag中添加一个新的 )
 	//3.如果是只有唯一的一个是开放的。那么就把新选择的运单放在这个出库单中
 	//4.如果是有多个开放的那么就弹出来一个窗口让他选择是那个出库单
 	//5.多个运单出库的时候要判断是不是同一个下的 仓库如果不是的就给出提示。
    function assignStore(billId,printPage){
    	 
    	
        var _ids = billId || getSelectedIds();
        // 加上如果是Ids 中的数据不是待打印就要提示他 那个运单不是待打印d
        if(_ids.length < 1){showMessage("请先选择运单","alert");return ;};
        var isAllStatusInOkFlag = isAllStatusIsOk(_ids);
        if(!isAllStatusInOkFlag){showMessage("所选择的运单并非全是待打印","alert"); return ;}
        // 是不是所有选择的运单都在同一个仓库下
		var array = _ids.split(",");
		var _psId = "";
		var flag = true;
		for(var index  =0 , count = array.length ; index < count ; index++ ){
			var node =  $("#id_" + array[index]);
		 	var value = node.attr("psId");
		 	if(index == 0){
		 		_psId = value;
		 		continue;
			 }else{
				 if(_psId*1 != value * 1){
					 flag = false;
					 break;
				 }
			 }
		}
		if(!flag){
			 showMessage("所选择的运单不是在同一个仓库下","alert")
			return ;
		}
        var params = {ids:_ids, ps_id:_psId };
         var str = jQuery.param(params);
        
       
		$.ajax({
			url:'<%= assignStoreAction%>',
			dataType:'json',
			data:str,
			success:function(data){
				 if(data.flag ==="success"){
					 if(data.state === "selectstore"){
						 //选择一个出库单
						 modRole(billId,printPage,_psId);
					  }
					  if(data.state === "addinnew"){
						  //添加到新的里面.然后就是要让页面刷新一次
						  $("#sub_out_id").val(data.stroe);
						  if(printPage){
					  		 printWayBill(billId,printPage);
					    	}
						  ajaxGetInfoList();
					  }
					  if(data.state === "addinonlyone"){
						  $("#sub_out_id").val(data.stroe);
						  if(printPage){
					  		 printWayBill(billId,printPage);
					    	}
						 ajaxGetInfoList();
					  }
				  }
				    
				 
		 	}
		})
        
    }
    // 是不是所有选择的运单是待打印的。只有待打印的运单才能选择出库通知单
    function isAllStatusIsOk(ids){
    	var flag = true;
    	var array = ids.split(",");
    	if(array && array.length > 0){
        	for(var index = 0, count = array.length ; index < count ; index++ ){
        		var node =  $("#id_" + array[index]);
        		var status = node.attr("status") * 1;
        		if(status != 0){
            		flag = false;
            		break;
            	}
            }
        }
        return flag;
    }
    // 创建一个运单然后。设置开放的状态在tag上添加一个标签
    function createNewStore(){
        $.ajax({
			url:'<%= addNewStoreAction%>'+"?ps_id="+ '<%= loginUserInfo.getString("ps_id")%>',
			dataType:'json',
			success:function(data){
				if(data && data.flag == "success"){
					var array = [];
					array.push(data.entityid);
					array.push('');
					create(array);	 
				}else{
					showMessage("添加失败","error");
				}
			}
		
        })
    }
    function modRole(billId,printPage,_psId) 
    {
    	$.prompt(
    	"<div id='title'> [在仓库"+_psId+"] 选择出库单</div><br><br>出库单：<select id='storeList' name='storeList' width='200px'><option value=''>选择出库单</option></select>",
    	{
       		  loaded:
    				function (){
						// 得到所有开放的出库单
						$.ajax({
							url:'<%= getAllOpenStoreByPsId%>' + "?ps_id="+ _psId,
							dataType:'json',
							success:function(data){
								if(data && data.flag == "success"){
									var options  = "";
									for(var index = 0 , count = data.value.length ; index < count ; index++){
										options += "<option value='"+data.value[index].out_id+"'>"+data.value[index].out_id+"</option>";
									};
									if(options != ""){
										$("#storeList").append(options);
									}
									$("#outId").val("");
								};
							}
						})
    				}
    		  ,
    		  callback: 
    				function (v,m,f)
    				{	
  				 		var ids = billId  || getSelectedIds();
    					if (v=="y"){
        				 	var value = $("#outId").val();
    						if(value.length < 1){
        		
    							showMessage('请选择一个出库单',"alert");
    						}else{
        						//将某个订单update out_id;
        						$.ajax({
            						url:'<%=updateWallBillOutId %>' + "?ids="+ ids + "&out_id=" + value,
            						dataType:'text',
            						success:function(data){
                						if(data =="success"){
                							$("#sub_out_id").val(value);
                							if(printPage){
										  		 printWayBill(billId,printPage);
										    	}
                							  ajaxGetInfoList();
                    						
                    					}else{
                    						showMessage('保存失败',"error");
                        				}
            						}
            					})
        					}
    					}
    				}
    		  
    		  ,
    		  overlayspeed:"fast",
    		  buttons: { 提交: "y", 取消: "n" }
    	});
    }
    function print(_this,billId,outId,printPage){
     	if(outId.length > 0){       	
         	 printWayBill(billId,printPage);
        }else{
        	//不存在的出库单情况是和assign的一样的  ,选择一个出库过后就打印;
        	assignStore(billId,printPage);
        }
    } 
    // 运单拆分
    function chaifen(billId,outId)
    {
    	 var uri = "split_waybill.html?way_bill_id="+billId;
        
		 $.artDialog.open(uri,{title: '拆分运单 [' + billId +'] ',width:'760px',height:'560px',fixed:true, lock: true,opacity: 0.3});
    }
    
    function reDispatched(waybill_id)
    {
    	 var uri = "re_dispatched_print_center.html?way_bill_id="+waybill_id;
        
		 $.artDialog.open(uri,{title: '运单重新发货 [' + waybill_id +'] ',width:'760px',height:'560px',fixed:true, lock: true,opacity: 0.3});
    }
    
    function inputValue(_this){
    	var node = $(_this);
		var value = node.val();
		node.val(value.replace(/[^0-9]/g,''));
    }
  
	 //页面全选 操作等
	 function checkAll(){
		var check = $("#allCheck");
		if(check.attr("checked")){
			// 查找所有的input然后把
			$("input[type='checkbox']",$("#tbody")).each(function(){
				var _this = $(this);
				_this.attr("checked",true);
				_this.parent().parent().addClass("over");
			})
		}else{
				
			$("input[type='checkbox']",$("#tbody")).attr("checked",false);
			$("tr",$("#tbody")).removeClass("over");
		}
	 }
	 function singleClick(_this){
		 var node = $(_this);
		 node.parent().parent().removeClass("over");
		 $("#allCheck").attr("checked",checkIsAllChecked());
	 }
	 function checkIsAllChecked(){
		 var flag = true;
		 var inputs = $("input[type='checkbox']",$("#tbody"));
		 for(var index = 0 ; index < inputs.length ; index++ ){
			 if(!$(inputs[index]).attr("checked")){
				 flag = false;
				 break;
			  }
		 }
		 return flag;
	}
	 
	 // 获取tbody下所有打钩的选项的Ids
	 function getSelectedIds(){
		var ids= ""; 
		 var inputs = $("input[type='checkbox']:checked",$("#tbody"));
		 if(inputs.length > 0){
			 for(var index = 0 ; index < inputs.length; index++ ){
				 ids += ","+$(inputs[index]).attr("id").replace("id_","");
			}
		 } 
		 return ids == ""?"":ids.substr(1);
	 }
	//stateBox 信息提示框
		function showMessage(_content,_state){
			var o =  {
				state:_state || "succeed" ,
				content:_content,
				corner: true
			 };
			 var  _self = $("body"),
			_stateBox = $("<div style='font-size:14px;'/>").addClass("ui-stateBox").html(o.content);
			_self.append(_stateBox);	
			if(o.corner){
				_stateBox.addClass("ui-corner-all");
			}
			if(o.state === "succeed"){
				_stateBox.addClass("ui-stateBox-succeed");
				setTimeout(removeBox,1500);
			}else if(o.state === "alert"){
				_stateBox.addClass("ui-stateBox-alert");
				setTimeout(removeBox,2000);
			}else if(o.state === "error"){
				_stateBox.addClass("ui-stateBox-error");
				setTimeout(removeBox,2800);
			}
			_stateBox.fadeIn("fast");
			function removeBox(){
				_stateBox.fadeOut("fast").remove();
		 }
		}
		
	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
	
	function showOrder(waybill_id)
	{
		tb_show('查看运单所含订单',"show_order.html?waybill_id="+waybill_id+"&TB_iframe=true&height=300&width=250",false);
	}
	
	function downloadWayBillOrder()
	{
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administraor/waybill/exportWayBill.action',
					type: 'post',
					dataType: 'json',
					cache:false,
					data:
					{
						p_name:$("#search_key").val(),
						pcid:$("#filter_pcid").val(),
						pro_line_id:$("#filter_productLine").val(),
						order_source:$("#order_source").val(),
						ps_id:$("#cid").val(),
						ca_id:$("#sale_area").val(),
						ccid:$("#ccid_hidden").val(),
						pro_id:$("#pro_id").val(),
						out_id:$("#selectOutIds").val(),
						status:$("#waybill_status").val(),
						product_status:$("#product_status").val(),
						sc_id:$("#scid").val(),
						st:$("#st_date").val(),
						en:$("#en_date").val()
					},
					
					beforeSend:function(request){
						 
					},
					
					error: function(e){
						 
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						 
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
		
	}
	function showMoreCondition(){
		$(".advance").slideToggle("slow");
	}
	
	function showWaybillLog(waybill_id)
	{
		$.artDialog.open("show_waybill_log.html?waybill_id="+waybill_id,{title: "运单日志",width:'600px',height:'400px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function queryWayBillId()
   	{
   	
   		var searchkey = $("#oid").val();
		
		searchkey = searchkey.replace(/\"/g,'');
		var val_search = "\""+searchkey+"\"";
		$("#oid").val(val_search);
		
		if($.trim(searchkey).length < 1)
		{
			showMessage("请先输入查询条件","alert");
			return ;
		}

	   	$("input[name='cmd']",$("#queryForm")).val("waybillId");
	   	$("#search_mode").val(1);
	   	var obj = {cmd:"waybillId",search_mode:1,oid:val_search};
	   	ajaxGetInfoList(jQuery.param(obj));
   	}
   	
   	function searchRepalceWaybill(searchkey)
   	{
		searchkey = searchkey.replace(/\"/g,'');
		var val_search = "\""+searchkey+"\"";
		$("#oid").val(val_search);
		
		if($.trim(searchkey).length < 1)
		{
			showMessage("请先输入查询条件","alert");
			return ;
		}

	   	$("input[name='cmd']",$("#queryForm")).val("waybillId");
	   	$("#search_mode").val(1);
	   	var str =  $("#queryForm").serialize();
	   	ajaxGetInfoList(str);
   	}
   	
   	function queryWayBillIdRightButton()
   	{
   		var searchkey = $("#oid").val();
		
		searchkey = searchkey.replace(/\"/g,'');
		
		$("#oid").val(searchkey);
		
		if($.trim(searchkey).length < 1)
		{
			showMessage("请先输入查询条件","alert");
			return ;
		}

	   	$("input[name='cmd']",$("#queryForm")).val("waybillId");
	   	$("#search_mode").val(2);
	   	var str =  $("#queryForm").serialize();
	   	ajaxGetInfoList(str);
   	}
   	
   	function eso_button_even()
	{
		document.getElementById("eso_search").oncontextmenu=function(event) {  
			if (document.all) window.event.returnValue = false;// for IE  
			else event.preventDefault();  
		};  
	
		document.getElementById("eso_search").onmouseup=function(oEvent) {  
			if (!oEvent) oEvent=window.event;  
			if (oEvent.button==2) 
			{  
	         	queryWayBillIdRightButton();
	    	}  
		}  
	}
</script>
</head>
<body>

<form action="" name="download_form"></form>

 
	<input type="hidden" id="outId" name="outId" value=""/>
	<div id="fixWidth" class="ui-widget-content ui-corner-all" style="">
 	<div unselectable="on" style="-moz-user-select: none; position: relative; z-index: 3000; display: block;">
		<span class="ui-state-active ui-corner-tl ui-corner-bl stPrev stNav" onclick="flowTag('prev');" unselectable="on" style="-moz-user-select: none; cursor: pointer; z-index: 1000; position: absolute; top: 3px; height: 30px; left: 3px;" title="Previous tab">
			<span class="ui-icon ui-icon-carat-1-w" unselectable="on"  style="-moz-user-select: none; margin-top: 6px;">向前</span>
		</span>
		<span class="ui-state-active ui-corner-tr ui-corner-br stNext stNav" onclick="flowTag('next');" unselectable="on" style="-moz-user-select: none; cursor: pointer; z-index: 1000; position: absolute; top: 3px; height: 30px; right: 3px;" title="Next tab">
			<span class="ui-icon ui-icon-carat-1-e" style="margin-top: 6px;">向后</span>
		</span>
	</div>
 	 <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="padding: 2px; position: relative;">
	    <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" style="overflow: hidden;">
		    <div class="stTabsInnerWrapper" style="width: 30000px;">
			    <span class="stWidthChecker" style="display: block;" id="stWidthChecker">
				   <li class="ui-state-default ui-corner-top " unselectable="on" style="-moz-user-select: none; margin-left: 18px;" onmouseover="changeToHover(this);" onmouseout="changToDefault(this)" onclick="clickLi(this);">
				   		 <a id="a_query" href="#query" style="color: black;" value="">常用工具</a>
				    </li>
				      <li class="ui-state-default ui-corner-top " unselectable="" style="-moz-user-select: none;" onmouseover="changeToHover(this);" onmouseout="changToDefault(this)" onclick="clickLi(this);">
				   		 <a id="a_statistics" href="#statistics" style="color: black;" value="">监控统计</a>
				    </li>
				    <li class="ui-state-default ui-corner-top ui-state-selected ui-state-active" unselectable="on" style="-moz-user-select: none; " onmouseover="changeToHover(this);" onmouseout="changToDefault(this)" onclick="clickLi(this);">
				   		 <a id="a_dingdan" href="#dingdan_1" style="color: black;" value="">所有运单</a>
				    </li>
				    
				    <%
				     if(store != null && store.length > 0){
				    	 for(int index = 0 , count = store.length ; index < count ;index++ ){
				    %>
					     <li class="ui-corner-top ui-state-default" unselectable="on" style="-moz-user-select: none;" onmouseover="changeToHover(this);" onmouseout="changToDefault(this)" onclick="clickLi(this);">
					  		 <a href="#" style="color: black;" value='<%= store[index].getString("out_id")%>'>第&nbsp;<%= store[index].getString("out_id") %>&nbsp;出库单</a>
					   		<!--  <span class="ui-tabs-close ui-icon ui-icon-close" title="Close this tab" style="float: left; cursor: pointer; margin: 4px 2px 0pt -11px;"></span>
					   		 -->
					    </li>
				    <% 	 
				    	 }
				     }
				    %>
			    </span>
		    </div>
	    </ul>
    </div>
  		<div id="query" style="display:none;height:65px;margin-top:10px;margin-left:10px;" class="dingdan1">
  			 
  				<input type="hidden" name="cmd" id="r_cmd_value" value="query"/>
  				<input type="hidden" name="search_mode" id="search_mode"/>
  				<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
		            <tr>
		              <td width="30%" style="padding-top:3px;">
					   <div id="easy_search_father">
					   <div id="easy_search"><a href="javascript:queryWayBillId()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"></a></div>
					   </div>
						<table width="485" border="0" cellspacing="0" cellpadding="0">
						  <tr>
						    <td width="418">
								<div  class="search_shadow_bg">
									<input type="text" name="oid" id="oid" value="" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333"/>
								</div>
							</td>
						    <td width="67">
							 <a href="javascript:void(0)" id="search_help" style="padding:0px;color: #000000;text-decoration: none;"><img src="../imgs/help.gif" border="0" align="absmiddle" border="0"></a>
							</td>
						  </tr>
						</table>
						<script>eso_button_even();</script>		
					</td>
		              <td width="40%" ></td>
		              <td width="17%" align="right" valign="middle">
					 </td>
		            </tr>
		          </table>
  			</p>
  	 
  		</div>
		 <div id="filter"  style="margin-top:5px;border:0px solid green; margin-left:10px;padding-bottom:4px;">
 			 	<div class="basic" style="">
 			 		<table class="fixTable">
 			 			<tr>
 			 				<td align="left" style="">
	 			 					<input name="st" type="text" id="st_date"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:65px;"/>
	 			 					至
	 			 					<input name="en" type="text" id="en_date" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:65px;" />
											 
 			 				</td>
 			 				<td align="left">
 			 				 <select name="order_source" id="order_source" style="border:1px #CCCCCC solid" >
				          		<option value="">全部来源</option>
								<option value="<%=OrderMgr.ORDER_SOURCE_EBAY%>"><%=OrderMgr.ORDER_SOURCE_EBAY%></option>
								<option value="<%=OrderMgr.ORDER_SOURCE_WEBSITE%>"><%=OrderMgr.ORDER_SOURCE_WEBSITE%></option>
								<option value="<%=OrderMgr.ORDER_SOURCE_DIRECTPAY%>"><%=OrderMgr.ORDER_SOURCE_DIRECTPAY%></option>
								<option value="<%=OrderMgr.ORDER_SOURCE_MANUAL%>"><%=OrderMgr.ORDER_SOURCE_MANUAL%></option>
								<option value="<%=OrderMgr.ORDER_SOURCE_QUOTE%>"><%=OrderMgr.ORDER_SOURCE_QUOTE%></option>
								<option value="<%=OrderMgr.ORDER_SOURCE_WANRRANTY%>"><%=OrderMgr.ORDER_SOURCE_WANRRANTY%></option>
								<option value="<%=OrderMgr.ORDER_SOURCE_PAY%>"><%=OrderMgr.ORDER_SOURCE_PAY%></option>
								<option value="<%=OrderMgr.ORDER_SPLIT%>"><%=OrderMgr.ORDER_SPLIT%></option>
								<option value="<%=OrderMgr.ORDER_LOCAL_BUY%>"><%=OrderMgr.ORDER_LOCAL_BUY%></option>
								<option value="<%=OrderMgr.ORDER_DIRECT_BUY%>"><%=OrderMgr.ORDER_DIRECT_BUY%></option>
								<option value="<%=OrderMgr.ORDER_AMAZON%>"><%=OrderMgr.ORDER_AMAZON%></option>
								<option value="<%=OrderMgr.ORDER_ECRATER%>"><%=OrderMgr.ORDER_ECRATER%></option>
								<option value="<%=OrderMgr.ORDER_BONANZA%>"><%=OrderMgr.ORDER_BONANZA%></option>
				        	</select>
					      仓库:<select id="cid" onchange="ajaxGetShipCompany(this.value,'scid')">
					     			 <option value="0">全部仓库</option>
					     			 <%
					     			 	DBRow[] storageRows = catalogMgr.getProductStorageCatalogTree();
										String qx;
										
										for ( int i=0; i<storageRows.length; i++ )
										{
											if ( storageRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
									%>
				          <option value="<%=storageRows[i].get("id",0l)%>" <%=storageRows[i].get("id",0l)==adminLoggerBean.getPs_id()?"selected=\"selected\"":""%> > 
										     <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageRows[i].get("level",0))%>
										     <%=qx%>
										     <%=storageRows[i].getString("title")%>          </option>
										<%
										}
										%>
					     			 </select>
 			 			 
 			 				 <select id="scid">
 			 				 	
 			 				 </select>
 			 				 </td>
 			 				 <td style="text-align:left;"> 
	 			 					 &nbsp;&nbsp;<select id="selectOutIds" name="selectOutIds">
	 			 					 	<option value="">所有出库单</option>
	 			 					 	<%
		 			 					  if(store != null && store.length > 0){
		 							    	 for(int index = 0 , count = store.length ; index < count ;index++ ){
		 							    %>
		 							    	<option value='<%=store[index].getString("out_id") %>'>第&nbsp;<%=store[index].getString("out_id") %>&nbsp;出库单</option>
		 							    <% 		 
		 							    	 }
		 			 					  }
	 			 					 	%>
	 			 					 </select>
	 			 					 <select id="waybill_status" name="status">
	 			 					 	<option value="-1">全部状态</option>
	 			 					 	<option value="<%=WayBillOrderStatusKey.SPLIT %>">拆分中</option>
	 			 					 	<option value="<%=WayBillOrderStatusKey.WAITPRINT%>" selected="selected">待打印</option>
	 			 					 	<option value="<%=WayBillOrderStatusKey.PERINTED%>">已打印</option>
	 			 					 	<option value="<%=WayBillOrderStatusKey.SHIPPED%>">已发货</option>
	 			 					 	<option value="<%=WayBillOrderStatusKey.CANCEL%>">已取消</option>
	 			 					 </select>
	 			 					 <select id="product_status" name="product_status">
	 			 					 	<option value="-1">货物状态</option>
	 			 					 	<option value="<%=ProductStatusKey.IN_STORE%>">有货</option>
	 			 					 	<option value="<%=ProductStatusKey.STORE_OUT%>">缺货</option>
	 			 					 </select>
	 			 					 <select name="internal_tracking_status" id="internal_tracking_status" style="border:1px #CCCCCC solid" onchange="selectTracking(this.value)">
									          <option value="-1">运输状态</option>
												<%
						   							ArrayList waybillInternalTrack = waybillInternalTrackingKey.getWaybillInternalTrack();
						   							
						   							for(int i = 0;i<waybillInternalTrack.size();i++)
						   							{
						   						%>
						   						<option value="<%=waybillInternalTrack.get(i)%>"><%=waybillInternalTrackingKey.getWaybillInternalTrack(waybillInternalTrack.get(i).toString())%></option>
						   						<%
						   							}
						   						%>
									        </select>
	 			 			</td>
 			 				 <td> <input type="button" name="" value="查询" class="button_long_search" onclick="filter()"/> </td>
  			 			</tr>
 			 			</table>
 			 			<table>
 			 				<tr>
 			 				<td>
 			 				 商品:<input type="text" id="search_key" name="search_key" onclick="cleanProductLine('')" style="width:180px;"/> 	
 			 				</td>
 			 				<td>
 			 						<input type="button" onclick="assignStore()" class="long-button-export"   value="分配出库单" />
	 			 				 	 <input type="button" class="long-long-button" value="缺货清单" onclick="printLacking()"/>	 					 
	 			 					 <input name="button2" type="button" value="下载" onclick="downloadWayBillOrder()" class="long-button-export"/>
 			 				</td>
 			 				<td style="text-align:left;">
	 			 				<select id="sale_area" onchange="getAreaCountryByCaId(this.value)" style="width:100px;">
							  		 <option value="0">全部区域</option>
							  		<%
							  			DBRow[] areas = productMgr.getAllCountryAreas(null);
							  			for(int i = 0;i<areas.length;i++)
							  			{
									%>
										<option value="<%=areas[i].get("ca_id",0l)%>"><%=areas[i].getString("name")%></option>
									<%
							  			}
							  		%>
							  	</select>
							      <select name="ccid_hidden" style="display: none;width:100px;" id="ccid_hidden" onChange="getStorageProvinceByCcid(this.value);">
									  <option value="0">全部国家</option>
							      </select>
							      <select name="pro_id" id="pro_id" style="display: none;width:100px;">
							      		<option value="0">全部区域</option>
							      </select>
 			 				</td>
 			 			</tr>
 			 			</table>
 			 	 </div>
 			 	 <table>
 			 			<tr>
 			 	 		<td width="430px;" align="left" bgcolor="" style="">
 			 					<ul id="productLinemenu" class="mcdropdown_menu"  >
					             <li rel="0">所有产品线</li>
					             <%
									  DBRow p1[] = productLineMgrTJH.getAllProductLine();
									  for (int i=0; i<p1.length; i++){
											out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
											out.println("</li>");
									  }
								  %>
					           </ul>
					           <input type="text" name="productLine" id="productLine" value="" />
					           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
 			 			</td>
 			 	 		<td id="categorymenu_td" width="430px;" align="left" valign="bottom"   style="padding-left:10px;border:0px solid red;padding-top:-30px;"> 
						           <ul id="categorymenu" class="mcdropdown_menu">
									  <li rel="0">所有分类</li>
									  <%
										  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
										  for (int i=0; i<c1.length; i++)
										  {
												out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
									
												  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
												  if (c2.length>0)
												  {
												  		out.println("<ul>");	
												  }
												  for (int ii=0; ii<c2.length; ii++)
												  {
														out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
														
															DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
															  if (c3.length>0)
															  {
																	out.println("<ul>");	
															  }
																for (int iii=0; iii<c3.length; iii++)
																{
																		out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																		out.println("</li>");
																}
															  if (c3.length>0)
															  {
																	out.println("</ul>");	
															  }
															  
														out.println("</li>");				
												  }
												  if (c2.length>0)
												  {
												  		out.println("</ul>");	
												  }
												  
												out.println("</li>");
										  }
										  %>
									</ul>
							  <input type="text" name="category" id="category" value="" />
							  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"/>
 			 				</td>
 			 			 
 			 					
 			 		 
 			 			</tr>
 			 		</table>  
 			 	  
  				</div>
  				<div id="statistics" align="left"  style="height:105px;margin-top:5px;border:0px solid green; margin-left:10px;display:none;">
  					<form action="" name="statistics_form" id="statistics_form">
  						<input type="hidden" name="cmd" value="statistics"/>
	  					<table align="left" cellpadding="0" cellspacing="5">
	  						<tr>
	  							<td align="left">
	  								<input name="st" type="text" id="st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;"/>
								</td>
								<td align="left">
	  								<select name="ps_id" id="statistics_ps_id" onchange="ajaxGetShipCompany(this.value,'statistics_ship_company')">
						     			 <option value="0">全部仓库</option>
						     			 <%
											for ( int i=0; i<storageRows.length; i++ )
											{
												if ( storageRows[i].get("parentid",0) != 0 )
												 {
												 	qx = "├ ";
												 }
												 else
												 {
												 	qx = "";
												 }
										%>
					          <option value="<%=storageRows[i].get("id",0l)%>" <%=storageRows[i].get("id",0l)==adminLoggerBean.getPs_id()?"selected=\"selected\"":""%> > 
											     <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageRows[i].get("level",0))%>
											     <%=qx%>
											     <%=storageRows[i].getString("title")%>          </option>
											<%
											}
											%>
			     			    </select>
			     			    &nbsp;&nbsp;
			     			    <select id="statistics_ship_company" name="sc_id">
                                  <option value="0">全部快递</option>
                                </select>
								</td>
	  							<td align="left">
	  								运单重量
	  								<select id="difference_type" name="difference_type">
	  									<option value="<%=WeightDifferentKey.DIFFERENCE%>" selected="selected">差异</option>
	  									<option value="<%=WeightDifferentKey.BIG %>">大于</option>
	  									<option value="<%=WeightDifferentKey.SMAIL%>">小于</option>
	  								</select>
	  								发货重量大于<input style="width: 30px;" name="difference" type="text" id="difference" value=""/>%	  							</td>
  							  
	  							<td>
	  								<input type="button" value="查询" class="button_long_search" onclick="wayBillStatistics()"/>	 &nbsp;&nbsp;
	  								<input type="button" value="发货统计导出" class="long-button" onclick="dataCountExport()"/>	  							</td>
	  						</tr>
	  						<tr>
	  							<td align="left">
	  								<input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
								</td>
	  							<td align="left">
	  								<input type="radio" name="cost_type" value="1" checked="checked"/>打印用时<input name="cost_type" type="radio" value="2"/>配货用时<input type="radio" name="cost_type" value="3"/>曾缺货	  			</td>
	  							<td>
	  								大于<input style="width: 30px;" id="cost" name="cost" value=""/>时
								</td>
	  							<td>
	  								&nbsp;
								</td>
	  						</tr>
							<tr>
								<td>&nbsp;</td>
								<td>未处理时间大于<input style="width: 30px;" type="text" name="unfinished" id="unfinished"/>天</td>
								<td>
									缺货未完成大于<input style="width: 30px;" type="text" name="store_out_unfinished" id="store_out_unfinished"/>天
								</td>
							</tr>
	  					</table>
	  				</form>
  					<script type="text/javascript">
  						$("#st").date_input();
						$("#en").date_input();
						$("#st_date").date_input();
						$("#en_date").date_input();
  					</script>
  				</div>
		  </div>
<form action="" name="filter_form" id="filter_form">
	<input type="hidden" name="p_name"/>
	<input type="hidden" name="pcid"/>
	<input type="hidden" name="pro_line_id"/>
	<input type="hidden" name="order_source"/>
	<input type="hidden" name="ps_id"/>
	<input type="hidden" name="ca_id"/>
	<input type="hidden" name="ccid"/>
	<input type="hidden" name="pro_id"/>
	<input type="hidden" name="out_id"/>
	<input type="hidden" name="status"/>
	<input type="hidden" name="product_status"/>
	<input type="hidden" name="sc_id"/>
	<input type="hidden" name="st"/>
	<input type="hidden" name="en"/>
	<input type="hidden" name="internal_tracking_status"/>
</form>

<br />
<script type="text/javascript">
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}
});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
					if(id==<%=pro_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"",<%=pcid%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
				}
});
<%
if (pcid>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=pcid%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 

<%
if (pro_line_id!=0)
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue('<%=pro_line_id%>');
<%
}
else
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue(0);
<%
}
%>

function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"search_key").val("");
}

	//销售区域切换国家
function getAreaCountryByCaId(ca_id)
{

		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getAreaCountrysJSON.action",
				{ca_id:ca_id},
				function callback(data)
				{ 
					$("#ccid_hidden").clearAll();
					$("#ccid_hidden").addOption("全部国家","0");
					
					if (data!="")
					{
						$("#ccid_hidden").css("display","");
						$("#pro_id").css("display","none");
						$("#pro_id").clearAll();
						
						$.each(data,function(i){
							$("#ccid_hidden").addOption(data[i].c_country,data[i].ccid);
						});
					}
					else
					{
						$("#ccid_hidden").css("display","none");
						$("#pro_id").css("display","none");
					}
				}
		);
}

//国家地区切换
function getStorageProvinceByCcid(ccid)
{

		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("全部区域","0");
					
					if (data!="")
					{
						$("#pro_id").css("display","");
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					else
					{
						$("#pro_id").css("display","none");
					}
					
					if (ccid>0)
					{
						$("#pro_id").addOption("Others","-1");
					}
				}
		);
}


	function initTag(){
		// 如果是有out_id的就让他选中
		var outId = '<%= StringUtil.getString(request,"out_id") %>';
	 	if(outId.length > 0){
		 	// 让选中的那个选中
	 		$(".ui-state-default").removeClass("ui-state-active ui-state-selected");
	 	 	$("a[value='"+outId+"']").parent().addClass("ui-state-active ui-state-selected");
	 	 	// 然后把selectOutIds 中的值固定
	 	 	var selectOutIds = $("#selectOutIds");
	 	 	$("option[value='"+outId+"']",selectOutIds).attr("selected",true);
	 	 	selectOutIds.attr("disabled",true);
		}
		
	}
	function changeToHover(_this){
		var li = $(_this);
		li.addClass("ui-state-hover");
	}
	function changToDefault(_this){
		var li = $(_this);
		li.removeClass("ui-state-hover");
	}
	
	function clickLi(_this){
		//页面要提交 然后要把颜色改变了为红颜色
		var li = $(_this);
		if(li.find("#a_query").length > 0)
		{
			$(".ui-state-default").removeClass("ui-state-active").removeClass("ui-state-selected");
			li.addClass("ui-state-selected").css("color","red").addClass("ui-state-active");
			$("#query").css("display","block");
			$("#filter").css("display","none");
			$("#statistics").css("display","none");
			queryClick("dd");
		}else if(li.find("#a_statistics").length > 0 ){
			// 运单统计页面显示 statistics
			$("#query").css("display","none");
			$("#filter").css("display","none");
			$("#statistics").css("display","block");
			$(".ui-state-default").removeClass("ui-state-active").removeClass("ui-state-selected");
			li.addClass("ui-state-selected").css("color","red").addClass("ui-state-active");
			 // 这里执行 ajaxGetInfoList();
		}else{
			$("#statistics").css("display","none");
			$("#query").css("display","none");
			$("#filter").css("display","block");
			$(".ui-state-default").removeClass("ui-state-active").removeClass("ui-state-selected");
			li.addClass("ui-state-selected").css("color","red").addClass("ui-state-active");
			var html = $("a",li).attr("value");
			$("#sub_out_id").val(html);
			$("input[name='out_id']",$("#dataForm")).val(html);
			// 以前是 tagClickForm  提交
			ajaxGetInfoList($("#filter_form").serialize());
			var selectOutIds = $("#selectOutIds");
			if(html.length < 1){
				selectOutIds.attr("disabled",false);
				 $("option:first",selectOutIds).attr("selected",true);
			}else{
	 	 		$("option[value='"+html+"']",selectOutIds).attr("selected",true);
	 	 		selectOutIds.attr("disabled",true);
			}
			//$("#tagClickForm").submit();
		}
	}
	
	function flowTag(flag){
		var stWidthChecker = $("#stWidthChecker");
		var value = "";
		var marginLeft = stWidthChecker.css("margin-left").replace("px","") * 1;
		if(flag === "prev"){
			if(marginLeft == 0){
				return ;
			}else{
				value = (marginLeft + 100) + "px";
			}
		}else{
			value = (marginLeft - 100) + "px";
		};
		stWidthChecker.animate({marginLeft:value}, 200);
	}
	
	function filter()
	{
		document.filter_form.p_name.value = $("#search_key").val();
		document.filter_form.pcid.value = $("#filter_pcid").val();
		document.filter_form.pro_line_id.value = $("#filter_productLine").val();
		document.filter_form.order_source.value = $("#order_source").val();
		document.filter_form.ps_id.value = $("#cid").val();
		document.filter_form.ca_id.value = $("#sale_area").val();
		document.filter_form.ccid.value = $("#ccid_hidden").val()
		document.filter_form.pro_id.value = $("#pro_id").val()
		document.filter_form.out_id.value = $("#selectOutIds").val();
		document.filter_form.status.value = $("#waybill_status").val();
		document.filter_form.product_status.value = $("#product_status").val();
		document.filter_form.sc_id.value = $("#scid").val();
		document.filter_form.st.value = $("#st_date").val();
		document.filter_form.en.value = $("#en_date").val();
		document.filter_form.internal_tracking_status.value = $("#internal_tracking_status").val();
	 
		ajaxGetInfoList($("#filter_form").serialize());
	}
</script>
<form name="print_waybill_form" id="print_waybill_form" method="post" target="_blank">
	<input type="hidden" name="waybill_id"/>
</form>
<form name="print_form" method="post" action="order/print/dhl_lodop.html" target="_blank">
	<input type="hidden" id="waybill_id" name="waybill_id" />
</form>
<form id="tagClickForm" name="tagClickForm">
	<input type="hidden" value="<%= loginUserInfo.getString("ps_id") %>"  name="ps_id"/>
	<input type="hidden" name="out_id" id="sub_out_id" value="" />
	<input type="hidden" name="cmd" value="click" />
</form>
  <form name="dataForm" id="dataForm">
    <input type="hidden" name="p" />
	<input type="hidden" name="cmd" value="<%=cmd%>"/>
	<input type="hidden" name="p_name" value="<%=p_name%>"/>
	<input type="hidden" name="pcid" value="<%=pcid%>"/>
	<input type="hidden" name="pro_line_id" value="<%=pro_line_id%>"/>
	<input type="hidden" name="order_source" value="<%=order_source%>"/>
	<input type="hidden" name="ps_id" value="<%=ps_id%>"/>
	<input type="hidden" name="ca_id" value="<%=ca_id%>"/>
	<input type="hidden" name="ccid" value="<%=ccid%>"/>
	<input type="hidden" name="pro_id" value="<%=pro_id%>"/>
	<input type="hidden" name="out_id" value="<%=out_id%>"/>
	<input type="hidden" name="status" value="<%=status%>"/>
	<input type="hidden" name="trace_type" value="<%=trace_type%>"/>
  </form>
   <script type="text/javascript">
   
   function puchaseDeliveryCount(pc_id,ps_id,type)
	{
		$.artDialog.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/purchase_delivery_count.html?pc_id="+pc_id+"&ps_id="+ps_id+"&type="+type,{title: "采购及在途数",width:'600px',height:'400px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function wayBillReturn(waybill_id)
	{
		$.artDialog.open("waybill_return.html?waybill_id="+waybill_id,{title: "运单被退回",width:'500px',height:'200px',fixed:true, lock: true,opacity: 0.3,fixed: true});
	}
	
	function wayBillResult(waybill_id)
	{
		$.artDialog.open("waybill_result.html?waybill_id="+waybill_id,{title: "运单被退回",width:'450px',height:'200px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function followup(waybill_id)
	{
		$.artDialog.open("add_waybill_note.html?waybill_id="+waybill_id,{title: "运单跟进",width:'420px',height:'280px',fixed:true, lock: true,opacity: 0.3});
	}
	
   function printWayBill(waybill_id,print_page){

	    //declare
		 var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+print_page+"?waybill_id="+waybill_id;
		 var form = $("#print_waybill_form");
		 $("input",form).val(waybill_id);
		 form.attr("action","<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+print_page)
		// form.submit();
		 $.artDialog.open(uri, {title: '打印运单',width:'960px',height:'500px', lock: true,opacity: 0.3,fixed: true});
   }
   function ajaxGetInfoList(str) {
 	 	var dataForm = str || $("#dataForm").serialize();
 	 	$.ajax({
 	 	 	url:'waybill_list.html',
 	 	 	dataType:'html',
 	 	 	type:'post',
 	 	 	data:dataForm,
 	 	 	beforeSend:function(XHR){
 	 			  
 	 	 	},
 	 	 	success:function(da){
 	 	 	 
 	 	 		$(".dataList").html(da);
 	 	 	}
 	 	})
   }
   
   function refreshWindow()
   {
		window.location.reload();
   }
   
   <%
   	if(cmd.equals("trace"))
   	{
   		String para = "cmd="+cmd+"&ps_id="+ps_id+"&trace_type="+trace_type;
   %>
   		ajaxGetInfoList("<%=para%>");
   <%
   	}
   	else
   	{
   %>
   		ajaxGetInfoList();
   <%
   	}
   %>
   
	//这两个方法没有写好。应该和成一个方法 。算了  不和了。
  	function queryClick(v){
   	 if(!v && $.trim($("#oid").val()).length < 1){
   	   	 showMessage("请输入订单号","alert");
   	   	 return ;
   	 }
   	 $("input[name='cmd']",$("#queryForm")).val("query");
   	   	 var str = $("#queryForm").serialize();
   	  	 ajaxGetInfoList(str);
   	}
   	function queryWayBill(){
   	 if($.trim($("#oid").val()).length < 1){
   	   	 showMessage("请输入运单号","alert");
   	   	 return ;
   	 }
   	  $("input[name='cmd']",$("#queryForm")).val("waybill");
   	   	var str =  $("#queryForm").serialize();
   		 ajaxGetInfoList(str);
   	}
   	
   	
   	function ajaxGetShipCompany(ps_id,id)
   	{
   		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getExpressCompanysByPsIdJSON.action',
			dataType:'json',
			data:{ps_id:ps_id},
			success:function(data)
			{
				$("#"+id).clearAll();
				$("#"+id).addOption("全部快递","0");
				
				if (data!="")
				{
					$.each(data,function(i)
					{
						$("#"+id).addOption(data[i].name,data[i].sc_id);
					});
				}
		 	}
		})
   	}
   	
   	function wayBillStatistics()
   	{
   		ajaxGetInfoList($("#statistics_form").serialize())
   	}
   	
   	ajaxGetShipCompany($("#cid").val(),"scid");
   	ajaxGetShipCompany($("#statistics_ps_id").val(),"statistics_ship_company");

	function dataCountExport()
  	 {
  	 var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/waybill/export_waybill_data_count.html";
	 $.artDialog.open(uri, {title: '发货统计导出',width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
  	 }
	</script>
 <div class="dataList" style=" clear:both;">
 	
 </div>
<br />
</body>
</html>