<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.exception.purchase.FileException"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.LableTemplateKey"%>
<%@ include file="../../include.jsp"%> 
<%
 	PageCtrl pc = new PageCtrl();
	pc.setPageSize(30);
	pc.setPageNo(1);
	String cmd = StringUtil.getString(request,"cmd");
	
	DBRow rows[];
	if(cmd.equals("products"))
	{
		rows = lableTemplateMgrZJ.getAllProductTemplate(null);
	}
	else
	{
		rows = lableTemplateMgrZJ.getAllLableTemplate(null);
	}
	
	//采购单id
	long purchase_id = StringUtil.getLong(request,"purchase_id");
	//商品id
	long pc_id = StringUtil.getLong(request,"pc_id");
	//工厂类型
	String factory_type=StringUtil.getString(request,"factory_type");
	//供应商id
	long supplierSid=StringUtil.getLong(request,"supplierSid");
	
	//收货仓库
	String warehouse=StringUtil.getString(request,"warehouse");
	
	LableTemplateKey lableTemplateKey = new LableTemplateKey();
 %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}

.lableshow tr.alt td 
{
	background: white;

}

.lableshow tr.over td
{
	background: #E6F3C5;

}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	color: #999999;
	text-decoration: none;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: none;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: none;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: none;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: none;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: none;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: none;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: none;
	font-family: Arial;
	font-size:14px;
}
</style>
<script type="text/javascript">
	function onTable()
	{
		$(".lableshow tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
		$(".lableshow tr:even").addClass("alt");
	}
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="onTable()">
	<table width="98%" align="center" border="0" cellspacing="0" cellpadding="0">
	<%
		for(int i=0;i<rows.length;i++)
		{
			if(purchase_id==0&&rows[i].get("lable_type",0)==LableTemplateKey.SEQUENCELABEL)
			{
				continue;//序列号判断下是否是从采购单进入，不是的话不显示序列号打印
			}
			
			if(i==0||rows[i].get("lable_type",0)!=rows[i-1].get("lable_type",0))
			{
	%>
		<tr>
			<td colspan="2" style="padding-top: 10px;"></td>
		</tr>
		<tr>
			 <td align="left" colspan="2" style="font-size:20px;padding-left: 15px;" bgcolor="#eeeeee">
				<%=lableTemplateKey.getLableTemplateNameById(rows[i].get("lable_type",0))%>
			</td>
		</tr>
	<%
			}
	%>
		<tr>
			<td>
				<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="lableshow">
					<tr>
						<td width="200" nowrap="nowrap" align="right" valign="middle"" style="border-right:2px #eeeeee solid;border-bottom:2px #eeeeee solid;">
							<table align="left" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="left" style="padding-left: 15px;">标签名称:</td>
									<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong><%=rows[i].getString("template_name")%></strong></td>
								</tr>
								<tr>
									<td align="left" style="padding-left: 15px;">纸张宽度:</td>
									<td align="left" nowrap="nowrap" style="padding-left:15px;"><%=rows[i].get("print_range_width",0f)%></td>
								</tr>
								<tr>
									<td align="left" style="padding-left: 15px;">纸张高度:</td>
									<td align="left" nowrap="nowrap" style="padding-left:15px;"><%=rows[i].get("print_range_height",0f)%></td>
								</tr>
							</table>
						</td>
						<td align="left" valign="middle" style="padding:10px;padding-left:25px;border-bottom:2px #eeeeee solid;">
							<a href="javascript:printPage(<%=rows[i].get("lable_template_id",0l)%>,<%=rows[i].get("lable_type",0)%>);">
								<img src="<%=ConfigBean.getStringValue("systenFolder")+"administrator/lable_template/"+rows[i].getString("img_path")%>">
							</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
	<%	
		}
	%>
	</table>
	<script type="text/javascript">
		function printPage(lable_template_id,lable_type)
		{
			var cmd = "<%=cmd%>";
			var href;
			if(cmd=="products")
			{
				href = "../lable_template/print_product_lable.html?lable_template_id="+lable_template_id+"&purchase_id=<%=purchase_id%>&supplierSid=<%=supplierSid%>";
			}
			else if(cmd=="file")
			{
				href = "../product/printbarcode_upload_excel.html?lable_template_id="+lable_template_id+"&cmd=file";
			}
			else
			{
				href = "../lable_template/print_one_product.html?pc_id=<%=pc_id%>&lable_template_id="+lable_template_id+"&purchase_id=<%=purchase_id%>&warehouse=<%=warehouse%>&factory_type=<%=factory_type%>&supplierSid=<%=supplierSid%>";
			}
			if(lable_type==<%=LableTemplateKey.DAMAGEDLABEL%>)
			{
				href = "../lable_template/print_damaged_product.html?pc_id=<%=pc_id%>&lable_template_id="+lable_template_id;
			}
			window.location.href = href;
		}
	</script>
</body>
</html>



