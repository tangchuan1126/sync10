<%@page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.api.OrderMgr"%>
<%@page import="com.cwc.app.key.WeightDifferentKey"%>
<%@page import="com.fedex.ship.stub.WeightUnits"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long adminId=adminLoggerBean.getAdid();
	//根据登录帐号判断是否为客户
	Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录 	
	//如果是用户登录 查询用户下的产品
	DBRow[] titles;
	long number=0;
	if(bl){
		number=1;
		titles=adminMgrZwb.getProprietaryByadminId(adminLoggerBean.getAdid());
	}else{
		number=0;
		titles=mgrZwb.selectAllTitle();
	}
%>
<title>运单处理</title>

</head>
<body >
	<div id="tabs">
		<ul>
			<li><a href="#av0" >B2C运单查询</a></li>	
			<li><a href="#av1" onclick="add_out_list(0,0,<%=ProductStoreBillKey.WAYBILL_ORDER%>,1)">B2C运单生成拣货单</a></li>	  
			<li><a href="#av2" >B2B转运单查询</a></li>
			<li><a href="#av3" onclick="find_out_transport_list(0,0,<%=ProductStoreBillKey.TRANSPORT_ORDER%>,1)">B2B转运单生成拣货单</a></li>	 
		</ul>
	    <div id="av0">
	    	<div id="tiaojian" style="border-bottom:1px #CCC dashed; display:none">
	    		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
				    <td width="10%" align="right" height="30px;">
			    		<span style="font-weight:bold;font-size:12px;">条件：</span>
				    </td>
				    <td width="90%" valign="middle"> 
				    	<div id="condition" style="min-width; float:left"></div>
				    	<div style="float:left">
				    	    <span style="margin-left:20px;color:red" id="tiaoshu"></span>
				    		<input type="button" value="创建拣货单" class="long-button-export" onclick="addOutOrder()"/>
				    	</div>
				    </td>
				  </tr>
				</table> 
	    	</div>
	    	<div id="cangku" style="border-bottom:1px #CCC dashed;">
			    <table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
				    <td width="10%" align="right" height="50px;">
			    		<span style="font-weight:bold;font-size:12px;">仓库：</span>
				    </td>
				    <td width="90%">
				       	<%DBRow[] storageRows = catalogMgr.getProductStorageCatalogTree();%>
						<%for ( int i=0; i<storageRows.length; i++ ){%>
						 	  <div style="width:200px;float:left; margin-left:10px;">
						 	  	 <a href="javascript:void(0)" onclick="addCondition('<%=storageRows[i].get("id",0l)%>','<%=storageRows[i].getString("title")%>','cangku')" style="color:#069;text-decoration:none;outline:none;"><%=storageRows[i].getString("title")%></a>
						 	  </div>
						<%}%>				
				    </td>
				  </tr>
				</table> 
			</div>
			<div id="kuaidi" style="border-bottom:1px #CCC dashed; display:none">
			    <table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
				    <td width="10%" align="right" height="45px;">
			    		<span style="font-weight:bold;font-size:12px;">快递：</span>
				    </td>
				    <td width="90%" id="kuaidivalue">
				        
				    </td>
				  </tr>
				</table> 
			</div>	
			<div id="zhongliang" style="border-bottom:1px #CCC dashed; display:none" >
			    <table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
				    <td width="10%" align="right" height="30px;">
			    		<span style="font-weight:bold;font-size:12px;">重量段：</span>
				    </td>
				    <td width="90%">
				    	<div style="width:95px;float:left;margin-left:10px;">
					       	<a href="javascript:void(0)" onclick="addCondition('1kg','1kg','zhongliang')" style="color:#069;text-decoration:none;outline:none;">1千克以下</a>
					    </div>
					    <div style="width:95px;float:left;margin-left:10px;">
					       	<a href="javascript:void(0)" onclick="addCondition('2kg','2kg','zhongliang')" style="color:#069;text-decoration:none;outline:none;">2千克以下</a>
					    </div>
					    <div style="width:95px;float:left;margin-left:10px;">
					       	<a href="javascript:void(0)" onclick="addCondition('3kg','3kg','zhongliang')" style="color:#069;text-decoration:none;outline:none;">3千克以下</a>
					    </div>			
				    </td>
				  </tr>
				</table> 
			</div>	
			<div id="baohan" style="border-bottom:1px #CCC dashed; display:none">
			    <table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
				    <td width="10%" align="right" height="30px;">
			    		<span style="font-weight:bold;font-size:12px;">包含件数：</span>
				    </td>
				    <td width="90%" id="baohanCondition">
					    <div style="width:200px;float:left;margin-left:10px;">
					       <a href="javascript:void(0)" onclick="addCondition('1','1','baohan')" style="color:#069;text-decoration:none;outline:none;">
					       <%DBRow onerow=waybillMgrZwb.seachPkcount(1);%>
					       	1件(<%=onerow.get("count",0l) %>)
					       </a>
					    </div>	
					    <div style="width:200px;float:left;margin-left:10px;">
					       <a href="javascript:void(0)" onclick="addCondition('2','2','baohan')" style="color:#069;text-decoration:none;outline:none;">
					       <%DBRow tworow=waybillMgrZwb.seachPkcount(2);%>
					       	2件(<%=tworow.get("count",0l) %>)
					       </a>
					    </div>	
					    <div style="width:200px;float:left;margin-left:10px;">
					       <a href="javascript:void(0)" onclick="addCondition('3','3','baohan')" style="color:#069;text-decoration:none;outline:none;">
					       	 <%DBRow threerow=waybillMgrZwb.seachPkcount(3);%>
					       	3件(<%=threerow.get("count",0l) %>)
					       </a>
					    </div>	
					    <div style="width:200px;float:left;margin-left:10px;">
					       <a href="javascript:void(0)" onclick="addCondition('4','4','baohan')" style="color:#069;text-decoration:none;outline:none;">
					      	  <%DBRow muchrow=waybillMgrZwb.seachPkcountMuch(4);%>
					      	 4件以上(<%=muchrow.get("count",0l) %>)
					       </a>
					    </div>			
				    </td>
				  </tr>
				</table> 
			</div>
			<div id="line" style="border-bottom:1px #CCC dashed; display:none">
			    <table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
				    <td width="10%" align="right" height="70px;">
			    		<span style="font-weight:bold;font-size:12px;">产品线：</span>
				    </td>
				    <td width="90%" id="lineCondition">
				        <%DBRow p1[] = productLineMgrTJH.getAllProductLine();%>
						<%for (int i=0; i<p1.length; i++){%>
						 	  <div style="width:200px;float:left;margin-left:10px;">
						 	  	 <a href="javascript:void(0)" onclick="addCondition('<%=p1[i].get("id",0l)%>','<%=p1[i].getString("name")%>','line')" style="color:#069;text-decoration:none;outline:none;">
						 	  	 	<%=p1[i].getString("name")%>
						 	  	 	<%DBRow countRow=waybillMgrZwb.getWaybillByCountLineId(p1[i].get("id",0l)); %>
						 	  	 	(<%=countRow.get("count",0l)%>)						
						 	  	 </a>
						 	  </div>
						<%}%>	   
				    </td>
				  </tr>
				</table> 
			</div> 
			<br/>
			<!-- 查询结果 -->
			<div id="dataList">
		    </div>   
	    </div>
	    <div id="av1">
	       <div id="outOrder" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">	                   
		    	<input id="findWaybillId" type="text"  onblur="outInput()" onfocus="inputIn()" value="*拣货单号查询" style="color:silver;" > 
		      仓库： <select id="cid">
					<option value="0" selected="selected">全部仓库</option>
					<option value="100203"> PA </option>
					<option value="100000"> BJ </option>
					<option value="100006"> GZ </option>
					<option value="100043"> LA </option>
					<option value="100197"> BS </option>
					<option value="100199"> 海运中转库 </option>
					<option value="100200"> Amazon </option>
					<option value="100201"> Amazon UK </option>
					<option value="100202"> CHUKOU1 UK </option>
			   </select>
			   <input type="hidden" id="out_for_type" value="<%=ProductStoreBillKey.WAYBILL_ORDER%>"  />
			   <input class="button_long_search" type="button" onclick="findOutList()" value="过滤">
	       </div>
	       <!-- 出库单查询结果 -->
	       <div id="outbound_list">
		   </div>   
	    </div>
	    <div id="av2">
	        <div id="transportTiaojian" style="border-bottom:1px #CCC dashed; display:none">
		   		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
				    <td width="10%" align="right" height="30px;">
			    		<span style="font-weight:bold;font-size:12px;">条件：</span>
				    </td>
				    <td width="90%" valign="middle"> 
				    	<div id="transportCondition" style="min-width; float:left"></div>
				    	<div style="float:left">
				    	    <span style="margin-left:20px;color:red" id="transportTiaoshu"></span>
				    		<input type="button" value="创建拣货单" class="long-button-export" onclick="addOutOrderByTransport()"/>
				    	</div>
				    </td>
				  </tr>
				</table> 
		   	</div>
		   	<div id="chukucangku" style="border-bottom:1px #CCC dashed;">
			    <table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
				    <td width="10%" align="right" height="50px;">
			    		<span style="font-weight:bold;font-size:12px;">出货仓库：</span>
				    </td>
				    <td width="90%">			
						<%for ( int i=0; i<storageRows.length; i++ ){%>
						 	  <div style="width:200px;float:left; margin-left:10px;">
						 	  	 <a href="javascript:void(0)" onclick="addTransport('<%=storageRows[i].get("id",0l)%>','<%=storageRows[i].getString("title")%>','chukucangku')" style="color:#069;text-decoration:none;outline:none;"><%=storageRows[i].getString("title")%></a>
						 	  </div>
						<%}%>				
				    </td>
				  </tr>
				</table> 
			</div>
			<div id="shouhuocangku" style="border-bottom:1px #CCC dashed; display:none">
			    <table width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr>
				    <td width="10%" align="right" height="50px;">
			    		<span style="font-weight:bold;font-size:12px;">收货仓库：</span>
				    </td>
				    <td width="90%">			
						<%for ( int i=0; i<storageRows.length; i++ ){%>
						 	  <div style="width:200px;float:left; margin-left:10px;">
						 	  	 <a href="javascript:void(0)" onclick="addTransport('<%=storageRows[i].get("id",0l)%>','<%=storageRows[i].getString("title")%>','shouhuocangku')" style="color:#069;text-decoration:none;outline:none;"><%=storageRows[i].getString("title")%></a>
						 	  </div>
						<%}%>				
				    </td>
				  </tr>
				</table> 
			</div>
			<br/>
	    	<div id="transport_list">
	    	</div>
	    </div>
	    <div id="av3">
	    	<div id="outOrderTransport" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">	                   
		    	<input id="findTransportId" type="text"  onblur="outInputTransport()" onfocus="inputInTransport()" value="*拣货单号查询" style="color:silver;" > 
		      仓库： <select id="cidTransport">
					<option value="0" selected="selected">全部仓库</option>
					<option value="100203"> PA </option>
					<option value="100000"> BJ </option>
					<option value="100006"> GZ </option>
					<option value="100043"> LA </option>
					<option value="100197"> BS </option>
					<option value="100199"> 海运中转库 </option>
					<option value="100200"> Amazon </option>
					<option value="100201"> Amazon UK </option>
					<option value="100202"> CHUKOU1 UK </option>
			   </select>
			   <input type="hidden" id="out_for_transport" value="<%=ProductStoreBillKey.TRANSPORT_ORDER%>"  />
			   <input class="button_long_search" type="button" onclick="findOutTransportList()" value="过滤">
	       </div>
	       <!-- 出库单查询结果 -->
	       <div id="out_transport_list">
		   </div>  
	    </div>
	    <!-- 打印页面 -->
	    <div id="printWaybill" style="display:none">    
	    </div>
	</div>
    <input type="hidden" id="uri" value="<%=ConfigBean.getStringValue("systenFolder")%>"/>
     <!-- 加载运单js -->
	<script type="text/javascript" src="../js/waybill/waybillCondition.js"></script>
</body>
</html>