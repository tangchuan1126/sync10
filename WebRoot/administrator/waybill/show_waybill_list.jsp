<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>运单列表</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>


<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />


 <style type="text/css">
div.dingdan1{height:100px; border: 1px solid #DDDDDD;border-radius: 7px 7px 7px 7px;padding: 5px 5px 5px 15px;width:1050px;margin-bottom:5px;}
table.fixTable td{height:30px;}
.zebraTable tr td{height:30px;}
.zebraTable tr td a:link {color:#000000;text-decoration: none;font-weight:bold;font-size:13px;}
.zebraTable tr td a:visited {color: #000000;text-decoration: none;font-weight:bold;font-size:13px;}
.zebraTable tr td a:hover {color: #000000;text-decoration: underline;font-weight:bold;font-size:13px;}
.zebraTable tr td a:active {color: #000000;text-decoration: none;font-weight:bold;font-size:13px;}
span.create{display:block;float:left;border:1px solid red;cursor:pointer;}
p{display:block;clear:both;}
span.valueName{width:40%;border:0px solid red;display:block;float:left;text-align:right;}
span.valueSpan{width:56%;border:0px solid green;float:left;overflow:hidden;text-align:left;text-indent:4px;}


span.costName{width:45%;float:left;text-align:right;}
span.costValue{width:50%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:bold;}

span.weightName{width:60%;display:block;float:left;text-align:right;border:0px solid red;}
span.weightValue{width:30%;display:block;float:left; text-indent:4px;font-weight:bold;}

span.infoName{width:45%;float:left;text-align:right;}
span.infoValue{width:50%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:bold;}

span.stateName{width:15%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
span.topInfo{display:block;position:relative;top:-20px;width:50px;height:20px;line-height:20px;left:10px;border:1px solid red;}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
.fix{text-align:left;width:95%;}
.title{font-size:12px;color:green;font-weight:blod;}
</style>
 <%
 PageCtrl pc = new PageCtrl();
 pc.setPageNo(StringUtil.getInt(request,"p"));
 pc.setPageSize(20);
 DBRow waybill[] = null;
 //
  
 WayBillOrderStatusKey wos = new WayBillOrderStatusKey();
 String cmd = StringUtil.getString(request,"cmd");
 String oidR = StringUtil.getString(request,"oid");
 if(cmd != null && cmd.length() > 0){
	  if(cmd.equals("comefromout")){
		 long outId = Long.parseLong(StringUtil.getString(request,"out_id"));
		 waybill = waybillMgrZR.getWayBillByOutIdPc(outId,pc);
	  } 
	  if(cmd.equals("oid")){
		  long orderId = (oidR != null && oidR.length() > 0 ) ?Long.parseLong(oidR):0l; 
			 if(orderId != 0l){
				 waybill = wayBillMgrZJ.getWayBillByOId(orderId,pc);
			 } 
	  }
	  if(cmd.equals("waybill")){
	  	 String trackingNumber = oidR;
		 if(trackingNumber.length() > 0){
			 waybill = wayBillMgrZJ.getWayBillByTrackingNumber(trackingNumber,pc);
		 } 
	  }
 }
 
     
 %>
<script type="text/javascript">
	 function onLoadInitZebraTable(){
	 	$(".zebraTable tr").mouseover(function() {$(this).addClass("over");}).mouseout(function(){
		 	var _this = $(this);
		 	var check = $("input[type='checkbox']",_this);
		 	if(check &&  check.length > 0 && check.attr("checked")){
			 }else{
		 		$(this).removeClass("over");
			 }
		 });
	 	$(".zebraTable tr:even").addClass("alt");
	 }
 
	 
	  
	 
	 onLoadInitZebraTable();
	 
		function inputIn(){
			var node = $("#oid");
			if( node.val() === "*输入运单号或者订单号"){
				node.val("");
			}
			node.css("color","black");
		}
		function outInput(){
			var  st = $("#oid").val();
			if($.trim(st).length < 1) {
				$("#oid").val("*输入运单号或者订单号").css("color","silver");
			}
		}
		function queryClick(target){
			var value = $("#oid").val();
			if($.trim(value).length < 1 || $.trim(value) === "*输入运单号或者订单号"){
				showMessage("请先输出单号","alert");
				return ;
			}
		 
			$("#pageOid").val($.trim(value));
			$("#pageCmd").val(target);
			$("#pageForm").submit();
		}
		//stateBox 信息提示框
		function showMessage(_content,_state){
			var o =  {
				state:_state || "succeed" ,
				content:_content,
				corner: true
			 };
			 var  _self = $("body"),
			_stateBox = $("<div style='font-size:14px;'/>").addClass("ui-stateBox").html(o.content);
			_self.append(_stateBox);	
			if(o.corner){
				_stateBox.addClass("ui-corner-all");
			}
			if(o.state === "succeed"){
				_stateBox.addClass("ui-stateBox-succeed");
				setTimeout(removeBox,1500);
			}else if(o.state === "alert"){
				_stateBox.addClass("ui-stateBox-alert");
				setTimeout(removeBox,2000);
			}else if(o.state === "error"){
				_stateBox.addClass("ui-stateBox-error");
				setTimeout(removeBox,2800);
			}
			_stateBox.fadeIn("fast");
			function removeBox(){
				_stateBox.fadeOut("fast").remove();
		 }
		}	
</script>
</head>
<body>
 <table width="99%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table"  style="margin-top:5px;">
<thead>
<tr>
 
<th width="21%"   class="right-title"  style="text-align: center;">编号</th>
 <th width="16%"  class="right-title" style="text-align: center;">运单明细</th>
 <th width="16%"   class="right-title"  style="text-align: center;">运单号</th>
 <th width="12%"   class="right-title"  style="text-align: center;">费用</th>
  <th width="13%"  class="right-title"  style="text-align: center;" >重量</th>
  <th width="18%"  class="right-title"  style="text-align: center;" >地址</th>
 
</tr>
</thead>
<tbody id="tbody">
<div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
		 <ul>
			 	<li><a href="#query">常用工具</a></li>	 
		 </ul>
		<div id="query" style="display:block;margin-top:10px;margin-left:10px;height:40px;text-align:left;"   >
  			<form action="" id="queryForm">
  					 
  					<input type="text" id="oid" name="oid"  class="" style="height:25px;line-height:25px;border:1px solid silver;width:400px;color:silver;" value="*输入运单号或者订单号"  onfocus="inputIn()" onblur="outInput()"/>
	  				<input type="button" value="基于订单" onclick="queryClick('oid')" class="button_long_search"  />
	  				<input type="button" value="基于运单" onclick="queryClick('waybill')" class="button_long_search"  />
	  			</p>
  			</form>
  		</div>
</div>
<script type="text/javascript">
$("#tabs").tabs({});
</script>
  <%
   	if(null != waybill && waybill.length > 0){
   		for(int index = 0, count = waybill.length ; index < count ; index++ ){
  %>
  		<tr>
   			<td>
		  			<fieldset   class="set" style="border:2px solid #993300;">
		  				 <legend>
		  				 		<%
		  				 			String showTime = waybill[index].getString("create_date");
		  				 			showTime =   showTime.substring(5,16) ;
		  				 		%>
								<span style="" class="title"><%=showTime  %>[<%= waybill[index].get("waybill_id",0l) %>]</span>
						</legend>
		  				 <%
		  				 	DBRow[] orderRows = wayBillMgrZJ.returnOrdersByWayBillId(waybill[index].get("waybill_id",0l));
		  				 	if( null != orderRows && orderRows.length > 0){
		  				 		 for(DBRow row : orderRows){
		  				 			 long oid = row.get("oid",0l);
		  				 			 if(oid != 0l){
		  				 				DBRow dateRow = waybillMgrZR.getPostDateByOid(oid);
		  				 				String postDate = "";
		  				 				if(dateRow != null) {
		  				 					postDate = dateRow.getString("post_date");
		  				 				}
		  				 				%>
		  				 				<p style="text-align:left;text-indent:5px;font-weight:normal;">订单: 
		  				 					<span class="alert-text"><%=row.get("oid",0l) %></span> &nbsp;
		  				 					<span class="countTime" end='<%=waybill[index].getString("create_date") %>' start='<%=postDate %>'></span>
		  				 				</p>
		  				 				<% 
 		  				 			}
		  				 		}
		  				 		
		  				 	}
		  				 %> 
		  				</p>
		  				 
		  			</fieldset>
		  			 
  					<%
  							//在这里判断当前的状态是 什么然后选择那些count he state 
  							
  							
  							DBRow userInfo = null ;
  							 String userName = "";
  							String[] arrayCount = {"create_account","print_account","delivery_account","cancel_account","split_account"};
  							String[] arrayTime = {"create_date","print_date","delivery_date","cancel_date","split_date"};
  							int w = waybill[index].get("status",0);
  						 
  							long countId = waybill[index].get(arrayCount[w],0l);
  							String dateTime = waybill[index].getString(arrayTime[w]);
  							userInfo = waybillMgrZR.getUserNameById(countId);
  							if(userInfo != null ){
  								  userName =  userInfo.getString("employe_name");
  							}
  					
  							dateTime = dateTime.length() >= 19 ? dateTime.substring(0,19):dateTime;
  							String wayBillState = wos.getStatusById(w);
  					%>
  			<fieldset style="" class="set" style="padding-bottom:4px;">
  				 <legend>
						<span class="title"><%= wayBillState%></span>
				</legend>
  				 
  				<p>
  					<span class="alert-text stateName"><img title="操作人" src="../imgs/order_client.gif" /></span>
  					<span class="stateValue"><%=userName %></span>
  				</p>
  				<p>
  					<span class="alert-text stateName"><img  title="操作时间" src="../imgs/alarm-clock--arrow.png" /></span>
  					<span class="stateValue"><%= dateTime%></span>
  				</p>
  			</fieldset>
  	 		</td>
  	 		<td style="padding:0px;margin:0px;">
  	 			<!-- 运单明细 -->
  	 			<fieldset style="" class="set fix">
		  			 <legend><span style="" class="title"><%= catalogMgr.getDetailProductStorageCatalogById(waybill[index].get("ps_id",0l)).getString("title")%> 仓库</span></legend>		 
		  		
  	 			<%
  	 				DBRow[] wayBillItems = wayBillMgrZJ.getWayBillOrderItems(waybill[index].get("waybill_id",0l));
  	 				if(null != wayBillItems && wayBillItems.length > 0){
  	 					for(DBRow row : wayBillItems){
  	 			%>
  	 				<p>
  	 					<span class="alert-text" style="font-weight:normal;"><%= row.getString("p_name") %></span> &nbsp;x&nbsp;<span style="color:blue;" class="quantity"><%= row.getString("quantity") %></span>
  	 				</p>
  	 				
  	 			<% 
  	 					
  	 					}
  	 				}
  	 			%>
  	 			
  	 			</fieldset>
  	 		 	 <p style="text-align:left;text-indent:5px;margin-top:3px;" class="alert-text countTotal">总件数:&nbsp;<span class="thisValue"></span></p>
  	 		</td>
  			<td>
  				<!--  运单号 和状态 -->
  				<p>
  					<span class="infoName"><%=expressMgr.getDetailCompany(waybill[index].get("sc_id",0l)).getString("name")%>:</span>
  					<span class="infoValue" style="color:green;"><%= waybill[index].getString("tracking_number")%></span>
  				</p>
  				
  			 
  				 
  				<p>
  					<span class="infoName">出库单:</span>
  					<span class="infoValue" style="color:green;">
  						<%= waybill[index].getString("out_id").length() < 1 ? "无":waybill[index].getString("out_id")%> 
  					</span>
  				</p> 
  				<p>
  					<span class="infoName">打印用时:</span>
  					<span class="infoValue countTime"   end = "<%=waybill[index].getString("print_date") %>"  start="<%=waybill[index].getString("create_date") %>"></span>
  				</p>
  				<p>
  					<span class="infoName">配货用时:</span>
  					<span class="infoValue countTime" end = "<%=waybill[index].getString("delivery_date") %>"  start="<%=waybill[index].getString("print_date") %>"></span>
  				</p>
  					 
  							
  			</td>
  			<td>
  				<!-- 费用 -->
  				<p>
  					<span class="alert-text costName">运费:</span>
  					<span class="costValue"><%= waybill[index].getString("shipping_cost")%></span>
  				</p>
  				<p>
  					<span class="alert-text costName">成本:</span>
  					<span class="costValue"><%= waybill[index].getString("waybill_prodcut_cost")%></span>
  				</p>
  				 
  				<p>
  					<span class="alert-text costName">销售额:</span>
  					<span class="costValue"><%= waybillMgrZR.getSaleRoomByWalBillId(waybill[index].get("waybill_id",0l)).get("sum_sale",0.0f)%></span>
  				</p>
  			</td>
  			 
  		 	<!-- 所属仓库
  				<td class="ps_id" href="<%=waybill[index].get("ps_id",0l)%>"><%= catalogMgr.getDetailProductStorageCatalogById(waybill[index].get("ps_id",0l)).getString("title")%></td>
  			  -->
  			<td> 
  				<!-- 重量 -->
  				<p>
  					<span class="weightName">运单重量:</span>
  					<span class="weightValue"><%= waybill[index].getString("all_weight")%>&nbsp;kg</span>
  				</p>
  				<p>
  					<span class="weightName">打印重量:</span>
  					<span class="weightValue"><%= waybill[index].get("print_weight",0.0f)%>&nbsp;kg</span>
  				</p>
  				<p>
  					<span class="weightName">发货重量:</span>
  					<span class="weightValue"><%= waybill[index].get("delivery_weight",0.0f)%>&nbsp;kg</span>
  				</p>	 
  				
  				
  			</td>
  			<td style="padding-top:5px;padding-bottom:5px;">
  				<!--  地址信息  -->
  			     <p>	
  					<span class="alert-text valueName">Name:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_name")%></span>
				 </p>
				 <p>	
  					<span class="alert-text valueName">Street:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_street")%></span>
				 </p>
				  <p>	
  					<span class="alert-text valueName">City:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_city")%></span>
				 </p>
				 <p>	
  					<span class="alert-text valueName">State:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_state")%></span>
				 </p>
				   <p>	
  					<span class="alert-text valueName">Zip:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_zip")%></span>
				 </p>
				 <p>	
  					<span class="alert-text valueName">Tel:</span>
  					<span class="valueSpan"><%= waybill[index].getString("tel")%></span>
				 </p>
				  <p>	
  					<span class="alert-text valueName">Country:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_country")%></span>
				 </p>
				 
				
  			</td>
  			 
  		</tr>
  <%  
   		}
   	}else{
   	%>
   		<tr style="background:#E6F3C5;">
   			<td colspan="13" style="height:120px;">无数据</td>
   			 
   		</tr>
   	<% 
   	}
  %>
</tbody>
</table>
<script type="text/javascript">
	//countTotal 计算商品总共件数
	 
	$(".countTotal").each(function(){
		var _this = $(this);
		var node = _this.parent().find(".quantity");
		var sum = 0;
		for(var index = 0 , count = node.length ; index < count ; index++ ){
			sum += $(node.get(index)).html() * 1;
		}
		 _this.find(".thisValue").html(sum);

	})
	
 	 // 计算打印用时,配货用时
	 $(".countTime").each(function(){
		var _this = $(this);
	 
		 

		if(_this.attr("end").length > 0 && _this.attr("start").length > 0){
			 var dateEnd = covertStrToDate(_this.attr("end").substr(0,19));
			 var dateStart = covertStrToDate(_this.attr("start").substr(0,19));
			 var v = "" ;
			if(dateEnd && dateStart){
				 v = count(dateEnd,dateStart);
			}
			_this.html(v);
		} 
	 })
	 // 计算时间 4:34 
	 function count(dateEnd,dateStart){
			var end = dateEnd.getTime();
			var start = dateStart.getTime();
			var value = end - start;
	 		if(value > 0){
		 		var str = "";
		 		var hours = value / (1000 * 60 * 60) + "";
		 		 
		 		if(hours.indexOf(".") != -1){
			 		var array = hours.split(".");
					var xiaoshu = "0."+ array[1].substr(1);
					var  min =fixMin( parseInt(xiaoshu * 60));
					str  =array[0] +("小时" + (min * 1 < 1?"":min + "分" ) );
		 		}else{
			 		str = hours + "小时";
			 	}
		 		return str;
		 	}
	 }
	 function fixMin(value){
		 return value < 10 ?("0"+value):value;
	 }
	 function covertStrToDate(str){
		 var array = str.split(/\s+/);
		 var date  ;
		 if(array.length == 2){
			 var dateArray = array[0].split("-");
			 var timeArray = array[1].split(":");
			 if(dateArray.length == 3 && timeArray.length == 3){
				 date = new Date();
				 date.setFullYear(dateArray[0],dateArray[1] - 1,dateArray[2]);
				 date.setHours(timeArray[0],timeArray[1],timeArray[2]);
				 return date;
			 }
			 
		 }
 
	 }
	 	 
</script>
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
    </td>
  </tr>
</table>
 
<form action="" id="pageForm">
	<input type="hidden" name="cmd" value="<%=cmd %>" id="pageCmd"/>
	<input type="hidden" name="out_id" value="<%= StringUtil.getString(request,"out_id") %>"/>
 	<input type="hidden" name="p" value="" id="pageCount"/>
 	 <input type="hidden" name="oid" value="<%=oidR %>" id="pageOid"/>
</form> 
<script type="text/javascript">
function go(number){
	 $("#pageCount").val(number);
	 $("#pageForm").submit();
}
jQuery(function($){
	var cmd = '<%= cmd%>';
	if(cmd === "oid" || cmd === "waybill"){
		$("#oid").val('<%= StringUtil.getString(request,"oid")%>');
		$("#oid").css("color","block");
	}
	
})
</script>
<br />
</body>
</html>