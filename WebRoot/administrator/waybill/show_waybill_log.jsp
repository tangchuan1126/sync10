<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.ProductEditTypeKey"%>
<%@page import="com.cwc.app.key.ProductEditReasonKey"%>
<%@page import="com.cwc.app.key.WaybillInternalOperKey"%>
<%@page import="com.cwc.app.api.zj.WaybillLogMgrZJ"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@ include file="../../include.jsp"%>
<%
	long waybill_id = StringUtil.getLong(request,"waybill_id");
	DBRow[] rows = waybillLogMgrZJ.getWayBillLogByWaybillId(waybill_id);
	
	WaybillInternalOperKey waybillInternalOperKey = new WaybillInternalOperKey(); 
	WaybillInternalTrackingKey waybillInternalTrackingKey = new WaybillInternalTrackingKey();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>运单日志</title>
    
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
	function checkTracking()
	{
		if($("#internal_tracking_status").val()!=-1)
		{
			return true;
		}
		else
		{
			alert("请选择运输状态");
			return false;
		}
	}
</script>

</head>
  
  <body>
  	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  		<tr>
  			<td>
  				<br/>
  				<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" class="zebraTable" >
  					<tr>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">内部运单号</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">外部运单号</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">内部操作</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">外部状态</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">操作人</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">操作时间</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">StatusCode</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">Definition</th>
  						<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">TrackingWeight</th>
  					</tr>
  					<%
  						for(int i = 0;i<rows.length;i++)
  						{
  					%>
	  					<tr>
	  						<td height="30"><%=rows[i].getString("waybill_id")%></td>
	  						<td><%=rows[i].getString("trackingNumber")%>&nbsp;</td>
	  						<td><%=waybillInternalOperKey.getWaybillInternalOper(rows[i].getString("waybill_internal_status"))%></td>
	  						<td>
	  							<%=waybillInternalTrackingKey.getWaybillInternalTrack(rows[i].getString("waybill_external_status"))%>&nbsp;
							</td>
	  						<td><%=adminMgr.getDetailAdmin(rows[i].get("operator_adid",0l)).getString("employe_name")%></td>
	  						<td><%=rows[i].getString("operator_time")%></td>
	  						<td><%=rows[i].getString("tracking_status_code")%>&nbsp;</td>
	  						<td><%=rows[i].getString("tracking_definition")%>&nbsp;</td>
	  						<td><%=rows[i].getString("tracking_weight")%>&nbsp;</td>
	  					</tr>
  					<%
  						}
  					%>
  				</table>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/updateArtificialWaybillTracing.action" name="update_artificial_waybill_tracking" onsubmit="return checkTracking()">
  				<input type="hidden" name="waybill_id" value="<%=waybill_id%>"/>
				<table width="100%">
  					<tr>
  						<td align="right">
  							修改运单的运输状态
  							<select name="internal_tracking_status" id="internal_tracking_status" style="border:1px #CCCCCC solid" onchange="selectTracking(this.value)">
								<option value="-1">运输状态</option>
												<%
						   							ArrayList waybillInternalTrack = waybillInternalTrackingKey.getWaybillInternalTrack();
						   							
						   							for(int i = 0;i<waybillInternalTrack.size();i++)
						   							{
						   						%>
						   						<option value="<%=waybillInternalTrack.get(i)%>"><%=waybillInternalTrackingKey.getWaybillInternalTrack(waybillInternalTrack.get(i).toString())%></option>
						   						<%
						   							}
						   						%>
								</select>
  						</td>
  						<td>
  							<input type="submit" name="Submit2" value="确定" class="normal-green">
							<input type="button" name="Submit2" value="关闭" class="normal-white" onClick="$.artDialog.close();">
  						</td>
  					</tr>
  				</table>
  				</form>
  			</td>
  		</tr>
  	</table>
	  	
  </body>
</html>
