<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	long ps_id = StringUtil.getLong(request,"ps_id");
	int[] product_status = new int[1];
	product_status[0] = ProductStatusKey.STORE_OUT;
																			
	DBRow[] lackingProducts = productStorageMgrZJ.getAllProductStorageByPcid(0l,ps_id,ProductStatusKey.STORE_OUT,0l,-1,null);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>缺货通知单</title>
</head>
<body>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table"  style="margin-top:5px;">
	<thead>
		<tr>
			<td colspan="6">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td style="font-family: Arial Black;font-size: 19px; padding-bottom:10px;">
							<%
								DBRow catalogPs = catalogMgr.getDetailProductStorageCatalogById(ps_id);
								
								if(catalogPs ==null)
								{
									catalogPs = new DBRow();
									catalogPs.add("title","全部");
								}
								
								out.print(catalogPs.getString("title"));
							%>仓库缺货通知单
						</td>
						<td nowrap="nowrap" align="left" style="font-family: 黑体;font-size:large;padding-bottom:10px;">
							<%=DateUtil.NowStr()%>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							包含运单:
							<%
								DBRow[] wayBillOrders = wayBillMgrZJ.filterWayBillOrder("","",null,0,0,"",ps_id,0,0,0,0,null,-1,0,ProductStatusKey.STORE_OUT,0);
								for(int q=0;q<wayBillOrders.length;q++)
								{
									out.print(wayBillOrders[q].get("waybill_id",0l));
									out.print(" ");
								}
							%>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">商品名称</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">缺货运单编号</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">缺货数量</th
		</tr>
	</thead>
	<tbody id="tbody">
	  <%
	   	if(null != lackingProducts&&lackingProducts.length > 0)
	   	{
	   		for(int i=0;i<lackingProducts.length;i++)
	   		{
	  %>
	  		<tr>
	  			<td align="left" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;"><%=productMgr.getDetailProductByPcid(lackingProducts[i].get("pc_id",0l)).getString("p_name")%></td>
	  			<td align="left" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;">
	  				<%
	  					DBRow[] waybills = wayBillMgrZJ.getWayBillOrderByPcid(lackingProducts[i].get("pc_id",0l),ps_id,product_status);
	  					for(int j=0;j<waybills.length;j++)
	  					{
	  						out.println(waybills[j].get("waybill_id",0l));
	  						out.println("");
	  					}
	  				%>
	  			</td>
	  			<td align="center" style="border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 16px;"><%=lackingProducts[i].get("store_count",0f)%></td>
	  		</tr>
	  <%  
	   		}
	   	}
	   	else
	   	{
	  %>
	   		<tr style="background:#E6F3C5;">
	   			<td colspan="9" style="height:120px;">无数据</td>
	   		</tr>
	  <% 
	   	}
	  %>
	</tbody>
</table>
</body>
</html>