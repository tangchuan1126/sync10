<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.WaybillInternalOperKey"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.key.WaybillTrackeTypeKey"%>
<%@page import="com.cwc.app.key.SystemTrackingKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>运单处理</title>


<link type="text/css" href="../comm.css" rel="stylesheet" />
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>


<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />


 <style type="text/css">
div.dingdan1{border: 0px solid #DDDDDD;border-radius: 7px 7px 7px 7px;padding: 5px 5px 5px 15px;width:1050px;margin-bottom:5px;}
table.fixTable td{height:30px;}
 
span.create{display:block;float:left;border:1px solid red;cursor:pointer;}
p{display:block;clear:both;}
span.valueName{width:40%;border:0px solid red;display:block;float:left;text-align:right;}
span.valueSpan{width:56%;border:0px solid green;float:left;overflow:inherit;text-align:left;text-indent:4px;}
span.costName{width:45%;float:left;text-align:right;}
span.costValue{width:50%;display:block;float:left;text-indent:4px;overflow:inherit;text-align:left;font-weight:bold;}
span.weightName{width:60%;display:block;float:left;text-align:right;border:0px solid red;}
span.weightValue{width:30%;display:block;float:left; text-indent:4px;font-weight:bold;}
span.infoName{width:45%;float:left;text-align:right;}
span.infoValue{width:50%;display:block;float:left;text-indent:4px;overflow:inherit;text-align:left;font-weight:bold;}
span.stateName{width:15%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:inherit;text-align:left;font-weight:normal;}
span.topInfo{display:block;position:relative;top:-20px;width:50px;height:20px;line-height:20px;left:10px;border:1px solid red;}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
.fix{text-align:left;width:95%;}
.title{font-size:12px;color:green;font-weight:blod;}
.track_title{font-size:12px;font-weight:blod;}
.zebraTable td {border:none;}
tr.split td input{margin-top:2px;}
 </style>
 <%
 PageCtrl pc = new PageCtrl();
 pc.setPageNo(StringUtil.getInt(request,"p"));
 pc.setPageSize(systemConfig.getIntConfigValue("order_page_count"));
 DBRow waybill[] = null;
 //
 String p_name = StringUtil.getString(request,"p_name","");
	   

	   
 String order_source =  StringUtil.getString(request,"order_source","");
 long ps_id = StringUtil.getLong(request,"ps_id",0l);
 long ca_id = StringUtil.getLong(request,"ca_id",0l);
 long ccid = StringUtil.getLong(request,"ccid",0l);
 long pro_id = StringUtil.getLong(request,"pro_id",0l);
 long out_id = StringUtil.getLong(request,"out_id",0l);
 long pcid = StringUtil.getLong(request,"pcid",0l);;
 long pro_line_id = StringUtil.getLong(request,"pro_line_id",0l); ;
 int status = StringUtil.getInt(request,"status");
 int product_status = StringUtil.getInt(request,"product_status");
 WayBillOrderStatusKey wos = new WayBillOrderStatusKey();
 
 String cmd = StringUtil.getString(request,"cmd");
 
 String st = StringUtil.getString(request,"st");
 String en = StringUtil.getString(request,"en");
 int date_type = StringUtil.getInt(request,"date_type",-1);
 float differences = StringUtil.getFloat(request,"difference",-1);
 int cost = StringUtil.getInt(request,"cost",-1);
 int cost_type = StringUtil.getInt(request,"cost_type",-1);
 long sc_id = StringUtil.getLong(request,"sc_id",0);
 int unfinished = StringUtil.getInt(request,"unfinished",-1);
 int store_out_unfinished = StringUtil.getInt(request,"store_out_unfinished",-1);
 int internal_tracking_status = StringUtil.getInt(request,"internal_tracking_status");
 
 int difference_type = StringUtil.getInt(request,"difference_type");
 int trace_type = StringUtil.getInt(request,"trace_type");
 int search_mode = StringUtil.getInt(request,"search_mode");
 
 if(cmd.equals("waybillId"))
 {
	
 	String searchKey = StringUtil.getString(request,"oid");
 	if(searchKey.length() > 0)
	{
 		//waybill = wayBillMgrZJ.getWayBillOrderByWayBillId(waybill_id,pc);
 		waybill = waybillMgrZR.getWayBillBySearchKey(searchKey.toLowerCase(),search_mode,pc);
 	}
 }
 else if(cmd.equals("statistics"))
 {
 	waybill = wayBillMgrZJ.statisticsWayBillOrder(st,en,1,sc_id,differences,difference_type,cost,cost_type,ps_id,unfinished,store_out_unfinished,pc);
	
 }
 else if(cmd.equals("trace"))
 {
 	if(trace_type == SystemTrackingKey.Stockout)
 	{
 		waybill = wayBillMgrZJ.getStockoutWaybillByPs(ps_id,pc);
 	}
 	else if(trace_type == SystemTrackingKey.Overdue)
 	{
 		waybill = wayBillMgrZJ.getOverdueSendWaybillOrderByPs(ps_id,pc);
 	}
 }
 else
 {
	   
	   waybill= wayBillMgrZJ.filterWayBillOrder(st,en,p_name,pro_line_id,pcid,order_source,ps_id,ca_id,ccid,pro_id,out_id,pc,status,sc_id,product_status,internal_tracking_status);
 }
 
  ProductStatusKey productStatus = new ProductStatusKey();
  WaybillInternalTrackingKey  waybillInternalTrackingKey = new WaybillInternalTrackingKey();
 %>
<script type="text/javascript">
function refreshWindow(){
	var number = '<%= StringUtil.getInt(request,"p")%>' * 1;
	go(number);
}
function followup(waybill_id)
{
	$.artDialog.open("add_waybill_note.html?waybill_id="+waybill_id,{title: "运单跟进",width:'420px',height:'280px',fixed:true, lock: true,opacity: 0.3});
}
	function getMoreNote(waybillId){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/waybill/show_waybill_note.html?waybill_id="+waybillId; 
		 $.artDialog.open(uri , {title: '更多运单日志',width:'600px',height:'280px', lock: true,opacity: 0.3,fixed: true});
	}
 	//
 	function printChuku(out_id)
 	{
		visionariPrinter.PRINT_INIT("出库单");
		visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
		visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../waybill/out_list_print.html?out_id="+out_id);
		if(visionariPrinter.PREVIEW()>0)
		{
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/outbound/closeOutbound.action',
					type: 'post',
					dataType: 'html',
					timeout: 30000,
					cache:false,
					data:"out_id="+out_id,
									
				beforeSend:function(request){
				},
									
				error: function(){
					alert("打印出库单失败，请重新打印");
				},
									
				success: function(msg){
					if (msg!="ok")
					{
						alert("打印出库单失败，请重新打印");
					}
				}
			});
		}
 	}
	 //页面全选 操作等
	 function checkAll(){
		var check = $("#allCheck");
		if(check.attr("checked")){
			// 查找所有的input然后把
			$("input[type='checkbox']",$("#tbody")).each(function(){
				var _this = $(this);
				_this.attr("checked",true);
				_this.parent().parent().addClass("over");
			})
		}else{
				
			$("input[type='checkbox']",$("#tbody")).attr("checked",false);
			$("tr",$("#tbody")).removeClass("over");
		}
	 }
	 function singleClick(_this){
		 var node = $(_this);
		 node.parent().parent().removeClass("over");
		 $("#allCheck").attr("checked",checkIsAllChecked());
	 }
	 function checkIsAllChecked(){
		 var flag = true;
		 var inputs = $("input[type='checkbox']",$("#tbody"));
		 for(var index = 0 ; index < inputs.length ; index++ ){
			 if(!$(inputs[index]).attr("checked")){
				 flag = false;
				 break;
			  }
		 }
		 return flag;
	}
	 
	 // 获取tbody下所有打钩的选项的Ids
	 function getSelectedIds(){
		var ids= ""; 
		 var inputs = $("input[type='checkbox']:checked",$("#tbody"));
		 if(inputs.length > 0){
			 for(var index = 0 ; index < inputs.length; index++ ){
				 ids += ","+$(inputs[index]).attr("id").replace("id_","");
			}
		 } 
		 return ids == ""?"":ids.substr(1);
	 }
	 
	 function cancelWayBill(waybill_id,trancing_number)
		{
			if(confirm("确定要取消"+trancing_number+"运单吗？"))
			{
				var para = "waybill_id="+waybill_id;//传Oid是为了初始化订单购物车
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/CancelWayBillOrderFromStore.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
						//$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">取消中...</span>' });
					},
					
					error: function(){
					},
					
					success: function(data)
					{
						//$.unblockUI();
						if(data.result=="ok")
						{
							window.location.reload();	
						}
					}
				});
			}
			
		}
	 
	 onLoadInitZebraTable();
	 jQuery(function($){
		var cmenu3 = $(".can_send_message");
		cmenu3.each(function(){
			var node = $(this);
			var order_id = node.attr("order_id");
	 
			var menu = [
					
				{"<span style='font-size:15px;'>Send Ebay Message</span>":{}},
				$.contextMenu.separator,
				{'Send Message':{
				    icon:'../js/contextMenu/page_white_add.png',
				    onclick:function(){
				    	openSendMessageUrl(order_id );
				    }
				}}
				 
				
			]
			 node.contextMenu(menu,{theme:'vista'}); 
		})
	  })
	 function openSendMessageUrl(order_id){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/send_ebay_message.html?oid="+order_id;
	    $.artDialog.open(uri , {title: 'Send Ebay Message',width:'700px',height:'280px', lock: true,opacity: 0.3,fixed: true});
 	}
</script>
</head>
<body>
 <table width="99%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" isNeed="true" isBottom="true" style="margin-top:5px;" >
<thead>
<tr>
<th width="3%" class="right-title"   ><input type="checkbox" id="allCheck" onclick="checkAll();"/></th>
<th width="16%"   class="right-title"  style="text-align: center;">编号</th>
 <th width="16%"  class="right-title" style="text-align: center;">运单明细</th>
 <th width="16%"   class="right-title"  style="text-align: center;">运单号</th>
 <th width="8%"   class="right-title"  style="text-align: center;">费用</th>
  <th width="8%"  class="right-title"  style="text-align: center;" >重量</th>
  <th width="18%"  class="right-title"  style="text-align: center;" >地址</th>
    <th width="10%" class="right-title" style="text-align: center;">日志</th>
</tr>
</thead>
<tbody id="tbody">
  <%
   	if(null != waybill && waybill.length > 0){
   		for(int index = 0, count = waybill.length ; index < count ; index++ ){
  %>
  		<tr>
  			<td><input type="checkbox" id= 'id_<%= waybill[index].getString("waybill_id")%>' onclick="singleClick(this);" psId = "<%=waybill[index].getString("ps_id") %>" status="<%= waybill[index].get("status",0) %>"/></td>
  			<td>
		  			<fieldset   class="set" style="border:2px solid #993300;">
		  				 <legend>
		  				 		<%
		  				 			String showTime = waybill[index].getString("create_date");
		  				 			showTime =   showTime.substring(5,16) ;
		  				 		%>
								<span style="" class="title"><%= waybill[index].get("waybill_id",0l)%></span>
						</legend>
		  				 <%
		  				 	DBRow[] orderRows = wayBillMgrZJ.returnOrdersByWayBillId(waybill[index].get("waybill_id",0l));
		  				 	if( null != orderRows && orderRows.length > 0){
		  				 		 for(DBRow row : orderRows){
		  				 			 long oid = row.get("oid",0l);
		  				 			 if(oid != 0l)
		  				 			 {
		  				 				DBRow dateRow = orderMgr.getDetailPOrderByOid(oid);
		  				 				long postDate = 0;
		  				 				long hour = 0;
		  				 				long minute = 0;
		  				 				long second =0;
		  				 				if(dateRow != null)
		  				 				 {
		  				 					postDate = dateRow.get("outbound_cost",0l)/1000;
		  				 					
		  				 					hour = postDate/3600;
   											minute = postDate%3600/60;
   											second = postDate%60/60;
		  				 					
		  				 				}
		  				 				%>
		  				 				<p style="text-align:left;text-indent:5px;font-weight:normal;">
		  				 					<!-- ebay 订单是可以发送Ebay Message的 -->
		  				 				<% boolean isEbayOrder = row.getString("order_source").equals("EBAY"); %>
		  				 				<span style="color:blue;" order_id = '<%= row.get("oid",0l)%>' class='<%=(isEbayOrder?"can_send_message":"") %>'><%=row.getString("order_source") %></span>: 
		  				 			
		  				 					<span class="alert-text"><%=row.get("oid",0l) %></span> &nbsp;
		  				 					<%=hour+"时"+minute+"分"+second+"秒" %>
		  				 					<!-- <span class="countTime" end='<%=waybill[index].getString("create_date") %>' start='<%=postDate %>'></span> -->
		  				 				</p>
		  				 				<% 
		  				 		 }
		  				 		}
		  				 	}
		  				 %> 
		  				 <%
		  				 	if(waybill[index].get("replace_waybill_id",0l)>0)
		  				 	{
		  				 %>
		  				 	<p style="text-align:left;text-indent:5px;font-weight:normal;">
		  				 		替换:<a href="javascript:searchRepalceWaybill('<%=waybill[index].get("replace_waybill_id",0l)%>')"><%=waybill[index].get("replace_waybill_id",0l)%></a>
		  					</p>
		  				 <%
		  				 	}
		  				 %>
		  				 
		  			</fieldset>
  					<%
  							//在这里判断当前的状态是 什么然后选择那些count he state 
  							DBRow userInfo = null ;
  							 String userName = "";
  							String[] arrayCount = {"create_account","print_account","delivery_account","cancel_account","split_account"};
  							String[] arrayTime = {"create_date","print_date","delivery_date","cancel_date","split_date"};
  							int w = waybill[index].get("status",0);
  							long countId = waybill[index].get(arrayCount[w],0l);
  							String dateTime = waybill[index].getString(arrayTime[w]);
  							userInfo = waybillMgrZR.getUserNameById(countId);
  							if(userInfo != null ){
  								  userName =  userInfo.getString("employe_name");
  							}
  							dateTime = dateTime.length() >= 19 ? dateTime.substring(0,19):dateTime;
  							String wayBillState = wos.getStatusById(w);
  							
  							String tracking_status = waybillInternalTrackingKey.getWaybillInternalTrack(waybill[index].getString("internal_tracking_status"));
  							
  							DBRow shipping_company = expressMgr.getDetailCompany(waybill[index].get("sc_id",0l));
  							String company_name = shipping_company.getString("name");
  					%>
  			<fieldset style="" class="set" style="padding-bottom:4px;">
  				 <legend>
						<span class="title">
							<%=wayBillState%>
						</span>
						<%
							if(w==WayBillOrderStatusKey.SHIPPED&&!tracking_status.equals(""))
							{
								String trackingUrl = trackingUrl = shipping_company.getString("tracking_url").replace("[trackingNumber]",waybill[index].getString("tracking_number"));
						%>
							|<span class="track_title">
								<a href="<%=trackingUrl%>" target="_blank"><%=tracking_status%></a>
								<a href="javascript:showWaybillLog(<%=waybill[index].get("waybill_id",0)%>)"><img src="../imgs/modify.gif" width="12" height="12" border="0" alt="运单日志"/></a>
							</span>
						<%
							}
						%>
				</legend>
  				<p>
  					<span class="alert-text stateName"><img title="操作人" src="../imgs/order_client.gif" /></span>
  					<span class="stateValue"><%=userName %></span>
  				</p>
  				<p>
  					<span class="alert-text stateName"><img  title="操作时间" src="../imgs/alarm-clock--arrow.png" /></span>
  					<span class="stateValue"><%= dateTime%></span>
  				</p>
  			</fieldset>
  	 		</td>
  	 		<td style="padding:0px;margin:0px;">
  	 			<!-- 运单明细 -->
  	 			<fieldset style="" class="set fix">
		  			 <legend><span style="" class="title"><%= catalogMgr.getDetailProductStorageCatalogById(waybill[index].get("ps_id",0l)).getString("title")%> 仓库</span></legend>		 
  	 			<%
  	 				DBRow[] wayBillItems = wayBillMgrZJ.getWayBillOrderItems(waybill[index].get("waybill_id",0l));
  	 				if(null != wayBillItems && wayBillItems.length > 0){
  	 					for(DBRow row : wayBillItems){
  	 			%>
  	 				<p>
  	 					<span class="alert-text" style="font-weight:normal;"><%= row.getString("p_name") %></span> &nbsp;x&nbsp;<span style="color:blue;" class="quantity"><%= row.getString("quantity") %></span>
  	 					<%
  	 						if(row.get("product_status",0)==ProductStatusKey.STORE_OUT)
  	 						{
  	 					%>
  	 						<a href="javascript:puchaseDeliveryCount(<%=row.get("pc_id",0l)%>,<%=waybill[index].get("ps_id",0l)%>,'deliveryCount')">[<%=productStatus.getOrderHandleById(row.getString("product_status"))%>]</a>
  	 					<%
  	 						}
  	 					%>
  	 				</p>
  	 			<% 
  	 					}
  	 				}
  	 			%>
  	 			</fieldset>
  	 		 	 <p style="text-align:left;text-indent:5px;margin-top:3px;" class="alert-text countTotal">总件数:&nbsp;<span class="thisValue"></span></p>
  	 		</td>
  			<td nowrap="nowrap">
  				<!--  运单号 和状态 -->
  				<p>
  					<span class="infoName" style="width:100%;text-align:left;">
  						<% 
  							
  							out.print(company_name);
  						%>
  					</span>
  				</p>
  				<p>
  					<span class="infoValue" style="width:100%;color:green;text-align:left;"><%= waybill[index].getString("tracking_number")%></span>
  				</p>
  				<p>
  					<span class="infoName">出库单:</span>
  					<span class="infoValue" style="color:green;">
  						<%= waybill[index].getString("out_id").length() < 1 ? "无":waybill[index].getString("out_id")%>
  						&nbsp;
  						<%if(waybill[index].getString("out_id").length() > 0){%>
  						<!--<img onclick="printChuku()" style="cursor:pointer;width:12px;height:12px;" src="../imgs/order_printer.gif" /> -->
  						<%}%>
  					</span>
  				</p> 
  				<p>
  					<span class="infoName">创建时间:</span>
  					<span class="infoValue"><%=showTime%></span>
  				</p>
  				<%
  					long lacking_cost = waybill[index].get("lacking_cost",0l)/1000;
  					
  					long lacking_cost_hour = lacking_cost/3600;
   					long lacking_cost_minute = lacking_cost%3600/60;
   					long lacking_cost_second = lacking_cost%60/60;
   					
   					if(lacking_cost>0)
   					{
   				%>
   				<p>
   					<span class="infoName">曾缺货:</span>
   					<%=lacking_cost_hour+"时"+lacking_cost_minute+"分"+lacking_cost_second+"秒"%>
   				</p>
   				<%
   					}
  				%>
  				<p>
  					<span class="infoName">打印用时:</span>
  					<%
  						
  						long print_cost = waybill[index].get("print_cost",0l)/1000;
  						
  						long print_cost_hour = print_cost/3600;
   						long print_cost_minute = print_cost%3600/60;
   						long print_cost_second = print_cost%60/60;   						
  						if(print_cost>0)
  						{
  							out.print(print_cost_hour+"时"+print_cost_minute+"分"+print_cost_second+"秒");
  						}
  						
  					%>

  				</p>
  				<p>
  					<span class="infoName">配货用时:</span>
  					<%
  						long delivery_cost = waybill[index].get("delivery_cost",0l)/1000;
  						
  						long delivery_cost_hour = delivery_cost/3600;
   						long delivery_cost_minute = delivery_cost%3600/60;
   						long delivery_cost_second = delivery_cost%60/60;   						
  						
  						if(delivery_cost>0)
  						{
  							out.print(delivery_cost_hour+"时"+delivery_cost_minute+"分"+delivery_cost_second+"秒");
  						}
  					%>
  				</p>
  				
  			</td>
  			<td nowrap="nowrap">
  				<!-- 费用 -->
  				<p>
  					<span class="alert-text costName">估算运费:</span>
  					<span class="costValue"><%= waybill[index].getString("shipping_cost")%></span>
  				</p>
  				<p>
  					<span class="alert-text costName">通讯运费:</span>
  					<span class="costValue"><%= waybill[index].getString("shippment_rate")%></span>
  				</p>
  				<p>
  					<span class="alert-text costName">成本:</span>
  					<span class="costValue"><%= waybill[index].getString("waybill_prodcut_cost")%></span>
  				</p>
  				<p>
  					<span class="alert-text costName">销售额:</span>
  					<span class="costValue"><%= waybillMgrZR.getSaleRoomByWalBillId(waybill[index].get("waybill_id",0l)).get("sum_sale",0.0f)%></span>
  				</p>
  			</td>
  		 	<!-- 所属仓库
  			<td class="ps_id" href="<%=waybill[index].get("ps_id",0l)%>"><%= catalogMgr.getDetailProductStorageCatalogById(waybill[index].get("ps_id",0l)).getString("title")%></td>
  			  -->
  			<td nowrap="nowrap"> 
  				<!-- 重量 -->
  				<p>
  					<span class="weightName">运单重量:</span>
  					<span class="weightValue"><%= waybill[index].getString("all_weight")%>&nbsp;kg</span>
  				</p>
  				<p>
  					<span class="weightName">打印重量:</span>
  					<span class="weightValue"><%= waybill[index].get("print_weight",0.0f)%>&nbsp;kg</span>
  				</p>
  				<p>
  					<span class="weightName">发货重量:</span>
  					<span class="weightValue"><%= waybill[index].get("delivery_weight",0.0f)%>&nbsp;kg</span>
  				</p>
  				
  				
  					<p>
	  					<span class="weightName">运输重量:</span>
	  					<span class="weightValue">
	  						<%
  								if(waybill[index].get("tracking_weight",0.0f)>0)
  								{
  							%>
	  							<%=waybill[index].get("tracking_weight",0.0f)%>&nbsp;kg
	  						<%
			  					}
			  				%>
	  					</span>
	  				</p>
  			</td>
  			<td style="padding-top:5px;padding-bottom:5px;">
  				<!--  地址信息  -->
  			     <p>	
  					<span class="alert-text valueName">Name:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_name")%></span>
				 </p>
				  <p>	
  					<span class="alert-text valueName">Street:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_street")%></span>
				 </p>
				  <p>	
  					<span class="alert-text valueName">City:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_city")%></span>
				 </p>
				  <p>	
  					<span class="alert-text valueName">State:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_state")%></span>
				 </p>
				  <p>	
  					<span class="alert-text valueName">Zip:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_zip")%></span>
				 </p>
  				<p>	
  					<span class="alert-text valueName">Tel:</span>
  					<span class="valueSpan"><%= waybill[index].getString("tel")%></span>
				 </p>
				 <p>	
  					<span class="alert-text valueName">Country:</span>
  					<span class="valueSpan"><%= waybill[index].getString("address_country")%></span>
				 </p>
				 <%
				 	if(!waybill[index].getString("residential_status").equals(""))
				 	{
				 %>
				 	<p align="center">
				 		<span style="font-style: italic;"><%=waybill[index].getString("residential_status")%></span>
				 	</p>
				 <%	
				 	}
				 %>
  			</td>
  			<td>
  				<%
  				WaybillTrackeTypeKey trackeTypeKey = new WaybillTrackeTypeKey();
  					long wayBillIdTemp = waybill[index].get("waybill_id",0l);
  					if(wayBillIdTemp != 0l){
  						DBRow[] noteRows =  waybillMgrZR.getWayBillNoteBy(wayBillIdTemp,4);		
  						boolean hasMore = noteRows.length > 3 ? true:false;
  						int noteCount = hasMore?3:noteRows.length ; 
  						for(int noteIndex =0  ; noteIndex < noteCount ; noteIndex++ ){
  							DBRow noteTemp = noteRows[noteIndex];
  							%>
  							 <div style="font-size:12px;margin-bottom:3px;line-height:15px;word-wrap:break-word;width:220px;border-bottom:1px dashed silver;padding-bottom:3px;padding-top:3px;">
								<span style="font-size:12px;">
							 		<font style="color:#f60;">
							 			<%= trackeTypeKey.getWaybillTrackeType(noteTemp.get("trace_type",0)+"")%>
							 		</font><br />
									<strong><%= noteTemp.getString("employe_name") %></strong>
									<span style="color:#999999;font-size:11px;font-farmliy:Verdana"><%= noteTemp.getString("post_date") %></span>
								</span><br />
								<%=noteTemp.getString("note")  %>
 							  </div>
  							<% 
  						}
  						if(hasMore){
  							%>
  								<a href="javascript:getMoreNote('<%=wayBillIdTemp  %>')">更多</a>
  							<%
  						}
  					}
  				%>
  			</td>
  		</tr>
  		<tr class="split">
  			<td colspan="7">
  			 <!-- 
				1.( parent_id = 0) 或者 -1  && 待打印 可以拆分
				2. 非拆分中 且 不是非取消　可以　打印
				3. parent_id -1 的时候是表示的是表示的拆分中  
			 -->
  				<%
		  			String parentWayBillId = waybill[index].getString("parent_waybill_id");
		  			
		  			boolean splitFlag = false;
		  		%>
  				<%
  					if(waybill[index].get("status",0)!=WayBillOrderStatusKey.CANCEL  && waybill[index].get("status",0)!=WayBillOrderStatusKey.SPLIT&&waybill[index].get("product_status",0)!=ProductStatusKey.STORE_OUT)
  					{
  				%>
  					<input class="long-button-print" type="button" value="打印运单" onclick="print(this,'<%= waybill[index].getString("waybill_id")%>','<%= waybill[index].getString("out_id")%>','<%= expressMgr.getDetailCompany(waybill[index].get("sc_id",0l)).getString("print_page")%>')"/>
  				<%
  					}
  				%>
  				<!--  拆分按钮的 待打印、已打印、拆分中，并且非缺货运单-->
  				<%	
		  			if((waybill[index].get("status",0)==WayBillOrderStatusKey.SPLIT||waybill[index].get("status",0)==WayBillOrderStatusKey.WAITPRINT||waybill[index].get("status",0)==WayBillOrderStatusKey.PERINTED)&&waybill[index].get("product_status",0)!=ProductStatusKey.STORE_OUT)
		  			{
		  				if(!backUpStorageMgrZJ.existsSplitRequireDetailForWaybill(waybill[index].get("waybill_id",0l)))
  						{
		  		%>
		  		   <tst:authentication bindAction="com.cwc.app.api.zr.WayBillMgrZR.splitWayBill">
		  		   	<input class="short-short-button-convert" type="button"   value='拆分'  onclick="chaifen('<%= waybill[index].getString("waybill_id")%>','<%= waybill[index].getString("out_id")%>')"/>
		  		   </tst:authentication>	
		  	    <%
		  	    		}
		  			}
		  	    %> 	
  				<% 
  					if(waybill[index].get("status",0)==WayBillOrderStatusKey.PERINTED||waybill[index].get("status",0)==WayBillOrderStatusKey.WAITPRINT)
  					{
  						if(!backUpStorageMgrZJ.existsSplitRequireDetailForWaybill(waybill[index].get("waybill_id",0l)))
  						{
  				%>
  				<tst:authentication bindAction="com.cwc.app.api.zj.WayBillMgrZJ.cancelWayBillOrderFromStore">
  					<input class="short-short-button-cancel" type="button" value="取消" onclick="cancelWayBill(<%=waybill[index].get("waybill_id",0l)%>,'<%=waybill[index].getString("tracking_number")%>')"/>
  				</tst:authentication>
  				<% 
  						}
  					}
  				%>
  				<%
  					if(waybill[index].get("status",0)==WayBillOrderStatusKey.SHIPPED)
  					{
  				%>
  					 <input class="long-button" type="button" value="运单异常" onclick="wayBillReturn(<%=waybill[index].get("waybill_id",0l)%>)"/>
  					 &nbsp;&nbsp;
  					 <input class="long-button" type="button" value="新单发货" onclick="reDispatched(<%=waybill[index].get("waybill_id",0l)%>)"/>
  				<%
  					}
  				%>
  				
  				<%
  					if(waybill[index].get("return_type",0)!=0)
  					{
  				%>
  					 <input class="long-button" type="button" value="被退处理" onclick="wayBillResult(<%=waybill[index].get("waybill_id",0l)%>)"/>
  				<%
  					}
  				%>
  			</td>
  			<td>
  				<input type="button" class="short-short-button-mod" onclick="followup(<%=waybill[index].get("waybill_id",0l)%>)" value="跟进"/>
  			</td>
  		</tr>
  <%  
   		}
   	}else{
   	%>
   		<tr style="background:#E6F3C5;">
   			<td colspan="13" style="height:120px;">无数据</td>
   			 
   		</tr>
   	<% 
   	}
  %>
</tbody>
</table>
<script type="text/javascript">
	//countTotal 计算商品总共件数
	$(".countTotal").each(function(){
		var _this = $(this);
		var node = _this.parent().find(".quantity");
		var sum = 0;
		for(var index = 0 , count = node.length ; index < count ; index++ ){
			sum += $(node.get(index)).html() * 1;
		}
		 _this.find(".thisValue").html(sum);
	})
 	 // 计算打印用时,配货用时
	 $(".countTime").each(function(){
		var _this = $(this);
		if(_this.attr("end").length > 0 && _this.attr("start").length > 0){
			 var dateEnd = covertStrToDate(_this.attr("end").substr(0,19));
			 var dateStart = covertStrToDate(_this.attr("start").substr(0,19));
			 var v = "" ;
			if(dateEnd && dateStart){
				 v = count(dateEnd,dateStart);
			}
			_this.html(v);
		} 
	 })
	 // 计算时间 4:34 
	 function count(dateEnd,dateStart){
			var end = dateEnd.getTime();
			var start = dateStart.getTime();
			var value = end - start;
	 		if(value > 0){
		 		var str = "";
		 		var hours = value / (1000 * 60 * 60) + "";
		 		if(hours.indexOf(".") != -1){
			 		var array = hours.split(".");
					var xiaoshu = "0."+ array[1].substr(1);
					var  min =fixMin( parseInt(xiaoshu * 60));
					str  =array[0] +("小时" + (min * 1 < 1?"":min + "分" ) );
		 		}else{
			 		str = hours + "小时";
			 	}
		 		return str;
		 	}
	 }
	 function fixMin(value){
		 return value < 10 ?("0"+value):value;
	 }
	 function covertStrToDate(str){
		 var array = str.split(/\s+/);
		 var date  ;
		 if(array.length == 2){
			 var dateArray = array[0].split("-");
			 var timeArray = array[1].split(":");
			 if(dateArray.length == 3 && timeArray.length == 3){
				 date = new Date();
				 date.setFullYear(dateArray[0],dateArray[1] - 1,dateArray[2]);
				 date.setHours(timeArray[0],timeArray[1],timeArray[2]);
				 return date;
			 }
		 }
	 }
</script>
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
    </td>
  </tr>
</table>
<form id="queryForm" action="">
	<input type="hidden" class="p" name="page" value="<%=StringUtil.getInt(request,"p") %>"/>
	<input type="hidden" id="cmd" name="cmd" value="<%=cmd %>"/>
	<input type="hidden" id="oid" name="oid" value="<%=StringUtil.getString(request,"oid")%>"/>
	<input type="hidden" id="search_mode" name="search_mode" value="<%=StringUtil.getString(request,"search_mode")%>"/>
</form>

<form id="filterForm" action = "">
	<input type="hidden" name="p" value="<%=StringUtil.getInt(request,"p") %>"/>
	<input type="hidden" id="p_name" name="p_name" value="<%=p_name%>"/>
	<input type="hidden" id="pcid" name="pcid" value="<%=pcid%>"/>
	<input type="hidden" id="pro_line_id" name="pro_line_id" value="<%=pro_line_id%>"/>
	<input type="hidden" id="order_source" name="order_source" value="<%=order_source%>"/>
	<input type="hidden" id="ps_id" name="ps_id" value="<%=ps_id%>"/>
	<input type="hidden" id="ca_id" name="ca_id" value="<%=ca_id%>"/>
	<input type="hidden" id="ccid" name="ccid" value="<%=ccid%>"/>
	<input type="hidden" id="pro_id" name="pro_id" value="<%=pro_id%>"/>
	<input type="hidden" id="out_id" name="out_id" value="<%=out_id%>"/>
	<input type="hidden" id="status" name="status" value="<%=status%>"/>
	<input type="hidden" id="product_status" name="product_status" value="<%=product_status%>"/>
	<input name="st" id="st_date" type="hidden" value="<%=st%>"/>
	<input name="en" id="en_date" type="hidden" value="<%=en%>"/>
	<input name="sc_id" id="scid" type="hidden" value="<%=sc_id %>"/>
	<input name="internal_tracking_status" id="internal_tracking_status" type="hidden" value="<%=internal_tracking_status%>"/>
</form>

<form action="" name="statistics_form" id="statistics_form">
	<input type="hidden" class="p" name="page" value="<%=StringUtil.getInt(request,"p") %>" />
	<input name="st" id="st" type="hidden" value="<%=st%>"/>
	<input name="en" id="en" type="hidden" value="<%=en%>"/>
	<input name="cost" id="cost" type="hidden" value="<%=cost%>"/>
	<input name="cost_type" id="cost_type" type="hidden" value="<%=cost_type%>"/>
	<input name="differences" id="differences" type="hidden" value="<%=differences%>"/>
	<input name="sc_id" id="sc_id" type="hidden" value="<%=sc_id%>"/>
	<input name="ps_id" id="ps_id" type="hidden" value="<%=ps_id%>"/>
	<input name="unfinished" id="unfinished" type="hidden" value="<%=unfinished%>"/>
</form>
<script type="text/javascript">
function go(number){
	 var cmd = '<%= cmd%>';
	 if(cmd === "query" || cmd === "waybill" || cmd === "waybillId")
	 {
		 	 
		 $("#queryForm .p").val(number);
		 $("input[name='p']",$("#queryForm")).val(number);
		 var object = {cmd:$("#cmd").val(),oid:$("#oid").val(),p:number,search_mode:$("search_mode").val()};
		 var str = ( jQuery.param(object));
		 ajaxGetInfoList(str);
	 }
	 else if(cmd === "statistics")
	 {
	 	var object = {
	 					cmd:cmd,
	 					st:$("#st").val(),
	 					en:$("#en").val(),
	 					cost:$("#cost").val(),
	 					cost_type:$("#cost_type").val(),
	 					differences:$("#differences").val(),
	 					sc_id:$("#sc_id").val(),
	 					ps_id:$("#ps_id").val(),
	 					unfinished:$("#unfinished").val(),
	 					p:number
	 				 };
	 	var str = (jQuery.param(object));
		ajaxGetInfoList(str);
	 }
	 else if(cmd==="trace")
	 {
	 	var para = {
	 					cmd:cmd,
	 					trace_type:<%=trace_type%>,
	 					ps_id:<%=ps_id%>,
	 					p:number
	 			   };
		var str = (jQuery.param(para));
		ajaxGetInfoList(str);
	 }
	 else
	 {
		 var object = {
		 	st:$("#st_date").val(),
	 		en:$("#en_date").val(),
		 	p_name:$("#p_name").val(), 
		 	pcid:$("#pcid").val(), 
		 	pro_line_id:$("#pro_line_id").val(), 
		 	order_source:$("#order_source").val(), 
		 	ps_id:$("#ps_id").val(), 
		 	ca_id:$("#ca_id").val(), 
		 	ccid:$("#ccid").val(), 
		 	pro_id:$("#pro_id").val(), 
		 	out_id:$("#out_id").val(),
		 	status:$("#status").val(),
		 	product_status:$("#product_status").val(),
			p:number,
			sc_id:$("#sc_id").val(),
			internal_tracking_status:$("#internal_tracking_status").val()
		 }
		 var str = jQuery.param(object);
		 ajaxGetInfoList(str); 
	 }
}
</script>
<br />
</body>
</html>