<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.api.OrderMgr"%>
<%@page import="com.fedex.ship.stub.WeightUnits"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 

<html>
  <head>
    <title>数据监控统计导出页面</title>

<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
String st = StringUtil.getString(request,"st");
String en = StringUtil.getString(request,"en");
TDate tDate = new TDate();
tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));

String input_st_date,input_en_date;
if ( st.equals("") )
{	
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}
else
{	
	input_st_date = st;
}


if ( en.equals("") )
{	
	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
}
else
{	
	input_en_date = en;
}
%>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
function onLoadInit()
{
	$("#st").date_input();
	$("#en").date_input();
	onLoadInitZebraTable();
}


function closeWin()
{
  $.artDialog.close();
}

function dataExport()
{
	$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">正在导出，请稍后......</div>'});
	var para = "st="+$("#st").val()+"&en="+$("#en").val()+"&ps_id="+$("#ps_id").val();
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/exportWaybillDataCount.action',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request){
		},
		
		error: function(){
			//alert("提交失败，请重试！");
		},
		
		success: function(msg)
		{
			if (msg=="WareHouseErrorException")
			{
				alert("此选项下没有数据，请重新选择！");
				$.unblockUI();
			}
			else
			{
				parent.window.location = "../../"+msg;
				closeWin();
			}
		}
	});

}
</script>
  </head>
  
  
 <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInit();">

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top" ><br /><br /><br />

      <table width="500" border="0" align="center" cellpadding="4" cellspacing="0">
          <tr>
             <td align="right">时间范围:</td>
             <td width="20"></td>
		     <td >
	  			从&nbsp;<input name="st" type="text" id="st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;"/>&nbsp;
	  			至&nbsp;<input name="en" type="text" id="en"  value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;"/>
		     </td>
	     </tr>
	
         <tr>
           <td width="110" align="right">仓库:</td>
            <td></td>
           <td>
		        <select id="ps_id" >
		     			 <option value="0">全部仓库</option>
		     			 <%
		     			 	DBRow[] storageRows = catalogMgr.getProductStorageCatalogTree();
							String qx;
							
							for ( int i=0; i<storageRows.length; i++ )
							{
								if ( storageRows[i].get("parentid",0) != 0 )
								 {
								 	qx = "├ ";
								 }
								 else
								 {
								 	qx = "";
								 }
						%>
				      <option value="<%=storageRows[i].get("id",0l)%>"  > 
										     <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageRows[i].get("level",0))%>
										     <%=qx%>
										     <%=storageRows[i].getString("title")%>          </option>
										<%
										}
										%>
			  </select>
	    </td>
      </tr>
</table>

	
	
    </td></tr>
  <tr>
    <td align="right" class="win-bottom-line">
      <input type="button" name="Submit2" value="导出" class="normal-green-long" onClick="dataExport();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
    </td>
  </tr>
</table> 
</body>
</html>
