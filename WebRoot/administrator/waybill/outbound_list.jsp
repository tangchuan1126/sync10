<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>出库单管理</title>
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script language="javascript" src="../js/print/LodopFuncs.js"></script>
<script language="javascript" src="../js/print/m.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
.zebraTable tr td{line-height:25px;text-align:center;cursor:pointer;}
 
p.wayBill{display:block;width:115px;padding:0px;float:left;border:1px solid silver;margin-top:2px;margin-left:2px;margin-bottom:2px;border-radius: 4px 4px 4px 4px;}
 div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:500px;height:30px;background:white;margin-top:200px;line-height:30px;font-size:14px;}
</style>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<%
	long lable_template_id = StringUtil.getLong(request,"lable_template_id",100013l);
	DBRow lable_template = lableTemplateMgrZJ.getDetailLableTemplateById(lable_template_id);
	
	// 读取当前用户的所在仓库的所有的出库单
	 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	 long adid = adminLoggerBean.getAdid(); 
	 long psId = adminLoggerBean.getPs_id();
	 
	 // page
	 PageCtrl pc = new PageCtrl();
	 pc.setPageNo(StringUtil.getInt(request,"p"));
	 pc.setPageSize(10);
	 DBRow[] rows = null;
	 String cmd = StringUtil.getString(request,"cmd");
	 if(cmd != null && cmd.length() > 0){
		 DBRow queryRow = new DBRow();
		 if(cmd.equals("query")){
	 queryRow.add("cmd","query");
	 queryRow.add("out",StringUtil.getString(request,"out_id"));
	 rows = waybillMgrZR.getOutBoundByRow(queryRow,pc);
	 
		 }else if(cmd.equals("filter")){
	 queryRow.add("cmd","filter");
	 queryRow.add("state",StringUtil.getString(request,"state"));
		 	 queryRow.add("ps_id",StringUtil.getString(request,"ps_id"));
		 	 rows = waybillMgrZR.getOutBoundByRow(queryRow,pc);
		 }
	 }else{
		 rows = waybillMgrZR.getOutBoundByPsId(psId,pc);
	 }
	 
	 // uri 
	String addNewStoreAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/AddNewStoreAction.action";
	String updateStoreAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/waybill/UpdateStoreAction.action";
%>
<script type="text/javascript">
	jQuery(function($){
		initTab();
		initShow();

		$("#waybillId").keydown(function(event){
			if (event.keyCode==13)
			{
				query();
			}
		});
	})
	function initTab(){

		 $("#tabs").tabs({
				cache: true,
				cookie: { expires: 30000 } ,
			});
	}
	function initShow(){
		var cmd = "<%= cmd%>";
		if(cmd && cmd.length > 0){
			if(cmd === "query"){
				$("#waybillId").val('<%= StringUtil.getString(request,"out_id")%>').css("color","black");
			}
			if(cmd === "filter"){
				 var psId = '<%= StringUtil.getString(request,"ps_id")%>';
				 var state = '<%= StringUtil.getString(request,"state")%>'; 
				 $("#cid option[value='"+psId+"']").attr("selected",true);
				 $("#state option[value='"+state+"']").attr("selected",true);
			}
		}
	}
	function go(number){
		$("#pageCount").val(number);
		
		$("#pageForm").submit();
	}
	function inputIn(){
	 	$("#waybillId").val("").css("color","black");
	}
	function outInput(){
		var  st = $("#waybillId").val();
		if($.trim(st).length < 1) {
			$("#waybillId").val("*出库单号查询").css("color","silver");
		}
	}
	function query(){
		var input = $("#waybillId");
		 
		if(input.val() === "*出库单号查询" || $.trim(input.val()).length < 1){
			showMessage("请输入出库单号","alert");
			return ;
		}
		$("#out_id").val(input.val());
		$("#cmd").val("query");
		$("#subForm").submit();
	}
	function addNew(){
		$(".cssDivschedule").css("display","block");
		$.ajax({
			url:'<%= addNewStoreAction%>'+"?ps_id="+ '<%= psId%>',
			dataType:'json',
			success:function(data){
				if(data && data.flag == "success"){
					
					 window.location.reload();	 
				}else{
					$(".cssDivschedule").fadeOut();
					showMessage("添加失败","error");
				}
			}
		
        })
	}
	function valueValidate(_this){
		var node =  $(_this);
		var value = node.val();
		node.val(value.replace(/[^0-9]/g,''));
	}
	function filter(){
		$("#sub_state").val($("#state").val());
		$("#cmd").val("filter");
		$("#ps_id").val($("#cid").val());
		$("#subForm").submit();
	}
	
	function ajaxUpdate(outId, _state,oldState){
		if(oldState === "true" && _state * 1 == 1){
			showMessage("该出库单已经打开","alert");
			return ;
		}
		if(oldState === "false" && _state * 1 == 0){
			showMessage("该出库单已经关闭","alert");
			return ;
		}
		$(".cssDivschedule").css("display","block");
		var str =  jQuery.param({out_id:outId,state:_state});
		$.ajax({
			url:'<%= updateStoreAction%>',
			data:str,
			dataType:'text',	
			success:function(data){
				if(data && data === "success"){
					window.location.reload();	 
				}else{
					$(".cssDivschedule").fadeOut();
					showMessage("系统出错了.","error");
				}
				
			}
		})	
	}
	function showMore(outId){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/waybill/show_waybill_list.html?";
		 var str = jQuery.param({cmd:"comefromout",out_id:outId});
		  
		 $.artDialog.open(uri + str, {title: '更多运单',width:'1100px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function printOut(out_id)
 	{
		visionariPrinter.PRINT_INIT("出库单");
		visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
		visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../waybill/out_list_print.html?out_id="+out_id);
		if(visionariPrinter.PREVIEW()>0)
		{
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/outbound/closeOutbound.action',
					type: 'post',
					dataType: 'html',
					timeout: 30000,
					cache:false,
					data:"out_id="+out_id,
									
				beforeSend:function(request){
				},
									
				error: function(){
					alert("打印出库单失败，请重新打印");
				},
									
				success: function(msg){
					if (msg!="ok")
					{
						alert("打印出库单失败，请重新打印");
					}
				}
			});
		}
 	}
 	
 	function printLocationOut(out_id)
 	{
		visionariPrinter.PRINT_INIT("出库单");
		visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
		visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../waybill/out_list_location_print.html?out_id="+out_id);
		if(visionariPrinter.PREVIEW()>0)
		{
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/outbound/closeOutbound.action',
					type: 'post',
					dataType: 'html',
					timeout: 30000,
					cache:false,
					data:"out_id="+out_id,
									
				beforeSend:function(request){
				},
									
				error: function(){
					alert("打印出库单失败，请重新打印");
				},
									
				success: function(msg){
					if (msg!="ok")
					{
						alert("打印出库单失败，请重新打印");
					}
				}
			});
		}
 	}
 	
 	function printOutSplitRequire(out_id)
 	{
		visionariPrinter.PRINT_INIT("出库单");
		visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
		visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../waybill/out_list_split_require_print.html?out_id="+out_id);
		if(visionariPrinter.PREVIEW()>0)
		{
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/backUpStorage/performSplitRequire.action',
					type: 'post',
					dataType: 'html',
					timeout: 30000,
					cache:false,
					data:"out_id="+out_id,
									
				beforeSend:function(request){
				},
									
				error: function(){
					alert("打印拆货要求失败，请重新打印");
				},
									
				success: function(msg)
				{
					if (msg!="ok")
					{
						alert("打印拆货要求失败，请重新打印");
					}
					else
					{
						$.ajax({
								url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/backUpStorage/getToPcidsForOut.action',
								type: 'post',
								dataType: 'json',
								timeout: 30000,
								cache:false,
								data:"out_id="+out_id,
							beforeSend:function(request){
							},
							error: function(){
							},
							success: function(data)
							{
								for(i=0;i<data.length;i++)
								{
									printSplitGoodLabel(data[i].to_pc_id);
								}
							}
						});
					}
				}
			});
		}
 	}
 	
 	//打印拆散商品标签
 	function printSplitGoodLabel(to_pc_id)
	{
		var context = "";
		var uri = "<%=ConfigBean.getStringValue("systenFolder")+"administrator/"+lable_template.getString("template_path")%>";
	 	var counts = 1;
	  	var para = "print_range_width=<%=lable_template.getString("print_range_width")%>&print_range_height=<%=lable_template.getString("print_range_height")%>&printer=<%=lable_template.getString("printer")%>&paper=<%=lable_template.getString("paper")%>&to_pc_id="+to_pc_id+"&counts="+counts;
	  
	  
		$.ajax({
			url:uri,
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			async:false,
				beforeSend:function(request){
				},
				error: function(){
				},
				success: function(html)
				{
					context = html;
				}
			});					
		var printer_count =  visionariPrinter.GET_PRINTER_COUNT();
		var printer = "<%=lable_template.getString("printer")%>";
		
		var printerExist = "false";
		
		for(var i = 0;i<printer_count;i++)
		{
			if(printer==visionariPrinter.GET_PRINTER_NAME(i))
			{
				printerExist = "true";
				break;
			}
		}
		if(printerExist=="false")
		{
			printer = visionariPrinter.SELECT_PRINTER();
			if(printer !=-1)
			{
				printer=visionariPrinter.GET_PRINTER_NAME(printer);
				var strResult=visionariPrinter.GET_PAGESIZES_LIST(printer,",");
				visionariPrinter.PRINT_INITA(0,0,"<%=Float.parseFloat(lable_template.getString("print_range_width"))%>mm","<%=Float.parseFloat(lable_template.getString("print_range_height"))%>mm",to_pc_id);
				visionariPrinter.SET_PRINTER_INDEXA (printer);//指定打印机打印  
				visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"<%=lable_template.getString("paper")%>");
				visionariPrinter.ADD_PRINT_HTM(0,"0.5mm","100%","100%","<body leftmargin='0' topmargin='0' border='0'>"+context+"</body>");
				visionariPrinter.SET_PRINT_COPIES(1);
				//visionariPrinter.PREVIEW();
				visionariPrinter.PRINT();		
			}
		}
		else
		{
			visionariPrinter.PRINT_INITA(0,0,"<%=Float.parseFloat(lable_template.getString("print_range_width"))%>mm","<%=Float.parseFloat(lable_template.getString("print_range_height"))%>mm",to_pc_id);
			visionariPrinter.SET_PRINTER_INDEXA (printer);//指定打印机打印  
			visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"<%=lable_template.getString("paper")%>");
			visionariPrinter.ADD_PRINT_HTM(0,"0.5mm","100%","100%","<body leftmargin='0' topmargin='0'>"+context+"</body>");
			visionariPrinter.SET_PRINT_COPIES(1);
			//visionariPrinter.PREVIEW();
			visionariPrinter.PRINT();
		}
	}
 	
 	function showOutSplitRequire(out_id)
 	{
 		$.artDialog.open("<%=ConfigBean.getStringValue("systenFolder")%>waybill/out_list_split_require_print.html?out_id="+out_id,{title: '',width:'1000px',height:'400px', lock: true,opacity: 0.3,fixed: true});
 	}
 	
 	
	function print_browse(out_id){
 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/waybill/out_list_print_browse.html?out_id="+out_id; 
		 $.artDialog.open(uri , {title: '打印查看',width:'1000px',height:'400px', lock: true,opacity: 0.3,fixed: true});
 	}
</script>

</head>
<body onload = "onLoadInitZebraTable()">
 <div class="cssDivschedule">
 		<div class="innerDiv">
 			请稍等.......
 		</div>
 </div>
 <form action="" id="subForm">
 	<input type="hidden" name="cmd" id="cmd" value="" />
 	<input type="hidden" name="ps_id" id="ps_id" value=""/>
 	<input type="hidden" name="out_id" id="out_id" />
 	<input type="hidden" name="state" id="sub_state" />
 </form>
 	<div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
		 <ul>
			 <li><a href="#tool">常用工具</a></li>
			 <li><a href="#filter">过滤</a></li>		 
		 </ul>
		 <div id="tool" style="height:40px;overflow:hidden;">
			 <p style="border:0px solid;margin-top:10px;line-height:30px;height:30px;text-align:left; ">
		  		<input type="text" name="waybillId" id="waybillId" style="width:400px;height:20px;line-height:20px;color:silver;" value="*出库单号查询" onfocus="inputIn()" onblur="outInput()" onkeyup="valueValidate(this)"/>
		  		<input type="button" value="查询" class="button_long_search" onclick="query()"/>
		  		<input type="button" value="新增" class="long-button-add" onclick="addNew()"/>
		 	 </p>
		 </div>
		 <div id="filter" style="height:40px;overflow:hidden;">
		 	 <p style="border:0px solid;margin-top:10px;line-height:30px;height:30px;text-align:left; ">
		  		 发货仓库：<select id="cid">
					     			 <option value="0">全部仓库</option>
					     			 <%
					     			 	DBRow[] storageRows = catalogMgr.getProductStorageCatalogTree();
										String qx;
										
										for ( int i=0; i<storageRows.length; i++ )
										{
											if ( storageRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
									%>
				          <option value="<%=storageRows[i].get("id",0l)%>" <%=storageRows[i].get("id",0l)==adminLoggerBean.getPs_id()?"selected=\"selected\"":""%> > 
										     <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageRows[i].get("level",0))%>
										     <%=qx%>
										     <%=storageRows[i].getString("title")%>          </option>
										<%
										}
										%>
					    </select>	 
		  		 状态 : <select id="state" >
				  		 	<option value="-1">全部</option>
				  		 	<option value="0">关闭</option>
				  		 	<option value="1">打开</option>
		  			  </select>
		  			  <input type="button" value="过滤" class="button_long_search" onclick="filter()"/>
		 	 </p>
		 </div>
	</div>
	
	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"   class="zebraTable" style="margin-left:3px;margin-top:5px;">
	  <tr> 
	  	<th width="5%" style="vertical-align: center;text-align: center;" class="left-title"><input type="checkbox" id="allCheck" onclick="checkAll()"/></th>
	  	<th width="5%" style="vertical-align: center;text-align: center;" class="right-title">仓库</th>
	  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">出库单</th>
	  	<th width="35%" style="vertical-align: center;text-align: center;" class="right-title">包含运单</th>
        <th width="15%" style="vertical-align: center;text-align: center;" class="right-title">创建时间</th>
       	<th width="10%" style="vertical-align: center;text-align: center;border-right:0px;" class="right-title">状态</th>
        <th width="15%" style="vertical-align: center;text-align: center;" class="left-title">操作</th>
      </tr>
      <tbody id="tbody">
      <%
      	if(rows != null && rows.length > 0){
      		for(DBRow row : rows){
      	%>
      	<tr>
      		<td><input type="checkbox" id="id_<%= row.getString("out_id") %>" onclick="singleClick(this)" /></td>
      		
      		<td><%= row.getString("title") %></td>
      		<td>[<%=row.getString("out_id") %>] </td>
      		<td>
      		<!-- 这个出库单下面的所有运单 -->
      			<%
      				long outId = row.get("out_id",0l);
      				if(outId !=0l ) {
      					DBRow[] wayBills  = waybillMgrZR.getWayBillListByOutId(outId);	
      					if(wayBills != null && wayBills.length > 0){
      						String[] arrayCreate = {"create_account","print_account","delivery_account","cancel_account","split_account"};
							String[] arrayTime = {"create_date","print_date","delivery_date","cancel_date","split_date"};
      						String[] arrayValue= {"待打印","已打印","已发货","已取消","拆分中"};
							for(DBRow wayBill : wayBills){
      						%>
      							<p class="wayBill" style="">
      								<span style="display:block;float:left;margin-left:4px;">[<span style="color:#f60;"><%= wayBill.getString("waybill_id") %></span>] </span>
      									<%	int index = wayBill.get("status",0);%>
      								<span style="display:block;float:right;margin-right:4px;"><%=arrayValue[index] %></span>
      							</p>	
      						<% 
      						}
							%>
								<p style="text-align:right;"><a href="javascript:showMore('<%= outId%>')">更多</a></p>
							<% 
      					}
      					
      				}
      				
      			%>
      		</td>
      		<td><%= row.getString("create_time").substring(0,19) %></td>
      		<td><%=row.getString("state").equals("true")?"开放中":"关闭中"%></td>
      		<td>
      			<input type="button" value="打开" class="short-short-button-ok" style="cursor:pointer;" onclick="ajaxUpdate('<%= row.get("out_id",0l)%>','1','<%=row.getString("state") %>')"/>
      			<input type="button" value="关闭" class="short-short-button-del" style="cursor:pointer;" onclick="ajaxUpdate('<%= row.get("out_id",0l)%>','0','<%=row.getString("state") %>')"/>
      			<input type="button" value="打印" class="short-short-button-mod" style="cursor:pointer;" onclick="printOut('<%= row.get("out_id",0l)%>')"/>
      			<input type="button" value="查看" class="short-short-button-mod" style="cursor:pointer;" onclick="print_browse('<%= row.get("out_id",0l)%>')"/>
      			<input type="button" value="打印位置" class="long-button-mod" style="cursor:pointer;" onclick="printLocationOut('<%= row.get("out_id",0l)%>')"/>
      			<%
      				if(backUpStorageMgrZJ.existsSplitRequireDetailForOut(row.get("out_id",0l)))
      				{
      			%>
      				<input type="button" value="拆货需求" class="long-button-mod" style="cursor:pointer;" onclick="printOutSplitRequire(<%= row.get("out_id",0l)%>)"/>	
      			<%
      				}
      			%>
      		</td>
      	</tr>
      	<% 		
      		}
      		
      	}else{
      		%>
      		<tr style="background:#E6F3C5;">
   				<td colspan="7" style="height:120px;">无数据</td>
   			</tr>
      		<% 
      		 
      	}
      %>
      	 
      </tbody>
      
	</table>

<form action="" id="pageForm">
	<input type="hidden" name="p" id="pageCount"/>
	<input type="hidden" name="cmd" id="pageCmd" value="<%=StringUtil.getString(request,"cmd") %>" />
	<input type="hidden" name="out_id" value="<%=StringUtil.getString(request,"out_id") %>"/>
	<input type="hidden" name="ps_id" value="<%=StringUtil.getString(request,"ps_id") %>"/>
	<input type="hidden" name="state" value="<%=StringUtil.getString(request,"state") %>"/>
</form>


 <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" style="margin-left:3px;">
  
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
    </td>
  </tr>
</table>
<script type="text/javascript">
//页面全选 操作等
function checkAll(){
	var check = $("#allCheck");
	if(check.attr("checked")){
		// 查找所有的input然后把
		$("input[type='checkbox']",$("#tbody")).each(function(){
			var _this = $(this);
			_this.attr("checked",true);
			_this.parent().parent().addClass("over");
		})
	}else{
			
		$("input[type='checkbox']",$("#tbody")).attr("checked",false);
		$("tr",$("#tbody")).removeClass("over");
	}
}
function singleClick(_this){
	 var node = $(_this);
	 node.parent().parent().removeClass("over");
	 $("#allCheck").attr("checked",checkIsAllChecked());
}
function checkIsAllChecked(){
	 var flag = true;
	 var inputs = $("input[type='checkbox']",$("#tbody"));
	 for(var index = 0 ; index < inputs.length ; index++ ){
		 if(!$(inputs[index]).attr("checked")){
			 flag = false;
			 break;
		  }
	 }
	 return flag;
}
function onLoadInitZebraTable(){
	$(".zebraTable tr").mouseover(function() {$(this).addClass("over");}).mouseout(function(){
	 	var _this = $(this);
	 	var check = $("input[type='checkbox']",_this);
	 	if(check &&  check.length > 0 && check.attr("checked")){
		 }else{
	 		$(this).removeClass("over");
		 }
	 });
	$(".zebraTable tr:even").addClass("alt");
}
// 获取tbody下所有打钩的选项的Ids
function getSelectedIds(){
	var ids= ""; 
	 var inputs = $("input[type='checkbox']:checked",$("#tbody"));
	 if(inputs.length > 0){
		 for(var index = 0 ; index < inputs.length; index++ ){
			 ids += ","+$(inputs[index]).attr("id").replace("id_","");
		}
	 } 
	 return ids == ""?"":ids.substr(1);
}
//stateBox 信息提示框
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
	 var  _self = $("body"),
	_stateBox = $("<div style='font-size:14px;'/>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
 }
}
</script>
</body>
</html>