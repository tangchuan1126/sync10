<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@ include file="../../include.jsp"%> 
<html>
<% 
	//分页
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(20);

    long out_id=StringUtil.getLong(request,"out_id");  //出库单id
    long ps_id=StringUtil.getLong(request,"ps_id");   //仓库id
    long out_for_type=StringUtil.getLong(request,"out_for_type");
	DBRow[] outRow=waybillMgrZwb.selectAllOutOrder(out_id,ps_id,out_for_type,pc);
    
%>
<head>
<script>
function openOutOrder(out_id,out_for_type,create_time){
	var ur=url+'administrator/order/print/dialog_transport_list.html?out_id='+out_id+'&out_for_type='+out_for_type+'&create_time='+create_time;
	dia=$.artDialog.open(ur, {id:'out_dialog',title: '出库单',width:'900px',height:'500px', lock: true});
}

function go(number){
	find_out_transport_list(<%=out_id%>,<%=ps_id%>,<%=out_for_type%>,number);
}
</script>   
</head>
<body>
	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"   class="zebraTable" style="margin-left:3px;margin-top:5px;">
		<tr> 
		  	<th width="5%"  style="vertical-align: center;text-align: center;" class="right-title">拣货单ID</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">仓库</th>
		  	<th width="13%" style="vertical-align: center;text-align: center;" class="right-title">详细</th>
	        <th width="15%" style="vertical-align: center;text-align: center;" class="right-title">创建时间</th>
	       	<th width="10%" style="vertical-align: center;text-align: center;border-right:0px;" class="right-title">包含单据类型</th>
	     </tr>
     	 <tbody id="tbody">
     	 <%for(int i=0;i<outRow.length;i++){ %>
	     	<tr>
	     	 	<td align="center" height="20px;"><%=outRow[i].get("out_id",0l) %></td>
	     	 	<td align="center"><%= catalogMgr.getDetailProductStorageCatalogById(outRow[i].get("ps_id",0l)).getString("title")%></td>
	     	 	<td align="center">
	     	 		<a style="color:blue;text-decoration:none" href="javascript:void(0)" onclick="openOutOrder(<%=outRow[i].get("out_id",0l) %>,<%=outRow[i].get("out_for_type",0l)%>,'<%=outRow[i].getString("create_time")%>')">查看包含转运单</a>
	     	 	</td>
	     	 	<td align="center"><%=outRow[i].getString("create_time").substring(5,16)%></td>
	     	 	<td align="center">
					<span style="color:red">转运单</span>
	     	 	</td>
	     	</tr>
	     <%} %>
      	 </tbody>
    </table>
    <br/>
    <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <tr>
	    <td height="28" align="right" valign="middle">
	        <%
			int pre = pc.getPageNo() - 1;
			int next = pc.getPageNo() + 1;
			out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
			out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
			out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
			out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
			out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
	     	 跳转到
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
	        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
	    </td>
	  </tr>
	</table>  
</body>
</html>
