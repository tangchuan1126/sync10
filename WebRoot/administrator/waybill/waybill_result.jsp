<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.WayBillReturnKey"%>
<%@page import="com.cwc.app.key.WayBillResultKey"%>
<%@ include file="../../include.jsp"%> 
<%
	long waybill_id = StringUtil.getLong(request,"waybill_id");
	
	DBRow waybill = wayBillMgrZJ.getDetailInfoWayBillById(waybill_id);
	
	int result = waybill.get("result",0);
	String result_note = waybill.getString("result_note");
%>
<html>
  <head>
    <title>运单被退货</title>
    <link href="../comm.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
	<script type="text/javascript">
		function wayBillReturnSub()
		{
			var result = $("[name='result']:checked").val();
			var note = $("#result_note").val()
			
			if(typeof(result)==="undefind")
			{
				alert("请选择退货原因");
			}
			else if(note==="")
			{
				alert("请填写退货备注");
			}
			else
			{
				document.waybill_result.submit();
			}
		}
	</script>
  </head>
  
  <body  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  	<form name="waybill_result" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/wayBillResult.action">
  		<input type="hidden" name="waybill_id" value="<%=waybill_id%>"/>
  		<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  			<tr>
  				<td valign="top">
  					<table width="100%">
				    	<tr>
				    		<td>处理结果:</td>
				    		<td>
				    			<input type="radio" name="result" value="<%=WayBillResultKey.HEAVYHAIR%>" <%=result==WayBillResultKey.HEAVYHAIR?"checked=\"checked\"":""%>/>重新发货
				    			<input type="radio" name="result" value="<%=WayBillResultKey.REFUND%>" <%=result==WayBillResultKey.REFUND?"checked=\"checked\"":""%>/>客户退款
				    		</td>
				    	</tr>
				    	<tr>
				    		<td>备注:</td>
				    		<td>
				    			<textarea id="result_note" name="result_note" rows="5" cols="50"><%=result_note.trim()%></textarea>
				    		</td>
				    	</tr>
				    </table>
  				</td>
  			</tr>
  			<tr>
				<td align="right" valign="bottom">				
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
							  <input type="button" name="Submit2" value="确定" class="normal-green" onclick="wayBillReturnSub()">
							  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close()">
							</td>
						</tr>
					</table>
				</td>
			</tr>
  		</table>
  	</form>
  </body>
</html>
