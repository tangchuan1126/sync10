<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WaybillTrackeTypeKey"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean,com.cwc.app.key.AfterServiceKey,com.cwc.app.api.OrderMgr"%>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add Waybill Note</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet"/>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css"/>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>
 
<style type="text/css">
*{margin:0px;padding:0px;}
div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .title{ background: url("../js/popmenu/pro_title.jpg") no-repeat scroll left center transparent;color: #FFFFFF;display: table;font-size: 14px; height: 27px;padding-left: 5px; padding-top: 3px;width: 356px;line-height:27px;}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:80px;text-align:center;border:1px solid silver;margin:0px auto;width:300px;height:30px;background:white;line-height:30px;font-size:14px;}	
</style>
<%
	long waybill_id = StringUtil.getLong(request,"waybill_id");
	DBRow waybill = wayBillMgrZJ.getDetailInfoWayBillById(waybill_id);
	WaybillTrackeTypeKey waybillTrackeTypeKey = new WaybillTrackeTypeKey();
%>
<script type="text/javascript">
	jQuery(function($){
		init(<%=waybill.get("product_status",0)%>,<%=waybill.get("status",0)%>)
	})	
	function init(product_status,status)
	{
		var trace_type_radio = "";
		//跟据订单状态，提供不同跟踪类型
		if (product_status==<%=ProductStatusKey.STORE_OUT%>)
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=WaybillTrackeTypeKey.StockoutTrack%>'/><%=waybillTrackeTypeKey.getWaybillTrackeType(String.valueOf(WaybillTrackeTypeKey.StockoutTrack))%> &nbsp;";
		}
		else if(product_status==<%=ProductStatusKey.IN_STORE%>)
		{
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=WaybillTrackeTypeKey.OverdueSendTrack%>'/><%=waybillTrackeTypeKey.getWaybillTrackeType(String.valueOf(WaybillTrackeTypeKey.OverdueSendTrack))%> &nbsp;";
			
			trace_type_radio += "<input type='radio' name='trace_type' id='trace_type' value='<%=WaybillTrackeTypeKey.PackingTrack%>'/><%=waybillTrackeTypeKey.getWaybillTrackeType(String.valueOf(WaybillTrackeTypeKey.PackingTrack))%> &nbsp;";
		}
		
		$("#radioNoteType").html(trace_type_radio);
	
	}
	 function cancel(){$.artDialog && $.artDialog.close();}
	 function submitForm()
	 {
		var submitForm = $("#submitForm");
		$(".cssDivschedule").css("display","block");
		 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/addWaybillNote.action',
			data:submitForm.serialize(),
			dataType:'json',
			success:function(data)
			{
				if(data.result=="true")
				{
					$(".cssDivschedule").css("display","none");
					$.artDialog.opener && $.artDialog.opener.refreshWindow();
					cancel();
				}
			},
		 
			error:function(data)
			{
				$.artDialog.opener && $.artDialog.opener.refreshWindow();
				cancel();
			}
		})
	}
		function cancel(){
			$.artDialog && $.artDialog.close();
		}
		
		function changeETA()
		{
			var note = $("#orderNoteWinText").val();
			var eta = "预计"+$("#eta").val()+"到货";
			var newNote  = eta+note;
			$("#orderNoteWinText").val(newNote);
		}
		
		function stcalendarCallback(date, month, year)
		{
			day = date;
			date = year+"-"+month+"-"+day;
			$("#eta").val(date);
			
			var note = $("#orderNoteWinText").val();
			var eta = "预计"+$("#eta").val()+"到货";
			var newNote  = eta+note;
			$("#orderNoteWinText").val(newNote);
		}
 </script>
</head>
<body   >
<!-- 遮盖层 -->
  <div class="cssDivschedule">
 		<div class="innerDiv">
 			请稍等.......
 		</div>
 </div>
 	<!-- 上面显示已经添加的Order的Note -->
 	<!-- 下面显示添加Order Note的页面 -->
 	<div class="title" style="font-weight: bold;margin-bottom:5px;">运单跟进[单号:<%=waybill_id%>]</div>
 	<form id="submitForm">
 		<input type="hidden" name="waybill_id" value="<%=waybill_id%>"/>
 		<%
 			if(waybill.get("product_status",0)==ProductStatusKey.STORE_OUT)
 			{
 		%>
 				ETA:<input name="eta" type="text" id="eta"  value=""  style="border:1px #CCCCCC solid;width:85px;"/>
 				<script language="JavaScript" type="text/javascript" >
			    	stfooCalendar = new dynCalendar('stfooCalendar', 'stcalendarCallback', '../js/images/');
			    </script>	
 		<%
 			}
 		%>
	 	<p id="radioNoteType"></p>
<textarea name='note' id='orderNoteWinText'  style='width:405px;height:147px;margin-top:10px;'>
</textarea>
	  
	 	 <div class="buttonDiv" style="margin-top:5px;"> 
	 	 	<input type="button" id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil"  onclick="submitForm()" value="提交"> 
			<button id="jqi_state0_button取消" value="n" class="jqidefaultbutton specl" name="jqi_state0_button取消" onclick="cancel();">取消</button>
	 	 </div>
 	</form>
 	<script type="text/javascript">
 	
 	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 		
 	</script>
</body>
</html>
