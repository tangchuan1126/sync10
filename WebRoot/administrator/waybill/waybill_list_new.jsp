<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.key.WaybillInternalOperKey"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.key.WaybillTrackeTypeKey"%>
<%@page import="com.cwc.app.key.SystemTrackingKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 

<html>
<head>

<%
     //分页
     PageCtrl pc = new PageCtrl();
	 pc.setPageNo(StringUtil.getInt(request,"p"));
	 pc.setPageSize(10);

	long ps_id=StringUtil.getLong(request,"ps_id");			  //仓库id
	long sc_id=StringUtil.getLong(request,"sc_id");     	  //快递id
	long line_id=StringUtil.getLong(request,"line"); 	      //产品线id
	long all_weight=StringUtil.getLong(request,"allWeight");  //重量
	long pkcount=StringUtil.getLong(request,"pkcount");		  //包含数量
	
	
	WayBillOrderStatusKey wos = new WayBillOrderStatusKey();
	ProductStatusKey productStatus = new ProductStatusKey();
	WaybillInternalTrackingKey  waybillInternalTrackingKey = new WaybillInternalTrackingKey();
	
	
	DBRow[] waybill=null;
	if(line_id==0 && pkcount!=4){
		waybill=waybillMgrZwb.seachWaybillDetailedNoLine(sc_id,ps_id,all_weight,pkcount,pc);
	}else if(line_id==0 && pkcount==4){
		waybill=waybillMgrZwb.seachWaybillDetailedNoLineFour(sc_id,ps_id,all_weight,pkcount,pc);
	}else if(line_id!=0 && pkcount!=4){
		waybill=waybillMgrZwb.seachWaybillDetailed(line_id,sc_id,ps_id,all_weight,pkcount,pc);
	}else if(line_id!=0 && pkcount==4){
		waybill=waybillMgrZwb.seachWaybillDetailedFour(line_id,sc_id,ps_id,all_weight,pkcount,pc);
	}
	
	String ids="";
%>
<style type="text/css" media="all">
	@import "../js/thickbox/global.css";
	@import "../js/thickbox/thickbox.css";
</style>
<style type="text/css">
	div.dingdan1{border: 0px solid #DDDDDD;border-radius: 7px 7px 7px 7px;padding: 5px 5px 5px 15px;width:1050px;margin-bottom:5px;}
	table.fixTable td{height:30px;}
	span.create{display:block;float:left;border:1px solid red;cursor:pointer;}
	p{display:block;clear:both;}
	span.valueName{width:40%;border:0px solid red;display:block;float:left;text-align:right;}
	span.valueSpan{width:56%;border:0px solid green;float:left;overflow:inherit;text-align:left;text-indent:4px;}
	span.costName{width:45%;float:left;text-align:right;}
	span.costValue{width:50%;display:block;float:left;text-indent:4px;overflow:inherit;text-align:left;font-weight:bold;}
	span.weightName{width:60%;display:block;float:left;text-align:right;border:0px solid red;}
	span.weightValue{width:30%;display:block;float:left; text-indent:4px;font-weight:bold;}
	span.infoName{width:45%;float:left;text-align:right;}
	span.infoValue{width:50%;display:block;float:left;text-indent:4px;overflow:inherit;text-align:left;font-weight:bold;}
	span.stateName{width:15%;float:left;text-align:right;font-weight:normal;}
	span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:inherit;text-align:left;font-weight:normal;}
	span.topInfo{display:block;position:relative;top:-20px;width:50px;height:20px;line-height:20px;left:10px;border:1px solid red;}
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.fix{text-align:left;width:95%;}
	.title{font-size:12px;color:green;font-weight:blod;}
	.track_title{font-size:12px;font-weight:blod;}
	tr.split td input{margin-top:2px;}
</style>
<script>
//分页页码
function go(number){
   detConditionSeach(number);
}

</script>
</head>
<body>
	<table width="99%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" isNeed="true" isBottom="true" style="margin-top:5px;" >
		<thead>
			<tr>
				<th width="16%" class="right-title"  style="text-align: center;">编号</th>
			    <th width="16%" class="right-title"  style="text-align: center;">运单明细</th>
			    <th width="16%" class="right-title"  style="text-align: center;">运单号</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">费用</th>
			    <th width="8%"  class="right-title"  style="text-align: center;">重量</th>
			    <th width="18%" class="right-title"  style="text-align: center;">地址</th>
			</tr>
		</thead>
	    <tbody id="tbody">
	     
	     <% if(null != waybill && waybill.length > 0){  %>
		 <%   for(int index = 0, count = waybill.length ; index < count ; index++ ){ %>
	    	<tr>
	    		<td>
			  		<fieldset class="set" style="border:2px solid #993300;">
			  			 <legend>
			  				 <%
	  				 			String showTime = waybill[index].getString("create_date");
	  				 			showTime =   showTime.substring(5,16) ;
			  				 %>
							<span style="" class="title"><%= waybill[index].get("waybill_id",0l)%></span>
						 </legend>
			  				 <%
			  				 	DBRow[] orderRows = wayBillMgrZJ.returnOrdersByWayBillId(waybill[index].get("waybill_id",0l));
			  				 	if( null != orderRows && orderRows.length > 0){
			  				 		 for(DBRow row : orderRows){
			  				 			 long oid = row.get("oid",0l);
			  				 			 if(oid != 0l){
			  				 				DBRow dateRow = orderMgr.getDetailPOrderByOid(oid);
			  				 				long postDate = 0;
			  				 				long hour = 0;
			  				 				long minute = 0;
			  				 				long second =0;
			  				 				if(dateRow != null){
			  				 					postDate = dateRow.get("outbound_cost",0l)/1000;
			  				 					hour = postDate/3600;
	   											minute = postDate%3600/60;
	   											second = postDate%60/60;
			  				 				}
			  				 				%>
			  				 				<p style="text-align:left;text-indent:5px;font-weight:normal;">
			  				 					<!-- ebay 订单是可以发送Ebay Message的 -->
			  				 				<% boolean isEbayOrder = row.getString("order_source").equals("EBAY"); %>
			  				 				<span style="color:blue;" order_id = '<%= row.get("oid",0l)%>' class='<%=(isEbayOrder?"can_send_message":"") %>'><%=row.getString("order_source") %></span>: 
			  				 					<span class="alert-text"><%=row.get("oid",0l) %></span> &nbsp;
			  				 					<%=hour+"时"+minute+"分"+second+"秒" %>
			  				 					<!-- <span class="countTime" end='<%=waybill[index].getString("create_date") %>' start='<%=postDate %>'></span> -->
			  				 				</p>
			  				 				<% 
			  				 		 }
			  				 	   }
			  				 	}
			  				 %> 
			  				 <%if(waybill[index].get("replace_waybill_id",0l)>0){%>
			  				 	<p style="text-align:left;text-indent:5px;font-weight:normal;">
			  				 		替换:<a href="javascript:searchRepalceWaybill('<%=waybill[index].get("replace_waybill_id",0l)%>')"><%=waybill[index].get("replace_waybill_id",0l)%></a>
			  					</p>
			  				 <%}%> 
			  			</fieldset>
	  					<%
  							//在这里判断当前的状态是 什么然后选择那些count he state 
  							DBRow userInfo = null ;
  							String userName = "";
  							String[] arrayCount = {"create_account","print_account","delivery_account","cancel_account","split_account"};
  							String[] arrayTime = {"create_date","print_date","delivery_date","cancel_date","split_date"};
  							int w = waybill[index].get("status",0);
  							long countId = waybill[index].get(arrayCount[w],0l);
  							String dateTime = waybill[index].getString(arrayTime[w]);
  							userInfo = waybillMgrZR.getUserNameById(countId);
  							if(userInfo != null ){
  								  userName =  userInfo.getString("employe_name");
  							}
  							dateTime = dateTime.length() >= 19 ? dateTime.substring(0,19):dateTime;
  							String wayBillState = wos.getStatusById(w);
  							String tracking_status = waybillInternalTrackingKey.getWaybillInternalTrack(waybill[index].getString("internal_tracking_status"));
  							DBRow shipping_company = expressMgr.getDetailCompany(waybill[index].get("sc_id",0l));
  							String company_name = shipping_company.getString("name");
	  					%>
			  			<fieldset style="" class="set" style="padding-bottom:4px;">
			  				 <legend>
									<span class="title">
										<%=wayBillState%>
									</span>
									<%
										if(w==WayBillOrderStatusKey.SHIPPED&&!tracking_status.equals("")){
											String trackingUrl = trackingUrl = shipping_company.getString("tracking_url").replace("[trackingNumber]",waybill[index].getString("tracking_number"));
									%>
										|<span class="track_title">
											<a href="<%=trackingUrl%>" target="_blank"><%=tracking_status%></a>
											<a href="javascript:showWaybillLog(<%=waybill[index].get("waybill_id",0)%>)"><img src="../imgs/modify.gif" width="12" height="12" border="0" alt="运单日志"/></a>
										</span>
									<%}%>
							</legend>
			  				<p>
			  					<span class="alert-text stateName"><img title="操作人" src="../imgs/order_client.gif" /></span>
			  					<span class="stateValue"><%=userName %></span>
			  				</p>
			  				<p>
			  					<span class="alert-text stateName"><img  title="操作时间" src="../imgs/alarm-clock--arrow.png" /></span>
			  					<span class="stateValue"><%= dateTime%></span>
			  				</p>
		  			</fieldset>
	  	 		</td>
	    		<td style="padding:0px;margin:0px;">
	  	 			<!-- 运单明细 -->
	  	 			<fieldset style="" class="set fix">
			  			 <legend><span style="" class="title"><%= catalogMgr.getDetailProductStorageCatalogById(waybill[index].get("ps_id",0l)).getString("title")%> 仓库</span></legend>		 
	  	 			 <%
	  	 				DBRow[] wayBillItems = wayBillMgrZJ.getWayBillOrderItems(waybill[index].get("waybill_id",0l));
	  	 				if(null != wayBillItems && wayBillItems.length > 0){
	  	 					for(DBRow row : wayBillItems){
	  	 			 %>
	  	 				<p>
	  	 					<span class="alert-text" style="font-weight:normal;"><%= row.getString("p_name") %></span> &nbsp;x&nbsp;<span style="color:blue;" class="quantity"><%= row.getString("quantity") %></span>
	  	 					<% if(row.get("product_status",0)==ProductStatusKey.STORE_OUT){ %>
	  	 						<a href="javascript:puchaseDeliveryCount(<%=row.get("pc_id",0l)%>,<%=waybill[index].get("ps_id",0l)%>,'deliveryCount')">[<%=productStatus.getOrderHandleById(row.getString("product_status"))%>]</a>
	  	 					<%}%>
	  	 				</p>
	  	 			 <% 
	  	 					}
	  	 				}
	  	 			 %>
	  	 			 </fieldset>
	  	 		 	 <p style="text-align:left;text-indent:5px;margin-top:3px;" class="alert-text countTotal">总件数:&nbsp;<span class="thisValue"></span></p>
	  	 		</td>
	    		<td nowrap="nowrap">
	  				<!--  运单号 和状态 -->
	  				<p>
	  					<span class="infoName" style="width:100%;text-align:left;">
	  						<% out.print(company_name);%>
	  					</span>
	  				</p>
	  				<p>
	  					<span class="infoValue" style="width:100%;color:green;text-align:left;"><%= waybill[index].getString("tracking_number")%></span>
	  				</p>
	  				<p>
	  					<span class="infoName">出库单:</span>
	  					<span class="infoValue" style="color:green;">
	  						<%= waybill[index].getString("out_id").length() < 1 ? "无":waybill[index].getString("out_id")%>
	  						&nbsp;
	  						<%if(waybill[index].getString("out_id").length() > 0){%>
	  						<!--<img onclick="printChuku()" style="cursor:pointer;width:12px;height:12px;" src="../imgs/order_printer.gif" /> -->
	  						<%}%>
	  					</span>
	  				</p> 
	  				<p>
	  					<span class="infoName">创建时间:</span>
	  					<span class="infoValue"><%=showTime%></span>
	  				</p>
	  				<%
	  					long lacking_cost = waybill[index].get("lacking_cost",0l)/1000;
	  					long lacking_cost_hour = lacking_cost/3600;
	   					long lacking_cost_minute = lacking_cost%3600/60;
	   					long lacking_cost_second = lacking_cost%60/60;
	   					if(lacking_cost>0){
	   				%>
	   				<p>
	   					<span class="infoName">曾缺货:</span>
	   					<%=lacking_cost_hour+"时"+lacking_cost_minute+"分"+lacking_cost_second+"秒"%>
	   				</p>
	   				<%}%>
	  				<p>
	  					<span class="infoName">打印用时:</span>
	  					<%
	  						long print_cost = waybill[index].get("print_cost",0l)/1000;
	  						long print_cost_hour = print_cost/3600;
	   						long print_cost_minute = print_cost%3600/60;
	   						long print_cost_second = print_cost%60/60;   						
	  						if(print_cost>0){
	  							out.print(print_cost_hour+"时"+print_cost_minute+"分"+print_cost_second+"秒");
	  						}
	  					%>
	  				</p>
	  				<p>
	  					<span class="infoName">配货用时:</span>
	  					<%
	  						long delivery_cost = waybill[index].get("delivery_cost",0l)/1000;
	  						long delivery_cost_hour = delivery_cost/3600;
	   						long delivery_cost_minute = delivery_cost%3600/60;
	   						long delivery_cost_second = delivery_cost%60/60;   						
	  						if(delivery_cost>0){
	  							out.print(delivery_cost_hour+"时"+delivery_cost_minute+"分"+delivery_cost_second+"秒");
	  						}
	  					%>
	  				</p>
	  			</td>
	    		<td nowrap="nowrap">
	  				<!-- 费用 -->
	  				<p>
	  					<span class="alert-text costName">估算运费:</span>
	  					<span class="costValue">&nbsp;&nbsp;<%= waybill[index].getString("shipping_cost")%></span>
	  				</p>
	  				<p>
	  					<span class="alert-text costName">通讯运费:</span>
	  					<span class="costValue">&nbsp;&nbsp;<%= waybill[index].getString("shippment_rate")%></span>
	  				</p>
	  				<p>
	  					<span class="alert-text costName">成本:</span>
	  					<span class="costValue">&nbsp;&nbsp;<%= waybill[index].getString("waybill_prodcut_cost")%></span>
	  				</p>
	  				<p>
	  					<span class="alert-text costName">销售额:</span>
	  					<span class="costValue">&nbsp;&nbsp;<%= waybillMgrZR.getSaleRoomByWalBillId(waybill[index].get("waybill_id",0l)).get("sum_sale",0.0f)%></span>
	  				</p>
	  			</td>
	    		<td nowrap="nowrap"> 
	  				<!-- 重量 -->
	  				<p>
	  					<span class="weightName">运单重量:</span>
	  					<span class="weightValue"><%= waybill[index].getString("all_weight")%>&nbsp;kg</span>
	  				</p>
	  				<p>
	  					<span class="weightName">打印重量:</span>
	  					<span class="weightValue"><%= waybill[index].get("print_weight",0.0f)%>&nbsp;kg</span>
	  				</p>
	  				<p>
	  					<span class="weightName">发货重量:</span>
	  					<span class="weightValue"><%= waybill[index].get("delivery_weight",0.0f)%>&nbsp;kg</span>
	  				</p>
  					<p>
	  					<span class="weightName">运输重量:</span>
	  					<span class="weightValue">
	  						<%if(waybill[index].get("tracking_weight",0.0f)>0){%>
	  							<%=waybill[index].get("tracking_weight",0.0f)%>&nbsp;kg
	  						<%}%>
	  					</span>
	  				</p>
	  			</td>
	    		<td style="padding-top:5px;padding-bottom:5px;">
	  				<!--  地址信息  -->
	  			     <p>	
	  					<span class="alert-text valueName">Name:</span>
	  					<span class="valueSpan"><%= waybill[index].getString("address_name")%></span>
					 </p>
					  <p>	
	  					<span class="alert-text valueName">Street:</span>
	  					<span class="valueSpan"><%= waybill[index].getString("address_street")%></span>
					 </p>
					  <p>	
	  					<span class="alert-text valueName">City:</span>
	  					<span class="valueSpan"><%= waybill[index].getString("address_city")%></span>
					 </p>
					  <p>	
	  					<span class="alert-text valueName">State:</span>
	  					<span class="valueSpan"><%= waybill[index].getString("address_state")%></span>
					 </p>
					  <p>	
	  					<span class="alert-text valueName">Zip:</span>
	  					<span class="valueSpan"><%= waybill[index].getString("address_zip")%></span>
					 </p>
	  				<p>	
	  					<span class="alert-text valueName">Tel:</span>
	  					<span class="valueSpan"><%= waybill[index].getString("tel")%></span>
					 </p>
					 <p>	
	  					<span class="alert-text valueName">Country:</span>
	  					<span class="valueSpan"><%= waybill[index].getString("address_country")%></span>
					 </p>
					 <%if(!waybill[index].getString("residential_status").equals("")){%>
					 	<p align="center">
					 		<span style="font-style: italic;"><%=waybill[index].getString("residential_status")%></span>
					 	</p>
					 <%}%>
	  			</td>
	    	</tr>
	       <%} %> 
	     <%}else{ %>
	     	<tr style="background:#E6F3C5;">
   				<td colspan="13" style="height:120px;">无数据</td>
   			</tr>
	     <%} %>
	    </tbody>
	</table> 
	<br />
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <tr>
	    <td height="28" align="right" valign="middle">
	        <%
			int pre = pc.getPageNo() - 1;
			int next = pc.getPageNo() + 1;
			out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
			out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
			out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
			out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
			out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
	     	 跳转到
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
	        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
	    </td>
	  </tr>
	</table> 
	<input type="hidden" value="<%= pc.getAllCount() %>" id="sumCount" />
</body>
</html>
<script>
//countTotal 计算商品总共件数
$(".countTotal").each(function(){
	var _this = $(this);
	var node = _this.parent().find(".quantity");
	var sum = 0;
	for(var index = 0 , count = node.length ; index < count ; index++ ){
		sum += $(node.get(index)).html() * 1;
	}
	 _this.find(".thisValue").html(sum);
})
	 // 计算打印用时,配货用时
 $(".countTime").each(function(){
	var _this = $(this);
	if(_this.attr("end").length > 0 && _this.attr("start").length > 0){
		 var dateEnd = covertStrToDate(_this.attr("end").substr(0,19));
		 var dateStart = covertStrToDate(_this.attr("start").substr(0,19));
		 var v = "" ;
		if(dateEnd && dateStart){
			 v = count(dateEnd,dateStart);
		}
		_this.html(v);
	} 
 })
 // 计算时间 4:34 
 function count(dateEnd,dateStart){
		var end = dateEnd.getTime();
		var start = dateStart.getTime();
		var value = end - start;
 		if(value > 0){
	 		var str = "";
	 		var hours = value / (1000 * 60 * 60) + "";
	 		if(hours.indexOf(".") != -1){
		 		var array = hours.split(".");
				var xiaoshu = "0."+ array[1].substr(1);
				var  min =fixMin( parseInt(xiaoshu * 60));
				str  =array[0] +("小时" + (min * 1 < 1?"":min + "分" ) );
	 		}else{
		 		str = hours + "小时";
		 	}
	 		return str;
	 	}
 }
 function fixMin(value){
	 return value < 10 ?("0"+value):value;
 }
 function covertStrToDate(str){
	 var array = str.split(/\s+/);
	 var date  ;
	 if(array.length == 2){
		 var dateArray = array[0].split("-");
		 var timeArray = array[1].split(":");
		 if(dateArray.length == 3 && timeArray.length == 3){
			 date = new Date();
			 date.setFullYear(dateArray[0],dateArray[1] - 1,dateArray[2]);
			 date.setHours(timeArray[0],timeArray[1],timeArray[2]);
			 return date;
		 }
	 }
 }
</script>