<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.WayBillReturnKey"%>
<%@ include file="../../include.jsp"%> 
<%
	long waybill_id = StringUtil.getLong(request,"waybill_id");
	
	DBRow waybill = wayBillMgrZJ.getDetailInfoWayBillById(waybill_id);
	
	int return_type = waybill.get("return_type",0);
	String return_note = waybill.getString("return_note");
%>
<html>
  <head>
    <title>运单被退货</title>
    <link href="../comm.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
	<script type="text/javascript">
		function wayBillReturnSub()
		{
			var return_type = $("[name='return_type']:checked").val();
			var note = $("#return_note").val()
			
			if(typeof(return_type)==="undefind")
			{
				alert("请选择退货原因");
			}
			else if(note==="")
			{
				alert("请填写退货备注");
			}
			else
			{
				document.waybill_return.submit();
			}
		}
	</script>
  </head>
  
  <body  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  	<form name="waybill_return" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybill/wayBillReturn.action">
  		<input type="hidden" name="waybill_id" value="<%=waybill_id%>"/>
  		<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  			<tr>
  				<td valign="top">
  					<table width="100%">
				    	<tr>
				    		<td>原因:</td>
				    		<td>
				    			<input type="radio" name="return_type" value="<%=WayBillReturnKey.ADDRESSERROR%>" <%=return_type==WayBillReturnKey.ADDRESSERROR?"checked=\"checked\"":""%>/>地址错误
				    			<input type="radio" name="return_type" value="<%=WayBillReturnKey.WEIGHTERROR%>" <%=return_type==WayBillReturnKey.WEIGHTERROR?"checked=\"checked\"":""%>/>重量错误
				    			<input type="radio" name="return_type" value="<%=WayBillReturnKey.CUSTOMSRETURN%>" <%=return_type==WayBillReturnKey.CUSTOMSRETURN?"checked=\"checked\"":""%>/>海关退货
				    			<input type="radio" name="return_type" value="<%=WayBillReturnKey.CALLOFF%>" <%=return_type==WayBillReturnKey.CALLOFF?"checked=\"checked\"":""%>/>我方截货
				    			<input type="radio" name="return_type" value="<%=WayBillReturnKey.DELIVERYDAMAGED%>" <%=return_type==WayBillReturnKey.DELIVERYDAMAGED?"checked=\"checked\"":""%>/>运输残损
				    			<input type="radio" name="return_type" value="<%=WayBillReturnKey.DELIVERYLOST%>" <%=return_type==WayBillReturnKey.CALLOFF?"checked=\"checked\"":""%>/>运输丢失
				    		</td>
				    	</tr>
				    	<tr>
				    		<td>备注:</td>
				    		<td>
				    			<textarea id="return_note" name="return_note" rows="5" cols="70"><%=return_note.trim()%></textarea>
				    		</td>
				    	</tr>
				    </table>
  				</td>
  			</tr>
  			<tr>
				<td align="right" valign="bottom">				
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
							  <input type="button" name="Submit2" value="确定" class="normal-green" onclick="wayBillReturnSub()">
							  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close()">
							</td>
						</tr>
					</table>
				</td>
			</tr>
  		</table>
  	</form>
  </body>
</html>
