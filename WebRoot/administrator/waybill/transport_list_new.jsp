<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.ProductStorageTypeKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.DeclarationKey"%>
<%@page import="com.cwc.app.key.ClearanceKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<jsp:useBean id="purchasekey" class="com.cwc.app.key.PurchaseKey"/>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="fileWithTypeKey" class="com.cwc.app.key.FileWithTypeKey"/>
<jsp:useBean id="transportCertificateKey" class="com.cwc.app.key.TransportCertificateKey"/>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/>
 <jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey"/>
<jsp:useBean id="transportQualityInspectionKey" class="com.cwc.app.key.TransportQualityInspectionKey"/>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%>
<html>
<% 	
    long send_psid=StringUtil.getLong(request,"send_psid");
    long receive_psid=StringUtil.getLong(request,"receive_psid");
	DBRow[] rows=transportMgrZwb.findTransport(send_psid,receive_psid);
	DBRow countRow=transportMgrZwb.findTransportCount(send_psid,receive_psid);
	TransportOrderKey transportOrderKey = new TransportOrderKey();
	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
%>
<head>
<script>
//页面全选 操作等
function checkAll(){
	var check = $("#allCheck");
	if(check.attr("checked")){
		$("input[name='choose']").attr("checked",true);
	}else{		
		$("input[name='choose']").attr("checked",false);
	}
}
</script> 
<style type="text/css">
.set{padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;border: 2px solid blue;} 
ul.processUl{list-style-type:none;}
ul.processUl li {line-height:20px;border-bottom:1px dashed  silver;clear:both;}
ul.processUl li span.right{dispaly:block;float:right;margin-right:3px;width:57px;text-align:left;}
ul.processUl li span.left{dispaly:block;float:left;}


span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
span.stateName{width:70px;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:190px;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;word-wrap:break-all;}
</style>  
</head>
<body>
    <input type="hidden" value="<%=countRow.get("count",0l) %>" id="transport_list_count" />
	<div id="transportList">
	    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true" isBottom="true">
		    <tr> 
		   		<th width="2%"  nowrap="nowrap"  class="left-title  "  style="vertical-align: center;text-align: center;"><input type="checkbox" name="allCheck" id="allCheck" onclick="checkAll()" /></th>
		        <th width="26%" nowrap="nowrap"  class="left-title  "  style="vertical-align: center;text-align: center;">转运单基本信息</th>
		        <th width="20%" nowrap="nowrap"  class="left-title  "  style="vertical-align: center;text-align: center;">库房信息</th>
		        <th width="22%" nowrap="nowrap"  class="right-title "  style="vertical-align: center;text-align: center;">运输信息</th>
		        <th width="20%" nowrap="nowrap"  class="right-title "  style="vertical-align: center;text-align: center;">流程信息</th>
		  	</tr>
		  	<%for(int i = 0 ;i<rows.length;i++){%>
		  	<tr>
		  		<td><input type="checkbox" name="choose" id="choose" value="<%=rows[i].get("transport_id",0l)%>" /></td>
		  		<td>
		  			<fieldset class="set">
	  				<!-- 如果是交货行转运单那么就是Id显示成蓝色 -->
	  					<%String fontColor = rows[i].get("purchase_id",0l) > 0l ? "mediumseagreen;":"#f60;";%>
	  					<legend>
	  					<a style="color:<%=fontColor %>" target="_blank" href="../transport/transport_order_detail.html?transport_id=<%=rows[i].getString("transport_id")%>">T<%=rows[i].getString("transport_id")%></a>
	  					&nbsp;<%=transportOrderKey.getTransportOrderStatusById(rows[i].get("transport_status",0)) %>
	  					</legend>
	  					<p style="text-align:left;clear:both;">
	  						<span class="stateName">创建人:</span>
	  						<span class="stateValue">
	  						<%
	  						 	DBRow createAdmin = adminMgr.getDetailAdmin(rows[i].get("create_account_id",0l));
	  						 	if(createAdmin!=null){
	  						 		out.print(createAdmin.getString("employe_name"));
	  						 	} 
	  						 %> 
	  						 </span>
	  					</p>
	  					<p style="text-align:left;clear:both;">
	  						<span class="stateName">允许装箱:</span>
	  						<span class="stateValue">
	  						<%
	  						 	DBRow packingAdmin = adminMgr.getDetailAdmin(rows[i].get("packing_account",0l));
	  						 	if(packingAdmin!=null){
	  						 		out.print(packingAdmin.getString("employe_name"));
	  						 	} 
	  						 %>
	  						 </span>
	  					</p>
	  					<p style="text-align:left;clear:both;">
	  						<span class="stateName">创建时间:</span><span class="stateValue"><%=tDate.getFormateTime(rows[i].getString("transport_date"))%></span>
	  					</p>
	  					<p style="text-align:left;clear:both;">
	  					  <span class="stateName"> 更新时间:</span><span class="stateValue"><%= tDate.getFormateTime(rows[i].getString("updatedate")) %></span>
	  					</p>
	  				</fieldset>
	  				<%
  					DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(rows[i].getString("purchase_id"));
  					if(!"0".equals(rows[i].getString("purchase_id"))){
	  				%>
	  				<fieldset class="set" style="border-color:#993300;">
	  					<legend>
	  						<a href="javascript:void(0)" onclick="window.open('<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail.html?purchase_id=<%=rows[i].get("purchase_id",0) %>')">P<%=rows[i].getString("purchase_id")%></a>
		  					&nbsp;<%DBRow storage = catalogMgr.getDetailProductStorageCatalogById(purchase.get("ps_id",0l));out.print(null==storage?"":storage.getString("title"));%>
	  					</legend>
	  					<%
		  					String styleWrap = "<p style='text-align:left;clear:both;'>";
		  					String supplierStyle = "<p style='text-align:left;clear:both;'><span class='stateName'>&nbsp;</span><span class='stateValue'>";
							try{
								DBRow supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(purchase.getString("supplier")));
								if(supplier!=null){
									DBRow productLine = productLineMgrTJH.getProductLineById(supplier.get("product_line_id",0l));
									if(productLine !=null){
										out.println(styleWrap+"<span class='stateName'>产品线:</span><span class='stateValue'>"+productLine.getString("name")+"</span></p>");
									}
									out.println(supplierStyle+supplier.getString("sup_name")+"</span></p>");
								}
							}catch(NumberFormatException e){
								out.println(supplierStyle+purchase.getString("supplier")+"</span></p>");
							}
						%>
						<p style="text-align:left;clear:both;">
							<span class="stateName">主流程:</span>
							<span class="stateValue"><%=purchasekey.getQuoteStatusById(purchase.getString("purchase_status"))%></span>
						</p>
				  		<p style="text-align:left;clear:both;">
				  			<span class="stateName">负责人:</span>
				  			<span class="stateValue"><%=purchase.getString("proposer") %></span>
				  		</p>
				  		<p style="text-align:left;clear:both;">
				  			<span class="stateName">采购时间:</span>
				  			<span class="stateValue"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(purchase.getString("purchase_date")))%></span>
				  		</p>
				  		<p style="text-align:left;clear:both;">
				  			<span class="stateName">更新日期:</span>
				  			<span class="stateValue"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(purchase.getString("updatetime")))%></span>
				  		</p>
						<p style="text-align:left;clear:both;">
							<span class="stateName">总体积:</span>
							<span class="stateValue"><%=purchaseMgr.getPurchaseVolume(purchase.get("purchase_id",0l))%> cm³</span>
							</p>
						<p style="text-align:left;clear:both;">
							<span class="stateName">总重量:</span>
							<span class="stateValue"><%=purchaseMgr.getPurchaseWeight(purchase.get("purchase_id",0l))%> Kg</span>
						</p>
						<p style="text-align:left;clear:both;">
							<span class="stateName">总金额:</span>
							<span class="stateValue"><%=purchaseMgr.getPurchasePrice(purchase.get("purchase_id",0l))%> RMB</span>
						</p>
						<p style="text-align:left;clear:both;">
							<span class="stateName">价格确认:</span>
							<span class="stateValue"><%=purchase.get("price_affirm_over",0d)==0?df.format(tDate.getDiffDate(purchase.getString("purchase_date"),"dd"))+"天":purchase.get("price_affirm_over",0d)+"天完成"%></span>
						</p>
						<%DBRow prepareDBRow = preparePurchaseMgrZwb.getPreparePurchaseFunds(Long.valueOf(rows[i].getString("purchase_id")));
			               if(prepareDBRow!=null){ 
			                  if(prepareDBRow.getString("currency").equals("USD")){ 
					    %>
					               <p style="text-align:left;clear:both;"><span class="stateName">预申请金额：</span><span class="stateValue"><%=prepareDBRow.getString("amount")%><%=prepareDBRow.getString("currency")%>/<%=prepareDBRow.getString("standard_money")%>RMB</span></p>
				                <%}else{%>
				                   <p style="text-align:left;clear:both;"><span class="stateName">预申请金额：</span><span class="stateValue"><%=prepareDBRow.getString("amount")%><%=prepareDBRow.getString("currency")%></span></p>
				                <%} %>   
		               <%} %>
	  				</fieldset>
		  				<%
		  					DBRow[] transportOrderRows = transportMgrZyj.getTransportRowsByPurchaseId(Long.valueOf(rows[i].getString("purchase_id")));
		  					for(int tr = 0; tr < transportOrderRows.length; tr ++){
		  						if(!rows[i].getString("transport_id").equals(transportOrderRows[tr].getString("transport_id"))){
						%>
				  				<fieldset class="set" style='border-color:#993300;'>
				  					<legend>
				  					<a style="color:mediumseagreen;" target="_blank" href="transport_order_detail.html?transport_id=<%=transportOrderRows[tr].getString("transport_id")%>">T<%=transportOrderRows[tr].getString("transport_id")%></a>
				  					&nbsp;<%=transportOrderKey.getTransportOrderStatusById(transportOrderRows[tr].get("transport_status",0)) %>
				  					</legend>
				  					<p style="text-align:left;clear:both;">
				  						<span class="stateName">创建人:</span>
				  						<span class="stateValue">
				  						<%
				  						 	DBRow createAdminTransprot = adminMgr.getDetailAdmin(transportOrderRows[tr].get("create_account_id",0l));
				  						 	if(createAdminTransprot!=null){
				  						 		out.print(createAdminTransprot.getString("employe_name"));
				  						 	} 
				  						 %> 
				  						 </span>
				  					</p>
				  					<p style="text-align:left;clear:both;">
				  						<span class="stateName">允许装箱:</span>
				  						<span class="stateValue">
				  						<%
				  						 	DBRow packingAdminTransprot = adminMgr.getDetailAdmin(transportOrderRows[tr].get("packing_account",0l));
				  						 	if(packingAdminTransprot!=null){
				  						 		out.print(packingAdminTransprot.getString("employe_name"));
				  						 	} 
				  						 %>
				  						 </span>
				  					</p>
				  					<p style="text-align:left;clear:both;">
				  						<span class="stateName">创建时间:</span><span class="stateValue"><%=tDate.getFormateTime(transportOrderRows[tr].getString("transport_date"))%></span>
				  					</p>
				  					<p style="text-align:left;clear:both;">
				  					  <span class="stateName"> 更新时间:</span><span class="stateValue"><%= tDate.getFormateTime(transportOrderRows[tr].getString("updatedate")) %></span>
				  					</p>
				  				</fieldset>
		  				<%}}}%>
		  		</td>
		  		<td>
		  			<div style="height:20px;">
	  				提货仓库:
	  				<% 
	  					int fromPsType = rows[i].get("from_ps_type",0);
	  					//如果是供应商的Type那么就需要去查询供应商的名称
	 	  				if(fromPsType == ProductStorageTypeKey.SupplierWarehouse){
	 	  					DBRow temp = supplierMgrTJH.getDetailSupplier(rows[i].get("send_psid",0l));
	 	  					if(temp != null){
	 	  						out.println(temp.getString("sup_name"));
	 	  					}
	 	  				}else{
	 	  					DBRow storageCatalog = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("send_psid",0l));
	 	  					if(storageCatalog != null){
	 	  						out.print(storageCatalog.getString("title"));
	 	  					}
	 	  				}
	  				%>
	  				</div>
	  				<div style="height:20px;">
	  					收货仓库:<%=catalogMgr.getDetailProductStorageCatalogById(rows[i].get("receive_psid",0l)).getString("title")%> 
	  				</div>
	  				<div style="height:20px;">
	  					&nbsp;&nbsp;&nbsp;收货人:
	  					<%	 
	  						long deliveryer_id = rows[i].get("deliveryer_id",0l);
	  						if(deliveryer_id != 0L){
	  							out.println(null!=adminMgr.getDetailAdmin(deliveryer_id)?adminMgr.getDetailAdmin(deliveryer_id).getString("employe_name"):"");
	  						}
	  					%>
	  				</div>
	  				<div style="height:20px;">
	  					收货时间:
	  					<%
	  						String deliveryTime = rows[i].getString("deliveryed_date");
	  						 out.print(deliveryTime.length() > 10 ?deliveryTime.substring(0,10): deliveryTime);
	  					%>
	  				</div>
	  				<div style="height:20px;">
	  					&nbsp;&nbsp;&nbsp;入库人:
	  					<%
	  						long warehousinger_id = rows[i].get("warehousinger_id",0l);
							if(warehousinger_id != 0L){
								out.println(adminMgr.getDetailAdmin(warehousinger_id).getString("employe_name"));
							}
	  					%>
	  				</div>
	  				<div style="margin-bottom: 5px; height:20px;">
	  					入库时间:
	  					<%  
	  						String warehousingTime = rows[i].getString("warehousing_date");
	  						out.print(warehousingTime.length() > 10 ?warehousingTime.substring(0,10): warehousingTime);
	  					%>
	  				</div>
		  		</td>
		  		<td>
		  			<div style="margin-top: 5px; height:20px;">运单号:<%=rows[i].getString("transport_waybill_number")%></div>
	  				<div style="height:20px;">货运公司:<%=rows[i].getString("transport_waybill_name")%></div>
	  				<div style="height:20px;">承运公司:<%=rows[i].getString("carriers")%></div>
	  				<%
	  					long transport_send_country = rows[i].get("transport_send_country",0l);
	  					DBRow send_country_row = transportMgrLL.getCountyById(Long.toString(transport_send_country));
	  					long transport_receive_country = rows[i].get("transport_receive_country",0l);
	  					DBRow receive_country_row = transportMgrLL.getCountyById(Long.toString(transport_receive_country));					
	  				%>
	  				<div style="height:20px;">始发国/始发港:<%=send_country_row==null?"无":send_country_row.getString("c_country")%>/<%=rows[i].getString("transport_send_place").equals("")?"无":rows[i].getString("transport_send_place")%></div>
	  				<div style="height:20px;">目的国/目的港:<%=receive_country_row==null?"无":receive_country_row.getString("c_country")%>/<%=rows[i].getString("transport_receive_place").equals("")?"无":rows[i].getString("transport_receive_place")%></div>
	  				<div style="height:20px;">总体积:<%=transportMgrZJ.getTransportVolume(rows[i].get("transport_id",0l))%> cm³</div>
	  				<div style="height:20px;">总重量:<%=transportMgrZJ.getTransportWeight(rows[i].get("transport_id",0L)) %> Kg</div>
	  				<div style="height:20px;">总金额:<%=transportMgrZJ.getTransportSendPrice(rows[i].get("transport_id",0L)) %> RMB</div>
		 			<%
					if(rows[i].get("stock_in_set",1)==1){
						out.println("<font color='red'><b>");
					}
					%>
		  		</td>
		  		<td>
		  			<!--   对于有的流程 运费,清关,报关有了跟进中的时候才显示 花费多长的时间 -->
	  				<!-- 流程的跟进如果是完成+ 已经上传文件的显示绿色。没有上传文件的就是红色。完成了的用粗体显示 -->
	  				<ul class="processUl">
		  					<li>
								<span class="left">货物状态:
								<%=transportOrderKey.getTransportOrderStatusById(rows[i].get("transport_status",0)) %>
								<%
		  							 DBRow[] transportPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 4);
		  							 if(transportPersons != null && transportPersons.length > 0){
		  						%>
		  							<br/>
		  							<%for(DBRow tempUserRow : transportPersons){%>
		  								<%= tempUserRow.getString("employe_name")%>
		  							<%}%>
		  						<%}%>
								</span>
								<span class="right" style="">
								<%=rows[i].getString("all_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("all_over")+"天完成"%>
								</span>
		  					</li>
		  					<li>
		  						<span class="left">
		 	  						实物图片:
		 	  						<%
			  						//计算颜色
			  						String productFileClass = "" ;
		  							int productFileInt = rows[i].get("product_file",TransportProductFileKey.NOPRODUCTFILE);
		  							if(productFileInt == TransportProductFileKey.FINISH){
		  								productFileClass += "spanBold";
		  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("product_file",rows[i].get("transport_id",0l),FileWithTypeKey.product_file);
		  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
		  									productFileClass += " fontGreen";
		  								}else{
		  									productFileClass += " fontRed";
		  								}
		  							}else if(productFileInt != TransportProductFileKey.NOPRODUCTFILE){
		  								productFileClass += "spanBold spanBlue";
		  							}
		  							%>
		 	  						<span class="<%=productFileClass %>"><%=transportProductFileKey.getStatusById(rows[i].get("product_file",TransportProductFileKey.NOPRODUCTFILE)) %></span>
			  						<%
			  							 DBRow[] productFilePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 10);
			  							 if(productFilePersons != null && productFilePersons.length > 0){
		  							%>
		  	  							<br/>
		  	  							<%for(DBRow tempUserRow : productFilePersons){%>
			  								<%= tempUserRow.getString("employe_name")%>&nbsp;
			  							<%}%>
			  						<%}%>
			  					</span>
			  					<span class="right">
			  						<%=rows[i].get("product_file",TransportProductFileKey.NOPRODUCTFILE) == TransportProductFileKey.NOPRODUCTFILE?"":(rows[i].getString("product_file_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("product_file_over")+"天完成")%>
		  						</span>
		  					</li>
		  					<li>
		  						<span class="left">
		  							内部标签:
		  							<%
			  						//计算颜色
			  						String tagClass = "" ;
		  							int tagInt = rows[i].get("tag",TransportTagKey.NOTAG);
		  							if(tagInt == TransportTagKey.FINISH){
		  								tagClass += "spanBold";
		  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.TRANSPORT_TAG);
		  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
		  									tagClass += " fontGreen";
		  								}else{
		  									tagClass += " fontRed";
		  								}
		  							}else if(tagInt != TransportTagKey.NOTAG){
		  								tagClass += "spanBold spanBlue";
		  							}
			  					%>
		  						<span class="<%=tagClass %>"><%=transportTagKey.getTransportTagById(rows[i].get("tag",TransportTagKey.NOTAG))%></span>
		  							<%
		  							 DBRow[] tagPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 8);
		  							 if(tagPersons != null && tagPersons.length > 0){
		  							%>
		  	  							<br/>
		  	  							<%for(DBRow tempUserRow : tagPersons){%>
		  									 <%= tempUserRow.getString("employe_name")%>&nbsp;
		  								<%}%>
		  							<%}%>
		  						</span>
		  						<span class="right">	
		  							<%=rows[i].get("tag",01)==1?"":(rows[i].getString("tag_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("tag_over")+"天完成")%>
		  						</span>
		  					</li>
		  					<li>
		  						<span class="left">	
			  				质检流程:
		  							<%
			  						//计算颜色
			  						String qualityInspectionClass = "" ;
		  							int qualityInspectionInt = rows[i].get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY);
		  							if(qualityInspectionInt == TransportQualityInspectionKey.FINISH){
		  								qualityInspectionClass += "spanBold";
		  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.TRANSPORT_QUALITYINSPECTION);
		  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
		  									qualityInspectionClass += " fontGreen";
		  								}else{
		  									qualityInspectionClass += " fontRed";
		  								}
		  							}else if(qualityInspectionInt != TransportQualityInspectionKey.NO_NEED_QUALITY){
		  								qualityInspectionClass += "spanBold spanBlue";
		  							}
		  							%>
		  							<span class="<%=qualityInspectionClass %>"><%=transportQualityInspectionKey.getStatusById(rows[i].get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY)+"") %></span>
		  							<%
			  							 DBRow[] qualityInspectionPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 11);
			  							 if(qualityInspectionPersons != null && qualityInspectionPersons.length > 0){
			  						%>
			  	  						<br/>
			  	  						<%for(DBRow tempUserRow : qualityInspectionPersons){%>
			  								<%= tempUserRow.getString("employe_name")%>&nbsp;
			  							<%}%>
			  						<%}%>
		  						</span>
		  						<span class="right">
			  						<%=rows[i].get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY) == TransportQualityInspectionKey.NO_NEED_QUALITY?"":(rows[i].getString("quality_inspection_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("quality_inspection_over")+"天完成")%>
		  						</span>
		  					</li>
		  					<li>
		  						<span class="left">
			  				出口报关:
		  					<%
		  						//计算颜色
		  						String declarationKeyClass = "" ;
	  							int declarationInt = rows[i].get("declaration",declarationKey.NODELARATION);
	  							if(declarationInt == declarationKey.FINISH){
	  								declarationKeyClass += "spanBold";
	  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.transport_declaration);
	  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
	  									declarationKeyClass += " fontGreen";
	  								}else{
	  									declarationKeyClass += " fontRed";
	  								}
	  							}else if(declarationInt != declarationKey.NODELARATION ){
	  								declarationKeyClass +=  "spanBold spanBlue";
	  							}
		  					%>
		  					<span class='<%= declarationKeyClass%>'><%=declarationKey.getStatusById(rows[i].get("declaration",TransportStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>
	  						<%
	  							 DBRow[] declarationPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 7);
	  							 if(declarationPersons != null && declarationPersons.length > 0){
	  						%>
	  	  						<br/>
	  	  						<%for(DBRow tempUserRow : declarationPersons){%>
	  								<%= tempUserRow.getString("employe_name")%>&nbsp;
	  							<%}%>
	  	  					<%}%>
	  						</span>
	  						<span class="right" style="">
	 	  					<%
		  						if(rows[i].getString("declaration_over").trim().length() > 0){
		  							out.println(rows[i].getString("declaration_over")+"天完成");
		  						}else{
		  							if(rows[i].get("declaration",DeclarationKey.NODELARATION) != DeclarationKey.NODELARATION){
			  							 String time = transportMgrZr.getProcessSpendTime(rows[i].get("transport_id",0l),7,DeclarationKey.DELARATING);
			  							if(time.trim().length() > 0 ){
			  								out.println(df.format(tDate.getDiffDate(time,"dd")));
			  							}
		  							}
		  						}
		  					%>
		  					</span>
	  					</li>
	  					<li>
	  					<span class="left">
		  			进口清关:
	  						<%
		  						//计算颜色
		  						String clearanceKeyClass = "" ;
	  							int clearanceInt = rows[i].get("clearance",clearanceKey.NOCLEARANCE);
	  							if( clearanceInt == clearanceKey.FINISH){
	  								clearanceKeyClass += "spanBold";
	  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.transport_clearance);
	  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
	  									clearanceKeyClass += " fontGreen";
	  								}else{
	  									clearanceKeyClass += " fontRed";
	  								}
	  							}else if(clearanceInt != clearanceKey.NOCLEARANCE){
	  								clearanceKeyClass +=  "spanBold spanBlue";
	  							}
		  					%>
				  			<span class="<%= clearanceKeyClass%>"><%=clearanceKey.getStatusById(rows[i].get("clearance",ClearanceKey.NOCLEARANCE)) %></span>
				  			<%
	  							 DBRow[] clearancePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 6);
	  							 if(clearancePersons != null && clearancePersons.length > 0){
							%>
	  							<br/>
	  							<%for(DBRow tempUserRow : clearancePersons){%>
	  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
	  							 <%}%>
	  						 <%}%>
				  		</span>
				  		<span class="right">
		  				<%
	  						if(rows[i].getString("clearance_over").trim().length() > 0){
	  							out.println(rows[i].getString("clearance_over")+"天完成");
	  						}else{
	  							if(rows[i].get("clearance",ClearanceKey.NOCLEARANCE) != ClearanceKey.NOCLEARANCE){
		  							 String time = transportMgrZr.getProcessSpendTime(rows[i].get("transport_id",0l),7,ClearanceKey.CLEARANCEING);
		  							if(time.trim().length() > 0 ){
		  								out.println(df.format(tDate.getDiffDate(time,"dd"))+"天");
		  							}
	  							}
	  						}
		  		 		%>
				  		 	</span>
	  					</li>
	  					<li>
	  						<span class="left">
	  							运费流程:
	  							<%
		  						//计算颜色
		  						String stockInSetClass = "" ;
	  							int stockInSetInt = rows[i].get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET);
	  							if(stockInSetInt == TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH){
	  								stockInSetClass += "spanBold";
	  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.TRANSPORT_STOCKINSET);
	  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
	  									stockInSetClass += " fontGreen";
	  								}else{
	  									stockInSetClass += " fontRed";
	  								}
	  							}else if(stockInSetInt != TransportStockInSetKey.SHIPPINGFEE_NOTSET){
	  								stockInSetClass += "spanBold spanBlue";
	  							}
	  							%>
	  							<span class='<%=stockInSetClass %>'><%=transportStockInSetKey.getStatusById(rows[i].get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>
		  						<%
		  							 DBRow[] stockInSetPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 5);
		  							 if(stockInSetPersons != null && stockInSetPersons.length > 0){
	  							%>
	  	  							<br/>
	  	  							<%for(DBRow tempUserRow : stockInSetPersons){%>
		  								 <%= tempUserRow.getString("employe_name")%>&nbsp;
		  							<%}%>
	  	  						<%}%>
		  					</span>
		  					<span class="right">
		  					<%
		  						if(rows[i].getString("stock_in_set_over").trim().length() > 0){
		  							out.println(rows[i].getString("stock_in_set_over")+"天");
		  						}else{
		  							if(rows[i].get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET) != TransportStockInSetKey.SHIPPINGFEE_NOTSET){
			  							 String time = transportMgrZr.getProcessSpendTime(rows[i].get("transport_id",0l),5,transportStockInSetKey.SHIPPINGFEE_SET);
			  							if(time.trim().length() > 0 ){
			  								out.println(df.format(tDate.getDiffDate(time,"dd"))+"天完成");
			  							}
		  							}
		  						}
		  					%>
		  					</span>
	  					</li>
	  					<li>
	  						<span class="left">
	  							单证状态:
	  							<%
		  						//计算颜色
		  						String certificateClass = "" ;
	  							int certificateInt = rows[i].get("certificate",TransportCertificateKey.NOCERTIFICATE);
	  							if(certificateInt == TransportCertificateKey.FINISH){
	  								certificateClass += "spanBold";
	  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.transport_certificate);
	  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
	  									certificateClass += " fontGreen";
	  								}else{
	  									certificateClass += " fontRed";
	  								}
	  							}else if(certificateInt != TransportCertificateKey.NOCERTIFICATE){
	  								certificateClass += "spanBold spanBlue";
	  							}
	  							%>
	  							<span class="<%=certificateClass %>"><%=transportCertificateKey.getStatusById(rows[i].get("certificate",TransportCertificateKey.NOCERTIFICATE))%></span>
	  							<%
		  							 DBRow[] certificatePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 9);
		  							 if(certificatePersons != null && certificatePersons.length > 0){
	  							%>
	  	  							<br/>
	  	  							<%for(DBRow tempUserRow : certificatePersons){%>
		  								 <%= tempUserRow.getString("employe_name")%>&nbsp;
		  							<%}%>
		  						<%}%>
	  						</span>
	  						<span class="right">
	  							<%=rows[i].get("certificate",01)==1?"":(rows[i].getString("certificate_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("certificate_over")+"天完成")%><br/>
	  						</span>
	  					</li>
	  					<li>
	  						<span class="left">
	  							第三方标签:
	  							<%
		  						//计算颜色
		  						String tagClassThird = "" ;
	  							int tagIntThird = rows[i].get("tag_third",TransportTagKey.NOTAG);
	  							if(tagIntThird == TransportTagKey.FINISH){
	  								tagClassThird += "spanBold";
	  								int[] fileType	= {FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE};
	  					  			if(purchaseMgrZyj.checkPurchaseThirdTagFileFinish(rows[i].get("transport_id",0L),fileType,"transport_tag_types"))
	  					  			{
	  									tagClassThird += " fontGreen";
	  								}else{
	  									tagClassThird += " fontRed";
	  								}
	  							}else if(tagIntThird != TransportTagKey.NOTAG){
	  								tagClassThird += "spanBold spanBlue";
	  							}
		  					%>
	  						<span class="<%=tagClassThird %>"><%=transportTagKey.getTransportTagById(rows[i].get("tag_third",TransportTagKey.NOTAG))%></span>
	  							<%
	  							 DBRow[] tagPersonsThird	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.THIRD_TAG);
	  							 if(tagPersonsThird != null && tagPersonsThird.length > 0){
	  							%>
	  	  							<br/>
	  	  							<%for(DBRow tempUserRowThird : tagPersonsThird){%>
	  								 	<%= tempUserRowThird.getString("employe_name")%>&nbsp;
	  								 <%}%>
	  	  					<%}%>
	  						</span>
	  						<span class="right">	
	  							<%=rows[i].get("tag_third",01)==1?"":(rows[i].getString("tag_third_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("tag_third_over")+"天完成")%>
	  						</span>
	  					</li>
	  				</ul>
		  		</td>
		  	</tr>
		  	<%} %>
		</table>
	</div>
</body>
</html>
