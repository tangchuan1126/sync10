<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<jsp:useBean id="payTypeKey" class="com.cwc.app.key.PayTypeKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	long out_id = StringUtil.getLong(request,"out_id");
	
	DBRow outbound = outboundOrderMgrZJ.getDetailOutboundById(out_id);
	
	Map<String,DBRow> list = backUpStorageMgrZJ.splitRequire(out_id);

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>打印出库单</title>
</head>
<body>
<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table"  style="margin-top:5px;">
	<thead>
		<tr>
			<td colspan="4">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td style="font-family: Arial Black;font-size: 19px; padding-bottom:10px;"><%=catalogMgr.getDetailProductStorageCatalogById(outbound.get("ps_id",0l)).getString("title")%>第<%=out_id%>号出库单</td>
						<td nowrap="nowrap" align="left" style="font-family: 黑体;font-size:large;padding-bottom:10px;">
							<%=DateUtil.NowStr()%>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							包含运单:
							<%
								DBRow[] wayBillOrders = wayBillMgrZJ.filterWayBillOrder("","",null,0,0,"",0,0,0,0,out_id,null,-1,0,0,0);
								for(int q=0;q<wayBillOrders.length;q++)
								{
									out.print(wayBillOrders[q].get("waybill_id",0l));
									out.print(" ");
								}
							%>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">拆散商品</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">获得商品</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-top: 2px #000000 solid;border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">获得数量</th>
			<th align="left"  class="left-title" style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-right: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">最终商品</th>
		</tr>
	</thead>
	<tbody id="tbody">
		<%
			Set<String> splitRequire = list.keySet();
		
			if (splitRequire.size()>0) 
			{
				for (String pcid : splitRequire) 
				{
					DBRow splitRequireDetail = list.get(pcid);
					
					String[] ids = pcid.split(",");
					
					long split_pc_id = Long.parseLong(ids[0]);
					long lastToPcid = Long.parseLong(ids[1]);
					
					ArrayList<String> splitRequireDetails = splitRequireDetail.getFieldNames();
					
					for(int i = 0;i<splitRequireDetails.size();i++)
					{
						long need_pc_id = Long.parseLong(splitRequireDetails.get(i));
						if(i==0)
						{
		%>
						<tr>
							<td align="left" rowspan="<%=splitRequireDetails.size()%>" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size:10px;">
								<%
									DBRow splitRequireProduct = productMgr.getDetailProductByPcid(split_pc_id);
									out.print(splitRequireProduct.getString("p_name")+"("+split_pc_id+")");
								%>
							</td>
							<td align="left" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 10px;">
								<%
									DBRow needProduct = productMgr.getDetailProductByPcid(need_pc_id);
									out.print(needProduct.getString("p_name")+"("+splitRequireDetails.get(i)+")");
								%>
							</td>
							<td align="center" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 10px;"><%=splitRequireDetail.getString(splitRequireDetails.get(i))%></td>
							<td rowspan="<%=splitRequireDetails.size()%>" style="border-left: 2px #000000 solid;border-right: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 10px;">
								<%=lastToPcid%>
							</td>
						</tr>
		<%	
						}
						else
						{
		%>
						<tr>
							<td align="left" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 10px;">
								<%
									DBRow needProduct = productMgr.getDetailProductByPcid(need_pc_id);
									out.print(needProduct.getString("p_name")+"("+splitRequireDetails.get(i)+")");
								%>
							</td>
							<td align="center" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:Arial;font-size: 10px;">
								<%=splitRequireDetail.getString(splitRequireDetails.get(i))%>
							</td>
						</tr>
		<%
						}
					}
				}
			}
		%>
	</tbody>
</table>
</body>
</html>