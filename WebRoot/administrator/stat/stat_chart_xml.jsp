<%byte[] utf8Bom = new byte[]{(byte) 0xef, (byte) 0xbb, (byte) 0xbf};String utf8BomStr = new String(utf8Bom,"UTF-8");%><%=utf8BomStr%><?xml version='1.0' encoding='UTF-8'?><%@ page contentType="text/xml;charset=utf-8"%><%@ include file="../../include.jsp"%> 
<%
/**
//所有库存商品
DBRow allStorageProducts[] = productMgr.getProductStorages(0,null);
DBRow lackingProducts[] = productMgr.getProductStorages(1,null);

//计算有货总库存量
float totalstore = 0;
for (int i=0; i<allStorageProducts.length; i++)
{
	if ( allStorageProducts[i].get("store_count",0f)>=allStorageProducts[i].get("store_count_alert",0f) )
	{
		totalstore++;
	}
}
**/
%>

<chart caption='当前缺货商品统计' logoURL='../imgs/stat_logo.png'  logoPosition='BR' logoAlpha='30'  subcaption='独立仓库' baseFontSize='12' palette='4' decimals='0' enableSmartLabels='1' enableRotation='0' bgColor='99CCFF,FFFFFF' bgAlpha='40,100' bgRatio='0,100' bgAngle='360' showBorder='1' startingAngle='70' >
<set label="France" value="17"/>
<set label="India" value="12"/>
<set label="Brazil" value="18"/>
<set label="USA" value="8"/>
<set label="Australia" value="10"/>
<set label="Japan" value="7" isSliced="1"/>
<set label="England" value="5" isSliced="1"/>
<set label="Nigeria" value="12" isSliced="1"/>
<set label="Italy" value="8"/>
<set label="China" value="10"/>
<set label="Canada" value="19"/>
<set label="Germany" value="15"/>            
</chart>