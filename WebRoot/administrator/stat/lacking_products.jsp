<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
 	//long stt = System.currentTimeMillis();

 String pscid = StringUtil.getString(request,"pscid");
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 String printDate = DateUtil.NowStr();
 String printer = adminLoggerBean.getAccount();

 String input_st_date = StringUtil.getString(request,"input_st_date");
 TDate tDate = new TDate();

 if (input_st_date.equals(""))
 {
 	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
 }



 int level;
 long id;
 long parentid;

 if (!pscid.equals(""))
 {
 	level = StringUtil.getInt(pscid.split("-")[0]);
 	id = StringUtil.getLong(pscid.split("-")[1]);
 	parentid = StringUtil.getLong(pscid.split("-")[2]);
 }
 else
 {
 	level = 0;
 	id = 0;
 	parentid = 0;
 }

 DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
 Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));


 //分页打印参数
 float storageNameHeight = 9;		//仓库名称高度
 float tableTitleHeight = 5;		//表格标题高度
 float productNameHeight = 4;		//商品名称高度
 float onePageHeight = 280;			//一个打印页面的高度283  

 float totalProductNameHeight = 0;	//所有商品高度之和（后面计算总页数用到）

 //存储打印数据
 DBRow storageContainers[] = new DBRow[0];
 DBRow productCatalogContainers[] = new DBRow[0];
 DBRow productContainers[] = new DBRow[0];



 if (!pscid.equals(""))
 {

 //按实际打印顺序存储数据
 ArrayList storageContainerAl = new ArrayList();
 DBRow stat_catalog[];
 DBRow productStorageCatalogs[] = catalogMgr.getProductStorageCatalogByParentId(0l,null);
 for (int i=0; i<productStorageCatalogs.length; i++)
 {
 	if ( (level==1&&id!=productStorageCatalogs[i].get("id",0l)) || (level==2&&parentid!=productStorageCatalogs[i].get("id",0l)) )
 	{
 		continue;
 	}

 	DBRow productStorageCatalogs_son[] = catalogMgr.getProductStorageCatalogByParentId(productStorageCatalogs[i].get("id",0l),null);
 	for (int j=0; j<productStorageCatalogs_son.length; j++)
 	{
 		if (level==2&&id!=productStorageCatalogs_son[j].get("id",0l))
 		{
 	continue;
 		}
 		
 		//货架分类
 		stat_catalog = orderMgr.getStatLackingProductsPCByPscid(productStorageCatalogs_son[j].get("id",0l));
 	
 		if (stat_catalog.length==0)
 		{
 	continue;
 		}
 	
 		//获得统计数据(catalog)
 		ArrayList productCatalogContainerAl = new ArrayList();
 		for (int k=0; k<stat_catalog.length; k++)
 		{	
 	  String catalogStr = "";
 	  int jj=0;
 	  DBRow allFather[] = tree.getAllFather(stat_catalog[k].get("catalog_id",0l));
 	  for (; jj<allFather.length-1; jj++)
 	  {
 		for (int jjj=0;jjj<jj; jjj++)
 		{
 			catalogStr += "&nbsp;&nbsp;&nbsp;";
 		}
 		catalogStr += allFather[jj].getString("title");
 		catalogStr += "<br>";
 	  }
 	  
 		for (int jjj=0;jjj<jj; jjj++)
 		{
 			catalogStr += "&nbsp;&nbsp;&nbsp;";
 		}
 		
 	  DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(stat_catalog[k].getString("catalog_id")));
 	  if (catalog!=null)
 	  {
 		catalogStr += catalog.getString("title");
 	  }
 	  
 	  
 	  
 	  
 	 //获得统计数据(product) 
 	ArrayList productContainerAl = new ArrayList();
 	
 	DBRow stat_product[] = orderMgr.getStatLackingProductsByPscidCatalogid(productStorageCatalogs_son[j].get("id",0l),stat_catalog[k].get("catalog_id",0l));
 	
 	for (int kk=0; kk<stat_product.length; kk++)
 	{
 		//---封装数据：统计商品数据
 		DBRow productContainer = stat_product[kk];			
 		productContainerAl.add(productContainer);
 	}
 	productContainers = (DBRow[])productContainerAl.toArray(new DBRow[0]);

 	//---封装数据：商品分类
 	DBRow productCatalogContainer = new DBRow();
 	productCatalogContainer.add("catalog_name",catalogStr);
 	productCatalogContainer.add("products",productContainers);
 	productCatalogContainerAl.add(productCatalogContainer);
 	productCatalogContainer.add("pcc_height",productContainers.length*productNameHeight);

 	totalProductNameHeight += productContainers.length*productNameHeight;
 		}

 		//---封装数据：仓库名称
 		DBRow storageContainer = new DBRow();
 		storageContainer.add("storage_name",productStorageCatalogs[i].getString("title")+" - "+productStorageCatalogs_son[j].getString("title"));
 		storageContainer.add("stat_data",(DBRow[])productCatalogContainerAl.toArray(new DBRow[0]));
 		storageContainerAl.add(storageContainer);
 	}
 }
 storageContainers = (DBRow[])storageContainerAl.toArray(new DBRow[0]);

 }
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<link rel="stylesheet" href="../js/fusioncharts/contents/style.css" type="text/css" />
<script language="JavaScript" src="../js/fusioncharts/jsclass/fusioncharts.js"></script>


<script>
function printStat()
{
	window.open("stat_printer.html?printArea=stat_content_print");
}

function stcalendarCallback(date, month, year)
{
	day = date;
	date = year+"-"+month+"-"+day;
	document.stat_form.input_st_date.value = date;

}
</script>



</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  >

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 统计报表 »   缺货通知单</td>
  </tr>
</table>
<br>


<table width="97%" border="0" cellpadding="4" cellspacing="0" style="margin-left:20px;">
  <tr>
    <td width="79%" bgcolor="#E3F2E3">
	<form action="" method="get" name="stat_form">
	
        <select name="pscid" id="pscid">
          <%
String qx;

for ( int i=0; i<treeRows.length; i++ )
{
	if (treeRows[i].get("level",0)>1)
	{
		continue;
	}

	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
          <option value="<%=treeRows[i].get("level",0)%>-<%=treeRows[i].getString("id")%>-<%=treeRows[i].getString("parentid")%>" <%=id==treeRows[i].get("id",0l)?"selected":""%> > 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>          </option>
          <%
}
%>
        </select>
    <input name="Submit4" type="submit" class="long-button-stat"  value="统计">
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="button" name="Submit" value="打 印" class="long-button-print" onClick="printStat()">
	</form>	</td>
    <td width="21%" align="left" valign="middle" bgcolor="#E3F2E3"><label></label></td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="display:">
  <tr>
    <td>
	
<div id="stat_content">  


<%
for (int i=0; i<storageContainers.length; i++)
{
	productCatalogContainers = (DBRow[])storageContainers[i].get("stat_data",new Object());
%>   
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td align="center" valign="middle"  style="font-size:15px;font-weight:bold;font-family:Arial;padding-bottom:10px;"><%=storageContainers[i].getString("storage_name")%></td>
  </tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#999999">
  <tr>
    <td align="left" valign="middle"  style="font-size:12px;font-family:Arial;padding-left:8px;background:#999999;color:#FFFFFF;border-left:1px #ffffff solid;font-weight:bold">商品分类</td>
    <td bgcolor="#FFFFFF"><table width="100%" height="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#999999">

      <tr>
        <td width="36%" align="left" valign="middle"  style="background:#999999;font-size:12px;font-family:Arial;color:#FFFFFF;border-left:1px #ffffff solid;font-weight:bold">商品名称</td>
        <td width="19%" align="center" valign="middle"  style="background:#999999;color:#FFFFFF;border-left:1px #ffffff solid;font-weight:bold">库存</td>
        <td width="13%" align="center" valign="middle"  style="background:#999999;color:#FFFFFF;border-left:1px #ffffff solid;font-weight:bold">存放位置</td>
        <td width="13%" align="center" valign="middle"  style="background:#999999;color:#FFFFFF;border-left:1px #ffffff solid;font-weight:bold">&nbsp;</td>
        <td width="12%" align="center" valign="middle"  style="background:#999999;color:#FFFFFF;border-left:1px #ffffff solid;font-weight:bold">&nbsp;</td>
        <td width="7%" align="center" valign="middle"  style="background:#999999;color:#FFFFFF;border-left:1px #ffffff solid;">&nbsp;</td>
      </tr>

    </table></td>
  </tr>
<%
for (int ii=0; ii<productCatalogContainers.length; ii++)
{
	productContainers = (DBRow[])productCatalogContainers[ii].get("products",new Object());
%>
  <tr>
    <td width="18%" height="25" align="left" valign="middle" bgcolor="#eeeeee" style="font-size:12px;font-family:Arial;padding-left:8px;">
	  <%=productCatalogContainers[ii].getString("catalog_name")%></td>
    <td width="82%" bgcolor="#FFFFFF"><table width="100%" height="1%" border="0" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
      
	<%
	for (int iii=0; iii<productContainers.length; iii++)
	{

	%>
      <tr>
        <td width="36%" height="25" align="left" valign="middle"  style="background:#ffffff;font-family:Arial;"><%=productContainers[iii].getString("p_name")%></td>
        <td width="19%" align="center" valign="middle"  style="background:#ffffff;"><%=productContainers[iii].get("store_count",0f)%><%=productContainers[iii].getString("unit_name")%></td>
        <td width="13%" align="center" valign="middle"  style="background:#ffffff;"><%=productContainers[iii].getString("store_station")%></td>
        <td width="13%" align="center" valign="middle"  style="background:#ffffff;">&nbsp;</td>
        <td width="12%" align="center" valign="middle"  style="background:#ffffff;">&nbsp;</td>
        <td width="7%" align="center" valign="middle"  style="background:#ffffff;">&nbsp;</td>
      </tr>
	<%
	}
	%>
    </table></td>
  </tr>
<%
}
%>
</table>
<br>

<%
}

%>



</div>
	
	</td>
  </tr>
</table>



<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="display:none">
  <tr>
    <td>
	
<div id="stat_content_print">  


<%
//计算总页数：先计算每个仓库需要打印多少页，然后把总和加起来
//float totalPageHeight = storageContainers.length * (storageNameHeight+tableTitleHeight) + totalProductNameHeight;
//float totalPage = ((totalPageHeight + onePageHeight) - 1) / onePageHeight;
float totalPage = 0;
for (int i_page=0; i_page<storageContainers.length; i_page++)
{
	DBRow rows[] = (DBRow[])storageContainers[i_page].get("stat_data",new Object());
	float totalOneStorageProductNameHeight = 0;
	
	for (int j_page=0; j_page<rows.length; j_page++)
	{
		totalOneStorageProductNameHeight += StringUtil.getFloat(rows[j_page].getString("pcc_height"));
	}
	
	totalPage += (int)( ((storageNameHeight + tableTitleHeight+totalOneStorageProductNameHeight) + onePageHeight) - 1 ) / (int)onePageHeight;
}



int curPage = 0;
curPage++;

float totalheight = 0;
int pi = 0;
int catalogSt = 0;//打印货架起始坐标
int productSt = 0;//打印商品起始坐标

boolean nowSplitPageFlag = false;
boolean pageHeadFlag = false;
boolean breakOutCatalog = false;

String preStorage = "";

for (; pi<storageContainers.length; pi++)
{
	productCatalogContainers = (DBRow[])storageContainers[pi].get("stat_data",new Object());
	String storage_name = storageContainers[pi].getString("storage_name");
	
	//商品溢出页面，需要翻页
	if (nowSplitPageFlag||(storage_name.equals(preStorage)==false&&preStorage.equals("")==false) )
	{
		out.println("<div class='NextPage'></div>");
		nowSplitPageFlag = false;
		totalheight = 0;//打印总高度清零
		
		pageHeadFlag = true;
	}
	
	//防止表头超溢出页面
	//计算的高度：仓库名字+表头+一条商品记录的高度（防止有些情况出现只打印了一个表头）
	if ( (totalheight+storageNameHeight + tableTitleHeight + StringUtil.getFloat(productCatalogContainers[0].getString("pcc_height")))>onePageHeight )
	{
		pi = pi - 1;
		
		//翻页
		nowSplitPageFlag = true;
		
		continue;
	}
	
	totalheight += storageNameHeight + tableTitleHeight;	
%> 
<table width="98%" border="0" cellspacing="0" cellpadding="0"  >
  <tr>
    <td width="23%" align="left" valign="middle" style="font-size:15px;font-weight:bold;font-family:'黑体';" >
	<%
	if (pi==0||pageHeadFlag)
	{
	%>
	<%=input_st_date%> 缺货通知单
	<%
	}
	else
	{
		out.println("&nbsp;");
	}
	%>
	
	</td>
    <td width="45%" align="center" valign="middle"  style="font-size:18px;font-weight:bold;font-family:'黑体';"><%=storage_name%></td>
    <td width="32%" align="right" valign="middle"  style="font-size:11px;font-family:Arial;padding-bottom:10px;">
	<%
	if (pi==0||pageHeadFlag)
	{
	%>
	
	[<%=curPage++%>/<%=(int)totalPage%>] [<%=printDate%>] [<%=printer%>]
	
	<%
		pageHeadFlag = false;
	}
	else
	{
		out.println("&nbsp;");
	}
	%>	</td>
  </tr>
</table>

<table width="98%" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
  <tr>
    <td align="left" valign="middle"  style="font-size:12px;font-family:Arial;padding-left:8px;background:#000000;color:#FFFFFF;">商品分类</td>
    <td bgcolor="#FFFFFF"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">

      <tr>
        <td width="36%" align="left" valign="middle"  style="font-weight:bold;background:#ffffff;font-size:12px;font-family:Arial;background:#000000;color:#FFFFFF;border-left:1px #ffffff solid;">商品名称</td>
        <td width="19%" align="center" valign="middle"  style="font-weight:bold;background:#ffffff;background:#000000;color:#FFFFFF;border-left:1px #ffffff solid;">库存</td>
        <td width="13%" align="center" valign="middle"  style="font-weight:bold;background:#ffffff;background:#000000;color:#FFFFFF;border-left:1px #ffffff solid;">存放位置</td>
        <td width="13%" align="center" valign="middle"  style="font-weight:bold;background:#ffffff;background:#000000;color:#FFFFFF;border-left:1px #ffffff solid;">&nbsp;</td>
        <td width="12%" align="center" valign="middle"  style="font-weight:bold;background:#ffffff;background:#000000;color:#FFFFFF;border-left:1px #ffffff solid;">&nbsp;</td>
        <td width="7%" align="center" valign="middle"  style="font-weight:bold;background:#ffffff;background:#000000;color:#FFFFFF;border-left:1px #ffffff solid;">&nbsp;</td>
      </tr>

    </table></td>
  </tr>
<%
for (int pii=catalogSt; pii<productCatalogContainers.length; pii++)
{
	if (breakOutCatalog)
	{
		breakOutCatalog = false;
		break;
	}

	//判断是否需要换页
	if ( (totalheight+productNameHeight)>onePageHeight )
	{
		pi = pi - 1;//重复再打一次当前仓库
		catalogSt = pii;//换页后，从当前记录开始打印
			
		nowSplitPageFlag = true;
		
		break;
	}
	else
	{
		catalogSt = 0;
	}
		
	productContainers = (DBRow[])productCatalogContainers[pii].get("products",new Object());
%>
  <tr>
    <td width="18%" align="left" valign="middle" bgcolor="#eeeeee" style="font-size:8px;font-family:Arial;padding-left:1px;">
	  <%=productCatalogContainers[pii].getString("catalog_name")%></td>
    <td width="82%" bgcolor="#FFFFFF"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
      
	<%
	for (int iii=productSt; iii<productContainers.length; iii++)
	{
		//判断是否需要换页
		if ( (totalheight+productNameHeight)>onePageHeight )
		{
			pi = pi - 1;//重复再打一次当前仓库
			catalogSt = pii;//换页后，从当前记录开始打印
			productSt = iii;//换页后，从当前记录开始打印

			//因为需要进入上一个循环才能网上退出一个循环，所以必须pii-1
			pii--;
			
			breakOutCatalog = true;
			nowSplitPageFlag = true;
			
			break;
		}
		else
		{
			catalogSt = 0;
			productSt = 0;
		}
	
		totalheight += productNameHeight;
	
	%>
      <tr>
        <td width="36%" align="left" valign="middle"  style="background:#ffffff;font-size:12px;font-family:Arial;padding-left:1px;"><%=productContainers[iii].getString("p_name")%></td>
        <td width="19%" align="center" valign="middle"  style="background:#ffffff;"><%=productContainers[iii].get("store_count",0f)%><%=productContainers[iii].getString("unit_name")%></td>
        <td width="13%" align="center" valign="middle"  style="background:#ffffff;"><%=productContainers[iii].getString("store_station")%></td>
        <td width="13%" align="center" valign="middle"  style="background:#ffffff;">&nbsp;</td>
        <td width="12%" align="center" valign="middle"  style="background:#ffffff;">&nbsp;</td>
        <td width="7%" align="center" valign="middle"  style="background:#ffffff;">&nbsp;</td>
      </tr>
	<%
	}
	%>
    </table></td>
  </tr>
<%
}
%>
</table>

<%

preStorage = storage_name;

}
%>



</div>
	
	</td>
  </tr>
</table>

<br>
<br>
</body>
</html>
<%
//long ent = System.currentTimeMillis();
//System.out.println("Leaving Page time:"+(ent-stt));
%>
