<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String printArea = StringUtil.getString(request,"printArea");

//打印区域
float mm_pix = 3.78f;
float print_range_width = 200f*mm_pix;
float print_range_height = 285f*mm_pix;
float print_range_left = 5*mm_pix;
float print_range_top = 10*mm_pix;

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>打印报表</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<style media=print>  
.noPrint{display:none;}<!--用本样式在打印时隐藏非打印项目-->  
</style>

<style>
#container
{
	width:<%=print_range_width%>px;


	
	left:<%=print_range_left%>px;
	top:<%=print_range_top%>px;
	
	font-weight:normal;
	border:0px #FF0000 solid;
	overflow:hidden;
}

.NextPage{
page-break-before:always;
}
</style>



<script>
function printInit()
{
	xprint.printing.header = ""; //页眉 
	xprint.printing.footer = ""; //页脚
	xprint.printing.portrait = true; //控制横打还是竖打，true为竖打，false为横打
	xprint.printing.leftMargin = 0; //左边距 
	xprint.printing.topMargin = 0; //上边距 
	xprint.printing.rightMargin = 0; //右边距  
	xprint.printing.bottomMargin = 0; //下边距 
	xprint.printing.portrait = true;
}

function printBef()
{
	
}

function printAft()
{
	
}

function doPrint()
{
	printBef();
	xprint.printing.printer="Adobe PDF";
	//xprint.printing.printer="Microsoft Office Document Image Writer"
	
	//xprint.printing.printer="Brother MFC-7420 Printer";	
	xprint.printing.Print(false); //直接打印，true:弹出选择打印机窗口,false:直接打印  
	printAft(); 
}

function onLoadPrintContent()
{
	$("#container").html(opener.document.getElementById("<%=printArea%>").innerHTML);
}

if (!$.browser.msie)
{
	alert("请使用IE打印！");
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="printInit();onLoadPrintContent();" >
<object id="xprint" style="display:none"
  classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814"
  codebase="http://www.1you.com/paypal/scriptx.cab#Version=6,1,431,2">
</object>

<div class="noPrint">
<table width="100%" border="0" align="center" cellpadding="8" cellspacing="0" >
  <tr>
    <td width="21%" align="left" valign="middle">
      <input type="button" name="Submit" value="打 印" class="long-button-print" onClick="doPrint()"></td>
  </tr>
</table>
</div>

<div id="container">
</div>


</body>
</html>
