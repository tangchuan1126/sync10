<%@page import="com.cwc.app.key.ControlTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../include.jsp"%> 
<%@ page import="com.cwc.authentication.AdminAuthenCenter"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>无标题文档</title>
<style type="text/css">a {blr:expression(this.onFocus=this.blur())}</style>
 
<LINK href="navi.css" rel="stylesheet" type="text/css">
<LINK href="ht.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>		
			
<SCRIPT language="javascript" type="text/javascript" >
					
// JavaScript Document
var originalClassName;
var originalMenuClassName;
var lastSelectedMenu = null;

function highlightCategoryBar(sender,verb)
{

	if(verb == "over")
	{
		sender.className = "navi_Category_Selected";
	}
	else
	{
		sender.className = "navi_Category_Unselected";
	}
}
function switchCategoryBar(i)
{
	var obj = document.getElementById("menu_"+i);
	
	if(obj.style.display == "")
	{
		obj.style.display = "none";
	}
	else
	{
		obj.style.display = "";
	}
}

function highlightMenuBar(sender,verb)
{
	if(verb == "over")
	{
		originalMenuClassName = sender.className;
		sender.className = "navi_Item_Selected";
	}
	else
	{
		if(lastSelectedMenu == sender)
		{
			sender.className = "navi_Item_Selected";
		}
		else
		{
			sender.className = "navi_Item_Unselected";
		}
	}
}
function switchMenuBar(sender)
{
	if(lastSelectedMenu != null)
	{
		lastSelectedMenu.className = "navi_Item_Unselected";
	}
	sender.className = "navi_Item_Selected";
	lastSelectedMenu = sender;
}									



var t=null;
function ajaxLoadOrderTasksCount()
{
	var para = "1=1";

	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/getOrderTasksCountAjax.action',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,
		
		beforeSend:function(request){
		},
		
		error: function(){
			//$("#orderTasksCount").html("(?)");
		},
		
		success: function(html){
			if (html!="error")
			{
				$("#orderTasksCount").html("("+html+")");
			}
		}
	});

	clearTimeout(t);
	monitorTraceOrdersTimer = setTimeout("ajaxLoadOrderTasksCount()",1000*60*5);
}


</SCRIPT>
									
<STYLE>
BODY 
{ 
SCROLLBAR-FACE-COLOR: #eeeeee; SCROLLBAR-HIGHLIGHT-COLOR: #ffffff; SCROLLBAR-SHADOW-COLOR: #ffffff; SCROLLBAR-ARROW-COLOR: #c4c8d2; scrollbar-3d-light-color: #ffffff; scrollbar-dark-shadow-color: #ffffff;
-moz-user-select: none; 
}
.navi_Category_Selected
{
background:url(imgs/sideNavCategorySelectedBg.gif);
font-weight:normal;
color:#AA0000;
}

.navi_Category_Unselected
{
background:url(imgs/sideNavCategoryBg.gif);
font-weight:normal;
color:#444444;
}
</STYLE>

</head>									
<body  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  oncontextmenu="window.event.returnValue=false"   onselectstart="event.returnValue=false"   ondragstart="window.event.returnValue=false"   onsource="event.returnValue=false"   onLoad="ajaxLoadOrderTasksCount()">

<DIV id="oMTData"   style="height:100%; overflow:auto; overflow-x:hidden;">

<%
	DBRow fatherNav[] = adminMgr.getControlTreeByParentidOrderBySort(0l,ControlTypeKey.Left);
DBRow sonNav[] = null;
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);			

for (int i=0; i<fatherNav.length; i++)
{
	try
	{
		AdminAuthenCenter.isPermitPage(fatherNav[i].getString("link"),adminLoggerBean.getAdgid(),adminLoggerBean.getAdid());
	}
	catch (com.cwc.exception.OperationNotPermitException e)
	{
		continue;
	}

	sonNav = adminMgr.getControlTreeByParentidOrderBySort(fatherNav[i].get("id",0l),ControlTypeKey.Left);
%>
 
<TABLE width="100%" cellpadding="0" cellspacing="0" border="0" >
	<TR class="navi_Category_Unselected" onClick="switchCategoryBar('<%=i%>');" onMouseOver="highlightCategoryBar(this,'over');"
		onMouseOut="highlightCategoryBar(this,'out');" style="cursor:pointer">
		<TD width="32" align="center">
			<IMG src="imgs/group_arrow.gif" width="19" height="18" align="absMiddle">		</TD>
		<TD width="110" class="navi_Category_Item"><%=fatherNav[i].getString("title")%></TD>
		<TD width="24" align="center">
			<DIV>
				<IMG src="imgs/group_downarrow.gif" width="15" height="15" align="absMiddle">			</DIV>
		</TD> 
	</TR>
	<TR>
		<TD colspan="3">
			<DIV style="DISPLAY:<%=fatherNav[i].getString("title").indexOf("订单管理")>=0?"":"none"%>" id="menu_<%=i%>">
				<TABLE width="100%" cellpadding="3" cellspacing="0" border="0">
				
				
<%
for (int j=0; j<sonNav.length; j++)
{
	try
	{
		AdminAuthenCenter.isPermitPage(sonNav[j].getString("link"),adminLoggerBean.getAdgid(),adminLoggerBean.getAdid());
	}
	catch (com.cwc.exception.OperationNotPermitException e)
	{
		continue;
	}
%>				
					<TR class="navi_Item_Unselected" onClick="return switchMenuBar(this);" onMouseOver="return highlightMenuBar(this,'over');"
						onMouseOut="return highlightMenuBar(this,'out');" >
						<TD width="3" bgcolor="#d3eb9a"></TD>
						<TD width="24" align="center" valign="middle">
							<IMG src="imgs/item3.gif" align="absMiddle">
						</TD>
						<TD class="navi_ItemName">
							<a href="<%=sonNav[j].getString("link")%>" id="Adminleft1_A10"  title="<%=sonNav[j].getString("title")%>" class="menuItem" target="main" onFocus=this.blur() style="display:block;height:20px;" onclick="window.parent.document.title='<%=sonNav[j].getString("title")%>'" >
								<%=sonNav[j].getString("title")%> 	
								
								<%
								if (sonNav[j].getString("title").equals("订单任务"))
								{
								%>
								<span id="orderTasksCount" style="font-family: Arial;font-size: 12px;font-weight:normal;color:#FF0000"></span>
								<%
								}
								%>
														
							</a>					
						</TD>
					</TR>
<%
}
%>					
					
					
				</TABLE>
				
				

			</DIV>
		</TD>
	</TR>
</TABLE>

<%
}
%>


<table width="100%" height="3" border="0" cellpadding="3" cellspacing="0">
  <tr>
    <td bgcolor="#07ADFF"></td>
  </tr>
</table>
</div>


</body>
</html>
