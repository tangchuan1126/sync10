<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long round_invoice_id = StringUtil.getLong(request,"round_invoice_id");
	
	DBRow roundInvoice = invoiceMgrZJ.getDetailRoundInvoiceById(round_invoice_id);
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加发票模板样式</title>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script type="text/javascript">
  function modRoundInvoiceInfo(){
  	  var f = document.addRoundInvoiceForm;
      
      parent.document.add_round_invoice_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/modRoundInvoice.action";
      parent.document.add_round_invoice_form.proMaterialChinese.value = f.proMaterialChinese.value;
      parent.document.add_round_invoice_form.proTextRfeChinese.value = f.proTextRfeChinese.value;
      parent.document.add_round_invoice_form.proTextDogChinese.value = f.proTextDogChinese.value;
      parent.document.add_round_invoice_form.round_invoice_dog.value = f.proTextDog.value;
      parent.document.add_round_invoice_form.round_invoice_ref.value = f.proTextRfe.value;
      parent.document.add_round_invoice_form.round_invoice_uv.value = f.proUv.value;
      parent.document.add_round_invoice_form.round_invoice_tv.value = f.proTv.value;
      parent.document.add_round_invoice_form.round_invoice_id.value = <%=round_invoice_id%>;
      parent.document.add_round_invoice_form.round_invoice_hs_code.value = f.prohs.value;
      parent.document.add_round_invoice_form.round_invoice_material.value = f.proMaterial.value;
      parent.document.add_round_invoice_form.submit();
  }
</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }

</style>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
			<form name="addRoundInvoiceForm" method="post" action="" >
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td width="80px;">&nbsp;</td>
					    <td style=" font-size:14px;  font-weight:bolder" align="left">英文</td>
					    <td style=" font-size:14px;  font-weight:bold" align="left">中文</td>
					  </tr>
					  <tr>
					    <td height="60px;" align="right" style=" font-size:14px; color:#00C; font-weight:bold">货物描述：</td>
					    <td align="left" valign="middle"><textarea name='proTextDog' id='proTextDog' style='width:220px;height:50px;'><%=roundInvoice.getString("round_invoice_dog")%></textarea></td>
					    <td align="left" valign="middle"><textarea name='proTextDogChinese' id='proTextDogChinese' style='width:220px;height:50px;'><%=roundInvoice.getString("round_invoice_dog_chinese")%></textarea></td>
					  </tr>
					  <tr>
					    <td height="60px;" align="right" style=" font-size:14px; color:#00C; font-weight:bold">出口描述：</td>
					    <td align="left" valign="middle"><textarea name='proTextRfe' id='proTextRfe' style='width:220px;height:50px;'><%=roundInvoice.getString("round_invoice_ref")%></textarea></td>
					    <td align="left" valign="middle"><textarea name='proTextRfeChinese' id='proTextRfeChinese' style='width:220px;height:50px;'><%=roundInvoice.getString("round_invoice_rfe_chinese")%></textarea></td>
					  </tr>
					  <tr>
					    <td height="40px;" align="right" style=" font-size:14px; color:#00C; font-weight:bold">hs：</td>
					    <td align="left" valign="middle"><input name='prohs' type='text' id='prohs'  style='width:220px;' value="<%=roundInvoice.getString("round_invoice_hs_code")%>"/></td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td height="40px;" align="right" style=" font-size:14px; color:#00C; font-weight:bold">材质：</td>
					     <td align="left" valign="middle"><input name='proMaterial' type='text' id='proMaterial'  style='width:220px;'  value="<%=roundInvoice.getString("round_invoice_material") %>" /></td>
					    <td align="left" valign="middle"><input name='proMaterialChinese' type='text' id='proMaterialChinese'  style='width:220px;' value="<%=roundInvoice.getString("round_invoice_material_chinese") %>" /></td>
					  </tr>
					  <tr>
					    <td height="40px;" align="right" style=" font-size:14px; color:#00C; font-weight:bold">单位价值：</td>
					    <td align="left" valign="middle"><input name='proUv' type='text' id='proUv'  style='width:220px;' value="<%=roundInvoice.getString("round_invoice_uv")%>" /></td>
					    <td>&nbsp;</td>
					  </tr>
					  <tr>
					    <td height="40px;" align="right" style=" font-size:14px; color:#00C; font-weight:bold">总价值：</td>
					    <td align="left" valign="middle"><input name='proTv' type='text' id='proTv'  style='width:220px;' value="<%=roundInvoice.getString("round_invoice_tv")%>" /></td>
					    <td>&nbsp;</td>
					  </tr>
					</table>
			   </form>		
		    </td>
          </tr>
       </table>
     </td>
  </tr>
  <tr>
  	<td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存" class="normal-green" onClick="modRoundInvoiceInfo();">
     <input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close();">
	</td>
  </tr>
</table> 
</body>
</html>
