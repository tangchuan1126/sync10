<%@ page contentType="text/html;charset=utf-8"%>
<%@taglib prefix="ss" uri="/WEB-INF/tag-lib/spring.tld"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript" src="../../common.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../comm.css" type="text/css"></link>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<script language="JavaScript" type="text/JavaScript">
function delDelivererInfo(di_id)
{
	if (confirm("确认删除？"))
	{
		document.temp_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/delDelivererInfo.action";
		document.temp_form.di_id.value = di_id;
		document.temp_form.submit();
	}
}

function showDiv(divId)
{
	if (document.getElementById(divId).style.display=="none")
	{
		document.getElementById(divId).style.display="block";
	}else{
        document.getElementById(divId).style.display="none";
	}

}
function addInvoice(id,title)
	{
		tb_show(''+title+'-仓库增加发货人信息','add_invoice.html?ps_id='+id+'&TB_iframe=true&height=400&width=380',false);
	
	}
function updateInvoice(di_id,title)
{
	tb_show(''+title+'-仓库修改发货人信息','update_invoice.html?di_id='+di_id+'&TB_iframe=true&height=400&width=380',false);			
}
function closeWin()
{
	tb_remove();
}

</script> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 基础数据管理 »   出口发件人信息</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="id" >
</form>
<form  method="post" name="temp_form" >
   <input type="hidden" name="di_id">
</form>

<form  method="post" name="add_invoice_form" >
   <input type="hidden" name="di_id">
   <input type="hidden" name="CompanyName">
   <input type="hidden" name="AddressLine1">
   <input type="hidden" name="AddressLine2">
   <input type="hidden" name="AddressLine3">
   <input type="hidden" name="City">
   <input type="hidden" name="DivisionCode">
   <input type="hidden" name="PostalCode">
   <input type="hidden" name="CountryCode">
   <input type="hidden" name="CountryName">
   <input type="hidden" name="PhoneNumber">
   <input type="hidden" name="ps_id"/>
   <input type="hidden" name="backurl" value="/administrator/invoice/deliverer_info.html"/>
</form>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
      <tr> 
        <th  style="vertical-align: center;text-align: center;" class="left-title">所属仓库</th>
        <th width="51%"  style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
      </tr>
<%
	DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();
	for ( int i=0; i<treeRows.length; i++ )
	{
%>
    <tr > 
     <td width="33%" height="60" valign="middle">
     <a href="javascript:void(0);" onclick="showDiv('div<%=i%>')">
     <img src="../js/images/next.gif" border="0" /> 
        <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
      				<%
		if (treeRows[i].get("parentid",0) == 0)
		{
			out.println("<span style='font-size:14px;font-weight:bold'>"+treeRows[i].getString("title")+"</span>");
		}
		else
		{
			out.println(treeRows[i].getString("title"));
		}
		%>
		</a>
		</td>
      <td align="right" valign="middle">
     
        <input name="Submit" type="button" class="long-long-button-add" onClick="addInvoice(<%=treeRows[i].getString("id")%>,'<%=treeRows[i].getString("title") %>')" value="增加发货人">
		</td>
    </tr>
    <tr>
      <td colspan="5">
          <div id="div<%=i %>" style="display:none">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
<thead>
  <tr> 
     <!-- <th width="4%">&nbsp;<div style="display:none">ID</div></th> --> 
      <th width="17%" style="vertical-align: center;text-align: center;" class="right-title">公司名称</th>
      <th width="18%" style="vertical-align: center;text-align: center;" class="right-title">地址</th>
      <th width="6%" style="vertical-align: center;text-align: center;" class="right-title">州/省</th>
      <th width="4%" style="vertical-align: center;text-align: center;" class="right-title">邮编ZIP</th>
      <th width="5%" style="vertical-align: center;text-align: center;" class="right-title">国家代码</th>
      <th width="6%" style="vertical-align: center;text-align: center;" class="right-title">国家名</th>
      <th width="8%" style="vertical-align: center;text-align: center;" class="right-title">联系电话</th>
      <th width="7%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
  </tr>
</thead>
<%
DBRow rows[] =storageSortMgr.getDetailStorageSortById(treeRows[i].get("id",0l));
DBRow storage = invoiceTypeMgr.getStorageCategory(treeRows[i].get("id",0l));
for (int a=0; a<rows.length; a++)
{
%>
      <tr>
      <!-- <td height="90" align="center" valign="middle"><div style="display:none"><%= rows[a].getString("di_id") %></div></td>-->
        <td height="70" align="left"" valign="middle"><img src="../imgs/user.png"></img>&nbsp;&nbsp;&nbsp;<%=rows[a].getString("CompanyName")%></td>
        <td align="left" valign="middle" ><%=rows[a].getString("AddressLine1")+"<br>"+rows[a].getString("AddressLine2")+"<br>"+rows[a].getString("AddressLine3")+"<br>"+rows[a].getString("City")%></td>
        <td align="center" valign="middle"><%=rows[a].getString("DivisionCode")%>&nbsp;</td>
        <td align="center" valign="middle"><%=rows[a].getString("PostalCode")%>&nbsp;</td>
        <td align="center" valign="middle"><%=rows[a].getString("CountryCode")%>&nbsp;</td>
        <td align="center" valign="middle"><%=rows[a].getString("CountryName")%>&nbsp;</td>
        <td align="center" valign="middle"><%=rows[a].getString("PhoneNumber")%>&nbsp;</td>
        <!-- <td align="center" valign="middle"><%=rows[a].getString("title") %>&nbsp;</td> -->
        <td align="center" valign="middle">
        	<input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="updateInvoice(<%=rows[a].getString("di_id")%>,'<%=storage.getString("title") %>')">
        <br>
		<br>
        <input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delDelivererInfo(<%=rows[a].getString("di_id")%>)"></td>
    </tr>
<%
}
%>
</table>
</div>
   </td>
     </tr>
        <%	
}
%>
</table>
</body>
</html>
