<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long invoice_id = StringUtil.getLong(request,"invoice_id");
	
	DBRow[] roundInvoices = invoiceMgrZJ.getAllRoundInvoicesByInvoiceId(invoice_id);	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>模板样式列表</title>
</head>

<body>
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		<tr>
			<th width="16%" nowrap="nowrap" style="vertical-align: center;text-align: center;" class="right-title">货物描述</th>
			<th width="22%" nowrap="nowrap" style="vertical-align: center;text-align: center;" class="right-title">出口理由</th>
			<th width="22%" nowrap="nowrap" style="vertical-align: center;text-align: center;" class="right-title">HSCODE</th>
			<th width="10%" nowrap="nowrap" style="vertical-align: center;text-align: center;" class="right-title">材质</th>
			<th width="10%" nowrap="nowrap" style="vertical-align: center;text-align: center;" class="right-title">单位价值(US $)</th>
			<th width="10%" nowrap="nowrap" style="vertical-align: center;text-align: center;" class="right-title">总价值 (US $)</th>
			<th width="10%" nowrap="nowrap" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		</tr>
		<%
			for(int i=0;i<roundInvoices.length;i++)
			{
		%>
		<tr>
			<td align="center" valign="middle"><%=roundInvoices[i].getString("round_invoice_dog")%></td>
			<td align="center" valign="middle"><%=roundInvoices[i].getString("round_invoice_ref")%></td>
			<td align="center" valign="middle"><%=roundInvoices[i].getString("round_invoice_hs_code")%>&nbsp;</td>
			<td align="center" valign="middle"><%=roundInvoices[i].getString("round_invoice_material")%>&nbsp;</td>
			<td align="center" valign="middle"><%=roundInvoices[i].getString("round_invoice_uv")%>&nbsp;</td>
			<td align="center" valign="middle"><%=roundInvoices[i].getString("round_invoice_tv")%>&nbsp;</td>
			<td align="center" valign="middle">
				
				<input name="Submit32" type="button" class="short-short-button-mod" value="修改" onClick="modRoundInvoice(<%=roundInvoices[i].get("round_invoice_id",0l)%>)">
				&nbsp;&nbsp;
				<input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delRoundInvoice(<%=roundInvoices[i].getString("invoice_template_id")%>,<%=roundInvoices[i].get("round_invoice_id",0l)%>)">
			</td>
		</tr>
		<%
			}
		%>
	</table>
</body>
</html>