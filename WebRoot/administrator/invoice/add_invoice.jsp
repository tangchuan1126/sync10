<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long ps_id = StringUtil.getLong(request,"ps_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加出口发件人信息</title>
		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script type="text/javascript">

function addinvoiceInfo()
{
	 if(document.add_form.CompanyNameText.value== "")
	{
		alert("请填写公司名称");
	}
	else
	{
		parent.document.add_invoice_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/addStorageSort.action";
		parent.document.add_invoice_form.CompanyName.value=document.add_form.CompanyNameText.value;
		parent.document.add_invoice_form.AddressLine1.value=document.add_form.AddressLine1Text.value;
		parent.document.add_invoice_form.AddressLine2.value=document.add_form.AddressLine2Text.value; 
		parent.document.add_invoice_form.AddressLine3.value=document.add_form.AddressLine3Text.value;
		parent.document.add_invoice_form.City.value=document.add_form.CityText.value;
		parent.document.add_invoice_form.DivisionCode.value=document.add_form.DivisionCodeText.value;
		parent.document.add_invoice_form.PostalCode.value=document.add_form.PostalCodeText.value;
		parent.document.add_invoice_form.CountryCode.value=document.add_form.CountryCodeText.value;
		parent.document.add_invoice_form.CountryName.value=document.add_form.CountryNameText.value;
		parent.document.add_invoice_form.PhoneNumber.value=document.add_form.PhoneNumberText.value;	
		parent.document.add_invoice_form.ps_id.value = <%=ps_id%>;
		parent.document.add_invoice_form.submit();
	}
}
</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
</style>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_form" method="post" action="" >
  <table width="98%" border="0" cellspacing="5" cellpadding="2">
    <tr>
     	<td align="right" valign="middle" class="STYLE1 STYLE2">CompanyName:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="left" valign="middle">
			<input name='CompanyNameText' type='text' id='CompanyNameText'  style="width:180px;"/>
		</td>
    </tr>
    <tr>
         <td align="right" valign="middle" class="STYLE1 STYLE2">AddressLine1:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="left" valign="middle">
			<input name='AddressLine1Text' type='text' id='AddressLine1Text' style="width:180px;" />
		</td>
    </tr>
    <tr>
       <td align="right" valign="middle" class="STYLE1 STYLE2">AddressLine2:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="left" valign="middle">
			<input name='AddressLine2Text' type='text' id='AddressLine2Text' style="width:180px;" />
		</td>
    </tr>
    <tr>
      <td align="right" valign="middle" class="STYLE1 STYLE2">AddressLine3:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="left" valign="middle">
			<input name='AddressLine3Text' type='text' id='AddressLine3Text' style="width:180px;" />
		</td>
       </tr>
    </tr>
    <tr>
      <td align="right" valign="middle" class="STYLE1 STYLE2">City:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="left" valign="middle">
			<input name='CityText' type='text' id='CityText' style="width:180px;" />
		</td>
    </tr>
    <tr>
    	<td align="right" valign="middle" class="STYLE1 STYLE2">DivisionCode:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="left" valign="middle">
			<input name='DivisionCodeText' type='text' id='DivisionCodeText' style="width:180px;" />
		</td>
    </tr>
    <tr>
		<td align="right" valign="middle" class="STYLE1 STYLE2">PostalCode:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="left" valign="middle">
			<input name='PostalCodeText' type='text' id='PostalCodeText' style="width:180px;" />
		</td>
	</tr>
	<tr>
		<td align="right" valign="middle" class="STYLE1 STYLE2">CountryCode:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="left" valign="middle">
			<input name='CountryCodeText' type='text' id='CountryCodeText' style="width:180px;" />
		</td>
	</tr>
	<tr>
		<td align="right" valign="middle" class="STYLE1 STYLE2">CountryName:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="left" valign="middle">
			<input name='CountryNameText' type='text' id='CountryNameText' style="width:180px;" />
		</td>
	</tr>
	<tr>
		<td align="right" valign="middle" class="STYLE1 STYLE2">PhoneNumber:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="left" valign="middle">
			<input name='PhoneNumberText' type='text' id='PhoneNumberText' style="width:180px;" />
		</td>
	</tr>
  </table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存" class="normal-green" onClick="addinvoiceInfo();">
	<input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog && $.artDialog.close()">
	</td>
  </tr>
</table> 
</body>
</html>
