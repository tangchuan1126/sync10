<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long invoiceId = StringUtil.getLong(request,"invoiceId");
	DBRow row = invoiceTypeMgr.getDetailInvoiceTemplateById(invoiceId);
	
	DBRow[] roundInvoices = invoiceMgrZJ.getAllRoundInvoicesByInvoiceId(invoiceId);
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改发票模板</title>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script type="text/javascript">
  function modInvoiceInfo(){
  	  var f = document.modInvoiceForm;
      if(f.proTName.value == "")
      {
      	alert("请输入模板名称");
      }
      else
      {
      	parent.document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/modInvoiceTemplateTJH.action";
      	parent.document.add_form.invoice_id.value = <%=invoiceId %>;
      	parent.document.add_form.dog.value = f.proTextDog.value;
      	parent.document.add_form.rfe.value = f.proTextRfe.value;
      	parent.document.add_form.uv.value = f.proUv.value;
      	parent.document.add_form.tv.value = f.proTv.value;
      	parent.document.add_form.di_id.value = f.di_id.value;
      	parent.document.add_form.ps_id.value = f.ps_id.value;
      	parent.document.add_form.t_name.value = f.proTName.value;
      	parent.document.add_form.is_round_invoice.value = f.is_round_invoice.value;
      	parent.document.add_form.hs_code.value = f.proHS.value;
      	parent.document.add_form.material.value = f.proMaterial.value;
      	parent.document.add_form.submit();
      }
  }
</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="modInvoiceForm" method="post" action="" >
<input type="hidden" name="ps_id" value="<%=row.get("ps_id",0l) %>"/>
  <table width="98%" border="0" cellspacing="5" cellpadding="2">
    <tr>
      		<td align="left" valign="middle" class="STYLE1 STYLE2">模板名称</td>
            <td align="left" valign="middle">
	           <input type="text" id="proTName" name="proTName" style='width:180px;' value="<%=row.getString("t_name") %>">
            </td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="STYLE1 STYLE2">发件人</td>
          	<td align="left" valign="middle">
          		<select name="di_id">
          			<option value="0">随机</option>
          			<%
          				DBRow invoice [] = storageSortMgr.getDetailStorageSortById(row.get("ps_id",0l));
          				for(int j=0;j<invoice.length;j++)
          				{
          			 %>
          			 <option value="<%=invoice[j].get("di_id",0l)%>" <%=invoice[j].get("di_id",0l) == row.get("di_id",0l)?"selected":"" %>><%=invoice[j].getString("CompanyName") %></option>
          			 <%} %>
          		</select>
          	</td>
    </tr>
    <%
    	if(roundInvoices.length>0)
    	{
    %>
    	<tr>
    		<td align="left" valign="middle" class="STYLE1 STYLE2">内容随机</td>
    		<td align="left" valign="middle" class="STYLE1 STYLE2">
    			<input type="radio" name="radio_round_invoice" value="1" <%=row.get("is_round_invoice",0)==1?"checked=\"checked\"":""%> onclick='$("#is_round_invoice").val(this.value)'/>是
    			<input type="radio" name="radio_round_invoice" value="2" <%=row.get("is_round_invoice",0)==2?"checked=\"checked\"":""%> onclick='$("#is_round_invoice").val(this.value)'/>否
    			<input name="is_round_invoice" id="is_round_invoice" type="hidden" value="<%=row.get("is_round_invoice",0)%>"/>
    		</td>
    	</tr>
    	<tr>
    <%
    	}
    	else
    	{
    %>
    	<tr>
    		<input name="is_round_invoice" type="hidden" value="2"/>
    <%
    	}
    %>
    
       <td align="left" valign="middle" class="STYLE1 STYLE2">货物描述</td>
       <td align="left" valign="middle"><textarea name='proTextDog' id='proTextDog' style='width:300px;height:80px;'><%=row.getString("dog") %></textarea></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="STYLE1 STYLE2">出口描述</td>
            <td align="left" valign="middle"><textarea name='proTextRfe' id='proTextRfe' style='width:300px;height:80px;'><%=row.getString("rfe") %></textarea></td>
       </tr>
    </tr>
    <tr>
      <td align="left" valign="middle" class="STYLE1 STYLE2">HS</td>
            <td align="left" valign="middle">
                  <input name='proHS' type='text' id='proHS'  style='width:180px;' value="<%=row.getString("hs_code") %>"/>
            </td>
    </tr>
    <tr>
    	<td align="left" valign="middle" class="STYLE1 STYLE2">材质</td>
        <td align="left" valign="middle"><input name='proMaterial' type='text' id='proMaterial' value="<%=row.getString("material")%>"  style='width:180px;'/></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="STYLE1 STYLE2">单位价值</td>
            <td align="left" valign="middle">
                  <input name='proUv' type='text' id='proUv'  style='width:180px;' value="<%=row.getString("uv") %>"/>
            </td>
    </tr>
    <tr>
    	<td align="left" valign="middle" class="STYLE1 STYLE2">总价值</td>
        <td align="left" valign="middle"><input name='proTv' type='text' id='proTv' value="<%=row.getString("tv") %>" style='width:180px;'/></td>
    </tr>
  </table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存" class="normal-green" onClick="modInvoiceInfo();">
     <input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close();">
	</td>
  </tr>
</table> 
</body>
</html>
