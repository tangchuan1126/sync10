<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long ps_id = StringUtil.getLong(request,"ps_id");
	DBRow category = catalogMgr.getDetailProductStorageCatalogById(ps_id);
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=category.getString("title") %>仓库增加发票模板</title>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script type="text/javascript">
  function addInvoiceInfo(){
  	  var f = document.addInvoiceForm;
      if(f.t_name.value == "")
      {
      	alert("请输入模板名称");
      }
      else
      {
      	parent.document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/addInvoiceTemplateTJH.action";
      	parent.document.add_form.dog.value = f.proTextDog.value;
      	parent.document.add_form.rfe.value = f.proTextRfe.value;
      	parent.document.add_form.uv.value = f.proUv.value;
      	parent.document.add_form.tv.value = f.proTv.value;
      	parent.document.add_form.di_id.value = f.di_id.value;
      	parent.document.add_form.ps_id.value = <%=ps_id %>;
      	parent.document.add_form.t_name.value = f.t_name.value;
      	parent.document.add_form.hs_code.value = f.proHS.value;
      	parent.document.add_form.material.value = f.proMaterial.value;
      	parent.document.add_form.submit();
      }
  }
</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }

</style>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="addInvoiceForm" method="post" action="" >
  <table width="98%" border="0" cellspacing="5" cellpadding="2">
    <tr>
      		<td align="left" valign="middle" class="STYLE1 STYLE2">模板名称</td>
            <td align="left" valign="middle">
	            <input type="text" id="t_name" name="t_name" style='width:180px;'>
            </td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="STYLE1 STYLE2">发件人</td>
          	<td align="left" valign="middle">
          		<select name="di_id">
          			<option value="0">随机</option>
          			<%
          				DBRow invoice [] = storageSortMgr.getDetailStorageSortById(ps_id);
          				for(int j=0;j<invoice.length;j++)
          				{
          			 %>
          			 <option value="<%=invoice[j].getString("di_id")%>"><%=invoice[j].getString("CompanyName") %></option>
          			 <%} %>
          		</select>
          	</td>
    </tr>
    <tr>
       	<td align="left" valign="middle" class="STYLE1 STYLE2">货物描述</td>
       	<td align="left" valign="middle"><textarea name='proTextDog' id='proTextDog' style='width:300px;height:80px;'></textarea></td>
    </tr>
    <tr>
      	<td align="left" valign="middle" class="STYLE1 STYLE2">出口描述</td>
        <td align="left" valign="middle"><textarea name='proTextRfe' id='proTextRfe' style='width:300px;height:80px;'></textarea></td>
       </tr>
    </tr>
    <tr>
      <td align="left" valign="middle" class="STYLE1 STYLE2">HS</td>
            <td align="left" valign="middle">
                  <input name='proHS' type='text' id='proHS'  style='width:180px;'/>
            </td>
    </tr>
    <tr>
    	<td align="left" valign="middle" class="STYLE1 STYLE2">材质</td>
        <td align="left" valign="middle"><input name='proMaterial' type='text' id='proMaterial'  style='width:180px;'/></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="STYLE1 STYLE2">单位价值</td>
            <td align="left" valign="middle">
                  <input name='proUv' type='text' id='proUv'  style='width:180px;'/>
            </td>
    </tr>
    <tr>
    	<td align="left" valign="middle" class="STYLE1 STYLE2">总价值</td>
        <td align="left" valign="middle"><input name='proTv' type='text' id='proTv'  style='width:180px;'/></td>
    </tr>
  </table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  	<td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存" class="normal-green" onClick="addInvoiceInfo();">
     <input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog && $.artDialog.close()">
	</td>
  </tr>
</table> 
</body>
</html>
