<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>发票管理</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />


<script type="text/javascript">
//加载tabs控件
jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
});
jQuery(function($){
	$("#tabs2").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
});
jQuery(function($){
	$("#tabs1").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
});

//形式发票模版
function updateInvoiceTemplate(invoiceId,title){
	$.artDialog.open("mod_invoice_template.html?invoiceId="+invoiceId, {title:"修改"+title+"-仓库的发票模板",width:'450px',height:'410px',fixed:true, lock: true,opacity: 0.3});
}
function addInvoiceTemplate(ps_id,title){
	$.artDialog.open("add_invoice_template.html?ps_id="+ps_id, {title:title+"-仓库增加发票模板",width:'450px',height:'410px',fixed:true, lock: true,opacity: 0.3});
}
function addRoundInvoice(invoiceId,t_name,title){
	$.artDialog.open("add_round_invoice.html?invoice_id="+invoiceId, {title:title+"-"+t_name+"-增加发票样式",width:'600px',height:'400px',fixed:true, lock: true,opacity: 0.3});
}

//形式发票模版子页面
function modRoundInvoice(round_invoice_id){
	$.artDialog.open("mod_round_invoice.html?round_invoice_id="+round_invoice_id, {title:"修改模板样式",width:'600px',height:'400px',fixed:true, lock: true,opacity: 0.3});
}
function delRoundInvoice(invoice_id,round_invoice_id){
	if(confirm("确定删除选定的模板样式？")){
		document.del_round_invoice_form.invoice_id.value = invoice_id;
		document.del_round_invoice_form.round_invoice_id.value = round_invoice_id;
		document.del_round_invoice_form.submit();
	}
}

//出口发信人信息
function addInvoice(id,title)
{
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/invoice/add_invoice.html?ps_id='+id; 
	 $.artDialog.open(uri , {title: '新增开发者',width:'420px',height:'400px', lock: true,opacity: 0.3,fixed: true});	
}
function updateInvoice(di_id,title)
{
  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/invoice/update_invoice.html?di_id='+di_id; 
  $.artDialog.open(uri , {title: '新增开发者',width:'420px',height:'400px', lock: true,opacity: 0.3,fixed: true});			
}

function delDelivererInfo(di_id)
{
	if (confirm("确认删除？")){
		document.temp_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/delDelivererInfo.action";
		document.temp_form.di_id.value = di_id;
		document.temp_form.submit();
	}
}


</script>

</head>
<body onload="onLoadInitZebraTable();">
   
<div style="border-bottom:2px #CCC solid; height:25px;">
	   <img width="17" height="12" align="absmiddle" alt="title" src="../imgs/page_title.gif">
	   <span style="font-size:14px; font-weight: bold;">基础数据管理》》发票样式管理</span>
</div>
<br/>
<div id="tabs">
	<ul>
		<li><a href="#usual_tools">形式发票模版</a></li>
		<li><a href="#advan_search">出口发件人信息</a></li>		 
	</ul>
	<!-- 形式发票模版 -->
	<div id="usual_tools">
		 <div id="tabs1">
		     <ul>
		        <%DBRow catalogMgrRows[] = catalogMgr.getProductDevStorageCatalogTree(); %>
		        <%for(int w=0;w<catalogMgrRows.length;w++){ %>
		            <li><a href="#<%=catalogMgrRows[w].getString("title")%>"><%=catalogMgrRows[w].getString("title") %></a></li>
		        <%} %>		
		     </ul>
		        <%for ( int k=0; k<catalogMgrRows.length; k++ ){%>
		          <div id="<%=catalogMgrRows[k].getString("title")%>">
		               <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
						   <input name="Submit3" type="button" class="long-long-button-add" onClick="addInvoiceTemplate(<%=catalogMgrRows[k].getString("id") %>,'<%=catalogMgrRows[k].getString("title") %>')" value="增加发票模板">
					   </div>
			           <br/>
						  <%
								DBRow [] rows = invoiceTypeMgr.getDetailInvoiceById(catalogMgrRows[k].get("id",0l));
								DBRow category = invoiceTypeMgr.getStorageCategory(catalogMgrRows[k].get("id",0l));
								for(int ii=0;ii<rows.length;ii++){%>
					    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
						  <tr> 
						      <th width="9%" style="vertical-align: center;text-align: center;" class="left-title">发件人</th>
						      <th width="9%" style="vertical-align: center;text-align: center;" class="right-title">模板名称</th>
						      <th width="14%" style="vertical-align: center;text-align: center;" class="right-title">货物描述</th>
						      <th width="14%" style="vertical-align: center;text-align: center;" class="right-title">出口理由</th>
						      <th width="13%" style="vertical-align: center;text-align: center;" class="right-title">HSCODE</th>
						      <th width="13%" style="vertical-align: center;text-align: center;" class="right-title">材质</th>
						      <th width="11%" style="vertical-align: center;text-align: center;" class="right-title">单位价值(US $)</th>
						      <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">总价值 (US $)</th>
						      <th width="6%" style="vertical-align: center;text-align: center;" class="right-title">操作样式</th>
						      <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
						  </tr> 
						  <% DBRow[] roundInvoices = invoiceMgrZJ.getAllRoundInvoicesByInvoiceId(rows[ii].get("invoice_id",0l)); %>					 
						      <%if(roundInvoices.length==0){ %>
						      <tr>
							      <td align="left" valign="middle" >
							  			<% if(rows[ii].get("di_id",0l) == 0){
												out.print("<img src='../imgs/user.png' />&nbsp;&nbsp;&nbsp;随机");
											}else{
								  				DBRow invoice = invoiceTypeMgr.getDetailInvoiceCompanyNameByDiId(rows[ii].get("di_id",0l));
								  				out.print("<img src='../imgs/user.png' />&nbsp;&nbsp;&nbsp;"+invoice.getString("CompanyName"));
								  			}%>
						  		 </td>
						  		 <td align="center" valign="middle" >
						        	<%=rows[ii].getString("t_name")%><br/>
						         </td>
						         <td>&nbsp;</td>
						         <td>&nbsp;</td>
						         <td>&nbsp;</td>
						         <td>&nbsp;</td>
						         <td>&nbsp;</td>
						         <td>&nbsp;</td>
						         <td>&nbsp;</td>
						         <td align="right" valign="middle">
							        <input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="updateInvoiceTemplate(<%=rows[ii].getString("invoice_id")%>,'<%=category.getString("title") %>')"><br><br>
							        <input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delDelivererInfo(<%=rows[ii].getString("invoice_id")%>)"><br><br>
							        <input name="Submit32" type="button" class="long-button-add" value="添加样式" onClick="addRoundInvoice(<%=rows[ii].get("invoice_id",0l)%>,'<%=rows[ii].getString("t_name")%>','<%=category.getString("title")%>')">
						         </td>
						      </tr>
						      <%}else{ %>  
						          <%for(int b=0;b<roundInvoices.length;b++){%>
								  <tr>
								  	 <%if(b==0){ %>
								  	 <td align="left" valign="middle" rowspan="<%= roundInvoices.length%>">
							  			<% if(rows[ii].get("di_id",0l) == 0){
												out.print("<img src='../imgs/user.png' />&nbsp;&nbsp;&nbsp;随机");
											}else{
								  				DBRow invoice = invoiceTypeMgr.getDetailInvoiceCompanyNameByDiId(rows[ii].get("di_id",0l));
								  				out.print("<img src='../imgs/user.png' />&nbsp;&nbsp;&nbsp;"+invoice.getString("CompanyName"));
								  			}%>
							  		 </td>
							  		 <td align="center" valign="middle" rowspan="<%= roundInvoices.length%>">
							        	<%=rows[ii].getString("t_name")%><br/>
							        	<div><%=roundInvoices[b].getString("round_invoice_dog_chinese")%></div>
							         </td>
							         <%} %>
							  		 <td align="center" valign="middle">
							  		 	<%=roundInvoices[b].getString("round_invoice_dog")%>
							  		 	<div><%=roundInvoices[b].getString("round_invoice_ref_chinese")%></div>
							  		 </td>
							  		 <td  align="center" valign="middle">
							  		 	<%=roundInvoices[b].getString("round_invoice_ref")%>
							  		 </td>
							  		 <td  align="center" valign="middle">
							  		 	<%=roundInvoices[b].getString("round_invoice_hs_code")%>
							  		 </td>
							  		 <td  align="center" valign="middle">
							  			 <%=roundInvoices[b].getString("round_invoice_material")%><br/>
							  			 <div><%=roundInvoices[b].getString("material_chinese_name")%></div>
									 </td>
							  		 <td  align="center" valign="middle">
							  			 <%=roundInvoices[b].getString("round_invoice_uv")%>
							  		 </td>
							  		 <td align="center" valign="middle">
							  			 <%=roundInvoices[b].getString("round_invoice_tv")%>&nbsp;
							  		 </td>
							  		 <td align="right" valign="middle">
										<input name="Submit32" type="button" class="short-short-button-mod" value="修改" onClick="modRoundInvoice(<%=roundInvoices[b].get("round_invoice_id",0l)%>)">
										<input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delRoundInvoice(<%=roundInvoices[b].getString("invoice_template_id")%>,<%=roundInvoices[b].get("round_invoice_id",0l)%>)">
									 </td>
									  <%if(b==0){ %>
							  		 <td align="right" valign="middle" rowspan="<%= roundInvoices.length%>" >
								        <input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="updateInvoiceTemplate(<%=rows[ii].getString("invoice_id")%>,'<%=category.getString("title") %>')"><br><br>
								        <input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delDelivererInfo(<%=rows[ii].getString("invoice_id")%>)"><br><br>
								        <input name="Submit32" type="button" class="long-button-add" value="添加样式" onClick="addRoundInvoice(<%=rows[ii].get("invoice_id",0l)%>,'<%=rows[ii].getString("t_name")%>','<%=category.getString("title")%>')">
							         </td> 
							         <%} %>
								  </tr>
							      <%}%>	
							  <%}%>	     				
						</table>
					  <%} %>
		          </div>
		        <%} %>  
		 </div>
	</div>
	<!-- 出口发件人信息 -->
	<div id="advan_search">
         <div id="tabs2">
			<ul>
			    <%DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();%>
			    <%for ( int i=0; i<treeRows.length; i++ ){%>
				   <li><a href="#<%=treeRows[i].getString("title")%>"><%=treeRows[i].getString("title")%></a></li>
				<%} %>		 
			</ul>
			    <%for ( int j=0; j<treeRows.length; j++ ){%>
			       <div id="<%=treeRows[j].getString("title")%>" >
				       <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
						   <input name="Submit" type="button" class="long-long-button-add" onClick="addInvoice(<%=treeRows[j].getString("id")%>,'<%=treeRows[j].getString("title") %>')" value="增加发货人">
					   </div>
			           <br/>
	                   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
							<thead>
							  <tr> 							 
							      <th width="17%" style="vertical-align: center;text-align: center;" class="right-title">公司名称</th>
							      <th width="18%" style="vertical-align: center;text-align: center;" class="right-title">地址</th>
							      <th width="6%" style="vertical-align: center;text-align: center;" class="right-title">州/省</th>
							      <th width="4%" style="vertical-align: center;text-align: center;" class="right-title">邮编ZIP</th>
							      <th width="5%" style="vertical-align: center;text-align: center;" class="right-title">国家代码</th>
							      <th width="6%" style="vertical-align: center;text-align: center;" class="right-title">国家名</th>
							      <th width="8%" style="vertical-align: center;text-align: center;" class="right-title">联系电话</th>
							      <th width="7%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
							  </tr>
							</thead>
							<%
							DBRow rows[] =storageSortMgr.getDetailStorageSortById(treeRows[j].get("id",0l));
							DBRow storage = invoiceTypeMgr.getStorageCategory(treeRows[j].get("id",0l));
							for (int a=0; a<rows.length; a++){%>
							      <tr>						    
							        <td height="70" align="left"" valign="middle"><img src="../imgs/user.png"></img>&nbsp;&nbsp;&nbsp;<%=rows[a].getString("CompanyName")%></td>
							        <td align="left" valign="middle" ><%=rows[a].getString("AddressLine1")+"<br>"+rows[a].getString("AddressLine2")+"<br>"+rows[a].getString("AddressLine3")+"<br>"+rows[a].getString("City")%></td>
							        <td align="center" valign="middle"><%=rows[a].getString("DivisionCode")%>&nbsp;</td>
							        <td align="center" valign="middle"><%=rows[a].getString("PostalCode")%>&nbsp;</td>
							        <td align="center" valign="middle"><%=rows[a].getString("CountryCode")%>&nbsp;</td>
							        <td align="center" valign="middle"><%=rows[a].getString("CountryName")%>&nbsp;</td>
							        <td align="center" valign="middle"><%=rows[a].getString("PhoneNumber")%>&nbsp;</td>						    
							        <td align="center" valign="middle">
							        	<input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="updateInvoice(<%=rows[a].getString("di_id")%>,'<%=storage.getString("title") %>')">
								        <br>
										<br>
								        <input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delDelivererInfo(<%=rows[a].getString("di_id")%>)">
							        </td>
							    </tr>
							<%}%>
					   </table>
		           </div>
			    <%} %>	
		  </div>        
    </div>
</div>
<!-- 各种form -->
<form  method="post" name="add_invoice_form" >
   <input type="hidden" name="di_id">
   <input type="hidden" name="CompanyName">
   <input type="hidden" name="AddressLine1">
   <input type="hidden" name="AddressLine2">
   <input type="hidden" name="AddressLine3">
   <input type="hidden" name="City">
   <input type="hidden" name="DivisionCode">
   <input type="hidden" name="PostalCode">
   <input type="hidden" name="CountryCode">
   <input type="hidden" name="CountryName">
   <input type="hidden" name="PhoneNumber">
   <input type="hidden" name="ps_id"/>
   <input type="hidden" name="backurl" value="/administrator/invoice/deliverer_info.html"/>
</form>
<form  method="post" name="temp_form" >
   <input type="hidden" name="di_id">
</form>
<!-- 发票form -->
 <form  method="post" name="add_form" >
    <input type="hidden" name="dog">
    <input type="hidden" name="rfe">
    <input type="hidden" name="uv">
    <input type="hidden" name="tv">
    <input type="hidden" name="invoice_id">
    <input type="hidden" name="di_id">
    <input type="hidden" name="ps_id" />
    <input type="hidden" name="t_name" />
    <input type="hidden" name="is_round_invoice"/>
    <input type="hidden" name="hs_code"/>
    <input type="hidden" name="material"/>
  </form>
  <form  method="post" name="add_round_invoice_form" >
    <input type="hidden" name="round_invoice_dog">
    <input type="hidden" name="round_invoice_ref">
    <input type="hidden" name="round_invoice_uv">
    <input type="hidden" name="round_invoice_tv">
    <input type="hidden" name="invoice_template_id">
    <input type="hidden" name="round_invoice_hs_code"/>
    <input type="hidden" name="round_invoice_id"/>
    <input type="hidden" name="round_invoice_material">
    <input type="hidden" name="proTextDogChinese">
    <input type="hidden" name="proTextRfeChinese">
    <input type="hidden" name="proMaterialChinese">
  </form>
  <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/delRoundInvoice.action" name="del_round_invoice_form" method="post">
  	<input type="hidden" name="invoice_id"/>
  	<input type="hidden" name="round_invoice_id">
  </form>
  	
</body>
</html>