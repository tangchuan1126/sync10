<%@ page contentType="text/html;charset=utf-8"%>
<%@ include file="../../include.jsp"%> 

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../common.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />


<link href="../comm.css" rel="stylesheet" type="text/css"/>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />



<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
		
<script language="JavaScript" type="text/JavaScript">
<!--
function delDelivererInfo(invoice_id)
{
	if (confirm("确认删除？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/delInvoiceTemplatesByInvoiceId.action";
		document.del_form.invoice_id.value = invoice_id;
		document.del_form.submit();
	}
}
function addInvoiceTemplate(ps_id,title)
{
	$.artDialog.open("add_invoice_template.html?ps_id="+ps_id, {title:title+"-仓库增加发票模板",width:'450px',height:'410px',fixed:true, lock: true,opacity: 0.3});
}

function updateInvoice(invoiceId,title)
{
	$.artDialog.open("mod_invoice_template.html?invoiceId="+invoiceId, {title:"修改"+title+"-仓库的发票模板",width:'450px',height:'410px',fixed:true, lock: true,opacity: 0.3});
}

function addRoundInvoice(invoiceId,t_name,title)
{
	$.artDialog.open("add_round_invoice.html?invoice_id="+invoiceId, {title:title+"-"+t_name+"-增加发票样式",width:'400px',height:'410px',fixed:true, lock: true,opacity: 0.3});
}

function modRoundInvoice(round_invoice_id)
{
	$.artDialog.open("mod_round_invoice.html?round_invoice_id="+round_invoice_id, {title:"修改模板样式",width:'400px',height:'410px',fixed:true, lock: true,opacity: 0.3});
}

function closeWin()
{
	tb_remove();
}
function showDiv(divId)
{
	if (document.getElementById(divId).style.display=="none")
	{
		document.getElementById(divId).style.display="block";
	}else{
        document.getElementById(divId).style.display="none";
	}
}

function ajaxLoadRoundInvoice(invoice_id)
{
	if($("#div_invoice_"+invoice_id).html().length>0)
	{
		$("#div_invoice_"+invoice_id).html("")
	}
	else
	{
		$.ajax({
				url: 'round_invoice_list.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:{invoice_id:invoice_id},
				beforeSend:function(request){

				},
				
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				
				success: function(html){
					$("#div_invoice_"+invoice_id).html(html);
				}
			});
	}
}

function delRoundInvoice(invoice_id,round_invoice_id)
{
	if(confirm("确定删除选定的模板样式？"))
	{
		document.del_round_invoice_form.invoice_id.value = invoice_id;
		document.del_round_invoice_form.round_invoice_id.value = round_invoice_id;
		document.del_round_invoice_form.submit();
	}
}
//-->
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 模板管理 »   形式发票模板</td>
  </tr>
</table>
<br>
  <form  method="post" name="add_form" >
    <input type="hidden" name="dog">
    <input type="hidden" name="rfe">
    <input type="hidden" name="uv">
    <input type="hidden" name="tv">
    <input type="hidden" name="invoice_id">
    <input type="hidden" name="di_id">
    <input type="hidden" name="ps_id" />
    <input type="hidden" name="t_name" />
    <input type="hidden" name="is_round_invoice"/>
    <input type="hidden" name="hs_code"/>
    <input type="hidden" name="material"/>
  </form>
  
  <form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/invoice/delRoundInvoice.action" name="del_round_invoice_form" method="post">
  	<input type="hidden" name="invoice_id"/>
  	<input type="hidden" name="round_invoice_id">
  </form>
  
  <form  method="post" name="add_round_invoice_form" >
    <input type="hidden" name="round_invoice_dog">
    <input type="hidden" name="round_invoice_ref">
    <input type="hidden" name="round_invoice_uv">
    <input type="hidden" name="round_invoice_tv">
    <input type="hidden" name="invoice_template_id">
    <input type="hidden" name="round_invoice_hs_code"/>
    <input type="hidden" name="round_invoice_id"/>
    <input type="hidden" name="round_invoice_material">
  </form>

  <form  method="post" name="del_form" >
    <input type="hidden" name="invoice_id">
  </form>
  
  <form  method="post" name="temp_form" >
              <input type="hidden" name="id">
</form>
<form  method="post" name="deliverer_form" >
              <input type="hidden" name="di_id">
</form>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
      <tr> 
        <th  style="vertical-align: center;text-align: center;" class="left-title">所属仓库</th>
        <th width="51%"  style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
      </tr>
    <%
DBRow treeRows[] = catalogMgr.getProductDevStorageCatalogTree();

for ( int i=0; i<treeRows.length; i++ )
{
%>
    <tr > 
     <td width="33%" height="60" valign="middle">
     	<a href="javascript:void(0);" onClick="showDiv('div<%=i%>')">
	     	<img border="0" src="../js/images/next.gif" />
	        <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
	      				<%
			if (treeRows[i].get("parentid",0) == 0)
			{
				out.println("<span style='font-size:14px;font-weight:bold'>"+treeRows[i].getString("title")+"</span>");
			}
			else
			{
				out.println(treeRows[i].getString("title"));
			}
			%>
		</a>		 
		</td>
      <td align="right" valign="middle">
      	<input name="Submit3" type="button" class="long-long-button-add" onClick="addInvoiceTemplate(<%=treeRows[i].getString("id") %>,'<%=treeRows[i].getString("title") %>')" value="增加发票模板">
	 </td>
    </tr>

    <tr>
      <td colspan="2">
        <div id="div<%=i%>" style="display: none">
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
			  <tr> 
			      <th width="14%" style="vertical-align: center;text-align: center;" class="left-title">发件人</th>
			      <th width="13%" style="vertical-align: center;text-align: center;" class="right-title">模板名称</th>
			      <th width="16%" style="vertical-align: center;text-align: center;" class="right-title">货物描述</th>
			      <th width="16%" style="vertical-align: center;text-align: center;" class="right-title">出口理由</th>
			      <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">HS</th>
			      <th width="6%" style="vertical-align: center;text-align: center;" class="right-title">材质</th>
			      <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">单位价值(US $)</th>
			      <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">总价值 (US $)</th>
			      <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
			  </tr>
			  <%
			DBRow [] rows = invoiceTypeMgr.getDetailInvoiceById(treeRows[i].get("id",0l));
			DBRow category = invoiceTypeMgr.getStorageCategory(treeRows[i].get("id",0l));
			for(int ii=0;ii<rows.length;ii++)
			{
			    
			%>
			  <tr>
			  		<td align="left" valign="middle" height="63">
			  			<% 
				  			if(rows[ii].get("di_id",0l) == 0)
							{
								out.print("<img src='../imgs/user.png' />&nbsp;&nbsp;&nbsp;随机");
							}
				  			else
				  			{
				  				DBRow invoice = invoiceTypeMgr.getDetailInvoiceCompanyNameByDiId(rows[ii].get("di_id",0l));
				  				out.print("<img src='../imgs/user.png' />&nbsp;&nbsp;&nbsp;"+invoice.getString("CompanyName"));
				  			}
			  			%>
			  		</td>
			        
			        <td align="center" valign="middle">
			        	<a href="javascript:ajaxLoadRoundInvoice(<%=rows[ii].get("invoice_id",0l)%>)">
			        		<%=rows[ii].getString("t_name")%>
			        	</a>
			        </td>
			        <% 
			        	if(rows[ii].get("is_round_invoice",0)==2)//不随机
			        	{
			        %>
			        <td align="left" valign="middle" ><%=rows[ii].getString("dog")%></td>
			        <td align="center" valign="middle"><%=rows[ii].getString("rfe")%>&nbsp;</td>
			        <td align="center" valign="middle"><%=rows[ii].getString("hs_code")%>&nbsp;</td>
			        <td align="center" valign="middle"><%=rows[ii].getString("material")%>&nbsp;</td>
			        <td align="center" valign="middle"><%=rows[ii].getString("uv")%></td>
			        <td align="center" valign="middle"><%=rows[ii].getString("tv")%>&nbsp;</td>
			        <% 
			        	}
			        	else
			        	{
			        %>
			        <td align="left" valign="middle" >随机</td>
			        <td align="center" valign="middle">随机</td>
			        <td align="center" valign="middle">随机</td>
			        <td align="center" valign="middle">随机</td>
			        <td align="center" valign="middle">随机</td>
			        <td align="center" valign="middle">随机</td>
			        <%
			        	}
			        %>
			        
			        <td align="center" valign="middle">
			        <input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="updateInvoice(<%=rows[ii].getString("invoice_id")%>,'<%=category.getString("title") %>')">
			          <br>
			          <br>
			        <input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delDelivererInfo(<%=rows[ii].getString("invoice_id")%>)">
			          <br>
			          <br>
			        <input name="Submit32" type="button" class="long-button-add" value="添加样式" onClick="addRoundInvoice(<%=rows[ii].get("invoice_id",0l)%>,'<%=rows[ii].getString("t_name")%>','<%=category.getString("title")%>')">
			        </td>
			        
			  </tr>
			  <tr>
			  	<td colspan="7">
			  		<div id="div_invoice_<%=rows[ii].get("invoice_id",0l)%>"></div>
			  	</td>
			  </tr>
			  <%} %>
			</table>
   		</div>
   	</td>
   </tr>
       <%} %>
   </table>
   
   </body>
</html>









