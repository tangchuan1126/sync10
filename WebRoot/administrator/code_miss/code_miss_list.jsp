<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 

<%
	 //分页参数设置
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	DBRow[] rows =  codeMissMgr.getAllCodeMiss(pc);
	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
//分页方法
function go(number){ 
	 $("#pageCount").val(number);
	 $("#pageForm").submit();	
	};

</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
.search_input{width:400px; height:20px;border:0px;bgcolor:none;}
</style>
</head>
<body onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 基础数据 »   数据列表</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form name="listForm" method="post">
    <tr height="20px"> 
        <th style="vertical-align: center;text-align: center;" class="left-title">MPN/SKU</th>
        <th style="vertical-align: center;text-align: center;" class="right-title">订单号</th>
        <th width="26%" style="vertical-align: center;text-align: center;" class="right-title">Item_Number</th>
    </tr>

    <%
    if(rows!=null && rows.length>0){
			for(int i = 0 ; i<rows.length; i++){
				
	%>
    <tr > 
      <td  height="40" align="center" valign="middle" style='word-break:break-all;'><%=rows[i].getString("miss_code")%></td>
      <td   width="50%" align="center" valign="middle" style='word-break:break-all;'>
      		<%
      		DBRow[] rows1 = codeMissMgr.getMissID(rows[i].getString("miss_code"));
      		if(rows1 != null && rows1.length > 0){
      			for(int k = 0 ; k < rows1.length; k++){
      				%>
     			 <div style="float:left;text-align:center;">[ <%=rows1[k].getString("miss_id") %> ]&nbsp;&nbsp;</div> 				
      		<%
      			}
      		}
      	%>	
      </td>     
      <td   align="center" valign="middle" style='word-break:break-all;'>
      		<%
      			DBRow[] rows2 =  codeMissMgr.getItemNumber(rows[i].getString("miss_code"));
      			if(rows2!=null && rows2.length>0){
    				for(int j = 0 ; j<rows2.length; j++){
      		%>
      		<div style="float:left;text-align:center">[ <%=rows2[j].getString("item_number") %> ]&nbsp;&nbsp;</div>
      		<%}
    		} %>   	
      </td>
    </tr>
    <%
	}
 }
%>
</form>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="pageForm" id="pageForm" action="">
	    <input type="hidden" name="p" id="pageCount">
	  </form>
	  <tr>
	    <td height="28" align="right" valign="middle">
	    <%
		int pre = pc.getPageNo() - 1;
		int next = pc.getPageNo() + 1;
		out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总记录：" + pc.getAllCount() + " &nbsp;&nbsp;");
		out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
		out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
		out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
		out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
		%>
	      跳转到
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
	        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
	    </td>
	  </tr>
	</table>
<br>
</body>
</html>
