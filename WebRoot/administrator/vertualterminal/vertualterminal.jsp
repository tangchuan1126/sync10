 <%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.Date"%>
<%@ include file="../../include.jsp"%> 
 <%@ page import="com.cwc.app.beans.AdminLoginBean;"%>
 <jsp:useBean id="currencyKey" class="com.cwc.app.key.CurrencyKey"/>
<%
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(20);	
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 	String cmd = StringUtil.getString(request,"cmd");
 	String txnId = "";
 	String state = "";
%>
<%
	
	DBRow[] rows = null ;
	if(cmd.equals("filter")){
		txnId = StringUtil.getString(request,"txnId");
		if(txnId.indexOf("*") != -1){
			txnId = "";
		}
		state = StringUtil.getString(request,"state");
		rows =  vertualTerminalMgrQLL.getPaymentVertualTerminal(pc,txnId,state);
	}else{
		rows = vertualTerminalMgrQLL.getAllVertualTerminal(pc);
	}
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/select.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<style type="text/css">
	.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	.title{font-size:12px;color:green;font-weight:blod;}
	 span.stateName{width:35%;float:left;text-align:right;font-weight:normal;display:block;}
	 span.stateValue{width:60%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
</style>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
 
<script language="javascript">
	function executeVertualTerminal(){
		// 嵌入系统的收款界面
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "pay.html"; 
		$.artDialog.open(uri , {title: '客户付款',width:'780px',height:'630px', lock: true,opacity: 0.3,fixed: true});
		 
	}
	function clearValue(){
		var node = $("#txnId");
		var txnId = node.val();
		if(txnId === "*请输入txnId"){
			node.val("");
		}
	}
	function checkValue(){
		var node = $("#txnId");
		if(node.val().length < 1){
			node.val("*请输入txnId")
		}
	}
	function filter(){
		var form = $("#submitForm");
	
		form.submit();
	 
	}
	jQuery(function($){
		var cmd = '<%= cmd%>';
		var txnId = '<%= txnId%>';
		var state = '<%= state%>'; 
		if(cmd === "filter"){
			 if(txnId.length > 0 ){
				$("#txnId").val(txnId);
			 }
			 $("#state option[value='"+state+"']").attr("selected",true);
			 var dataForm = $("#dataForm");
			 $("#dataFormCmd").val("filter");
			 $("#dataFormState").val(state); 
			 $("#dataFormTxnId").val(txnId); 
 
			 
		}
	})
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
 	<div id="tabs">
 		<ul>
 			<li><a href="#tool">常用工具</a></li>
 		</ul>
 		<div id="tool" style="height:50px;">
 			<form  id="submitForm">
 					<input type="hidden" name="cmd" value="filter"/>
	 				TXNID : <input type="text" id="txnId" name="txnId" value="*请输入txnId" style="height: 20px;line-height: 20px;width: 400px;" onfocus="clearValue()" onblur ="checkValue();" />
	 			    State : <select name="state" id="state">
	 			    			<option value="-1">All</option>
	 			    			<option value="0">Success</option>
	 			    			<option value="1">SuccessWithWarning</option>
	 			    			<option value="2">Failure</option>
	 			    		</select>
	 		
	 			<input class="normal-green" type="button" onclick="filter()" value="过滤" />
	 			<input class="normal-green" type="button" onclick="executeVertualTerminal()" value="执行收款" />
 				</form>
 		</div>
 	</div>
    <script type="text/javascript">
    	var tabs = $("#tabs");
    	tabs.tabs();
     </script>
 
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  >
		<tr>
			   <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle"> 收款记录</td>
		 </tr>  
		 <tr>
		 	 <td>
		 	 	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true">
		 	 			<tr>
		 	 				<th width="12%" style="vertical-align: center;text-align: center;" class="left-title">TXNID</th>
		 	 				<th width="12%" style="vertical-align: center;text-align: center;" class="left-title">收款时间</th>
		 	 				<th width="14%" style="vertical-align: center;text-align: center;" class="left-title">收款结果</th>
		 	 				<th width="18%" style="vertical-align: center;text-align: center;" class="left-title">原因</th>
		 	 				<th width="8%" style="vertical-align: center;text-align: center;" class="left-title">AMOUNT</th>
		 	 				<th width="25%" style="vertical-align: center;text-align: center;"  class="left-title">Card Info</th>
		 	 				 
		 	 			</tr>
		 	 			<%
		 	 			if(rows != null && rows.length > 0 ){
		 	 				for(int i =0 ;i<rows.length;i++)
		 	 				{
		 	 				%>
				 	 		<tr>
				 	 			<td align="center"><%=rows[i].getString("txn_id") %>&nbsp;</td>
				 	 			<td align="center"><%=rows[i].getString("deal_time") %></td>
				 	 			<td align="center"><%=rows[i].getString("result") %></td>
				 	 			<td align="center"><%=rows[i].getString("result_reasion") %>&nbsp;</td>
				 	 			<td align="center"><%=rows[i].getString("currency_code") %> : <%=rows[i].getString("amount") %></td>
				 	 			<td>
				 	 				<!-- card base info  -->
				 	 				<fieldset class="set" style="padding-bottom:4px;border:2px solid #993300;">
						  				 <legend>
												<span class="title">
													<span style="font-weight:bold;">Card Number : <%=rows[i].getString("acct") %></span>
												</span>
										</legend>
										<p style="clear:both;">
											<span class="stateName">All Name:</span>
											<span class="stateValue"><%=rows[i].getString("first_name") %> &nbsp;<%=rows[i].getString("last_name") %></span>
										</p>
										<p style="clear:both;">
											<span class="stateName">Expdate:</span>
											<span class="stateValue"><%=rows[i].getString("expdate") %></span>
										</p>
										<p  style="clear:both;">
											<span class="stateName">Country Code:</span>
											<span class="stateValue"><%=rows[i].getString("country_code") %></span>
										</p>
										<p  style="clear:both;">
											<span class="stateName">Address:</span>
											<span class="stateValue"><%=rows[i].getString("state") %>&nbsp;<%=rows[i].getString("city") %>&nbsp;<%=rows[i].getString("street") %></span>
										</p>
										<p>
											<span class="stateName">Zip:</span>
											<span class="stateValue"><%=rows[i].getString("zip") %></span>
										</p>
										 
										 
									</fieldset>
				 	 			</td>
				 	 		</tr>
		 	 				<% 
		 	 				}
		 	 			}else{
		 	 				 %>
		 	 				 <tr>
	 							<td colspan="6" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	 						</tr>
		 	 				 <% 
		 	 			}
		 	 			 %>
		 	 	</table>
		 	 </td>
		 </tr>      
	</table>
	
<br />
 <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
 <form name="dataForm" id="dataForm">
    <input type="hidden" name="p" />
    <input type="hidden" name="cmd" id="dataFormCmd"/>
    <input type="hidden" name="txnId" id="dataFormTxnId"/>
    <input type="hidden" name="state" id="dataFormState"/>
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
     	 跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
</body>
</html>
 