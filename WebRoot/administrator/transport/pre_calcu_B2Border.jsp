<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.beans.PreCalcuB2BOrderBean"%>
<%@page import="com.cwc.app.key.B2BOrderStatusKey"%>
<%@ include file="../../include.jsp"%>
<%
long transport_id = StringUtil.getLong(request,"transport_id");

DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
long deliver_ccid = transport.get("deliver_ccid",0l);
long deliver_pro_id = transport.get("deliver_pro_id",0l);

long optimal_psid = productMgr.getPriorDeliveryWarehouse(deliver_ccid,deliver_pro_id);

PreCalcuB2BOrderBean optimalCalcuB2BOrder = productStoreMgrZJ.calcuB2bOrderOptimalWareHouse(transport_id,optimal_psid,session);
ArrayList<PreCalcuB2BOrderBean> notOptimalCalcuB2BOrderList = productStoreMgrZJ.calcuB2BOrderNotOptimalWareHouse(transport_id,optimal_psid,session);

B2BOrderStatusKey b2bOrderStatusKey = new B2BOrderStatusKey();

boolean priorShow = false;
if(optimal_psid!=0)
{
	priorShow = true;
}

//没有选择仓库，则系统自动计算
//抄单一次后，走的路线不同
//System.out.println(select_ccid+ " + "+select_pro_id+" + "+detailOrder.get("ps_id",0l));

%>
<html>
<head>
<style>
.unSelect
{
	border:2px #cccccc solid;
	padding:2px;
	width:98%;
	background:#FFFFFF;
	-webkit-border-radius:5px;-moz-border-radius:5px; 
}
.beSelect
{
	border:2px #99CC00 solid;
	padding:2px;
	width:98%;
	background:#F3FAE4;
	-webkit-border-radius:5px;-moz-border-radius:5px;
}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单预处理</title>
<link href="../comm.css" rel="stylesheet" type="text/css">

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
<%
	if(priorShow==true)
	{
		long ps_id = optimalCalcuB2BOrder.getPs_id();
		DBRow[] optimalItems = optimalCalcuB2BOrder.getResult();
%>
		<tr>
		    <td align="center" valign="middle"  >
			    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			      <tr>
			        <td width="40%" align="center" valign="middle"><hr width="95%" size="1" style="color:#cccccc;"></td>
			        <td width="15%" align="center" valign="middle" style="color:#999999;font-weight:bold">最优仓库</td>
			        <td width="45%" align="center" valign="middle"><hr width="95%" size="1"  style="color:#cccccc;"></td>
			      </tr>
			    </table>
		    </td>
		</tr>
		<tr>
			<td align="left" valign="top" style="padding:5px;">
				<fieldset id="fieldset_<%=ps_id%>" class="unSelect">
					<legend style="font-size:15px;font-weight:bold;color:#333333;font-family:Arial, Helvetica, sans-serif;">
						<input type="radio" name="ps_id" id="ps_id_<%=ps_id%>" value="<%=ps_id%>" onClick="beSelected(<%=ps_id%>);">
		 				<label for="ps_id_<%=ps_id%>" style="cursor:hand">
						<%=catalogMgr.getDetailProductStorageCatalogById(ps_id).getString("title")%> warehouse 
						</label>
						<%=b2bOrderStatusKey.getB2BOrderStatusKeyById(optimalCalcuB2BOrder.getOrderStatus())%>
					</legend>
					<table width="97%" border="0" cellspacing="1" cellpadding="0">
						<tr>
							<td></td>
							<td>总库存</td>
							<td>CLP</td>
							<td>BLP</td>
							<td>Orignail</td>
							<td></td>
						</tr>
					<%
					for (int i=0; i<optimalItems.length; i++)
					{
						DBRow detailP = productMgr.getDetailProductByPcid(optimalItems[i].get("pc_id",0l));
					%>
						<tr>
						    <td style="font-weight:normal;font-family:Arial, Helvetica, sans-serif">
								<%=detailP.getString("p_name")%>
							</td>
							<td><%=optimalItems[i].get("product_count",0)%>/<%=optimalItems[i].get("product_store_count",0)%></td>
							<td><%=optimalItems[i].get("clp_count",0)%>/<%=optimalItems[i].get("clp_store_count",0)%></td>
							<td><%=optimalItems[i].get("blp_count",0)%>/<%=optimalItems[i].get("blp_store_count",0)%></td>
							<td><%=optimalItems[i].get("orignail_count",0)%>/<%=optimalItems[i].get("orignail_store_count",0)%></td>
				    		<td style="font-family:Arial, Helvetica, sans-serif;color:#999999;font-weight:normal">
				    			<%=b2bOrderStatusKey.getB2BOrderStatusKeyById(optimalItems[i].get("order_item_status",0))%>
				    		</td>
				  		</tr>
					<%
					}
					%>
					</table>
				</fieldset>	
			</td>
		</tr>
	<%
	}
	%>
		<tr>
		    <td align="center" valign="middle"  >
			    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			      <tr>
			        <td width="40%" align="center" valign="middle"><hr width="95%" size="1" style="color:#cccccc;"></td>
			        <td width="15%" align="center" valign="middle" style="color:#999999;font-weight:bold">其他仓库</td>
			        <td width="45%" align="center" valign="middle"><hr width="95%" size="1"  style="color:#cccccc;"></td>
			      </tr>
			    </table>
		    </td>
		</tr>
	<%
		for(int j=0;j<notOptimalCalcuB2BOrderList.size();j++)
		{
			PreCalcuB2BOrderBean notOptimalCalcuB2bOrder = notOptimalCalcuB2BOrderList.get(j);
			long ps_id = notOptimalCalcuB2bOrder.getPs_id();
			DBRow[] notOptimalItems = notOptimalCalcuB2bOrder.getResult();
	%>
		<tr>
			<td align="left" valign="top" style="padding:5px;">
				<fieldset id="fieldset_<%=ps_id%>" class="unSelect">
					<legend style="font-size:15px;font-weight:bold;color:#333333;font-family:Arial, Helvetica, sans-serif;">
						<input type="radio" name="ps_id" id="ps_id_<%=ps_id%>" value="<%=ps_id%>" onClick="beSelected(<%=ps_id%>);">
		 				<label for="ps_id_<%=ps_id%>" style="cursor:hand">
						<%=catalogMgr.getDetailProductStorageCatalogById(ps_id).getString("title")%> warehouse 
						</label>
						<%=b2bOrderStatusKey.getB2BOrderStatusKeyById(notOptimalCalcuB2bOrder.getOrderStatus())%>
					</legend>
					<table width="97%" border="0" cellspacing="1" cellpadding="0">
						<tr>
							<td></td>
							<td>总库存</td>
							<td>CLP</td>
							<td>BLP</td>
							<td>Orignail</td>
							<td></td>
						</tr>
					<%
					for (int i=0; i<notOptimalItems.length; i++)
					{
						DBRow detailP = productMgr.getDetailProductByPcid(notOptimalItems[i].get("pc_id",0l));
					%>
						<tr>
						    <td style="font-weight:normal;font-family:Arial, Helvetica, sans-serif">
								<%=detailP.getString("p_name")%>
							</td>
							<td><%=notOptimalItems[i].get("product_count",0)%>/<%=notOptimalItems[i].get("product_store_count",0)%></td>
							<td><%=notOptimalItems[i].get("clp_count",0)%>/<%=notOptimalItems[i].get("clp_store_count",0)%></td>
							<td><%=notOptimalItems[i].get("blp_count",0)%>/<%=notOptimalItems[i].get("blp_store_count",0)%></td>
							<td><%=notOptimalItems[i].get("orignail_count",0)%>/<%=notOptimalItems[i].get("orignail_store_count",0)%></td>
				    		<td style="font-family:Arial, Helvetica, sans-serif;color:#999999;font-weight:normal">
				    			<%=b2bOrderStatusKey.getB2BOrderStatusKeyById(notOptimalItems[i].get("order_item_status",0))%>
				    		</td>
				  		</tr>
					<%
					}
					%>
					</table>
				</fieldset>	
			</td>
		</tr>
	<%
		}
	%>
</table>
<script>
function recomExpress()
{
	$("#recom_express_tr").show();
}

function beSelected(ps_id)
{
var stCatalogs = "";
<%
DBRow stCatalogs[] = catalogMgr.getProductDevStorageCatalogByParentId(0,null);//不区分海外、当地仓库
for (int i=0; i<stCatalogs.length; i++)
{
%>
stCatalogs += "<%=stCatalogs[i].getString("id")%>,";
<%
}
%>

	document.getElementById("fieldset_"+ps_id).style.cssText = "border:2px #99CC00 solid;background:#F3FAE4;-webkit-border-radius:5px;-moz-border-radius:5px;";
	
	var stCatalogsA = stCatalogs.split(",");
	for (i=0; i<stCatalogsA.length; i++)
	{
		if (stCatalogsA[i]==""||ps_id==stCatalogsA[i]*1)
		{
			continue;
		}

		document.getElementById("fieldset_"+stCatalogsA[i]).style.cssText = "border:2px #cccccc solid;background:#FFFFFF;-webkit-border-radius:5px;-moz-border-radius:5px;";
	}
	
}
</script>
</body>

</html>