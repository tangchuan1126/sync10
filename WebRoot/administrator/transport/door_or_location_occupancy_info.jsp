<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>

<script type="text/javascript">
jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
});
$(function(){
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/door_use_info_list.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		beforeSend:function(request){
		},
		error: function(){
			alert("加载门占用信息失败！");
		},
		success: function(html){
			$("#door").html(html);
		}
	});
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/location_use_info_list.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		beforeSend:function(request){
		},
		error: function(){
			alert("加载门占用信息失败！");
		},
		success: function(html){
			$("#location").html(html);
		}
	});
});

</script>

</head>
<body style="margin-top: 0px; margin-bottom: 0px;">
<div class="demo" style="width:98%;margin:0px auto;" >
	<div id="tabs" style="margin-top:15px;">
		<ul>
			<li><a href="#door">门</a></li>
			<li><a href="#location">位置</a></li>
		</ul>
		<div id="door"></div>
		<div id="location"></div>
	
	</div>
</div>

</body>
</html>