<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
	cartWaybillB2BMgrZJ.cleanCart(request);
	
	long transport_id = StringUtil.getLong(request, "transport_id");
	DBRow[] showReceiveAllocate = transportMgrZJ.showReceiveAllocate(transport_id);
%>
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>


	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">

	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>

	<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

	<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";

--%>
@import "../js/thickbox/thickbox.css";
</style>
	<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
	<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
	<script src="../js/thickbox/global.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
	<script src="../js/jgrowl/jquery.jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css" />
	<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" />

	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<style type="text/css">
td.topBorder {
	border-top: 1px solid silver;
}
</style>
</head>
<body>
	<div id="tabs" style="width: 98%">
		<ul>
			<li>
				<a href="#receive">保留库存</a>
			</li>
		</ul>
		<div id="receive">
			<table width="100%" cellpadding="0" cellspacing="0"
				style="border: 1px solid silver;">
				<tr>
					<td width="40%" valign="top">
						<div>
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td align="left"></td>
									<td align="right">
										CLP
									</td>
									<td align="right">
										BLP
									</td>
									<td align="right">
										ILP
									</td>
									<td align="right">
										Original
									</td>
								</tr>
								<%
									for (int i = 0; i < showReceiveAllocate.length; i++) 
									{
										String backgroundColor = (i % 2 == 1 ? "#f9f9f9" : "white");
								%>
								<tr	style="height:22px;line-height:22px;background:<%=backgroundColor%>">
									<td class="topBorder" style="padding-left: 10px; padding-top: 5px; width: 120px;" nowrap="nowrap" align="left">
										<input id="<%=showReceiveAllocate[i].get("transport_detail_id",0l)%>" type="checkbox" onclick="checkBoxClick(this.value)" value="<%=showReceiveAllocate[i].get("transport_detail_id",0l)%>">
										<%=showReceiveAllocate[i].getString("p_name")%>
									</td>
									<td class="topBorder" align="right" nowrap="nowrap">
										<%=showReceiveAllocate[i].get("needCLP", 0)%>
										<input type="hidden" id="<%=showReceiveAllocate[i].get("transport_detail_id",0l)%>_clp_count" name="<%=showReceiveAllocate[i].get("transport_detail_id",0l)%>_clp_count" value="<%=showReceiveAllocate[i].get("needCLP",0)%>"/>
									</td>
									<td class="topBorder" align="right" nowrap="nowrap">
										<%=showReceiveAllocate[i].get("needBLP", 0)%>
										<input type="hidden" id="<%=showReceiveAllocate[i].get("transport_detail_id",0l)%>_blp_count" name="<%=showReceiveAllocate[i].get("transport_detail_id",0l)%>_blp_count" value="<%=showReceiveAllocate[i].get("needBLP", 0)%>"/>
									</td>
									<td class="topBorder" align="right" nowrap="nowrap">
										<%=showReceiveAllocate[i].get("needILP",0)%>
										<input type="hidden" id="<%=showReceiveAllocate[i].get("transport_detail_id",0l)%>_ilp_count" name="<%=showReceiveAllocate[i].get("transport_detail_id",0l)%>_ilp_count" value="<%=showReceiveAllocate[i].get("needILP", 0)%>"/>
									</td>
									<td class="topBorder" align="right" nowrap="nowrap">
										<%=showReceiveAllocate[i].get("needOriginal", 0)%>
										<input type="hidden" name="<%=showReceiveAllocate[i].get("transport_detail_id",0l)%>_count" value="<%=showReceiveAllocate[i].get("needOriginal", 0)%>"/>
									</td>
								</tr>
								<%
									}
								%>
							</table>
						</div>
					</td>
					<td
						style="width: 5%; border-left: 1px solid silver; border-right: 1px solid silver;">
						&nbsp;&nbsp;
					</td>
					<td align="center" style="width: 45%;">
						<div id="pre_calcu_order_page"></div>
						<div id="pre_calcu_order_info"></div>
					</td>
				</tr>
				<tr>
					<td colspan="3" class="topBorder">
					</td>
				</tr>
			</table>
			<table width="100%" cellpadding="0" cellspacing="0"
				style="padding-top: 5px;">
				<tr>
					<td></td>
					<td align="right">
						&nbsp;
					</td>
				</tr>
			</table>
		</div>
	</div>

	<script type="text/javascript">
		$("#tabs").tabs({
			cache: false,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			load: function(event, ui) {onLoadInitZebraTable();}	,
			selected:0,
		});
		function preCalcuOrder(transport_id)
		{
			$.ajax({
				url: 'pre_calcu_B2Border.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:{
					transport_id:transport_id
				},
				
				beforeSend:function(request){
					$("#pre_calcu_order_info").text("正在计算......");
				},
				
				error: function(){
					$("#pre_calcu_order_info").text("计算失败！");
				},
				
				success: function(html){
					$("#pre_calcu_order_page").html(html);
					$("#pre_calcu_order_info").text("");
				}
			});
		}
		
		function checkBoxClick(transport_detail_id)
		{
			if($("#"+transport_detail_id).attr("checked")=="checked")
			{
				putToB2BWaybillForItem(transport_detail_id);
			}
			else
			{
				removeToB2BWaybillForItem(transport_detail_id);
			}
		}
		
		function putToB2BWaybillForItem(transport_detail_id)
		{
			var clp_count = $("#"+transport_detail_id+"_clp_count").val();
			var blp_count = $("#"+transport_detail_id+"_blp_count").val();
			var ilp_count = $("#"+transport_detail_id+"_ilp_count").val();
			var count = $("#"+transport_detail_id+"_count").val();
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybillB2B/putToWaybillB2B.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				async:false,
				data:{
					transport_detail_id:transport_detail_id,
					clp_count:clp_count,
					blp_count:blp_count,
					ilp_count:ilp_count,
					count:count
				},
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(msg){
					if(msg=="ok")
					{
						preCalcuOrder(<%=transport_id%>)
					}
				}
			});	
		}
		function removeToB2BWaybillForItem(transport_detail_id)
		{
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/waybillB2B/reomveToWaybillB2B.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				async:false,
				data:{
					transport_detail_id:transport_detail_id
				},
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(msg){
					if(msg=="ok")
					{
						preCalcuOrder(<%=transport_id%>)
					}
				}
			});	
		}
		preCalcuOrder(<%=transport_id%>);
	</script>
</body>