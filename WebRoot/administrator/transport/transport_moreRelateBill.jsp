<html xmlns="http://www.w3.org/1999/xhtml">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<jsp:useBean id="purchasekey" class="com.cwc.app.key.PurchaseKey"/>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>


 <head>
	
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>
 	
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/art/plugins/jquery.artDialog.source.js" ></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />


<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<!-- tabs 
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script> 
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
-->  
 

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
  

<style type="text/css">
.set{padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;border: 2px solid blue;} 

body{font-size:12px;}	
p{text-align:left;}

.zebraTable td { border-bottom: 0px solid #DDDDDD;padding-left: 10px;}
.set{padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;border: 2px solid blue;} 

span.stateName{width:15%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
.zebraTable td { border-bottom: 0px solid #DDDDDD;padding-left: 10px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
</style>


<%
	TransportOrderKey transportOrderKey = new TransportOrderKey();
	String transport_id =  StringUtil.getString(request,"transport_id");
	String purchase_id  =  StringUtil.getString(request,"purchase_id");
	//本转运单详情
	DBRow  billDetail =  transportMgrZJ.getDetailTransportById(Long.valueOf(transport_id));
	DBRow  purchase     =  purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id);
	DBRow[] relateBill  =  transportMgrZyj.getTransportRowsByPurchaseId(Long.valueOf(purchase_id));
	DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
	int count = 0;
	if(relateBill != null)
	{
		count = relateBill.length;
	}
 %>



  </head>
  
  <body  >
  <!--
  <div id="demo">
  	<div id="tabs">
		<ul>
			<li><a href="#transport_search">所属采购单</a></li>
			<li><a href="#transport_filter">其他相关运单</a></li>
		</ul>
		<div id="transport_search"></div>
		<div id="transport_filter"></div>
	</div>
  </div>
    -->
  <table id="moreInfoTable" width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="" isNeed="true" isBottom="true" >
	   <thead >
	   	    <tr style="display:none"> 
		        <th colspan="3"  width="100%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">转运单T<%=transport_id %></th>
		  	</tr>
		    <tr> 
		        <th width="50%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">相关单据</th>
		        <th width="50%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">资金情况</th>
		  	</tr>
	  	</thead>
	 	<tbody>
	 	<tr>
	 		<td>
	 			<fieldset class="set" id="<%=transport_id%>">
  				<!-- 如果是交货行转运单那么就是Id显示成蓝色 -->
  					<%
  						String fontColor = Long.valueOf(purchase_id) > 0l ? "mediumseagreen;":"#f60;";
  					%>
  					<legend>
  					<a style="color:<%=fontColor %>" target="_blank" href="transport_order_in_detail.html?transport_id=<%=transport_id%>">T<%=transport_id%></a>
  					&nbsp;<%=transportOrderKey.getTransportOrderStatusById(billDetail.get("transport_status",0)) %>
  					</legend>
			  		<p style="text-align:left;clear:both; ">
			  				<span class="stateName">创建人:</span>
			  				<span class="stateValue">
				  				<%
				  					DBRow createAdmin = adminMgr.getDetailAdmin(billDetail.get("create_account_id",0l));
				  					if(createAdmin!=null)
				  					{
				  						out.print(createAdmin.getString("employe_name"));
				  					} 
				  				 %> 
			  				</span>
			  				</p>
			  					
			  				<p style="text-align:left;clear:both;">
			  					<span class="stateName">TITLE:</span>
			  					<span class="stateValue">
			  						<%
			  						DBRow titleRow = proprietaryMgrZyj.findProprietaryByTitleId(billDetail.get("title_id", 0L));
			  						if(null != titleRow)
			  						{  							
				  						out.println(titleRow.getString("title_name"));
			  						}
			  						%>
			  					</span>
			  				</p>
			  					
			  				<p style="text-align:left;clear:both;">
			  					<span class="stateName">允许装箱:</span>
			  					<span class="stateValue"><%
			  						 	DBRow packingAdmin = adminMgr.getDetailAdmin(billDetail.get("packing_account",0l));
			  						 	if(packingAdmin!=null)
			  						 	{
			  						 		out.print(packingAdmin.getString("employe_name"));
			  						 	} 
			  						 %>
			  					 </span>
			  				</p>
			  				<p style="text-align:left;clear:both;">
			  					<span class="stateName">创建时间:</span><span class="stateValue"><%=tDate.getFormateTime(billDetail.getString("transport_date"))%></span>
			  				</p>
			  				<p style="text-align:left;clear:both;">
			  				  <span class="stateName"> 更新时间:</span><span class="stateValue"><%= tDate.getFormateTime(billDetail.getString("updatedate")) %></span>
			  				</p>
  				</fieldset>
	 		</td>
	 		<td>
		 		<fieldset class="set" style='border-color:blue;'>
					<%
							String statusNameTrSelf = "";
							String appMoneyStateStrSelf = "";
							DBRow[] applyMoneySelf = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,billDetail.get("transport_id",0L),FinanceApplyTypeKey.DELIVERY_ORDER);
								if(applyMoneySelf.length > 0)
								{
									List imageListTrSelf = applyMoneyMgrLL.getImageList(applyMoneySelf[0].getString("apply_id"),"1");
									//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
								 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneySelf[0].getString("status")) && imageListTrSelf.size() < 1)
								 	{
								 		statusNameTrSelf = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
								 	}else{
								 		statusNameTrSelf = fundStatusKey.getFundStatusKeyNameById(applyMoneySelf[0].getString("status"));	
		 							}
								 	statusNameTrSelf = "("+statusNameTrSelf+")";
									appMoneyStateStrSelf = applyMoneySelf[0].get("amount",0.0) + applyMoneySelf[0].getString("currency");
		 						}
		 				%>
						<legend>
							<a target="_blank" href='<%=ConfigBean.getStringValue("systenFolder") %>administrator/transport/transport_order_detail.html?transport_id=<%=transport_id%>'>T<%=transport_id %></a>
							交货单货款
						</legend>
						<table style="width:100%">
							<tr><td>交货单金额:<%=transportMgrZJ.getTransportSendPrice(billDetail.get("transport_id",0L)) %>RMB</td></tr>
	 					<%
							if(applyMoneySelf.length > 0)
							{
								String moneyStandardDeliverStrSelf = "";
			  					if(!"RMB".equals(applyMoneySelf[0].getString("currency")))
			  					{
			  						moneyStandardDeliverStrSelf = "/"+applyMoneySelf[0].getString("standard_money")+"RMB";
			  					}
								out.println("<tr><td><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneySelf[0].get("apply_id",0l)+")\">F"+applyMoneySelf[0].get("apply_id",0)+"</a>"+ statusNameTrSelf + appMoneyStateStrSelf+moneyStandardDeliverStrSelf+"</td></tr>");
								DBRow[] applyTransferRowsSelf = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneySelf[0].getString("apply_id"));
					  			for(int ii=0;applyTransferRowsSelf!=null && ii<applyTransferRowsSelf.length;ii++) {
					  				String transfer_idSelf = applyTransferRowsSelf[ii].getString("transfer_id")==null?"":applyTransferRowsSelf[ii].getString("transfer_id");
					  				String amountSelf = applyTransferRowsSelf[ii].getString("amount")==null?"":applyTransferRowsSelf[ii].getString("amount");
					  				String currencySelf = applyTransferRowsSelf[ii].getString("currency")==null?"":applyTransferRowsSelf[ii].getString("currency");
					  				int moneyStatusSelf = applyTransferRowsSelf[ii].get("status",0);
					  				String transferMoneyStandardDeliverStrSelf = "";
					  				if(!"RMB".equals(currencySelf))
					  				{
					  					transferMoneyStandardDeliverStrSelf = "/"+applyTransferRowsSelf[ii].getString("standard_money")+"RMB";
					  				}
				  					if(moneyStatusSelf != 0 )
					  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_idSelf+"')\">W"+transfer_idSelf+"</a>转完"+amountSelf+" "+currencySelf+transferMoneyStandardDeliverStrSelf+"</td></tr>");
					  				else
					  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_idSelf+"')\">W"+transfer_idSelf+"</a>申请"+amountSelf+" "+currencySelf+transferMoneyStandardDeliverStrSelf+"</td></tr>");
									}
							}
						%>
						</table>
					</fieldset>	
	 		</td>
	 	
	 	</tr>
	 	
	 	<!-- 采购单 -->
		  <tr  nowrap="nowrap">
		  	<td>
		 		 	<fieldset class="set" style="border-color:#993300;">
  					<legend>
  						<a href="javascript:void(0)" onclick="window.open('<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail.html?purchase_id=<%=purchase_id %>')">P<%=purchase_id%></a>
	  					&nbsp;
	  					<%
	  						DBRow storage = catalogMgr.getDetailProductStorageCatalogById(purchase.get("ps_id",0l));
	  						out.print(null==storage?"":storage.getString("title"));
	  					%>
  					</legend>
  					<%
	  					String styleWrap = "<p style='text-align:left;clear:both;'>";
	  					String supplierStyle = "<p style='text-align:left;clear:both;'><span class='stateName'>&nbsp;</span><span class='stateValue'>";
						try
						{
							DBRow supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(purchase.getString("supplier")));
							if(supplier!=null)
							{
								DBRow productLine = productLineMgrTJH.getProductLineById(supplier.get("product_line_id",0l));
								if(productLine !=null)
								{
									out.println(styleWrap+"<span class='stateName'>产品线:</span><span class='stateValue'>"+productLine.getString("name")+"</span></p>");
								}
								%>
								<%
								out.println(supplierStyle+supplier.getString("sup_name")+"</span></p>");
							}
						}
						catch(NumberFormatException e)
						{
							out.println(supplierStyle+purchase.getString("supplier")+"</span></p>");
						}
					%>
					<p style="text-align:left;clear:both;">
						<span class="stateName">主流程:</span>
						<span class="stateValue"><%=purchasekey.getQuoteStatusById(purchase.getString("purchase_status"))%></span>
					</p>
			  		<p style="text-align:left;clear:both;">
			  			<span class="stateName">负责人:</span>
			  			<span class="stateValue"><%=purchase.getString("proposer") %></span>
			  		</p>
			  		<p style="text-align:left;clear:both;">
			  			<span class="stateName">采购时间:</span>
			  			<span class="stateValue"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(purchase.getString("purchase_date")))%></span>
			  		</p>
			  		<p style="text-align:left;clear:both;">
			  			<span class="stateName">更新日期:</span>
			  			<span class="stateValue"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(purchase.getString("updatetime")))%></span>
			  		</p>
					<p style="text-align:left;clear:both;">
						<span class="stateName">总体积:</span>
						<span class="stateValue"><%=purchaseMgr.getPurchaseVolume(purchase.get("purchase_id",0l))%> cm³</span>
						</p>
					<p style="text-align:left;clear:both;">
						<span class="stateName">总重量:</span>
						<span class="stateValue"><%=purchaseMgr.getPurchaseWeight(purchase.get("purchase_id",0l))%> Kg</span>
					</p>
					<p style="text-align:left;clear:both;">
						<span class="stateName">总金额:</span>
						<span class="stateValue"><%=purchaseMgr.getPurchasePrice(purchase.get("purchase_id",0l))%> RMB</span>
					</p>
					<p style="text-align:left;clear:both;">
						<span class="stateName">价格确认:</span>
						<span class="stateValue"><%=purchase.get("price_affirm_over",0d)==0?df.format(tDate.getDiffDate(purchase.getString("purchase_date"),"dd"))+"天":purchase.get("price_affirm_over",0d)+"天完成"%></span>
					</p>
					<%DBRow prepareDBRow = preparePurchaseMgrZwb.getPreparePurchaseFunds(Long.valueOf(purchase_id));
		               if(prepareDBRow!=null)
		               { 
		                  if(prepareDBRow.getString("currency").equals("USD"))
		                  { 
				    %>
				               <p style="text-align:left;clear:both;"><span class="stateName">预申请金额：</span><span class="stateValue"><%=prepareDBRow.getString("amount")%><%=prepareDBRow.getString("currency")%>/<%=prepareDBRow.getString("standard_money")%>RMB</span></p>
			                <%
			                }else
			                {%>
			                   <p style="text-align:left;clear:both;"><span class="stateName">预申请金额：</span><span class="stateValue"><%=prepareDBRow.getString("amount")%><%=prepareDBRow.getString("currency")%></span></p>
			                <%
			                } %>   
	               <%} %>
  			</fieldset>
		  	</td>
		  	<td>
		  	
		  	<!-- 采购单的资金情况 -->
  			<fieldset class="set" style="border-color:#993300; display:">
			<%
				DBRow[] applyMoneysPurchase = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100009,Long.valueOf(purchase_id),FinanceApplyTypeKey.PURCHASE_ORDER);
					//applyMoneyMgrLL.getApplyMoneyByBusiness(rows[i].getString("purchase_id"),4);//
			%>
			<legend style="font-size:12px;font-weight:normal;color:#000000;background:none;">
				<span style="font-size:12px;color:#000000;font-weight:normal;">
				<a href="javascript:void(0)" onclick="window.open('<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail.html?purchase_id=<%=purchase_id %>')">P<%=purchase_id%></a>
				<%
					String statusName = "";
					if(applyMoneysPurchase.length > 0)
					{
						List imageList = applyMoneyMgrLL.getImageList(applyMoneysPurchase[0].getString("apply_id"),"1");
						//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
					 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneysPurchase[0].getString("status")) && imageList.size() < 1)
					 	{
					 		statusName = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
					 	}else{
					 		statusName = fundStatusKey.getFundStatusKeyNameById(applyMoneysPurchase[0].getString("status"));	
						}
					 	statusName = "("+statusName+")";
					}
					
					if(Long.valueOf(transport_id)==101291)
		  			{
		  				int c = 1;
		  			}
				%>
				采购单定金
				(<%
					if(purchase!=null)
					{
						if(purchase.get("apply_money_over",0d)==0)
						{
							out.print(df.format(tDate.getDiffDate(purchase.getString("purchase_date"),"dd"))+"天");
						}
						else
						{
							out.print(purchase.get("apply_money_over",0d)+"天完成");
						}
					}
				%>)
				</span>
			</legend>
			<table style="width:100%">
				<tr style='width:100%'>
					<td style='width:100%'>采购单金额:<%=purchaseMgr.getPurchasePrice(Long.valueOf(purchase_id))%>RMB</td>
				</tr>
				<%
	  				if(applyMoneysPurchase.length>0) {
	  					String moneyStandardStr = "";
	  					if(!"RMB".equals(applyMoneysPurchase[0].getString("currency")))
	  					{
	  						moneyStandardStr = "/"+applyMoneysPurchase[0].getString("standard_money")+"RMB";
	  					}
	  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneysPurchase[0].get("apply_id",0l)+")\">F"+applyMoneysPurchase[0].get("apply_id",0)+"</a>"+ statusName +applyMoneysPurchase[0].get("amount",0f)+applyMoneysPurchase[0].getString("currency")+moneyStandardStr+"</td></tr>");
	  					DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneysPurchase[0].getString("apply_id"));
		 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
			  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
			  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
			  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
			  				int moneyStatus = applyTransferRows[ii].get("status",0);
			  				String transferMoneyStandardStr = "";
			  				if(!"RMB".equals(currency))
			  				{
			  					transferMoneyStandardStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
			  				}
			  				if(moneyStatus != 0 )
				  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
			  				else
				  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
				  			}
			  			}
	  		%>
			</table>
			</fieldset>
		  	</td>
		  </tr>
		  
		  <!-- 采购单相关的其他转运单 -->
		    <% 
		  	if(count > 1)
		  	{
		  		for(int i = 0 ;i < count; i++)
		  		{
		  			if(!transport_id.equals(relateBill[i].getString("transport_id")))
	  				{
	  			%>
	  				<tr>
	  					<td>
	  					<fieldset  class="set" style='border-color:#993300;'>
	  					<legend>
			  					<a style="color:mediumseagreen;" target="_blank" href="transport_order_detail.html?transport_id=<%=relateBill[i].getString("transport_id")%>">T<%=relateBill[i].getString("transport_id")%></a>
			  					&nbsp;<%=transportOrderKey.getTransportOrderStatusById(relateBill[i].get("transport_status",0)) %>
			  			</legend>
	  					<p style="text-align:left;clear:both;">
			  						<span class="stateName">创建人:</span>
			  						<span class="stateValue">
			  						<%
			  						 	DBRow createAdminTransprot = adminMgr.getDetailAdmin(relateBill[i].get("create_account_id",0l));
			  						 	if(createAdminTransprot!=null)
			  						 	{
			  						 		out.print(createAdminTransprot.getString("employe_name"));
			  						 	} 
			  						 %> 
			  						 </span>
			  					</p>
			  					<p style="text-align:left;clear:both;">
			  						<span class="stateName">TITLE:</span>
			  						<span class="stateValue">
			  						<%
				  					DBRow relateDeliveryTitleRow = proprietaryMgrZyj.findProprietaryByTitleId(relateBill[i].get("title_id", 0L));
			  						if(null != relateDeliveryTitleRow)
			  						{  							
				  						out.println(relateDeliveryTitleRow.getString("title_name"));
			  						}
			  						%>
			  						</span>
			  					</p>
			  					<p style="text-align:left;clear:both;">
			  						<span class="stateName">允许装箱:</span>
			  						<span class="stateValue"><%
			  						 	DBRow packingAdminTransprot = adminMgr.getDetailAdmin(relateBill[i].get("packing_account",0l));
			  						 	if(packingAdminTransprot!=null)
			  						 	{
			  						 		out.print(packingAdminTransprot.getString("employe_name"));
			  						 	} 
			  						 %>
			  						 </span>
			  					</p>
			  					<p style="text-align:left;clear:both;">
			  						<span class="stateName">创建时间:</span><span class="stateValue"><%=tDate.getFormateTime(relateBill[i].getString("transport_date"))%></span>
			  					</p>
			  					<p style="text-align:left;clear:both;">
			  					  <span class="stateName"> 更新时间:</span><span class="stateValue"><%= tDate.getFormateTime(relateBill[i].getString("updatedate")) %></span>
			  					</p>
			  				</fieldset>
			  				</td>
			  			<!-- 资金情况 -->
			  				<td>
			  				 <fieldset class="set" style='border-color:#993300;'>
								<%
										String statusNameTr = "";
										String appMoneyStateStr = "";
										String selfColor = "";
										DBRow[] applyMoneyDelivers = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,relateBill[i].get("transport_id",0L),FinanceApplyTypeKey.DELIVERY_ORDER);
										if(applyMoneyDelivers.length > 0)
										{
												List imageListTr = applyMoneyMgrLL.getImageList(applyMoneyDelivers[0].getString("apply_id"),"1");
												//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
											 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneyDelivers[0].getString("status")) && imageListTr.size() < 1)
											 	{
											 		statusNameTrSelf = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
											 	}else{
											 		statusNameTrSelf = fundStatusKey.getFundStatusKeyNameById(applyMoneyDelivers[0].getString("status"));	
					 							}
											 	statusNameTrSelf = "("+statusNameTrSelf+")";
												appMoneyStateStr = applyMoneyDelivers[0].get("amount",0.0) + applyMoneyDelivers[0].getString("currency");
					 					}
					 				%>
									<legend>
										<a target="_blank" href='<%=ConfigBean.getStringValue("systenFolder") %>administrator/transport/transport_order_detail.html?transport_id=<%=relateBill[i].get("transport_id",0L)%>'>T<%=relateBill[i].get("transport_id",0L) %></a>
										交货单货款
									</legend>
									<table style="width:100%">
										<tr><td>交货单金额:<%=transportMgrZJ.getTransportSendPrice(relateBill[i].get("transport_id",0L)) %>RMB</td></tr>
				 					<%
										if(applyMoneyDelivers.length > 0)
										{
												String moneyStandardDeliverStr = "";
							  					if(!"RMB".equals(applyMoneyDelivers[0].getString("currency")))
							  					{
							  						moneyStandardDeliverStr = "/"+applyMoneyDelivers[0].getString("standard_money")+"RMB";
							  					}
												out.println("<tr><td><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneyDelivers[0].get("apply_id",0l)+")\">F"+applyMoneyDelivers[0].get("apply_id",0)+"</a>"+ statusNameTr + appMoneyStateStr+moneyStandardDeliverStr+"</td></tr>");
												DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneyDelivers[0].getString("apply_id"));
									  			
							  					for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) 
							  					{
										  			String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
									  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
									  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
									  				int moneyStatus = applyTransferRows[ii].get("status",0);
									  				String transferMoneyStandardDeliverStr = "";
									  				if(!"RMB".equals(currency))
									  				{
									  					transferMoneyStandardDeliverStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
									  				}
								  					if(moneyStatus != 0 )
								  					{
									  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardDeliverStr+"</td></tr>");
									  				}
									  				else
									  				{
									  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardDeliverStr+"</td></tr>");
													}
												}
										  }
									%>
									</table>
								</fieldset>	
			  				</td>
			  			</tr>	
	  			<% 		}
	  				}
		  		}
		  	%>
		  
		  <tr>
		  	<td colspan="2" align="right" class="win-bottom-line">
     			<input type="button" id=close value="关闭" class="normal-white" style="cursor: pointer" onclick="closeWindow()" >
     			&nbsp;	&nbsp;&nbsp;&nbsp;
		  </tr>
	  </tbody>
  </table>
  </body>
  <script type="text/javascript">
 	function closeWindow()
 	{
 		$.artDialog.close();
 	}
 	 
 	/*
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		show:function(event,ui)
			 {
			 	if(ui.index==2)
			 	{
			 		
			 	}
			 }
	});


	*/
	
	function goFundsTransferListPage(id)
	{
	 window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;      
	}	
	
	
	
(function( $, undefined ) {
$.fn.fixHeader = function(options){
    var defaults = {
        width: '',
        height: ''

    };
    options = $.extend({}, defaults, options);
    var elem = this;
 	elem.width(options.width);

    if(options.height == ''){
        return this;
    }

    var thead = elem.find('thead');
    var fixTable = elem.clone().empty().removeAttr('id');
    //set head default background-color
    if(fixTable.css('background-color') == 'transparent' || fixTable.css('background-color') == ''){
        fixTable.css('background-color', '#fff');
    }
    fixTable.css({
        'position': 'absolute',
        'top': '0px',
        'border-bottom': $('tr:eq(0)', thead).find('th:eq(0), td:eq(0)').css('border-bottom-width')
    });


    $('tr:eq(0)', thead).find('th, td').each(function(){
        var col = $(this);

        if($.browser.mozilla){
            col.width(col.width());
        }
        else if($.browser.chrome){
            var colBorderWidth = parseInt(col.css('border-right-width'));
            if(colBorderWidth){
                col.width(col.width()+colBorderWidth);
            }
            else{
                col.width(col.width());
            }
        }
        else if($.browser.msie){
            var colBorderWidth = parseInt(col.css('border-right-width'));
            if(colBorderWidth){
                col.width(col.width()+colBorderWidth+colBorderWidth/2);
            }
            else{
                col.width(col.width());
            }
        }
        else{
            var colBorderWidth = parseInt(col.css('border-right-width'));
            if(colBorderWidth){
                col.width(col.width()+colBorderWidth);
            }
            else{
                col.width(col.width());
            }
        }
    });

    //make head
    var dummyHead = thead.clone();
    thead.appendTo(fixTable);
    dummyHead.prependTo(elem);


    var tbodyWrapper = elem.wrap('<div class="body-wrapper"></div>').parent();
    var tableWrapper = tbodyWrapper.wrap('<div class="table-wrapper" style="position:relative;"/>').parent();
    setTableWidth();
    setWrapperSize();

    fixTable.prependTo(tableWrapper);

    return this;

    function setTableWidth(){
        if($.browser.mozilla){
            elem.width(elem.width());
            fixTable.css('width',elem.css('width'));
        }
        else if($.browser.chrome){
            elem.width(elem.outerWidth());
            fixTable.width(elem.outerWidth());
        }
        else if($.browser.msie){
            elem.width(elem.outerWidth());
            fixTable.width(elem.outerWidth());
        }
        else{
            elem.width(elem.outerWidth());
            fixTable.width(elem.outerWidth());
        }
    }
    function setWrapperSize(){
        var elemWidth = elem.outerWidth(true);
        var elemHeight = elem.outerHeight(true);
        var scrollBarWidth = 20;

        if(options.width == ''){
            tbodyWrapper.css({
                'width': (elemWidth+scrollBarWidth) + 'px',
                'height': options.height,
                'overflow-x': 'hidden',
                'overflow-y': 'auto'
            });
        }
        else{
            if(elemWidth <= options.width){
                tbodyWrapper.css({
                    'width': options.width+'px',
                    'height': options.height,
                    'overflow-x': 'hidden',
                    'overflow-y': 'auto'
                });
            }
            else{
                tableWrapper.css({
                        'width': options.width,
                        'height': options.height,
                        'overflow': 'auto'
                });
                tableWrapper.scroll(function(){
                    fixTable.css('top',tableWrapper.scrollTop()+'px');
                });
            }
        }
    }
};


})( jQuery );
	
	
	$('#moreInfoTable').fixHeader({height:500,width:970});
	</script>
</html>
