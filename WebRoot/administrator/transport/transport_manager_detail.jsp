<%@ page import="com.cwc.app.util.ConfigBean" language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.TransportRoleKey"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="transportOrderKey" class="com.cwc.app.key.TransportOrderKey"/>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/> 
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/>
<jsp:useBean id="transportQualityInspectionKey" class="com.cwc.app.key.TransportQualityInspectionKey"/> 
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey"></jsp:useBean>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>

<%

   	long transport_id = StringUtil.getLong(request,"transport_id");
 	boolean edit = false;
 	
 	TDate tDate = new TDate();
	//fileFlag
	String fileFlag= StringUtil.getString(request,"flag");
	
	//转运单
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
	
	//转运单title
	String title_name = "";
	DBRow titleRow = proprietaryMgrZyj.findProprietaryByTitleId(transport.get("title_id", 0L));
	if(null != titleRow)
	{  							
		title_name = titleRow.getString("title_name");
	}
	
	
	//提货仓库
	String send_psName = "";
	if(transport.get("purchase_id",0l)==0)
	{
		send_psName = null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"";
		session.setAttribute("billOfLading",null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
	}
	else
	{
		send_psName = supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name");
	}
	
	//收货仓库
	String recivie_psName = null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):"";
 	
 	
 	HashMap followuptype = new HashMap();
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改转运单详细记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	
	
	
	// 单证
	String valueCertificate = systemConfig.getStringConfigValue("transport_certificate");
	String[] arraySelectedCertificate = valueCertificate.split("\n");
	//将arraySelected组成一个List
	ArrayList<String> selectedListCertificate= new ArrayList<String>();//类型对应值数组
	ArrayList<String> selectedListCertificateClass= new ArrayList<String>();//类型数组 与selectedListCertificate 一一对应
	for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ )
	{
			String tempStr = arraySelectedCertificate[index];
			if(tempStr.indexOf("=") != -1){
				String[] tempArray = tempStr.split("=");
				String tempHtml = tempArray[1];
				selectedListCertificateClass.add(tempArray[0]);//新增类型
				selectedListCertificate.add(tempHtml);
			}
	}
	int file_with_type = StringUtil.getInt(request,"file_with_type");
	String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
	String file_with_class = StringUtil.getString(request,"file_with_class");
	DBRow[] imageListCertificate = fileMgrZJ.getFileByWithIdAndType(transport_id,FileWithTypeKey.transport_certificate);
 	Map<String,List<DBRow>> mapCertificate = new HashMap<String,List<DBRow>>();
 	TransportCertificateKey certificateKey = new TransportCertificateKey();
	if(imageListCertificate != null && imageListCertificate.length > 0)
	{
		//map<string,List<DBRow>>
		for(int index = 0 , count = imageListCertificate.length ; index < count ;index++ )
		{
			DBRow temp = imageListCertificate[index];
			List<DBRow> tempListRow = mapCertificate.get(temp.getString("file_with_class"));
			if(tempListRow == null)
			{
				tempListRow = new ArrayList<DBRow>();
			}
			tempListRow.add(temp);
			mapCertificate.put(temp.getString("file_with_class"),tempListRow);
		}
	}
	
	//上传单证后回调刷新单证信息url;
	String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/transport/transport_refresh_certify.html?transport_id="+transport_id;
	
	//单证文件基本路径
	String transport_base_path = ConfigBean.getStringValue("systenFolder") + "upload/"+ systemConfig.getStringConfigValue("file_path_transport");
	
	//删除文件url
	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	
	//下载url
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	
	
 %>
 
 <%
 		//判断当前登录人属于发货方还是属于收货方
 		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean( request.getSession(true) );
		long loginPsId = adminLoggerBean.getPs_id();
		
		//判断登录人 所属角色（发货方还是收货方）
		int loginRole = 0;
		if(loginPsId == transport.get("send_psid", 0l)  && loginPsId != transport.get("receive_psid", 0l))
		{
			loginRole = TransportRoleKey.TRANSPORT_ROLE_SEND;//发货方
		}
		
		if(loginPsId == transport.get("receive_psid", 0l) && loginPsId != transport.get("send_psid", 0l))
		{
			loginRole = TransportRoleKey.TRANSPORT_ROLE_RECEIVE;//收货方
		}
		
		if(loginPsId == transport.get("send_psid", 0l) && loginPsId == transport.get("receive_psid", 0l)) 
		{
			loginRole = -1;//既是发货也是收货
		}
		
		
		//登录人有那些权限
		boolean stockInSetAuth =  (loginRole==-1)||(loginRole == transport.get("stock_in_set_responsible",0)) ?true:false;
		boolean declarationAuth = (loginRole==-1)||(loginRole == transport.get("declaration_responsible",0))?true:false;
		boolean clearanceAuth = (loginRole==-1)||(loginRole == transport.get("clearance_responsible",0))?true:false;
		boolean certificateAuth = (loginRole==-1)||(loginRole == transport.get("certificate_responsible",0))?true:false;
		
  %>
 
 <%
 	//业务逻辑判断：判断是否有报关，假设有的话 状态颜色显示效果
 	
 	//计算颜色
 	int stockInSetInt = transport.get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET);
	String stockInSetClass = "" ;
	String stock_in_set_state ="未完成";
 	if(stockInSetInt != transportStockInSetKey.SHIPPINGFEE_NOTSET && stockInSetAuth) 
	{
		if(stockInSetInt == TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH)
		{
  			stockInSetClass += "spanBold";
  			DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_STOCKINSET);
  			if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  			{
  				stockInSetClass += " fontGreen";
  				stock_in_set_state = "已完成";
  			}
  			else
  			{
  				stockInSetClass += " fontRed";
  				stock_in_set_state = "未上传凭证";
  			}
		}
		else 
		{
			stockInSetClass += "spanBold spanBlue";
  		}
	} 	
 	
 	//计算报关颜色和状态
 	int declarationInt = transport.get("declaration",declarationKey.NODELARATION);
    String declarationKeyClass = "" ;
	String declarationKeyClass_state = "未完成" ;
 	if(declarationInt != declarationKey.NODELARATION && declarationAuth) 
	{
			if(declarationInt == declarationKey.FINISH)
			{
  				declarationKeyClass += "spanBold";
  				DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_declaration);
  				if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  				{
  					declarationKeyClass += " fontGreen";
  					declarationKeyClass_state = "已完成";
  				}else
  				{
  					declarationKeyClass += " fontRed";
  					declarationKeyClass_state = "未上传凭证";
  				}
			}
			else 
			{
				declarationKeyClass +=  "spanBold spanBlue";
  			}
   } 
   
   //清关显示颜色样式
   int clearanceInt = transport.get("clearance",clearanceKey.NOCLEARANCE);
   String clearanceKeyClass = "" ;
   String clearanceKeyClass_state ="未完成";
   if(clearanceInt != clearanceKey.NOCLEARANCE && clearanceAuth) 
   {
	    if( clearanceInt == clearanceKey.FINISH)
		{
  			clearanceKeyClass += "spanBold";
  			DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_clearance);
  			if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  			{
  				clearanceKeyClass += " fontGreen";
  				clearanceKeyClass_state ="已完成";
  			}
  			else
  			{
  				clearanceKeyClass += " fontRed";
  				clearanceKeyClass_state ="未上传凭证";
  			}
		}
		else 
		{
			clearanceKeyClass +=  "spanBold spanBlue";
  		}
	} 
 
	 //计算单证的颜色和样式
 	int certificateInt = transport.get("certificate",TransportCertificateKey.NOCERTIFICATE);
	String certificateClass = "" ;
	String certificateClass_state ="未完成";
 	if(certificateInt != certificateKey.NOCERTIFICATE && certificateAuth) 
	{
  		if(certificateInt == TransportCertificateKey.FINISH)
  		{
  			certificateClass += "spanBold";
  			DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_certificate);
  			if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  			{
  				certificateClass += " fontGreen";
  				certificateClass_state = "已完成";
  			}
  			else
  			{
  				certificateClass += " fontRed";
  				certificateClass_state = "有未上传的凭证";
  			}
  		}
  		else 
  		{
  			    certificateClass += "spanBold spanBlue";
  		}
   }
%>
 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>转运单 T<%=transport_id %></title>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>

<!--  无用js 
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script> 
<script type="text/javascript" src="../js/popmenu/common.js"></script> -->

<!-- 无用js -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script> 
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" /> 

<!-- 该js导致prettyPhoto 布局错误,怀疑覆盖了jquery某些核心功能
<script type="text/javascript" src="../js/select.js"></script>  -->

<!-- 无用js 
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script> -->

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>


<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>   
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />


<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script> 

<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css"; 

</style>
<style type="text/css">
	body{text-align:left;}
	ol.fundsOl{
		list-style-type: none;	
	}
	ol.fundsOl li{
		width: 350px;
		height: 200px;
		float: left;
		line-height: 25px;
	}
</style>

<!--  无用js -->
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>


<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jqgrid 引用 -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>
<!-- 在线阅读  -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>

<style>

/*图片预览设置按钮路径*/
.smooth_zoom_icons {
	background-image: url("../js/picture_online_show/images/icons.png");
}

a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}
.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
.ui-datepicker {z-index:1200;}
.rotate
{
    /* for Safari */
    -webkit-transform: rotate(-90deg);

    /* for Firefox */
    -moz-transform: rotate(-90deg);

    /* for Internet Explorer */
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
}
.ui-jqgrid .ui-jqgrid-htable th
{
	font-size:11px;
}
	
.ui-jqgrid tr.jqgrow td 
{
	white-space: normal !important;
	height:auto;
	vertical-align:text-top;
	padding-top:2px;
}
.set
{
	border:2px #999999 solid;
	padding:2px;
	width:90%;
	word-break:break-all;
	margin-top:10px;
	margin-top:5px;
	line-height:18px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	margin-bottom: 10px;
}
.create_order_button{background-attachment: fixed;background: url(../imgs/create_order.jpg);background-repeat: no-repeat;background-position: center center;height: 51px;width: 129px;color: #000000;border: 0px;}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
.zebraTable td {line-height:25px;height:25px;}
.right-title{line-height:20px;height:20px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
span.moredown {
	margin: 5;
	background-attachment: fixed;
	background: url(../imgs/maps/hide_light_down.png);
	background-repeat: no-repeat;background-position: center center;height: 16px;width: 16px;color: #000000;border: 0px;
}
span.moreup {
	margin: 5;
	background-attachment: fixed;
	background: url(../imgs/maps/hide_light_up.png);
	background-repeat: no-repeat;background-position: center center;height: 16px;width: 16px;color: #000000;border: 0px;
}
div.tabdown {
	background-attachment: fixed;
	background: url(../imgs/tabdown.png);
	background-repeat: no-repeat;background-position: center center;height: 15px;width: 79px;color: #000000;border: 0px;
	position:relative;
}
div.tabup {
	background-attachment: fixed;
	background: url(../imgs/tabup.png);
	background-repeat: no-repeat;background-position: center center;height: 15px;width: 79px;color: #000000;border: 0px;
	position:relative;
}

.smallGallery .nav{
    width:105px;
}
.mediumGallery .nav{
    width:165px;
}
.largeGallery .nav{
    width:225px;
}
.smallGallery .images,
.mediumGallery .images,
.largeGallery .images{
    padding:10px;
    background-color:#f9f9f9;
    border:1px solid #fff;
    position:relative;
    -moz-box-shadow:1px 1px 5px #aaa;
    -webkit-box-shadow:1px 1px 5px #aaa;
    box-shadow:1px 1px 5px #aaa;
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border-radius:5px;
}
.nav {
    height: 20px;
    padding-left: 4px;
}
.smallGallery .images,
.smallGallery .singleImg div{
    width:102px;
    height:102px;
}
.mediumGallery .images,
.mediumGallery .singleImg div{
    width:162px;
    height:108px;
}
.largeGallery .images,
.largeGallery .singleImg div{
    width:222px;
    height:222px;
}
.microGallery{
    position:relative;
    margin:30px 10px 10px 10px;
    float:left;
}
.microGallery img{
    margin-left:auto;
    margin-right:auto;
    border:none;
    -moz-box-shadow:0px 2px 4px #777;
    -webkit-box-shadow:0px 2px 4px #777;
    box-shadow:0px 2px 4px #777;
    display:none;
}
a.thumbview{
    opacity:0.6;
    width:20px;
    height:21px;
    float:left;
    cursor:pointer;
}
.images div{
    display:table-cell;
    vertical-align:middle;
    text-align:center;
    position:relative;
}

.smallGallery .thumbs div,
.mediumGallery .thumbs div,
.largeGallery .thumbs div{
    float:left;
    margin:2px;
    cursor:pointer;
}

.smallGallery .thumbs div{
    width:30px;
    height:30px;
}
.mediumGallery .thumbs div{
    width:50px;
    height:50px;
}
.largeGallery .thumbs div{
    width:70px;
    height:70px;
}
</style>

</head>
<body margin="1" onLoad="onLoadInitZebraTable()">
<div class="demo">
	<div style="min-height: 25px">
		<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
			<tr height="30px">
				<td>
					&nbsp;&nbsp;
					
					
					<input name="button" type="button" class="long-button" value="修改各流程信息" onClick="updateTransportAllProdures()" />
				
					<input name="button" type="button" value="运单修改" onClick="showFreight()" class="long-button" />
					<%
						if(transport.get("transport_status",0)==TransportOrderKey.READY)
						{
					%>
						<input  type="button" class="long-button-upload" value="上传转运单" onclick="uploadTransportOrderDetail()"/>
					<%	
						}
					%>
					<input type="button" onclick="uploadInvoice(<%=transport_id%>)" value="上传商业发票" class="long-button-upload">
					
					<input  type="button" class="long-button-next" value="下载转运单" onclick="downloadTransportOrder(<%=transport_id%>)"/>
			  		<input type="button" class="long-button-print" value="打印转运单" onclick="print()"/>
					
				</td>
			</tr>
		</table>
	</div>
	<div id="transportMainTabs" style="min-height: 25px">
		<ul>
			<li ondblclick="opBasicTab()"><a href="#transport_basic_info"><span>基础信息</span></a></li>
			<li ondblclick="opBasicTab()"><a href="#transport_info"><span>货单信息</span></a></li>
		</ul>
		<div id="transport_basic_info">
			<table id="basic_info" width="100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
				<tr id="basic_info_main" >
					<td align="center" width="33%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">转运单号：</td>
									<td style="width: 80%;" nowrap="nowrap">T<%=transport_id%></td>
								</tr>
						</table>
					</td>
					<td align="center" width="33%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">TITLE：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=title_name %></td>
								</tr>
						</table>
					</td>
					<td align="center" width="34%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">货物状态：</td>
									<td style="width: 80%;" nowrap="nowrap">
									<%=transportOrderKey.getTransportOrderStatusById(transport.get("transport_status",0)) %>
									</td>
								</tr>
						</table>
					</td>
				</tr>
				</thead>
				<tbody>
				<tr id="basic_info_detail" style="display:none;">
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>运单基础信息</legend>
							<table style="width:100%;border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">运单号：</td>
									<td style="width: 80%;" nowrap="nowrap">T<%=transport_id%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">TITLE：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=title_name %></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">创建人：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("create_account")%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">创建时间：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=tDate.getEnglishFormateTime(transport.getString("transport_date"))%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">ETA：</td>
									<td style="width: 80%;" nowrap="nowrap">
									<% 
										if(transport.getString("transport_receive_date").trim().length() > 0)
										{
											out.println(tDate.getEnglishFormateTime(transport.getString("transport_receive_date")));
										}
 									%>
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">允许装箱：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<% 
										DBRow packinger = adminMgr.getDetailAdmin(transport.get("packing_account",0l));
										if(packinger!=null)
										{
											out.print(packinger.getString("employe_name"));
										}
									%>
									</td>
								</tr>
							</table>
						</fieldset>
					</td>
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>运输基础信息</legend>
							<table style="width:100%;border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">货物状态：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<%=transportOrderKey.getTransportOrderStatusById(transport.get("transport_status",0)) %>
									</td>
								</tr>
								<%
									if(stockInSetAuth)
									{
								 %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">运费状态：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=stockInSetClass%>"><%=transportStockInSetKey.getStatusById(transport.get("stock_in_set",transportStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>
									</td>
								</tr>
								  <%} %>
								 <%
									if(declarationAuth)
									{
								 %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">出口报关：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=declarationKeyClass%>"><%=declarationKey.getStatusById(transport.get("declaration",01)) %></span>
									</td>
								</tr>
								<%} %>
								
								 <%
									if(clearanceAuth)
									{
								 %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">进口清关：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=clearanceKeyClass%>"><%=clearanceKey.getStatusById(transport.get("clearance",01)) %></span>
									</td>
								</tr>
								<%} %>
								
								<%
									if(certificateAuth)
									{
								 %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">单证流程：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=certificateClass%>"><%=certificateKey.getStatusById(transport.get("certificate",certificateKey.NOCERTIFICATE)) %></span>
									</td>
								</tr>
								<%} %>
							</table>
						</fieldset>
					</td>
					<td align="center" width="25%" valign="top" style="display:none">
						<fieldset class="set">
							<legend>流程基础信息</legend>
							<table style="width:100%;border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">制签流程：</td>
									<td style="width: 80%;" nowrap="nowrap">
									<% 
										if(transport.get("tag",transportTagKey.NOTAG) == transportTagKey.TAG)
										{
											out.print("制签中");
										}
										else
										{
											out.print(transportTagKey.getTransportTagById(transport.get("tag",transportTagKey.NOTAG)));
										}
									%>
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">实物图片：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<%= transportProductFileKey.getStatusById(transport.get("product_file",transportProductFileKey.NOPRODUCTFILE)) %>	
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">质检流程：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<%= transportQualityInspectionKey.getStatusById(transport.get("quality_inspection",transportQualityInspectionKey.NO_NEED_QUALITY)+"") %>
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">单证流程：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<%=certificateKey.getStatusById(transport.get("certificate",certificateKey.NOCERTIFICATE)) %>
									</td>
								</tr>
							</table>
						</fieldset>
					</td>
					<td align="center" width="25%" valign="top">
						<fieldset style="border-color:green;" class="set">
							<legend>运费:<%=computeProductMgrCCC.getSumTransportFreightCost(String.valueOf(transport_id)).get("sum_price",0d) %>RMB</legend>
							<table align="left" style="width:100%">
						 	</table>
					 	</fieldset>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
		<div id="transport_info">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr id="transport_basic_main">
						<td align="center" width="33%">
							<table style="border:0">
									<tr>
										<td style="text-align:right;width: 20%;" nowrap="nowrap">提货仓库：</td>
										<td style="width: 80%;" nowrap="nowrap">
											<%=send_psName %>
										</td>
									</tr>
							</table>
						</td>
						<td align="center" width="33%">
							<table style="border:0">
									<tr>
										<td style="text-align:right;width: 20%;" nowrap="nowrap">货运公司：</td>
										<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_waybill_name") %></td>
									</tr>
							</table>
						</td>
						<td align="center" width="34%">
							<table style="border:0">
									<tr>
										<td style="text-align:right;width: 20%;" nowrap="nowrap">收货仓库：</td>
										<td style="width: 80%;" nowrap="nowrap"><%=recivie_psName %> </td>
									</tr>
							</table>
						</td>
					</tr>
				</thead>
				<tbody>
					<tr id="transport_basic_detail" style="display:none;">
						<td>
						<table border="0" width="100%">
							<tr>
								<td width="30%" align="center" valign="top">
								<fieldset class="set">
									<legend title="提货仓库"><%=send_psName %></legend>
									<table style="width:100%;border:1">
										<tr>
											<td nowrap="nowrap" title="门牌号"><%=null!=transport?transport.getString("send_house_number"):"" %></td>
										</tr>
										<tr>
											<td title="街道"><%=null!=transport?transport.getString("send_street"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="城市"><%=null!=transport?transport.getString("send_city"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="邮编"><%=null!=transport?transport.getString("send_zip_code"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系人"><%=null!=transport?transport.getString("send_name"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系电话"><%=null!=transport?transport.getString("send_linkman_phone"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属省份">
											<%
												long ps_id3 = null!=transport?transport.get("send_pro_id",0L):0L ;
												DBRow psIdRow3 = productMgr.getDetailProvinceByProId(ps_id3);
											%>
											<%=(psIdRow3 != null? psIdRow3.getString("pro_name"):transport.getString("address_state_send") ) %>
											</td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属国家">
											<%
												long ccid3 = null!=transport?transport.get("send_ccid",0L):0L; 
												DBRow ccidRow3 = orderMgr.getDetailCountryCodeByCcid(ccid3);
			 								%>
											<%= (ccidRow3 != null?ccidRow3.getString("c_country"):"" ) %>	
											</td>
										</tr>
									</table>
								</fieldset>
								</td>
								<td width="40%" align="center" valign="bottom">
								
									<%
										int transport_method = transport.get("transportby",0);
										String transport_title = transportWayKey.getStatusById(transport.get("transportby",0));
										String transport_img_src =""; 
										
										if(transport_method == transportWayKey.SEAWAY)
										{
											//transport_title = "海运";
											transport_img_src ="../imgs/air_type.png";
										}
										else if(transport_method == transportWayKey.LANDWAY)
										{
											//transport_title = "陆运";
											transport_img_src ="../imgs/air_type.png";
										}
										else if(transport_method == transportWayKey.AIRWAY)
										{
											//transport_title ="空运";
											transport_img_src = "../imgs/air_type.png";
										}
										else
										{
											//transport_title ="快递";
											transport_img_src = "../imgs/air_type.png";
										}
										
									%>
									<img alt="货运方式:<%=transport_title %>" title="货运方式:<%=transport_title %>" width="176" height="89" src="../imgs/air_type.png" border="0">
									<fieldset class="set">
										<table style="width:100%;border:0">
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">货运公司：</td>
												<td style="width: 80%;" nowrap="nowrap">
													<%=transport.getString("transport_waybill_name") %>
												</td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">运输方式：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transportWayKey.getStatusById(transport.get("transportby",0)) %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">发货港：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_send_place")%></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">运单号：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_waybill_number") %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">承运公司：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("carriers") %></td>
											</tr>
						
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">目的港：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_receive_place")%></td>
											</tr>
										</table>
									</fieldset>
								</td>
								<td width="30%" align="center" valign="top">
								<fieldset class="set">
									<legend title="收货仓库"><%=recivie_psName %></legend>
									<table style="width:100%;border:1">
										<tr>
											<td nowrap="nowrap" title="门牌号"><%=transport.getString("deliver_house_number") %></td>
										</tr>
										<tr>
											<td title="街道"><%=transport.getString("deliver_street") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="城市"><%=transport.getString("deliver_city") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="邮编"><%=transport.getString("deliver_zip_code") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系人"><%=transport.getString("transport_linkman")%></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系电话"><%=transport.getString("transport_linkman_phone")%></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属省份">
											<%
												long ps_id4 = null!=transport?transport.get("deliver_pro_id",0l):0L;
												DBRow psIdRow4 = productMgr.getDetailProvinceByProId(ps_id4);
											%>
											<%=(psIdRow4 != null? psIdRow4.getString("pro_name"):transport.getString("address_state_deliver") ) %>
											</td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属国家">
												<%
													long ccid4 = null!=transport?transport.get("deliver_ccid",0l):0L; 
													DBRow ccidRow4 = orderMgr.getDetailCountryCodeByCcid(ccid4);
												%>
												<%= (ccidRow4 != null?ccidRow4.getString("c_country"):"" ) %>	
											</td>
										</tr>
									</table>
								</fieldset>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div id="tabbar" class="tabdown" style="height:15px;padding-top:0px;text-align:center;margin:0 auto;padding-left:90%; float:inherit ;cursor:pointer;" title="更多""></div>
	<br/>
	<div id="transportTabs" >
		<ul>
			<%
				boolean hasData = false;
			 %>
			<%if(transport.get("stock_in_set",transportStockInSetKey.SHIPPINGFEE_NOTSET) != transportStockInSetKey.SHIPPINGFEE_NOTSET && stockInSetAuth) 
			{
				 hasData = true;
  			%>
			<li ondblclick="opTabs2()"><a href="#transport_price_info" title="运费信息:  <%=stock_in_set_state%> "><span class="<%=stockInSetClass %>">运费信息</span></a></li>
			<%} %>
			
			<%if(transport.get("declaration",declarationKey.NODELARATION) != declarationKey.NODELARATION && declarationAuth) 
			{
				 hasData = true;
	  		 %>
			<li ondblclick="opTabs2()"><a href="#transport_out_info" title="出口报关:  <%=declarationKeyClass_state%>"><span class="<%=declarationKeyClass %>">出口报关 </span></a></li>
			<%} %>
		
			<%if(transport.get("clearance",clearanceKey.NOCLEARANCE) != clearanceKey.NOCLEARANCE && clearanceAuth) 
			{
				hasData = true;
	  		%>
			<li ondblclick="opTabs2()"><a href="#transport_in_info" title="进口清关:  <%=clearanceKeyClass_state%>"><span class="<%= clearanceKeyClass%>">进口清关</span></a></li>
			<%} %>	
		
		<!-- 单证信息 -->
			<%if(transport.get("certificate",certificateKey.NOCERTIFICATE) != certificateKey.NOCERTIFICATE && certificateAuth) 
			{
				hasData = true;
  			%>	
			<li ondblclick="opTabs2()"><a href="#transport_order_prove" title="单证信息:  <%=certificateClass_state%>"><span class='<%= certificateClass%>'>单证信息</span></a></li>
			<%} %>
	  <!--	<li><a href="#product_file" title="实物图片:"><span>实物图片</span></a></li>
			<li><a href="#quality_inspection" title="质检:"><span>质检信息</span></a></li> -->
		</ul>
		
	 <%
	//如果采购单ID为0，为转运单，否则为交货单
	int associateTypeId = 0 == transport.get("purchase_id",0L)?6:5;
 	if(stockInSetInt != transportStockInSetKey.SHIPPINGFEE_NOTSET && stockInSetAuth)
	 {
	 %>
		<div id="transport_price_info" style="display:none">
		<p style="margin-bottom:5px;">
		<!--   运费流程阶段:<span style="color:green;font-weight:bold;"><%=transportStockInSetKey.getStatusById(transport.get("stock_in_set",transportStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>-->	
		</p>
		
		<% double transport_price = computeProductMgrCCC.getSumTransportFreightCost(transport.getString("transport_id")).get("sum_price",0d); %>
		
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="10px">
					<td >
					<%
					double applyTransferFreightTotal = applyFundsMgrZyj.getTransportFreightTotalApply(transport.get("transport_id",0L),associateTypeId);
					%>
	     				<input name="button" type="button" value="运费修改" onClick="showFreightCost()" class="long-button" style="display:"/>
		     		<%
		     		if(transport.get("stock_in_set",1)==2) 
		     		{
		     		%>
			        	<input name="button" type="button" value="申请运费" onClick="applyFreigth('<%=transport_price %>','<%=applyTransferFreightTotal %>','<%=transport.get("purchase_id",0) %>')" class="long-button"/>
		     		<%
		     		}
		     		%>
					
					<input class="short-button" type="button" onclick='stock_in_set(<%=transport_id%>)' value="运费跟进"/>
					<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id%>',5,'运费日志')"/>
					</td>
				</tr>
			</table>
			<table id="tables" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table">
				<thead>
					<tr>
						<th width="21%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>
						<th width="13%" nowrap="nowrap" class="right-title"  style="text-align: center;">小计</th>
					</tr>
				</thead>
				<tbody>
				
				 <%
				 	 DBRow[] rows = freightMgrLL.getTransportFreightCostByTransportId(transport.getString("transport_id"));//更新数据
					 DBRow transportRow = transportMgrLL.getTransportById(transport.getString("transport_id"));;
			    	 float sum_price = 0;
				     if(rows!=null)
				     {
					     for(int i = 0;i<rows.length;i++)
					     {
					    	 DBRow row = rows[i];
					    	 long tfc_id = row.get("tfc_id",0l);
				    		 double tfc_unit_count = row.get("tfc_unit_count",0d);
				    		 String tfc_project_name = "";
				    		 String tfc_way = "";
				    		 String tfc_unit = "";
				    		 String tfc_currency = "";
				    		 double tfc_unit_price = 0;
				    		 double tfc_exchange_rate = 0;
					    	 if(tfc_id != 0) 
					    	 {//以前有数据
					    		 tfc_project_name = row.getString("tfc_project_name");
					    		 tfc_way = row.getString("tfc_way");
					    		 tfc_unit = row.getString("tfc_unit");
					    		 tfc_unit_price = row.get("tfc_unit_price",0d);
					    		 tfc_currency = row.getString("tfc_currency");
					    		 tfc_exchange_rate = row.get("tfc_exchange_rate",0d);
					    	 }
			    			 String bgCol = "#FFFFFF";
		     				if(1 == i%2)
		     				{
		     					bgCol = "#F9F9F9";
		     				}
			      %>
					 <tr height="30px" style="padding-top:3px;background-color: <%=bgCol %>">
			        <td valign="middle" style="padding-top:3px;">
			        	<input type="hidden" name="tfc_id" id="tfc_id" value="<%=tfc_id %>"/>
			        	<input type="hidden" name="tfc_project_name" id="tfc_project_name" value="<%=tfc_project_name %>"/>
			        	<input type="hidden" name="tfc_way" id="tfc_way" value="<%=tfc_way %>"/>
			        	<input type="hidden" name="tfc_unit" id="tfc_unit" value="<%=tfc_unit %>"/>
			        	
						<%=tfc_project_name %>	
			        </td>
			         <td valign="middle" nowrap="nowrap" >       
			      		<%=tfc_way %>
			        </td>
			        <td align="left" valign="middle">
						<%=tfc_unit %>
			        </td>
			        <td >
						<%=tfc_unit_price %>
			        </td>
			        <td >
						<%=tfc_currency%>
			        </td>
			        <td >
						<%=tfc_exchange_rate %>
			        </td>
			        <td valign="middle" >       
						<%=tfc_unit_count %>
			        </td>
			        <td valign="middle" nowrap="nowrap">       
					<%=MoneyUtil.round(tfc_unit_price*tfc_exchange_rate*tfc_unit_count,2)%>
		        </td>
			     </tr>
			     <%
			     		sum_price += tfc_unit_price*tfc_exchange_rate*tfc_unit_count;
			    		}
			     }
			     %>
					<tr>
						<td colspan="2">
							<fieldset class="set" style="border-color:green;">
								<legend>运费:<%=transport_price%>RMB</legend>
					<%
						if(transport.get("stock_in_set",1)==1) 
						{
							out.println("</b></font>");
						}
					%>
					<table style="width:100%" align="left">
					<%
						DBRow[] applyMoneys = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(10015,transport.get("transport_id",0L),associateTypeId);
							//applyMoneyMgrLL.getApplyMoneyByBusiness(transport.getString("transport_id"),associateTypeId);//
						for(int j=0; j<applyMoneys.length; j++)
						{
							String freightCostStr = "";
							if(!"RMB".equals(applyMoneys[j].getString("currency")))
							{
								freightCostStr = "/"+applyMoneys[j].get("standard_money",0f)+"RMB";
							}
					%>	
								<tr><td align='left' style="border: 0px;">
								<a href="javascript:void(0)" onClick='goApplyFunds(<%=applyMoneys[j].get("apply_id",0) %>)'>F<%=applyMoneys[j].get("apply_id",0) %></a>
								<%
									List imageListStock = applyMoneyMgrLL.getImageList(applyMoneys[j].getString("apply_id"),"1");
									String stockStatusName = "";
									//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
								 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneys[j].getString("status")) && imageListStock.size() < 1)
								 	{
								 		stockStatusName = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
								 	}else{
								 		stockStatusName = fundStatusKey.getFundStatusKeyNameById(applyMoneys[j].getString("status"));	
								 	}
								 	stockStatusName = "("+stockStatusName+")";
								%>
								<%=stockStatusName %>
								<%=applyMoneys[j].get("amount",0f) %><%=applyMoneys[j].getString("currency") %><%=freightCostStr %>
								</td></tr>
							<%
						 			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneys[j].getString("apply_id"));
						 				//applyMoneyMgrLL.getApplyTransferByBusiness(transport.getString("transport_id"),6);
						 			if(applyTransferRows.length>0) {
						 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
						 					if(applyTransferRows[ii].get("apply_money_id",0) == applyMoneys[j].get("apply_id",0)) {
							  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
							  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
							  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
							  				int moneyStatus = applyTransferRows[ii].get("status",0);
							  				String transferMoneyStandardFreightStr = "";
							  				if(!"RMB".equals(currency))
							  				{
							  					transferMoneyStandardFreightStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
							  				}
							  				if(moneyStatus != 0 )
							  					out.println("<tr><td align='left' style='border: 0px;'><a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>已转账"+amount+" "+currency+transferMoneyStandardFreightStr+"</td></tr>");
							  				else
							  					out.println("<tr><td align='left' style='border: 0px;'><a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请转账"+amount+" "+currency+transferMoneyStandardFreightStr+"</td></tr>");
							  				}	
						 				}
						 			}
						 		}
						 	%>
				 	</table>
				</fieldset>
					 	</td>
					 	<td colspan="5" align="right">总计</td>
					 	<td nowrap="nowrap"><%=sum_price %></td>
					</tr>
				</tbody>
			</table>
			<br/>
			
			<!--  
			<table style="border: 0; width: 98%">
			 <tr>
	     		<td colspan="10" align="left" style="border: 0">
				<%
					//double applyTransferFreightTotal = applyFundsMgrZyj.getTransportFreightTotalApply(transport.get("transport_id",0L),associateTypeId);
				%>
	     			<input name="button" type="button" value="运费修改" onClick="showFreightCost()" class="long-button" style="display:"/>
		     	<%
		     		//if(transport.get("stock_in_set",1)==2) 
		     		//{
		     	%>
			        	<input name="button" type="button" value="申请运费" onClick="applyFreigth('<%=sum_price %>','<%=applyTransferFreightTotal %>','<%=transport.get("purchase_id",0) %>')" class="long-button"/>
		     	<%
		     		//}
		     	%>
	     		</td>
	     	</tr>
	 </table> -->
			
		</div>
		<%} %>	
		
		
	<!-- 出口报关 -->
	<%if(transport.get("declaration",declarationKey.NODELARATION) != declarationKey.NODELARATION && declarationAuth) 
	{%>	
		<div id="transport_out_info" style="display:none">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="10px">
					<td>
						<input type="button" value="报关跟进" class="short-button" onclick='declarationButton(<%=transport_id%>)'/>
					</td>
				</tr>
			</table>
			<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" >
				<thead>
				<tr>
					<th width="10%" nowrap="nowrap" class="right-title" style="text-align: center;">操作员</th>
					<th width="40%" nowrap="nowrap" class="right-title" style="text-align: center;">内容</th>
					<th width="15%" nowrap="nowrap" class="right-title" style="text-align: center;">预计完成时间</th>
					<th width="15%" nowrap="nowrap" class="right-title" style="text-align: center;">记录时间</th>
					<th width="15%" nowrap="nowrap" class="right-title" style="text-align: center;">操作类型</th>
				</tr>
				</thead>
						<tbody>
						 <%
					    	int[] transport_type = {7};
							DBRow [] transportRows = transportMgrLL.getTransportLogsByTransportIdAndType(transport_id,transport_type);
					     	if(null != transportRows && transportRows.length > 0)
					     	{
					     		for(int i = transportRows.length - 1; i >=0 ; i--)
					     		{
					     			DBRow transRow = transportRows[i];
				   		  %>			
				     			 <tr height="30px">
							     	<td><%=null == adminMgrLL.getAdminById(transRow.getString("transporter_id"))?"":adminMgrLL.getAdminById(transRow.getString("transporter_id")).getString("employe_name") %></td>
							     	<td><%=transRow.getString("transport_content") %>&nbsp;</td>
							     	<td><%=transRow.getString("time_complete") %>&nbsp;</td>
							     	<td><%= tDate.getFormateTime(transRow.getString("transport_date")) %>&nbsp;</td>
				  					<td><%=followuptype.get(transRow.get("transport_type",0))%>&nbsp;</td>
							     </tr>
				     <%		
				     		  }
				     	  }
				     	  else
				     	  {
				     %>		
				     	<tr>
				     		<td colspan="5" align="center" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;"> 无数据 </td>
				     	</tr>
				     <%		
				     	 }
					 %>
					</tbody>
			</table>
		</div>
		<%} %>	
		
		<!--进口清关 -->
	<%if(transport.get("clearance",clearanceKey.NOCLEARANCE) != clearanceKey.NOCLEARANCE && clearanceAuth) {%>
		<div id="transport_in_info" style="display:none">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="10px">
					<td>
						<input type="button"  value="清关跟进" class="short-button" onclick='clearanceButton(<%=transport_id%>)'/>
					</td>
				</tr>
			</table>
			<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable">
				<thead>
					<tr>
						<th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
						<th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
						<th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
						<th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
						<th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
					</tr>
				</thead>
				<tbody>
					 <%
		    			int[] transport_type_clearance = {6};
						DBRow [] transportRowsClearance = transportMgrLL.getTransportLogsByTransportIdAndType(transport_id,transport_type_clearance);
		     			if(null != transportRowsClearance && transportRowsClearance.length > 0)
		     			{
		     				for(int i =( transportRowsClearance.length-1); i >=0 ; i-- )
		     				{
		     					DBRow transRow = transportRowsClearance[i];
		 		    %>			
		     			 <tr height="30px"  >
					     	<td><%=null == adminMgrLL.getAdminById(transRow.getString("transporter_id"))?"":adminMgrLL.getAdminById(transRow.getString("transporter_id")).getString("employe_name") %></td>
					     	<td><%=transRow.getString("transport_content") %>&nbsp;</td>
					     	<td><%=transRow.getString("time_complete") %>&nbsp;</td>
					     	<td><%=transRow.getString("transport_date") %>&nbsp;</td>
		  					<td><%=followuptype.get(transRow.get("transport_type",0))%>&nbsp;</td>
						</tr>
		    	 <%		
		     				}
		     		  }
		     		  else
		     		  {
		   	     %>		
		     	<tr>
		     		<td colspan="5" align="center" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
		     	</tr>
		    	 <%		
		   	 		 }
				%>
				</tbody>
			</table>
		</div>
	<%} %>		
	
	<%if(transport.get("certificate",certificateKey.NOCERTIFICATE) != certificateKey.NOCERTIFICATE && certificateAuth) 
	{%>
		<div id="transport_order_prove" style="display:none"><!-- 初始化隐藏 -->
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="10px">
					<td>
						<input class="short-button" type="button" value="单证跟进" onclick="followUpCerficate();">
						<input class="short-button" type="button" value="查看日志" onclick="showSingleLogs('<%=transport_id%>',9,'单证流程')">
					</td>
				</tr>
			</table>
			<form name="uploadCertificateImageForm" id="uploadCertificateImageForm" value="uploadCertificateImageForm" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/transport/TransportCertificateUpFileAction.action" %>'   method="post">	
				<input type="hidden" name="backurl"  value=""/>
				<input type="hidden" name="file_with_id" value="<%=transport_id %>" />
				<input type="hidden" name="sn" id="sn" value="T_certificate"/>
				<input type="hidden" name="path" id="path" value="<%=systemConfig.getStringConfigValue("file_path_transport") %>"/>
				<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.transport_certificate %>"/>
				<input type="hidden" name="file_names" />
				<input type="hidden" name="file_with_class" id="file_with_class_certificate">
			</form>
			<div id="tabsCertificate" style="margin-top:5px;">
				<ul>
					<%
			 		if(selectedListCertificate != null && selectedListCertificate.size() > 0)
			 		{
			 			for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ )
			 			{
			 			%>
			 				<li><a href="#transport_certificate_<%=selectedListCertificateClass.get(index) %>"> <%=selectedListCertificate.get(index) %></a></li>
			 			<% 	
			 			}
			 		}
			 	%>
				</ul>
				<!-- 遍历出Div 然后显示出来里面的 数据-->
			 	<%
					for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ )
					{
				%>
				<!-- 使用文件类型id -->
				<div id="transport_certificate_<%=selectedListCertificateClass.get(index) %>" style="height:174px; overflow: auto;">
					 <%
			 			List<DBRow> arrayLisTemp = mapCertificate.get(selectedListCertificateClass.get(index));
			 		 %>
			 		<input type="button" class="long-button" onclick="uploadFile('uploadCertificateImageForm','<%=selectedListCertificateClass.get(index) %>');" value="选择文件" />
					<input type="button" class="long-button" onclick="onlineScanner('uploadCertificateImageForm','<%=selectedListCertificateClass.get(index) %>');" value="在线获取" />
						
						<% 
			 			if(arrayLisTemp != null && arrayLisTemp.size() > 0 )
			 			{
			 			%>
			 			<ul style="margin: 0 0 0 5px;width: 100%">
				 			<%for(int listIndex = 0 , total = arrayLisTemp.size() ; listIndex < total ; listIndex++ )
				 			{
			 				 %>
			 				 			<!-- 下面代码修改时transport_refresh_certify.jsp 页面 代码 同步修改，保持内容一致 -->
			 						   <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
			 			 	           <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
			 			 	           <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
			 			 	            <%
							 			 	   String tempFilePath = transport_base_path +"/"+arrayLisTemp.get(listIndex).getString("file_name");
							 			 	   String tempFileName = arrayLisTemp.get(listIndex).getString("file_name");
							 	        %>
			 			 	 	    
			 			 	 	    		<li style="list-style:none;float:left;border: 1px solid #beceeb;margin: 5px 0 0 15px;height: 120px;width: 100px">
							 			 	     	 <!-- 删除按钮 -->
							 			 	    <div align="right" style="height:20px;width:100%;background-color: RGB(238,243,250)" >
							 			 	    <img width="13px" height="13px"  title="下载 " src="../imgs/transport/download-over.png"
							 			 	     onmouseover="this.src='../imgs/transport/download-hover.png'" onmouseout="this.src='../imgs/transport/download-over.png'" 
							 			 	      onclick="downloadFile('<%=tempFileName %>');"
							 			 	      />
							 			 	     	<% if(!(transport.get("certificate",0) == TransportCertificateKey.FINISH ))
							 			 	     	{
			 										%>
							 			 	     	<img width="13px" height="13px"  title="删除 " src="../imgs/transport/del_over.png"   onmouseover="getFcuse(this)" onmouseout="lostFcuse(this)" 
							 			 	     		onclick="deleteFileCommon('<%=arrayLisTemp.get(listIndex).get("file_id",0l)%>','file','<%=systemConfig.getStringConfigValue("file_path_transport") %>','file_id','<%=index %>','<%=selectedListCertificateClass.get(index) %>')" />
							 			 	     	<%} %>
							 			 	     	</div>
							 			 	     	
								 			 	 <%
							 			 	 	 if(StringUtil.isPictureFile(arrayLisTemp.get(listIndex).getString("file_name")))
							 			 	 	 { %>
							 			 	     	<!-- 图片 -->
							 			 	     	<div class="image_div" style="height:100px;width:100%;">
								 			 	     	<a class="image_a" style="height:100%;width:100%;text-decoration:none;line-height: 100%" 
								 			 	     	href='<%=tempFilePath%>' rel="prettyPhoto[gallery<%=selectedListCertificateClass.get(index) %>]" title='<%=tempFileName%>'>	
								 			 	     		<img width="80px" height="100px"  title="<%=tempFileName %> " src="<%=tempFilePath %> " onmouseover="addBorder(this)" onmouseout="hiddenBorder(this)" />
								 			 	     	</a>
							 			 	     	</div>
							 			 	     	<%
				 			 	         		 } 
				 			 	         		 else if(StringUtil.isOfficeFile(arrayLisTemp.get(listIndex).getString("file_name")))
				 			 	         		 {
				 			 	         		 	//office文件
				 			 	 					String officeFileImgPath = "";
				 			 	 					String officefileName = arrayLisTemp.get(listIndex).getString("file_name");
				 			 	 				
				 			 	 					if(officefileName.endsWith(".xls") || officefileName.endsWith(".xlsx"))
				 			 	 					{
				 			 	 						officeFileImgPath ="../imgs/transport/xlsFile.jpg";
				 			 	 					}
				 			 	 					else if(officefileName.endsWith(".doc") || officefileName.endsWith(".docx"))
				 			 	 					{
				 			 	 						officeFileImgPath ="../imgs/transport/wordFile.jpg";
				 			 	 					}
				 			 	 					else if(officefileName.endsWith(".pdf"))
				 			 	 					{
				 			 	 						officeFileImgPath ="../imgs/transport/pdfFile.jpg";
				 			 	 					}
				 			 	 				 %>
							 			 	     <!--office文件 -->
							 			 	     <div class="image_div" style="height:100px;width:100%;" >
							 			 	     	 <a href="javascript:void(0)"  file_id='<%=arrayLisTemp.get(listIndex).get("file_id",0l) %>' 
							 			 	     	 	onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("file_path_transport")%>')" file_is_convert='<%=arrayLisTemp.get(listIndex).get("file_is_convert",0) %>'>
								 			 	      <img width="80px" height="100px"  title="<%=tempFileName %> " src="<%=officeFileImgPath %>"  />
								 			 	     </a>
							 			 	     </div>
							 			 	      <%}
				 			 	          		  else
				 			 	           			 { %>
				 			 	            		<!-- 其他文件 -->
	 		 			 	 	          			 <%} %>
						 			 	    </li>
			 					<%
			 					}	%>
			 					</ul>
			 			<%} %>
				</div>
			<% } %>
			</div>
		</div>
		<%} %>
	 </div>
	 <div id="tabbar2" class="tabdown" style="height:15px;padding-top:0px;text-align:center;margin:0 auto;padding-left:90%; float:inherit ;cursor:pointer;" title="更多""></div>
	
	<form name="download_form" method="post"></form>
	
	<script type="text/javascript">
	//所有流程都没有的，隐藏div，避免影响页面布局
	if("<%=hasData%>"=="false")
	{
		$("#transportTabs").hide();
		$("#tabbar2").hide();
	}
	</script>
	
	<div style="min-height: 30px">
		<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="right">
					<input type="button" value="发货差异" onclick="showDifferent(0)" class="short-button"/>&nbsp;&nbsp;
				</td>
			</tr>
		</table>
	</div>
	<div id="detail" align="left" style="padding:0px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr><td><table id="gridtest"></table><div id="pager2"></div></td></tr>
		</table>
		<br/>
</div>

<script type="text/javascript">
var tabbar2show = false;
$('#tabbar').bind("click",opBasicTab);
function opBasicTab()
{
	if ($("#basic_info_detail").css('display')=='none')
	 {
		$("#basic_info_main").hide();
		$("#basic_info_detail").show();
		$("#transport_basic_main").hide();
		$("#transport_basic_detail").show();
		$("#tabbar").attr("class", "tabup");
	} 
	else 
	{
		$("#basic_info_main").show();
		$("#basic_info_detail").hide();
		$("#transport_basic_main").show();
		$("#transport_basic_detail").hide();
		$("#tabbar").attr("class", "tabdown");
	}
}

$('#tabbar2').bind("click",opTabs2);
function opTabs2()
{
	if (!tabbar2show)
	 {
		$("#transport_price_info").show();
		$("#transport_out_info").show();
		$("#transport_in_info").show();
		$("#transport_order_prove").show();
		$("#tabbar2").attr("class", "tabup");
		tabbar2show=true;
	} else {
		$("#transport_price_info").hide();
		$("#transport_out_info").hide();
		$("#transport_in_info").hide();
		$("#transport_order_prove").hide();
		$("#tabbar2").attr("class", "tabdown");
		tabbar2show=false;
	}
}




$("#transportMainTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tagTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tabsCertificate").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: {expires: 30000},
	load: function(event, ui) {onLoadInitZebraTable();}
});
//var lastsel;
function buttonFormat( cellvalue, options, rowObject )
{
	return "<button onclick='alert(1)' title='a'>上传图片</button><br/><button onclick='alert(2)' title='b'>上传标签</button>";
}
function buttonUnFormat( cellvalue, options, cell)
{  
    return "&nbsp;";  
}
var mygrid = $("#gridtest").jqGrid({
	sortable:true,
	url:'dataTransportOrderDetail.html',//dataDeliveryOrderDetails.html
	datatype: "json",
	width:parseInt(document.body.clientWidth-2),
	height:255,
	autowidth: false,
	shrinkToFit: true,
	postData:{transport_id:<%=transport_id%>},
	jsonReader:{
		id: 'transport_detail_id',
		repeatitems: false
	},
	colNames:['transport_detail_id','商品名','商品条码','货物数','备件数','计划装箱数','V(cm³),W(Kg),运费','所在箱号','商品序列号','使用CLP','使用BLP','transport_id','transport_pc_id','clp_type_id','blp_type_id','批次'], 
	colModel:[ 
		{name:'transport_detail_id',index:'transport_detail_id',hidden:true,sortable:false},
		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
		{name:'p_code',width:40,index:'p_code',align:'left'},
		{name:'transport_delivery_count',width:40,index:'transport_delivery_count',editrules:{required:true,number:true,minValue:0,integer:true},editable:<%=edit%>,sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},
		{name:'transport_backup_count',width:40,index:'transport_backup_count',editrules:{required:true,number:true,minValue:0,integer:true},editable:<%=edit%>,sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},
		{name:'transport_count',width:40,index:'transport_count',editrules:{required:true,number:true,minValue:1},sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},				   	
		{name:'volume_weight_freight',width:70,index:'volume_weight_freight',align:'left'},
		{name:'transport_box',width:40,index:'transport_box',editable:<%=edit%>},
		{name:'transport_product_serial_number',width:50,index:'transport_product_serial_number',editable:<%=edit%>},
		{name:'clp_type',width:30,index:'clp_type',editable:<%=edit%>,edittype:'select'},
		{name:'blp_type',width:30,index:'blp_type',edittype:'select'},
		{name:'transport_id',index:'transport_id',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=transport_id%>},hidden:true,sortable:false},
		{name:'transport_pc_id',index:'transport_pc_id',hidden:true,sortable:false},
		{name:'clp_type_id',index:'clp_type_id',hidden:true,sortable:false},
		{name:'blp_type_id',index:'blp_type_id',hidden:true,sortable:false},
		{name:'lot_number',width:40,index:'lot_number',editable:<%=edit%>,editrules:{required:true},hidden:false,sortable:false},
	],
	rowNum:-1,//-1显示全部
	pgtext:false,
	pgbuttons:false,
	mtype: "GET",
	gridview: true,
	rownumbers:true,
	pager:'#pager2',
	sortname: 'transport_detail_id',
	viewrecords: true,
	sortorder: "asc",
	cellEdit: true,
	cellsubmit: 'remote',
	multiselect: false,
	onSelectCell:function(id,name)
	{
		if(name=="button")
		{
			var product_id = jQuery("#gridtest").jqGrid('getCell',id,'transport_pc_id');
			addProductPicture(product_id);
		}
		
		format(id);
	},
	formatCell:function(id,name,val,iRow,iCol)
	{
		format(id);
	},
   	afterEditCell:function(id,name,val,iRow,iCol)
   	{ 
   		if(name=='p_name')
   		{
   			autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
   		}
   		select_iRow = iRow;
   		select_iCol = iCol;
    },
   	afterSaveCell:function(rowid,name,val,iRow,iCol)
 	{
   		if(name=='p_name'||name=='transport_delivery_count'||name=='transport_backup_count')
   		{
   			ajaxModProductName(jQuery("#gridtest"),rowid);
   			getTransportSumVWP(<%=transport_id%>);
   	  	}
   	  	if(name=='clp_type'||name=='blp_type')
   	  	{
   	  		ajaxModProductName(jQuery("#gridtest"),rowid);
   	  		format(rowid);
		}
   	},
   	beforeSelectRow:function(id, e)
   	{
   		var product_id = jQuery("#gridtest").jqGrid('getCell',id,'transport_pc_id');
   		//alert(id+":"+product_id);
   		return true;
   	},
   	cellurl:'#',
   	errorCell:function(serverresponse, status)
   	{
   		alert(errorMessage(serverresponse.responseText));
   	},
   	editurl:'#',
   	caption:''
});
jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}},{caption:"www",title:"自定义显示字段"});
$("#gview_gridtest a.ui-jqgrid-titlebar-close").attr("style","display:none");
//mygrid.setCaption("wwww");
$("#transportTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
	,show: function(event, ui){
		//alert(ui.index);
	}
});
function beforeShowAdd(formid)
{
	
}
function beforeShowEdit(formid)
{

}
function afterShowAdd(formid)
{
	autoComplete($("#p_name"));
}
function afterShowEdit(formid)
{
	autoComplete($("#p_name"));
}

function errorFormat(serverresponse,status)
{
	return errorMessage(serverresponse.responseText);
}
function refreshWindow(){
	window.location.reload();
}
function ajaxModProductName(obj,rowid,col)
{
	var unit_name;//单字段修改商品名时更换单位专用
	var para ="transport_detail_id="+rowid;
	$.ajax({
		url: '/Sync10/action/administrator/transport/getTransportDetailJSON.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		async:false,
		data:para,
		
		beforeSend:function(request){
		},
		
		error: function(){
			alert("提交失败，请重试！");
		},
		
		success: function(data)
		{
			obj.setRowData(rowid,data);
		}
	});
}
function getTransportSumVWP(transport_id)
{
$.ajax({
	url: '/Sync10/action/administrator/transport/getTransportSumVWPJson.action',
	type: 'post',
	dataType: 'json',
	timeout: 60000,
	cache:false,
	data:{transport_id:transport_id},
	
	beforeSend:function(request){
	},
	error: function(e){
		alert(e);
		alert("提交失败，请重试！");
	},
	
	success: function(date)
	{
		//mygrid.setCaption("总体积:"+date.volume+" cm³ 总货款:"+date.send_price+" RMB 总重量:"+date.weight+" Kg");
		mygrid.setCaption("总体积:"+date.volume+" cm³ 总重量:"+date.weight+" Kg");
		/*
		$("#sum_volume").html("总体积:"+date.volume+" cm³");
		$("#sum_weight").html("总重量:"+date.weight+" Kg");
		$("#sum_price").html("总货款:"+date.send_price+" RMB");
		*/
	}
});
}
function showPictrueOnline0(fileWithType,fileWithId,currentName)
{
	var obj = {
		file_with_type:fileWithType,
		file_with_id:fileWithId,
		current_name:currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'/Sync10/upload/transport'
	}
	if (window.top && window.top.openPictureOnlineShow) {
		window.top.openPictureOnlineShow(obj);
	} else {
		openArtPictureOnlineShow(obj,'/Sync10/');
	}
}
function showPictrueOnline(fileWithType,fileWithId,currentName,product_file_type)
{
    var obj = {
		file_with_type:fileWithType,
		file_with_id:fileWithId,
		current_name:currentName,
		product_file_type:product_file_type,
		cmd:"multiFile",
		table:'product_file',
		base_path:'/Sync10/upload/transport'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'/Sync10/');
	}
}

function format(id)
{
	var product_id = jQuery("#gridtest").jqGrid('getCell',id,'transport_pc_id');
	var clp_type_id = jQuery("#gridtest").jqGrid('getCell',id,'clp_type_id');
	var serial_number = jQuery("#gridtest").jqGrid('getCell',id,'transport_product_serial_number');
	if(serial_number.length>0) {
		$("#gridtest").jqGrid('setColProp','blp_type',{editable:false});
		$("#gridtest").jqGrid('setColProp','clp_type',{editable:false});
	} else {
		$("#gridtest").jqGrid('setColProp','clp_type',{editType:"select",editoptions:{value:getCLPSelect(product_id)}});
		if (clp_type_id!=0) {
			$("#gridtest").jqGrid('setColProp','blp_type',{editable:false});
		} else {
			$("#gridtest").jqGrid('setColProp','blp_type',{editable:true,editType:"select",editoptions:{value:getBLPSelect(product_id,01)}});
		}
	}
	
}
function getCLPSelect(pc_id)
{
}
function getBLPSelect(pc_id,receive_id)
{
}
function addProductPicture(pc_id)
{
	 //添加商品图片
	var transport_id = '<%= transport_id%>';
	var uri = "/Sync10/administrator/transport/transport_product_picture_up.html?transport_id="+transport_id+"&pc_id="+pc_id;
	$.artDialog.open(uri , {title: "商品范例上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,close:function(){window.location.reload();},fixed: true});
}
$(document).ready(function(){
	$(function(){
		$(window).resize(function(){
			jQuery("#gridtest").setGridWidth(document.body.clientWidth-2);
		});
	});
});

getTransportSumVWP(<%=transport_id%>);
</script>


<script type="text/javascript">

function goFundsTransferListPage(id)
{
	window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
}

//查看运费申请单
function goApplyFunds(id)
{
	var id="'F"+id+"'";
	window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=searchby_index&search_mode=1&apply_id="+id);       
}


//下载转运单
function downloadTransportOrder(transport_id)
	{
		var para = "transport_id="+transport_id;
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/downTransportOrder.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
	}
	
//打印转运单		
function print()
{
	visionariPrinter.PRINT_INIT("转运单");
			
	visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
	visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../transport/print_transport_order_detail.html?transport_id=<%=transport_id%>");
				
	visionariPrinter.PREVIEW();
}

//申请运费
function applyFreigth(amount,applyTransferFreightTotal,purchase_id) {
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_money_transport_insert.html?associationId=<%=transport_id%>&amount='+amount+'&subject=1&purchase_id='+purchase_id+"&applyTransferFreightTotal="+applyTransferFreightTotal;
	$.artDialog.open(url, {title: '申请运费',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
//修改运费
function showFreightCost(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+'administrator/transport/setTransportFreight.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>&fr_id=<%=transport.getString("fr_id")%>&purchase_id=<%=transport.get("purchase_id",0) %>';
	$.artDialog.open(uri, {title: '修改运费',width:'700px',height:'500px', lock: true,opacity: 0.3});
}

//修改各流程信息
function updateTransportAllProdures()
{

	//var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_wayout_update.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>";
	
	// administrator/transport/transport_wayout_update.html?
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_flow_update.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>";
	
	$.artDialog.open(uri, {title: '修改各流程信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}

function showTransit()
{
	//var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_transit_update.html?transport_id=<%=transport.getString("transport_id")%>";
	<%
		String uri = "";
		if(transport.get("purchase_id",0l)==0)
		{
			uri = ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_basic_update.html?isOutter=2&transport_id="+transport.getString("transport_id");
		}
		else
		{
			uri = uri = ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_purchase_basic_update.html?isOutter=2&transport_id="+transport.getString("transport_id"); 
		}
	%>
	$.artDialog.open('<%=uri%>', {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}

//修改运单
function showFreight()
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_freight_update.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}

//运费跟进
function stock_in_set(transport_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_stockinset.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "运费流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}

//查看日志
function showSingleLogs(transport_id,transport_type,title){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_show_single_log.html?transport_id="+<%= transport_id%>+"&transport_type="+transport_type;
	$.artDialog.open(uri , {title: title+"["+<%= transport_id%>+"]",width:'870px',height:'470px', lock: true,opacity: 0.3,fixed: true});	
}

//上传商业发票
function uploadInvoice(transport_id)
{
	$.artDialog.open("transport_upload_invoice.html?transport_id="+transport_id, {title: '转运单上传商业发票',width:'670px',height:'400px', lock: true,opacity: 0.3});
}

//上传转运单
function uploadTransportOrderDetail()
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_upload_excel.html?transport_id=<%=transport_id%>';
	$.artDialog.open(url, {title: '上传转运单',width:'800px',height:'500px', lock: true,opacity: 0.3});
		//tb_show('上传转运单','transport_upload_excel.html?transport_id=<%=transport_id%>&TB_iframe=true&height=500&width=800',false);
}
	
//报关跟进
function declarationButton(transport_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_declaration.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "报关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}

//清关跟进
function clearanceButton(transport_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_clearance.html?transport_id="+transport_id; 
	$.artDialog.open(uri , {title: "清关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}

//单证跟进
function followUpCerficate(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_certificate_follow_up.html?transport_id="+<%= transport_id%>;
	$.artDialog.open(uri , {title: "单证跟进["+<%= transport_id%>+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}

function showDifferent(selectIndex)
{
	$.artDialog.open('transport_different_show.html?transport_id=<%=transport_id%>&select_index='+selectIndex, {title: "转运单差异",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){
	//window.location.reload();
	}});
}

//文件上传
function uploadFile(_target,file_with_class){
	
	//设定 将要上的传文件的类型
 	$("#file_with_class_certificate").val(file_with_class);
 
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					// this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   		 }});
}

function onlineScanner(target,file_with_class)
{
	//设定 将要上的传文件的类型
 	$("#file_with_class_certificate").val(file_with_class);
	var _target = "";
	if(target == "uploadCertificateImageForm"){
		_target = "uploadCertificateImageForm";
	}else if(target == "qualityInspectionForm" ){
		_target = "qualityInspectionForm";
	}
    var uri = '/Sync10/' + "administrator/file/picture_online_scanner.html?target="+_target; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}

//jquery file up 回调函数
function uploadFileCallBack(fileNames,_target)
{
    var targetNode = $("#"+_target);
    $("input[name='file_names']",targetNode).val(fileNames);
    if(fileNames.length > 0 && fileNames.split(",").length > 0 )
    {
        $("input[name='file_names']").val(fileNames);
        var myform = $("#"+_target);
        
        //1=装箱单 2=装运包装  3=报关文件 4=清关文件
        var file_with_class = $("#file_with_class_certificate").val();
        
        //上传完毕回调url(暂时保留)
		 $("input[name='backurl']",targetNode).val("<%= backurl%>" + "&file_with_class="+file_with_class);
		 
		 
		 // file_with_class 由表单post提交
		  //myform.submit();
		  
		  //改用ajax提交，局部刷新单证图片数据
		   var url ="<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/transport/TransportCertificateUpFileAction.action" %>";
		   
		  var $tabs = $('#tabsCertificate').tabs();
		   // index-取展示的div索引
		  var index = $tabs.tabs('option', 'selected');
		  
		  data = {
		  backurl:			"<%=backurl%>"+"&index="+index+"&file_with_class="+file_with_class,
		  file_with_id:		'<%=transport_id %>',
		  sn:				'T_certificate',
		  path:				'<%=systemConfig.getStringConfigValue("file_path_transport") %>',
		  file_with_type:	'<%= FileWithTypeKey.transport_certificate %>',
		  file_names:		fileNames,
		  file_with_class: file_with_class
		  };
			$.ajax({
					url:url,
					dataType:'text',
					data:data,
					success:function(html)
					{
						$("#transport_certificate_"+file_with_class).html($.trim(html));
						//上传完毕，重新绑定刷新过来的数据的图片事件
						bindImgEvent();
					}
				});
		  
	}
}

    //file表中的  单证信息的图片在线显示
 function showPictrueOnlinecer(fileWithType,fileWithId , currentName)
 {
    	var file_with_class = $("#file_with_class_certificate").val();
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			file_with_class :file_with_class,
			cmd:"sortMultiFile",
			table:'file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_transport")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow)
	    {
			window.top.openPictureOnlineShow(obj);
		}
		else
		{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}

/**
//选择单证类型，改变tab的焦点	
function fileWithClassCertifycateChange(){
	var file_with_class = $("#file_with_class_certificate").val();
   	$("#tabsCertificate").tabs( "select" , file_with_class * 1-1 );
}	

//选择tab的事件
$("#tabsCertificate").tabs({
 		cache: true,
 		 
		select: function(event, ui){
			 //$("#file_with_class_certificate").val();
			//不需要事件了，上传时 和删除时候 文件类型 作为参数已赋值
		} 
 });
 	 **/
 	 
//删除单证 	 
function deleteFileCommon(file_id , tableName , folder,pk,index,file_with_class){

	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:tableName,file_id:file_id,folder:folder,pk:pk},
		success:function(data){
			if(data && data.flag === "success")
			{
				//window.location.reload();
				refreshCertifyPicture(file_with_class);
			}
			else
			{
				showMessage("系统错误,请稍后重试","error");
			}
		},
		error:function(){
			showMessage("系统错误,请稍后重试","error");
		}
})
} 	 


function refreshCertifyPicture(file_with_class)
{
	$.ajax({
			url:'transport_refresh_certify.html',
			dataType:'text',
			data:{transport_id:'<%=transport_id%>',file_with_class:file_with_class},
			success:function(html)
			{
				$("#transport_certificate_"+file_with_class).html($.trim(html));
				//新加载进来的组件重新绑定事件
				bindImgEvent();
			}
			});
}

//设置鼠标移动到图片上的事件
function getFcuse(obj)
{
	obj.src="../imgs/transport/del_hover.png";
	$(obj).css({
	 "width": "13px",
	 "height": "13px",
	 "cursor": "pointer"
	});
}

function lostFcuse(obj)
{
	obj.src="../imgs/transport/del_over.png";
	$(obj).css({
	 "width": "13px",
	 "height": "13px",
	});
}

//设置鼠标移动到图片上的事件
function addBorder(obj)
{
	$(obj).css({
	 "border-style": "solid",
	 "border-width": "2px ",
	 "border-color": "blue "
	});
}

function hiddenBorder(obj)
{
	$(obj).css({
	 "border-width": "0",
	});
}

//下载单证文件
function downloadFile(fileName)
{
	if(fileName != "" )
	{
		var url = "<%= downLoadFileAction%>?file_name="+fileName+"&folder=<%= systemConfig.getStringConfigValue("file_path_transport")%>";
		window.open(url);
	}
}
 
</script>
<!--  图片预览代码 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>  -->
<link type="text/css" href="../js/picture_online_show/css/prettyPhoto.css" rel="stylesheet" />
<script type="text/javascript" src="../js/picture_online_show/jquery.opacityrollover.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.smoothZoom.min.js"></script>
<script type="text/javascript">

//绑定图片预览事件，局部刷新后 刷新的组件需要重新绑定事件 
function bindImgEvent()
{
	//调用插件
	jQuery(function($) {
			var onMouseOutOpacity = 0.67;
			$('.image_div').opacityrollover({
				mouseOutOpacity : 1.0,
				mouseOverOpacity : onMouseOutOpacity,
				fadeSpeed : 'fast'
			});

			var canvasWidth = 806;
			var canvasHeight = 447;
			if ($(window).width() * 1 < 1000) {
				canvasWidth = 600;
				canvasHeight = 347;
			}

			$("a.image_a")
					.prettyPhoto(
							{
								default_width : canvasWidth,
								default_height : canvasHeight,
								slideshow : false, /* false OR interval time in ms */
								autoplay_slideshow : false, /* true/false */
								opacity : 0.50, /* opacity of background black */
								theme : 'facebook', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
								modal : false, /* If set to true, only the close button will close the window */
								overlay_gallery : false,
								changepicturecallback : setZoom,
								callback : closeZoom,
								social_tools : false,
								image_markup : '<div style="width:'+canvasWidth+'px; height:'+canvasHeight+'px;" ><div id="fullResImage"  ><img id="rotateImg"  src="{path}" /></div></div>',
								fixed_size : true,

								/******************************************
								Enable Responsive settings below if needed.                                                                 
								Max width and height values are optional.
								 ******************************************/
								responsive : false,
								responsive_maintain_ratio : true,
								max_WIDTH : '',
								max_HEIGHT : ''
							});
		
		});
}

//DOM加载完毕后绑定图片预览事件
bindImgEvent();
	
	
function setZoom() {
	var imgObj = new Image();
	imgObj.src = $("#rotateImg").attr('src');
	var sizeImg = imgObj.width>imgObj.height?imgObj.width:imgObj.height;
	$('#fullResImage').css({
		    "width":sizeImg+"px",
		    "height":sizeImg+"px",
		    });
	 if(imgObj.width<imgObj.height){				//高图需要进行横向居中。正好相等则什么也不做 
		    	 $("#rotateImg").css({
					"position" : "absolute",
					"left" : "25%"
				});
	}else if(imgObj.width>imgObj.height){		//宽图需要进行纵向居中
		    	var topsize = Math.floor((1-imgObj.height/imgObj.width)/2*100);//计算应距离上边距百分之多少，并给过宽的图片加行top属性
			   $("#rotateImg").css({
					"position" : "absolute",
					"top" :""+topsize+"%",
				});
	}
	$('#fullResImage').smoothZoom('destroy').smoothZoom();

}

function closeZoom() 
{
	indexZ = 0;
	$('#fullResImage').smoothZoom('destroy');
}
		//图片旋转

var indexZ = 0;
function turnLeft()
 {
	if (indexZ == 360 | indexZ == -360)
	indexZ = 0;
	$("#rotateImg").css({
		"-moz-transform" : "rotate(" + (indexZ -= 90) + "deg)",
		"-webkit-transform" : "rotate(" + (indexZ) + "deg)"
	});

} 

function turnRight() 
{
	if (indexZ == 360 | indexZ == -360)
		indexZ = 0;
	$("#rotateImg").css({
		"-moz-transform" : "rotate(" + (indexZ += 90) + "deg)"
	});
	$("#rotateImg").css({
		"-webkit-transform" : "rotate(" + (indexZ) + "deg)"
	});
}
 

</script>

</body>
</html>