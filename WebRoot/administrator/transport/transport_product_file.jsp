<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*"%>

<jsp:useBean id="fileWithClassKey" class="com.cwc.app.key.FileWithClassKey"/>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>单证流程处理</title>
<%
		String updateTransportAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportProductFileCompleteAction.action";
		long transport_id = StringUtil.getLong(request,"transport_id");
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		TDate tdate = new TDate();
		// 读取系统配置的商品文件的类型
		String valueProductFile = systemConfig.getStringConfigValue("transport_product_file");
	 	String[] arrayProductFileSelected = valueProductFile.split("\n");
	 	String transportProductFileFollowUp = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportProductFileFollowUpAction.action";
		int p=StringUtil.getInt(request,"p");
%>
<script language="javascript" src="../../common.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
		
	 
	 
	<script type="text/javascript">
	
	 function productFileCompelte(){
		var isFlag =  window.confirm("所有商品相关文件都上传了吗？");
	    	 if(isFlag){
	    		 $.ajax({
		    			url:'<%= updateTransportAction%>',
	    			data:{transport_id:'<%= transport_id%>',transport_date:'<%= transport.getString("transport_date")%>'},
		    			dataType:'json',
		    			success:function(data){
		    				if(data && data.flag == "success"){
		    					window.location.reload();
		    				}else{showMessage("系统错误,请稍后重试","alert");}
		    			},
		    			error:function(){
		    			  showMessage("系统错误,请稍后重试","alert");
		    			}
		    		})
	 		   }
	    }
	function refreshWindow(){window.location.reload();}
	function showSingleLogs(transport_id,transport_type){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_show_single_log.html?transport_id="+<%= transport_id%>+"&transport_type="+transport_type;
	 	$.artDialog.open(uri , {title: "实物图片日志["+<%= transport_id%>+"]",width:'870px',height:'470px', lock: true,opacity: 0.3,fixed: true});	
	}
	jQuery(function($){

	    $('#eta').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
			onSelect:function(dateText, inst){
				setContentHtml(dateText);
			}
	    });
	    $("#product_file option[value='<%= transport.get("product_file",transportProductFileKey.PRODUCTFILE)%>']").attr("selected",true);
	    changeProductFileType();
	})
	function changeProductFileType(){
		var product_file = $("#product_file").val();
		if(product_file * 1 == '<%= transportProductFileKey.FINISH %>' * 1){
		   var  notfinish_span = $("#notfinish_span");
		   notfinish_span.css("display","none");
		}else{
		    var  notfinish_span = $("#notfinish_span");
			notfinish_span.attr("style","");
		}
		setContentHtml();
	}
	function setContentHtml(){
		var productFileHTML = $.trim($("#product_file option:selected").html());
		var html = "";
		if($("#product_file").val() * 1 == '<%= transportProductFileKey.FINISH%>'){
			html = "[图片采集完成]完成:"
		}else{
			html = "["+productFileHTML+"]阶段预计"+$("#eta").val()+"完成:";
		}
		 
		var contentNode = $("#context");
		var contentNodeValue = contentNode.val();
		 
		if(contentNodeValue.indexOf("完成:") != -1){
			var index = contentNodeValue.indexOf("完成:")
			html += contentNodeValue.substr(index +3);
			$("#context").val(html);
		}else{
			$("#context").val(html + contentNodeValue);
		}
	}
	function submitForm(){

		var myform = $("#myform");
	 
	 
		$.ajax({
			url:'<%= transportProductFileFollowUp%>',
			dataType:'json',
			data:$("#myform").serialize(),
			success:function(data){
				if(data && data.flag === "success"){
				    cancel();
				    $.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
				}
			},
			error:function(){
				showMessage("系统错误!","error");
			}
		})
	}
	function cancel(){
 		$.artDialog && $.artDialog.close();
 	}
	</script>
	<style type="text/css">
	div.buttonDiv{border: 1px solid #FFFFFF;padding: 5px 0;text-align: right;}
	input.buttonSpecil {background-color: #BF5E26;}
 	.jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
	.specl:hover{background-color: #728a8c;}
	#ui-datepicker-div{z-index:9999;display:none;}
	</style>
</head>

<body>
			<fieldset style="border:2px #cccccc solid;-webkit-border-radius:2px;-moz-border-radius:2px;margin-bottom: 6px;">
			<legend style="font-size:15px;font-weight:normal;color:#999999;">实物图片跟进</legend>
   	 <form id="myform" >
		 
		<input type="hidden" name="transport_id" value="<%=transport_id %>"/>
		  <%
		 	if(transport.get("product_file",transportProductFileKey.PRODUCTFILE) == transportProductFileKey.FINISH){
		 		%>
		 		<input type="hidden" name="product_file" value='<%=transport.get("product_file",transportProductFileKey.FINISH) %>'/>
		 		<%
		 	}
		 %>
		 	<input type="hidden" name="transport_date" value='<%=transport.getString("transport_date") %>' />
		 <table>
		 	<tr>
		 		<td style="text-align:right;width: 12%;">实物图片阶段:</td>
		 		<td align="left" width="15%">
		 			<select name="product_file" id="product_file" <%= (transport.get("product_file",transportProductFileKey.PRODUCTFILE) == transportProductFileKey.FINISH ? "disabled":"") %> onchange="changeProductFileType();">
		 				 <option value="<%= transportProductFileKey.PRODUCTFILE%>"><%= transportProductFileKey.getStatusById(transportProductFileKey.PRODUCTFILE)%></option>
		 				 <option value="<%= transportProductFileKey.FINISH%>"><%= transportProductFileKey.getStatusById(transportProductFileKey.FINISH)%></option>
		 			</select>
		 		</td>
		 		<td align="left">&nbsp;&nbsp;
					<span id="notfinish_span" style=''>
		 		 		预计本阶段完成时间:<input type="text" id="eta" name="eta" value="<%= tdate.getYYMMDDOfNow() %>"/></span>
		 			</span>
		 			<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',10)"/>
				</td>
		 	</tr>
		 	<tr>
		 		<td style="text-align:right;width: 10%;">备注</td>
		 		<td colspan="2" align="left">
		 			<textarea style="width:500px;height:185px;" id="context" name="context"></textarea>
		 		</td>
		 	</tr>
		 </table>
 	 
 	  </form>		
	   <div class="buttonDiv" style="margin-top:5px;"> 
	 	 	<input type="button" id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil"  onclick="submitForm()" value="提交"> 
			<button id="jqi_state0_button取消" value="n" class="jqidefaultbutton specl" name="jqi_state0_button取消" onclick="cancel();">取消</button>
	  </div>
   			</fieldset>
 			<div id="tabs">
  	            <ul>
					   <%if(arrayProductFileSelected != null && arrayProductFileSelected.length > 0){
 									for(int index = 0 , count = arrayProductFileSelected.length ; index < count ; index++ ){
 									String tempStr = arrayProductFileSelected[index];
 									String tempValue = "" ;
 									String tempHtml = "" ;
 									if(tempStr.indexOf("=") != -1){
 										String[] tempArray = tempStr.split("=");
 										tempValue = tempArray[0];
 										tempHtml = tempArray[1];
 									}
 								%>		
 				  <li><a href="transport_product_file_tright.html?transport_id=<%=transport_id %>&product_file_type=<%=tempValue %>&file_type_name=<%=tempHtml %>&p=<%=p %>"><%=tempHtml %></a></li>
 								
 								      
 								<%	} 
 								}
 						%>
			    </ul>
			  </div>
			  <script type="text/javascript">
			  $("#tabs").tabs({
					spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
					cookie: { expires: 30000 } ,
					load: function(event, ui) {onLoadInitZebraTable();}	
				});
			  </script>
<script type="text/javascript">
//stateBox 信息提示框
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
	 var  _self = $("body"),
	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
			 
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
 }
}
</script>
</body>
</html>