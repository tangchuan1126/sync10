<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ClearanceKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
 
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.ArrayList" %>
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>实物图片跟进</title>

<!--  基本样式和javascript -->
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
 
 
<style type="text/css">
*{margin:0px;padding:0px;}
div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .title{ background: url("../js/popmenu/pro_title.jpg") no-repeat scroll left center transparent;color: #FFFFFF;display: table;font-size: 14px; height: 27px;padding-left: 5px; padding-top: 3px;width: 356px;line-height:27px;}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:80px;text-align:center;border:1px solid silver;margin:0px auto;width:300px;height:30px;background:white;line-height:30px;font-size:14px;}	
	#ui-datepicker-div{z-index:9999;display:none;}
</style>
<style type="text/css">
 	select.key{display:none;}
</style>
<%
 
	long transport_id = StringUtil.getLong(request,"transport_id");
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id) ;
	TDate tdate = new TDate();
	TransportProductFileKey productfileKey = new TransportProductFileKey();
	String transportProductFileFollowUp = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportProductFileFollowUpAction.action";
 %>
<script type="text/javascript">
jQuery(function($){

    $('#eta').datepicker({
		dateFormat:"yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect:function(dateText, inst){
			setContentHtml(dateText);
		}
    });
    $("#product_file option[value='<%= transport.get("product_file",productfileKey.PRODUCTFILE)%>']").attr("selected",true);
    changeProductFileType();
})
function changeProductFileType(){
	var product_file = $("#product_file").val();
	if(product_file * 1 == '<%= productfileKey.FINISH %>' * 1){
	   var  notfinish_span = $("#notfinish_span");
	   notfinish_span.css("display","none");
	}else{
	    var  notfinish_span = $("#notfinish_span");
		notfinish_span.attr("style","");
	}
	setContentHtml();
}
function setContentHtml(){
	var productFileHTML = $.trim($("#product_file option:selected").html());
	var html = "";
	if($("#product_file").val() * 1 == '<%= productfileKey.FINISH%>'){
		html = "[图片采集完成]完成:"
	}else{
		html = "["+productFileHTML+"]阶段预计"+$("#eta").val()+"完成:";
	}
	 
	var contentNode = $("#context");
	var contentNodeValue = contentNode.val();
	 
	if(contentNodeValue.indexOf("完成:") != -1){
		var index = contentNodeValue.indexOf("完成:")
		html += contentNodeValue.substr(index +3);
		$("#context").val(html);
	}else{
		$("#context").val(html + contentNodeValue);
	}
}
function submitForm(){
	$.ajax({
		url:'<%= transportProductFileFollowUp%>',
		dataType:'json',
		data:$("#myform").serialize(),
		success:function(data){
			if(data && data.flag){
				if(data.flag == 'success')
				{
					cancel();
					$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
				}
				else
				{
					alert(data.flag);
				}
			}
		},
		error:function(){
			showMessage("系统错误!","error");
		}
	})
}
</script>
</head>
<body>
	  
	 <form id="myform" >
		 
		<input type="hidden" name="transport_id" value="<%=transport_id %>"/>
		  <%
		 	if(transport.get("product_file",productfileKey.PRODUCTFILE) == productfileKey.FINISH){
		 		%>
		 		<input type="hidden" name="product_file" value='<%=transport.get("product_file",productfileKey.FINISH) %>'/>
		 		<%
		 	}
		 %>
		 	<input type="hidden" name="transport_date" value='<%=transport.getString("transport_date") %>' />
		 <table>
		 	<tr>
		 		<td style="text-align:right;">实物图片阶段:</td>
		 		<td>
		 			<select name="product_file" id="product_file" <%= (transport.get("product_file",productfileKey.PRODUCTFILE) == productfileKey.FINISH ? "disabled":"") %> onchange="changeProductFileType();">
		 				 <option value="<%= productfileKey.PRODUCTFILE%>"><%= productfileKey.getStatusById(productfileKey.PRODUCTFILE)%></option>
		 				 <option value="<%= productfileKey.FINISH%>"><%= productfileKey.getStatusById(productfileKey.FINISH)%></option>
		 			</select>
		 			<span id="notfinish_span" style=''>
		 		 		预计本阶段完成时间:<input type="text" id="eta" name="eta" value="<%= tdate.getYYMMDDOfNow() %>"/></span>
		 			</span>
		 			 
		 		</td>
		 		 
		 	</tr>
		  
		 	<tr>
		 		<td style="text-align:right;">备注</td>
		 		<td>
		 			<textarea style="width:400px;height:185px;" id="context" name="context"></textarea>
		 		</td>
		 	</tr>
		 </table>
 	 
 	  </form>		
	   <div class="buttonDiv" style="margin-top:5px;"> 
	 	 	<input type="button" id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil"  onclick="submitForm()" value="提交"> 
			<button id="jqi_state0_button取消" value="n" class="jqidefaultbutton specl" name="jqi_state0_button取消" onclick="cancel();">取消</button>
	  </div>
	  		
 	  		 
 	<script type="text/javascript">
 	function cancel(){
 		$.artDialog && $.artDialog.close();
 	}
 	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 		
 	</script> 
	 
</body>
</html>