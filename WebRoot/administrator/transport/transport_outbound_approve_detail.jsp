<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.ApproveTransportKey"%>
<%@ include file="../../include.jsp"%> 
<%
long tsa_id = StringUtil.getLong(request,"tsa_id");
long transport_id = StringUtil.getLong(request,"transport_id",0);

int approve_status = StringUtil.getInt(request,"approve_status");
String backurl = StringUtil.getString(request,"backurl");

DBRow transportOutboundDifferents[] = transportOutboundApproveMgrZJ.getTransportOutboundApproverDetailsByTsaid(tsa_id,null);

DBRow transportOutboundDifferentsSN[] = transportOutboundApproveMgrZJ.getTransportOutboundApproveDetailSNsByTsaid(tsa_id,null);



boolean checkstorage = false;//扣除多余数量仓库不缺货
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script language="javascript">
<!--
var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}  

function checkForm(theForm)
{
	var reason = theForm.note;
	var tsad_ids = theForm.tsad_ids;
	
	var reasonSN = theForm.noteSN;
	var tsads_ids = theForm.tsads_ids;
	
	var haveEmpty = false;
	var oneSelected = false;
	
	var selectIndex = -1;
	
	//先判断是否数组
	if(typeof tsad_ids.length == "undefined")
	{		
			if (reason.value=="")
			{
				reason.style.background = "#EDF36D";
				haveEmpty = true;
				selectIndex = 0;
			}
	}
	else
	{
		for (i=0; i<tsad_ids.length; i++)
		{
			if (reason[i].value=="")
			{
				reason[i].style.background = "#EDF36D";
				haveEmpty = true;
				selectIndex = 0;
			}
			else
			{
				reason[i].style.background = "";
			}
		}
	}
	
	
	//序列号差异
	if(typeof tsads_ids.length == "undefined")
	{		
			if (reasonSN.value=="")
			{
				reasonSN.style.background = "#EDF36D";
				haveEmpty = true;
				if(selectIndex==-1)
				{
					selectIndex = 1;
				}
			}
	}
	else
	{
		for (i=0; i<tsads_ids.length; i++)
		{
			if (reasonSN[i].value=="")
			{
				reasonSN[i].style.background = "#EDF36D";
				haveEmpty = true;
				if(selectIndex==-1)
				{
					selectIndex = 1;
				}
			}
			else
			{
				reasonSN[i].style.background = "";
			}
		}
	}
	
	if (haveEmpty)
	{
		alert("有商品差异原因没填写");
		
		if(selectIndex!=-1)
		{
			$("#tabs").tabs("select",selectIndex);
		}
		return(false);
	}

	return(true);
}

$(function(){
	 $("#tabs").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
		});	
 })
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();keepAlive()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购及转运应用 » 交货单审核</td>
  </tr>
</table>
<br>
<form name="form1" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/approveTransportOutbound.action" onSubmit="return checkForm(this)">
	<input type="hidden" name="tsa_id" value="<%=tsa_id%>">
	<input type="hidden" name="backurl" value="<%=backurl%>">
<div id="tabs">
	 <ul>
		 <li><a href="#differentCount">数量审核</a></li>
		 <li><a href="#differentSN">序列号审核</a></li>		
	 </ul>
	 <div id="differentCount">
		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		    <tr> 
		        <th width="20%" class="left-title" style="vertical-align: center;text-align: center;">商品名称</th>
		        <th width="28%" align="center" class="right-title" style="vertical-align: center;text-align: center;">商品条码</th>
		        <th width="45%" style="vertical-align: center;text-align: left;" class="right-title">差异原因</th>
		    </tr>
		
		<%
		for (int i=0; i<transportOutboundDifferents.length; i++)
		{
			if(transportOutboundDifferents[i].get("lacking",0)==1)
			{
				checkstorage = true;
			}
		%>
		
		    <tr> 
		      <td width="20%" height="80" align="center" valign="middle">
		      	<font color="<%=transportOutboundDifferents[i].get("lacking",0)==1?"red":""%>">
		      		<%=transportOutboundDifferents[i].getString("product_name")%>
		      	</font>
		      	<input name="tsad_ids" type="hidden"" value="<%=transportOutboundDifferents[i].getString("tsad_id")%>"/>
		      </td>
		      <td align="center" valign="middle"  ><%=transportOutboundDifferents[i].getString("product_code")%></td>
		      <td align="left" valign="middle" id="tad_<%=transportOutboundDifferents[i].getString("tsad_id")%>" >
		      <table width="98%" border="0" cellspacing="4" cellpadding="0">
		          <tr>
		            <th  style="vertical-align: center;text-align: left;font-size:15px;color:#333333" >实际发货量：<span style="font-weight:bold;color:#0000FF"><%=transportOutboundDifferents[i].getString("transport_send_count")%></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 应发货量：<span style="font-weight:bold;color:#FF0000"><%=transportOutboundDifferents[i].getString("transport_count")%></span>	
					</th>
		          </tr>
		          <tr>
		            <th  style="vertical-align: center;text-align: left;font-size:15px;">
						<%
			if (approve_status==ApproveTransportKey.APPROVE||transportOutboundDifferents[i].get("approve_status",0)==ApproveTransportKey.APPROVE)
			{
			%>
					<span style="color:#990000;font-weight:normal"><%=transportOutboundDifferents[i].getString("note")%></span>
			<%
			}
			else
			{
			%>
				<input type="text" name="note_<%=transportOutboundDifferents[i].getString("tsad_id")%>" id="note" style="width:400px;height:25px;font-size:14px;border:#cccccc solid 1px;color:#FF0000" value="<%=transportOutboundDifferents[i].getString("note")%>"> 
			<%
			}
			%>
					
					</th>
		          </tr>
		        </table>
		     </td>
		  </tr>
		<%
		}
		%>
		</table>
		<br>
		<table width="98%" border="0" align="center" cellpadding="8" cellspacing="0">
		  <tr>
		    <td align="center" valign="middle" bgcolor="#eeeeee">
			<%
			if (approve_status==ApproveTransportKey.WAITAPPROVE&&checkstorage==false)
			{
			%>
			  <input name="Submit" type="submit" class="long-button-redtext" value="审核通过"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<%
			}
			%>
		    
		      <input name="Submit2" type="button" class="long-button" value="返回" onClick="history.back();">
		    </td>
		  </tr>
		</table>
	</div>
	<div id="differentSN">
		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		    <tr> 
		        <th width="20%" class="left-title" style="vertical-align: center;text-align: center;">商品名称</th>
		        <th width="14%" align="center" class="right-title" style="vertical-align: center;text-align: center;">商品条码</th>
		        <th width="14%" align="center" class="right-title" style="vertical-align: center;text-align: center;">序列号</th>
		        <th width="45%" style="vertical-align: center;text-align: left;" class="right-title">差异原因</th>
		    </tr>
		    <%
		    	for(int i = 0;i<transportOutboundDifferentsSN.length;i++)
		    	{
		    %>
		    <tr> 
		      <td width="20%" height="80" align="center" valign="middle">
		      	<font color="<%=transportOutboundDifferentsSN[i].get("lacking",0)==1?"red":""%>">
		      		<%=transportOutboundDifferentsSN[i].getString("product_name")%>
		      	</font>
		      	<input name="tsads_ids" type="hidden" value="<%=transportOutboundDifferentsSN[i].getString("tsads_id")%>"/>
		      </td>
		      <td align="center" valign="middle"><%=transportOutboundDifferentsSN[i].getString("product_code")%></td>
		      <td align="center" valign="middle"><%=transportOutboundDifferentsSN[i].getString("serial_number")%></td>
		      <td align="left" valign="middle" id="tad_<%=transportOutboundDifferentsSN[i].getString("tsad_id")%>" >
		      <table width="98%" border="0" cellspacing="4" cellpadding="0">
		          <tr>
		            <th  style="vertical-align: center;text-align: left;font-size:15px;color:#333333" >实际发货量：<span style="font-weight:bold;color:#0000FF"><%=transportOutboundDifferentsSN[i].getString("transport_send_count")%></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 应发货量：<span style="font-weight:bold;color:#FF0000"><%=transportOutboundDifferentsSN[i].getString("transport_count")%></span>	
					</th>
		          </tr>
		          <tr>
		            <th  style="vertical-align: center;text-align: left;font-size:15px;">
						<%
						if (approve_status==ApproveTransportKey.APPROVE||transportOutboundDifferentsSN[i].get("approve_status",0)==ApproveTransportKey.APPROVE)
						{
						%>
								<span style="color:#990000;font-weight:normal"><%=transportOutboundDifferentsSN[i].getString("note")%></span>
						<%
						}
						else
						{
						%>
							<input type="text" name="noteSN_<%=transportOutboundDifferentsSN[i].getString("tsad_id")%>" id="noteSN" style="width:400px;height:25px;font-size:14px;border:#cccccc solid 1px;color:#FF0000" value="<%=transportOutboundDifferentsSN[i].getString("note")%>"> 
						<%
						}
						%>
					
					</th>
		          </tr>
		        </table>
		     </td>
		  </tr>
		    <%
		    	}
		    %>
		 </table>
		 <br>
		<table width="98%" border="0" align="center" cellpadding="8" cellspacing="0">
		  <tr>
		    <td align="center" valign="middle" bgcolor="#eeeeee">
			<%
			if (approve_status==ApproveTransportKey.WAITAPPROVE)
			{
			%>
			  <input name="Submit" type="submit" class="long-button-redtext" value="审核通过"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<%
			}
			%>
		      <input name="Submit2" type="button" class="long-button" value="返回" onClick="history.back();">
		    </td>
		  </tr>
		</table>
	</div>
</div>
</form>
<div id="keep_alive" style="display:none"></div>
<br>
<br>
</body>
</html>
