<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.LoadUnloadRelationTypeKey"%>
<%@page import="com.cwc.app.key.TransportRegistrationTypeKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>选择装缷门或位置</title>
<%
	TDate td = new TDate();
	td.addHour(1);
	String	book_start_time = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay()+" "+DateUtil.getStrCurrHour()+":"+DateUtil.getStrCurrMinute();
	String 	book_end_time =	  td.getStringYear()+"-"+td.getStringMonthDouble()+"-"+td.getStringDayDouble()+" "+td.getStringHourDouble()+":"+td.getStringMinuteDouble();
	long transport_id =	StringUtil.getLong(request, "transport_id");
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
	long send_psid	= transport.get("send_psid", 0L);
	long receive_psid	= transport.get("receive_psid", 0L);
	int rel_occupancy_use = StringUtil.getInt(request, "rel_occupancy_use");
	long psId = 0;
	if(TransportRegistrationTypeKey.SEND == rel_occupancy_use)
	{
		psId = send_psid;
	}
	else if(TransportRegistrationTypeKey.DELEIVER == rel_occupancy_use)
	{
		psId = receive_psid;
	}
	if(0 == psId)
	{
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean( request.getSession(true) );
		psId = adminLoggerBean.getPs_id();
	}
%>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<script language="javascript" src="../../common.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script src="../js/datepicker/date_HHMMSS.js" type="text/javascript"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript">
$.blockUI.defaults = {
		  css: { 
		   padding:        '8px',
		   margin:         0,
		   width:          '170px', 
		   top:            '45%', 
		   left:           '40%', 
		   textAlign:      'center', 
		   color:          '#000', 
		   border:         '3px solid #999999',
		   backgroundColor:'#eeeeee',
		   '-webkit-border-radius': '10px',
		   '-moz-border-radius':    '10px',
		   '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		   '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		  },
		  //设置遮罩层的样式
		  overlayCSS:  { 
		   backgroundColor:'#000', 
		   opacity:        '0.6' 
		  },
		  baseZ: 99999, 
		  centerX: true,
		  centerY: true, 
		  fadeOut:  1000,
		  showOverlay: true
		 };
$(function(){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/door_use_info_list.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		beforeSend:function(request){
		},
		error: function(){
			showMessage("加载门占用信息失败","error");
		},
		success: function(html){
			$("#door").html(html);
		}
	});
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/location_use_info_list.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		beforeSend:function(request){
		},
		error: function(){
			showMessage("加载位置占用信息失败","error");
		},
		success: function(html){
			$("#location").html(html);
		}
	});
	$("#isSearchByDate").val(2);
});
function addSearchCondition()
{
	var searchCondition = "";
	if(1 == $("#isSearchByDate").val())
	{
		var starttime=new Date(($("#book_start_time").val()).replace(/-/g,"/"));
	    var endtime=new Date(($("#book_end_time").val()).replace(/-/g,"/"));
	    if(starttime < endtime)
		{
	    	searchCondition = '&cmd='+$("#cmd").val()+'&book_start_time='+$("#book_start_time").val()+'&book_end_time='+$("#book_end_time").val();
		}
	    else
	    {
	    	alert("结束时间需晚于开始时间");
	    	return;
	    }
	}
	return searchCondition;
}
function goDoor(index)
{
    $.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/door_use_info_list.html',
		type: 'post',
		data: 'pDoor='+index+addSearchCondition(),
		dataType: 'html',
		timeout: 60000,
		cache:false,
		beforeSend:function(request){
		},
		error: function(){
			showMessage("加载门占用信息失败","error");
		},
		success: function(html){
			$("#door").html(html);
		}
	});
}
function goLoc(index){
    $.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/location_use_info_list.html',
		type: 'post',
		data: 'pLoc='+index+addSearchCondition(),
		dataType: 'html',
		timeout: 60000,
		cache:false,
		beforeSend:function(request){
		},
		error: function(){
			showMessage("加载位置占用信息失败","error");
		},
		success: function(html){
			$("#location").html(html);
		}
	});
}
function submitData(){
	if($("#choosed_door_or_location_table tr").length > 1)
	{
		var isCanSub = true;
		var allStartTimes = $(".testOccupancyStartInput");
		var allEndTimes = $(".testOccupancyEndInput");
		for(var i = 0; i < allStartTimes.length; i ++)
		{
			if(null == $(allStartTimes[i]).val() || '' == $(allStartTimes[i]).val())
			{
				alert("请填写门/位置的开始占用时间");
				isCanSub = false;
				break;
			}
		}
		/*
		if(isCanSub)
		{
			for(var i = 0; i < allEndTimes.length; i ++)
			{
				if(null == $(allEndTimes[i]).val() || '' == $(allEndTimes[i]).val())
				{
					alert("请填写门/位置的结束占用时间");
					isCanSub = false;
					break;
				}
			}
		}
		if(isCanSub)
		{
			for(var i = 0; i < allStartTimes.length; i ++)
			{
				var starttime=new Date(($(allStartTimes[i]).val()).replace(/-/g,"/"));
			    var endtime=new Date(($(allEndTimes[i]).val()).replace(/-/g,"/"));
				if(starttime >= endtime)
				{
					alert("结束时间需晚于开始时间");
					isCanSub = false;
					break;
				}
			}
			
		}
		*/
		if(null == $("#adminUserNames").val() || '' == $("#adminUserNames").val())
		{
			alert("请选择负责人");
		}
		else
		{
			if(isCanSub)
			{
				if($("input:checkbox[name=isMail]").attr("checked"))
			   	{
					$("#isNeedMail").val(2);
			   	}
			   	if($("input:checkbox[name=isMessage]").attr("checked"))
			   	{
					$("#isNeedMessage").val(2);
			   	}
			   	if($("input:checkbox[name=isPage]").attr("checked"))
			   	{
					$("#isNeedPage").val(2);
			   	}
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				 $.ajax({
						url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/AddDoorOrLocationUseInfoAction.action',
						data:$("#subForm").serialize(),
						dataType:'json',
						type:'post',
						beforeSend:function(request){
							$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
						},
						success:function(data){
							$.unblockUI();
							if(data && data.flag == "true")
							{
								setTimeout("windowClose()", 1000);
							}
							else
							{
								alert("门/位置已经被占用，请重新选择门/位置或更改占用时间");
							}
						},
						error:function(){
							$.unblockUI();
						}
					})	
			}
		}
	}
	else
	{
		alert("请选择门或位置");
	}
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function closeWin()
{
	$.artDialog && $.artDialog.close();
}
function isRightDateFormat(obj)
{
	if(null != obj.value && "" != obj.value)
	{
		//  \s  /^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))(\s(([01]\d{1})|(2[0123])):([0-5]\d):([0-5]\d))?$/;
		var reg = /^(\d{4}-\d{2}-\d{2}\s{1}\d{2}:([0-5]\d))?$/;
		if(!reg.test(obj.value))
		{
			alert("请输入正确的时间格式:2013-07-02 11:30");
		}
	}
}
function chooseStorage(obj)
{
	var objValue		= obj.value;
	var infoArr			= objValue.split("_");
	var occupancyType	= infoArr[0];
	var occupancyTypeNa = infoArr[1];
	var occupancyId		= infoArr[2];
	var occupancyName	= infoArr[3];
	var occupancyTypeAndId	= occupancyType + "_" + occupancyId;

	if($("#choosed_door_or_location_table tr[id='"+occupancyTypeAndId+"']").attr("id"))
	{
		if(!$("input:checkbox[value='"+objValue+"']").attr("checked"))
		{
			$("#choosed_door_or_location_table tr[id='"+occupancyTypeAndId+"']").remove();
		}
	}
	else
	{
		if($("input:checkbox[value='"+objValue+"']").attr("checked"))
		{
			addChoosedDoorOrLocationTr(occupancyType,occupancyTypeNa,occupancyId,occupancyName);
		}
	}
	
}
function addChoosedDoorOrLocationTr(occupancyType,occupancyTypeNa,occupancyId,occupancyName)
{
	var html = '<tr id=' + (occupancyType+"_"+occupancyId) + '>'
			+'		<td>' + occupancyName+'&nbsp;('+occupancyTypeNa+')'
			+'			<input type="hidden" name="occupancy_types" value="'+occupancyType+'"/>'
			+'			<input type="hidden" name="occupancy_ids" value="'+occupancyId+'"/>'
			+'		</td>'
			+'		<td>'
			+'			<input maxlength="100" value="'+$("#book_start_time").val()+'" name="book_start_times" onclick="SelectDate(this,\'yyyy-MM-dd hh:mm\',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}" class="testOccupancyStartInput"/>&nbsp;'
			//+'			--<input maxlength="100" value="'+$("#book_end_time").val()+'" name="book_end_times" onclick="SelectDate(this,\'yyyy-MM-dd hh:mm\',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}" class="testOccupancyEndInput"/>'
			+'		</td>'
			+'		<td>'
			+'			<a href="javascript:void(0)" onclick="deleteThisTr(this,\''+(occupancyType+"_"+occupancyId+"_"+occupancyName)+'\')" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>'
			+'		</td>'
			+'	</tr>';
	$("#choosed_door_or_location_table tr:last").after($(html));
}
function deleteThisTr(delBut,trValue)
{
	$(delBut).parent().parent().remove();
	$("input:checkbox[value='"+trValue+"']").attr("checked",false);
}
function loadCanOrProbableLoadDoorOrLocation(canOrProbable)
{
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	var starttime=new Date(($("#book_start_time").val()).replace(/-/g,"/"));
    var endtime=new Date(($("#book_end_time").val()).replace(/-/g,"/"));
    $("#isSearchByDate").val(1);
    $(".long-button").css("color", "black");
    $("#"+canOrProbable).css("color", "red");
    $("#cmd").val(canOrProbable);
    
	if(starttime < endtime)
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/door_use_info_list.html',
			type: 'post',
			data:{cmd:canOrProbable, book_start_time:$("#book_start_time").val(), book_end_time:$("#book_end_time").val()},
			dataType: 'html',
			timeout: 60000,
			cache:false,
			beforeSend:function(request){
				
			},
			error: function(){
				showMessage("加载门占用信息失败","error");
				$.unblockUI();
			},
			success: function(html){
				$("#door").html(html);
				//$.unblockUI();
			}
		});
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/location_use_info_list.html',
			type: 'post',
			data:{cmd:canOrProbable, book_start_time:$("#book_start_time").val(), book_end_time:$("#book_end_time").val()},
			dataType: 'html',
			timeout: 60000,
			cache:false,
			beforeSend:function(request){
			},
			error: function(){
				showMessage("加载位置占用信息失败","error");
				$.unblockUI();
			},
			success: function(html){
				$("#location").html(html);
				//$.unblockUI();
			}
		});
		$.unblockUI();
	}
	else
	{
		alert("结束时间需晚于开始时间");
	}
}
function selectAdminUsers(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUsers").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setSelectAdminUsers'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
};
function setSelectAdminUsers(user_ids , user_names){
	$("#adminUserIds").val(user_ids);
	$("#adminUserNames").val(user_names);
}
function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);};
</script>

</head>
<body style="margin-top: 0px; margin-bottom: 0px;">
<input type="hidden" id="isSearchByDate"/>
<div style="width: 100%; height: 100%;margin-top: 0px; margin-bottom: 0px; ">
<table style="width: 100%; height: 100%;">
	<tr style="width: 100%;height:10%;" valign="top">
		<td colspan="2" align="left" valign="top" style="height: 100%; width: 100%;">	
			<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;width: 90%;margin-top: 10px;margin-bottom: 10px;">
				<legend style="font-size:15px;font-weight:normal;color:#999999;">
					<%
						if(TransportRegistrationTypeKey.SEND == rel_occupancy_use)
						{
							out.println("所选装货门/位置");
						}
						else if(TransportRegistrationTypeKey.DELEIVER == rel_occupancy_use)
						{
							out.println("所选缷货门/位置");
						}
					%>
				</legend>	
				<form action="" id="subForm" method="post">
					<input type="hidden" name="rel_type" id="rel_type" value='<%=ProductStoreBillKey.TRANSPORT_ORDER %>'/>
					<input type="hidden" name="rel_id" id="rel_id" value='<%=transport_id %>'/>
					<input type="hidden" name="ps_id" value='<%=psId %>'/>
					<input type="hidden" name="is_registration" id="is_registration" value='1'/>
					<input type="hidden" name="rel_occupancy_use" value='<%=rel_occupancy_use %>'/>
					<table width="100%" border="0" cellspacing="3" cellpadding="2" style="border-bottom: 2px #cccccc solid; margin-bottom: 3px;">
						<tr>
							<td width="30%">
								负责人:<input name="adminUserNames" id="adminUserNames" value="" onclick="selectAdminUsers()"/>
								<input type="hidden" name="adminUserIds" id="adminUserIds" value=""/>
							</td>
							<td colspan="2">
								通知：
						   		<input type="checkbox"  name="isMail" id="isMail" checked="checked"/>邮件
						   		<input type="checkbox"  name="isMessage" id="isMessage" checked="checked"/>短信
						   		<input type="checkbox"  name="isPage" id="isPage" checked="checked"/>页面
						   		<input type="hidden"  name="isNeedMail" id="isNeedMail"/>
						   		<input type="hidden"  name="isNeedMessage" id="isNeedMessage" />
						   		<input type="hidden"  name="isNeedPage" id="isNeedPage" />
							</td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="3" cellpadding="2" id="choosed_door_or_location_table" style="margin-top: 5px;">
	        			<tr>
							<td width="30%">门/位置</td><td width="60%">预定开始时间</td><td width="10%">操作</td>
						</tr>
					</table>
				</form>
			</fieldset>
		</td>
	</tr>
	<tr style="width: 100%;height:10%;" valign="top">
		<td colspan="2" align="left" valign="top" style="height: 100%; width: 100%;">	
			<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;width: 90%;margin-top: 10px;margin-bottom: 10px;">
				<legend style="font-size:15px;font-weight:normal;color:#999999;">
					<%
						if(TransportRegistrationTypeKey.SEND == rel_occupancy_use)
						{
							out.println("搜索装货门/位置");
						}
						else if(TransportRegistrationTypeKey.DELEIVER == rel_occupancy_use)
						{
							out.println("搜索缷货门/位置");
						}
					%>
				</legend>	
				<form action="" id="searchForm" method="post">
					<input type="hidden" name="choosed_doors_or_locations_type_ids" id="choosed_doors_or_locations_type_ids"/>
					<table width="100%" border="0" cellspacing="3" cellpadding="2">
	        			<tr>
							<td>
								预定开始时间
							</td>
							<td>
								<input value='<%=book_start_time %>' maxlength="100"  id="book_start_time" name="book_start_time" onclick="SelectDate(this,'yyyy-MM-dd hh:mm',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>&nbsp;
								--&nbsp;<input value='<%=book_end_time %>' maxlength="100" id="book_end_time" name="book_end_time" onclick="SelectDate(this,'yyyy-MM-dd hh:mm',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>
								<input type="hidden" name="cmd" id="cmd"/>
<%--								<input type="radio" name="occupancy_type" value="1" onclick="loadCanLoadDoorOrLocation(this)"/>门&nbsp;--%>
<%--								<input type="radio" name="occupancy_type" value="2" onclick="loadCanLoadDoorOrLocation(this)"/>位置--%>
								<input type="button" class="long-button" style="color: black;" id="canOccupancy" value="满足条件" onclick="loadCanOrProbableLoadDoorOrLocation('canOccupancy')"/>
<%--								<input type="button" class="long-button" style="color: black;" id="probableOccupancy" value="可能满足条件" onclick="loadCanOrProbableLoadDoorOrLocation('probableOccupancy')"/>--%>
							</td>
						</tr>
						<tr style="width: 80%;">
							<td style="height: 100%" colspan="2">
								<div class="demo" style="width:98%;margin:0px auto;" >
									<div id="tabs" style="margin-top:15px;">
										<ul>
											<li><a href="#door">装卸门</a></li>
											<li><a href="#location">装卸位置</a></li>
										</ul>
										<div id="door"></div>
										<div id="location"></div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</form>
			</fieldset>
		</td>
	</tr>
  <tr style="height: 10%;">
		<td align="right" valign="bottom" style="width: 100%; height: 100%;">				
			<table style="height: 100%;width: 100%;">
				<tr style="height: 100%;width: 100%;">
					<td colspan="2" align="right" valign="middle" style="height: 100%;width: 100%;" class="win-bottom-line">				
						<input type="button" class="normal-green" value="提交" onclick="submitData();"/>
						<input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>		
	<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>