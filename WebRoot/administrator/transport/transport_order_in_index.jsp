<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.ProductStorageTypeKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.DeclarationKey"%>
<%@page import="com.cwc.app.key.ClearanceKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@page import="com.cwc.app.key.LoadUnloadRelationTypeKey"%>
<%@page import="com.cwc.app.key.LoadUnloadOccupancyStatusKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.TransportRegistrationTypeKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<jsp:useBean id="purchasekey" class="com.cwc.app.key.PurchaseKey" />
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey" />
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey" />
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey" />
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey" />
<jsp:useBean id="fileWithTypeKey"
	class="com.cwc.app.key.FileWithTypeKey" />
<jsp:useBean id="transportCertificateKey"
	class="com.cwc.app.key.TransportCertificateKey" />
<jsp:useBean id="transportTagKey"
	class="com.cwc.app.key.TransportTagKey" />
<jsp:useBean id="transportStockInSetKey"
	class="com.cwc.app.key.TransportStockInSetKey" />
<jsp:useBean id="transportQualityInspectionKey"
	class="com.cwc.app.key.TransportQualityInspectionKey" />
<jsp:useBean id="transportProductFileKey"
	class="com.cwc.app.key.TransportProductFileKey" />
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate" />
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%>
<%
	String key = StringUtil.getString(request,"key");
	
	long send_psid = StringUtil.getLong(request,"send_psid",0);
	long receive_psid = StringUtil.getLong(request,"receive_psid",0);
	int status = StringUtil.getInt(request,"status",0);
	int declaration = StringUtil.getInt(request,"declarationStatus",0);
	int clearance = StringUtil.getInt(request,"clearanceStatus",0);
	int invoice = StringUtil.getInt(request,"invoiceStatus",0);
	int drawback = StringUtil.getInt(request,"drawbackStatus",0);
	int day = StringUtil.getInt(request,"day",3);
	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	int analysisType = StringUtil.getInt(request,"analysisType",0);
	int analysisStatus = StringUtil.getInt(request,"analysisStatus",0);
	int transport_status = StringUtil.getInt(request,"transport_status",0);
	int stock_in_set = StringUtil.getInt(request,"stock_in_set",0);
	long dept = StringUtil.getLong(request,"dept",0);
	long create_account_id = StringUtil.getLong(request,"create_account_id",0);
	long dept1 = StringUtil.getLong(request,"dept1",0);
	long create_account_id1 = StringUtil.getLong(request,"create_account_id1",0);
	
	long product_line_id = StringUtil.getLong(request,"product_line_id");
	String store_title = StringUtil.getString(request,"store_title");
	String product_line_title = StringUtil.getString(request,"product_line_title");
	
	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(systemConfig.getIntConfigValue("order_page_count"));
	
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
	
	String input_st_date,input_en_date;
	if ( st.equals("") )
	{	
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		st = input_st_date;
	}
	else
	{	
		input_st_date = st;
	}
									
	if ( en.equals("") )
	{	
		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		en = input_en_date;
	}
	else
	{	
		input_en_date = en;
	}
	
	String cmd = StringUtil.getString(request,"cmd");
	int search_mode = StringUtil.getInt(request,"search_mode");
	
	// modifyed by liuyi at 2014-7-23 14:21
	AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean( request.getSession(true) );
	long psId = adminLoggerBean.getPs_id();
	//默认 到达仓库为本人所在仓库
	if(cmd.equals("")) 
	{
	 	receive_psid=psId;
	}
	//标志 收货
	String flag = "in";
	DBRow[] rows; 
	if(cmd!=null)
	{
		if(cmd.equals("filter"))
		{
			rows = transportMgrLY.fillterTransport(send_psid,receive_psid,pc,status, declaration, clearance, invoice, drawback,0,stock_in_set, create_account_id);
		}
		else if(cmd.equals("search"))
		{
			rows = transportMgrZJ.searchTransportByNumber(key,search_mode,pc);
		}
		else if(cmd.equals("followup")) 
		{
			rows = transportMgrZJ.fillterTransport(send_psid,receive_psid,pc,transport_status, declaration, clearance, invoice, drawback,day,stock_in_set, create_account_id1);
		}
		else if(cmd.equals("analysis"))
		{
			rows = transportMgrLL.getAnalysis(st,en,analysisType,analysisStatus,day,pc);
		}
		else if(cmd.equals("ready_delivery"))
		{
			rows = transportMgrZJ.getNeedTrackReadyDelivery(product_line_id,pc);
		}
		else if(cmd.equals("tag_delivery"))
		{
			rows = transportMgrZJ.getNeedTrackTagDelivery(product_line_id,pc);
		}
		else if(cmd.equals("third_tag_delivery"))
		{
			rows = transportMgrZJ.getNeedTrackThirdTagDelivery(product_line_id,pc);
		}
		else if(cmd.equals("quality_inspection_delivery"))
		{
			rows = transportMgrZJ.getNeedTrackQualityInspectionDelivery(product_line_id,pc);
		}
		else if(cmd.equals("product_file_delivery"))
		{
			rows = transportMgrZJ.getNeedTrackProductFileDelivery(product_line_id,pc);
		}
		else if(cmd.equals("ready_send_ps")||cmd.equals("packing_send_ps")||cmd.equals("tag_send_ps")||cmd.equals("product_file_send_ps")||cmd.equals("quality_inspection_send_ps"))
		{
			rows = transportMgrZJ.getNeedTrackSendTransportByPsid(send_psid,cmd,pc);
		}
		else if(cmd.equals("intransit_receive_ps")||cmd.equals("alreadyRecive_receive_ps"))
		{
			rows = transportMgrZJ.getNeedTrackReceiveTransportByPsid(receive_psid,cmd,pc);
		}
		else if(cmd.equals("track_certificate")||cmd.equals("track_clearance")||cmd.equals("track_declaration"))
		{
			rows = transportMgrZJ.trackOceanShippingTransport(cmd,pc);
		}
		else
		{
		    //收货 -- 默认获取 接收地址为登录员工 所在仓库  
			rows = transportMgrLY.fillterTransport(0,receive_psid,pc,0,0,0,0,0,0,0,0);
		}
	}
	else
	{
		rows = deliveryMgrZJ.getAllDeliveryOrder(pc);
	}
	
	TransportOrderKey transportOrderKey = new TransportOrderKey();
	DBRow[] accounts = applyMoneyMgrLL.getAllByReadView("accountReadViewLL");
	DBRow[] accountCategorys = applyMoneyMgrLL.getAllByTable("accountCategoryBeanLL");
	DBRow[] adminGroups = applyMoneyMgrLL.getAllByTable("adminGroupBeanLL");
	DBRow[] productLineDefines = applyMoneyMgrLL.getAllByTable("productLineDefineBeanLL");
	HashMap followuptype = new HashMap();
	//操作日志类型:1表示进度跟进2:表示财务跟进;3修改日志;1货物状态;5运费流程;6进口清关;7出口报关8标签流程;9:单证流程
	followuptype.put(1,"创建记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"内部标签");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	followuptype.put(13,"到货通知仓库");
	followuptype.put(14,"第三方标签");
	followuptype.put(15,"缷货司机签到");
	followuptype.put(16, "装货司机签到");
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>转运单处理</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}

.set {
	padding: 2px;
	width: 90%;
	word-break: break-all;
	margin-top: 10px;
	margin-top: 5px;
	line-height: 15px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	margin-bottom: 10px;
	border: 2px solid blue;
}

p {
	text-align: left;
}

tr.split td input {
	margin-top: 2px;
}

ul.processUl {
	list-style-type: none;
}

ul.processUl li {
	line-height: 20px;
	border-bottom: 1px dashed silver;
	clear: both;
}

ul.processUl li span.right {
	dispaly: block;
	float: right;
	margin-right: 3px;
	width: 57px;
	text-align: left;
}

ul.processUl li span.left {
	dispaly: block;
	float: left;
}
-->
</style>

<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../common.js"></script>


<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />



<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css"
	rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>


<style type="text/css" media="all">
@import "../js/thickbox/global.css";

@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css"
	href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<!-- 引入下啦选择 -->
<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="../js/easyui/themes_visionari/icon.css" />
<script type="text/javascript" src="../js/easyui/jquery.easyui.menu.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript'
	src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript'
	src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 在线图片预览 -->
<script type="text/javascript"
	src="../js/office_file_online/officeFileOnline.js"></script>
<style>
body {
	font-size: 12px;
}

.search_shadow_bg {
	background: url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width: 408px;
	height: 36px;
	padding-top: 3px;
	padding-left: 3px;
	margin-bottom: 5px;
}

.search_input {
	background: url(../imgs/search_bg.jpg) repeat 0 0;
	width: 400px;
	height: 24px;
	font-weight: bold;
	border: 1px #bdbdbd solid;
}

#easy_search_father {
	position: absolute;
	width: 0px;
	height: 0px;
	z-index: 1;
}

#easy_search {
	position: absolute;
	left: 318px;
	top: -2px;
	width: 55px;
	height: 30px;
	z-index: 9999;
	visibility: visible;
}

span.stateName {
	width: 15%;
	float: left;
	text-align: right;
	font-weight: normal;
}

span.stateValue {
	width: 80%;
	display: block;
	float: left;
	text-indent: 4px;
	overflow: hidden;
	text-align: left;
	font-weight: normal;
}

.zebraTable td {
	border-bottom: 0px solid #DDDDDD;
	padding-left: 10px;
}

.privateTable td {
	border-bottom: 0px solid #DDDDDD;
	padding-left: 2px;
}

.privateTable .tdName
{
	text-align:right;
	padding: 0;
	border: 0;
	white-space:nowrap;
}

.privateTable .tdValue
{
	padding-left: 8px;
	border: 0;
	white-space:nowrap;
}

span.spanBold {
	font-weight: bold;
}

span.fontGreen {
	color: green;
}

span.fontRed {
	color: red;
}

span.spanBlue {
	color: blue;
}

span.stateName {
	width: 50px;
	float: left;
	text-align: right;
	font-weight: normal;
}

span.stateValue {
	width: 125px;
	display: block;
	float: left;
	text-indent: 4px;
	overflow: hidden;
	text-align: left;
	font-weight: normal;
	word-wrap: break-all;
}

.custom-title {
	border: 0px;
	background-color: #e5e5e5;
	height: 30px;
	font-weight: bold;
	color: #333333;
	padding: 5px;
	font-size: 12px;
	text-align: center;
}
</style>
<script>
	function search()
	{
		var val = $("#search_key").val();
				
		if(val.trim()=="")
		{
			alert("请输入要查询的关键字");
		}
		else
		{
			var val_search = "\'"+val.toLowerCase()+"\'";
			$("#search_key").val(val_search);
			document.search_form.key.value = val_search;
			document.search_form.search_mode.value = 1;
			document.search_form.submit();
		}
	}
	
	function searchRightButton()
	{
		var val = $("#search_key").val();
				
		if (val=="")
		{
			alert("你好像忘记填写关键词了？");
		}
		else
		{
			val = val.replace(/\'/g,'');
			$("#search_key").val(val);
			document.search_form.key.value = val;
			document.search_form.search_mode.value = 2;
			document.search_form.submit();
		}
	}
	
	function eso_button_even()
	{
		document.getElementById("eso_search").oncontextmenu=function(event) 
		{  
			if (document.all) window.event.returnValue = false;// for IE  
			else event.preventDefault();  
		};  
			
		document.getElementById("eso_search").onmouseup=function(oEvent) 
		{  
			if (!oEvent) oEvent=window.event;  
			if (oEvent.button==2) 
			{  
			   searchRightButton();
			}  
		}  
	}
	
	function promptCheck(v,m,f)
	{
		if (v=="y")
		{
			 if(f.content == "")
			 {
					alert("请填写适当备注");
					return false;
			 }
			 
			 else
			 {
			 	if(f.content.length>300)
			 	{
			 		alert("内容太多，请简略些");
					return false;
			 	}
			 }
			 
			 return true;
		}
	}


	function remark(transport_id)
	{
		var text = "<div id='title'>跟进</div><br />";
		text += "备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>";
		$.prompt(text,
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/transportRemarkAction.action"
										document.followup_form.transport_id.value = transport_id;
										document.followup_form.transport_content.value = "备注:"+f.content;
										document.followup_form.type.value = 1;
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
	}
	
	function followup(transport_id){
 	 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_follow_up.html"; 
 			uri += "?transport_id="+transport_id;
 			$.artDialog.open(uri , {title: "转运单跟进["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function goFundsTransferListPage(id)
	{
	 window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;      
	}
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}
	function goFundsTransferListPage(id)
	{
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id;      
	}
	function goApplyFunds(id)
	{
		var id="'F"+id+"'";
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=searchby_index&search_mode=1&apply_id="+id);       
	}
	$(document).ready(function()
	{
		//alert('');
		getLevelSelect(0, nameArray, widthArray, centerAccounts, null,vname,'<%=dept%>');
		getLevelSelect(1, nameArray, widthArray, centerAccounts, $("#dept"),vname,'<%=create_account_id%>');
		getLevelSelect(0, nameArray1, widthArray1, centerAccounts, null,vname1,'<%=dept1%>');
		getLevelSelect(1, nameArray1, widthArray1, centerAccounts, $("#dept1"),vname1,'<%=create_account_id1%>');
		$.unblockUI();
		$("#dataTable").show();
	});
	<%//level_id,value,name
		String str = "var centerAccounts = new Array(";
		str += "new Array('01',0,'选择部门')";
		str += ",new Array('01.0',0,'选择职员')";
		for(int i=0;i<adminGroups.length;i++) {//部门
			DBRow adminGroup = adminGroups[i];
			str += ",new Array('0"+(i+2)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
			str += ",new Array('0"+(i+2)+".0',0,'选择职员')";
			for(int ii=0;ii<accounts.length;ii++) {
				DBRow account = accounts[ii];
				if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
					str += ",new Array('0"+(i+2)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
			}
		}
		str+= ");";
		out.println(str);
	%>
	var nameArray = new Array('dept','create_account_id');
	var widthArray = new Array(100,100);
	var vname = new Array('nameArray','widthArray','centerAccounts','vname');
	var nameArray1 = new Array('dept1','create_account_id1');
	var widthArray1 = new Array(100,100);
	var vname1 = new Array('nameArray1','widthArray1','centerAccounts','vname1');
	function getLevelSelect(level,nameArray, widthArray, selectArray,o,vnames,value) {
			if(level<nameArray.length) {
				var name = nameArray[level];
				var width = widthArray[level];
				var levelId = o==null?"":$("option:selected",o).attr('levelId');
				var onchangeStr = "";
				if(level==nameArray.length-1)
					onchangeStr = "";
				else
					onchangeStr = "getLevelSelect("+(level+1)+","+vnames[0]+","+vnames[1]+","+vnames[2]+",this,"+vnames[3]+")";
				var selectHtml = "&nbsp;&nbsp;<select name='"+name+"' id='"+name+"' style='width:"+width+"px;' onchange="+onchangeStr+"> ";
				//alert(selectArray);
				for(var i=0;i<selectArray.length;i++) {
					if(levelId!="") {
						var levelIdChange = selectArray[i][0].replace(levelId+".");
						var levelIds = levelIdChange.split(".");	
						//alert(levelIdChange);
						if(levelIdChange!=selectArray[i][0]&&levelIds.length==1){
							//alert(levelId+",value="+selectArray[i][0]+",change='"+levelIdChange+"',"+levelIds.length);
							selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
						}
					}
					else {
						var levelIdChange = selectArray[i][0];
						//alert(levelIdChange);
						var levelIds = levelIdChange.split(".");
						if(levelIds.length==1){
							//alert(levelId+","+selectArray[i][0]+levelId1);
							selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
						}
					}
				}
				selectHtml += "</select>";
				$('#'+name+'_div').html('');
				$('#'+name+'_div').append(selectHtml);
				//alert(selectHtml);
				$('#'+name).val(value);
				getLevelSelect(level+1,nameArray,widthArray,centerAccounts,$('#'+name),vnames);
			}
	}
	jQuery(function($){
 
		addAutoComplete($("#search_key"),
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/transport/GetSearchTransportJSONAction.action",
				"merge_field","transport_id");
	})
	
	function uploadFile(association_id,association_type)
	{
		var url = "transport_upload_image.html?association_id="+association_id+"&association_type="+association_type;
		$.artDialog.open(url, {title: '交货单文件',width:'700px',height:'600px',lock: true,opacity: 0.3,fixed: true});
		//tb_show('创建交货单','administrator_create_delivery.html?TB_iframe=true&height=500&width=700',false);
	}
	
	function addDeliveryTransportOrder()
	{
		var url = "add_purchase_transport.html";
		$.artDialog.open(url, {title: '交货单信息',width:'700px',height:'600px', lock: true,opacity: 0.3,fixed: true});
		//tb_show('创建交货单','administrator_create_delivery.html?TB_iframe=true&height=500&width=700',false);
	}
	function refreshWindow(){
		go($("#jump_p2").val());
	}
	function transport_certificate(transport_id,file_with_type){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_certificate.html"; 
		uri += "?transport_id="+transport_id + "&file_with_type="+file_with_type;
		$.artDialog.open(uri, {title: '单证流程',width:'700px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	function clearanceButton(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_clearance.html?transport_id="+transport_id; 
		$.artDialog.open(uri , {title: "清关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function declarationButton(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_declaration.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "报关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function stock_in_set(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_stockinset.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "运费流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function quality_inspectionKey(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_quality_inspection.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "质检流程["+transport_id+"]",width:'570px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	}
	function product_file(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_file.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "实物图片["+transport_id+"]",width:'850px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	function tag(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_tag.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "跟进内部标签["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function tagThird(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_third_tag.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "跟进第三方标签["+transport_id+"]",width:'570px',height:'290px', lock: true,opacity: 0.3,fixed: true});
	}
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
	
	function transportTrackCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'first'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg += "<a  href='javascript:trackDeliveryCount()'>工厂交货("+data.track_delivery_count+")</a>&nbsp;&nbsp;";
				mesg += "<a  href='javascript:trackSendCount()'>仓库发货("+data.track_send_count+")</a>&nbsp;&nbsp;";
				mesg += "<a  href='javascript:trackReciveCount()'>仓库收货("+data.track_recive_count+")</a>&nbsp&nbsp;";
				mesg += "<a href='javascript:trackOceanShippingCount()'>海运("+data.ocean_shipping_count+")";
				$("#transport_followup").html(mesg);
			}		
		});
	}
	function trackOceanShippingCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'ocean_shipping'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>海运:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:needTrackOceanShipping(\""+data[i].cmd+"\")'>"+data[i].track_title+"("+data[i].track_count+")</a>&nbsp;&nbsp;"
				});
				$("#transport_followup").html(mesg);
			}		
		});
		
		
	}
	
	function trackDeliveryCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'ready_delivery'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>产品线:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:needTrackDeliveryProductLine("+data[i].product_line_id+",\""+data[i].product_line_name+"\")'>"+data[i].product_line_name+"("+data[i].track_count+")</a>&nbsp;&nbsp;"
				});
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function trackSendCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'send_store_transport'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>仓库:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:trackSendCountByPsid("+data[i].ps_id+",\""+data[i].title+"\")'>"+data[i].title+"("+data[i].need_track_send_count+")</a>&nbsp;&nbsp;"
				});
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function trackSendCountByPsid(ps_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:
			{
				type:'send_store_transport_ps',
				ps_id:ps_id
			},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:trackSendCount()'>"+title+"</a>:&nbsp;&nbsp;";
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"ready_send_ps\",\""+title+"\")'>备货中("+data.need_track_ready_transprot_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"packing_send_ps\",\""+title+"\")'>装箱中("+data.need_track_packing_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"tag_send_ps\",\""+title+"\")'>内部标签("+data.need_track_tag_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"tag_send_ps\",\""+title+"\")'>第三方标签("+data.need_track_third_tag_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"product_file_send_ps\",\""+title+"\")'>实物图片("+data.need_track_product_file_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"quality_inspection_send_ps\",\""+title+"\")'>质检报告("+data.need_track_quality_inspection_transport_count+")</a>&nbsp;&nbsp;"
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function trackReciveCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'recive_store_transport'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>仓库:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:trackReciveCountByPsid("+data[i].ps_id+",\""+data[i].title+"\")'>"+data[i].title+"("+data[i].need_track_recive_count+")</a>&nbsp;&nbsp;"
				});
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function trackReciveCountByPsid(ps_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:
			{
				type:'recive_store_transport_ps',
				ps_id:ps_id
			},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg += "<a  href='javascript:trackReciveCount()'>"+title+"</a>:&nbsp;&nbsp;";
				mesg += "<a  href='javascript:needTrackReceiveTransport("+data.ps_id+",\"intransit_receive_ps\",\""+title+"\")'>运输中("+data.need_track_intransit_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackReceiveTransport("+data.ps_id+",\"alreadyRecive_receive_ps\",\""+title+"\")'>到货未入库("+data.need_track_alreadyrecive_transport_count+")</a>&nbsp;&nbsp;"
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function needTrackDeliveryProductLine(product_line_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/deliveryTrackByProductLine.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{product_line_id:product_line_id},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:trackDeliveryCount()'>"+title+":</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a href='javascript:needTrackDelivery("+product_line_id+",\""+title+"\",\""+data[i].cmd+"\")'>"+data[i].track_tilte+"("+data[i].track_count+")</a>&nbsp;&nbsp;"
				});
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function needTrackDelivery(product_line_id,product_line_title,cmd)
	{
		//alert(cmd);
		document.track_form.product_line_id.value = product_line_id;
		document.track_form.cmd.value = cmd;
		document.track_form.product_line_title.value = product_line_title
		document.track_form.submit();
	}
	
	function needTrackOceanShipping(type)
	{
		document.track_form.cmd.value = type;
		document.track_form.submit();
	}
	
	function needTrackSendTransport(ps_id,type,title)
	{
		document.track_form.send_psid.value = ps_id;
		document.track_form.cmd.value = type;
		document.track_form.store_title.value = title;
		document.track_form.submit();
	}
	
	function needTrackReceiveTransport(ps_id,type,title)
	{
		document.track_form.receive_psid.value = ps_id;
		document.track_form.cmd.value = type;
		document.track_form.store_title.value = title;
		document.track_form.submit();
	}
/*	function produresInfo(transport_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/test_produres.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '流程测试['+transport_id+'] ',width:'800px',height:'600px', lock: true,opacity: 0.3});
	}
	function produresInfoUpdate(transport_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/test_produres_update.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '流程测试更新['+transport_id+'] ',width:'800px',height:'600px', lock: true,opacity: 0.3});
	}*/
	function goodsArriveDelivery(transport_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_goods_arrive_delivery.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '到货派送['+transport_id+"]",width:'500px',height:'200px', lock: true,opacity: 0.3});
	}
	function inbound(transport_id){
		 showPictrueOnline('<%= FileWithTypeKey.TRANSPORT_RECEIVE%>',transport_id,"",0,"transport");
	}
	function outbound(transport_id){
			 showPictrueOnline('<%= FileWithTypeKey.TRANSPORT_OUTBOUND%>',transport_id,"",0,"transport");
	
	}
	function showPictrueOnline(fileWithType,fileWithId ,currentName,productFileType,uploadPath){
	   var obj = {
	   		file_with_type:fileWithType,
	   		file_with_id : fileWithId,
	   		current_name : currentName ,
	   		product_file_type:productFileType,
 	   		cmd:"multiFile",
	   		table:'file',
	   		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+uploadPath
		}
	   if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}		
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"
	onLoad="onLoadInitZebraTable()">
	<div class="demo" style="width: 98%; min-width: 800px">
		<div id="tabs">
			<ul>
				<li><a href="#transport_search">常用工具</a></li>
				<li><a href="#transport_filter">高级搜索</a></li>
				<li style="display:none"><a href="#transport_followup">需跟进</a></li>
			</ul>
			<div id="transport_search">
				<table width="100%" height="61" border="0" cellpadding="0"
					cellspacing="0">
					<tr>
						<td width="30%" style="padding-top: 3px;">
							<div id="easy_search_father">
								<div id="easy_search">
									<a href="javascript:search()"><img id="eso_search"
										src="../imgs/easy_search.gif" width="70" height="29"
										border="0" /></a>
								</div>
							</div>
							<table width="485" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="418">
										<div class="search_shadow_bg">
											<input name="search_key" type="text" class="search_input"
												style="font-size: 17px; font-family: Arial; color: #333333"
												id="search_key" onkeydown="if(event.keyCode==13)search()"
												value="<%=key%>" />
										</div>
									</td>
									<td width="67"></td>
								</tr>
							</table> <script>eso_button_even();</script>
						</td>
						<td width="33%"></td>
						<td width="24%"></td>
					</tr>
				</table>
			</div>

			<div id="transport_filter">
				<table width="100%" height="61" border="0" cellpadding="0"
					cellspacing="0">
					<tr>
						<td align="left">
							<div style="float: left;">
								<select id="send_ps" name="send_ps" style="width: 100px">
									<option value="0">转运仓库</option>
									<%
										for(int j = 0;j<ps.length;j++)
															{
									%>
									<option value="<%=ps[j].get("id",0l)%>"
										<%=send_psid==ps[j].get("id",0l)?"selected":""%>><%=ps[j].getString("title")%></option>
									<%
										}
									%>
								</select> &nbsp;&nbsp; <select id="receive_ps" name="receive_ps"
									style="width: 100px">
									<option value="0">目的仓库</option>
									<%
										for(int j = 0;j<ps.length;j++)
															{
									%>
									<option value="<%=ps[j].get("id",0l)%>"
										<%=receive_psid==ps[j].get("id",0l)?"selected":""%>><%=ps[j].getString("title")%></option>
									<%
										}
									%>
								</select> &nbsp;&nbsp; <select id="transport_status"
									name="transport_status" style="width: 100px">
									<option value="0">货物状态</option>
									<option value="<%=TransportOrderKey.INTRANSIT%>"
										<%=transport_status==TransportOrderKey.INTRANSIT?"selected":""%>><%=transportOrderKey.getTransportOrderStatusById(TransportOrderKey.INTRANSIT)%></option>
									<option value="<%=TransportOrderKey.AlREADYARRIAL%>"
										<%=transport_status==TransportOrderKey.AlREADYARRIAL?"selected":""%>><%=transportOrderKey.getTransportOrderStatusById(TransportOrderKey.AlREADYARRIAL)%></option>
									<option value="<%=TransportOrderKey.RECEIVEING%>"
										<%=transport_status==TransportOrderKey.RECEIVEING?"selected":""%>><%=transportOrderKey.getTransportOrderStatusById(TransportOrderKey.RECEIVEING)%></option>
								</select> <select id="stock_in_set" name="stock_in_set"
									style="width: 100px">
									<option value="0">运费流程</option>
									<%
										ArrayList<String> stockInsetKey = transportStockInSetKey.getStatus();
															for(String s : stockInsetKey){
									%>
									<option value="<%=s%>"
										<%=(stock_in_set==Integer.parseInt(s+"")) ? "selected" :""%>><%=transportStockInSetKey.getStatusById(Integer.parseInt(s+""))%>
									</option>
									<%
										}
									%>
								</select>
							</div> &nbsp;&nbsp;
							<div style="float: left;" id="dept_div" name="dept_div"></div>
							&nbsp;&nbsp;
							<div id='create_account_id_div' name='create_account_id_div'
								style="float: left;"></div>
						</td>
					</tr>
					<tr>
						<td align="left" nowrap="nowrap"
							style="font-family: 宋体; font-size: 12px;"><select
							name="declarationStatus" id="declarationStatus"
							style="width: 100px">
								<option value="0">出口报关流程</option>
								<%
									ArrayList statuses21 = declarationKey.getStatuses();
									          			for(int i=0;i<statuses21.size();i++) {
									          				int statuse = Integer.parseInt(statuses21.get(i).toString());
									          				String key1 = declarationKey.getStatusById(statuse);
									          				out.println("<option value='"+statuse+"' "+(declaration==statuse?"selected":"")+">"+key1+"</option>");
									          			}
								%>
						</select> &nbsp;&nbsp; <select name="clearanceStatus" id="clearanceStatus"
							style="width: 100px">
								<option value="0">进口清关流程</option>
								<%
									ArrayList statuses31 = clearanceKey.getStatuses();
									          			for(int i=0;i<statuses31.size();i++) {
									          				int statuse = Integer.parseInt(statuses31.get(i).toString());
									          				String key1 = clearanceKey.getStatusById(statuse);
									          				out.println("<option value='"+statuse+"' "+(clearance==statuse?"selected":"")+">"+key1+"</option>");
									          			}
								%>
						</select> &nbsp;&nbsp; <select name="invoiceStatus" id="invoiceStatus"
							style="width: 100px">
								<option value="0">发票流程</option>
								<%
									ArrayList statuses11 = invoiceKey.getInvoices();
									          			for(int i=0;i<statuses11.size();i++) {
									          				int statuse = Integer.parseInt(statuses11.get(i).toString());
									          				String key1 = invoiceKey.getInvoiceById(statuse);
									          				out.println("<option value='"+statuse+"' "+(invoice==statuse?"selected":"")+">"+key1+"</option>");
									          			}
								%>
						</select> &nbsp;&nbsp; <select name="drawbackStatus" id="drawbackStatus"
							style="width: 100px">
								<option value="0">退税流程</option>
								<%
									ArrayList statuses41 = drawbackKey.getDrawbacks();
									          			for(int i=0;i<statuses41.size();i++) {
									          				int statuse = Integer.parseInt(statuses41.get(i).toString());
									          				String key1 = drawbackKey.getDrawbackById(statuse);
									          				out.println("<option value='"+statuse+"' "+(drawback==statuse?"selected":"")+">"+key1+"</option>");
									          			}
								%>
						</select> &nbsp;&nbsp; <input type="button" class="button_long_refresh"
							value="过滤" onclick="filter()" /></td>
					</tr>
				</table>
			</div>

			<div id="transport_followup"></div>
		</div>
		<script>
		
		$.blockUI.defaults = {
				css: { 
					padding:        '8px',
					margin:         0,
					width:          '170px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #999999',
					backgroundColor:'#eeeeee',
					'-webkit-border-radius': '10px',
					'-moz-border-radius':    '10px',
				    '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			   		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
				baseZ: 99999, 
			    centerX: true,
			    centerY: true,
				fadeOut:  1000
			};
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		
		
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		show:function(event,ui)
			 {
				//需跟进：2 
			 	if(ui.index==2222)
			 	{
			 		<%if(cmd.equals("ready_delivery")||cmd.equals("tag_delivery")||cmd.equals("quality_inspection_delivery")||cmd.equals("product_file_delivery"))
			 			{%>
			 			needTrackDeliveryProductLine(<%=product_line_id%>,"<%=product_line_title%>","<%=cmd%>");
			 		<%}
			 			else if(cmd.equals("ready_send_ps")||cmd.equals("packing_send_ps"))
			 			{%>
			 			trackSendCountByPsid(<%=send_psid%>,"<%=store_title%>")
			 		<%}
			 			else if(cmd.equals("intransit_receive_ps")||cmd.equals("alreadyRecive_receive_ps"))
			 			{%>
			 			trackReciveCountByPsid(<%=receive_psid%>,"<%=store_title%>")
			 		<%}
			 			else
			 			{%>
			 			transportTrackCount();
			 		<%}%>
			 	}
			 }
	});
	</script>

		<br />
		<div id="system_menu" style="display: none"></div>
		<table id="dataTable" width="98%" border="0" align="center" style="display:none"
			cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true"
			isBottom="true">
			<thead id="tableHead" width="100%">
				<tr>
					<th width="25%" nowrap="nowrap" class="right-title"
						style="vertical-align: center; text-align: center;">转运单基本信息</th>
					<th width="27%" nowrap="nowrap" class="right-title"
						style="vertical-align: center; text-align: center;">库房及运输信息</th>
					<!--   <th width="18%" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;dislay:none">资金情况</th>  -->
					<th width="18%" nowrap="nowrap" class="right-title"
						style="vertical-align: center; text-align: center;">流程信息</th>
					<th width="20%" nowrap="nowrap" class="right-title"
						style="vertical-align: center; text-align: center;">跟进</th>
				</tr>
			</thead>
			<%
				for(int i = 0 ;i<rows.length;i++)
			  		{
			%>
			<tr align="center">
				<td height="40" nowrap="nowrap">
					<!-- 设置主流程 id -->
					<fieldset class="set" id="<%=rows[i].getString("transport_id")%>">
						<!-- 如果是交货行转运单那么就是Id显示成蓝色 -->
						<%
							String fontColor = rows[i].get("purchase_id",0l) > 0l ? "mediumseagreen;":"#f60;";
						%>
						<legend>
							<a style="color:<%=fontColor%>" target="_blank"
								href="transport_order_in_detail.html?transport_id=<%=rows[i].getString("transport_id")%>">T<%=rows[i].getString("transport_id")%></a>
							&nbsp;<%=transportOrderKey.getTransportOrderStatusById(rows[i].get("transport_status",0))%>
						</legend>
						<table width="100%" cellspacing="0" cellpadding="0" border="0" class="privateTable">
							<tr>
								<td width="65%">
									<table style="border-right: 1px dashed #C0C0C0">
										<tr>
											<td colspan="2" height="5px"></td>
										</tr>
										<tr>
											<td width="20%"><span class="stateName">创建人:</span></td>
											<td><span class="stateValue">
											 <%
 											DBRow createAdmin = adminMgr.getDetailAdmin(rows[i].get("create_account_id",0l));
	 			  						 	if(createAdmin!=null)
	 			  						 	{
	 			  						 		out.print(createAdmin.getString("employe_name"));
	 			  						 	}
											 %>
											</span></td>
										</tr>
										<tr>
											<td><span class="stateName">TITLE:</span></td>
											<td><span class="stateValue"> 
											<%
 											DBRow titleRow = proprietaryMgrZyj.findProprietaryByTitleId(rows[i].get("title_id", 0L));
		 			  						if(null != titleRow)
		 			  						{  							
		 				  						out.println(titleRow.getString("title_name"));
		 			  						}
 											%>
											</span></td>
										</tr>
										<tr>
											<td><span class="stateName">允许装箱:</span></td>
											<td><span class="stateValue">
											<%
												DBRow packingAdmin = adminMgr.getDetailAdmin(rows[i].get("packing_account",0l));
												if(packingAdmin!=null)
												{
													out.print(packingAdmin.getString("employe_name"));
												}
											%>
											</span></td>
										</tr>
										<tr>
											<td><span class="stateName">创建时间:</span></td>
											<td><span class="stateValue"><%=tDate.getEnglishFormateTime(rows[i].getString("transport_date"))%></span>
											</td>
										</tr>
										<tr>
											<td><span class="stateName"> 更新时间:</span></td>
											<td><span class="stateValue"><%=tDate.getEnglishFormateTime(rows[i].getString("updatedate"))%></span>
											</td>
										</tr>
										<tr>
											<td><span class="stateName"><strong>
														ETA:</strong></span></td>
											<td><span class="stateValue" style="font-weight: bold">
													<%=tDate.getEnglishFormateTime(rows[i].getString("transport_receive_date"))%>
											</span></td>
										</tr>
									</table>
								</td>
								<td>
									<table width="100%" height="100%" border="0px"
										cellpadding="0px" cellspacing="0px">
										<tr>
											<td align="right" width="30%" style="padding-left: 5px">总容器:</td>
											<td>&nbsp;<%=outboundOrderMgrZJ.getOutListContainerCountBySystemBill(ProductStoreBillKey.TRANSPORT_ORDER, rows[i].get("transport_id", 0l)) %></td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- 如果属于转运单，则有关联单按钮 -->
							<%
								if(rows[i].get("purchase_id",0l)!=0)
							    {
							%>
							<tr style="display: none">
								<td colspan="2" align="right"><a href="javascript:void(0)"
									onclick='moreRelateBill(<%=rows[i].get("transport_id",0l)%>,<%=rows[i].getString("purchase_id")%>)'
									style="color: green;"> 关联单 </a></td>
							</tr>
							<%
								}
							%>

						</table>
					</fieldset>
				</td>

				<!-- 库房及运输信息 -->
				<td align="left">
					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="privateTable">
						<tr>
							<td width="30%" class="tdName">提货仓库:</td>
							<td class="tdValue">
								<%
									int fromPsType = rows[i].get("from_ps_type",0);
					  					//如果是供应商的Type那么就需要去查询供应商的名称
					 	  				if(fromPsType == ProductStorageTypeKey.SupplierWarehouse){
					 	  					DBRow temp = supplierMgrTJH.getDetailSupplier(rows[i].get("send_psid",0l));
					 	  					if(temp != null){
					 	  						out.println(temp.getString("sup_name"));
					 	  					}
					 	  				}
					 	  				else
					 	  				{
					 	  					DBRow storageCatalog = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("send_psid",0l));
					 	  					if(storageCatalog != null){
					 	  						out.print(storageCatalog.getString("title"));
					 	  					}
					 	  				}
								%>
							</td>
						</tr>
						<tr style="display: none">
							<td  >收货仓库:</td>
							<td>
								 <%=null!=catalogMgr.getDetailProductStorageCatalogById(rows[i].get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(rows[i].get("receive_psid",0l)).getString("title"):""%>
							</td>
						</tr>
						<tr style="display: none">
							<td >收货人:</td>
							<td>
								<%
									long deliveryer_id = rows[i].get("deliveryer_id",0l);
							  		if(deliveryer_id != 0L)
							  		{
							  			out.println(null!=adminMgr.getDetailAdmin(deliveryer_id)?adminMgr.getDetailAdmin(deliveryer_id).getString("employe_name"):"");
							  		}
								%>
							</td>
						</tr>
						<tr style="display: none">
							<td >收货时间:</td>
							<td>
							<%
								String deliveryTime = rows[i].getString("deliveryed_date");
					  			out.print(deliveryTime.length() > 10 ?deliveryTime.substring(0,10): deliveryTime);
							%>
							</td>
						</tr>
						<tr style="display: none">
							<td  >入库人:</td>
							<td>
							<%
								long warehousinger_id = rows[i].get("warehousinger_id",0l);
								if(warehousinger_id != 0L)
								{
									out.println(adminMgr.getDetailAdmin(warehousinger_id).getString("employe_name"));
								}
							%>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<hr style="width:90%;border-top:1px dashed silver;height:1px;border-bottom:0px">
							</td>
						</tr>
						<tr>
							<td  class="tdName">运单号:</td>
							<td class="tdValue"> 
								<%=rows[i].getString("transport_waybill_number")%>
							</td>
						</tr>
						<tr>
							<td class="tdName">货运公司:</td>
							<td class="tdValue">
								<%=rows[i].getString("transport_waybill_name")%>
							</td>
						</tr>
						<tr>
							<td class="tdName">承运公司:</td>
							<td class="tdValue">
								<%=rows[i].getString("carriers")%>
							</td>
						</tr>
						<%
 							long transport_send_country = rows[i].get("transport_send_country",0l);
   						 	DBRow send_country_row = transportMgrLL.getCountyById(Long.toString(transport_send_country));
   							long transport_receive_country = rows[i].get("transport_receive_country",0l);
   							DBRow receive_country_row = transportMgrLL.getCountyById(Long.toString(transport_receive_country));
						 %>
						<tr>
							<td class="tdName" nowrap="nowrap">始发国/始发港:</td>
							<td class="tdValue">
								<%=send_country_row==null?"无":send_country_row.getString("c_country")%>/<%=rows[i].getString("transport_send_place").equals("")?"无":rows[i].getString("transport_send_place")%>
							</td>
						</tr>
						<tr>
							<td class="tdName">总体积:</td>
							<td class="tdValue">
							<%=transportMgrZJ.getTransportVolume(rows[i].get("transport_id",0l))%> cm³
							</td>
						</tr>
						<tr>
							<td class="tdName">总重量:</td>
							<td class="tdValue">
								<%=transportMgrZJ.getTransportWeight(rows[i].get("transport_id",0L))%> Kg
							</td>
						</tr>
					</table>
				</td>

				<!-- 流程信息 -->
				<td nowrap="nowrap" style="text-align: left;">
					<!--   对于有的流程 运费,清关,报关有了跟进中的时候才显示 花费多长的时间 --> <!-- 流程的跟进如果是完成+ 已经上传文件的显示绿色。没有上传文件的就是红色。完成了的用粗体显示 -->
					<ul class="processUl">

						<li><span class="left">货物状态:&nbsp; <%=transportOrderKey.getTransportOrderStatusById(rows[i].get("transport_status",0))%>
								<%
									DBRow[] transportPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 4);
								  							 if(transportPersons != null && transportPersons.length > 0){
								%> <br /> <%
 	for(DBRow tempUserRow : transportPersons){
 %> <%=tempUserRow.getString("employe_name")%> <%
 	}
   							 }
 %>
						</span> <span class="right" style=""> <%=rows[i].getString("all_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("all_over")+"天完成"%>
						</span></li>





						<%
							//计算 质检流程 颜色
							  						String qualityInspectionClass = "" ;
							  						String qualityHidden ="display:none";
						  							int qualityInspectionInt = rows[i].get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY);
						  							if(qualityInspectionInt == TransportQualityInspectionKey.FINISH){
						  								qualityInspectionClass += "spanBold";
						  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.TRANSPORT_QUALITYINSPECTION);
						  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
						  									qualityInspectionClass += " fontGreen";
						  									
						  								}else{
						  									qualityInspectionClass += " fontRed";
						  									qualityHidden ="display:block";
						  								}
						  							}else if(qualityInspectionInt != TransportQualityInspectionKey.NO_NEED_QUALITY){
						  								qualityInspectionClass += "spanBold spanBlue";
						  								qualityHidden ="display:block";
						  							}
						%>

						<li style="<%=qualityHidden%>; display:none"><span
							class="left"> 质检流程: <span
								class="<%=qualityInspectionClass%>"><%=transportQualityInspectionKey.getStatusById(rows[i].get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY)+"")%></span>
								<%
									DBRow[] qualityInspectionPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 11);
									if(qualityInspectionPersons != null && qualityInspectionPersons.length > 0)
									 {
								%> <br />
								 <%
 									for(DBRow tempUserRow : qualityInspectionPersons)
 									{
 									%> 
 										<%=tempUserRow.getString("employe_name")%>&nbsp; 
 									<%
 									}
 	  							 }
 						%>
						</span> <span class="right"> <%=rows[i].get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY) == TransportQualityInspectionKey.NO_NEED_QUALITY?"":(rows[i].getString("quality_inspection_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("quality_inspection_over")+"天完成")%>
						</span></li>
					</ul>
				</td>

				<!-- 跟进 -->
				<td align="left">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<col width="20%">
						<col width="80%">
						<%
							DBRow[] transportLogsRow = transportMgrZr.getTransportLogs(rows[i].get("transport_id",0l),4);
								  					if(transportLogsRow.length > 0){
								  						int count = transportLogsRow.length >= 4 ? 3:transportLogsRow.length;
								  						for(int m = 0; m < count; m ++){
								  							DBRow transportLog = transportLogsRow[m];
						%>
						<tr height="22px">
							<td nowrap="nowrap" align="left"><font style="color: #f60;"><%=followuptype.get(transportLog.get("transport_type",0))%></font>:
							</td>
							<td align="left" nowrap><strong><%=adminMgrLL.getAdminById(transportLog.getString("transporter_id")).getString("employe_name")%></strong>
								<span
								style="color: #999999; font-size: 11px; font-farmliy: Verdana; display: inline-block"><%=tDate.getFormateTime(transportLog.getString("transport_date"))%></span>
								</span></td>
						</tr>
						<tr>
							<td colspan="2"><%=transportLog.getString("transport_content")%>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center" style="padding-left: 2;"><hr
									style="width: 90%; border-top: 1px dashed silver; height: 1px; border-bottom: 0px"></td>
						</tr>

						<%
							}
								  					}
								  					if(transportLogsRow.length >= 4){
						%>
						<tr>
							<td nowrap="nowrap"><a href="javascript:void(0)"
								onclick='transportLogs(<%=rows[i].get("transport_id",0l)%>)'
								style="color: green;">更多</a></td>
							<td></td>
						</tr>

						<%
							}
						%>
					</table>
				</td>
			</tr>
			<tr class="split">
				<td colspan="6" style="text-align: right; padding-right: 20px;">
					<input type="button" value="收货图片" class="long-button"
					onclick="inbound('<%=rows[i].get("transport_id",0l)%>')" /> <input
					style="display: none" type="button" value="装货货图片"
					class="long-button"
					onclick="outbound('<%=rows[i].get("transport_id",0l)%>')" /> <%
 						if(TransportOrderKey.INTRANSIT == rows[i].get("transport_status",0))
   					{
					 %> 
					 <input type="button" value="到货派送" class="long-button"
					onclick="goodsArriveDelivery(<%=rows[i].get("transport_id",0l)%>)" />
					<%
						}
					%>
					 <%
 						//转运单需要司机签到
 		  	  		DBRow[] locationOccupancysLoad = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(0,0,
 		  	  			ProductStoreBillKey.TRANSPORT_ORDER, rows[i].get("transport_id",0l) , -LoadUnloadOccupancyStatusKey.QUIT, null, null, null, null,rows[i].get("receive_psid", 0L),2,TransportRegistrationTypeKey.DELEIVER);
 		  	  		DBRow[] locationOccupancysUnload = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(0,0,
 		  	  			ProductStoreBillKey.TRANSPORT_ORDER, rows[i].get("transport_id",0l) , -LoadUnloadOccupancyStatusKey.QUIT, null, null, null, null,rows[i].get("send_psid", 0L),2,TransportRegistrationTypeKey.SEND);
 		  	 		//如果提到仓库一致
 		  	  		//if(0==1 && rows[i].get("receive_psid", 0L) == rows[i].get("send_psid", 0L) && 0==locationOccupancysLoad.length && 0 == locationOccupancysUnload.length)
 		  	  		//{
				   %> 
				  <%
 					//缷货司机已签到
 		  	  		if(0 != locationOccupancysLoad.length)
 		  	  		{
 	  	  				String buttonColorLoad = "";
 		  	  			for(int m = 0; m < locationOccupancysLoad.length; m ++)
 		  	  			{
 		  	  				if(LoadUnloadOccupancyStatusKey.BOOKING == locationOccupancysLoad[m].get("occupancy_status",0))
 		  	  				{
 		  	  					buttonColorLoad = "color:red;";
 		  	  					break;
 		  	  				}
 		  	  			}
				  %> 
				 	 &nbsp;&nbsp;
				 	 <input type="button" class="long-button" style='<%=buttonColorLoad%>' value="缷货司机已签到"
					onclick="bookDoorOrLocationUpdateOrView('<%=rows[i].get("transport_id",0l)%>','<%=TransportRegistrationTypeKey.DELEIVER%>')" />
				  <%
					}
					else
					{ 
						//缷货司机签到
						if(psId == rows[i].get("receive_psid", 0L))
						{
					%> 
					&nbsp;&nbsp;<input  type="button" class="long-button" value="缷货司机签到"
					onclick="bookDoorOrLocation('<%=rows[i].get("transport_id",0l)%>','<%=TransportRegistrationTypeKey.DELEIVER%>')" />
					<%
						}
					}
					%>
				</td>
			</tr>
			<%
				}
			%>
		</table>
		<br />
		<table width="100%" border="0" align="center" cellpadding="3"
			cellspacing="0">
			<tr>
				<td height="28" align="right" valign="middle"
					class="turn-page-table">
					<%
						int pre = pc.getPageNo() - 1;
						int next = pc.getPageNo() + 1;
						out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
						out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
						out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
						out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
						out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
					%> 跳转到 <input name="jump_p2" type="text" id="jump_p2"
					style="width: 28px;" value="<%=pc.getPageNo()%>" /> <input
					name="Submit22" type="button" class="page-go"
					style="width: 28px; padding-top: 0px;"
					onClick="javascript:go(document.getElementById('jump_p2').value)"
					value="GO" />
				</td>
			</tr>
		</table>

		<form action="transport_order_in_index.html" method="get"
			name="search_form">
			<input type="hidden" name="key" /> <input type="hidden" name="cmd"
				value="search" /> <input type="hidden" name="search_mode" />
		</form>

		<form action="transport_order_in_index.html" method="post"
			name="filter_form" id="filter_form">
			<input type="hidden" name="send_psid" /> <input type="hidden"
				name="receive_psid" /> <input type="hidden" name="status" /> <input
				type="hidden" name="declarationStatus" /> <input type="hidden"
				name="clearanceStatus" /> <input type="hidden" name="drawbackStatus" />
			<input type="hidden" name="invoiceStatus" /> <input type="hidden"
				name="stock_in_set" /> <input type="hidden" name="dept" /> <input
				type="hidden" name="create_account_id" /> <input type="hidden"
				name="transport_status" /> <input type="hidden" name="cmd"
				value="filter" />
		</form>

		<form action="transport_order_in_index.html" method="post"
			name="track_form">
			<input type="hidden" name="send_psid" /> <input type="hidden"
				name="receive_psid" /> <input type="hidden" name="product_line_id" />
			<input type="hidden" name="cmd" /> <input type="hidden"
				name="store_title" /> <input type="hidden" name="product_line_title" />
		</form>

		<form name="dataForm" method="post">
			<strong> <input type="hidden" name="p" /> <input
				type="hidden" name="status" value="<%=status%>" /> <input
				type="hidden" name="cmd" value="<%=cmd%>" /><input type="hidden"
				name="number" value="<%=key%>" /> <input type="hidden"
				name="receive_psid" value="<%=receive_psid%>" /> <input
				type="hidden" name="send_psid" value="<%=send_psid%>" /> <input
				type="hidden" name="declarationStatus" value="<%=declaration%>" /> <input
				type="hidden" name="clearanceStatus" value="<%=clearance%>" /> <input
				type="hidden" name="drawbackStatus" value="<%=drawback%>" /> <input
				type="hidden" name="invoiceStatus" value="<%=invoice%>" /> <input
				type="hidden" name="st" value="<%=st%>" /> <input type="hidden"
				name="en" value="<%=en%>" /> <input type="hidden"
				name="analysisType" value="<%=analysisType%>" /> <input
				type="hidden" name="analysisStatus" value="<%=analysisStatus%>" /> <input
				type="hidden" name="day" value="<%=day%>" /> <input type="hidden"
				name="stock_in_set" value="<%=stock_in_set%>" /> <input
				type="hidden" name="dept" value="<%=dept%>" /> <input type="hidden"
				name="create_account_id" value="<%=create_account_id%>" /> <input
				type="hidden" name="dept1" value="<%=dept1%>" /> <input
				type="hidden" name="create_account_id1"
				value="<%=create_account_id1%>" /> <input type="hidden"
				name="product_line_id" value="<%=product_line_id%>" /> <input
				type="hidden" name="store_title" value="<%=store_title%>" /> <input
				type="hidden" name="product_line_title"
				value="<%=product_line_title%>" />
			</strong>
		</form>

		<form
			action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/creatDeliveryOrder.action"
			name="add_deliveryOrder_form">
			<input type="hidden" name="purchase_id" /> <input type="hidden"
				name="backurl"
				value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/administrator_delivery_order_detail.html" />
		</form>

		<form action="" method="post" name="applicationApprove_form">
			<input type="hidden" name="transport_id" />
		</form>

		<form
			action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/approveTransportOutbound.action"
			method="post" name="applicationOutboundApprove_form">
			<input type="hidden" name="transport_id" />
		</form>
		<form action="" method="post" name="followup_form">
			<input type="hidden" name="transport_id" /> <input type="hidden"
				name="type" value="1" /> <input type="hidden" name="stage" value="1" />
			<input type="hidden" name="transport_type" value="1" /> <input
				type="hidden" name="transport_content" />
		</form>
	</div>
	<script type="text/javascript">
	function search()
	{
		if($("#search_key").val().length>0)
		{
			document.search_form.key.value = $("#search_key").val();
			document.search_form.submit();
		}
		else
		{
			alert("请输入转运单号");
		}
	}
	
	function filter()
	{
		document.filter_form.send_psid.value = $("#send_ps").getSelectedValue();
		document.filter_form.receive_psid.value = $("#receive_ps").getSelectedValue();
		document.filter_form.status.value = $("#transport_status").getSelectedValue();
		document.filter_form.declarationStatus.value = $("#declarationStatus").getSelectedValue();
		document.filter_form.clearanceStatus.value = $("#clearanceStatus").getSelectedValue();
		document.filter_form.invoiceStatus.value = $("#invoiceStatus").getSelectedValue();
		document.filter_form.drawbackStatus.value = $("#drawbackStatus").getSelectedValue();
		document.filter_form.stock_in_set.value = $("#stock_in_set").getSelectedValue();//运费流程
		document.filter_form.dept.value = $("#dept").getSelectedValue();
		document.filter_form.create_account_id.value = $("#create_account_id").getSelectedValue();
		document.filter_form.transport_status.value = $("#transport_status").getSelectedValue();
		
		document.filter_form.submit();
	}
	
	function delTransport(transport_id)
	{
		if(confirm("确定删除转运单T"+transport_id+"？"))
		{
			var para = "transport_id="+transport_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminitrrator/transport/delTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function applicationApprove(transport_id)
	{
		if(confirm("确定申请T"+transport_id+"转运单完成?"))
		{
			document.applicationApprove_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/addTransportApprove.action";
			document.applicationApprove_form.transport_id.value = transport_id;
			document.applicationApprove_form.submit();
		}
	}
	
	function closeWin()
	{
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
	
	function transportLogs(transport_id)
	{
		var uri = 'transport_logs.html?transport_id='+transport_id;
		$.artDialog.open(uri, {title: '日志 转运单号:'+transport_id,width:'970px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function addTransportOrder()
	{
		var uri = "add_transport.html";
		$.artDialog.open(uri, {title: '创建转运单',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function createDeliveryOrder(purchase_id)
	{
		document.add_deliveryOrder_form.purchase_id.value = purchase_id;
		document.add_deliveryOrder_form.submit();	
		tb_remove();
	}	
	
	//卸货司机签到
	function bookDoorOrLocation(transport_id, rel_occupancy_use)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/book_door_or_location.html?transport_id='+transport_id+'&rel_occupancy_use='+rel_occupancy_use; 
		$.artDialog.open(uri , {title: "转运单["+transport_id+"]司机签到",width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	
	function approveOutbound(transport_id)
	{
		document.applicationOutboundApprove_form.transport_id.value = transport_id;
		document.applicationOutboundApprove_form.submit();
	}
	
	function reBackTransport(transport_id)
	{
		if(confirm("确定转运单T"+transport_id+"停止装箱？（将按照转运商品回退库存）"))
		{
			var para = "transport_id="+transport_id+"&is_need_notify_executer=true";
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/reBackTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function reStorageTransport(transport_id)
	{
		if(confirm("确定转运单T"+transport_id+"中止运输？"))
		{
			var para = "transport_id="+transport_id+"&is_need_notify_executer=true";
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/reStorageTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
</script>

	<!-- 浮动表头 -->
	<link href="../js/table_header_lock/float_header/css/component.css"
		rel="stylesheet" type="text/css" />
	<script
		src="../js/table_header_lock/float_header/jquery.ba-throttle-debounce.min.js"
		type="text/javascript"></script>
	<script
		src="../js/table_header_lock/float_header/jquery.stickyheader.js"
		type="text/javascript"></script>

	<script type="text/javascript">
//更多单据
function moreRelateBill(transportId,purchase_id)
{
	var uri = "moreRelateBill.html?transport_id="+transportId+"&purchase_id="+purchase_id;
	$.artDialog.open(uri, {title: '转运单相关信息:'+transportId,width:'970px',height:'500px', lock: true,opacity: 0.3});
}

//浮动表头
$("#dataTable").floatHeader(null);
</script>

</body>
</html>



