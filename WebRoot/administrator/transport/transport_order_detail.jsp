<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<jsp:useBean id="transportOrderKey" class="com.cwc.app.key.TransportOrderKey"/>
<jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey"></jsp:useBean>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/> 
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="transportQualityInspectionKey" class="com.cwc.app.key.TransportQualityInspectionKey"/> 
<% 
	long transport_id = StringUtil.getLong(request,"transport_id");
 	TDate tDate = new TDate();
	//fileFlag
	String fileFlag= StringUtil.getString(request,"flag");
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
	int isPrint = StringUtil.getInt(request,"isPrint",0);
	
	int number = 0;
	boolean edit = false;
	
	//准备中的转运单,并且没有货款申请,转运单明细可修改
	if(transport.get("transport_status",0)==TransportOrderKey.READY&&applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,transport_id,FinanceApplyTypeKey.DELIVERY_ORDER).length==0)
	{
		edit = true;
	}
	HashMap followuptype = new HashMap();
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改转运单详细记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	String updateTransportCertificateAction =   ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportCertificateCompleteAction.action";
	//读取配置文件中的purchase_tag_types
		String tagType = systemConfig.getStringConfigValue("transport_tag_types");
		String[] arraySelected = tagType.split("\n");
		//将arraySelected组成一个List
		ArrayList<String> selectedList= new ArrayList<String>();
		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
				String tempStr = arraySelected[index];
				if(tempStr.indexOf("=") != -1){
					String[] tempArray = tempStr.split("=");
					String tempHtml = tempArray[1];
					selectedList.add(tempHtml);
				}
		}
		// 获取所有的关联图片 然后在页面上分类
		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
		int[] productFileTyps = {FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE};
		DBRow[] imagesRows = purchaseMgrZyj.getAllProductTagFilesByPcId(transport_id,productFileTyps );//临时拷贝样式，使用方法为采购单，后续开发请注意
		if(imagesRows != null && imagesRows.length > 0 )
		{
			for(int index = 0 , count = imagesRows.length ; index < count ; index++ )
			{
				DBRow tempRow = imagesRows[index];
				String product_file_type = tempRow.getString("product_file_type");
				List<DBRow> tempListRow = imageMap.get(product_file_type);
				if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1))
				{
					tempListRow = new ArrayList<DBRow>();
				}
				tempListRow.add(tempRow);
				imageMap.put(product_file_type,tempListRow);
			}
		}
		// 单证
		String valueCertificate = systemConfig.getStringConfigValue("transport_certificate");
	String[] arraySelectedCertificate = valueCertificate.split("\n");
	//将arraySelected组成一个List
	ArrayList<String> selectedListCertificate= new ArrayList<String>();
	for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
			String tempStr = arraySelectedCertificate[index];
			if(tempStr.indexOf("=") != -1){
				String[] tempArray = tempStr.split("=");
				String tempHtml = tempArray[1];
				selectedListCertificate.add(tempHtml);
			}
	}
	int file_with_type = StringUtil.getInt(request,"file_with_type");
	String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
	String file_with_class = StringUtil.getString(request,"file_with_class");
	DBRow[] imageListCertificate = fileMgrZJ.getFileByWithIdAndType(transport_id,FileWithTypeKey.transport_certificate);
 	Map<String,List<DBRow>> mapCertificate = new HashMap<String,List<DBRow>>();
 	TransportCertificateKey certificateKey = new TransportCertificateKey();
	if(imageListCertificate != null && imageListCertificate.length > 0){
		//map<string,List<DBRow>>
		for(int index = 0 , count = imageListCertificate.length ; index < count ;index++ ){
			DBRow temp = imageListCertificate[index];
			List<DBRow> tempListRow = mapCertificate.get(temp.getString("file_with_class"));
			if(tempListRow == null){
				tempListRow = new ArrayList<DBRow>();
			}
			tempListRow.add(temp);
			mapCertificate.put(temp.getString("file_with_class"),tempListRow);
		}
	}
	// 实物图片
	String updateTransportAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportProductFileCompleteAction.action";
	String valueProductFile = systemConfig.getStringConfigValue("transport_product_file");
	String[] arrayProductFileSelected = valueProductFile.split("\n");
 	String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/transport/transport_order_detail.html?transport_id="+transport_id;
 	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=0==transport.get("purchase_id",0L)?"转运单":"交货型转运单"%></title>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<style type="text/css">
	body{text-align:left;}
	ol.fundsOl{
		list-style-type: none;	
	}
	ol.fundsOl li{
		width: 350px;
		height: 200px;
		float: left;
		line-height: 25px;
	}
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<!-- jqgrid 引用 -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>
<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>

<script type="text/javascript">
	var select_iRow;
	var select_iCol;				
	
	function print()
	{
		visionariPrinter.PRINT_INIT("转运单");
			
			visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../transport/print_transport_order_detail.html?transport_id=<%=transport_id%>");
				
			visionariPrinter.PREVIEW();
	}
		
		function searchPurchaseDetail()
		{
			if($("#search_key").val().trim()=="")
			{
				alert("请输入要关键字")
			}
			else
			{
				document.search_form.purchase_name.value=$("#search_key").val();
				document.search_form.submit();
			}	
		}
	
	function uploadTransportOrderDetail()
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_upload_excel.html?transport_id=<%=transport_id%>';
		$.artDialog.open(url, {title: '上传转运单',width:'800px',height:'500px', lock: true,opacity: 0.3});
		//tb_show('上传转运单','transport_upload_excel.html?transport_id=<%=transport_id%>&TB_iframe=true&height=500&width=800',false);
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
			 
	function clearPurchase()
	{
		if(confirm("确定清空该采购单内所有商品吗？"))
		{
			document.del_form.submit();
		}
	}
	
	function virtualDeliveryOrder(filename,purchase_id)
	{
		document.virtual_transport_form.filename.value = filename;
		document.virtual_transport_form.purchase_id.value = purchase_id;
		
		document.virtual_transport_form.submit();
		
		tb_remove();
		
	}
	
	function logout()
	{
		if (confirm("确认退出系统吗？"))
		{
			document.logout_form.submit();
		}
	}
	
	function downloadTransportOrder(transport_id)
	{
		var para = "transport_id="+transport_id;
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/downTransportOrder.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
	}
	
	function saveDeliveryOrder()
	{
			if(confirm("确定保存对此交货单的修改吗？"))
			{
				jQuery("#gridtest").jqGrid('saveCell',select_iRow,select_iCol);
				<%
					if(transport_id !=0)
					{
				%>
						document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/saveDeliveryOrder.action";
				<%	
					}
					else
					{
				%>
					
					document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/addDeliveryOrder.action";
				<%	
					}
				%>
				
				document.save_deliveryOrder_form.waybill_number.value =  $("#waybill_id").val();
				document.save_deliveryOrder_form.waybill_name.value = $("#waybill").val();
				document.save_deliveryOrder_form.arrival_eta.value = $("#eta").val();
				
				document.save_deliveryOrder_form.submit();
			}
	}
	
	function autoComplete(obj)
	{
		addAutoComplete(obj,
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
	}
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	function refreshWindow(){
		window.location.reload();
	}
	
	function ajaxModProductName(obj,rowid,col)
	{
		var unit_name;//单字段修改商品名时更换单位专用
		var para ="transport_detail_id="+rowid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportDetailJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				async:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}
	function showStockOut(transport_id) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_stock_temp_out.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '转运单出库',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function allocate(transport_id) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/allocate_detail.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '拣货单详细信息',width:'900px',height:'500px', lock: true,opacity: 0.3});
	}
	function goodsArriveDelivery(transport_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_goods_arrive_delivery.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '到货派送['+transport_id+"]",width:'500px',height:'200px', lock: true,opacity: 0.3});
	}
	function showStockIn(transport_id) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_stock_temp_in.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '转运单入库',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}
	
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
	
	function createWaybill(transport_id)
	{
		
		$.artDialog.open("transport_waybill.html?transport_id="+transport_id, {title: '转运单创建运单',width:'670px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function uploadInvoice(transport_id)
	{
		$.artDialog.open("transport_upload_invoice.html?transport_id="+transport_id, {title: '转运单上传商业发票',width:'670px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function printWayBill(id,print_page)
	{
		 
		 var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+print_page+"?id="+id+"&type=T";
		
		 $.artDialog.open(uri, {title: '打印运单',width:'960px',height:'500px', lock: true,opacity: 0.3});
   	}
	function changeType(obj) {
		$('#declaration').attr('style','display:none');
		$('#clearance').attr('style','display:none');
		if($(obj).val()==1) {
			$('#clearance').attr('style','');
		}else if($(obj).val()==2) {
			$('#declaration').attr('style','');
		}
	}
	function promptCheck(v,m,f)
	{
		if (v=="y")
		{
			 if(f.content == "")
			 {
					alert("请填写适当备注");
					return false;
			 }
			 else
			 {
			 	if(f.content.length>300)
			 	{
			 		alert("内容太多，请简略些");
					return false;
			 	}
			 }
			 return true;
		}
	}
	function followup(transport_id,declaration,clearance)
	{
		if((declaration>1&&declaration!=4)||(clearance>1&&clearance!=4)) {
			var text = "<div id='title'>转运单跟进[单号:"+transport_id+"]</div><br />";
			text += "跟进范围:<select id='type' name='type' onchange='changeType(this)'>";
			text += declaration>1&&declaration!=4?"<option value='2'>出口报关</option>":"";
			text += clearance>1&&clearance!=4?"<option value='1'>进口清关</option>":"";
			text += "</select>&nbsp;&nbsp;";
			text += "跟进阶段:";
			var b = false;		
			b1 = declaration>1&&declaration!=4;
	  		text += "<select name='declaration' id='declaration' "+(!b?(b1?"":"style='display:none'"):"style='display:none'")+">";
	  		<%	
	  			ArrayList statuses2 = declarationKey.getStatuses();
	  			for(int i=2;i<statuses2.size();i++) {
	  				int statuse = Integer.parseInt(statuses2.get(i).toString());
	  				String key1 = declarationKey.getStatusById(statuse);
			%>
			text += "<option value='<%=statuse%>'><%=key1%></option>";
			<%
	  			}
	  		%>
	  		text += "</select>";
			b = b?b:b1;  		
			b1 = clearance>1&&clearance!=4;
	  		text += "<select name='clearance' id='clearance' "+(!b?(b1?"":"style='display:none'"):"style='display:none'")+">";
	  		<%	
	  			ArrayList statuses3 = clearanceKey.getStatuses();
	  			for(int i=2;i<statuses3.size();i++) {
	  				int statuse = Integer.parseInt(statuses3.get(i).toString());
	  				String key1 = clearanceKey.getStatusById(statuse);
			%>
			text += "<option value='<%=statuse%>'><%=key1%></option>";
			<%
	  			}
	  		%>
	  		text += "</select>";
			b = b?b:b1;
			text += '<br/>ETA:<input type="text" value="" id="eta" name="eta" />';
			$('#eta').datepicker({
				dateFormat:"yy-mm-dd",
				changeMonth: true,
				changeYear: true,
				onSelect:function(dateText, inst){
					var content = $("#content").val();
					 if(content.indexOf("预计时间") != -1){
						 content = content.replace("预计时间","").substr(11);
					}
					 $("#content").val("预计时间" + dateText + ":"+content);
				}
			});
			text += "<br/>备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>";
			$.prompt(text,
			{
				  submit:promptCheck,
		   		  loaded:
						function ()
						{
						}
				  ,
				  callback: 
						function (v,m,f)
						{
							if (v=="y")
							{
									document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/transportLogsInsertAction.action"
									document.followup_form.transport_id.value = transport_id;
									document.followup_form.type.value = f.type;
									if(f.type==1) {
										document.followup_form.stage.value = f.clearance;
									}else if(f.type==2) {
										document.followup_form.stage.value = f.declaration;
									}
									document.followup_form.transport_content.value = f.content;
									document.followup_form.transport_type.value = 1;
									document.followup_form.expect_date.value = f.eta;
									document.followup_form.submit();	
							}
						}
				  ,
				  overlayspeed:"fast",
				  buttons: { 提交: "y", 取消: "n" }
			});
		}else {
			alert("没有跟进项目");
		}
	}
	function followup_clearance_declaration(transport_id,declaration,clearance){
		if((declaration>1&&declaration!=4)||(clearance>1&&clearance!=4)){
		 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport_log/transport_log_declaration_clear_add.html"; 
				uri += "?transport_id="+transport_id+"&declaration="+declaration+"&clearance="+clearance;
	 		$.artDialog.open(uri , {title: "转运单跟进["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
		}else{
			alert("无跟进项目!");
		}
	}
	function followupTransport(transport_id)
	{
	 	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' +"administrator/transport_log/transport_log_declaration_clear_add.html?transport_id="+transport_id;
		 $.artDialog.open(uri , {title: "日志跟进["+transport_id+"]",width:'450px',height:'320px', lock: true,opacity: 0.3,fixed: true});
	}
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
 	function deleteFileTable(_file_id){
  	   $.ajax({
 			url:'<%= deleteFileAction%>',
 			dataType:'json',
 			data:{table_name:'file',file_id:_file_id,folder:'',pk:'file_id'},
 			success:function(data){
 				if(data && data.flag === "success"){
 					window.location.reload();
 				}else{
 					showMessage("系统错误,请稍后重试","error");
 				}
 			},
 			error:function(){
 				showMessage("系统错误,请稍后重试","error");
 			}
 		})
  	}
	function goApplyFunds(id)
	{
		var id="'F"+id+"'";
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=searchby_index&search_mode=1&apply_id="+id);       
	}

	function onlineScanner(target){
		var _target = "";
		if(target == "uploadCertificateImageForm"){
			_target = "uploadCertificateImageForm";
		}else if(target == "qualityInspectionForm" ){
			_target = "qualityInspectionForm";
		}
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target="+_target; 
		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}

	//file表中的图片在线显示
    function showPictrueOnlinef(fileWithType,fileWithId , currentName){
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			cmd:"multiFile",
			table:'file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_transport")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}

    //file表中的  单证信息的图片在线显示
    function showPictrueOnlinecer(fileWithType,fileWithId , currentName){
    	var file_with_class = $("#file_with_class_certificate").val();
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			file_with_class :file_with_class,
			cmd:"sortMultiFile",
			table:'file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_transport")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}

	
	//product_file表中的图片在线显示
	function showPictrueOnline1(fileWithType,fileWithId , currentName,product_file_type){
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			product_file_type:product_file_type,
			cmd:"multiFile",
			table:'product_file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_product")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}

	function getBLPSelect(pc_id,receive_id)
	{
		var selectString = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/LPType/getBLPSelectForJqgrid.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			async:false,
			data:{pc_id:pc_id,to_ps_id:receive_id},
			
			beforeSend:function(request){
			},
			
			error: function(){
				alert("提交失败，请重试！");
			},
			
			success: function(data)
			{
				selectString = data;
			}
		});
		return selectString;
	}

	function getCLPSelect(pc_id)
	{
		var selectString = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/LPType/getCLPSelectForJqgrid.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			async:false,
			data:{
				pc_id:pc_id,
				to_ps_id:<%=transport.get("receive_psid",0l)%>,
				title_id:<%=transport.get("title_id",0l)%>
			},
			
			beforeSend:function(request){
			},
			
			error: function(){
				alert("提交失败，请重试！");
			},
			
			success: function(data)
			{
				selectString = data;
			}
		});
		return selectString;
	}
</script>



<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:text-top;
		padding-top:2px;
	}
.set{
	border:2px #999999 solid;
	padding:2px;
	width:90%;
	word-break:break-all;
	margin-top:10px;
	margin-top:5px;
	line-height:18px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	margin-bottom: 10px;
}
	.create_order_button{background-attachment: fixed;background: url(../imgs/create_order.jpg);background-repeat: no-repeat;background-position: center center;height: 51px;width: 129px;color: #000000;border: 0px;}
	.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
	.zebraTable td {line-height:25px;height:25px;}
	.right-title{line-height:20px;height:20px;}
	span.spanBold{font-weight:bold;}
	span.fontGreen{color:green;}
	span.fontRed{color:red;}
	span.spanBlue{color:blue;}
</style>
<script>
function openApplyMoneyInsert() {
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/financial_management/apply_money_transport_insert.html?associationId=<%=transport.getString("transport_id")%>";

	$.artDialog.open(uri, {title: '申请费用',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showTransit()
{
	//var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_transit_update.html?transport_id=<%=transport.getString("transport_id")%>";
	<%
		String uri = "";
		if(transport.get("purchase_id",0l)==0)
		{
			uri = ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_basic_update.html?isOutter=2&transport_id="+transport.getString("transport_id");
		}
		else
		{
			uri = uri = ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_purchase_basic_update.html?isOutter=2&transport_id="+transport.getString("transport_id"); 
		}
	%>
	$.artDialog.open('<%=uri%>', {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function updateTransportAllProdures(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_wayout_update.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '修改各流程信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showFreight(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_freight_update.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showFreightCost(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+'administrator/transport/setTransportFreight.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>&fr_id=<%=transport.getString("fr_id")%>&purchase_id=<%=transport.get("purchase_id",0) %>';
	$.artDialog.open(uri, {title: '修改运费',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showWayout(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_wayout_update.html?transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showSetDrawback(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transportSetDrawback.html?transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}

function applyFreigth(amount,applyTransferFreightTotal,purchase_id) {
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_money_transport_insert.html?associationId=<%=transport_id%>&amount='+amount+'&subject=1&purchase_id='+purchase_id+"&applyTransferFreightTotal="+applyTransferFreightTotal;
	$.artDialog.open(url, {title: '申请运费',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function fileWithClassCertifycateChange(){
	var file_with_class = $("#file_with_class_certificate").val();
   	$("#tabsCertificate").tabs( "select" , file_with_class * 1-1 );
}
function deleteFileCommon(file_id , tableName , folder,pk){
	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:tableName,file_id:file_id,folder:folder,pk:pk},
		success:function(data){
			if(data && data.flag === "success"){
				window.location.reload();
			}else{
				showMessage("系统错误,请稍后重试","error");
			}
		},
		error:function(){
			showMessage("系统错误,请稍后重试","error");
		}
})
}
 
jQuery(function($){
	if($.trim('<%= fileFlag%>') === "1"){
		showMessage("上传文件出错","error");
	}
	if($.trim('<%= fileFlag%>') === "2"){
		showMessage("请选择正确的文件类型","error");
	}
})
function cerficateCompelte(){
	$.artDialog.confirm('单证流程完成后文件只能上传不能删除,确认继续吗？', function(){
		$.ajax({
			url:'<%= updateTransportCertificateAction%>',
			data:{transport_id:'<%= transport_id%>',transport_date:'<%= transport.getString("transport_date")%>'},
			dataType:'json',
			success:function(data){
				if(data && data.flag == "success"){
					window.location.reload();
				}else{showMessage("系统错误,请稍后重试","alert");}
			},
			error:function(){
			  showMessage("系统错误,请稍后重试","alert");
			}
		})
	}, function(){
	});
}
function productFileCompelte(){
	var isFlag =  window.confirm("所有商品相关文件都上传了吗？");
	 if(isFlag){
		 $.ajax({
	    			url:'<%= updateTransportAction%>',
	    			data:{transport_id:'<%= transport_id%>',transport_date:'<%= transport.getString("transport_date")%>'},
	    			dataType:'json',
	    			success:function(data){
	    				if(data && data.flag == "success"){
	    					window.location.reload();
	    				}else{showMessage("系统错误,请稍后重试","alert");}
	    			},
	    			error:function(){
	    			  showMessage("系统错误,请稍后重试","alert");
	    			}
	    		})
		   }
}
function followUpCerficate(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_certificate_follow_up.html?transport_id="+<%= transport_id%>;
	$.artDialog.open(uri , {title: "单证跟进["+<%= transport_id%>+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
function productFileFollowUp(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_file_follow_up.html?transport_id="+<%= transport_id%>;
	$.artDialog.open(uri , {title: "实物图片跟进["+<%= transport_id%>+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});	
}
function showSingleLogs(transport_id,transport_type,title){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_show_single_log.html?transport_id="+<%= transport_id%>+"&transport_type="+transport_type;
	$.artDialog.open(uri , {title: title+"["+<%= transport_id%>+"]",width:'870px',height:'470px', lock: true,opacity: 0.3,fixed: true});	
}
function clearanceButton(transport_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_clearance.html?transport_id="+transport_id; 
	$.artDialog.open(uri , {title: "清关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
function declarationButton(transport_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_declaration.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "报关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
function quality_inspectionKey(transport_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_quality_inspection.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "质检流程["+transport_id+"]",width:'570px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}
function tag(transport_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_tag.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "跟进内部标签["+transport_id+"]",width:'570px',height:'290px', lock: true,opacity: 0.3,fixed: true});
}
function tagThird(transport_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_third_tag.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "跟进第三方标签["+transport_id+"]",width:'570px',height:'290px', lock: true,opacity: 0.3,fixed: true});
}
function transportApplyMoney(_purchase_id,_type){

	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder") %>action/administrator/transport/CheckTransportApplyPromptAction.action',
		data:'transport_id=<%= transport_id%>',
		dataType:'json',
		type:'post',
		success:function(data){
			if(data && data.result)
			{
				alert(data.result);
			}
			else
			{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_apply_money.html?purchase_id="+_purchase_id+"&id="+_type+"&transport_id="+<%= transport_id%>;
				$.artDialog.open(uri , {title: "交货单["+'<%= transport_id%>'+"]申请货款",width:'900px',height:'570px', lock: true,opacity: 0.3,fixed: true});
			}
		},
		error:function(){
			showMessage("系统错误","error");
		}
	});
    
}
 
function stock_in_set(transport_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_stockinset.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "运费流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,_target){
    var targetNode = $("#"+_target);
    $("input[name='file_names']",targetNode).val(fileNames);
    if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
        $("input[name='file_names']").val(fileNames);
        var myform = $("#"+_target);
        var file_with_class = $("#file_with_class").val();
			$("input[name='backurl']",targetNode).val("<%= backurl%>" + "&file_with_class="+file_with_class);
		  	myform.submit();
	}
}

function updateTransportVW(transport_id)
	{
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/updateTransportDetailVW.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:{transport_id:transport_id},
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date)
					{
						if(date.result=="ok")
						{
							window.location.reload();
						}
					}
				});
	}
function transportProductFileFinishToNeed()
{
	if(confirm("您确定要修改实物图片流程从完成到需要阶段吗？"))
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_product_file_finish_to_need.html?transport_id=<%=transport_id%>";
		$.artDialog.open(uri, {title: '修改实物图片完成-->需要['+<%=transport_id%>+']',width:'570px',height:'280px', lock: true,opacity: 0.3});

	}
}
</script>
</head>

<body onload="onLoadInitZebraTable();">
<div class="demo">
<div id="transportTabs">
	<ul>
		<li><a href="#transport_basic_info">基础信息<span> </span></a></li>
		<li><a href="#transport_info">运单信息<span> </span></a></li>
		<%if(transport.get("stock_in_set",transportStockInSetKey.SHIPPINGFEE_NOTSET) != transportStockInSetKey.SHIPPINGFEE_NOTSET) {%>
			<%
	  						//计算颜色
	  						String stockInSetClass = "" ;
				int stockInSetInt = transport.get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET);
				if(stockInSetInt == TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH){
  								stockInSetClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_STOCKINSET);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									stockInSetClass += " fontGreen";
  								}else{
  									stockInSetClass += " fontRed";
  								}
				}else if(stockInSetInt != TransportStockInSetKey.SHIPPINGFEE_NOTSET){
					stockInSetClass += "spanBold spanBlue";
  							}
  				 %>
			<li><a href="#transport_price_info"><span class="<%=stockInSetClass %>">运费信息</span></a></li>
		<%} %>
	 	<%if(transport.get("declaration",declarationKey.NODELARATION) != declarationKey.NODELARATION) {%>
			<%
	  						//计算颜色
	  						String declarationKeyClass = "" ;
				int declarationInt = transport.get("declaration",declarationKey.NODELARATION);
				if(declarationInt == declarationKey.FINISH){
  								declarationKeyClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_declaration);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									declarationKeyClass += " fontGreen";
  								}else{
  									declarationKeyClass += " fontRed";
  								}
				}else if(declarationInt != declarationKey.NODELARATION ){
					declarationKeyClass +=  "spanBold spanBlue";
  							}
	  	 %>
			<li><a href="#transport_out_info"><span class="<%=declarationKeyClass %>">出口报关 </span></a></li>
		<%} %>
		<%if(transport.get("clearance",clearanceKey.NOCLEARANCE) != clearanceKey.NOCLEARANCE) {%>
			 <%
	  						//计算颜色
	  						String clearanceKeyClass = "" ;
				int clearanceInt = transport.get("clearance",clearanceKey.NOCLEARANCE);
				if( clearanceInt == clearanceKey.FINISH){
  								clearanceKeyClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_clearance);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									clearanceKeyClass += " fontGreen";
  								}else{
  									clearanceKeyClass += " fontRed";
  								}
				}else if(clearanceInt != clearanceKey.NOCLEARANCE){
					clearanceKeyClass +=  "spanBold spanBlue";
  							}
	  					%>
			<li><a href="#transport_in_info"><span class="<%= clearanceKeyClass%>">进口清关</span></a></li>
		<%} %>
	 				<%
	  						//计算颜色
	  						String tagClass = "" ;
  							int tagInt = transport.get("tag",TransportTagKey.NOTAG);
  							if(tagInt == TransportTagKey.FINISH){
  								tagClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_TAG);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									tagClass += " fontGreen";
  								}else{
  									tagClass += " fontRed";
  								}
  							}else if(tagInt != TransportTagKey.NOTAG){
  								tagClass += "spanBold spanBlue";
  							}
	  					%>
		<li><a href="#transport_tag_make"><span class='<%= tagClass%>'>内部标签</span></a></li>
		
		
		<%
		int tagIntThird = transport.get("tag_third",TransportTagKey.NOTAG);
		if(tagIntThird != TransportTagKey.NOTAG)
		{
			//计算颜色
			String tagClassThird = "" ;
			
			if(tagIntThird == TransportTagKey.FINISH){
				tagClassThird += "spanBold";
				int[] fileType	= {FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE};
		  		if(purchaseMgrZyj.checkPurchaseThirdTagFileFinish(transport_id,fileType,"transport_tag_types"))
		  		{
					tagClassThird += " fontGreen";
				}else{
					tagClassThird += " fontRed";
				}
			}else if(tagIntThird != TransportTagKey.NOTAG){
				tagClassThird += "spanBold spanBlue";
			}
		%>
		<li><a href="#transport_tag_third"><span class='<%= tagClassThird%>'>第三方标签</span></a></li>
 		<%} %>
 		<%if(transport.get("certificate",certificateKey.NOCERTIFICATE) != certificateKey.NOCERTIFICATE) {%>
  							<%
	  						//计算颜色
	  						String certificateClass = "" ;
  							int certificateInt = transport.get("certificate",TransportCertificateKey.NOCERTIFICATE);
  							if(certificateInt == TransportCertificateKey.FINISH){
  								certificateClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_certificate);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									certificateClass += " fontGreen";
  								}else{
  									certificateClass += " fontRed";
  								}
  							}else if(certificateInt != TransportCertificateKey.NOCERTIFICATE){
  								certificateClass += "spanBold spanBlue";
  							}
  			 %>
			<li><a href="#transport_order_prove"><span class='<%= certificateClass%>'>单证信息</span></a></li>
		<%} %>
		<%
	  						//计算颜色
	  						String productFileClass = "" ;
  							int productFileInt = transport.get("product_file",TransportProductFileKey.NOPRODUCTFILE);
  							if(productFileInt == TransportProductFileKey.FINISH){
  								productFileClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("product_file",transport.get("transport_id",0l),FileWithTypeKey.product_file);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									productFileClass += " fontGreen";
  								}else{
  									productFileClass += " fontRed";
  								}
  							}else if(productFileInt != TransportProductFileKey.NOPRODUCTFILE){
  								productFileClass += "spanBold spanBlue";
  							}
  			 %>
			<li><a href="#product_file"><span class='<%= productFileClass%>'>实物图片</span></a></li>
 
		<%if(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY) != TransportQualityInspectionKey.NO_NEED_QUALITY) {%>
						<%
	  						//计算颜色
	  						String qualityInspectionClass = "" ;
							int qualityInspectionInt = transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY);
							if(qualityInspectionInt == TransportQualityInspectionKey.FINISH){
  								qualityInspectionClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_QUALITYINSPECTION);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									qualityInspectionClass += " fontGreen";
  								}else{
  									qualityInspectionClass += " fontRed";
  								}
							}else if(qualityInspectionInt != TransportQualityInspectionKey.NO_NEED_QUALITY){
								qualityInspectionClass += "spanBold spanBlue";
  							}
  					 %>
			<li><a href="#quality_inspection"><span class='<%= qualityInspectionClass%>'>质检</span></a></li>
		<%} %>
	</ul>
	<div id="transport_basic_info">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="60%" valign="top">
					<table width="100%">
						<tr>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">转运单号：</font></td>
							<td align="left" width="16%">T<%=transport_id%></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">预计到达日期：</font></td>
							<td align="left" width="16%"><% 
								if(transport.getString("transport_receive_date").trim().length() > 0){
									out.println(tDate.getFormateTime(transport.getString("transport_receive_date")));
								}
 								%>
							</td>
							<td width="16%" align="right"height="25"><font style="font-family: 黑体; font-size: 14px;">创建人：</font></td>
							<td align="left" width="16%"><%=transport.getString("create_account")%></td>
						</tr>
						<tr>
							<td align="right" width="16%" height="25"><font style="font-family: 黑体; font-size: 14px;">允许装箱：</font></td>
							<td align="left" width="16%">
								<% 
									DBRow packinger = adminMgr.getDetailAdmin(transport.get("packing_account",0l));
									if(packinger!=null)
									{
										out.print(packinger.getString("employe_name"));
									}
								%>
							</td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">创建时间：</font></td>
							<td align="left"><%=tDate.getFormateTime(transport.getString("transport_date"))%></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">TITLE：</font></td>
							<td align="left">
							<%
		  						DBRow titleRow = proprietaryMgrZyj.findProprietaryByTitleId(transport.get("title_id", 0L));
		  						if(null != titleRow)
		  						{  							
									out.println(titleRow.getString("title_name"));
		  						}
		  						else
		  						{
		  							out.println("&nbsp;");
		  						}
							%>
							</td>
						</tr>
						<tr>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">货物状态：</font></td>
							<td align="left" width="16%"><%=transportOrderKey.getTransportOrderStatusById(transport.get("transport_status",0)) %></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">出口报关：</font></td>
							<td align="left" width="16%"><%=declarationKey.getStatusById(transport.get("declaration",01)) %></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">进口清关：</font></td>
							<td align="left" width="16%"><%=clearanceKey.getStatusById(transport.get("clearance",01)) %></td>
						</tr>
						<tr>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">运费状态：</font></td>
							<td align="left" width="16%"><%=transportStockInSetKey.getStatusById(transport.get("stock_in_set",transportStockInSetKey.SHIPPINGFEE_NOTSET)) %></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">制签流程：</font></td>
							<td align="left" width="16%">
							<% 
								if(transport.get("tag",transportTagKey.NOTAG) == transportTagKey.TAG){
									out.print("制签中");
								}else{
									out.print(transportTagKey.getTransportTagById(transport.get("tag",transportTagKey.NOTAG)));
								}
							 %>
							</td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">单证流程：</font></td>
							<td align="left" width="16%"><%=certificateKey.getStatusById(transport.get("certificate",certificateKey.NOCERTIFICATE)) %></td>
						</tr>
						<tr>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">实物图片：</font></td>
							<td align="left" width="16%"><%= transportProductFileKey.getStatusById(transport.get("product_file",transportProductFileKey.NOPRODUCTFILE)) %></td>
							<td width="16%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">质检：</font></td>
							<td align="left" width="16%"><%= transportQualityInspectionKey.getStatusById(transport.get("quality_inspection",transportQualityInspectionKey.NO_NEED_QUALITY)+"") %></td>
							<td width="16%" align="right" height="25"></td>
							<td align="left" width="16%"></td>
						</tr>
					</table>
				</td>
				<td width="40%">
				<%
					if(!transport.getString("purchase_id").equals("0"))
					{
				%>
				
						<fieldset class="set" style="border-color:#993300;">
						<%
							DBRow[] applyMoneysPurchase = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100009,transport.get("purchase_id",0L),FinanceApplyTypeKey.PURCHASE_ORDER);
								//applyMoneyMgrLL.getApplyMoneyByBusiness(transport.getString("purchase_id"),4);//
						%>
						<legend style="font-size:12px;font-weight:normal;color:#000000;background:none;">
							<span style="font-size:12px;color:#000000;font-weight:normal;">
							<a href="javascript:void(0)" onclick="window.open('<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail.html?purchase_id=<%=transport.get("purchase_id",0) %>')">P<%=transport.getString("purchase_id")%></a>
								<%
								DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(transport.getString("purchase_id"));
								String statusName = "";
								if(applyMoneysPurchase.length > 0)
													{
									List imageList = applyMoneyMgrLL.getImageList(applyMoneysPurchase[0].getString("apply_id"),"1");
									//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
								 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneysPurchase[0].getString("status")) && imageList.size() < 1)
													{
								 		statusName = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
								 	}else{
								 		statusName = fundStatusKey.getFundStatusKeyNameById(applyMoneysPurchase[0].getString("status"));	
													}
								 	statusName = "("+statusName+")";
								}
								%>
								采购单定金
								(<%=purchase.get("apply_money_over",0d)==0?df.format(tDate.getDiffDate(purchase.getString("purchase_date"),"dd"))+"天":purchase.get("apply_money_over",0d)+"天完成"%>)
							</span>
						</legend>
						<table style="width:100%">
							<tr style="width:100%">
								<td style="width:100; padding-left: 10px;">
									采购单金额:<%=purchaseMgr.getPurchasePrice(transport.get("purchase_id",0l))%>RMB
										</td>
									</tr>
											<%
				  				if(applyMoneysPurchase.length>0) {
				  					String moneyStandardStr = "";
				  					if(!"RMB".equals(applyMoneysPurchase[0].getString("currency")))
				  					{
				  						moneyStandardStr = "/"+applyMoneysPurchase[0].getString("standard_money")+"RMB";
				  					}
				  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneysPurchase[0].get("apply_id",0l)+")\">F"+applyMoneysPurchase[0].get("apply_id",0)+"</a>"+ statusName +applyMoneysPurchase[0].get("amount",0f)+applyMoneysPurchase[0].getString("currency")+moneyStandardStr+"</td></tr>");
				  					DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneysPurchase[0].getString("apply_id"));
					  			for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
					  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
					  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
					  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
					  				int moneyStatus = applyTransferRows[ii].get("status",0);
					  				String transferMoneyStandardStr = "";
					  				if(!"RMB".equals(currency))
					  				{
					  					transferMoneyStandardStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
					  				}
					  				if(moneyStatus != 0 )
						  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
					  				else
						  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
						  			}
					  			}
											%>
								</table>
						</fieldset>
			<% 
				  				DBRow[] deliveryOrderRows = transportMgrZyj.getTransportRowsByPurchaseId(Long.valueOf(transport.getString("purchase_id")));
								if(deliveryOrderRows.length > 0)
								{
									for(int de = 0; de < deliveryOrderRows.length; de ++)
									{
										String statusNameTr = "";
										String appMoneyStateStr = "";
										String selfColor = "";
										if(transport.getString("transport_id").equals(deliveryOrderRows[de].getString("transport_id")))
										{
											selfColor = "border-color:blue;";
										}else{
											selfColor = "border-color:#993300;";
										}
										DBRow[] applyMoneyDelivers = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,deliveryOrderRows[de].get("transport_id",0L),FinanceApplyTypeKey.DELIVERY_ORDER);
											if(applyMoneyDelivers.length > 0)
											{
												List imageListTr = applyMoneyMgrLL.getImageList(applyMoneyDelivers[0].getString("apply_id"),"1");
												//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
											 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneyDelivers[0].getString("status")) && imageListTr.size() < 1)
											 	{
											 		statusNameTr = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
											 	}else{
											 		statusNameTr = fundStatusKey.getFundStatusKeyNameById(applyMoneyDelivers[0].getString("status"));	
											 	}
											 	statusNameTr = "("+statusNameTr+")";
											appMoneyStateStr = statusNameTr + applyMoneyDelivers[0].get("amount",0.0) + applyMoneyDelivers[0].getString("currency");
											}
											%>
									<fieldset class="set" style="<%=selfColor %>">
										<legend>
											<a target="_blank" href='<%=ConfigBean.getStringValue("systenFolder") %>administrator/transport/transport_order_detail.html?transport_id=<%=deliveryOrderRows[de].getString("transport_id") %>'>T<%=deliveryOrderRows[de].getString("transport_id") %></a>
											交货单货款
										</legend>
										<table style="width:100%">
											<tr style="width:100%">
												<td style="width:100%;padding-left: 10px;">
													交货单金额:<%=transportMgrZJ.getTransportSendPrice(deliveryOrderRows[de].get("transport_id",0L)) %>RMB
										</td>
									</tr>
											<%
											if(applyMoneyDelivers.length > 0)
											{
												String moneyStandardDeliverStr = "";
							  					if(!"RMB".equals(applyMoneyDelivers[0].getString("currency")))
							  					{
							  						moneyStandardDeliverStr = "/"+applyMoneyDelivers[0].getString("standard_money")+"RMB";
							  					}
												out.println("<tr><td><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneyDelivers[0].get("apply_id",0l)+")\">F"+applyMoneyDelivers[0].get("apply_id",0)+"</a>"+ appMoneyStateStr+moneyStandardDeliverStr+"</td></tr>");
												DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneyDelivers[0].getString("apply_id"));
									  			for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
									  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
									  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
									  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
									  				int moneyStatus = applyTransferRows[ii].get("status",0);
									  				String transferMoneyStandardDeliverStr = "";
									  				if(!"RMB".equals(currency))
									  				{
									  					transferMoneyStandardDeliverStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
									  				}
								  					if(moneyStatus != 0 )
									  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardDeliverStr+"</td></tr>");
									  				else
									  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardDeliverStr+"</td></tr>");
									  			}
											}
			%>
								</table>
									</fieldset>	
							<%			
									}
								}
							%>
				<%
					}
				%>
				</td>
			</tr>
			<tr><td colspan="2" height="15px"></td></tr>
			<tr>
				<td colspan="2" align="left"><br/>
					<% 
						if(transport.get("transport_status",0)!=TransportOrderKey.AlREADYARRIAL&&transport.get("transport_status",0)!=TransportOrderKey.APPROVEING&&transport.get("transport_status",0)!=TransportOrderKey.FINISH)
						{
					%>
							<input name="button" type="button" class="long-button" value="明细修改" onClick="showTransit()"/>
					<%
						}
					%>
					
					
					<input name="button" type="button" class="long-button" value="修改各流程信息" onClick="updateTransportAllProdures()"/>
					<%
						if(transport.get("transport_status",0)==TransportOrderKey.READY)
						{
					%>
						<input  type="button" class="long-button-upload" value="上传转运单" onclick="uploadTransportOrderDetail()"/>
					<%	
						}
					%>
					
					
					<input  type="button" class="long-button-next" value="下载转运单" onclick="downloadTransportOrder(<%=transport_id%>)"/>
			  		<input type="button" class="long-button-print" value="打印转运单" onclick="print()"/>
				  	<% 
					  	int is_not_page_sub	= transport.get("is_not_page_sub", 0);	//2.手机提交过来的交货单
				  		int is_relate_purchase	= transport.get("is_relate_purchase", 0);	//是否与采购单关联1是2否
						if(transport.get("transport_status",0)==TransportOrderKey.READY&&transport.get("purchase_id",0l)==0&&is_not_page_sub!=2&&2!=is_relate_purchase)
						{
					%>
							<input name="button" type="button" class="long-button" value="装箱" onClick="readyPacking(<%=transport_id%>)"/>
					<% 
						}
					%>
				   <% 
					if((transport.get("purchase_id",0l)==0l&&transport.get("transport_status",0)==TransportOrderKey.PACKING) ||(transport.get("purchase_id",0l)!=0l&&transport.get("transport_status",0)==TransportOrderKey.READY)
							|| (transport.get("purchase_id",0l)==0l&&transport.get("transport_status",0)==TransportOrderKey.READY&&(2==is_not_page_sub||2==is_relate_purchase)))
					{
				   %>
						<input name="button" type="button" class="long-button" value="转运单出库" onClick="showStockOut(<%=transport_id%>)"/>
						<input name="button" type="button" class="long-button" value="allocate" onClick="allocate(<%=transport_id%>)"/>
				   <%
					}
				   %>
				   <%
  					if(TransportOrderKey.INTRANSIT == transport.get("transport_status",0))
  					{
	  				%>
	  				<input type="button" value="到货派送" class="long-button" onclick="goodsArriveDelivery(<%=transport_id%>)"/>
	  				<tst:authentication bindAction="com.cwc.app.api.zj.TransportMgrZJ.transportWarehousingSub">
	  				&nbsp;&nbsp;<input type="button" value="到货并入库" class="long-button" onclick="showStockIn(<%=transport_id%>)"/>
	  				</tst:authentication>
	  				<%		
	  					}
	  				%>
				   <% 
					if((transport.get("transport_status",0)==TransportOrderKey.AlREADYARRIAL))
					{
				   %>
					<tst:authentication bindAction="com.cwc.app.api.zj.TransportMgrZJ.transportWarehousingSub">
						<input name="button" type="button" class="long-button" value="转运单入库" onClick="showStockIn(<%=transport_id%>)"/>
					</tst:authentication>
				   <%
					}
				   %>
			  		<!-- 转运单资金申请 -->
			  		<!-- 
			  			1.交货单(有关联的) 
			  			2.质检,标签,上图图片 (有并且都完成。或者是没有的)
			  			显示资金申请的按钮
			  			3.申请过后就不能再显示这个按钮
			  		-->
			  		<%
			  			long tempPurchaseId = transport.get("purchase_id",0l);
			  			if(tempPurchaseId != 0l)
			  			{	
			  		%>
			  			<input type="button" class="long-button" value="申请货款" onclick="transportApplyMoney('<%=tempPurchaseId %>','100001');"/>
			  		<%
			  			}
			  		%>
				</td>
			</tr>
		</table>
	</div>
	<div id="transport_info">
		<table border="0" width="100%">
			<tr>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">货运公司：</font></td>
			  	<td width="40%" align="left" height="25"><%=transport.getString("transport_waybill_name") %></td>
			  	<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">运单号：</font></td>
				<td width="40%" align="left" height="25"><%=transport.getString("transport_waybill_number") %></td>
			</tr>
			<tr>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">运输方式：</font></td>
			  	<td width="40%" align="left" height="25"><%=transportWayKey.getStatusById(transport.get("transportby",0)) %></td>
			  	<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">承运公司：</font></td>
				<td width="40%" align="left" height="25"><%=transport.getString("carriers") %></td>
			</tr>
			<tr>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">发货港：</font></td>
				<td width="40%" align="left" height="25"><%=transport.getString("transport_send_place")%></td>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">目的港：</font></td>
				<td width="40%" align="left" height="25"><%=transport.getString("transport_receive_place")%></td>
			</tr>
			<tr><td colspan="4" height="15"></td></tr>
			<tr>
				<td style="width:10%;text-align:right;">
					<font style="font-family: 黑体; font-size: 14px;">&nbsp;</font>
				</td>
				<td style="width:90%;" colspan="3">
					<table style="width:100%;" class="addr_table">
						<tr>
							<td style="width:45%;">
								<table style="width:100%;border-collapse:collapse ;">
									<tr>
										<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">提货地址</font></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">提货仓库：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												if(transport.get("purchase_id",0l)==0)
												{
													out.print(null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
													session.setAttribute("billOfLading",null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
												}
												else
												{
													out.print(supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name"));
												}
											%>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_house_number"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_street"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_city"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_zip_code"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
										<td style="border: 1px solid silver;width: 80%;">
			  								<%
												long ps_id3 = null!=transport?transport.get("send_pro_id",0L):0L ;
												DBRow psIdRow3 = productMgr.getDetailProvinceByProId(ps_id3);
											%>
											<%=(psIdRow3 != null? psIdRow3.getString("pro_name"):transport.getString("address_state_send") ) %>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ccid3 = null!=transport?transport.get("send_ccid",0L):0L; 
												DBRow ccidRow3 = orderMgr.getDetailCountryCodeByCcid(ccid3);
			 								%>
											<%= (ccidRow3 != null?ccidRow3.getString("c_country"):"" ) %>	
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_name"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_linkman_phone"):"" %></td>
									</tr>
								</table>
							</td>
							<td style="width:10%;text-align: center;font-size: 18px;">
								-->
							</td>
							<td style="width:45%;">
								<table style="width:100%;border-collapse:collapse ;" class="addr_table">
									<tr>
										<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">收货地址</font></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">收货仓库：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):""%></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_house_number") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_street") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_city") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_zip_code") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ps_id4 = null!=transport?transport.get("deliver_pro_id",0l):0L;
												DBRow psIdRow4 = productMgr.getDetailProvinceByProId(ps_id4);
											%>
											<%=(psIdRow4 != null? psIdRow4.getString("pro_name"):transport.getString("address_state_deliver") ) %>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ccid4 = null!=transport?transport.get("deliver_ccid",0l):0L; 
												DBRow ccidRow4 = orderMgr.getDetailCountryCodeByCcid(ccid4);
											%>
											<%= (ccidRow4 != null?ccidRow4.getString("c_country"):"" ) %>	
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("transport_linkman")%></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("transport_linkman_phone")%></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table><br/>
		<table width="100%">
			<tr>
				<td align="left"">
					<input name="button" type="button" value="运单修改" onClick="showFreight()" class="long-button"/>
					<input  type="button" class="long-button-upload" value="上传商业发票" onclick="uploadInvoice(<%=transport_id %>)"/>
					<%
						if(transport.getString("invoice_path").length()>0)
						{
					%>
					<input type="button" value="下载商业发票" class="long-button-next" onclick="window.location.href='../../<%=transport.getString("invoice_path")%>'"/>
					<%
						}
					%>
			<%
  					if(transport.get("transport_status",0)==TransportOrderKey.INTRANSIT)
  					{
  			%>
<%--									&nbsp;&nbsp;<a href="#" onClick="createWaybill(<%=transport.get("transport_id",0l)%>)">生成快递单</a>--%>
									<input name="button" type="button" value="生成快递单" onClick="createWaybill(<%=transport.get("transport_id",0l)%>)" class="long-button"/>
			<%
					}
							
			%>
			<%
		  	  			DBRow shippingCompany = expressMgr.getDetailCompany(transport.get("sc_id",0l));
		  	  			if(shippingCompany !=null)
		  	  			{
		  	 %>
<%--					  	  				&nbsp;&nbsp;<a href="#" onClick="printWayBill(<%=transport.get("transport_id",0l)%>,'<%=shippingCompany.getString("internal_print_page")%>')">打印快递单</a>--%>
					  	  				<input type="button" class="long-button-print" value="打印快递单" onclick="printWayBill(<%=transport.get("transport_id",0l)%>,'<%=shippingCompany.getString("internal_print_page")%>')"/>
		  	 <%	
		  	  			}
		  	 %>
					</td>
				</tr>
			</table>
		 
							
							
							  	  		
	</div>
 <%
	//如果采购单ID为0，为转运单，否则为交货单
	int associateTypeId = 0 == transport.get("purchase_id",0L)?6:5;
 	if(transport.get("stock_in_set",transportStockInSetKey.SHIPPINGFEE_NOTSET) != transportStockInSetKey.SHIPPINGFEE_NOTSET)
 {%>
	<div id="transport_price_info">
	<p style="margin-bottom:5px;">
		 运费流程阶段:<span style="color:green;font-weight:bold;"><%=transportStockInSetKey.getStatusById(transport.get("stock_in_set",transportStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>
		  <input class="short-button" type="button" onclick='stock_in_set(<%=transport_id%>)' value="运费跟进"/>
			<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',5,'运费日志')"/> 		
	</p>
						<%
					DBRow[] rows = freightMgrLL.getTransportFreightCostByTransportId(transport.getString("transport_id"));//更新数据
					DBRow transportRow = transportMgrLL.getTransportById(transport.getString("transport_id"));;
				%>
		<table id="tables" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table">
			     <tr>
			       <th width="21%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
			       <th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>
			       <th width="13%" nowrap="nowrap" class="right-title"  style="text-align: center;">小计</th>	
			     </tr>
			     <%
		    	 float sum_price = 0;
			     if(rows!=null){
				     for(int i = 0;i<rows.length;i++)
				     {
				    	 DBRow row = rows[i];
				    	 long tfc_id = row.get("tfc_id",0l);
			    		 double tfc_unit_count = row.get("tfc_unit_count",0d);
			    		 String tfc_project_name = "";
			    		 String tfc_way = "";
			    		 String tfc_unit = "";
			    		 String tfc_currency = "";
			    		 double tfc_unit_price = 0;
			    		 double tfc_exchange_rate = 0;
				    	 if(tfc_id != 0) {//以前有数据
				    		 tfc_project_name = row.getString("tfc_project_name");
				    		 tfc_way = row.getString("tfc_way");
				    		 tfc_unit = row.getString("tfc_unit");
				    		 tfc_unit_price = row.get("tfc_unit_price",0d);
				    		 tfc_currency = row.getString("tfc_currency");
				    		 tfc_exchange_rate = row.get("tfc_exchange_rate",0d);
				    	 }
		    	 String bgCol = "#FFFFFF";
     			if(1 == i%2){
     				bgCol = "#F9F9F9";
     			}
			      %>
	     <tr height="30px" style="padding-top:3px;background-color: <%=bgCol %>">
			        <td valign="middle" style="padding-top:3px;">
			        	<input type="hidden" name="tfc_id" id="tfc_id" value="<%=tfc_id %>"/>
			        	<input type="hidden" name="tfc_project_name" id="tfc_project_name" value="<%=tfc_project_name %>"/>
			        	<input type="hidden" name="tfc_way" id="tfc_way" value="<%=tfc_way %>"/>
			        	<input type="hidden" name="tfc_unit" id="tfc_unit" value="<%=tfc_unit %>"/>
			        	
						<%=tfc_project_name %>	
			        </td>
			         <td valign="middle" nowrap="nowrap" >       
			      		<%=tfc_way %>
			        </td>
			        <td align="left" valign="middle">
						<%=tfc_unit %>
			        </td>
			        <td >
						<%=tfc_unit_price %>
			        </td>
			        <td >
						<%=tfc_currency%>
			        </td>
			        <td >
						<%=tfc_exchange_rate %>
			        </td>
			        <td valign="middle" >       
						<%=tfc_unit_count %>
			        </td>
			        <td valign="middle" nowrap="nowrap">       
					<%=MoneyUtil.round(tfc_unit_price*tfc_exchange_rate*tfc_unit_count,2)%>
		        </td>
			     </tr>
			     <%
			     		sum_price += tfc_unit_price*tfc_exchange_rate*tfc_unit_count;
			    	 }
			     %>
				 <tr height="30px">
				 	<td colspan="2">
		 		<fieldset class="set" style="border-color:green;">
					<legend>运费:<%=computeProductMgrCCC.getSumTransportFreightCost(transport.getString("transport_id")).get("sum_price",0d) %>RMB</legend>
					<%
						if(transport.get("stock_in_set",1)==1) {
							out.println("</b></font>");
						}
					%>
					<table style="width:100%" align="left">
					<%
						DBRow[] applyMoneys = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(10015,transport.get("transport_id",0L),associateTypeId);
							//applyMoneyMgrLL.getApplyMoneyByBusiness(transport.getString("transport_id"),associateTypeId);//
						for(int j=0; j<applyMoneys.length; j++){
							String freightCostStr = "";
							if(!"RMB".equals(applyMoneys[j].getString("currency")))
							{
								freightCostStr = "/"+applyMoneys[j].get("standard_money",0f)+"RMB";
							}
					%>	
								<tr><td align='left' style="border: 0px;">
								<a href="javascript:void(0)" onClick='goApplyFunds(<%=applyMoneys[j].get("apply_id",0) %>)'>F<%=applyMoneys[j].get("apply_id",0) %></a>
								<%
									List imageListStock = applyMoneyMgrLL.getImageList(applyMoneys[j].getString("apply_id"),"1");
									String stockStatusName = "";
									//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
								 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneys[j].getString("status")) && imageListStock.size() < 1)
								 	{
								 		stockStatusName = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
								 	}else{
								 		stockStatusName = fundStatusKey.getFundStatusKeyNameById(applyMoneys[j].getString("status"));	
								 	}
								 	stockStatusName = "("+stockStatusName+")";
								%>
								<%=stockStatusName %>
								<%=applyMoneys[j].get("amount",0f) %><%=applyMoneys[j].getString("currency") %><%=freightCostStr %>
								</td></tr>
							<%
						 			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneys[j].getString("apply_id"));
						 				//applyMoneyMgrLL.getApplyTransferByBusiness(transport.getString("transport_id"),6);
						 			if(applyTransferRows.length>0) {
						 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
						 					if(applyTransferRows[ii].get("apply_money_id",0) == applyMoneys[j].get("apply_id",0)) {
							  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
							  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
							  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
							  				int moneyStatus = applyTransferRows[ii].get("status",0);
							  				String transferMoneyStandardFreightStr = "";
							  				if(!"RMB".equals(currency))
							  				{
							  					transferMoneyStandardFreightStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
							  				}
							  				if(moneyStatus != 0 )
							  					out.println("<tr><td align='left' style='border: 0px;'><a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>已转账"+amount+" "+currency+transferMoneyStandardFreightStr+"</td></tr>");
							  				else
							  					out.println("<tr><td align='left' style='border: 0px;'><a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请转账"+amount+" "+currency+transferMoneyStandardFreightStr+"</td></tr>");
							  				}	
						 				}
						 			}
						 		}
						 	%>
				 	</table>
			 	</fieldset>
				 	</td>
				 	<td colspan="5" align="right">总计</td>
				 	<td nowrap="nowrap"><%=sum_price %></td>
				 </tr>
				 <%
			     }
			     %>
	     </table><br/>
	     <table style="border: 0; width: 98%">
			 <tr>
	     		<td colspan="10" align="left" style="border: 0">
				<%
					double applyTransferFreightTotal = applyFundsMgrZyj.getTransportFreightTotalApply(transport.get("transport_id",0L),associateTypeId);
				%>
	     			<input name="button" type="button" value="运费修改" onClick="showFreightCost()" class="long-button"/>
		     	<%
		     		if(transport.get("stock_in_set",1)==2) 
		     		{
		     	%>
			        	<input name="button" type="button" value="申请运费" onClick="applyFreigth('<%=sum_price %>','<%=applyTransferFreightTotal %>','<%=transport.get("purchase_id",0) %>')" class="long-button"/>
		     	<%
		     		}
		     	%>
	     		</td>
	     	</tr>
	 </table>
    </div>
<%} %>	
<%if(transport.get("declaration",declarationKey.NODELARATION) != declarationKey.NODELARATION) {%>
	<div id="transport_out_info">
		<p style="margin-bottom:5px;">
			 出口报关阶段:
			 <span style="color:green;font-weight:bold;"><%=declarationKey.getStatusById(transport.get("declaration",declarationKey.NODELARATION)) %></span>
			 <input class="short-button" type="button" onclick='declarationButton(<%=transport_id%>)' value="报关跟进"/>
		</p>
		<table  width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" >
				<tr>
		       <th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
		       <th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
		     </tr>
		     <%
		    	int[] transport_type = {7};
				DBRow [] transportRows = transportMgrLL.getTransportLogsByTransportIdAndType(transport_id,transport_type);
		     	if(null != transportRows && transportRows.length > 0){
		     		for(int i = transportRows.length - 1; i >=0 ; i--){
		     			DBRow transRow = transportRows[i];
		     			 
		     %>			
		     			 <tr height="30px">
					     	<td><%=null == adminMgrLL.getAdminById(transRow.getString("transporter_id"))?"":adminMgrLL.getAdminById(transRow.getString("transporter_id")).getString("employe_name") %></td>
					     	<td><%=transRow.getString("transport_content") %></td>
					     	<td><%=transRow.getString("time_complete") %></td>
					     	<td><%= tDate.getFormateTime(transRow.getString("transport_date")) %></td>
		  					 <td><%=followuptype.get(transRow.get("transport_type",0))%></td>
					     </tr>
		     <%		
		     		}
		     	}else{
		     %>		
		     	<tr>
		     		<td colspan="5" align="center" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;"> 无数据 </td>
		     	</tr>
		     <%		
		     	}
			%>
			</table>
	</div>
<%} %>	
<%if(transport.get("clearance",clearanceKey.NOCLEARANCE) != clearanceKey.NOCLEARANCE) {%>
	<div id="transport_in_info">
	<p style="margin-bottom:5px;">
				 进口清关阶段:
				 <span style="color:green;font-weight:bold;"><%=clearanceKey.getStatusById(transport.get("clearance",clearanceKey.NOCLEARANCE)) %></span>
				<input class="short-button" type="button" onclick='clearanceButton(<%=transport_id%>)' value="清关跟进"/>
			</p>
		<table id="tableLog" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable"  >
			<tr>
		       <th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
		       <th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
		     </tr>
		     <%
		    	int[] transport_type_clearance = {6};
				DBRow [] transportRowsClearance = transportMgrLL.getTransportLogsByTransportIdAndType(transport_id,transport_type_clearance);
		     	if(null != transportRowsClearance && transportRowsClearance.length > 0){
		     		for(int i =( transportRowsClearance.length-1); i >=0 ; i-- ){
		     			DBRow transRow = transportRowsClearance[i];
		     %>			
		     			 <tr height="30px"  >
					     	<td><%=null == adminMgrLL.getAdminById(transRow.getString("transporter_id"))?"":adminMgrLL.getAdminById(transRow.getString("transporter_id")).getString("employe_name") %></td>
					     	<td><%=transRow.getString("transport_content") %></td>
					     	<td><%=transRow.getString("time_complete") %></td>
					     	<td><%=transRow.getString("transport_date") %></td>
		  					 <td><%=followuptype.get(transRow.get("transport_type",0))%></td>
				</tr>
		     <%		
		     		}
		     	}else{
		     %>		
		     	<tr>
		     		<td colspan="5" align="center" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
		     	</tr>
		     <%		
		     	}
			%>
			</table>
	</div>
<%} %>	
 
	<div id="transport_tag_make">
	<table style="margin-top: 6px;">
		<tr>
			<td align="right">
			内部标签阶段:<span style="color:green;font-weight:bold;"><%= transportTagKey.getTransportTagById(transport.get("tag",transportTagKey.NOTAG)) %></span>
			&nbsp;</td>
			<td align="left">
			<input type="button" class="long-button" value="跟进内部标签" onclick="tag('<%=transport_id %>')"/>
			<input type="button" class="long-button" value="内部标签日志" onclick="showSingleLogs('<%=transport_id %>',8,'内部标签')"/> 
			<input type="button" class="long-button-print" value="制作唛头" onclick="shippingMark()"/>
			<input type="button" class="long-button-print" value="制作标签" onclick="printLabel()"/>
			</td>
		</tr>
	</table>
	</div>
<%
if(tagIntThird != TransportTagKey.NOTAG)
{
%>
 <div id = "transport_tag_third">
 <table style="margin-top: 6px;">
		<tr>
			<td align="right">
			第三方标签阶段:<span style="color:green;font-weight:bold;"><%= transportTagKey.getTransportTagById(transport.get("tag_third",transportTagKey.NOTAG)) %></span>
			&nbsp;</td>
			<td align="left">
			<input type="button" class="long-button" value="跟进第三方标签" onclick="tagThird('<%=transport_id %>')"/>
			<input type="button" class="long-button" value="第三方标签日志" onclick="showSingleLogs('<%=transport_id %>',<%=TransportLogTypeKey.THIRD_TAG %>,'第三方标签')"/>
			</td>
		</tr>
	</table>
		<div id="tagTabs" style="margin-top:15px;">
   		<ul>
		<%
				 		if(selectedList != null && selectedList.size() > 0){
				 			for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
		%>	
				 				<li><a href="#transport_product_tag_<%=index %>"> <%=selectedList.get(index) %></a></li>
				 			<% 	
				 			}
				 		}
			 		%>
	   		 	</ul>
	   		 	<%
	   		 		for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
	   		 				List<DBRow> tempListRows = imageMap.get((index+1)+"");
	   		 			%>
	   		 			 <div id="transport_product_tag_<%= index%>">
	   		 			 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
	   		 			 			<tr> 
				  						<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
				  						<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
				  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
				  					</tr>
	   		 			 	<%
	   		 			 		if(tempListRows != null && tempListRows.size() > 0){
	   		 			 			 for(DBRow row : tempListRows){
	   		 			 			%>
	   		 			 				<tr style="height: 25px;">
	   		 			 					<td>
	   		 			 					 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	 	                 <%if(StringUtil.isPictureFile(row.getString("file_name"))){ %>
				 			 	             <p>
				 			 	 	            <a href="javascript:void(0)" onclick="showPictrueOnline1('<%=FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE %>','<%=transport.get("transport_id",0l) %>','<%=row.getString("file_name") %>','<%=row.get("product_file_type",01) %>');"><%=row.getString("file_name") %></a>
				 			 	             </p>
			 			 	                 <%}else{ %>
 		 			 	 	  		           <p><a href='<%= downLoadFileAction%>?file_name=<%=row.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_product")%>'><%=row.getString("file_name") %></a></p> 
 		 			 	 	                <%} %>
	   		 			 					
	   		 			 					</td>
	   		 			 					<td><%=null != productMgr.getDetailProductByPcid(row.get("pc_id",0))?productMgr.getDetailProductByPcid(row.get("pc_id",0)).getString("p_name"):""  %></td>
	   		 			 					<td>
	   		 			 						<% if(transport.get("tag_third",0) != TransportTagKey.FINISH){ %>
	   		 			 							<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
	   		 			 				 		<%} %>	
	   		 			 				 	</td>
	   		 			 				</tr>	
	   		 			 			<% 
	   		 			 		 }
	   		 			 		}else{
	   		 			 			%>
	 		 			 			<tr>
	 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	 								</tr>
	   		 			 			<%
	   		 			 		}
	   		 			 	%>
	   		 			 	 	</table>
			</div>
		<%	
			} 
		%>
	</div>
 </div>
<%} %>
 <%if(transport.get("certificate",certificateKey.NOCERTIFICATE) != certificateKey.NOCERTIFICATE) {%>
	<div id="transport_order_prove">
	<p style="margin-bottom:5px;">
		单证流程阶段:<span style="color:green;font-weight:bold;"><%= certificateKey.getStatusById(transport.get("certificate",0)) %></span>
		 <input type="button" class="short-button" value="单证跟进" onclick="followUpCerficate();"/>
	 	 <input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',9,'单证')"/> 
	</p>
			<table width="90%" border="0" align="center"   cellspacing="0" style="margin-left:18px;margin-top:-8px;">
		  <tr>
		    <td>
				 <fieldset style="border:2px #cccccc solid;padding:2px;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<legend style="font-size:15px;font-weight:normal;color:#999999;">上传文件</legend>
					<form name="uploadCertificateImageForm" id="uploadCertificateImageForm" value="uploadCertificateImageForm" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/transport/TransportCertificateUpFileAction.action" %>'   method="post">	
						<input type="hidden" name="backurl"  value=""/>
						<input type="hidden" name="file_with_id" value="<%=transport_id %>" />
						<input type="hidden" name="sn" id="sn" value="T_certificate"/>
						<input type="hidden" name="path" id="path" value="<%=systemConfig.getStringConfigValue("file_path_transport") %>"/>
						<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.transport_certificate %>"/>
						<input type="hidden" name="file_names" />
						<table width="90%" border="0">
						 	<td height="25" align="right"  style="width:120px;"  class="STYLE2" nowrap="nowrap">上传文件:</td>
						       <td>	 
						     	  &nbsp;<input type="button"  class="long-button" onclick="uploadFile('uploadCertificateImageForm');" value="选择文件" />
						     	  <input type="button" class="long-button" onclick="onlineScanner('uploadCertificateImageForm');" value="在线获取" />
						 	   </td>
							 </tr>
							 <tr>
							 	<td height="25" align="right"  class="STYLE2" nowrap="nowrap">文件分类:</td>
							 	<td>
						        	&nbsp;<select name="file_with_class" id="file_with_class_certificate" onchange="fileWithClassCertifycateChange();">
		 								<%if(arraySelectedCertificate != null && arraySelectedCertificate.length > 0){
		 									for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
		 									String tempStr = arraySelectedCertificate[index];
		 									String tempValue = "" ;
		 									String tempHtml = "" ;
		 									if(tempStr.indexOf("=") != -1){
		 										String[] tempArray = tempStr.split("=");
		 										tempValue = tempArray[0];
		 										tempHtml = tempArray[1];
		 									}
		 								%>		
		 									<option value="<%=tempValue %>"><%=tempHtml %></option>
		 								<%	} 
		 								}
		 								%>
						        	</select>
						        	
							 	</td>
							 </tr>
						</table>
					</form>
				</fieldset>	
			</td>
		  </tr>
		</table>
			<div id="tabsCertificate" style="margin-top:10px;">
			 <ul>	
			 	<%
			 		if(selectedListCertificate != null && selectedListCertificate.size() > 0){
			 			for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ ){
			 			%>
			 				<li><a href="#transport_certificate_<%=index %>"> <%=selectedListCertificate.get(index) %></a></li>
			 			<% 	
			 			}
			 		}
			 	%>
			 </ul>
			 	<!-- 遍历出Div 然后显示出来里面的 数据-->
			 	<%
					for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ ){
						%>
							<div id="transport_certificate_<%=index %>">
			 					<%
			 						List<DBRow> arrayLisTemp = mapCertificate.get(""+(index+1));
			 						%>
			 					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
			 						<tr> 
				  						<th width="75%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
				  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
				  					</tr>
			 						<% 
			 						if(arrayLisTemp != null && arrayLisTemp.size() > 0 ){
			 							for(int listIndex = 0 , total = arrayLisTemp.size() ; listIndex < total ; listIndex++ ){
			 							 %>
			 								<tr>
			 									<td>
			 									 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
			 			 	                     <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
			 			 	                     <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
			 			 	 	                 <%if(StringUtil.isPictureFile(arrayLisTemp.get(listIndex).getString("file_name"))){ %>
					 			 	             <p>
					 			 	 	            <a href="javascript:void(0)" onclick="showPictrueOnlinecer('<%=FileWithTypeKey.transport_certificate %>','<%=transport.get("transport_id",0l) %>','<%=arrayLisTemp.get(listIndex).getString("file_name") %>');"><%=arrayLisTemp.get(listIndex).getString("file_name") %></a>
					 			 	             </p>
				 			 	                 <%} else if(StringUtil.isOfficeFile(arrayLisTemp.get(listIndex).getString("file_name"))){%>
				 			 	 		          <p>
				 			 	 			          <a href="javascript:void(0)"  file_id='<%=arrayLisTemp.get(listIndex).get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("file_path_transport")%>')" file_is_convert='<%=arrayLisTemp.get(listIndex).get("file_is_convert",0) %>'><%=arrayLisTemp.get(listIndex).getString("file_name") %></a>
				 			 	 		          </p>
				 			 	                <%}else{ %>
	 		 			 	 	  		           <p><a href='<%= downLoadFileAction%>?file_name=<%=arrayLisTemp.get(listIndex).getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_transport")%>'><%=arrayLisTemp.get(listIndex).getString("file_name") %></a></p> 
	 		 			 	 	                <%} %>
			 									</td><td>
			 										<!--  如果是整个的单据流程已经结束那么就是不应该有删除的文件的按钮的 -->
			 										<%
			 											if(!(transport.get("certificate",0) == TransportCertificateKey.FINISH )){
			 												%>
			 												 <a href="javascript:deleteFileCommon('<%=arrayLisTemp.get(listIndex).get("file_id",0l)%>','file','<%=systemConfig.getStringConfigValue("file_path_transport") %>','file_id')">删除</a>
			 												<%  
			 											}
			 										%>	
	 		 									</td>
			 								</tr>
			 							 <%
			 							}	
			 						}else{
			 					 	%>
			 								<tr>
			 									<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
			 								</tr>
			 							<%
			 						}
			 					%>
			 					</table>	
							 </div>
						<%
					}
			 	%>
		</div>
	</div>
<%} %>	
	<div id="product_file">
		<p style="margin-bottom:5px;">
		 	  图片流程阶段:<span style="color:green;font-weight:bold;"><%= transportProductFileKey.getStatusById(transport.get("product_file",0))%></span>
   		  	 <input type="button" value="实物图片跟进" class="long-button" onclick="productFileFollowUp();"/>
   		 	 <input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',10,'实物图片')"/>
   		 	 <%
				if(TransportProductFileKey.FINISH == transport.get("product_file",0))
				{
			%>
				<input name="button" type="button" class="long-long-button" value="实物图片完成-->需要" onClick="transportProductFileFinishToNeed()"/>
			<%		
				}
			%>
		</p>
		<div style="padding-top:5px;padding-bottom:5px;padding-left:10px;">
   			</div>		  
 			<div id="tabs">
  	            <ul>
					   <%if(arrayProductFileSelected != null && arrayProductFileSelected.length > 0){
 									for(int index = 0 , count = arrayProductFileSelected.length ; index < count ; index++ ){
 									String tempStr = arrayProductFileSelected[index];
 									String tempValue = "" ;
 									String tempHtml = "" ;
 									if(tempStr.indexOf("=") != -1){
 										String[] tempArray = tempStr.split("=");
 										tempValue = tempArray[0];
 										tempHtml = tempArray[1];
 									}
 								%>		
 				  <li><a href="transport_product_file_tright.html?transport_id=<%=transport_id %>&product_file_type=<%=tempValue %>&file_type_name=<%=tempHtml %>"><%=tempHtml %></a></li>
 								<%	} 
 								}
 						%>
			    </ul>
			  </div>
	</div>
 
 <%if(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY) != TransportQualityInspectionKey.NO_NEED_QUALITY) {%>
	<div id="quality_inspection">
		<p style="margin-bottom:5px;">
			质检流程阶段:<span style="font-weight:bold;color:green;"><%=transportQualityInspectionKey.getStatusById(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY)+"") %></span>
			 <input class="short-button" type="button" onclick='quality_inspectionKey(<%=transport_id%>)' value="质检跟进"/>
			 <input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',11,'质检')"/>
		</p>
		<form  id="qualityInspectionForm"   method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/transport/TransportQualityInspectionAction.action" %>'>
		<!-- 读取质检报告的状态 -->
			<table>
				<tr>
					<td style="width:85px;"><%=transportQualityInspectionKey.getStatusById(transport.get("quality_inspection",transportQualityInspectionKey.NO_NEED_QUALITY)+"") %>：</td>					
					<td style="width:700px;">
					 	<input type="hidden" name="backurl" value="<%=backurl %>" />
						<input type="hidden" name="sn" value="T_quality_inspection"/>
						<input type="hidden" name="quality_inspection" value='<%= transport.get("quality_inspection",transportQualityInspectionKey.NO_NEED_QUALITY) %>' />
		 				<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.TRANSPORT_QUALITYINSPECTION %>" />
		 				<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_transport")%>" />
					 	<input type="hidden" name="file_names" />
					 	<input type="hidden" name="cmd" value="submit"/>	<!-- 表示页面提交 -->
						<input type="hidden" name="transport_id" value="<%=transport_id %>"/>
		 			 	<input type="button"  class="long-button" onclick="uploadFile('qualityInspectionForm');" value="选择文件" />
		 			 	 <input type="button" class="long-button" onclick="onlineScanner('qualityInspectionForm');" value="在线获取" />
		 			</td>
		     </tr>
			</table>
						<table>
		     <%
							 DBRow[] fileRows = transportMgrZr.getFileByFilwWidthIdAndFileType("file",transport_id,FileWithTypeKey.TRANSPORT_QUALITYINSPECTION);
							 if(fileRows != null && fileRows.length > 0 ){
								 for(DBRow tempRow  : fileRows){
		     %>			
									<tr style="height: 25px;">
										<td width="100px" align="right">
											<%=null == adminMgrLL.getAdminById(tempRow.getString("upload_adid"))?"":adminMgrLL.getAdminById(tempRow.getString("upload_adid")).getString("employe_name") %>
										</td>
										<td width="100px;" align="center">
										<% if(!"".equals(tempRow.getString("upload_time"))){
												out.print(tDate.getFormateTime(tempRow.getString("upload_time")));
		     		}
		     %>		
										</td>
										<td width="60%" align="left">
										   <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	                     <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
		 			 	                     <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
		 			 	 	                 <%if(StringUtil.isPictureFile(tempRow.getString("file_name"))){ %>
				 			 	             <p>
				 			 	 	            <a href="javascript:void(0)" onclick="showPictrueOnlinef('<%=FileWithTypeKey.TRANSPORT_QUALITYINSPECTION %>','<%=transport.get("transport_id",0l) %>','<%=tempRow.getString("file_name") %>','<%=tempRow.get("product_file_type",01) %>');"><%=tempRow.getString("file_name") %></a>
				 			 	             </p>
			 			 	                 <%} else if(StringUtil.isOfficeFile(tempRow.getString("file_name"))){%>
			 			 	 		          <p>
			 			 	 			          <a href="javascript:void(0)"  file_id='<%=tempRow.get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("file_path_transport")%>')" file_is_convert='<%=tempRow.get("file_is_convert",0) %>'><%=tempRow.getString("file_name") %></a>
			 			 	 		          </p>
			 			 	                <%}else{ %>
 		 			 	 	  		           <p><a href='<%= downLoadFileAction%>?file_name=<%=tempRow.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_transport")%>'><%=tempRow.getString("file_name") %></a></p> 
 		 			 	 	                <%} %>
										</td> 
										<!--  判断是不是可以删除 -->
										<% if(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY)!= TransportQualityInspectionKey.FINISH) {%> 
		     								 <td>
		     								 	<a  href="javascript:deleteFileTable('<%= tempRow.get("file_id",0l)%>')">删除</a>
		     								</td>
		     							<%} %>
		     	</tr>
		     <%		
								 }
									 
								 
								 
		     	}
			%>
						 </table>
				
			
			
		
		</form>
	</div>
<%} %>
</div>
<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
 	<tr>
 		<td align="left">
 			<input type='button' class='long-button' value='实物图片' onclick='addProductsPicture()'/> 
			<input type='button' class='long-long-button' value='第三方标签打印文件' onclick='addProductTagTypesFile()'/>"		
 		</td>
 		<td align="right">
 			<input type="button" value="发货差异" onclick="showDifferent(0)" class="short-button"/>&nbsp;&nbsp;
 			<input type="button" value="收货差异" onclick="showDifferent(1)" class="short-button"/>&nbsp;&nbsp;
 			<input type='button' class='long-button' value="更新重量与体积" onclick='updateTransportVW(<%=transport_id%>)'/>
 		</td>
 	</tr>
 </table>

<script>
	$("#transportTabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	 
	});
	$("#tagTabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	$("#tabsCertificate").tabs({
 		cache: true,
 		 
		select: function(event, ui){
			 $("#file_with_class_certificate option[value='"+(ui.index+1)+"']").attr("selected",true);
		} 
 	 });
	 // 首先获取 大的tabs,然后获取小的tabs。
	var bigIndex = ($("#transportTabs").tabs("option","selected")) * 1;
	if(bigIndex == 5){
		var selectedIndex = '<%= file_with_class%>' * 1 ;
		$("#file_with_class_certificate option[value='"+selectedIndex+"']").attr("selected",true);
	}

	function format(id)
	{
		var product_id = jQuery("#gridtest").jqGrid('getCell',id,'transport_pc_id');
		var clp_type_id = jQuery("#gridtest").jqGrid('getCell',id,'clp_type_id');
		var serial_number = jQuery("#gridtest").jqGrid('getCell',id,'transport_product_serial_number');

		if(serial_number.length>0)
		{
			$("#gridtest").jqGrid('setColProp','blp_type',{editable:false});
			$("#gridtest").jqGrid('setColProp','clp_type',{editable:false});
		}
		else
		{
			$("#gridtest").jqGrid('setColProp','clp_type',{editType:"select",editoptions:{value:getCLPSelect(product_id)}});
			if (clp_type_id!=0)
			{
				$("#gridtest").jqGrid('setColProp','blp_type',{editable:false});
			}
			else
			{
				$("#gridtest").jqGrid('setColProp','blp_type',{editable:true,editType:"select",editoptions:{value:getBLPSelect(product_id,<%=transport.get("receive_psid",0l)%>)}});
			}
		}
	}
</script>
	<div id="detail" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table id="gridtest"></table>
	<div id="pager2"></div> 
	
	<script type="text/javascript">
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					sortable:true,
					url:'dataTransportOrderDetail.html',//dataDeliveryOrderDetails.html
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.98),
					height:255,
					autowidth: false,
					shrinkToFit:true,
					postData:{transport_id:<%=transport_id%>},
					jsonReader:{
				   			id:'transport_detail_id',
	                        repeatitems : false
	                	},
	                	colNames:['transport_detail_id','商品名','商品条码','货物数','备件数','计划装箱数','V(cm³),W(Kg),<%=transport.get("transport_status",0)==TransportOrderKey.FINISH?"运费":"估算运费"%>','所在箱号','商品序列号','使用CLP','使用BLP','transport_id','transport_pc_id','clp_type_id','blp_type_id','批次','已上传图片'], 
				   	colModel:[ 
				   		{name:'transport_detail_id',index:'transport_detail_id',hidden:true,sortable:false},
				   		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
				   		{name:'p_code',index:'p_code',align:'left'},
				   		{name:'transport_delivery_count',width:50,index:'transport_delivery_count',editrules:{required:true,number:true,minValue:0,integer:true},editable:<%=edit%>,sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},
				   		{name:'transport_backup_count',width:50,index:'transport_backup_count',editrules:{required:true,number:true,minValue:0,integer:true},editable:<%=edit%>,sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},
				   		{name:'transport_count',width:50,index:'transport_count',editrules:{required:true,number:true,minValue:1},sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},				   	
						{name:'volume_weight_freight',width:80,index:'volume_weight_freight',align:'left'},
				   		{name:'transport_box',width:50,index:'transport_box',editable:<%=edit%>},
				   		{name:'transport_product_serial_number',width:50,index:'transport_product_serial_number',editable:<%=edit%>},
				   		{name:'clp_type',width:30,index:'clp_type',editable:<%=edit%>,edittype:'select'},
				   		{name:'blp_type',width:30,index:'blp_type',edittype:'select'},
				   		{name:'transport_id',index:'transport_id',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=transport_id%>},hidden:true,sortable:false},
				   		{name:'transport_pc_id',index:'transport_pc_id',hidden:true,sortable:false},
				   		{name:'clp_type_id',index:'clp_type_id',hidden:true,sortable:false},
				   		{name:'blp_type_id',index:'blp_type_id',hidden:true,sortable:false},
				   		{name:'lot_number',width:60,index:'lot_number',editable:<%=edit%>,editrules:{required:true},hidden:false,sortable:false},
				   		{name:'button',index:'button',align:'left',sortable:false},
				   		], 
				   	rowNum:-1,//-1显示全部
				   	pgtext:false,
				   	pgbuttons:false,
				   	mtype: "GET",
				   	gridview: true, 
					rownumbers:true,
				   	pager:'#pager2',
				   	sortname: 'transport_detail_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
				   	multiselect: true,
				   	onSelectCell:function(id,name)
					{
	                		if(name=="button")
   					 		{
	                			var product_id = jQuery("#gridtest").jqGrid('getCell',id,'transport_pc_id');
   					 			addProductPicture(product_id);
   					 		}

	                		format(id);
					},
					formatCell:function(id,name,val,iRow,iCol)
					{
						format(id);
					},
				   	afterEditCell:function(id,name,val,iRow,iCol)
				   	{ 
				   		if(name=='p_name') 
				   		{
				   			autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
				   		}
				   		select_iRow = iRow;
				   		select_iCol = iCol;
				    }, 
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				 	{ 
				   		if(name=='p_name'||name=='transport_delivery_count'||name=='transport_backup_count') 
				   		{
				   			ajaxModProductName(jQuery("#gridtest"),rowid);
				   			getTransportSumVWP(<%=transport_id%>);
				   	  	}
				   	  	if(name=='clp_type'||name=='blp_type')
				   	  	{
				   	  		ajaxModProductName(jQuery("#gridtest"),rowid);
				   	  		format(rowid);
						}

				   	}, 
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/gridEditTransportDetail.action',
				   	errorCell:function(serverresponse, status)
				   	{
				   		alert(errorMessage(serverresponse.responseText));
				   	},
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/gridEditTransportDetail.action'
				}); 		   	
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
			   
			   
			   function intransitDelivery(transport_id,number)
				{
			
					var para = "transport_id="+transport_id;
					$.ajax({
							url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/intransitTransport.action',
							type: 'post',
							dataType: 'json',
							timeout: 60000,
							cache:false,
							data:para,
										
							beforeSend:function(request)
							{
							},
							error: function(e)
							{
								alert(e);
								alert("提交失败，请重试！");
							},
							success: function(data)
							{
								if(data.rel)
								{
									window.location.reload();
								}
								
							}
						});						
				}
				
				function getTransportSumVWP(transport_id)
				{
					$.ajax({
								url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportSumVWPJson.action',
								type: 'post',
								dataType: 'json',
								timeout: 60000,
								cache:false,
								data:{transport_id:transport_id},
								
								beforeSend:function(request){
								},
								
								error: function(e){
									alert(e);
									alert("提交失败，请重试！");
								},
								
								success: function(date)
								{
									$("#sum_volume").html("总体积:"+date.volume+" cm³");
									$("#sum_weight").html("总重量:"+date.weight+" Kg");
									$("#sum_price").html("总货款:"+date.send_price+" RMB");
								}
							});
				}
	</script>
		</td>
	</tr>
</table>
<table width="100%">
	<tr>
		<td colspan="2">
			<span id="sum_volume"></span>
			<span id="sum_price"></span>
			<span id="sum_weight"></span>
		</td>
	</tr>
</table>
	</div>
  </div>
</div>

<form name="download_form" method="post"></form>

<form action="" method="post" name="applicationApprove_form">
	<input type="hidden" name="transport_id"/>
</form>
<form action="" method="post" name="followup_form">
	<input type="hidden" name="transport_id"/>
	<input type="hidden" name="type" value="1"/>
	<input type="hidden" name="stage" value="1"/>
	<input type="hidden" name="transport_type" value="1"/>
	<input type="hidden" name="transport_content"/>
	<input type="hidden" name="expect_date"/>
</form>
<script type="text/javascript">
	//$("#eta").date_input();
	
	function modTransport()
	{
		tb_show('修改转运单','mod_transport.html?transport_id=<%=transport_id%>&TB_iframe=true&height=500&width=800',false);
	}
	
	
	function readyPacking(transport_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_storage_show.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '转运缺货商品',width:'700px',height:'400px', lock: true,opacity: 0.3});
		//tb_show('转运缺货商品','transport_storage_show.html?transport_id='+transport_id+'&TB_iframe=true&height=400&width=700',false);
	}
	
	function receiveAllocate(transport_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/receive_allocate.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '预保留库存',width:'870px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function applicationApprove(transport_id)
	{
		if(confirm("确定申请T"+transport_id+"转运单完成?"))
		{
			document.applicationApprove_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/addTransportApprove.action";
			document.applicationApprove_form.transport_id.value = transport_id;
			document.applicationApprove_form.submit();
		}
	}
	function addProductsPicture(){
		var transport_id = '<%= transport_id%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_picture_up.html?transport_id="+transport_id;
	 	var addUri =  getSelectedIdAndNames();
	 	if($.trim(addUri).length < 1 ){
			showMessage("请先选择商品!","alert");
			return ;
		}else{uri += addUri;}
		$.artDialog.open(uri , {title: "实物图片上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	function addProductPicture(pc_id){
		 //添加商品图片
		var transport_id = '<%= transport_id%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_picture_up.html?transport_id="+transport_id+"&pc_id="+pc_id;
		$.artDialog.open(uri , {title: "商品范例上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,close:function(){window.location.reload();},fixed: true});
	}
	function addProductTagTypesFile(){
		 //添加商品标签
		var transport_id = '<%= transport_id%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_tag_file.html?transport_id="+transport_id ;
		var addUri =  getSelectedIdAndNames();
	 	if($.trim(addUri).length < 1 ){
			showMessage("请先选择商品!","alert");
			return ;
		}else{uri += addUri;}
		 $.artDialog.open(uri , {title: "第三方标签上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	function getSelectedIdAndNames(){
		s = jQuery("#gridtest").jqGrid('getGridParam','selarrrow')+"";  
	  	 
	 	var array = s.split(",");
 		var strIds = "";
 		var strNames = "";
	 	for(var index = 0 , count = array.length ; index < count ; index++ ){
		   var number = array[index];
	 	   var thisid= $("#gridtest").getCell(number,"transport_pc_id");
		    var thisName =$("#gridtest").getCell(number,"p_name"); 
	 	  	strIds += (","+thisid);
	 	  	strNames += (","+thisName);
		}
		if(strIds.length > 1 ){
		    strIds = strIds.substr(1);
		    strNames = strNames.substr(1);
		}
		if(strIds+"" === "false"){return "";}
		return  "&pc_id="+strIds;
	}
	//点击弹出制作麦头的界面
	function shippingMark()
	{
		var purchaseId = <%=transport.get("purchase_id",01)%>;
		//如果不是0就是交货型转运单
        if(purchaseId==0){
		    var transport_id=<%=transport_id%>;
            var awb="<%=transport.getString("transport_waybill_number")%>";
            var awbName="<%=transport.getString("transport_waybill_name")%>";
		    var menpai="<%=transport.getString("deliver_house_number")%>";
		    var jiedao="<%=transport.getString("deliver_street") %>";
		    var chengshi="<%=transport.getString("deliver_city") %>";
		    var youbian="<%=transport.getString("deliver_zip_code") %>";
		    var shengfen="<%=ps_id4%>";
		    var guojia="<%= ccid4%>";
		    var lianxiren="<%=transport.getString("transport_linkman")%>";
		    var dianhua="<%=transport.getString("transport_linkman_phone")%>";
		    var receive_psid = "<%=transport.get("receive_psid",0l)%>";
		    var uri = "../lable_template/print_shpping_marjsp.html?awbName="+awbName+"&dianhua="+dianhua+"&lianxiren="+lianxiren+"&guojia="+guojia+"&shengfen="+shengfen+"&youbian="+youbian+"&chengshi="+chengshi+"&jiedao="+jiedao+"&menpai="+menpai+"&transport_id="+transport_id+"&awb="+awb+"&receive_psid="+receive_psid; 
			$.artDialog.open(uri , {title: '唛头标签打印',width:'600px',height:'420px', lock: true,opacity: 0.3,fixed: true});
		}else{
			var transport_id=<%=transport_id%>;
            var awb="<%=transport.getString("transport_waybill_number")%>";
            var awbName="<%=transport.getString("transport_waybill_name")%>";
		    var menpai="<%=transport.getString("deliver_house_number")%>";
		    var jiedao="<%=transport.getString("deliver_street") %>";
		    var chengshi="<%=transport.getString("deliver_city") %>";
		    var youbian="<%=transport.getString("deliver_zip_code") %>";
		    var shengfen="<%=ps_id4%>";
		    var guojia="<%= ccid4%>";
		    var lianxiren="<%=transport.getString("transport_linkman")%>";
		    var dianhua="<%=transport.getString("transport_linkman_phone")%>";
		    var receive_psid = "<%=transport.get("receive_psid",0l)%>";
		    var uri = "../lable_template/print_shpping_marjsp.html?purchase_id="+purchaseId+"&awbName="+awbName+"&dianhua="+dianhua+"&lianxiren="+lianxiren+"&guojia="+guojia+"&shengfen="+shengfen+"&youbian="+youbian+"&chengshi="+chengshi+"&jiedao="+jiedao+"&menpai="+menpai+"&transport_id="+transport_id+"&awb="+awb+"&receive_psid="+receive_psid;
			$.artDialog.open(uri , {title: '唛头标签打印',width:'600px',height:'420px', lock: true,opacity: 0.3,fixed: true});
		}
	} 
	//点击制作标签后修改成弹出dialog
	function printLabel()
	{
		//如果不是0就是交货型转运单
		var purchaseId = <%=transport.get("purchase_id",0l)%>;
		<%
			if(transport.get("purchase_id",0l)==0)
			{
		%>
               
             var supplierSid =<%=transport.get("send_psid",0l) %>;
             var transport_id=<%=transport_id%>;
             var warehouse="<%=null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):""%>";
         	 var uri = "../lable_template/made_tranoport_internal_label.html?warehouse="+warehouse+"&transport_id="+transport_id+"&purchase_id="+purchaseId+"&supplierName="+""+"&supplierSid="+supplierSid; 
 			 $.artDialog.open(uri , {title: '转运单内部标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});		
        <%	}
        	else
        	{
        %>
            var supplierName="<%=supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name")%>";
            var supplierSid =<%=transport.get("send_psid",0l) %>;
            var transport_id=<%=transport_id%>;
        	var uri = "../lable_template/made_tranoport_label.html?transport_id="+transport_id+"&purchase_id="+purchaseId+"&supplierName="+supplierName+"&supplierSid="+supplierSid; 
			$.artDialog.open(uri , {title: '转运单交货型标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
        <%
        	}	
		%>
	}
	
	function showDifferent(selectIndex)
	{
		$.artDialog.open('transport_different_show.html?transport_id=<%=transport_id%>&select_index='+selectIndex, {title: "转运单差异",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	
</script>
<script>
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	cookie: {expires: 30000},
	load: function(event, ui) {onLoadInitZebraTable();}	
});

getTransportSumVWP(<%=transport_id%>);
</script>	
<script type="text/javascript">
//stateBox 信息提示框
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
	 var  _self = $("body"),
	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
 }
}
</script>
</body>
</html>

