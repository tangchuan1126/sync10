<%@ page import="com.cwc.app.util.ConfigBean" language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>  

<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="transportOrderKey" class="com.cwc.app.key.TransportOrderKey"/>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/> 
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/>
<jsp:useBean id="transportQualityInspectionKey" class="com.cwc.app.key.TransportQualityInspectionKey"/> 
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey"></jsp:useBean>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<%
	TransportCertificateKey certificateKey = new TransportCertificateKey();
   	long transport_id = StringUtil.getLong(request,"transport_id");
 	boolean edit = false;
 	
 	TDate tDate = new TDate();
	//fileFlag
	String fileFlag= StringUtil.getString(request,"flag");
	
	//转运单
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
	
	//转运单title
	String title_name = "";
	DBRow titleRow = proprietaryMgrZyj.findProprietaryByTitleId(transport.get("title_id", 0L));
	if(null != titleRow)
	{  							
		title_name = titleRow.getString("title_name");
	}
	
	
	//提货仓库
	String send_psName = "";
	if(transport.get("purchase_id",0l)==0)
	{
		send_psName = null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"";
		session.setAttribute("billOfLading",null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
	}
	else
	{
			send_psName = supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name");
	}
	
	//收货仓库
	String recivie_psName = null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):"";
 	
 	
 	HashMap followuptype = new HashMap();
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改转运单详细记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
 %>
 
 <%
 	//业务逻辑判断：判断是否有报关，假设有的话 状态颜色显示效果
 	
 	//计算颜色
 	int stockInSetInt = transport.get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET);
	String stockInSetClass = "" ;
	String stock_in_set_state ="未完成";
 	if(stockInSetInt != transportStockInSetKey.SHIPPINGFEE_NOTSET) 
	{
		if(stockInSetInt == TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH)
		{
  			stockInSetClass += "spanBold";
  			DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_STOCKINSET);
  			if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  			{
  				stockInSetClass += " fontGreen";
  				stock_in_set_state = "已完成";
  			}
  			else
  			{
  				stockInSetClass += " fontRed";
  				stock_in_set_state = "未上传凭证";
  			}
		}
		else 
		{
			stockInSetClass += "spanBold spanBlue";
  		}
	} 	
 	
 	//计算报关颜色和状态
 	int declarationInt = transport.get("declaration",declarationKey.NODELARATION);
    String declarationKeyClass = "" ;
	String declarationKeyClass_state = "未完成" ;
 	if(declarationInt != declarationKey.NODELARATION ) 
	{
			if(declarationInt == declarationKey.FINISH)
			{
  				declarationKeyClass += "spanBold";
  				DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_declaration);
  				if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  				{
  					declarationKeyClass += " fontGreen";
  					declarationKeyClass_state = "已完成";
  				}else
  				{
  					declarationKeyClass += " fontRed";
  					declarationKeyClass_state = "未上传凭证";
  				}
			}
			else 
			{
				declarationKeyClass +=  "spanBold spanBlue";
  			}
   } 
   
   //清关显示颜色样式
   int clearanceInt = transport.get("clearance",clearanceKey.NOCLEARANCE);
   String clearanceKeyClass = "" ;
   String clearanceKeyClass_state ="未完成";
   if(clearanceInt != clearanceKey.NOCLEARANCE ) 
   {
	    if( clearanceInt == clearanceKey.FINISH)
		{
  			clearanceKeyClass += "spanBold";
  			DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_clearance);
  			if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  			{
  				clearanceKeyClass += " fontGreen";
  				clearanceKeyClass_state ="已完成";
  			}
  			else
  			{
  				clearanceKeyClass += " fontRed";
  				clearanceKeyClass_state ="未上传凭证";
  			}
		}
		else 
		{
			clearanceKeyClass +=  "spanBold spanBlue";
  		}
	} 
 
	 //计算单证的颜色和样式
 	int certificateInt = transport.get("certificate",TransportCertificateKey.NOCERTIFICATE);
	String certificateClass = "" ;
	String certificateClass_state ="未完成";
 	if(certificateInt != certificateKey.NOCERTIFICATE ) 
	{
  		if(certificateInt == TransportCertificateKey.FINISH)
  		{
  			certificateClass += "spanBold";
  			DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_certificate);
  			if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  			{
  				certificateClass += " fontGreen";
  				certificateClass_state = "已完成";
  			}
  			else
  			{
  				certificateClass += " fontRed";
  				certificateClass_state = "有未上传的凭证";
  			}
  		}
  		else 
  		{
  			    certificateClass += "spanBold spanBlue";
  		}
   }
   
   	//计算颜色  质检 
	String qualityInspectionClass = "" ;
	int qualityInspectionInt = transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY);
  if(qualityInspectionInt != TransportQualityInspectionKey.NO_NEED_QUALITY) 
  {
					
		
		if(qualityInspectionInt == TransportQualityInspectionKey.FINISH)
		{
  			qualityInspectionClass += "spanBold";
  			DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_QUALITYINSPECTION);
  			if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  			{
  				qualityInspectionClass += " fontGreen";
  			}else
  			{
  				qualityInspectionClass += " fontRed";
  			}
		}
		else if(qualityInspectionInt != TransportQualityInspectionKey.NO_NEED_QUALITY)
		{
				qualityInspectionClass += "spanBold spanBlue";
  		}
  	}
 
 
	//计算颜色 商品文件
	 String productFileClass = "" ;
  	int productFileInt = transport.get("product_file",TransportProductFileKey.NOPRODUCTFILE);
  	if(productFileInt == TransportProductFileKey.FINISH)
  	{
  		productFileClass += "spanBold";
  		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("product_file",transport.get("transport_id",0l),FileWithTypeKey.product_file);
  		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  		{
  			productFileClass += " fontGreen";
  		}
  		else
  		{
  			productFileClass += " fontRed";
  		}
  	}
  	else if(productFileInt != TransportProductFileKey.NOPRODUCTFILE)
  	{
  			productFileClass += "spanBold spanBlue";
  	} 
 
 //计算颜色 标签
	String tagClass = "" ;
  	int tagInt = transport.get("tag",TransportTagKey.NOTAG);
  	if(tagInt == TransportTagKey.FINISH)
  	{
  		tagClass += "spanBold";
  		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_TAG);
  			if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 )
  			{
  					tagClass += " fontGreen";
  			}
  			else
  			{
  				tagClass += " fontRed";
  			}
  	}
  	else if(tagInt != TransportTagKey.NOTAG)
  	{
  			tagClass += "spanBold spanBlue";
    }
 
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>转运单 T<%=transport_id %></title>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<style type="text/css">
	body{text-align:left;}
	ol.fundsOl{
		list-style-type: none;	
	}
	ol.fundsOl li{
		width: 350px;
		height: 200px;
		float: left;
		line-height: 25px;
	}
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jqgrid 引用 -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>
<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}
.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
.ui-datepicker {z-index:1200;}
.rotate
{
    /* for Safari */
    -webkit-transform: rotate(-90deg);

    /* for Firefox */
    -moz-transform: rotate(-90deg);

    /* for Internet Explorer */
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
}
.ui-jqgrid .ui-jqgrid-htable th
{
	font-size:11px;
}
	
.ui-jqgrid tr.jqgrow td 
{
	white-space: normal !important;
	height:auto;
	vertical-align:text-top;
	padding-top:2px;
}
.set
{
	border:2px #999999 solid;
	padding:2px;
	width:90%;
	word-break:break-all;
	margin-top:10px;
	margin-top:5px;
	line-height:18px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	margin-bottom: 10px;
}
.create_order_button{background-attachment: fixed;background: url(../imgs/create_order.jpg);background-repeat: no-repeat;background-position: center center;height: 51px;width: 129px;color: #000000;border: 0px;}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
.zebraTable td {line-height:25px;height:25px;}
.right-title{line-height:20px;height:20px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
span.moredown {
	margin: 5;
	background-attachment: fixed;
	background: url(../imgs/maps/hide_light_down.png);
	background-repeat: no-repeat;background-position: center center;height: 16px;width: 16px;color: #000000;border: 0px;
}
span.moreup {
	margin: 5;
	background-attachment: fixed;
	background: url(../imgs/maps/hide_light_up.png);
	background-repeat: no-repeat;background-position: center center;height: 16px;width: 16px;color: #000000;border: 0px;
}
div.tabdown {
	background-attachment: fixed;
	background: url(../imgs/tabdown.png);
	background-repeat: no-repeat;background-position: center center;height: 15px;width: 79px;color: #000000;border: 0px;
	position:relative;
}
div.tabup {
	background-attachment: fixed;
	background: url(../imgs/tabup.png);
	background-repeat: no-repeat;background-position: center center;height: 15px;width: 79px;color: #000000;border: 0px;
	position:relative;
}

.smallGallery .nav{
    width:105px;
}
.mediumGallery .nav{
    width:165px;
}
.largeGallery .nav{
    width:225px;
}
.smallGallery .images,
.mediumGallery .images,
.largeGallery .images{
    padding:10px;
    background-color:#f9f9f9;
    border:1px solid #fff;
    position:relative;
    -moz-box-shadow:1px 1px 5px #aaa;
    -webkit-box-shadow:1px 1px 5px #aaa;
    box-shadow:1px 1px 5px #aaa;
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border-radius:5px;
}
.nav {
    height: 20px;
    padding-left: 4px;
}
.smallGallery .images,
.smallGallery .singleImg div{
    width:102px;
    height:102px;
}
.mediumGallery .images,
.mediumGallery .singleImg div{
    width:162px;
    height:108px;
}
.largeGallery .images,
.largeGallery .singleImg div{
    width:222px;
    height:222px;
}
.microGallery{
    position:relative;
    margin:30px 10px 10px 10px;
    float:left;
}
.microGallery img{
    margin-left:auto;
    margin-right:auto;
    border:none;
    -moz-box-shadow:0px 2px 4px #777;
    -webkit-box-shadow:0px 2px 4px #777;
    box-shadow:0px 2px 4px #777;
    display:none;
}
a.thumbview{
    opacity:0.6;
    width:20px;
    height:21px;
    float:left;
    cursor:pointer;
}
.images div{
    display:table-cell;
    vertical-align:middle;
    text-align:center;
    position:relative;
}

.smallGallery .thumbs div,
.mediumGallery .thumbs div,
.largeGallery .thumbs div{
    float:left;
    margin:2px;
    cursor:pointer;
}

.smallGallery .thumbs div{
    width:30px;
    height:30px;
}
.mediumGallery .thumbs div{
    width:50px;
    height:50px;
}
.largeGallery .thumbs div{
    width:70px;
    height:70px;
}
</style>
<style type="text/css">
.tcol {
	line-height:18px;
	height:18px;
	border-color: #bbbbbb;
	border-style: solid;
	border-width: 0 0 1px 0;
	padding: 2px;
	color: #333333;
	font-size: 12px;
	text-align: left;
}
.tfoot {
	line-height:18px;
	height:18px;
	border-color: #bbbbbb;
	border-style: solid;
	border-width: 0 0 1px 0;
	padding: 0px;
	color: #333333;
	font-size: 12px;
	text-align: left;
}
.dTable tbody tr.over td {
	background: #E6F3C5;
}
.dTable tbody tr.open td {
	border-width: 0 0 0 0;
}
</style>
</head>
<body margin="1" onLoad="onLoadInitZebraTable()">
<div class="demo">
	<div style="min-height: 25px">
		<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
			<tr height="30px">
				<td>
					&nbsp;&nbsp;
					<%if(TransportOrderKey.INTRANSIT == transport.get("transport_status",0))
					{
					%>
					<input type="button"  onclick="goodsArriveDelivery(<%=transport_id%>)" value="到货派送" class="long-button" name="button">
					<%} %>
					<input type="button"  onclick="downloadTransportOrder(<%=transport_id%>)" value="下载转运单" class="long-button-next">
					<input type="button"  onclick="print()" value="打印运单" class="long-button-print">
				</td>
			</tr>
		</table>
	</div>
	<div id="transportMainTabs" style="min-height: 25px">
		<ul>
			<li ondblclick="opBasicTab()"><a href="#transport_basic_info" ><span>基础信息</span></a></li>
			<li ondblclick="opBasicTab()"><a href="#transport_info"><span>货单信息</span></a></li>
		</ul>
		<div id="transport_basic_info">
			<table id="basic_info" width="100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
				<tr id="basic_info_main" >
					<td align="center" width="33%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">转运单号：</td>
									<td style="width: 80%;" nowrap="nowrap">T<%=transport_id%></td>
								</tr>
						</table>
					</td>
					<td align="center" width="33%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">TITLE：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=title_name %></td>
								</tr>
						</table>
					</td>
					<td align="center" width="34%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">货物状态：</td>
									<td style="width: 80%;" nowrap="nowrap">
									<%=transportOrderKey.getTransportOrderStatusById(transport.get("transport_status",0)) %>
									</td>
								</tr>
						</table>
					</td>
				</tr>
				</thead>
				<tbody>
				<tr id="basic_info_detail" style="display:none;">
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>运单基础信息</legend>
							<table style="width:100%;border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">运单号：</td>
									<td style="width: 80%;" nowrap="nowrap">T<%=transport_id%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">TITLE：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=title_name %></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">创建人：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("create_account")%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">创建时间：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=tDate.getFormateTime(transport.getString("transport_date"))%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">ETA：</td>
									<td style="width: 80%;" nowrap="nowrap">
									<% 
										if(transport.getString("transport_receive_date").trim().length() > 0)
										{
											out.println(tDate.getEnglishFormateTime(transport.getString("transport_receive_date")));
										}
 									%>
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">允许装箱：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<% 
										DBRow packinger = adminMgr.getDetailAdmin(transport.get("packing_account",0l));
										if(packinger!=null)
										{
											out.print(packinger.getString("employe_name"));
										}
									%>
									</td>
								</tr>
							</table>
						</fieldset>
					</td>
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>运输基础信息</legend>
							<table style="width:100%;border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">货物状态：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<%=transportOrderKey.getTransportOrderStatusById(transport.get("transport_status",0)) %>
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">运费状态：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=stockInSetClass%>"><%=transportStockInSetKey.getStatusById(transport.get("stock_in_set",transportStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">出口报关：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=declarationKeyClass%>"><%=declarationKey.getStatusById(transport.get("declaration",01)) %></span>
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">进口清关：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=clearanceKeyClass%>"><%=clearanceKey.getStatusById(transport.get("clearance",01)) %></span>
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">单证流程：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=certificateClass%>"><%=certificateKey.getStatusById(transport.get("certificate",certificateKey.NOCERTIFICATE)) %></span>
									</td>
								</tr>
							</table>
						</fieldset>
					</td>
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>流程基础信息</legend>
							<table style="width:100%;border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">制签流程：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=tagClass %>" ><%= transportTagKey.getTransportTagById(transport.get("tag",transportTagKey.NOTAG)) %>	</span>
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">实物图片：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=productFileClass %>" ><%= transportProductFileKey.getStatusById(transport.get("product_file",transportProductFileKey.NOPRODUCTFILE)) %>	</span>
									</td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">质检流程：</td>
									<td style="width: 80%;" nowrap="nowrap">
										<span class="<%=qualityInspectionClass%>"><%= transportQualityInspectionKey.getStatusById(transport.get("quality_inspection",transportQualityInspectionKey.NO_NEED_QUALITY)+"") %></span>
									</td>
								</tr>
							</table>
						</fieldset>
					</td>
					
				</tr>
				</tbody>
			</table>
		</div>
		<div id="transport_info">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr id="transport_basic_main">
						<td align="center" width="33%">
							<table style="border:0">
									<tr>
										<td style="text-align:right;width: 20%;" nowrap="nowrap">提货仓库：</td>
										<td style="width: 80%;" nowrap="nowrap">
											<%=send_psName %>
										</td>
									</tr>
							</table>
						</td>
						<td align="center" width="33%">
							<table style="border:0">
									<tr>
										<td style="text-align:right;width: 20%;" nowrap="nowrap">货运公司：</td>
										<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_waybill_name") %></td>
									</tr>
							</table>
						</td>
						<td align="center" width="34%">
							<table style="border:0">
									<tr>
										<td style="text-align:right;width: 20%;" nowrap="nowrap">收货仓库：</td>
										<td style="width: 80%;" nowrap="nowrap"><%=recivie_psName %> </td>
									</tr>
							</table>
						</td>
					</tr>
				</thead>
				<tbody>
					<tr id="transport_basic_detail" style="display:none;">
						<td>
						<table border="0" width="100%">
							<tr>
								<td width="30%" align="center" valign="top">
								<fieldset class="set">
									<legend title="提货仓库"><%=send_psName %></legend>
									<table style="width:100%;border:1">
										<tr>
											<td nowrap="nowrap" title="门牌号"><%=null!=transport?transport.getString("send_house_number"):"" %></td>
										</tr>
										<tr>
											<td title="街道"><%=null!=transport?transport.getString("send_street"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="城市"><%=null!=transport?transport.getString("send_city"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="邮编"><%=null!=transport?transport.getString("send_zip_code"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系人"><%=null!=transport?transport.getString("send_name"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系电话"><%=null!=transport?transport.getString("send_linkman_phone"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属省份">
											<%
												long ps_id3 = null!=transport?transport.get("send_pro_id",0L):0L ;
												DBRow psIdRow3 = productMgr.getDetailProvinceByProId(ps_id3);
											%>
											<%=(psIdRow3 != null? psIdRow3.getString("pro_name"):transport.getString("address_state_send") ) %>
											</td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属国家">
											<%
												long ccid3 = null!=transport?transport.get("send_ccid",0L):0L; 
												DBRow ccidRow3 = orderMgr.getDetailCountryCodeByCcid(ccid3);
			 								%>
											<%= (ccidRow3 != null?ccidRow3.getString("c_country"):"" ) %>	
											</td>
										</tr>
									</table>
								</fieldset>
								</td>
								<td width="40%" align="center" valign="bottom">
								
									<%
										int transport_method = transport.get("transportby",0);
										String transport_title = transportWayKey.getStatusById(transport.get("transportby",0));
										String transport_img_src =""; 
										switch (transport_method)
										{
										case 1:
											//transport_title = "海运";
											transport_img_src ="../imgs/air_type.png";
											break;
										case 2:
											//transport_title = "陆运";
											transport_img_src ="../imgs/air_type.png";
											break;
										case 3:
											//transport_title ="空运";
											transport_img_src = "../imgs/air_type.png";
											break;
										default:
											//transport_title ="快递";
											transport_img_src = "../imgs/air_type.png";
										}
										
									%>
									<img alt="货运方式:<%=transport_title %>" title="货运方式:<%=transport_title %>" width="176" height="89" src="../imgs/air_type.png" border="0">
									<fieldset class="set">
										<table style="width:100%;border:0">
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">货运公司：</td>
												<td style="width: 80%;" nowrap="nowrap">
													<%=transport.getString("transport_waybill_name") %>
												</td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">运输方式：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transportWayKey.getStatusById(transport.get("transportby",0)) %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">发货港：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_send_place")%></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">运单号：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_waybill_number") %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">承运公司：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("carriers") %></td>
											</tr>
						
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">目的港：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_receive_place")%></td>
											</tr>
										</table>
									</fieldset>
								</td>
								<td width="30%" align="center" valign="top">
								<fieldset class="set">
									<legend title="收货仓库"><%=recivie_psName %></legend>
									<table style="width:100%;border:1">
										<tr>
											<td nowrap="nowrap" title="门牌号"><%=transport.getString("deliver_house_number") %></td>
										</tr>
										<tr>
											<td title="街道"><%=transport.getString("deliver_street") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="城市"><%=transport.getString("deliver_city") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="邮编"><%=transport.getString("deliver_zip_code") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系人"><%=transport.getString("transport_linkman")%></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系电话"><%=transport.getString("transport_linkman_phone")%></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属省份">
											<%
												long ps_id4 = null!=transport?transport.get("deliver_pro_id",0l):0L;
												DBRow psIdRow4 = productMgr.getDetailProvinceByProId(ps_id4);
											%>
											<%=(psIdRow4 != null? psIdRow4.getString("pro_name"):transport.getString("address_state_deliver") ) %>
											</td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属国家">
												<%
													long ccid4 = null!=transport?transport.get("deliver_ccid",0l):0L; 
													DBRow ccidRow4 = orderMgr.getDetailCountryCodeByCcid(ccid4);
												%>
												<%= (ccidRow4 != null?ccidRow4.getString("c_country"):"" ) %>	
											</td>
										</tr>
									</table>
								</fieldset>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div id="tabbar" class="tabdown" style="height:15px;padding-top:0px;text-align:center;margin:0 auto;padding-left:90%; float:inherit ;cursor:pointer;" title="更多""></div>
	<br/>
	
	<div id="transportTabs" style="display:none">
		<ul>
			<li><a href="#transport_price_info" title="运费流程阶段:需要运费 "><span>运费信息</span></a></li>
			<li><a href="#transport_out_info" title="出口报关:进行中"><span>出口报关 </span></a></li>
			<!--<li><a href="#transport_in_info" title="进口清关:进行中"><span>进口清关</span></a></li>-->
			<li><a href="#transport_tag_make" title="内部标签:"><span>内部标签</span></a></li>
			<li><a href="#transport_tag_third" title="第三方标签:"><span>第三方标签</span></a></li>
			<li><a href="#transport_order_prove" title="单证信息:"><span class="spanBold spanBlue">单证信息</span></a></li>
			<li><a href="#product_file" title="实物图片:"><span>实物图片</span></a></li>
			<li><a href="#quality_inspection" title="质检:"><span>质检信息</span></a></li>
		</ul>
		<div id="transport_price_info">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="10px">
					<td colspan="3">
			<!--  	<input name="button" type="button" value="运费修改" onClick="showFreightCost()" class="long-button"/>
					<input name="button" type="button" value="申请运费" onClick="applyFreigth('0','0','0')" class="long-button"/>
					<input type="button" onclick="uploadInvoice(<%=transport_id%>)" value="上传商业发票" class="long-button-upload">
					<input class="short-button" type="button" onclick='stock_in_set(<%=transport_id%>)' value="运费跟进"/>
					<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id%>',5,'运费流程')"/>
					-->	
					</td>
				</tr>
			</table>
			<table id="tables" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table">
				<thead>
					<tr>
						<th width="21%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
						<th width="11%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>
						<th width="13%" nowrap="nowrap" class="right-title"  style="text-align: center;">小计</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="2">
							<fieldset class="set" style="border-color:green;">
								<legend>运费:0.0RMB</legend>
								<table style="width:100%" align="left"></table>
							</fieldset>
					 	</td>
					 	<td colspan="5" align="right">总计</td>
					 	<td nowrap="nowrap">0.0</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="transport_out_info">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="10px">
					<td>
					<!--  
						<input type="button" value="报关跟进" class="short-button" onclick='declarationButton(<%=transport_id%>)'/>
						<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id%>',7,'出口报关')"/>
					-->
					</td>
				</tr>
			</table>
			<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" >
				<thead>
				<tr>
					<th width="10%" nowrap="nowrap" class="right-title" style="text-align: center;">操作员</th>
					<th width="40%" nowrap="nowrap" class="right-title" style="text-align: center;">内容</th>
					<th width="15%" nowrap="nowrap" class="right-title" style="text-align: center;">预计完成时间</th>
					<th width="15%" nowrap="nowrap" class="right-title" style="text-align: center;">记录时间</th>
					<th width="15%" nowrap="nowrap" class="right-title" style="text-align: center;">操作类型</th>
				</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="5" style="text-align:center;line-height:80px;height:80px;border:1px solid silver;">无数据</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--
		<div id="transport_in_info">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="30px">
					<td>
						<input type="button"  value="清关跟进" class="short-button" onclick='clearanceButton(<%=transport_id%>)'/>
						<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id%>',6,'进口清关')"/>
					</td>
				</tr>
			</table>
			<br/>
			<table width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable">
				<thead>
					<tr>
						<th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
						<th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
						<th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
						<th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
						<th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="5" style="text-align:center;line-height:80px;height:80px;border:1px solid silver;">无数据</td>
					</tr>
				</tbody>
			</table>
		</div>
		-->
		<div id="transport_tag_make">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="30px">
					<td>
						<input type="button" class="long-button-print" value="制作唛头" onclick="shippingMark()"/>
						<input type="button" class="long-button-print" value="制作标签" onclick="printLabel()"/>
						<input type="button" class="long-button" value="内部标签跟进" onclick="tag('<%=transport_id%>')"/>
						<input type="button" class="long-button" value="查看日志" onclick="showSingleLogs('<%=transport_id %>',8,'内部标签')"/> 
					</td>
				</tr>
			</table>
		</div>
		<div id="transport_tag_third">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="30px">
					<td>
						<input type="button" class="long-button" value="第三方标签跟进" onclick="tagThird('<%=transport_id%>')"/>
						<input type="button" class="long-button" value="查看日志" onclick="showSingleLogs('<%=transport_id%>',14,'第三方标签')"/>
					</td>
				</tr>
			</table>
			<div id="tagTabs" style="margin-top:15px;">
				<ul>
					<li><a href="#transport_product_tag_0">AMZON标签</a></li>
					<li><a href="#transport_product_tag_1"> UPC标签</a></li>
				</ul>
				<div id="transport_product_tag_0">
					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
						<thead>
						<tr> 
							<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
							<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
							<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
						</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
							</tr>
						</tbody>
		  			</table>
			  	</div>
			  	<div id="transport_product_tag_1">
					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
						<thead>
						<tr> 
							<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
							<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
							<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
						</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
							</tr>
						</tbody>
		  			</table>
			  	</div>
			</div>
		</div>
		<div id="transport_order_prove">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="30px">
					<td>
						<input class="short-button" type="button" value="单证跟进" onclick="followUpCerficate();">
						<input class="short-button" type="button" value="查看日志" onclick="showSingleLogs('<%=transport_id%>',9,'单证流程')">
					</td>
				</tr>
			</table>
			<fieldset style="width:90%;border:2px #cccccc solid;padding:2px;-webkit-border-radius:7px;-moz-border-radius:7px;">
				<legend style="font-size:15px;font-weight:normal;color:#999999;">上传文件</legend>
				<table width="90%" border="0">
					<tr>
						<td height="25" align="right"  style="width:120px;"  class="STYLE2" nowrap="nowrap">上传文件:</td>
						<td>&nbsp;<input type="button" class="long-button" onclick="uploadFile('uploadCertificateImageForm');" value="选择文件" />
							<input type="button" class="long-button" onclick="onlineScanner('uploadCertificateImageForm');" value="在线获取" />
				 		</td>
					</tr>
					<tr>
						<td height="25" align="right"  class="STYLE2" nowrap="nowrap">文件分类:</td>
						<td>&nbsp;<select name="file_with_class" id="file_with_class_certificate" onchange="fileWithClassCertifycateChange();">
							<option value="1">装箱单 </option>
							<option value="2">装运包装 </option>
							<option value="3">报关文件 </option>
							<option value="4">清关文件</option>
						</select></td>
					</tr>
				</table>
			</fieldset>
			<div id="tabsCertificate" style="margin-top:10px;">
				<ul>
					<li><a href="#transport_certificate_0">装箱单</a></li>
					<li><a href="#transport_certificate_1">装运包装</a></li>
					<li><a href="#transport_certificate_2">报关文件</a></li>
					<li><a href="#transport_certificate_3">清关文件</a></li>
				</ul>
				<div id="transport_certificate_0">
					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
						<thead>
							<tr> 
		 						<th width="75%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
		 						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
		 					</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
							</tr>
						</tbody>
		 			</table>
				</div>
				<div id="transport_certificate_1">
					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
						<thead>
							<tr> 
		 						<th width="75%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
		 						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
		 					</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
							</tr>
						</tbody>
		 			</table>
				</div>
				<div id="transport_certificate_2">
					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
						<thead>
							<tr> 
		 						<th width="75%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
		 						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
		 					</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
							</tr>
						</tbody>
		 			</table>
				</div>
				<div id="transport_certificate_3">
					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
						<thead>
							<tr> 
		 						<th width="75%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
		 						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
		 					</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
							</tr>
						</tbody>
		 			</table>
				</div>
			</div>
		</div>
		<div id="product_file" style="height:200px">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="30px">
					<td>
						<input type="button" value="实物图片跟进" class="long-button" onclick="productFileFollowUp();"/>
						<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',10,'实物图片')"/>
					</td>
				</tr>
			</table>
			<div id="content1" style="float:left;padding: 5px;display:block;">
				<div class="mediumGallery">
					<div class="nav">商品包装</div>
					<div class="images thumbs">
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/1.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/2.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/3.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
					</div>
				</div>
			</div>
			<div id="content2" style="float:left;padding: 5px;display:block;">
				<div class="mediumGallery">
					<div class="nav">商品本身</div>
					<div class="images thumbs">
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/1.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/2.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
					</div>
				</div>
			</div>
			<div id="content3" style="float:left;padding: 5px;display:block;">
				<div class="mediumGallery">
					<div class="nav">商品底贴</div>
					<div class="images thumbs">
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/1.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/2.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/3.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/4.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/5.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
					</div>
				</div>
			</div>
			<div id="content4" style="float:left;padding: 5px;display:block;">
				<div class="mediumGallery">
					<div class="nav">商品称重</div>
					<div class="images thumbs">
					</div>
				</div>
			</div>
			<div id="content5" style="float:left;padding: 5px;display:block;">
				<div class="mediumGallery">
					<div class="nav">商品标签</div>
					<div class="images thumbs">
						<div style="display: table-cell;"><img width="50" height="33" alt="" src="http://tympanus.net/Tutorials/MicroGallery/Gallery1/5.JPG" style="display: inline;" onclick="showPictrueOnline('7','1000071','T_product_1000071_基础信息.jpg','1','1000038');"></div>
					</div>
				</div>
			</div>
		</div>
		<div id="quality_inspection">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="30px">
					<td>
						<input type="button"  value="质检跟进" class="short-button" onclick='quality_inspectionKey(<%=transport_id%>)'/>
						<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',11,'质检流程')"/>
					</td>
				</tr>
			</table>
			<fieldset style="width:90%;border:2px #cccccc solid;padding:2px;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<legend style="font-size:15px;font-weight:normal;color:#999999;">上传质检文件</legend>
					<table width="90%" border="0">
						<tr>
							<td height="25" align="right"  style="width:120px;"  class="STYLE2" nowrap="nowrap">上传文件:</td>
							<td>&nbsp;<input type="button"  class="long-button" onclick="uploadFile('uploadCertificateImageForm');" value="选择文件" />
							<input type="button" class="long-button" onclick="onlineScanner('qualityInspectionForm');" value="在线获取" /></td>
						</tr>
					</table>
			</fieldset>
		</div>
	</div>
	<div style="min-height: 25px">
		<table style="margin-top:0px;margin-bottom:4px; padding: 0" width="98%" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td align="right">
					<input type="button" value="收货差异" onclick="showDifferent(1)" class="short-button"/>&nbsp;&nbsp;
				</td>
			</tr>
		</table>
	</div>
	
	<!--  <div id="detail" align="left" style="padding:0px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	</div>-->
	
	<div id="detail" align="left" style="padding:0px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<table id="dTable" border="0" cellpadding="0" cellspacing="0" style="width:100%; padding:0;" class="dTable">
			<caption id="caption" class="tcaption" style="background-color: #CCCCCC;  border-color: #bbbbbb; border-style: solid; border-width: 1px 1px 1px 0;color: #333333;font-size: 12px;font-weight: bold;height: 20px;padding: 5px;text-align: left;"> </caption>
			<thead>
				<tr class="head" style="background-color: #e5e5e5;height: 20px;padding: 5px;">
					<th class="right-title tleft" style="vertical-align: center; text-align: center;">商品名(商品条码)</th>
					<th class="right-title" style="vertical-align: center; text-align: center;">货物总数</th>
					<th class="right-title" style="vertical-align: center; text-align: center;">V(cm³),W(Kg),估算运费</th>
					<th class="right-title" style="vertical-align: center; text-align: center;">商品序列号</th>
					<th class="right-title" style="vertical-align: center; text-align: center;">容器数</th>
					<th class="right-title tright" style="vertical-align: center; text-align: center;">批次</th>
				</tr>
			</thead>
			<tbody class="tdata">
				<tr id="T0" class="row" style="height: 20px; ">
					<td class="tleft tcol tright" colspan="6">&nbsp;</td>
				</tr>
			</tbody>
			<tfoot>
				<tr style="background-color: #e5e5e5;height: 20px;padding: 5px;">
					<td colspan="6" class="tfoot">
						<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
							<tr>
								<td height="28" align="right" valign="middle" class="turn-page-table">
								</td>
							</tr>
					   </table>				
					</td>
				</tr>
			</tfoot>
			
		</table>
	</div>
	
	<br/>
</div>
<form name="download_form" method="post"></form>

<script type="text/javascript">

function setPage(data)
{
	var pageHtml = "页数：" + data.page + "/" + data.total + " &nbsp;&nbsp;总数：" +  data.records + " &nbsp;&nbsp;";
	try
	{
		var  nowPage = parseInt(data.page);
		var  prePage = parseInt(nowPage) - 1;
		var  nextPage  = parseInt(nowPage) + 1;
		var  lastPage  = parseInt(data.total);
		if(nowPage > lastPage )
		{
			nowPage = 1;
		}
		
		pageHtml += getAlinkHtml("gop","首页","javascript:go(1)",null,nowPage != 1 );
		pageHtml += getAlinkHtml("gop","上一页","javascript:go(" + prePage + ")",null,prePage >= 1 );
		pageHtml += getAlinkHtml("gop","下一页","javascript:go("+nextPage+")",null,nextPage <= lastPage );
		pageHtml += getAlinkHtml("gop","末页","javascript:go("+lastPage+")",null,nowPage != lastPage);
		
		pageHtml += "跳转到 <input name='jump_p2' type='text' id='jump_p2' style='width: 28px;' value='"+nowPage+"' /> ";
		pageHtml += "<input name='Submit22' type='button' class='page-go' style='width: 28px; padding-top: 0px;' onClick=\"javascript:go(document.getElementById('jump_p2').value)\" value='GO' />";

		$(".dTable tfoot .turn-page-table").html(pageHtml);
	}
	catch(e)
	{
		alert("系统异常");
	}
}

function go(page)
{
	if(!(/^[1-9]\d*$/.test(page)))
	{
		alert("请输入正确的页数");
		return;
	}
	 getProductData(page);
}

function getAlinkHtml( classn, linkName, linkUrl, title, flag)
{
	 var tempHtml = "";

     if( flag )
     {
    	 tempHtml += "\n<a href=\""+linkUrl+"\"";
         
         if (classn!=null||!classn.equals(""))
         {
        	 tempHtml += " class='"+classn+"' ";
         }
     }
     else
     {
    	 tempHtml += "\n<font color=\"#AAAAAA\"";
     }

     if( title != null && title.length() > 0 )
     {
    	 tempHtml += " title=\""+title+"\"";
     }

     tempHtml += ">" + linkName + "<";
     
     if( flag )
     {
    	 tempHtml += "/a>\n";
     }
     else
     {
    	 tempHtml += "/font>\n";
     }
     return tempHtml;
}

$(document).ready(function(){
	 //获取总重量和总体积
	 getTransportSumVWP(<%=transport_id%>);
	 getProductData(1);
});


function getProductData(page)
{
	 //获取商品详情
	 $.ajax({
			url:'<%= ConfigBean.getStringValue("systenFolder") %>action/administrator/transport/getTransportDetailJson.action',
			type: 'POST',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			async:false,
			data:{transport_id:<%=transport_id%>,page:page,rows:"5"},
			beforeSend:function(request){
			},
			error: function(){
				alert("获取明细失败，请重试！");
			},
			success: function(data)
			{
				(function(){setRowData(data);})();
				//设置分页
				(function(){setPage(data);})();
			}
		});
}
function setRowData(data)
{
	$(".dTable tbody.tdata").html("<tr id=\"T0\" class=\"row\" style=\"height: 20px; \"> <td class=\"tleft tcol tright\" colspan=\"6\">&nbsp;</td></tr>");
	if (data.rows.length>0) 
	{
		$("#T0").hide();
		for (var i=0;i<data.rows.length;i++) 
		{
			var item = data.rows[i];
			var row1 ="<tr id=\"T"+(i+1)+"\" class=\"row\" onclick=\"showDetail("+(i+1)+",this);\" height=\"20px;\">"+
				"<td class=\"tleft tcol\">"+item.p_name+"("+item.p_code+")</td><td class=\"tcol\" style=\"text-align: center\">"+parseInt(item.transport_count)+"</td>"+
				"<td class=\"tcol\">"+item.volume_weight_freight+"</td><td class=\"tcol\">"+item.transport_product_serial_number+"&nbsp;</td>"+
				"<td class=\"tcol\" style=\"text-align: center\">"+"&nbsp;</td><td class=\"tcol tright\">"+item.lot_number+"&nbsp;</td></tr>";
			$(".dTable tbody.tdata").append(row1);
			var row2 = "<tr id=\"D"+(i+1)+"\" class=\"row\" style=\"display: none\" onclick=\"showDetail("+(i+1)+",this);\"><td class=\"tcol tfoot\" colspan=\"6\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"15\"><tr>";
			if (item.containers && item.containers.length>0) 
			{
				
				if(typeof(item.containers) == "object") 
				{
					for (var j=0;j<item.containers.length;j++) 
					{
						var container = item.containers[j];
						var type ="";
						if(container.pick_container_type==0) {
							type ="Original";
							row2 +="<td style=\"margin-right:5px;\"><fieldset class=\"set\"><legend style=\"min-width:60px\">"+type+": "+container.pick_up_quantity+"</legend><table style=\"margin: 0px auto 0px 0px;\"><tbody><tr><td style=\"font-size:12px;\" nowrap=\"nowrap\"><font color=\"blue\">Original</font></td></tr></tbody></table></fieldset></td>";
							continue;
						}
						if (container.pick_container_type==1) {
							type ="CLP";
						} else if(container.pick_container_type==2) {
							type ="BLP";
						} else if(container.pick_container_type==3) {
							type ="TLP";
						} else if(container.pick_container_type==4) {
							type ="ILP";
						}
						row2 +="<td style=\"margin-right:5px;\"><fieldset class=\"set\"><legend style=\"min-width:60px\">"+type+": "+container.pick_up_quantity+"</legend><table style=\"margin: 0px auto 0px 0px;\"><tbody><tr><td style=\"font-size:12px;\" nowrap=\"nowrap\"><font color=\"blue\">"+container.code+"</font></td></tr></tbody></table></fieldset></td>";
					}
				}
			}
			row2 += "</tr></table></td></tr>";
			$(".dTable tbody.tdata").append(row2);
	    }
	
		$(".dTable tbody tr.row").mouseover(function() 
		{
			var vid = ($(this).attr("id")).substring(1);
			if (vid!="0") {
				$("#T"+vid).addClass("over");
				$("#D"+vid).addClass("over");
			}
		}).mouseout(function() {
			var vid = ($(this).attr("id")).substring(1);
			if (vid!="0") {
				$("#T"+vid).removeClass("over");
				$("#D"+vid).removeClass("over");
			}
		}).click(function(){
			var vid = ($(this).attr("id")).substring(1);
			if (vid!="0") {
				$(this).addClass("over");
			}
		});
  }
}

function showDetail(id,sobj) {
	if ($("#D"+id).css('display')=='none') {
		$("#T"+id).addClass("over");
		$("#D"+id).show();
		$("#T"+id).addClass("open");
	} else {
		$("#T"+id).removeClass("open");
		$("#D"+id).hide();
	}
}

function getTransportSumVWP(transport_id)
{
$.ajax({
	url: '/Sync10/action/administrator/transport/getTransportSumVWPJson.action',
	type: 'post',
	dataType: 'json',
	timeout: 60000,
	cache:false,
	data:{transport_id:transport_id},
	
	beforeSend:function(request){
	},
	error: function(e){
		alert("提交失败，请重试！");
	},
	
	success: function(date)
	{
		//mygrid.setCaption("总体积:"+date.volume+" cm³ 总货款:"+date.send_price+" RMB 总重量:"+date.weight+" Kg");
		$("#caption").html("总体积:"+date.volume+" cm³ 总重量:"+date.weight+" Kg");
		/*
		$("#sum_volume").html("总体积:"+date.volume+" cm³");
		$("#sum_weight").html("总重量:"+date.weight+" Kg");
		$("#sum_price").html("总货款:"+date.send_price+" RMB");
		*/
	}
});
}



function onLoadInitZebraTable()
{

}

$('#tabbar').bind("click",opBasicTab);
function opBasicTab()
{
	if ($("#basic_info_detail").css('display')=='none')
	 {
		$("#basic_info_main").hide();
		$("#basic_info_detail").show();
		$("#transport_basic_main").hide();
		$("#transport_basic_detail").show();
		$("#tabbar").attr("class", "tabup");
		} 
		else 
		{
		$("#basic_info_main").show();
		$("#basic_info_detail").hide();
		$("#transport_basic_main").show();
		$("#transport_basic_detail").hide();
		$("#tabbar").attr("class", "tabdown");
	}
}




$("#transportMainTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) 
	{
		onLoadInitZebraTable();
	}
});

$("#tagTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tabsCertificate").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: {expires: 30000},
	load: function(event, ui) {onLoadInitZebraTable();}
});
//var lastsel;
function buttonFormat( cellvalue, options, rowObject )
{
	return "<button onclick='alert(1)' title='a'>上传图片</button><br/><button onclick='alert(2)' title='b'>上传标签</button>";
}

function buttonUnFormat( cellvalue, options, cell)
{  
    return "&nbsp;";  
}

function transportCount( cellvalue, options, rowObject )
{
	return "<button onclick='alert(1)' title='a'>上传图片</button><br/><button onclick='alert(2)' title='b'>上传标签</button>";
}

function showPictrueOnline0(fileWithType,fileWithId,currentName)
{
	var obj = {
		file_with_type:fileWithType,
		file_with_id:fileWithId,
		current_name:currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'/Sync10/upload/transport'
	}
	if (window.top && window.top.openPictureOnlineShow) {
		window.top.openPictureOnlineShow(obj);
	} else {
		openArtPictureOnlineShow(obj,'/Sync10/');
	}
}
function showPictrueOnline(fileWithType,fileWithId,currentName,product_file_type)
{
    var obj = {
		file_with_type:fileWithType,
		file_with_id:fileWithId,
		current_name:currentName,
		product_file_type:product_file_type,
		cmd:"multiFile",
		table:'product_file',
		base_path:'/Sync10/upload/transport'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'/Sync10/');
	}
}
function onlineScanner(target)
{
	var _target = "";
	if(target == "uploadCertificateImageForm"){
		_target = "uploadCertificateImageForm";
	}else if(target == "qualityInspectionForm" ){
		_target = "qualityInspectionForm";
	}
    var uri = '/Sync10/' + "administrator/file/picture_online_scanner.html?target="+_target; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}

function addProductPicture(pc_id)
{
	 //添加商品图片
	var transport_id = '<%= transport_id%>';
	var uri = "/Sync10/administrator/transport/transport_product_picture_up.html?transport_id="+transport_id+"&pc_id="+pc_id;
	$.artDialog.open(uri , {title: "商品范例上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,close:function(){window.location.reload();},fixed: true});
}

</script>

<script type="text/javascript">

//到货派送
function goodsArriveDelivery(transport_id)
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_goods_arrive_delivery.html?transport_id='+transport_id;
	$.artDialog.open(url, {title: '到货派送['+transport_id+"]",width:'500px',height:'200px', lock: true,opacity: 0.3});
}

//下载转运单
function downloadTransportOrder(transport_id)
	{
		var para = "transport_id="+transport_id;
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/downTransportOrder.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
	}
	
//打印转运单		
function print()
{
	visionariPrinter.PRINT_INIT("转运单");
			
	visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
	visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../transport/print_transport_order_detail.html?transport_id=<%=transport_id%>");
				
	visionariPrinter.PREVIEW();
}

//发货差异
function showDifferent(selectIndex)
{
	$.artDialog.open('transport_different_show.html?transport_id=<%=transport_id%>&select_index='+selectIndex, {title: "转运单差异",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
}
</script>

</body>
</html>