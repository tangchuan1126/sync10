<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.DeclarationKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
 
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.ArrayList" %>
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>报关流程</title>

<!--  基本样式和javascript -->
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<!-- 遮罩 -->
 <script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
 <!-- 文件预览  -->
 <script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<style type="text/css">
*{margin:0px;padding:0px;}
div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .title{ background: url("../js/popmenu/pro_title.jpg") no-repeat scroll left center transparent;color: #FFFFFF;display: table;font-size: 14px; height: 27px;padding-left: 5px; padding-top: 3px;width: 356px;line-height:27px;}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:80px;text-align:center;border:1px solid silver;margin:0px auto;width:300px;height:30px;background:white;line-height:30px;font-size:14px;}	
	#ui-datepicker-div{z-index:9999;display:none;}
	ul.fileUL{list-style-type:none;}
	ul.fileUL li{line-height:20px;height:20px;padding-left:3px;padding-right:3px;margin-left:2px;float:left;margin-top:1px;border:1px solid silver;}
</style>
<style type="text/css">
 	select.key{display:none;}
</style>
<%
 
	long transport_id = StringUtil.getLong(request,"transport_id");
	DBRow transportRow = transportMgrZJ.getDetailTransportById(transport_id) ;

	int declaration = transportRow.get("declaration",0);
	DeclarationKey key = new DeclarationKey();
	TDate tdate = new TDate();
	String handleTransportDeclaration = ConfigBean.getStringValue("systenFolder") + "action/administrator/transport/TransportDeclarationAction.action";
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
%>
 <script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
 jQuery(function($){

	    
	 	if('<%= declaration%>' * 1 > 0){
			$("#declaration option[value='"+'<%=declaration%>'+"']").attr("selected",true);
		}
	 	$('#eta').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
		 
			onSelect:function(dateText, inst){

				setContentHtml(dateText);
				
			}
			
		});
	 	changeDeclarationType();
})
function setContentHtml(value){
	 var clearanceHTML = $.trim($("#declaration option:selected").html());
		var html = "";
		if($("#declaration").val() * 1 == '<%= DeclarationKey.FINISH%>'){
			html = "[报关完成]完成:"
		}else{
			html = "["+clearanceHTML+"]阶段预计"+$("#eta").val()+"完成:";
		}
		 
		var contentNode = $("#context");
		var contentNodeValue = contentNode.val();
		 
		if(contentNodeValue.indexOf("完成:") != -1){
			var index = contentNodeValue.indexOf("完成:")
			html += contentNodeValue.substr(index +3);
			$("#context").val(html);
		}else{
			$("#context").val(html + contentNodeValue);
		}
}
function submitForm(){
	// 如果是完成的状态那么就是应该form表单提交 包含有文件

	// 如果不是的那么就是普通的表单提交
	if(!validate()){return ;}
	var declaration = $("#declaration")
 	 
 	ajaxSubmit();
	 
}
function ajaxSubmit(){
  
	$.ajax({
		url:'<%= handleTransportDeclaration %>',
		data:$("#myform").serialize(),
		dataType:'json',
		beforeSend:function(request){
 		  $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
			$.unblockUI();
			if(data && data.flag == "success"){
				cancel();
				$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			}else{	showMessage("系统错误","error");}
		},
		error:function(){
			showMessage("系统错误","error");
		}
	})
}
function validate(){
	
	if($("#context").val().length < 1){
		showMessage("请输入备注","alert");
		return false;
	}
	var declaration = $("#declaration")
	if(declaration.val()*1  == '<%= DeclarationKey.FINISH%>' * 1){
	    $("#eta").val("");
	}else{
		if($("#eta").val().length < 1){
			showMessage("请选择时间","alert");
			return false;
		}
	}
	return true ;
}
function changeDeclarationType(){
	var declaration = $("#declaration");
	if(declaration.val() != '<%= DeclarationKey.FINISH%>'){
		$("#notfinish_span").css("display","inline-block");
		$("#file_up_span").css("display","none");
		$("#file_up_tr").css("display","none");
	}else{
		$("#notfinish_span").css("display","none");
		$("#file_up_span").css("display","inline-block");
		if($(".fileUL li").length > 0 ){
			$("#file_up_tr").attr("style","");
		}
	}
	setContentHtml();
}
function downLoad(fileName , tableName , folder){
	 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ '<%= systemConfig.getStringConfigValue("file_path_transport")%>');
}
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
// 回显要求看上去已经是上传了的文件。所以上传是要加上前缀名的sn
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_names']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			 
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
		
	 	//判断是不是有了
	 	if($("a",$("#over_file_td")).length > 0){
			var td = $("#over_file_td");
	
			td.append(lis);
		}else{
			$("#file_up_tr").attr("style","");
			$("#jquery_file_up").append(lis);
		}
	} 
	

}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+"_"+'<%= transport_id%>'+"_" + fileName;
    var  a = "<p class='new'><a href="+uri+">"+showName+"</a>&nbsp;&nbsp;(New)</p>";
    return a ;
}
//显示在线图片
function showPictrueOnline(fileWithType,fileWithId , currentName){
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_transport")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}
function onlineScanner(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
</script>
</head>
<body>
	 
	 <!--  读取清关 状态然后放在selecte 这个是会修改住信息上的 清关信息的 ,去读当前的清关流程的状态 勾选出来-->
	 <form id="myform">
 		<input type="hidden" name="transport_id" value="<%=transport_id %>"/>
		<%
			if(declaration ==  DeclarationKey.FINISH){
				%>
					<input type="hidden" name="declaration" value="<%=declaration %>" />
				<% 
			}
		%>
		 <table>
		 	<tr>
		 		<td style="text-align:right;">报关流程:</td>
		 		<td>
		 			<select name="declaration" <%=(declaration== DeclarationKey.FINISH?"disabled":"") %> id="declaration" onchange="changeDeclarationType();">
		 				<option value='<%= DeclarationKey.DELARATING %>'><%= key.getStatusById(DeclarationKey.DELARATING) %></option>
 		 				<option value='<%= DeclarationKey.FINISH %>'><%= key.getStatusById(DeclarationKey.FINISH) %></option>
		 			</select>
		 			<span id="notfinish_span" style=''>
		 		 		预计本阶段完成时间:<input type="text" id="eta" name="eta" value="<%= tdate.getYYMMDDOfNow() %>"/></span>
		 			</span>
		 			<span id="file_up_span" style="display:none;">
		 				<input type="hidden" id="sn" name="sn" value="T_declaration"/>
		 				<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.transport_declaration %>" />
		 				<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_transport")%>" />
		 				<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="上传凭证" />
		 				<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
		 			</span>
		 		</td>
		 		</tr>
		 		<tr id="file_up_tr" style='display:none;'>
		 		 	<td>完成凭证:</td>
		 		 	<td  >
		 		 		 
			              	<div id="jquery_file_up">	
			              		<input type="hidden" name="file_names" value=""/>
			              		 
			              	</div>
		 		 	</td>
		 		 </tr>
		 		 <%
		 		// 如果是完成应该在下面显示文件然后支持下载链接
		 		 if(transportRow.get("declaration",DeclarationKey.NODELARATION) == DeclarationKey.FINISH ){
		 			 DBRow[] fileRows = transportMgrZr.getFileByFilwWidthIdAndFileType("file",transportRow.get("transport_id",0l),FileWithTypeKey.transport_declaration );
		 				if(fileRows != null && fileRows.length > 0 ){
		 			 %>
		 			<tr>
		 			 	 <td>完成凭证:</td>
		 			 	 <td id="over_file_td">
		 			 	 <%for(DBRow fileRow : fileRows){ %>
		 			 	 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	 <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
		 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
		 			 	 	<%if(StringUtil.isPictureFile(fileRow.getString("file_name"))){ %>
				 			 	 <p>
				 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.transport_declaration %>','<%=transportRow.get("transport_id",0l) %>','<%=fileRow.getString("file_name") %>');"><%=fileRow.getString("file_name") %></a>
				 			 	 </p>
			 			 	 <%} else if(StringUtil.isOfficeFile(fileRow.getString("file_name"))){%>
			 			 	 		<p>
			 			 	 			<a href="javascript:void(0)"  file_id='<%=fileRow.get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("file_path_transport")%>')" file_is_convert='<%=fileRow.get("file_is_convert",0) %>'><%=fileRow.getString("file_name") %></a>
			 			 	 		</p>
			 			 	 <%}else{ %>
 		 			 	 	  		<p><a href='<%= downLoadFileAction%>?file_name=<%=fileRow.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_transport")%>'><%=fileRow.getString("file_name") %></a></p> 
 		 			 	 	  <%} %>
		 			 	 <%} %>
		 			 	 </td>
		 			 </tr>
		 			
		 			 <%
		 			 }
		 		 }
		 	%>
		 	</tr>
		 	<tr>
		 		<td style="text-align:right;">备注</td>
		 		<td>
		 			<textarea style="width:400px;height:185px;" id="context" name="context"></textarea>
		 		</td>
		 	</tr>
		 </table>
 	 
 	  </form>		
	   <div class="buttonDiv" style="margin-top:5px;"> 
	 	 	<input type="button" id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil"  onclick="submitForm()" value="提交"> 
			<button id="jqi_state0_button取消" value="n" class="jqidefaultbutton specl" name="jqi_state0_button取消" onclick="cancel();">取消</button>
	  </div>
	  		
 	  		 
 	<script type="text/javascript">
 	function cancel(){
 		$.artDialog && $.artDialog.close();
 	}
 	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 		
 	</script> 
	 
</body>
</html>