<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	long transport_id = StringUtil.getLong(request,"transport_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>交货单号：<%=transport_id %></title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll; 
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>



<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

	<script language="javascript" src="../../common.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	
<link href="../comm.css" rel="stylesheet" type="text/css"/>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>


<script src="../js/zebra/zebra.js" type="text/javascript"></script>

<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
</style>

</head>

<body >
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <tr>
  	<td valign="top" style="padding:15px;"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center">
				<span style="font-family:'黑体'; font-size:25px;"></span><br/>
            </td>
          </tr>
        </table>
	    
	  <br>
  	  <table width="98%" height="100%"  border="0" cellpadding="0" cellspacing="0">
  	    <tr>
  	    <!--  操作日志类型:1表示进度跟进2:表示财务跟进;3修改日志;1货物状态;5运费流程;6进口清关;7出口报关8标签流程;9:单证流程 10.实物图图片流程11.质检流程 -->
  	      <td  align="left" valign="top">
  	        <div class="demo">
  	          <div id="tabs">
  	            <ul>
	  	              <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>">全部日志<span> </span></a></li>
					  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=1">货物日志<span> </span></a></li>
					  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=2">财务日志<span> </span></a></li>
					  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=3">修改日志<span> </span></a></li>
 					  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=5">运费流程<span> </span></a></li>
					  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=6">进口清关<span> </span></a></li>
					  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=7">出口报关<span> </span></a></li>
					  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=8">内部标签<span> </span></a></li>
					  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=9">单证流程<span> </span></a></li>
					  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=10">实物图片<span> </span></a></li>
			 		  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=11">质检流程<span> </span></a></li>
			  		  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=12">商品标签<span> </span></a></li>
			  		  <li><a href="transport_logs_tright.html?transport_id=<%=transport_id %>&transport_type=14">第三方标签<span> </span></a></li>
			    </ul>
			  </div>
		    </div>
				  <script>
					$("#tabs").tabs({
						cache: true,
						spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
						cookie: { expires: 30000 } ,
						load: function(event, ui) {onLoadInitZebraTable();}	
					});
					</script>	</td>
	    </tr>
      </table>
  </td></tr>
 <tr>
 	<td align="right" class="win-bottom-line">
     <input type="button" name="Submit2" value="关闭" class="normal-white" onClick="closeWindow()">	</td>
 </tr>
</table>
<script type="text/javascript">
function closeWindow(){
	parent.location.reload();
	$.artDialog.close();
}
</script>
</body>
</html>

