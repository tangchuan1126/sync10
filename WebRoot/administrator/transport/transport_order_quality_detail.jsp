<%@page language="java" contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey" />
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey" />
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey" />
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey" />
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey" />
<jsp:useBean id="transportOrderKey" class="com.cwc.app.key.TransportOrderKey" />
<jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey" />
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey" />
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey" />
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="transportQualityInspectionKey" class="com.cwc.app.key.TransportQualityInspectionKey" />
<jsp:useBean id="transportRoleKey" class="com.cwc.app.key.TransportRoleKey" /><%
	long transport_id = StringUtil.getLong(request,"transport_id");
	TDate tDate = new TDate();
	//fileFlag
	String fileFlag= StringUtil.getString(request,"flag");
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
	int isPrint = StringUtil.getInt(request,"isPrint",0);
	//转运单title
	String title_name = "";
	DBRow titleRow = proprietaryMgrZyj.findProprietaryByTitleId(transport.get("title_id", 0L));
	if(null != titleRow)
	{
		title_name = titleRow.getString("title_name");
	}
	
	AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(request.getSession(true));
	long psId = adminLoggerBean.getPs_id();
	int number = 0;
	boolean edit = false;
	
	//仅能编辑管辖的仓库运单
	if (transport!=null && transport.get("send_psid", 0L)==psId) {
	
	}
	
	HashMap followuptype = new HashMap();
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改转运单详细记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	
	String downLoadFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	String updateTransportCertificateAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportCertificateCompleteAction.action";
	//读取配置文件中的purchase_tag_types
	String tagType = systemConfig.getStringConfigValue("transport_tag_types");
	String[] arraySelected = tagType.split("\n");
	//将arraySelected组成一个List
	ArrayList<String> selectedList= new ArrayList<String>();
	for(int index = 0 , count = arraySelected.length ; index < count ; index++ )
	{
		String tempStr = arraySelected[index];
		if(tempStr.indexOf("=") != -1){
			String[] tempArray = tempStr.split("=");
			String tempHtml = tempArray[1];
			selectedList.add(tempHtml);
		}
	}
	// 获取所有的关联图片 然后在页面上分类
	Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
	int[] productFileTyps = {FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE};
	DBRow[] imagesRows = purchaseMgrZyj.getAllProductTagFilesByPcId(transport_id,productFileTyps );//临时拷贝样式，使用方法为采购单，后续开发请注意
	if(imagesRows != null && imagesRows.length > 0 )
	{
		for(int index = 0 , count = imagesRows.length ; index < count ; index++ )
		{
			DBRow tempRow = imagesRows[index];
			String product_file_type = tempRow.getString("product_file_type");
			List<DBRow> tempListRow = imageMap.get(product_file_type);
			if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1))
			{
				tempListRow = new ArrayList<DBRow>();
			}
			tempListRow.add(tempRow);
			imageMap.put(product_file_type,tempListRow);
		}
	}
	// 单证
	String valueCertificate = systemConfig.getStringConfigValue("transport_certificate");
	String[] arraySelectedCertificate = valueCertificate.split("\n");
	//将arraySelected组成一个List
	ArrayList<String> selectedListCertificate= new ArrayList<String>();
	for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
		String tempStr = arraySelectedCertificate[index];
		if(tempStr.indexOf("=") != -1){
			String[] tempArray = tempStr.split("=");
			String tempHtml = tempArray[1];
			selectedListCertificate.add(tempHtml);
		}
	}
	int file_with_type = StringUtil.getInt(request,"file_with_type");
	String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
	String file_with_class = StringUtil.getString(request,"file_with_class");
	DBRow[] imageListCertificate = fileMgrZJ.getFileByWithIdAndType(transport_id,FileWithTypeKey.transport_certificate);
	Map<String,List<DBRow>> mapCertificate = new HashMap<String,List<DBRow>>();
	TransportCertificateKey certificateKey = new TransportCertificateKey();
	if(imageListCertificate != null && imageListCertificate.length > 0)
	{
		//map<string,List<DBRow>>
		for(int index = 0 , count = imageListCertificate.length ; index < count ;index++ ){
			DBRow temp = imageListCertificate[index];
			List<DBRow> tempListRow = mapCertificate.get(temp.getString("file_with_class"));
			if(tempListRow == null){
				tempListRow = new ArrayList<DBRow>();
			}
			tempListRow.add(temp);
			mapCertificate.put(temp.getString("file_with_class"),tempListRow);
		}
	}
	// 实物图片
	String updateTransportAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportProductFileCompleteAction.action";
	String valueProductFile = systemConfig.getStringConfigValue("transport_product_file");
	String[] arrayProductFileSelected = valueProductFile.split("\n");
	String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/transport/transport_order_quality_detail.html?transport_id="+transport_id;
	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
	//计算报关颜色
	String declarationKeyClass = "" ;
	int declarationInt = transport.get("declaration",declarationKey.NODELARATION);
	if(declarationInt == declarationKey.FINISH){
		declarationKeyClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_declaration);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			declarationKeyClass += " fontGreen";
		} else {
			declarationKeyClass += " fontRed";
		}
	} else if(declarationInt != declarationKey.NODELARATION ) {
		declarationKeyClass +=  "spanBold spanBlue";
	}
	//计算清关颜色
	String clearanceKeyClass = "" ;
	int clearanceInt = transport.get("clearance",clearanceKey.NOCLEARANCE);
	if( clearanceInt == clearanceKey.FINISH){
		clearanceKeyClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_clearance);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			clearanceKeyClass += " fontGreen";
		} else {
			clearanceKeyClass += " fontRed";
		}
	} else if(clearanceInt != clearanceKey.NOCLEARANCE) {
		clearanceKeyClass +=  "spanBold spanBlue";
	}
	//计算费用标签颜色
	String stockInSetClass = "" ;
	int stockInSetInt = transport.get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET);
	if(stockInSetInt == TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH){
		stockInSetClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_STOCKINSET);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			stockInSetClass += " fontGreen";
		} else {
			stockInSetClass += " fontRed";
		}
	} else if(stockInSetInt != TransportStockInSetKey.SHIPPINGFEE_NOTSET) {
		stockInSetClass += "spanBold spanBlue";
	}
	//计算内部标签颜色
	String tagClass = "";
	int tagInt = transport.get("tag",TransportTagKey.NOTAG);
	if(tagInt == TransportTagKey.FINISH){
		tagClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_TAG);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			tagClass += " fontGreen";
		} else {
			tagClass += " fontRed";
		}
	} else if(tagInt != TransportTagKey.NOTAG) {
		tagClass += "spanBold spanBlue";
	}
	int tagIntThird = transport.get("tag_third",TransportTagKey.NOTAG);
	//计算第三方标签颜色
	String tagClassThird = "";
	if(tagIntThird == TransportTagKey.FINISH){
		tagClassThird += "spanBold";
		int[] fileType	= {FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE};
	 	if(purchaseMgrZyj.checkPurchaseThirdTagFileFinish(transport_id,fileType,"transport_tag_types"))
	 	{
			tagClassThird += " fontGreen";
		} else {
			tagClassThird += " fontRed";
		}
	} else if(tagIntThird != TransportTagKey.NOTAG) {
		tagClassThird += "spanBold spanBlue";
	}
	//计算实物图片颜色
	String productFileClass = "";
	int productFileInt = transport.get("product_file",TransportProductFileKey.NOPRODUCTFILE);
	if(productFileInt == TransportProductFileKey.FINISH){
		productFileClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("product_file",transport.get("transport_id",0l),FileWithTypeKey.product_file);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			productFileClass += " fontGreen";
		} else {
			productFileClass += " fontRed";
		}
	} else if(productFileInt != TransportProductFileKey.NOPRODUCTFILE) {
		productFileClass += "spanBold spanBlue";
	}
	//计算质量流程颜色
	String qualityInspectionClass = "";
	int qualityInspectionInt = transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY);
	if(qualityInspectionInt == TransportQualityInspectionKey.FINISH){
		qualityInspectionClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_QUALITYINSPECTION);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			qualityInspectionClass += " fontGreen";
		} else {
			qualityInspectionClass += " fontRed";
		}
	} else if(qualityInspectionInt != TransportQualityInspectionKey.NO_NEED_QUALITY) {
		qualityInspectionClass += "spanBold spanBlue";
	}
	//计算颜色
	String certificateClass = "" ;
	int certificateInt = transport.get("certificate",TransportCertificateKey.NOCERTIFICATE);
	if(certificateInt == TransportCertificateKey.FINISH){
		certificateClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_certificate);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ) {
			certificateClass += " fontGreen";
		} else {
			certificateClass += " fontRed";
		}
	} else if(certificateInt != TransportCertificateKey.NOCERTIFICATE) {
		certificateClass += "spanBold spanBlue";
	}
%><html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>转运单 T<%=transport_id%></title>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<style type="text/css">
	body{text-align:left;}
	ol.fundsOl{
		list-style-type: none;	
	}
	ol.fundsOl li{
		width: 350px;
		height: 200px;
		float: left;
		line-height: 25px;
	}
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jqgrid 引用 -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>
<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}
.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
.ui-datepicker {z-index:1200;}
.rotate
{
    /* for Safari */
    -webkit-transform: rotate(-90deg);

    /* for Firefox */
    -moz-transform: rotate(-90deg);

    /* for Internet Explorer */
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
}
.ui-jqgrid .ui-jqgrid-htable th
{
	font-size:11px;
}
	
.ui-jqgrid tr.jqgrow td 
{
	white-space: normal !important;
	height:auto;
	vertical-align:text-top;
	padding-top:2px;
}
.set
{
	border:2px #999999 solid;
	padding:2px;
	width:90%;
	word-break:break-all;
	margin-top:10px;
	margin-top:5px;
	line-height:18px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	margin-bottom: 10px;
}
.create_order_button{background-attachment: fixed;background: url(../imgs/create_order.jpg);background-repeat: no-repeat;background-position: center center;height: 51px;width: 129px;color: #000000;border: 0px;}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
.zebraTable td {line-height:25px;height:25px;}
.right-title{line-height:20px;height:20px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
span.moredown {
	margin: 5;
	background-attachment: fixed;
	background: url(../imgs/maps/hide_light_down.png);
	background-repeat: no-repeat;background-position: center center;height: 16px;width: 16px;color: #000000;border: 0px;
}
span.moreup {
	margin: 5;
	background-attachment: fixed;
	background: url(../imgs/maps/hide_light_up.png);
	background-repeat: no-repeat;background-position: center center;height: 16px;width: 16px;color: #000000;border: 0px;
}
div.tabdown {
	background-attachment: fixed;
	background: url(../imgs/tabdown.png);
	background-repeat: no-repeat;background-position: center center;height: 15px;width: 79px;color: #000000;border: 0px;
	position:relative;
}
div.tabup {
	background-attachment: fixed;
	background: url(../imgs/tabup.png);
	background-repeat: no-repeat;background-position: center center;height: 15px;width: 79px;color: #000000;border: 0px;
	position:relative;
}

.smallGallery .nav{
    width:105px;
}
.mediumGallery .nav{
    width:165px;
}
.largeGallery .nav{
    width:225px;
}
.smallGallery .images,
.mediumGallery .images,
.largeGallery .images{
    padding:10px;
    background-color:#f9f9f9;
    border:1px solid #fff;
    position:relative;
    -moz-box-shadow:1px 1px 5px #aaa;
    -webkit-box-shadow:1px 1px 5px #aaa;
    box-shadow:1px 1px 5px #aaa;
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border-radius:5px;
}
.nav {
    height: 20px;
    padding-left: 4px;
}
.smallGallery .images,
.smallGallery .singleImg div{
    width:102px;
    height:102px;
}
.mediumGallery .images,
.mediumGallery .singleImg div{
    width:162px;
    height:108px;
}
.largeGallery .images,
.largeGallery .singleImg div{
    width:222px;
    height:222px;
}
.microGallery{
    position:relative;
    margin:30px 10px 10px 10px;
    float:left;
}
.microGallery img{
    margin-left:auto;
    margin-right:auto;
    border:none;
    -moz-box-shadow:0px 2px 4px #777;
    -webkit-box-shadow:0px 2px 4px #777;
    box-shadow:0px 2px 4px #777;
    display:none;
}
a.thumbview{
    opacity:0.6;
    width:20px;
    height:21px;
    float:left;
    cursor:pointer;
}
.images div{
    display:table-cell;
    vertical-align:middle;
    text-align:center;
    position:relative;
}

.smallGallery .thumbs div,
.mediumGallery .thumbs div,
.largeGallery .thumbs div{
    float:left;
    margin:2px;
    cursor:pointer;
}

.smallGallery .thumbs div{
    width:30px;
    height:30px;
}
.mediumGallery .thumbs div{
    width:50px;
    height:50px;
}
.largeGallery .thumbs div{
    width:70px;
    height:70px;
}
</style>
</head>
<body margin="1" onLoad="onLoadInitZebraTable()">
<div class="demo">
	<div style="min-height: 25px">
		<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
			<tr height="30px">
				<td>
					&nbsp;&nbsp;<input type="button" onClick="updateTransportAllProdures()" value="修改各流程信息" class="long-button"/>
					&nbsp;&nbsp;<input type="button" onclick="downloadTransportOrder('<%=transport_id%>')" value="下载运单" class="long-button-next">
					&nbsp;&nbsp;<input type="button" onclick="print()" value="打印运单" class="long-button-print">
				</td>
			</tr>
		</table>
	</div>
	<div id="transportMainTabs" style="min-height: 25px">
		<ul>
			<li ondblclick="opBasicTab()"><a href="#transport_basic_info"><span>基础信息</span></a></li>
			<li ondblclick="opBasicTab()"><a href="#transport_info"><span>货单信息</span></a></li>
		</ul>
		<div id="transport_basic_info">
			<table id="basic_info" width="100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
				<tr id="basic_info_main" >
					<td align="center" width="33%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">转运单号：</td>
									<td style="width: 80%;" nowrap="nowrap">T<%=transport_id%></td>
								</tr>
						</table>
					</td>
					<td align="center" width="33%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">TITLE：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=title_name%></td>
								</tr>
						</table>
					</td>
					<td align="center" width="34%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">货物状态：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=transportOrderKey.getTransportOrderStatusById(transport.get("transport_status",0))%></td>
								</tr>
						</table>
					</td>
				</tr>
				</thead>
				<tbody>
				<tr id="basic_info_detail" style="display:none;">
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>运单基础信息</legend>
							<table style="width:100%;border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">运单号：</td>
									<td style="width: 80%;" nowrap="nowrap">T<%=transport_id%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">TITLE：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=title_name%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">创建人：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("create_account")%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">创建时间：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=tDate.getEnglishFormateTime(transport.getString("transport_date"))%></td>
								</tr>
								<% if (transport!=null && transport.get("send_psid", 0L)==psId) { %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">ETD：</td>
									<td style="width: 80%;" nowrap="nowrap"><%
										if(transport.getString("transport_out_date").trim().length() > 0){
											out.print(tDate.getEnglishFormateTime(transport.getString("transport_out_date")));
										} else {
											out.print("&nbsp;");
										}
									%></td>
								</tr>
								<% } %>
								<% if (transport!=null && transport.get("receive_psid", 0L)==psId) { %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">ETA：</td>
									<td style="width: 80%;" nowrap="nowrap"><%
										if(transport.getString("transport_receive_date").trim().length() > 0){
											out.print(tDate.getEnglishFormateTime(transport.getString("transport_receive_date")));
										} else {
											out.print("&nbsp;");
										}
									%></td>
								</tr>
								<% } %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">允许装箱：</td>
									<td style="width: 80%;" nowrap="nowrap"><% 
										DBRow packinger = adminMgr.getDetailAdmin(transport.get("packing_account",0l));
										if(packinger!=null)
										{
											out.print(packinger.getString("employe_name"));
										}
										else
										{
											out.print("&nbsp;");
										}
									%></td>
								</tr>
							</table>
						</fieldset>
					</td>
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>运输基础信息</legend>
							<table style="width:100%;border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">货物状态：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=transportOrderKey.getTransportOrderStatusById(transport.get("transport_status",0)) %></td>
								</tr>
								<%if (declarationKey.NODELARATION != transport.get("declaration",declarationKey.NODELARATION)) { %>
								<%	String declaration_responsible = "";
									String declarationPersonsList = "";
									DBRow[] declarationPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 7);
									if(declarationPersons != null && declarationPersons.length > 0){
										
										for(DBRow tempUserRow : declarationPersons){
											declarationPersonsList += (tempUserRow.getString("employe_name")+"&nbsp");
										}
									}
									if (transport.get("declaration_responsible", 0)==1) {
										if (declarationPersonsList.length()>0) {
											declaration_responsible = "(发货方："+declarationPersonsList+")";
										} else {
											declaration_responsible = "(发货方"+declarationPersonsList+")";
										}
										
									} else if (transport.get("declaration_responsible", 0)==2) {
										if (declarationPersonsList.length()>0) {
											declaration_responsible = "(收货方："+declarationPersonsList+")";
										} else {
											declaration_responsible = "(收货方"+declarationPersonsList+")";
										}
									} else {
										declaration_responsible = "("+declarationPersonsList+")";
									}
									 %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">出口报关：</td>
									<td style="width: 80%;"><span class="<%=declarationKeyClass%>"><%=declaration_responsible%><%=declarationKey.getStatusById(transport.get("declaration",01)) %></span></td>
								</tr>
								<%}%>
								<%if (clearanceKey.NOCLEARANCE != transport.get("clearance",clearanceKey.NOCLEARANCE)) { %>
								<%	String clearance_responsible ="";
									String clearancePersonsList ="";
									
									DBRow[] clearancePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 6);
  							 		if(clearancePersons != null && clearancePersons.length > 0){
  							 			for(DBRow tempUserRow : clearancePersons){
  							 				clearancePersonsList += (tempUserRow.getString("employe_name")+"&nbsp;");
  							 			}
  							 		}
									if (transport.get("clearance_responsible", 0)==1) {
										if (clearancePersonsList.length()>0) {
											clearance_responsible = "(发货方："+clearancePersonsList+")";
										} else {
											clearance_responsible = "(发货方"+clearancePersonsList+")";
										}
									} else if (transport.get("clearance_responsible", 0)==2) {
										if (clearancePersonsList.length()>0) {
											clearance_responsible = "(收货方："+clearancePersonsList+")";
										} else {
											clearance_responsible = "(收货方"+clearancePersonsList+")";
										}
									} else {
										clearance_responsible = "("+clearancePersonsList+")";
									}
								%>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">进口清关：</td>
									<td style="width: 80%;"><span class="<%=clearanceKeyClass%>"><%=clearance_responsible%><%=clearanceKey.getStatusById(transport.get("clearance",01)) %></span></td>
								</tr>
								<%}%>
								<%if (TransportStockInSetKey.SHIPPINGFEE_NOTSET != transport.get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET)) {%><% 
									String stock_in_set_responsible= "";
									String stockInSetPersonsList = "";
									DBRow[] stockInSetPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 5);
	  							 	if(stockInSetPersons != null && stockInSetPersons.length > 0){
	  							 		for(DBRow tempUserRow : stockInSetPersons){
	  							 			stockInSetPersonsList+=(tempUserRow.getString("employe_name")+"&nbsp;");
	  							 		}
	  							 	}
									if (transport.get("stock_in_set_responsible", 0)==1) {
										if (stockInSetPersonsList.length()>0) {
											stock_in_set_responsible = "(发货方："+stockInSetPersonsList+")";
										} else {
											stock_in_set_responsible = "(发货方"+stockInSetPersonsList+")";
										}
									} else if (transport.get("stock_in_set_responsible", 0)==2) {
										if (stockInSetPersonsList.length()>0) {
											stock_in_set_responsible = "(收货方："+stockInSetPersonsList+")";
										} else {
											stock_in_set_responsible = "(收货方"+stockInSetPersonsList+")";
										}
									} else {
										stock_in_set_responsible = "("+stockInSetPersonsList+")";
									}
								%>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">运费状态：</td>
									<td style="width: 80%;"><span class="<%=stockInSetClass%>"><%=stock_in_set_responsible%><%=transportStockInSetKey.getStatusById(transport.get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET)) %></span></td>
								</tr>
								<%}%>
							</table>
						</fieldset>
					</td>
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>流程基础信息</legend>
							<table style="width:100%;border:0">
								<%	if (transportTagKey.NOTAG != transport.get("tag",transportTagKey.NOTAG)) { %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">制签流程：</td>
									<td style="width: 80%;"><%
										out.print("<span class='"+tagClass+"'>");
										String tag_responsible = "";
										String tagPersonsList = "";
DBRow[] tagPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 8);
if(tagPersons != null && tagPersons.length > 0){
	for(DBRow tempUserRow : tagPersons){
		tagPersonsList+= (tempUserRow.getString("employe_name")+"&nbsp;");
	}
}
										if (transport.get("tag_responsible", 0)==1) {
											if (tagPersonsList.length()>0) {
												tag_responsible = "(发货方:"+tagPersonsList+")";
											} else {
												tag_responsible = "(发货方"+tagPersonsList+")";
											}
										} else if (transport.get("tag_responsible", 0)==2) {
											if (tagPersonsList.length()>0) {
												tag_responsible = "(收货方:"+tagPersonsList+")";
											} else {
												tag_responsible = "(收货方"+tagPersonsList+")";
											}
										} else {
											tag_responsible = "("+tagPersonsList+")";
										}
										out.print(tag_responsible);
										if(transport.get("tag",transportTagKey.NOTAG) == transportTagKey.TAG)
										{
											out.print("制签中");
										}
										else
										{
											out.print(transportTagKey.getTransportTagById(transport.get("tag",transportTagKey.NOTAG)));
										}
										out.print("</span>");
									%></td>
								</tr>
								<%	}
									if (transportTagKey.NOTAG != transport.get("tag_third",transportTagKey.NOTAG)) { %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">第三方标签：</td>
									<td style="width: 80%;"><%
										out.print("<span class='"+tagClassThird+"'>");
										String tag_third_responsible = "";
										String tagPersonsThirdList = "";
										DBRow[] tagPersonsThird	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.THIRD_TAG);
										if(tagPersonsThird != null && tagPersonsThird.length > 0){
											for(DBRow tempUserRow : tagPersonsThird){
												tagPersonsThirdList += (tempUserRow.getString("employe_name")+"&nbsp;");
											}
										}
										if (transport.get("tag_third_responsible", 0)==1) {
											if (tagPersonsThirdList.length()>0) {
												tag_third_responsible = "(发货方:"+tagPersonsThirdList+")";
											} else {
												tag_third_responsible = "(发货方"+tagPersonsThirdList+")";
											}
											
										} else if (transport.get("tag_third_responsible", 0)==2) {
											if (tagPersonsThirdList.length()>0) {
												tag_third_responsible = "(收货方:"+tagPersonsThirdList+")";
											} else {
												tag_third_responsible = "(收货方"+tagPersonsThirdList+")";
											}
										} else {
											tag_third_responsible = "("+tagPersonsThirdList+")";
										}
										out.print(tag_third_responsible);
										
										if(transport.get("tag_third",transportTagKey.NOTAG) == transportTagKey.TAG)
										{
											out.print("制签中");
										}
										else
										{
											out.print(transportTagKey.getTransportTagById(transport.get("tag_third",transportTagKey.NOTAG)));
										}
										out.print("</span>");
									%></td>
								</tr>
								<%	}
									if (TransportProductFileKey.NOPRODUCTFILE != transport.get("product_file",transportProductFileKey.NOPRODUCTFILE)) { %><%
										String productFilePersonsList = "";
										DBRow[] productFilePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 10);
										if(productFilePersons != null && productFilePersons.length > 0){
											for(DBRow tempUserRow : productFilePersons){
												productFilePersonsList+=(tempUserRow.getString("employe_name")+"&nbsp;");
											}
										}
										String product_file_responsible = "";
										if (transport.get("product_file_responsible", 0)==1) {
											if (productFilePersonsList.length()>0) {
												product_file_responsible = "(发货方:"+productFilePersonsList+")";
											} else {
												product_file_responsible = "(发货方"+productFilePersonsList+")";
											}
											
										} else if (transport.get("product_file_responsible", 0)==2) {
											if (productFilePersonsList.length()>0) {
												product_file_responsible = "(收货方:"+productFilePersonsList+")";
											} else {
												product_file_responsible = "(收货方"+productFilePersonsList+")";
											}
										} else {
											product_file_responsible = "("+productFilePersonsList+")";
										}
									%>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">实物图片：</td>
									<td style="width: 80%;"><span class='<%=productFileClass%>'><%=product_file_responsible%><%=transportProductFileKey.getStatusById(transport.get("product_file",transportProductFileKey.NOPRODUCTFILE)) %></span></td>
								</tr>
								<%	}
									if (transportQualityInspectionKey.NO_NEED_QUALITY != transport.get("quality_inspection",transportQualityInspectionKey.NO_NEED_QUALITY)) {%><%
										String quality_inspection_responsible = "";
										String qualityInspectionPersonsList = "";
										DBRow[] qualityInspectionPersons = scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 11);
										if(qualityInspectionPersons != null && qualityInspectionPersons.length > 0){
											for(DBRow tempUserRow : qualityInspectionPersons){
												qualityInspectionPersonsList+=(tempUserRow.getString("employe_name")+"&nbsp;");
											}
										}
										if (transport.get("quality_inspection_responsible", 0)==1) {
											if (qualityInspectionPersonsList.length()>0) {
												quality_inspection_responsible = "(发货方:"+qualityInspectionPersonsList+")";
											} else {
												quality_inspection_responsible = "(发货方"+qualityInspectionPersonsList+")";
											}
										} else if (transport.get("quality_inspection_responsible", 0)==2) {
											if (qualityInspectionPersonsList.length()>0) {
												quality_inspection_responsible = "(收货方:"+qualityInspectionPersonsList+")";
											} else {
												quality_inspection_responsible = "(收货方"+qualityInspectionPersonsList+")";
											}
										} else {
											quality_inspection_responsible = "("+qualityInspectionPersonsList+")";
										}
									%>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">质检流程：</td>
									<td style="width: 80%;"><span class='<%=qualityInspectionClass%>'><%=quality_inspection_responsible%><%=transportQualityInspectionKey.getStatusById(transport.get("quality_inspection",transportQualityInspectionKey.NO_NEED_QUALITY)+"") %></span></td>
								</tr>
								<%	}
									if (certificateKey.NOCERTIFICATE != transport.get("certificate",certificateKey.NOCERTIFICATE)) {%><%
										String certificate_responsible = "";
										String certificatePersonsList = "";
										DBRow[] certificatePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 9);
										if(certificatePersons != null && certificatePersons.length > 0){
											for(DBRow tempUserRow : certificatePersons){
												certificatePersonsList += tempUserRow.getString("employe_name")+"&nbsp;";
											}
										}
	  							 		if (transport.get("certificate_responsible", 0)==1) {
	  							 			if (certificatePersonsList.length()>0) {
	  							 				certificate_responsible = "(发货方:"+certificatePersonsList+")";
	  							 			} else {
												certificate_responsible = "(发货方"+certificatePersonsList+")";
	  							 			}
										} else if (transport.get("certificate_responsible", 0)==2) {
											if (certificatePersonsList.length()>0) {
												certificate_responsible = "(收货方:"+certificatePersonsList+")";
											} else {
												certificate_responsible = "(收货方"+certificatePersonsList+")";
											}
										} else {
											certificate_responsible = "("+certificatePersonsList+")";
										}
									%>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">单证流程：</td>
									<td style="width: 80%;"><span class='<%=certificateClass%>'><%=certificate_responsible%><%=certificateKey.getStatusById(transport.get("certificate",certificateKey.NOCERTIFICATE)) %></span></td>
								</tr>
								<%	} %>
							</table>
						</fieldset>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
		<div id="transport_info">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr id="transport_basic_main">
						<td align="center" width="33%">
							<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">发货仓库：</td>
									<td style="width: 80%;" nowrap="nowrap"><%
										if(transport.get("purchase_id",0l)==0)
										{
											out.print(null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
											session.setAttribute("billOfLading",null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
										}
										else
										{
											out.print(supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name"));
										}
									%></td>
								</tr>
							</table>
						</td>
						<td align="center" width="33%">
							<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">运输公司：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_waybill_name") %></td>
								</tr>
							</table>
						</td>
						<td align="center" width="34%">
							<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">收货仓库：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):""%></td>
								</tr>
							</table>
						</td>
					</tr>
				</thead>
				<tbody>
					<tr id="transport_basic_detail" style="display:none;">
						<td>
						<table border="0" width="100%">
							<tr>
								<td width="30%" align="center" valign="top">
								<fieldset class="set">
									<legend title="提货仓库"><%
										if(transport.get("purchase_id",0l)==0)
										{
											out.print(null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
											session.setAttribute("billOfLading",null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
										}
										else
										{
											out.print(supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name"));
										}
									%></legend>
									<table style="width:100%;border:1">
										<tr>
											<td nowrap="nowrap" title="门牌号"><%=null!=transport?transport.getString("send_house_number"):"" %></td>
										</tr>
										<tr>
											<td title="街道"><%=null!=transport?transport.getString("send_street"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="城市"><%=null!=transport?transport.getString("send_city"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="邮编"><%=null!=transport?transport.getString("send_zip_code"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系人"><%=null!=transport?transport.getString("send_name"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系电话"><%=null!=transport?transport.getString("send_linkman_phone"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属省份"><%
												long ps_id3 = null!=transport?transport.get("send_pro_id",0L):0L ;
												DBRow psIdRow3 = productMgr.getDetailProvinceByProId(ps_id3);
											%><%=(psIdRow3 != null? psIdRow3.getString("pro_name"):transport.getString("address_state_send") ) %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属国家"><%
												long ccid3 = null!=transport?transport.get("send_ccid",0L):0L; 
												DBRow ccidRow3 = orderMgr.getDetailCountryCodeByCcid(ccid3);
											%><%= (ccidRow3 != null?ccidRow3.getString("c_country"):"" ) %></td>
										</tr>
									</table>
								</fieldset>
								</td>
								<td width="40%" align="center" valign="bottom">
									<% if (transport.get("transportby",0)==transportWayKey.AIRWAY) { %>
									<img alt="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" title="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" width="176" height="89" src="../imgs/air_type.png" border="0">
									<% } else if (transport.get("transportby",0)==transportWayKey.SEAWAY) { %>
									<img alt="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" title="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" width="176" height="89" src="../imgs/sea_type.png" border="0">
									<% } else if (transport.get("transportby",0)==transportWayKey.LANDWAY) { %>
									<img alt="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" title="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" width="176" height="89" src="../imgs/land_type.png" border="0">
									<% } else if (transport.get("transportby",0)==transportWayKey.EXPRESSWAY) { %>
									<img alt="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" title="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" width="176" height="89" src="../imgs/express_type.png" border="0">
									<% } %>
									<fieldset class="set">
										<table style="width:100%;border:0">
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">运输公司：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_waybill_name") %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">运输方式：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transportWayKey.getStatusById(transport.get("transportby",0)) %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">发货港：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_send_place") %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">运单号：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_waybill_number") %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">承运公司：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("carriers") %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">目的港：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_receive_place") %></td>
											</tr>
										</table>
									</fieldset>
								</td>
								<td width="30%" align="center" valign="top">
								<fieldset class="set">
									<legend title="收货仓库"><%=null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):""%></legend>
									<table style="width:100%;border:1">
										<tr>
											<td nowrap="nowrap" title="门牌号"><%=transport.getString("deliver_house_number") %></td>
										</tr>
										<tr>
											<td title="街道"><%=transport.getString("deliver_street") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="城市"><%=transport.getString("deliver_city") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="邮编"><%=transport.getString("deliver_zip_code") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系人"><%=transport.getString("transport_linkman")%></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系电话"><%=transport.getString("transport_linkman_phone")%></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属省份"><%
												long ps_id4 = null!=transport?transport.get("deliver_pro_id",0l):0L;
												DBRow psIdRow4 = productMgr.getDetailProvinceByProId(ps_id4);
											%><%=(psIdRow4 != null? psIdRow4.getString("pro_name"):transport.getString("address_state_deliver") ) %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属国家"><%
												long ccid4 = null!=transport?transport.get("deliver_ccid",0l):0L; 
												DBRow ccidRow4 = orderMgr.getDetailCountryCodeByCcid(ccid4);
											%><%= (ccidRow4 != null?ccidRow4.getString("c_country"):"" ) %></td>
										</tr>
									</table>
								</fieldset>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div id="tabbar" class="tabdown" style="height:15px;padding-top:0px;text-align:center;margin:0 auto;padding-left:90%; float:inherit ;cursor:pointer;" title="更多""></div>
	<% boolean hasData = false; %>
	<% if (transport.get("tag",transportTagKey.NOTAG)!= transportTagKey.NOTAG || tagIntThird != TransportTagKey.NOTAG || transport.get("product_file",transportProductFileKey.NOPRODUCTFILE) != transportProductFileKey.NOPRODUCTFILE || transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY) != TransportQualityInspectionKey.NO_NEED_QUALITY) { %>
	<br/>
	<div id="transportTabs">
		<ul>
			<%	if ( transport.get("tag",transportTagKey.NOTAG)!= transportTagKey.NOTAG && ( (transport.get("send_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_SEND==transport.get("tag_responsible",0))||(transport.get("receive_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_RECEIVE==transport.get("tag_responsible",0)) ) ) { hasData = true;%>
			<li ondblclick="opBasicTab2()"><a href="#transport_tag_make" title="内部标签:<%= transportTagKey.getTransportTagById(transport.get("tag",transportTagKey.NOTAG)) %>"><span class='<%=tagClass%>'>内部标签</span></a></li>
			<%	}%>
			<%	if(tagIntThird != TransportTagKey.NOTAG && ( (transport.get("send_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_SEND==transport.get("tag_third_responsible",0))||(transport.get("receive_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_RECEIVE==transport.get("tag_third_responsible",0)) ))
				{
				hasData = true;
			%>
			<li ondblclick="opBasicTab2()"><a href="#transport_tag_third" title="第三方标签:"><span class='<%=tagClassThird%>'>第三方标签</span></a></li>
			<%	}
				if (transport.get("product_file",transportProductFileKey.NOPRODUCTFILE) != transportProductFileKey.NOPRODUCTFILE && ( (transport.get("send_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_SEND==transport.get("product_file_responsible",0))||(transport.get("receive_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_RECEIVE==transport.get("product_file_responsible",0)) )) {
				hasData = true;
  			%>
			<li ondblclick="opBasicTab2()"><a href="#product_file" title="实物图片:<%=transportProductFileKey.getStatusById(productFileInt)%>"><span class='<%= productFileClass%>'>实物图片</span></a></li>
			<%	}%>
			<%	if(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY) != TransportQualityInspectionKey.NO_NEED_QUALITY && ( (transport.get("send_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_SEND==transport.get("quality_inspection_responsible",0))||(transport.get("receive_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_RECEIVE==transport.get("quality_inspection_responsible",0)) ))
				{
				hasData = true;
			%>
			<li ondblclick="opBasicTab2()"><a href="#quality_inspection" title="质检:<%=transportQualityInspectionKey.getStatusById(""+transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY)) %>"><span class="<%=qualityInspectionClass%>">质检信息</span></a></li>
			<%	}%>
		</ul>
		<% if (transport.get("tag",transportTagKey.NOTAG)!= transportTagKey.NOTAG && ( (transport.get("send_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_SEND==transport.get("tag_responsible",0))||(transport.get("receive_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_RECEIVE ==transport.get("tag_responsible",0))) ) { %>
		<div id="transport_tag_make" style="display:none">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="30px">
					<td>
						<input type="button" class="long-button-print" value="制作唛头" onclick="shippingMark()"/>
						<input type="button" class="long-button-print" value="制作标签" onclick="printLabel()"/>
						<input type="button" class="long-button" value="内部标签跟进" onclick="tag('<%=transport_id%>')"/>
						<input type="button" class="long-button" value="查看日志" onclick="showSingleLogs('<%=transport_id %>',8,'内部标签')"/> 
					</td>
				</tr>
			</table>
		</div>
		<% } %>
		<%	if (transport.get("tag_third",TransportTagKey.NOTAG)!= TransportTagKey.NOTAG && ( (transport.get("send_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_SEND==transport.get("tag_third_responsible",0))||(transport.get("receive_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_RECEIVE==transport.get("tag_third_responsible",0)) )) {%>
		<div id="transport_tag_third" style="display:none">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="30px">
					<td>
						<input type="button" class="long-button" value="第三方标签跟进" onclick="tagThird('<%=transport_id%>')"/>
						<input type="button" class="long-button" value="查看日志" onclick="showSingleLogs('<%=transport_id%>',14,'第三方标签')"/>
					</td>
				</tr>
			</table>
			<div id="tagTabs" style="margin-top:15px;">
				<ul><%
if(selectedList != null && selectedList.size() > 0)
{
	for(int index = 0 , count = selectedList.size() ; index < count ; index++ )
	{
		out.println("<li><a href=\"#transport_product_tag_"+index+"\">"+selectedList.get(index)+"</a></li>");
	}
}%></ul>
<%
	for(int index = 0,count = selectedList.size() ; index < count ; index++ )
	{
		List<DBRow> tempListRows = imageMap.get((index+1)+"");
%>
				<div id="transport_product_tag_<%= index%>">
					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
						<thead>
						<tr> 
							<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
							<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
							<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
						</tr>
						</thead>
						<tbody>
						<%	if(tempListRows != null && tempListRows.size() > 0){
								for(DBRow row : tempListRows){ %>
							<tr style="height: 25px;">
								<td><!-- 如果是图片文件那么就是要调用在线预览的图片 -->
								<%if(StringUtil.isPictureFile(row.getString("file_name"))){ %>
									<p><a href="javascript:void(0)" onclick="showPictrueOnline1('<%=FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE %>','<%=transport.get("transport_id",0l) %>','<%=row.getString("file_name") %>','<%=row.get("product_file_type",01) %>');"><%=row.getString("file_name") %></a></p>
								<%} else {%>
									<p><a href='<%= downLoadFileAction%>?file_name=<%=row.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_productr")%>'><%=row.getString("file_name") %></a></p>
								<%}%></td>
 		 			 			<td><%=null != productMgr.getDetailProductByPcid(row.get("pc_id",0))?productMgr.getDetailProductByPcid(row.get("pc_id",0)).getString("p_name"):""%></td>
 		 			 			<td><%if(transport.get("tag_third",0) != TransportTagKey.FINISH){ %><a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a><%} else {out.print("&nbsp;");}%></td>
 		 			 		</tr>
	   		 			<%		}%>
	   		 			<%	} else {%>
							<tr style="height: 25px;">
								<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
							</tr>
	   		 			<%	}%>
	   		 			</tbody>
					</table>
				</div>
<%
	}
%>
			</div>
		</div>
		<%	}
			if (transport.get("product_file",transportProductFileKey.NOPRODUCTFILE) != transportProductFileKey.NOPRODUCTFILE &&  ( (transport.get("send_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_SEND==transport.get("product_file_responsible",0))||(transport.get("receive_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_RECEIVE==transport.get("product_file_responsible",0)) )) {%>
		<div id="product_file" style="height:200px;display:none;min-width:1002px">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="30px">
					<td>
						<input type="button" value="实物图片跟进" class="long-button" onclick="productFileFollowUp();"/>
						<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',10,'实物图片')"/>
						<%	if(TransportProductFileKey.FINISH == transport.get("product_file",0)) {%><input name="button" type="button" class="long-long-button" value="实物图片完成-->需要" onClick="transportProductFileFinishToNeed()"/><%}%>
					</td>
				</tr>
			</table>
			<div id="content1" style="float:left;padding: 5px;display:block;">
				<div class="mediumGallery">
					<div class="nav">商品包装</div>
					<div class="images thumbs"></div>
				</div>
			</div>
			<div id="content2" style="float:left;padding: 5px;display:block;">
				<div class="mediumGallery">
					<div class="nav">商品本身</div>
					<div class="images thumbs"></div>
				</div>
			</div>
			<div id="content3" style="float:left;padding: 5px;display:block;">
				<div class="mediumGallery">
					<div class="nav">商品底贴</div>
					<div class="images thumbs"></div>
				</div>
			</div>
			<div id="content4" style="float:left;padding: 5px;display:block;">
				<div class="mediumGallery">
					<div class="nav">商品称重</div>
					<div class="images thumbs"></div>
				</div>
			</div>
			<div id="content5" style="float:left;padding: 5px;display:block;">
				<div class="mediumGallery">
					<div class="nav">商品标签</div>
					<div class="images thumbs"></div>
				</div>
			</div>
		</div>
		<%	}
			if(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY) != TransportQualityInspectionKey.NO_NEED_QUALITY && ((transport.get("send_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_SEND==transport.get("quality_inspection_responsible",0))||(transport.get("receive_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_RECEIVE==transport.get("quality_inspection_responsible",0)) ) ) {	%>
		<div id="quality_inspection" style="display:none">
			<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
				<tr height="30px">
					<td>
						<input type="button"  value="质检跟进" class="short-button" onclick='quality_inspectionKey(<%=transport_id%>)'/>
						<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',11,'质检流程')"/>
					</td>
				</tr>
			</table>
			<fieldset style="width:90%;border:2px #cccccc solid;padding:2px;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<legend style="font-size:15px;font-weight:normal;color:#999999;">上传质检文件</legend>
					<%if(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY)!= TransportQualityInspectionKey.FINISH) {%>
					<table width="90%" border="0">
						<tr>
							<td height="25" align="right"  style="width:120px;"  class="STYLE2" nowrap="nowrap">上传文件:</td>
							<td>&nbsp;<input type="button"  class="long-button" onclick="uploadFile('qualityInspectionForm');" value="选择文件" />
							<input type="button" class="long-button" onclick="onlineScanner('qualityInspectionForm');" value="在线获取" /></td>
						</tr>
					</table>
					<%}%>
					<form  id="qualityInspectionForm"   method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/transport/TransportQualityInspectionAction.action" %>'>
						<input type="hidden" name="backurl" value="<%=backurl %>" />
						<input type="hidden" name="sn" value="T_quality_inspection"/>
						<input type="hidden" name="quality_inspection" value='<%= transport.get("quality_inspection",transportQualityInspectionKey.NO_NEED_QUALITY) %>' />
		 				<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.TRANSPORT_QUALITYINSPECTION %>" />
		 				<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_transport")%>" />
					 	<input type="hidden" name="file_names" />
					 	<input type="hidden" name="cmd" value="submit"/>	<!-- 表示页面提交 -->
						<input type="hidden" name="transport_id" value="<%=transport_id %>"/>
					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable" style="margin:10px">
						<thead>
						<tr> 
							<th width="100px" style="vertical-align: center;text-align: center;" class="right-title">上传人</th>
							<th width="100px" style="vertical-align: center;text-align: center;" class="right-title">上传时间</th>
							<th width="60%" style="vertical-align: center;text-align: center;" class="right-title">文件</th>
							<%if(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY)!= TransportQualityInspectionKey.FINISH) {%>
							<th width="100px" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
							<%}%>
						</tr>
						</thead>
						<tbody>
		     <%
							DBRow[] fileRows = transportMgrZr.getFileByFilwWidthIdAndFileType("file",transport_id,FileWithTypeKey.TRANSPORT_QUALITYINSPECTION);
							if(fileRows != null && fileRows.length > 0 ){
								for(DBRow tempRow  : fileRows){
		     %>
									<tr style="height: 25px;">
										<td width="100px" align="right">
											<%=null == adminMgrLL.getAdminById(tempRow.getString("upload_adid"))?"":adminMgrLL.getAdminById(tempRow.getString("upload_adid")).getString("employe_name") %>
										</td>
										<td width="100px;" align="center">
			<%								if(!"".equals(tempRow.getString("upload_time"))){
												out.print(tDate.getFormateTime(tempRow.getString("upload_time")));
											}
			%>
										</td>
										<td width="60%" align="left">
										   <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	                     <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
		 			 	                     <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
		 			 	 	                 <%if(StringUtil.isPictureFile(tempRow.getString("file_name"))){ %>
				 			 	             <p>
				 			 	 	            <a href="javascript:void(0)" onclick="showPictrueOnlinef('<%=FileWithTypeKey.TRANSPORT_QUALITYINSPECTION %>','<%=transport.get("transport_id",0l) %>','<%=tempRow.getString("file_name") %>','<%=tempRow.get("product_file_type",01) %>');"><%=tempRow.getString("file_name") %></a>
				 			 	             </p>
			 			 	                 <%} else if(StringUtil.isOfficeFile(tempRow.getString("file_name"))){%>
			 			 	 		          <p>
			 			 	 			          <a href="javascript:void(0)"  file_id='<%=tempRow.get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("file_path_transport")%>')" file_is_convert='<%=tempRow.get("file_is_convert",0) %>'><%=tempRow.getString("file_name") %></a>
			 			 	 		          </p>
			 			 	                <%}else{ %>
 		 			 	 	  		           <p><a href='<%=downLoadFileAction%>?file_name=<%=tempRow.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_transport")%>'><%=tempRow.getString("file_name") %></a></p> 
 		 			 	 	                <%} %>
										</td>
										<%if(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY)!= TransportQualityInspectionKey.FINISH) {%>
										<td>
											<!-- 判断是不是可以删除  -->
											<a  href="javascript:deleteFileTable('<%= tempRow.get("file_id",0l)%>')">删除</a>
											
										</td>
										<%}%>
		     							
		     	</tr>
		     <%
								}
							}
			%>
					</tbody>
					</table>
					</form>
			</fieldset>
		</div>
		<%	}%>
	</div>
	<div id="tabbar2" class="tabdown" style="height:15px;padding-top:0px;text-align:center;margin:0 auto;padding-left:90%; float:inherit ;cursor:pointer;" title="更多""></div>
	<% } %>
	<script type="text/javascript">
	if("<%=hasData%>"=="false") {
		$("#transportTabs").hide();
		$("#tabbar2").hide();
	}
	</script>
	<%= (hasData?"<br/>":"") %>
	<div id="detail" align="left" style="padding:0px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr><td><table id="gridtest"></table><div id="pager2"></div></td></tr>
		</table>
		<br/>
	</div>
</div>
<script type="text/javascript">
function print()
{
	visionariPrinter.PRINT_INIT("转运单");
	visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
	visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../transport/print_transport_order_detail.html?transport_id=<%=transport_id%>");
	visionariPrinter.PREVIEW();
}
if (typeof(window.tabbar2show) == "undefined") window.tabbar2show = false;
function opBasicTab()
{
	if ($("#basic_info_detail").css('display')=='none')
	{
		$("#basic_info_main").hide();
		$("#basic_info_detail").show();
		$("#transport_basic_main").hide();
		$("#transport_basic_detail").show();
		$("#tabbar").attr("class", "tabup");
	} 
	else 
	{
		$("#basic_info_main").show();
		$("#basic_info_detail").hide();
		$("#transport_basic_main").show();
		$("#transport_basic_detail").hide();
		$("#tabbar").attr("class", "tabdown");
	}
}
$('#tabbar').bind("click", opBasicTab);
function opBasicTab2()
{
	if (!window.tabbar2show) {
		$("#transport_tag_make").show();
		$("#transport_tag_third").show();
		$("#product_file").show();
		$("#quality_inspection").show();
		$('#tabbar2').attr("class", "tabup");
		window.tabbar2show=true;
	} else {
		$("#transport_tag_make").hide();
		$("#transport_tag_third").hide();
		$("#product_file").hide();
		$("#quality_inspection").hide();
		$('#tabbar2').attr("class", "tabdown");
		window.tabbar2show=false;
	}
}
$('#tabbar2').bind("click",opBasicTab2);

$("#transportMainTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tagTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tabsCertificate").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: {expires: 30000},
	load: function(event, ui) {onLoadInitZebraTable();}
});
//var lastsel;
function buttonFormat( cellvalue, options, rowObject )
{
	var product_id = rowObject['transport_pc_id'];
	
	return "<%if (transport.get("product_file",transportProductFileKey.NOPRODUCTFILE) != transportProductFileKey.NOPRODUCTFILE &&( (transport.get("send_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_SEND==transport.get("product_file_responsible",0))||(transport.get("receive_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_RECEIVE==transport.get("product_file_responsible",0)) )) {%><button onclick='addProductPicture(\""+product_id+"\")' title='上传图片'>上传图片</button><br/><%} if (transport.get("tag_third",TransportTagKey.NOTAG)!= TransportTagKey.NOTAG && ( (transport.get("send_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_SEND==transport.get("tag_third_responsible",0))||(transport.get("receive_psid",0l)==psId && transportRoleKey.TRANSPORT_ROLE_RECEIVE==transport.get("tag_third_responsible",0)) )) {%><button onclick='addProductTagTypesFile(\""+product_id+"\")' title='上传标签'>上传标签</button><%}%>&nbsp;";
}
function buttonUnFormat( cellvalue, options, cell)
{  
    return "&nbsp;";  
}
function showPic(transport_id, product_id) {
	$("#content1 .images").empty();
	$("#content2 .images").empty();
	$("#content3 .images").empty();
	$("#content4 .images").empty();
	$("#content5 .images").empty();
	var para ="transport_id="+transport_id+"&product_id="+product_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportPicsJson.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		async:false,
		data:para,
		beforeSend:function(request)
		{
		},
		error: function()
		{
			alert("获取图片信息失败，请重试！");
		},
		success: function(data)
		{
			$.each(data,function(idx, item){
				//
				if (item.product_file_type=="1") {
					$("#content1 .images").append("<div style=\"display: table-cell;\"><img width=\"50\" height=\"33\" alt=\"\" src=\"<%=ConfigBean.getStringValue("systenFolder")%>upload/product/"+item.file_name+"\" style=\"display: inline;\" onclick=\"showPictrueOnlineWithDel('7','"+item.file_with_id+"','"+item.file_name+"','"+item.product_file_type+"','"+item.pc_id+"','"+item.p_name+"');\"></div>");
				} else if (item.product_file_type=="2") {
					$("#content2 .images").append("<div style=\"display: table-cell;\"><img width=\"50\" height=\"33\" alt=\"\" src=\"<%=ConfigBean.getStringValue("systenFolder")%>upload/product/"+item.file_name+"\" style=\"display: inline;\" onclick=\"showPictrueOnlineWithDel('7','"+item.file_with_id+"','"+item.file_name+"','"+item.product_file_type+"','"+item.pc_id+"','"+item.p_name+"');\"></div>");
				} else if (item.product_file_type=="3") {
					$("#content3 .images").append("<div style=\"display: table-cell;\"><img width=\"50\" height=\"33\" alt=\"\" src=\"<%=ConfigBean.getStringValue("systenFolder")%>upload/product/"+item.file_name+"\" style=\"display: inline;\" onclick=\"showPictrueOnlineWithDel('7','"+item.file_with_id+"','"+item.file_name+"','"+item.product_file_type+"','"+item.pc_id+"','"+item.p_name+"');\"></div>");
				} else if (item.product_file_type=="4") {
					$("#content4 .images").append("<div style=\"display: table-cell;\"><img width=\"50\" height=\"33\" alt=\"\" src=\"<%=ConfigBean.getStringValue("systenFolder")%>upload/product/"+item.file_name+"\" style=\"display: inline;\" onclick=\"showPictrueOnlineWithDel('7','"+item.file_with_id+"','"+item.file_name+"','"+item.product_file_type+"','"+item.pc_id+"','"+item.p_name+"');\"></div>");
				} else if (item.product_file_type=="5") {
					$("#content5 .images").append("<div style=\"display: table-cell;\"><img width=\"50\" height=\"33\" alt=\"\" src=\"<%=ConfigBean.getStringValue("systenFolder")%>upload/product/"+item.file_name+"\" style=\"display: inline;\" onclick=\"showPictrueOnlineWithDel('7','"+item.file_with_id+"','"+item.file_name+"','"+item.product_file_type+"','"+item.pc_id+"','"+item.p_name+"');\"></div>");
				}
			});
		}
	});
}
var rownum = 0;
var mygrid = $("#gridtest").jqGrid({
	sortable:true,
	url:'./dataTransportOrderDetail.jsp',//dataDeliveryOrderDetails.html
	datatype: "json",
	width:parseInt(document.body.clientWidth-2),
	height:255,
	autowidth: false,
	shrinkToFit: true,
	postData:{transport_id:<%=transport_id%>},
	jsonReader:{
		id: 'transport_detail_id',
		repeatitems: false
	},
	colNames:['transport_detail_id','商品名','商品条码','货物数','备件数','计划装箱数','V(cm³),W(Kg),运费','所在箱号','商品序列号','使用CLP','使用BLP','transport_id','transport_pc_id','clp_type_id','blp_type_id','批次','操作'], 
	colModel:[ 
		{name:'transport_detail_id',index:'transport_detail_id',hidden:true,sortable:false},
		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
		{name:'p_code',width:40,index:'p_code',align:'left'},
		{name:'transport_delivery_count',width:40,index:'transport_delivery_count',editrules:{required:true,number:true,minValue:0,integer:true},editable:<%=edit%>,sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},
		{name:'transport_backup_count',width:40,index:'transport_backup_count',editrules:{required:true,number:true,minValue:0,integer:true},editable:<%=edit%>,sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},
		{name:'transport_count',width:40,index:'transport_count',editrules:{required:true,number:true,minValue:1},sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}},				   	
		{name:'volume_weight_freight',width:70,index:'volume_weight_freight',align:'left'},
		{name:'transport_box',width:40,index:'transport_box',editable:<%=edit%>},
		{name:'transport_product_serial_number',width:50,index:'transport_product_serial_number',editable:<%=edit%>},
		{name:'clp_type',width:30,index:'clp_type',editable:<%=edit%>,edittype:'select'},
		{name:'blp_type',width:30,index:'blp_type',editable:<%=edit%>,edittype:'select'},
		{name:'transport_id',index:'transport_id',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=transport_id%>},hidden:true,sortable:false},
		{name:'transport_pc_id',index:'transport_pc_id',hidden:true,sortable:false},
		{name:'clp_type_id',index:'clp_type_id',hidden:true,sortable:false},
		{name:'blp_type_id',index:'blp_type_id',hidden:true,sortable:false},
		{name:'lot_number',width:40,index:'lot_number',editable:<%=edit%>,editrules:{required:true},hidden:false,sortable:false},
		{name:'button',width:40,index:'button',align:'left',sortable:false,title:false,formatter:buttonFormat, unformat:buttonUnFormat},
	],
	rowNum:-1,//-1显示全部
	pgtext:false,
	pgbuttons:false,
	mtype: "GET",
	gridview: true,
	rownumbers:true,
	pager:'#pager2',
	sortname: 'transport_detail_id',
	viewrecords: true,
	sortorder: "asc",
	cellEdit: true,
	cellsubmit: 'remote',
	multiselect: false,
	onSelectCell:function(id,name)
	{
		if(name=="button")
		{
			//var product_id = jQuery("#gridtest").jqGrid('getCell',id,'transport_pc_id');
			//addProductPicture(product_id);
		}
		
		format(id);
	},
	formatCell:function(id,name,val,iRow,iCol)
	{
		format(id);
	},
   	afterEditCell:function(id,name,val,iRow,iCol)
   	{ 
   		if(name=='p_name')
   		{
   			autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
   		}
   		select_iRow = iRow;
   		select_iCol = iCol;
    },
   	afterSaveCell:function(rowid,name,val,iRow,iCol)
 	{
   		if(name=='p_name'||name=='transport_delivery_count'||name=='transport_backup_count')
   		{
   			ajaxModProductName(jQuery("#gridtest"),rowid);
   			getTransportSumVWP(<%=transport_id%>);
   	  	}
   	  	if(name=='clp_type'||name=='blp_type')
   	  	{
   	  		ajaxModProductName(jQuery("#gridtest"),rowid);
   	  		format(rowid);
		}
   	},
   	beforeSelectRow:function(id, e)
   	{
   		if (id!= rownum) {
   			rownum = id;
   			var product_id = jQuery("#gridtest").jqGrid('getCell',id,'transport_pc_id');
   			showPic(<%=transport_id%>,product_id);
   		}
   		return true;
   	},
   	cellurl:'#',
   	errorCell:function(serverresponse, status)
   	{
   		alert(errorMessage(serverresponse.responseText));
   	},
   	editurl:'#',
   	caption:''
});
jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}},{caption:"www",title:"自定义显示字段"});
$("#gview_gridtest a.ui-jqgrid-titlebar-close").attr("style","display:none");
//mygrid.setCaption("wwww");
$("#transportTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
	,show: function(event, ui){
		//alert(ui.index);
	}
});
function beforeShowAdd(formid)
{
	
}
function beforeShowEdit(formid)
{

}
function afterShowAdd(formid)
{
	autoComplete($("#p_name"));
}
function afterShowEdit(formid)
{
	autoComplete($("#p_name"));
}

function errorFormat(serverresponse,status)
{
	return errorMessage(serverresponse.responseText);
}
function refreshWindow(){
	window.location.reload();
}
function ajaxModProductName(obj,rowid,col)
{
	var unit_name;//单字段修改商品名时更换单位专用
	var para ="transport_detail_id="+rowid;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportDetailJSON.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		async:false,
		data:para,
		beforeSend:function(request){
		},
		
		error: function(){
			alert("提交失败，请重试！");
		},
		
		success: function(data)
		{
			obj.setRowData(rowid,data);
		}
	});
}
function getTransportSumVWP(transport_id)
{
$.ajax({
	url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportSumVWPJson.action',
	type: 'post',
	dataType: 'json',
	timeout: 60000,
	cache:false,
	data:{transport_id:transport_id},
	
	beforeSend:function(request){
	},
	error: function(e){
		alert(e);
		alert("提交失败，请重试！");
	},
	success: function(date)
	{
		//mygrid.setCaption("总体积:"+date.volume+" cm³ 总货款:"+date.send_price+" RMB 总重量:"+date.weight+" Kg");
		mygrid.setCaption("总体积："+date.volume+" cm³，总重量："+date.weight+" Kg");
	}
});
}
function showPictrueOnline0(fileWithType,fileWithId,currentName)
{
	var obj = {
		file_with_type:fileWithType,
		file_with_id:fileWithId,
		current_name:currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%=ConfigBean.getStringValue("systenFolder")%>upload/transport'
	}
	if (window.top && window.top.openPictureOnlineShow) {
		window.top.openPictureOnlineShow(obj);
	} else {
		openArtPictureOnlineShow(obj,'<%=ConfigBean.getStringValue("systenFolder")%>');
	}
}
function showPictrueOnline(fileWithType,fileWithId,currentName,product_file_type)
{
    var obj = {
		file_with_type:fileWithType,
		file_with_id:fileWithId,
		current_name:currentName,
		product_file_type:product_file_type,
		cmd:"multiFile",
		table:'product_file',
		base_path:'<%=ConfigBean.getStringValue("systenFolder")%>upload/product'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%=ConfigBean.getStringValue("systenFolder")%>');
	}
}

function showPictrueOnlineWithDel(fileWithType,fileWithId,currentName,product_file_type,p_id,p_name)
{
	var obj = {
		file_with_type:fileWithType,
		file_with_id:fileWithId,
		current_name:currentName,
		product_file_type:product_file_type,
		cmd:"multiFile",
		table:'product_file',
		edit: <%=productFileInt==TransportProductFileKey.FINISH%>,
		base_path:'<%=ConfigBean.getStringValue("systenFolder")%>upload/product'
	}
	var param = jQuery.param(obj);
	var uri = "<%= ConfigBean.getStringValue("systenFolder")%>administrator/transport/picture_online_show_with_del.html?" +param;
	var title = "["+p_name+"]";
	switch (product_file_type)
	{
		case "1":
			title += "商品包装";
			break;
		case "2":
			title += "商品本身";
			break;
		case "3":
			title += "商品底贴";
			break;
		case "4":
			title += "商品称重";
			break;
		case "5":
			title += "商品标签";
			break;
	}
	$.artDialog.open(uri , {title: title,width:'1100px',height:'600px', lock: true,opacity: 0.3,close:function(){showPic(<%=transport_id%>,p_id);},fixed: true});
}
function onlineScanner(target)
{
	var _target = "";
	if(target == "uploadCertificateImageForm"){
		_target = "uploadCertificateImageForm";
	}else if(target == "qualityInspectionForm" ){
		_target = "qualityInspectionForm";
	}
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target="+_target; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function format(id)
{
	var product_id = jQuery("#gridtest").jqGrid('getCell',id,'transport_pc_id');
	var clp_type_id = jQuery("#gridtest").jqGrid('getCell',id,'clp_type_id');
	var serial_number = jQuery("#gridtest").jqGrid('getCell',id,'transport_product_serial_number');
	if(serial_number.length>0) {
		$("#gridtest").jqGrid('setColProp','blp_type',{editable:false});
		$("#gridtest").jqGrid('setColProp','clp_type',{editable:false});
	} else {
		$("#gridtest").jqGrid('setColProp','clp_type',{editType:"select",editoptions:{value:getCLPSelect(product_id)}});
		if (clp_type_id!=0) {
			$("#gridtest").jqGrid('setColProp','blp_type',{editable:false});
		} else {
			$("#gridtest").jqGrid('setColProp','blp_type',{editable:true,editType:"select",editoptions:{value:getBLPSelect(product_id,01)}});
		}
	}
	
}
function getCLPSelect(pc_id)
{
}
function getBLPSelect(pc_id,receive_id)
{
}
function addProductPicture(pc_id)
{
	 //添加商品图片
	var transport_id = '<%=transport_id%>';
	var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_product_picture_up.html?transport_id="+transport_id+"&pc_id="+pc_id;
	$.artDialog.open(uri , {title: "商品范例上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,close:function(){window.location.reload();},fixed: true});
}
function addProductTagTypesFile(pc_id)
{
	 //添加商品标签
	var transport_id = '<%=transport_id%>';
	var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_product_tag_file.html?transport_id="+transport_id+"&pc_id="+pc_id;
	$.artDialog.open(uri , {title: "第三方标签上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
}
function downloadTransportOrder(transport_id)
{
	var para = "transport_id="+transport_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/downTransportOrder.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:para,
		beforeSend:function(request){
		},
		error: function(e){
			alert(e);
			alert("提交失败，请重试！");
		},
		success: function(data){
			if(data["canexport"]=="true")
			{
				document.download_form.action=data["fileurl"];
				document.download_form.submit();
			}
			else
			{
				alert("无法下载！");
			}
		}
	});
}
function shippingMark()
{
	var purchaseId = <%=transport.get("purchase_id",01)%>;
	//如果不是0就是交货型转运单
	if(purchaseId==0)
	{
		var transport_id=<%=transport_id%>;
		var awb="<%=transport.getString("transport_waybill_number")%>";
		var awbName="<%=transport.getString("transport_waybill_name")%>";
		var menpai="<%=transport.getString("deliver_house_number")%>";
		var jiedao="<%=transport.getString("deliver_street") %>";
		var chengshi="<%=transport.getString("deliver_city") %>";
		var youbian="<%=transport.getString("deliver_zip_code") %>";
		var shengfen="<%=ps_id4%>";
		var guojia="<%=ccid4%>";
		var lianxiren="<%=transport.getString("transport_linkman")%>";
		var dianhua="<%=transport.getString("transport_linkman_phone")%>";
		var receive_psid = "<%=transport.get("receive_psid",0l)%>";
		var uri = "../lable_template/print_shpping_marjsp.html?awbName="+awbName+"&dianhua="+dianhua+"&lianxiren="+lianxiren+"&guojia="+guojia+"&shengfen="+shengfen+"&youbian="+youbian+"&chengshi="+chengshi+"&jiedao="+jiedao+"&menpai="+menpai+"&transport_id="+transport_id+"&awb="+awb+"&receive_psid="+receive_psid; 
		$.artDialog.open(uri , {title: '唛头标签打印',width:'600px',height:'420px', lock: true,opacity: 0.3,fixed: true});
	} else {
		var transport_id=<%=transport_id%>;
		var awb="<%=transport.getString("transport_waybill_number")%>";
		var awbName="<%=transport.getString("transport_waybill_name")%>";
		var menpai="<%=transport.getString("deliver_house_number")%>";
		var jiedao="<%=transport.getString("deliver_street") %>";
		var chengshi="<%=transport.getString("deliver_city") %>";
		var youbian="<%=transport.getString("deliver_zip_code") %>";
		var shengfen="<%=ps_id4%>";
		var guojia="<%=ccid4%>";
		var lianxiren="<%=transport.getString("transport_linkman")%>";
		var dianhua="<%=transport.getString("transport_linkman_phone")%>";
		var receive_psid = "<%=transport.get("receive_psid",0l)%>";
		var uri = "../lable_template/print_shpping_marjsp.html?purchase_id="+purchaseId+"&awbName="+awbName+"&dianhua="+dianhua+"&lianxiren="+lianxiren+"&guojia="+guojia+"&shengfen="+shengfen+"&youbian="+youbian+"&chengshi="+chengshi+"&jiedao="+jiedao+"&menpai="+menpai+"&transport_id="+transport_id+"&awb="+awb+"&receive_psid="+receive_psid;
		$.artDialog.open(uri , {title: '唛头标签打印',width:'600px',height:'420px', lock: true,opacity: 0.3,fixed: true});
	}
}
function printLabel()
{
	//如果不是0就是交货型转运单
	var purchaseId = <%=transport.get("purchase_id",0l)%>;
	<%if(transport.get("purchase_id",0l)==0) {%>
		var supplierSid =<%=transport.get("send_psid",0l)%>;
		var transport_id=<%=transport_id%>;
		var warehouse="<%=null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):""%>";
		var uri = "../lable_template/made_tranoport_internal_label.html?warehouse="+warehouse+"&transport_id="+transport_id+"&purchase_id="+purchaseId+"&supplierName="+""+"&supplierSid="+supplierSid;
		$.artDialog.open(uri , {title: '转运单内部标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	<%} else {%>
		var supplierName="<%=supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name")%>";
		var supplierSid=<%=transport.get("send_psid",0l)%>;
		var transport_id=<%=transport_id%>;
		var uri = "../lable_template/made_tranoport_label.html?transport_id="+transport_id+"&purchase_id="+purchaseId+"&supplierName="+supplierName+"&supplierSid="+supplierSid; 
		$.artDialog.open(uri , {title: '转运单交货型标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	<%}%>
}
function tag(transport_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_tag.html?transport_id="+transport_id;
	$.artDialog.open(uri,{title: "跟进内部标签["+transport_id+"]",width:'570px',height:'290px', lock: true,opacity: 0.3,fixed: true});
}
function showSingleLogs(transport_id,transport_type,title)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_show_single_log.html?transport_id="+<%= transport_id%>+"&transport_type="+transport_type;
	$.artDialog.open(uri , {title: title+"["+<%= transport_id%>+"]",width:'870px',height:'470px', lock: true,opacity: 0.3,fixed: true});
}
function tagThird(transport_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_third_tag.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "跟进第三方标签["+transport_id+"]",width:'570px',height:'290px', lock: true,opacity: 0.3,fixed: true});
}
function productFileFollowUp()
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_file_follow_up.html?transport_id="+<%= transport_id%>;
	$.artDialog.open(uri , {title: "实物图片跟进["+<%= transport_id%>+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
function quality_inspectionKey(transport_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_quality_inspection.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "质检流程["+transport_id+"]",width:'570px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:8,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true, close:function(){
		//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
		this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
	}});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,_target)
{
    var targetNode = $("#"+_target);
    $("input[name='file_names']",targetNode).val(fileNames);
    if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
        $("input[name='file_names']").val(fileNames);
        var myform = $("#"+_target);
        var file_with_class = $("#file_with_class").val();
			$("input[name='backurl']",targetNode).val("<%= backurl%>" + "&file_with_class="+file_with_class);
		  	myform.submit();
	}
}
function showPictrueOnlinef(fileWithType,fileWithId , currentName)
{
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_transport")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	} else {
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}
function deleteFileTable(_file_id)
{
	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:'file',file_id:_file_id,folder:'',pk:'file_id'},
		success:function(data){
			if(data && data.flag === "success"){
				window.location.reload();
			} else {
				showMessage("系统错误,请稍后重试","error");
			}
		},
		error:function(){
			showMessage("系统错误,请稍后重试","error");
		}
	});
}
function updateTransportAllProdures()
{
	//var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_wayout_update.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>";
	
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_flow_for_quality_update.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '修改各流程信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function transportProductFileFinishToNeed()
{
	if(confirm("您确定要修改实物图片流程从完成到需要阶段吗？"))
	{
		var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_product_file_finish_to_need.html?transport_id=<%=transport_id%>";
		$.artDialog.open(uri, {title: '修改实物图片完成-->需要['+<%=transport_id%>+']',width:'570px',height:'280px', lock: true,opacity: 0.3});
	}
}
$(document).ready(function(){
	$(function(){
		$(window).resize(function(){
			jQuery("#gridtest").setGridWidth(document.body.clientWidth-2);
		});
	});
	//$("#product_file div.mediumGallery div.images div").attr();
});

getTransportSumVWP(<%=transport_id%>);
</script>
<form name="download_form" method="post"></form>
</body>
</html>