<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.ll.Utils,java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.ClearanceKey"%>
<%@page import="com.cwc.app.key.DeclarationKey"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="com.cwc.app.key.QualityInspectionKey"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@page import="com.cwc.app.key.ProductStorageTypeKey"%>
<%@page import="com.cwc.app.key.TransportRoleKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/> 
<jsp:useBean id="transportCertificateKey" class="com.cwc.app.key.TransportCertificateKey"/> 
<jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey"/> 
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<jsp:useBean id="transportQualityInspectionKeyKey" class="com.cwc.app.key.TransportQualityInspectionKey"/> 
<%@ include file="../../include.jsp"%>
<%
	AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean( request.getSession(true) );
long loginPsId = adminLoggerBean.getPs_id();

String transport_id = StringUtil.getString(request,"transport_id").equals("")?"0":StringUtil.getString(request,"transport_id");
String tgroup = "";
String qgroup = "";
DBRow row = transportMgrLL.getTransportById(transport_id);

//发货方信息
long send_psid = row.get("send_psid", 0l);
String send_ps_name = "";
int fromPsType = row.get("from_ps_type",0);
//如果是供应商的Type那么就需要去查询供应商的名称
if(fromPsType == ProductStorageTypeKey.SupplierWarehouse)
{
	DBRow temp = supplierMgrTJH.getDetailSupplier(send_psid);
	if(temp != null)
	{
		send_ps_name = temp.getString("sup_name");
	}
}
else
{
	DBRow storageCatalog = catalogMgr.getDetailProductStorageCatalogById(send_psid);
	if(storageCatalog != null)
	{
		send_ps_name = storageCatalog.getString("title");
	}
}

//收货方信息
long receive_psid = row.get("receive_psid", 0l);
String receive_ps_name = "";
//收货仓库
DBRow receive_ps = catalogMgr.getDetailProductStorageCatalogById(receive_psid);
if(receive_ps != null)
{
	receive_ps_name = receive_ps.getString("title");
}

//判断登录人 所属角色（发货方还是收货方）
int loginRole = 0;
if(loginPsId == send_psid)
{
	loginRole = TransportRoleKey.TRANSPORT_ROLE_SEND;//发货人
}
else if(loginPsId == receive_psid)
{
	loginRole = TransportRoleKey.TRANSPORT_ROLE_RECEIVE;//收货人
}
else
{
	loginRole = 0;//都不是
}

//如果发货地址与提货地址相同，清关和报关默认不需要
int delieverCcid = row.get("deliver_ccid",0);
int sendCcid = row.get("send_ccid",0);
boolean ccidIsSame = delieverCcid==sendCcid?true:false;
String transport_waybill_number = row.getValue("transport_waybill_number")==null?"0":row.getValue("transport_waybill_number").toString();
String transport_waybill_name = row.getValue("transport_waybill_name")==null?"":row.getValue("transport_waybill_name").toString();
String transportby = row.getValue("transportby")==null?"0":row.getValue("transportby").toString();

String declaration = row.getValue("declaration")==null?"0":row.getValue("declaration").toString();
//报关负责人
int declaration_responsible =  row.get("declaration_responsible", 0);

String clearance = row.getValue("clearance")==null?"0":row.getValue("clearance").toString();
//清关负责人
int clearance_responsible =  row.get("clearance_responsible", 0);

//质检负责方
int quality_inspection_responsible = row.get("quality_inspection_responsible",0);
//标签负责方
int tag_responsible = row.get("tag_responsible", 0);
//第三方标签负责方
int tag_third_responsible = row.get("tag_third_responsible", 0);
//实物图片负责方
int product_file_responsible = row.get("product_file_responsible", 0);


String drawback = row.getValue("drawback")==null?"1":row.getValue("drawback").toString();
String invoice = row.getValue("invoice")==null?"1":row.getValue("invoice").toString();
String transport_send_country = row.getValue("transport_send_country")==null?"1":row.getValue("transport_send_country").toString();
String transport_receive_country = row.getValue("transport_receive_country")==null?"1":row.getValue("transport_receive_country").toString();
String transport_send_place = row.getValue("transport_send_place")==null?"":row.getValue("transport_send_place").toString();
String transport_receive_place = row.getValue("transport_receive_place")==null?"":row.getValue("transport_receive_place").toString();
String transport_linkman = row.getValue("transport_linkman")==null?"":row.getValue("transport_linkman").toString();
String transport_linkman_phone = row.getValue("transport_linkman_phone")==null?"":row.getValue("transport_linkman_phone").toString();
String transport_address = row.getValue("transport_address")==null?"":row.getValue("transport_address").toString();
String remark = row.getValue("remark")==null?"":row.getValue("remark").toString();
String transport_receive_date = row.getValue("transport_receive_date")==null?"":row.getValue("transport_receive_date").toString();
String fr_id = row.getValue("fr_id")==null?"":row.getValue("fr_id").toString();
int transport_status = row.get("transport_status",0);
int finished = StringUtil.getInt(request,"finished",0);
int product_file = row.get("product_file",0);
int tag = row.get("tag",0);
int tag_third = row.get("tag_third",0);
int stock_in_set = row.get("stock_in_set",0);
//运费负责人
int stock_in_set_responsible = row.get("stock_in_set_responsible",0);
int certificate = row.get("certificate",0);
//单证负责方
int certificate_responsible = row.get("certificate_responsible",0);
int quality_inspection = row.get("quality_inspection",0);



//查询各流程任务的负责人
String adminUserIdsTransport		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 4);
String adminUserNamesTransport		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 4);
String adminUserIdsDeclaration		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 7);
String adminUserNamesDeclaration	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 7);
String adminUserIdsClearance		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 6);
String adminUserNamesClearance		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 6);
String adminUserIdsProductFile		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 10);
String adminUserNamesProductFile	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 10);
String adminUserIdsTag				= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 8);
String adminUserNamesTag			= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 8);
String adminUserIdsStockInSet		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 5);
String adminUserNamesStockInSet		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 5);
String adminUserIdsCertificate		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 9);
String adminUserNamesCertificate	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 9);
String adminUserIdsQualityInspection	= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 11);
String adminUserNamesQualityInspection	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, 11);
String adminUserIdsTagThird			= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.THIRD_TAG);
String adminUserNamesTagThird		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(transport_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.THIRD_TAG);

DBRow[] countyRows = transportMgrLL.getCounty();
int isOutter = StringUtil.getInt(request, "isOutter");
String isSubmitSuccess = StringUtil.getString(request, "isSubmitSuccess");

String previousUrl	= ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_basic_update.html?transport_id="+transport_id;
long purchase_id	= row.get("purchase_id",0);
boolean isDelivery	= false;
if(purchase_id!=0)
{
	isDelivery	= true;
	previousUrl = ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_purchase_basic_update.html?transport_id="+transport_id;
}

//是否有报关操作权限
boolean declaration_op_auth = declaration_responsible == loginRole?true:false;
boolean clearance_op_auth = clearance_responsible == loginRole?true:false;
boolean stock_in_set_op_auth = stock_in_set_responsible == loginRole?true:false;
boolean certificate_op_auth = certificate_responsible == loginRole?true:false;

boolean tag_third_op_auth = tag_third_responsible == loginRole?true:false;
boolean tag_op_auth = tag_responsible == loginRole?true:false;
boolean product_file_op_auth = product_file_responsible == loginRole?true:false;
boolean quality_inspection_op_auth = quality_inspection_responsible == loginRole?true:false;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改转运单各流程信息</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script>
$(document).ready(function(){
});

function adminUserTransportNew(ps_id,set_ids_node_id,set_names_node_id)
{
	var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/admin/admin_user.html"; 
	var option = {
		single_check:0, 						// 1表示的 单选
		user_ids:$("#"+set_ids_node_id).val(), //需要回显的UserId
		not_check_user:"",					//某些人不 会被选中的
		proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
		ps_id:ps_id,						//所属仓库
		set_id_val_node : set_ids_node_id,
		set_name_val_node : set_names_node_id,
		handle_method:'setIdAndName'
	};
	uri  = uri+"?"+jQuery.param(option);
	$.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
};

function setIdAndName(ids,names,node_ids_id,node_names_id)
{
	$("#" + node_ids_id).val(ids);
	$("#" + node_names_id).val(names);
}


function setParentUserShow(ids,names,  methodName,node_ids_id,node_names_id)
{
	eval(methodName)(ids,names,node_ids_id,node_names_id);
};
	
	
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="apply_money_form" id="apply_money_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/TransportAllProduresUpdateAction.action">
<input type="hidden" name="transport_id" value="<%=transport_id%>"/>
<input type="hidden" name="freight_changed" id="freight_changed" value="0"/>
<input name="finished" id="finished" type="hidden">
<input type="hidden" name="isOutter" value="<%=isOutter %>"/>

<!-- 各流程 -->
<input type="hidden" name="declaration" value="<%=declaration %>"/>
<input type="hidden" name="clearance" value="<%=clearance %>"/>
<input type="hidden" name="stock_in_set" value="<%=stock_in_set %>"/>
<input type="hidden" name="certificate" value="<%=certificate %>"/>

<input type="hidden" name="quality_inspection" value="<%=quality_inspection %>"/>
<input type="hidden" name="product_file" value="<%=product_file %>"/>
<input type="hidden" name="tag" value="<%=tag %>"/>
<input type="hidden" name="tag_third" value="<%=tag_third %>"/>

<!-- 负责人的Ids -->
<input type="hidden" name="adminUserIdsTransport" id="adminUserIdsTransport" value='<%=adminUserIdsTransport %>'/>
<input type="hidden" name="adminUserIdsDeclaration" id="adminUserIdsDeclaration" value='<%=adminUserIdsDeclaration %>'/>
<input type="hidden" name="adminUserIdsClearance" id="adminUserIdsClearance" value='<%=adminUserIdsClearance %>'/>
<input type="hidden" name="adminUserIdsStockInSet" id="adminUserIdsStockInSet" value='<%=adminUserIdsStockInSet %>'/>
<input type="hidden" name="adminUserIdsCertificate" id="adminUserIdsCertificate" value='<%=adminUserIdsCertificate %>'/>

<input type="hidden" name="adminUserIdsQualityInspection" id="adminUserIdsQualityInspection" value='<%=adminUserIdsQualityInspection %>'/>
<input type='hidden' name="adminUserIdsProductFile" id="adminUserIdsProductFile" value='<%=adminUserIdsProductFile %>'/>
<input type="hidden" name="adminUserIdsTag" id="adminUserIdsTag" value='<%=adminUserIdsTag %>'/>
<input type="hidden" name="adminUserIdsTagThird" id="adminUserIdsTagThird" value='<%=adminUserIdsTagThird %>'/>


<!-- 各流程，是否发各类通知 -->
<input type="hidden" name="needMailTransport"/>
<input type="hidden" name="needMessageTransport"/>
<input type="hidden" name="needPageTransport"/>

<input type="hidden" name="needMailDeclaration"/>
<input type="hidden" name="needMessageDeclaration"/>
<input type="hidden" name="needPageDeclaration"/>

<input type="hidden" name="needMailClearance"/>
<input type="hidden" name="needMessageClearance"/>
<input type="hidden" name="needPageClearance"/>

<input type="hidden" name="needMailStockInSet"/>
<input type="hidden" name="needMessageStockInSet"/>
<input type="hidden" name="needPageStockInSet"/>

<input type="hidden" name="needMailCertificate"/>
<input type="hidden" name="needMessageCertificate"/>
<input type="hidden" name="needPageCertificate"/>

<input type="hidden" name="needMailQualityInspection"/>
<input type="hidden" name="needMessageQualityInspection"/>
<input type="hidden" name="needPageQualityInspection"/>

<input type="hidden" name="needMailProductFile"/>
<input type="hidden" name="needMessageProductFile"/>
<input type="hidden" name="needPageProductFile"/>

<input type="hidden" name="needMailTag"/>
<input type="hidden" name="needMessageTag"/>
<input type="hidden" name="needPageTag"/>

<input type="hidden" name="needMailTagThird"/>
<input type="hidden" name="needMessageTagThird"/>
<input type="hidden" name="needPageTagThird"/>

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">转运单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
	        <tr height="25px">
	          <td width="13%" align="right" class="STYLE2">转运单号:</td>
	          <td width="87%" align="left" valign="middle">
	          	<%="T"+transport_id%>
	          </td>
	        </tr>
	        <!-- 运输负责人 -->
	        <tr height="25px" id="transportPersonTr" style="<%=tgroup%>">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">运输负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input type="text" id="adminUserNamesTransport" name="adminUserNamesTransport" style="width:180px;" onclick="adminUserTransport()" value='<%=adminUserNamesTransport %>'/>
			    	通知：
			   		<%
			    		DBRow scheduleTransport	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(transport_id), ModuleKey.TRANSPORT_ORDER, 4);
				    	int mailTransport			= 2;//1提醒
			    		int messageTransport		= 2;
			    		int pageTransport			= 2;	
			   		 	if(null != scheduleTransport){
				   		 	mailTransport			= scheduleTransport.get("sms_email_notify",2);//1提醒
				    		messageTransport		= scheduleTransport.get("sms_short_notify",2);
				    		pageTransport			= scheduleTransport.get("is_need_replay",2);
			    		}
			    	%>
			   		<input type="checkbox" name="isMailTransport" <%= (1 == mailTransport || 2 == mailTransport) ? "checked":""%>/>邮件
			   		<input type="checkbox" name="isMessageTransport" <%if(1 == messageTransport || 2 == messageTransport){out.print("checked='checked'");}%>/>短信
			   		<input type="checkbox" name="isPageTransport" <%if(1 == pageTransport || 2 == pageTransport){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
			
			<!-- 出口报关 -->
	        <tr height="25px" style="<%=tgroup%>">
	        	   <td width="13%" align="right" class="STYLE2">出口报关:</td>
	         		<td width="87%" align="left" valign="middle" id="declarationTr">
	         	<%
		   	 		String declarationPersonTrStyle = "";
		   	 		String declaration_resp_id = "";
		   	 		if((ccidIsSame && declaration.equals("0")) || declaration.equals("1"))
		   	 		{
		   	 			declarationPersonTrStyle = "display:none";
		    		}
	         			//有报关，报关负责方不能修改，只能修改具体负责人
	         		if( !declaration.equals(DeclarationKey.NODELARATION+""))
	         		{
	         			if(declaration_responsible == TransportRoleKey.TRANSPORT_ROLE_SEND)
	        			{
	        				declaration_resp_id = send_psid+"";
	        				//发货方负责报关
	        				if ("".equals(send_ps_name)) {
	        					out.print(declarationKey.getStatusById(Integer.parseInt(declaration))+": 发货方负责");
	        				} else {
	        					out.print(declarationKey.getStatusById(Integer.parseInt(declaration))+": 发货方(<font color='blue'>"+send_ps_name+"</font>)负责");
	        				}
	        			}
	        			else if(declaration_responsible == TransportRoleKey.TRANSPORT_ROLE_RECEIVE)
	        			{
	        				declaration_resp_id = receive_psid+"";
	        				if ("".equals(receive_ps_name)) {
	        					out.print(declarationKey.getStatusById(Integer.parseInt(declaration))+": 收货方负责");
	        				} else {
	        					out.print(declarationKey.getStatusById(Integer.parseInt(declaration))+": 收货方(<font color='blue'>"+receive_ps_name+"</font>)负责");
	        				}
	        			}
	        			else
	        			{
	        				//出现这种情况的原因是  需要报关，但是没有指派给发货方或者收货方
	        				out.print("流程未指派");
	        				declarationPersonTrStyle = "display:none";
	        			}
	         		}
	         		else
	         		{		
	         				out.print(declarationKey.getStatusById(Integer.parseInt(declaration)));
	         		}
	         	%>
	      			</td>
	      	</tr>
			<tr height="25px" style='<%=declarationPersonTrStyle %>;<%=tgroup%>' id="declarationPersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input type="text" <%=declaration_op_auth?"":"disabled" %> readonly id="adminUserNamesDeclaration" name="adminUserNamesDeclaration" value='<%=adminUserNamesDeclaration %>' style="width:180px;" 
			    	onclick="adminUserTransportNew('<%=declaration_resp_id%>','adminUserIdsDeclaration','adminUserNamesDeclaration')"/>
			    	通知：
			    	<%
			    		DBRow scheduleDeclaration	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(transport_id), ModuleKey.TRANSPORT_ORDER, 7);
				    	int mailDeclaration			= 2;//1提醒
			    		int messageDeclaration		= 2;
			    		int pageDeclaration			= 2;	
			   		 	if(null != scheduleDeclaration){
				   		 	mailDeclaration			= scheduleDeclaration.get("sms_email_notify",2);//1提醒
				    		messageDeclaration		= scheduleDeclaration.get("sms_short_notify",2);
				    		pageDeclaration			= scheduleDeclaration.get("is_need_replay",2);
			    		}
			   		 	//System.out.println(mailDeclaration+"---"+messageDeclaration+"---"+pageDeclaration);
			    		
			    	%>
			   		<input <%=declaration_op_auth?"":"disabled" %> type="checkbox" name="isMailDeclaration" <%= ((1 == mailDeclaration || 2 == mailDeclaration)&& declaration_op_auth) ? "checked":""%>/>邮件
			   		<input <%=declaration_op_auth?"":"disabled" %> type="checkbox" name="isMessageDeclaration" <%if((1 == messageDeclaration || 2 == messageDeclaration )&&declaration_op_auth){out.print("checked='checked'");}%>/>短信
			   		<input <%=declaration_op_auth?"":"disabled" %> type="checkbox" name="isPageDeclaration" <%if((1 == pageDeclaration || 2 == pageDeclaration)&&declaration_op_auth){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" style="<%=tgroup%>" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
			
			<!-- 进口清关 -->
			<tr height="25px" style="<%=tgroup%>">
        		<td width="13%" align="right" class="STYLE2">进口清关:</td>
		        <td width="87%" align="left" valign="middle" id="clearanceTr">
		       <%
		      	  String clearancePersonTrStyle = "";
		   	 		if((ccidIsSame && clearance.equals("0")) || clearance.equals("1"))
		   	 		{
		   	 		clearancePersonTrStyle = "display:none";
		    		}
		    		
		    		String clearce_resp_id = "";
		        	if(!clearance.equals(ClearanceKey.NOCLEARANCE+""))
		        	{
		        		if(clearance_responsible == TransportRoleKey.TRANSPORT_ROLE_SEND)
		        		{
		        			clearce_resp_id = send_psid+"";
		        			if ("".equals(send_ps_name)) {
		        				out.print(clearanceKey.getStatusById(Integer.parseInt(clearance))+": 发货方负责");
		        			} else {
		        				out.print(clearanceKey.getStatusById(Integer.parseInt(clearance))+": 发货方(<font color='blue'>"+send_ps_name+"</font>)负责");
		        			}
		        		}
		        		else if(clearance_responsible == TransportRoleKey.TRANSPORT_ROLE_RECEIVE)
		        		{
		        			clearce_resp_id = receive_psid+"";
		        			if ("".equals(receive_ps_name)) {
		        				out.print(clearanceKey.getStatusById(Integer.parseInt(clearance))+": 收货方负责");
		        			} else {
		        				out.print(clearanceKey.getStatusById(Integer.parseInt(clearance))+": 收货方(<font color='blue'>"+receive_ps_name+"</font>)负责");
		        			}
		        			
		        		}
		        		else
		        		{
		        			clearancePersonTrStyle = "display:none";
		        			out.print("流程未指派");
		        		}
		        	}
		        	else
		        	{
		        		out.print(clearanceKey.getStatusById(Integer.parseInt(clearance)));
		        	}
		       %>
				</td>
	        </tr>
	        <tr height="25px" style='<%=clearancePersonTrStyle %>;<%=tgroup%>' id="clearancePersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input  <%=clearance_op_auth?"":"disabled" %> readonly  type="text" id="adminUserNamesClearance" name="adminUserNamesClearance" value='<%=adminUserNamesClearance %>' style="width:180px;" 
			    	onclick="adminUserTransportNew('<%=clearce_resp_id%>','adminUserIdsClearance','adminUserNamesClearance')"/>
			    	通知：
			    	<%
			    		DBRow scheduleClearance	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(transport_id), ModuleKey.TRANSPORT_ORDER, 6);
				    	int mailClearance			= 2;//1提醒
			    		int messageClearance		= 2;
			    		int pageClearance			= 2;	
			   		 	if(null != scheduleClearance){
				   		 	mailClearance			= scheduleClearance.get("sms_email_notify",2);//1提醒
				    		messageClearance		= scheduleClearance.get("sms_short_notify",2);
				    		pageClearance			= scheduleClearance.get("is_need_replay",2);
				    		
			    		}
			   		 //System.out.println(mailClearance+"---"+messageClearance+"---"+pageClearance);
			    		
			    	%>
			   		<input <%=clearance_op_auth?"":"disabled" %> type="checkbox" name="isMailClearance" <%if((1 == mailClearance || 2 == mailClearance)&&clearance_op_auth){out.print("checked='checked'");}%>/>邮件
			   		<input <%=clearance_op_auth?"":"disabled" %> type="checkbox" name="isMessageClearance"  <%if((1 == messageClearance || 2 == messageClearance)&&clearance_op_auth){out.print("checked='checked'");}%>/>短信
			   		<input <%=clearance_op_auth?"":"disabled" %> type="checkbox" name="isPageClearance"  <%if((1 == pageClearance || 2 == pageClearance)&&clearance_op_auth){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" style="<%=tgroup%>" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
			
			<!-- 运费流程 -->
	        <tr height="25px" style="<%=tgroup%>">
	       	  <td width="13%" align="right" class="STYLE2">运费流程:</td>
	          <td width="87%" align="left" valign="middle" id="stockInSetTr">
	          <%
	          	String stockInSetPersonTrStyle = "display:none";
	          	String stockInset_resp_id = "";
		   	 	if(1 != stock_in_set)
		   	 	{
		          		if(stock_in_set_responsible == TransportRoleKey.TRANSPORT_ROLE_SEND)
		          		{
		          			stockInSetPersonTrStyle = "";
		          			stockInset_resp_id = send_psid+"";
		          			if ("".equals(send_ps_name)) {
		          				out.print(transportStockInSetKey.getStatusById(stock_in_set)+": 发货方负责");
		          			} else {
		          				out.print(transportStockInSetKey.getStatusById(stock_in_set)+": 发货方(<font color='blue'>"+send_ps_name+"</font>)负责");
		          			}
		          		}
		          		else if(stock_in_set_responsible == TransportRoleKey.TRANSPORT_ROLE_RECEIVE)
		          		{
		          			stockInSetPersonTrStyle = "";
		          			stockInset_resp_id = receive_psid +"";
		          			if ("".equals(receive_ps_name)) {
		          				out.print(transportStockInSetKey.getStatusById(stock_in_set)+": 收货方负责");
		          			} else {
		          				out.print(transportStockInSetKey.getStatusById(stock_in_set)+": 收货方(<font color='blue'>"+receive_ps_name+"</font>)负责");
		          			}
		          		}
		          		else
		          		{
		          			out.print("流程未指派");
		          		}
	          	}
	          	else
	          	{
	          		out.print(transportStockInSetKey.getStatusById(stock_in_set));
	          	}
	          	
	          %>
	          </td>
	        </tr>
	        <tr height="25px" style="<%=stockInSetPersonTrStyle %>;<%=tgroup%>" id="stockInSetPersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input <%=stock_in_set_op_auth?"":"disabled" %> readonly type="text"  id="adminUserNamesStockInSet" name="adminUserNamesStockInSet" value='<%=adminUserNamesStockInSet %>' style="width:180px;" onclick="adminUserTransportNew('<%=stockInset_resp_id%>','adminUserIdsStockInSet','adminUserNamesStockInSet')"/>
			    	通知：
			    	<%
			    		DBRow scheduleStockInSet	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(transport_id), ModuleKey.TRANSPORT_ORDER, 5);
				    	int mailStockInSet			= 2;//1提醒
			    		int messageStockInSet		= 2;
			    		int pageStockInSet			= 2;	
			   		 	if(null != scheduleStockInSet){
				   		 	mailStockInSet			= scheduleStockInSet.get("sms_email_notify",2);//1提醒
				    		messageStockInSet		= scheduleStockInSet.get("sms_short_notify",2);
				    		pageStockInSet			= scheduleStockInSet.get("is_need_replay",2);
			    		}
			   		 //System.out.println(mailStockInSet+"---"+messageStockInSet+"---"+pageStockInSet);
			    	%>
			   		<input <%=stock_in_set_op_auth?"":"disabled" %>  type="checkbox" name="isMailStockInSet" <%if((1 == mailStockInSet || 2 == mailStockInSet)&&stock_in_set_op_auth){out.print("checked='checked'");}%>/>邮件
			   		<input <%=stock_in_set_op_auth?"":"disabled" %>  type="checkbox" name="isMessageStockInSet" <%if((1 == messageStockInSet || 2 == messageStockInSet)&&stock_in_set_op_auth){out.print("checked='checked'");}%>/>短信
			   		<input <%=stock_in_set_op_auth?"":"disabled" %> type="checkbox" name="isPageStockInSet" <%if((1 == pageStockInSet || 2 == pageStockInSet)&&stock_in_set_op_auth){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" style="<%=tgroup%>" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
			
			<!-- 单证 -->
	        <tr height="25px" style="<%=tgroup%>">
	          <td width="13%" align="right" class="STYLE2">单证:</td>
	          <td width="87%" align="left" valign="middle" id="certificateTr">
	          
	          <%
	          	String certificatePersonTrStyle = "display:none";
	          	String certify_resp_id ="";
		   	 	if(1 != certificate)
		   	 	{
		          			if(certificate_responsible == TransportRoleKey.TRANSPORT_ROLE_SEND)
		          			{
			          			certificatePersonTrStyle = "";
			          			certify_resp_id = send_psid+"";
			          			if ("".equals(send_ps_name)) {
			          				out.print(transportCertificateKey.getStatusById(certificate)+": 发货方负责");
			          			} else {
			          				out.print(transportCertificateKey.getStatusById(certificate)+": 发货方(<font color='blue'>"+send_ps_name+"</font>)负责");
			          			}
		          			}
			          		else if(certificate_responsible == TransportRoleKey.TRANSPORT_ROLE_RECEIVE)
			          		{
			          			certificatePersonTrStyle = "";
			          			certify_resp_id = receive_psid+"";
			          			if ("".equals(receive_ps_name)) {
			          				out.print(transportCertificateKey.getStatusById(certificate)+": 收货方负责");
			          			} else {
			          				out.print(transportCertificateKey.getStatusById(certificate)+": 收货方(<font color='blue'>"+receive_ps_name+"</font>)负责");
			          			}
			          		}
			          		else
			          		{
			          			out.print("流程未指派");
			          		}
		          	}
		          	else
		          	{
		          		out.print(transportCertificateKey.getStatusById(certificate));
		          	}
	          	
	          %>
	          </td>
	        </tr>
	        <tr height="25px" style="<%=certificatePersonTrStyle %>;<%=tgroup%>" id="certificatePersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input <%=certificate_op_auth?"":"disabled" %> readonly type="text" id="adminUserNamesCertificate" name="adminUserNamesCertificate" value='<%=adminUserNamesCertificate %>' style="width:180px;" 
			    	onclick="adminUserTransportNew('<%=certify_resp_id%>','adminUserIdsCertificate','adminUserNamesCertificate')"/>
			    	通知：
			    	<%
			    		DBRow scheduleCertificate	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(transport_id), ModuleKey.TRANSPORT_ORDER, 9);
				    	int mailCertificate			= 2;//1提醒
			    		int messageCertificate		= 2;
			    		int pageCertificate			= 2;	
			   		 	if(null != scheduleCertificate){
				   		 	mailCertificate			= scheduleCertificate.get("sms_email_notify",2);//1提醒
				    		messageCertificate		= scheduleCertificate.get("sms_short_notify",2);
				    		pageCertificate			= scheduleCertificate.get("is_need_replay",2);
			    		}
			   		 //System.out.println(mailCertificate+"---"+messageCertificate+"---"+pageCertificate);
			    	%>
			   		<input <%=certificate_op_auth?"":"disabled" %> type="checkbox" name="isMailCertificate" <%if((1 == mailCertificate || 2 == mailCertificate)&&certificate_op_auth){out.print("checked='checked'");}%>/>邮件
			   		<input <%=certificate_op_auth?"":"disabled" %> type="checkbox" name="isMessageCertificate" <%if((1 == messageCertificate || 2 == messageCertificate)&&certificate_op_auth){out.print("checked='checked'");}%>/>短信
			   		<input <%=certificate_op_auth?"":"disabled" %> type="checkbox" name="isPageCertificate" <%if((1 == pageCertificate || 2 == pageCertificate)&&certificate_op_auth){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" style="<%=tgroup%>" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>

			<!-- 质检报告 -->
			<tr height="25px" style="<%=qgroup%>">
				<td width="13%" align="right" class="STYLE2">质检报告:</td>
				<td width="87%" align="left" valign="middle" id="qualityInspectionTr"><%
String qualityInspectionPersonTrStyle = "display:none";
String qualityInspection_resp_id ="";
if(1 != quality_inspection)
{
	if(quality_inspection_responsible == TransportRoleKey.TRANSPORT_ROLE_SEND)
	{
		qualityInspectionPersonTrStyle = "";
		qualityInspection_resp_id = send_psid+"";
		if ("".equals(send_ps_name)) {
			out.print(transportQualityInspectionKeyKey.getStatusById(quality_inspection+"")+": 发货方负责");
		} else {
			out.print(transportQualityInspectionKeyKey.getStatusById(quality_inspection+"")+": 发货方(<font color='blue'>"+send_ps_name+"</font>)负责");
		}
	}
	else if(quality_inspection_responsible == TransportRoleKey.TRANSPORT_ROLE_RECEIVE)
	{
		qualityInspectionPersonTrStyle = "";
		qualityInspection_resp_id = receive_psid+"";
		if ("".equals(receive_ps_name)) {
			out.print(transportQualityInspectionKeyKey.getStatusById(quality_inspection+"")+": 收货方负责");
		} else {
			out.print(transportQualityInspectionKeyKey.getStatusById(quality_inspection+"")+": 收货方(<font color='blue'>"+receive_ps_name+"</font>)负责");
		}
	}
	else
	{
		out.print("流程未指派");
	}
}
else
{
	out.print(transportQualityInspectionKeyKey.getStatusById(quality_inspection+""));
}

%></td>
	        </tr>
	        <tr height="25px;" style="<%=qualityInspectionPersonTrStyle %>;<%=qgroup%>" id="qualityInspectionPersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人：</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input <%=quality_inspection_op_auth?"":"disabled"%> type="text" id="adminUserNamesQualityInspection" name="adminUserNamesQualityInspection" value='<%=adminUserNamesQualityInspection %>' style="width:180px;"
			    		onclick="adminUserTransportNew('<%=qualityInspection_resp_id%>','adminUserIdsQualityInspection','adminUserNamesQualityInspection')"/>
			    	通知：
			    	<%
			    		DBRow scheduleQualityInspection	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(transport_id), ModuleKey.TRANSPORT_ORDER, 11);
				    	int mailQualityInspection			= 2;//1提醒
			    		int messageQualityInspection		= 2;
			    		int pageQualityInspection			= 2;	
			   		 	if(null != scheduleQualityInspection){
				   		 	mailQualityInspection			= scheduleQualityInspection.get("sms_email_notify",2);//1提醒
				    		messageQualityInspection		= scheduleQualityInspection.get("sms_short_notify",2);
				    		pageQualityInspection			= scheduleQualityInspection.get("is_need_replay",2);
			    		}
			    	%>
			   		<input <%=quality_inspection_op_auth?"":"disabled" %> type="checkbox" name="isMailQualityInspection" <%if((1 == mailQualityInspection || 2 == mailQualityInspection)&&quality_inspection_op_auth){out.print("checked='checked'");}%>/>邮件
			   		<input <%=quality_inspection_op_auth?"":"disabled" %> type="checkbox" name="isMessageQualityInspection" <%if((1 == messageQualityInspection || 2 == messageQualityInspection)&&quality_inspection_op_auth){out.print("checked='checked'");}%>/>短信
			   		<input <%=quality_inspection_op_auth?"":"disabled" %> type="checkbox" name="isPageQualityInspection" <%if((1 == pageQualityInspection || 2 == pageQualityInspection)&&quality_inspection_op_auth){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" style="<%=qgroup%>" align="center" valign="middle" ><td colspan="2"><hr width="96%" color="silver"/></td></tr>
			
			<!-- 实物图片 -->
	        <tr height="25px" style="<%=qgroup%>">
	          <td width="13%" align="right" class="STYLE2">实物图片:</td>
	          <td width="87%" align="left" valign="middle"><%
String productFilePersonTrStyle = "display:none";
String productFile_resp_id ="";
if(1 != product_file)
{
	if(product_file_responsible == TransportRoleKey.TRANSPORT_ROLE_SEND)
	{
		productFilePersonTrStyle = "";
		productFile_resp_id = send_psid+"";
		if ("".equals(send_ps_name)) {
			out.print(transportProductFileKey.getStatusById(product_file)+": 发货方负责");
		} else {
			out.print(transportProductFileKey.getStatusById(product_file)+": 发货方(<font color='blue'>"+send_ps_name+"</font>)负责");
		}
		
	}
	else if(product_file_responsible == TransportRoleKey.TRANSPORT_ROLE_RECEIVE)
	{
		productFilePersonTrStyle = "";
		productFile_resp_id = receive_psid+"";
		if ("".equals(receive_ps_name)) {
			out.print(transportProductFileKey.getStatusById(product_file)+": 收货方负责");
		} else {
			out.print(transportProductFileKey.getStatusById(product_file)+": 收货方(<font color='blue'>"+receive_ps_name+"</font>)负责");
		}
	}
	else
	{
		out.print("流程未指派");
	}
}
else
{
	out.print(transportProductFileKey.getStatusById(product_file));
}
				%></td>
	        </tr>
	        <tr height="25px" style="<%=productFilePersonTrStyle %>;<%=qgroup%>" id="productFilePersonTr" >
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人:</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input <%=product_file_op_auth?"":"disabled" %> readonly type="text" id="adminUserNamesProductFile" name="adminUserNamesProductFile" value='<%=adminUserNamesProductFile %>' style="width:180px;"  
			    		onclick="adminUserTransportNew('<%=productFile_resp_id%>','adminUserIdsProductFile','adminUserNamesProductFile')" />
			    	通知：
			    	<%
			    		DBRow scheduleProductFile	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(transport_id), ModuleKey.TRANSPORT_ORDER, 10);
				    	int mailProductFile			= 2;//1提醒
			    		int messageProductFile		= 2;
			    		int pageProductFile			= 2;	
			   		 	if(null != scheduleProductFile){
				   		 	mailProductFile			= scheduleProductFile.get("sms_email_notify",2);//1提醒
				    		messageProductFile		= scheduleProductFile.get("sms_short_notify",2);
				    		pageProductFile			= scheduleProductFile.get("is_need_replay",2);
			    		}
			   		// System.out.println(mailProductFile+"---"+messageProductFile+"---"+pageProductFile);
			    		
			    	%>
			   		<input <%=product_file_op_auth?"":"disabled" %> type="checkbox" name="isMailProductFile" <%if((1 == mailProductFile || 2 == mailProductFile)&&product_file_op_auth){out.print("checked='checked'");}%>/>邮件
			   		<input <%=product_file_op_auth?"":"disabled" %> type="checkbox" name="isMessageProductFile" <%if((1 == messageProductFile || 2 == messageProductFile)&&product_file_op_auth){out.print("checked='checked'");}%>/>短信
			   		<input <%=product_file_op_auth?"":"disabled" %> type="checkbox" name="isPageProductFile" <%if((1 == pageProductFile || 2 == pageProductFile)&&product_file_op_auth){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" style="<%=qgroup%>" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
			
			<!-- 内部标签 -->
			<tr height="25px" style="<%=qgroup%>">
				<td width="13%" align="right" class="STYLE2">内部标签:</td>
				<td width="87%" align="left" valign="middle" id="transportTr"><%
String tagPersonTrStyle = "display:none";
String tag_resp_id ="";
if(1 != tag)
{
	if(tag_responsible == TransportRoleKey.TRANSPORT_ROLE_SEND)
	{
		tagPersonTrStyle = "";
		tag_resp_id = send_psid+"";
		if ("".equals(send_ps_name)) {
			out.print(transportTagKey.getTransportTagById(tag)+": 发货方负责");
		} else {
			out.print(transportTagKey.getTransportTagById(tag)+": 发货方(<font color='blue'>"+send_ps_name+"</font>)负责");
		}
	}
	else if(tag_responsible == TransportRoleKey.TRANSPORT_ROLE_RECEIVE)
	{
		tagPersonTrStyle = "";
		tag_resp_id = receive_psid+"";
		if ("".equals(receive_ps_name)) {
			out.print(transportTagKey.getTransportTagById(tag)+": 收货方负责");
		} else {
			out.print(transportTagKey.getTransportTagById(tag)+": 收货方(<font color='blue'>"+receive_ps_name+"</font>)负责");
		}
	}
	else
	{
		out.print("流程未指派");
	}
}
else
{
	out.print(transportTagKey.getTransportTagById(tag));
}
%></td>
			</tr>
	        <tr height="25px" style="<%=tagPersonTrStyle%>;<%=qgroup%>" id="tagPersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人:</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input <%=tag_op_auth?"":"disabled" %> readonly type="text" id="adminUserNamesTag" name="adminUserNamesTag" value='<%=adminUserNamesTag %>' style="width:180px;" 
			    	onclick="adminUserTransportNew('<%=tag_resp_id%>','adminUserIdsTag','adminUserNamesTag')"/>
			    	通知：
			    	<%
			    		DBRow scheduleTag	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(transport_id), ModuleKey.TRANSPORT_ORDER, 8);
				    	int mailTag			= 2;//1提醒
			    		int messageTag		= 2;
			    		int pageTag			= 2;	
			   		 	if(null != scheduleTag){
				   		 	mailTag			= scheduleTag.get("sms_email_notify",2);//1提醒
				    		messageTag		= scheduleTag.get("sms_short_notify",2);
				    		pageTag			= scheduleTag.get("is_need_replay",2);
			    		}
			    	%>
			   		<input <%=tag_op_auth?"":"disabled" %> type="checkbox" name="isMailTag" <%if((1 == mailTag || 2 == mailTag)&&tag_op_auth){out.print("checked='checked'");}%>/>邮件
			   		<input <%=tag_op_auth?"":"disabled" %> type="checkbox" name="isMessageTag" <%if((1 == messageTag || 2 == messageTag)&&tag_op_auth){out.print("checked='checked'");}%>/>短信
			   		<input <%=tag_op_auth?"":"disabled" %> type="checkbox" name="isPageTag" <%if((1 == pageTag || 2 == pageTag)&&tag_op_auth){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" style="<%=qgroup%>" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
			
			<!-- 第三方标签 -->
			<tr height="25px" style="<%=qgroup%>">
				<td width="13%" align="right" class="STYLE2">第三方标签:</td>
				<td width="87%" align="left" valign="middle" id="transportTrThird"><%
String tagThirdPersonTrStyle = "display:none";
String tagThird_resp_id ="";
if(1 != tag_third)
{
	if(tag_third_responsible == TransportRoleKey.TRANSPORT_ROLE_SEND)
	{
		tagThirdPersonTrStyle = "";
		tagThird_resp_id = send_psid+"";
		if ("".equals(send_ps_name)) {
			out.print(transportTagKey.getTransportTagById(tag_third)+": 发货方负责");
		} else {
			out.print(transportTagKey.getTransportTagById(tag_third)+": 发货方(<font color='blue'>"+send_ps_name+"</font>)负责");
		}
	}
	else if(tag_third_responsible == TransportRoleKey.TRANSPORT_ROLE_RECEIVE)
	{
		tagThirdPersonTrStyle = "";
		tagThird_resp_id = receive_psid+"";
		if ("".equals(receive_ps_name)) {
			out.print(transportTagKey.getTransportTagById(tag_third)+": 收货方负责");
		} else {
			out.print(transportTagKey.getTransportTagById(tag_third)+": 收货方(<font color='blue'>"+receive_ps_name+"</font>)负责");
		}
	}
	else
	{
		out.print("流程未指派");
	}
}
else
{
	out.print(transportTagKey.getTransportTagById(tag_third));
}
%></td>
			</tr>
	        <tr height="25px" style="<%=tagThirdPersonTrStyle %>;<%=qgroup%>" id="tagThirdPersonTr">
				<td width="13%"  align="right" valign="middle" class="STYLE2" nowrap="nowrap">负责人:</td>
			    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
			    	<input <%=tag_third_op_auth?"":"disabled" %> readonly type="text" id="adminUserNamesTagThird" name="adminUserNamesTagThird" value='<%=adminUserNamesTagThird %>' style="width:180px;" 
			    	onclick="adminUserTransportNew('<%=tagThird_resp_id%>','adminUserIdsTagThird','adminUserNamesTagThird')"/>
			    	通知：
			    	<%
			    		DBRow scheduleTagThird	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(transport_id), ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.THIRD_TAG);
				    	int mailTagThird		= 2;//1提醒
			    		int messageTagThird		= 2;
			    		int pageTagThird		= 2;
			   		 	if(null != scheduleTagThird){
				   		 	mailTagThird		= scheduleTagThird.get("sms_email_notify",2);//1提醒
				    		messageTagThird		= scheduleTagThird.get("sms_short_notify",2);
				    		pageTagThird		= scheduleTagThird.get("is_need_replay",2);
			    		}
			    	%>
			   		<input <%=tag_third_op_auth?"":"disabled" %> type="checkbox" name="isMailTagThird" <%if((1 == mailTagThird || 2 == mailTagThird)&&tag_third_op_auth){out.print("checked='checked'");}%>/>邮件
			   		<input <%=tag_third_op_auth?"":"disabled" %> type="checkbox" name="isMessageTagThird" <%if((1 == messageTagThird || 2 == messageTagThird)&&tag_third_op_auth){out.print("checked='checked'");}%>/>短信
			   		<input <%=tag_third_op_auth?"":"disabled" %> type="checkbox" name="isPageTagThird" <%if((1 == pageTagThird || 2 == pageTagThird)&&tag_third_op_auth){out.print("checked='checked'");}%>/>页面
				</td>
			</tr>
			<tr height="15px;" style="display:none" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
		</table>
	</fieldset>	
	</td>
  </tr>
</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
    	 <input name="insert" type="button" class="normal-green-long" onclick="submitApply(this)" value="完成" >
     	 <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
</form>
<form name="previousStepForm" id="previousStepForm" action="<%=previousUrl%>" method="post">
</form>
<script type="text/javascript">
	function submitApply(obj)
	{
		obj.disabled="true";
<% if ("".equals(tgroup)) {%>
		if($("input:checkbox[name=isMailDeclaration]").attr("checked"))
		{
			document.apply_money_form.needMailDeclaration.value = 2;
		}
		if($("input:checkbox[name=isMessageDeclaration]").attr("checked"))
		{
			document.apply_money_form.needMessageDeclaration.value = 2;
		}
		if($("input:checkbox[name=isPageDeclaration]").attr("checked"))
		{
			document.apply_money_form.needPageDeclaration.value = 2;
		}

		if($("input:checkbox[name=isMailClearance]").attr("checked"))
		{
			document.apply_money_form.needMailClearance.value = 2;
		}
		if($("input:checkbox[name=isMessageClearance]").attr("checked"))
		{
			document.apply_money_form.needMessageClearance.value = 2;
		}
		if($("input:checkbox[name=isPageClearance]").attr("checked"))
		{
			document.apply_money_form.needPageClearance.value = 2;
		}


		if($("input:checkbox[name=isMailStockInSet]").attr("checked"))
		{
			document.apply_money_form.needMailStockInSet.value = 2;
		}
		if($("input:checkbox[name=isMessageStockInSet]").attr("checked"))
		{
			document.apply_money_form.needMessageStockInSet.value = 2;
		}
		if($("input:checkbox[name=isPageStockInSet]").attr("checked"))
		{
			document.apply_money_form.needPageStockInSet.value = 2;
		}

		if($("input:checkbox[name=isMailCertificate]").attr("checked"))
		{
			document.apply_money_form.needMailCertificate.value = 2;
		}
		if($("input:checkbox[name=isMessageCertificate]").attr("checked"))
		{
			document.apply_money_form.needMessageCertificate.value = 2;
		}
		if($("input:checkbox[name=isPageCertificate]").attr("checked"))
		{
			document.apply_money_form.needPageCertificate.value = 2;
		}
		//不是无需报关
		if(document.apply_money_form.declaration.value != "" && document.apply_money_form.declaration.value != "1")
		{
			if(isEmpty($("#adminUserIdsDeclaration").val()))
			{
				alert("请选择报关负责人");
				obj.disabled=false;
				return;
			}
		}
		if(document.apply_money_form.clearance.value != "" && document.apply_money_form.clearance.value != "1")
		{
			if(isEmpty($("#adminUserIdsClearance").val()))
			{
				alert("请选择清关负责人");
				obj.disabled=false;
				return;
			}
		}
		//下面校验必选项
		if(document.apply_money_form.stock_in_set.value != "" && document.apply_money_form.stock_in_set.value != "1")
		{
			if(isEmpty($("#adminUserIdsStockInSet").val()))
			{
				alert("请选择运输负责人");
				obj.disabled=false;
				return;
			}
		}
		//下面校验必选项
		if(document.apply_money_form.certificate.value != "" && document.apply_money_form.certificate.value != "1")
		{
			if(isEmpty($("#adminUserIdsCertificate").val()))
			{
				alert("请选择单证负责人");
				obj.disabled=false;
				return;
			}
		}
<%}%>
<%if ("".equals(qgroup)) {%>
		
		
		if($("input:checkbox[name=isMailQualityInspection]").attr("checked"))
		{
			document.apply_money_form.needMailQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isMessageQualityInspection]").attr("checked"))
		{
			document.apply_money_form.needMessageQualityInspection.value = 2;
		}
		if($("input:checkbox[name=isPageQualityInspection]").attr("checked"))
		{
			document.apply_money_form.needPageQualityInspection.value = 2;
		}
		
		if($("input:checkbox[name=isMailProductFile]").attr("checked"))
		{
			document.apply_money_form.needMailProductFile.value = 2;
		}
		if($("input:checkbox[name=isMessageProductFile]").attr("checked"))
		{
			document.apply_money_form.needMessageProductFile.value = 2;
		}
		if($("input:checkbox[name=isPageProductFile]").attr("checked"))
		{
			document.apply_money_form.needPageProductFile.value = 2;
		}
		if($("input:checkbox[name=isMailTag]").attr("checked"))
		{
			document.apply_money_form.needMailTag.value = 2;
		}
		if($("input:checkbox[name=isMessageTag]").attr("checked"))
		{
			document.apply_money_form.needMessageTag.value = 2;
		}
		if($("input:checkbox[name=isPageTag]").attr("checked"))
		{
			document.apply_money_form.needPageTag.value = 2;
		}
		
		if($("input:checkbox[name=isMailTagThird]").attr("checked"))
		{
			document.apply_money_form.needMailTagThird.value = 2;
		}
		if($("input:checkbox[name=isMessageTagThird]").attr("checked"))
		{
			document.apply_money_form.needMessageTagThird.value = 2;
		}
		if($("input:checkbox[name=isPageTagThird]").attr("checked"))
		{
			document.apply_money_form.needPageTagThird.value = 2;
		}
		
		if(document.apply_money_form.quality_inspection.value != "" && document.apply_money_form.quality_inspection.value != "1")
		{
			if(isEmpty($("#adminUserIdsQualityInspection").val()))
			{
				alert("请选择质检负责人");
				obj.disabled=false;
				return;
			}
		}
		if(document.apply_money_form.product_file.value != "" && document.apply_money_form.product_file.value != "1")
		{
			if(isEmpty($("#adminUserIdsProductFile").val()))
			{
				alert("请选择实物图片负责人");
				obj.disabled=false;
				return;
			}
		}
		if(document.apply_money_form.tag.value != "" && document.apply_money_form.tag.value != "1")
		{
			if(isEmpty($("#adminUserIdsTag").val()))
			{
				alert("请选择内部标签负责人");
				obj.disabled=false;
				return;
			}
		}
		if(document.apply_money_form.tag_third.value != "" && document.apply_money_form.tag_third.value != "1")
		{
			if(isEmpty($("#adminUserIdsTagThird").val()))
			{
				alert("请选择地三方标签负责人");
				obj.disabled=false;
				return;
			}
		}
<%}%>
		document.apply_money_form.submit();
}
function isEmpty(str)
{
	return str == ""|| str == undefined || str == null || str == "null" || str == "undefined";
}
function closeWindow()
{
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
}
function previousStep()
{
	document.previousStepForm.submit();
}
function adminUserTransport()
{
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
 	 var option = {
		 single_check:0, 					// 1表示的 单选
		 user_ids:$("#adminUserIdsTransport").val(), //需要回显的UserId
		 not_check_user:"",					//某些人不 会被选中的
		 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
		 ps_id:'0',						//所属仓库
		 handle_method:'setParentUserShowTransport'
 	 };
 	 uri  = uri+"?"+jQuery.param(option);
 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
</script>
</body>
</html>