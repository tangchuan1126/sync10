<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.ll.Utils,java.text.SimpleDateFormat"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<%@ include file="../../include.jsp"%>
<%
	String transport_id = StringUtil.getString(request,"transport_id").equals("")?"0":StringUtil.getString(request,"transport_id");
	DBRow row = transportMgrLL.getTransportById(transport_id);
	
	String transport_waybill_number = row.getValue("transport_waybill_number")==null?"0":row.getValue("transport_waybill_number").toString();
	String transport_waybill_name = row.getValue("transport_waybill_name")==null?"":row.getValue("transport_waybill_name").toString();
	String transportby = row.getValue("transportby")==null?"0":row.getValue("transportby").toString();
	String carriers = row==null?"":row.getString("carriers");
	String declaration = row.getValue("declaration")==null?"1":row.getValue("declaration").toString();
	String clearance = row.getValue("clearance")==null?"1":row.getValue("clearance").toString();
	String drawback = row.getValue("drawback")==null?"1":row.getValue("drawback").toString();
	String invoice = row.getValue("invoice")==null?"1":row.getValue("invoice").toString();
	String transport_send_country = row.getValue("transport_send_country")==null?"0":row.getValue("transport_send_country").toString();
	String transport_receive_country = row.getValue("transport_receive_country")==null?"0":row.getValue("transport_receive_country").toString();
	String transport_send_place = row.getValue("transport_send_place")==null?"":row.getValue("transport_send_place").toString();
	String transport_receive_place = row.getValue("transport_receive_place")==null?"":row.getValue("transport_receive_place").toString();
	String transport_linkman = row.getValue("transport_linkman")==null?"":row.getValue("transport_linkman").toString();
	String transport_linkman_phone = row.getValue("transport_linkman_phone")==null?"":row.getValue("transport_linkman_phone").toString();
	String transport_address = row.getValue("transport_address")==null?"":row.getValue("transport_address").toString();
	String remark = row.getValue("remark")==null?"":row.getValue("remark").toString();
	String transport_receive_date = row.getValue("transport_receive_date")==null?"":row.getValue("transport_receive_date").toString();
	String fr_id = row.getValue("fr_id")==null?"0":row.getValue("fr_id").toString();
	int finished = StringUtil.getInt(request,"finished",0);
	int transport_status = row.get("transport_status",0);
	int isOutter = StringUtil.getInt(request, "isOutter");
	DBRow[] countyRows = transportMgrLL.getCounty();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>运单</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script>
	var updated = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated").equals("1")) {
	%>
			updated = true;
	<%
		}
	%>
	function init() {
		if(updated) {
			parent.location.reload();
			closeWindow();
		}
	}
	
	$(document).ready(function(){
		$("#transport_waybill_number").val("<%=transport_waybill_number%>");
		$("#transport_waybill_number").val("<%=transport_waybill_number%>");
		$("#transport_waybill_name").val("<%=transport_waybill_name%>");
		$("#transport_send_country").val("<%=transport_send_country%>");
		$("#transport_receive_country").val("<%=transport_receive_country%>");
		$("#transport_send_place").val("<%=transport_send_place%>");
		$("#transport_receive_place").val("<%=transport_receive_place%>");
		init();
	});
	
	function setFreight(fr_id,waybill_name,carriers,transportby,transportby_name, begin_country, begin_country_name, end_country, end_country_name, begin_port, end_port) 
	{
		$("#fr_id").val(fr_id);
		$("#transport_waybill_name").val(waybill_name);
		$("#carriers").val(carriers);	
		$("#transportby").val(transportby);
		$("#transportby_name").val(transportby_name);
		$("#transport_send_country").val(begin_country);
		$("#transport_send_country_name").val(begin_country_name);
		$("#transport_receive_country").val(end_country);
		$("#transport_receive_country_name").val(end_country_name);
		$("#transport_send_place").val(begin_port);
		$("#transport_receive_place").val(end_port);
	}

	function openFreightSelect() 
	{
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_resources/freight_resources_select.html";
		freightSelectWindow = $.artDialog.open(url, {title: '运输资源选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="frm1" id="frm1" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/transportFreightUpdateAction.action">
<input type="hidden" name="transport.transport_id" value="<%=transport_id%>"/>
<input type="hidden" name="transport.fr_id" id="fr_id" value="<%=fr_id%>"/>
<input type="hidden" name="isOutter" id="isOutter" value="<%=isOutter%>"/>
<input type="hidden" name="freight_changed" id="freight_changed" value="0"/>
<input name="finished" id="finished" type="hidden">

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">转运单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
	        <tr height="29">
	          <td align="right" class="STYLE2">转运单号:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<%="T"+transport_id%>
	          </td>
	          <td align="right" class="STYLE2"></td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">货运公司:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="transport.transport_waybill_name" value="<%=transport_waybill_name %>" id="transport_waybill_name" readonly="readonly"><br/>
	          	<input type="button" name="waybillSelect" id="waybillSelect" value="选择运输资源" onclick="openFreightSelect()">
	          </td>
	          <td align="right" class="STYLE2">运单号</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="transport.transport_waybill_number" id="transport_waybill_number" value='<%=transport_waybill_number %>'>
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">运输方式:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="hidden" name="transport.transportby" id="transportby" value="<%=transportby %>">
	          	<input type="text" name="transportby_name" id="transportby_name" value="<%=transportWayKey.getStatusById(Integer.parseInt(transportby)) %>" readonly="readonly">
	          </td>
	          <td align="right" class="STYLE2">承运公司:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="transport.carriers" id="carriers" readonly="readonly" value='<%=carriers %>'>
	          </td>
	        </tr>
	        <tr height="29" id="tr1" name="tr1">
	          <td align="right" class="STYLE2">始发港:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="transport.transport_send_place" value="<%=transport_send_place %>" id="transport_send_place" readonly="readonly">
	          </td>
	          <td align="right" class="STYLE2">目的港:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="transport.transport_receive_place" value="<%=transport_receive_place %>" id="transport_receive_place" readonly="readonly">
	          </td>
	        </tr>
		</table>
	</fieldset>	
	</td>
  </tr>
</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
    <%
    	if(isOutter == 2) {
    %>
    		<input name="insert" type="button" class="normal-green-long" onclick="submitApply(0)" value="完成" >
    <%
    	}else {
    %>
		    <input name="insert" type="button" class="normal-green-long" onclick="previousStep()" value="上一步" >
      		<input name="insert" type="button" class="normal-green-long" onclick="submitApply(1)" value="下一步" >
    <%
    	}
    %>
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
  </form>
<form name="previousStepForm" id="previousStepForm" action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_wayout_update.html" method="post">
	<input type="hidden" name="transport_id" value="<%=transport_id %>"/>
  </form>
<script type="text/javascript">
<!--
	function submitApply(finished)
	{
		$("#finished").val(finished);
		document.frm1.submit();
	}
	function closeWindow(){
		$.artDialog.close();
		//self.close();
	}
	function previousStep(){
		document.previousStepForm.submit();
	};
//-->
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 

</script>
</body>
</html>

