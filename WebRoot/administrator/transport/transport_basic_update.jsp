<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.StorageTypeKey"%>
<%@page import="java.util.List"%>
<%@ include file="../../include.jsp"%> 
<%
	DBRow treeRows[]	= catalogMgr.getProductStorageCatalogTree();
	String transport_id = StringUtil.getString(request, "transport_id");
	DBRow transportRow	= null;
	if(!"".equals(transport_id)){
		transportRow	= transportMgrLL.getTransportById(transport_id);
	}
	long countryId = 0L;
	if(null != transportRow){
		countryId = transportRow.get("deliver_ccid",0L);
	}
	Long deliver_pro_id = 0L;
	if(null != transportRow){
		deliver_pro_id = transportRow.get("deliver_pro_id",0L);
	}
	Long sendCountryId = 0L;
	if(null != transportRow){
		sendCountryId = transportRow.get("send_ccid",0L);
	}
	Long send_pro_id = 0L;
	if(null != transportRow){
		send_pro_id = transportRow.get("send_pro_id",0L);
	}
	int isOutter = StringUtil.getInt(request, "isOutter");
	String isSubmitSuccess = StringUtil.getString(request, "isSubmitSuccess");
	DBRow[] titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true, 0L, "", null, request);  //客户登录列出自己的title
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增转运单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 


<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script type="text/javascript">

	jQuery(function($){
			$('#transport_out_date,#transport_receive_date').datepicker({
				dateFormat:"yy-mm-dd",
				changeMonth: true,
				changeYear: true,
			});
			$("#ui-datepicker-div").css("display","none");
			if('0' != '<%=countryId %>'){
				getStorageProvinceByCcid('<%=countryId %>','<%=deliver_pro_id %>','deliver_pro_id' );
			}
			if('0' != '<%=sendCountryId %>'){
				getStorageProvinceByCcid('<%=sendCountryId %>','<%=send_pro_id %>','send_pro_id');
			}
			if('2' == '<%=isSubmitSuccess%>'){
				//cancel();
				$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			}
			if(1==$("#receive_psid option:selected").attr("stroageType"))
			{
				$("#transportArrrivalDateTr").css("display","");
			}
			else
			{
				$("#transportArrrivalDateTr").css("display","none");
			}
			if(1==$("#psid option:selected").attr("stroageType"))
			{
				$("#transportOutDateTr").css("display","");
			}
			else
			{
				$("#transportOutDateTr").css("display","none");
			}
			var stroage_type_receive = $("#receive_psid option:selected").attr("stroageType");
			$("#receive_ps_type").val(stroage_type_receive);

			var stroage_type_send = $("#psid option:selected").attr("stroageType");
			$("#send_ps_type").val(stroage_type_send);

			/*if('<%=StorageTypeKey.SELF%>'*1==stroage_type_receive*1)
			{
				$("#transportArrrivalDateTr").css("display","");
			}
			else
			{
				$("#transportArrrivalDateTr").css("display","none");
				$("#transport_receive_date").val("");
			}
			if('<%=StorageTypeKey.SELF%>'*1==stroage_type_send*1)
			{
				$("#transportOutDateTr").css("display","");
			}
			else
			{
				$("#transportOutDateTr").css("display","none");
				$("#transport_out_date").val("");
			}*/
			
	});
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	function stateDivSend(pro_id)
	{
		if(pro_id==-1)
		{
			$("#state_div_send").css("display","inline");
		}
		else
		{
			$("#state_div_send").css("display","none");
			$("#address_state_send").val("");
		}
	}
	function stateDivDeliver(pro_id)
	{
		if(pro_id==-1)
		{
			$("#state_div_deliver").css("display","inline");
		}
		else
		{
			$("#state_div_deliver").css("display","none");
			$("#address_state_deliver").val("");
		}
	}
	
	//国家地区切换
function getStorageProvinceByCcid(ccid,pro_id,id)
{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#"+id).clearAll();
					$("#"+id).addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#"+id).addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#"+id).addOption("手工输入","-1");
					}
				});
				
				if (pro_id!=""&&pro_id!=0)
				{
					$("#"+id).setSelectedValue(pro_id);
					if(id=="deliver_pro_id")
					{
						stateDivDeliver(pro_id);
					}
					else
					{
						stateDivSend(pro_id);
					}
				}
}

	function selectDeliveryStorage(obj)
	{
		var ps_id = obj.value;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{ps_id:ps_id},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data){
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					//$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.deliver_city);
					$("#deliver_ccid").val(data["deliver_nation"]);
					
					getStorageProvinceByCcid($("#deliver_ccid").val(),data["deliver_pro_id"],"deliver_pro_id");
					
					$("#deliver_name").val(data["deliver_contact"]);
					$("#deliver_linkman_phone").val(data["deliver_phone"]);
				}
			});
		/*var stroage_type = $("option:selected", obj).attr("stroageType");
		if(1==stroage_type)
		{
			$("#transportArrrivalDateTr").css("display","");
		}
		else
		{
			$("#transportArrrivalDateTr").css("display","none");
		}*/
	}
	
	function selectSendStorage(obj)
	{
		var ps_id = obj.value;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					ps_id:ps_id
				},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data)
				{
					$("#send_house_number").val(data["send_house_number"]);
					$("#send_street").val(data["send_street"]);
					//$("#send_address3").val(data.deliver_address3);
					$("#send_zip_code").val(data.send_zip_code);
					$("#send_name").val(data.send_contact);
					$("#send_linkman_phone").val(data.send_phone);
					$("#send_city").val(data.send_city);
					$("#send_ccid").val(data["send_nation"]);
					
					getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id");
				}
			});
		/*var stroage_type = $("option:selected", obj).attr("stroageType");
		if(1==stroage_type)
		{
			$("#transportOutDateTr").css("display","");
		}
		else
		{
			$("#transportOutDateTr").css("display","none");
		}*/
	}
	
	function submitCheck()
	{
		if($("#receive_psid").val()==0)
		{
			alert("请选择目的仓库");
			
		}
		else if($("#deliver_ccid").val()==0)
		{
			alert("请选择目的国家");
			
		}
		else if($("#deliver_city").val()=="")
		{
			alert("请填写目的城市");
			$("#deliver_city").focus();
			
		}
		else if($("#deliver_house_number").val()==""||$("#deliver_street").val()=="")
		{
			alert("请填写目的地址");
		}
		else if($("#deliver_zip_code").val()=="")
		{
			alert("交货地址邮编");
			$("#deliver_zip_code").focus();
		}
		else if($("#deliver_name").val()=="")
		{
			alert("请填写交货联系人");
			$("#deliver_name").focus();
		}
		else if($("#psid").val()==0)
		{
			alert("请选择发货仓库");
		}
		else if($("#send_ccid").val()==0)
		{
			alert("请选择发货国家");
			
		}
		else if($("#send_city").val()=="")
		{
			alert("请填写发货城市");
			$("#send_city").focus();
		}
		else if($("#send_house_number").val()==""||$("#send_street").val()=="")
		{
			alert("请填写发货地址");
		}
		else if($("#send_zip_code").val()=="")
		{
			alert("请填写发货地邮编");
			$("#send_zip_code").focus();
		}
		else if($("#send_name").val()=="")
		{
			alert("请填写发货联系人");
			$("#send_name").focus();
		}
		else if(-1 == $("#title_id").val())
		{
			alert("请选择title");
			tabSelect(2);
		}
		else
		{
			document.add_form.submit();
		}
	}
	function closeWindow(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	}
	function changeSendStorageType(obj)
	{
		var typeId = obj.value;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/GetStorageCatalogsByTypeAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:{typeId:typeId},
			beforeSend:function(request){
			},
			error: function(){
			},
			
			success: function(data)
			{
				$("#psid").clearAll();
				$("#psid").addOption("请选择","0");
				if (data!="")
				{
					$.each(data,function(i){
						$("#psid").addOption(data[i].title,data[i].id);
					});
				}
				$("#send_house_number").val("");
				$("#send_street").val("");
				$("#send_zip_code").val("");
				$("#send_name").val("");
				$("#send_linkman_phone").val("");
				$("#send_city").val("");
				$("#send_ccid").val("");
				$("#send_pro_id").val("");
				$("#state_div_send").css("display","none");
				$("#address_state_send").val("");
				//getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id");
			}
		});
		if('<%=StorageTypeKey.SELF%>'*1==typeId*1)
		{
			$("#transportOutDateTr").css("display","");
		}
		else
		{
			$("#transportOutDateTr").css("display","none");
			$("#transport_out_date").val("");
		}
		
	}
	function changeReceiveStorageType(obj)
	{
		var typeId = obj.value;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/GetStorageCatalogsByTypeAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:{typeId:typeId},
			beforeSend:function(request){
			},
			error: function(){
			},
			
			success: function(data)
			{
				$("#receive_psid").clearAll();
				$("#receive_psid").addOption("请选择","0");
				if (data!="")
				{
					$.each(data,function(i){
						$("#receive_psid").addOption(data[i].title,data[i].id);
					});
				}
				$("#deliver_house_number").val("");
				$("#deliver_street").val("");
				$("#deliver_zip_code").val("");
				$("#deliver_name").val("");
				$("#deliver_linkman_phone").val("");
				$("#deliver_city").val("");
				$("#deliver_ccid").val("");
				$("#deliver_pro_id").val("");
				$("#state_div_deliver").css("display","none");
				$("#address_state_deliver").val("");
			}
		});
		
		if('<%=StorageTypeKey.SELF%>'*1==typeId*1)
		{
			$("#transportArrrivalDateTr").css("display","");
		}
		else
		{
			$("#transportArrrivalDateTr").css("display","none");
			$("#transport_receive_date").val("");
		}
		
	}
	function changeProIdDeliver(obj)
	{
		stateDivDeliver(obj.value);
	}
	function changeProIdSend(obj)
	{
		stateDivSend(obj.value);
	}
	$(function(){
		$(".chzn-select").chosen({no_results_text: "没有该选项:"});
	});
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
<form name="add_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/updateTransportAction.action">
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_wayout_update.html"/>
	<input type="hidden" name="transport_id" value='<%=transport_id %>'/>
	<input type="hidden" name="isOutter" value="<%=isOutter%>"/>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="left" valign="top"><br>
            	  <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;"> 
						请选择提货仓库与接收仓库 
						</td>
					  </tr>
				  </table>
				  
				  <fieldset class="set" style="padding-bottom:4px;text-align: left;">
	  				<legend>
						<span style="" class="title"> 
							接收地址	 
						</span>
					</legend>
						 <table width="95%" align="center" cellpadding="2" cellspacing="0">
						 	<tr valign="bottom">
									  <td align="right">目的仓库</td>
								      <td>
								      <select id="receive_ps_type" onchange="changeReceiveStorageType(this)">
						 					<option value="-1">请选择</option>
								      		<%
									      		StorageTypeKey storageTypeKey = new StorageTypeKey();
								      			List<String> storageTypes = storageTypeKey.getStorageTypeKeys();
								      			for(int i = 0; i < storageTypes.size(); i ++)
								      			{
								      		%>
								      			<option value='<%=storageTypes.get(i) %>'><%=storageTypeKey.getStorageTypeKeyName(storageTypes.get(i)) %></option>
								      		<%		
								      			}
								      		%>
								      	</select>
								      	<select name='receive_psid' id='receive_psid' onchange="selectDeliveryStorage(this)">
		                                <option value="0">请选择...</option>
		                                <%
											  for ( int i=0; i<treeRows.length; i++ )
											  {
												  long productCatalog = 0L;
												  if(null != transportRow){
													  productCatalog = transportRow.get("receive_psid",0L);
												  }
												  if(treeRows[i].getString("id").equals(productCatalog+"")){
										%>
			                                <option value='<%=treeRows[i].getString("id")%>' selected="selected" stroageType='<%=treeRows[i].get("storage_type",0) %>'><%=treeRows[i].getString("title")%></option>
			                            <%				  
													  
												  }else{
										%>
			                                <option value='<%=treeRows[i].getString("id")%>' stroageType='<%=treeRows[i].get("storage_type",0) %>'><%=treeRows[i].getString("title")%></option>
			                            <% 
												  }
											}
										%>
		                              </select>
								      </td>
									</tr>
						 	<tr>
						 		<td align="right">目的国家</td>
						 		<td>
						 			<%
									DBRow countrycode[] = orderMgr.getAllCountryCode();
									String selectBg="#ffffff";
									String preLetter="";
									%>
							      <select name="deliver_ccid" id="deliver_ccid" onChange="getStorageProvinceByCcid(this.value,0,'deliver_pro_id');">
								  <option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++)
								  {
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
									{
										if (selectBg.equals("#eeeeee"))
										{
											selectBg = "#ffffff";
										}
										else
										{
											selectBg = "#eeeeee";
										}
									}  	
									preLetter = countrycode[i].getString("c_country").substring(0,1);
									if(countrycode[i].getString("ccid").equals(countryId+"")){
								 %>
								    <option style="background:<%=selectBg%>;"  value="<%=countrycode[i].getString("ccid")%>" selected="selected"><%=countrycode[i].getString("c_country")%></option>
								 <%		
									}else{
								 %>
								    <option style="background:<%=selectBg%>;"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								 <%		
									}
								  }
								  %>
							      </select>		
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">目的省份</td>
						 		<td>
						 			<select name="deliver_pro_id" id="deliver_pro_id" onchange="changeProIdDeliver(this)">
								    </select>		
								      <div style="padding-top: 10px;display:none;" id="state_div_deliver">
											<input type="text" name="address_state_deliver" id="address_state_deliver" value='<%=null!=transportRow?transportRow.getString("address_state_deliver"):"" %>'/>
									  </div>
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址门牌号</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_house_number" id="deliver_house_number" value='<%=null!=transportRow?transportRow.getString("deliver_house_number"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址街道</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_street" id="deliver_street" value='<%=null!=transportRow?transportRow.getString("deliver_street"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">目的城市</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_city" id="deliver_city" value='<%=null!=transportRow?transportRow.getString("deliver_city"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址邮编</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_zip_code" id="deliver_zip_code" value='<%=null!=transportRow?transportRow.getString("deliver_zip_code"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货联系人</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_name" id="deliver_name" value='<%=null!=transportRow?transportRow.getString("transport_linkman"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货联系人电话</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_linkman_phone" id="deliver_linkman_phone" value='<%=null!=transportRow?transportRow.getString("transport_linkman_phone"):"" %>'/></td>
						 	</tr>
						 	<tr id="transportArrrivalDateTr">
						 		<td align="right">ETA</td>
						 		<td><input width="80%" type="text" name="transport_receive_date" id="transport_receive_date" value='<%=!"".equals(transportRow.getString("transport_receive_date"))?new TDate(transportRow.getString("transport_receive_date")).formatDate("YYYY-MM-dd"):""%>'></td>
						 	</tr>
						 </table>
						</fieldset>
						<br/>
						<%
							if(transportRow.get("transport_status",0)==TransportOrderKey.READY)
							{
				  		%>
						<fieldset class="set" style="padding-bottom:4px;text-align: left;">
			  				<legend>
								<span style="" class="title"> 
									提货地址	 
								</span>
							</legend>
						<table width="95%" align="center" cellpadding="2" cellspacing="0">
						 	<tr>
						 		<td align="right">转运仓库</td>
						 		<td>
						 			<select id="send_ps_type" onchange="changeSendStorageType(this)">
						 					<option value="-1">请选择</option>
								      		<%
								      			for(int i = 0; i < storageTypes.size(); i ++)
								      			{
								      		%>
								      			<option value='<%=storageTypes.get(i) %>'><%=storageTypeKey.getStorageTypeKeyName(storageTypes.get(i)) %></option>
								      		<%		
								      			}
								      		%>
								      	</select>
						 				<select id="psid" name="send_psid" onchange="selectSendStorage(this)">
							     			<option value="0">请选择...</option>
		                                <%
											for ( int i=0; i<treeRows.length; i++ )
											{
												long sendProductCatalog = 0L;
												if(null != transportRow){
													sendProductCatalog = transportRow.get("send_psid",0L);
												}
												if(treeRows[i].getString("id").equals(sendProductCatalog+"")){
										%>
		                                	<option value='<%=treeRows[i].getString("id")%>' selected="selected" stroageType='<%=treeRows[i].get("storage_type",0) %>'><%=treeRows[i].getString("title")%></option>
		                                <%			
												}else{
										%>
		                                	<option value='<%=treeRows[i].getString("id")%>' stroageType='<%=treeRows[i].get("storage_type",0) %>'><%=treeRows[i].getString("title")%></option>
		                                <%		
												}
											}
										%>
							     		</select>
							    </td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货国家</td>
						 		<td>
							      <select name="send_ccid" id="send_ccid" onChange="getStorageProvinceByCcid(this.value,0,'send_pro_id');">
								  <option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++)
								  {
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
									{
										if (selectBg.equals("#eeeeee"))
										{
											selectBg = "#ffffff";
										}
										else
										{
											selectBg = "#eeeeee";
										}
									}  	
									if(countrycode[i].getString("ccid").equals(sendCountryId+"")){
								 %>
								    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("ccid")%>" selected="selected"><%=countrycode[i].getString("c_country")%></option>
								 <%		
									}else{
								 %>
								    <option style="background:<%=selectBg%>;"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								 <%		
									}
								  }
								  %>
							      </select>		
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货省份</td>
						 		<td>
						 			<select name="send_pro_id" id="send_pro_id" onchange="changeProIdSend(this)">
								    </select>		
								      	<div style="padding-top: 10px;display:none;" id="state_div_send">
											<input type="text" name="address_state_send" id="address_state_send" value='<%=null!=transportRow?transportRow.getString("address_state_send"):"" %>'/>
										</div>
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货地址门牌号</td>
						 		<td><input style="width: 300px;" type="text" name="send_house_number" id="send_house_number" value='<%=null!=transportRow?transportRow.getString("send_house_number"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货地址街道</td>
						 		<td><input style="width: 300px;" type="text" name="send_street" id="send_street" value='<%=null!=transportRow?transportRow.getString("send_street"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货城市</td>
						 		<td><input style="width: 300px;" type="text" name="send_city" id="send_city" value='<%=null!=transportRow?transportRow.getString("send_city"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货地址邮编</td>
						 		<td><input style="width: 300px;" type="text" name="send_zip_code" id="send_zip_code" value='<%=null!=transportRow?transportRow.getString("send_zip_code"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货联系人</td>
						 		<td><input style="width: 300px;" type="text" name="send_name" id="send_name" value='<%=null!=transportRow?transportRow.getString("send_name"):"" %>'/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货联系人电话</td>
						 		<td><input style="width: 300px;" type="text" name="send_linkman_phone" id="send_linkman_phone" value='<%=null!=transportRow?transportRow.getString("send_linkman_phone"):"" %>'/></td>
						 	</tr>
						 	<tr id="transportOutDateTr">
						 		<td align="right">ETD</td>
						 		<td><input width="80%" type="text" name="transport_out_date" id="transport_out_date" value='<%=!"".equals(transportRow.getString("transport_out_date"))?new TDate(transportRow.getString("transport_out_date")).formatDate("yyyy-MM-dd"):"" %>'></td>
						 	</tr>
						 </table>
						</fieldset>
						<%
							}
						%>
						<fieldset class="set" style="padding-bottom:4px;text-align: left;">
			  				<legend>
								<span style="" class="title">
									其他信息
								</span>
							</legend>
							<table width="95%" align="center" cellpadding="2" cellspacing="0">
							 	<tr>
							 		<td width="25%" align="right">备注</td>
							 		<td width="75%">
							 		<textarea rows="2" cols="70" name="remark" id="remark"><%=null!=transportRow?transportRow.getString("remark"):"" %></textarea>
							 		</td>
							 	</tr>
							 	<tr>
									<td width="25%" align="right">TITLE</td>
									<td width="75%">
										<div class="side-by-side clearfix">
											<select name="title_id" id="title_id" class="chzn-select" data-placeholder="Choose a Title..." style="width:250px;" tabindex="1">
												<option value="-1">请选择</option>
												<%
													for(int i = 0; i < titles.length; i ++)
													{
														String isSeled = "";
														if(null!=transportRow && titles[i].get("title_id", 0L) == transportRow.get("title_id", 0L))
														{
															isSeled = "selected = 'selected'";
														}
												%>
													<option value='<%=titles[i].get("title_id", 0L) %>' <%=isSeled %>><%=titles[i].getString("title_name") %></option>
												<%
													}
												%>
											</select>
										</div>
									</td>
								</tr>
						 	</table>
						</fieldset>
				</td>
			 </tr>
             <tr>
                <td  align="right" valign="middle" class="win-bottom-line">	
                 <%
			    	if(2 != isOutter){
			    %>
			    	<input type="button" name="Submit2" value="下一步" class="normal-green" onclick="submitCheck()">
			    <%
			    	}else{
			    %>		
			    	<input type="button" name="Submit2" value="完成" class="normal-green" onclick="submitCheck()">
			    <%
			    	}
			    %>			
			    	<input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWindow();"></td>
             </tr>
            </table>
      </form>
</body>
</html>
