<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@ include file="../../include.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>商品标签上传</title>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<!-- 在线阅读 -->
	<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script> 
  	
  	<%
		String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
  		String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
  		String pc_id = StringUtil.getString(request,"pc_id");
  		DBRow[] productRows = transportMgrZr.getProductByPcIds(pc_id);
  		long transport_id = StringUtil.getLong(request,"transport_id");
  		DBRow transportRow = transportMgrZJ.getDetailTransportById(transport_id);
  		//读取配置文件中的transport_tag_types
  		String value = systemConfig.getStringConfigValue("transport_tag_types");
  		String file_with_class = StringUtil.getString(request,"file_with_class");
  		String[] arraySelected = value.split("\n");
  		//将arraySelected组成一个List
  		ArrayList<String> selectedList= new ArrayList<String>();
  		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
  				String tempStr = arraySelected[index];
  				if(tempStr.indexOf("=") != -1){
  					String[] tempArray = tempStr.split("=");
  					String tempHtml = tempArray[1];
  					selectedList.add(tempHtml);
  				}
  		}
  		// 获取所有的关联图片 然后在页面上分类
  		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
  		DBRow[] imagesRows = transportMgrZr.getAllProductFileByPcId(pc_id,FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE,transport_id);
  		 
		if(imagesRows != null && imagesRows.length > 0 ){
			for(int index = 0 , count = imagesRows.length ; index < count ; index++ ){
				DBRow tempRow = imagesRows[index];
				String product_file_type = tempRow.getString("product_file_type");
				List<DBRow> tempListRow = imageMap.get(product_file_type);
				if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1)){
					tempListRow = new ArrayList<DBRow>();
				}
				tempListRow.add(tempRow);
				imageMap.put(product_file_type,tempListRow);
		
			}
		}
		String backurl =  ConfigBean.getStringValue("systenFolder") + "administrator/transport/transport_product_tag_file.html?transport_id="+transport_id+"&pc_id="+pc_id;
  	%>
  	<script type="text/javascript">
  		
  		function submitForm(){
  	  		var file = $("#file");
  	  		if(file.val().length < 1){
  	  	  		showMessage("请选择上传文件","alert");
				return ;
  	  	  	}
			var myform = $("#myform");
			myform.submit();
  	  	}
  	  	function fileWithClassTypeChange(){
			var file_with_className = $("#file_with_className");
			var selectedHtml = $("#file_with_class option:selected").html();
			file_with_className.val(selectedHtml);
			 
    	 }
   	 jQuery(function($){
   
	   	
	   	  $("#tabs").tabs({
	   			cache: true,
	  			select: function(event, ui){
					 $("#file_with_class option[value='"+(ui.index+1)+"']").attr("selected",true);
				} 
	   	 });
	   	if('<%= file_with_class%>'.length > 0){
			 $("#tabs").tabs( "select" , '<%= file_with_class%>' * 1-1 );
			 $("#file_with_class option[value='"+'<%= file_with_class%>'+"']").attr("selected",true);
		}
	   	 fileWithClassTypeChange();
   	  })
  	function deleteFile(file_id){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'product_file',file_id:file_id,folder:'product',pk:'pf_id'},
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}else{
					showMessage("系统错误,请稍后重试","error");
				}
			},
			error:function(){
				showMessage("系统错误,请稍后重试","error");
			}
		})
	}
   	function downLoad(fileName , tableName , folder){
   		 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ '<%= systemConfig.getStringConfigValue("file_path_product")%>');
   }
   	function fileWithClassChangeTag(){
		var file_with_class = $("#file_with_class").val();
		$("#tabs").tabs( "select" , file_with_class * 1-1 );
	}
   	//文件上传
   	  //做成文件上传后,然后页面刷新提交数据如果是有添加文件
   	    function uploadFile(_target){
   	        var targetNode = $("#"+_target);
   	        var fileNames = $("input[name='file_names']").val();
   	        var obj  = {
   	    	     reg:"picture_office",
   	    	     limitSize:2,
   	    	     target:_target
   	    	 }
   	        var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
   	    	uri += jQuery.param(obj);
   	    	 if(fileNames && fileNames.length > 0 ){
   	    		uri += "&file_names=" + fileNames;
   	    	}
   	    	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
   	        		 close:function(){
   						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
   						 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   	       		 }});
   	    }
   	  //jquery file up 回调函数
   	    function uploadFileCallBack(fileNames,target){
   	        if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
   	            $("input[name='file_names']").val(fileNames);
   	            var myform = $("#myform");
   	            var file_with_class = $("#file_with_class").val();
   				$("#backurl").val("<%= backurl%>" + "&file_with_class="+file_with_class);
   	            myform.submit();
   	           
   	    	}
   	        
   	    }

   	 function onlineScanner(){
 	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
 		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
 	}

   	 //图片在线显示  		 
   	 function showPictrueOnline(fileWithType,fileWithId ,currentName,productFileType,pcId){
   	   var obj = {
   	   		file_with_type:fileWithType,
   	   		file_with_id : fileWithId,
   	   		current_name : currentName ,
   	   		product_file_type:productFileType,
   	   		pc_Id : pcId,
   	   		cmd:"multiFile",
   	   		table:'product_file',
   	   		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_product")%>'
   		}
   	   if(window.top && window.top.openPictureOnlineShow){
   			window.top.openPictureOnlineShow(obj);
      		}else{
      		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
      		}
      	}		  			 
	
  	</script>
  	<style type="text/css">
  		.zebraTable td {line-height:25px;height:25px;}
		.right-title{line-height:20px;height:20px;}
		ul.ul_p_name{list-style-type:none;margin-left:-44px;}
		ul.ul_p_name li{margin-top:1px;margin-left:3px;line-height:25px;height:25px;padding-left:5px;padding-right:5px;float:left;border:1px solid silver;}
  	</style>
  </head>
 
  <body onload = "onLoadInitZebraTable()">
  
   	<form id="myform"  method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/transport/TransportProductTagTypesAction.action" %>'>
   		<input type="hidden" name="pc_id" value="<%=pc_id %>" />
   		 <input type="hidden" name="backurl" id="backurl" value=""/>
   		<input type="hidden" name="transport_id" value="<%=transport_id %>"/>
   		<table>
	   		 <tr>
	   			<td style="width:55px;">
	   				关联商品:
	   			</td>
	   			<td colspan="2" style="text-align:left;">
	   				<%
   				 	if(productRows != null && productRows.length > 0){
   				 		 %>
   					<ul class="ul_p_name" style="float:left;">	
   				 		 <% 
   				 		for(DBRow tempRow : productRows){
   				 		 %>
   				 		 	<li> <%=tempRow.getString("p_name") %></li>
   				 		 <% 
   				 		}
  						 %>
  					</ul>
  						 <% 
   				 	}
   				 %>
	   			</td>
	   		</tr>
   			<tr>
   				<td>文件类型:</td>
   				<td>
   					<select id="file_with_class" name="file_with_class" onchange="fileWithClassTypeChange();fileWithClassChangeTag();">
   					 	<%if(arraySelected != null && arraySelected.length > 0){
 									for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
 									String tempStr = arraySelected[index];
 									String tempValue = "" ;
 									String tempHtml = "" ;
 									 
 									if(tempStr.indexOf("=") != -1){
 										String[] tempArray = tempStr.split("=");
 										tempValue = tempArray[0];
 										tempHtml = tempArray[1];
 									}
 								%>		
 									<option value="<%=tempValue %>"><%=tempHtml %></option>
 
 								<%	} 
 								}
 								%>
   					</select>
   					<input type="button"  class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件" />
   					<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
   				</td>
   				<td>
   					<input type="hidden" name="file_with_class_name" id="file_with_class_name"/>
   					<input type="hidden" name="sn" value="T_product_tag"/>
		 			<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE %>" />
		 			<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_product")%>" />
   					<input type="hidden" name="file_names" />
   					
   				</td>
   				 
   			</tr>
   			<tr>
   				<td>&nbsp;</td>
   				<td colspan="2">
   				
   					第三方标签流程阶段:<span style="color:green;"><%=TransportTagKey.TAG==transportRow.get("tag_third",0)?"制签中":transportTagKey.getTransportTagById(transportRow.get("tag_third",0))%></span>
   				</td>
   			</tr>
   			<tr>
   				<td>&nbsp;</td>
   				<td colspan="2">
   					注:需要第三方标签时，在此需上传打印所需文件；<br/>
   					&nbsp;&nbsp;&nbsp;内部标签无需上传文件，请在内部标签功能中制作标签
   				</td>
   			</tr>
   		</table>
   		 
   		 <div id="tabs" style="margin-top:15px;">
		   		 	<ul>
		   		 		<%
					 		if(selectedList != null && selectedList.size() > 0){
					 			for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
					 			%>
					 				<li><a href="#transport_product_<%=index %>"> <%=selectedList.get(index) %></a></li>
					 			<% 	
					 			}
					 		}
				 		%>
		   		 	</ul>
   		 	<%
   		 		for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
   		 				List<DBRow> tempListRows = imageMap.get((index+1)+"");
   		 				
   		 			%>
   		 			 <div id="transport_product_<%= index%>">
   		 			 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
   		 			 			<tr> 
			  						<th width="55%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
			  						<th width="35%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
			  						<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
			  					</tr>
   		 			
   		 			 	<%
   		 			 		if(tempListRows != null && tempListRows.size() > 0){
   		 			 		 
   		 			 			 for(DBRow row : tempListRows){
   		 			 			%>
   		 			 				<tr>
   		 			 					<td>
   		 			 					<!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	 	                 <%if(StringUtil.isPictureFile(row.getString("file_name"))){ %>
				 			 	             <p>
				 			 	 	            <a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE%>','<%=transportRow.get("transport_id",0l) %>','<%=row.getString("file_name") %>','<%=row.get("product_file_type",01) %>','<%=pc_id %>');"><%=row.getString("file_name") %></a>
				 			 	             </p>
			 			 	                 <%}else{ %>
  		 			 					         <a href='<%= downLoadFileAction%>?file_name=<%=row.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_product")%>'><%=row.getString("file_name") %></a>
  		 			 					     <%} %>
   		 			 					</td>
   		 			 					<td>
   		 			 						<%=null != productMgr.getDetailProductByPcid(row.get("pc_id",0))?productMgr.getDetailProductByPcid(row.get("pc_id",0)).getString("p_name"):""  %>
   		 			 					</td>
   		 			 					<td>
    		 			 				 
   		 			 						<% if(transportRow.get("tag_third",0) != TransportTagKey.FINISH){ %>
   		 			 							<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
   		 			 				 		<%} else{out.print("&nbsp;");}%>	
   		 			 				 	</td>
   		 			 				</tr>	
   		 			 			<% 
   		 			 		 }
   		 			 		}else{
   		 			 			%>
 		 			 			<tr>
 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
 								</tr>
   		 			 			<%
   		 			 		}
   		 			 	%>
   		 			 	 	</table>
   		 			 </div>
   		 			<% 
   		 		}
   		 	%>
   		 	
   		 </div>
   		 

   	</form>	
  </body>
  <script type="text/javascript">
//stateBox 信息提示框
  function showMessage(_content,_state){
  	var o =  {
  		state:_state || "succeed" ,
  		content:_content,
  		corner: true
  	 };
   
  	 var  _self = $("body"),
  	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
  	_self.append(_stateBox);	
  	 
  	if(o.corner){
  		_stateBox.addClass("ui-corner-all");
  	}
  	if(o.state === "succeed"){
  		_stateBox.addClass("ui-stateBox-succeed");
  		setTimeout(removeBox,1500);
  	}else if(o.state === "alert"){
  		_stateBox.addClass("ui-stateBox-alert");
  		setTimeout(removeBox,2000);
  	}else if(o.state === "error"){
  		_stateBox.addClass("ui-stateBox-error");
  		setTimeout(removeBox,2800);
  	}
  	_stateBox.fadeIn("fast");
  	function removeBox(){
  		_stateBox.fadeOut("fast").remove();
   }
  }
  </script>
</html>
