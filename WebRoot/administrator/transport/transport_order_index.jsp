<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.ProductStorageTypeKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.DeclarationKey"%>
<%@page import="com.cwc.app.key.ClearanceKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@page import="com.cwc.app.key.LoadUnloadRelationTypeKey"%>
<%@page import="com.cwc.app.key.LoadUnloadOccupancyStatusKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.TransportRegistrationTypeKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<jsp:useBean id="purchasekey" class="com.cwc.app.key.PurchaseKey"/>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="fileWithTypeKey" class="com.cwc.app.key.FileWithTypeKey"/>
<jsp:useBean id="transportCertificateKey" class="com.cwc.app.key.TransportCertificateKey"/>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/>
 <jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey"/>
<jsp:useBean id="transportQualityInspectionKey" class="com.cwc.app.key.TransportQualityInspectionKey"/>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%> 
<%
 	String key = StringUtil.getString(request,"key");
 	
 	long send_psid = StringUtil.getLong(request,"send_psid",0);
 	long receive_psid = StringUtil.getLong(request,"receive_psid",0);
 	int status = StringUtil.getInt(request,"status",0);
 	int declaration = StringUtil.getInt(request,"declarationStatus",0);
 	int clearance = StringUtil.getInt(request,"clearanceStatus",0);
 	int invoice = StringUtil.getInt(request,"invoiceStatus",0);
 	int drawback = StringUtil.getInt(request,"drawbackStatus",0);
 	int day = StringUtil.getInt(request,"day",3);
 	String st = StringUtil.getString(request,"st");
 	String en = StringUtil.getString(request,"en");
 	int analysisType = StringUtil.getInt(request,"analysisType",0);
 	int analysisStatus = StringUtil.getInt(request,"analysisStatus",0);
 	int transport_status = StringUtil.getInt(request,"transport_status",0);
 	int stock_in_set = StringUtil.getInt(request,"stock_in_set",0);
 	long dept = StringUtil.getLong(request,"dept",0);
 	long create_account_id = StringUtil.getLong(request,"create_account_id",0);
 	long dept1 = StringUtil.getLong(request,"dept1",0);
 	long create_account_id1 = StringUtil.getLong(request,"create_account_id1",0);
 	
 	long product_line_id = StringUtil.getLong(request,"product_line_id");
 	String store_title = StringUtil.getString(request,"store_title");
 	String product_line_title = StringUtil.getString(request,"product_line_title");
 	
 	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
 	df.applyPattern("0.0");
 	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
 	
 	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(systemConfig.getIntConfigValue("order_page_count"));
 	
 	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
 	
 	String input_st_date,input_en_date;
 	if ( st.equals("") )
 	{	
 		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
 		st = input_st_date;
 	}
 	else
 	{	
 		input_st_date = st;
 	}
 							
 	if ( en.equals("") )
 	{	
 		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
 		en = input_en_date;
 	}
 	else
 	{	
 		input_en_date = en;
 	}
 	
 	String cmd = StringUtil.getString(request,"cmd");
 	int search_mode = StringUtil.getInt(request,"search_mode");
 	
 	DBRow[] rows; 
 	if(cmd!=null)
 	{
 		if(cmd.equals("filter"))
 		{
 	rows = transportMgrZJ.fillterTransport(send_psid,receive_psid,pc,status, declaration, clearance, invoice, drawback,0,stock_in_set, create_account_id);
 		}
 		else if(cmd.equals("search"))
 		{
 	rows = transportMgrZJ.searchTransportByNumber(key,search_mode,pc);
 		}
 		else if(cmd.equals("followup")) 
 		{
 	rows = transportMgrZJ.fillterTransport(send_psid,receive_psid,pc,transport_status, declaration, clearance, invoice, drawback,day,stock_in_set, create_account_id1);
 		}
 		else if(cmd.equals("analysis"))
 		{
 	rows = transportMgrLL.getAnalysis(st,en,analysisType,analysisStatus,day,pc);
 		}
 		else if(cmd.equals("ready_delivery"))
 		{
 	rows = transportMgrZJ.getNeedTrackReadyDelivery(product_line_id,pc);
 		}
 		else if(cmd.equals("tag_delivery"))
 		{
 	rows = transportMgrZJ.getNeedTrackTagDelivery(product_line_id,pc);
 		}
 		else if(cmd.equals("third_tag_delivery"))
 		{
 	rows = transportMgrZJ.getNeedTrackThirdTagDelivery(product_line_id,pc);
 		}
 		else if(cmd.equals("quality_inspection_delivery"))
 		{
 	rows = transportMgrZJ.getNeedTrackQualityInspectionDelivery(product_line_id,pc);
 		}
 		else if(cmd.equals("product_file_delivery"))
 		{
 	rows = transportMgrZJ.getNeedTrackProductFileDelivery(product_line_id,pc);
 		}
 		else if(cmd.equals("ready_send_ps")||cmd.equals("packing_send_ps")||cmd.equals("tag_send_ps")||cmd.equals("product_file_send_ps")||cmd.equals("quality_inspection_send_ps"))
 		{
 	rows = transportMgrZJ.getNeedTrackSendTransportByPsid(send_psid,cmd,pc);
 		}
 		else if(cmd.equals("intransit_receive_ps")||cmd.equals("alreadyRecive_receive_ps"))
 		{
 	rows = transportMgrZJ.getNeedTrackReceiveTransportByPsid(receive_psid,cmd,pc);
 		}
 		else if(cmd.equals("track_certificate")||cmd.equals("track_clearance")||cmd.equals("track_declaration"))
 		{
 	rows = transportMgrZJ.trackOceanShippingTransport(cmd,pc);
 		}
 		else
 		{
 	rows = transportMgrZJ.fillterTransport(0,0,pc,0,0,0,0,0,0,0,0);
 		}
 	}
 	else
 	{
 		rows = deliveryMgrZJ.getAllDeliveryOrder(pc);
 	}
 	
 	TransportOrderKey transportOrderKey = new TransportOrderKey();
 	DBRow[] accounts = applyMoneyMgrLL.getAllByReadView("accountReadViewLL");
 	DBRow[] accountCategorys = applyMoneyMgrLL.getAllByTable("accountCategoryBeanLL");
 	DBRow[] adminGroups = applyMoneyMgrLL.getAllByTable("adminGroupBeanLL");
 	DBRow[] productLineDefines = applyMoneyMgrLL.getAllByTable("productLineDefineBeanLL");
 	HashMap followuptype = new HashMap();
 	//操作日志类型:1表示进度跟进2:表示财务跟进;3修改日志;1货物状态;5运费流程;6进口清关;7出口报关8标签流程;9:单证流程
 	followuptype.put(1,"创建记录"); 
 	followuptype.put(2,"财务记录");
 	followuptype.put(3,"修改记录");
 	followuptype.put(4,"货物状态");
 	followuptype.put(5,"运费流程");
 	followuptype.put(6,"进口清关");
 	followuptype.put(7,"出口报关");
 	followuptype.put(8,"内部标签");
 	followuptype.put(9,"单证流程");
 	followuptype.put(10,"实物图片");
 	followuptype.put(11,"质检流程");
 	followuptype.put(12,"商品标签");
 	followuptype.put(13,"到货通知仓库");
 	followuptype.put(14,"第三方标签");
 	followuptype.put(15,"缷货司机签到");
 	followuptype.put(16, "装货司机签到");
 	AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean( request.getSession(true) );
 	long psId = adminLoggerBean.getPs_id();
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>转运单处理</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
.set{padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;border: 2px solid blue;} 
p{text-align:left;}
tr.split td input{margin-top:2px;}
ul.processUl{list-style-type:none;}
ul.processUl li {line-height:20px;border-bottom:1px dashed  silver;clear:both;}
ul.processUl li span.right{dispaly:block;float:right;margin-right:3px;width:57px;text-align:left;}
ul.processUl li span.left{dispaly:block;float:left;}
-->
</style>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>
 

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
 
 

<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
  
 

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
 
 <!-- 引入下啦选择 -->
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css" />
<script type="text/javascript" src="../js/easyui/jquery.easyui.menu.js"></script>
  
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>


<!-- 在线图片预览 -->
	 <script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<style>
body{font-size:12px;}	
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
span.stateName{width:15%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
.zebraTable td { border-bottom: 0px solid #DDDDDD;padding-left: 10px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
span.stateName{width:50px;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:190px;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;word-wrap:break-all;}
</style>
<script>
	function search()
	{
		var val = $("#search_key").val();
				
		if(val.trim()=="")
		{
			alert("请输入要查询的关键字");
		}
		else
		{
			var val_search = "\'"+val.toLowerCase()+"\'";
			$("#search_key").val(val_search);
			document.search_form.key.value = val_search;
			document.search_form.search_mode.value = 1;
			document.search_form.submit();
		}
	}
	
	function searchRightButton()
	{
		var val = $("#search_key").val();
				
		if (val=="")
		{
			alert("你好像忘记填写关键词了？");
		}
		else
		{
			val = val.replace(/\'/g,'');
			$("#search_key").val(val);
			document.search_form.key.value = val;
			document.search_form.search_mode.value = 2;
			document.search_form.submit();
		}
	}
	
	function eso_button_even()
	{
		document.getElementById("eso_search").oncontextmenu=function(event) 
		{  
			if (document.all) window.event.returnValue = false;// for IE  
			else event.preventDefault();  
		};  
			
		document.getElementById("eso_search").onmouseup=function(oEvent) 
		{  
			if (!oEvent) oEvent=window.event;  
			if (oEvent.button==2) 
			{  
			   searchRightButton();
			}  
		}  
	}
	
	function promptCheck(v,m,f)
	{
		if (v=="y")
		{
			 if(f.content == "")
			 {
					alert("请填写适当备注");
					return false;
			 }
			 
			 else
			 {
			 	if(f.content.length>300)
			 	{
			 		alert("内容太多，请简略些");
					return false;
			 	}
			 }
			 
			 return true;
		}
	}

	function changeType(obj) {
		$('#invoice').attr('style','display:none');
		$('#declaration').attr('style','display:none');
		$('#clearance').attr('style','display:none');
		$('#drawback').attr('style','display:none');
		if($(obj).val()==1) {
			$('#clearance').attr('style','');
		}else if($(obj).val()==2) {
			$('#declaration').attr('style','');
		}else if($(obj).val()==3) {
			$('#invoice').attr('style','');
		}else if($(obj).val()==4) {
			$('#drawback').attr('style','');
		}
	}

	function remark(transport_id)
	{
		var text = "<div id='title'>跟进</div><br />";
		text += "备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>";
		$.prompt(text,
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/transportRemarkAction.action"
										document.followup_form.transport_id.value = transport_id;
										document.followup_form.transport_content.value = "备注:"+f.content;
										document.followup_form.type.value = 1;
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
	}
	
	function followup(transport_id){
 	 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_follow_up.html"; 
 			uri += "?transport_id="+transport_id;
	 		$.artDialog.open(uri , {title: "转运单跟进["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function bookDoorOrLocation(transport_id, rel_occupancy_use)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/book_door_or_location.html?transport_id='+transport_id+'&rel_occupancy_use='+rel_occupancy_use; 
		$.artDialog.open(uri , {title: "转运单["+transport_id+"]司机签到",width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	function bookDoorOrLocationUpdateOrView(transport_id, rel_occupancy_use)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/book_door_or_location_update_or_view.html?transport_id='+transport_id+'&rel_occupancy_use='+rel_occupancy_use; 
		$.artDialog.open(uri , {title: "转运单["+transport_id+"]司机已签到",width:'800px',height:'450px', lock: true,opacity: 0.3,fixed: true
		/*
			,close:function()
			{
				$.artDialog.confirm('需要刷新转运单页面吗？', function()
				{
					window.location.reload();
				}, function(){});
			}	
		*/
		});
	}
	function goFundsTransferListPage(id)
	{
	 window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;      
	}
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}
	function goFundsTransferListPage(id)
	{
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id;      
	}
	function goApplyFunds(id)
	{
		var id="'F"+id+"'";
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=searchby_index&search_mode=1&apply_id="+id);       
	}
	$(document).ready(function(){
		//alert('');
		getLevelSelect(0, nameArray, widthArray, centerAccounts, null,vname,'<%=dept%>');
		getLevelSelect(1, nameArray, widthArray, centerAccounts, $("#dept"),vname,'<%=create_account_id%>');
		getLevelSelect(0, nameArray1, widthArray1, centerAccounts, null,vname1,'<%=dept1%>');
		getLevelSelect(1, nameArray1, widthArray1, centerAccounts, $("#dept1"),vname1,'<%=create_account_id1%>');
	});
	<%//level_id,value,name
		String str = "var centerAccounts = new Array(";
		str += "new Array('01',0,'选择部门')";
		str += ",new Array('01.0',0,'选择职员')";
		for(int i=0;i<adminGroups.length;i++) {//部门
			DBRow adminGroup = adminGroups[i];
			str += ",new Array('0"+(i+2)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
			str += ",new Array('0"+(i+2)+".0',0,'选择职员')";
			for(int ii=0;ii<accounts.length;ii++) {
				DBRow account = accounts[ii];
				if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
					str += ",new Array('0"+(i+2)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
			}
		}
		str+= ");";
		out.println(str);
	%>
	var nameArray = new Array('dept','create_account_id');
	var widthArray = new Array(120,120);
	var vname = new Array('nameArray','widthArray','centerAccounts','vname');
	var nameArray1 = new Array('dept1','create_account_id1');
	var widthArray1 = new Array(120,120);
	var vname1 = new Array('nameArray1','widthArray1','centerAccounts','vname1');
	function getLevelSelect(level,nameArray, widthArray, selectArray,o,vnames,value) {
			if(level<nameArray.length) {
				var name = nameArray[level];
				var width = widthArray[level];
				var levelId = o==null?"":$("option:selected",o).attr('levelId');
				var onchangeStr = "";
				if(level==nameArray.length-1)
					onchangeStr = "";
				else
					onchangeStr = "getLevelSelect("+(level+1)+","+vnames[0]+","+vnames[1]+","+vnames[2]+",this,"+vnames[3]+")";
				var selectHtml = "&nbsp;&nbsp;<select name='"+name+"' id='"+name+"' style='width:"+width+"px;' onchange="+onchangeStr+"> ";
				//alert(selectArray);
				for(var i=0;i<selectArray.length;i++) {
					if(levelId!="") {
						var levelIdChange = selectArray[i][0].replace(levelId+".");
						var levelIds = levelIdChange.split(".");	
						//alert(levelIdChange);
						if(levelIdChange!=selectArray[i][0]&&levelIds.length==1){
							//alert(levelId+",value="+selectArray[i][0]+",change='"+levelIdChange+"',"+levelIds.length);
							selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
						}
					}
					else {
						var levelIdChange = selectArray[i][0];
						//alert(levelIdChange);
						var levelIds = levelIdChange.split(".");
						if(levelIds.length==1){
							//alert(levelId+","+selectArray[i][0]+levelId1);
							selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
						}
					}
				}
				selectHtml += "</select>";
				$('#'+name+'_div').html('');
				$('#'+name+'_div').append(selectHtml);
				//alert(selectHtml);
				$('#'+name).val(value);
				getLevelSelect(level+1,nameArray,widthArray,centerAccounts,$('#'+name),vnames);
			}
	}
	jQuery(function($){
 
		addAutoComplete($("#search_key"),
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/transport/GetSearchTransportJSONAction.action",
				"merge_field","transport_id");
	})
	
	function uploadFile(association_id,association_type)
	{
		var url = "transport_upload_image.html?association_id="+association_id+"&association_type="+association_type;
		$.artDialog.open(url, {title: '交货单文件',width:'700px',height:'600px',lock: true,opacity: 0.3,fixed: true});
		//tb_show('创建交货单','administrator_create_delivery.html?TB_iframe=true&height=500&width=700',false);
	}
	
	function addDeliveryTransportOrder()
	{
		var url = "add_purchase_transport.html";
		$.artDialog.open(url, {title: '交货单信息',width:'700px',height:'600px', lock: true,opacity: 0.3,fixed: true});
		//tb_show('创建交货单','administrator_create_delivery.html?TB_iframe=true&height=500&width=700',false);
	}
	function refreshWindow(){
		go($("#jump_p2").val());
	}
	function transport_certificate(transport_id,file_with_type){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_certificate.html"; 
		uri += "?transport_id="+transport_id + "&file_with_type="+file_with_type;
		$.artDialog.open(uri, {title: '单证流程',width:'700px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	function clearanceButton(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_clearance.html?transport_id="+transport_id; 
		$.artDialog.open(uri , {title: "清关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function declarationButton(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_declaration.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "报关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function stock_in_set(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_stockinset.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "运费流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function quality_inspectionKey(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_quality_inspection.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "质检流程["+transport_id+"]",width:'570px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	}
	function product_file(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_file.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "实物图片["+transport_id+"]",width:'850px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	function tag(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_tag.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "跟进内部标签["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function tagThird(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_third_tag.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "跟进第三方标签["+transport_id+"]",width:'570px',height:'290px', lock: true,opacity: 0.3,fixed: true});
	}
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
	
	function transportTrackCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'first'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg += "<a  href='javascript:trackDeliveryCount()'>工厂交货("+data.track_delivery_count+")</a>&nbsp;&nbsp;";
				mesg += "<a  href='javascript:trackSendCount()'>仓库发货("+data.track_send_count+")</a>&nbsp;&nbsp;";
				mesg += "<a  href='javascript:trackReciveCount()'>仓库收货("+data.track_recive_count+")</a>&nbsp&nbsp;";
				mesg += "<a href='javascript:trackOceanShippingCount()'>海运("+data.ocean_shipping_count+")";
				$("#transport_followup").html(mesg);
			}		
		});
	}
	function trackOceanShippingCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'ocean_shipping'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>海运:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:needTrackOceanShipping(\""+data[i].cmd+"\")'>"+data[i].track_title+"("+data[i].track_count+")</a>&nbsp;&nbsp;"
				});
				$("#transport_followup").html(mesg);
			}		
		});
		
		
	}
	
	function trackDeliveryCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'ready_delivery'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>产品线:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:needTrackDeliveryProductLine("+data[i].product_line_id+",\""+data[i].product_line_name+"\")'>"+data[i].product_line_name+"("+data[i].track_count+")</a>&nbsp;&nbsp;"
				});
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function trackSendCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'send_store_transport'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>仓库:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:trackSendCountByPsid("+data[i].ps_id+",\""+data[i].title+"\")'>"+data[i].title+"("+data[i].need_track_send_count+")</a>&nbsp;&nbsp;"
				});
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function trackSendCountByPsid(ps_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:
			{
				type:'send_store_transport_ps',
				ps_id:ps_id
			},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:trackSendCount()'>"+title+"</a>:&nbsp;&nbsp;";
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"ready_send_ps\",\""+title+"\")'>备货中("+data.need_track_ready_transprot_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"packing_send_ps\",\""+title+"\")'>装箱中("+data.need_track_packing_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"tag_send_ps\",\""+title+"\")'>内部标签("+data.need_track_tag_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"tag_send_ps\",\""+title+"\")'>第三方标签("+data.need_track_third_tag_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"product_file_send_ps\",\""+title+"\")'>实物图片("+data.need_track_product_file_transport_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackSendTransport("+data.ps_id+",\"quality_inspection_send_ps\",\""+title+"\")'>质检报告("+data.need_track_quality_inspection_transport_count+")</a>&nbsp;&nbsp;"
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function trackReciveCount()
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{type:'recive_store_transport'},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:transportTrackCount()'>仓库:</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a  href='javascript:trackReciveCountByPsid("+data[i].ps_id+",\""+data[i].title+"\")'>"+data[i].title+"("+data[i].need_track_recive_count+")</a>&nbsp;&nbsp;"
				});
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function trackReciveCountByPsid(ps_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportTrackCount.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:
			{
				type:'recive_store_transport_ps',
				ps_id:ps_id
			},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg += "<a  href='javascript:trackReciveCount()'>"+title+"</a>:&nbsp;&nbsp;";
				mesg += "<a  href='javascript:needTrackReceiveTransport("+data.ps_id+",\"intransit_receive_ps\",\""+title+"\")'>运输中("+data.need_track_intransit_count+")</a>&nbsp;&nbsp;"
				mesg += "<a  href='javascript:needTrackReceiveTransport("+data.ps_id+",\"alreadyRecive_receive_ps\",\""+title+"\")'>到货未入库("+data.need_track_alreadyrecive_transport_count+")</a>&nbsp;&nbsp;"
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function needTrackDeliveryProductLine(product_line_id,title)
	{
		var mesg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/deliveryTrackByProductLine.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{product_line_id:product_line_id},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				mesg +="<a  href='javascript:trackDeliveryCount()'>"+title+":</a>&nbsp;&nbsp;"
				$.each(data,function(i)
				{
					mesg += "<a href='javascript:needTrackDelivery("+product_line_id+",\""+title+"\",\""+data[i].cmd+"\")'>"+data[i].track_tilte+"("+data[i].track_count+")</a>&nbsp;&nbsp;"
				});
				$("#transport_followup").html(mesg);
			}		
		});
	}
	
	function needTrackDelivery(product_line_id,product_line_title,cmd)
	{
		//alert(cmd);
		document.track_form.product_line_id.value = product_line_id;
		document.track_form.cmd.value = cmd;
		document.track_form.product_line_title.value = product_line_title
		document.track_form.submit();
	}
	
	function needTrackOceanShipping(type)
	{
		document.track_form.cmd.value = type;
		document.track_form.submit();
	}
	
	function needTrackSendTransport(ps_id,type,title)
	{
		document.track_form.send_psid.value = ps_id;
		document.track_form.cmd.value = type;
		document.track_form.store_title.value = title;
		document.track_form.submit();
	}
	
	function needTrackReceiveTransport(ps_id,type,title)
	{
		document.track_form.receive_psid.value = ps_id;
		document.track_form.cmd.value = type;
		document.track_form.store_title.value = title;
		document.track_form.submit();
	}
/*	function produresInfo(transport_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/test_produres.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '流程测试['+transport_id+'] ',width:'800px',height:'600px', lock: true,opacity: 0.3});
	}
	function produresInfoUpdate(transport_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/test_produres_update.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '流程测试更新['+transport_id+'] ',width:'800px',height:'600px', lock: true,opacity: 0.3});
	}*/
	function goodsArriveDelivery(transport_id)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_goods_arrive_delivery.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '到货派送['+transport_id+"]",width:'500px',height:'200px', lock: true,opacity: 0.3});
	}
	function inbound(transport_id){
		 showPictrueOnline('<%= FileWithTypeKey.TRANSPORT_RECEIVE%>',transport_id,"",0,"transport");
	}
	function outbound(transport_id){
			 showPictrueOnline('<%= FileWithTypeKey.TRANSPORT_OUTBOUND%>',transport_id,"",0,"transport");
	
	}
	function showPictrueOnline(fileWithType,fileWithId ,currentName,productFileType,uploadPath){
	   var obj = {
	   		file_with_type:fileWithType,
	   		file_with_id : fileWithId,
	   		current_name : currentName ,
	   		product_file_type:productFileType,
 	   		cmd:"multiFile",
	   		table:'file',
	   		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+uploadPath
		}
	   if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}		
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br/>
<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#transport_search">常用工具</a></li>
			<li><a href="#transport_filter">高级搜索</a></li>

			<li><a href="#transport_followup">需跟进</a></li>
			<!--
			<li><a href="#transport_analysis">转运单监控</a></li>
			-->
		</ul>
		<div id="transport_search">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
									 <input name="search_key" type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="search_key" onkeydown="if(event.keyCode==13)search()" value="<%=key%>"/>
									</div>
								</td>
								<td width="67">
									 
								</td>
							</tr>
						</table>
						<script>eso_button_even();</script>
				</td>
	              <td width="33%"></td>
	              <td width="24%" align="right" valign="middle">
	              	<tst:authentication bindAction="com.cwc.app.api.zj.DeliveryMgrZJ.addDeliveryOrder">
						<a href="javascript:addDeliveryTransportOrder();"><img src="../imgs/create_delivery_bg.jpg" width="129" height="51" border="0"/></a>
					</tst:authentication>
			  			&nbsp;&nbsp;
			  		<a href="javascript:addTransportOrder();"><img src="../imgs/cerate_transport.jpg" width="129" height="51" border="0"/></a>
				  </td>
	            </tr>
	          </table>
		</div>
		
		<div id="transport_filter">
		    <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left">
						<div style="float:left;">
						<select id="send_ps" name="send_ps">
							<option value="0">转运仓库</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("id",0l)%>" <%=send_psid==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
							<%
								}
							%>
						</select>
						&nbsp;&nbsp;
						<select id="receive_ps" name="receive_ps">
							<option value="0">目的仓库</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("id",0l)%>" <%=receive_psid==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
							<%
								}
							%>
						</select>
						&nbsp;&nbsp;
						<select id="transport_status" name="transport_status">
							<option value="0">货物状态</option>
							<option value="<%=TransportOrderKey.NOFINISH%>" <%=transport_status==TransportOrderKey.NOFINISH||transport_status==0?"selected":"" %>><%=transportOrderKey.getTransportOrderStatusById(TransportOrderKey.NOFINISH)%></option>
					 		<option value="<%=TransportOrderKey.READY%>" <%=transport_status==TransportOrderKey.READY?"selected":""%>><%=transportOrderKey.getTransportOrderStatusById(TransportOrderKey.READY)%></option>
							<option value="<%=TransportOrderKey.PACKING%>" <%=transport_status==TransportOrderKey.PACKING?"selected":""%>><%=transportOrderKey.getTransportOrderStatusById(TransportOrderKey.PACKING)%></option>
						 	<option value="<%=TransportOrderKey.INTRANSIT%>" <%=transport_status==TransportOrderKey.INTRANSIT?"selected":""%>><%=transportOrderKey.getTransportOrderStatusById(TransportOrderKey.INTRANSIT)%></option>
  						 	<option value="<%=TransportOrderKey.AlREADYARRIAL%>" <%=transport_status==TransportOrderKey.AlREADYARRIAL?"selected":""%>><%=transportOrderKey.getTransportOrderStatusById(TransportOrderKey.AlREADYARRIAL)%></option>
 							<option value="<%=TransportOrderKey.APPROVEING%>" <%=transport_status==TransportOrderKey.APPROVEING?"selected":""%>><%=transportOrderKey.getTransportOrderStatusById(TransportOrderKey.APPROVEING)%></option>
							<option value="<%=TransportOrderKey.FINISH%>" <%=transport_status==TransportOrderKey.FINISH?"selected":""%>><%=transportOrderKey.getTransportOrderStatusById(TransportOrderKey.FINISH)%></option>
						</select>
						
						<select id="stock_in_set" name="stock_in_set">
							<option value="0">运费流程</option>
							<%
								ArrayList<String> stockInsetKey = transportStockInSetKey.getStatus();
								for(String s : stockInsetKey){
									%>
									<option value='<%=s %>'><%=  transportStockInSetKey.getStatusById(Integer.parseInt(s+""))%>	</option>
									<%
								}
							%>
						</select>
						</div>
						&nbsp;&nbsp;
						<div style="float:left;" id="dept_div" name="dept_div">
						</div>
						&nbsp;&nbsp;
						<div id='create_account_id_div' name='create_account_id_div' style="float:left;">
						</div>
					</td>
				  </tr>
				  <tr>
				  	<td align="left" nowrap="nowrap" style="font-family: 宋体;font-size: 12px;">
				  <select name="declarationStatus" id="declarationStatus">
				  	<option value="0">出口报关流程</option>
	          		<%	
	          			ArrayList statuses21 = declarationKey.getStatuses();
	          			for(int i=0;i<statuses21.size();i++) {
	          				int statuse = Integer.parseInt(statuses21.get(i).toString());
	          				String key1 = declarationKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"' "+(declaration==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
	          	<select name="clearanceStatus" id="clearanceStatus">
	          		<option value="0">进口清关流程</option>
	          		<%	
	          			ArrayList statuses31 = clearanceKey.getStatuses();
	          			for(int i=0;i<statuses31.size();i++) {
	          				int statuse = Integer.parseInt(statuses31.get(i).toString());
	          				String key1 = clearanceKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"' "+(clearance==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
	          	<select name="invoiceStatus" id="invoiceStatus">
	          		<option value="0">发票流程</option>
	          		<%
	          			ArrayList statuses11 = invoiceKey.getInvoices();
	          			for(int i=0;i<statuses11.size();i++) {
	          				int statuse = Integer.parseInt(statuses11.get(i).toString());
	          				String key1 = invoiceKey.getInvoiceById(statuse);
	          				out.println("<option value='"+statuse+"' "+(invoice==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
	          	<select name="drawbackStatus" id="drawbackStatus">
	          		<option value="0">退税流程</option>
	          		<%	
	          			ArrayList statuses41 = drawbackKey.getDrawbacks();
	          			for(int i=0;i<statuses41.size();i++) {
	          				int statuse = Integer.parseInt(statuses41.get(i).toString());
	          				String key1 = drawbackKey.getDrawbackById(statuse);
	          				out.println("<option value='"+statuse+"' "+(drawback==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
						<input type="button" class="button_long_refresh" value="过滤" onclick="filter()"/></td>
				</tr>
			</table>
		</div>
						
	 	<div id="transport_followup">
	 		
	 	</div>
	</div>
</div>
<script>
 
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		show:function(event,ui)
			 {
			 	if(ui.index==2)
			 	{
			 		<%
			 			if(cmd.equals("ready_delivery")||cmd.equals("tag_delivery")||cmd.equals("quality_inspection_delivery")||cmd.equals("product_file_delivery"))
			 			{
			 		%>
			 			needTrackDeliveryProductLine(<%=product_line_id%>,"<%=product_line_title%>","<%=cmd%>");
			 		<%
			 			}
			 			else if(cmd.equals("ready_send_ps")||cmd.equals("packing_send_ps"))
			 			{
			 		%>
			 			trackSendCountByPsid(<%=send_psid%>,"<%=store_title%>")
			 		<%	
			 			}
			 			else if(cmd.equals("intransit_receive_ps")||cmd.equals("alreadyRecive_receive_ps"))
			 			{
			 		%>
			 			trackReciveCountByPsid(<%=receive_psid%>,"<%=store_title%>")
			 		<%
			 			}
			 			else
			 			{
			 		%>
			 			transportTrackCount();
			 		<%
			 			}
			 		%>
			 	}
			 }
	});
	</script>

	<br/>
	<div id="system_menu" style="display:none">
		
	</div>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true" isBottom="true">
    <tr> 
        <th width="26%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">转运单基本信息</th>
        <th width="19%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">库房及运输信息</th>
        <th width="22%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">资金情况</th>
        <th width="18%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">流程信息</th>
        <th width="15%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">跟进</th>
  	</tr>
  	<%
  		for(int i = 0 ;i<rows.length;i++)
  		{
  	%>
  		<tr align="center">
  			<td height="40" nowrap="nowrap">
  				<fieldset class="set">
  				<!-- 如果是交货行转运单那么就是Id显示成蓝色 -->
  					<%
  						String fontColor = rows[i].get("purchase_id",0l) > 0l ? "mediumseagreen;":"#f60;";
  					%>
  					<legend>
  					<a style="color:<%=fontColor %>" target="_blank" href="transport_order_detail.html?transport_id=<%=rows[i].getString("transport_id")%>">T<%=rows[i].getString("transport_id")%></a>
  					&nbsp;<%=transportOrderKey.getTransportOrderStatusById(rows[i].get("transport_status",0)) %>
  					</legend>
  					<p style="text-align:left;clear:both;">
  						<span class="stateName">创建人:</span>
  						<span class="stateValue">
  						<%
  						 	DBRow createAdmin = adminMgr.getDetailAdmin(rows[i].get("create_account_id",0l));
  						 	if(createAdmin!=null)
  						 	{
  						 		out.print(createAdmin.getString("employe_name"));
  						 	} 
  						 %> 
  						 </span>
  					</p>
  					
  					<p style="text-align:left;clear:both;">
  						<span class="stateName">TITLE:</span>
  						<span class="stateValue">
  						<%
  						DBRow titleRow = proprietaryMgrZyj.findProprietaryByTitleId(rows[i].get("title_id", 0L));
  						if(null != titleRow)
  						{  							
	  						out.println(titleRow.getString("title_name"));
  						}
  						%>
  						</span>
  					</p>
  					
  					<p style="text-align:left;clear:both;">
  						<span class="stateName">允许装箱:</span>
  						<span class="stateValue"><%
  						 	DBRow packingAdmin = adminMgr.getDetailAdmin(rows[i].get("packing_account",0l));
  						 	if(packingAdmin!=null)
  						 	{
  						 		out.print(packingAdmin.getString("employe_name"));
  						 	} 
  						 %>
  						 </span>
  					</p>
  					<p style="text-align:left;clear:both;">
  						<span class="stateName">创建时间:</span><span class="stateValue"><%=tDate.getFormateTime(rows[i].getString("transport_date"))%></span>
  					</p>
  					<p style="text-align:left;clear:both;">
  					  <span class="stateName"> 更新时间:</span><span class="stateValue"><%= tDate.getFormateTime(rows[i].getString("updatedate")) %></span>
  					</p>
  				</fieldset>
  				<%
  					DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(rows[i].getString("purchase_id"));
  					if(rows[i].get("purchase_id",0l)!=0)
  					{
  				%>
  				<fieldset class="set" style="border-color:#993300;">
  					<legend>
  						<a href="javascript:void(0)" onclick="window.open('<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail.html?purchase_id=<%=rows[i].get("purchase_id",0) %>')">P<%=rows[i].getString("purchase_id")%></a>
	  					&nbsp;
	  					<%
	  						DBRow storage = catalogMgr.getDetailProductStorageCatalogById(purchase.get("ps_id",0l));
	  						out.print(null==storage?"":storage.getString("title"));
	  					%>
  					</legend>
  					<%
	  					String styleWrap = "<p style='text-align:left;clear:both;'>";
	  					String supplierStyle = "<p style='text-align:left;clear:both;'><span class='stateName'>&nbsp;</span><span class='stateValue'>";
						try
						{
							DBRow supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(purchase.getString("supplier")));
							if(supplier!=null)
							{
								DBRow productLine = productLineMgrTJH.getProductLineById(supplier.get("product_line_id",0l));
								if(productLine !=null)
								{
									out.println(styleWrap+"<span class='stateName'>产品线:</span><span class='stateValue'>"+productLine.getString("name")+"</span></p>");
								}
								out.println(supplierStyle+supplier.getString("sup_name")+"</span></p>");
							}
						}
						catch(NumberFormatException e)
						{
							out.println(supplierStyle+purchase.getString("supplier")+"</span></p>");
						}
					%>
					<p style="text-align:left;clear:both;">
						<span class="stateName">主流程:</span>
						<span class="stateValue"><%=purchasekey.getQuoteStatusById(purchase.getString("purchase_status"))%></span>
					</p>
			  		<p style="text-align:left;clear:both;">
			  			<span class="stateName">负责人:</span>
			  			<span class="stateValue"><%=purchase.getString("proposer") %></span>
			  		</p>
			  		<p style="text-align:left;clear:both;">
			  			<span class="stateName">采购时间:</span>
			  			<span class="stateValue"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(purchase.getString("purchase_date")))%></span>
			  		</p>
			  		<p style="text-align:left;clear:both;">
			  			<span class="stateName">更新日期:</span>
			  			<span class="stateValue"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(purchase.getString("updatetime")))%></span>
			  		</p>
					<p style="text-align:left;clear:both;">
						<span class="stateName">总体积:</span>
						<span class="stateValue"><%=purchaseMgr.getPurchaseVolume(purchase.get("purchase_id",0l))%> cm³</span>
						</p>
					<p style="text-align:left;clear:both;">
						<span class="stateName">总重量:</span>
						<span class="stateValue"><%=purchaseMgr.getPurchaseWeight(purchase.get("purchase_id",0l))%> Kg</span>
					</p>
					<p style="text-align:left;clear:both;">
						<span class="stateName">总金额:</span>
						<span class="stateValue"><%=purchaseMgr.getPurchasePrice(purchase.get("purchase_id",0l))%> RMB</span>
					</p>
					<p style="text-align:left;clear:both;">
						<span class="stateName">价格确认:</span>
						<span class="stateValue"><%=purchase.get("price_affirm_over",0d)==0?df.format(tDate.getDiffDate(purchase.getString("purchase_date"),"dd"))+"天":purchase.get("price_affirm_over",0d)+"天完成"%></span>
					</p>
					<%DBRow prepareDBRow = preparePurchaseMgrZwb.getPreparePurchaseFunds(Long.valueOf(rows[i].getString("purchase_id")));
		               if(prepareDBRow!=null){ 
		                  if(prepareDBRow.getString("currency").equals("USD")){ 
				    %>
				               <p style="text-align:left;clear:both;"><span class="stateName">预申请金额：</span><span class="stateValue"><%=prepareDBRow.getString("amount")%><%=prepareDBRow.getString("currency")%>/<%=prepareDBRow.getString("standard_money")%>RMB</span></p>
			                <%}else{%>
			                   <p style="text-align:left;clear:both;"><span class="stateName">预申请金额：</span><span class="stateValue"><%=prepareDBRow.getString("amount")%><%=prepareDBRow.getString("currency")%></span></p>
			                <%} %>   
	               <%} %>
  				</fieldset>
	  				<%
	  					DBRow[] transportOrderRows = transportMgrZyj.getTransportRowsByPurchaseId(Long.valueOf(rows[i].getString("purchase_id")));
	  					for(int tr = 0; tr < transportOrderRows.length; tr ++)
	  					{
	  						if(!rows[i].getString("transport_id").equals(transportOrderRows[tr].getString("transport_id")))
	  						{
					%>
			  				<fieldset class="set" style='border-color:#993300;'>
			  					<legend>
			  					<a style="color:mediumseagreen;" target="_blank" href="transport_order_detail.html?transport_id=<%=transportOrderRows[tr].getString("transport_id")%>">T<%=transportOrderRows[tr].getString("transport_id")%></a>
			  					&nbsp;<%=transportOrderKey.getTransportOrderStatusById(transportOrderRows[tr].get("transport_status",0)) %>
			  					</legend>
			  					<p style="text-align:left;clear:both;">
			  						<span class="stateName">创建人:</span>
			  						<span class="stateValue">
			  						<%
			  						 	DBRow createAdminTransprot = adminMgr.getDetailAdmin(transportOrderRows[tr].get("create_account_id",0l));
			  						 	if(createAdminTransprot!=null)
			  						 	{
			  						 		out.print(createAdminTransprot.getString("employe_name"));
			  						 	} 
			  						 %> 
			  						 </span>
			  					</p>
			  					<p style="text-align:left;clear:both;">
			  						<span class="stateName">TITLE:</span>
			  						<span class="stateValue">
			  						<%
				  					DBRow relateDeliveryTitleRow = proprietaryMgrZyj.findProprietaryByTitleId(rows[i].get("title_id", 0L));
			  						if(null != relateDeliveryTitleRow)
			  						{  							
				  						out.println(relateDeliveryTitleRow.getString("title_name"));
			  						}
			  						%>
			  						</span>
			  					</p>
			  					<p style="text-align:left;clear:both;">
			  						<span class="stateName">允许装箱:</span>
			  						<span class="stateValue"><%
			  						 	DBRow packingAdminTransprot = adminMgr.getDetailAdmin(transportOrderRows[tr].get("packing_account",0l));
			  						 	if(packingAdminTransprot!=null)
			  						 	{
			  						 		out.print(packingAdminTransprot.getString("employe_name"));
			  						 	} 
			  						 %>
			  						 </span>
			  					</p>
			  					<p style="text-align:left;clear:both;">
			  						<span class="stateName">创建时间:</span><span class="stateValue"><%=tDate.getFormateTime(transportOrderRows[tr].getString("transport_date"))%></span>
			  					</p>
			  					<p style="text-align:left;clear:both;">
			  					  <span class="stateName"> 更新时间:</span><span class="stateValue"><%= tDate.getFormateTime(transportOrderRows[tr].getString("updatedate")) %></span>
			  					</p>
			  				</fieldset>
	  				<%		
	  						}
	  					}
  					}
	  				%>
  			</td>
  			<td align="left">
  				<p>
  				提货仓库:
  				<% 
  					int fromPsType = rows[i].get("from_ps_type",0);
  					//如果是供应商的Type那么就需要去查询供应商的名称
 	  				if(fromPsType == ProductStorageTypeKey.SupplierWarehouse){
 	  					DBRow temp = supplierMgrTJH.getDetailSupplier(rows[i].get("send_psid",0l));
 	  					if(temp != null){
 	  						out.println(temp.getString("sup_name"));
 	  					}
 	  				}
 	  				else
 	  				{
 	  					DBRow storageCatalog = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("send_psid",0l));
 	  					if(storageCatalog != null){
 	  						out.print(storageCatalog.getString("title"));
 	  					}
 	  				}
  				%>
  				</p>
  				<p>
  					收货仓库:<%=null!=catalogMgr.getDetailProductStorageCatalogById(rows[i].get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(rows[i].get("receive_psid",0l)).getString("title"):""%> 
  				</p>
  				<p>
  					&nbsp;&nbsp;&nbsp;&nbsp;收货人:
  					<%	 
  						long deliveryer_id = rows[i].get("deliveryer_id",0l);
  						if(deliveryer_id != 0L){
  							out.println(null!=adminMgr.getDetailAdmin(deliveryer_id)?adminMgr.getDetailAdmin(deliveryer_id).getString("employe_name"):"");
  						}
  					%>
  				</p>
  				<p>
  					收货时间:<%
  						String deliveryTime = rows[i].getString("deliveryed_date");
  						 out.print(deliveryTime.length() > 10 ?deliveryTime.substring(0,10): deliveryTime);
  					%>
  				</p>
  				<p>
  					&nbsp;&nbsp;&nbsp;&nbsp;入库人:<%
  						long warehousinger_id = rows[i].get("warehousinger_id",0l);
						if(warehousinger_id != 0L){
							out.println(adminMgr.getDetailAdmin(warehousinger_id).getString("employe_name"));
						}
  					%>
  				</p>
  				<p style="border-bottom:1px dashed silver;margin-bottom: 5px;">
  					入库时间:
  					<%  
  						String warehousingTime = rows[i].getString("warehousing_date");
  						out.print(warehousingTime.length() > 10 ?warehousingTime.substring(0,10): warehousingTime);
  					%>
  				</p>
  				<p style="margin-top: 5px;">运单号:<%=rows[i].getString("transport_waybill_number")%></p>
  				<p>货运公司:<%=rows[i].getString("transport_waybill_name")%></p>
  				<p>承运公司:<%=rows[i].getString("carriers")%></p>
  				<%
  					long transport_send_country = rows[i].get("transport_send_country",0l);
  					DBRow send_country_row = transportMgrLL.getCountyById(Long.toString(transport_send_country));
  					long transport_receive_country = rows[i].get("transport_receive_country",0l);
  					DBRow receive_country_row = transportMgrLL.getCountyById(Long.toString(transport_receive_country));					
  				%>
  				<p>始发国/始发港:<%=send_country_row==null?"无":send_country_row.getString("c_country")%>/<%=rows[i].getString("transport_send_place").equals("")?"无":rows[i].getString("transport_send_place")%></p>
  				<p>目的国/目的港:<%=receive_country_row==null?"无":receive_country_row.getString("c_country")%>/<%=rows[i].getString("transport_receive_place").equals("")?"无":rows[i].getString("transport_receive_place")%></p>
  				<p>总体积:<%=transportMgrZJ.getTransportVolume(rows[i].get("transport_id",0l))%> cm³</p>
  				<p>总重量:<%=transportMgrZJ.getTransportWeight(rows[i].get("transport_id",0L)) %> Kg</p>
  				<p>总金额:<%=transportMgrZJ.getTransportSendPrice(rows[i].get("transport_id",0L)) %> RMB</p>
 			<%
			if(rows[i].get("stock_in_set",1)==1) {
				out.println("<font color='red'><b>");
			}
			%>
  			</td>
  			<td>
			<%
			if(!"".equals(rows[i].getString("purchase_id")) && !rows[i].getString("purchase_id").equals("0"))
			{
		%>
  				<fieldset class="set" style="border-color:#993300;">
			<%
				DBRow[] applyMoneysPurchase = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100009,rows[i].get("purchase_id",0L),FinanceApplyTypeKey.PURCHASE_ORDER);
					//applyMoneyMgrLL.getApplyMoneyByBusiness(rows[i].getString("purchase_id"),4);//
			%>
			<legend style="font-size:12px;font-weight:normal;color:#000000;background:none;">
				<span style="font-size:12px;color:#000000;font-weight:normal;">
				<a href="javascript:void(0)" onclick="window.open('<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail.html?purchase_id=<%=rows[i].get("purchase_id",0) %>')">P<%=rows[i].getString("purchase_id")%></a>
				<%
					String statusName = "";
					if(applyMoneysPurchase.length > 0)
					{
						List imageList = applyMoneyMgrLL.getImageList(applyMoneysPurchase[0].getString("apply_id"),"1");
						//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
					 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneysPurchase[0].getString("status")) && imageList.size() < 1)
					 	{
					 		statusName = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
					 	}else{
					 		statusName = fundStatusKey.getFundStatusKeyNameById(applyMoneysPurchase[0].getString("status"));	
						}
					 	statusName = "("+statusName+")";
					}
					
					if(rows[i].get("transport_id",0)==101291)
		  			{
		  				int c = 1;
		  				//System.out.println(c);
		  			}
				%>
				采购单定金
				(<%
					if(purchase!=null)
					{
						if(purchase.get("apply_money_over",0d)==0)
						{
							out.print(df.format(tDate.getDiffDate(purchase.getString("purchase_date"),"dd"))+"天");
						}
						else
						{
							out.print(purchase.get("apply_money_over",0d)+"天完成");
						}
					}
				%>)
				</span>
			</legend>
			<table style="width:100%">
				<tr style='width:100%'>
					<td style='width:100%'>采购单金额:<%=purchaseMgr.getPurchasePrice(rows[i].get("purchase_id",0l))%>RMB</td>
				</tr>
				<%
	  				if(applyMoneysPurchase.length>0) {
	  					String moneyStandardStr = "";
	  					if(!"RMB".equals(applyMoneysPurchase[0].getString("currency")))
	  					{
	  						moneyStandardStr = "/"+applyMoneysPurchase[0].getString("standard_money")+"RMB";
	  					}
	  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneysPurchase[0].get("apply_id",0l)+")\">F"+applyMoneysPurchase[0].get("apply_id",0)+"</a>"+ statusName +applyMoneysPurchase[0].get("amount",0f)+applyMoneysPurchase[0].getString("currency")+moneyStandardStr+"</td></tr>");
	  					DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneysPurchase[0].getString("apply_id"));
		 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
			  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
			  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
			  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
			  				int moneyStatus = applyTransferRows[ii].get("status",0);
			  				String transferMoneyStandardStr = "";
			  				if(!"RMB".equals(currency))
			  				{
			  					transferMoneyStandardStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
			  				}
			  				if(moneyStatus != 0 )
				  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
			  				else
				  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
				  			}
			  			}
	  		%>
			</table>
			</fieldset>
			<fieldset class="set" style='border-color:blue;'>
				<%
						String statusNameTrSelf = "";
						String appMoneyStateStrSelf = "";
						DBRow[] applyMoneySelf = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,rows[i].get("transport_id",0L),FinanceApplyTypeKey.DELIVERY_ORDER);
							if(applyMoneySelf.length > 0)
							{
								List imageListTrSelf = applyMoneyMgrLL.getImageList(applyMoneySelf[0].getString("apply_id"),"1");
								//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
							 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneySelf[0].getString("status")) && imageListTrSelf.size() < 1)
							 	{
							 		statusNameTrSelf = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
							 	}else{
							 		statusNameTrSelf = fundStatusKey.getFundStatusKeyNameById(applyMoneySelf[0].getString("status"));	
	 							}
							 	statusNameTrSelf = "("+statusNameTrSelf+")";
								appMoneyStateStrSelf = applyMoneySelf[0].get("amount",0.0) + applyMoneySelf[0].getString("currency");
	 						}
	 				%>
					<legend>
						<a target="_blank" href='<%=ConfigBean.getStringValue("systenFolder") %>administrator/transport/transport_order_detail.html?transport_id=<%=rows[i].getString("transport_id") %>'>T<%=rows[i].getString("transport_id") %></a>
						交货单货款
					</legend>
					<table style="width:100%">
						<tr><td>交货单金额:<%=transportMgrZJ.getTransportSendPrice(rows[i].get("transport_id",0L)) %>RMB</td></tr>
 					<%
						if(applyMoneySelf.length > 0)
						{
							String moneyStandardDeliverStrSelf = "";
		  					if(!"RMB".equals(applyMoneySelf[0].getString("currency")))
		  					{
		  						moneyStandardDeliverStrSelf = "/"+applyMoneySelf[0].getString("standard_money")+"RMB";
		  					}
							out.println("<tr><td><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneySelf[0].get("apply_id",0l)+")\">F"+applyMoneySelf[0].get("apply_id",0)+"</a>"+ statusNameTrSelf + appMoneyStateStrSelf+moneyStandardDeliverStrSelf+"</td></tr>");
							DBRow[] applyTransferRowsSelf = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneySelf[0].getString("apply_id"));
				  			for(int ii=0;applyTransferRowsSelf!=null && ii<applyTransferRowsSelf.length;ii++) {
				  				String transfer_idSelf = applyTransferRowsSelf[ii].getString("transfer_id")==null?"":applyTransferRowsSelf[ii].getString("transfer_id");
				  				String amountSelf = applyTransferRowsSelf[ii].getString("amount")==null?"":applyTransferRowsSelf[ii].getString("amount");
				  				String currencySelf = applyTransferRowsSelf[ii].getString("currency")==null?"":applyTransferRowsSelf[ii].getString("currency");
				  				int moneyStatusSelf = applyTransferRowsSelf[ii].get("status",0);
				  				String transferMoneyStandardDeliverStrSelf = "";
				  				if(!"RMB".equals(currencySelf))
				  				{
				  					transferMoneyStandardDeliverStrSelf = "/"+applyTransferRowsSelf[ii].getString("standard_money")+"RMB";
				  				}
			  					if(moneyStatusSelf != 0 )
				  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_idSelf+"')\">W"+transfer_idSelf+"</a>转完"+amountSelf+" "+currencySelf+transferMoneyStandardDeliverStrSelf+"</td></tr>");
				  				else
				  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_idSelf+"')\">W"+transfer_idSelf+"</a>申请"+amountSelf+" "+currencySelf+transferMoneyStandardDeliverStrSelf+"</td></tr>");
								}
						}
					%>
					</table>
				</fieldset>	
	  			<%
	  				DBRow[] deliveryOrderRows = transportMgrZyj.getTransportRowsByPurchaseId(Long.valueOf(rows[i].getString("purchase_id")));
					if(deliveryOrderRows.length > 0)
					{
						for(int de = 0; de < deliveryOrderRows.length; de ++)
						{
							if(!rows[i].getString("transport_id").equals(deliveryOrderRows[de].getString("transport_id")))
							{
							
								String statusNameTr = "";
								String appMoneyStateStr = "";
								String selfColor = "";
								DBRow[] applyMoneyDelivers = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,deliveryOrderRows[de].get("transport_id",0L),FinanceApplyTypeKey.DELIVERY_ORDER);
									if(applyMoneyDelivers.length > 0)
									{
										List imageListTr = applyMoneyMgrLL.getImageList(applyMoneyDelivers[0].getString("apply_id"),"1");
										//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
									 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneyDelivers[0].getString("status")) && imageListTr.size() < 1)
									 	{
									 		statusNameTr = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
									 	}else{
									 		statusNameTr = fundStatusKey.getFundStatusKeyNameById(applyMoneyDelivers[0].getString("status"));	
			 							}
									 	statusNameTr = "("+statusNameTr+")";
										appMoneyStateStr = applyMoneyDelivers[0].get("amount",0.0) + applyMoneyDelivers[0].getString("currency");
			 						}
			 	%>
							<fieldset class="set" style='border-color:#993300;'>
								<legend>
									<a target="_blank" href='<%=ConfigBean.getStringValue("systenFolder") %>administrator/transport/transport_order_detail.html?transport_id=<%=deliveryOrderRows[de].getString("transport_id") %>'>T<%=deliveryOrderRows[de].getString("transport_id") %></a>
									交货单货款
								</legend>
								<table style="width:100%">
									<tr><td>交货单金额:<%=transportMgrZJ.getTransportSendPrice(deliveryOrderRows[de].get("transport_id",0L)) %>RMB</td></tr>
		  					<%
									if(applyMoneyDelivers.length > 0)
									{
										String moneyStandardDeliverStr = "";
					  					if(!"RMB".equals(applyMoneyDelivers[0].getString("currency")))
					  					{
					  						moneyStandardDeliverStr = "/"+applyMoneyDelivers[0].getString("standard_money")+"RMB";
					  					}
										out.println("<tr><td><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneyDelivers[0].get("apply_id",0l)+")\">F"+applyMoneyDelivers[0].get("apply_id",0)+"</a>"+ statusNameTr + appMoneyStateStr+moneyStandardDeliverStr+"</td></tr>");
										DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneyDelivers[0].getString("apply_id"));
							  			for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
							  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
							  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
							  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
							  				int moneyStatus = applyTransferRows[ii].get("status",0);
							  				String transferMoneyStandardDeliverStr = "";
							  				if(!"RMB".equals(currency))
							  				{
							  					transferMoneyStandardDeliverStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
							  				}
						  					if(moneyStatus != 0 )
							  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardDeliverStr+"</td></tr>");
							  				else
							  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardDeliverStr+"</td></tr>");
	  								}
									}
								%>
								</table>
							</fieldset>	
					<%		
				
							}
						}
					}
	  					%>
		<%
			}
		%>
  				<fieldset class="set" style="border-color:green;">
					<legend>运费:<%=computeProductMgrCCC.getSumTransportFreightCost(rows[i].getString("transport_id")).get("sum_price",0d) %>RMB</legend>
  						<%
						if(rows[i].get("stock_in_set",1)==1) {
							out.println("</b></font>");
						}
					%>
					<table style="width:100%" align="left">
					<%
						//如果采购单ID为0，为转运单，否则为交货单
						int associateTypeId = 0 == rows[i].get("purchase_id",0L)?6:5;
						DBRow[] applyMoneys = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(10015,rows[i].get("transport_id",0L),associateTypeId);
							//applyMoneyMgrLL.getApplyMoneyByBusiness(rows[i].getString("transport_id"),associateTypeId);//
						for(int j=0; j<applyMoneys.length; j++){
							String freightCostStr = "";
							if(!"RMB".equals(applyMoneys[0].getString("currency")))
							{
								freightCostStr = "/"+applyMoneys[j].get("standard_money",0f)+"RMB";
							}
					%>	
							<tr><td align='left'>
							<a href="javascript:void(0)" onClick='goApplyFunds(<%=applyMoneys[j].get("apply_id",0) %>)'>F<%=applyMoneys[j].get("apply_id",0) %></a>
									<%
										List imageListStock = applyMoneyMgrLL.getImageList(applyMoneys[j].getString("apply_id"),"1");
										String stockStatusName = "";
										//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
									 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneys[j].getString("status")) && imageListStock.size() < 1)
									 	{
									 		stockStatusName = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
									 	}else{
									 		stockStatusName = fundStatusKey.getFundStatusKeyNameById(applyMoneys[j].getString("status"));	
									 	}
									 	stockStatusName = "("+stockStatusName+")";
  								 %>
									<%=stockStatusName %>
									<%=applyMoneys[j].get("amount",0f) %><%=applyMoneys[0].getString("currency") %><%=freightCostStr %>
								</td></tr>
  								 <% 
							 			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneys[j].getString("apply_id"));
							 				//applyMoneyMgrLL.getApplyTransferByBusiness(rows[i].getString("transport_id"),6);
							 			if(applyTransferRows.length>0) {
							 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
							 					if(applyTransferRows[ii].get("apply_money_id",0) == applyMoneys[j].get("apply_id",0)) {
								  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
								  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
								  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
								  				int moneyStatus = applyTransferRows[ii].get("status",0);
								  				String transferMoneyStandardFreightStr = "";
								  				if(!"RMB".equals(currency))
								  				{
								  					transferMoneyStandardFreightStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
								  				}
								  				if(moneyStatus != 0 )
								  					out.println("<tr><td align='left'><a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardFreightStr+"</td></tr>");
								  				else
								  					out.println("<tr><td align='left'><a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardFreightStr+"</td></tr>");
								  				}	
							 				}
  								 }
  							 }
  						%>
				 	</table>
			 	</fieldset>
  			</td>
  			<td nowrap="nowrap" style="text-align:left;">
  			<!--   对于有的流程 运费,清关,报关有了跟进中的时候才显示 花费多长的时间 -->
  				<!-- 流程的跟进如果是完成+ 已经上传文件的显示绿色。没有上传文件的就是红色。完成了的用粗体显示 -->
  				<ul class="processUl">
  				 
  					<li>
						<span class="left">货物状态:
						<%=transportOrderKey.getTransportOrderStatusById(rows[i].get("transport_status",0)) %>
						<%
  							 DBRow[] transportPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 4);
  							 if(transportPersons != null && transportPersons.length > 0){
  						%>
  							<br/>
  						<%
  								 for(DBRow tempUserRow : transportPersons){
  								 %>
  								 	<%= tempUserRow.getString("employe_name")%>
  								 <% 
  								 }
  							 }
  						%>
						</span>
						<span class="right" style="">
						<%=rows[i].getString("all_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("all_over")+"天完成"%>
						</span>
  					</li>
  					<li>
  						<span class="left">
 	  						实物图片:
 	  						<%
	  						//计算颜色
	  						String productFileClass = "" ;
  							int productFileInt = rows[i].get("product_file",TransportProductFileKey.NOPRODUCTFILE);
  							if(productFileInt == TransportProductFileKey.FINISH){
  								productFileClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("product_file",rows[i].get("transport_id",0l),FileWithTypeKey.product_file);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									productFileClass += " fontGreen";
  								}else{
  									productFileClass += " fontRed";
  								}
  							}else if(productFileInt != TransportProductFileKey.NOPRODUCTFILE){
  								productFileClass += "spanBold spanBlue";
  							}
  							%>
 	  						<span class="<%=productFileClass %>"><%=transportProductFileKey.getStatusById(rows[i].get("product_file",TransportProductFileKey.NOPRODUCTFILE)) %></span>
	  						<%
	  							 DBRow[] productFilePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 10);
	  							 if(productFilePersons != null && productFilePersons.length > 0){
  							%>
  	  							<br/>
  	  						<%		 
	  								 for(DBRow tempUserRow : productFilePersons){
	  								 %>
	  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
	  								 <% 
	  								 }
	  							 }
  							%>
	  					</span>
	  					<span class="right">
	  			<%=rows[i].get("product_file",TransportProductFileKey.NOPRODUCTFILE) == TransportProductFileKey.NOPRODUCTFILE?"":(rows[i].getString("product_file_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("product_file_over")+"天完成")%>
  						</span>
  					</li>
  					<li>
  						<span class="left">
  							内部标签:
  							<%
	  						//计算颜色
	  						String tagClass = "" ;
  							int tagInt = rows[i].get("tag",TransportTagKey.NOTAG);
  							if(tagInt == TransportTagKey.FINISH){
  								tagClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.TRANSPORT_TAG);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									tagClass += " fontGreen";
  								}else{
  									tagClass += " fontRed";
  								}
  							}else if(tagInt != TransportTagKey.NOTAG){
  								tagClass += "spanBold spanBlue";
  							}
	  					%>
  						<span class="<%=tagClass %>"><%=transportTagKey.getTransportTagById(rows[i].get("tag",TransportTagKey.NOTAG))%></span>
  							<%
  							 DBRow[] tagPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 8);
  							 if(tagPersons != null && tagPersons.length > 0){
  							%>
  	  							<br/>
  	  						<%	
  								 for(DBRow tempUserRow : tagPersons){
  								 %>
  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
  								 <% 
  								 }
  							 }
  						%>
  						</span>
  						<span class="right">	
  							<%=rows[i].get("tag",01)==1?"":(rows[i].getString("tag_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("tag_over")+"天完成")%>
  						</span>
  					</li>
  					<li>
  						<span class="left">	
	  			质检流程:
  							<%
	  						//计算颜色
	  						String qualityInspectionClass = "" ;
  							int qualityInspectionInt = rows[i].get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY);
  							if(qualityInspectionInt == TransportQualityInspectionKey.FINISH){
  								qualityInspectionClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.TRANSPORT_QUALITYINSPECTION);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									qualityInspectionClass += " fontGreen";
  								}else{
  									qualityInspectionClass += " fontRed";
  								}
  							}else if(qualityInspectionInt != TransportQualityInspectionKey.NO_NEED_QUALITY){
  								qualityInspectionClass += "spanBold spanBlue";
  							}
  							%>
  							<span class="<%=qualityInspectionClass %>"><%=transportQualityInspectionKey.getStatusById(rows[i].get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY)+"") %></span>
  							<%
	  							 DBRow[] qualityInspectionPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 11);
	  							 if(qualityInspectionPersons != null && qualityInspectionPersons.length > 0){
	  						%>
	  	  						<br/>
	  	  					<%	
	  								 for(DBRow tempUserRow : qualityInspectionPersons){
	  								 %>
	  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
	  								 <% 
	  								 }
	  							 }
  							%>
  						</span>
  						<span class="right">
	  			<%=rows[i].get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY) == TransportQualityInspectionKey.NO_NEED_QUALITY?"":(rows[i].getString("quality_inspection_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("quality_inspection_over")+"天完成")%>
  						</span>
  					</li>
  					<li>
  						<span class="left">
	  			出口报关:
	  					<%
	  						//计算颜色
	  						String declarationKeyClass = "" ;
  							int declarationInt = rows[i].get("declaration",declarationKey.NODELARATION);
  							if(declarationInt == declarationKey.FINISH){
  								declarationKeyClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.transport_declaration);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									declarationKeyClass += " fontGreen";
  								}else{
  									declarationKeyClass += " fontRed";
  								}
  							}else if(declarationInt != declarationKey.NODELARATION ){
  								declarationKeyClass +=  "spanBold spanBlue";
  							}
	  					%>
	  					<span class='<%= declarationKeyClass%>'><%=declarationKey.getStatusById(rows[i].get("declaration",TransportStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>
  						<%
  							 DBRow[] declarationPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 7);
  							 if(declarationPersons != null && declarationPersons.length > 0){
  						%>
  	  						<br/>
  	  					<%	
  								 for(DBRow tempUserRow : declarationPersons){
  								 %>
  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
  								 <% 
  								 }
  							 }
  						%>
  						</span>
  						<span class="right" style="">
 	  			<%
	  						if(rows[i].getString("declaration_over").trim().length() > 0){
	  							out.println(rows[i].getString("declaration_over")+"天完成");
	  						}else{
	  							if(rows[i].get("declaration",DeclarationKey.NODELARATION) != DeclarationKey.NODELARATION){
		  							 String time = transportMgrZr.getProcessSpendTime(rows[i].get("transport_id",0l),7,DeclarationKey.DELARATING);
		  							if(time.trim().length() > 0 ){
		  								out.println(df.format(tDate.getDiffDate(time,"dd")));
		  							}
	  							}
	  						}
	  					%>
	  					</span>
  					</li>
  					<li>
  					<span class="left">
	  			进口清关:
  						<%
	  						//计算颜色
	  						String clearanceKeyClass = "" ;
  							int clearanceInt = rows[i].get("clearance",clearanceKey.NOCLEARANCE);
  							if( clearanceInt == clearanceKey.FINISH){
  								clearanceKeyClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.transport_clearance);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									clearanceKeyClass += " fontGreen";
  								}else{
  									clearanceKeyClass += " fontRed";
  								}
  							}else if(clearanceInt != clearanceKey.NOCLEARANCE){
  								clearanceKeyClass +=  "spanBold spanBlue";
  							}
	  					%>
			  			<span class="<%= clearanceKeyClass%>"><%=clearanceKey.getStatusById(rows[i].get("clearance",ClearanceKey.NOCLEARANCE)) %></span>
			  			<%
  							 DBRow[] clearancePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 6);
  							 if(clearancePersons != null && clearancePersons.length > 0){
						%>
  							<br/>
  						<%	
  								 for(DBRow tempUserRow : clearancePersons){
  								 %>
  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
  								 <% 
  								 }
  							 }
  						%>
			  		</span>
			  		<span class="right">
	  			<%
	  						if(rows[i].getString("clearance_over").trim().length() > 0){
	  							out.println(rows[i].getString("clearance_over")+"天完成");
	  						}else{
	  							if(rows[i].get("clearance",ClearanceKey.NOCLEARANCE) != ClearanceKey.NOCLEARANCE){
		  							 String time = transportMgrZr.getProcessSpendTime(rows[i].get("transport_id",0l),7,ClearanceKey.CLEARANCEING);
		  							if(time.trim().length() > 0 ){
		  								out.println(df.format(tDate.getDiffDate(time,"dd"))+"天");
		  							}
	  							}
	  						}
	  		 	%>
			  		 	</span>
  					</li>
  					<li>
  						<span class="left">
  							运费流程:
  							<%
	  						//计算颜色
	  						String stockInSetClass = "" ;
  							int stockInSetInt = rows[i].get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET);
  							if(stockInSetInt == TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH){
  								stockInSetClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.TRANSPORT_STOCKINSET);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									stockInSetClass += " fontGreen";
  								}else{
  									stockInSetClass += " fontRed";
  								}
  							}else if(stockInSetInt != TransportStockInSetKey.SHIPPINGFEE_NOTSET){
  								stockInSetClass += "spanBold spanBlue";
  							}
  							%>
  							<span class='<%=stockInSetClass %>'><%=transportStockInSetKey.getStatusById(rows[i].get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET)) %></span>
	  						<%
	  							 DBRow[] stockInSetPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 5);
	  							 if(stockInSetPersons != null && stockInSetPersons.length > 0){
  							%>
  	  							<br/>
  	  						<%	
	  								 for(DBRow tempUserRow : stockInSetPersons){
	  								 %>
	  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
	  								 <% 
	  								 }
	  							 }
  							%>
	  					</span>
	  					<span class="right">
	  					<%
	  						if(rows[i].getString("stock_in_set_over").trim().length() > 0){
	  							out.println(rows[i].getString("stock_in_set_over")+"天");
	  						}else{
	  							if(rows[i].get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET) != TransportStockInSetKey.SHIPPINGFEE_NOTSET){
		  							 String time = transportMgrZr.getProcessSpendTime(rows[i].get("transport_id",0l),5,transportStockInSetKey.SHIPPINGFEE_SET);
		  							if(time.trim().length() > 0 ){
		  								out.println(df.format(tDate.getDiffDate(time,"dd"))+"天完成");
		  							}
	  							}
	  						}
	  					%>
	  					</span>
  					</li>
  					<li>
  						<span class="left">
  							单证状态:
  							<%
	  						//计算颜色
	  						String certificateClass = "" ;
  							int certificateInt = rows[i].get("certificate",TransportCertificateKey.NOCERTIFICATE);
  							if(certificateInt == TransportCertificateKey.FINISH){
  								certificateClass += "spanBold";
  								DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("transport_id",0l),FileWithTypeKey.transport_certificate);
  								if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
  									certificateClass += " fontGreen";
  								}else{
  									certificateClass += " fontRed";
  								}
  							}else if(certificateInt != TransportCertificateKey.NOCERTIFICATE){
  								certificateClass += "spanBold spanBlue";
  							}
  							%>
  							<span class="<%=certificateClass %>"><%=transportCertificateKey.getStatusById(rows[i].get("certificate",TransportCertificateKey.NOCERTIFICATE))%></span>
  							<%
	  							 DBRow[] certificatePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, 9);
	  							 if(certificatePersons != null && certificatePersons.length > 0){
  							%>
  	  							<br/>
  	  						<%	
	  								 for(DBRow tempUserRow : certificatePersons){
	  								 %>
	  								 	<%= tempUserRow.getString("employe_name")%>&nbsp;
	  								 <% 
	  								 }
	  							 }
  							%>
  						</span>
  						<span class="right">
  							<%=rows[i].get("certificate",01)==1?"":(rows[i].getString("certificate_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("certificate_over")+"天完成")%><br/>
  						</span>
  					</li>
  					<li>
  						<span class="left">
  							第三方标签:
  							<%
	  						//计算颜色
	  						String tagClassThird = "" ;
  							int tagIntThird = rows[i].get("tag_third",TransportTagKey.NOTAG);
  							if(tagIntThird == TransportTagKey.FINISH){
  								tagClassThird += "spanBold";
  								int[] fileType	= {FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE};
  					  			if(purchaseMgrZyj.checkPurchaseThirdTagFileFinish(rows[i].get("transport_id",0L),fileType,"transport_tag_types"))
  					  			{
  									tagClassThird += " fontGreen";
  								}else{
  									tagClassThird += " fontRed";
  								}
  							}else if(tagIntThird != TransportTagKey.NOTAG){
  								tagClassThird += "spanBold spanBlue";
  							}
	  					%>
  						<span class="<%=tagClassThird %>"><%=transportTagKey.getTransportTagById(rows[i].get("tag_third",TransportTagKey.NOTAG))%></span>
  							<%
  							 DBRow[] tagPersonsThird	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("transport_id")), ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.THIRD_TAG);
  							 if(tagPersonsThird != null && tagPersonsThird.length > 0){
  							%>
  	  							<br/>
  	  						<%	
  								 for(DBRow tempUserRowThird : tagPersonsThird){
  								 %>
  								 	<%= tempUserRowThird.getString("employe_name")%>&nbsp;
  								 <% 
  								 }
  							 }
  						%>
  						</span>
  						<span class="right">	
  							<%=rows[i].get("tag_third",01)==1?"":(rows[i].getString("tag_third_over").equals("")?df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":rows[i].getString("tag_third_over")+"天完成")%>
  						</span>
  					</li>
  				</ul>
		  	</td>
		 
  			 
  			<td align="left">
  				<%
  					DBRow[] transportLogsRow = transportMgrZr.getTransportLogs(rows[i].get("transport_id",0l),4);
  					if(transportLogsRow.length > 0){
  						int count = transportLogsRow.length >= 4 ? 3:transportLogsRow.length;
  						for(int m = 0; m < count; m ++){
  							DBRow transportLog = transportLogsRow[m];
  				%>			
  							 <div style="font-size:12px;margin-bottom:3px;line-height:15px;word-wrap:break-word;width:90%;border-bottom:1px dashed silver;padding-bottom:3px;padding-top:3px;">
								<span style="font-size:12px;">
							 		<font style="color:#f60;"><%=followuptype.get(transportLog.get("transport_type",0))%></font>:
									<strong><%=adminMgrLL.getAdminById(transportLog.getString("transporter_id")).getString("employe_name") %></strong>
									<span style="color:#999999;font-size:11px;font-farmliy:Verdana"><%= tDate.getFormateTime(transportLog.getString("transport_date")) %></span>
								</span><br />
								<%=transportLog.getString("transport_content") %>
 							  </div>
  				<%			
  						}
  					}
  					if(transportLogsRow.length >= 4){
  				%>		
  					<a href="javascript:void(0)" onclick='transportLogs(<%=rows[i].get("transport_id",0l)%>)' style="color:green;">更多</a>
  				<%	
  					}
  				%>
  			</td>
	  	</tr>
	  	<tr class="split">
  			<td colspan="6" style="text-align:right;padding-right:20px;">
  		<!--	<input type="button" value="流程信息" class="long-button" onclick="produresInfo('<%=rows[i].get("transport_id",0l)%>')"/> -->
<!--  			<input type="button" value="流程信息更新" class="long-button" onclick="produresInfoUpdate('<%=rows[i].get("transport_id",0l)%>')"/> -->
  				<!-- inbound outbound picture -->
  				 <input type="button" value="收货图片" class="long-button" onclick="inbound('<%=rows[i].get("transport_id",0l)%>')"/>
  				 <input type="button" value="装货货图片" class="long-button" onclick="outbound('<%=rows[i].get("transport_id",0l)%>')"/>
  				
				<%
  					if(TransportOrderKey.INTRANSIT == rows[i].get("transport_status",0))
  					{
  				%>
  				<input type="button" value="到货派送" class="long-button" onclick="goodsArriveDelivery(<%=rows[i].get("transport_id",0l)%>)"/>
  				<%		
  					}
  				%>
  				<%
  					int association_type_id = 0==rows[i].get("purchase_id",0l)?6:5;
  					if(rows[i].get("transport_status",0)==TransportOrderKey.READY && 0==applyFundsMgrZyj.getApplyMoneyByAssociationIdAndAssociationTypeIdNoCancel(rows[i].get("transport_id",0l),association_type_id).length)
  					{
  				%>
					&nbsp;&nbsp;<input type="button" value="删除" class="short-short-button-del" onclick="delTransport(<%=rows[i].get("transport_id",0l)%>)"/> 
  				<%		
  					}
  				%>
  				<%
  					if(rows[i].get("transport_status",0)==TransportOrderKey.PACKING&&rows[i].get("purchase_id",0l)==0l)
  					{
  				%>
  					<tst:authentication bindAction="com.cwc.app.api.zj.TransportMgrZJ.rebackTransport">
  					<input type="button" value="停止装箱" class="long-button-redtext" onclick="reBackTransport(<%=rows[i].get("transport_id",0l)%>)"/>
  					</tst:authentication>
  				<%
  					}
  				%>
  				
  				<%
  					if(rows[i].get("transport_status",0)==TransportOrderKey.INTRANSIT)
  					{
  				%>
  					<tst:authentication bindAction="com.cwc.app.api.zj.TransportMgrZJ.reStorageTransport">
  					&nbsp;&nbsp;<input type="button" value="中止运输" class="long-button-yellow" onclick="reStorageTransport(<%=rows[i].get("transport_id",0l)%>)"/> 
  					</tst:authentication>
  				<%
  					}
  				%>
  				
	  	  		<% 
	  	  			if(rows[i].get("product_file",TransportProductFileKey.NOPRODUCTFILE) != TransportProductFileKey.NOPRODUCTFILE)
	  	  			{ 
	  	  		%>
	  	  			&nbsp;&nbsp;<input  type="button" class="long-button" value="实物图片" onclick="product_file(<%=rows[i].get("transport_id",0l)%>)"/>
	  	  		<%
	  	  			}
	  	  		%>
  				
  				<%
  				 	if(rows[i].get("declaration",1) != declarationKey.NODELARATION ){  				 	//有报关 
  				%>
  				 		&nbsp;&nbsp;<input class="short-short-button-merge" type="button" onclick="declarationButton(<%=rows[i].get("transport_id",0l)%>)" value="报关"/> 	
  				<%
  					}
  				%>
	  	  		<%
  				 	if(rows[i].get("clearance",1) != clearanceKey.NOCLEARANCE ){// 有清关
  					%>
  					&nbsp;&nbsp;<input class="short-short-button-merge" type="button" onclick="clearanceButton(<%=rows[i].get("transport_id",0l)%>)" value="清关"/> 
  					<% 		
  				  	}
  				 %>
  				  <%if(rows[i].get("stock_in_set",1) != transportStockInSetKey.SHIPPINGFEE_NOTSET){%>
  				 	&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="运费" onclick="stock_in_set(<%=rows[i].get("transport_id",0l) %>,<%=FileWithTypeKey.TRANSPORT_STOCKINSET %>)"/>
  				 <%	 
  					 }
  				 %>
  				 <%if(rows[i].get("quality_inspection",1) != transportQualityInspectionKey.NO_NEED_QUALITY){%>
  				 	&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="质检" onclick="quality_inspectionKey(<%=rows[i].get("transport_id",0l) %>,<%=FileWithTypeKey.TRANSPORT_QUALITYINSPECTION %>)"/>
  				 <%	 
  					 }
  				 %>
  				 <%
  				 	if(rows[i].get("tag",TransportTagKey.NOTAG) != transportTagKey.NOTAG){
  				 %>
					&nbsp;&nbsp;<input type="button" class="long-button" value="跟进内部标签" onclick="tag(<%=rows[i].get("transport_id",0l) %>,<%=FileWithTypeKey.TRANSPORT_TAG %>)"/>
  				 <% 
  				 	}
  				 %>
  				 <%
  				 	if(rows[i].get("tag_third",TransportTagKey.NOTAG) != transportTagKey.NOTAG){
  				 %>
  				 &nbsp;&nbsp;<input type="button" class="long-button" value="跟进第三方标签" onclick="tagThird('<%=rows[i].get("transport_id",0l) %>')"/>
  				 <% 
  				 	}
  				 %>
	  	  		<%
	  	  			if(rows[i].get("certificate",1) !=  transportCertificateKey.NOCERTIFICATE){ //单证不等于 不需要显示
	  	  		%>
	  	  			&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="单证" onclick="transport_certificate(<%=rows[i].get("transport_id",0l) %>,<%=FileWithTypeKey.transport_certificate %>)"/>
	  	  		<%
	  	  			}
	  	  			String transportStatusStr = transportOrderKey.getTransportOrderStatusById(rows[i].get("transport_status",0));
		  	  		if(transportStatusStr.contains("</font>"))
					{
						int fontStaIndex = transportStatusStr.indexOf(">");
						int fontEndIndex = transportStatusStr.indexOf("</font>");
						transportStatusStr = transportStatusStr.substring(fontStaIndex+1, fontEndIndex);
					}
	  	  		%>
	  	  		&nbsp;&nbsp;<input  type="button" class="long-button" value='跟进<%=transportStatusStr %>' onclick='followup(<%=rows[i].get("transport_id",0l)%>)'/>
 	  	  		<%
	  	  		//转运单需要司机签到

		  	  		DBRow[] locationOccupancysLoad = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(0,0,
		  	  			ProductStoreBillKey.TRANSPORT_ORDER, rows[i].get("transport_id",0l) , -LoadUnloadOccupancyStatusKey.QUIT, null, null, null, null,rows[i].get("receive_psid", 0L),2,TransportRegistrationTypeKey.DELEIVER);
		  	  		DBRow[] locationOccupancysUnload = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(0,0,
		  	  			ProductStoreBillKey.TRANSPORT_ORDER, rows[i].get("transport_id",0l) , -LoadUnloadOccupancyStatusKey.QUIT, null, null, null, null,rows[i].get("send_psid", 0L),2,TransportRegistrationTypeKey.SEND);
		  	 		//如果提到仓库一致
		  	  		//if(0==1 && rows[i].get("receive_psid", 0L) == rows[i].get("send_psid", 0L) && 0==locationOccupancysLoad.length && 0 == locationOccupancysUnload.length)
		  	  		//{
		  	  		%>
<%--	  	  			&nbsp;&nbsp;<input type="button" class="long-button" value="装货司机签到" onclick="bookDoorOrLocation('<%=rows[i].get("transport_id",0l) %>','<%=TransportRegistrationTypeKey.SEND %>')"/>--%>
<%--	  	  			&nbsp;&nbsp;<input type="button" class="long-button" value="缷货司机签到" onclick="bookDoorOrLocation('<%=rows[i].get("transport_id",0l) %>','<%=TransportRegistrationTypeKey.DELEIVER %>')"/>--%>
	  	  			<%		
		  	  		//}
		  	 		%>
		  	  		<%
		  	  		//缷货司机已签到
		  	  		if(0 != locationOccupancysLoad.length)
		  	  		{
			  	  		String buttonColorLoad = "";
		  	  			for(int m = 0; m < locationOccupancysLoad.length; m ++)
		  	  			{
		  	  				if(LoadUnloadOccupancyStatusKey.BOOKING == locationOccupancysLoad[m].get("occupancy_status",0))
		  	  				{
		  	  					buttonColorLoad = "color:red;";
		  	  					break;
		  	  				}
		  	  			}
		  	  		%>
			  			&nbsp;&nbsp;<input type="button" class="long-button" style='<%=buttonColorLoad %>' value="缷货司机已签到" onclick="bookDoorOrLocationUpdateOrView('<%=rows[i].get("transport_id",0l) %>','<%=TransportRegistrationTypeKey.DELEIVER %>')"/>
			  		<%	
		  	  		}
		  	  		else
		  	  		{
		  	  			//缷货司机签到
			  	  		if(psId == rows[i].get("receive_psid", 0L))
			  	  		{
			  	  	%>
	  	  				&nbsp;&nbsp;<input type="button" class="long-button" value="缷货司机签到" onclick="bookDoorOrLocation('<%=rows[i].get("transport_id",0l) %>','<%=TransportRegistrationTypeKey.DELEIVER %>')"/>
	  	  			<%	
	  	  			}
		  	  		}
		  	  		%>
	  	  			<%
	  	  			if(0 != locationOccupancysUnload.length)
	  	  			{
	  	  				//装货司机已签到
		  	  			String buttonColorUnload = "";
		  	  			for(int m = 0; m < locationOccupancysUnload.length; m ++)
		  	  			{
			  	  			if(LoadUnloadOccupancyStatusKey.BOOKING == locationOccupancysUnload[m].get("occupancy_status",0))
		  	  				{
				  	  			buttonColorUnload = "color:red;";
		  	  					break;
		  	  				}
		  	  			}
				  	%>
		  	  			&nbsp;&nbsp;<input type="button" class="long-button" style='<%=buttonColorUnload %>' value="装货司机已签到" onclick="bookDoorOrLocationUpdateOrView('<%=rows[i].get("transport_id",0l) %>','<%=TransportRegistrationTypeKey.SEND %>')"/>
		  	  		<%
	  	  			}
	  	  			else
	  	  			{
	  	  			//装货司机签到
			  	  		if(psId == rows[i].get("send_psid", 0L))
			  	  		{
			  	  	%>
		  	  			&nbsp;&nbsp;<input type="button" class="long-button" value="装货司机签到" onclick="bookDoorOrLocation('<%=rows[i].get("transport_id",0l) %>','<%=TransportRegistrationTypeKey.SEND %>')"/>
		  	  		<%
			  	  		}	
	  	  			}
	  	  			%>	  	  		
  			</td>
	  	</tr>
  	<%
  		}
  	%>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>

<form action="transport_order_index.html" method="get" name="search_form">
	<input type="hidden" name="key"/>
	<input type="hidden" name="cmd" value="search"/>
	<input type="hidden" name="search_mode"/>
</form>

<form action="transport_order_index.html" method="post" name="filter_form">
	<input type="hidden" name="send_psid"/>
	<input type="hidden" name="receive_psid"/>
	<input type="hidden" name="status"/>
	<input type="hidden" name="declarationStatus"/>
	<input type="hidden" name="clearanceStatus"/>
	<input type="hidden" name="drawbackStatus"/>
	<input type="hidden" name="invoiceStatus"/>
	<input type="hidden" name="stock_in_set"/>
	<input type="hidden" name="dept"/>
	<input type="hidden" name="create_account_id"/>	
	<input type="hidden" name="cmd" value="filter"/>
</form>

<form action="transport_order_index.html" method="post" name="track_form">
	<input type="hidden" name="send_psid"/>
	<input type="hidden" name="receive_psid"/>
	<input type="hidden" name="product_line_id"/>
	<input type="hidden" name="cmd"/>
	<input type="hidden" name="store_title"/>
	<input type="hidden" name="product_line_title"/>
</form>

<form name="dataForm" method="post">
        <strong>
        <input type="hidden" name="p" />
        <input type="hidden" name="status" value="<%=status%>" />
        <input type="hidden" name="cmd" value="<%=cmd%>" /><input type="hidden" name="number" value="<%=key%>" />
        <input type="hidden" name="receive_psid" value="<%=receive_psid%>"/>
        <input type="hidden" name="send_psid" value="<%=send_psid%>"/>
        <input type="hidden" name="declarationStatus" value="<%=declaration%>"/>
		<input type="hidden" name="clearanceStatus" value="<%=clearance%>"/>
		<input type="hidden" name="drawbackStatus" value="<%=drawback%>"/>
		<input type="hidden" name="invoiceStatus" value="<%=invoice%>"/>
		<input type="hidden" name="st" value="<%=st%>"/>
	    <input type="hidden" name="en" value="<%=en%>"/>
	    <input type="hidden" name="analysisType" value="<%=analysisType%>"/>
	    <input type="hidden" name="analysisStatus" value="<%=analysisStatus%>"/>
	    <input type="hidden" name="day" value="<%=day%>"/>
	    <input type="hidden" name="stock_in_set" value="<%=stock_in_set %>"/>
	    <input type="hidden" name="dept" value="<%=dept %>"/>
		<input type="hidden" name="create_account_id" value="<%=create_account_id %>"/>	
		<input type="hidden" name="dept1" value="<%=dept1 %>"/>
		<input type="hidden" name="create_account_id1" value="<%=create_account_id1 %>"/>	
		<input type="hidden" name="product_line_id" value="<%=product_line_id%>"/>
		<input type="hidden" name="store_title" value="<%=store_title%>"/>
		<input type="hidden" name="product_line_title" value="<%=product_line_title%>"/>
        </strong>
        
		
		
  </form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/creatDeliveryOrder.action" name="add_deliveryOrder_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/administrator_delivery_order_detail.html"/>
</form>

<form action="" method="post" name="applicationApprove_form">
	<input type="hidden" name="transport_id"/>
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/approveTransportOutbound.action" method="post" name="applicationOutboundApprove_form">
	<input type="hidden" name="transport_id"/>
</form>
<form action="" method="post" name="followup_form">
	<input type="hidden" name="transport_id"/>
	<input type="hidden" name="type" value="1"/>
	<input type="hidden" name="stage" value="1"/>
	<input type="hidden" name="transport_type" value="1"/>
	<input type="hidden" name="transport_content"/>
</form>
<script type="text/javascript">
	function search()
	{
		if($("#search_key").val().length>0)
		{
			document.search_form.key.value = $("#search_key").val();
			document.search_form.submit();
		}
		else
		{
			alert("请输入转运单号");
		}
	}
	
	function filter()
	{
		document.filter_form.send_psid.value = $("#send_ps").getSelectedValue();
		document.filter_form.receive_psid.value = $("#receive_ps").getSelectedValue();
		document.filter_form.status.value = $("#transport_status").getSelectedValue();
		document.filter_form.declarationStatus.value = $("#declarationStatus").getSelectedValue();
		document.filter_form.clearanceStatus.value = $("#clearanceStatus").getSelectedValue();
		document.filter_form.invoiceStatus.value = $("#invoiceStatus").getSelectedValue();
		document.filter_form.drawbackStatus.value = $("#drawbackStatus").getSelectedValue();
		document.filter_form.stock_in_set.value = $("#stock_in_set").getSelectedValue();
		document.filter_form.dept.value = $("#dept").getSelectedValue();
		document.filter_form.create_account_id.value = $("#create_account_id").getSelectedValue();
		   
		document.filter_form.submit();
	}
	
	function delTransport(transport_id)
	{
		if(confirm("确定删除转运单T"+transport_id+"？"))
		{
			var para = "transport_id="+transport_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminitrrator/transport/delTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function applicationApprove(transport_id)
	{
		if(confirm("确定申请T"+transport_id+"转运单完成?"))
		{
			document.applicationApprove_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/addTransportApprove.action";
			document.applicationApprove_form.transport_id.value = transport_id;
			document.applicationApprove_form.submit();
		}
	}
	
	function closeWin()
	{
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}

	function transportLogs(transport_id)
	{
		var uri = 'transport_logs.html?transport_id='+transport_id;
		$.artDialog.open(uri, {title: '日志 转运单号:'+transport_id,width:'970px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function addTransportOrder()
	{
		var uri = "add_transport.html";
		$.artDialog.open(uri, {title: '创建转运单',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function createDeliveryOrder(purchase_id)
	{
		document.add_deliveryOrder_form.purchase_id.value = purchase_id;
		document.add_deliveryOrder_form.submit();	
		tb_remove();
	}	
	
	
	
	function approveOutbound(transport_id)
	{
		document.applicationOutboundApprove_form.transport_id.value = transport_id;
		document.applicationOutboundApprove_form.submit();
	}
	
	function reBackTransport(transport_id)
	{
		if(confirm("确定转运单T"+transport_id+"停止装箱？（将按照转运商品回退库存）"))
		{
			var para = "transport_id="+transport_id+"&is_need_notify_executer=true";
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/reBackTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function reStorageTransport(transport_id)
	{
		if(confirm("确定转运单T"+transport_id+"中止运输？"))
		{
			var para = "transport_id="+transport_id+"&is_need_notify_executer=true";
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/reStorageTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
</script>
</body>
</html>



