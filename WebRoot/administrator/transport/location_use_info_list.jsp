<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.LoadUnloadOccupancyTypeKey"%>
<%@page import="com.cwc.app.key.LoadUnloadRelationTypeKey"%>
<%@page import="com.cwc.app.key.LoadUnloadOccupancyStatusKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@ include file="../../include.jsp"%> 
<%
 	int pLoc = StringUtil.getInt(request,"pLoc");
 pLoc = 0==pLoc?1:pLoc;
 PageCtrl pcLoc = new PageCtrl();
 pcLoc.setPageNo(pLoc);
 pcLoc.setPageSize(5);
 AdminLoginBean adminLoggerBeanLoc = new AdminMgr().getAdminLoginBean( request.getSession(true) );
 long pscIdLoc = adminLoggerBeanLoc.getPs_id();
 String cmd = StringUtil.getString(request, "cmd");
 String book_start_time = StringUtil.getString(request, "book_start_time");
 String book_end_time = StringUtil.getString(request, "book_end_time");
 //System.out.println("loc:"+cmd+"---"+book_start_time+"---"+book_end_time+"---"+pLoc);
 DBRow[] loadUnloadLocation = new DBRow[0];
 if("canOccupancy".equals(cmd))
 {
 	loadUnloadLocation = doorOrLocationOccupancyMgrZyj.getBookLocationInfoCanChoose(book_start_time, book_end_time , pscIdLoc, pcLoc);
 }
 else if("probableOccupancy".equals(cmd))
 {
 	loadUnloadLocation = doorOrLocationOccupancyMgrZyj.getBookLocationInfoProbableChoose(book_start_time, book_end_time , pscIdLoc, pcLoc);
 }
 else
 {
 	loadUnloadLocation = storageDoorLocationMgrZYZ.getSearchLoadUnloadLocation(null,pscIdLoc,pcLoc);
 }
 %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>装卸位置列表</title>
  </head>
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="30%" class="right-title" style="vertical-align: center;text-align: center;">位置编号</th>
        <th width="60%" class="right-title" style="vertical-align: center;text-align: center;">占用情况</th>
        <th width="10%" style="vertical-align: center;text-align: center;" class="left-title">操作</th>
    </tr>
  <%
  		if(loadUnloadLocation != null){
  			for(int i=0; i<loadUnloadLocation.length; i++){
  %>
    <tr>
      <td  width="30%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=loadUnloadLocation[i].getString("location_name") %></td>
      <td>
      	<table>
      		
      				<%
      					DBRow[] locationOccupancys = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(LoadUnloadOccupancyTypeKey.LOCATION,loadUnloadLocation[i].get("id",0L),
      							ProductStoreBillKey.TRANSPORT_ORDER,0, -LoadUnloadOccupancyStatusKey.QUIT, null, null, null, null, 0,1,0);
      					if(locationOccupancys.length > 0)
      					{
      						for(int j = 0; j < locationOccupancys.length; j ++)
      						{
      				%>
      						<tr>
				      			<td>
				      				<%=locationOccupancys[j].getString("book_start_time") %>
				      			</td>
				      			<td>
				      				<%=locationOccupancys[j].getString("book_end_time") %>
				      			</td>
<%--				      			<td>--%>
<%--				      				<%=new LoadUnloadRelationTypeKey().getLoadUnloadRelationTypeName(locationOccupancys[j].get("rel_type",0)) %>--%>
<%--				      				:<%=locationOccupancys[j].get("rel_id",0) %>--%>
<%--				      			</td>--%>
				      		</tr>
      				<%			
      						}
      					}
      					else
      					{
      						out.println("");
      					}
      				%>
      			
      	</table>
      </td>
   	  <td  width="10%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
   	 	 <input type="checkbox" value='<%=LoadUnloadOccupancyTypeKey.LOCATION+"_"+new LoadUnloadOccupancyTypeKey().getLoadUnloadOccupancyTypeName(LoadUnloadOccupancyTypeKey.LOCATION)+"_"+loadUnloadLocation[i].get("id",0L)+"_"+loadUnloadLocation[i].getString("location_name") %>' onclick="chooseStorage(this)"/>
      </td>
    </tr>
   <%	}
  	} 
  	%>
    </table>
    <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	    <tr> 
		    <td height="28" align="right" valign="middle"> 
		      <%
				int preLoc = pcLoc.getPageNo() - 1;
				int nextLoc = pcLoc.getPageNo() + 1;
				out.println("页数：" + pcLoc.getPageNo() + "/" + pcLoc.getPageCount() + " &nbsp;&nbsp;总数：" + pcLoc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","首页","javascript:goLoc(1)",null,pcLoc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:goLoc(" + preLoc + ")",null,pcLoc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:goLoc(" + nextLoc + ")",null,pcLoc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","末页","javascript:goLoc(" + pcLoc.getPageCount() + ")",null,pcLoc.isLast()));
			 %>
		              跳转到 
		      <input name="jump_p2" type="text" id="loc_jump_p2" style="width:28px;" value="<%=pLoc%>"> 
		      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:goLoc(document.getElementById('loc_jump_p2').value)" value="GO"> 
		    </td>
	    </tr>
	</table> 
  </body>
</html>
