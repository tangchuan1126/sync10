<%@page language="java" contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey" />
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey" />
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey" />
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey" />
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey" />
<jsp:useBean id="transportOrderKey" class="com.cwc.app.key.TransportOrderKey" />
<jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey" />
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey" />
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey" />
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="transportQualityInspectionKey" class="com.cwc.app.key.TransportQualityInspectionKey" /><%
	long transport_id = StringUtil.getLong(request,"transport_id");
	TDate tDate = new TDate();
	//fileFlag
	String fileFlag= StringUtil.getString(request,"flag");
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
	int isPrint = StringUtil.getInt(request,"isPrint",0);
	//转运单title
	String title_name = "";
	DBRow titleRow = proprietaryMgrZyj.findProprietaryByTitleId(transport.get("title_id", 0L));
	if(null != titleRow)
	{
		title_name = titleRow.getString("title_name");
	}
	
	AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(request.getSession(true));
	long psId = adminLoggerBean.getPs_id();
	int number = 0;
	boolean edit = false;
	
	//仅能编辑管辖的仓库运单
	if (transport!=null && transport.get("send_psid", 0L)==psId) {
	
	}
	
	HashMap followuptype = new HashMap();
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改转运单详细记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"实物图片流程");
	followuptype.put(11,"质检流程");
	followuptype.put(12,"商品标签");
	
	String downLoadFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	String updateTransportCertificateAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportCertificateCompleteAction.action";
	//读取配置文件中的purchase_tag_types
	String tagType = systemConfig.getStringConfigValue("transport_tag_types");
	String[] arraySelected = tagType.split("\n");
	//将arraySelected组成一个List
	ArrayList<String> selectedList= new ArrayList<String>();
	for(int index = 0 , count = arraySelected.length ; index < count ; index++ )
	{
		String tempStr = arraySelected[index];
		if(tempStr.indexOf("=") != -1){
	String[] tempArray = tempStr.split("=");
	String tempHtml = tempArray[1];
	selectedList.add(tempHtml);
		}
	}
	// 获取所有的关联图片 然后在页面上分类
	Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
	int[] productFileTyps = {FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE};
	DBRow[] imagesRows = purchaseMgrZyj.getAllProductTagFilesByPcId(transport_id,productFileTyps );//临时拷贝样式，使用方法为采购单，后续开发请注意
	if(imagesRows != null && imagesRows.length > 0 )
	{
		for(int index = 0 , count = imagesRows.length ; index < count ; index++ )
		{
	DBRow tempRow = imagesRows[index];
	String product_file_type = tempRow.getString("product_file_type");
	List<DBRow> tempListRow = imageMap.get(product_file_type);
	if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1))
	{
		tempListRow = new ArrayList<DBRow>();
	}
	tempListRow.add(tempRow);
	imageMap.put(product_file_type,tempListRow);
		}
	}
	// 单证
	String valueCertificate = systemConfig.getStringConfigValue("transport_certificate");
	String[] arraySelectedCertificate = valueCertificate.split("\n");
	//将arraySelected组成一个List
	ArrayList<String> selectedListCertificate= new ArrayList<String>();
	for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
		String tempStr = arraySelectedCertificate[index];
		if(tempStr.indexOf("=") != -1){
	String[] tempArray = tempStr.split("=");
	String tempHtml = tempArray[1];
	selectedListCertificate.add(tempHtml);
		}
	}
	int file_with_type = StringUtil.getInt(request,"file_with_type");
	String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
	String file_with_class = StringUtil.getString(request,"file_with_class");
	DBRow[] imageListCertificate = fileMgrZJ.getFileByWithIdAndType(transport_id,FileWithTypeKey.transport_certificate);
	Map<String,List<DBRow>> mapCertificate = new HashMap<String,List<DBRow>>();
	TransportCertificateKey certificateKey = new TransportCertificateKey();
	if(imageListCertificate != null && imageListCertificate.length > 0)
	{
		//map<string,List<DBRow>>
		for(int index = 0 , count = imageListCertificate.length ; index < count ;index++ ){
	DBRow temp = imageListCertificate[index];
	List<DBRow> tempListRow = mapCertificate.get(temp.getString("file_with_class"));
	if(tempListRow == null){
		tempListRow = new ArrayList<DBRow>();
	}
	tempListRow.add(temp);
	mapCertificate.put(temp.getString("file_with_class"),tempListRow);
		}
	}
	// 实物图片
	String updateTransportAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportProductFileCompleteAction.action";
	String valueProductFile = systemConfig.getStringConfigValue("transport_product_file");
	String[] arrayProductFileSelected = valueProductFile.split("\n");
	String backurl = ConfigBean.getStringValue("systenFolder") + "administrator/transport/transport_order_out_detail.html?transport_id="+transport_id;
	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
	
	//计算报关颜色
	String declarationKeyClass = "" ;
	int declarationInt = transport.get("declaration",declarationKey.NODELARATION);
	if(declarationInt == declarationKey.FINISH){
		declarationKeyClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_declaration);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			declarationKeyClass += " fontGreen";
		} else {
			declarationKeyClass += " fontRed";
		}
	} else if(declarationInt != declarationKey.NODELARATION ) {
		declarationKeyClass +=  "spanBold spanBlue";
	}
	//计算清关颜色
	String clearanceKeyClass = "" ;
	int clearanceInt = transport.get("clearance",clearanceKey.NOCLEARANCE);
	if( clearanceInt == clearanceKey.FINISH){
		clearanceKeyClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_clearance);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			clearanceKeyClass += " fontGreen";
		} else {
			clearanceKeyClass += " fontRed";
		}
	} else if(clearanceInt != clearanceKey.NOCLEARANCE) {
		clearanceKeyClass +=  "spanBold spanBlue";
	}
	//计算费用标签颜色
	String stockInSetClass = "" ;
	int stockInSetInt = transport.get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET);
	if(stockInSetInt == TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH){
		stockInSetClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_STOCKINSET);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			stockInSetClass += " fontGreen";
		} else {
			stockInSetClass += " fontRed";
		}
	} else if(stockInSetInt != TransportStockInSetKey.SHIPPINGFEE_NOTSET) {
		stockInSetClass += "spanBold spanBlue";
	}
	//计算内部标签颜色
	String tagClass = "";
	int tagInt = transport.get("tag",TransportTagKey.NOTAG);
	if(tagInt == TransportTagKey.FINISH){
		tagClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_TAG);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			tagClass += " fontGreen";
		} else {
			tagClass += " fontRed";
		}
	} else if(tagInt != TransportTagKey.NOTAG) {
		tagClass += "spanBold spanBlue";
	}
	int tagIntThird = transport.get("tag_third",TransportTagKey.NOTAG);
	//计算第三方标签颜色
	String tagClassThird = "";
	if(tagIntThird == TransportTagKey.FINISH){
		tagClassThird += "spanBold";
		int[] fileType	= {FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE};
	 	if(purchaseMgrZyj.checkPurchaseThirdTagFileFinish(transport_id,fileType,"transport_tag_types"))
	 	{
			tagClassThird += " fontGreen";
		} else {
			tagClassThird += " fontRed";
		}
	} else if(tagIntThird != TransportTagKey.NOTAG) {
		tagClassThird += "spanBold spanBlue";
	}
	//计算实物图片颜色
	String productFileClass = "";
	int productFileInt = transport.get("product_file",TransportProductFileKey.NOPRODUCTFILE);
	if(productFileInt == TransportProductFileKey.FINISH){
		productFileClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("product_file",transport.get("transport_id",0l),FileWithTypeKey.product_file);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			productFileClass += " fontGreen";
		} else {
			productFileClass += " fontRed";
		}
	} else if(productFileInt != TransportProductFileKey.NOPRODUCTFILE) {
		productFileClass += "spanBold spanBlue";
	}
	//计算质量流程颜色
	String qualityInspectionClass = "";
	int qualityInspectionInt = transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY);
	if(qualityInspectionInt == TransportQualityInspectionKey.FINISH){
		qualityInspectionClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.TRANSPORT_QUALITYINSPECTION);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ){
			qualityInspectionClass += " fontGreen";
		} else {
			qualityInspectionClass += " fontRed";
		}
	} else if(qualityInspectionInt != TransportQualityInspectionKey.NO_NEED_QUALITY) {
		qualityInspectionClass += "spanBold spanBlue";
	}
	//计算颜色
	String certificateClass = "" ;
	int certificateInt = transport.get("certificate",TransportCertificateKey.NOCERTIFICATE);
	if(certificateInt == TransportCertificateKey.FINISH){
		certificateClass += "spanBold";
		DBRow tempFileArray = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",transport.get("transport_id",0l),FileWithTypeKey.transport_certificate);
		if(tempFileArray != null && tempFileArray.get("sum_file",0) > 0 ) {
			certificateClass += " fontGreen";
		} else {
			certificateClass += " fontRed";
		}
	} else if(certificateInt != TransportCertificateKey.NOCERTIFICATE) {
		certificateClass += "spanBold spanBlue";
	}
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>转运单 T<%=transport_id%></title>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<style type="text/css">
	body{text-align:left;}
	ol.fundsOl{
		list-style-type: none;	
	}
	ol.fundsOl li{
		width: 350px;
		height: 200px;
		float: left;
		line-height: 25px;
	}
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jqgrid 引用 -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>
<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}
.text-line
{
	font-size:12px;
}
a.logout:link {
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:visited {
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
	font-size: 12px;
	color: #000000;
	text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
.ui-datepicker {z-index:1200;}
.rotate {
	/* for Safari */
	-webkit-transform: rotate(-90deg);

	/* for Firefox */
	-moz-transform: rotate(-90deg);

	/* for Internet Explorer */
	filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
}
.ui-jqgrid .ui-jqgrid-htable th
{
	font-size:11px;
}
	
.ui-jqgrid tr.jqgrow td 
{
	white-space: normal !important;
	height:auto;
	vertical-align:text-top;
	padding-top:2px;
}
.set
{
	border:2px #999999 solid;
	padding:2px;
	width:90%;
	word-break:break-all;
	margin-top:10px;
	margin-top:5px;
	line-height:18px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	margin-bottom: 10px;
}
.create_order_button{background-attachment: fixed;background: url(../imgs/create_order.jpg);background-repeat: no-repeat;background-position: center center;height: 51px;width: 129px;color: #000000;border: 0px;}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
.zebraTable td {line-height:25px;height:25px;}
.right-title{line-height:20px;height:20px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
span.moredown {
	margin: 5;
	background-attachment: fixed;
	background: url(../imgs/maps/hide_light_down.png);
	background-repeat: no-repeat;background-position: center center;height: 16px;width: 16px;color: #000000;border: 0px;
}
span.moreup {
	margin: 5;
	background-attachment: fixed;
	background: url(../imgs/maps/hide_light_up.png);
	background-repeat: no-repeat;background-position: center center;height: 16px;width: 16px;color: #000000;border: 0px;
}
div.tabdown {
	background-attachment: fixed;
	background: url(../imgs/tabdown.png);
	background-repeat: no-repeat;background-position: center center;height: 15px;width: 79px;color: #000000;border: 0px;
	position:relative;
}
div.tabup {
	background-attachment: fixed;
	background: url(../imgs/tabup.png);
	background-repeat: no-repeat;background-position: center center;height: 15px;width: 79px;color: #000000;border: 0px;
	position:relative;
}

.smallGallery .nav{
	width:105px;
}
.mediumGallery .nav{
	width:165px;
}
.largeGallery .nav{
	width:225px;
}
.smallGallery .images,
.mediumGallery .images,
.largeGallery .images{
	padding:10px;
	background-color:#f9f9f9;
	border:1px solid #fff;
	position:relative;
	-moz-box-shadow:1px 1px 5px #aaa;
	-webkit-box-shadow:1px 1px 5px #aaa;
	box-shadow:1px 1px 5px #aaa;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
}
.nav {
	height: 20px;
	padding-left: 4px;
}
.smallGallery .images,
.smallGallery .singleImg div{
	width:102px;
	height:102px;
}
.mediumGallery .images,
.mediumGallery .singleImg div{
	width:162px;
	height:108px;
}
.largeGallery .images,
.largeGallery .singleImg div{
	width:222px;
	height:222px;
}
.microGallery{
	position:relative;
	margin:30px 10px 10px 10px;
	float:left;
}
.microGallery img{
	margin-left:auto;
	margin-right:auto;
	border:none;
	-moz-box-shadow:0px 2px 4px #777;
	-webkit-box-shadow:0px 2px 4px #777;
	box-shadow:0px 2px 4px #777;
	display:none;
}
a.thumbview{
	opacity:0.6;
	width:20px;
	height:21px;
	float:left;
	cursor:pointer;
}
.images div{
	display:table-cell;
	vertical-align:middle;
	text-align:center;
	position:relative;
}

.smallGallery .thumbs div,
.mediumGallery .thumbs div,
.largeGallery .thumbs div{
	float:left;
	margin:2px;
	cursor:pointer;
}

.smallGallery .thumbs div{
	width:30px;
	height:30px;
}
.mediumGallery .thumbs div{
	width:50px;
	height:50px;
}
.largeGallery .thumbs div{
	width:70px;
	height:70px;
}
</style>
<style type="text/css">
.tcol {
	line-height:18px;
	height:18px;
	border-color: #bbbbbb;
	border-style: solid;
	border-width: 0 0 1px 0;
	padding: 2px;
	color: #333333;
	font-size: 12px;
	text-align: left;
}
.tfoot {
	line-height:18px;
	height:18px;
	border-color: #bbbbbb;
	border-style: solid;
	border-width: 0 0 1px 0;
	padding: 0px;
	color: #333333;
	font-size: 12px;
	text-align: left;
}
.dTable tbody tr.over td {
	background: #E6F3C5;
}
.dTable tbody tr.open td {
	border-width: 0 0 0 0;
}
</style>
</head>
<body margin="1" onLoad="onLoadInitZebraTable()">
<div class="demo">
	<div style="min-height: 25px">
		<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
			<tr height="30px">
				<td>
					<%if (transport!=null && transport.get("send_psid", 0L)==psId) { %>
						<%
						int association_type_id = 0==transport.get("purchase_id",0l)?6:5;
						if(transport.get("transport_status",0)==TransportOrderKey.READY && 0==applyFundsMgrZyj.getApplyMoneyByAssociationIdAndAssociationTypeIdNoCancel(transport.get("transport_id",0l),association_type_id).length) {
						%>
					&nbsp;&nbsp;<input type="button" value="删除" class="short-short-button-del" onclick="delTransport(<%=transport.get("transport_id",0l)%>)"/> 
						<% }%>
						<%if(transport.get("transport_status",0)==TransportOrderKey.PACKING&&transport.get("purchase_id",0l)==0l) {%>
					<tst:authentication bindAction="com.cwc.app.api.zj.TransportMgrZJ.rebackTransport">
					&nbsp;&nbsp;<input type="button" value="停止装箱" class="long-button-redtext" onclick="reBackTransport(<%=transport.get("transport_id",0l)%>)"/>
					</tst:authentication>
  						<% } %>
						<%
						if(transport.get("transport_status",0)==TransportOrderKey.INTRANSIT) { %>
					<tst:authentication bindAction="com.cwc.app.api.zj.TransportMgrZJ.reStorageTransport">
					&nbsp;&nbsp;<input type="button" onclick="reStorageTransport('<%=transport_id%>');" value="中止运输" class="long-button">
					</tst:authentication>
						<% } %>
						<% if(transport.get("transport_status",0)==TransportOrderKey.READY || transport.get("transport_status",0)==TransportOrderKey.PACKING) { %>
					<tst:authentication bindAction="com.cwc.app.api.zj.TransportMgrZJ.reStorageTransport">
					&nbsp;&nbsp;<input name="button" type="button" class="long-button" value="转运单出库" onClick="showStockOut(<%=transport_id%>)"/>
					</tst:authentication>
						<% } %>
					&nbsp;&nbsp;<input name="button" type="button" class="long-button" value="修改各流程信息" onClick="updateTransportAllProdures()"/>
					<% } %>
					&nbsp;&nbsp;<input type="button" onclick="downloadTransportOrder('<%=transport_id%>')" value="下载运单" class="long-button-next">
					&nbsp;&nbsp;<input type="button" onclick="print()" value="打印运单" class="long-button-print">
				</td>
			</tr>
		</table>
	</div>
	<div id="transportMainTabs" style="min-height: 25px">
		<ul>
			<li ondblclick="opBasicTab()"><a href="#transport_basic_info"><span>基础信息</span></a></li>
			<li ondblclick="opBasicTab()"><a href="#transport_info"><span>货单信息</span></a></li>
		</ul>
		<div id="transport_basic_info">
			<table id="basic_info" width="100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
				<tr id="basic_info_main" >
					<td align="center" width="33%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">转运单号：</td>
									<td style="width: 80%;" nowrap="nowrap">T<%=transport_id%></td>
								</tr>
						</table>
					</td>
					<td align="center" width="33%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">TITLE：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=title_name%></td>
								</tr>
						</table>
					</td>
					<td align="center" width="34%">
						<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">货物状态：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=transportOrderKey.getTransportOrderStatusById(transport.get("transport_status",0))%></td>
								</tr>
						</table>
					</td>
				</tr>
				</thead>
				<tbody>
				<tr id="basic_info_detail" style="display:none;">
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>运单基础信息</legend>
							<table style="width:100%;border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">运单号：</td>
									<td style="width: 80%;" nowrap="nowrap">T<%=transport_id%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">TITLE：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=title_name%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">创建人：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("create_account")%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">创建时间：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=tDate.getEnglishFormateTime(transport.getString("transport_date"))%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">ETD：</td>
									<td style="width: 80%;" nowrap="nowrap"><%
										if(transport.getString("transport_out_date").trim().length() > 0){
											out.print(tDate.getEnglishFormateTime(transport.getString("transport_out_date")));
										} else {
											out.print("&nbsp;");
										}
									%></td>
								</tr>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">允许装箱：</td>
									<td style="width: 80%;" nowrap="nowrap"><% 
										DBRow packinger = adminMgr.getDetailAdmin(transport.get("packing_account",0l));
										if(packinger!=null)
										{
											out.print(packinger.getString("employe_name"));
										}
										else
										{
											out.print("&nbsp;");
										}
									%></td>
								</tr>
							</table>
						</fieldset>
					</td>
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>运输基础信息</legend>
							<table style="width:100%;border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">货物状态：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=transportOrderKey.getTransportOrderStatusById(transport.get("transport_status",0)) %></td>
								</tr>
								<%if (declarationKey.NODELARATION != transport.get("declaration",declarationKey.NODELARATION)) { %>
								<%	String declaration_responsible = "";
									String declarationPersonsList = "";
									DBRow[] declarationPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 7);
									if(declarationPersons != null && declarationPersons.length > 0){
										
										for(DBRow tempUserRow : declarationPersons){
											declarationPersonsList += (tempUserRow.getString("employe_name")+"&nbsp");
										}
									}
									if (transport.get("declaration_responsible", 0)==1) {
										if (declarationPersonsList.length()>0) {
											declaration_responsible = "(发货方："+declarationPersonsList+")";
										} else {
											declaration_responsible = "(发货方"+declarationPersonsList+")";
										}
										
									} else if (transport.get("declaration_responsible", 0)==2) {
										if (declarationPersonsList.length()>0) {
											declaration_responsible = "(收货方："+declarationPersonsList+")";
										} else {
											declaration_responsible = "(收货方"+declarationPersonsList+")";
										}
									} else {
										declaration_responsible = "("+declarationPersonsList+")";
									}
									 %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">出口报关：</td>
									<td style="width: 80%;"><span class="<%=declarationKeyClass%>"><%=declaration_responsible%><%=declarationKey.getStatusById(transport.get("declaration",01)) %></span></td>
								</tr>
								<%}%>
								<%if (clearanceKey.NOCLEARANCE != transport.get("clearance",clearanceKey.NOCLEARANCE)) { %>
								<%	String clearance_responsible ="";
									String clearancePersonsList ="";
									
									DBRow[] clearancePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 6);
  							 		if(clearancePersons != null && clearancePersons.length > 0){
  							 			for(DBRow tempUserRow : clearancePersons){
  							 				clearancePersonsList += (tempUserRow.getString("employe_name")+"&nbsp;");
  							 			}
  							 		}
									if (transport.get("clearance_responsible", 0)==1) {
										if (clearancePersonsList.length()>0) {
											clearance_responsible = "(发货方："+clearancePersonsList+")";
										} else {
											clearance_responsible = "(发货方"+clearancePersonsList+")";
										}
									} else if (transport.get("clearance_responsible", 0)==2) {
										if (clearancePersonsList.length()>0) {
											clearance_responsible = "(收货方："+clearancePersonsList+")";
										} else {
											clearance_responsible = "(收货方"+clearancePersonsList+")";
										}
									} else {
										clearance_responsible = "("+clearancePersonsList+")";
									}
								%>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">进口清关：</td>
									<td style="width: 80%;"><span class="<%=clearanceKeyClass%>"><%=clearance_responsible%><%=clearanceKey.getStatusById(transport.get("clearance",01)) %></span></td>
								</tr>
								<%}%>
								<%if (TransportStockInSetKey.SHIPPINGFEE_NOTSET != transport.get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET)) {%><% 
									String stock_in_set_responsible= "";
									String stockInSetPersonsList = "";
									DBRow[] stockInSetPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 5);
	  							 	if(stockInSetPersons != null && stockInSetPersons.length > 0){
	  							 		for(DBRow tempUserRow : stockInSetPersons){
	  							 			stockInSetPersonsList+=(tempUserRow.getString("employe_name")+"&nbsp;");
	  							 		}
	  							 	}
									if (transport.get("stock_in_set_responsible", 0)==1) {
										if (stockInSetPersonsList.length()>0) {
											stock_in_set_responsible = "(发货方："+stockInSetPersonsList+")";
										} else {
											stock_in_set_responsible = "(发货方"+stockInSetPersonsList+")";
										}
									} else if (transport.get("stock_in_set_responsible", 0)==2) {
										if (stockInSetPersonsList.length()>0) {
											stock_in_set_responsible = "(收货方："+stockInSetPersonsList+")";
										} else {
											stock_in_set_responsible = "(收货方"+stockInSetPersonsList+")";
										}
									} else {
										stock_in_set_responsible = "("+stockInSetPersonsList+")";
									}
								%>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">运费状态：</td>
									<td style="width: 80%;"><span class="<%=stockInSetClass%>"><%=stock_in_set_responsible%><%=transportStockInSetKey.getStatusById(transport.get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET)) %></span></td>
								</tr>
								<%}%>
							</table>
						</fieldset>
					</td>
					<td align="center" width="25%" valign="top">
						<fieldset class="set">
							<legend>流程基础信息</legend>
							<table style="width:100%;border:0">
								<%	if (transportTagKey.NOTAG != transport.get("tag",transportTagKey.NOTAG)) { %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">制签流程：</td>
									<td style="width: 80%;"><%
										out.print("<span class='"+tagClass+"'>");
										String tag_responsible = "";
										String tagPersonsList = "";
DBRow[] tagPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 8);
if(tagPersons != null && tagPersons.length > 0){
	for(DBRow tempUserRow : tagPersons){
		tagPersonsList+= (tempUserRow.getString("employe_name")+"&nbsp;");
	}
}
										if (transport.get("tag_responsible", 0)==1) {
											if (tagPersonsList.length()>0) {
												tag_responsible = "(发货方:"+tagPersonsList+")";
											} else {
												tag_responsible = "(发货方"+tagPersonsList+")";
											}
										} else if (transport.get("tag_responsible", 0)==2) {
											if (tagPersonsList.length()>0) {
												tag_responsible = "(收货方:"+tagPersonsList+")";
											} else {
												tag_responsible = "(收货方"+tagPersonsList+")";
											}
										} else {
											tag_responsible = "("+tagPersonsList+")";
										}
										out.print(tag_responsible);
										if(transport.get("tag",transportTagKey.NOTAG) == transportTagKey.TAG)
										{
											out.print("制签中");
										}
										else
										{
											out.print(transportTagKey.getTransportTagById(transport.get("tag",transportTagKey.NOTAG)));
										}
										out.print("</span>");
									%></td>
								</tr>
								<%	}
									if (transportTagKey.NOTAG != transport.get("tag_third",transportTagKey.NOTAG)) { %>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">第三方标签：</td>
									<td style="width: 80%;"><%
										out.print("<span class='"+tagClassThird+"'>");
										String tag_third_responsible = "";
										String tagPersonsThirdList = "";
										DBRow[] tagPersonsThird	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.THIRD_TAG);
										if(tagPersonsThird != null && tagPersonsThird.length > 0){
											for(DBRow tempUserRow : tagPersonsThird){
												tagPersonsThirdList += (tempUserRow.getString("employe_name")+"&nbsp;");
											}
										}
										if (transport.get("tag_third_responsible", 0)==1) {
											if (tagPersonsThirdList.length()>0) {
												tag_third_responsible = "(发货方:"+tagPersonsThirdList+")";
											} else {
												tag_third_responsible = "(发货方"+tagPersonsThirdList+")";
											}
											
										} else if (transport.get("tag_third_responsible", 0)==2) {
											if (tagPersonsThirdList.length()>0) {
												tag_third_responsible = "(收货方:"+tagPersonsThirdList+")";
											} else {
												tag_third_responsible = "(收货方"+tagPersonsThirdList+")";
											}
										} else {
											tag_third_responsible = "("+tagPersonsThirdList+")";
										}
										out.print(tag_third_responsible);
										
										if(transport.get("tag_third",transportTagKey.NOTAG) == transportTagKey.TAG)
										{
											out.print("制签中");
										}
										else
										{
											out.print(transportTagKey.getTransportTagById(transport.get("tag_third",transportTagKey.NOTAG)));
										}
										out.print("</span>");
									%></td>
								</tr>
								<%	}
									if (TransportProductFileKey.NOPRODUCTFILE != transport.get("product_file",transportProductFileKey.NOPRODUCTFILE)) { %><%
										String productFilePersonsList = "";
										DBRow[] productFilePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 10);
										if(productFilePersons != null && productFilePersons.length > 0){
											for(DBRow tempUserRow : productFilePersons){
												productFilePersonsList+=(tempUserRow.getString("employe_name")+"&nbsp;");
											}
										}
										String product_file_responsible = "";
										if (transport.get("product_file_responsible", 0)==1) {
											if (productFilePersonsList.length()>0) {
												product_file_responsible = "(发货方:"+productFilePersonsList+")";
											} else {
												product_file_responsible = "(发货方"+productFilePersonsList+")";
											}
											
										} else if (transport.get("product_file_responsible", 0)==2) {
											if (productFilePersonsList.length()>0) {
												product_file_responsible = "(收货方:"+productFilePersonsList+")";
											} else {
												product_file_responsible = "(收货方"+productFilePersonsList+")";
											}
										} else {
											product_file_responsible = "("+productFilePersonsList+")";
										}
									%>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">实物图片：</td>
									<td style="width: 80%;"><span class='<%=productFileClass%>'><%=product_file_responsible%><%=transportProductFileKey.getStatusById(transport.get("product_file",transportProductFileKey.NOPRODUCTFILE)) %></span></td>
								</tr>
								<%	}
									if (transportQualityInspectionKey.NO_NEED_QUALITY != transport.get("quality_inspection",transportQualityInspectionKey.NO_NEED_QUALITY)) {%><%
										String quality_inspection_responsible = "";
										String qualityInspectionPersonsList = "";
										DBRow[] qualityInspectionPersons = scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 11);
										if(qualityInspectionPersons != null && qualityInspectionPersons.length > 0){
											for(DBRow tempUserRow : qualityInspectionPersons){
												qualityInspectionPersonsList+=(tempUserRow.getString("employe_name")+"&nbsp;");
											}
										}
										if (transport.get("quality_inspection_responsible", 0)==1) {
											if (qualityInspectionPersonsList.length()>0) {
												quality_inspection_responsible = "(发货方:"+qualityInspectionPersonsList+")";
											} else {
												quality_inspection_responsible = "(发货方"+qualityInspectionPersonsList+")";
											}
										} else if (transport.get("quality_inspection_responsible", 0)==2) {
											if (qualityInspectionPersonsList.length()>0) {
												quality_inspection_responsible = "(收货方:"+qualityInspectionPersonsList+")";
											} else {
												quality_inspection_responsible = "(收货方"+qualityInspectionPersonsList+")";
											}
										} else {
											quality_inspection_responsible = "("+qualityInspectionPersonsList+")";
										}
									%>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">质检流程：</td>
									<td style="width: 80%;"><span class='<%=qualityInspectionClass%>'><%=quality_inspection_responsible%><%=transportQualityInspectionKey.getStatusById(transport.get("quality_inspection",transportQualityInspectionKey.NO_NEED_QUALITY)+"") %></span></td>
								</tr>
								<%	}
									if (certificateKey.NOCERTIFICATE != transport.get("certificate",certificateKey.NOCERTIFICATE)) {%><%
										String certificate_responsible = "";
										String certificatePersonsList = "";
										DBRow[] certificatePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 9);
										if(certificatePersons != null && certificatePersons.length > 0){
											for(DBRow tempUserRow : certificatePersons){
												certificatePersonsList += tempUserRow.getString("employe_name")+"&nbsp;";
											}
										}
	  							 		if (transport.get("certificate_responsible", 0)==1) {
	  							 			if (certificatePersonsList.length()>0) {
	  							 				certificate_responsible = "(发货方:"+certificatePersonsList+")";
	  							 			} else {
												certificate_responsible = "(发货方"+certificatePersonsList+")";
	  							 			}
										} else if (transport.get("certificate_responsible", 0)==2) {
											if (certificatePersonsList.length()>0) {
												certificate_responsible = "(收货方:"+certificatePersonsList+")";
											} else {
												certificate_responsible = "(收货方"+certificatePersonsList+")";
											}
										} else {
											certificate_responsible = "("+certificatePersonsList+")";
										}
									%>
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">单证流程：</td>
									<td style="width: 80%;"><span class='<%=certificateClass%>'><%=certificate_responsible%><%=certificateKey.getStatusById(transport.get("certificate",certificateKey.NOCERTIFICATE)) %></span></td>
								</tr>
								<%	} %>
							</table>
						</fieldset>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
		<div id="transport_info">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr id="transport_basic_main">
						<td align="center" width="33%">
							<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">发货仓库：</td>
									<td style="width: 80%;" nowrap="nowrap"><%
										if(transport.get("purchase_id",0l)==0)
										{
											out.print(null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
											session.setAttribute("billOfLading",null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
										}
										else
										{
											out.print(supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name"));
										}
									%></td>
								</tr>
							</table>
						</td>
						<td align="center" width="33%">
							<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">运输公司：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_waybill_name") %></td>
								</tr>
							</table>
						</td>
						<td align="center" width="34%">
							<table style="border:0">
								<tr>
									<td style="text-align:right;width: 20%;" nowrap="nowrap">收货仓库：</td>
									<td style="width: 80%;" nowrap="nowrap"><%=null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):""%></td>
								</tr>
							</table>
						</td>
					</tr>
				</thead>
				<tbody>
					<tr id="transport_basic_detail" style="display:none;">
						<td>
						<table border="0" width="100%">
							<tr>
								<td width="30%" align="center" valign="top">
								<fieldset class="set">
									<legend title="提货仓库"><%
										if(transport.get("purchase_id",0l)==0)
										{
											out.print(null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
											session.setAttribute("billOfLading",null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"):"");
										}
										else
										{
											out.print(supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name"));
										}
									%></legend>
									<table style="width:100%;border:1">
										<tr>
											<td nowrap="nowrap" title="门牌号"><%=null!=transport?transport.getString("send_house_number"):"" %></td>
										</tr>
										<tr>
											<td title="街道"><%=null!=transport?transport.getString("send_street"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="城市"><%=null!=transport?transport.getString("send_city"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="邮编"><%=null!=transport?transport.getString("send_zip_code"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系人"><%=null!=transport?transport.getString("send_name"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系电话"><%=null!=transport?transport.getString("send_linkman_phone"):"" %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属省份"><%
												long ps_id3 = null!=transport?transport.get("send_pro_id",0L):0L ;
												DBRow psIdRow3 = productMgr.getDetailProvinceByProId(ps_id3);
											%><%=(psIdRow3 != null? psIdRow3.getString("pro_name"):transport.getString("address_state_send") ) %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属国家"><%
												long ccid3 = null!=transport?transport.get("send_ccid",0L):0L; 
												DBRow ccidRow3 = orderMgr.getDetailCountryCodeByCcid(ccid3);
											%><%= (ccidRow3 != null?ccidRow3.getString("c_country"):"" ) %></td>
										</tr>
									</table>
								</fieldset>
								</td>
								<td width="40%" align="center" valign="bottom">
									<% if (transport.get("transportby",0)==transportWayKey.AIRWAY) { %>
									<img alt="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" title="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" width="176" height="89" src="../imgs/air_type.png" border="0">
									<% } else if (transport.get("transportby",0)==transportWayKey.SEAWAY) { %>
									<img alt="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" title="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" width="176" height="89" src="../imgs/sea_type.png" border="0">
									<% } else if (transport.get("transportby",0)==transportWayKey.LANDWAY) { %>
									<img alt="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" title="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" width="176" height="89" src="../imgs/land_type.png" border="0">
									<% } else if (transport.get("transportby",0)==transportWayKey.EXPRESSWAY) { %>
									<img alt="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" title="货运方式:<%=transportWayKey.getStatusById(transport.get("transportby",0)) %>" width="176" height="89" src="../imgs/express_type.png" border="0">
									<% } %>
									<fieldset class="set">
										<table style="width:100%;border:0">
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">运输公司：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_waybill_name") %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">运输方式：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transportWayKey.getStatusById(transport.get("transportby",0)) %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">发货港：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_send_place") %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">运单号：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_waybill_number") %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">承运公司：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("carriers") %></td>
											</tr>
											<tr>
												<td style="text-align:right;width: 20%;" nowrap="nowrap">目的港：</td>
												<td style="width: 80%;" nowrap="nowrap"><%=transport.getString("transport_receive_place") %></td>
											</tr>
										</table>
									</fieldset>
								</td>
								<td width="30%" align="center" valign="top">
								<fieldset class="set">
									<legend title="收货仓库"><%=null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):""%></legend>
									<table style="width:100%;border:1">
										<tr>
											<td nowrap="nowrap" title="门牌号"><%=transport.getString("deliver_house_number") %></td>
										</tr>
										<tr>
											<td title="街道"><%=transport.getString("deliver_street") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="城市"><%=transport.getString("deliver_city") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="邮编"><%=transport.getString("deliver_zip_code") %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系人"><%=transport.getString("transport_linkman")%></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="联系电话"><%=transport.getString("transport_linkman_phone")%></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属省份"><%
												long ps_id4 = null!=transport?transport.get("deliver_pro_id",0l):0L;
												DBRow psIdRow4 = productMgr.getDetailProvinceByProId(ps_id4);
											%><%=(psIdRow4 != null? psIdRow4.getString("pro_name"):transport.getString("address_state_deliver") ) %></td>
										</tr>
										<tr>
											<td nowrap="nowrap" title="所属国家"><%
												long ccid4 = null!=transport?transport.get("deliver_ccid",0l):0L; 
												DBRow ccidRow4 = orderMgr.getDetailCountryCodeByCcid(ccid4);
											%><%= (ccidRow4 != null?ccidRow4.getString("c_country"):"" ) %></td>
										</tr>
									</table>
								</fieldset>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div id="tabbar" class="tabdown" style="height:15px;padding-top:0px;text-align:center;margin:0 auto;padding-left:90%; float:inherit ;cursor:pointer;" title="更多""></div>
	<div style="min-height: 30px">
		<table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="right">
					<% if (transport!=null && transport.get("send_psid", 0L)==psId) { %>
					<input type="button" value="发货差异" onclick="showDifferent(0)" class="short-button"/>&nbsp;&nbsp;
					<% } %>
				</td>
			</tr>
		</table>
	</div>
	<div id="detail" align="left" style="padding:0px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<table id="dTable" border="0" cellpadding="0" cellspacing="0" style="width:100%; padding:0;" class="dTable">
			<caption class="tcaption" style="background-color: #CCCCCC; border-color: #bbbbbb; border-style: solid; border-width: 1px 1px 1px 0;color: #333333;font-size: 12px;font-weight: bold;height: 20px;padding: 5px;text-align: left;"></caption>
			<thead>
				<tr class="head" style="background-color: #e5e5e5;height: 20px;padding: 5px;">
					<th class="right-title tleft" style="vertical-align: center; text-align: center;">商品名(商品条码)</th>
					<th class="right-title" style="vertical-align: center; text-align: center;">货物总数</th>
					<th class="right-title" style="vertical-align: center; text-align: center;">V(cm³),W(Kg),估算运费</th>
					<th class="right-title" style="vertical-align: center; text-align: center;">商品序列号</th>
					<th class="right-title" style="vertical-align: center; text-align: center;">容器数</th>
					<th class="right-title tright" style="vertical-align: center; text-align: center;">批次</th>
				</tr>
			</thead>
			<tbody class="tdata">
				<tr id="T0" class="row" style="height: 20px">
					<td class="tleft tcol tright" colspan="6">&nbsp;</td>
				</tr>
			</tbody>
			<tfoot>
				<tr style="background-color: #e5e5e5;height: 20px;padding: 5px;">
					<td colspan="6" class="tfoot">
						<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0"><tr><td height="28" align="right" valign="middle" class="turn-page-table"></td></tr></table>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
<script type="text/javascript">
function print()
{
	visionariPrinter.PRINT_INIT("转运单");
	visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
	visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../transport/print_transport_order_detail.html?transport_id=<%=transport_id%>");
	visionariPrinter.PREVIEW();
}
function opBasicTab()
{
	if ($("#basic_info_detail").css('display')=='none')
	{
		$("#basic_info_main").hide();
		$("#basic_info_detail").show();
		$("#transport_basic_main").hide();
		$("#transport_basic_detail").show();
		$("#tabbar").attr("class", "tabup");
	} 
	else 
	{
		$("#basic_info_main").show();
		$("#basic_info_detail").hide();
		$("#transport_basic_main").show();
		$("#transport_basic_detail").hide();
		$("#tabbar").attr("class", "tabdown");
	}
}
$('#tabbar').bind("click", opBasicTab);
$("#transportMainTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tagTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tabsCertificate").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
});
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: {expires: 30000},
	load: function(event, ui) {onLoadInitZebraTable();}
});
function showDetail(id,sobj)
{
	if ($("#D"+id).css('display')=='none') {
		$("#T"+id).addClass("over");
		$("#D"+id).show();
		$("#T"+id).addClass("open");
	} else {
		$("#T"+id).removeClass("open");
		$("#D"+id).hide();
	}
}
function getProductData(page)
{
	$(".dTable tbody.tdata").html("<tr id=\"T0\" class=\"row\" style=\"height: 20px\"><td class=\"tleft tcol tright\" colspan=\"6\">&nbsp;</td></tr>");
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportDetailJson.action',
		type: 'POST',
		dataType: 'json',
		timeout: 60000,
		cache: false,
		async: false,
		data: {transport_id:<%=transport_id%>,page:page,rows:15},
		beforeSend: function(request){
		},
		error: function() {
			alert("获取明细失败，请重试！");
		},
		success: function(data) {
			(function(){setRowData(data);})();
			(function(){setPage(data);})();
		}
	});
}
function setCaption(text)
{
	$("#dTable caption.tcaption").html(text);
}
function setRowData(data)
{
	if (data.rows.length>0) {
		$("#T0").hide();
		for (var i=0;i<data.rows.length;i++) {
			var item = data.rows[i];
			var row1 ="<tr id=\"T"+(i+1)+"\" class=\"row\" onclick=\"showDetail("+(i+1)+",this);\" height=\"20px;\">"+
				"<td class=\"tleft tcol\">"+item.p_name+"("+item.p_code+")</td><td class=\"tcol\" style=\"text-align: center\">"+item.transport_count+"</td>"+
				"<td class=\"tcol\">"+item.volume_weight_freight+"</td><td class=\"tcol\">"+item.transport_product_serial_number+"&nbsp;</td>"+
				"<td class=\"tcol\" style=\"text-align: center\">"+item.container_quantity+"&nbsp;</td><td class=\"tcol tright\">"+item.lot_number+"&nbsp;</td></tr>";
			
			$(".dTable tbody.tdata").append(row1);
			if (item.containers && item.containers.length>0) {
				var row2 = "<tr id=\"D"+(i+1)+"\" class=\"row\" style=\"display:none\" onclick=\"showDetail("+(i+1)+",this);\"><td class=\"tcol tfoot\" colspan=\"6\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"15\"><tr>";
				if(typeof(item.containers) == "object") {
					for (var j=0;j<item.containers.length;j++) {
						var container = item.containers[j];
						var type ="";
						if(container.pick_container_type==0) {
							type ="Original";
							row2 +="<td style=\"margin-right:5px;\"><fieldset class=\"set\"><legend style=\"min-width:60px;white-space: nowrap;\">"+type+": "+container.pick_up_quantity+"</legend><table style=\"margin: 0px auto 0px 0px;\"><tbody><tr><td style=\"font-size:12px;\" nowrap=\"nowrap\"><font color=\"blue\">Original</font></td></tr></tbody></table></fieldset></td>";
							continue;
						}
						if (container.pick_container_type==1) {
							type ="CLP";
						} else if(container.pick_container_type==2) {
							type ="BLP";
						} else if(container.pick_container_type==3) {
							type ="TLP";
						} else if(container.pick_container_type==4) {
							type ="ILP";
						} else {
							break;
						}
						row2 +="<td style=\"margin-right:5px;\"><fieldset class=\"set\"><legend style=\"min-width:60px\">"+type+": "+container.pick_up_quantity+"</legend><table style=\"margin: 0px auto 0px 0px;\"><tbody><tr><td style=\"font-size:12px;\" nowrap=\"nowrap\"><font color=\"blue\">"+container.code+"</font></td></tr></tbody></table></fieldset></td>";
					}
				}
				row2 += "</tr></table></td></tr>";
				$(".dTable tbody.tdata").append(row2);
			}
		}
		$(".dTable tbody tr.row").mouseover(function() {
			var vid = ($(this).attr("id")).substring(1);
			if (vid!="0") {
				$("#T"+vid).addClass("over");
				$("#D"+vid).addClass("over");
			}
		}).mouseout(function() {
			var vid = ($(this).attr("id")).substring(1);
			if (vid!="0") {
				$("#T"+vid).removeClass("over");
				$("#D"+vid).removeClass("over");
			}
		}).click(function(){
			var vid = ($(this).attr("id")).substring(1);
			if (vid!="0") {
				$(this).addClass("over");
			}
		});
	}
}
function setPage(data)
{
	var pageHtml = "页数：" + data.page + "/" + data.total + " &nbsp;&nbsp;总数：" +  data.records + " &nbsp;&nbsp;";
	try {
		var  nowPage = parseInt(data.page);
		var  prePage = parseInt(nowPage) - 1;
		var  nextPage  = parseInt(nowPage) + 1;
		var  lastPage  = parseInt(data.total);
		if(nowPage > lastPage ) {
			nowPage = 1;
		}
		pageHtml += getAlinkHtml("gop","首页","javascript:go(1)",null,nowPage != 1);
		pageHtml += getAlinkHtml("gop","上一页","javascript:go(" +prePage+ ")",null,prePage >= 1);
		pageHtml += getAlinkHtml("gop","下一页","javascript:go(" +nextPage+ ")",null,nextPage <= lastPage);
		pageHtml += getAlinkHtml("gop","末页","javascript:go(" +lastPage+ ")",null,nowPage != lastPage);
		
		pageHtml += "跳转到 <input name='jump_p2' type='text' id='jump_p2' style='width: 28px;' value='"+nowPage+"' /> ";
		pageHtml += "<input name='Submit22' type='button' class='page-go' style='width: 28px; padding-top: 0px;' onClick=\"javascript:go(document.getElementById('jump_p2').value)\" value='GO' />";

		$(".dTable tfoot td.turn-page-table").html(pageHtml);
	} catch(e) {
		alert("系统异常");
	}
}
function go(page)
{
	if(!(/^[1-9]\d*$/.test(page))) {
		alert("请输入正确的页数");
		return;
	}
	getProductData(page);
}
function getAlinkHtml( classn, linkName, linkUrl, title, flag)
{
	 var tempHtml = "";
     if( flag ) {
    	 tempHtml += "\n<a href=\""+linkUrl+"\"";
         if (classn!=null||!classn.equals("")) {
        	 tempHtml += " class='"+classn+"' ";
         }
     } else {
    	 tempHtml += "\n<font color=\"#AAAAAA\"";
     }
     if( title != null && title.length() > 0 ) {
    	 tempHtml += " title=\""+title+"\"";
     }
     tempHtml += ">" + linkName + "<";
     if( flag ) {
    	 tempHtml += "/a>\n";
     } else {
    	 tempHtml += "/font>\n";
     }
     return tempHtml;
}
$("#transportTabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>',
	cookie: { expires: 30000 },
	load: function(event, ui) {onLoadInitZebraTable();}
	,show: function(event, ui){
		//alert(ui.index);
	}
});
function errorFormat(serverresponse,status)
{
	return errorMessage(serverresponse.responseText);
}
function refreshWindow(){
	window.location.reload();
}
function ajaxModProductName(obj,rowid,col)
{
	var unit_name;//单字段修改商品名时更换单位专用
	var para ="transport_detail_id="+rowid;
	$.ajax({
		url: '/Sync10/action/administrator/transport/getTransportDetailJSON.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		async:false,
		data:para,
		beforeSend:function(request){
		},
		
		error: function(){
			alert("提交失败，请重试！");
		},
		success: function(data) {
			obj.setRowData(rowid,data);
		}
	});
}
function getTransportSumVWP(transport_id)
{
	$.ajax({
		url: '/Sync10/action/administrator/transport/getTransportSumVWPJson.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:{transport_id:transport_id},
		
		beforeSend:function(request){
		},
		error: function(e){
			alert(e);
			alert("提交失败，请重试！");
		},
		success: function(date) {
			//mygrid.setCaption("总体积:"+date.volume+" cm³ 总货款:"+date.send_price+" RMB 总重量:"+date.weight+" Kg");
			//mygrid.setCaption("总体积:"+date.volume+" cm³ 总重量:"+date.weight+" Kg");
			setCaption("总体积："+date.volume+" cm³，总重量："+date.weight+" Kg");
		}
	});
}
function showPictrueOnline0(fileWithType,fileWithId,currentName)
{
	var obj = {
		file_with_type:fileWithType,
		file_with_id:fileWithId,
		current_name:currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'/Sync10/upload/transport'
	}
	if (window.top && window.top.openPictureOnlineShow) {
		window.top.openPictureOnlineShow(obj);
	} else {
		openArtPictureOnlineShow(obj,'/Sync10/');
	}
}
function showPictrueOnline(fileWithType,fileWithId,currentName,product_file_type)
{
	var obj = {
		file_with_type:fileWithType,
		file_with_id:fileWithId,
		current_name:currentName,
		product_file_type:product_file_type,
		cmd:"multiFile",
		table:'product_file',
		base_path:'/Sync10/upload/transport'
	}
	if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
		openArtPictureOnlineShow(obj,'/Sync10/');
	}
}
function onlineScanner(target)
{
	var _target = "";
	if(target == "uploadCertificateImageForm"){
		_target = "uploadCertificateImageForm";
	}else if(target == "qualityInspectionForm" ){
		_target = "qualityInspectionForm";
	}
	var uri = '/Sync10/' + "administrator/file/picture_online_scanner.html?target="+_target; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function addProductPicture(pc_id)
{
	 //添加商品图片
	var transport_id = '<%=transport_id%>';
	var uri = "/Sync10/administrator/transport/transport_product_picture_up.html?transport_id="+transport_id+"&pc_id="+pc_id;
	$.artDialog.open(uri , {title: "商品范例上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,close:function(){window.location.reload();},fixed: true});
}
function addProductTagTypesFile(pc_id)
{
	 //添加商品标签
	var transport_id = '<%=transport_id%>';
	var uri = "/Sync10/administrator/transport/transport_product_tag_file.html?transport_id="+transport_id+"&pc_id="+pc_id;
	$.artDialog.open(uri , {title: "第三方标签上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
}
function showDifferent(selectIndex)
{
	$.artDialog.open('transport_different_show.html?transport_id=<%=transport_id%>&select_index='+selectIndex, {title: "转运单差异",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
}
function downloadTransportOrder(transport_id)
{
	var para = "transport_id="+transport_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/downTransportOrder.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache: false,
		data: para,
		beforeSend: function(request){
		},
		error: function(e){
			alert(e);
			alert("提交失败，请重试！");
		},
		success: function(date){
			if(date["canexport"]=="true")
			{
				document.download_form.action=date["fileurl"];
				document.download_form.submit();
			}
			else
			{
				alert("无法下载！");
			}
		}
	});
}
function updateTransportAllProdures()
{
	//var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_wayout_update.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>";
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_flow_for_out_update.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '修改各流程信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function reStorageTransport(transport_id)
{
	if(confirm("确定转运单T"+transport_id+"中止运输？")) {
		var para = "transport_id="+transport_id+"&is_need_notify_executer=true";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/reStorageTransport.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache: false,
			data: para,
			beforeSend:function(request)
			{
			},
			error: function(e)
			{
				alert(e);
				alert("提交失败，请重试！");
			},
			success: function(data)
			{
				if(data.rel)
				{
					window.location.reload();
				}
			}
		});
	}
}
function showStockOut(transport_id)
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_stock_temp_out.html?transport_id='+transport_id;
	$.artDialog.open(url, {title: '转运单出库',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function allocate(transport_id)
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/allocate_detail.html?transport_id='+transport_id;
	$.artDialog.open(url, {title: '拣货单详细信息',width:'900px',height:'500px', lock: true,opacity: 0.3});
}
function delTransport(transport_id)
{
	if(confirm("确定删除转运单T"+transport_id+"？"))
	{
		var para = "transport_id="+transport_id;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminitrrator/transport/delTransport.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache: false,
			data: para,
						
			beforeSend: function(request)
			{
			},
			error: function(e)
			{
				alert(e);
				alert("提交失败，请重试！");
			},
			success: function(data)
			{
				if(data.rel)
				{
					window.close();
				}
				
			}
		});
	}
}
function reBackTransport(transport_id)
{
	if(confirm("确定转运单T"+transport_id+"停止装箱？（将按照转运商品回退库存）"))
	{
		var para = "transport_id="+transport_id+"&is_need_notify_executer=true";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/reBackTransport.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:para,
						
			beforeSend:function(request)
			{
			},
			error: function(e)
			{
				alert(e);
				alert("提交失败，请重试！");
			},
			success: function(data)
			{
				if(data.rel)
				{
					window.location.reload();
				}
				
			}
		});
	}							
}
$(document).ready(function(){
	getProductData(1);
	getTransportSumVWP(<%=transport_id%>);
});
</script>
<form name="download_form" method="post"></form>
<form action="" method="post" name="applicationApprove_form">
	<input type="hidden" name="transport_id"/>
</form>
<form action="" method="post" name="followup_form">
	<input type="hidden" name="transport_id"/>
	<input type="hidden" name="type" value="1"/>
	<input type="hidden" name="stage" value="1"/>
	<input type="hidden" name="transport_type" value="1"/>
	<input type="hidden" name="transport_content"/>
	<input type="hidden" name="expect_date"/>
</form>
</body>
</html>