<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@page import="com.cwc.app.key.ContainerTypeKey" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
	cartWaybillB2BMgrZJ.cleanCart(request);
	long transport_id = StringUtil.getLong(request, "transport_id");
	DBRow[] transportOrderOutList = transportMgrZJ.getTransportOrderOutList(transport_id,ProductStoreBillKey.TRANSPORT_ORDER);
%>
<head>
	<script	src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

	<script type='text/javascript' src='../js/jquery.form.js'></script>

	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyui/themes_visionari/icon.css">

	<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/common.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/select.js"></script>

	<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

	<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
	<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

	<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";

--%>
@import "../js/thickbox/thickbox.css";
</style>
	<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
	<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
	<script src="../js/thickbox/global.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
	<script src="../js/jgrowl/jquery.jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css" />
	<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" />

	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<style type="text/css">
td.topBorder{
	border-top: 1px solid silver;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
	
		
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		<thead>
			<tr>
			  <th width="15%"   class="right-title"  style="text-align: center;">位置</th>
			  <th width="15%"  class="right-title" style="text-align: center;">商品</th>
			  <th width="20%"  class="right-title" style="text-align: center;">从何种类型容器里拣货</th>
			  <th width="15%"  class="right-title" style="text-align: center;">从哪个容器里拿货</th>
			  <th width="15%"  class="right-title" style="text-align: center;">捡取容器</th>
			  <th width="15%"  class="right-title" style="text-align: center;">捡取容器号</th>
			  <th width="15%"  class="right-title" style="text-align: center;">数量</th>
			</tr>
		</thead>
		<tbody>
       		<%
						for (int i = 0; i < transportOrderOutList.length; i++) 
						{
							
					%>
				<tr	 >
					<td width="10%" height="30" align="center" valign="middle" style='word-break:break-all'>
						<%=transportOrderOutList[i].getString("slc_position_all")%>
					</td>
					<td width="10%" height="30" align="center" valign="middle" style='word-break:break-all' >
						
					    <%=orderProcessMgr.getProductById(transportOrderOutList[i].get("out_list_pc_id",01)).getString("p_name") %> 
					</td>
					
					<td  width="10%" height="30" align="center" valign="middle" style='word-break:break-all'>
						<%
						if(transportOrderOutList[i].getString("from_container_type").equals("1")){
						%>
						
							CLP[<%=LPTypeMgrZJ.getCLPName(transportOrderOutList[i].get("from_container_type_id",0l))%>]
						<%
						}
						else if(transportOrderOutList[i].getString("from_container_type").equals("2")){
						%>
						
							BLP[<%=LPTypeMgrZJ.getBLPName(transportOrderOutList[i].get("from_container_type_id",0l))%>]
						<%
						}
						else if(transportOrderOutList[i].getString("from_container_type").equals("3")){
						%>
							TLP<%=transportOrderOutList[i].getString("from_container_type") %>
						
						<%
						}else if(transportOrderOutList[i].getString("from_container_type").equals("0")){
						%>
							
						             位置直取
						<%
						}
						%>
					</td>
					<td width="10%" height="30" align="center" valign="middle" style='word-break:break-all' >
						<%
					    	if(transportOrderOutList[i].getString("from_con_id").equals("0")){
					    %>
							    无指定
					    <%
						}else{
						%>
							<%=transportOrderOutList[i].getString("from_con_id")%>
						<% 
						}
						%>
				    </td>
					<td  width="10%" height="30" align="center" valign="middle" style='word-break:break-all'>
					
						<%
						if(transportOrderOutList[i].getString("pick_container_type").equals("1")){
						%>
						
							CLP[<%=LPTypeMgrZJ.getCLPName(transportOrderOutList[i].get("pick_container_type_id",0l))%>]
						<%
						}
						else if(transportOrderOutList[i].getString("pick_container_type").equals("2")){
						%>
						
							BLP[<%=LPTypeMgrZJ.getBLPName(transportOrderOutList[i].get("pick_container_type_id",0l))%>]
						<%
						}
						else if(transportOrderOutList[i].getString("pick_container_type").equals("3")){
						%>
							TLP<%=transportOrderOutList[i].getString("pick_container_type") %>
						
						<%
						}else if(transportOrderOutList[i].getString("pick_container_type").equals("0")){
						%>
						
						  	  无指定
						<%
						}
						%>
					</td>
					<td width="10%" height="30" align="center" valign="middle" style='word-break:break-all' >
					    <%
					    	if(transportOrderOutList[i].getString("pick_con_id").equals("0")){
					    %>
					    	  无指定
					    <%
						}else{
						%>
							<%=transportOrderOutList[i].getString("pick_con_id")%>
						<% 
						}
						%>
						
				    </td>
					<td  width="10%" height="30" align="center" valign="middle" style='word-break:break-all'>
						<%
							if(transportOrderOutList[i].getString("from_container_type").equals("0") && 
							   transportOrderOutList[i].getString("from_con_id").equals("0")&& 
							   transportOrderOutList[i].getString("pick_container_type").equals("0")&& 
							   transportOrderOutList[i].getString("pick_con_id").equals("0")   ){
						%>
							<%=transportOrderOutList[i].getString("pick_up_quantity")%>
						<%
							}else{
						%>
							&nbsp;
						<%
							}
						%>
					</td>
				
		   </tr>
		   <%
					}
			%>
       </tbody>
	</table>

		
	
</body>