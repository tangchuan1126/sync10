<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	String transport_id = StringUtil.getString(request,"transport_id");
	DBRow row = transportMgrLL.getDrawback(transport_id);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>退税设置</title>
	<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
	<script language="javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
	
	<script type="text/javascript" src="../js/select.js"></script>
	
	<link href="../comm.css" rel="stylesheet" type="text/css"/>
	
	<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<style type="text/css" media="all">
	@import "../js/thickbox/global.css";
	@import "../js/thickbox/thickbox.css";
	</style>
	<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
	<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
	<script src="../js/thickbox/global.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
	<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<script>
	var updated = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated").equals("1")) {
	%>
			updated = true;
	<%
		}
	%>
	function init() {
		if(updated) {
			closeWindow();
		}

		setSumPrice(this);
	}
	
	$(document).ready(function(){
		init();
	});
	</script>
  </head>
  
  <body onload="onLoadInitZebraTable()">
  	<br/>
  	<br/>
  	<fieldset style="border:2px #cccccc solid;width:98%">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">退税金额</legend>
	<form action="<%=ConfigBean.getStringValue("systenFolder") %>action/administrator/transport/transportSetDrawbackAction.action" name="drawbackFrm">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr>
    		<th height="30">开票金额</th>
    		<th></th>
    		<th>增值税</th>
    		<th></th>
    		<th>退税率</th>
    		<th></th>
    		<th>常数1</th>
    		<th></th>
    		<th>开票金额</th>
    		<th></th>
    		<th>常数2</th>
    		<th></th>
    		<th>常数3</th>
    		<th></th>
    		<th>退税收入</th>
    	</tr>
    	<tr>
    		<td align="center"  height="50">
    			<input type="hidden" name="transport_drawback.id" id="id" value="<%=row.get("id",0) %>">
    			<input type="hidden" name="transport_drawback.transport_id" id="transport_id" value="<%=transport_id%>">
    			<input type="text" name="transport_drawback.invoice_price" id="invoice_price" size="4" value="<%=row.getString("invoice_price") %>" onkeyup="setSumPrice(this)">
    		</td>
    		<td align="center">/</td>
    		<td align="center"><input type="text" name="transport_drawback.tax_rate" id="tax_rate" value="<%=row.get("tax_rate",0f)==0?"1.17":row.get("tax_rate",0f) %>" size="4" onkeyup="setSumPrice(this)"></td>
    		<td align="center">*</td>
    		<td align="center"><input type="text" name="transport_drawback.drawback_tax" id="drawback_tax" size="4" value="<%=row.getString("drawback_tax")%>" onkeyup="setSumPrice(this)"></td>
    		<td align="center">*</td>
    		<td align="center"><input type="text" name="transport_drawback.ratio1" id="ratio1" size="4" value="<%=row.get("ratio1",0f)==0?"0.97":row.get("ratio1",0f) %>" onkeyup="setSumPrice(this)"></td>
    		<td align="center">-</td>
    		<td align="center" id="str_invoice_price"><%=row.getString("invoice_price") %></td>
    		<td align="center">*</td>
    		<td align="center"><input type="text" name="transport_drawback.ratio2" id="ratio2" size="4" value="<%=row.get("ratio2",0f)==0?"0.004":row.get("ratio2",0f) %>" onkeyup="setSumPrice(this)"></td>
    		<td align="center">-</td>
    		<td align="center"><input type="text" name="transport_drawback.ratio3" id="ratio3" size="4" value="<%=row.get("ratio3",0f)==0?"150":row.get("ratio3",0f) %>" onkeyup="setSumPrice(this)"></td>
    		<td align="center">=</td>
    		<td align="center" id="sum_price"></td>
    	</tr>
    	<tr>
    		<td colspan="15" align="right" height="30">
    			<input type="button" value="提交" class="long-button" onclick="setDrawback()">
    			<input name="cancel" type="button" class="long-button" onclick="closeWindow()" value="取消" >
    		</td>
    	</tr>
    </table>
    </fieldset>
    </form>
    <script type="text/javascript">
	<!--
		function closeWindow(){
			$.artDialog.close();
			//self.close();
		}

		function setSumPrice(obj) {
			var price1 = $('#invoice_price').val() * $('#ratio2').val();
			price1 = price1<80?80:price1;
			price1 = price1>800?800:price1;
			var price2 = $('#invoice_price').val() / $('#drawback_tax').val() * $('#drawback_tax').val() * $('#ratio1').val() - price1 - $('#ratio3').val();
			$('#sum_price').html(price2); 
			if(obj.id=="invoice_price") {
				$('#str_invoice_price').html(obj.value);
			} 
		}
	
		function setDrawback() {
			if(!isFloat($('#invoice_price').val())) {
				alert("开票金额不是数字!");
				return;
			}
			
			if(!isFloat($('#tax_rate').val())) {
				alert("增值税不是数字!");
				return;
			}

			if(!isFloat($('#drawback_tax').val())) {
				alert("退税率不是数字!");
				return;
			}

			if(!isFloat($('#ratio1').val())) {
				alert("常数1不是数字!");
				return;
			}

			if(!isFloat($('#ratio2').val())) {
				alert("常数2不是数字!");
				return;
			}

			if(!isFloat($('#ratio3').val())) {
				alert("ratio3不是数字!");
				return;
			}

			drawbackFrm.submit();
		}
		function isFloat(str){
			if(isInt(str))
			return true;
			var reg = /^(-|\+)?\d+\.\d*$/;
			return reg.test(str);
		}
		function isInt(str){
			var reg = /^(-|\+)?\d+$/ ;
			return reg.test(str);
		}
	//-->
	</script>
  </body>
</html>
