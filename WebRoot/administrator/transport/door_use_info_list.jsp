<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.LoadUnloadOccupancyTypeKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.LoadUnloadRelationTypeKey"%>
<%@page import="com.cwc.app.key.LoadUnloadOccupancyStatusKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@ include file="../../include.jsp"%> 
<%
 	int pDoor = StringUtil.getInt(request,"pDoor");
 pDoor = 0 == pDoor ? 1:pDoor;
 PageCtrl pcDoor = new PageCtrl();
 pcDoor.setPageNo(pDoor);
 pcDoor.setPageSize(5);
 AdminLoginBean adminLoggerBeanDoor = new AdminMgr().getAdminLoginBean( request.getSession(true) );
 long psIdDoor = adminLoggerBeanDoor.getPs_id();
 DBRow[] storageDoorList = new DBRow[0];
 String cmd = StringUtil.getString(request, "cmd");
 String book_start_time = StringUtil.getString(request, "book_start_time");
 String book_end_time = StringUtil.getString(request, "book_end_time");
 //System.out.println("door:"+cmd+"---"+book_start_time+"---"+book_end_time+"---"+pDoor);
 if("canOccupancy".equals(cmd))
 {
 	storageDoorList = doorOrLocationOccupancyMgrZyj.getBookDoorInfoCanChoose(book_start_time, book_end_time , psIdDoor, pcDoor);
 }
 else if("probableOccupancy".equals(cmd))
 {
 	storageDoorList = doorOrLocationOccupancyMgrZyj.getBookDoorInfoProbableChoose(book_start_time, book_end_time , psIdDoor,pcDoor);
 }
 else
 {
 	storageDoorList = storageDoorLocationMgrZYZ.getSearchStorageDoor(null,psIdDoor,pcDoor);
 }
 %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>装卸门列表</title>
 </head>
  
   <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="30%" class="right-title" style="vertical-align: center;text-align: center;">门牌号</th>
        <th width="60%" class="right-title" style="vertical-align: center;text-align: center;">占用情况</th>
        <th width="10%" style="vertical-align: center;text-align: center;" class="left-title">操作</th>
    </tr>
  	<%
  		if(storageDoorList != null){
  				
  			for(int i = 0 ;i<storageDoorList.length; i++){
  	%>
    <tr>
      <td  width="30%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=storageDoorList[i].getString("doorId") %></td>
   	 <td>
      	<table>
      		
      				<%
      					DBRow[] locationOccupancys = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(LoadUnloadOccupancyTypeKey.DOOR,storageDoorList[i].get("sd_id",0L),
      							ProductStoreBillKey.TRANSPORT_ORDER,0 , -LoadUnloadOccupancyStatusKey.QUIT, null, null, null, null,0,1,0);
      					if(locationOccupancys.length > 0)
      					{
      						for(int j = 0; j < locationOccupancys.length; j ++)
      						{
      				%>
      						<tr>
				      			<td>
				      				<%=locationOccupancys[j].getString("book_start_time") %>
				      			</td>
				      			<td>
				      				<%=locationOccupancys[j].getString("book_end_time") %>
				      			</td>
<%--				      			<td>--%>
<%--				      				<%=new LoadUnloadRelationTypeKey().getLoadUnloadRelationTypeName(locationOccupancys[j].get("rel_type",0)) %>--%>
<%--				      				:<%=locationOccupancys[j].get("rel_id",0) %>--%>
<%--				      			</td>--%>
				      		</tr>
      				<%			
      						}
      					}
      					else
      					{
      						out.println("");
      					}
      				%>
      			
      	</table>
      </td>
   	  <td  width="10%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
   	  	<input type="checkbox" value='<%=LoadUnloadOccupancyTypeKey.DOOR+"_"+new LoadUnloadOccupancyTypeKey().getLoadUnloadOccupancyTypeName(LoadUnloadOccupancyTypeKey.DOOR)+"_"+storageDoorList[i].get("sd_id",0L)+"_"+storageDoorList[i].getString("doorId") %>' onclick="chooseStorage(this)"/>
      </td>
    </tr>
   <% }
  	}
   %>
    </table>
    <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
        <tr> 
		    <td height="28" align="right" valign="middle"> 
		      <%
				int preDoor = pcDoor.getPageNo() - 1;
				int nextDoor = pcDoor.getPageNo() + 1;
				out.println("页数：" + pcDoor.getPageNo() + "/" + pcDoor.getPageCount() + " &nbsp;&nbsp;总数：" + pcDoor.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","首页","javascript:goDoor(1)",null,pcDoor.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:goDoor(" + preDoor + ")",null,pcDoor.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:goDoor(" + nextDoor + ")",null,pcDoor.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","末页","javascript:goDoor(" + pcDoor.getPageCount() + ")",null,pcDoor.isLast()));
			  %>
		                跳转到 
		      <input name="jump_p2" type="text" id="door_jump_p2" style="width:28px;" value="<%=pDoor%>"> 
		      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:goDoor(document.getElementById('door_jump_p2').value)" value="GO"> 
		    </td>
        </tr>
	</table> 
  </body>
</html>
