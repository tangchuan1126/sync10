<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<jsp:useBean id="transportOrderKey" class="com.cwc.app.key.TransportOrderKey"/>
<jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey"></jsp:useBean>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/> 
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="transportQualityInspectionKey" class="com.cwc.app.key.TransportQualityInspectionKey"/> 

<%
	long transport_id = StringUtil.getLong(request,"transport_id");
	
	//转运单
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
	
	int requestIndex = StringUtil.getInt(request,"index");
	
	String file_with_class = StringUtil.getString(request,"file_with_class");
	// 单证
	String valueCertificate = systemConfig.getStringConfigValue("transport_certificate");
	String[] arraySelectedCertificate = valueCertificate.split("\n");
	
	//将arraySelected组成一个List
	ArrayList<String> selectedListCertificate= new ArrayList<String>();//类型对应值数组
	ArrayList<String> selectedListCertificateClass= new ArrayList<String>();//类型数组 与selectedListCertificate 一一对应
	
	for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
			String tempStr = arraySelectedCertificate[index];
			if(tempStr.indexOf("=") != -1){
				String[] tempArray = tempStr.split("=");
				String tempHtml = tempArray[1];
				selectedListCertificateClass.add(tempArray[0]);//新增类型
				selectedListCertificate.add(tempHtml);
			}
	}
	int file_with_type = StringUtil.getInt(request,"file_with_type");
	String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
	
	DBRow[] imageListCertificate = fileMgrZJ.getFileByWithIdAndType(transport_id,FileWithTypeKey.transport_certificate);
 	Map<String,List<DBRow>> mapCertificate = new HashMap<String,List<DBRow>>();
 	TransportCertificateKey certificateKey = new TransportCertificateKey();
	if(imageListCertificate != null && imageListCertificate.length > 0){
		//map<string,List<DBRow>>
		for(int index = 0 , count = imageListCertificate.length ; index < count ;index++ ){
			DBRow temp = imageListCertificate[index];
			List<DBRow> tempListRow = mapCertificate.get(temp.getString("file_with_class"));
			if(tempListRow == null){
				tempListRow = new ArrayList<DBRow>();
			}
			tempListRow.add(temp);
			mapCertificate.put(temp.getString("file_with_class"),tempListRow);
		}
	}

//单证文件基本路径
	String transport_base_path = ConfigBean.getStringValue("systenFolder") + "upload/"+ systemConfig.getStringConfigValue("file_path_transport");
 %>
 		 	<%
			for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ )
			{
				//根据div 索引来判断取得是哪个类型的，也可以通过file_with_class来直接从mapCertificate中取出arrayListTemp
				if( file_with_class.equals(selectedListCertificateClass.get(index)))
				{
			 			List<DBRow> arrayLisTemp = mapCertificate.get(selectedListCertificateClass.get(index));
			 		 %>
			 		<input type="button" class="long-button" onclick="uploadFile('uploadCertificateImageForm','<%=file_with_class %>');" value="选择文件" />
					<input type="button" class="long-button" onclick="onlineScanner('uploadCertificateImageForm');" value="在线获取" />
				
						<% 
			 			if(arrayLisTemp != null && arrayLisTemp.size() > 0 )
			 			{
			 			%>
			 			<ul style="margin: 0 0 0 5px;width: 100%">
			 			
				 		<% for(int listIndex = 0 , total = arrayLisTemp.size() ; listIndex < total ; listIndex++ )
				 			{
				 			
							 	 String tempFilePath = transport_base_path +"/"+arrayLisTemp.get(listIndex).getString("file_name");
							 	 String tempFileName = arrayLisTemp.get(listIndex).getString("file_name");
						%>
						<li style="list-style:none;float:left;border: 1px solid #beceeb;margin: 5px 0 0 15px;height: 120px;width: 100px">
						 <!-- 删除按钮 -->
						   <div align="right" style="height:20px;width:100%;background-color: RGB(238,243,250)"">
						   	 <img width="13px" height=""13px"  title="下载 " src="../imgs/transport/download-over.png"
							 	onmouseover="this.src='../imgs/transport/download-hover.png'" onmouseout="this.src='../imgs/transport/download-over.png'" 
								 onclick="downloadFile('<%=tempFileName %>');"
								 />
						   
							 	<% if(!(transport.get("certificate",0) == TransportCertificateKey.FINISH ))
							 	{
			 					%>
							 	<img width="13px" height="13px" title="删除 " src="../imgs/transport/del_over.png"   onmouseover="getFcuse(this)" onmouseout="lostFcuse(this)" 
							 		onclick="deleteFileCommon('<%=arrayLisTemp.get(listIndex).get("file_id",0l)%>','file','<%=systemConfig.getStringConfigValue("file_path_transport") %>','file_id','<%=index %>','<%=selectedListCertificateClass.get(index) %>')" />
							 	<%} %>
						  </div>
							 			 	     	
							 <%
							 if(StringUtil.isPictureFile(arrayLisTemp.get(listIndex).getString("file_name")))
							 { %>
							<!-- 图片 -->
							 <div class="image_div" style="height:100px;width:100%;">
								<a class="image_a" style="height:100%;width:100%;text-decoration:none;line-height: 100%" 
								 href='<%=tempFilePath%>' rel="prettyPhoto[gallery<%=selectedListCertificateClass.get(index) %>]" title='<%=tempFileName%>'>	
								 	<img width="80px" height="100px"  title="<%=tempFileName %> " src="<%=tempFilePath %> " onmouseover="addBorder(this)" onmouseout="hiddenBorder(this)" />
								</a>
							</div>
							 <%
				 			 } 
				 			 else if(StringUtil.isOfficeFile(arrayLisTemp.get(listIndex).getString("file_name")))
				 			  {
				 			 	   //office文件
					 			 	 String officeFileImgPath = "";
					 			 	 String officefileName = arrayLisTemp.get(listIndex).getString("file_name");
					 			 	 				
					 			 	 if(officefileName.endsWith(".xls") || officefileName.endsWith(".xlsx"))
					 			 	 {
					 			 	 		officeFileImgPath ="../imgs/transport/xlsFile.jpg";
					 			 	 }
					 			 	 else if(officefileName.endsWith(".doc") ||officefileName.endsWith(".docx"))
					 			 	 {
					 			 	 		officeFileImgPath ="../imgs/transport/wordFile.jpg";
					 			 	 }
					 			 	 else if(officefileName.endsWith(".pdf"))
					 			 	 {
					 			 	 		officeFileImgPath ="../imgs/transport/pdfFile.jpg";
					 			     }
				 			 	%>
							 	 <!--office文件 -->
							 	<div class="image_div" style="height:100px;width:100%;" >
							 		<a href="javascript:void(0)"  file_id='<%=arrayLisTemp.get(listIndex).get("file_id",0l) %>' 
							 			onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("file_path_transport")%>')" file_is_convert='<%=arrayLisTemp.get(listIndex).get("file_is_convert",0) %>'>
								 		<img width="80px" height="100px"  title="<%=tempFileName %> " src="<%=officeFileImgPath %>"  />
								 	</a>
							 	</div>
							 	 <%}
				 			 	 else
				 			 	  { %>
				 			 	      <!-- 其他文件 -->
	 		 			 	 	 <%} %>
	 		 			 	 	 
			 					<%
				 			}	%>
				 			</li>
				 			</ul>
			 			 <%}
			 }
		} %>