<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.DifferentKey"%>
<%@ include file="../../include.jsp"%> 
<%
	long transport_id = StringUtil.getLong(request,"transport_id");
	int selectIndex = StringUtil.getInt(request,"select_index");
	
	DBRow[] planSendDifferents = transportMgrZJ.planSendDifferents(transport_id);
	DBRow[] sendReceiveDifferents = transportMgrZJ.sendReceiveDifferents(transport_id);
	
	DBRow[] planSendDifferentsSN = transportMgrZJ.planSendDifferentsSN(transport_id);
	DBRow[] sendReceiveDifferentsSN = transportMgrZJ.sendReceiveDifferentsSN(transport_id);
	
	DifferentKey differentKey = new DifferentKey();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script language="javascript">
$(function(){
	 $("#all_different").tabs({
			cache: true,
			selected:<%=selectIndex%>,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
		});	
	
	$("#different_send_show").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
		});	
	
	$("#different_receive_show").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
		});	
		
		
 })
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购及转运应用 » 交货单审核</td>
  </tr>
</table>
<br>
	<div id="all_different">
		 <ul>
			 <li><a href="#different_send_show">发货差异</a></li>
			 <li><a href="#different_receive_show">收货差异</a></li>		
		 </ul>
		 <div id="different_send_show">
		 	<ul>
		 		<li><a href="#send_quantity_different">数量差异</a></li>
		 		<li><a href="#send_sn_different">序列号差异</a></li>
		 	</ul>
		 	<div id="send_quantity_different">
		 		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 			<tr>
		 				<td style="font-size:16px;">商品名</td>
		 				<td style="font-size:16px;" align="center">计划数</td>
		 				<td style="font-size:16px;" align="center">发货数</td>
		 				<td style="font-size:16px;" align="center">不同数</td>
		 				<td style="font-size:16px;" align="center">状态</td>
		 			</tr>
		 			<%
		 				for(int i = 0;i<planSendDifferents.length;i++)
		 				{
		 			%>
		 				<tr>
		 					<td style="font-size:16px;"><%=planSendDifferents[i].getString("plan_p_name")%></td>
		 					<td style="font-size:16px;" align="center"><%=planSendDifferents[i].get("plan_count",0f)%></td>
		 					<td style="font-size:16px;" align="center"><%=planSendDifferents[i].get("send_count",0f)%></td>
		 					<td style="font-size:16px;" align="center">
		 						<%
		 							int differentType = planSendDifferents[i].get("different_type",0);
		 							String color = "";
		 							if(differentType==DifferentKey.Equal)
		 							{
		 								color = "green";
		 							}
		 							if(differentType==DifferentKey.More)
		 							{
		 								color = "blue";
		 							}
		 							if(differentType==DifferentKey.Lack)
		 							{
		 								color = "red";
		 							}
		 						%>
		 						<%=planSendDifferents[i].get("different_count",0f)%>
		 					</td>
		 					<td  style="font-size:16px;color:<%=color%>" align="center"><%=differentKey.getDifferentKeyValue(planSendDifferents[i].get("different_type",0))%></td>
		 				</tr>
		 			<%
		 				}
		 			%>
		 		</table>
		 	</div>
		 	<div id="send_sn_different">
		 		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 			<tr>
		 				<td style="font-size:16px;">商品名</td>
		 				<td style="font-size:16px;" align="center">序列号</td>
		 				<td style="font-size:16px;" align="center">计划数</td>
		 				<td style="font-size:16px;" align="center">发货数</td>
		 				<td style="font-size:16px;" align="center">不同数</td>
		 				<td style="font-size:16px;" align="center">状态</td>
		 			</tr>
		 			<%
		 				for(int i = 0;i<planSendDifferentsSN.length;i++)
		 				{
		 			%>
		 			<tr>
		 				<td style="font-size:16px;"><%=planSendDifferentsSN[i].getString("plan_p_name")%></td>
		 				<td style="font-size:16px;" align="center"><%=planSendDifferentsSN[i].getString("plan_serial_number")%></td>
		 				<td style="font-size:16px;" align="center"><%=planSendDifferentsSN[i].get("plan_count",0f)%></td>
		 				<td style="font-size:16px;" align="center"><%=planSendDifferentsSN[i].get("send_count",0f)%></td>
		 				<td style="font-size:16px;" align="center"><%=planSendDifferentsSN[i].get("different_count",0f)%></td>
		 				<td style="font-size:16px;" align="center"><%=differentKey.getDifferentKeyValue(planSendDifferents[i].get("different_type",0))%></td>
		 			</tr>
		 			<%
		 				}
		 			%>
		 		</table>
		 	</div>
		 </div>
		  <div id="different_receive_show">
		 	<ul>
		 		<li><a href="#receive_quantity_diffeent">数量差异</a></li>
		 		<li><a href="#receive_sn_different">序列号差异</a></li>
		 	</ul>
		 	<div id="receive_quantity_diffeent">
		 		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 			<tr>
		 				<td style="font-size:16px;">商品名</td>
		 				<td style="font-size:16px;" align="center">发货数</td>
		 				<td style="font-size:16px;" align="center">收货数</td>
		 				<td style="font-size:16px;" align="center">不同数</td>
		 				<td style="font-size:16px;" align="center">状态</td>
		 			</tr>
		 			<%
		 				for(int i = 0;i<sendReceiveDifferents.length;i++)
		 				{
		 			%>
		 				<tr>
		 					<td style="font-size:16px;"><%=sendReceiveDifferents[i].getString("send_p_name")%></td>
		 					<td style="font-size:16px;" align="center"><%=sendReceiveDifferents[i].get("send_count",0f)%></td>
		 					<td style="font-size:16px;" align="center"><%=sendReceiveDifferents[i].get("receive_count",0f)%></td>
		 					<td style="font-size:16px;" align="center">
		 						<%
		 							int differentType = sendReceiveDifferents[i].get("different_type",0);
		 							String color = "";
		 							if(differentType==DifferentKey.Equal)
		 							{
		 								color = "green";
		 							}
		 							if(differentType==DifferentKey.More)
		 							{
		 								color = "blue";
		 							}
		 							if(differentType==DifferentKey.Lack)
		 							{
		 								color = "red";
		 							}
		 						%>
		 						<%=sendReceiveDifferents[i].get("different_count",0f)%>
		 					</td>
		 					<td  style="font-size:16px;color:<%=color%>" align="center"><%=differentKey.getDifferentKeyValue(sendReceiveDifferents[i].get("different_type",0))%></td>
		 				</tr>
		 			<%
		 				}
		 			%>
		 		</table>
		 	</div>
		 	<div id="receive_sn_different">
		 		<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
		 			<tr>
		 				<td style="font-size:16px;">商品名</td>
		 				<td style="font-size:16px;" align="center">序列号</td>
		 				<td style="font-size:16px;" align="center">发货数</td>
		 				<td style="font-size:16px;" align="center">收货数</td>
		 				<td style="font-size:16px;" align="center">不同数</td>
		 				<td style="font-size:16px;" align="center">状态</td>
		 			</tr>
		 			<%
		 				for(int i = 0;i<sendReceiveDifferentsSN.length;i++)
		 				{
		 			%>
		 			<tr>
		 				<td style="font-size:16px;"><%=sendReceiveDifferentsSN[i].getString("p_name")%></td>
		 				<td style="font-size:16px;" align="center"><%=sendReceiveDifferentsSN[i].getString("serial_number")%></td>
		 				<td style="font-size:16px;" align="center"><%=sendReceiveDifferentsSN[i].get("send_count",0f)%></td>
		 				<td style="font-size:16px;" align="center"><%=sendReceiveDifferentsSN[i].get("receive_count",0f)%></td>
		 				<td style="font-size:16px;" align="center"><%=sendReceiveDifferentsSN[i].get("different_count",0f)%></td>
		 				<td style="font-size:16px;" align="center"><%=differentKey.getDifferentKeyValue(sendReceiveDifferentsSN[i].get("different_type",0))%></td>
		 			</tr>
		 			<%
		 				}
		 			%>
		 		</table>
		 	</div>
		 </div>
	</div>
<br>
<br>
</body>
</html>
