<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.WaybillInternalTrackingKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.LoadUnloadRelationTypeKey"%>
<%@page import="com.cwc.app.key.LoadUnloadOccupancyStatusKey"%>
<%@page import="com.cwc.app.key.LoadUnloadOccupancyTypeKey"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.cwc.app.key.TransportRegistrationTypeKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.TransportLogTypeKey"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>选择门或位置</title>
<%
	TDate td = new TDate();
	td.addHour(1);
	String	book_start_time = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay()+" "+DateUtil.getStrCurrHour()+":"+DateUtil.getStrCurrMinute();
	String 	book_end_time =	  td.getStringYear()+"-"+td.getStringMonthDouble()+"-"+td.getStringDayDouble()+" "+td.getStringHourDouble()+":"+td.getStringMinuteDouble();
	long transport_id =	StringUtil.getLong(request, "transport_id");
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
	long send_psid	= transport.get("send_psid", 0L);
	long receive_psid	= transport.get("receive_psid", 0L);
	int rel_occupancy_use = StringUtil.getInt(request, "rel_occupancy_use");
	long psId = 0;
	if(TransportRegistrationTypeKey.SEND == rel_occupancy_use)
	{
		psId = send_psid;
	}
	else if(TransportRegistrationTypeKey.DELEIVER == rel_occupancy_use)
	{
		psId = receive_psid;
	}
	if(0 == psId)
	{
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean( request.getSession(true) );
		psId = adminLoggerBean.getPs_id();
	}
%>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<script language="javascript" src="../../common.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script src="../js/datepicker/date_HHMMSS.js" type="text/javascript"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript">
/*
$.blockUI.defaults = {
		  css: { 
		   padding:        '8px',
		   margin:         0,
		   width:          '170px', 
		   top:            '45%', 
		   left:           '40%', 
		   textAlign:      'center', 
		   color:          '#000', 
		   border:         '3px solid #999999',
		   backgroundColor:'#eeeeee',
		   '-webkit-border-radius': '10px',
		   '-moz-border-radius':    '10px',
		   '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		   '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		  },
		  //设置遮罩层的样式
		  overlayCSS:  { 
		   backgroundColor:'#000', 
		   opacity:        '0.6' 
		  },
		  baseZ: 99999, 
		  centerX: true,
		  centerY: true, 
		  fadeOut:  1000,
		  showOverlay: true
		 };
$(function(){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/door_use_info_list.html',
		type: 'post',
		data:{transport_id:<%=transport_id%>},
		dataType: 'html',
		timeout: 60000,
		cache:false,
		beforeSend:function(request){
		},
		error: function(){
			showMessage("加载门占用信息失败","error");
		},
		success: function(html){
			$("#door").html(html);
		}
	});
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/location_use_info_list.html',
		type: 'post',
		data:{transport_id:<%=transport_id%>},
		dataType: 'html',
		timeout: 60000,
		cache:false,
		beforeSend:function(request){
		},
		error: function(){
			showMessage("加载位置占用信息失败","error");
		},
		success: function(html){
			$("#location").html(html);
		}
	});

});
function goDoor(index){
    $.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/door_use_info_list.html',
		type: 'post',
		data:{pDoor:index,transport_id:<%=transport_id%>},
		dataType: 'html',
		timeout: 60000,
		cache:false,
		beforeSend:function(request){
		},
		error: function(){
			showMessage("加载门占用信息失败","error");
		},
		success: function(html){
			$("#door").html(html);
		}
	});
}
function goLoc(index){
    $.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/location_use_info_list.html',
		type: 'post',
		data:{pLoc:index,transport_id:<%=transport_id%>},
		dataType: 'html',
		timeout: 60000,
		cache:false,
		beforeSend:function(request){
		},
		error: function(){
			showMessage("加载位置占用信息失败","error");
		},
		success: function(html){
			$("#location").html(html);
		}
	});
}
function submitData(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	 $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/AddDoorOrLocationUseInfoAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
				if(data && data.flag == "success"){
					setTimeout("windowClose()", 1000);
				}
			},
			error:function(){
				$.unblockUI();
			}
		})	
};
function isRightDateFormat(obj)
{
	if(null != obj.value && "" != obj.value)
	{
		//  \s  /^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))(\s(([01]\d{1})|(2[0123])):([0-5]\d):([0-5]\d))?$/;
		var reg = /^(\d{4}-\d{2}-\d{2}\s{1}\d{2}:([0-5]\d))?$/;
		if(!reg.test(obj.value))
		{
			alert("请输入正确的时间格式:2013-07-02 11:30");
		}
	}
}
function chooseStorage(obj)
{
	var objValue		= obj.value;
	var infoArr			= objValue.split("_");
	var occupancyType	= infoArr[0];
	var occupancyTypeNa = infoArr[1];
	var occupancyId		= infoArr[2];
	var occupancyName	= infoArr[3];
	var occupancyTypeAndId	= occupancyType + "_" + occupancyId;

	if($("#choosed_door_or_location_table tr[id='"+occupancyTypeAndId+"']").attr("id"))
	{
		if(!$("input:checkbox[value='"+objValue+"']").attr("checked"))
		{
			$("#choosed_door_or_location_table tr[id='"+occupancyTypeAndId+"']").remove();
		}
	}
	else
	{
		if($("input:checkbox[value='"+objValue+"']").attr("checked"))
		{
			addChoosedDoorOrLocationTr(occupancyType,occupancyTypeNa,occupancyId,occupancyName);
		}
	}
	
}
function addChoosedDoorOrLocationTr(occupancyType,occupancyTypeNa,occupancyId,occupancyName)
{
	var html = '<tr id=' + (occupancyType+"_"+occupancyId) + '>'
			+'		<td>'+occupancyTypeNa + ":&nbsp;" + occupancyName
			+'			<input type="hidden" name="occupancy_types" value="'+occupancyType+'"/>'
			+'			<input type="hidden" name="occupancy_ids" value="'+occupancyId+'"/>'
			+'		</td>'
			+'		<td>'
			+'			<input maxlength="100" value="<%=book_start_time%>" name="book_start_times" onclick="SelectDate(this,\'yyyy-MM-dd hh:mm\',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>&nbsp;--'
			+'			<input maxlength="100" value="<%=book_end_time%>" name="book_end_times" onclick="SelectDate(this,\'yyyy-MM-dd hh:mm\',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>'
			+'		</td>'
			+'		<td>'
			+'			<a href="javascript:void(0)" onclick="deleteThisTr(this,\''+(occupancyType+"_"+occupancyId+"_"+occupancyName)+'\')" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>'
			+'		</td>'
			+'	</tr>';
	$("#choosed_door_or_location_table tr:last").after($(html));
}
function deleteThisTr(delBut,trValue)
{
	$(delBut).parent().parent().remove();
	$("input:checkbox[value='"+trValue+"']").attr("checked",false);
}
function loadCanLoadDoorOrLocation()
{
	
}
*/
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function cancelOccupancy(id)
{
		$.artDialog.confirm('确定要取消[ '+id+' ]吗？', function(){
			$("#occupancy_status").val('<%=LoadUnloadOccupancyStatusKey.QUIT %>');
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/UpdateDoorOrLocationUseInfoStatueAction.action',
				data:$("#subForm").serialize()+"&id="+id,
				dataType:'json',
				type:'post',
				beforeSend:function(request){
				
				},
				success:function(data){
					if(data && data.flag == "success"){
						refreshWindow();
					}
				},
				error:function(){
					
				}
			})	

    	}, function(){
    	});
}
function releaseOccupancy(id)
{
	if(null != $("#book_end_time_"+id).val() && '' != $("#book_end_time_"+id).val())
	{
		var starttime=new Date(($("#book_start_time_"+id).val()).replace(/-/g,"/"));
	    var endtime=new Date(($("#book_end_time_"+id).val()).replace(/-/g,"/"));
	    if(starttime < endtime)
		{
			$.artDialog.confirm('确定要释放[ '+id+' ]吗？', function(){
				$("#occupancy_status").val('<%=LoadUnloadOccupancyStatusKey.RELEASE %>');
				$.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/UpdateDoorOrLocationUseInfoStatueAction.action',
					data:$("#subForm").serialize()+"&id="+id,
					dataType:'json',
					type:'post',
					beforeSend:function(request){
					
					},
					success:function(data){
						if(data && data.flag == "success"){
							refreshWindow();
						}
					},
					error:function(){
						
					}
				})	
		
			}, function(){
			});
		}
	    else
		{
			alert("结束时间需晚于开始时间");
		}
	}
	else
	{
		alert("释放时间不能为空");
	}
}
function bookDoorOrLocation(transport_id, rel_occupancy_use)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/book_door_or_location.html?transport_id='+transport_id+'&rel_occupancy_use='+rel_occupancy_use; 
	$.artDialog.open(uri , {title: "转运单["+transport_id+"]司机签到",width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
}
function refreshWindow() {window.location.reload();}
</script>

</head>
<body style="margin-top: 0px; margin-bottom: 0px;">
<div style="width: 100%; height: 100%;margin-top: 0px; margin-bottom: 0px; ">
<table style="width: 100%; height: 100%;">
	<tr style="width: 100%;height:10%;" valign="top">
		<td colspan="2" align="left" valign="top" style="height: 100%; width: 100%;">	
		<%
			if(TransportRegistrationTypeKey.SEND == rel_occupancy_use || 0 == rel_occupancy_use)
			{
		%>
			<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;width: 90%;margin-top: 10px;margin-bottom: 10px;">
				<legend style="font-size:15px;font-weight:normal;color:#999999;">
					<%
					DBRow[] locationOccupancys = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(0,0,
							ProductStoreBillKey.TRANSPORT_ORDER, transport_id , -LoadUnloadOccupancyStatusKey.QUIT, null, null, null, null,send_psid,2,TransportRegistrationTypeKey.SEND);
					if(locationOccupancys.length > 0)
					{
					%>
						<img title="签到人" src="../imgs/order_client.gif" />
	 					<%=null==adminMgrLL.getAdminById(locationOccupancys[0].getString("creator"))?"":adminMgrLL.getAdminById(locationOccupancys[0].getString("creator")).getString("employe_name")%>
	  					<img  title="签到时间" src="../imgs/alarm-clock--arrow.png"/>
	  					<%
	      					SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	      					SimpleDateFormat to = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	      				%>
	      				<%=to.format(from.parse(locationOccupancys[0].getString("create_time"))) %>
	      				装货签到
					<%	
					}
					else{out.println("未签到装货");}
					%>
					
				</legend>	
				<form action="" id="subForm" method="post">
					<input type="hidden" name="rel_type" id="rel_type" value='<%=ProductStoreBillKey.TRANSPORT_ORDER %>'/>
					<input type="hidden" name="rel_id" id="rel_id" value='<%=transport_id %>'/>
					<input type="hidden" name="is_registration" id="is_registration" value='1'/>
					<input type="hidden" name="occupancy_status" id="occupancy_status" value=""/>
					<table width="100%" border="0" cellspacing="3" cellpadding="2" style="border-bottom: 2px #cccccc solid; margin-bottom: 3px;">
						<tr>
							<td width="10%" align="left">负责人:</td>
							<td width="25%" align="left"><%=transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(transport_id), ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.UNLOAD_REGISTER) %></td>
							<td width="45%" align="left">通知：
								<%
						    		DBRow schedule = scheduleMgrZR.getScheduleByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.UNLOAD_REGISTER);
							    	int isMail			= 2;//1提醒
						    		int isMessage		= 2;
						    		int isPage			= 2;	
						   		 	if(null != schedule){
						   		 		isMail		= schedule.get("sms_email_notify",2);//1提醒
							   		 	isMessage	= schedule.get("sms_short_notify",2);
							    		isPage		= schedule.get("is_need_replay",2);
						    		}
						   		 //System.out.println(mailCertificate+"---"+messageCertificate+"---"+pageCertificate);
						    	%>
						   		<input type="checkbox" name="isMail" <%if(1 == isMail || 2 == isMail){out.print("checked='checked'");}%>/>邮件
						   		<input type="checkbox" name="isMessage" <%if(1 == isMessage || 2 == isMessage){out.print("checked='checked'");}%>/>短信
						   		<input type="checkbox" name="isPage" <%if(1 == isPage || 2 == isPage){out.print("checked='checked'");}%>/>页面
					   		</td>
					   		<td width="20%" align="left">
					   			<%	
						  	  		//if(TransportOrderKey.PACKING == transport.get("transport_status",0))
						  	  		//{
						  	  	%>
					  	  			&nbsp;&nbsp;<input type="button" class="long-button" value="装货司机签到" onclick="bookDoorOrLocation('<%=transport_id %>','<%=TransportRegistrationTypeKey.SEND %>')"/>
					  	  		<%
						  	  		//}
					  	  		%>
					   		</td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="3" cellpadding="2" id="choosed_door_or_location_table" style="margin-top: 5px;">
	        			<tr>
							<td width="10%" align="left">占用号</td><td width="25%" align="left">占用门/位置</td><td width="45%" align="left">占用时间段</td><td width="20%"></td>
						</tr>
						<%
						if(locationOccupancys.length > 0)
						{
							for(int i = 0; i < locationOccupancys.length; i ++)
							{
								DBRow doorOrLocRow = new DBRow();
								String doorOrLocName = "";
								if(LoadUnloadOccupancyTypeKey.DOOR == locationOccupancys[i].get("occupancy_type",0))
								{
									doorOrLocRow = storageDoorLocationMgrZYZ.getDetailStorageDoor(locationOccupancys[i].get("rl_id",0L));
									doorOrLocName = doorOrLocRow.getString("doorId");
								}
								else
								{
									doorOrLocRow = storageDoorLocationMgrZYZ.getDetailLoadUnloadLocation(locationOccupancys[i].get("rl_id",0L));
									doorOrLocName = doorOrLocRow.getString("location_name");
								}
						%>
								<tr id='<%=locationOccupancys[i].get("occupancy_type",0)+"_"+locationOccupancys[i].get("rl_id",0L) %>'>
									<td>&nbsp;<%=locationOccupancys[i].get("id",0) %></td>
									<td><%=doorOrLocName %>&nbsp;(<%=new LoadUnloadOccupancyTypeKey().getLoadUnloadOccupancyTypeName(locationOccupancys[i].get("occupancy_type",0)) %>)
										<input type="hidden" name="occupancy_types" value='<%=locationOccupancys[i].get("occupancy_type",0)%>'/>
										<input type="hidden" name="occupancy_ids" value='<%=locationOccupancys[i].get("rl_id",0L) %>'/>
									</td>
									<td>
									<%=locationOccupancys[i].getString("book_start_time")%>&nbsp;&nbsp;
									<input value='<%=locationOccupancys[i].getString("book_start_time")%>' type="hidden" id='book_start_time_<%=locationOccupancys[i].get("id",0) %>' name='book_start_time_<%=locationOccupancys[i].get("id",0) %>'/>
									<%
									if("".equals(locationOccupancys[i].getString("book_end_time")))
									{
									%>
									--&nbsp;&nbsp;<input value='' maxlength="100" id='book_end_time_<%=locationOccupancys[i].get("id",0) %>' name='book_end_time_<%=locationOccupancys[i].get("id",0) %>' onclick="SelectDate(this,'yyyy-MM-dd hh:mm',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>
									<%	
									}
									else
									{
									%>
									--&nbsp;&nbsp;<%=locationOccupancys[i].getString("book_end_time")%>
									<%
									}
									%>
<%--										<input maxlength="100" type="hidden" value='<%=locationOccupancys[i].getString("book_start_time")%>' name="book_start_times" onclick="SelectDate(this,\'yyyy-MM-dd hh:mm\',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>&nbsp;----%>
<%--										<input maxlength="100" type="hidden" value='<%=locationOccupancys[i].getString("book_end_time")%>' name="book_end_times" onclick="SelectDate(this,\'yyyy-MM-dd hh:mm\',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>--%>
									</td>
									<td>
										<%
											//DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
											//Date book_end_time_date = df.parse(locationOccupancys[i].getString("book_end_time"));
											//Date now_date	= df.parse(DateUtil.NowStr());
											//if(book_end_time_date.after(now_date))
											if("".equals(locationOccupancys[i].getString("book_end_time")))
											{
										%>
										<input type="button" class="short-short-button" value="释放" onclick='releaseOccupancy(<%=locationOccupancys[i].get("id",0) %>)'/>
										<input type="button" class="short-short-button" value="取消" onclick='cancelOccupancy(<%=locationOccupancys[i].get("id",0) %>)'/>
										<%		
											}
										%>
<%--										<a href="javascript:void(0)" onclick="deleteThisTr(this,'<%=locationOccupancys[i].get("occupancy_type",0)+"_"+locationOccupancys[i].get("rl_id",0L)+"_"+doorOrLocName %>')" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>'--%>
											
									</td>
								</tr>
							
						<%
							}
						}
						%>
					</table>
				</form>
			</fieldset>
		<%} %>	
		<%
			if(TransportRegistrationTypeKey.DELEIVER == rel_occupancy_use || 0 == rel_occupancy_use)
			{
		%>
			<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;width: 90%;margin-top: 10px;margin-bottom: 10px;">
				<legend style="font-size:15px;font-weight:normal;color:#999999;">
					<%
					DBRow[] locationOccupancysUnload = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(0,0,
							ProductStoreBillKey.TRANSPORT_ORDER, transport_id , -LoadUnloadOccupancyStatusKey.QUIT, null, null, null, null,receive_psid,2,TransportRegistrationTypeKey.DELEIVER);
					if(locationOccupancysUnload.length > 0)
					{
					%>
						<img title="签到人" src="../imgs/order_client.gif" />
	 					<%=null==adminMgrLL.getAdminById(locationOccupancysUnload[0].getString("creator"))?"":adminMgrLL.getAdminById(locationOccupancysUnload[0].getString("creator")).getString("employe_name")%>
	  					<img  title="签到时间" src="../imgs/alarm-clock--arrow.png"/>
	  					<%
	      					SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	      					SimpleDateFormat to = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	      				%>
	      				<%=to.format(from.parse(locationOccupancysUnload[0].getString("create_time"))) %>
	      				缷货签到
					<%	
					}else{out.println("未签到缷货");}
					%>
				</legend>	
				<form action="" id="subForm" method="post">
					<input type="hidden" name="rel_type" id="rel_type" value='<%=ProductStoreBillKey.TRANSPORT_ORDER %>'/>
					<input type="hidden" name="rel_id" id="rel_id" value='<%=transport_id %>'/>
					<input type="hidden" name="is_registration" id="is_registration" value='1'/>
					<input type="hidden" name="occupancy_status" id="occupancy_status" value=""/>
					<table width="100%" border="0" cellspacing="3" cellpadding="2" style="border-bottom: 2px #cccccc solid; margin-bottom: 3px;">
						<tr>
							<td width="10%" align="left">负责人:</td>
							<td width="25%" align="left"><%=transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(String.valueOf(transport_id), ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.LOAD_REGISTER) %></td>
							<td width="45%" align="left">通知：
								<%
						    		DBRow schedule = scheduleMgrZR.getScheduleByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.LOAD_REGISTER);
							    	int isMail			= 2;//1提醒
						    		int isMessage		= 2;
						    		int isPage			= 2;	
						   		 	if(null != schedule){
						   		 		isMail		= schedule.get("sms_email_notify",2);//1提醒
							   		 	isMessage	= schedule.get("sms_short_notify",2);
							    		isPage		= schedule.get("is_need_replay",2);
						    		}
						   		 //System.out.println(mailCertificate+"---"+messageCertificate+"---"+pageCertificate);
						    	%>
						   		<input type="checkbox" name="isMail" <%if(1 == isMail || 2 == isMail){out.print("checked='checked'");}%>/>邮件
						   		<input type="checkbox" name="isMessage" <%if(1 == isMessage || 2 == isMessage){out.print("checked='checked'");}%>/>短信
						   		<input type="checkbox" name="isPage" <%if(1 == isPage || 2 == isPage){out.print("checked='checked'");}%>/>页面
					   		</td>
					   		<td width="20%" align="left">
					   			<%	
						  	  		//if(TransportOrderKey.PACKING == transport.get("transport_status",0))
						  	  		//{
						  	  	%>
					  	  			&nbsp;&nbsp;<input type="button" class="long-button" value="缷货司机签到" onclick="bookDoorOrLocation('<%=transport_id %>','<%=TransportRegistrationTypeKey.DELEIVER %>')"/>
					  	  		<%
						  	  		//}
					  	  		%>
					   		</td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="3" cellpadding="2" id="choosed_door_or_location_table">
	        			<tr>
							<td>占用号</td><td>占用门/位置</td><td>占用时间段</td><td>&nbsp;<!-- 操作 --></td>
						</tr>
						<%
						if(locationOccupancysUnload.length > 0)
						{
							for(int i = 0; i < locationOccupancysUnload.length; i ++)
							{
								DBRow doorOrLocRow = new DBRow();
								String doorOrLocName = "";
								if(LoadUnloadOccupancyTypeKey.DOOR == locationOccupancysUnload[i].get("occupancy_type",0))
								{
									doorOrLocRow = storageDoorLocationMgrZYZ.getDetailStorageDoor(locationOccupancysUnload[i].get("rl_id",0L));
									doorOrLocName = doorOrLocRow.getString("doorId");
								}
								else
								{
									doorOrLocRow = storageDoorLocationMgrZYZ.getDetailLoadUnloadLocation(locationOccupancysUnload[i].get("rl_id",0L));
									doorOrLocName = doorOrLocRow.getString("location_name");
								}
						%>
								<tr id='<%=locationOccupancysUnload[i].get("occupancy_type",0)+"_"+locationOccupancysUnload[i].get("rl_id",0L) %>'>
									<td><%=locationOccupancysUnload[i].get("id",0) %></td>
									<td><%=doorOrLocName %>&nbsp;(<%=new LoadUnloadOccupancyTypeKey().getLoadUnloadOccupancyTypeName(locationOccupancysUnload[i].get("occupancy_type",0)) %>)
										<input type="hidden" name="occupancy_types" value='<%=locationOccupancysUnload[i].get("occupancy_type",0)%>'/>
										<input type="hidden" name="occupancy_ids" value='<%=locationOccupancysUnload[i].get("rl_id",0L) %>'/>
									</td>
									<td>
									<%=locationOccupancysUnload[i].getString("book_start_time")%>&nbsp;&nbsp;
									<input value='<%=locationOccupancysUnload[i].getString("book_start_time")%>' type="hidden" id='book_start_time_<%=locationOccupancysUnload[i].get("id",0) %>' name='book_start_time_<%=locationOccupancysUnload[i].get("id",0) %>'/>
									<%
									if("".equals(locationOccupancysUnload[i].getString("book_end_time")))
									{
									%>
									--&nbsp;&nbsp;<input value='' maxlength="100" id='book_end_time_<%=locationOccupancysUnload[i].get("id",0) %>' name='book_end_time_<%=locationOccupancysUnload[i].get("id",0) %>' onclick="SelectDate(this,'yyyy-MM-dd hh:mm',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>
									<%	
									}
									else
									{
									%>
									--&nbsp;&nbsp;<%=locationOccupancysUnload[i].getString("book_end_time")%>
									<%
									}
									%>
<%--										<input maxlength="100" type="hidden" value='<%=locationOccupancysUnload[i].getString("book_start_time")%>' name="book_start_times" onclick="SelectDate(this,\'yyyy-MM-dd hh:mm\',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>&nbsp;----%>
<%--										<input maxlength="100" type="hidden" value='<%=locationOccupancysUnload[i].getString("book_end_time")%>' name="book_end_times" onclick="SelectDate(this,\'yyyy-MM-dd hh:mm\',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>--%>
									</td>
									<td>
										<%
											//DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
											//Date book_end_time_date = df.parse(locationOccupancysUnload[i].getString("book_end_time"));
											//Date now_date	= df.parse(DateUtil.NowStr());
											if("".equals(locationOccupancysUnload[i].getString("book_end_time")))
											{
										%>
										<input type="button" class="short-short-button" value="释放" onclick='releaseOccupancy(<%=locationOccupancysUnload[i].get("id",0) %>)'/>
										<input type="button" class="short-short-button" value="取消" onclick='cancelOccupancy(<%=locationOccupancysUnload[i].get("id",0) %>)'/>
										<%		
											}
										%>
<%--										<a href="javascript:void(0)" onclick="deleteThisTr(this,'<%=locationOccupancys[i].get("occupancy_type",0)+"_"+locationOccupancys[i].get("rl_id",0L)+"_"+doorOrLocName %>')" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>'--%>
											
									</td>
								</tr>
							
						<%
							}
						}
						%>
					</table>
				</form>
			</fieldset>
		<%} %>	
		</td>
	</tr>
<%--	<tr style="width: 100%;height:10%;" valign="top">--%>
<%--		<td colspan="2" align="left" valign="top" style="height: 100%; width: 100%;">	--%>
<%--			<fieldset style="border:2px #cccccc solid;padding:5px;-webkit-border-radius:5px;-moz-border-radius:5px;width: 90%;margin-top: 10px;margin-bottom: 10px;">--%>
<%--				<legend style="font-size:15px;font-weight:normal;color:#999999;">--%>
<%--					<%--%>
<%--						if(send_psid == psId)--%>
<%--						{--%>
<%--							out.println("搜索装货门/位置");--%>
<%--						}--%>
<%--						else--%>
<%--						{--%>
<%--							out.println("搜索缷货门/位置");--%>
<%--						}--%>
<%--					%>--%>
<%--				</legend>	--%>
<%--				<form action="" id="searchForm" method="post">--%>
<%--					<input type="hidden" name="choosed_doors_or_locations_type_ids" id="choosed_doors_or_locations_type_ids"/>--%>
<%--					<table width="100%" border="0" cellspacing="3" cellpadding="2">--%>
<%--	        			<tr>--%>
<%--							<td>--%>
<%--								预定时间--%>
<%--							</td>--%>
<%--							<td>--%>
<%--								<input value='<%=book_start_time %>' maxlength="100"  id="book_start_time" name="book_start_time" onclick="SelectDate(this,'yyyy-MM-dd hh:mm',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>&nbsp;----%>
<%--								<input value='<%=book_end_time %>' maxlength="100" id="book_end_time" name="book_end_time" onclick="SelectDate(this,'yyyy-MM-dd hh:mm',1)" onkeydown="if(event.keyCode==13){dateInputOnKeydown();}"/>--%>
<%--								<input type="button" class="long-button" value="满足条件" onclick="loadCanLoadDoorOrLocation()"/>--%>
<%--								<input type="button" class="long-button" value="可能满足条件" onclick="loadCanLoadDoorOrLocation()"/>--%>
<%--							</td>--%>
<%--						</tr>--%>
<%--					</table>--%>
<%--				</form>--%>
<%--			</fieldset>--%>
<%--		</td>--%>
<%--	</tr>--%>
<%--	<tr style="width: 80%;">--%>
<%--		<td style="height: 100%">--%>
<%--			<div class="demo" style="width:98%;margin:0px auto;" >--%>
<%--				<div id="tabs" style="margin-top:15px;">--%>
<%--					<ul>--%>
<%--						<li><a href="#door">装卸门</a></li>--%>
<%--						<li><a href="#location">装卸位置</a></li>--%>
<%--					</ul>--%>
<%--					<div id="door"></div>--%>
<%--					<div id="location"></div>--%>
<%--				</div>--%>
<%--			</div>--%>
<%--		</td>--%>
<%--	</tr>--%>
<%--  <tr style="height: 10%;">--%>
<%--		<td align="right" valign="bottom" style="width: 100%; height: 100%;">				--%>
<%--			<table style="height: 100%;width: 100%;">--%>
<%--				<tr style="height: 100%;width: 100%;">--%>
<%--					<td colspan="2" align="right" valign="middle" style="height: 100%;width: 100%;" class="win-bottom-line">				--%>
<%--						<input type="button" class="normal-green" value="提交" onclick="submitData();"/>--%>
<%--						<input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">--%>
<%--					</td>--%>
<%--				</tr>--%>
<%--			</table>--%>
<%--		</td>--%>
<%--	</tr>--%>
</table>
</div>		
	<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>