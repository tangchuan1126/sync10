<!-- 
-- @首页
-- @since Sync10-ui 1.0
-- @author subin
-->
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../include.jsp"%>
<%
response.setHeader("Pragma","No-cache");
response.setHeader("Cache-Control","no-cache");
response.setDateHeader("Expires", 0);

AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

String homePage = accountMgr.getAccountDefaultHomePage(adminLoggerBean.getAdid());
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><%=systemConfig.getStringConfigValue("webtitle")%> &#8482; </title>
</head>
<frameset rows="45,*" cols="*" frameborder="NO" border="0" framespacing="0">
	
	<!-- 顶部信息条  -->
	<frame src="information.html" name="topFrame" scrolling="no" noresize >
	
	<frameset cols="240,14,*" frameborder="NO" border="0" framespacing="0" id="mainFrm">
		<!-- 左侧菜单  -->
		<frame src="navigationBar.html" name="leftFrame" scrolling="no" noresize>
		<!-- 左侧菜单显示/隐藏  -->
		<frame src="hidden.htm" scrolling="NO" name="switch" noresize>
		<!-- 右侧主页  -->
		<frame src="<%=homePage%>" name="main" scrolling="auto" noresize>	
	</frameset>
</frameset>
</html>
 
