<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="returnServiceAccessoriesKey" class="com.cwc.app.key.ReturnServiceAccessoriesKey"></jsp:useBean>
<jsp:useBean id="returnServicePackagingKey" class="com.cwc.app.key.ReturnServicePackagingKey"></jsp:useBean>
<jsp:useBean id="returnServicePalletItemStatusKey" class="com.cwc.app.key.ReturnServicePalletItemStatusKey"></jsp:useBean>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<jsp:useBean id="returnServiceCustRefNoKey" class="com.cwc.app.key.ReturnServiceCustRefNoKey"></jsp:useBean>
<html>
<head>

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>

<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>


<%
long item_id = StringUtil.getLong(request, "item_id");
DBRow returnRow = returnMgrZyj.findReturnPalletItemRowById(item_id);
long pallet_view_id = StringUtil.getLong(request, "pallet_view_id");
pallet_view_id = null==returnRow?pallet_view_id:returnRow.get("pallet_view_id", 0L);
DBRow palletViewRow = returnMgrZyj.findReturnPalletRowById(pallet_view_id);

%>
<script type="text/javascript">
$(function(){
	if(0 == "<%=item_id%>"*1)
	{
		addOneRowAndRef();
	}
	$("#box_serial_number").focus(function()
	{
		$(this).val($("#serial_number").val());
	});
});
function addRow()
{
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/return_order/ReturnPalletItemAddOrUpdateAction.action',
		data:$("#subForm").serialize(),
		dataType:'json',
		type:'post',
		success:function(data){
			if(data && data.flag)
			{
				showMessageStatus("操作成功", 1000);
				if(0 == "<%=item_id%>"*1)
				{
					addOneRowAndRef();
					clearAllField();
					$.artDialog.opener.updatePalletItemCountSpan  && $.artDialog.opener.updatePalletItemCountSpan();
				}
				else
				{
					windowClose();
				}
			}
		},
		error:function(){
			showMessageStatus("系统错误", 1000);
		}
	});
}
function showMessageStatus(content, secs)
{
	var d = $.artDialog({
	    content: content
	});
	d.show();
	setTimeout(function () {
	    d.close();
	}, secs);	
}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function addOneRowAndRef(index)
{
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_order/return_pallet_item_list.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:{pallet_view_id:'<%=pallet_view_id%>', p:index},
		
		beforeSend:function(request){
		},
		
		error: function(){
		},
		
		success: function(html)
		{
			$("#items").html(html);
		}
	});
}
function go(number)
{
	addOneRowAndRef(number);
}
function clearAllField()
{
	$("input[type='checkbox'][name='packaging']").attr("checked",false);
	$("input[type='checkbox'][name='accessories']").attr("checked",false);
	$("input[name='serial_number']").val("");
	$("input[name='box_serial_number']").val("");
	$("input[name='model']").val("");
	$("input[name='sku']").val("");
	$("select[name='status']").val("");
}
</script>

<title>return item view</title>
</head>
<body>
<%
String divHeight = "465px";
if(0 == item_id)
{
	divHeight = "100%";
}%>
    <div style="height:<%=divHeight %>; border:0px solid red;overflow:auto">
	    <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:935px; height:20px;">
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td width="15%" align="left">
			    	PalletId:<span style="color: blue;"><%=pallet_view_id%></span>
			    </td>
			    <td width="30%" align="left">
			   		Pallet Process Date:<span style="color: blue;"><%=tDate.getFormateTime(palletViewRow.getString("pallet_process_date"), "yyyy-MM-dd") %></span>
			    </td>
			    <td width="20%" align="left">
			   		Cust RefNo:<span style="color: blue;"><%=returnServiceCustRefNoKey.getReturnServiceCustRefNoKeyById(palletViewRow.getString("cust_ref_type"))%></span>
			   		<%=palletViewRow.getString("cust_ref_no") %>
			    </td>
			    <td width="35%" align="left">
			   		RA/RV:<span style="color: blue;"><%=palletViewRow.getString("ra_rv") %></span>
			    </td>
			  </tr>
			</table>	
	    </div>
	    <br/>
    <form id="subForm" method="post">
    	<input type="hidden" name="pallet_view_id" value="<%=pallet_view_id%>">
    	<input type="hidden" name="pallet_status" value="<%=palletViewRow.getString("status")%>">
	     <div id="return_item_view" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:935px;">
		  <div id="av">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" >
							<tr>
							  		<td width="15%" align="right">
							  			<span>Packaging:</span>
							  		</td>
							  		<td colspan="3">
							  			<%
							  				ArrayList returnServicePackagingKeys = returnServicePackagingKey.getReturnServicePackagingKeys();
							  				for(int i = 0; i < returnServicePackagingKeys.size(); i ++)
							  				{
							  			%>
									  <input type="checkbox" name="packaging" value="<%=String.valueOf(returnServicePackagingKeys.get(i))%>"
									  	<%=null!=returnRow&&returnRow.getString("packaging").contains(","+String.valueOf(returnServicePackagingKeys.get(i))+",")?"checked":"" %>
									  >
									  	<%=returnServicePackagingKey.getReturnServicePackagingKeyById(String.valueOf(returnServicePackagingKeys.get(i))) %>
									  <%
							  				}
									  %>
							  		</td>
							</tr>
						  	<tr>
						  		<td width="15%" align="right">
				  					<span name="title">Accessories:</span>
				  				</td>
						  		<td colspan="3">
						  			<%
						  				ArrayList returnServiceAccessoriesKeys = returnServiceAccessoriesKey.getReturnServiceAccessoriesKeys();
						  				for(int i = 0; i < returnServiceAccessoriesKeys.size(); i ++)
						  				{
						  			%>
						  			<input type="checkbox" name="accessories" value="<%=String.valueOf(returnServiceAccessoriesKeys.get(i)) %>"
						  				<%=null!=returnRow&&returnRow.getString("accessories").contains(","+String.valueOf(returnServiceAccessoriesKeys.get(i))+",")?"checked":"" %>
						  			>
										<%=returnServiceAccessoriesKey.getReturnServiceAccessoriesKeyById(String.valueOf(returnServiceAccessoriesKeys.get(i))) %>						  			
						  			<%		
						  				}
						  			%>
						  		</td>
						  	</tr>
							<tr height="30px;">		
							 	<td width="15%" align="right">
									<span name="title">Serial Number:</span>
								</td>
								<td width="20%" align="left">
									<input type="text" id="serial_number" name="serial_number" size="22" value="<%=null==returnRow?"":returnRow.getString("serial_number") %>" />
								</td>
								<td width="15%" align="right">
						  		 	<span name="title">Box Serial Number:</span>
						  		</td>	
							 	<td width="20%" align="left">
							   		<input type="text" id="box_serial_number" name="box_serial_number" size="22" value="<%=null==returnRow?"":returnRow.getString("box_serial_number") %>" />
								</td>
							 	<td rowspan="2" align="center" style="vertical-align: bottom;">
							 		
							 	</td>		
							</tr>
							<tr height="30px;">					
								<td width="15%" align="right">
									<span name="title">Model:</span>
								</td>	
							   <td width="20%" align="left">
							   		<input type="text" name="model" id="model"  size="22" value="<%=null==returnRow?"":returnRow.getString("model") %>" />
						  	   </td>	
								<td width="15%" align="right">
									<span name="title">SKU:</span>
								</td>
								<td width="20%" colspan="2" align="left">
							   		<input type="text" id="sku" name="sku" size="22" value="<%=null==returnRow?"":returnRow.getString("sku") %>" />
							   </td>										
							</tr>
							
							<tr height="30px;">					
								<td width="15%" align="right">
									<span name="title">Status:</span>
								</td>
								<td width="20%" align="left">
									<select name="status" id="status">
										<%
											ArrayList returnServicePalletItemStatusKeys = returnServicePalletItemStatusKey.getReturnServicePalletItemStatusKeys();
											for(int i = 0; i < returnServicePalletItemStatusKeys.size(); i ++)
											{
										%>
										<option value="<%=String.valueOf(returnServicePalletItemStatusKeys.get(i))%>"
											<%=null!=returnRow&&String.valueOf(returnServicePalletItemStatusKeys.get(i)).equals(returnRow.getString("status"))?"selected=selected":"" %>
										>
											<%=returnServicePalletItemStatusKey.getReturnServicePalletItemStatusKeyById(String.valueOf(returnServicePalletItemStatusKeys.get(i))) %>
										</option>
										<%		
											}
										%>
									</select>
								</td>	
								<td width="15%" align="right">
						  		 	<span name="title">RA/RV:</span>
						  		</td>	
							 	<td width="20%" align="left">
							   		<input type="text" id="ra_rv" name="ra_rv" size="22" value="<%=null==returnRow?palletViewRow.getString("ra_rv"):returnRow.getString("ra_rv") %>" />
								</td>
								<td width="15%" align="left">
									<%if(0 == item_id)
								 	{%>
							 		<input type="button" value="Add" class="short-short-button" name="Add" onclick="addRow()"/><br>
							 		<%} %>
								</td>
							</tr>
						 </table>
						 <hr size="1"/>
					<input type="hidden" name="item_id" id="item_id" value="<%=item_id%>">
					<input type="hidden" name="creator" id="creator" value="<%=null==returnRow?"":returnRow.get("creator", 0L) %>">
					<input type="hidden" name="create_date" id="create_date" value="<%=null==returnRow?"":returnRow.getString("create_date") %>">
				 </div>
	    </div>
	 </form>
	 <div id="items" style="width:98%;">
	    
	 </div>
    </div>
     <%if(0 != item_id)
	{%>
	 <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;width:935px;margin-top:2px;vertical-align: bottom;">
    	<input class="normal-green" type="button" onclick="addRow()" value="SUBMIT" >
    </div>
    <%} %>
    
    <div id="avg" style="width:368px; display:block; border:1px solid red;visibility: hidden;">
		<table  width="368px" border="0px" cellspacing="0" cellpadding="0" align="center" id="printTb">
			<tr>
				<td  colspan="3" width="100%" height="60" style="border-bottom:2px solid black;">
					<div align="left" style="padding-left:8px;padding-left:8px;padding-top:8px;">Model#:<span style="font-weight:bold;" id="model_item"></span></div><br/>
					<div align="center" style="padding-bottom:4px;"><img id="model_item_barcode"/></div>
				</td>
			</tr>
			<tr>
				<td  colspan="3" width="100%" height="60" style="border-bottom:2px solid black;">
					<div align="left" style="padding-left:5px;padding-left:8px;padding-top:6px;">SKU/Past#:<span style="font-weight:bold;" id="sku_item"></span></div><br/>
					<div align="center" style="padding-bottom:4px;"><img src="" id="sku_item_barcode"/></div>
				</td>
			</tr>
			<tr>
				<td  colspan="3" width="100%" height="60" style="border-bottom:2px solid black;">
					<div align="left" style="padding-left:8px;padding-top:6px;">S/N#:<span style="font-weight:bold;" id="sn_item"></span></div><br/>
					<div align="center" style="padding-bottom:4px;"><img src="" id="sn_item_barcode"/></div>
				</td>
			</tr>
			<tr>
				<td  colspan="3" width="100%" height="60" style="border-bottom:2px solid black;">
					<div align="left" style="padding-left:5px;padding-left:8px;padding-top:6px;">RA/RV#:<span style="font-weight:bold;" id="ra_rv_item"></span></div><BR/>
					<div align="center" style="padding-bottom:4px;"><img src="" id="ra_rv_item_barcode"/></div>
				</td>
			</tr>
			<tr>
				<td width="50%" height="55" style="border-bottom:2px solid black; border-right:2px solid black;">
					<div align="left" style="padding-left:5px;padding-left:8px;">Pallet ID</div>
					<div align="center"><span style="font-weight:bold;" id="pallet_id_pallet"></span></div>
				</td>
				<td width="50%" style="border-bottom:2px solid black;">
					<div align="left" style="padding-left:5px;">Item ID</div>
					<div align="center"><span style="font-weight:bold;" id="item_id_item"></span></div>
				</td>
			</tr>
			<tr>
				<td  colspan="3" width="100%" height="50" style="border-bottom:2px solid black;">
					<div align="left" style="padding-left:5px;">Processed Date</div>
					<div align="center"><span style="font-weight:bold;" id="processd_date_pallet"></span></div>
				</td>
			</tr>
		</table>
	</div>
    
</body>
</html>
<script>
$.blockUI.defaults = {
		 css: { 
		  padding:        '8px',
		  margin:         0,
		  width:          '170px', 
		  top:            '45%', 
		  left:           '40%', 
		  textAlign:      'center', 
		  color:          '#000', 
		  border:         '3px solid #999999',
		  backgroundColor:'#ffffff',
		  '-webkit-border-radius': '10px',
		  '-moz-border-radius':    '10px',
		  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		 },
		 //设置遮罩层的样式
		 overlayCSS:  { 
		  backgroundColor:'#000', 
		  opacity:        '0.6' 
		 },
		 
		 baseZ: 99999, 
		 centerX: true,
		 centerY: true, 
		 fadeOut:  1000,
		 showOverlay: true
		};
	
	var p = 0;
	function add(packaging, accessories, serial_number,box_serial_number,model,sku,receive_tracking,item_id, status, ra_rv){
		p++;
		var con=$('#return_item_view');
		var html='';
		html='<div id="av">'+
					'<table width="100%" border="0" cellspacing="0" cellpadding="0" >'+
							'<tr>'+
							  		'<td width="20%" align="right">'+
							  			'<span>Packaging:</span>'+
							  		'</td>'+
							  		'<td colspan="3">'+
									  '<input type="checkbox" name="packaging">Original Box'+ 
									  '<input type="checkbox" name="packaging">Original Cushion'+
							  		'</td>'+
							'</tr>'+
						  	'<tr>'+
						  	'	<td width="20%" align="right">'+
				  					'<span name="title">Accessories:</span>'+
				  				'</td>'+
						  		'<td colspan="3">'+
						  		  '<input type="checkbox" name="accessories">Stand/Base'+
								  '<input type="checkbox" name="accessories">AC Adapter'+
								  '<input type="checkbox" name="accessories">Remote'+
								  '<input type="checkbox" name="accessories">HDM/Cable'+
								  '<input type="checkbox" name="accessories">3D Glass'+
								  '<input type="checkbox" name="accessories">RCA Cable'+
						  		'</td>'+
						  	'</tr>'+
							'<tr height="30px;">'+		
							 	'<td width="20%" align="right">'+
									'<span name="title">Serial Number:</span>'+
								'</td>'+
								'<td width="30%" align="left">'+
									'<input type="text" id="serial_number_'+p+'" name="serial_number" size="22" value="'+serial_number+'" />'+
								'</td>'+
								'<td width="15%" align="right">'+
						  		 	'<span name="title">Box Serial Number:</span>'+
						  		'</td>'+	
							 	'<td width="15%" align="left">'+
							   		'<input type="text" id="box_serial_number_'+p+'" name="box_serial_number" size="22" value="'+box_serial_number+'" />'+
								'</td>'+
							 	'<td rowspan="2" align="center" style="vertical-align: bottom;">'+
							 		'<input type="button" value="Del" zhi="'+p+'" class="short-short-button-del" name="detButton" /><br>'+
							 		'<input type="button" value="Print" zhi="'+p+'" class="short-short-button" name="detButton" /><br>'+
							 		'<input type="button" value="Add" zhi="'+p+'" class="short-short-button" name="Add" onclick="add(\'\',\'\',\'\',\'\',\'\')"/><br>'+
							 	'</td>'+		
							'</tr>'+
							'<tr height="30px;">'+					
								'<td width="15%" align="right">'+
									'<span name="title">Model:</span>'+
								'</td>'+	
							   '<td width="15%" align="left">'+
							   		'<input type="text" name="model" id="model_'+p+'"  size="22" value="'+model+'" />'+
						  	   '</td>'+	
								'<td width="20%" align="right">'+
									'<span name="title">SKU:</span>'+
								'</td>'+
								'<td width="30%" colspan="2" align="left">'+
							   		'<input type="text" id="sku_'+p+'" name="sku" size="22" value="'+sku+'" />'+
							   '</td>'+										
							'</tr>'+
							
							'<tr height="30px;">'+					
								'<td width="20%" align="right">'+
									'<span name="title">Status:</span>'+
								'</td>'+
								'<td width="30%" align="left">'+
									'<select name="Status" id="Status_'+p+'">'+
										'<option value="1">NA</option>'+
										'<option value="2">BER/Damaged</option>'+
									'</select>'+
								'</td>'+	
								'<td width="10%" align="right">'+
						  		 	'<span name="title">RA/RV:</span>'+
						  		'</td>'+	
							 	'<td width="15%" align="left">'+
							   		'<input type="text" id="ra_rv_'+p+'" name="ra_rv" size="22" value="'+ra_rv+'" />'+
								'</td>'+
							'</tr>'+
						 '</table>'+
						 '<hr size="1"/>'+
					'<input type="hidden" name="item_id" id="item_id_"'+p+' value="'+item_id+'">'+
					
				 '</div>';
	    
		var $container=$(html).appendTo(con);                    //获得容器

	    var $det_button=$("input[name='detButton']",$container); //获得删除按钮
	    //绑定删除事件
	    $det_button.click(function(){
		    var zhi=$(this).attr("zhi");
		    var detail_id=$('#detailid'+zhi).val();
		    
	    	//删除操作 弹出窗口 提示 是否删除
	    	$.artDialog({
			    content: 'do you want to delete?',
			    icon: 'question',
			    width: 200,
			    height: 70,
			    title:'',
			    okVal: 'Confirm',
			    ok: function () {			
			    	$container.remove('#av');	
			    },
			    cancelVal: 'Cancel',
			    cancel: function(){
				}
			});	    	   		 	    	    	
		});
	}
	function printItem(model, sku, sn, ra_rv, item_id)
    {
	   	// var imgSrc = '/barbecue/barcode?data='+data[i].container+'&width=1&height=35&type=code39';
	   	 $("#model_item").text(model);
	   	 $("#model_item_barcode").attr("src",imgBarcodeSrc(model));
	   	 $("#sku_item").text(sku);
	   	 $("#sku_item_barcode").attr("src",imgBarcodeSrc(sku));
	   	 $("#sn_item").text(sn);
	   	 $("#sn_item_barcode").attr("src",imgBarcodeSrc(sn));
	   	 $("#ra_rv_item").text(ra_rv);
	   	 $("#ra_rv_item_barcode").attr("src",imgBarcodeSrc(ra_rv));
	   	 $("#model_item").text(model);
	   	 $("#item_id_item").text(item_id);
	   	 $("#pallet_id_pallet").text('<%=pallet_view_id%>');
	   	 $("#processd_date_pallet").text('<%=tDate.getFormateTime(palletViewRow.getString("pallet_process_date"), "yyyy-MM-dd") %>');
	   	 // src="" 
	   	printInstantly();
    }
    
    function imgBarcodeSrc(code)
    {
   		 return '/barbecue/barcode?data='+code+'&width=1&height=35&type=code39';
    }
    
    function printInstantly(){
		var dis=$("div[id='avg']");
		for(var i=0;i<dis.length;i++){
			 visionariPrinter.PRINT_INITA(0,0,"102mm","152mm","Carton Label");
	         visionariPrinter.SET_PRINT_PAGESIZE(1,0,0,"102X152");            
	    	 visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%",$(dis[i]).html());
			 visionariPrinter.SET_PRINT_COPIES(1);
			 //visionariPrinter.PREVIEW();
			 visionariPrinter.PRINT();
		}
	}
</script>
