<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="returnServiceCustRefNoKey" class="com.cwc.app.key.ReturnServiceCustRefNoKey"></jsp:useBean>
<jsp:useBean id="returnServicePalletHandleStatusKey" class="com.cwc.app.key.ReturnServicePalletHandleStatusKey"></jsp:useBean>
<jsp:useBean id="returnReceiveFromTypeKey" class="com.cwc.app.key.ReturnReceiveFromTypeKey"/>
<jsp:useBean id="returnReceiveCategoryKey" class="com.cwc.app.key.ReturnReceiveCategoryKey"/>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!--时间控件-->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<style>
.set{padding:0px;width:95%;word-break:break-all;margin-top:0px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 0px;border: 2px solid blue;} 
p{text-align:left;}
span.stateName{width:32%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:68%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
.zebraTable td { border-bottom: 0px solid #DDDDDD;padding-left: 10px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
	
}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:500px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>

<style type="text/css">
	body {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
	td {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
</style>
<%
	int p = StringUtil.getInt(request,"p");
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(p);
	pc.setPageSize(10);
	String cmd = StringUtil.getString(request,"cmd");
	long return_receive_id = StringUtil.getLong(request, "return_receive_id");
	String receive_from = returnReceiveFromTypeKey.getReturnReceiveFromTypeKeyValue(StringUtil.getString(request,"receive_from"));
	String receive_category = returnReceiveCategoryKey.getReturnReceiveCategoryKeyValue(StringUtil.getString(request,"receive_category"));
	
	String ra_rv = StringUtil.getString(request, "ra_rv");
	String palletNo = StringUtil.getString(request, "palletNo");
	String cust_ref_type = StringUtil.getString(request, "cust_ref_type");
	String cust_ref_no = StringUtil.getString(request, "cust_ref_no");
	
	DBRow[] returnRows = returnMgrZyj.findReturnPalletRows(return_receive_id, pc);
	if("search".equals(cmd)){
		returnRows = returnMgrZyj.getReturnPalletsByPara(return_receive_id,palletNo,ra_rv,cust_ref_type,cust_ref_no,pc);
	}

%>
<title>return</title>
<script type="text/javascript">
jQuery(function($){
	 
});
function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
		
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
		   searchRightButton();
		}  
	}  
}
function search()
{
	var val = $("#search_key").val();
			
	if(val.trim()=="")
	{
		alert("Please enter key word for searching");
	}
	else
	{
		var val_search = "\'"+val.toUpperCase()+"\'";
		$("#search_key").val(val_search);
		document.search_form.key.value = val_search;
		document.search_form.search_mode.value = 1;
		document.search_form.submit();
	}
}

function searchRightButton()
{
	var val = $("#search_key").val();
			
	if (val=="")
	{
		alert("Please enter key word for searching");
	}
	else
	{
		val = val.replace(/\'/g,'');
		$("#search_key").val(val);
		document.search_form.key.value = val;
		document.search_form.search_mode.value = 2;
		document.search_form.submit();
	}
}

function filter()
{
	$("#search_form").submit();
}
function go(index){
	var form = $("#pageForm");
	$("input[name='p']",form).val(index);
	form.submit();
}
</script>
</head>
<body onLoad="onLoadInitZebraTable()">
	<div class="demo" style="width:98%">
		<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:850px; height:20px;">
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td width="20%" align="right">
					<div style="min-width;float:left"> ReceiveNo:<span style="color: blue;"><%=return_receive_id %></span> </div>
			    </td>
			    <td width="30%">
			    	<div style="min-width;float:left"> Receive From:<span style="color: blue;"><%=receive_from %></span> </div>
			    </td>
			    <td width="50%">
			    	<div style="float:left" align="center">
			    		Receive Category:<span style="color: blue;"><%=receive_category %></span>
			    	</div>
			    </td>
			  </tr>
			</table>	
	    </div>
    </div>
    <form id="search_form" name="search_form" method="post" action="pallet_view_list.html">
		<input type="hidden" name="cmd" value="search"/>
		<input type="hidden" name="return_receive_id" value="<%=return_receive_id%>"/>
   		<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left"> 
					PalletNo:
					<input type="text" id="palletNo" name="palletNo" value="<%=palletNo%>">
					&nbsp; &nbsp;
					RA/RV:
					<input type="text" id="ra_rv" name="ra_rv" value="<%=ra_rv%>">
					&nbsp; &nbsp;
					<select name="cust_ref_type">
						<%
							ArrayList returnServiceCustRefNoKeys = returnServiceCustRefNoKey.getReturnServiceCustRefNoKeys();
							for(int i = 0; i < returnServiceCustRefNoKeys.size(); i ++)
							{
								
						%>
							<option value="<%=String.valueOf(returnServiceCustRefNoKeys.get(i))%>"
								<%=String.valueOf(returnServiceCustRefNoKeys.get(i)).equals(cust_ref_type)?"selected=selected":"" %>
							>
								<%=returnServiceCustRefNoKey.getReturnServiceCustRefNoKeyById(String.valueOf(returnServiceCustRefNoKeys.get(i)))%>
							</option>
						<%
							}
						%>
					</select>
					&nbsp;<input type="text" id="cust_ref_no" name="cust_ref_no" size="22" value="<%=cust_ref_no %>" />					
					&nbsp;&nbsp;&nbsp;&nbsp;					
        			<input name="Submit" type="button" class="button_long_refresh" onClick="filter()" value=" 过 滤 ">
				</td>
			</tr>
		</table>
	</form>
<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0"  class="zebraTable" style="margin-left:3px;margin-top:5px;">
		  <tr> 
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">PalletNo</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Status</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Total QTY</th>
		  	<th width="15%" style="vertical-align: center;text-align: center;" class="right-title">Pallet Process Date</th>
	        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Cust RefNo</th>
	        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">RA/RV</th>
	        <th width="15%" style="vertical-align: center;text-align: center;" class="right-title">Operate</th>
	      </tr>
	      <tbody class="mytbody">
	      	<%
	      		if(returnRows.length > 0)
	      		{
	      			for(int i = 0; i < returnRows.length; i ++)
	      			{
	      				DBRow returnRow = returnRows[i];
	      	%>
	      		<tr>
	      			<td><%=returnRow.getString("pallet_no") %></td>
	      			<td><%=returnServicePalletHandleStatusKey.getReturnServicePalletHandleStatusById(returnRow.get("status", 0)) %></td>
	      			<td>
	      				<%=returnMgrZyj.countPalletItemCountByPalletId(returnRow.get("pallet_view_id", 0L)) %>
	      			</td>
	      			<td><%=tDate.getFormateTime(returnRow.getString("pallet_process_date"), "yyyy-MM-dd") %></td>
	      			<td>
	      				<%=0!=returnRow.get("cust_ref_type", 0)?returnServiceCustRefNoKey.getReturnServiceCustRefNoKeyById(returnRow.get("cust_ref_type", 0))+"#:"+returnRow.getString("cust_ref_no"):"" %>
	      			</td>
	      			<td><%=returnRow.getString("ra_rv") %></td>
	      			<td>
		      			<br><input class="short-short-button" type="button" onclick="process(<%=returnRow.get("pallet_view_id", 0L) %>)" value="Process" >
		      			<input class="short-button" type="button" onclick="printLabel()" value="PrintLabel" ><br><br>
	      			</td>
	      		</tr>	
	      	<%
	      			}
	      		}
	      		else
	      		{	
	      	%>
	      		<td colspan="8" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	      	<%
	      		}
	      	%>
		  </tbody>
	</table>
<form action="" id="pageForm" method="post">
		<input type="hidden" name="p" id="pageCount" value="<%=p %>"/>
		<input type="hidden" name="cmd" value=""/>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle">
	<%
		int pre = pc.getPageNo() - 1;
		int next = pc.getPageNo() + 1;
		out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
		out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
		out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
		out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
		out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
      Goto
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
</body>
 <script>
     $("#tabs").tabs({
  	    cache: true,
  		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
  		cookie: { expires: 30000 } ,
  		show:function(event,ui)
  			 {
  			 	
  			 }
  	});
   //添加时间控件
	 $('.dateClass').datepicker({
		dateFormat:"yy-mm-dd",
		changeMonth: true,
		changeYear: true,
	});
	$("#ui-datepicker-div").css("display","none");
	
     function addPalletView()
     {
    	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/pallet_view.html"; 
     	$.artDialog.open(uri , {title: "palletView",width:'900px',height:'530px', lock: true,opacity: 0.3,fixed: true});
     }
     
     function process(pallet_view_id)
     {
    	 var return_receive_id = '<%=return_receive_id %>';
    	 var receive_from = '<%=receive_from %>';
    	 var receive_category = '<%=receive_category %>';
    	 
    	 
    	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/pallet_view.html"; 
    	 uri += '?pallet_view_id='+pallet_view_id+'&return_receive_id='+return_receive_id+'&receive_from='+receive_from+'&receive_category='+receive_category;
    	$.artDialog.open(
    			uri ,
    			{
    				title: "palletView",
    				width:'900px',
    				height:'530px',
    				lock: true,
    				opacity: 0.3,
    				fixed: true,
    				close: function() {  
    					window.location.reload();
    	            }  
    			});
    
     }
     //打印标签，未完成
     function printLabel()
     {
    	 
     }
     
	 function refreshWindow(){
   		window.location.reload();
   	};
 </script>
</html>

