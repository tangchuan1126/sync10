<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.ReturnReceiveFromTypeKey" %>
<jsp:useBean id="returnReceiveFromTypeKey" class="com.cwc.app.key.ReturnReceiveFromTypeKey"/>
<%@page import="com.cwc.app.key.ReturnReceiveCategoryKey" %>
<jsp:useBean id="returnReceiveCategoryKey" class="com.cwc.app.key.ReturnReceiveCategoryKey"/>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<jsp:useBean id="returnOrderStatusKey" class="com.cwc.app.key.ReturnOrderStatusKey"/>



<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!--时间控件-->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<style>
.set{padding:2px;width:95%;word-break:break-all;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 0px;border: 2px solid blue;} 
p{text-align:left;}
span.stateName{width:32%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:68%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
.zebraTable td { border-bottom: 0px solid #DDDDDD;padding-left: 10px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
	
}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:500px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>

<style type="text/css">
	body {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
	td {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
</style>
<%
	String cmd = StringUtil.getString(request,"cmd");
	long receive_id = StringUtil.getLong(request,"receive_id");
	String receive_from = StringUtil.getString(request, "receive_from");
	String receive_category = StringUtil.getString(request, "receive_category");
	String input_st_date = StringUtil.getString(request,"input_st_date");
	String input_en_date = StringUtil.getString(request,"input_en_date");
	if ("".equals(input_st_date)&&"".equals(input_en_date))
	{
		input_st_date = DateUtil.LastMonthDatetime("yyyy-MM-dd");//获取上个月时间
		input_en_date = DateUtil.FormatDatetime("yyyy-MM-dd");//获取当前时间
		
	}
	
	PageCtrl pc = new PageCtrl();//定义页数对象
	pc.setPageNo(StringUtil.getInt(request,"p"));//获取request中是第几页
	pc.setPageSize(20);//10表示每页显示的条数
	DBRow[] rows = returnOrderMgrGql.getReturnOrdet(pc);
	if("search".equals(cmd)){
		rows = returnOrderMgrGql.getReturnOrdersById(receive_id,pc);
	}else if("filter".equals(cmd)){
		rows = returnOrderMgrGql.getReturnOrdetsByPara(receive_from,receive_category,input_st_date,input_en_date,pc);
	}
	
%>

<title>return</title>
<script type="text/javascript">
//查询
function checkForm(thrForm)
{
	if (thrForm.receive_id.value=="")
	{
		alert("Please enter ReceiveNo for searching");
		return(false);
	}
	else
	{
		return(true);
	}
}
function search()
{
	var val = $("#receive_id").val();
			
	if(val.trim()=="")
	{
		alert("Please enter ReceiveNo for searching");
	}
	else
	{

	}
}

function searchRightButton()
{
	var val = $("#receive_id").val();
			
	if (val=="")
	{
		alert("Please enter ReceiveNo for searching");
	}
	else
	{
		val = val.replace(/\'/g,'');
		$("#receive_id").val(val);

	}
}

function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
		
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
		   searchRightButton();
		}  
	}  
}

	function filter()
	{
		$("#form1").submit();
	}

</script>
</head>
<body onLoad="onLoadInitZebraTable()">
<div class="demo" style="width:98%">
	  <div id="tabs">
		<ul>
			<li><a href="#return_search">Common Tools</a></li>
			<li><a href="#return_filter">Advanced Search</a></li>
		</ul>
		<div id="return_search">
			<form name="search_form" method="post" action="return_list_index.html" onSubmit="return checkForm(this)">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td >
									<div  class="search_shadow_bg">
									 <input name="receive_id" type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="receive_id" onkeydown="if(event.keyCode==13)search()" value="<%=receive_id==0?"":receive_id%>"/>
<%--									 <input type="button" class="button_long_refresh" value="Refresh" onclick="filter('refresh');"/>--%>
									</div>
								</td>
								<td width="3px"></td>
								<td width="167" align="center">
								</td>
							</tr>
						</table>
						<script>eso_button_even();</script>
				   </td>
				   <td width="70%" align="right">
				  		 <input class="long-long-button-add" type="button" onclick="receivingView()" value="Create Retail Receiving">
 						 <input class="long-long-button-add" type="button" onclick="endUser()" value="Create End User Receiving">
				   </td>
	            </tr>
	          </table>
	          <input type="hidden" name="cmd" value="search">
	         </form>
	      </div>
	      <div id="return_filter">
	      	<form id="form1" name="form1" method="post" action="return_list_index.html">
				<input name="cmd" value="filter" type="hidden"/>
	      		<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left">
						开始 :
              			<input name="input_st_date" type="text" id="input_st_date" class="dateClass" size="9" value="<%=input_st_date%>" readonly> 
              			 &nbsp;结束 :
              			<input name="input_en_date" type="text" id="input_en_date" class="dateClass" size="9" value="<%=input_en_date%>"  readonly>
						&nbsp; &nbsp; 
						<select name="receive_from" id="receive_from">
							<option value=""> Receive From</option>
							<%
								ArrayList list = returnReceiveFromTypeKey.getReturnReceiveFromTypeKeys();
								for(int i=0;i<list.size();i++){ 
							%>
								
								<option value="<%=list.get(i)%>" <%=list.get(i).equals(receive_from)?"selected":"" %> >
									<%=returnReceiveFromTypeKey.getReturnReceiveFromTypeKeyValue(Integer.parseInt(list.get(i)+"")) %>
								</option>
							
							<%} %>
						</select>
						&nbsp; &nbsp;
						<select name="receive_category" id="receive_category">
							<option value=""> Receive Category</option>
							<%
								ArrayList list1 = returnReceiveCategoryKey.getReturnReceiveCategoryKeys();
								for(int i=0;i<list1.size();i++){ 
							%>
								<option value="<%=list1.get(i)%>" <%=list1.get(i).equals(receive_category)?"selected":"" %> >
									<%=returnReceiveCategoryKey.getReturnReceiveCategoryKeyValue(Integer.parseInt(list1.get(i)+"")) %>
								</option>
							
							<%} %>
						</select>
						&nbsp; &nbsp;
	          			<input name="Submit" type="button" class="button_long_refresh" onClick="filter()" value=" 过   滤 ">
					</td>
				</tr>
			</table>
			</form>
	      </div>
       </div>
      
    </div>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="true" class="zebraTable" isBottom="true" style="margin-left:3px;margin-top:5px;">
		  <tr class="split"> 
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">ReceiveNo</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Status</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Receive From</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Store</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Receive Date</th>
	        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Pallet Count</th>
	        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">BOL / Tracking</th>
	        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Receive Carrier</th>
	        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Receive Category</th>
	        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;Operate</th>
	      </tr>
	      <tbody class="mytbody">
	      	
	      		<%if(rows != null && rows.length > 0){
		      		for(DBRow row : rows){
		      	%>
	      			<tr>
		      			<td align="center" valign="middle" style='word-break:break-all'><%=row.getString("receive_id") %></td>
		      			<td align="center" valign="middle" style='word-break:break-all'><%=returnOrderStatusKey.getReturnOrderStatusValue(row.getString("status")) %></td>
		      			<td align="center" valign="middle" style='word-break:break-all'><%=returnReceiveFromTypeKey.getReturnReceiveFromTypeKeyValue(row.getString("receive_from")) %></td>
		      			<td align="center" valign="middle" style='word-break:break-all'><%=row.getString("receive_store") %></td>
		      			<td align="center" valign="middle" style='word-break:break-all'><%=tDate.getFormateTime(row.getString("dock_receive_date"), "yyyy-MM-dd")%></td>
		      			<td align="center" valign="middle" style='word-break:break-all'><%=row.getString("update_pallet_count") %></td>
		      			<td align="center" valign="middle" style='word-break:break-all'><%=row.getString("bol_tracking") %></td>
		      			<td align="center" valign="middle" style='word-break:break-all'><%=row.getString("receive_carrier") %></td>
		      			<td align="center" valign="middle" style='word-break:break-all'><%=returnReceiveCategoryKey.getReturnReceiveCategoryKeyValue(row.getString("receive_category")) %></td>
		      			<td>
		      				<input class="long-button" type="button" onclick="Edit(<%=row.get("receive_id",01)%>,<%=row.get("receive_category",01) %>)" value="Edit" >
		      				<input class="long-button" type="button" onclick="process(<%=row.get("receive_id",01)%>,<%=row.getString("receive_from") %>,<%=row.getString("receive_category") %>)" value="Process" ><br>
		      			</td>
	      		</tr>	
<%--	      		<tr class="split">--%>
<%--  					<td colspan="10" style="height:24px;line-height:24px;background-image:url('../imgs/linebg_24.jpg');text-align:right;padding-right:20px;">--%>
<%--  				<input type="button" value="WindowCheckIn" class="long-button" onclick="checkInWindow('<%=row.getString("dlo_id") %>','<%=row.getString("zone_id") %>')" />&nbsp;&nbsp; --%>
<%--	  			<input type="button" value="WarehouseCheckIn" class="long-long-button" onclick="warehouseCheckIn(<%=row.getString("dlo_id") %>)" />&nbsp;&nbsp;--%>
<%--	  			<input type="button" value="GateCheckOut" class="long-button" onclick="outfangfa(<%=row.getString("dlo_id") %>)" id="checkOut" />&nbsp;&nbsp;   	       	--%>
<%--		  			</td>--%>
<%--		  	    </tr>--%>
	      	<%	}
	      	}else
	      		{	
	      	%>
	      		<td colspan="10" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	      	<%
	      		}
	      	%>
		  </tbody>
	</table>
	<br>
	<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" action="return_list_index.html" method="post">
	    <input type="hidden" name="p"/>
	  </form>
	  <tr>
	    <td height="28" align="right" valign="middle" class="turn-page-table">
	        <%
				int pre = pc.getPageNo() - 1;
				int next = pc.getPageNo() + 1;
				out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
				out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
				out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
				out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
				out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
			%>
	      Goto 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
	    </td>
	  </tr>
   </table>
	    

</body>

 <script>
     $("#tabs").tabs({
  	    cache: true,
  		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
  		cookie: { expires: 30000 } ,
  		show:function(event,ui)
  			 {
  			 	
  			 }
  	});
     
   //添加时间控件
	 $('.dateClass').datepicker({
		dateFormat:"yy-mm-dd",
		changeMonth: true,
		changeYear: true,
	});
	$("#ui-datepicker-div").css("display","none");
     
     //刷新页面
     function refreshWindow(){
    	window.location.reload();
    };
    
    //创建Create End User
     function endUser()
     {
    	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/return_end_user_receiving_view.html?editType=add"; 
    	 $.artDialog.open(uri , {title: "Create End User Receiving",width:'900px',height:'530px', lock: true,opacity: 0.3,fixed: true});		
     }

     //创建Create Retail Receiving
     function receivingView()
     {
     	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/receiving_view.html?editType=add";
     	$.artDialog.open(uri , {title: "Create Retail Receiving",width:'900px',height:'530px', lock: true,opacity: 0.3,fixed: true});
     }
     
     //跳转到pallet view 页面
    function process(id,receiveFrom,category)
     {
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/pallet_view_list.html";
		uri += "?return_receive_id="+id+"&receive_from="+receiveFrom+"&receive_category="+category;
		$.artDialog.open(uri , {title: "palletView",width:'900px',height:'530px', lock: true,opacity: 0.3,fixed: true});		
     }
     
     //修改
     function Edit(id,category)
     {
    	
    	if(category==1)//修改Retail Receiving
    	{
    		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/receiving_view.html?receive_id="+id+"&editType=edit"; 
       		$.artDialog.open(uri , {title: "Edit &nbsp;  ReceiveNo:"+id,width:'900px',height:'530px', lock: true,opacity: 0.3,fixed: true});
    	
    	}else if(category==2){//修改End User
    		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/return_end_user_receiving_view.html?receive_id="+id+"&editType=edit"; 
          	$.artDialog.open(uri , {title: "Edit &nbsp;  ReceiveNo:"+id,width:'900px',height:'530px', lock: true,opacity: 0.3,fixed: true});
    	}
    	 	
     }
 </script>
</html>
