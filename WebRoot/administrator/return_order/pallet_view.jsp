<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<jsp:useBean id="returnServiceCustRefNoKey" class="com.cwc.app.key.ReturnServiceCustRefNoKey"></jsp:useBean>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script> 
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<%
long pallet_view_id = StringUtil.getLong(request, "pallet_view_id");
DBRow palletViewRow = returnMgrZyj.findReturnPalletRowById(pallet_view_id);
String receive_from_text = StringUtil.getString(request, "receive_from_text");
String store_text = StringUtil.getString(request, "store_text");;

long return_receive_id = StringUtil.getLong(request, "return_receive_id");
String receive_from = StringUtil.getString(request, "receive_from");
String receive_category = StringUtil.getString(request, "receive_category");


String pallet_process_date =  DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
%>

<title>pallet_view</title>
</head>
<body>
    <div style="height:465px; border:0px solid red;overflow:auto">
	    <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:850px; height:20px;">
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			  	<td width="20%" align="right">
					<div style="min-width;float:left"> ReceiveNo: <span style="color: blue;"><%=return_receive_id %></span> </div>
			    </td>
			    <td width="30%">
			    	<div style="min-width;float:left"> Receive From:<span style="color: blue;"><%=receive_from %></span> </div>
			    </td>
			    <td width="50%">
			    	<div style="float:left" align="center">
			    		Receive Category:<span style="color: blue;"><%=receive_category %></span>
			    	</div>
			    </td>
			  </tr>
			</table>		
	    </div>
	    <br/>
	     <div id="pallet_view" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:850px;">
		  <div id="av">
		  <form id="subForm" method="post">
		  	<input type="hidden" name="pallet_view_id" value="<%=pallet_view_id%>">
		  	<input type="hidden" name="pallet_no" value="<%=null==palletViewRow?"":palletViewRow.getString("pallet_no") %>">
		  	<input type="hidden" name="return_receive_id" value="<%=null==palletViewRow?0L:palletViewRow.get("return_receive_id", 0L) %>">
		  	<input type="hidden" name="status" value="<%=null==palletViewRow?0:palletViewRow.get("status", 0) %>">
		  	<input type="hidden" name="creator" value="<%=null==palletViewRow?0L:palletViewRow.get("creator", 0L) %>">
		  	<input type="hidden" name="create_date" value="<%=null==palletViewRow?"":palletViewRow.getString("create_date") %>">
		  	
		  		<table width="100%" border="0" cellspacing="0" cellpadding="0" >
						<tr height="30px;">
							<td width="15%" align="right">
								<span name="title">Pallet Process Date:</span>
							</td>
							<td width="15%" align="left">
						   		<input type="text" id="pallet_process_date" name="pallet_process_date" class="pallet_process_date" size="22" 
						   		value="<%=(null==palletViewRow || ("".equals(palletViewRow.getString("pallet_process_date")))) ? pallet_process_date:tDate.getFormateTime(palletViewRow.getString("pallet_process_date"), "yyyy-MM-dd") %>" />
						   </td>
						   <td width="15%" align="right">
								<span name="title">Cust RefNo:</span>
							</td>
							<td width="55%" align="left">
								<select name="cust_ref_type">
									<%
										ArrayList returnServiceCustRefNoKeys = returnServiceCustRefNoKey.getReturnServiceCustRefNoKeys();
										for(int i = 0; i < returnServiceCustRefNoKeys.size(); i ++)
										{
											
									%>
											<option value="<%=String.valueOf(returnServiceCustRefNoKeys.get(i))%>"
												<%=null!=palletViewRow&&String.valueOf(returnServiceCustRefNoKeys.get(i)).equals(palletViewRow.getString("cust_ref_type"))?"selected=selected":"" %>
											>
												<%=returnServiceCustRefNoKey.getReturnServiceCustRefNoKeyById(String.valueOf(returnServiceCustRefNoKeys.get(i)))%>
											</option>
									<%
										}
									%>
								</select>
								&nbsp;<input type="text" id="cust_ref_no" name="cust_ref_no" size="22"
								 value="<%=null==palletViewRow?"":palletViewRow.getString("cust_ref_no") %>" />
							</td>
						</tr>
						<tr height="30px;">		
							<td width="15%" align="right">
					  		 	<span name="title">RA/RV:</span>
					  		</td>	
						 	<td width="15%" align="left">
						   		<input type="text" id="ra_rv" name="ra_rv" size="22" 
						   		value="<%=null==palletViewRow?"":palletViewRow.getString("ra_rv") %>" />
							</td>
						 	<td width="15%" align="right">
					  		 	<span name="title">Total QTY:</span>
					  		</td>	
						 	<td width="55%" align="left">
						   		<span id="palletItemCountSpan"><%=returnMgrZyj.countPalletItemCountByPalletId(pallet_view_id) %></span>
							</td>
						</tr>
						 </table>
					 </form>
					 <hr size="1"/>
				 </div>
	    </div>
    </div>
     <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;width:850px;margin-top:2px;vertical-align: bottom;">
    	<input class="normal-green" type="button" onclick="submitData(1)" value="ItemScan" >
    	<input class="normal-green" type="button" onclick="submitData()" value="SUBMIT" >
    </div>
</body>
</html>
<script>

function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};

function submitData(type){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/return_order/ReturnPalletAddOrUpdateAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag)
				{
					showMessageStatus("操作成功", 1500);
					if(1*type == 1)
					{
						 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/return_item_view.html"; 
						    uri += '?pallet_view_id=<%=pallet_view_id%>';
							$.artDialog.open(uri , {title: "itemView",width:'1000px',height:'530px', lock: true,opacity: 0.3,fixed: true}); 
					}
				}
			},
			error:function(){
				showMessageStatus("系统错误", 1500);
			}
		});
};

//同步pallet页面中的ItemCount
function updatePalletItemCountSpan(){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/return_order/ReturnPalletItemCountGetAction.action',
		data:{pallet_id:<%=pallet_view_id%>},
		dataType:'json',
		type:'post',
		success:function(data){
			if(data && data.flag)
			{
				$("#palletItemCountSpan").text(data.flag);
			}
		},
		error:function(){
			showMessageStatus("系统错误", 1500);
		}
	});
};

function showMessageStatus(content, secs)
{
	var d = $.artDialog({
	    content: content
	});
	d.show();
	setTimeout(function () {
	    d.close();
	}, secs);	
}
$.blockUI.defaults = {
		 css: { 
		  padding:        '8px',
		  margin:         0,
		  width:          '170px', 
		  top:            '45%', 
		  left:           '40%', 
		  textAlign:      'center', 
		  color:          '#000', 
		  border:         '3px solid #999999',
		  backgroundColor:'#ffffff',
		  '-webkit-border-radius': '10px',
		  '-moz-border-radius':    '10px',
		  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		 },
		 //设置遮罩层的样式
		 overlayCSS:  { 
		  backgroundColor:'#000', 
		  opacity:        '0.6' 
		 },
		 
		 baseZ: 99999, 
		 centerX: true,
		 centerY: true, 
		 fadeOut:  1000,
		 showOverlay: true
		};
	$(function(){
		//add("","","","");
		$('.pallet_process_date').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
		});
		$("#ui-datepicker-div").css("display","none");
		
		/*
		   // 时间设置
        $('#example_1').datetimepicker({
            timeFormat: "HH:mm",
            dateFormat: "yy-mm-dd"
        });*/
	});
	var p = 0;
	function add(receive_category,rma,rv,cust_refno){
		p++;
		var con=$('#pallet_view');
		var html='';
		html='<div id="av">'+
					'<table width="100%" border="0" cellspacing="0" cellpadding="0" >'+
						'<tr height="30px;">'+
									'<td width="20%" align="right">'+
									'<span name="title">Pallet Process Date:</span>'+
								'</td>'+
								'<td width="30%" colspan="4" align="left">'+
							   		'<input type="text" id="pallet_process_date_'+p+'" name="pallet_process_date" class="pallet_process_date" size="22" value="" />'+
							   '</td>'+
						'</tr>'+
							'<tr height="30px;">'+		
								'<td width="10%" align="right">'+
						  		 	'<span name="title">RA/RV:</span>'+
						  		'</td>'+	
							 	'<td width="15%" align="left">'+
							   		'<input type="text" id="rma_'+p+'" name="rma" size="22" value="'+rma+'" />'+
								'</td>'+
							 	'<td width="20%" align="right">'+
									'<span name="title">Cust RefNo:</span>'+
								'</td>'+
								'<td width="35%" align="left">'+
									'<select name="cust_refno">'+
										'<option value="1">BL#</option>'+
										'<option value="2">DOC#</option>'+
										'<option value="3">CLAIM#</option>'+
										'<option value="4">other</option>'+
									'</select>'+
									'&nbsp;<input type="text" id="cust_refno_'+p+'" name="cust_refno" size="22" value="'+cust_refno+'" />'+
								'</td>'+
							 	'<td rowspan="2" align="center" style="vertical-align: bottom;">'+
							 		'<input type="button" value="Del" id="delete_'+p+'" class="short-short-button-del" name="detButton" /><br><br>'+
							 	'</td>'+		
							'</tr>'+
							'<tr height="30px;">'+					
							   '<td width="10%" align="right">'+
					  		 	'<span name="title">Total QTY:</span>'+
					  		'</td>'+	
						 	'<td width="15%" align="left">'+
						   		'item 数量'+
							'</td>'+
							 
							   
							'</tr>'+
						 '</table>'+
						 '<hr size="1"/>'+
				 '</div>';
	    
		var $container=$(html).appendTo(con);                    //获得容器

	    var $det_button=$("input[name='detButton']",$container); //获得删除按钮
	    //绑定删除事件
	    $det_button.click(function(){
	    	var zhi=$(this).attr("id").split("_")[1];
		    var detail_id=$('#detailid'+zhi).val();
		    
	    	//删除操作 弹出窗口 提示 是否删除
	    	$.artDialog({
			    content: 'do you want to delete?',
			    icon: 'question',
			    width: 200,
			    height: 70,
			    title:'',
			    okVal: 'Confirm',
			    ok: function () {			
			    	$container.remove('#av');	
			    },
			    cancelVal: 'Cancel',
			    cancel: function(){
				}
			});	    	   		 	    	    	
		});
		
		var $process_button=$("input[name='processButton']",$container);
		   
	    $process_button.click(function(){
		    var zhi2=$(this).attr("id").split("_")[1];
		    var receive_category_text = $("#receive_category_"+zhi2).val();
		    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/return_item_view.html"; 
		    uri += '?receive_category_text='+receive_category_text;
			$.artDialog.open(uri , {title: "itemView",width:'900px',height:'530px', lock: true,opacity: 0.3,fixed: true});   		 	    	    	
		});
	}
	function process()
	{
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/return_item_view.html"; 
		    uri += '?pallet_view_id=<%=pallet_view_id%>&ra_rv=<%=null==palletViewRow?"":palletViewRow.getString("ra_rv") %>';
			$.artDialog.open(uri , {title: "itemView",width:'1000px',height:'530px', lock: true,opacity: 0.3,fixed: true}); 
	}

</script>
