<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="returnReceiveFromTypeKey" class="com.cwc.app.key.ReturnReceiveFromTypeKey"/>
<jsp:useBean id="returnReceiveCategoryKey" class="com.cwc.app.key.ReturnReceiveCategoryKey"/>
<jsp:useBean id="returnOrderStatusKey" class="com.cwc.app.key.ReturnOrderStatusKey"/>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>

<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>
<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script> 
<!-- 打印 -->
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<script src="../js/custom_seach/custom_analyze.js" type="text/javascript"></script>


<%
	
	
	String editType = StringUtil.getString(request,"editType");
	String returnOrderEditAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/return/returnOrderEditAction.action";
	
	DBRow row = null;
	long receive_id  = 0;
	String receive_from = "";
	String receive_store = "";
	String package_count = "";
	String bol_tracking = "";
	String update_pallet_count = "";
	String dock_receive_date =  DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
	String receive_carrier = "";
	String receive_category = String.valueOf(returnReceiveCategoryKey.END_USER);//end user
	String status = String.valueOf(returnOrderStatusKey.RECEIVING);//

	if("edit".equals(editType)){
		receive_id  = StringUtil.getLong(request,"receive_id");
		row = returnOrderMgrGql.returnOrderById(receive_id);
		receive_from = row.getString("receive_from");
		receive_store = row.getString("receive_store");
		package_count = row.getString("package_count");
		bol_tracking = row.getString("bol_tracking");
		update_pallet_count = row.getString("update_pallet_count");
		dock_receive_date = tDate.getFormateTime(row.getString("dock_receive_date"), "yyyy-MM-dd") ;
		receive_carrier = row.getString("receive_carrier") ;
		receive_category = row.getString("receive_category") ;
		status = row.getString("status");
		editType = "edit";
	}
	
	ArrayList list = returnReceiveFromTypeKey.getReturnReceiveFromTypeKeys();
	
%>

<title>end user view</title>
</head>
<body>
    <div style="height:465px; border:0px solid red;overflow:auto">
	    <br/>
	     <div id="receiving_view" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:850px;">
		  	<div id="av">
		  		<form id="editForm" name="editForm" action="">
		  		<input type="hidden" id="status" name="status" value="<%=status%>">
		  		<%if("edit".equals(editType)){ %>
		  			<input type="hidden" id="receive_id" name="receive_id" value="<%=receive_id%>" >
		  		<%} %>
		  			<input type="hidden" id="editType" name="editType" value="<%=editType%>">
		  			<input type="hidden" id="status" name="status" value="0">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" >
							<tr height="30px;">		
							 	<td width="20%" align="right">
									<span name="title">Receive From:</span>
								</td>
								<td width="30%" align="left">
									<select name="receive_from" id="receive_from">
										<%for(int i=0;i<list.size();i++){ %>
											<option value="<%=list.get(i)%>" <%=list.get(i).equals(receive_from)?"selected":"" %> >
												<%=returnReceiveFromTypeKey.getReturnReceiveFromTypeKeyValue(Integer.parseInt(list.get(i)+"")) %>
											</option>
										
										<%} %>
									</select>
								</td>
								<td width="15%" align="right">
						  		 	<span name="title">Store:</span>
						  		</td>	
							 	<td width="15%" align="left">
							   		<input type="text" id="receive_store" name="receive_store" size="25" value="<%=receive_store %>" />
								</td>
							</tr>
							<tr height="30px;">					
								<td width="20%" align="right">
									<span name="title">Dock Receive Date:</span>
								</td>
								<td width="30%" align="left">  
							   		<input type="text" id="dock_receive_date" name="dock_receive_date" class="receive_date" size="25" value="<%=dock_receive_date %>" />
							   </td>
							   <td width="15%" align="right">
								<span name="title">Package Count:</span>
								</td>	
							   <td width="15%" align="left">
							   		<input type="text" name="package_count" id="package_count"  size="25" value="<%=package_count %>" />
						  	   </td>	
							</tr>
							<tr height="30px;">					
								<td width="15%" align="right">
									<span name="title">BOL / Tracking:</span>
								</td>
							   <td width="15%" align="left">
							   		<input type="text" name="bol_tracking" id="bol_tracking"  size="25" value="<%=bol_tracking %>" />
						  	   </td>
						  	 <td width="20%" align="right">
								<span name="title">Receive Carrier:</span>
							</td>
							   <td width="15%" align="left">
							   		<input type="text" name="receive_carrier" id="receive_carrier"  size="25" value="<%=receive_carrier %>" />
						  	   </td>	
																		
							</tr>
							<tr height="30px;">					
								<td width="15%" align="right">
									<span name="title">Updated Pallet Count:</span>
								</td>	
							   <td width="15%" align="left">
							   		<input type="text" name="update_pallet_count" id="update_pallet_count"  size="25" value="<%=update_pallet_count%>" />
						  	   </td>	
						  	
								<td width="15%" align="right">
									<span name="title">Receive Category:</span>
								</td>
								<td width="30%" align="left">
									&nbsp;&nbsp;<span name="title">End User</span>
									<input type="hidden" name="receive_category" id="receive_category" value="<%=receive_category%>" />
								</td>
							</tr>
						 </table>
					</form>
						 <hr size="1"/>
				 </div>
	    </div>
    </div>
     <div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;width:850px;margin-top:2px;vertical-align: bottom;">
         	<input class="normal-green" type="button" onclick="tijiao()" value="PrintPalletLabel" >
    	<input class="normal-green" type="button" onclick="returnEdit()" value="SUBMIT" >
    </div>
</body>
</html>
<script>
$.blockUI.defaults = {
		 css: { 
		  padding:        '8px',
		  margin:         0,
		  width:          '170px', 
		  top:            '45%', 
		  left:           '40%', 
		  textAlign:      'center', 
		  color:          '#000', 
		  border:         '3px solid #999999',
		  backgroundColor:'#ffffff',
		  '-webkit-border-radius': '10px',
		  '-moz-border-radius':    '10px',
		  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		 },
		 //设置遮罩层的样式
		 overlayCSS:  { 
		  backgroundColor:'#000', 
		  opacity:        '0.6' 
		 },
		 
		 baseZ: 99999, 
		 centerX: true,
		 centerY: true, 
		 fadeOut:  1000,
		 showOverlay: true
		};

	//添加时间控件
	 $('#dock_receive_date').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
		});
		$("#ui-datepicker-div").css("display","none");
		
		function returnEdit(){
			ajaxReturnEdit();
<%--		 	if(validateFrom()){--%>
<%--		   		 ajaxAddBoxType();--%>
<%--			}--%>
		}
		

		function validateFrom(){
			var receive_from  = $("#receive_from");
		    var receive_store = $("#receive_store") ;
		    var dock_receive_date = $("#dock_receive_date");
		    var package_count = $("#package_count");
		    var bol_tracking = $("#bol_tracking");
		    var receive_carrier = $("#receive_carrier");
		    var update_pallet_count = $("#update_pallet_count");
		    if(receive_from.val() * 1 == 0){
			    showMessage("请选择Receive From类型.","alert");
			    receive_from.focus();
				return false ;
			}
		    if(receive_store.val() * 1 == 0){
			    showMessage("请填入Store信息.","alert");
			    receive_store.focus();
				return false ;
			}
		    if(dock_receive_date.val() * 1 == 0){
			    showMessage("填入Dock Receive Date信息.","alert");
			    dock_receive_date.focus();
				return false ;
			}
		    if(package_count.val() * 1 == 0){
			    showMessage("请填入Package Count信息.","alert");
			    package_count.focus();
				return false ;
			}
		    if(bol_tracking.val() * 1 == 0){
			    showMessage("请填入BOL / Tracking信息.","alert");
			    bol_tracking.focus();
				return false ;
			}
		    if(receive_carrier.val() * 1 == 0){
			    showMessage("填入Receive Carrier信息.","alert");
			    receive_carrier.focus();
				return false ;
			}
		    if(update_pallet_count.val() * 1 == 0){
			    showMessage("请 填入Updated Pallet Count信息.","alert");
			    update_pallet_count.focus();
				return false ;
			}
			return true ;
		}
		 
		function ajaxReturnEdit(){
		    $.ajax({
				url:'<%=returnOrderEditAction%>',
				data:$("#editForm").serialize(),
				dataType:'json',
				type:'post',
				beforeSend:function(request){
			      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			    },
				success:function(data){
				  $.unblockUI();
			 
				  if(data != null){
						if(data.flag == "success"){
							showMessageStatus("操作成功", 500);
							setTimeout("windowClose()", 1000);
						}
						if(data.flag == "error"){
						    showMessage("系统错误添加失败.","alert");
						}
				  }else{
				      showMessage("系统错误添加失败.","alert");
				  }
				},
				error:function(){
				    $.unblockUI();
				}
			})
		}
		
		//显示操作成功
		function showMessageStatus(content, secs)
		{
			var d = $.artDialog({
			    content: content
			});
			d.show();
			setTimeout(function () {
			    d.close();
			}, secs);	
		}
		
		function windowClose(){
			$.artDialog && $.artDialog.close();
			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
		};
		 

</script>
