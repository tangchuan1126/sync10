<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<jsp:useBean id="returnServicePackagingKey" class="com.cwc.app.key.ReturnServicePackagingKey"></jsp:useBean>
<jsp:useBean id="returnServiceAccessoriesKey" class="com.cwc.app.key.ReturnServiceAccessoriesKey"></jsp:useBean>
<jsp:useBean id="returnServicePalletHandleStatusKey" class="com.cwc.app.key.ReturnServicePalletHandleStatusKey"></jsp:useBean>
<jsp:useBean id="returnServicePalletItemStatusKey" class="com.cwc.app.key.ReturnServicePalletItemStatusKey"></jsp:useBean>

<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<html>
<head>
<%
int p = StringUtil.getInt(request,"p");
PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(10);
long pallet_view_id = StringUtil.getLong(request, "pallet_view_id");
DBRow[] returnRows = returnMgrZyj.findReturnPalletItemRows(pallet_view_id, pc);

%>
<title>return</title>
<script type="text/javascript">
$(function(){
	$(".zebraTable tbody tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
	$(".zebraTable tbody tr:odd").addClass("alt");
});
function editOneRow(item_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/return_item_view.html"; 
    uri += '?item_id='+item_id;
	$.artDialog.open(uri , {title: "itemView",width:'1000px',height:'530px', lock: true,opacity: 0.3,fixed: true}); 	
}
function del(item_id)
{
	if(confirm("确定要删除：["+item_id+"]吗？")){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/return_order/ReturnPalletItemDeleteAction.action',
			data:{item_id:item_id},
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag=='true')
				{
					showMessageStatus("删除成功", 1500);
					refreshWindow();
					$.artDialog.opener.updatePalletItemCountSpan  && $.artDialog.opener.updatePalletItemCountSpan();
				}
			},
			error:function(){
				showMessage("系统错误","error");
			}
		});
	}
}
//刷新页面
function refreshWindow(){
	window.location.reload();
};
</script>
<style type="text/css">
.zebraTable td{border-bottom: 1px solid #dddddd;padding-left:10px;}
.zebraTable tr.alt td {background: #f9f9f9;}
.zebraTable tr.over td{background: #E6F3C5;}
</style>
</head>
<body onLoad="onLoadInitZebraTable()">
	<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0"  class="zebraTable" style="margin-left:3px;margin-top:5px;">
		  <tr> 
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Packaging</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Accessories</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Serial Number</th>
		  	<th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Box Serial Number</th>
	        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">Model</th>
	        <th width="19%" style="vertical-align: center;text-align: center;" class="right-title">SKU</th>
	        <th width="9%" style="vertical-align: center;text-align: center;" class="right-title">Status</th>
	        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">RA/RV</th>
	        <th width="12%" style="vertical-align: center;text-align: center;" class="right-title">Operate</th>
	      </tr>
	      <tbody class="mytbody">
	      	<%
	      		if(returnRows.length > 0)
	      		{
	      			for(int i = 0; i < returnRows.length; i ++)
	      			{
	      				DBRow returnRow = returnRows[i];
	      	%>
	      		<tr>
	      			<td>
						<%=returnMgrZyj.changeServicePackagingKeyStrToNameStr(returnRow.getString("packaging"))%>
						
					</td>
	      			<td>
						<%=returnMgrZyj.changeServiceAccessoriesKeyStrToNameStr(returnRow.getString("accessories"))%>
					</td>
	      			<td>
	      				<%="".equals(returnRow.getString("serial_number"))?"&nbsp;":returnRow.getString("serial_number") %>
	      			</td>
	      			<td>
	      				<%="".equals(returnRow.getString("box_serial_number"))?"&nbsp;":returnRow.getString("box_serial_number") %>
	      			</td>
	      			<td>
	      				<%="".equals(returnRow.getString("model"))?"&nbsp;":returnRow.getString("model") %>
	      			</td>
	      			<td>
	      				<%="".equals(returnRow.getString("sku"))?"&nbsp;":returnRow.getString("sku") %>
	      			</td>
	      			<td>
	      				<%=returnServicePalletItemStatusKey.getReturnServicePalletItemStatusKeyById(returnRow.get("status", 0)) %>
	      			</td>
	      			<td>
	      				<%=returnRow.getString("ra_rv") %>
	      			</td>
	      			<td align="center">
		      			<br><input class="short-short-button" type="button" onclick="editOneRow(<%=returnRow.get("item_id", 0L) %>)" value="Edit" ><br>
		      			<input type="button" value="Del" class="short-short-button-del" onclick="del(<%=returnRow.get("item_id", 0L) %>)" name="detButton" />
						<input type="button" value="Print" class="short-short-button" name="printButton"
						 onclick="printItem(<%=returnRow.getString("model")%>, <%=returnRow.getString("sku")%>, <%=returnRow.getString("serial_number")%>, <%=returnRow.getString("ra_rv")%>, <%=returnRow.get("item_id", 0L) %>)"/><br>
	      			</td>
	      		</tr>	
	      	<%
	      			}
	      		}
	      		else
	      		{	
	      	%>
	      		<td colspan="9" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	      	<%
	      		}
	      	%>
		  </tbody>
	</table>
	
<form action="" id="pageForm" method="post">
		<input type="hidden" name="p" id="pageCount" value="<%=p %>"/>
		<input type="hidden" name="cmd" value=""/>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle">
	<%
		int pre = pc.getPageNo() - 1;
		int next = pc.getPageNo() + 1;
		out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
		out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
		out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
		out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
		out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
      Goto
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
</body>
 <script>
     function addPalletView()
     {
    	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_order/pallet_view.html"; 
     	$.artDialog.open(uri , {title: "palletView",width:'900px',height:'530px', lock: true,opacity: 0.3,fixed: true});
     }
     
 </script>
</html>

