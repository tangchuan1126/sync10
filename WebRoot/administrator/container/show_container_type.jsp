<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.fr.base.Inter"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	int type_id = StringUtil.getInt(request, "type_id");
	DBRow containerInfo = containerMgrZYZ.getDetailContainerType(type_id);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Show Basic Container Type</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<script>
$(function(){
	$(".chzn-select").chosen({no_results_text: "no this options:"});
});
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

function closeWin()
{
	$.artDialog.close();
}

</script>
<style type="text/css">
.STYLE1 {width:35%; font-size:14px; font-family: Arial, Helvetica, sans-serif;}
.STYLE2 {color: #0066FF; width:65%; font-size:14px; font-family: Arial, Helvetica, sans-serif;}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="addContainerTypeForm" id="addContainerTypeForm" method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top" style="vertical-align: middle;">	
 	<table width="100%" border="0" cellspacing="3" cellpadding="2" style="line-height: 20px;">
<!--  			<tr> -->
<!-- 			  	<td align="right" valign="middle" class="STYLE1">Container Category : </td> -->
<!-- 			    <td align="left" valign="middle" class="STYLE2">  -->
<%-- 			    	<%=containerTypeKey.getContainerTypeKeyValue(containerInfo.get("container_type", 0))%> --%>
<!--  			    </td> -->
<!-- 			</tr> -->
       		 <tr>
			  	 <td align="right" valign="middle" class="STYLE1" >Packaging Type : </td>
			    <td align="left" valign="middle" class="STYLE2"> 
			    	<%=containerInfo.getString("type_name") %>
			    </td>
			</tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE1" >Length : </td>
			    <td align="left" valign="middle" class="STYLE2">
			    	<%=containerInfo.get("length",0f) %>
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE1" >Width : </td>
			    <td align="left" valign="middle" class="STYLE2">
			    	<%=containerInfo.get("width",0f) %>
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE1" >Height : </td>
			    <td align="left" valign="middle" class="STYLE2">
			    	<%=containerInfo.get("height",0f) %>
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE1" >Weight : </td>
			    <td align="left" valign="middle" class="STYLE2">
			    	<%=containerInfo.get("weight",0f) %>
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE1" >Max Capacity : </td>
			    <td align="left" valign="middle" class="STYLE2">
			    	<%=containerInfo.get("max_load",0f) %>
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE1" >Max Height : </td>
			    <td align="left" valign="middle" class="STYLE2">
			    	<%=containerInfo.get("max_height",0f) %>
			    </td>
			  </tr>
			  
			 <tr>
				<td align="right" valign="middle" class="STYLE1" >LengthUOM : </td>
			    <td align="left" valign="middle" class="STYLE2">
					<%=lengthUOMKey.getLengthUOMKey(containerInfo.get("length_uom", 0))%>
			    </td>
			</tr>
			<tr>
				<td align="right" valign="middle" class="STYLE1" >WeightUOM : </td>
			    <td align="left" valign="middle" class="STYLE2">
				    <%=weightUOMKey.getWeightUOMKey(containerInfo.get("weight_uom", 0))%>
			    </td>
		  </tr>
    </table>

</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
		<a name="Submit2" value="Close" class="buttons primary big" onClick="closeWin();">Close</a> 
	</td>
  </tr>
</table>
</form>	
</body>
</html>
