<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.fr.base.Inter"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.PriceUOMKey"%>
<jsp:useBean id="priceUOMKey" class="com.cwc.app.key.PriceUOMKey"></jsp:useBean>
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<%
	
	String AddContainerTypeAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/container/AddContainerTypeAction.action";
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	int containerType = StringUtil.getInt(request, "containerType");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add Basic Container Type</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<script>
$(function(){
	$(".chzn-select").chosen({no_results_text: "no this options:"});
});
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
/*	$(function(){
		$("input[type!='button']").attr("style","text-transform:uppercase");
	});*/

</script>
<script>

function addContainerType()
{
		  
	var f = document.addContainerTypeForm;

	if(f.typeName.value == ""){
		showMessage("Please input Type Name","alert");
		return false;
	}
	else if (f.length.value == 0){
		showMessage("Please input Length","alert");
		return false;
	 }
	else if (f.width.value == 0){
	 	showMessage("Please input Width","alert");
	 	return false;
	 }
	else if (f.height.value == 0){
	 	showMessage("Please input Height","alert");
	 	return false;
	 }
	else if (f.weight.value == 0){
	 	showMessage("Please input Weight","alert");
	 	return false;
	 }
	else if (f.max_load.value == 0){
	 	showMessage("Please input Max Capacity","alert");
	 	return false;
	 }
	else if (f.max_height.value == 0){
	 	showMessage("Please input max Height","alert");
	 	return false;
	 }
	else if (f.length_uom.value == 0){
	 	showMessage("Please input length uom","alert");
	 	return false;
	 }
	else if (f.weight_uom.value == 0){
	 	showMessage("Please input weight uom","alert");
	 	return false;
	 }
	else{
		ajaxModContainerType();
	}
}
function ajaxModContainerType(){

	$.ajax({
		url:'<%=AddContainerTypeAction%>',
		data:$("#addContainerTypeForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   },
	   success:function(data){
		   
		   $.unblockUI();
			if(data && data.flag == "true"){
				
			    setTimeout("windowClose()", 1000);
			    
			} else if(data && data.flag == "false"){
				
				showMessage(data.info,"alert");
				
			} else{
				
				showMessage("System error","error");
			}
		},
		error:function(){
		    $.unblockUI();
			showMessage("System error","error");
		}
	});
}

function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function closeWin()
{
	$.artDialog.close();
}

</script>
<style type="text/css">
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="addContainerTypeForm" id="addContainerTypeForm" method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	

 	<table width="100%" border="0" cellspacing="3" cellpadding="2" style="padding-top:10px;">
 	 		<tr style="display: none">
			  	<td align="right" valign="middle" class="STYLE2" >Container Category:</td>
			    <td align="left" valign="middle" > 
			    	<div class="side-by-side clearfix">
					 <select name="container_type" id="container_type" class="chzn-select" data-placeholder="Choose a Type..." tabindex="1"  style="width:100px">
			    	<%
			    	List<String> arrayList =(ArrayList<String>)	containerTypeKey.getContainerTypeKeys();
			    	for(String i : arrayList){
			    		String selectStr = (i.equals(containerType+""))?"selected=selected":"";
			    		if(ContainerTypeKey.Original != Integer.parseInt(i))
			    		{
			    		%>
			    			<option value='<%= i %>' <%=selectStr %>><%= containerTypeKey.getContainerTypeKeyValue(i) %></option>
			    		<%
			    		}
			    	}
			    	%>
			    	</select>
					</div>
 			    </td>
			</tr>
      		<tr>
				<td align="right" valign="middle"><b>Packaging Type:</b></td>
			    <td align="left" valign="middle" > 
			    	<input name="typeName" id="typeName" type="text" value="" size="35px" style="height:30px;"/>
			    </td>
			</tr>
			<tr>
				 <td align="right" valign="middle"><b>Length:</b></td>
			    <td align="left" valign="middle" >
			    <input name="length" id="length" type="text" value="" size="35px" style="height:30px;"/>
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle"><b>Width:</b></td>
			    <td align="left" valign="middle" >
			    <input name="width" id="width" type="text" value="" size="35px" style="height:30px;"/>
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle"><b>Height:</b></td>
			    <td align="left" valign="middle" >
			    <input name="height" id="height" type="text" value="" size="35px" style="height:30px;">
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle"><b>Weight:</b></td>
			    <td align="left" valign="middle" >
			    <input name="weight" id="weight" type="text" value="" size="35px" style="height:30px;">
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle"><b>Max Capacity:</b></td>
			    <td align="left" valign="middle" >
			    <input name="max_load" id="max_load" type="text" value="" size="35px" style="height:30px;">
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle"><b>Max Height:</b></td>
			    <td align="left" valign="middle" >
			    <input name="max_height" id="max_height" type="text" value="" size="35px" style="height:30px;">
			    </td>
			  </tr>
			  
			 <tr>
				<td align="right" valign="middle"><b>LengthUOM:</b></td>
			    <td align="left" valign="middle" >
<!-- 			    <input name="length_uom" type="text" id="length_uom" value="" size="35px"> -->
						<%ArrayList lengthUomKeys = lengthUOMKey.getLengthUOMKeys(); %>
						<div class="side-by-side clearfix">
				 	     <select id="length_uom" name="length_uom" class="chzn-select" data-placeholder="Choose a UOM..." tabindex="1"  style="width:100px;height:30px;">
				 	     	<%for(int a=0;a<lengthUomKeys.size();a++){ %>
						    <option value="<%=Integer.parseInt(String.valueOf(lengthUomKeys.get(a))) %>" ><%=lengthUOMKey.getLengthUOMKey(Integer.parseInt(String.valueOf(lengthUomKeys.get(a))))%></option>
						    <%}%>
						 </select>
						 </div>
			    </td>
			</tr>
			<tr>
				<td align="right" valign="middle"><b>WeightUOM:</b></td>
			    <td align="left" valign="middle" >
<!-- 			    <input name="weight_uom" type="text" id="weight_uom" value="" size="35px"> -->
					<%ArrayList weightUomKeys = weightUOMKey.getWeightUOMKeys(); %>
					<div class="side-by-side clearfix">
			 	     <select id="weight_uom" name="weight_uom" class="chzn-select" data-placeholder="Choose a UOM..." tabindex="1"  style="width:100px;height:30px;">
			 	     	<%for(int a=0;a<weightUomKeys.size();a++){ %>
					    <option value="<%=Integer.parseInt(String.valueOf(weightUomKeys.get(a))) %>" ><%=weightUOMKey.getWeightUOMKey(Integer.parseInt(String.valueOf(weightUomKeys.get(a))))%></option>
					    <%}%>
					 </select>
					 </div>
			    </td>
		  </tr>
    </table>

</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <a name="Submit2" value="Submit" class="buttons primary big" onClick="addContainerType();">Submit</a><hidden></hidden>

	  <a name="Submit2" value="Cancel" class="buttons big" onClick="closeWin();">Cancel</a>
	</td>
  </tr>
</table>
</form>	
</body>
</html>
