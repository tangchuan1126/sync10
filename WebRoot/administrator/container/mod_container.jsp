<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<%
	long containerId = StringUtil.getLong(request,"containerId");
	DBRow row = containerMgrZYZ.getDetailContainer(containerId);
	
	DBRow[] containerTypeRow = containerMgrZYZ.getAllContainerType(null,"");
	String ModContainerAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/container/ModContainerAction.action";
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改容器</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>

function submitContainer()
{
		  
	var f = document.modContainerForm;

	if(f.container.value == ""){
		showMessage("请填写编号","alert");
		return false;
	}
	else if (f.hardwareId.value == ""){
		showMessage("请填写硬件号","alert");
		return false;
	 }
	else if (f.typeId.value == 0){
	 	showMessage("请选择托盘类型","alert");
	 	return false;
	 }
	else{
		modContainer();
	}
}
function modContainer(){

	$.ajax({
		url:'<%=ModContainerAction%>',
		data:$("#modContainerForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   },
		success:function(data){
		   $.unblockUI();
			if(data && data.flag == "true"){
				showMessage("修改成功","succeed");
			    setTimeout("windowClose()", 1000);
			}else{
				showMessage("修改失败","alert");
			}
		},
		error:function(){
			showMessage("系统错误","error");
		}
	});
}

function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function closeWin()
{
	$.artDialog.close();
}
var types = [] ;
var initContainerType ;
var initContainerValue = '<%= row.get("type_id",0l)%>';
jQuery(function($){

	 $("#typeId option").each(function(index,_this){
	 		types.push(_this);
	 });
 	 initContainerType = $("#typeId option:selected").attr("container_type");
  	 initContainerTypeAndContainer(initContainerType,initContainerValue);
  	 
})
//init initContainerType 容器类型 initContainerValue：容器名称ID
function initContainerTypeAndContainer(initContainerType,initContainerValue){
 	var container_type = $("#container_type option:selected").val();
	if(initContainerType){
		container_type = initContainerType;
	}
	var insertValue = [] ;
	 
	if(types && types.length > 0){
		for(var index = 0 , count = types.length ;index < count ; index++   ){
  			if($(types[index]).attr("container_type") == container_type+"" || $(types[index]).val() === "0" ){
				insertValue.push(types[index]);
			}
		}
	}
	$("#typeId option").remove();
	$("#typeId").append(insertValue);
	if(initContainerValue){
	 
 	 	$("#container_type option[value='"+initContainerType+"']").attr("selected",true);
		$("#typeId option[value='"+initContainerValue+"']").attr("selected",true);
	}else{
		$("#typeId option[value='0']").attr("selected",true);
	}	
	
 
}
</script>
<style type="text/css">
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="modContainerForm" id="modContainerForm" method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	
    <input type="hidden"  id="containerId" name="containerId" value="<%=containerId %>"/>
<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:90%;">
  <legend style="font-size:15px;font-weight:normal;color:#999999;">修改容器托盘</legend>
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
 				   <tr>
			  		<td align="right" valign="middle" class="STYLE2" colspan="1" >容器分类:</td>
			  		<td align="left" valign="middle" colspan="3">
			  				<select name="container_type" id="container_type" onchange="initContainerTypeAndContainer();">
						 
							  	<option value='<%=containerTypeKey.TLP %>' ><%= containerTypeKey.getContainerTypeKeyValue(containerTypeKey.TLP) %></option>
					    	 
						</select>
			  		</td>
			  </tr>
			  
			  <tr>
			  	<td align="right" valign="middle" class="STYLE2" colspan="1">类型名称:</td>
			    <td align="left" valign="middle" colspan="3">
				<select name="typeId" id="typeId" >
	                   <option value="0">选择类型名称</option>
	                   <%
	                   		for(int i = 0;i < containerTypeRow.length ;i++){
	                   		 if(containerTypeRow[i].getString("type_id").equals(row.getString("type_id"))){
	                   %>
	                   <option style="background:#fffffffff;" container_type = '<%=containerTypeRow[i].get("container_type",0) %>'  value="<%=containerTypeRow[i].get("type_id",0l)%>" selected="selected"><%=containerTypeRow[i].getString("type_name") %></option>
	                   <%} else{
	                   	%>
	                   	 <option style="background:#fffffffff;" container_type = '<%=containerTypeRow[i].get("container_type",0) %>' value="<%=containerTypeRow[i].get("type_id",0l)%>"><%=containerTypeRow[i].getString("type_name") %></option>
	                   	<%}
	                   }%>
	              </select>     
			    </td>
			  </tr>
 	
       		 <tr>
			  	 <td align="right" valign="middle" class="STYLE2" >容器条码:</td>
			    <td align="left" valign="middle" > 
			    	<input name="container" id="container" type="text" value="<%=row.getString("container") %>"  size="45px"/>
			    </td>
			</tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE2" >RFID:</td>
			    <td align="left" valign="middle" >
			    <input name="hardwareId" id="hardwareId" type="text" value="<%=row.getString("hardwareId") %>" size="45px">
			    </td>
			  </tr>
			  
		
    </table>
 </fieldset>
</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存修改" class="normal-green" onClick="submitContainer();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table>
</form>	
</body>
</html>
