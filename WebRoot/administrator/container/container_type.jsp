<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.LengthUOMKey"%>
<jsp:useBean id="lengthUOMKey" class="com.cwc.app.key.LengthUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.WeightUOMKey"%>
<jsp:useBean id="weightUOMKey" class="com.cwc.app.key.WeightUOMKey"></jsp:useBean>
<%@page import="com.cwc.app.key.PriceUOMKey"%>
<jsp:useBean id="priceUOMKey" class="com.cwc.app.key.PriceUOMKey"></jsp:useBean>
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<%
int p = StringUtil.getInt(request,"p");
PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(30);

String searchKey = StringUtil.getString(request, "search_key");

DBRow[] containerType = containerMgrZYZ.getAllContainerType(pc, searchKey);

String delContainerTypeAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/container/DeleteContainerTypeAction.action";
String exportAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/container/ExportContainer.action";
ContainerTypeKey containerTypeKey = new ContainerTypeKey();
%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Packaging Type</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script> 

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css"; 
</style>

<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/simple.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<style type="text/css">
  .content{padding:10px;}
  
  .ui-widget-content{
border:1px #CFCFCF solid;
border-radius: 0px;
 	-webkit-border-radius: 0px;
-moz-border-radius:0;
}
.ui-widget-header{
	background:#f1f1f1 !important;
	border-radius: 0;
	border-top:0;
	border-left:0;
	border-right:0;
	border-bottom: 1px #CFCFCF solid;
}
.ui-tabs .ui-tabs-nav li a{
	padding: .5em 1em 13px !important;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
	  height: 40px;
	  margin-top: -4px !important;
	background:#fff;
}
.ui-tabs .ui-tabs-nav{
	padding :0 .2em 0;
}
.ui-tabs{
	padding:0;
}
   
 .panel {
margin-top:23px;
  margin-bottom: 20px;
  background-color: #fff;
  border: 1px #CFCFCF solid;
  -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
  box-shadow: 0 1px 1px rgba(0,0,0,.05);
  
}
.panel .panelTitle{
	  background: #f1f1f1;
	  height: 40px;
	  line-height: 40px;
	  padding:0 15px;
}
.panel .panelTitle .title-left{
	    float: left;
}
.panel .panelTitle .title-right{
	  float: right;
}
.clear{
	  clear: both;
}
.right-title{
	height:40px;
	background-color:#fff;
}
.zebraTable th.right-title,.zebraTable th.left-title{background-color:#fff;}
.zebraTable th.right-title{border-left:0;border-right:0;}
.zebraTable th.left-title{border-right:0;}
	
.aui_titleBar{  border: 1px #DDDDDD solid;}
.aui_header{height: 40px;line-height: 40px;}
.aui_title{
  height: 40px;
  width: 100%;
  font-size: 1.2em !important;
  margin-left: 20px;
  font-weight: 700;
  text-indent: 0;
}
.aui_close{
		color:#fff !important;
		text-decoration:none !important;
}


.breadnav {
            padding:0 10px; height:25px;margin-bottom: 18px; 
        }
        .breadcrumb{
          font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
          font-size: 14px;
          line-height: 1.42857143;
        }
        .breadcrumb {
          margin-top:0;
	  padding: 8px 15px;
          margin-bottom: 20px;
          list-style: none;
          background-color: #f5f5f5;
          border-radius: 4px;
        }

        .breadcrumb > li {
          display: inline-block;
        }

        .breadcrumb > .active {
          color: #777;
        }

        .breadcrumb > li + li:before {
            padding: 0 5px;
            color: #ccc;
            content: "/\00a0";
        }
        .breadcrumb a{
		      color: #337ab7;
                text-decoration: none;	
		}

</style>
<script>
$(function(){
	$(".chzn-select").chosen({no_results_text: "no this options:"});
});
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};

</script>
<script type="text/javascript">
	//修改容器托盘类型
	function modContainerType(typeId){
		var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/mod_container_type.html?type_id="+typeId; 
	    $.artDialog.open(uri , {title: "修改容器类型",width:'520px',height:'350px', lock: true,opacity: 0.3,fixed: true});
	}
	
	//添加容器托盘类型
	function addContainerType(){
		var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/add_container_type.html?containerType="+$("#container_type").val(); 
	    $.artDialog.open(uri , {title: "Add Packaging Type",width:'520px',height:'440px', lock: true,opacity: 0.3,fixed: true});
   }
	 
	function deleteContainerType(typeId,typeName, container_type, containerTypeName){
	if(typeId!=0 ){
		
		var d = $.artDialog({
			lock: true
			,opacity: 0.3
			,width:300
			,height:70
			,fixed: true
			,title: 'Delete '+typeName + ' ?'
			,content: 'Are you sure to delete [ '+typeName+'] ?'
			 ,icon:'warning'
			,button:[
					{
						name: 'Sure',
						callback: function () {
							ajaxDeleteContainerType(typeId, container_type, containerTypeName, d);
							
							return false ;
						},
						focus:true
						
					},
					{
						name: 'Cancel'
					}]
		});
		d.show();
	}
	}
	//删除
	function ajaxDeleteContainerType(typeId, container_type, containerTypeName, d){
		var para = 'typeId='+typeId+'&container_type='+container_type;
		$.ajax({
			url:'<%=delContainerTypeAction%>',
			data:para,
			type:'post',
			dataType:'json',
			beforeSend:function(){
				 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
		    success:function(data){
			    $.unblockUI();
			    d.close();
			    if(data && data.flag == "true"){
			    	refreshWindow();
			    }else if(data && data.flag == "false"){
					showMessage("Can’t delete..","alert");
					//refreshWindow();
				}
		    },
			error:function(){
				d.close();
		    	$.unblockUI();
				showMessage("System error","error");
		    }
		});
	}
	//刷新
	function refreshWindow(){
		window.location.reload();
	}

	function prints(container_type, type_id){
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/container_print_batch.html?container_type="+container_type+"&type_id="+type_id;
		$.artDialog.open(url , {title: 'Batch print TLP',width:'700px',height:'550px', lock: true,opacity: 0.3,fixed: true});
	}
	function changeContainerType(obj)
	{
		$("#container_type").val($(obj).val());
		$("#searchForm").submit();
	}
	
	function down(){
		
		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		
		var para='search_key='+$("#search_key").val();
		
		$.ajax({
			url: '<%=exportAction%>',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:para,
			cache:false,
			success: function(date){
				$.unblockUI();       //遮罩关闭
				if(date["flag"]=="true"){
					document.download_form.action=date["fileurl"];
					document.download_form.submit();				
				}	
			}
		});
	}
	
function advancedSearch(){
	
	$("#searchForm").submit();
}

function downloadTemplate(){
	
	var url = '../../administrator/container/PackagingType.xlsm';
	
	document.downloadTemplateForm.action = url;
    document.downloadTemplateForm.submit();
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<form action="" name="downloadTemplateForm" id="downloadTemplateForm"></form>
	
	<!-- <div style="border-bottom:1px solid #CFCFCF; height:25px;padding-top: 13px;margin-left: 10px;margin-bottom: 3px;">
		<img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">
		<span style="font-size: 13px;font-weight: bold"> Setup » Packaging Type</span>
	</div> -->
	
<!-- 	<div class="breadnav"> -->
<!-- 	<ol class="breadcrumb"> -->
<!-- 	            <li><a href="#">Setup</a></li> -->
<!-- 	            <li class="active">Packaging Type</li>  -->
<!-- 	        </ol> -->
<!-- 	</div> -->
	
<!--    <div class="content"> -->
	
   <div id="tabs" style="margin-top: 10px;">
		<ul>
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#av1">Advanced Search</a></li>	 
		</ul>
		<div id="av1">
		<form id="searchForm" action="" method="post">
			<table width="100%" style="margin-left: 5px;">
				<tr>
					<td width="120px">Packaging Type : </td>
					<td align="left" width="120px">
						<input style="width: 210px;height:30px;" name="search_key" type="text" id="search_key" value="<%= searchKey %>">
					</td>
					<td align="left">
						<a name="search" type="submit" class="buttons primary big" value="Search" onclick="advancedSearch()" >
							<i class="icon-search"></i>&nbsp;Search
						</a>
					</td>
				</tr>
			</table>
		</form>
		</div>
	</div>
	
	<script type="text/javascript">
	  	$("#tabs").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
		});
	    $("#tabs").tabs("select",0);  //默认选中第一个
	</script>
	
	
	<div class="panel">
		<div class="panelTitle">
			<div class="title-right">		
				<div class="buttons-group minor-group" style="margin-top:6px;">		 		
				<a name="Submit3" class="buttons" value="Add Packaging Type" onClick="addContainerType();"><i class="icon-plus"></i>&nbsp;Add Packaging Type</a>
				<!-- <a name="Submit4" class="buttons" value="Export Packaging Type" onClick="down();"><i class="icon-share"></i>&nbsp;Export Packaging Type</a> -->
				<a name="Submit" class="buttons" onClick="downloadTemplate()"><i class="icon-download-alt"></i>&nbsp;Download Template</a>
				</div>
			</div>
		</div>
	
	
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
     <!--    <th width="9%" class="right-title" style="vertical-align: center;text-align: center;">LP Category</th> -->
        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">Packaging Type</th>
        <th width="8%" class="left-title" style="vertical-align: center;text-align: center;">Length</th>
        <th width="8%" style="vertical-align: center;text-align: center;" class="left-title">Width</th>
        <th width="8%" class="left-title" style="vertical-align: center;text-align: center;">Height</th>
        <th width="8%" style="vertical-align: center;text-align: center;" class="left-title">Weight</th>
        <th width="8%" style="vertical-align: center;text-align: center;" class="left-title">Max Capacity</th>
        <th width="8%" style="vertical-align: center;text-align: center;" class="left-title">Max Height</th>
        <th width="6%" style="vertical-align: center;text-align: center;" class="left-title">Length&nbsp;UOM</th>
        <th width="6%" style="vertical-align: center;text-align: center;" class="left-title">Weight&nbsp;UOM</th>
        <th width="16%" style="vertical-align: center;text-align: center;" class="left-title">Operation</th>
    </tr>
  <%
  	if(containerType.length > 0){
  		
  		for(int i = 0;i < containerType.length; i++){
  %>
    <tr>
  <!--       <td  width="9%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
      	<%=containerTypeKey.getContainerTypeKeyValue(containerType[i].get("container_type",containerTypeKey.CLP)) %>
      </td> -->
	<%
		String length = String.valueOf(containerType[i].get("length",0f));
		String width = String.valueOf(containerType[i].get("width",0f));
		String height = String.valueOf(containerType[i].get("height",0f));
		String weight = String.valueOf(containerType[i].get("weight",0f));
		String max_load = String.valueOf(containerType[i].get("max_load",0f));
		String max_height = String.valueOf(containerType[i].get("max_height",0f));
		
		if(!"".equals(length) && length != null && length.matches("[0-9]\\d*\\.?00*")){
			length = length.substring(0,length.indexOf("."));
		}
		if(!"".equals(width) && width != null && width.matches("[0-9]\\d*\\.?00*")){
			width = width.substring(0,width.indexOf("."));
		}
		if(!"".equals(height) && height != null && height.matches("[0-9]\\d*\\.?00*")){
			height = height.substring(0,height.indexOf("."));
		}
		if(!"".equals(weight) && weight != null && weight.matches("[0-9]\\d*\\.?00*")){
			weight = weight.substring(0,weight.indexOf("."));
		}
		if(!"".equals(max_load) && max_load != null && max_load.matches("[0-9]\\d*\\.?00*")){
			max_load = max_load.substring(0,max_load.indexOf("."));
		}
		if(!"".equals(max_height) && max_height != null && max_height.matches("[0-9]\\d*\\.?00*")){
			max_height = max_height.substring(0,max_height.indexOf("."));
		}
	%>
    <td  width="15%" height="60" align=" " valign="middle" style='word-break:break-all;font-weight:bold' ><%=containerType[i].getString("type_name") %></td>
      <td  width="8%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=length %></td>
      <td  width="8%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=width %></td>
      <td  width="8%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=height %></td>
      <td  width="8%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=weight %></td>
      <td  width="8%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=max_load %></td>
      <td  width="8%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=max_height %></td>
      <td  width="6%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=lengthUOMKey.getLengthUOMKey(containerType[i].get("length_uom", 0))%></td>
      <td  width="6%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=weightUOMKey.getWeightUOMKey(containerType[i].get("weight_uom", 0))%></td>
   	  <td  width="16%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold'> 
      	<a name="Submit2" class="buttons" value="Del" onClick="deleteContainerType(<%=containerType[i].get("type_id",0l) %>,'<%=containerType[i].getString("type_name") %>','<%=containerType[i].get("container_type",0)%>','<%=containerTypeKey.getContainerTypeKeyValue(containerType[i].get("container_type",containerTypeKey.CLP)) %>');"><i class="icon-trash"></i>&nbsp;Del</a>
<%--      	<%if(containerType[i].get("container_type",containerTypeKey.CLP)==3){%> --%>
<%--      	 	<input type="button" value="BatchPrint" class="short-button" onclick='prints(<%=containerTypeKey.TLP%>,<%=containerType[i].get("type_id",0l)%>)'/> --%>
     		<a value=" BatchPrint"  class="buttons"  onclick='prints(<%=containerTypeKey.TLP%>,<%=containerType[i].get("type_id",0l)%>)'><i class="icon-print"></i>&nbsp;Batch Print TLP</a>
<%--      	<%}%> --%>
      </td>
    </tr>
    <%} 
    }else{%>
    	<tr>
   		 	<td colspan="11" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Data</td>
   		</tr>
    <%} %>
    </table>
    <br/>
    <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" action="container_type.html">
	          <input type="hidden" name="p">
	          <input type="hidden" name="search_key" value="<%=searchKey %>" />      
	  </form>
	        <tr> 
	          
	    <td height="28" align="right" valign="middle"> 
	      <%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
	     Goto
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
	    </td>
	        </tr>
</table> 
</div>
<form action="" name="download_form" id="download_form">
</form>

</div>

  </body>
</html>
