<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<%

DBRow[] containerTypeRow = containerMgrZYZ.getAllContainerType(null,"");

String AddContainerAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/container/AddContainerAction.action";

ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add TLP</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>

function addContainer()
{
		  
	var f = document.addContainerForm;
	/*
	if(f.container.value == ""){
		showMessage("请填写编号","alert");
		return false;
	}
	else if (f.hardwareId.value == ""){
		showMessage("请填写硬件号","alert");
		return false;
	 }else */
	 if(f.container_type == 0){
	 	showMessage("Select Lp category","alert");
	 	return;
	 }
	else if (f.typeId.value == 0){
	 	showMessage("Select Packaging type","alert");
	 	return;
	 }
	else{
		ajaxAddContainer();
	}
}
function ajaxAddContainer(){

	$.ajax({
		url:'<%=AddContainerAction%>',
		data:$("#addContainerForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   },
		success:function(data){
		   $.unblockUI();
			if(data && data.flag == "true"){
			    setTimeout("windowClose()", 1000);
			}
		},
		error:function(){
			showMessage("System error","error");
			$.unblockUI();
		}
	});
}

function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function closeWin()
{
	$.artDialog.close();
}
//
$(function(){  
	$("#typeId").chosen({no_results_text: "no this options:"});
});  
/*
var types = [] ;
jQuery(function($){

	 $("#typeId option").each(function(index,_this){
	 		types.push(_this);
	 });
 	 initContainerTypeAndContainer();
  	 
})
function initContainerTypeAndContainer(){
	var container_type = $("#container_type option:selected").val();
	var insertValue = [] ;
	 
	if(types && types.length > 0){
		for(var index = 0 , count = types.length ;index < count ; index++   ){
  			if($(types[index]).attr("container_type") == container_type+"" || $(types[index]).val() === "0" ){
				insertValue.push(types[index]);
			}
		}
	}
	$("#typeId option").remove();
	$("#typeId").append(insertValue);
	$("#typeId option[value='0']").attr("selected",true);
 
}
*/
</script>
<style type="text/css">
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}

#typeId_chzn ul.chzn-results{  height: 150px;max-height: 150px;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="addContainerForm" id="addContainerForm" method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	

 	<table width="100%" border="0" cellspacing="3" cellpadding="2" style="padding-top:10px;">
 			<tr>
			  		<td align="right" valign="middle" colspan="1" ><b>LP Category: </b></td>
			  		<td align="left" valign="middle" colspan="3">
<%--			  				<select name="container_type" id="container_type" onchange="initContainerTypeAndContainer();">--%>
<%--					    			<option value='<%= containerTypeKey.TLP %>' ><%= containerTypeKey.getContainerTypeKeyValue( containerTypeKey.TLP) %></option>--%>
<%--							</select>--%>
						<%= containerTypeKey.getContainerTypeKeyValue( containerTypeKey.TLP) %>
						<input type="hidden" name="container_type" id="container_type" value="<%=containerTypeKey.TLP%>">
			  		</td>
			  </tr>
			  <tr>
			  	<td align="right" valign="middle" colspan="1"><b>Packaging Type: </b></td>
			    <td align="left" valign="middle" colspan="3">
				<select name="typeId" id="typeId" class="chzn-select" data-placeholder="Select..." tabindex="1" style="height:30px;width: 210px;">
	                   <option value=""></option>
	                   <%
	                   		for(int i = 0;i < containerTypeRow.length ;i++){
	                   %>
	                  	 <option style="background:#fffffffff;" value="<%=containerTypeRow[i].get("type_id",0l)%>" container_type="<%=containerTypeRow[i].get("container_type",0) %>"><%=containerTypeRow[i].getString("type_name") %></option>
	                   <%} %>
	              </select>     
			    </td>
			  </tr>
<!--        		 <tr> -->
<!-- 			  	 <td align="right" valign="middle" class="STYLE2" >LP No:</td> -->
<!-- 			    <td align="left" valign="middle" >  -->
<!-- 			    	<input name="container" id="container" type="text" value=""  size="45px"/> -->
<!-- 			    </td> -->
<!-- 			</tr> -->
			<tr>
				 <td align="right" valign="middle"><b>RFID: </b></td>
			    <td align="left" valign="middle" >
			    <input name="hardwareId" id="hardwareId" type="text" value="" size="45px" style="width: 210px;height:30px;">
			    </td>
			  </tr>
			  
			  
    </table>

</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
    
	 <a name="Submit2" value="Submit" class="buttons primary big " onClick="addContainer();">Submit</a>

	  <a name="Submit2" value="Cancel" class="buttons big " onClick="closeWin();">Cancel</a>
	</td>
  </tr>
</table>
</form>	
</body>
</html>
