<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<html>
<head>
<title>打印</title>
</head>
<%
	long id=StringUtil.getLong(request,"id");
	DBRow row=clpTypeMgrZr.findContainerById(id);
	DBRow ilp=clpTypeMgrZr.findIlpTypeProductByIlpTypeId(row.get("type_id",0l));
	//DBRow product=clpTypeMgrZr.findContainerProduct(ilp.get("ibt_pc_id",0l));
	
%>
<body>
	<br/>
	<div align="center" >
		<div name="rongqi" align="left" style="width:360px; border:1px solid red;">
		 	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
			      <tr>
			        <td width="36%" rowspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;" >
			        	<span style="font-size:100px; font-family: Verdana;">
			            	<span style="font-size:100px; font-family: Verdana;">		                 
			                       I    
						    </span>
			            </span>
			        </td>
			        <td height="36"  colspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;" >
			        	<span style=" font-size: 12px;font-family: Verdana;">Date</span>
			        </td>
			        <td width="42%" align="center" style="border-bottom:2px solid black;" >
			        	<span style="font-weight: bold; font-size:13px;font-family: Verdana;">
			        		<%=new TDate().getFormateTime(DateUtil.NowStr(), "yy-MM-dd") %>
			        	</span>
			        </td>
			      </tr>
			      <tr>
			        <td height="58" colspan="3" style="border-bottom:2px solid black;" >
			      	    <div style="padding-left:8px;font-size: 12px;font-family: Verdana;">Container Type Id</div>
			            <div style="font-weight: bold;font-size:13px; height:50px; line-height:50px;font-family: Verdana;" align="center" >
			               <%=ilp.get("ibt_id",0l) %>
			            </div>				           
			        </td>
			      </tr>
			      <tr>
			        <td height="43" style="border-bottom:2px solid black;border-right:2px solid black;">
				        <div style="padding-left:8px; font-size: 12px;font-family: Verdana;">Pid</div>
				        <div style="font-weight: bold;font-size:13px;font-family: Verdana;" align="center"><%=ilp.get("ibt_pc_id",0l)%></div>			    
			        </td>
			        <td colspan="3" style="border-bottom:2px solid black;" >
				        <div style="padding-left:8px;font-size: 12px;font-family: Verdana;">Product Name</div>
				        <div style="font-weight: bold;font-size:13px;font-family: Verdana;" align="center"><%=ilp.getString("p_name") %>&nbsp;</div>
			        </td>
			      </tr>
			      <tr>
			        <td height="49" colspan="4" style="border-bottom:2px solid black;" >
			        	<div align="left" style="padding-left:8px;font-size: 12px;font-family: Verdana;">Lot</div>
			        	<div align="center" style="font-size:13px;font-weight: bold;font-family: Verdana;"><%=row.getString("lot_number") %>&nbsp;</div>
			        </td>
			      </tr>
			      <tr>
			        <td height="51" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
			        	<div style="padding-left:8px;font-size: 12px;font-family: Verdana;">Customer</div>
				        <div style="font-weight:bold;font-size:13px;font-family: Verdana;" align="center" >
				       		&nbsp;
				        </div>
			        </td>
			        <td colspan="2" style="border-bottom:2px solid black;">			 
				        <div style="padding-left:8px;font-size: 12px;font-family: Verdana;">Supplier</div>
				        <div style="font-weight:bold;font-family: Verdana;" align="center" >
				        	 <%DBRow titleRow=proprietaryMgrZyj.findProprietaryByTitleId(row.get("title_id",0l)); %>
						        <%if(titleRow!=null){ %>
									<%= titleRow.getString("title_name")%>
								<%}else{ %>
									&nbsp;
								<%} %>
				        </div>
			        </td>
			      </tr>
			      <tr>
			        <td height="50" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
			      		<div style="padding-left:8px;font-size: 12px;font-family: Verdana;">Total-Products</div>
			        	<div align="center" style="font-size:13px;font-weight: bold;font-family: Verdana;" >
			        		<%=ilp.get("ibt_total",0l) %>&nbsp;
			        	</div>
			        </td>
			        <td colspan="2" style="border-bottom:2px solid black;">
                       <div style="padding-left:8px;font-size: 12px;font-family: Verdana;">Total-Packages</div>
                       <div align="center" style="font-size:13px;font-weight: bold;font-family: Verdana;" >
					   		<%=ilp.get("ibt_total",0l) %>&nbsp;
                       </div>
			        </td>
			      </tr>
			      <tr>
			        <td height="49" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
                       <div style="padding-left:8px;font-size: 12px;font-family: Verdana;">Piece</div>
                       <div align="center" style="font-size:13px;font-weight: bold;font-family: Verdana;" >
					   		Product
                       </div>  
			        </td>
			        <td colspan="2" style="border-bottom:2px solid black;">                 
                        <div style="padding-left:8px;font-size: 12px;font-family: Verdana;">Pakcages</div>
                        <div align="center" style="font-size:13px;font-weight: bold;font-family: Verdana;" >
					   		<%=ilp.get("ibt_total_length",0l) %>&nbsp;*&nbsp;<%=ilp.get("ibt_total_width",0l) %>&nbsp;*&nbsp;<%=ilp.get("ibt_total_height",0l) %>
                        </div>

			        </td>
			      </tr>		
			      <tr>
			        <td height="83" colspan="4">
			        	<div align="center">
				        	<img src="/barbecue/barcode?data=<%=row.getString("container") %>&width=1&height=40&type=code39&drawText=true&st=true" />
				        </div>
				        <div align="center" style="font-family: Verdana;font-size: 12px;font-weight: bold;">	
				        	<%=row.getString("container") %>
				        </div>
			        </td>
			      </tr>
			      <tr>
			        <td colspan="2">&nbsp;</td>
			        <td width="13%">&nbsp;</td>
			        <td>&nbsp;</td>
			      </tr>
			</table>
	    </div>	
	</div>
</body>
</html>
