<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.key.HandStatusleKey"%>
<%@ page import="com.cwc.app.key.ProductStatusKey"%>
<%@ page import="com.cwc.app.key.ContainerTypeKey" %>
<%@ include file="../../include.jsp"%>
<html>
<head>
<title>打印</title>
</head>
<%
	long id=StringUtil.getLong(request,"id");
	DBRow row=clpTypeMgrZr.findContainerById(id);
	DBRow clp=clpTypeMgrZr.selectContainerClpById(row.get("type_id",0l)); 
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();
%>
<body>
	<br/>
	<div align="center" >
		<div id="rongqi" name="rongqi" align="left" style="width:368px; border:1px solid red">
		 	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
			      <tr>
			        <td width="36%" rowspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;" >
			        	<span style="font-size:100px; font-family: Verdana, Geneva, sans-serif;">
			            	<span style="font-size:100px; font-family: Verdana, Geneva, sans-serif;">			               
			                       C		                
						    </span>
			            </span>
			        </td>
			        <td height="36"  colspan="2" align="center" style="border-bottom:2px solid black;border-right:2px solid black;" >
			        	<span style="font-weight: bold;">Date</span>
			        </td>
			        <td width="42%" align="center" style="border-bottom:2px solid black;" >
			        	<span style="font-weight: bold; font-size:18px" id="dateShow">
			        		<%=new TDate().getFormateTime(DateUtil.NowStr(), "yy-MM-dd") %>
			        	</span>
			        </td>
			      </tr>
			      <tr>
			        <td height="58" colspan="3" style="border-bottom:2px solid black;" >
			        	<div style="padding-left:8px;font-size: 12px;">Container Type Id</div>
			            <div style="font-weight: bold;font-size:18px; height:50px; line-height:50px;" align="center" >
			                <%=clp.get("sku_lp_type_id",0l) %>
			            </div>
			        </td>
			      </tr>
			      <tr>
			        <td height="43" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
				        <div style="padding-left:8px; font-size: 12px;">Pid</div>
				        <div style="font-weight: bold;font-size:18px;" align="center"><%=clp.get("sku_lp_pc_id",0l) %></div>			    
			        </td>
			        <td colspan="2" style="border-bottom:2px solid black;" >
				        <div style="padding-left:8px;font-size: 12px;">Product Name</div>
				        <div style="font-weight: bold;font-size:18px;" align="center"><%=clp.getString("p_name") %>&nbsp;</div>
			        </td>
			      </tr>
			      <tr>
			        <td height="49" colspan="4" style="border-bottom:2px solid black;" >
			        	<div align="left" style="padding-left:8px;font-size: 12px;">Lot</div>
			        	<div align="center" style="font-size:18px;font-weight: bold;"><%=row.getString("lot_number") %>&nbsp;</div>
			        </td>
			      </tr>
			      <tr>
			        <td height="51" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
			        	<div style="padding-left:8px;font-size: 12px;">Customer</div>
				        <div style="font-weight:bold;font-size:18px;" align="center" >
				       		&nbsp;
				        </div>
			        </td>
			        <td height="51" colspan="2" style="border-bottom:2px solid black;">			 
				        <div style="padding-left:8px;font-size: 12px;">Supplier</div>
				        <div style="font-weight:bold;font-size:18px;" align="center">
					        <%DBRow titleRow=proprietaryMgrZyj.findProprietaryByTitleId(row.get("title_id",0l)); %>
							<%= null!=titleRow?titleRow.getString("title_name"):"&nbsp;"%>
				        </div>
			        </td>
			      </tr>
			      <tr>
			        <td height="50" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
			      		<div style="padding-left:8px;font-size: 12px;">Total-Products</div>
			        	<div align="center" style="font-size:18px;font-weight: bold;" >			     
	                        <%=clp.get("sku_lp_total_piece",0l) %>              		         
			        	</div>
			        </td>
			        <td colspan="2" style="border-bottom:2px solid black;">	
                     	<div style="padding-left:8px;font-size: 12px;">Total-Packages</div>
	                    <div align="center" style="font-size:18px;font-weight: bold;" >
						   	   <%=clp.get("sku_lp_total_box",0l) %>
	                    </div>	
			        </td>
			      </tr>
			      <tr>
			        <td height="49" colspan="2" style="border-bottom:2px solid black;border-right:2px solid black;">
	                        <div style="padding-left:8px;font-size: 12px;">Piece</div>
	                        <div align="center" style="font-size:18px;font-weight: bold;" >
	                        	<%if(clp.get("sku_lp_box_type",0l)==0){ %>
	                        		Product
	                        	<%}else{%>
	                        		<% DBRow blpRow=clpTypeMgrZr.selectContainerBlpById(clp.get("sku_lp_box_type",0l)); %>
	                        		BLP&nbsp;<%=blpRow.get("box_total_length",0l) %>X<%=blpRow.get("box_total_width",0l) %>X<%=blpRow.get("box_total_height",0l) %>
	                        	<%} %>
	                   		</div>	            
			        </td>
			        <td colspan="2" style="border-bottom:2px solid black;">			        
	                        <div style="padding-left:8px;font-size: 12px;">Pakcages</div>
	                        <div align="center" style="font-size:18px;font-weight: bold;" >
	                        	<%=clp.get("sku_lp_box_length",0l) %>X<%=clp.get("sku_lp_box_width",0l) %>X<%=clp.get("sku_lp_box_height",0l) %>
	                   		</div>	                
			        </td>
			      </tr>		
			      <tr>
			        <td height="83" colspan="4">
			        	<div align="center">
				        	<img src="/barbecue/barcode?data=<%=row.getString("container") %>&width=1&height=40&type=code39&drawText=true&st=true" />
				        </div>
				        <div align="center">	
				        	<%=row.getString("container") %>
				        </div>
			        </td>
			      </tr>
			      <tr>
			        <td colspan="2">&nbsp;</td>
			        <td width="13%">&nbsp;</td>
			        <td>&nbsp;</td>
			      </tr>
			</table>
	    </div>	
	</div>
</body>
</html>
