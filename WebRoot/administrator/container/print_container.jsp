<%@page import="com.cwc.app.key.ContainerTypeKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 

<html>
<head>
<title>Print Container</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
</head>
<%
	long id=StringUtil.getLong(request,"id");
	int container_type = StringUtil.getInt(request, "container_type");
%>
  
<body>
 <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="lableshow">
<!-- 	<tr>
		<td width="220" nowrap="nowrap" align="right" valign="middle" style="border-right:2px #eeeeee solid;border-bottom:2px #eeeeee solid;">
			<table align="left" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" style="padding-left: 15px;">标签名称:</td>
					<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong>容器签60X30</strong></td>
				</tr>
				<tr>
					<td align="left" style="padding-left: 15px;">纸张宽度:</td>
					<td align="left" nowrap="nowrap" style="padding-left:15px;">60</td>
				</tr>
				<tr>
					<td align="left" style="padding-left: 15px;">纸张高度:</td>
					<td align="left" nowrap="nowrap" style="padding-left:15px;">30</td>
				</tr>
			</table>
		</td>
		<td align="left" valign="middle" style="padding:10px;padding-left:25px;border-bottom:2px #eeeeee solid;">
			<a href="javascript:printPage();">
				<img src="<%=ConfigBean.getStringValue("systenFolder")+"administrator/lable_template/template_img/rongqi60X30.png"%>">
			</a>
		</td>
	</tr>
	<tr>
		<td width="220" nowrap="nowrap" align="right" valign="middle" style="border-right:2px #eeeeee solid;border-bottom:2px #eeeeee solid;">
			<table align="left" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" style="padding-left: 15px;">标签名称:</td>
					<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong>容器签80X35</strong></td>
				</tr>
				<tr>
					<td align="left" style="padding-left: 15px;">纸张宽度:</td>
					<td align="left" nowrap="nowrap" style="padding-left:15px;">80</td>
				</tr>
				<tr>
					<td align="left" style="padding-left: 15px;">纸张高度:</td>
					<td align="left" nowrap="nowrap" style="padding-left:15px;">35</td>
				</tr>
			</table>
		</td>
		<td align="left" valign="middle" style="padding:10px;padding-left:25px;border-bottom:2px #eeeeee solid;">
			<a href="javascript:printPageDa();">
				<img src="<%=ConfigBean.getStringValue("systenFolder")+"administrator/lable_template/template_img/rongqi80X35.png"%>">
			</a>
		</td>
	</tr> -->
	<tr>
		<td width="220" nowrap="nowrap" align="right" valign="middle" style="border-right:2px #eeeeee solid;border-bottom:2px #eeeeee solid;">
			<table align="left" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" style="padding-left: 15px;">Label Name:</td>
					<td align="left" nowrap="nowrap" style="padding-left:15px;"><strong>102X152</strong></td>
				</tr>
				<tr>
					<td align="left" style="padding-left: 15px;">Paper Width:</td>
					<td align="left" nowrap="nowrap" style="padding-left:15px;">102</td>
				</tr>
				<tr>
					<td align="left" style="padding-left: 15px;">Paper Heigth:</td>
					<td align="left" nowrap="nowrap" style="padding-left:15px;">152</td>
				</tr>
			</table>
		</td>
		<td align="left" valign="middle" style="padding:10px;padding-left:25px;border-bottom:2px #eeeeee solid;">
			<a href="javascript:print102X152Page();">
				<%
				String imgName = "";
				if(ContainerTypeKey.ILP==container_type)
				{
					imgName = "ilp.jpg";
				}
				else if(ContainerTypeKey.BLP==container_type)
				{
					imgName = "blp.jpg";
				}
				else if(ContainerTypeKey.CLP==container_type)
				{
					imgName = "clp.jpg";
				}%>
				<img src="<%=ConfigBean.getStringValue("systenFolder")+"administrator/lable_template/template_img/"+imgName%>">
			</a>
		</td>
	</tr>
</table>
</body>
</html>
<script>
    function printPageDa(){
    	 var id=<%=id%>;
 		 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/lable_template/template/print_container_da.html?id="+id; 
	     $.artDialog.open(uri , {title: "Print Container",width:'700px',height:'400px', lock: true,opacity: 0.3,fixed: true});
    }
	function printPage(){
		 var id=<%=id%>;
 		 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/lable_template/template/print_container_small.html?id="+id; 
	     $.artDialog.open(uri , {title: "Print Container",width:'700px',height:'400px', lock: true,opacity: 0.3,fixed: true});
	}
	function print102X152Page(){
   	     var id=<%=id%>;
   	     var uri = '';
   	     if(<%=ContainerTypeKey.ILP==container_type%>)
    	 {
    	 	uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/container_print_one_i.html?id="+id;
    	 }
   	     else if(<%=ContainerTypeKey.BLP==container_type%>)
	 	 {
	 	 	uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/container_print_one_b.html?id="+id;
	 	 }
	     else if(<%=ContainerTypeKey.CLP==container_type%>)
	 	 {
	 	 	uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/container_print_one_c.html?id="+id;
	 	 }
	     $.artDialog.open(uri , {title: "Print Container",width:'700px',height:'400px', lock: true,opacity: 0.3,fixed: true});
   }
	
</script>
