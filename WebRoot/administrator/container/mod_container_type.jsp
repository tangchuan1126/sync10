<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>

<%
	long typeId = StringUtil.getLong(request,"type_id");
	DBRow row = containerMgrZYZ.getDetailContainerType(typeId);
	ContainerTypeKey containerTypeKey = new ContainerTypeKey();

	String ModContainerTypeAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/container/ModContainerTypeAction.action";
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改容器类型</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>

function modContainerType()
{
		  
	var f = document.modContainerTypeForm;

	if(f.typeName.value == ""){
		showMessage("请填写类型名称","alert");
		return false;
	}
	else if (f.length.value == 0){
		showMessage("请填写长","alert");
		return false;
	 }
	else if (f.width.value == 0){
	 	showMessage("请填写宽","alert");
	 	return false;
	 }
	else if (f.height.value == 0){
	 	showMessage("请填写高","alert");
	 	return false;
	 }
	else if (f.weight.value == 0){
	 	showMessage("请填写容器重量","alert");
	 	return false;
	 }
	else if (f.max_load.value == 0){
	 	showMessage("请填写最大承重","alert");
	 	return false;
	 }
	else if (f.max_height.value == 0){
	 	showMessage("请填写最大高度","alert");
	 	return false;
	 }
	else{
		ajaxModContainerType();
	}
}
function ajaxModContainerType(){

	$.ajax({
		url:'<%=ModContainerTypeAction%>',
		data:$("#modContainerTypeForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   },
		success:function(data){
		   $.unblockUI();
			if(data && data.flag == "true"){
				showMessage("修改成功","succeed");
			    setTimeout("windowClose()", 1000);
			}else{
				showMessage("修改失败","error");
			}
		},
		error:function(){
		    $.unblockUI();
			showMessage("系统错误","error");
		}
	});
}

function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function closeWin()
{
	$.artDialog.close();
}
jQuery(function($){
	// 
	$("#container_type option[value="+'<%= row.get("container_type",containerTypeKey.CLP)%>'+"]").attr("selected",true);
     	
})

</script>
<style type="text/css">
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="modContainerTypeForm" id="modContainerTypeForm" method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	
    <input type="hidden"  id="typeId" name="typeId" value="<%=typeId %>"/>
<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:90%;">
  <legend style="font-size:15px;font-weight:normal;color:#999999;">修改容器类型</legend>
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
        	<tr>
			  	 <td align="right" valign="middle" class="STYLE2" >容器分类:</td>
			    <td align="left" valign="middle" > 
			    	<select name="container_type" id="container_type">
			    	<%
			    	List<String> arrayList =(ArrayList<String>)	containerTypeKey.getContainerTypeKeys();
			    	for(String i : arrayList){
			    		%>
			    			<option value='<%= i %>' ><%= containerTypeKey.getContainerTypeKeyValue(i) %></option>
			    		<%
			    	}
			    	%>
			    	
			    		
			    	</select>
 			    </td>
			</tr>
        	<tr>
			  	 <td align="right" valign="middle" class="STYLE2" >类型名称:</td>
			    <td align="left" valign="middle" > 
			    	<input name="typeName" id="typeName" type="text" value="<%=row.getString("type_name") %>"  size="45px"/>
			    </td>
			</tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE2" >长(cm):</td>
			    <td align="left" valign="middle" >
			    <input name="length" id="length" type="text" value="<%=row.get("length",0f) %>" size="35px">
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE2" >宽(cm):</td>
			    <td align="left" valign="middle" >
			    <input name="width" id="width" type="text" value="<%=row.get("width",0f) %>" size="35px">
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE2" >高(cm):</td>
			    <td align="left" valign="middle" >
			    <input name="height" id="height" type="text" value="<%=row.get("height",0f) %>" size="35px">
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE2" >体重(kg):</td>
			    <td align="left" valign="middle" >
			    <input name="weight" id="weight" type="text" value="<%=row.get("weight",0f) %>" size="35px">
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE2" >最大承重(kg):</td>
			    <td align="left" valign="middle" >
			    <input name="max_load" id="max_load" type="text" value="<%=row.get("max_load",0f) %>" size="35px">
			    </td>
			  </tr>
			<tr>
				 <td align="right" valign="middle" class="STYLE2" >最大高度(cm):</td>
			    <td align="left" valign="middle" >
			    <input name="max_height" id="max_height" type="text" value="<%=row.get("max_load",0f) %>" size="35px">
			    </td>
			  </tr>  
			 
    </table>
 </fieldset>
</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存修改" class="normal-green" onClick="modContainerType();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table>
</form>	
</body>
</html>
