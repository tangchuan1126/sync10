<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.List,com.cwc.app.key.ContainerTypeKey,java.util.ArrayList" %>
<%

String cmd = StringUtil.getString(request,"cmd");
int p = StringUtil.getInt(request,"p");

PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(30);
int container_type =  StringUtil.getInt(request,"container_type", ContainerTypeKey.TLP);
String search_key = StringUtil.getString(request,"search_key");
long typeId = StringUtil.getLong(request,"typeId");
//System.out.print("container_type:"+container_type+",search_key:"+search_key+",typeId:"+typeId);
//查询所有的LP
DBRow[] containerList = containerMgrZyj.findContainerByTypeContainerTypeName(search_key,typeId,container_type,pc);

//查询所有的Packaging Type
DBRow[] containerTypeRow = containerMgrZYZ.getAllContainerType(null, "");

DBRow[] clpConType = containerMgrZYZ.getClpContainerType();
DBRow[] tlpConType = containerMgrZYZ.getTlpContainerType();

ContainerTypeKey containerTypeKey = new ContainerTypeKey();

String DeleteContainerAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/container/DeleteContainerAction.action";
String ExportContainerAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/container/ExportContainer.action";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Manage LP</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script> 

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css"; 
</style>
<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/simple.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<style type="text/css">
 .searchbarGray{
        color:#c4c4c4;font-family: Arial
   }
   .content{padding:10px;}
   
   .ui-widget-content{
	border:1px #CFCFCF solid;
	border-radius: 0px;
  	-webkit-border-radius: 0px;
	-moz-border-radius:0;
}
.ui-widget-header{
	background:#f1f1f1 !important;
	border-radius: 0;
	border-top:0;
	border-left:0;
	border-right:0;
	border-bottom: 1px #CFCFCF solid;
}
.ui-tabs .ui-tabs-nav li a{
	padding: .5em 1em 13px !important;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
	height: 40px;
	margin-top: -4px !important;
	background:#fff;
}
.ui-tabs .ui-tabs-nav{
	padding :0 .2em 0;
}
.ui-tabs{
	padding:0;
}
   
.panel {
	margin-top:23px;
	margin-bottom: 20px;
	background-color: #fff;
	border: 1px #CFCFCF solid;
	-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
	box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.panel .panelTitle{
	background: #f1f1f1;
	height: 40px;
	line-height: 40px;
	padding:0 15px;
}
.panel .panelTitle .title-left{
	float: left;
}
.panel .panelTitle .title-right{
	float: right;
}
.clear{
	clear: both;
}
.right-title{
	height:40px;
	background-color:#fff;
}
.zebraTable th.right-title,.zebraTable th.left-title{background-color:#fff;}
.zebraTable th.right-title{border-left:0;border-right:0;}
.zebraTable th.left-title{border-right:0;}
	
.aui_titleBar{  

	border: 1px #DDDDDD solid;
}

.aui_header{

	height: 40px;
	line-height: 40px;
}

.aui_title{
	
	height: 40px;
	width: 100%;
	font-size: 1.2em !important;
	margin-left: 20px;
	font-weight: 700;
	text-indent: 0;
}

.aui_close{
	color:#fff !important;
	text-decoration:none !important;
}
div.chzn-container{
	vertical-align: middle;
}

.breadnav {
            padding:0 10px; height:25px;margin-bottom: 16px;
        } 
        .breadcrumb{
          font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
          font-size: 14px;
          line-height: 1.42857143;
        }
        .breadcrumb {
          margin-top:0;
	  padding: 8px 15px;
          margin-bottom: 20px;
          list-style: none;
          background-color: #f5f5f5;
          border-radius: 4px;
        }

        .breadcrumb > li {
          display: inline-block;
        }

        .breadcrumb > .active {
          color: #777;
        }

        .breadcrumb > li + li:before {
            padding: 0 5px;
            color: #ccc;
            content: "/\00a0";
        }
        .breadcrumb a{
		      color: #337ab7;
                text-decoration: none;	
		}
</style>
<script>

$(function(){
	$(".chzn-select").chosen({no_results_text: "no this options:"});
	$("#container_type").chosen();
	//$("#typeId").chosen();
});
$.blockUI.defaults = {
		
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '230px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">

//修改容器托盘信息
function modContainer(containerId){
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/mod_container.html?containerId="+containerId; 
    $.artDialog.open(uri , {title: "修改容器信息",width:'520px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}

//添加容器托盘信息
function addContainer(){
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/add_container.html"; 
    $.artDialog.open(uri , {title: "Add TLP",width:'520px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
//导入
function importContainers(){
	 var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/container_upload_excel.html"; 
	   $.artDialog.open(uri , {title: "上传容器信息",width:'1000px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}
//删除 提示
function deleteContainer(containerId,container){
	if(containerId!=0 ){
		$.artDialog.confirm('Are you sure to delete '+container+'?', function(){
			ajaxDeleteContainer(containerId);
		}, function(){
		});
	}
}
//删除
function ajaxDeleteContainer(containerId){
	$.ajax({
		url:'<%=DeleteContainerAction%>',
		data:'containerId='+containerId,
		dataType:'json',
		type:'post',
		beforeSend:function(){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
			$.unblockUI();
			if(data && data.flag == "true"){
				refreshWindow();
			}else{
				showMessage("System error","alert");
			}
		},
		error:function(){
			showMessage("System error","error");
			$.unblockUI();
		}
	});
}

function refreshWindow(){
	window.location.reload();
}

function exportContainer(){
	
	var	para = "search_key="+$("#search_key").val()+"&type_id="+$("#type_id").val()+"&container_type="+$("#container_type").val()
	
	$.ajax({
		url:'<%=ExportContainerAction%>',
		type:'post',
		dataType:'json',
		timeout: 60000,
		cache:false,
		data:para,
		beforeSend:function(request){
			$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">Exporting......</div>'});
		},
		error:function(){
			showMessage("System error, try it again...","error");
		},
		success:function(msg){
		   
			if(msg && msg.flag == "true"){
				document.export_lp.action=msg["fileurl"];
				document.export_lp.submit();
				
				$.unblockUI();
			}else{
				showMessage("No Data","alert");
				$.unblockUI();
			}
		}
	});
}

//文件导入
function importContainer(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"xls",
	     limitSize:2,
	     target:_target,
	     limitNum:1
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: 'Upload Files',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames){
	if($.trim(fileNames).length > 0 ){
		
		$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">检查文件内容是否有错误<br/>请稍后......</span>'});
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/container/check_container_show.html?fileNames="+fileNames;
		$.artDialog.open(uri,{title: "检测上传容器信息",width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true});
		$.unblockUI();
	}
	
}

//修改盒子
function updateContainer(con_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_container_update.html?con_id="+con_id;
	$.artDialog.open(uri,{title: "修改盒子",width:'500px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}
//修改CLP容器
function updateClp(con_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_container_update.html?con_id="+con_id;
	$.artDialog.open(uri,{title: "修改CLP托盘",width:'650px',height:'420px', lock: true,opacity: 0.3,fixed: true});
}

function advancedSearch(){
	$("#search_form").submit();
}

function outClpSelect(id,selected){
	
	var option = '<option value="0">Packaging Type...</option>';
	
	<%for(int i = 0;i < clpConType.length ;i++){%>
	
	option += '<option style="background:#fffffffff;" value="<%=clpConType[i].get("type_id",0l)%>">';
	option += '<%= clpConType[i].getString("type_name") %></option>';
	
	<%} %>
	
	$("#"+id).html(option);
	
	$("#"+id).val(selected);
}

function outTlpSelect(id,selected){
	
	var option = '<option value="0">Packaging Type...</option>';
	
	<%for(int i = 0;i < tlpConType.length ;i++){%>
	
	option += '<option style="background:#fffffffff;" value="<%=tlpConType[i].get("type_id",0l)%>">';
	option += '<%= tlpConType[i].getString("type_name") %></option>';
	
	<%} %>
	
	$("#"+id).html(option);
	
	$("#"+id).val(selected);
}

function outallSelect(id,selected){
	
	var option = '<option value="0">Packaging Type...</option>';
	
	<%for(int i = 0;i < containerTypeRow.length ;i++){%>
	
	option += '<option style="background:#fffffffff;" value="<%=containerTypeRow[i].get("type_id",0l)%>">';
	option += '<%=containerTypeRow[i].getString("type_name") %></option>';
	
	<%} %>
	
	$("#"+id).html(option);
	
	$("#"+id).val(selected);
}

function changePackType(target){
	
	changePackType2(target);
	advancedSearch();
}

function changePackType2(target){
	
	var flag = $(target).val();
	
	var id = "typeId";
	
	var selected = "<%=typeId%>";
	
	if(flag == '1'){
		
		outClpSelect(id,selected);
		
	}else if(flag == '3'){
		
		outTlpSelect(id,selected);
		
	}else{
		
		outallSelect(id,selected);
	}
	$("#typeId").chosen().trigger("liszt:updated");
	
}

function changepacking(target){
	advancedSearch();
}

jQuery(function($){
	//$("#container_type").trigger("change");
	changePackType2($("#container_type"));
	
});
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">

<!-- <div style="border-bottom:1px solid #CFCFCF; height:25px;padding-top: 15px;margin-left: 10px;margin-bottom: 4px;">
	<img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">
	<span style="font-size: 13px;font-weight: bold"> Setup » Manage LP</span>
</div> 

<div class="breadnav">
<ol class="breadcrumb">
            <li><a href="#">Setup</a></li>
            <li class="active">Manage LP</li>
        </ol>
</div>-->

<div class="content">
	<div id="tabs">
		<ul>
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
				<a href="#av1">Advanced Search</a>
			</li>	 
		</ul>
		<div id="av1">
			<table width="100%">
				<tr>
					<td>
						<form name="export_lp" id="export_lp" method="post"></form>
						<form action="" name="search_form" id="search_form" method="post"> 
					   	<table width="100%" style="padding-left: 10px;">
					     <tr>
							<td>
LP : <input style="width: 210px;height:30px;" name="search_key" type="text" id="search_key"  value="<%=search_key %>" onkeypress="if(event.keyCode==13) {advancedSearch();}">
&nbsp;&nbsp; 
LP Category : 
<select name="container_type" style="height:30px;width:90px;" id="container_type" onchange="changePackType(this);">
	
	<option value="-1">Select...</option>
	<%List<String> arrayList = (ArrayList<String>)containerTypeKey.getContainerTypeKeys();
   		for(String i : arrayList){
   			
   			String selectStr = (i.equals(container_type+""))?"selected=selected":"";
   			if(ContainerTypeKey.Original != Integer.parseInt(i)) {
   	%>
   		<option value='<%= i %>' <%=(container_type ==Integer.parseInt(i) ? "selected" : "") %>>
   			<%= containerTypeKey.getContainerTypeKeyValue(i) %>
   		</option>
   	<%
   			}
   		}
   	%>
</select>
								
&nbsp;&nbsp;&nbsp;
Packaging Type : 
								
<select style="height:30px;width:155px;" name="typeId" id="typeId" onchange="changepacking(this);">
</select>
&nbsp;&nbsp;   
<input type="hidden" name="cmd" value="search">
<a name="Submit21" type="submit" class="buttons primary big" value="Search" onclick="advancedSearch()" ><i class="icon-search"></i>&nbsp;Search</a>
							</td>
							<td>
								<div id="jquery_file_up">
					              			<input type="hidden" id="file_names" name="file_names" value=""/>				 	
					            </div>
							</td>
		 			  </tr>
					</table>
				</form>
					</td>
				</tr>
			</table>
		
		
		</div>
	</div>
  
  	<script type="text/javascript">
	  	$("#tabs").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
		});
	    $("#tabs").tabs("select",0);  //默认选中第一个
	</script>
	
	
	
	<div class="panel">
		<div class="panelTitle"><div class="title-right">	
		<div class="buttons-group minor-group" style="margin-top:5px;">					
		<a name="Submit3" class="buttons" value="Add TLP" onClick="addContainer();"><i class="icon-plus"></i>&nbsp;Add TLP</a>
		<!-- <a name="import/import" class="buttons" value="Import TLP" onClick="importContainer('jquery_file_up');"><i class="icon-share"></i>&nbsp;Import TLP</a> -->
		<!-- <a name="import/export" class="buttons" value="Export TLP" onClick="exportContainer();"><i class="icon-reply"></i>&nbsp;Export TLP</a>  -->
		</div> 
						</div>
		</div>
	
	
	
	
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
    <tr> 
    	<th width="10%" class="right-title" style="vertical-align: center;text-align: center;">LP Category</th>
        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">LP</th>
        <th width="40%" style="vertical-align: center;text-align: center;" class="left-title">Packaging Type</th>
        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">RFID</th>
        <th width="" style="vertical-align: center;text-align: center;" class="left-title">Operation</th>
    </tr>
	  <%
	  	if(containerList!=null && containerList.length > 0 ){
	  		for(int i = 0;i < containerList.length; i++){
	  %>
    <tr>
		<td align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%= containerTypeKey.getContainerTypeKeyValue(containerList[i].get("container_type",0)) %>&nbsp;</td>
		<td align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=containerList[i].getString("container") %>&nbsp;</td>
		<td height="60"   valign="middle" style='word-break:break-all;font-weight:bold;text-indent: 10px;'>
     		<%-- <%
     			if(containerList[i].get("container_type",0) == containerTypeKey.CLP){
     				%>
     					<%= clpTypeMgrZr.getClpTypeNameNeedPName(containerList[i].get("type_id",0l)) %>
     				<% 
     			}
     		%> --%>
     		<%= containerList[i].getString("type_name") %>
      </td>
      <td align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=containerList[i].getString("hardwareId") %>&nbsp;</td>
      <td   height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
<%--       <input name="Submit2" type="button" class="short-short-button-del" value="Del" onClick="deleteContainer(<%=containerList[i].get("con_id",0l) %>,'<%=containerList[i].getString("container") %>');">--%>
       &nbsp;
       <a value="  Print Label"  class="buttons" onclick="printContainer(<%=containerList[i].get("con_id",0l) %>,<%=containerList[i].get("container_type",0)%>,<%=containerList[i].get("type_id",0)%>,<%=containerList[i].get("pc_id",0)%>)"><i class="icon-print"></i>&nbsp;Print Label</a> 
      </td>
    </tr> 
    <%}
  	}else{%>
  		<tr>
		     <td colspan="5" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Data</td>  			
		</tr>
  	<%} %>
    </table>
    <br/>
    <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" action="container_list.html" method="post">
	          <input type="hidden" name="p">
	          <input type="hidden" name="cmd" value="<%=cmd %>"/>
	          <input type="hidden" name="search_key" value="<%=search_key %>" />
	          <input type="hidden" name="typeId" value="<%=typeId %>" />
	          <input type="hidden" name="container_type" value="<%=container_type %>" />
	  </form>
	        <tr> 
	          
	    <td height="28" align="right" valign="middle"> 
	      <%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
	      Goto
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
	    </td>
	        </tr>
</table>
	</div>
</div>
  </body>
</html>
<script>
	function printContainer(id,type,clp_type_id,pc_id){
		if(type==<%=containerTypeKey.CLP%>){
			 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' +  "administrator/lable_template/template/print_container_c.html?id="+id + "&clp_type_id=" + clp_type_id + "&pc_id=" + pc_id; 
		     $.artDialog.open(uri , {title: "Print CLP"+id,width:'810px',height:'400px', lock: true,opacity: 0.3,fixed: true});
		}
<%-- 		else if(type==<%=containerTypeKey.BLP%>){ --%>
<%-- 			 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' +  "administrator/lable_template/template/print_container_b.html?id="+id;  --%>
// 		     $.artDialog.open(uri , {title: "Print BLP",width:'700px',height:'400px', lock: true,opacity: 0.3,fixed: true});
// 		}//去掉ILP和BLP
		else if(type==<%=containerTypeKey.TLP%>){
			 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' +  "administrator/lable_template/template/print_container_t.html?id="+id; 
		     $.artDialog.open(uri , {title: "Print TLP"+id,width:'700px',height:'400px', lock: true,opacity: 0.3,fixed: true});
		}
<%-- 		else if(type==<%=containerTypeKey.ILP%>){ --%>
<%-- 			 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' +  "administrator/lable_template/template/print_container_i.html?id="+id;  --%>
// 		     $.artDialog.open(uri , {title: "Print ILP",width:'700px',height:'400px', lock: true,opacity: 0.3,fixed: true});
// 		}//去掉ILP和BLP
		 
	}
</script>