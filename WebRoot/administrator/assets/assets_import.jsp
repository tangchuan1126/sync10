<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long acid = StringUtil.getLong(request,"acid");
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
long lid = StringUtil.getLong(request,"lid");
int state = StringUtil.getInt(request,"state",-1);
String type = StringUtil.getString(request,"type");
long productLine = StringUtil.getLong(request,"productLine");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(20);


DBRow rows[];
if (cmd.equals("filter"))
{
	rows = assetsMgr.getAssetsByCategoryIdAndLocationId(state,lid,acid,pc,productLine,type);
}
else if (cmd.equals("search"))
{
	rows = assetsMgr.getSearchAssets(key,pc,type);
}
else
{
	rows = assetsMgr.AssetsList(pc,type);
}	
Tree tree = new Tree(ConfigBean.getStringValue("assets_category"));
Tree locaton_tree = new Tree(ConfigBean.getStringValue("office_location"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

		
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript">
function exportAssets()
{
	var category_id= $("#filter_acid").val();
	var para = "category_id="+category_id;
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/exportAssets.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){
					if(date["canexport"]=="true")
					{
						document.export_assets.action=date["fileurl"];
						document.export_assets.submit();
					}
					else
					{
						alert("该分类与子类下无所选形式资产，无法导出");
					}
				}
			});
}


function filter()
{
		document.filter_search_form.cmd.value = "filter";		
		document.filter_search_form.acid.value = $("#filter_acid").val();
		document.filter_search_form.lid.value = $("#filter_lid").val();
		document.filter_search_form.submit();
}

function search()
{
	if ($("#search_key").val()=="")
	{
		alert("请填写关键字");
		return(false);
	}
	else
	{
		document.filter_search_form.cmd.value = "search";		
		document.filter_search_form.key.value = $("#search_key").val();		
		document.filter_search_form.submit();
	}
}


function closeWin()
{
	tb_remove();
}

function openCatalogMenu()
{
	$("#category").mcDropdown("#categorymenu").openMenu();
}
function closeWinRefresh()
{
	window.location.reload();
	tb_remove();
}
function importAssets()
{
	tb_show('上传资产信息','assets_upload_excel.html?TB_iframe=true&height=500&width=1000',false);
}

</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 资产管理 »   导入/导出资产</td>
  </tr>
</table>
<br>

<form name="filter_search_form" method="get"  action="assets_import.html">
<input type="hidden" name="cmd" >
<input type="hidden" name="acid" >
<input type="hidden" name="key" >
<input type="hidden" name="lid">

</form>

<form name="export_assets" method="post"></form>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="65%">
	
	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
	
	<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
	<ul id="categorymenu" class="mcdropdown_menu">
	  <li rel="0">所有分类</li>
	  <%
	  DBRow c1[] = assetsCategoryMgr.getAssetsCategoryChildren(0);
	  for (int i=0; i<c1.length; i++)
	  {
			out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("chName"));

			  DBRow c2[] = assetsCategoryMgr.getAssetsCategoryChildren(c1[i].get("id",0l));
			  if (c2.length>0)
			  {
			  		out.println("<ul>");	
			  }
			  for (int ii=0; ii<c2.length; ii++)
			  {
					out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("chName"));		
					
						DBRow c3[] = assetsCategoryMgr.getAssetsCategoryChildren(c2[ii].get("id",0l));
						  if (c3.length>0)
						  {
								out.println("<ul>");	
						  }
							for (int iii=0; iii<c3.length; iii++)
							{
									out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("chName"));
									out.println("</li>");
							}
						  if (c3.length>0)
						  {
								out.println("</ul>");
						  }
						  
					out.println("</li>");				
			  }
			  if (c2.length>0)
			  {
			  		out.println("</ul>");	
			  }
			  
			out.println("</li>");
	  }
	  %>
</ul>
	  <input type="text" name="category" id="category" value="" />
	  <input type="hidden" name="filter_acid" id="filter_acid" value="0"  />
	  

<script>
$("#category").mcDropdown("#categorymenu",{
		allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_acid").val(id);
				}

});

<%
if (acid>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=acid%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 

</script>	</td>
    <td width="434" height="29" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">&nbsp;&nbsp;
      <input name="Submit2" type="button" class="button_long_refresh" value="过滤" onClick="filter()">&nbsp;&nbsp;&nbsp;&nbsp;<input name="export" type="button" class="long-button" id="export" onClick="exportAssets()" value="导出资产"></td></tr>
  <tr>
  
  <td width="434" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
		<ul id="locationmenu" class="mcdropdown_menu">
		  <li rel="0">所有办公地点</li>
		  <%
		  DBRow l1[] = officeLocationMgr.getChildOfficeLocation(0);
		  for (int i=0; i<l1.length; i++)
		  {
				out.println("<li rel='"+l1[i].get("id",0l)+"'> "+l1[i].getString("office_name"));
	
				  DBRow l2[] = officeLocationMgr.getChildOfficeLocation(l1[i].get("id",0l));
				  if (l2.length>0)
				  {
						out.println("<ul>");	
				  }
				  for (int ii=0; ii<l2.length; ii++)
				  {
						out.println("<li rel='"+l2[ii].get("id",0l)+"'> "+l2[ii].getString("office_name"));		
						
							DBRow l3[] = officeLocationMgr.getChildOfficeLocation(l2[ii].get("id",0l));
							  if (l3.length>0)
							  {
									out.println("<ul>");	
							  }
								for (int iii=0; iii<l3.length; iii++)
								{
										out.println("<li rel='"+l3[iii].get("id",0l)+"'> "+l3[iii].getString("office_name"));
										out.println("</li>");
								}
							  if (l3.length>0)
							  {
									out.println("</ul>");
							  }
							  
						out.println("</li>");				
				  }
				  if (l2.length>0)
				  {
						out.println("</ul>");	
				  }
				  
				out.println("</li>");
		  }
		  %>
	</ul>
		<input name="location" id="location" type="text" value=""/>
		<input type="hidden" name="filter_lid" id="filter_lid" value="0" />
		
		<script>
			$("#location").mcDropdown("#locationmenu",{
					allowParentSelect:true,
					  select: 
					  
							function (id,name)
							{
								$("#filter_lid").val(id);
							}
			
			});
			
			<%
			if (lid>0)
			{
			%>
			$("#location").mcDropdown("#locationmenu").setValue(<%=lid%>);
			<%
			}
			else
			{
			%>
			$("#location").mcDropdown("#locationmenu").setValue(0);
			<%
			}
			%> 
			
		</script>
	</td>
  
    <td height="29" bgcolor="#eeeeee">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="border-bottom:1px #dddddd solid">
      <input name="import" type="button" class="long-button" id="import" onClick="importAssets()" value="导入资产">
    </span></td>
  </tr>
</table>
	
	</div>
	
	
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br>

<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	    <tr> 
			<th width="134" class="right-title">资产序列号</th>
	        <th width="297"  class="left-title" style="vertical-align: center;text-align: center;">名称</th>
	        <th width="203"  class="right-title" style="vertical-align: center;text-align: center;">条码</th>
	        <th width="176" align="center"  class="right-title" style="vertical-align: center;text-align: center;">类别</th>
	        <th width="139" align="center"  class="right-title" style="vertical-align: center;text-align: center;">单位</th>
	        <th width="122"  class="right-title" style="vertical-align: center;text-align: center;">规格</th>
	        <th width="142"  class="right-title" style="vertical-align: center;text-align: center;">单价</th>
	        <th width="134"  class="right-title" style="vertical-align: center;text-align: center;">采购日期</th>
	        <th width="165"  class="right-title" style="vertical-align: center;text-align: center;">供应商</th>
	        <th width="165"  class="right-title" style="vertical-align: center;text-align: center;">存放地点</th>
	        <th width="165"  class="right-title" style="vertical-align: center;text-align: center;">领用人</th>
			<th width="165"  class="right-title" style="vertical-align: center;text-align: center;">状态</th>
	    </tr>
	
	    <%
	for ( int i=0; i<rows.length; i++ )
	{
	
	%>
	    <tr  > 
		   <td style='word-break:break-all;' align="center">
		  	<%=rows[i].getString("aid") %>
		  </td>
	      <td height="80" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
	
	      <%=rows[i].getString("a_name")%>
		 </td>
	      <td align="center" valign="middle" style='word-break:break-all;' >      	
	      	<%=rows[i].getString("a_barcode")%>
	      	</td>
	      <td align="left" valign="middle" style="line-height:20px;">
		  <span style="color:#999999">
		  	 <%
			  DBRow allFather[] = tree.getAllFather(rows[i].get("category_id",0l));
			  for (int jj=0; jj<allFather.length-1; jj++)
			  {
			  	out.println("<a class='nine4' href='?cmd=filter&acid="+allFather[jj].getString("id")+"'>"+allFather[jj].getString("chName")+"</a><br>");
			
			  }
			  %>
		  </span>
		  
		  <%
		   DBRow catalog = assetsCategoryMgr.getDetailAssetsCategory(StringUtil.getLong(rows[i].getString("category_id")));
		  if (catalog!=null)
		  {
		  	out.println("<a href='?cmd=filter&acid="+rows[i].getString("category_id")+"'>"+catalog.getString("chName")+"</a>");
		  }
		  %>
		  </span>
		  
		  </td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("unit_name")%>&nbsp;</td>
	      <td align="center" valign="middle"   ><%=rows[i].getString("standard")%>&nbsp;</td>
	      <td align="center" valign="middle"   ><%=rows[i].get("unit_price",0d)%></td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("purchase_date")%>&nbsp;</td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("suppliers")%>&nbsp;</td>
	      <td align="center" valign="middle"  >
	      	<span style="color:#999999">
		  	 <%
			  DBRow allLocationFather[] = locaton_tree.getAllFather(rows[i].get("location_id",0l));
			  for (int k=0; k<allLocationFather.length-1; k++)
			  {
			  	out.println("<a class='nine4' href='?cmd=filter&lid="+allLocationFather[k].getString("id")+"'>"+allLocationFather[k].getString("office_name")+"</a><br>");
			
			  }
			  %>
		  </span>
		  
		  <%
		   DBRow location = officeLocationMgr.getDetailOfficeLocation(StringUtil.getLong(rows[i].getString("location_id")));
		  if (location!=null)
		  {
		  	out.println("<a href='?cmd=filter&lid="+rows[i].getString("location_id")+"'>"+location.getString("office_name")+"</a>");
		  }
		  %>
		  </span>
		  &nbsp;
	      </td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("requisitioned")%>&nbsp;</td>
	      <td align="center" valign="middle"  >
	      <% 
	      	if(rows[i].get("state",0) == 0){
	      		out.println("报废");
	      	}else if(rows[i].get("state",0) == 1){
	      		out.println("损坏");
	      	}else{
	      		out.println("良好");
	      	}
	      	%>
	      </td>
	    </tr>
	    <%
	}
	%>
	  
	</table>
</form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="assets_import.html">
    <input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="acid" value="<%=acid%>">
<input type="hidden" name="key" value="<%=key%>">
<input type="hidden" name="lid" value="<%=lid %>">

  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
