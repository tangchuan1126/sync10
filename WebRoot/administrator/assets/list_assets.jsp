<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.api.ll.AssetsAPI"%>
<%@page import="com.cwc.app.iface.ll.AssetsIFace"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
 	com.cwc.app.iface.ll.AssetsIFace assetsAPI = (com.cwc.app.iface.ll.AssetsIFace)MvcUtil.getBeanFromContainer("assetsAPI");

 long category_id = StringUtil.getLong(request,"filter.category_id");
 String cmd = StringUtil.getString(request,"cmd");
 String key = StringUtil.getString(request,"search.key");
 long location_id = StringUtil.getLong(request,"filter.location_id");
 int state = StringUtil.getInt(request,"filter.state",-1);
 String type = "1";

 PageCtrl pc = new PageCtrl();
 pc.setPageNo(StringUtil.getInt(request,"p"));
 pc.setPageSize(30);

 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

 DBRow rows[];

 rows = cmd.equals("filter")||cmd.equals("")?assetsAPI.filter(request,pc):assetsAPI.search(request,pc);

 Tree tree = new Tree(ConfigBean.getStringValue("assets_category"));
 Tree locaton_tree = new Tree(ConfigBean.getStringValue("office_location"));
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">



<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

		
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script language="javascript">

$().ready(function() {

	$("#state").val("<%=state%>");
	function log(event, data, formatted) {
		
	}

	$("#search_key").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/getSearchAssetsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		selectFirst: false,
		updownSelect:false,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},
		
				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].a_name,
    				value:data[i].a_name+"["+data[i].a_barcode+"]",
        			result:data[i].a_name
				};    
			  }   
			
		  	 return rows;   
			 },
		
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	$("#search_key").keydown(function(event){
			if(event.keyCode == 13)
			{
				search();
			}
		})
});



function filter()
{
		$('input[name=cmd]').val("filter");
		$('input[name=filter.category_id]').val($("#category_id").val());
		$('input[name=filter.location_id]').val($("#location_id").val());
		$('input[name=filter.state]').val($("#state").val());
		document.filter_search_form.submit();
}

function search()
{
	if (checkForm()){
		$('input[name=cmd]').val("search");	
		$('input[name=search.key]').val($("#search_key").val());	
			
		document.filter_search_form.submit();
	}
}

function modAssets(aid)
{
	tb_show('修改资产信息','mod_assets.html?aid='+aid+'&TB_iframe=true&height=350&width=700',false);

}

function modRecipients(aid)
{
	tb_show('修改资产领用人信息','mod_recipients_assets.html?aid='+aid+'&TB_iframe=true&height=350&width=700',false);
}

function delAssets(name,aid)
{
	if (confirm("确定删除 "+name+"？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/delAssets.action";
		document.del_form.a_id.value = aid;
		document.del_form.submit();	
	}
}

function switchCheckboxAction()
{
	$('input[name=switchCheckboxPid]').inverse();
}

function getAllChecked()
{
	return($('#switchCheckboxPid').checkbox());
}

function batchMod()
{
	var aids = getAllChecked();
	
	if ( aids=="" )
	{
		alert("最少选择一个资产信息");
	}
	else
	{
		tb_show('批量修改资产信息','batch_mod_assets.html?aids='+aids+'&TB_iframe=true&height=600&width=800',false);
	}
	
}

function addAssets()
{
	var id = $("#filter_acid").val();
	if (id==0)
	{
		alert("请选择一个资产类别");
		openCatalogMenu();
	}
	else
	{
		tb_show('增加资产','add_assets.html?category_id='+id+'&TB_iframe=true&height=350&width=700',false);
	}
}


function closeWin()
{
	tb_remove();
}

function closeWinNotRefresh()
{
	tb_remove();
}

function closeWinRefresh()
{
	window.location.reload();
	tb_remove();
}
function openCatalogMenu()
{
	$("#category").mcDropdown("#categorymenu").openMenu();
}

function print(aid)
{
	document.print_form.aid.value = aid; 
	document.print_form.submit();
}

function checkForm()
{
      if($("#search_key").val().trim()==""||$("#search_key").val().trim()=="*产资名称,条码,领用人"||$("#search_key").val()==null)
      {
         alert("请输入查询信息！");
         $("#search_key").focus();
         return false;
      }
      
      return true;
}

function changeColorBlack(){

	if($("#search_key").val()=="*产资名称,条码,领用人")
	{
 	 	 
 	 	 $("#search_key").val("");
 	 	 $("#search_key").removeAttr("class");
 	}
 	
}

function changeColorGray()
{
	 
	 if($("#search_key").val().trim()=="")	  
	 {
		  $("#search_key").val("*产资名称,条码,领用人");
		  $("#search_key").attr("class","searchbarGray");
	 }
}
  
function init()
{
 	if($("#search_key").val()=="*产资名称,条码,领用人")
	{	 	 	 	 		
	 	 $("#search_key").attr("class","searchbarGray");
	}
	else
	{		
		 $("#search_key").removeAttr("class");
	}
 
}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>

<!-- add -->

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 固定资产  »    资产清单</td>
  </tr>
</table>
<br>

 
<form name="del_form" method="post">
<input type="hidden" name="a_id">
</form>

<form name="mod_form" method="post">
<input type="hidden" name="a_id">
<input type="hidden" name="a_name">
<input type="hidden" name="category_id">
<input type="hidden" name="product_line_id">
<input type="hidden" name="a_barcode">

<input type="hidden" name="unit_name">
<input type="hidden" name="unit_price">
<input type="hidden" name="standard">
<input type="hidden" name="purchase_date">
<input type="hidden" name="suppliers">
<input type="hidden" name="location">
<input type="hidden" name="requisitioned_id">
<input type="hidden" name="requisitioned">
<input type="hidden" name="state">

</form>


<form name="filter_search_form" method="get"  action="list_assets.html">
<input type="hidden" name="cmd" >
<input type="hidden" name="filter.category_id">
<input type="hidden" name="search.key">
<input type="hidden" name="filter.location_id"> 
<input type="hidden" name="filter.state" />
</form>

<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%">

	<div class="demo" width="98%">

	<div id="tabs"> 
		<ul>
				<li><a href="#usual_tools">常用工具</a></li>
				<li><a href="#advan_search">高级搜索</a></li>		 
		</ul>
		
		<div id="usual_tools">
			<%
				String keyValue = key.equals("")?"*产资名称,条码,领用人":key;
			%>
			<input type="text" name="search_key" id="search_key"  onblur="changeColorGray()" onclick="changeColorBlack()" value="<%=keyValue %>" style="width:200px;" >
	        &nbsp;&nbsp;&nbsp;
			<input name="Submit4" type="button" class="button_long_search" value="搜索" onClick="search()" > 
			&nbsp;&nbsp; 
			<input type="button" class="long-long-button-add" value="申请固定资产"  onclick="tb_show('申请固定资产资金','../financial_management/add_assets.html?TB_iframe=true&height=370&width=700',false);"/>
		</div>
	
		<div id="advan_search">
			
	<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td width="423" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
	    <ul id="categorymenu" class="mcdropdown_menu">
		  <li rel="">所有分类</li>
		  <%
		  	if(1==1){
		      DBRow ca1[] = assetsCategoryMgr.getAssetsCategoryChildren(0,type);
			  for (int i=0; i<ca1.length; i++)
			  {
					out.println("<li rel='"+ca1[i].get("id",0l)+"'> "+ca1[i].getString("chName"));
					  DBRow[] ca2 = null;
					  	ca2 = assetsCategoryMgr.getAssetsCategoryChildren(ca1[i].get("id",0l),"1");
					  if (ca2.length>0)
					  {
					  		out.println("<ul>");	
					  }
					  for (int ii=0; ii<ca2.length; ii++)
					  {
							out.println("<li rel='"+ca2[ii].get("id",0l)+"'> "+ca2[ii].getString("chName"));		
								
								DBRow ca3[] = assetsCategoryMgr.getAssetsCategoryChildren(ca2[ii].get("id",0l));
								  if (ca3.length>0)
								  {
										out.println("<ul>");	
								  }
									for (int iii=0; iii<ca3.length; iii++)
									{
											out.println("<li rel='"+ca3[iii].get("id",0l)+"'> "+ca3[iii].getString("chName"));
											out.println("</li>");
									}
								  if (ca3.length>0)
								  {
										out.println("</ul>");
								  }
								
							out.println("</li>");				
					  }
					  if (ca2.length>0)
					  {
					  		out.println("</ul>");	
					  }
					  
					out.println("</li>");
			  }
			}
		  %>
		</ul>
	  
	  <input type="text" name="category" id="category" value="" />
	  <input type="hidden" name="category_id" id="category_id" value=""  />
	  
		<script>
		  $("#category").mcDropdown("#categorymenu",{
				allowParentSelect:true,
				  select: 
				  
						function (id,name)
						{
							$("#category_id").val(id);
						}
		});
		
		<%
		if (category_id>0)
		{
		%>
		$("#category").mcDropdown("#categorymenu").setValue(<%=category_id%>);
		<%
		}
		else
		{
		%>
		$("#category").mcDropdown("#categorymenu").setValue('');
		<%
		}
		%> 
		
		</script>	
	</td>
    <td width="434" height="29" bgcolor="#eeeeee" style="padding-left:10px;">
	
	<select id="state" >
      <option value="">所有状态</option>
      <option value="0">报废</option>
      <option value="1">损坏</option>
      <option value="2">良好</option>
      <option value="3">已申请</option>
      <option value="5">已付款</option>
     </select>				
    </td>
  </tr>
  <tr>
  	<td width="434"  align="left" height="29" bgcolor="#eeeeee" style="padding-left:10px;">
     	<ul id="locationmenu" class="mcdropdown_menu">
		  <li rel="">所有办公地点</li>
		  <%
		  DBRow l1[] = officeLocationMgr.getChildOfficeLocation(0);
		  for (int i=0; i<l1.length; i++)
		  {
				out.println("<li rel='"+l1[i].get("id",0l)+"'> "+l1[i].getString("office_name"));
	
				  DBRow l2[] = officeLocationMgr.getChildOfficeLocation(l1[i].get("id",0l));
				  if (l2.length>0)
				  {
						out.println("<ul>");	
				  }
				  for (int ii=0; ii<l2.length; ii++)
				  {
						out.println("<li rel='"+l2[ii].get("id",0l)+"'> "+l2[ii].getString("office_name"));		
						
							DBRow l3[] = officeLocationMgr.getChildOfficeLocation(l2[ii].get("id",0l));
							  if (l3.length>0)
							  {
									out.println("<ul>");	
							  }
								for (int iii=0; iii<l3.length; iii++)
								{
										out.println("<li rel='"+l3[iii].get("id",0l)+"'> "+l3[iii].getString("office_name"));
										out.println("</li>");
								}
							  if (l3.length>0)
							  {
									out.println("</ul>");
							  }
							  
						out.println("</li>");				
				  }
				  if (l2.length>0)
				  {
						out.println("</ul>");	
				  }
				  
				out.println("</li>");
		  }
		  %>
		</ul>
		
		<input name="location" id="location" type="text" value=""/>
		<input type="hidden" name="location_id" id="location_id" value="" />
		
		<script>
		<!--
			$("#location").mcDropdown("#locationmenu",{
					allowParentSelect:true,
					  select: 
					  
							function (id,name)
							{
								$("#location_id").val(id);
							}
			
			});
			
			<%
			if (location_id>0)
			{
			%>
			$("#location").mcDropdown("#locationmenu").setValue(<%=location_id%>);
			<%
			}
			else
			{
			%>
			$("#location").mcDropdown("#locationmenu").setValue('');
			<%
			}
			%>
			function jumpApplyFunds(assets_id)
			{
				tb_show('固定或样品资产申请','<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/assets_apply_funds.html?assets_id='+assets_id+'&flag=1&TB_iframe=true&height=370&width=630',false); 
			
			}
			//-->
		</script>
		  <input type="hidden" name="product_line_id" id="product_line_id" value="0" />

  	</td>
  	<td width="434" height="29" bgcolor="#eeeeee" style="padding-left:10px;">
     <input name="Submit2" type="button" class="button_long_refresh" value="过滤" onClick="filter()">&nbsp;&nbsp;&nbsp;&nbsp;
     <input name="import/export" type="button" class="long-button" value="资产导入" onClick="window.location.href='assets_import_or_export.html?type=<%=type %>'">&nbsp;&nbsp;&nbsp;  		
     <input name="import/export" type="button" class="long-button" value="资产导出" onClick="window.location.href='assets_import_or_export.html?type=<%=type %>'">&nbsp;&nbsp;&nbsp;  		
  	</td>
  </tr>

</table>
	</div>
	</div>
	</div>
	</td>
  </tr>
</table>

<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>

	<p>&nbsp;</p>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	    <tr> 
          <th width="3%" align="center" valign="middle"  class="left-title"><input type="checkbox" name="switchCheckbox" id="switchCheckbox" value="checkbox" onClick="switchCheckboxAction()"></th>
			<th width="4%" class="right-title" style="text-align:center">序列号</th>
	        <th width="9%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">名称</th>
	        <th width="11%"  class="right-title" style="vertical-align: center;text-align: center;">条码</th>
	        <th width="10%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">类别</th>
	        <th width="5%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">单位</th>
	        <th width="4%"  class="right-title" style="vertical-align: center;text-align: center;">规格</th>
	        <th width="4%"  class="right-title" style="vertical-align: center;text-align: center;">单价</th>
	        <th width="7%"  class="right-title" style="vertical-align: center;text-align: center;">采购日期</th>
	        <th width="7%"  class="right-title" style="vertical-align: center;text-align: center;">供应商</th>
	        <th width="7%"  class="right-title" style="vertical-align: center;text-align: center;">存放地点</th>
	        <th width="6%"  class="right-title" style="vertical-align: center;text-align: center;">领用人</th>	        
			<th width="5%"  class="right-title" style="vertical-align: center;text-align: center;">状态</th>
	        <th width="13%"  class="right-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
	    </tr>
	
	    <%
	for ( int i=0; i<rows.length; i++ )
	{
	
	%>
	    <tr  > 
	      <td height="60" align="center" valign="middle"   ><input type="checkbox" id="switchCheckboxPid" name="switchCheckboxPid" value="<%=rows[i].get("aid",0l)%>">      </td>
		  <td style='word-break:break-all;' align="center">
		 <%
		  if(rows[i].get("state",0) == 4)
		  {
		  %>
		  	<a href="javascript:void(0)" onclick="jumpApplyFunds('<%=rows[i].getString("aid") %>')"><%=rows[i].getString("aid") %></a>		  
		  <%
		  }
		  else
		  {
		   %>
		  <a href="javascript:void(0)" onclick="alert('该资产已申请资金!');	" ><%=rows[i].getString("aid") %></a>		  
		   <%
		  }
		  %>
		   
		  </td>
	      <td height="80" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;' >
	
	      <%=rows[i].getString("a_name")%>
		 </td>
	      <td align="center" valign="middle" style='word-break:break-all;' >      	
	      	<%=rows[i].getString("a_barcode")%>&nbsp;
	      	</td>
	      <td align="left" valign="middle">
		  <span style="color:#999999">
		  	 <%
			  DBRow allFather[] = tree.getAllFather(rows[i].get("category_id",0l));
			  for (int jj=0; jj<allFather.length-1; jj++)
			  {
			  	out.println("<a class='nine4' href='?cmd=filter&acid="+allFather[jj].getString("id")+"'>"+allFather[jj].getString("chName")+"</a><br>");
			
			  }
			  %>

		  
		  <%
		   DBRow catalog = assetsCategoryMgr.getDetailAssetsCategory(StringUtil.getLong(rows[i].getString("category_id")));
		  if (catalog!=null)
		  {
		  	out.println("<a href='?cmd=filter&acid="+rows[i].getString("category_id")+"'>"+catalog.getString("chName")+"</a>");
		  }
		  %>
		  </span>
		  
		  </td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("unit_name")%>&nbsp;</td>
	      <td align="center" valign="middle"   ><%=rows[i].getString("standard")%>&nbsp;</td>
	      <td align="center" valign="middle"   ><%=rows[i].get("unit_price",0d)%>&nbsp;</td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("purchase_date")%>&nbsp;</td>
	      <td align="center" valign="middle"  ><%=rows[i].getString("suppliers")%>&nbsp;</td>
	      <td align="left" valign="middle"  >
	      	 <span style="color:#999999">
		  	 <%
			  DBRow allLocationFather[] = locaton_tree.getAllFather(rows[i].get("location_id",0l));
			  for (int k=0; k<allLocationFather.length-1; k++)
			  {
			  	out.println("<a class='nine4' href='?cmd=filter&lid="+allLocationFather[k].getString("id")+"'>"+allLocationFather[k].getString("office_name")+"</a><br>");
			
			  }
			  %>
		  </span>
		  
		  <%
		   DBRow location = officeLocationMgr.getDetailOfficeLocation(StringUtil.getLong(rows[i].getString("location_id")));
		  if (location!=null)
		  {
		  	out.println("<a href='?cmd=filter&lid="+rows[i].getString("location_id")+"'>"+location.getString("office_name")+"</a>");
		  }
		  %>
		  </span>
	      </td>
	      <td align="center" valign="middle"><%=rows[i].getString("requisitioned")%>&nbsp;</td>	      
	      <td align="center" valign="middle">
	      <% 
	      	if(rows[i].get("state",0) == 0){
	      		out.println("报废");
	      	}else if(rows[i].get("state",0) == 1){
	      		out.println("损坏");
	      	}else if(rows[i].get("state",0) == 2)
	      	{
	      		out.println("良好");
	      	}
	      	else if(rows[i].get("state",0) == 3) 
	      	{
	      		out.println("已申请");
	      	}
	      	else if(rows[i].get("state",0) == 4)
	      	{
	      		out.println("未申请");
	      	}else if(rows[i].get("state",0) == 5)
	      	{
	      		out.println("已付款");
	      	}
	      	%>
	      </td>
	      <td align="left" valign="middle" nowrap="nowrap">
	        <input type="button" class="short-short-button-print" value="打印" onClick="print(<%=rows[i].getString("aid")%>)"/>&nbsp;&nbsp;
			<input type="button" class="short-short-button-mod" value="领用" onClick="modRecipients(<%=rows[i].getString("aid") %>)"/>
	        <br><br/>
	        <input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="modAssets(<%=rows[i].getString("aid")%>)">
	        &nbsp;&nbsp;
	        <input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delAssets('<%=rows[i].getString("a_name") %>',<%=rows[i].getString("aid") %>)">
	        </td>
	    </tr>
	    <%
	}
	%>
	  
	</table>
</form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="assets_list.html">
    <input type="hidden" name="p">
	<input type="hidden" name="cmd" value="<%=cmd%>">
	<input type="hidden" name="category_id" value="<%=category_id%>">
	<input type="hidden" name="key" value="<%=key%>">
	<input type="hidden" name="location_id" value="<%=location_id %>">
  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/printassets.html" method="get" target="_blank" name="print_form">
	<input type="hidden" name="aid" id="aid"/>
	<input type="hidden" id="print_range_width" name="print_range_width" value="80"/>
	<input type="hidden" id="print_range_height" name="print_range_height" value="35"/>
	<input type="hidden" id="printer" name="printer" value="LabelPrinter"/>
	<input type="hidden" id="paper" name="paper" value="100X50"/>
</form>
<p>&nbsp;</p>
</body>
</html>
