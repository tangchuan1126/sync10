<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.GoodsCaseKey"%>
<%@page import="com.cwc.app.key.GoodsApplyStatusKey"%>
<%
GoodsCaseKey goodsCaseKey=new GoodsCaseKey();
GoodsApplyStatusKey applyStatusKey = new GoodsApplyStatusKey();
long aid = StringUtil.getLong(request,"aid");
DBRow row = assetsMgr.getDetailAssets(aid);
String state=row.getString("state");
long lid = row.get("location_id",0l);
StringBuffer countryCodeSB = new StringBuffer("");
DBRow treeRows[] = assetsCategoryMgr.getAllAssetsCategoryTree();

String qx;

countryCodeSB.append("<select name='category_id' id='category_id' disabled>");
countryCodeSB.append("<option  value='0'>选择资产分类...</option>");
for (int i=0; i<treeRows.length; i++)
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
	 
	if(treeRows[i].get("id",0l)==row.get("category_id",0l))
	{
		countryCodeSB.append("<option  value='"+treeRows[i].getString("id")+"' selected>"+Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))+qx+treeRows[i].getString("chName")+"</option>");

	}
	else
	{
		countryCodeSB.append("<option  value='"+treeRows[i].getString("id")+"'>"+Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))+qx+treeRows[i].getString("chName")+"</option>");
	}
	}
	countryCodeSB.append("</select>");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改固定资产</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<script>
//追加商品信息
var va = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15'];
function addName(){	
	var len  = $(".name").length ;
	var table =  "<table class='receiver name next'>";
	table += "<tr><td style='"+"width:120px;text-align:center;"+"' class='STYLE2'>商品名"+va[len]+":</td>";
	table += "<td align='left' valign='middle' width='20%'><input name='aname' type='text' class='input-line' id='aname' >";
	table += "<td style='width:120px;text-align:right' class='STYLE2'>型号"+va[len]+":</td>";
	table += "<td><input type='text' name='model' class='noborder'/></td>";
	table += "<td style='width:80px;text-align:left;'><img alt='删除' src='../imgs/del.gif' onclick='deleteGoodsInfo(this)' /></td></tr></table>";
 	// 获取最后一个name的table 然后添加到他的后面。如果是没有的话,那么就选取H1标签然后添加到他的后面
	var addBar = $(".receiver").last();
	addBar.after($(table));
}
function deleteGoodsInfo(_this){
	var parentNode = $(_this).parent().parent().parent().parent(); 
	if($(".receiver").length == 1){
		alert("至少包含一个商品信息");
		return ;
	}
		 parentNode.remove();
}

function modAssets()
{
		  
	var f = document.mod_assets_form;
	var code = /[a-z]+|[\u4e00-\u9fa5]|[:?*......\"\\<>|~$&@^%·……]/;

	if(f.category_id.value == 0)
	{
		alert("请选择资产类别");
		return false;
	}
	var str  = "";
	var goodsInfo = $(".name");
	if(goodsInfo.length > 0 ){
		for(var index = 0 , count = goodsInfo.length ; index < count ; index++ ){
		//	aname,model	商品名、型号
			var _table = $(goodsInfo.get(index));
			var aname = $("input[name='aname']",_table).val();
			var model =  $("input[name='model']",_table).val();
			if($.trim(aname).length > 0 ){
				str += aname+"/";
			}else{
				alert("请 输入商品名称!");
				return ;
			}
			if($.trim(model).length > 0 ){
				str += model+",";
			}else{
				alert("请输入商品型号!");
				return ;
				}
		}
	}	
	 if (f.a_barcode.value == "")
	 {
		alert("请填写资产条码");
		return false;
	 }else if(f.unit_name.value == "0"){
			alert("请选择单位");
			return false;
		 }
	 else if(code.test(f.a_barcode.value))
	 {
	 	alert("资产条码只能是英文大写字母或数字或字符");
	 	return false;
	 }
	 if (f.unit_price.value == "")
	 {
		alert("请填写采购价格");
		return false;
	 }
	 else if(parseFloat(f.unit_price.value) != f.unit_price.value)
	 {
	 	alert("采购价格只能是数字");
	 	return false;
	 }
	 
	 else if(f.unit_price.value.match(/^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/))
	 {
	 	alert("采购价格不能为负数");
	 	return false;
	 }
	 if (f.location.value == 0){
	 	alert("请选择资产存放位置");
	 	return false;
	 }
	 else
	 {
	 	
		 parent.document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/modAssets.action";
			parent.document.mod_form.category_id.value = f.category_id.value;
			parent.document.mod_form.product_line_id.value = "<%=row.get("product_line_id",0)%>";		
			parent.document.mod_form.a_name.value = str;
			parent.document.mod_form.a_id.value="<%=row.get("aid",0) %>";
			parent.document.mod_form.a_barcode.value = f.a_barcode.value;
			parent.document.mod_form.unit_name.value = f.unit_name.value;
			parent.document.mod_form.unit_price.value = f.unit_price.value;
			parent.document.mod_form.purchase_date.value = f.purchase_Date.value;
			parent.document.mod_form.suppliers.value = f.suppliers.value;
			parent.document.mod_form.location.value = f.location.value;
			parent.document.mod_form.requisitioned_id.value = "<%=row.get("requisitioned_id",0)%>";
			parent.document.mod_form.requisitioned.value = "<%=row.getString("requisitioned") %>";
			parent.document.mod_form.state.value = f.state.value;
			parent.document.mod_form.handle_time.value = "<%=row.getString("handle_time") %>";
			parent.document.mod_form.applyState.value = f.applystate.value;
			parent.document.mod_form.goodsState.value = "<%=row.get("goodsState",0) %>";
			parent.document.mod_form.handle_price.value = f.handle_price.value;
			parent.document.mod_form.responsi_person.value = "<%=row.getString("responsi_person") %>";
			parent.document.mod_form.return_name.value = "<%=row.getString("return_name") %>";
			parent.document.mod_form.creater_id.value = "<%=row.get("creater_id",0) %>";
			parent.document.mod_form.requisition_date.value = "<%=row.getString("requisition_date") %>";
			parent.document.mod_form.consignee.value = "<%=row.getString("consignee") %>";
			parent.document.mod_form.receive_time.value = "<%=row.getString("receive_time") %>";
			parent.document.mod_form.currency.value = "<%=row.getString("currency") %>";
			parent.document.mod_form.center_account_id.value = "<%=row.getString("center_account_id") %>";
			
			parent.document.mod_form.submit();
	 }
}

function onLoadInit()
{
	$("#purchase_Date").date_input();
}
function closeWin()
{
	$.artDialog.close();
}

</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}
.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
table.receiver td {border:0px;}
table.receiver td input.noborder{border-bottom:1px solid silver;width:200px;}
table.receiver td.left{width:150px;text-align:right;}
table.receiver{border-collapse:collapse ;}
span._hand{cursor:pointer;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInit();">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="mod_assets_form" method="post" action="">
<input type="hidden"  id="aid" name="aid" value="<%=row.getString("aid") %>"/>
<input type="hidden" id="handle_price" name="handle_price" value="<%=row.getString("handle_price") %>"/>
<table width="98%" border="0" cellspacing="5" cellpadding="2">

  <tr>
    <td colspan="2" align="left" valign="middle" class="text-line">
    	<table width="100%" border="0" cellpadding="5" cellspacing="0" >
      <tr>
        <td colspan="1" align="left" valign="middle" class="STYLE1 STYLE2" width="60">
        
					固定资产类别
		</td>
        <td align="left" colspan="3">
        	<ul id="categorymenu" class="mcdropdown_menu">
					  <%
					  int type=1;
					  if(type==1) {
					  	  DBRow ca1[] = assetsCategoryMgr.getAssetsCategoryChildren(0,Integer.toString(type));
						  for (int i=0; i<ca1.length; i++)
						  {
								out.println("<li rel='"+ca1[i].get("id",0l)+"'> "+ca1[i].getString("chName"));
								  DBRow[] ca2 = null;
								  	ca2 = assetsCategoryMgr.getAssetsCategoryChildren(ca1[i].get("id",0l),"1");
								  if (ca2.length>0)
								  {
								  		out.println("<ul>");	
								  }
								  for (int ii=0; ii<ca2.length; ii++)
								  {
										out.println("<li rel='"+ca2[ii].get("id",0l)+"'> "+ca2[ii].getString("chName"));		
											
											DBRow ca3[] = assetsCategoryMgr.getAssetsCategoryChildren(ca2[ii].get("id",0l));
											  if (ca3.length>0)
											  {
													out.println("<ul>");	
											  }
												for (int iii=0; iii<ca3.length; iii++)
												{
														out.println("<li rel='"+ca3[iii].get("id",0l)+"'> "+ca3[iii].getString("chName"));
														out.println("</li>");
												}
											  if (ca3.length>0)
											  {
													out.println("</ul>");
											  }
											
										out.println("</li>");				
								  }
								  if (ca2.length>0)
								  {
								  		out.println("</ul>");	
								  }
								  
								out.println("</li>");
						  }
					  }else {
					  	  DBRow ca1[] = assetsCategoryMgr.getAssetsCategoryChildren(0,Integer.toString(type));
						  for (int i=0; i<ca1.length; i++)
						  {
								//out.println("<li onmousemove='catagory("+ca1[i].get("id",0l)+")' value='"+ca1[i].get("id",0l)+"'><a href='"+ca1[i].get("id",0l)+"'> "+ca1[i].getString("chName")+" </a>");
								  DBRow[] ca2 = null;
								  	ca2 = assetsCategoryMgr.getAssetsCategoryChildren(ca1[i].get("id",0l),"3");
								 
								  for (int ii=0; ii<ca2.length; ii++)
								  {
										out.println("<li rel='"+ca2[ii].get("id",0l)+"'> "+ca2[ii].getString("chName"));		
										
										out.println("</li>");				
								  }
								out.println("</li>");
						  }
					  }
					  %>
				</ul>
					  <input type="text" name="category" id="category" value="" />
					  <input type="hidden" name="category_id" id="category_id" value="0" />

       <script>
  $("#category").mcDropdown("#categorymenu",{
						allowParentSelect:true,
						  select: 
						  
								function (id,name)
								{
									$("#category_id").val(id);
									//alert($("#filter_lid1").val());
								}
				
				});								
				$("#category").mcDropdown("#categorymenu").setValue(<%=row.get("category_id",0)%>);
  </script>	
        </td>
      </tr>
    </table></td>
    </tr>
     <tr>
  <td align="right" valign="middle" class="STYLE3" nowrap="nowrap">商品信息</td>
  <td colspan="3">
    		<h1 id="addBar"></h1>
    	<%
    	String[] items = row.getString("a_name").split(",");
    	for(int k=0; k<items.length; k++){
    		 String[] nameModel = items[k].split("/");	
    	%>
    	<table class="receiver name next">
		      			<tr>
		      				<td style="width:120px;text-align:center;" class="STYLE2">商品名<%=k+1 %>:</td>
		      				<td align="left" valign="middle" width="20%"><input name="aname" type="text" class="input-line" id="aname" value="<%=nameModel[0] %>"></td>
		      				
		      				<td style="width:120px;text-align:right;" class="STYLE2">型号<%=k+1 %>:</td>
		      				<td align="left" valign="middle" >
		      				<%
		      					if(nameModel.length>1){
		      				%>
   							 <input name="model" type="text" id="model" class="input-line" value="<%=nameModel[1] %>">    
   							 <%}else{ %>
   							  <input name="model" type="text" id="model" class="input-line" value="<%=row.getString("standard")%>"/>  
   							  <%} %>
   							 </td>
		      				<td style="width:80px;text-align:left;">
		      				<img alt="删除" src="../imgs/del.gif" onclick="deleteGoodsInfo(this)" />
		      				<%if(k==0){ %>
		      				<img alt="添加" src="../imgs/add.gif" onclick="addName();">
		      				<%} %>
		      				</td>
		      			</tr>
		      	</table>		 
    		     <%} %>
    </td>
  </tr>
			  <tr>
			  	 <td align="left" valign="middle" class="STYLE3" >采购日期</td>
			    <td align="left" valign="middle" >
			    	<%
					TDate tDate = new TDate();
					tDate.addDay(-30);
					
					String input_st_date;
					if ( row.getString("purchase_date").equals("") )
					{	
						input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+1+"-"+tDate.getStringDay();
					}
					else
					{	
						input_st_date = row.getString("purchase_date");
					}
					
					
					%>
			    	<input type="text" name="purchase_Date" id="purchase_Date" value="<%=row.getString("purchase_date") %>"  style="border:1px #CCCCCC solid;width:150px;" >
			    </td>
			
				 <td align="left" valign="middle" class="STYLE3" >采购价格</td>
			    <td align="left" valign="middle" ><input name="unit_price" id="unit_price" type="text" value="<%=row.getString("unit_price") %>"   style="width:100px;"></td>
			  </tr>
			  
			  <tr>
			  	<td align="left" valign="middle" class="STYLE3" colspan="1">存放地点</td>
			    <td align="left" valign="middle" colspan="3">
			    	<ul id="locationmenu" class="mcdropdown_menu">
					  <li rel="0">所有分类</li>
					  <%
					  DBRow c1[] = officeLocationMgr.getChildOfficeLocation(0);
					  for (int i=0; i<c1.length; i++)
					  {
							out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("office_name"));
				
							  DBRow c2[] = officeLocationMgr.getChildOfficeLocation(c1[i].get("id",0l));
							  if (c2.length>0)
							  {
							  		out.println("<ul>");	
							  }
							  for (int ii=0; ii<c2.length; ii++)
							  {
									out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("office_name"));		
									
										DBRow c3[] = officeLocationMgr.getChildOfficeLocation(c2[ii].get("id",0l));
										  if (c3.length>0)
										  {
												out.println("<ul>");	
										  }
											for (int iii=0; iii<c3.length; iii++)
											{
													out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("office_name"));
													out.println("</li>");
											}
										  if (c3.length>0)
										  {
												out.println("</ul>");
										  }
										  
									out.println("</li>");				
							  }
							  if (c2.length>0)
							  {
							  		out.println("</ul>");	
							  }
							  
							out.println("</li>");
					  }
					  %>
				</ul>
					  <input type="text" name="location" id="location" value="" />
					  <input type="hidden" name="filter_lid" id="filter_lid" value=""  />
					  
				
				<script>
				$("#location").mcDropdown("#locationmenu",{
						allowParentSelect:true,
						  select: 
						  
								function (id,name)
								{
									$("#filter_lid").val(id);
								}
				
				});
				
				<%
				if (lid > 0)
				{
				%>
				$("#location").mcDropdown("#locationmenu").setValue(<%=lid%>);
				<%
				}
				else
				{
				%>
				$("#location").mcDropdown("#locationmenu").setValue(0);
				<%
				}
				%> 
				
				</script>
			    </td>
			  </tr>
			  
			  <tr>
			  	 <td align="left" valign="middle" class="STYLE3" >单位</td>
			    <td align="left" valign="middle" >
			     <select id="unit" name="unit_name">  				
    				 <option value="0" >--单位--</option>
    				<% 
    					if(row.getString("unit_name").equals("套")){
    						out.print("<option value='套' selected='selected'>套</option>");
    					}else 
    						out.print("<option value='套'>套</option>");
    					if(row.getString("unit_name").equals("个")){
    						out.print("<option value='个' selected='selected'>个</option>");
    					}else 
    						out.print("<option value='个'>个</option>");
    					if(row.getString("unit_name").equals("只")){
    						out.print("<option value='只' selected='selected'>只</option>");
    					}else 
    						out.print("<option value='只'>只</option>");
    					if(row.getString("unit_name").equals("对")){
    						out.print("<option value='对' selected='selected'>对</option>");
    					}else 
    						out.print("<option value='对' >对</option>");
    					if(row.getString("unit_name").equals("台")){
    						out.print("<option value='台' selected='selected'>台</option>");
    					}else
    						out.print("<option value='台'>台</option>");
    					if(row.getString("unit_name").equals("件")){
    						out.print("<option value='件' selected='selected'>件</option>");
    					}else 
    						out.print("<option value='件'>件</option>");	
    					if(row.getString("unit_name").equals("把")){
    						out.print("<option value='把' selected='selected'>把</option>");
    					}else{
    						out.print("<option value='把'>把</option>");
    					}
    
    				%>
    			</select>
				</td>
			    <td align="left" valign="middle" class="STYLE3" >条码</td>
			    <td align="left" valign="middle" ><input name="a_barcode" id="a_barcode" value="<%=row.getString("a_barcode") %>" type="text" class="input-line" ></td>
			    </td>
			       
			  </tr>
			  <tr>
			    <td align="left" valign="middle" class="STYLE3" colspan="1">供应商</td>
			    <td align="left" valign="middle" colspan="3"><input name="suppliers" id="suppliers" type="text" value="<%=row.getString("suppliers") %>"  class="input-line">
			      </td>
			  </tr>
			  
			  
			  
			  <tr>
			    <td align="left" valign="middle" class="STYLE3" >好坏情况</td>
			    <td align="left" valign="middle" >
			    	<select id="state" name="state">
			    		<%
			    			if(row.getString("state").equals(goodsCaseKey.intact))
			    			{		    			
			    		 %>
				   		 <option value="<%=goodsCaseKey.intact %>" selected="selected"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.intact) %></option>
				   		 <option value="<%=goodsCaseKey.function_damage %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.function_damage) %></option>
				   		 <option value="<%=goodsCaseKey.facade_damage %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.facade_damage) %></option>
				   		 <option value="<%=goodsCaseKey.scrap %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.scrap) %></option>
				    <%
				    	}
				    	else if(row.getString("state").equals(goodsCaseKey.function_damage))
				    	{
				     %>
				     	 <option value="<%=goodsCaseKey.function_damage %>" selected="selected"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.function_damage) %></option>
				     	<option value="<%=goodsCaseKey.intact %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.intact) %></option>
				   		 <option value="<%=goodsCaseKey.facade_damage %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.facade_damage) %></option>
				   		 <option value="<%=goodsCaseKey.scrap %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.scrap) %></option>
				    <%
				    	}
				    	else if(row.getString("state").equals(goodsCaseKey.facade_damage))
				    	{
				     %>
				     
				   		 <option value="<%=goodsCaseKey.facade_damage %>" selected="selected"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.facade_damage) %></option>
				     	<option value="<%=goodsCaseKey.intact %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.intact) %></option>
				   		 <option value="<%=goodsCaseKey.function_damage %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.function_damage) %></option>
				   		 <option value="<%=goodsCaseKey.scrap %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.scrap) %></option>
				    <%
				    	}
				    	else if(row.getString("state").equals(goodsCaseKey.scrap))
				    	{
				    	%>
				     	 <option value="<%=goodsCaseKey.intact %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.intact) %></option>
				   		 <option value="<%=goodsCaseKey.function_damage %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.function_damage) %></option>
				   		 <option value="<%=goodsCaseKey.facade_damage %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.facade_damage) %></option>
				   		 <option value="<%=goodsCaseKey.scrap %>" selected="selected"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.scrap) %></option>
				    	<%
				    	}else
				    	{
				    %>
				    	<option value="0">--好坏情况--</option>
				     	<option value="<%=goodsCaseKey.intact %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.intact) %></option>
				   		 <option value="<%=goodsCaseKey.function_damage %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.function_damage) %></option>
				   		 <option value="<%=goodsCaseKey.facade_damage %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.facade_damage) %></option>
				   		 <option value="<%=goodsCaseKey.scrap %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.scrap) %></option>
				    <%} %>
					</select>
			      </td>
			      <%
							if(row.getString("applystate").equals(applyStatusKey.already_apply)){
										out.print("<td><input type='hidden' id='applystate' name='applystate' value='3' /></td>");
								}else if(row.get("applystate",0)==0){
									out.print("<td><input type='hidden' id='applystate' name='applystate' value='0' /></td>");
								}else{
						%>
			      <td align="left" valign="middle" class="STYLE3" >资金申请:</td>
			    <td align="left" valign="middle" >	
						<select name="applystate" id="applystate">
							<%
								if(row.getString("applystate").equals(applyStatusKey.norequired_apply)){
							%>
								<option value="<%=applyStatusKey.norequired_apply%>" seleted='seleted'>不需要</option>
								 <option value="<%=applyStatusKey.no_apply%>">需要</option>
							<%
								}else if(row.getString("applystate").equals(applyStatusKey.no_apply)){
								%>
									<option value="<%=applyStatusKey.no_apply%>" seleted='seleted'>需要</option>
									 <option value="<%=applyStatusKey.norequired_apply%>">不需要</option>
								<%
								}
								%>	
								                               
	               </select>	    	
			      </td>
			      <%} %>
			  </tr>
  
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存修改" class="normal-green" onClick="modAssets();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table>
</body>
</html>
