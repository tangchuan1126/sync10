<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String aids = StringUtil.getString(request,"aids");
String assetsIds[] = aids.split(",");

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>批量修改资产</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script>
function batchMod()
{
	document.batchModForm.submit();
	//window.parent.location.reload();
}
</script>

</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;
}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">
    <form name="batchModForm" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/BatchModAssets.action">
    <input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>">
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">

    <tr> 
        <th style="vertical-align: center;text-align: center;" class="left-title">名称</th>
        <th style="vertical-align: center;text-align: center;" class="right-title">条码</th>
        <th width="12%" style="vertical-align: center;text-align: center;" class="right-title">单位</th>
        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">规格</th>
        <th width="9%" style="vertical-align: center;text-align: center;" class="right-title">采购单价</th>
        <th width="10%" style="vertical-align: center;text-align: center;" class="right-title">存放位置</th>
    </tr>
<%
for (int i=0; i<assetsIds.length; i++)
{
	DBRow assets = assetsMgr.getDetailAssets( StringUtil.getLong(assetsIds[i]) );
%>
    <tr > 
      <td   width="28%" height="60" align="center" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><label>
	   <input name="batch_aid" type="hidden"  value="<%=assets.getString("aid")%>">
        <input name="a_name_<%=assets.getString("aid")%>" type="text" style="width:200px;" value="<%=assets.getString("a_name")%>">
      </label></td>
      <td   width="31%" align="center" valign="middle" style='word-break:break-all;' ><input type="text" name="a_code_<%=assets.getString("aid")%>"  style="width:200px;" value="<%=assets.getString("a_barcode")%>"></td>
      <td   align="center" valign="middle" ><input type="text" name="unit_name_<%=assets.getString("aid")%>" style="width:30px;" value="<%=assets.getString("unit_name")%>"></td>
      <td   align="center" valign="middle" style='word-break:break-all;'><input type="text" name="standard_<%=assets.getString("aid")%>" style="width:50px;" value="<%=assets.getString("standard")%>"></td>
      <td   align="center" valign="middle" style='word-break:break-all;'><input type="text" name="unit_price_<%=assets.getString("aid")%>" style="width:50px;" value="<%=assets.getString("unit_price")%>"></td>
      <td   align="center" valign="middle" >
      <select name="location_<%=assets.getString("aid")%>">
   		<option value="0">请选择资产的存放位置</option>
   		<%
   			DBRow [] locations = officeLocationMgr.getAllOfficeLocation();
   			for(int j=0;j<locations.length;j++)
   			{
   			
   		 %>
   		 <option value="<%=locations[j].getString("id") %>" <%=locations[j].get("id",0l) == assets.get("location_id",0l)?"selected":"" %>><%=locations[j].getString("office_name") %></option>
   		 <%
   		 	}
   		  %>
   	</select>
      </td>
    </tr>
<%
}
%>
</table>
</form></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type='button'  class="normal-green-long" name='Submit22' value='保存修改'  onClick="batchMod()"/>
      <input name="Submit" type="button" class="normal-white" value="取消" onClick="parent.closeWin();">
	</td>
  </tr>
</table> 
</body>
</html>
