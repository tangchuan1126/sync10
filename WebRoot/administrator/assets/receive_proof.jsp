<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.cwc.app.key.GoodsStatusKey"%>
<%@ page import="com.cwc.app.key.FileWithTypeKey"%>
<%
long assets_id = StringUtil.getLong(request,"aid");
DBRow assets = assetsMgr.getDetailAssets(assets_id);
GoodsStatusKey goodsStatusKey=new GoodsStatusKey();
SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 

String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
String commonFileDeleteAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action"; 
String uploadImageAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/financial_management/ReceiveUploadImageAction.action";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>收货凭证</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<style type="text/css">
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
.setcenter{valign:middle; }
</style>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 文件预览  -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  $("#consignee").focus();

})
function init(){
$("#consignee").focus();
}
function closeWin()
{
	$.artDialog.close();	
	$.artDialog.opener.windowRefresh && $.artDialog.opener.windowRefresh();
}
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"picture",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target){
	if($.trim(fileNames).length > 0 ){
	    $("input[name='file_names']").val(fileNames);
	    var  o = {
	    	a_id:<%=assets_id%>,
		    file_name:$("input[name='file_names']").val(),
		    sn:$("input[name='sn']").val(),
		    path:$("input[name='path']").val(),
		    file_with_type:$("input[name='file_with_type']").val(),
		}
		$.ajax({
			url:'<%=uploadImageAction %>',
			dataType:'json',
			data:$.param(o),
			success:function(data){
				if(data && data.flag == "success"){
					window.location.reload();
				}else{
					alert("上传文件出错");
				}
			},
			error:function(){
				alert("系统错误!请稍后再试");
			}
		})
	}
	
}

//删除凭证
function deleteImage(table_name,pk,file_id){
	$.ajax({
		url:'<%= commonFileDeleteAction%>' + "?table_name="+table_name+"&pk="+pk+"&file_id="+file_id,
		dataType:'json',
		success:function(data){
			if(data && data.flag == "success"){
				window.location.reload();
			}else{
				alert("系统错误!请稍后重试");
			}
		},
		error:function(){alert("系统错误!请稍后重试");}
	})
}

//file表中的图片在线显示	
function showPictrueOnline(fileWithType,fileWithId , currentName){
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_receive")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}
function onlineScanner(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onload="init();">
<form name="receive_assets_form" method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center" onclick="init()">
  <tr>
    <td colspan="2" align="center" valign="top">
    <table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<input type="hidden"  id="aid" name="aid" value=""/>
<table width="100%" border="0" cellspacing="5" cellpadding="2" align="center">
<tr height="8">
<td>&nbsp;</td>
</tr>

  <tr>
			    <fieldset class="set" style="border:2px solid #c3c3c3;text-align:left；width:98%;">
  <legend ><span><%=assets_id %></span></legend>
  <p>
  <span class="STYLE3">商品名称</span>
   <span> <%
	          String[] a_name=null;
	          a_name=assets.getString("a_name").split(",");
	         // out.print(a_name.length);
	         for(int j=0;j<a_name.length;j++){
	        	  String[] splitModel=null;
	 	         String nameModel=a_name[j];
	 	         splitModel=nameModel.split("/");
	 	        out.print(splitModel[0]);
	 	        if(j!=a_name.length-1){
	 	        	out.print("，");
	 	        }
	          }
	          %></span>
  </p>
   <p>
  <span class="STYLE3">收货时间</span>
   <span>
			            <%=assets.getString("receive_time") %>  </span>
  </p>
   <p>
  <span class="STYLE3">&nbsp;&nbsp;收货人</span>
   <span> <%=assets.getString("consignee") %> </span></p>
<p>
  <span class="STYLE3">&nbsp;&nbsp;&nbsp;凭证:</span>
					<span>
		 				<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="上传凭证" />
		 				<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />	
		    				<div id="jquery_file_up" style="padding-left:60px;">
		    						<input type="hidden" id="sn" name="sn" value="Receive"/>
		 							<input type="hidden" id="file_with_type" name="file_with_type" value="<%= FileWithTypeKey.assets_receive %>" />
		 							<input type="hidden" id="path" name="path" value="<%= systemConfig.getStringConfigValue("file_path_receive")%>" />	
			              			<input type="hidden" id="file_names" name="file_names" value=""/>				 	
			            	</div> 
			          
   </span>
    <%
		 		// 如果是完成应该在下面显示文件然后支持下载链接
		 			 DBRow[] fileRows = transportMgrZr.getFileByFilwWidthIdAndFileType("file",assets.get("aid",0),FileWithTypeKey.assets_receive );
		 				if(fileRows != null && fileRows.length > 0 ){
		 			 %>
		 			<div id="over_file_td">
		 			 	 <%for(DBRow fileRow : fileRows){ %>
		 			 	 <div>
		 			 	  <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
		 			 	 	<%if(StringUtil.isPictureFile(fileRow.getString("file_name"))){ %>
				 			 	 <p>
				 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.assets_receive %>','<%=assets_id %>','<%=fileRow.getString("file_name") %>');"><%=fileRow.getString("file_name") %></a>
				 			 	 	<a  href="javascript:void(0)"  onclick="deleteImage('file','file_id','<%=fileRow.get("file_id",0) %>')">删除</a>
				 			 	 </p>
			 			 	 <%} else{ %>
 		 			 	 	  		<p><a href='<%= downLoadFileAction%>?file_name=<%=fileRow.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_receive")%>'><%=fileRow.getString("file_name") %></a>
 		 			 	 	  		<a  href="javascript:void(0)"  onclick="deleteImage('file','file_id','<%=fileRow.get("file_id",0) %>')">删除</a>
 		 			 	 	  		</p> <br/>
 		 			 	 	  <%} %>
		 			 	 
 		 			 	  	
		 			 	 </div>
		 			 	 <%} %>
		 			 </div>
		 			 <%
		 			 }
		 	%>
   </p>
  </fieldset>
</tr>
</table>
	</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	  <input type="button" name="Submit2" value="关闭" class="normal-white" onClick="closeWin();">
	</td>
	<td width="1%" align="left" valign="middle" class="win-bottom-line"></td>
  </tr>
</table>
</form>	
</body>
</html>
