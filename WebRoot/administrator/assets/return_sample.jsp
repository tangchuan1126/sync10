<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.cwc.app.key.GoodsStatusKey"%>
<%
long assets_id = StringUtil.getLong(request,"aid");
DBRow assets = assetsMgr.getDetailAssets(assets_id);
GoodsStatusKey goodsStatusKey=new GoodsStatusKey();
SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd"); 
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>退回商品</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<style type="text/css">
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
.setcenter{valign:middle; }
</style>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript">
function init(){


}
function closeWin()
{
  $.artDialog.close();
}
function returnAssets(aid){
	
       var f=return_assets_form;
       var p=parent.document.mod_form;
       if (f.responsi_person.value.trim() == "")
  	 {
  		alert("请填写退款负责人");
  		return false;
  	 }
       if (f.handle_price.value.trim() == "")
  	 	{
	  		alert("请填写退款额");
	  		return false;
  	 	}
	  	else if(parseFloat(f.handle_price.value) != f.handle_price.value)
	  	 {
	  	 	alert("退款额只能是数字");
	  	 	return false;
	  	 }
  		else if(f.handle_price.value.match(/^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/))
  		 {
	  	 	alert("退款额不能为负数");
	  	 	return false;
  	 	}else
       p.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/modAssets.action";
		p.category_id.value = f.category_id.value;
		p.product_line_id.value = f.product_line_id.value;		
		p.a_name.value = f.a_name.value;
		p.a_id.value=f.a_id.value;
		p.a_barcode.value = f.a_barcode.value;
		p.unit_name.value = f.unit_name.value;
		p.unit_price.value = f.unit_price.value;
		p.standard.value = f.standard.value;
		p.purchase_date.value = f.purchase_date.value;
		p.suppliers.value = f.suppliers.value;
		p.location.value = f.location.value;
		p.requisitioned_id.value = f.requisitioned_id.value;
		p.requisitioned.value = f.requisitioned.value;
		p.state.value = f.state.value;
		p.handle_time.value = f.handle_time.value;
		p.goodsState.value = f.goodsState.value;
		p.handle_price.value = f.handle_price.value;
		p.responsi_person.value = f.responsi_person.value;
		p.return_name.value = f.return_name.value;
		p.applyState.value= "<%=assets.getString("applyState")%>";
		p.center_account_id.value = f.center_account_id.value;
		p.creater_id.value = "<%=assets.getString("creater_id")%>";
		p.requisition_date.value = "<%=assets.getString("requisition_date")%>";
		p.consignee.value = "<%=assets.getString("consignee")%>";
		p.receive_time.value= "<%=assets.getString("receive_time")%>";
		p.currency.value ="<%=assets.getString("currency")%>";
		p.submit();	

}

</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onload="init();">
<form name="return_assets_form" method="post" action="">
<input type="hidden"  id="a_id" name="a_id" value="<%=assets_id %>"/>
<input type="hidden"  id="a_name" name="a_name" value="<%=assets.getString("a_name") %>"/>
<input type="hidden"  id="unit_price" name="unit_price" value="<%=assets.get("unit_price",0d)%>"/>
<input type="hidden"  id="unit_name" name="unit_name" value="<%=assets.getString("unit_name")%>"/>
<input type="hidden" id="a_barcode" name="a_barcode"  value="<%=assets.getString("a_barcode") %>"/>
<input type="hidden" id="standard" name="standard"  value="<%=assets.getString("standard") %>"/>
<input type="hidden" id="purchase_date" name="purchase_date"  value="<%=assets.getString("purchase_date") %>"/>
<input type="hidden" id="suppliers" name="suppliers"  value="<%=assets.getString("suppliers") %>"/>
<input type="hidden" id="category_id" name="category_id"  value="<%=assets.getValue("category_id") %>"/>
<input type="hidden" id="location" name="location"  value="<%=assets.getValue("location_id") %>"/>
<input type="hidden" id="requisitioned_id" name="requisitioned_id"  value="<%=assets.getValue("requisitioned_id") %>"/>
<input type="hidden" id="requisitioned" name="requisitioned"  value="<%=assets.getString("requisitioned") %>"/>
<input type="hidden" id="state" name="state"  value="<%=assets.getValue("state") %>"/>
<input type="hidden" id="product_line_id" name="product_line_id"  value="<%=assets.getValue("product_line_id") %>"/>
<input type="hidden" id="center_account_id" name="center_account_id"  value="<%=assets.getValue("center_account_id") %>"/>
<input type="hidden" id="goodsState" name="goodsState"  value="<%=goodsStatusKey.retreat %>"/>
<input type="hidden" id="handle_time" name="handle_time"  value="<%=sdf.format(new Date())%>"/>
<input type="hidden" id="return_name" name="return_name"  value="<%=assets.getValue("return_name")%>"/>

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td colspan="2" align="center" valign="top">
    <table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<input type="hidden"  id="aid" name="aid" value=""/>
<table width="100%" border="0" cellspacing="5" cellpadding="2" align="center">
<tr height="8">
<td>&nbsp;</td>
</tr>

  <tr>
			    <fieldset class="set" style="border:2px solid #c3c3c3;text-align:left；width:98%;">
  <legend ><span><%=assets_id %></span></legend>
  <p>
  <span class="STYLE3">商品名称</span>
   <span> <%
	          String[] a_name=null;
	          a_name=assets.getString("a_name").split(",");
	         // out.print(a_name.length);
	         for(int j=0;j<a_name.length;j++){
	        	  String[] splitModel=null;
	 	         String nameModel=a_name[j];
	 	         splitModel=nameModel.split("/");
	 	        out.print(splitModel[0]);
	 	        if(j!=a_name.length-1){
	 	        	out.print("，");
	 	        }
	          }
	          %></span>
  </p>
   <p>
  <span class="STYLE3">退货时间</span>
   <span>
			              <%=sdf.format(new Date())%></span>
  </p>
   <p>
  <span class="STYLE3">&nbsp;&nbsp;负责人</span>
   <span><input type="text" name="responsi_person" id="responsi_person" value=""  style="border:1px #CCCCCC solid;width:150px;" ></span>
  </p>
    <span class="STYLE3">&nbsp;&nbsp;退款额</span>
   <span> <input name="handle_price" id="handle_price" type="text" value=""  style="border:1px #CCCCCC solid;width:150px;"></span>
  </p>
  </fieldset>
			  </tr>
</table>
	</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="退货" class="normal-green" onClick="returnAssets(<%=assets_id %>);">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
	<td width="1%" align="left" valign="middle" class="win-bottom-line"></td>
  </tr>
</table>
</form>	
</body>
</html>
