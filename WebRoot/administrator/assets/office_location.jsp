<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
		<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
		<script type="text/javascript" src="../js/select.js"></script>

	
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="JavaScript1.2">

function addOfficeLocation(parentid)
{
	$.prompt(
	
	"<div id='title'>增加子类</div><br />父类：<input name='proTextParentLocation' type='text' id='proTextParentLocation' style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br>新建办公位置<br><input name='proTextLocation' type='text' id='proTextLocation' style='width:300px;'>",
	
	{
	      submit: checkOfficeLocation,
   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/officeLocation/getDetailOfficeLocationJSON.action",
							{parentid:parentid},
							function callback(data)
							{
								$("#proTextParentLocation").setSelectedValue(data.office_name);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/officeLocation/addOfficeLocation.action";
						document.add_form.parentid.value = parentid;
						document.add_form.office_name.value = f.proTextLocation;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function checkOfficeLocation(v,m,f)
{
	if (v=="y")
	{
		 if(f.proTextLocation == "")
		 {
				alert("请选填写办公位置");
				return false;
		 }
		 
		 return true;
	}

}



function modOfficeLocation(id)
{
	$.prompt(
	
	"<div id='title'>修改办公位置</div><br />办公地点<br><input name='proTextLocation' type='text' id='proTextLocation' style='width:300px;'>",
	
	{
		  submit: checkOfficeLocation,
   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/officeLocation/getDetailOfficeLocationJSON.action",
							{parentid:id},
							function callback(data)
							{
								$("#proTextLocation").setSelectedValue(data.office_name);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/location/modOfficeLocation.action";
						document.mod_form.id.value = id;
						document.mod_form.office_name.value = f.proTextLocation;		
						document.mod_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}






function delOfficeLocation(id)
{
	if (confirm("确认要删除该办公地点吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/officeLocation/delOfficeLocation.action";
		document.del_form.id.value = id;
		document.del_form.submit();
	}
}


</script>

<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 固定资产 »   办公地点</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="id" >

</form>

<form method="post" name="mod_form">
<input type="hidden" name="id" >
<input type="hidden" name="office_name" >
</form>

<form method="post" name="add_form">
<input type="hidden" name="parentid">
<input type="hidden" name="office_name">
</form>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
  <form name="listForm" method="post">
    <input type="hidden" name="id">
    <input type="hidden" name="parentid">
    <input type="hidden" name="imp_color">

      <tr> 
        <th colspan="2"  class="left-title">办公地点</th>
        <th  class="right-title" style="vertical-align: center;text-align: center;"  width="12%">ID</th>
        <th width="38%"  class="right-title">&nbsp;</th>
      </tr>

   	<tr>
    	<td height="39" colspan="4" valign="middle">
     
      	<script type="text/javascript">

		d = new dTree('d');
		d.add(0,-1,'办公地点</td><td align="center" valign="middle">&nbsp;</td><td align="center" valign="middle" width="38%"><input name="Submit" type="button" class="long-button-add" onClick="addOfficeLocation(0)" value="增加子类"></td></tr></table>');
		
		<%
			DBRow [] parentLocation = officeLocationMgr.getParentOfficeLocation();
			StringBuffer treeCode = new StringBuffer();
			for(int i=0;i<parentLocation.length;i++)
			{
				DBRow [] subLocation = officeLocationMgr.getChildOfficeLocation(parentLocation[i].get("id",0l));
				if(subLocation.length != 0)
				{
		%>
			d.add(<%=parentLocation[i].getString("id")%>,0,'<%=parentLocation[i].getString("office_name")%></td><td align="center" valign="middle" width="12%"><%=parentLocation[i].getString("id")%></td><td align="center" valign="middle" width="38%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delOfficeLocation(<%=parentLocation[i].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modOfficeLocation(<%=parentLocation[i].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addOfficeLocation(<%=parentLocation[i].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
		<%
				
				for(int ii=0;ii<subLocation.length;ii++)
				{
					DBRow [] childLocation = officeLocationMgr.getChildOfficeLocation(subLocation[ii].get("id",0l));
					if(childLocation.length != 0)
					{
		%>
		
		d.add(<%=subLocation[ii].getString("id")%>,<%=parentLocation[i].getString("id")%>,'<%=subLocation[ii].getString("office_name")%></td><td align="center" valign="middle" width="12%"><%=subLocation[ii].getString("id")%></td><td align="center" valign="middle" width="38%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delOfficeLocation(<%=subLocation[ii].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modOfficeLocation(<%=subLocation[ii].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addOfficeLocation(<%=subLocation[ii].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
		<%
						for(int j=0;j<childLocation.length;j++)
						{
							DBRow [] child1Location = officeLocationMgr.getChildOfficeLocation(childLocation[j].get("id",0l));
							if(child1Location.length != 0)
							{
							
		%>
		
		d.add(<%=childLocation[j].getString("id")%>,<%=subLocation[ii].getString("id")%>,'<%=childLocation[j].getString("office_name")%></td><td align="center" valign="middle" width="12%"><%=childLocation[j].getString("id")%></td><td align="center" valign="middle" width="38%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delOfficeLocation(<%=childLocation[j].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modOfficeLocation(<%=childLocation[j].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" onClick="addOfficeLocation(<%=childLocation[j].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
		
		<%
								for(int jj=0;jj<child1Location.length;jj++)
								{
		%>
		d.add(<%=child1Location[jj].getString("id")%>,<%=childLocation[j].getString("id")%>,'<%=child1Location[jj].getString("office_name")%></td><td align="center" valign="middle" width="12%"><%=child1Location[jj].getString("id")%></td><td align="center" valign="middle" width="38%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delOfficeLocation(<%=child1Location[jj].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modOfficeLocation(<%=child1Location[jj].getString("id")%>)" value="修改"></td></tr></table>','');
		<%
								}
							}
							else
							{
								
							
		%>
		
		d.add(<%=childLocation[j].getString("id")%>,<%=subLocation[ii].getString("id")%>,'<%=childLocation[j].getString("office_name")%></td><td align="center" valign="middle" width="12%"><%=childLocation[j].getString("id")%></td><td align="center" valign="middle" width="38%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delOfficeLocation(<%=childLocation[j].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modOfficeLocation(<%=childLocation[j].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" onClick="addOfficeLocation(<%=childLocation[j].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
		
		<%
							}
						}
					}
					else
					{
		%>
		
		d.add(<%=subLocation[ii].getString("id")%>,<%=parentLocation[i].getString("id")%>,'<%=subLocation[ii].getString("office_name")%></td><td align="center" valign="middle" width="12%"><%=subLocation[ii].getString("id")%></td><td align="center" valign="middle" width="38%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delOfficeLocation(<%=subLocation[ii].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modOfficeLocation(<%=subLocation[ii].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addOfficeLocation(<%=subLocation[ii].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
		<%
					}
				}
			}
			else
			{
		%>
		
			d.add(<%=parentLocation[i].getString("id")%>,0,'<%=parentLocation[i].getString("office_name")%></td><td align="center" valign="middle" width="12%"><%=parentLocation[i].getString("id")%></td><td align="center" valign="middle" width="38%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delOfficeLocation(<%=parentLocation[i].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="modOfficeLocation(<%=parentLocation[i].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" onClick="addOfficeLocation(<%=parentLocation[i].getString("id")%>)" value=" 增加子类"></td></tr></table>','');	
		<%
			}
		}
		%>

		document.write(d);

	</script>
	</td>
	</tr>
  </form>
</table>
<br>
<br>
<br>
</body>
</html>
