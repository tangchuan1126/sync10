<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="../js/select.js"></script>

	
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="JavaScript1.2">

function addAssetsCategory(parentId)
{
	$.prompt(
	
	"<div id='title'>增加子类</div><br />父类：<input name='assestParentName' type='text' id='assestParentName' style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br>新建类别<br>中文名称：<input name='assestChName' type='text' id='assestChName' style='width:300px;'><br>英文名称：<input name='assetsEnName' type='text' id='assetsEnName' style='width:300px;'><br>",
	
	{
	      submit: checkAddAssetsCategory,
   		  loaded:
		  
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/getDetailAssetsCategoryJSONAction.action",
							{id:parentId},
							function callback(data)
							{
								$("#assestParentName").val(data.chname+"["+data.enname+"]");
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/addAssetsCategoryAction.action";
						document.add_form.parentId.value = parentId;
						document.add_form.chName.value = f.assestChName;
						document.add_form.enName.value = f.assetsEnName;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function checkAddAssetsCategory(v,m,f)
{
	if (v=="y")
	{
		 if(f.proTextTitle == "")
		 {
				alert("请选填写新建分类名称");
				return false;
		 }
		 
		 return true;
	}

}

function delAssetsCategory(categoryId){
	if (confirm("确认操作吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/delAssetsCategory.action";
		document.del_form.id.value = categoryId;
		document.del_form.submit();
	}
}

function updateAssetsCategory(categoryId){
	$.prompt(
		"<div id='title'>修改分类</div><br />中文名称：<input name='assetsChName' type='text' id='assetsChName' style='width:300px;'><br>英文名称：<input name='assetsEnName' type='text' id='assetsEnName' style='width:300px;'><br>",
		{
			loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/getDetailAssetsCategoryJSONAction.action",
							{id:categoryId},
							function callback(data)
							{
								$("#assetsChName").val(data.chname);
								$("#assetsEnName").val(data.enname);
							}
					);
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/modAssetsCategory.action";
						document.mod_form.id.value = categoryId;
						document.mod_form.chName.value = f.assetsChName;
						document.mod_form.enName.value = f.assetsEnName;		
						document.mod_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 固定资产管理 »   资产类别</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="id" >

</form>

<form method="post" name="mod_form">
<input type="hidden" name="id" >
<input type="hidden" name="chName" >
<input type="hidden" name="enName">
</form>

<form method="post" name="add_form">
<input type="hidden" name="parentId">
<input type="hidden" name="chName">
<input type="hidden" name="enName">
</form>


<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
		    <input type="hidden" name="id">
		    <input type="hidden" name="parentId">
		    <input type="hidden" name="imp_color">
		
		      <tr> 
		        <th class="left-title" colspan="2">资产中文名称</th>
		        <th class="left-title" style="vertical-align: center;text-align: center;">英文名称</th>
		        <th  class="right-title" style="vertical-align: center;text-align: center;">编号</th>
		        <th width="25%"  class="right-title">&nbsp;</th>
		      </tr>
		      
		      <tr > 
			      <td height="39" colspan="2" valign="middle" >&nbsp;&nbsp;&nbsp;&nbsp;/</td>
			      <td width="8%" align="center" valign="middle"  >&nbsp;</td>
			      <td align="center" valign="middle"  >&nbsp;</td>
			      <td align="center" valign="middle"  ><input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(0)" value=" 增加子类"></td>
		    </tr>
		    <%
		
		String qx;
		int kk=0;
		DBRow treeRows[] = assetsCategoryMgr.getAllAssetsCategoryTree();
		for ( int i=0; i<treeRows.length; i++ )
		{
			
			if ( treeRows[i].get("parentid",0) != 0 )
			 {
			 	qx = "├ ";
			 }
			 else
			 {
			 	qx = "";
			 }
		%>
		    <tr > 
		      <td width="30%" height="60" valign="middle" colspan="2"> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 
			        <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
			        <%=qx%>
					
							<%
					if (treeRows[i].get("parentid",0) == 0)
					{
						out.println("<span style='font-size:14px;font-weight:bold'>"+treeRows[i].getString("chName")+"</span>");
					}
					else
					{
						out.println(treeRows[i].getString("chName"));
					}
					%>
			 </td>
			  <td class="">
			  	<%
				if (treeRows[i].get("parentid",0) == 0)
				{
					out.println("<span style='font-size:14px;font-weight:bold'>"+treeRows[i].getString("enName")+"</span>");
				}
				else
				{
					out.println(treeRows[i].getString("enName"));
				}
				%>
			  </td>
			  
			  <td width="8%" align="center" valign="middle"  > 
		        <%=treeRows[i].getString("id")%>      </td>
		      <td align="center" valign="middle" ><input name="Submit2s" type="button" class="short-short-button-del" onClick="delAssetsCategory(<%=treeRows[i].getString("id") %>)" value="删除">
			    &nbsp;&nbsp;&nbsp;&nbsp; 
		        <input name="Submit2" type="button" class="short-short-button-mod" onClick="updateAssetsCategory(<%=treeRows[i].getString("id") %>)" value="修改">
				&nbsp;&nbsp;&nbsp;&nbsp; 
				
		        <input name="Submit22" type="button" class="long-button-add" onClick="addAssetsCategory(<%=treeRows[i].getString("id")%>)" value="增加子类">
				
			</td>
		   </tr>
		    <%	
		}
		%>
	</table>
</form>

<br>
<br>
<br>
</body>
</html>
