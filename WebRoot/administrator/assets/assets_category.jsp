<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
		<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
		<script type="text/javascript" src="../js/select.js"></script>

	
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="JavaScript1.2">

function addAssetsCategory(parentId)
{
	$.prompt(
	
	"<div id='title'>增加子类</div><br />父类：<input name='assestParentName' type='text' id='assestParentName' style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br>新建类别<br>中文名称：<input name='assetsChName' type='text' id='assetsChName' style='width:300px;'><br>英文名称：<input name='assetsEnName' type='text' id='assetsEnName' style='width:300px;'><br>",
	
	{
	      submit: checkAddAssetsCategory,
   		  loaded:
		  
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/getDetailAssetsCategoryJSONAction.action",
							{id:parentId},
							function callback(data)
							{
								$("#assestParentName").val(data.chname+"["+data.enname+"]");
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/addAssetsCategoryAction.action";
						document.add_form.parentId.value = parentId;
						document.add_form.chName.value = f.assetsChName;
						document.add_form.enName.value = f.assetsEnName;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function checkAddAssetsCategory(v,m,f)
{
	if (v=="y")
	{
		 if(f.assetsChName == "")
		 {
				alert("请选填写类别中文名称");
				return false;
		 }
		 if(f.assetsEnName == "")
		 {
		 	alert("请填写类别英文名称");
		 	return false;
		 }
		 
		 return true;
	}

}

function delAssetsCategory(categoryId){
	if (confirm("确认操作吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/delAssetsCategory.action";
		document.del_form.id.value = categoryId;
		document.del_form.submit();
	}
}

function updateAssetsCategory(categoryId){
	$.prompt(
		"<div id='title'>修改分类</div><br />中文名称：<input name='assetsChName' type='text' id='assetsChName' style='width:300px;'><br>英文名称：<input name='assetsEnName' type='text' id='assetsEnName' style='width:300px;'><br>",
		{
			submit: checkAddAssetsCategory,
			loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/getDetailAssetsCategoryJSONAction.action",
							{id:categoryId},
							function callback(data)
							{
								$("#assetsChName").val(data.chname);
								$("#assetsEnName").val(data.enname);
							}
					);
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/modAssetsCategory.action";
						document.mod_form.id.value = categoryId;
						document.mod_form.chName.value = f.assetsChName;
						document.mod_form.enName.value = f.assetsEnName;		
						document.mod_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 固定资产管理 »   资产类别</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="id" >

</form>

<form method="post" name="mod_form">
<input type="hidden" name="id" >
<input type="hidden" name="chName" >
<input type="hidden" name="enName">
</form>

<form method="post" name="add_form">
<input type="hidden" name="parentId">
<input type="hidden" name="chName">
<input type="hidden" name="enName">
</form>


<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
		      <tr> 
		        <th width="45%" class="left-title" colspan="2">资产中文名称</th>
		        <th class="right-title" width="20%" style="vertical-align: center;text-align: center;">英文名称</th>
		        <th width="8%" class="right-title" style="vertical-align: center;text-align: center;">编号</th>
		        <th width="30%" class="right-title">&nbsp;</th>
		      </tr>
			
			<tr>
				<td colspan="5">
					<script type="text/javascript">
						d = new dTree('d');
						d.add(0,-1,'资产类别</td><td align="center" valign="middle" width="20%">&nbsp;</td><td align="center" valign="middle" width="8%">&nbsp;</td><td align="center" valign="middle" width="30%"><input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(0)" value=" 增加子类"></td></tr></table>');
						<%
							DBRow [] parentCategory = assetsCategoryMgr.getAssetsCategoryTree();
							for(int i=0;i<parentCategory.length;i++)
							{
								DBRow [] c1 = assetsCategoryMgr.getAssetsCategoryChildren(parentCategory[i].get("id",0l));
								if(c1.length != 0)
								{
						%>
							d.add(<%=parentCategory[i].getString("id")%>,0,'<%=parentCategory[i].getString("chName")%></td><td align="center" valign="middle" width="20%"><%=parentCategory[i].getString("enName")%></td><td align="center" valign="middle" width="8%"><%=parentCategory[i].getString("id")%></td><td align="center" valign="middle" width="30%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delAssetsCategory(<%=parentCategory[i].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateAssetsCategory(<%=parentCategory[i].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=parentCategory[i].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
						<%
								for(int ii=0;ii<c1.length;ii++)
								{
									DBRow [] c2 = assetsCategoryMgr.getAssetsCategoryChildren(c1[ii].get("id",0l));
									if(c2.length != 0)
									{
						%>
						
						d.add(<%=c1[ii].getString("id")%>,<%=parentCategory[i].getString("id")%>,'<%=c1[ii].getString("chName")%></td><td align="center" valign="middle" width="20%"><%=c1[ii].getString("enName")%></td><td align="center" valign="middle" width="8%"><%=c1[ii].getString("id")%></td><td align="center" valign="middle" width="30%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delAssetsCategory(<%=c1[ii].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateAssetsCategory(<%=c1[ii].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=c1[ii].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
						<%
										for(int j=0;j<c2.length;j++)
										{
											DBRow [] c3 = assetsCategoryMgr.getAssetsCategoryChildren(c2[j].get("id",0l));
											if(c3.length != 0)
											{
											
						%>
						
						d.add(<%=c2[j].getString("id")%>,<%=c1[ii].getString("id")%>,'<%=c2[j].getString("chName")%></td><td align="center" valign="middle" width="20%"><%=c2[j].getString("enName")%></td><td align="center" valign="middle" width="8%"><%=c2[j].getString("id")%></td><td align="center" valign="middle" width="30%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delAssetsCategory(<%=c2[j].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateAssetsCategory(<%=c2[j].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=c2[j].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
						
						<%
												for(int jj=0;jj<c3.length;jj++)
												{
						%>
						d.add(<%=c3[jj].getString("id")%>,<%=c2[j].getString("id")%>,'<%=c3[jj].getString("chName")%></td><td align="center" valign="middle" width="20%"><%=c3[jj].getString("enName")%></td><td align="center" valign="middle" width="8%"><%=c3[jj].getString("id")%></td><td align="center" valign="middle" width="30%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delAssetsCategory(<%=c3[jj].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateAssetsCategory(<%=c3[jj].getString("id")%>)" value="修改"></td></tr></table>','');
						<%
												}
											}
											else
											{
												
											
						%>
						
						d.add(<%=c2[j].getString("id")%>,<%=c1[ii].getString("id")%>,'<%=c2[j].getString("chName")%></td><td align="center" valign="middle" width="20%"><%=c2[j].getString("enName")%></td><td align="center" valign="middle" width="8%"><%=c2[j].getString("id")%></td><td align="center" valign="middle" width="30%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delAssetsCategory(<%=c2[j].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateAssetsCategory(<%=c2[j].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=c2[j].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
						
						<%
											}
										}
									}
									else
									{
						%>
						
						d.add(<%=c1[ii].getString("id")%>,<%=parentCategory[i].getString("id")%>,'<%=c1[ii].getString("chName")%></td><td align="center" valign="middle" width="20%"><%=c1[ii].getString("enName")%></td><td align="center" valign="middle" width="8%"><%=c1[ii].getString("id")%></td><td align="center" valign="middle" width="30%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delAssetsCategory(<%=c1[ii].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateAssetsCategory(<%=c1[ii].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp; <input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=c1[ii].getString("id")%>)" value=" 增加子类"></td></tr></table>','');
						<%
									}
								}
							}
							else
							{
						%>
						
							d.add(<%=parentCategory[i].getString("id")%>,0,'<%=parentCategory[i].getString("chName")%></td><td align="center" width="20%"><%=parentCategory[i].getString("enName")%></td><td align="center" valign="middle" width="8%"><%=parentCategory[i].getString("id")%></td><td align="center" valign="middle" width="30%"><input name="Submit32" type="button" class="short-short-button-del" onClick="delAssetsCategory(<%=parentCategory[i].getString("id")%>)" value="删除">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit3" type="button" class="short-short-button-mod" onClick="updateAssetsCategory(<%=parentCategory[i].getString("id")%>)" value="修改">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" onClick="addAssetsCategory(<%=parentCategory[i].getString("id")%>)" value=" 增加子类"></td></tr></table>','');	
						<%
							}
						}
						%>
				
						document.write(d);
				
					</script>
				</td>
			</tr>
	</table>
</form>

<br>
<br>
<br>
</body>
</html>
