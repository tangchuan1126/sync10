<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyStatusKey"%>
<%@page import="com.cwc.app.api.zzz.ApplyMoneyMgrZZZ"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%@page import="java.util.List"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.GoodsStatusKey"%>
<%@page import="com.cwc.app.key.GoodsCaseKey"%>
<%@page import="com.cwc.app.key.GoodsApplyStatusKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@ page import="com.cwc.app.key.FileWithTypeKey"%>
<%
	long acid = StringUtil.getLong(request,"acid");
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
long lid = StringUtil.getLong(request,"lid");
long productLine = StringUtil.getLong(request,"productLine");
int state = StringUtil.getInt(request,"state",-1);
int goodsState = StringUtil.getInt(request,"goodsStatus",-1);
String type = StringUtil.getString(request,"type").equals("")?"2":StringUtil.getString(request,"type");
int types= StringUtil.getInt(request,"types")==0?2:StringUtil.getInt(request,"types");
String money = StringUtil.getString(request,"money");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
ApplyStatusKey status=new ApplyStatusKey(); 

DBRow rows[];
if (cmd.equals("filter"))
{
	rows = assetsMgr.getAssetsByCategoryIdAndLocationId(state,goodsState,lid,100023,pc,productLine,type);
}
else if (cmd.equals("search"))
{
	rows = assetsMgr.getSearchAssets(key,pc,type);
}
else
{
	rows = assetsMgr.AssetsList(pc,type);
}	
Tree tree = new Tree(ConfigBean.getStringValue("product_line_define"));
Tree locaton_tree = new Tree(ConfigBean.getStringValue("office_location"));

DBRow applyMoney[]= null;

GoodsCaseKey goodsCaseKey=new GoodsCaseKey();
GoodsStatusKey goodsStatusKey=new GoodsStatusKey();
GoodsApplyStatusKey applyStatusKey = new GoodsApplyStatusKey();
FundStatusKey fundStatusKey = new FundStatusKey();

String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>样品清单</title>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- add -->
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- 文件预览  -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>

<script language="javascript">
function exportAssets()
{
	var location_id=$("#filter_lid").val();
	var product_line_id=$("#productLine").val();
	var state=$("#state").val();
	var goodsState=$("#goodsStatus").val();
	var t="2";
	var para = "location_id="+location_id+"&product_line_id="+product_line_id+"&state="+ state+"&goodsState="+goodsState+"&type="+t;
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/exportAssets.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){
					if(date["canexport"]=="true")
					{
						document.listForm.action=date["fileurl"];
						document.listForm.submit();
					}
					else
					{
						alert("该分类与子类下无所选形式资产，无法导出");
					}
				}
			});
}



$(document).ready(function() {
	if($("#search_key").val()=="*样品名称,编号,领用人")
	{
	 	 $("#search_key").attr("class","searchbarGray");
	}
	else
	{
		 $("#search_key").removeAttr("class");
	}

	$("#state").val("<%=state%>");
	function log(event, data, formatted) {
		
	}

	$("#search_key").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/getSearchAssetsJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		selectFirst: false,
		updownSelect:false,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},
		
				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].a_name,
    				value:data[i].a_name+"["+data[i].a_barcode+"]",
        			result:data[i].a_name
				};    
			  }   
			
		  	 return rows;   
			 },
		
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	$("#search_key").keydown(function(event){
			if(event.keyCode == 13)
			{
				search();
			}
		})
});



function filter()
{
		document.filter_search_form.cmd.value = "filter";		
		document.filter_search_form.acid.value = "100023";
		document.filter_search_form.lid.value = $("#filter_lid").val();
		document.filter_search_form.state.value = $("#state").val();
		document.filter_search_form.goodsStatus.value = $("#goodsStatus").val();
		document.filter_search_form.productLine.value = $("#productLine").val();
		document.filter_search_form.submit();
}

function search()
{
	if (checkForm()) {
		document.filter_search_form.cmd.value = "search";
		document.filter_search_form.key.value = $("#search_key").val();		
		document.filter_search_form.submit();
	}
}

//弹出dialog窗口申请样品资金
function addSample(){

	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/financial_management/add_sample.html"; 
    $.artDialog.open(uri , {title: "创建样品",width:'750px',height:'500px', lock: true,opacity: 0.3,fixed: true});

} 

function modAssets(aid)
{
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/assets/mod_sample.html?aid="+aid; 
    $.artDialog.open(uri , {title: "修改样品信息",width:'700px',height:'350px', lock: true,opacity: 0.3,fixed: true});

}

function modRecipients(aid)
{
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/assets/mod_recipients_sample.html?aid="+aid; 
    $.artDialog.open(uri , {title: "样品领用",width:'700px',height:'330px', lock: true,opacity: 0.3,fixed: true});
	
}

function receiveAssets(aid)
{
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/assets/receive_sample.html?aid="+aid; 
    $.artDialog.open(uri , {title: "收货",width:'650px',height:'400px', lock: true,opacity: 0.3,fixed: true});
}

function switchCheckboxAction()
{
	$('input[name=switchCheckboxPid]').inverse();
}

function getAllChecked()
{
	return($('#switchCheckboxPid').checkbox());
}

function batchMod()
{
	var aids = getAllChecked();
	
	if ( aids=="" )
	{
		alert("最少选择一个资产信息");
	}
	else
	{
		
		tb_show('批量修改资产信息','batch_mod_assets.html?aids='+aids+'&TB_iframe=true&height=600&width=800',false);
	}
	
}

function addAssets()
{
	var id = $("#filter_acid").val();
	if (id==0)
	{
		alert("请选择一个资产类别");
		openCatalogMenu();
	}
	else
	{
		tb_show('增加资产','add_assets.html?category_id='+id+'&TB_iframe=true&height=350&width=700',false);
	}
}

function windowRefresh(){
	window.location.reload();
}
function closeWin()
{
	$.artDialog.close();
}

function closeWinNotRefresh()
{
	tb_remove();
}

function closeWinRefresh()
{
	window.location.reload();
	$.artDialog.close();
}
function openCatalogMenu()
{
	$("#category").mcDropdown("#categorymenu").openMenu();
}

function print(aid)
{
	document.print_form.aid.value = aid; 
	document.print_form.submit();
}

function checkForm()
{
      if($("#search_key").val().trim()==""||$("#search_key").val().trim()=="*样品名称,编号,领用人"||$("#search_key").val()==null)
      {
         alert("请输入查询信息！");
         $("#search_key").focus();
         return false;
      }
      
      return true;
}


function changeColorBlack(){

	if($("#search_key").val()=="*样品名称,编号,领用人")
	{
 	 	 
 	 	 $("#search_key").val("");
 	 	 $("#search_key").removeAttr("class");
 	}
 	
}

function changeColorGray()
{
	 
	 if($("#search_key").val().trim()=="")	  
	 {
		  $("#search_key").val("*样品名称,编号,领用人");
		  $("#search_key").attr("class","searchbarGray");
	 }
}
  
function init()
{
 	if($("#search_key").val()=="*样品名称,编号,领用人")
	{	 	 	 	 		
	 	 $("#search_key").attr("class","searchbarGray");
	}
	else
	{		
		 $("#search_key").removeAttr("class");
	}
 
}
//出售
function saleAssets(aid)
{
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/assets/sale_sample.html?aid="+aid; 
    $.artDialog.open(uri , {title: "出售",width:'700px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}

//退回
function returnAssets(aid)
{
	var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/assets/return_sample.html?aid="+aid; 
	   $.artDialog.open(uri , {title: "退回",width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}
//换货
function exchangeAssets(aid)
{
      var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/assets/exchange_sample.html?aid="+aid; 
	   $.artDialog.open(uri , {title: "换货",width:'500px',height:'200px', lock: true,opacity: 0.3,fixed: true});
	
}
//导入样品
function importSample(type)
{
	 var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/assets/assets_upload_excel.html?type="+type; 
	   $.artDialog.open(uri , {title: "上传样品信息",width:'1000px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}
//收货凭证
function receiveAssetsProof(aid){
   var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/assets/receive_proof.html?aid="+aid; 
    $.artDialog.open(uri , {title: "收货凭证",width:'650px',height:'400px', lock: true,opacity: 0.3,fixed: true});
 
}
</script>
<script>
	function goApplyFundsPage(id) {
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=query&apply_id="+id);
	}
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
	jQuery(function($){
		onLoadInitZebraTable();
	})
	
	//file表中的图片在线显示	
function showPictrueOnline(fileWithType,fileWithId , currentName){
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_receive")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}	
	
</script>
<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
<style>

.zebraTable tr td a:visited {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:hover {
	color: #000000;
	text-decoration: underline;
	font-weight:bold;
	font-size:13px;
}
.zebraTable tr td a:active {
	color: #000000;
	text-decoration: none;
	font-weight:bold;
	font-size:13px;
}
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
.input-line
{
	width:200px;
	font-size:12px;
}
body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}
.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}
.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}
.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}
form
{
	padding:0px;
	margin:0px;
}
.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}
			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
			}
			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;	`
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
 .searchbarGray{
        color:#c4c4c4;font-family: Arial
   }
  .set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
 span.InfoLeft{width:85px;display:block;float:left;text-align:right;border:0px solid red;}
 span.InfoRight{display:block;text-align:left;float:left;text-indent:5px;}
 span.InfoRight2{width:65px;text-align:left;float:left;text-indent:2px;}
 p{clear:both;text-align:center;}
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 固定资产  »    样品清单</td>
  </tr>
</table>
<br>

<form name="del_form" method="post">
<input type="hidden" name="a_id">
</form>

<form name="mod_form" method="post">

<input type="hidden" name="a_id">
<input type="hidden" name="a_name">
<input type="hidden" name="category_id">
<input type="hidden" name="product_line_id">
<input type="hidden" name="a_barcode">
<input type="hidden" name="unit_name">
<input type="hidden" name="unit_price">
<input type="hidden" name="standard">
<input type="hidden" name="purchase_date">
<input type="hidden" name="suppliers">
<input type="hidden" name="location">
<input type="hidden" name="requisitioned_id">
<input type="hidden" name="requisitioned">
<input type="hidden" name="state">
<input type="hidden" name="center_account_id"  />
<input type="hidden" name="goodsState"  />
<input type="hidden" name="applyState" />
<input type="hidden" name="handle_time" />
<input type="hidden" name="handle_price"/>
<input type="hidden" name="responsi_person"/>
<input type="hidden" name="return_name"/>
<input type="hidden" name="receive_time"/>
<input type="hidden" name="creater_id"/>
<input type="hidden" name="requisition_date"/>
<input type="hidden" name="consignee"/>
<input type="hidden" name="currency"/>
<input type="hidden" name="sn">
<input type="hidden" name="file_name">
<input type="hidden" name="file_with_type">
<input type="hidden" name="path">

</form>

<form name="filter_search_form" method="get"  action="sample_list.html">
<input type="hidden" name="cmd" >
<input type="hidden" name="acid" >
<input type="hidden" name="key" >
<input type="hidden" name="lid"> 
<input type="hidden" name="state" />
<input type="hidden" name="goodsStatus" />
<input type="hidden" name="productLine" />
</form>

<table width="98%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <td width="99%">
	<div class="demo" width="98%">

	<div id="tabs"> 
		<ul>
				<li><a href="#usual_tools">常用工具</a></li>
				<li><a href="#advan_search">高级搜索</a></li>		 
		</ul>
		
		<div id="usual_tools">
			<input type="text" name="search_key" id="search_key" onblur="changeColorGray()" onclick="changeColorBlack()" value="*样品名称,编号,领用人" style="width:200px;" >
			&nbsp;&nbsp;&nbsp;
			<input name="Submit4" type="button" class="button_long_search" value="搜索" onClick="search()" />
			&nbsp;&nbsp;&nbsp;
			<input type="button" class="long-long-button-add" value="申请样品"  onclick="addSample();"/>
		</div>
		
		<div id="advan_search">
<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td width="30%" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
     <ul id="productLinemenu" class="mcdropdown_menu">
    			<li rel="0">所有产品线</li>
					  <%
		
					  	  DBRow ca1[] = assetsCategoryMgr.getAssetsCategoryChildren(0,"2");
						  for (int i=0; i<ca1.length; i++)
						  {
								//out.println("<li onmousemove='catagory("+ca1[i].get("id",0l)+")' value='"+ca1[i].get("id",0l)+"'><a href='"+ca1[i].get("id",0l)+"'> "+ca1[i].getString("chName")+" </a>");
								  DBRow[] ca2 = null;
								  	ca2 = assetsCategoryMgr.getAssetsCategoryChildren(ca1[i].get("id",0l),"3");
								 
								  for (int ii=0; ii<ca2.length; ii++)
								  {
										out.println("<li rel='"+ca2[ii].get("id",0l)+"'> "+ca2[ii].getString("chName"));		
										
										out.println("</li>");				
								  }
							
					  }
					  %>
				</ul>
					  <input type="text" name="productLine" id="productLine" value="" />
					  <input type="hidden" name="product_line_id" id="product_line_id" value="0" />

       <script>
			$("#productLine").mcDropdown("#productLinemenu",{
								allowParentSelect:true,
								  select: 
								  
										function (id,name)
										{
											$("#product_line_id").val(id);
											//alert($("#filter_lid1").val());
										}
						
						});								
			$("#productLine").mcDropdown("#productLinemenu").setValue(0);
		
			<%
			//System.out.println(productLine);
			if (productLine>0)
			{
			%>
			$("#productLine").mcDropdown("#productLinemenu").setValue(<%=productLine%>);
			<%
			}
			else
			{
			%>
			$("#productLine").mcDropdown("#productLinemenu").setValue(0);
			<%
			}
			%>
		</script>
			
    </td>
  	<td width="27%" height="29" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid"></td>
  	<td width="43%" height="29" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">
  	<select id="state" >
      <option value="-1">好坏情况</option>
      <option value="<%=goodsCaseKey.intact %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.intact) %></option>
      <option value="<%=goodsCaseKey.function_damage %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.function_damage) %></option>
      <option value="<%=goodsCaseKey.facade_damage %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.facade_damage) %></option>
      <option value="<%=goodsCaseKey.scrap %>"><%=goodsCaseKey.getGoodsCaseById(goodsCaseKey.scrap) %></option>
     </select>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     
     <select id="goodsStatus">
      <option value="-1">货物状态 </option>
      <option value="<%=goodsStatusKey.no_arrival %>"><%=goodsStatusKey.getGoodsStatusById(goodsStatusKey.no_arrival) %></option>
      <option value="<%=goodsStatusKey.arrival %>"><%=goodsStatusKey.getGoodsStatusById(goodsStatusKey.arrival) %></option>
      <option value="<%=goodsStatusKey.sell %>"><%=goodsStatusKey.getGoodsStatusById(goodsStatusKey.sell) %></option>
      <option value="<%=goodsStatusKey.retreat %>"><%=goodsStatusKey.getGoodsStatusById(goodsStatusKey.retreat) %></option>
      <option value="<%=goodsStatusKey.barter %>"><%=goodsStatusKey.getGoodsStatusById(goodsStatusKey.barter) %></option>
     </select>
  	</td>
  </tr>
  <tr>
  	<td width="434" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
		<ul id="locationmenu" class="mcdropdown_menu">
		  <li rel="0">所有办公地点</li>
		  <%
		  DBRow l1[] = officeLocationMgr.getChildOfficeLocation(0);
		  for (int i=0; i<l1.length; i++)
		  {
				out.println("<li rel='"+l1[i].get("id",0l)+"'> "+l1[i].getString("office_name"));
	
				  DBRow l2[] = officeLocationMgr.getChildOfficeLocation(l1[i].get("id",0l));
				  if (l2.length>0)
				  {
						out.println("<ul>");	
				  }
				  for (int ii=0; ii<l2.length; ii++)
				  {
						out.println("<li rel='"+l2[ii].get("id",0l)+"'> "+l2[ii].getString("office_name"));		
						
							DBRow l3[] = officeLocationMgr.getChildOfficeLocation(l2[ii].get("id",0l));
							  if (l3.length>0)
							  {
									out.println("<ul>");	
							  }
								for (int iii=0; iii<l3.length; iii++)
								{
										out.println("<li rel='"+l3[iii].get("id",0l)+"'> "+l3[iii].getString("office_name"));
										out.println("</li>");
								}
							  if (l3.length>0)
							  {
									out.println("</ul>");
							  }
							  
						out.println("</li>");				
				  }
				  if (l2.length>0)
				  {
						out.println("</ul>");	
				  }
				  
				out.println("</li>");
		  }
		  %>
	</ul>
	    <input name="location" id="location" type="text" value=""/>	
		<input type="hidden" name="filter_lid" id="filter_lid" value="0" />
		
		<script>
		<!--
			$("#location").mcDropdown("#locationmenu",{
					allowParentSelect:true,
					  select: 
					  
							function (id,name)
							{
								$("#filter_lid").val(id);
							}
			
			});
			
			<%
			if (lid>0)
			{
			%>
			$("#location").mcDropdown("#locationmenu").setValue(<%=lid%>);
			<%
			}
			else
			{
			%>
			$("#location").mcDropdown("#locationmenu").setValue(0);
			<%
			}
			%>
				function jumpApplyFunds(assets_id)
			{
				var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/financial_management/assets_apply_funds.html?type=2&assets_id="+assets_id; 
			    $.artDialog.open(uri , {title: "固定或样品资产申请",width:'750px',height:'500px', lock: true,opacity: 0.3,fixed: true});
			
			}
			//-->
		</script>
	</td>
	<td width="27%" height="29" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid"></td>
    <td width="43%" height="29" bgcolor="#eeeeee">
     <input name="Submit2" type="button" class="button_long_refresh" value="过滤" onClick="filter()">&nbsp;&nbsp;&nbsp;&nbsp;
     <input name="import/import" type="button" class="long-button" value="样品导入" onClick="importSample(<%=type %>)">&nbsp;&nbsp;&nbsp;
     <input name="import/export" type="button" class="long-button" value="样品导出" onClick="exportAssets()">&nbsp;&nbsp;&nbsp;
 </td>
  </tr>
</table>
		</div>
	</div>
	</div>
<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>

	</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
  <tr>
    <td></td>
  </tr>
</table>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" isNeed="true" class="zebraTable" isBottom="true">
	    <tr> 
          <th width="3%" align="center" valign="middle"  class="left-title"><input type="checkbox" name="switchCheckbox" id="switchCheckbox" value="checkbox" onClick="switchCheckboxAction()"></th>
			<th width="23%" class="right-title" style="text-align:center">序列号</th>
	        <th width="22%" align="center"  class="right-title" style="vertical-align: center;text-align: center;">产品信息</th>
	        <th width="26%"  class="right-title" style="vertical-align: center;text-align: center;">货物信息</th>
			<th width="29%"  class="right-title" style="vertical-align: center;text-align: center;">资金信息</th>
	    </tr>
	
	    <%
	for ( int i=0; i<rows.length; i++ )
	{	
	%>
	    <tr  > 
	      <td height="60" align="center" valign="middle">
	      <input type="checkbox" id="switchCheckboxPid" name="switchCheckboxPid" value="<%=rows[i].get("aid",0l)%>"></td>     
		  <td style='word-break:break-all;' align="center">
		  
		  <fieldset  class="set" style="border:2px solid #993300;text-align:left；width:150px;">
		  		<legend>
					<span class="title">
						<%
		  if(rows[i].get("applystate",0) == Integer.parseInt(applyStatusKey.no_apply))
		  {
		  %>
		  	<a href="javascript:void(0)" onclick="jumpApplyFunds('<%=rows[i].getString("aid") %>','<%=rows[i].getString("currency") %>')"><%=rows[i].getString("aid") %></a>		  
		  <%
		  }
		  else if(rows[i].get("applystate",0)==Integer.parseInt(applyStatusKey.norequired_apply))
		  {
		   %>
		  <a href="javascript:void(0)" onclick="alert('该资产无需申请资金!');	" ><%=rows[i].getString("aid") %></a>		  
		   <%
		  }
		  else
		  {
		   %>
		  <a href="javascript:void(0)" onclick="alert('该资产已申请资金!');	" ><%=rows[i].getString("aid") %></a>		  
		   <%
		  }
		  %>
					</span>
				</legend>
				<p><span class="alert-text InfoLeft">申请人：</span>
				<span class="InfoRight">
				<%
					if(rows[i].get("creater_id",0l)!=0){
						out.print(applyMoneyMgrLL.getApplyMoneyCreate(rows[i].getString("creater_id")).getString("employe_name"));
					}else{
						out.print("");
					}					
				%>
				</span></p>
				<p><span class="alert-text InfoLeft">商品价值：</span>
				<span class="InfoRight">
					<%=rows[i].get("unit_price",0d)%>/<%=rows[i].getString("unit_name")%>
				</span></p>
					<p><span class="alert-text InfoLeft">申请时间：</span>
				<span class="InfoRight"><%=rows[i].getString("purchase_date").equals("")?"":DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("purchase_date")))%></span></p>
  	</fieldset>				   
		  </td>
	      <td align="left" valign="middle" style="padding-top:10px;padding-bottom:10px;">
	          <fieldset style="border:2px #999999 solid;padding:2px;width:94%;word-break:break-all;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;padding-bottom:4px;text-align: left;">
	          <legend>
	          <span style="font-size:12px;">清单</span>
	          </legend>
	          <%
	          String[] a_name=null;
	          a_name=rows[i].getString("a_name").split(",");
	         for(int j=0;j<a_name.length;j++){
	          %>
	         <p>
	         <%
	         String[] splitModel=null;
	         String nameModel=a_name[j];
	         splitModel=nameModel.split("/");
	         %>
	         <span class="alert-text InfoLeft">商品名称<%=j+1 %>：</span>
	         <span class="InfoRight"><%=splitModel[0] %></span></p>
	       <p>  <span class="alert-text InfoLeft">型号<%=j+1 %>：</span> 
	         <% if(splitModel.length>1) {%>
	         <span  class="InfoRight"><%=splitModel[1] %></span>
	         <%}  else{%>
	          <span  class="InfoRight"><%=rows[i].getString("standard") %></span>
	         <%} %>
	         </p>
	         <%
	         }
	         %>
	         
	         </fieldset>
	      
	      <p>
			<span class="alert-text InfoLeft">产品线:</span>
	      
		  <span class="InfoRight">
		  	 <%
			  DBRow allFather[] = assetsCategoryMgr.getAssetsProductLine(StringUtil.getLong(rows[i].getString("product_line_id")));
			  for (int jj=0; jj<allFather.length; jj++)
			  {
			  	out.println("<a class='nine4' href='?cmd=filter&productLine="+allFather[jj].getString("id")+"'>"+allFather[jj].getString("name")+"</a>");			
			  }
			  %>&nbsp;
		  </span>
		</p>
		  <p>
			<span class="alert-text InfoLeft">供应商:</span>
			<span class="InfoRight"><%=rows[i].getString("suppliers")%>
	<input type="hidden" name="money" id="money" value="<%=money %>">
			</span>
		</p>
		  <p>
			<span class="alert-text InfoLeft">存放地点:</span>
			<span class="InfoRight">
				<span style="color:#999999">
		  	 <%
			  DBRow allLocationFather[] = locaton_tree.getAllFather(rows[i].get("location_id",0l));
			  for (int k=0; k<allLocationFather.length-1; k++)
			  {
			  	out.println("<a class='nine4' href='?cmd=filter&lid="+allLocationFather[k].getString("id")+"'>"+allLocationFather[k].getString("office_name")+"</a>");
			
			  }
			  %>&nbsp;
		  </span>
		  
		  <%
		   DBRow location = officeLocationMgr.getDetailOfficeLocation(StringUtil.getLong(rows[i].getString("location_id")));
		  if (location!=null)
		  {
		  	out.println("<a href='?cmd=filter&lid="+rows[i].getString("location_id")+"'>"+location.getString("office_name")+"</a>");
		  }
		  %>
		  </span>
		  &nbsp;
			</span>
		</p>
		  
		  </td>
	      <td align="center" valign="middle"  >
	      
	      <fieldset class="set" style="padding-bottom:4px;text-align: left;">
			<legend  style="font-size:12px;font-weight:200px;">好坏情况：
				<% 
	      	if(rows[i].getString("state").equals(goodsCaseKey.intact)){
	      		
	      		out.println(goodsCaseKey.getGoodsCaseById(goodsCaseKey.intact));
	      		
	      	}else if(rows[i].getString("state").equals(goodsCaseKey.function_damage)){
	      		
	      		out.println(goodsCaseKey.getGoodsCaseById(goodsCaseKey.function_damage));
	      		
	      	}else if(rows[i].getString("state").equals(goodsCaseKey.facade_damage))
	      	{
	      		out.println(goodsCaseKey.getGoodsCaseById(goodsCaseKey.facade_damage));
	      		
	      	}else if(rows[i].getString("state").equals(goodsCaseKey.scrap))
	      	{
	      		out.println(goodsCaseKey.getGoodsCaseById(goodsCaseKey.scrap));
	      	}
	      	%>
	      	 </legend>
	      	<p><span class="alert-text InfoLeft">领用人:</span>
				<span class="InfoRight"><%=rows[i].getString("requisitioned")%></span></p>
		    <p><span class="alert-text InfoLeft">领用时间:</span>
				<span class="InfoRight"><%=rows[i].getString("requisition_date").equals("")?"":DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("requisition_date")))%></span></p>
		</fieldset>
	      
	       <fieldset class="set" style="padding-bottom:4px;text-align: left;">
			<legend style="font-size:12px;font-weight:200px;">货物状态：
				<% 
	       if(rows[i].getString("goodsState").equals(goodsStatusKey.no_arrival)) 
	      	{
	      		out.println(goodsStatusKey.getGoodsStatusById(goodsStatusKey.no_arrival));
	      		
	      	}
	      	else if(rows[i].getString("goodsState").equals(goodsStatusKey.arrival))
	      	{
	      		out.println(goodsStatusKey.getGoodsStatusById(goodsStatusKey.arrival));
	      		
	      	}else if(rows[i].getString("goodsState").equals(goodsStatusKey.sell))
	      	{
	      		out.println(goodsStatusKey.getGoodsStatusById(goodsStatusKey.sell));
	      		
	      	}else if(rows[i].getString("goodsState").equals(goodsStatusKey.retreat))
	      	{
	      		out.println(goodsStatusKey.getGoodsStatusById(goodsStatusKey.retreat));
	      		
	      	}else if(rows[i].getString("goodsState").equals(goodsStatusKey.barter))
	      	{
	      		out.println(goodsStatusKey.getGoodsStatusById(goodsStatusKey.barter));
	      	}
	      	%></legend>
	      	<% 
	      	if(rows[i].getString("goodsState").equals(goodsStatusKey.arrival)){
	      		%>
	      	<p><span class="alert-text InfoLeft">收货人:</span>
				<span class="InfoRight"><%=rows[i].getString("consignee")==null?"":rows[i].getString("consignee")%></span></p>
		    <p><span class="alert-text InfoLeft">收货时间:</span>
				<span class="InfoRight"><%=rows[i].getString("receive_time").equals("")?"":DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("receive_time")))%></span></p>
	      	<p><span class="alert-text InfoLeft">凭证:</span>
			<span style="width:50px;"></span>
				<span>
				 <%
		 		// 如果是完成应该在下面显示文件然后支持下载链接
		 			 DBRow[] fileRows = transportMgrZr.getFileByFilwWidthIdAndFileType("file",rows[i].get("aid",0),FileWithTypeKey.assets_receive );
		 				if(fileRows != null && fileRows.length > 0 ){
		 			 %>
		 			 <br/>
		 			<div id="over_file_td">
		 			 	 <%for(DBRow fileRow : fileRows){ %>
		 			 	 
		 			 	  <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
		 			 	 	<%if(StringUtil.isPictureFile(fileRow.getString("file_name"))){ %>
				 			 	 <p>
				 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.assets_receive %>','<%=rows[i].get("aid",0l) %>','<%=fileRow.getString("file_name") %>');"><%=fileRow.getString("file_name") %></a>
				 			 	 </p>
			 			 	 <%} else{ %>
 		 			 	 	  		<p><a href='<%= downLoadFileAction%>?file_name=<%=fileRow.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_receive")%>'><%=fileRow.getString("file_name") %></a></p> <br/>
 		 			 	 	  <%} %>
		 			 	 <%} %>
		 			 </div>
		 			 <%
		 			 }else{
		 	%>
		 			 	<span class="InfoRight">凭证未上传！</span>
		 			 	<%} %>
				</span></p>
	      <%} %>
	      
	      	<% 
	      	if(rows[i].getString("goodsState").equals(goodsStatusKey.sell)){
	      		%>
	      	<p><span class="alert-text InfoLeft">收款人:</span>
				<span class="InfoRight"><%=rows[i].getString("responsi_person").equals("null")||rows[i].getString("responsi_person")==null? "":rows[i].getString("responsi_person")%></span></p>
		    <p><span class="alert-text InfoLeft">出售时间:</span>
				<span class="InfoRight"><%=rows[i].getString("handle_time").equals("")?"":DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("handle_time")))%></span></p>
				<p><span class="alert-text InfoLeft">出售价格:</span>
				<span class="InfoRight"><%=rows[i].getValue("handle_price")==null?"":rows[i].getValue("handle_price")%></span></p>
	      <%} %>
	      <% 
	      	if(rows[i].getString("goodsState").equals(goodsStatusKey.retreat)){
	      		%>
	      	<p><span class="alert-text InfoLeft">负责人:</span>
				<span class="InfoRight"><%=rows[i].getString("responsi_person")==null?"":rows[i].getString("responsi_person")%></span></p>
		    <p><span class="alert-text InfoLeft">退货时间:</span>
				<span class="InfoRight"><%=rows[i].getString("handle_time").equals("")?"":DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("handle_time")))%></span></p>
				<p><span class="alert-text InfoLeft">退款额:</span>
				<span class="InfoRight"><%=rows[i].getValue("handle_price")==null?"":rows[i].getValue("handle_price")%></span></p>
	      <%} %>
	      
	      <% 
	      	if(rows[i].getString("goodsState").equals(goodsStatusKey.barter)){
	      		%>
	      	<p><span class="alert-text InfoLeft">已换商品名称:</span>
				<span class="InfoRight"><%=rows[i].getString("return_name")==null?"":rows[i].getString("return_name")%></span></p>
		    <p><span class="alert-text InfoLeft">换货时间:</span>
				<span class="InfoRight"><%=rows[i].getString("handle_time").equals("")?"":DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("handle_time")))%></span></p>
	      <%} %>
		</fieldset>
		
	      </td>
	      <td align="center" valign="middle">
	       <fieldset class="set" style="border-color:#993300;">
			<%
				DBRow[] applyMoneys = applyMoneyMgrLL.getApplyMoneyByBusiness(rows[i].getString("aid"),3);//
			%>
			<legend style="font-size:12px;font-weight:normal;color:#000000;background:none;">
				<span style="font-size:12px;color:#000000;font-weight:normal;">
				<%=rows[i].getString("aid")%>样品资金&nbsp;|&nbsp;
				<% 
			 if(rows[i].get("applystate",0)== Integer.parseInt(applyStatusKey.no_apply))
	      	{
	      		out.print(applyStatusKey.getGoodsApplyStatusById(applyStatusKey.no_apply));
	      		
	      	}else if(rows[i].get("applystate",0) == Integer.parseInt(applyStatusKey.norequired_apply)){
	      		out.print(applyStatusKey.getGoodsApplyStatusById(applyStatusKey.norequired_apply));
	      	}else if(rows[i].getString("applystate").equals(applyStatusKey.already_apply)){
	      	%>
			<%
					String statusName = "";
					if(applyMoneys.length > 0)
					{
						List imageList = applyMoneyMgrLL.getImageList(applyMoneys[0].getString("apply_id"),"1");
						//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
					 	if((String.valueOf(fundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneys[0].getString("status")) && imageList.size() < 1)
					 	{
					 		statusName = fundStatusKey.getFundStatusKeyNameById(fundStatusKey.CANNOT_APPLY_TRANSFER+"");
					 	}else{
					 		statusName = fundStatusKey.getFundStatusKeyNameById(applyMoneys[0].getString("status"));	
					 	}
					 	statusName = "("+statusName+")";
					}
				%>
				<%=statusName %>
				<%} %>
				</span>
			</legend>
			<table style="width:100%">				
				<%
				if(rows[i].getString("applystate").equals(applyStatusKey.already_apply)){
	  				if(applyMoneys.length>0) {
	  					String strTemp = applyMoneys[0].get("status",0) == 2 ? "转完":"申请";
	  					String moneyStandardStr = "";
	  					if(!"RMB".equals(applyMoneys[0].getString("currency")))
	  					{
	  						moneyStandardStr = "/"+applyMoneys[0].getString("standard_money")+"RMB";
	  					}
		  				out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goApplyFundsPage('"+applyMoneys[0].get("apply_id",0l)+"')\">F"+applyMoneys[0].get("apply_id",0)+"</a>"+ strTemp +applyMoneys[0].get("amount",0f)+applyMoneys[0].getString("currency")+moneyStandardStr+"</td></tr>");
		  			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByBusiness(rows[i].getString("aid"),3);
		  			for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
		  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
		  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
		  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
		  				int moneyStatus = applyTransferRows[ii].get("status",0);
		  				String transferMoneyStandardStr = "";
		  				if(!"RMB".equals(currency))
		  				{
		  					transferMoneyStandardStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
		  				}
		  				if(moneyStatus != 0 )
			  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
		  				else
			  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
			  			}
		  			}
				}
	  		%>	  				
			</table>
			</fieldset>
	      </td>   
	    </tr>
	    <tr class="split">
	     <td colspan="8" style="padding-top:1px;text-align:right;">
 <% 
			if(rows[i].getString("applystate").equals(applyStatusKey.no_apply))
		 	 {
		%>
	        <input type="button" class="short-button-ok" value=" 申请资金" onclick="jumpApplyFunds('<%=rows[i].getString("aid") %>')"/>
	      <%} %>  
	        <input type="button" class="short-short-button-print" value="打印" onClick="print(<%=rows[i].getString("aid")%>)"/>&nbsp;&nbsp;
			<input type="button" class="short-short-button-mod" value="领用" onClick="modRecipients(<%=rows[i].getString("aid") %>)"/>
	        <input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="modAssets(<%=rows[i].getString("aid")%>)">
	        <% if(rows[i].get("goodsState",0) == 1){
	        %>	
	        <input name="Submit32" type="button" class="short-short-button" value="收货" onClick="receiveAssets(<%=rows[i].getString("aid")%>)">
	        <%} %>
	       
	        <% if(rows[i].get("goodsState",0) == 2){
	        %>	
	        <input name="Submit32" type="button" class="short-button" value="收货凭证" onClick="receiveAssetsProof(<%=rows[i].getString("aid")%>)">
	         <input name="Submit3" type="button"  class="short-short-button-ok" value="售出" onClick="saleAssets(<%=rows[i].getString("aid")%>)">
	        <input name="Submit32" type="button" class="short-short-button-return" value="退货" onClick="returnAssets(<%=rows[i].getString("aid")%>)">
	         <input name="Submit32" type="button" class="short-short-button-convert" value="换货" onClick="exchangeAssets(<%=rows[i].getString("aid")%>)">
	         <%} %>
	        </td>
	    </tr>
	    <%
	}
	%>	  
	</table>
</form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="sample_list.html">
    <input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="acid" value="<%=acid%>">
<input type="hidden" name="key" value="<%=key%>">
<input type="hidden" name="lid" value="<%=lid %>">
<input type="hidden" name="type" value="<%=type %>">
  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/printassets.html" method="get" target="_blank" name="print_form">
	<input type="hidden" name="aid" id="aid"/>
	<input type="hidden" id="print_range_width" name="print_range_width" value="80"/>
	<input type="hidden" id="print_range_height" name="print_range_height" value="35"/>
	<input type="hidden" id="printer" name="printer" value="LabelPrinter"/>
	<input type="hidden" id="paper" name="paper" value="100X50"/>
</form>
<p>&nbsp;</p>
</body>
</html>
