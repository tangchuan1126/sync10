<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.cwc.app.key.GoodsStatusKey"%>
<%@ page import="com.cwc.app.key.FileWithTypeKey"%>
<%
long assets_id = StringUtil.getLong(request,"aid");
DBRow assets = assetsMgr.getDetailAssets(assets_id);
GoodsStatusKey goodsStatusKey=new GoodsStatusKey();
SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 

String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>收货</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<style type="text/css">
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
.setcenter{valign:middle; }
</style>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript"><!--
$(document).ready(function() {
  $("#consignee").focus();

})
function init(){
$("#consignee").focus();
}
function closeWin()
{
  $.artDialog.close();
}
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"picture",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
// 回显要求看上去已经是上传了的文件。所以上传是要加上前缀名的sn
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_names']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			 
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
		
	 	//判断是不是有了
	 	if($("a",$("#over_file_td")).length > 0){
			var td = $("#over_file_td");
	
			td.append(lis);
		}else{
			$("#file_up_tr").attr("style","");
			$("#jquery_file_up").append(lis);
		}
	} 
	

}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+"_"+'<%= assets_id%>'+"_" + fileName;
    var  a = "<p class='new'><a href="+uri+">"+showName+"</a></p>";
    return a ;
  
}
function onlineScanner(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function receiveAssets(aid){
       var f=document.receive_assets_form; 
       var p=parent.document.mod_form;
       if (f.consignee.value.trim() == "")
    	 {
    		alert("请填写收货人");
    		return false;
    	 }
      	p.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/modAssets.action";
		p.category_id.value = f.category_id.value;
		p.product_line_id.value = f.product_line_id.value;		
		p.a_name.value = f.a_name.value;
		p.a_id.value=f.a_id.value;
		p.a_barcode.value = f.a_barcode.value;
		p.unit_name.value = f.unit_name.value;
		p.unit_price.value = f.unit_price.value;
		p.standard.value = f.standard.value;
		p.purchase_date.value = f.purchase_date.value;
		p.suppliers.value = f.suppliers.value;
		p.location.value = f.location.value;
		p.requisitioned_id.value = f.requisitioned_id.value;
		p.requisitioned.value = f.requisitioned.value;
		p.state.value = f.state.value;
		p.handle_time.value = f.handle_time.value;
		p.goodsState.value = f.goodsState.value;
		p.handle_price.value = f.handle_price.value;
		p.responsi_person.value = f.responsi_person.value;
		p.return_name.value = f.return_name.value;
		p.receive_time.value = f.receive_time.value;
		p.center_account_id.value = f.center_account_id.value;
		p.consignee.value = f.consignee.value;
		p.applyState.value =f.applyState.value;
		p.currency.value = f.currency.value;
		p.creater_id.value = f.creater_id.value;
		p.requisition_date.value = f.requisition_date.value;
		p.sn.value = $("#sn").val();
		p.file_name.value = $("#file_names").val();
		p.file_with_type.value = $("#file_with_type").val();
		p.path.value = $("#path").val();
		p.submit();	

}

--></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onload="init();">
<form name="receive_assets_form" method="post" action="">
<input type="hidden"  id="a_id" name="a_id" value="<%=assets_id %>"/>
<input type="hidden"  id="a_name" name="a_name" value="<%=assets.getString("a_name") %>"/>
<input type="hidden"  id="unit_price" name="unit_price" value="<%=assets.get("unit_price",0d)%>"/>
<input type="hidden"  id="unit_name" name="unit_name" value="<%=assets.getString("unit_name")%>"/>
<input type="hidden" id="a_barcode" name="a_barcode"  value="<%=assets.getString("a_barcode") %>"/>
<input type="hidden" id="standard" name="standard"  value="<%=assets.getString("standard") %>"/>
<input type="hidden" id="purchase_date" name="purchase_date"  value="<%=assets.getString("purchase_date") %>"/>
<input type="hidden" id="suppliers" name="suppliers"  value="<%=assets.getString("suppliers") %>"/>
<input type="hidden" id="category_id" name="category_id"  value="<%=assets.getValue("category_id") %>"/>
<input type="hidden" id="location" name="location"  value="<%=assets.getValue("location_id") %>"/>
<input type="hidden" id="requisitioned_id" name="requisitioned_id"  value="<%=assets.getValue("requisitioned_id") %>"/>
<input type="hidden" id="requisitioned" name="requisitioned"  value="<%=assets.getString("requisitioned") %>"/>
<input type="hidden" id="state" name="state"  value="<%=assets.get("state",0) %>"/>
<input type="hidden" id="product_line_id" name="product_line_id"  value="<%=assets.getValue("product_line_id") %>"/>
<input type="hidden" id="center_account_id" name="center_account_id"  value="<%=assets.getValue("center_account_id") %>"/>
<input type="hidden" id="goodsState" name="goodsState"  value="<%=goodsStatusKey.arrival %>"/>
<input type="hidden" id="handle_price" name="handle_price"  value="<%=assets.getValue("handle_price")%>"/>
<input type="hidden" id="responsi_person" name="responsi_person"  value="<%=assets.getValue("responsi_person")%>"/>
<input type="hidden" id="handle_time" name="handle_time"  value="<%=assets.getString("handle_time")%>"/>
<input type="hidden" id="receive_time" name="receive_time"  value="<%=sdf.format(new Date())%>"/>
<input type="hidden" id="return_name" name="return_name"  value="<%=assets.getValue("return_name")%>"/>
<input type="hidden" id="applyState" name="applyState"  value="<%=assets.get("applyState",0) %>"/>
<input type="hidden" id="creater_id" name="creater_id"  value="<%=assets.get("creater_id",0) %>"/>
<input type="hidden" id="requisition_date" name="requisition_date"  value="<%=assets.getString("requisition_date") %>"/>
<input type="hidden" id="currency" name="currency"  value="<%=assets.getString("currency")%>"/>


<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center" onclick="init()">
  <tr>
    <td colspan="2" align="center" valign="top">
    <table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<input type="hidden"  id="aid" name="aid" value=""/>
<table width="100%" border="0" cellspacing="5" cellpadding="2" align="center">
<tr height="8">
<td>&nbsp;</td>
</tr>

  <tr>
			    <fieldset class="set" style="border:2px solid #c3c3c3;text-align:left；width:98%;">
  <legend ><span><%=assets_id %></span></legend>
  <p>
  <span class="STYLE3">商品名称:</span>
   <span><%=assets.getString("a_name") %></span>
  </p>
   <p>
  <span class="STYLE3">收货时间:</span>
   <span>
			              <%=sdf.format(new Date())%></span>
  </p>
   <p>
  <span class="STYLE3">&nbsp;&nbsp;收货人:</span>
   <span><input type="text" name="consignee" id="consignee" value=""  style="border:1px #CCCCCC solid;width:150px;" ></span></p>
   <p>
  <span class="STYLE3">&nbsp;&nbsp;&nbsp;凭证:</span>

					<span>
		 				<input type="hidden" id="sn" name="sn" value="Receive"/>
		 				<input type="hidden" id="file_with_type" name="file_with_type" value="<%= FileWithTypeKey.assets_receive %>" />
		 				<input type="hidden" id="path" name="path" value="<%= systemConfig.getStringConfigValue("file_path_receive")%>" />
		 				<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="上传凭证" />	
		 				<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />	
		    			<span id="file_up_tr" style="display:none;">
		    				<div id="jquery_file_up" style="padding-left:60px;">	
			              			<input type="hidden" id="file_names" name="file_names" value=""/>				 	
			            	</div> 
			            </span>	
   </span>
    <%
		 		// 如果是完成应该在下面显示文件然后支持下载链接
		 			 DBRow[] fileRows = transportMgrZr.getFileByFilwWidthIdAndFileType("file",assets.get("aid",0),FileWithTypeKey.assets_receive );
		 				if(fileRows != null && fileRows.length > 0 ){
		 			 %>
		 			<div id="over_file_td">
		 			 	 <%for(DBRow fileRow : fileRows){ %>
 		 			 	  	<a href='<%= downLoadFileAction%>?file_name=<%=fileRow.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_receive")%>'><%=fileRow.getString("file_name") %></a>
		 			 	 <%} %>
		 			 </div>
		 			 <%
		 			 }
		 	%>
   </p>
  </fieldset>
</tr>
</table>
	</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="50%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="收货" class="normal-green" onClick="receiveAssets(<%=assets_id %>);">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
	<td width="1%" align="left" valign="middle" class="win-bottom-line"></td>
  </tr>
</table>
</form>	
</body>
</html>
