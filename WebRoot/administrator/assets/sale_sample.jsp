<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.cwc.app.key.GoodsStatusKey"%>
<%
SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
long assets_id = StringUtil.getLong(request,"aid");
DBRow assets = assetsMgr.getDetailAssets(assets_id);
GoodsStatusKey goodsStatusKey=new GoodsStatusKey();
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>出售</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
		
<script type="text/javascript">
 function saleAssets(aid)
 {
      var f=document.sale_sample_form;
           if (f.handle_price.value.trim() == "")
      {
      alert("请填写售货价格");
      return false;
      }
     else if(parseFloat(f.handle_price.value) != f.handle_price.value)
      {
       alert("售货价格只能是数字");
       return false;
      }
     else if(f.handle_price.value.match(/^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/))
      {
       alert("售货价格不能为负数");
       return false;
      }else
        parent.document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/modAssets.action";
		parent.document.mod_form.a_id.value = f.a_id.value;
		parent.document.mod_form.a_name.value = f.a_name.value;
		parent.document.mod_form.a_barcode.value = f.a_barcode.value;
		parent.document.mod_form.unit_name.value = f.unit_name.value;
		parent.document.mod_form.unit_price.value = f.unit_price.value;
		parent.document.mod_form.standard.value = f.standard.value;
		parent.document.mod_form.purchase_date.value = f.purchase_date.value;
		parent.document.mod_form.suppliers.value = f.suppliers.value;
		parent.document.mod_form.category_id.value = f.category_id.value;
		parent.document.mod_form.location.value = f.location.value;
		parent.document.mod_form.requisitioned_id.value = f.requisitioned_id.value;
		parent.document.mod_form.requisitioned.value = f.requisitioned.value;
		parent.document.mod_form.state.value = f.state.value;
		parent.document.mod_form.product_line_id.value = f.product_line_id.value;	
		parent.document.mod_form.center_account_id.value = f.center_account_id.value;		
		parent.document.mod_form.goodsState.value = f.goodsState.value;
		parent.document.mod_form.applyState.value = "<%=assets.get("applyState",0) %>";
		parent.document.mod_form.handle_time.value = f.handle_time.value;
	    parent.document.mod_form.handle_price.value = f.handle_price.value;
	    parent.document.mod_form.responsi_person.value = f.responsi_person.value;
		parent.document.mod_form.return_name.value = "<%=assets.getString("return_name") %>";
		parent.document.mod_form.creater_id.value= "<%=assets.getString("creater_id") %>";
		parent.document.mod_form.requisition_date.value = "<%=assets.getString("requisition_date") %>";
		parent.document.mod_form.consignee.value = "<%=assets.getString("consignee") %>";
		parent.document.mod_form.receive_time.value= "<%=assets.getString("receive_time") %>";
		parent.document.mod_form.currency.value= "<%=assets.getString("currency") %>";
		
		
		//parent.document.mod_form.return_name.value = f.return_name.value;
		parent.document.mod_form.submit();	
 }
 
 $(document).ready(function() {
   $("#responsi_person").focus();

 })
function closeWin()
{
  $.artDialog.close();
}
</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}
.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  >
<form name="sale_sample_form" id="sale_sample_form"  method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">
    <table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<input type="hidden"  id="a_id" name="a_id" value="<%=assets_id %>"/>
<input type="hidden"  id="a_name" name="a_name" value="<%=assets.getString("a_name") %>"/>
<input type="hidden"  id="unit_price" name="unit_price" value="<%=assets.get("unit_price",0d)%>"/>
<input type="hidden"  id="unit_name" name="unit_name" value="<%=assets.getString("unit_name")%>"/>
<input type="hidden" id="a_barcode" name="a_barcode"  value="<%=assets.getString("a_barcode") %>"/>
<input type="hidden" id="standard" name="standard"  value="<%=assets.getString("standard") %>"/>
<input type="hidden" id="purchase_date" name="purchase_date"  value="<%=assets.getString("purchase_date")%>"/>
<input type="hidden" id="suppliers" name="suppliers"  value="<%=assets.getString("suppliers") %>"/>
<input type="hidden" id="category_id" name="category_id"  value="<%=assets.getValue("category_id") %>"/>
<input type="hidden" id="location" name="location"  value="<%=assets.getValue("location_id") %>"/>
<input type="hidden" id="requisitioned_id" name="requisitioned_id"  value="<%=assets.getValue("requisitioned_id") %>"/>
<input type="hidden" id="requisitioned" name="requisitioned"  value="<%=assets.getString("requisitioned") %>"/>
<input type="hidden" id="state" name="state"  value="<%=assets.get("state",0) %>"/>
<input type="hidden" id="product_line_id" name="product_line_id"  value="<%=assets.getValue("product_line_id") %>"/>
<input type="hidden" id="center_account_id" name="center_account_id"  value="<%=assets.getValue("center_account_id") %>"/>
<input type="hidden" id="goodsState" name="goodsState"  value="<%=goodsStatusKey.sell %>"/>
<input type="hidden" id="handle_time" name="handle_time"  value="<%=sdf.format(new Date())%>"/>
<input type="hidden" id="return_name" name="return_name"  value="<%=assets.getValue("return_name")%>"/>
<table width="98%" border="0" cellspacing="5" cellpadding="2" align="center" onclick="init()">
<tr height="8">
<td>&nbsp;</td>
</tr>

  <tr>
			    <td align="left" valign="middle" class="STYLE3" >商品编号</td>
			    <td align="left" valign="middle" >
			      <label > </label>
			    	<span><%=assets_id %></span>
			    </td>
	
			    <td align="left" valign="middle" class="STYLE3" >商品名称</td>
		
			    <td align="left" valign="middle">
			    	    		      <%
	          String[] a_name=null;
	          a_name=assets.getString("a_name").split(",");
	         // out.print(a_name.length);
	         for(int j=0;j<a_name.length;j++){
	        	  String[] splitModel=null;
	 	         String nameModel=a_name[j];
	 	         splitModel=nameModel.split("/");
	 	        out.print(splitModel[0]);
	 	        if(j!=a_name.length-1){
	 	        	out.print("，");
	 	        }
	          }
	          %>
			   </td>
			   
			  </tr>
			  <tr>
			  	 <td align="left" valign="middle" class="STYLE3" >采购价格</td>
			    <td align="left" valign="middle" >
			    	<span><%=assets.get("unit_price",0d)%>/<%=assets.getString("unit_name")%></span>
			    </td>
			
				 <td align="left" valign="middle" class="STYLE3" >售出时间</td>
			 <td><span> 
			              <%=sdf.format(new Date())%>
			   </span></td>
			  </tr>
			  	  <tr>
			  	 <td align="left" valign="middle" class="STYLE3" >&nbsp;收款人</td>
			    <td align="left" valign="middle" >
			    	<input type="text" name="responsi_person" id="responsi_person" value=""  style="border:1px #CCCCCC solid;width:120px;" >
			    </td>
			
				 <td align="left" valign="middle" class="STYLE3" >售出价格</td>
			    <td align="left" valign="middle" >
			    <input name="handle_price" id="handle_price" type="text" value=""   style="border:1px #CCCCCC solid;width:120px;">
			    </td>
			  </tr>
</table>
	</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="售出" class="normal-green" onClick="saleAssets(<%=assets_id%>);">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
  </table>
  </form>	

</body>
</html>
