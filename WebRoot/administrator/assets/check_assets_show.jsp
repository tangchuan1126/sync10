<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.key.ImportAssetsErrorKey"%>
<%@ include file="../../include.jsp"%> 
<%

 	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(30);
 	String file = new String();
 	DBRow[] rows = null;
 	String type=StringUtil.getString(request,"type");
 	try
 	{
 		file = assetsMgr.importAssets(request);
 		HashMap<String,DBRow[]> error = assetsMgr.excelshow(file,"show",type); 
 		
 		rows = error.get("errorAssets");
 	}
 	catch(FileTypeException e)
 	{
 		out.print("<script>alert('只可上传.xls文件');window.history.go(-1)</script>");
 	}
 	
 	boolean submit = true;
 	String msg = "";
 	
 	ImportAssetsErrorKey importErrorKey = new ImportAssetsErrorKey();
 %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>检测资产</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/poshytip/jquery-1.4.min.js"></script>


<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>			

<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="onLoadInitZebraTable()" >
<table width="100%" cellpadding="0" cellspacing="0" height="100%">
  <tr>
  	<td valign="top" width="98%" align="center">
	<div id="tabs" style="width:95%; height:100%" align="center">
	<ul>
			<li><a href="#error_product">资产信息错误提示</a></li>
	</ul>
			<div id="error_product" align="center">
				<table width="100%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;" class="zebraTable">
					 <% 
						if(rows!=null&&rows.length>0)
						{	
							submit = false;
					%>
						<tr>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">资产名</th>
							<th width="10%" class="right-title "style="vertical-align: center;text-align: left;">资产类别</th>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">错误原因</th>
						</tr>
					<%
						for(int i=0;i<rows.length;i++)
						{
					%>
						  <tr align="center" valign="middle">
							<td height="40" align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;">
							  <%
	          String[] a_name=null;
	          a_name=rows[i].getString("a_name").split(",");
	         // out.print(a_name.length);
	         for(int j=0;j<a_name.length;j++){
	        	  String[] splitModel=null;
	 	         String nameModel=a_name[j];
	 	         splitModel=nameModel.split("/");
	 	        out.print(splitModel[0]);
	 	        if(j!=a_name.length-1){
	 	        	out.print("，");
	 	        }
	          }
	          %>
							&nbsp;</td>
							<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("category_name") %>&nbsp;</td>
							<td style="border-bottom: 1px solid #dddddd;padding-left:10px;" align="left">
								<% 
									out.print("<font color='red'>位置:资产表内第"+rows[i].getString("errorAssetsRow")+"行</font><br/>");
									String[] errorP = rows[i].getString("errorMessage").split(",");
									for(int j = 0;j<errorP.length;j++)
									{
										out.println("<font color='red'>"+(j+1)+".</font>"+importErrorKey.getErrorMessageById(errorP[j].toString()));
										if(j<errorP.length-1)
										{
											out.print("<br/>");
										}
									}
								%>
							</td>
						  </tr>
					  <%
							}
						}
						else
						{
					  %>
							<tr align="center" valign="middle">
							<th height="100%" align="center" style="padding-left:10px;font-size:xx-large;background-color: white;">资产数据检查无误</th>
							</tr>
					  <%
					  	}
					  %>
			  </table>
			</div>
			
	</div>
		
	</td>
  </tr>
  <script>
	$("#tabs").tabs({	
		cookie: { expires: 30000 } 
	});
</script>
 <tr>
 	<td valign="bottom">
 		<table width="100%" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="left" valign="middle" class="win-bottom-line">
					<%
						if(!msg.equals(""))
						{
					%>
						<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#000000"><%=msg%></span>
					<%
						}
					%>			  &nbsp;</td>
 				<td align="right" valign="middle" class="win-bottom-line"> 
			      <%  
			      	if(submit)
			      	{
			      %>
			      	<input type="button" name="Submit2" value="确定" class="normal-green" onClick="ajaxSaveProduct(<%=type %>)"/>
			      <%
			      	}
			      %>
				  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
   
</table>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/saveImportAssets.action?type="+<%=type %> name="saveAssets_form">
	<input type="hidden" name="tempfilename" id="tempfilename" value="<%=file%>"/>
</form>
<script type="text/javascript">
	
	function ajaxSaveProduct(type)
	{
		if(<%=submit%>)
		{
			var tempfilename = $("#tempfilename").val();	
			
			var para = "tempfilename="+tempfilename+"&&type="+type;
			
			$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true, 
			
				fadeOut:  2000
			};
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
			
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/saveImportAssets.action',
				type: 'post',
				dataType: 'json',
				timeout: 6000000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert(e);
					alert("提交失败，请重试！");
				},
				
				success: function(date){
					if (date["close"])
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">保存成功！</span>' });
						$.unblockUI();
						parent.closeWinRefresh();
					}
					else
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">'+date["error"]+'</span>' });
						alert();
					}
				}
			});
		}
		else
		{
			alert("<%=msg%>");
		}
	}

	function closeWin(){
		$.artDialog.close();
	}
</script>

</body>
</html>



