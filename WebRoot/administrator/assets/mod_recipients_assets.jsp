<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.Date"%>
<%@ page import="com.cwc.app.key.GoodsStatusKey"%>
<%
SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
long aid = StringUtil.getLong(request,"aid");
DBRow row = assetsMgr.getDetailAssets(aid);
DBRow[] adminGroups = assetsMgr.getAdminGroup();
DBRow[] admins = assetsMgr.getAdmin();
GoodsStatusKey goodsStatusKey=new GoodsStatusKey();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加资产</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script>
function closeWin()
{
 
  $.artDialog.close();
}
function modAssets()
{
	var f = document.mod_assets_form;

	parent.document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/modAssets.action";
	parent.document.mod_form.category_id.value = f.category_id.value;
	parent.document.mod_form.a_name.value = f.a_name.value;
	parent.document.mod_form.a_id.value=f.aid.value;
	parent.document.mod_form.a_barcode.value = f.a_barcode.value;
	parent.document.mod_form.unit_name.value = f.unit_name.value;
	parent.document.mod_form.unit_price.value = f.unit_price.value;
	parent.document.mod_form.purchase_date.value = f.purchase_Date.value;
	parent.document.mod_form.suppliers.value = f.suppliers.value;
	parent.document.mod_form.location.value = f.location.value;
	parent.document.mod_form.requisitioned_id.value = f.requisitioned_id.value;
	parent.document.mod_form.requisitioned.value = f.requisitioned.value;
	parent.document.mod_form.handle_time.value = f.handle_time.value;
	parent.document.mod_form.goodsState.value = "<%=row.getString("goodsState")%>";
	parent.document.mod_form.state.value= f.state.value;
	parent.document.mod_form.applyState.value= f.applyState.value;
	parent.document.mod_form.return_name.value = f.return_name.value;
	parent.document.mod_form.product_line_id.value = f.product_line_id.value;
	parent.document.mod_form.center_account_id.value = f.center_account_id.value;
	parent.document.mod_form.handle_price.value = f.handle_price.value;
	parent.document.mod_form.responsi_person.value= f.responsi_person.value;
	parent.document.mod_form.creater_id.value = f.creater_id.value;
	parent.document.mod_form.requisition_date.value = f.requisition_date.value;
	parent.document.mod_form.consignee.value = f.consignee.value;
	parent.document.mod_form.receive_time.value= f.receive_time.value;
	parent.document.mod_form.currency.value = f.currency.value;
	
	parent.document.mod_form.submit();
}


</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>

<script language="javascript">

<%
	String v = "var emp = new Array(";
	v += "new Array('0','0','0')";
	for(int i=0;i<admins.length;i++) {
		DBRow rowEmp = admins[i];
		
		v += ",new Array("+rowEmp.get("adid",0)+","+rowEmp.get("adgid",0)+",'"+rowEmp.getString("employe_name")+"')";
	} 
	v+= ");";
	out.println(v);
%>

function selectDept() {

	var adgid = mod_assets_form.adgid;	     
	var requisitioned_id = mod_assets_form.requisitioned_id;
	var deptId = 0;
	
	for(var i=0; i<adgid.options.length; i++) {
		if(adgid.options[i].selected==true) {
			deptId = adgid.options[i].value;
		}
	}
	
	getEmpSelect('requisitioned_id','chzn-select1',emp,deptId);
	//requisitioned_id.options[0].selected=true;
	//mod_assets_form.requistioned.value = "";
}




function selectEmp() {
	var requisitioned_id = mod_assets_form.requisitioned_id;
	
	for(var i=0; i<requisitioned_id.options.length; i++) {
		if(requisitioned_id.options[i].selected==true) {
			mod_assets_form.requistioned.value =  $(requisitioned_id.options[i]).attr("empName");
		}
	}
}

function getEmpSelect(name,className,empArray,deptId) {
	
	var selectHtml = "<select name='"+name+"' id='"+name+"' class='"+className+"' style='width:120px;' onchange='selectEmp()'>";
	selectHtml += "<option value='0' empName='' deptId='0'>选择职员</option> ";
	for(var i=0; i<empArray.length; i++) {

		if(deptId==0) {
			selectHtml += "<option value='"+empArray[i][0]+"' empName='"+empArray[i][2]+"' deptId='" + empArray[i][1] + "'>"+empArray[i][2]+"</option>";
		}else if(deptId == empArray[i][1]){
			selectHtml += "<option value='"+empArray[i][0]+"' empName='"+empArray[i][2]+"' deptId='" + empArray[i][1] + "'>"+empArray[i][2]+"</option>";
		}
		
	}
	selectHtml += "</select>";
	$('#empDiv').html('');
	$('#empDiv').append(selectHtml);
	$("."+className).chosen();
	$("."+className).chosen({no_results_text: "没有该人:"});
	$("."+className).chosen({allow_single_deselect:true}); 

}



</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="mod_assets_form" method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		

<input type="hidden"  id="aid" name="aid" value="<%=row.getString("aid") %>"/>
<input type="hidden" id="a_name" name="a_name"  value="<%=row.getString("a_name")%>"/>
<input type="hidden" id="handle_time" name="handle_time"  value="<%=row.getString("handle_time")%>"/>
<input type="hidden" id="return_name" name="return_name"  value="<%=row.getValue("return_name")%>"/>
<input type="hidden" id="product_line_id" name="product_line_id"  value="<%=row.getValue("product_line_id") %>"/>
<input type="hidden" id="center_account_id" name="center_account_id"  value="<%=row.getValue("center_account_id") %>"/>
<input type="hidden" id="goodsState" name="goodsState"  value="<%=row.getValue("goodsState") %>"/>
<input type="hidden" id="handle_price" name="handle_price"  value="<%=row.getValue("handle_price")%>"/>
<input type="hidden" id="responsi_person" name="responsi_person"  value="<%=row.getValue("responsi_person")%>"/>
<input type="hidden" id="creater_id" name="creater_id"  value="<%=row.getValue("creater_id") %>"/>
<input type="hidden" id="requisition_date" name="requisition_date"  value="<%=sdf.format(new Date()) %>"/>
<input type="hidden" id="consignee" name="consignee"  value="<%=row.getValue("consignee")%>"/>
<input type="hidden" id="receive_time" name="receive_time"  value="<%=row.getString("receive_time")%>"/>
<input type="hidden" id="currency" name="currency"  value="<%=row.getValue("currency")%>"/>
<input type="hidden" id="applyState" name="applyState"  value="<%=row.getValue("applyState")%>"/>


<table width="98%" border="0" cellspacing="5" cellpadding="2">

  <tr>
   <td align="right" valign="middle" class="STYLE1 STYLE2" width="300">资产类别：</td>
   
   <%
			if(row.get("category_id",0l) != 0)
			{
				DBRow cateogry = assetsCategoryMgr.getDetailAssetsCategory(row.get("category_id",0l));
			
		 %>
		    <td align="left" valign="middle"  width="240" colspan="3">
        	<%=cateogry.getString("chName")+"["+cateogry.getString("enName")+"]" %>
        	<input name="category_id" value="<%=row.getString("category_id") %>" type="hidden">
        </td>
        <%} %>
  
  
    </tr>
     <%
    String[] nameModel=null;
    nameModel=row.getString("a_name").split(",");
    for(int i=0;i<nameModel.length;i++)
    {
    %>
  		<tr>
  		 <%
  		         String[] splitModel=null;
  		         splitModel=nameModel[i].split("/");
  		      %>
			    <td align="right" valign="middle" class="STYLE1 STYLE2" width="300">资产名称<%=i+1 %>：</td>
			    <td align="left" valign="middle"  width="240">
			    	<%=splitModel[0]%>
			    	<input  value="<%=splitModel[0]%>" type="hidden"/>
			    </td>
			    
			     <td align="right" valign="middle" class="STYLE3" width="300">型号<%=i+1 %>：</td>
			      <%if(splitModel.length>1) {%>
			    <td align="left" valign="middle" >
			    	<%=splitModel[1] %>
			    	<input name="standard" type="hidden" value="<%=splitModel[1] %>">
			    </td>
			  <%} %>   
	      </tr>
	   <%} %>    
			  <tr>
			  	 <td align="right" valign="middle" class="STYLE3" width="300">采购日期：</td>
			    <td align="left" valign="middle" >
			    	<%
					 
					String input_st_date="";
					if ( row.getString("purchase_Date").equals("") )
					{	
						input_st_date = sdf.format(new java.util.Date());
					}
					else
					{	
						input_st_date = row.getString("purchase_Date");
					}
					
					
					%>
					<%=input_st_date %>
			    	<input type="hidden" name="purchase_Date" value="<%=input_st_date %>" >
			    </td>
			
				 <td align="right" valign="middle" class="STYLE3" width="300">商品价值：</td>
			    <td align="left" valign="middle" >
			    	<%=row.getString("unit_price") %>
			    	<input name="unit_price" type="hidden" value="<%=row.getString("unit_price") %>">
			    </td>
			  </tr>
			  <tr>
			  	 <td align="right" valign="middle" class="STYLE3" width="300">单位：</td>
			    <td align="left" valign="middle" >
			    	<%=row.getString("unit_name") %>
			    	<input name="unit_name" type="hidden" value="<%=row.getString("unit_name") %>">
				</td>
			  
			  <td align="right" valign="middle" class="STYLE3" width="300">条码：</td>
			    <td align="left" valign="middle" width="240">
			    	<%=row.getString("a_barcode") %>
			    	<input name="a_barcode" value="<%=row.getString("a_barcode") %>" type="hidden">
			    </td>
			  </tr>
			  <tr>
			    <td align="right" valign="middle" class="STYLE3" width="300">供应商：</td>
			    <td align="left" valign="middle" >
			    	<%=row.getString("suppliers") %>
			    	<input name="suppliers" type="hidden" value="<%=row.getString("suppliers") %>">
			      </td>
			      <%
			      	if(row.get("location_id",0l) != 0)
			      	{
			      		DBRow location = officeLocationMgr.getDetailOfficeLocation(row.get("location_id",0l));
			      	
			       %>
			       <td align="right" valign="middle" class="STYLE3" width="300">存放地点：</td>
			    	<td align="left" valign="middle" >
			    	<%=location.getString("office_name") %>
			    	<input type="hidden" name="location" value="<%=location.get("id",0l) %>"/>
			      </td>
			      <%} %>
			  </tr>
	
			      <tr>
			      <td align="right" valign="middle" class="STYLE3" width="300">好坏情况：</td>
			    <td align="left" valign="middle" >
			     <input type="hidden" name="state" value="<%=row.getString("state") %>">
			    	<%
			    		if(row.get("state",0l) == 1)
			    		{
			    	 %>
			    	完好
			    	<%
			    		}
			    		else if(row.get("state",0l) == 2)
			    		{
			    	 %>
			    	功能残损
			    	 <%
			    	 	}
			    		else if(row.get("state",0l) ==3)
			    	 	{
			    	  %>
			    	  外观残损
			    	  <%
			    	  	}
			    		else if(row.get("state",0l) ==4)
			    	 	{
			    	   %>
			    	   报废
			    	    <%
			    	  	}
			    	     %>
			      </td>
			    		       <td align="right" valign="middle" class="STYLE3" width="300">货物状态：</td>
			    <td align="left" valign="middle" >
			     <input type="hidden" name="goodsState" value="<%=row.getString("goodsState") %>">
			    	<%
			    		if(row.get("goodsState",0l) == 1)
			    		{
			    	 %>
			    	未到货
			    	<%
			    		}
			    		else if(row.get("goodsState",0l) == 2)
			    		{
			    	 %>
			    	已到货
			    	 <%
			    	 	}
			    		else if(row.get("goodsState",0l) ==3)
			    	 	{
			    	  %>
			    	 已出售
			    	  <%
			    	  	}
			    		else if(row.get("goodsState",0l) ==4)
			    	 	{
			    	   %>
			    	   已退货
			    	    <%
			    	  	}
			    		else if(row.get("goodsState",0l) ==5)
			    	 	{
			    	     %>
			    	  已换货   
			    	     <%
			    	 	}
			    	     %>
			      </td>  
	
			  </tr>
			  		  <tr>
			    <td align="right" valign="middle" class="STYLE3" width="300">领用人：</td>
			    <td align="left" valign="middle" colspan="3">
			    <div style="float:left;">
			    <select name="adgid" id="adgid" onchange="selectDept()" data-placeholder="请选择部门" class="chzn-select" style="width:120px;" >
				<option value="0">选择部门</option>
				<% 
		          	for(int i=0; i<adminGroups.length; i++) {
		          		long adgid = adminGroups[i].get("adgid",0);
		          		String name = adminGroups[i].getString("name");
		          		if(name.equals("售后客服") || name.equals("售中客服")) {
		          			continue;
		          		}
		          		
		          		if(adgid == StringUtil.getLong(request,"adgid"))
		          			out.println("<option value='"+adgid+"' selected>"+name+"</option>");
		          		else 
			          		out.println("<option value='"+adgid+"'>"+name+"</option>");
		          	}
		          %>
			</select>
			</div>
				<div id="empDiv" name="empDiv" style="float:left;">
				<select name="requisitioned_id" id="requisitioned_id" data-placeholder="请选择职员" class="chzn-select1" style="width:120px;" tabindex="2" onchange="selectEmp()">
		          <option value="0" deptId="0" empName="">选择职员</option>
		          <%
		          	for(int i=0; i<admins.length; i++) {
		          		long requisitioned_id = row.get("requisitioned_id",0);
		          		long adid = admins[i].get("adid",0);
		          		long adgid = admins[i].get("adgid",0);
		          		String employee_name = admins[i].getString("employe_name");
		          		if(adid == requisitioned_id)
		          			out.println("<option value='"+adid+"' empName='"+employee_name+"' deptId='" + adgid + "' selected>"+employee_name+"</option>");
		          		else 
			          		out.println("<option value='"+adid+"' empName='"+employee_name+"' deptId='" + adgid + "'>"+employee_name+"</option>");
		          	}
		          %>
		        </select>
		        </div>
			    	<input name="requisitioned" id="requistioned" type="hidden" value="<%=row.getString("requisitioned") %>"  class="input-line">
			      </td>
			      </tr>
  
</table>
	</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存修改" class="normal-green" onClick="modAssets();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table> 
</form>	
 <script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
	$(".chzn-select").chosen(); 
	$(".chzn-select-deselect").chosen({allow_single_deselect:true}); 
	$(".chzn-select1").chosen(); 
	$(".chzn-select-deselect1").chosen({allow_single_deselect:true}); 
</script>
</body>
</html>
