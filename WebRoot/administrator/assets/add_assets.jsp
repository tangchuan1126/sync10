<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long lid = StringUtil.getLong(request,"lid");
long category_id = StringUtil.getLong(request,"category_id");
DBRow sonCatalog[] = assetsCategoryMgr.getAssetsCategoryChildren(category_id);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加资产</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<script>
function addAssets()
{
	var f = document.add_assets_form;
	var code = /[a-z]+|[\u4e00-\u9fa5]|[:?*......\"\\<>^~&$@|]/;
	if (f.a_name.value == "")
	 {
		alert("请填写资产名称");
		return false;
	 }
	 if (f.code.value == "")
	 {
		alert("请填写资产条码");
		return false;
	 }
	 else if(code.test(f.code.value))
	 {
	 	alert("资产条码只能是英文大写字母或数字或字符");
	 	return false;
	 }
	if (f.unit_price.value == "")
	 {
		alert("请填写采购价格");
		return false;
	 }
	 else if(parseFloat(f.unit_price.value) != f.unit_price.value)
	 {
	 	alert("采购价格只能是数字");
	 	return false;
	 }
	else if(f.unit_price.value.match(/^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/))
	 {
	 	alert("采购价格不能为负数");
	 	return false;
	 }
	 if (f.location.value == 0){
	 	alert("请选择资产存放位置");
	 	return false;
	 }
	 else
	 {
			parent.document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/addAssets.action";
			parent.document.mod_form.category_id.value = <%=category_id%>;		
			parent.document.mod_form.a_name.value = f.a_name.value;
			parent.document.mod_form.a_barcode.value = f.code.value;
			parent.document.mod_form.unit_name.value = f.unit.value;
			parent.document.mod_form.unit_price.value = f.unit_price.value;
			parent.document.mod_form.standard.value = f.standard.value;
			parent.document.mod_form.purchase_date.value = f.purchase_date.value;
			parent.document.mod_form.suppliers.value = f.suppliers.value;
			parent.document.mod_form.location.value = f.location.value;
			parent.document.mod_form.requisitioned.value = f.requisitioned.value;
			parent.document.mod_form.state.value = f.state.value;
			parent.document.mod_form.submit();
	 }
}

if ( <%=sonCatalog.length%> > 0 )
{
	alert("资产必须增加到最底层类分，请重新选择");
	parent.openCatalogMenu();
	parent.closeWin();
	
}


function onLoadInit()
{
	$("#purchase_date").date_input();
}

</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInit();">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_assets_form" id="add_assets_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/assets/AddAssetsPrecessAction.action" >

<table width="98%" border="0" cellspacing="5" cellpadding="2">

  <tr>
    <td colspan="2" align="left" valign="middle" class="text-line"><table width="100%" border="0" cellpadding="5" cellspacing="0" >
      <tr>
        <td width="96%" style="color:#666666;font-size:20px;font-weight:bold;font-family:Arial, 宋体">
        
		<%
	  Tree tree = new Tree(ConfigBean.getStringValue("assets_category"));
	  DBRow allFather[] = tree.getAllFather(category_id);
	  for (int jj=0; jj<allFather.length-1; jj++)
	  {
	  	out.println(allFather[jj].getString("chName")+" » ");
	
	  }
	  
	  DBRow catalog = assetsCategoryMgr.getDetailAssetsCategory(category_id);
	  if (catalog!=null)
	  {
	  	out.println(catalog.getString("chName"));
	  }
	  %>		</td>
      </tr>
      <tr>
        <td style="color:#666666;font-size:13px;font-family:宋体">类别下增加资产......</td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE1 STYLE2" >名称</td>
    <td align="left" valign="middle" ><input name="a_name" type="text" class="input-line" id="a_name" ></td>
    
    <td align="left" valign="middle" class="STYLE3" >条码</td>
    <td align="left" valign="middle" ><input name="a_barcode" type="text" class="input-line" id="code" ></td>
  </tr>
  <tr>
  	 <td align="left" valign="middle" class="STYLE3" >采购日期</td>
    <td align="left" valign="middle" >
    	<%
		TDate tDate = new TDate();
		
		String input_st_date;
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		
		
		%>
    	<input type="text" name="purchase_date" id="purchase_date"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" >
    </td>

 <td align="left" valign="middle" class="STYLE3" >采购价格</td>
    <td align="left" valign="middle" ><input name="unit_price" type="text" id="unit_price"   style="width:100px;"></td>
  </tr>
  
  <tr>
  	<td align="left" valign="middle" class="STYLE3" colspan="1">存放地点</td>
    	<td align="left" valign="middle" colspan="3">
    		<ul id="locationmenu" class="mcdropdown_menu">
					  <li rel="0">所有分类</li>
					  <%
					  DBRow c1[] = officeLocationMgr.getChildOfficeLocation(0);
					  for (int i=0; i<c1.length; i++)
					  {
							out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("office_name"));
				
							  DBRow c2[] = officeLocationMgr.getChildOfficeLocation(c1[i].get("id",0l));
							  if (c2.length>0)
							  {
							  		out.println("<ul>");	
							  }
							  for (int ii=0; ii<c2.length; ii++)
							  {
									out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("office_name"));		
									
										DBRow c3[] = officeLocationMgr.getChildOfficeLocation(c2[ii].get("id",0l));
										  if (c3.length>0)
										  {
												out.println("<ul>");	
										  }
											for (int iii=0; iii<c3.length; iii++)
											{
													out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("office_name"));
													out.println("</li>");
											}
										  if (c3.length>0)
										  {
												out.println("</ul>");
										  }
										  
									out.println("</li>");				
							  }
							  if (c2.length>0)
							  {
							  		out.println("</ul>");	
							  }
							  
							out.println("</li>");
					  }
					  %>
				</ul>
					  <input type="text" name="location" id="location" value="" />
					  <input type="hidden" name="filter_lid" id="filter_lid" value=""  />
					  
				
				<script>
				$("#location").mcDropdown("#locationmenu",{
						allowParentSelect:true,
						  select: 
						  
								function (id,name)
								{
									$("#filter_lid").val(id);
								}
				});
				
				<%
				if (lid > 0)
				{
				%>
				$("#location").mcDropdown("#locationmenu").setValue(<%=lid%>);
				<%
				}
				else
				{
				%>
				$("#location").mcDropdown("#locationmenu").setValue(0);
				<%
				}
				%> 
				
				</script>
      </td>
  </tr>
  
  <tr>
  	 <td align="left" valign="middle" class="STYLE3" >单位</td>
    <td align="left" valign="middle" ><input name="unit_name" type="text" id="unit"  style="width:100px;" >
	</td>
   <td align="left" valign="middle" class="STYLE3" >规格</td>
    <td align="left" valign="middle" ><input name="standard" type="text" id="standard" class="input-line">
    </td>
       
  </tr>
 
 <tr>
    <td align="left" valign="middle" class="STYLE3" colspan="1">供应商</td>
    <td align="left" valign="middle" colspan="3"><input name="suppliers" type="text" id="suppliers"  class="input-line">
    <input type="hidden" name="category_id" id="category_id" value="<%=category_id %>"/>
      </td>
  </tr>
  
  <tr>
     <td align="left" valign="middle" class="STYLE3" >领用人</td>
    <td align="left" valign="middle" ><input name="requisitioned" type="text" id="requisitioned"  class="input-line">
      </td>
      <td align="left" valign="middle" class="STYLE3" >资产状态</td>
    <td align="left" valign="middle" >
    	<select id="state" runat="server">
	    <option value="0">报废</option>
	    <option value="1">损坏</option>
	    <option value="2" selected="selected">良好</option>
		</select>
      </td>
  </tr>
  
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line"><img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#999999">必须把资产增加到最底层分类</span>         </td>
    <td width="49%" align="right" class="win-bottom-line">    
	 <input type="button" name="Submit2" value="增加" class="normal-green" onClick="addAssets();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
	</td>
  </tr>
</table>
 <form name="dataForm" action="add_assets.html">
<input type="hidden" name="lid" value="<%=lid%>">


  </form>
</body>
</html>
