<%@page import="com.cwc.app.key.ProductFileTypeKey"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.util.StringUtil"%>
<%@page import="com.cwc.db.DBRow"%>
<%@page import="com.cwc.exception.JsonException"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.beans.jqgrid.FilterBean"%>
<%@page import="net.sf.json.JsonConfig"%>
<%@page import="com.cwc.app.beans.jqgrid.RuleBean"%>
<%@page import="com.cwc.json.JsonUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%! private static int oldpages = -1; %>
<%
	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	
	String pcid = StringUtil.getString(request,"pcid");
	String cmd = StringUtil.getString(request,"cmd");
	String key = StringUtil.getString(request,"key");
	int union_flag = StringUtil.getInt(request,"union_flag");
	String pro_line_id = StringUtil.getString(request,"pro_line_id");
	int product_file_types = StringUtil.getInt(request, "product_file_types");
	int product_upload_status = StringUtil.getInt(request, "product_upload_status");
	String title_id = StringUtil.getString(request, "title_id");
	long admin_user_id = StringUtil.getLong(request, "admin_user_id");
	
	boolean search = Boolean.parseBoolean(StringUtil.getString(request,"_search"));
	String sidx = StringUtil.getString(request,"sidx");//排序使用的字段
	String sord = StringUtil.getString(request,"sord");
	String filter = StringUtil.getString(request,"filters");
	FilterBean filterBean = null;
	if(search)
	{
		Map<String, Object> map = new HashMap<String, Object>();  //String-->Property ; Object-->Clss 
		map.put("rules", RuleBean.class);
		JsonConfig configjson = JsonUtils.getJsonConfig();
		configjson.setClassMap(map);
		filterBean = (FilterBean) JsonUtils.toBean(JSONObject.fromObject(filter),FilterBean.class,configjson); 
	}
			
	String c = StringUtil.getString(request,"rows");
	int pages = StringUtil.getInt(request,"page",1);
	PageCtrl pc = new PageCtrl();
	pc.setPageSize(Integer.parseInt(c));
	pc.setPageNo(pages);
	
	//System.out.println("data:"+title_id);
	
	if(oldpages != pages||pages==1)
	{
		DBRow rows[];
		if (cmd.equals("filter"))
		{
			//rows = productMgr.getProductByPcid(pcid,union_flag,pc);
			//rows = productMgrZJ.filterProduct(pcid, pro_line_id, union_flag, product_file_types, product_upload_status, pc);
			rows = proprietaryMgrZyj.filterProductByPcLineCategoryTitleId(true, pcid, pro_line_id, union_flag, product_file_types, product_upload_status,title_id, admin_user_id,0,0, pc, request,-1);
		}
		else if (cmd.equals("search"))
		{
			//rows = productMgr.getSearchProducts4CT(key,pc);
			//rows =  productMgrZJ.getDetailProductLikeSearch(key,pc);//productMgr.getSearchProducts4CT(key,pcid,pc);
			//System.out.println(pcid);
			rows = proprietaryMgrZyj.findDetailProductLikeSearch(true, key, title_id, admin_user_id,-1,0,0, pc, request, -1);
		}
		else if(cmd.equals("track_product_file"))
		{
			//rows = productMgrZyj.getProductInfosProductLineProductCodeByLineId(pro_line_id, pc);
			rows = proprietaryMgrZyj.findProductInfosProductLineProductCodeByLineId(true, pro_line_id, title_id, admin_user_id, pc, request);
		}
		else
		{
			//rows = productMgr.getAllProducts(pc);
			rows = proprietaryMgrZyj.findAllProductsByTitleAdid(true, title_id, admin_user_id, pc, request);
		}
		for(int i = 0;i<rows.length;i++)
		{
			if(rows[i].get("union_flag",0)==0)
			{
				rows[i].add("unionimg","<img src='../imgs/product.png'/>");
			}
			else
			{
				rows[i].add("unionimg","<a href='javascript:void(0)' title='View Suit Relationship'><img src='../imgs/union_product.png' border='0'/></a>");
			}
			StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
			  
		  	DBRow allFather[] = tree.getAllFather(rows[i].get("catalog_id",0l));
		  	for (int jj=0; jj<allFather.length-1; jj++)
		  	{
		  		catalogText.append("<img src='img/folderopen.gif'/>"+allFather[jj].getString("title")+"<br/>");
		  		String s = (jj==0) ? "<img src='img/joinbottom.gif'/>"  : "<img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>";
		  		catalogText.append(s);
		  	}
		  	
		  	DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
		  	if (catalog!=null)
		  	{
		  		catalogText.append("<img  src='img/page.gif'/>"+catalog.getString("title"));
		  	}
		  	
			//?cmd=filter&pcid="+rows[i].getString("catalog_id")+"&key=&union_flag=0
			rows[i].add("catalog_text",catalogText.toString());
			rows[i].remove("p_img");
			
			 /*	String img_text = "";
			
		 	if (productMgrZJ.getProductFileByPcid(rows[i].get("pc_id",0l), true, true).length>0)
			{
		      img_text = "<input name='Submit43' type='button' class='short-short-button' value='图片' onClick=\"showImg("+rows[i].get("pc_id",0l)+")\"/>";
			}			
			rows[i].add("img",img_text);*/
			
			StringBuffer html =  new StringBuffer();
			html.append("<span style='cursor: pointer;'>");
			// 显示出来商品文件的个数 和 商品标签的个数
			/*
			String value = systemConfig.getStringConfigValue("transport_product_file");
	  		String[] arraySelected = value.split("\n");
	  		ArrayList<String> selectedList= new ArrayList<String>();
	  		for(String tempSelect : arraySelected){
	  				if(tempSelect.indexOf("=") != -1){
	  					String[] tempArray = tempSelect.split("=");
	  					String tempHtml = tempArray[1];
	  					selectedList.add(tempHtml);
	  				}
	  		}*/
	  		ArrayList<String> selectedList= new ProductFileTypeKey().getProductFileTypeValue();
		 	Map<Integer,DBRow> productFileMap = transportMgrZr.getProductFileAndTagFile(rows[i].get("pc_id",0l),rows[i].get("pc_id",0l),FileWithTypeKey.PRODUCT_SELF_FILE);
		 	for(int indexOfList = 0 , countOfList = selectedList.size() ; indexOfList < countOfList ;indexOfList++ ){
		 		DBRow tempCount = productFileMap.get(indexOfList+1);
		 		int tempCountNumber = 0 ;
	 			tempCountNumber = null != tempCount?tempCount.get("count",0):0;
 				html.append(selectedList.get(indexOfList).trim());
 				html.append(":");
 				html.append(tempCountNumber);
 				if(1 == indexOfList%2)
 				{
 					html.append("<br/>");
 				}
 				else
 				{
 					html.append("&nbsp;");
 				}
		 	}
		 	html.append("</span>");
		 	rows[i].add("img",html.toString());
		 	String amozonCode = rows[i].getString("p_code2");
		 	String upcCode = rows[i].getString("upc");
			
			DBRow[] productCodes = productCodeMgrZJ.getUseProductCodes(rows[i].get("pc_id",0l));
			String pcodes = "";
			for(int j=0;j<productCodes.length;j++)
			{
				pcodes += productCodes[j].getString("p_code");
				
				if(j<productCodes.length-1)
				{
					pcodes +="\n";
				}
			}
			//rows[i].add("p_codes",pcodes);
			String p_codes_str = "";//"<input name='Submit43' type='button' class='short-short-button' value='条码' onclick='addProductCode("+rows[i].get("pc_id",0l)+")' title='"+pcodes+"'/>";
		
			if(!StringUtil.isBlank(amozonCode))
			{
				p_codes_str += "Amazon:"+amozonCode+"<br/>";
			}
			if(!StringUtil.isBlank(upcCode))
			{
				p_codes_str += "UPC:"+upcCode;
			}
			rows[i].add("p_codes",p_codes_str);
			
		}		
		
		DBRow data = new DBRow();
		data.add("page",pages);
		data.add("total",pc.getPageCount());
		
		data.add("rows",rows);
		oldpages = pages;
		data.add("records",pc.getAllCount());
		out.println(new JsonObject(data).toString());
	}
%>