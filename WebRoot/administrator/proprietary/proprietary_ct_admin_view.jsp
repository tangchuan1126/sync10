<%@page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html;charset=utf-8"%>
<%@ include file="../../include.jsp"%>
<%

String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"account");

Tree location_tree = new Tree(ConfigBean.getStringValue("office_location"));//获取office_location办公地点信息

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(10);

long title_id = StringUtil.getLong(request, "title_id");

DBRow[] rows = proprietaryMgrZyj.findAdminsByTitleId(true, 0L, title_id, 0, 0, pc, request);
//文件下载
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Title Linked Users</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	overflow-y:scroll;
	height:300px;
}

-->
</style>
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
  
   <script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">
<script type="text/javascript" src="../js/select.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>


<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 在线图片预览 -->
	 <script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<style type="text/css">
span.InfoLeft{width:60px;display:block;float:left;text-align:right;border:0px solid red;}
 span.InfoRight{display:block;text-align:left;float:left;text-indent:5px;}
 p{clear:both;text-align:center;}
</style>
</head>

<body onload="onLoadInitZebraTable()" leftmargin="10">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0" isNeed="false" class="zebraTable" isBottom="true">

    <tr> 
    	<th width="18%" nowrap="nowrap" class="right-title" style="text-align:center">Account</th>
        <th width="20%"  nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">Employee Name</th>
        <th width="31%" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">Warehouse / Area</th>
        <th width="31%" nowrap="nowrap"  class="right-title" style="vertical-align: center;text-align: center;">Department / Post</th>
    </tr>
  <%

String lockStr;
for ( int i=0; i<rows.length; i++ )
{
	if ( rows[i].get("llock",0)==0 )
	{
		lockStr	= "屏蔽";
	}
	else
	{
		lockStr	= "解除";
	}
%>
  <tr align="center">
  	<td height="30">
  		<%=rows[i].getString("account")%>
  	</td>
	<td>
		<%=rows[i].getString("employe_name")%>
	</td>
	<td>
		<%
		DBRow[] areaRows = accountMgr.findAdminWarehouseAreas(rows[i].get("adid", 0L));
		String allArea = ",";
		for(int j = 0; j < areaRows.length ; j ++)
		{
			String wa = areaRows[j].getString("warename")+(!StrUtil.isBlank(areaRows[j].getString("areaname"))?"»"+areaRows[j].getString("areaname"):"");
// 			if(allArea.indexOf(","+wa+",") == -1)
// 			{
				allArea += wa+",";
// 			}
		}
		if(areaRows.length > 0)
		{
			allArea = allArea.substring(1, allArea.length() -1);
			out.print("<span title="+allArea+">");
			out.print(areaRows[0].getString("warename")+(!StrUtil.isBlank(areaRows[0].getString("areaname"))?"»"+areaRows[0].getString("areaname"):""));
			if(areaRows.length > 1)out.print("&nbsp;...&nbsp;["+(areaRows.length-1)+"]");
			out.print("</span>");
			out.println();
		}
		else
		{
			out.println("&nbsp;");
		}
		%>
	</td>
    <td>
		<%
		DBRow[] deptposts = accountMgr.findAdminDepartmentPosts(rows[i].get("adid", 0L));
		String allPost = ",";
		for(int j = 0; j < deptposts.length; j ++)
		{
// 			if(allPost.indexOf(","+deptposts[j].getString("deptname")+"»"+deptposts[j].getString("postname")+",") == -1)
// 			{
				allPost += (deptposts[j].getString("deptname")+"»"+deptposts[j].getString("postname"))+","; 
// 			}
		}
		if(deptposts.length > 0)
		{
			allPost = allPost.substring(1, allPost.length() -1);
			out.print("<span title="+allPost+">");
			out.print(deptposts[0].getString("deptname")+"»"+deptposts[0].getString("postname"));
			if(deptposts.length > 1)out.print("&nbsp;...&nbsp;["+(deptposts.length-1)+"]");
			out.print("</span>");
			out.println();
		}
		else
		{
			out.println("&nbsp;");
		}
		%>
	</td>
  </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm"  action="proprietary_ct_admin_view.html"><!-- 只能以get方式提交 -->
    <input type="hidden" name="p"><!-- 点出go按钮或者点击翻页按钮时为此文本框赋值 -->
    <input type="hidden" name="cmd" value="<%=StringUtil.getString(request,"cmd") %>"/>
    <input type="hidden" name="proAdgid" value="<%=StringUtil.getString(request,"proAdgid") %>"/>
    <input type="hidden" name="proPsId" value="<%=StringUtil.getString(request,"proPsId") %>"/>
    <input type="hidden" name="proJsId" value="<%=StringUtil.getString(request,"proJsId") %>"/>
    <input type="hidden" name="filter_lid" value="<%=StringUtil.getString(request,"filter_lid") %>"/>
    <input type="hidden" name="lock_state" value="<%=StringUtil.getString(request,"lock_state") %>"/>
    <input type="hidden" name="title_id" value="<%=title_id%>"/>
  </form>
  <tr> 
    <td align="right" valign="middle" >
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " Total：" + pc.getAllCount() + " ");
out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      Goto
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
<br>
<script>
	
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>
</body>
</html>
