<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.ProductRelatesKey"%>
<%@ include file="../../include.jsp"%> 

<%
long user_adid = StringUtil.getLong(request, "user_adid");
long pc_id = StringUtil.getLong(request, "pc_id");
long pc_line_id = StringUtil.getLong(request, "pc_line_id");
String pc_line_title = StringUtil.getString(request, "pc_line_title", "");
int p = StringUtil.getInt(request,"p");
PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(10);
DBRow[] rows = proprietaryMgrZyj.findProprietaryAllByAdidAndProductLineId(user_adid, pc_line_id, pc_id, pc, request);
DBRow[] selRows = proprietaryMgrZyj.findProprietaryByAdidAndProductLineId(user_adid, pc_line_id, pc_id, null, request);
//System.out.println("pc_line:"+pc_id+","+pc_line_id+","+p);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>产品线title列表</title>
<%-- 
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
--%>
<script type="text/javascript">
function search()
{
	$("#search_form_pcLine").submit();
}
function refreshWindow()
{
	window.location.reload();
}
function submitSelectedPcLine(){
	if(volidate()){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryProductSeletedSaveAction.action',
			data:$("#subFormPcLine").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag)
				{
					if(data.flag == "-1")
					{
						showMessage("保存失败，此title已存在","alert");
					}
					else if(data.flag == "-2")
					{
						showMessage("保存失败，此title已被此用户所有","alert");
					}
					else if(data.flag == "-3")
					{
						showMessage("保存失败，此级别的title已存在","alert");
					}
					else
					{
						showMessage("保存成功","success");
						refreshWindow();
					}
				}
			},
			error:function(){
				showMessage("系统错误","error");
			}
		})	
	}
};
$(function()
{
	$("input:checkbox[name=proprietaryCheckPcLine]").click(
		function(){
			if($(this).attr("checked"))
			{
				var ht = "<input type='hidden' id='proprietary_product_line_"+$(this).val()+"' type='text' name='proprietary_product_pcLine' value='"+$(this).val()+"' />";
				$("#subFormPcLine").append($(ht));
			}
			else
			{
				$("#proprietary_product_line_"+$(this).val()).remove();
			}
		}
	);
});
function volidate(){
/*
	var checks = $("input:checkbox[name=proprietaryCheckPcLine]:checked");
	if(0 == checks.length)
	{
		alert("请选择title");
		return false;
	}
*/	
	return true;
};
///*
function go(index){
	var form = $("#pageForm");
	$("input[name='p']",form).val(index);
	form.submit();
}
//*/
</script>
<style type="text/css">
table.zebraTable tr{height: 30;}

</style>
</head>
  
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
	<br/>
	<form action="" name="search_form_pcLine" id="search_form_pcLine" method="post"> 
	
	</form>
		<table width="98%">
			<tr>
				<td align="left">
					<input name="select" type="button" class="long-button" value="提交" onclick="submitSelectedPcLine()"/>
					<%=!"".equals(pc_line_title)?("产品线："+pc_line_title):"" %>
				</td>
			</tr>
		</table>
	
	<br/>
	<form action="" id="subFormPcLine" method="post">
	<%
		for(int j = 0; j < selRows.length; j ++)
		{
	%>
	<input type="hidden" id='proprietary_product_line_<%=selRows[j].getString("title_id") %>' name="proprietary_product_pcLine"
				 value='<%=selRows[j].getString("title_id") %>'/>
	
	<%
		}
	%>
	<input type="hidden" name="user_adid" value='<%=user_adid %>'/>
	<input type="hidden" name="pc_id" value='<%=pc_id %>'/>
	<input type="hidden" name="pc_line_id" value='<%=pc_line_id %>'/>
	<input type="hidden" name="subFormType" value="<%=ProductRelatesKey.PC_LINE %>"/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
    		<th width="15%" class="left-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
	        <th width="20%" class="right-title" style="vertical-align: center;text-align: center;">titleId</th>
	        <th width="65%" class="left-title" style="vertical-align: center;text-align: center;">titleName</th>
		</tr>
		<%
		if(rows.length > 0)
		{
			for(int i = 0; i < rows.length; i ++)
			{
		%>
		<tr>
			<td width="15%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
   				<input type="checkbox" name="proprietaryCheckPcLine" <%=(0!=rows[i].get("tp_pc_id",0L)?"checked":"") %> value='<%=rows[i].get("title_id",0L) %>'/>
   			</td>
   			<td width="30%" align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
   				<%=rows[i].get("title_id",0L) %>
   			</td>
   			<td width="65%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
   				<%=rows[i].getString("title_name") %>
   			</td>
  		</tr>
		<%
			}
		}
		else
		{
		%>
		<tr>
			<td colspan="3" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
		</tr>
		<%		
		}
		%>
	</table>
	</form>
    <br/>
<form action="" id="pageForm" method="post">
	<input type="hidden" name="p" id="pageCount"/>
	<input type="hidden" name="cmd" value="cmd_pc_line"/>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle">
	<%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
          跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
</body>
</html>
