<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<%
	String fileName=StringUtil.getString(request,"fileName");
%>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

</head>
  
<body >
	<br/>
	<div style=" border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		上传结果
	</div>
	<br/>
    <div id="jieguo">
    
    </div>
</body>
</html>
<script>
(function(){
	   upload('<%=fileName %>');
})();

(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

function upload(fileName){

	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var para = 'fileName='+fileName;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/title/AjaxUploadTitleListAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:para,
		success: function(json){
			$.unblockUI();       //遮罩关闭		  
				var html='<table class="zebraTable" width="100%" border="0" cellspacing="1" cellpadding="0"  bgcolor="black">'+
							'<tr>'+
								'<th width="50%" style="vertical-align: center;text-align: center;" bgcolor="#eeeeee">TITLE名字</th>'+
								'<th style="vertical-align:center;text-align:center;" bgcolor="#eeeeee">错误信息</th>'+
							'</tr>';		
				var hl='</table>';
				var hh='';
				for(var i=0;i<json.length;i++){
					if(json[i].error=="成功"){
						hh+='<tr>'+
							   	'<td width="50%" align="left" bgcolor="#FFFFFF" style="color:green;padding-left:20px">'+json[i].titlename+'</td>'+
							    '<td align="center" bgcolor="#FFFFFF" style="color:green">'+json[i].error+'</td>'+
							'</tr>';
					}else{
						hh+='<tr>'+
						   	'<td width="50%" align="left" bgcolor="#FFFFFF" style="color:red;padding-left:20px">'+json[i].titlename+'</td>'+
						    '<td align="center" bgcolor="#FFFFFF" style="color:red">'+json[i].error+'</td>'+
						'</tr>';
					}
				}
				$('#jieguo').html(html+hh+hl);
		}
	});	 
}
</script>
