<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
String title_id = StringUtil.getString(request, "title_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Title Linked Category</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
.zebraTable td {
  /* border-bottom: 1px solid #dddddd; */
  padding-left: 10px;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
	    <input type="hidden" name="id">
	    <input type="hidden" name="parentid">
	    <input type="hidden" name="imp_color">
		<tr> 
          <th width="50%" colspan="2"  class="left-title">Category Name</th>
          <th width="25%"  class="right-title" style="vertical-align: center;text-align: center;">Product Quantity</th>
		</tr>
		<tr> 
			<td height="60" colspan="5">
			<script type="text/javascript">
				d = new dTree('d');
				d.add(0,-1,'Product Category</td><td align="center" valign="middle" width="70%">&nbsp;</td><td align="center" valign="middle" width="10%">&nbsp;</td></tr></table>');
				<%
					DBRow [] parentCategory = proprietaryMgrZyj.findProductCatagorysByTitleId(false, 0L, "", -1+"", "", title_id, 0, 0, null, request);
					for(int i=0;i<parentCategory.length;i++){	
				%>
					d.add(<%=parentCategory[i].getString("id")%>,<%=parentCategory[i].getString("parentid")%>,'<%=parentCategory[i].getString("title")%></td><td align="center" valign="middle" width="30%"><%=productMgrZyj.getProductsByCategoryidAndTitleid(parentCategory[i].get("id",0l), (title_id==null || title_id=="") ? 0 : Long.valueOf(title_id))%></td></tr></table>','');
				<%
					}
				%>
				document.write(d);
				
				function closeWin() {
					tb_remove();
				}
			</script>
			</td>
		</tr>
	</table>
</form>
<br>
</body>
</html>