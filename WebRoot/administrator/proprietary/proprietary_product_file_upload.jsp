<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@ include file="../../include.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>商品图片上传</title>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<!-- 在线图片预览 -->
	 <script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	 
  	
  	<%
	   	  		String updateTransportAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportProductFileCompleteAction.action";
	   	  			String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	   	  	  		String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	   	  	  		String pc_id = StringUtil.getString(request,"pc_id");
	   	  	  		String first_pc_name = StringUtil.getString(request, "theFirstName");
	   	  	  		DBRow[] productRows = transportMgrZr.getProductByPcIds(pc_id);
	   	  	  		//读取配置文件中的transport_product_file
	   	  	  		String value = systemConfig.getStringConfigValue("transport_product_file");
	   	  	  		String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
	   	  	  		String file_with_class = StringUtil.getString(request,"file_with_class");
	   	  	  		int is_unable_to_provide = StringUtil.getInt(request,"is_unable_to_provide");//是否是无法提供照片
	   	  	  		String[] arraySelected = value.split("\n");
	   	  	  		//将arraySelected组成一个List
	   	  	  		ArrayList<String> selectedList= new ArrayList<String>();
	   	  	  		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
	   	  	  				String tempStr = arraySelected[index];
	   	  	  				if(tempStr.indexOf("=") != -1){
	   	  	  					String[] tempArray = tempStr.split("=");
	   	  	  					String tempHtml = tempArray[1];
	   	  	  					selectedList.add(tempHtml);
	   	  	  				}
	   	  	  		}
	   	  	  		// 获取所有的关联图片 然后在页面上分类
	   	  	  		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
	   	  	  		DBRow[] imagesRows = productMgrZyj.getAllProductFileByPcId(pc_id,FileWithTypeKey.PRODUCT_SELF_FILE,pc_id,is_unable_to_provide);
	   	  	  		 
	   	  			if(imagesRows != null && imagesRows.length > 0 ){
	   	  		for(int index = 0 , count = imagesRows.length ; index < count ; index++ ){
	   	  			DBRow tempRow = imagesRows[index];
	   	  			String product_file_type = tempRow.getString("product_file_type");
	   	  			List<DBRow> tempListRow = imageMap.get(product_file_type);
	   	  			if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1)){
	   	  				tempListRow = new ArrayList<DBRow>();
	   	  			}
	   	  			tempListRow.add(tempRow);
	   	  			imageMap.put(product_file_type,tempListRow);
	   	  			
	   	  		}
	   	  			}
	   	  	  		String backurl =  ConfigBean.getStringValue("systenFolder") +"administrator/proprietary/proprietary_product_file_upload.html?pc_id="+pc_id+"&theFirstName="+first_pc_name ;
	   	  	  		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	   	  	  		DBRow adminRow = adminMgrGZY.getDetailAdmin(adminLoggerBean.getAdid());
	   	  	%>
  	<script type="text/javascript">
  		
  		function submitForm(){
  	  		var file = $("#file");
  	  		if(file.val().length < 1){
  	  	  		showMessage("请选择上传文件","alert");
				return ;
  	  	  	}
			var myform = $("#myform");
			myform.submit();
  	  	}
  	  	function fileWithClassTypeChange(){
		 	var file_with_class = $("#file_with_class").val();
		   	$("#tabs").tabs( "select" , file_with_class * 1-1 );
			 
    	 }
   	 jQuery(function($){
    
	   	  $("#tabs").tabs({
	   			cache: true,
	  			cookie: { expires: 30000 }, 
	  		 	select: function(event, ui){
				 $("#file_with_class option[value='"+(ui.index+1)+"']").attr("selected",true);
				}
	   	 });
	   	if('<%= file_with_class%>'.length > 0){
			 $("#tabs").tabs( "select" , '<%= file_with_class%>' * 1-1 );
			 $("#file_with_class option[value='"+'<%= file_with_class%>'+"']").attr("selected",true);
		}
	   	 fileWithClassTypeChange();
   	  })
  	function deleteFile(file_id){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'product_file',file_id:file_id,folder:'product',pk:'pf_id'},
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}else{
					showMessage("系统错误,请稍后重试","error");
				}
			},
			error:function(){
				showMessage("系统错误,请稍后重试","error");
			}
		})
	}
   	function downLoad(fileName , tableName , folder){
   		 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ '<%= systemConfig.getStringConfigValue("file_path_product")%>');
   }
  	//文件上传
    //做成文件上传后,然后页面刷新提交数据如果是有添加文件
      function uploadFile(_target){
          var targetNode = $("#"+_target);
          var fileNames = $("input[name='file_names']").val();
          var obj  = {
      	     reg:"picture_office",
      	     limitSize:2,
      	     target:_target
      	 }
          var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
      	uri += jQuery.param(obj);
      	 if(fileNames && fileNames.length > 0 ){
      		uri += "&file_names=" + fileNames;
      	}
      	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
          		 close:function(){
  					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
  					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
         		 }});
      }
    //jquery file up 回调函数
      function uploadFileCallBack(fileNames,target){
	  $("input[name='file_names']").val(fileNames);
          if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
              $("input[name='file_names']").val(fileNames);
              var myform = $("#myform");
              var file_with_class = $("#file_with_class").val();
  			$("#backurl").val("<%= backurl%>" + "&file_with_class="+file_with_class);
              myform.submit();
      	}
          
      }
      function onlineScanner(){
    	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
    	 $.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
      }
      function unableToProvidePhotos(product_name)
  	  {
  	  	var product_names = "";
  	  	if(product_name.length > 0)
  	  	{
			var product_name_arr = product_name.split(",");
			if(product_name_arr.length > 0)
			{
				for(var i = 0; i < product_name_arr.length; i ++)
				{
					product_names += product_name_arr[i];
					if(1 == i % 2)
					{
						product_names += "\n";
					}
					else
					{
						product_names += ",";
					}
				}
			}
  	  	}
  		if(confirm('<%=adminLoggerBean.getEmploye_name()%>,你确定无法提供以下商品的照片吗？\n'+product_names))
		{
  			$("input[name='file_names']").val(product_names);
            var file_with_class = $("#file_with_class").val();
			$("#backurl").val("<%= backurl%>" + "&file_with_class="+file_with_class);
			$("#is_unable_to_provide").val(1);
			$("#myform").submit();
		}
  	  }
  	</script>
  	<style type="text/css">
  		.zebraTable td {line-height:25px;height:25px;}
		.right-title{line-height:20px;height:20px;}
		ul.ul_p_name{list-style-type:none;margin-left:-44px;}
		ul.ul_p_name li{margin-top:1px;margin-left:3px;line-height:25px;height:25px;padding-left:5px;padding-right:5px;float:left;border:1px solid silver;}
  		ul.fileUL{list-style-type:none;clear:both;}
		ul.fileUL li{line-height:20px;height:20px;padding-left:3px;padding-right:3px;margin-left:2px;float:left;margin-top:1px;border:1px solid silver;}
  	</style>
  </head>
  
  <body onload = "onLoadInitZebraTable()">
   	<!-- 支持多个图片同时上传 ,下面显示图片(用轮播器显示)-->
   	<form id="myform"   method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/product/ProductPictureUploadAction.action" %>'>
   		<input type="hidden" name="pc_id" value="<%=pc_id %>" />
   		 <input type="hidden" name="backurl" id="backurl" value=""/>
   		 <input type="hidden" name="first_pc_name" value='<%=first_pc_name %>'/>
   		 <input type="hidden" name="is_unable_to_provide" id="is_unable_to_provide"/>
   		<table>
   		<tr>
   			<td style="width:55px;">
   				关联商品:
   			</td>
   			<td colspan="2" style="text-align:left;">
   				 <%
   					String product_names = "";
   				 	if(productRows != null && productRows.length > 0)
   				 	{
   				 		 %>
   						<ul class="ul_p_name" style="float:left;">	
   				 		 <% 
   				 			for(DBRow tempRow : productRows)
   				 			{
	   				 		String product_name = tempRow.getString("p_name");
	   				 		product_names += product_name + ",";
   				 		 %>
   				 		 	<li> <%=product_name%></li>
   				 		 <% 
   				 			}
  						 %>
  						</ul>
  						 <% 
  						 if(product_names.length() > 0)
  						 {
  							product_names = product_names.substring(0,product_names.length()-1);
  						 }
   				 	}
   				 %>
   			</td>
   		</tr>
   			<tr>
   				<td>文件类型:</td>
   				<td>
   					<select id="file_with_class" name="file_with_class" onchange="fileWithClassTypeChange();">
   					 	<%if(arraySelected != null && arraySelected.length > 0){
 									for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
 									String tempStr = arraySelected[index];
 									String tempValue = "" ;
 									String tempHtml = "" ;
 									 
 									if(tempStr.indexOf("=") != -1){
 										String[] tempArray = tempStr.split("=");
 										tempValue = tempArray[0];
 										tempHtml = tempArray[1];
 									}
 								%>		
 									<option value="<%=tempValue %>"><%=tempHtml %></option>
 
 								<%	} 
 								}
 								%>
   					</select>
   				</td>
   				<td>
		 			<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PRODUCT_SELF_FILE %>" />
		 			<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_product")%>" />
   					<input type="hidden" name="file_names" />
   				</td>
   				 
   			</tr>
   		</table>
   		 <!-- 显示出所有的商品图片 tabs-->
   		 <div id="tabs" style="margin-top:15px;">
		   		 	<ul>
		   		 		<%
					 		if(selectedList != null && selectedList.size() > 0){
					 			for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
					 			%>
					 				<li><a href="#transport_product_<%=index %>"> <%=selectedList.get(index) %></a></li>
					 			<% 	
					 			}
					 		}
				 		%>
		   		 	</ul>
   		 	<%
   		 		for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
   		 				List<DBRow> tempListRows = imageMap.get((index+1)+"");
   		 				
   		 			%>
   		 			 <div id="transport_product_<%= index%>">
   		 			 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
	   		 			 	<tr>
			   		 			 <%
			   		 			 	String product_photo_description = systemConfig.getStringConfigValue("product_photo_description");
			   		 			 	if(!"".equals(product_photo_description))
			   		 			 	{
			   		 			 		String[] product_photo_description_arr = product_photo_description.split("\n");
			   		 			 		if(index < product_photo_description_arr.length)
			   		 			 		{
			   		 			 %>
			   	   		 			 	<td colspan="2">注:<%=product_photo_description_arr[index] %></td>
			   	   		 		 <%				
			   		 			 		}
			   		 			 	}
			   		 			 %>
	   		 			 	</tr>
  		 			 		<tr> 
		  						<th width="55%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
		  						<th width="45%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
		  					</tr>
   		 			
   		 			 	<%
   		 			 		if(tempListRows != null && tempListRows.size() > 0){
   		 			 		 
   		 			 			 for(DBRow row : tempListRows){
   		 			 			%>
   		 			 				<tr>
   		 			 					<td>
   		 			 				     <%
				 			 	           if(row.get("is_unable_to_provide",0l)==2){
				 			 	         %>
   		 			 					<!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	 	                 <%if(StringUtil.isPictureFile(row.getString("file_name"))){ %>
				 			 	             <p>
				 			 	 	            <a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.PRODUCT_SELF_FILE %>','<%=row.get("file_with_id",0l) %>','<%=row.getString("file_name") %>','<%=row.get("product_file_type",01) %>','<%= pc_id %>','<%= systemConfig.getStringConfigValue("file_path_product")%>');"><%=row.getString("file_name") %></a>
				 			 	             </p>
			 			 	                 <%}else{ %>
   		 			 					        <a  href='<%= downLoadFileAction%>?file_name=<%=row.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_product")%>'><%=row.getString("file_name") %></a>
   		 			 					  <%} 
				 			 	          }else if(row.get("is_unable_to_provide",0l)==1){
				 			 	            	 if(StringUtil.isPictureFile(row.getString("file_name"))){ %>
					 			 	             <p>
					 			 	 	            <a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.PRODUCT_SELF_FILE %>','<%=row.get("file_with_id",0l) %>','<%=row.getString("file_name") %>','<%=row.get("product_file_type",01) %>','<%= pc_id %>','<%= systemConfig.getStringConfigValue("file_path_admin")%>');"><%=row.getString("file_name") %></a>
					 			 	             </p>
				 			 	                 <%}else{ %>
	   		 			 					         <a  href='<%= downLoadFileAction%>?file_name=<%=row.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_admin")%>'><%=row.getString("file_name") %></a>
	   		 			 					    <%} 
				 			 	           } %>
   		 			 					</td>
   		 			 					<td>
   		 			 						<%= row.getString("p_name")%> &nbsp;
   		 			 					</td>
   		 			 				</tr>	
   		 			 			<% 
   		 			 		 }
   		 			 		}else{
   		 			 			%>
 		 			 			<tr>
 									<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
 								</tr>
   		 			 			<%
   		 			 		}
   		 			 	%>
   		 			 	 	</table>
   		 			 </div>
   		 			<% 
   		 		}
   		 	%>
   		 	
   		 </div>
   		 

   	</form>	
  </body>
  <script type="text/javascript">
//显示在线图片
    //图片在线显示  		 
 function showPictrueOnline(fileWithType,fileWithId ,currentName,productFileType,pcId,uploadPath){
   var obj = {
   		file_with_type:fileWithType,
   		file_with_id : fileWithId,
   		current_name : currentName ,
   		product_file_type:productFileType,
   		pc_Id : pcId,
   		cmd:"multiFile",
   		table:'product_file',
   		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+uploadPath
	}
   if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
}		
  </script>
</html>
