<%@page import="com.cwc.app.key.YesOrNotKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String pcid = StringUtil.getString(request,"pcid");
String cmd = StringUtil.getString(request,"cmd","filter");
String key = StringUtil.getString(request,"key");
String pro_line_id = StringUtil.getString(request,"pro_line_id");
int product_file_types = StringUtil.getInt(request, "product_file_types");
int product_upload_status = StringUtil.getInt(request, "product_upload_status");
int union_flag = StringUtil.getInt(request,"union_flag", -1);
String title_id = StringUtil.getString(request, "title_id");
long admin_user_id = StringUtil.getLong(request, "admin_user_id");
boolean edit = true;
//System.out.println("admin_user_id:"+admin_user_id+",titlePc:"+title_id);
//System.out.println("proprietary_ct_product.jsp:cmd:"+cmd);
//System.out.println("pcid:"+pcid+",cmd:"+cmd+",key:"+key+",union_flag："+union_flag);	
//System.out.println(",pro_line_id:"+pro_line_id+",product_file_types:"+product_file_types+",product_upload_status:"+product_upload_status+",title_id:"+title_id);

Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Title Linked Product</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- Frank ****************** --%>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<%-- ***********************  --%>
<%-- 此版本的jquery ui与新版autocomplete不兼容
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-ui-1.8.14.custom.min.js"></script>
--%>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<%-- Frank ****************** --%>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<%-- ***********************  --%>

<%-- 此版本的jquery ui与新版autocomplete不兼容
<link type="text/css" href="../js/jqGrid-4.1.1/js/jquery-ui-1.8.1.custom.css" rel="stylesheet"/>
--%>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />


<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<style type="text/css">
	td.categorymenu_td div{margin-top:-3px;}
</style>
<script language="javascript">
var pcid = '<%=pcid%>';
var cmd = '<%=cmd%>';
var union_flag = '<%=union_flag%>';
$().ready(function() {

	function log(event, data, formatted) {
		
	}

	<%-- Frank ****************** --%>
	addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
	<%-- ***********************  --%>
	
	$("#search_key").keydown(function(event){
		if (event.keyCode==13)
		{
			search();
		}
	});

});

function filter()
{
	var product_file_types = $("#product_file_types").val();
	var product_upload_status = $("#product_upload_status").val();
	var filter_pcid = $("#filter_pcid").val();
	union_flag = $("input#union_flag:checked").val();
	var product_line_id = $("#filter_productLine").val();
	jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:'filter',pcid:filter_pcid,union_flag:union_flag,pro_line_id:product_line_id,product_file_types:product_file_types,product_upload_status:product_upload_status, title_id:'<%=title_id%>',admin_user_id:'<%=admin_user_id%>'},page:1}).trigger('reloadGrid');
	
	jQuery("#union").jqGrid('setGridParam',{url:"<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_sub_data_products.html?id="+0,page:1});
	jQuery("#union").jqGrid('setCaption',"Suit Relationship: ").trigger('reloadGrid');
		
}

function afilter(filter_pcid)
{
	jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:'filter',pcid:filter_pcid,union_flag:0},page:1}).trigger('reloadGrid');
	$("#category").mcDropdown("#categorymenu").setValue(filter_pcid);
	$("input[id=union_flag][value='<%=union_flag%>']").attr('checked',true); 
	
}

<%
StringBuffer countryCodeSB = new StringBuffer("");
DBRow treeRows[] = catalogMgr.getProductCatalogTree();
String qx;

countryCodeSB.append("0:选择商品分类;");
for (int i=0; i<treeRows.length; i++)
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
	 
	countryCodeSB.append(treeRows[i].getString("id")+":"+Tree.makeSpace("&nbsp&nbsp&nbsp",treeRows[i].get("level",0))+qx+treeRows[i].getString("title"));
	if(i<treeRows.length-1)
	{
		countryCodeSB.append(";");
	}
	
}
%>


function closeWin()
{
	tb_remove();
}

function modCatalogCloseWin(pc_id)
{
	//tb_remove();
	refProduct($("#gridtest"),pc_id);
	
}

function openCatalogMenu()
{
	$("#category").mcDropdown("#categorymenu").openMenu();
}

function showProductImg(img)
{
	if(img != "")
	{
		tb_show('',img+'?TB_iframe=true&height=350&width=700',false);
	}
	else
	{
		alert("该商品暂时没有图片");
	}
}

function addProductCode(id)
{
	
	$.artDialog.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_product_code_view.html?pc_id="+id, {title: '商品条码',width:'500px',height:'300px', lock: true,opacity: 0.3});
	//tb_show('修改商品分类','mod_product_catalog.html?pc_id='+id+'TB_iframe=true&height=200&width=300',false);
}

function autoComplete(obj)
	{
	<%-- Frank ****************** --%>
		addAutoComplete(obj,
				"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
				"merge_info",
				"p_name");
		
	}
	function beforeShowAdd(formid)
	{
		$("#tr_unit_name").remove();
		$("#tr_p_code").remove();
		$("#tr_weight").remove();
		$("#tr_unit_price").remove();
		$("#tr_volume").remove();
		$("#tr_gross_profit").remove();
		jQuery("#union").jqGrid('setGridParam',{editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditUnionJqgrid.action?set_pid='+union_id,});
	}
	
	function beforeShowAddProduct(formid)
	{
		$("#tr_alive_text").remove();
	}
	
	function beforeShowEdit(formid)
	{
		
	}
	
	function beforeShowDel(formid)
	{
		jQuery("#union").jqGrid('setGridParam',{editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditUnionJqgrid.action?set_pid='+union_id,});
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	
	function refProductUnion(obj,rowid,p_name)
	{
		var para ="p_name="+p_name;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductGridJSONByPname.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				async:false,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	
	function refProduct(obj,rowid)
	{
		var para ="pc_id="+rowid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductGridJSONById.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				async:false,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}
	
	function refushUnion(obj,pid)
	{
		var para ="pid="+pid;
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getUnionProductDetailsByPid.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(){
						alert("提交失败，请重试！");
					},
					
					success: function(data)
					{
						for(i=0;i<data.length;i++)
						{
							obj.setRowData(data[i].set_pid,data[i]);
						}
					}
				});
	}
	
	function afterComplete()
	{
		refProduct($("#gridtest"),union_id);
	}
	
	function search()
	{
		if ($("#search_key").val()=="")
		{
			alert("请填写关键字");
			return(false);
		}
		else
		{
			union_flag = $("input#union_flag:checked").val();
			var filter_pcid = $("#filter_pcid").val();
			var key = $("#search_key").val();
			jQuery("#gridtest").jqGrid('setGridParam',{postData:{cmd:'search',pcid:filter_pcid,key:key,title_id:'<%=title_id%>',admin_user_id:'<%=admin_user_id%>'}}).trigger('reloadGrid');
			
			jQuery("#union").jqGrid('setGridParam',{url:"<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_sub_data_products.html?id="+0,page:1});
			jQuery("#union").jqGrid('setCaption',"Suit Relationship: ").trigger('reloadGrid');
		}
	}
	
	
	function swichProductAlive(name,pc_id,curAlive)
	{
		var msg;
		
		if (curAlive==1)
		{
			msg = "确定禁止 "+name+" 抄单？";
		}
		else
		{
			msg = "确定允许 "+name+" 抄单？";
		}
	
		if (confirm(msg))
		{
			var para ="pc_id="+pc_id+"&curAlive="+curAlive;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/swichProductAlive.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert(e);
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					if(data.result=="true")
					{
						refProduct(jQuery("#gridtest"),pc_id);
					}
					
				}
			});
		}
	}
	
	function ajaxLoadCatalogMenuPage(id,title_id,category_id,divId)
	{
			var para = "id="+id+"&title_id="+title_id+"&catalog_id="+category_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
	
	function showImg(pc_id)
	{
		window.open("product_file.html?pc_id="+pc_id);
	}

	 function make_tab(productId)
	 {
		var purchase_id="";
		var factory_type="";
		var supplierSid="";
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/lable_template/lable_template_show.html?purchase_id='+purchase_id+'&pc_id='+productId+'&factory_type='+factory_type+'&supplierSid='+supplierSid; 
		$.artDialog.open(uri , {title: '标签列表',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	 }
	 function manage_pro_title(pc_id,p_name)
	 {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_list_by_product_related_for_select.html?pc_id="+pc_id;
		$.artDialog.open(url , {title: '管理title:'+p_name,width:'700px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	 };
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:middle;
		padding:3px;
	}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="65%">
	<div style="border:0px #dddddd solid;background:#eeeeee;padding:0px;-webkit-border-radius:7px;-moz-border-radius:7px;width:1200px;">
	</div>
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0" id="gridtest"></table>
<script type="text/javascript">
function addProductsPicture()
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_file_upload.html?";
	var addUri =  getSelectedIdAndNames();
 	if($.trim(addUri).length < 1 ){
		showMessage("请先选择商品!","alert");
		return ;
	}else{uri += addUri;}
 	var reg		= new RegExp("&","g"); //创建正则RegExp对象 
	var pc_ids	= addUri.substr(1).replace(reg,"=");  
	var pcIds	= pc_ids.split("=")[1];
	$.artDialog.open(uri , {title: "商品图片上传",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true,
		close:function()
		{
			if(pcIds.length > 0)
			{
				var pcIdArr = pcIds.split(",");
				if(pcIdArr.length > 0)
				{
					for(var i = 0; i < pcIdArr.length; i ++)
					{
						modCatalogCloseWin(pcIdArr[i]);
					}
				}
			}
		}
	});
}
function getSelectedIdAndNames(){
	s = jQuery("#gridtest").jqGrid('getGridParam','selarrrow')+"";
	var theFirstName = "";
 	var array = s.split(",");
	var strIds = "";
 	for(var index = 0 , count = array.length ; index < count ; index++ ){
 	   if(0 == index)
 	   {
 		  theFirstName = $("#gridtest").getCell(array[0],"p_name");
 	   }
	   var number = array[index];
 	   var thisid= $("#gridtest").getCell(number,"pc_id");
 	   strIds += (","+thisid);
	}
	if(strIds.length > 1 ){
	    strIds = strIds.substr(1);
	}
	if(strIds+"" === "false"){return "";}
	
	return  "&pc_id="+strIds+"&theFirstName="+theFirstName;
}
function addProductPicture(pc_id,name)
{
	//var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_product_file_upload.html?pc_id="+pc_id+"&theFirstName="+name;
	//$.artDialog.open(uri , {title: "商品["+pc_id+"]图片上传",width:'770px',height:'470px', lock: true,opacity: 0.3,close:function(){modCatalogCloseWin(pc_id);},fixed: true});
	
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_file_upload.html?pc_id="+pc_id+"&theFirstName="+name+'&can_operate=<%=YesOrNotKey.NO%>';
	$.artDialog.open(uri , {title: "Photos | "+name+"",width:'875px',height:'500px', lock: true,opacity: 0.3,close:function(){modCatalogCloseWin(pc_id);},fixed: true});
	
}
</script>
<div id="pager2"></div>
<br/>
<table id="union"></table>
<div id="pageunion"></div>  
	
	<script type="text/javascript">
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					//scroll:1,
					sortable:true,
					url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_data_products.html',
					postData:{cmd:"",pcid:<%="".equals(pcid)?"0":pcid%>,pro_line_id:<%="".equals(pro_line_id)?"0":pro_line_id%>,admin_user_id:<%=admin_user_id%>,title_id:<%="".equals(title_id)?"0":title_id%>},
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.99),
					height:235,
					autowidth: false,
					shrinkToFit:true,
					jsonReader:{
				   			id:'pc_id',
	                        repeatitems : false,
	                        subgrid:{repeatitems : false}
	                	},
				   	colNames:['pc_id','catalog_id','','Product Name','Main Code','Category','Unit','Length','Width','Height','Weight','Price','Volume'], 
				   	colModel:[ 
				   		{name:'pc_id',index:'pc_id',hidden:true,sortable:false},
				   		{name:'catalog_id',index:'catalog_id',hidden:true,sortable:false},
				   		{name:'unionimg',index:'unionimg',sortable:false,width:20},
				   		{name:'p_name',index:'p_name',align:'left',editrules:{required:true},width:200},
				   		{name:'p_code',index:'p_code',align:'left',editrules:{required:true},width:160},
				   		//{name:'p_codes',index:'p_codes',align:'left',width:160},
				   	
				   		{name:'catalog_text',index:'catalog_text',sortable:false,align:'left',width:160},
				   		{name:'unit_name',index:'unit_name',sortable:false,align:'center',width:60,width:35}, 
				   		
				   		{name:'length',index:'length',sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","},width:34},
				   		{name:'width',index:'width',sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","},width:34},
				   		{name:'heigth',index:'heigth',sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","},width:34},
				   		
				   		{name:'weight',index:'weight',sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:3,thousandsSeparator: ","},width:38},
				   		{name:'unit_price',index:'unit_price',formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","},sortable:false,align:'center',width:60},
				   		{name:'volume',index:'volume',align:'center',formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","},width:65}
				   		//,{name:'img',index:'img',sortable:false,align:'left',width:164}
				   		], 
				   	rowNum:50,//-1显示全部
				   	mtype: "GET",
					rownumbers:true,
				   	pager:'#pager2',
				   	sortname: 'pc_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				    cellEdit: true, 
				   	cellsubmit: 'remote',
				   	//multiselect: true,
				   	caption: "Product Informations",
				   	onSelectCell: function(id,name)
	   					{
	   					 	if(id == null) 
	   					 	{ 
	   					 		ids=0; 
	   					 		if(jQuery("#union").jqGrid('getGridParam','records')>0 )
	   					 		{
	   					 			jQuery("#union").jqGrid('setGridParam',{url:"<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_sub_data_products.html?id="+id,page:1});
	   					 			jQuery("#union").jqGrid('setCaption',"Suit Relationship: ").trigger('reloadGrid');
	   					 		} 
	   					 	} 
	   					 	else 
	   					 	{ 
	   					 		var p_name = jQuery("#gridtest").jqGrid('getCell',id,'p_name');
	   					 		if(name=="catalog_text")
	   					 		{
	   					 			updataProductCatalog(id);
	   					 		}
	   					 		if(name=="img")
	   					 		{
	   					 			addProductPicture(id,p_name);
	   					 		}
	   					 		
	   					 		jQuery("#union").jqGrid('setGridParam',{url:"<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_sub_data_products.html?id="+id,page:1});
	   					 		jQuery("#union").jqGrid('setCaption',p_name+" Suit Relationship").trigger('reloadGrid');
	   					 	} 
	   					 }
				   	}); 		   	
			   	
			   	var union_id;//增加、删除套装配件时记录当前主商品ID
			   	$("#union").jqGrid({
					sortable:true,
					url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_sub_data_products.html',
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.95),
					height:150,
					autowidth: false,
					shrinkToFit:true,
					caption: "Suit Relationship",
					jsonReader:{
				   			id:'pid',
	                        repeatitems : false,
	                	},
				   	
				   	colNames: ['set_pid','pid','catalog_id','Accessories Name','Combination Amount','Unit','Code','Category','Weight','Price','Volume','Gross Profit Rate%'], 
				   	colModel: [ 
				   				{name:'set_pid',index:'set_pid',editoptions:{readOnly:true},hidden:true,sortable:false},
				   				{name:'pid',index:'pid',hidden:true,sortable:false},
				   				{name:'catalog_id',index:'catalog_id',hidden:true,sortable:false},
				   				
				   				{name:"p_name",index:"p_name",width:300,align:"left"}, 
				   				{name:"quantity",index:"quantity",width:100,align:"center",width:80,formatter:'number',formatoptions:{decimalPlaces:0,thousandsSeparator: ","}}, 
				   				{name:"unit_name",index:"unit_name",width:100,align:"center",width:80},
				   				
				   				{name:'p_code',index:'p_code',align:'left'},
				   				{name:'catalog_text',index:'catalog_text',sortable:false,align:'left'},
				   				{name:'weight',index:'weight',sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:3,thousandsSeparator: ","},width:60},
				   				{name:'unit_price',index:'unit_price',formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","},sortable:false,align:'center',width:60},
				   				{name:'volume',index:'volume',align:'center',formatoptions:{decimalPlaces:3,thousandsSeparator: ","},width:60},
				   				{name:'gross_profit',index:'gross_profit',formatter:'currency',formatoptions:{decimalPlaces:1,thousandsSeparator: ",",suffix:"%"},width:60,align:'center'}
				   			  ],
				   	rowNum:100,//-1显示全部
				   	pgtext:false,
				   	pgbuttons:false,
				   	mtype: "GET",
				   	gridview:true,
					rownumbers:true,
				   	pager:'#pageunion',
				   	sortname: 'pc_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/gridEditUnionJqgrid.action',
				   	loadComplete:function(data)
				   	{
				   		union_id = data.union_id;
				   	}
				   	}); 		   	
			   	</script>

<br/>
</body>
</html>
