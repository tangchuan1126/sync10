<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@page import="org.directwebremoting.json.types.JsonArray"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<title>Manage Titles</title>
<link rel="stylesheet" type="text/css" href="../js/Font-Awesome/css/font-awesome.min.css" />

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/simple.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>

<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<style type="text/css">
	.myulAdmin,.myulPcLine,.myulPcCata{
		margin:0; 
		padding:0 10px;
	}
   .content{padding:10px;}
   
   .ui-widget-content{
	border:1px #CFCFCF solid;
	border-radius: 0px;
  	-webkit-border-radius: 0px;
	-moz-border-radius:0;
}
.ui-widget-header{
	background:#f1f1f1 !important;
	border-radius: 0;
	border-top:0;
	border-left:0;
	border-right:0;
	border-bottom: 1px #CFCFCF solid;
}
.ui-tabs .ui-tabs-nav li a{
	padding: .5em 1em 13px !important;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
	  height: 40px;
	  margin-top: -4px !important;
	background:#fff;
}
.ui-tabs .ui-tabs-nav{
	padding :0 .2em 0;
}
.ui-tabs{
	padding:0;
}
   
   .panel {
		margin-top:23px;
		  margin-bottom: 20px;
		  background-color: #fff;
		  border: 1px #CFCFCF solid;
		  -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
		  box-shadow: 0 1px 1px rgba(0,0,0,.05);
		  
		}
		.panel .panelTitle{
			  background: #f1f1f1;
			  height: 40px;
			  line-height: 40px;
			  padding:0 15px;
		}
		.panel .panelTitle .title-left{
			    float: left;
		}
		.panel .panelTitle .title-right{
			  float: right;
		}
		.clear{
			  clear: both;
		}
		.right-title{
			height:40px;
			background-color:#fff;
		}
		.zebraTable th.right-title,.zebraTable th.left-title{background-color:#fff;height:35px;}
		.zebraTable th.right-title{border-left:0;border-right:0;}
		.zebraTable th.left-title{border-right:0;}
		
		
		
		.aui_titleBar{  border: 1px #DDDDDD solid;}
		.aui_header{height: 40px;line-height: 40px;}
		.aui_title{
		  height: 40px;
		  width: 100%;
		  font-size: 1.2em !important;
		  margin-left: 20px;
		  font-weight: 700;
		  text-indent: 0;
		}
		.aui_close{
				color:#fff !important;
				text-decoration:none !important;
		}
		
		.breadnav {
            padding:0 10px; height:25px;margin-bottom: 18px;
        }
        .breadcrumb{
          font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
          font-size: 14px;
          line-height: 1.42857143;
        }
        .breadcrumb {
          padding: 8px 15px;
          margin-bottom: 20px;
          list-style: none;
          background-color: #f5f5f5;
          border-radius: 4px;
        }
	.breadcrumb a{
		      color: #337ab7;
                text-decoration: none;	
		}
        .breadcrumb > li {
          display: inline-block;
        }

        .breadcrumb > .active {
          color: #777;
        }

        .breadcrumb > li + li:before {
            padding: 0 5px;
            color: #ccc;
            content: "/\00a0";
        }
</style>

<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long adminId=adminLoggerBean.getAdid();
Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
String admin_user = StringUtil.getString(request, "admin_user");
long admin_user_id = StringUtil.getLong(request, "admin_user_id");
DBRow[] rows = proprietaryMgrZyj.findProductLinesIdAndNameByTitleId(true, 0l, "","", 0, 0, null, request);
%>
<script>
function loadData(){
	var productLine = <%=new JsonObject(rows).toString()%> ;
	var data=[
			 {
				 key:'Product Line：',
				 type:'line',

				 son_key:'Parent Category：',
				 url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadFirstProductCatalogAction.action',

				 son_key2:'Sub Category：',
				 url2:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadSecondProductCatalogAction.action',	
						
				 son_key3:'Sub-Sub Category：',
			 	 url3:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadThirdProductCatalogAction.action',
				 
				 array:productLine}
		     ];
	initializtion(data);  //初始化
}

function searchForm(){
	var admin_user_id=$('#admin_user_id').val();
    var para='admin_user_id='+admin_user_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
		type: 'post',
		dataType: 'html',
		data:para,
		async:false,
		success: function(html){
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	
}

function adminUserSelect(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:1, 					// 1表示的 单选
			 user_ids:$("#admin_user_id").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowSelect'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}


var line_id='';
var catalog_id='';
function custom_seach(){
	
    var array = getSeachValue();
    
    line_id='';
    catalog_id='';
    
    for(var i=0;i<array.length;i++){
    	
        if(array[i].type=='line'){
        	
        	line_id=array[i].val;
        
        }else if(array[i].type=='line_son'){
 		   
        	catalog_id=array[i].val;
 	    
        }else if(array[i].type=='line_son_son'){
 			
        	catalog_id=array[i].val;
 	    
        }else if(array[i].type=='line_son_son_son'){
 			
        	catalog_id=array[i].val;
 	    }
    }

    var para='filter_productLine='+line_id+'&filter_pcid='+catalog_id;
    
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
		type: 'post',
		dataType: 'html',
		data:para,
		async:false,
		success: function(html){
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	
}

function go(number){
	
	 var para='filter_productLine='+line_id+'&filter_pcid='+catalog_id+'&p='+number;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
			type: 'post',
			dataType: 'html',
			data:para,
			async:false,
			success: function(html){
				$("#showList").html(html);
				onLoadInitZebraTable(); //重新调用斑马线样式
			}
		});	
}
$(function(){
	addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryGetSearchJSONAction.action",
			"merge_field","merge_field");
});

function search(){
	
	var val = $("#search_key").val();
	
	var search_mode = 2;
	var para = "titleId="+val.trim().toLowerCase()+"&search_mode="+search_mode;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
		type: 'post',
		dataType: 'html',
		data:para,
		async:false,
		success: function(html){
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	
}

function searchRightButton(){
	
	var val = $("#search_key").val();
			
	if (val=="")
	{
		alert("你好像忘记填写关键词了？");
	}
	else
	{
		val = val.replace(/\'/g,'');
		$("#search_key").val(val);

		var titleId = val;
		var search_mode = 1;
		var para = "titleId="+titleId+"&search_mode="+search_mode;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
			type: 'post',
			dataType: 'html',
			data:para,
			async:false,
			success: function(html){
				$("#showList").html(html);
				onLoadInitZebraTable(); //重新调用斑马线样式
			}
		});	

	}
}

function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
		
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
		   searchRightButton();
		}  
	}  
}
function refreshWindow(){
	go($("#jump_p2").val());
}
function submitData(){
	$("#title_name").val($("#titleName").val().trim());
	if(volidate()){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryAddAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag){
					if(data.flag == "-1"){
						showMessage("This Title already exists.","alert");
					}else{
						showMessage("Successfully added title","succeed");
						go();
					}
				}
				$("#titleName").val("");
				$("#title_name").val("");
			},
			error:function(){
				showMessage("System error","error");
			}
		});
	}
	else
	{
		return false;
	}
};

function volidate(){
	if($("#title_name").val() == ""){
		showMessage("Please input the Title Name","alert");
		return false;
	}
	return true;
};
function clickAddTitleTab()
{
	$("#titleName").focus();
}

//无条件时刷新页面
function customRefresh(){
	
	line_id='';
    catalog_id='';
    
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
		type: 'post',
		dataType: 'html',
		async:false,
		success: function(html){
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});
	
	$("#tabs").tabs("select",1);
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="loadData()">   
<!-- <div style="border-bottom:1px solid #CFCFCF; height:25px;padding-top: 15px;margin-left: 10px;margin-bottom: 3px;">
		<img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">
		<span style="font-size: 13px;font-weight: bold"> Setup » Title</span>
</div> 

<div class="breadnav">
<ol class="breadcrumb">
            <li><a href="#">Setup</a></li>
            <li class="active">Title</li>
        </ol>
</div>-->

<div class="content">	     
<div id="tabs" >
	<ul>
		<li><a href="#av1">Advanced Search</a></li>
		<li><a href="#av2">Common Tools</a></li>
		<li><a href="#av3" onclick="clickAddTitleTab()">Add Title</a></li>
	</ul>
	<div id="av1">
		<div id="av"></div>
		<input type="hidden" id="atomicBomb" value=""/>
		<input type="hidden" id="title" />
	</div>
	<div id="av2">
        <div id="easy_search_father">
			<div id="easy_search">
				<a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search_2.png" width="70" height="60" border="0"/></a></div>
			</div>
			<input name="search_key" id="search_key" type="text" class="search_input" style="height: 30px;font-size:17px;font-family:  Arial;color:#333333"
				  onkeydown="if(event.keyCode==13)search()" autocomplete="off" value aria-haspopup="true" aria-autocomplete="list" />
	</div>	
	<div id="av3">
		<table style="margin-top: 3px;margin-bottom: 3px;">
			<tr>
				<td>Title Name:</td>
				<td>
					<input name="titleName" id="titleName"  style="width: 250px;height:30px;" onkeydown="if(event.keyCode==13)submitData()"/>
					<!--  onkeydown="if(event.keyCode==13)submitData()" -->
				</td>
				<td>
					<a name="add"  class="buttons" value="Add Title" onclick="submitData()"><i class="icon-plus"></i>&nbsp;Add Title</a>
				</td>
			</tr>
		</table>
		
	</div>
</div>
<form action="" id="subForm" method="post">
	<input name="title_name" id="title_name" style="width: 250px;" type="hidden"/>
</form>
<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>
<div id="showList"></div>
</div>	  
</body>
</html>
<script>
(function(){	
	$("#tabs").tabs("select",0);
	
	$.blockUI.defaults = {
			 css: { 
			  padding:        '8px',
			  margin:         0,
			  width:          '170px', 
			  top:            '45%', 
			  left:           '40%', 
			  textAlign:      'center', 
			  color:          '#000', 
			  border:         '3px solid #999999',
			  backgroundColor:'#ffffff',
			  '-webkit-border-radius': '10px',
			  '-moz-border-radius':    '10px',
			  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			 },
			 //设置遮罩层的样式
			 overlayCSS:  { 
			  backgroundColor:'#000', 
			  opacity:        '0.6' 
			 },		
			 baseZ: 99999, 
			 centerX: true,
			 centerY: true, 
			 fadeOut:  1000,
			 showOverlay: true
			};
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
		type: 'post',
		dataType: 'html',
		async:false,
		success: function(html){
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	
	 
})();
</script>
