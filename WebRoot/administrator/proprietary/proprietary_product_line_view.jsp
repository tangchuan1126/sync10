<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
String title_id = StringUtil.getString(request, "title_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Title Linked Product Line</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}

.zebraTable td {
  /* border-bottom: 1px solid #dddddd; */
  padding-left: 10px;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<form method="post" name="del_form">
	<input type="hidden" name="id" >
</form>
<form method="post" name="mod_form">
	<input type="hidden" name="name">
	<input type="hidden" name="id"> 
</form>
<form method="post" name="add_form">
	<input type="hidden" name="name">
	<input type="hidden" name="catalog_id">
	<input type="hidden" name="product_line_id">
	<input type="hidden" name="id"> 
</form>
<form action="post" name="clean_form">
	<input type="hidden" name="product_line_id"/>
	<input type="hidden" name="catalog_id"/>
</form>
<form action="" id="changeTitleForm" method="post">
</form>

</select>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
	      <tr> 
	        <th width="50%" class="left-title" colspan="2">Product Line Name</th>
	        <th width="25%" class="right-title" style="vertical-align: center;text-align: center;">Product Category Quantity</th>
	      </tr>			
			<tr>
				<td colspan="5">
					<script type="text/javascript">
						d = new dTree('d');
						d.add('0','-1','Product Line</td><td align="center" valign="middle" width="20%">&nbsp;</td><td align="center" valign="middle" width="10%">&nbsp;</td></tr></table>');
						<%
							DBRow [] productLines = proprietaryMgrZyj.findProductLinesByTitleId(false, 0L, "", title_id, 0, 0, null, request);
							if(productLines.length != 0){
								for(int i=0;i<productLines.length;i++){
						%>
							d.add('<%=productLines[i].getString("id")%>','0','<%=productLines[i].getString("name")%></td><td align="center" valign="middle" width="8%">&nbsp;</td><td align="center" valign="middle" width="30%">&nbsp;</td></tr></table>','');
						<%
						
						DBRow [] catalogs = proprietaryMgrZyj.findProductCatagorysByTitleId(false, 0L, productLines[i].get("id",""),"","", title_id, 0, 0, null, request);
						
						//DBRow [] catalogs = productLineMgrTJH.getProductCatalogByProductLineId(productLines[i].get("id",0l));
						
						for(int j=0;j<catalogs.length;j++){
						
							if(catalogs[j].get("parentid",0l)==0){
						%>
							d.add('<%=catalogs[j].getString("id")+"-"+productLines[i].getString("id")%>','<%=productLines[i].getString("id")%>','<%=catalogs[j].getString("title")%></td><td align="center" valign="middle" width="8%"><%=productMgrZyj.getProductsByCategoryid(catalogs[j].get("id",0l),null)%></td><td align="center" valign="middle" width="10%">&nbsp;</td></tr></table>','');
						<%				
							}else{
						%>
							d.add('<%=catalogs[j].getString("id")+"-"+productLines[i].getString("id")%>','<%=catalogs[j].getString("parentid")+"-"+productLines[i].getString("id")%>','<%=catalogs[j].getString("title")%></td><td align="center" valign="middle" width="8%"><%=productMgrZyj.getProductsByCategoryid(catalogs[j].get("id",0l),null)%></td><td align="center" valign="middle" width="10%">&nbsp;</td></tr></table>','');
						<%
										}
									}
								}
							}
						%>
						document.write(d);
					</script>
				</td>
			</tr>
	</table>
</form>
<br>
<br>
<br>
</body>
</html>
<script>
function addTitle(id,name){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_line_add_title.html?product_line_id="+id+"&product_line_name="+name; 
	 $.artDialog.open(uri , {title: '给产品线添加TITLE',width:'600px',height:'470px', lock: true,opacity: 0.3,fixed: true});
}
</script>


