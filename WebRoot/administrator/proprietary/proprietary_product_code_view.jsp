<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.CodeTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
long pc_id = StringUtil.getLong(request,"pc_id");
DBRow[] productCodes = productCodeMgrZJ.getProductCodesByPcid(pc_id);

CodeTypeKey codeTypeKey = new CodeTypeKey();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>title商品条码</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript">
	function windowClose()
	{
		$.artDialog && $.artDialog.close();
	}
</script>	
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr>
	  	<td colspan="2" valign="top">
	  		<table width="98%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
	  			<tr>
	  				<th class="right-title" style="text-align: center;">条码</th>
	  				<th class="right-title" style="text-align: center;">用途</th>
	  			</tr>
	  			<%
	  				for(int i=0;i<productCodes.length;i++)
	  				{
	  			%>
	  				<tr style="height: 30px;">
	  					<td><%=productCodes[i].getString("p_code")%></td>
	  					<td><%=codeTypeKey.getCodeTypeKeyById(productCodes[i].get("code_type",0))%></td>
	  				</tr>
	  			<%
	  				}
	  			%>
	  		</table>
	  	</td>
	  </tr>
	  <tr>
	    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
	    <td width="49%" align="right" class="win-bottom-line">
		  	<input type="button" name="Submit2" value="关闭" class="normal-white" onclick="windowClose()"/>
		</td>
	  </tr>
	</table> 
</body>
</html>
