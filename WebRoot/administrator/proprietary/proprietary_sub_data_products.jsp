<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.util.StringUtil"%>
<%@page import="com.cwc.db.DBRow"%>
<%@page import="com.cwc.exception.JsonException"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.beans.jqgrid.FilterBean"%>
<%@page import="net.sf.json.JsonConfig"%>
<%@page import="com.cwc.app.beans.jqgrid.RuleBean"%>
<%@page import="com.cwc.json.JsonUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ include file="../../include.jsp"%> 
<%	    
	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	
	long pc_id = StringUtil.getLong(request,"id");
	DBRow inSetProducts[] = productMgr.getProductsInSetBySetPid(pc_id);
	
	for(int i = 0;i<inSetProducts.length;i++)
	{
	  	StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
	  		  
		DBRow allFather[] = tree.getAllFather(inSetProducts[i].get("catalog_id",0l));
		for (int jj=0; jj<allFather.length-1; jj++)
		{
			catalogText.append(allFather[jj].getString("title")+"<br/>");
		}
		  
		catalogText.append("</span>");
		 
		DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(inSetProducts[i].getString("catalog_id")));
		if (catalog!=null)
		{
			catalogText.append(catalog.getString("title"));
		}

		inSetProducts[i].add("catalog_text",catalogText.toString());
		inSetProducts[i].remove("p_img");
		inSetProducts[i].add("gross_profit",(inSetProducts[i].get("gross_profit",0d)*100));
	}
	
	DBRow data = new DBRow();
	data.add("page",1);
	data.add("total",1);
	data.add("records",inSetProducts.length);
	data.add("rows",inSetProducts);
	
	DBRow userData = new DBRow();
	userData.add("set_id",pc_id);
	data.add("union_id",pc_id);
	out.println(new JsonObject(data).toString());
%>