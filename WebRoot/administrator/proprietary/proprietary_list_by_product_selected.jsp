<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%> 

<%
long user_adid = StringUtil.getLong(request, "user_adid");
long pc_id = StringUtil.getLong(request, "pc_id");
String p_name = StringUtil.getString(request, "p_name");
long pc_line_id = StringUtil.getLong(request, "pc_line_id");
long pc_catagory_id = StringUtil.getLong(request, "pc_catagory_id");
int p = StringUtil.getInt(request,"p");
int pc_admin_p = 0;
PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(10);
DBRow[] rows = proprietaryMgrZyj.findProprietaryByPcSelf(user_adid, pc_line_id,pc_catagory_id, pc_id, pc, request);
//System.out.println("pc_line:"+pc_id+","+pc_line_id+","+p);
//out.println("pc_line:"+pc_id+","+pc_line_id+","+p);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<title>Product Linked Title</title>



<script type="text/javascript">
(function(){
	$.blockUI.defaults = {
		css: { 
	  		padding:        '8px',
	  		margin:         0,
	  		width:          '170px', 
	  		top:            '45%', 
	  		left:           '40%', 
	  		textAlign:      'center', 
	  		color:          '#000', 
	  		border:         '3px solid #999999',
	  		backgroundColor:'#ffffff',
	  		'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
	  		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 	},
	 	//设置遮罩层的样式
	 	overlayCSS:  { 
	  		backgroundColor:'#000', 
	  		opacity:        '0.6' 
	 	},
	 
	 	baseZ: 99999, 
	 	centerX: true,
	 	centerY: true, 
	 	fadeOut:  1000,
	 	showOverlay: true
	};
})();



function addProprietary()
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_product_add.html?pc_id=<%=pc_id%>";
	$.artDialog.open(url , {title: '添加title',width:'400px',height:'150px', lock: true,opacity: 0.3,fixed: true});
}

function search()
{
	$("#search_form").submit();
}
function refreshWindow()
{
	window.location.reload();
}
function deleteById(tp_id,title_name)
{
	if(confirm("Are you sure to delete："+title_name+" ?")){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryProductDeleteAction.action',
			data:{tp_id:tp_id},
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag=="true")
				{
					showMessage("Delete Successfully","success");
					refreshWindow();
				}
			},
			error:function(){
				showMessage("System Error","error");
			}
		})	
	}
}
///*
function go(index){
	var form = $("#pageForm");
	$("input[name='p']",form).val(index);
	form.submit();
}
//*/
function submitProprietary()
{
	
}

function addTitle()
{
	
	var url ='<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_add_title.html?pc_id=<%=pc_id %>";
	$.artDialog.open(url , {title: 'Manage[<%=p_name%>] Linked Title',width:'600px',height:'400px', lock: true,opacity: 0.3,fixed: true,close:function(){refreshWindow();}});
}
</script>
<style type="text/css">
table.zebraTable tr{height: 30;}

</style>
</head>
  
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
	<br/>
	<form action="" name="search_form" id="search_form" method="post"> 
	<input type="button" class="long-long-200-button" value="Manage Product Linked Title" onclick="addTitle()" />
	
	</form>
<%--		<table width="98%">--%>
<%--			<tr>--%>
<%--				<td>--%>
<%--					<input name="add" type="button" class="long-button" value="添加" onclick="addProprietary()"/>--%>
<%--				</td>--%>
<%--			</tr>--%>
<%--		</table>--%>
	
	<br/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
    	  <%-- 	<th width="10%" class="right-title" style="vertical-align: center;text-align: center;">titleProductId</th>--%><%--
	        <th width="20%" class="right-title" style="vertical-align: center;text-align: center;">titleId</th>--%>
	       
	      
		
		<th width="55%" class="left-title" style="vertical-align: center;text-align: center;">Title Name</th>
		  <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">Operation</th>
		</tr>
		<%
		if(rows.length > 0)
		{
			for(int i = 0; i < rows.length; i ++)
			{
		%>
		<tr>
	
			<!--<td width="10%" align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
   				<%=rows[i].get("tp_id",0L) %>
   			</td>-->
   				<!--<td width="30%" align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
   				<%=rows[i].get("title_id",0L) %>
   			</td>
   			 -->
   			
   			  <td width="65%" align="left" valign="middle" style='word-break:break-all;font-weight:bold'>
   				<%=rows[i].getString("title_name") %>
   				<input type="hidden" name="productSelectedTitleNames" value='<%=rows[i].getString("title_name") %>'/>
   			</td>
   			<td width="15%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
   				<input name="delete" type="button" class="short-short-button" value="Del" onclick="managerTitle('add',<%=pc_id %>,<%=rows[i].getString("title_id") %>)"/>
   			</td>
   			 
  		</tr>
		<%
			}
		}
		else
		{
		%>
		<tr>
			<td colspan="4" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Data</td>
		</tr>
		<%		
		}
		%>
	</table>
    <br/>
<form action="" id="pageForm" method="post">
	<input type="hidden" name="p" id="pageCount"/>
	<input type="hidden" name="cmd" value="cmd_pc_self"/>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle">
	<%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop"," Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
          Goto
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
</body>
<script type="text/javascript">
var productSelectedTitleNames = new Array();
$(function()
{
	var proSelTitNas = $("input[name=productSelectedTitleNames]");
	if(proSelTitNas.length > 0)
	{
		productSelectedTitleNames = new Array(proSelTitNas.length);
		for(var i = 0; i < proSelTitNas.length; i ++)
		{
			productSelectedTitleNames[i] = $(proSelTitNas[i]).val();
		}
	}
});

function managerTitle(flag,pc_id,hasTitle){
	

	
	if(flag=='add'){
		
	
		
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryProductAddAction.action',
	  		dataType:'json',
	  		data:"flag="+flag+"&pc_id="+pc_id+"&title_id="+hasTitle,
	  		beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
	  		success:function(msg){
	  			
	  					var msgStr = "The following title can't be deleted：<br>";
	  					for(var i = 0; i < msg.result_infos.length; i++ ){  
	  						
	  						msgStr+=msg.result_infos[i].title_name+"<br>";
	  					
	  					}
	  					if(msg.result_infos.length!=0){
	  					showMessage(msgStr,"alert");
	  					setTimeout('location.reload()',1500);
	  					}else{
	  						
	  						location.reload();
	  					}
	  			
	  					//setTimeout('location.reload()',1500);
	  			
	  		},	
	  		error:function(){
				showMessage("System error","error");
			}
	  		
    	});
	}
}
</script>
</html>
