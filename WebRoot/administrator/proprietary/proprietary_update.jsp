<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<%
	long title_id	= StringUtil.getLong(request, "title_id");
	DBRow titleRow	= proprietaryMgrZyj.findProprietaryByTitleId(title_id);
	long p=StringUtil.getLong(request,"p");
	String title_name = "";
	if(null != titleRow)
	{
		title_name = titleRow.getString("title_name");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>title update</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<link rel="stylesheet" type="text/css" href="../buttons.css"  />

<script type="text/javascript">
$(function(){
	
});
function submitData(){
	if(volidate()){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryUpdateAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag)
				{
					if(data.flag == "-1")
					{
						showMessage("This title already exists","alert");
					}
					else if(data.flag == "-2")
					{
						showMessage("There is no changes","alert");
					}
					else
					{
						showMessage("Successfully","success");
						windowClose();
					}
					$.artDialog.opener.go(<%=p%>);
				}
			},
			error:function(){
				showMessage("System error","error");
			}
		})	
	}
};
	function volidate(){
		if($("#title_name").val() == ""){
			alert("titleName is blank.");
			return false;
		}
		if($("#title_priority") && $("#title_priority").val()=='')
		{
			alert("proprietary is blank.");
			return false;
		}
		return true;
	};
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};

</script>

</head>
<body>
		
		<form action="" id="subForm">
		<input type="hidden" name="title_id" id="title_id" value='<%=title_id %>'/>
			<table style="margin-top: 10px;margin-bottom: 10px;">
				<tr>
					<td>Title Name</td>
					<td>
						<input name="title_name" id="title_name" value="<%=title_name %>" style="width: 280px;"/>
					</td>
				</tr>
			</table>
		</form>
	
	<br/>
	<table align="right">
		<tr align="right">
			<td colspan="2"><a class="buttons primary big" value="Update" onclick="submitData();">Update</a></td> 
		</tr>
	</table>
</body>
</html>