<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
	long user_adid = StringUtil.getLong(request, "user_adid");
	String userName = StringUtil.getString(request, "userName");
	DBRow[] hasTitle = userTitleMgrIFaceSbb.getAdminHasTitleList(user_adid);
	DBRow[] unhasTitle = userTitleMgrIFaceSbb.getAdminUnhasTitleList(user_adid);
%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<title>选择title</title>

<script type="text/javascript">
(function(){
	$.blockUI.defaults = {
		css: { 
	  		padding:        '8px',
	  		margin:         0,
	  		width:          '170px', 
	  		top:            '45%', 
	  		left:           '40%', 
	  		textAlign:      'center', 
	  		color:          '#000', 
	  		border:         '3px solid #999999',
	  		backgroundColor:'#ffffff',
	  		'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
	  		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 	},
	 	//设置遮罩层的样式
	 	overlayCSS:  { 
	  		backgroundColor:'#000', 
	  		opacity:        '0.6' 
	 	},
	 
	 	baseZ: 99999, 
	 	centerX: true,
	 	centerY: true, 
	 	fadeOut:  1000,
	 	showOverlay: true
	};
})();

jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
        $("#tabs").tabs("select",0);
});
</script>
</head>
<body>

<div id="update">

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="50%" height="30px" bgcolor="#dddddd" style="text-align:center;padding-left:5px;border-right:1px solid #eeeeee;font-weight:bold">[&nbsp;<%=userName %>&nbsp;]已关联的TITLE</td>
			<td bgcolor="#dddddd" style="padding-left:5px;font-weight:bold;text-align:center;">[&nbsp;<%=userName %>&nbsp;]未关联的TITLE</td>
		</tr>
	</table>
	
	<div align="left" style=" margin-top:2px; height:388px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			    <td width="40%">
				    <div style="background-color:#FFF;border:2px #dddddd solid; height:378px; overflow:auto">
		        		 <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        		 	<!-- 循环已经选择TITLE  -->
		        		 	<input type="hidden" id="has_title" value=""/>
				     		<%for(int i=0;i<hasTitle.length;i++){ %>
							<tr>
								<td id="<%=hasTitle[i].getString("title_id") %>" style="border-bottom:1px solid #dddddd;cursor:pointer;" width="20%" height="30px" align="center" onclick="choiceHasTitle(this.id)">
							 		<%=hasTitle[i].getString("title_name") %>
							 	</td>
							</tr>
							<%} %>
						 </table>
		      		</div>
			    </td>
			    
			    <td align="center" width="20%">
			    	<a style="cursor:pointer">
			    		<img alt="" src="./img/add_jt.gif" onclick="managerTitle('del','<%=user_adid %>')" />
			    	</a>
			    	<br><br><br>
			    	<a style="cursor:pointer">
			    		<img alt="" src="./img/det_jt.gif" onclick="managerTitle('add','<%=user_adid %>')"/>
			    	</a>
			    </td>
			    
			    <td width="40%">
			   	    <div style="background-color:#FFF; border:2px #dddddd solid; height:378px;overflow:auto">
		        		 <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        		 	<!-- 循环未选择TITLE  -->
		        		 	<input type="hidden" id="unhas_title" value=""/>
				       		<%for(int i=0;i<unhasTitle.length;i++){ %>
							<tr>
								<td id="<%=unhasTitle[i].getString("title_id") %>" style="border-bottom:1px solid #dddddd;cursor:pointer;" width="20%" height="30px" align="center" onclick="choiceUnhasTitle(this.id)">
									<%=unhasTitle[i].getString("title_name") %>
								</td>
							</tr>
							<%} %>
						</table>
		    		</div>
				</td>
			</tr>
		</table>
	</div>         
	</div>
</body>
</html>

<script type="text/javascript">

function choiceHasTitle(id){
	
	if((','+$("#has_title").val()+',').indexOf(','+id+',')==-1){
		$("#"+id).css("background-color","#E6F3C5");
		if($("#has_title").val()==''){
			$("#has_title").val(id);
		}else{
			$("#has_title").val($("#has_title").val()+','+id);
		}
	}else{
		$("#"+id).css("background-color","white");
		$("#has_title").val($("#has_title").val().replace(","+id+",",","));
		$("#has_title").val($("#has_title").val().replace(","+id,""));
		$("#has_title").val($("#has_title").val().replace(id+",",""));
		$("#has_title").val($("#has_title").val().replace(id,""));
	}
}

function choiceUnhasTitle(id){
	
	if((','+$("#unhas_title").val()+',').indexOf(','+id+',')==-1){
		$("#"+id).css("background-color","#E6F3C5");
		if($("#unhas_title").val()==''){
			$("#unhas_title").val(id);
		}else{
			$("#unhas_title").val($("#unhas_title").val()+','+id);
		}
	}else{
		$("#"+id).css("background-color","white");
		$("#unhas_title").val($("#unhas_title").val().replace(","+id+",",","));
		$("#unhas_title").val($("#unhas_title").val().replace(","+id,""));
		$("#unhas_title").val($("#unhas_title").val().replace(id+",",""));
		$("#unhas_title").val($("#unhas_title").val().replace(id,""));
	}	
}

function managerTitle(flag,adid){
	
	var hasTitle = $("#has_title").val();
	var unhasTitle = $("#unhas_title").val();
	
	if(flag=='add'){
		
		if(hasTitle == ''){
			showMessage("请选择要删除的TITLE","alert");
			return false;
		}
		
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryAddAdminSelAction.action',
	  		dataType:'json',
	  		data:"flag="+flag+"&adid="+adid+"&title_id="+hasTitle,
	  		beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
	  		success:function(msg){
	  			location.reload();
	  		},
	  		error:function(){
				showMessage("系统错误","error");
			}
    	});
	}else{
	
		if(unhasTitle == ''){
			showMessage("请选择要添加的TITLE","alert");
			return false;
		}
		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		$.ajax({
	  		type:'post',
	  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryAddAdminSelAction.action',
	  		dataType:'json',
	  		data:"flag="+flag+"&adid="+adid+"&title_id="+unhasTitle,
	  		beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
	  		success:function(msg){
	  			location.reload();
	  		},
	  		error:function(){
				showMessage("系统错误","error");
			}
    	});
	}
	$("#unhas_title").val('');
	$("#has_title").val('');
	$.unblockUI();
}
</script>