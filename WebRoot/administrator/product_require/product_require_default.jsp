<%@ page contentType="text/html;charset=UTF-8"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.api.CatalogMgr" %>
<%@ page import="com.cwc.app.api.qll.ProductMgrQLL"%>
<html>
<%
	TDate tdate = new TDate();
	String input_en_date = StringUtil.getString(request,"input_en_date",tdate.formatDate("yyyy-MM-dd"));
	
	tdate.addDay(-30);
	
	String input_st_date = StringUtil.getString(request,"input_st_date",tdate.formatDate("yyyy-MM-dd"));
 %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>仓库备货预警</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>


<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">


<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />


<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>
<script type="text/javascript">
	function ajaxLoadCatalogMenuPage(id,divId)
	{
			var para = "id="+id+"&divId="+divId;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#"+divId+"categorymenu_td").html(html);
				}
			});
	}
	
	function cleanSearchKey(divId)
	{
		$("#"+divId+"search_product_name").val("");
	}
	
	$().ready(function() 
	{
		addAutoComplete($("#search_product_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",	  
			"p_name");
	});
</script>
</head>

<body >
			
<br/>
<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#supplier_search">备货预警</a></li>
		</ul>
		<div id="supplier_search">
			<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:99%;">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td align="left" width="19%" style="padding-top:3px;">仓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;库： 
					        <select name="cid" id="cid"  width="12">
							<option value="0">所有仓库</option>
					          <%
								DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
								String qx;
								
								for ( int i=0; i<treeRows.length; i++ )
								{
									if ( treeRows[i].get("parentid",0) != 0 )
									 {
									 	qx = "├ ";
									 }
									 else
									 {
									 	qx = "";
									 }
									 
									 if (treeRows[i].get("level",0)>1)
									 {
									 	continue;
									 }
								%>
					          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==StringUtil.getLong(request,"ps_id")?"selected":""%>> 
					          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
					          <%=qx%>
					          <%=treeRows[i].getString("title")%>  </option>
					          <%}%>
					        </select> 	
				  </td>
				  <td width="20%" align="left">
				  	采样开始时间：<input name="input_st_date" type="text" id="input_st_date" size="10" value="<%=input_st_date%>" readonly="readonly" />&nbsp;&nbsp;&nbsp; 
				  </td>
				  <td width="51%" align="left"> 
				  		<ul id="productLinemenu" class="mcdropdown_menu">
			             <li rel="0">所有产品线</li>
			             <%
							  DBRow p1[] = productLineMgrTJH.getAllProductLine();
							  for (int i=0; i<p1.length; i++)
							  {
									out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
									 
									out.println("</li>");
							  }
						  %>
			           </ul>
			           <input type="text" name="productLine" id="productLine" value="" />
			           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
				  </td>
				  <td width="10%" align="center"><input name="searchQLL" type="button" class="button_long_search" value="统计"  onClick="search()"/></td>
	            </tr>
	            <tr> 
	            	<td align="left" style="padding-top: 10px;">销售系数：
	            	  <input name="coefficient" id="coefficient" value="1" style="width:78px;"/>
            	  </td>
	            	<td align="left">
	            	采样结束时间：<input name="input_en_date" type="text" id="input_en_date" size="10" value="<%=input_en_date%>" readonly="readonly" />
	            	</td>
	           		<td  align="left" id="categorymenu_td"> 
				  	 <ul id="categorymenu" class="mcdropdown_menu">
					  <li rel="0">所有分类</li>
					  <%
						  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
						  for (int i=0; i<c1.length; i++)
						  {
								out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
					
								  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
								  if (c2.length>0)
								  {
								  		out.println("<ul>");	
								  }
								  for (int ii=0; ii<c2.length; ii++)
								  {
										out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
										
											DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
											  if (c3.length>0)
											  {
													out.println("<ul>");	
											  }
												for (int iii=0; iii<c3.length; iii++)
												{
														out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
														out.println("</li>");
												}
											  if (c3.length>0)
											  {
													out.println("</ul>");	
											  }
											  
										out.println("</li>");				
								  }
								  if (c2.length>0)
								  {
								  		out.println("</ul>");	
								  }
								  
								out.println("</li>");
						  }
						  %>
					</ul>
			  <input type="text" name="category" id="category" value="" />
			  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"/>
				  </td>
				  <td align="center"><input name="searchQLL" type="button" class="button_long_search" value="导出"  onClick="submitExport()"/></td>
	            </tr>
	            <tr>
	            	<td></td>
	            	<td>
	            		<input type="radio" name="is_alert" id="is_alert" value="1" checked="checked"/>预警商品
				   		<input type="radio" name="is_alert" id="is_alert" value="2"/>全部商品
	            	</td>
	            	<td  style="padding-top: 10px;">商品名称：<input type="text" name="search_product_name" id="search_product_name" onClick="cleanProductLine('')" style="width:200px;"/></td>
	            	<td></td>
	            </tr>
	          </table>
	          </div>
		</div>
		
	</div>
</div>

<script>
$("#input_st_date").date_input();
$("#input_en_date").date_input();


$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_pcid").val(id);
					
					if(id!=0)
					{
						cleanSearchKey('');
					}
				}

});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
					ajaxLoadCatalogMenuPage(id,"");
					if(id!=0)
					{
						cleanSearchKey('');
					}
						//$("#category").mcDropdown("#categorymenu").setValue(0);
				}

});

$("#category").mcDropdown("#categorymenu").setValue(0);


$("#productLine").mcDropdown("#productLinemenu").setValue(0);

	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});

function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function checkNull()
{
	var cid= $("#cid").val();
	var coefficient= $("#coefficient").val();
	var is_alert=$("input#is_alert:checked").val();
	
	if(cid==0)
	{
		alert("请选择你要统计的仓库!");
		return false;
	}
	else if(is_alert==null)
	{
		alert("请选择你要统计预警商品还是全部商品!");
		return false;
	}else
	if(coefficient==null)
	{
		alert("请填写销售系数!");
		return false;
	}else
	{
		return true;
	}	
	
}

function submitExport()
{
		if(checkNull())
		{
			var ps_id = $("#cid").val();
			var dayCountf=$("#dayCount").val();	
			var catal = $("#category").val();	
			var coefficientf = $("#coefficient").val();
			var search_product_namef= $("#search_product_name").val();
			var productLinef= $("#productLine").val();
			var part_or_allf=$("input#part_or_all:checked").val();
			//var para = "cmd=submit&ps_id="+ps_id+"&dayCountf="+dayCountf+"&categoryf="+categoryf+"&coefficientf="+coefficientf+"&search_product_namef="+search_product_namef+"&productLinef="+productLinef+"&part_or_allf="+part_or_allf;
			var para = "product_name="+$("#search_product_name").val()+"&catalog_id="+$("#filter_pcid").val()+"&pro_line_id="+$("#filter_productLine").val()+"&input_st_date="+$("#input_st_date").val()+"&input_en_date="+$("#input_en_date").val()+"&isAlert="+$('input[name="is_alert"]:checked').val()+"&ps_id="+$("#cid").val()+"&coefficient="+$("#coefficient").val();
			$.ajax({
						url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/exportStockWarnAction.action',
						type: 'post',
						dataType: 'json',
						timeout: 60000,
						cache:false,
						data:para,
						beforeSend:function(request){
						},
						
						error: function(){
							alert("提交失败，请重试！");
						},
						
						success: function(date){
							if(date["canexport"]=="true")
							{
								document.stockWarnForm.action=date["fileurl"];
								document.stockWarnForm.submit();
							}
							else
							{
								alert("该统计无数据，无法导出");
							}
						}
					});
	}
}
function ajaxLoadStockWarnPage(para)
{
	 	
	$.ajax({
		url: 'stockWarn.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:para,			
					
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
					
		error: function(){
			$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
		},
		success: function(html){
			$.unblockUI();
			$("#stockWarnList").html(html);
		}							
	});
}

$("#search_product_name").keydown(function(event){
	if (event.keyCode==13)
	{
		search();
	}
});

function search()
{
	if(checkNull())
	{
		var para = "product_name="+$("#search_product_name").val()+"&catalog_id="+$("#filter_pcid").val()+"&pro_line_id="+$("#filter_productLine").val()+"&input_st_date="+$("#input_st_date").val()+"&input_en_date="+$("#input_en_date").val()+"&isAlert="+$('input[name="is_alert"]:checked').val()+"&ps_id="+$("#cid").val()+"&coefficient="+$("#coefficient").val();
		ajaxLoadStockWarnPage(para);
	}
	
	
}


function ajaxLoadStockWarnDefultPage(para)
{
	 	
	$.ajax({
	url: 'stockWarn.html',
	type: 'post',
	dataType: 'html',
	timeout: 60000,
	cache:false,
	data:para,			
				
	beforeSend:function(request){
		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	},
				
	error: function(){
		$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
	},
	success: function(html){
		$.unblockUI();
		$("#stockWarnList").html(html);
		onLoadInitZebraTable();//表格斑马线
		$.scrollTo(0,500);//页面向上滚动
	}			
				
	});
}			

function goPage(p,cmd)
{
	var para = "p="+p+"&product_name="+$("#search_product_name").val()+"&catalog_id="+$("#filter_pcid").val()+"&pro_line_id="+$("#filter_productLine").val()+"&input_st_date="+$("#input_st_date").val()+"&input_en_date="+$("#input_en_date").val()+"&isAlert="+$('input[name="is_alert"]:checked').val()+"&ps_id="+$("#cid").val()+"&coefficient="+$("#coefficient").val();
	ajaxLoadStockWarnDefultPage(para);
}
</script>
<div id="stockWarnList">
</div>
<form name="stockWarnForm"></form>
<br/>
</body>
</html>