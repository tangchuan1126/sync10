<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.api.qll.ProductMgrQLL"%>
<%@ include file="../../include.jsp"%>
<%
	long catalog_id = StringUtil.getLong(request,"catalog_id");
	long pro_line_id = StringUtil.getLong(request,"pro_line_id");
	String product_name = StringUtil.getString(request,"product_name");
	
	long ps_id = StringUtil.getLong(request,"ps_id");
	int isAlert = StringUtil.getInt(request,"isAlert");
	float coefficient = StringUtil.getFloat(request,"coefficient");
	
	
	TDate tDate = new TDate();
	String input_en_date = StringUtil.getString(request,"input_en_date",tDate.formatDate("yyyy-MM-dd"));
	tDate.addDay(-30);
	String input_st_date = StringUtil.getString(request,"input_st_date",tDate.formatDate("yyyy-MM-dd"));
	
	if (input_st_date.equals("") && input_en_date.equals(""))
	{
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	}
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	DBRow[] rows =null;
	
	DBRow[] DateRow= ordersProcessMgrQLL.getDate(input_st_date,input_en_date);
	rows = productMgrQLL.preparationPlanAnalysis(input_st_date,input_en_date,catalog_id,pro_line_id,product_name,ps_id,isAlert,coefficient,pc);
	
 %>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>无标题文档</title>
	<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="../js/select.js"></script>
	<link href="../comm.css" rel="stylesheet" type="text/css"/>
  </head>  		
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="onLoadInitZebraTable()"   >
<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <tr> 
    <th width="9%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">产品线</th>
    <th width="9%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">产品类别</th>
    <th width="9%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">产品名称</th>
    <th width="9%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">仓库名称</th>
    <th width="9%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">平均销量</th>
    <th width="9%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">当前库存</th>
    <th width="7%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">可用天数</th>
    <th width="10%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">备货天数</th>
    <th width="7%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">备货期销量</th>
    <%
		if(DateRow!=null && DateRow.length!=0 )
		{
			for(int j=0; j<DateRow.length; j++ )
			{
	%>
	<th  align="center"  class="right-title" style="vertical-align: center;text-align: center;"><%=DateRow[j].getString("date")%></th>
	<% 
			}
		}					 
	%>
  </tr>	

  <%if(rows!=null) 
  	{
  		for(int i =0;i<rows.length;i++)
  		{
  			String warnColor="";
  			String dayCount="";
  			if(rows[i].getString("day_count").equals(""))
  			{
  				dayCount="0.0";
  			}
  			else
  			{
  				dayCount=rows[i].getString("day_count");
  			}
  			if(!rows[i].getString("store_count_alert").equals(""))
  			{
	  			if ( Double.parseDouble(dayCount)<=Double.parseDouble(rows[i].getString("store_count_alert")))
				{
					warnColor = "#0000FF";//"FFA800";//"#FFFF00";
				}
				else
				{
					warnColor = "#000000";
				}
  			}
  			double countDays=0;
  			if(!rows[i].getString("avg_count").equals("")&&!rows[i].getString("store_count_alert").equals(""))
  			{
  				countDays=Math.round(Double.parseDouble(rows[i].getString("store_count_alert"))*Double.parseDouble(rows[i].getString("avg_count"))*coefficient);
  			}
  			 %> 
  		 <tr >	
  		 <td align="center"  height="60"><%=rows[i].getString("product_line_title")%>&nbsp;</td>
  		 <td align="center"  height="60"><%=rows[i].getString("product_catalog_title")  %></td>
  		 <td align="center"  height="60"><%=rows[i].getString("p_name") %></td>
  		 <td align="center"  height="60"><%=rows[i].getString("title") %></td>
  		 <td align="center"  height="60"><%=rows[i].getString("avg_count") %></td>
  		 <td align="center"  height="60"><%=rows[i].getString("store_count") %></td>
  		 <td align="center" style="color:<%=warnColor%>;font-weight:bold;" height="60"><%=rows[i].getString("day_count").equals("")?0:rows[i].getString("day_count") %></td>
  		 <td align="center"  height="60"><%=rows[i].getString("store_count_alert") %></td>
  		 <td align="center"  height="60"><%=countDays%> </td>
  		 <%
			for(int j=0; j<DateRow.length; j++ )
			{
				if(!rows[i].getString("date_"+j).equals(""))
				{
		%>
		<td align="center"  height="60" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;'><%= rows[i].getString("date_"+j)%>  </td>
		<% 
				}
			}
		%>
  		 </tr>
  		 <%	}
  	}
  	  	 %>
  </table> 
  <br/>
  <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:goPage(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:goPage(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:goPage(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:goPage(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:goPage(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
  </table>
</body>
</html>
 
