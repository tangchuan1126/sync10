<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商品明细</title>
<%
	String p_name = StringUtil.getString(request,"p_name");
	double quantity = StringUtil.getDouble(request,"quantity");
	DBRow product =  productMgr.getDetailProductByPname(p_name);// product.getDetailProductByPname(p_name)
	DBRow[] productUnions  = null ;
	if(product != null){
		 productUnions = productMgr.getProductUnionsBySetPid(product.get("pc_id",0l));
	}
%>
<style type="text/css">
	*{margin:0px;padding:0px;}
	ul{list-style-type:none;}
	ul li{clear:both;}
	a{cursor:pointer;text-decoration:underline ;}
	input.quantity_input{font-weight:bold;color:#FF3300;width:35px;}
					
	
</style>
<script type="text/javascript">
 function showQuantityInput(_this){
    var checkbox = $(_this);
	var node = $(_this).parent().parent();
	var quantity_span = $(".quantity_span",node);
	var num = quantity_span.attr("number");
	var index = quantity_span.attr("index");
	var is_checked = $("input[name='is_checked_"+index+"']");
 
	if(!checkbox.attr("checked")){
	    quantity_span.html(""+num);
	    is_checked.val("0");
	    all();
	}else{
		quantity_span.html("<input type='text' name='quantity_"+index+"' value='"+num+"' class='quantity_input'/>")
		$("#myCheckBox").attr("checked",false);
		is_checked.val("1");
	}
 
 }
  
 // 如果所有的子项没有被选中,那么就是在套装商品应该要选中。
 function all(){
	var singleCheckbox = $(".singleCheckbox");
	var flag = true ;
	for(var index = 0 , count = singleCheckbox.length ; index < count ; index++ ){
		var node = $(singleCheckbox.get(index));
		if(node.attr("checked")){
		    flag = false;
		}
	}
 
	if(flag){
		var myCheckBox = $("#myCheckBox");
		myCheckBox.attr("checked",true);
	}
 }
 function showUnions(_this){
    var node = $(_this);
	var product_unions = $("#product_unions");
	product_unions.slideToggle("fast");
 }
 function allChecked(_this){
	var myCheckBox = $("#myCheckBox");
	if(!myCheckBox.attr("checked")){
	    myCheckBox.attr("checked",true);
	}else{
		//如果是套装的话,那么就是要散件不选中
		var singleCheckbox = $(".singleCheckbox");
		if(singleCheckbox && singleCheckbox.length > 0){
			for(var index = 0 , count = singleCheckbox.length ; index < count ; index++ ){
					var node = $(singleCheckbox.get(index));
					var nodeParent = node.parent().parent();
					var quantity_span = $(".quantity_span",nodeParent);
					var num = quantity_span.attr("number");
					 quantity_span.html(""+num);
					 node.attr("checked",false);
			}
		}
		 
	}
 }
</script>
</head>
<body>
 	<%
 	if(product != null){
 	%>	 
 		 
 		<input type="hidden" name="product_type" value='<%=product.get("union_flag",0) %>'/>
 		<input type="hidden" name="pid" value='<%=product.get("pc_id",0l) %>'/>
		<h4><a herf="javascript:void(0)" onclick="showUnions(this);"><%=product.getString("p_name") %></a><input type="checkbox" id="myCheckBox" checked onclick="allChecked(this);"/></h4>
		<%if(productUnions != null && productUnions.length > 0){ 
			int index = 1 ;
		%>
		<ul id="product_unions" style="display:none;">
			<%for(DBRow temp : productUnions){ %>
					
				<li>
					<span style="display:block;float:left;width:180px;border:0px solid silver;">├<%=temp.getString("p_name") %> </span> 
					<span style="display:block;float:left;padding-top:6px;"><input type="checkbox" class="singleCheckbox" onclick="showQuantityInput(this);"/></span>
					<span style="display:block;float:left;border:0px solid silver;padding-top:2px;margin-left:2px;" index="<%=index %>" class="quantity_span" number= "<%= temp.get("quantity",0.0d) * quantity%>"><%=temp.get("quantity",0.0d)*quantity %></span>
				</li>
				<input type="hidden" name="pid_<%=index %>" value='<%=temp.get("pid",0l) %>'/>
					<input type="hidden" name="p_name_<%=index %>" value='<%=temp.getString("p_name") %>'/>
					<input type="hidden" name="quantity_<%=index %>" value='<%=temp.get("quantity",0.0d) * quantity %>'/>
					<input type="hidden" name="unit_name_<%=index %>" value='<%=temp.getString("unit_name") %>'/>
					<input type="hidden" name="is_checked_<%=index %>" value="0"/>
			<%
			index++;
			}} %>
		</ul>
 	<%
 	}else{ %>
 		<p style="text-align:center;">
 			无此此商品[<%=p_name %>]
 		</p>
 	<%} %>
 	<script type="text/javascript">
 		isAddProduct = 1 ;
 	</script>
</body>
</html>