<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.ReturnProductKey"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductCheckItemTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultTypeKey"%>
 <%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>退货单关联单据</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 

 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>


<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/selectmenu/jquery.ui.selectmenu.js"></script>
<link type="text/css" href="../js/selectmenu/jquery.ui.selectmenu.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
<style type="text/css">
  td.right{text-align:right;font-weight:bold;color:#0066FF;}
  div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 input.next{background-color: #660033;color:white;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
 
</style>
<%
long rp_id = StringUtil.getLong(request,"rp_id");
DBRow row = new DBRow();
row = returnProductMgrZr.getReturnProductByRpId(rp_id);
%>
<script type="text/javascript">
var rp_id = '<%= row.get("rp_id",0l)%>';
var relationFlag  = false;
var isServiceHasReturn = false;
function cancel(){
	$.artDialog && $.artDialog.close();
}
function checkServieOrder(){
   	
}
function loadServiceOrderDetail(){
    var sid = $("#sid").val();
    var type = $("#type").val();
    $.ajax({
	  url:'<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/service_detail.html?sid="+sid+"&type="+type ,
		dataType:'html',
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
		   	$("#content").html(data);
		    
		},
		error:function(){$.unblockUI();showMessage("系统错误.","error");}
	}) 
}
function relationProduct(){
   	if(!relationFlag){
		alert("关联的单据不存在");
   	    return ;
   	}
	if(isServiceHasReturn){
		alert("关联的服务单已有退货单，请重新选择关联单据");
   	    return ;
   	}
	 $.artDialog.confirm("你确认关联<font style='font-weight:bold;'>["+typestr+"&nbsp;"+sid+"]</font>?", function(){
	     var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/ReturnProductRelationOrderAction.action';
		 var obj = {sid:sid,type:type,rp_id:'<%= rp_id%>'};
	     commonAjax(uri,obj)
	 }, function(){
		
	 });
}
 
function commonAjax(uri,dataJson){
    $.ajax({
	    url:uri,
	    data:dataJson,
	    dataType:'json',
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
	    	if(data && data.flag == "success"){
	    			cancel();
	    			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
		   	}else{
		   		 showMessage("系统错误.","error");
			}
    	},
    	error:function(){$.unblockUI(); showMessage("系统错误.","error");}
	})
}
jQuery(function($){
   if(rp_id * 1 > 0){
		// 表示的是已经有了关联
		var sid = '<%= row.get("sid",0l)%>';
		var oid = '<%= row.get("oid",0l)%>';
		var wid = '<%= row.get("wid",0l)%>';
		var type = "" ;
		var order_id  ;
		if(sid * 1 > 0){
		    type = "sid";
		    order_id = sid ;
		}else if(oid * 1 > 0){
		    type = "oid";
		    order_id = oid;
		}else{
		    type = "wid";
		    order_id = wid ;
		}
		$("#type option[value='"+type+"']").attr("selected",true);
		$("#sid").val(order_id);
		if(order_id * 1 > 0){
			loadServiceOrderDetail();
		}
   }
})
</script>
<script type="text/javascript">
 
</script>
</head>
<body>
	 
			 
   		 	<p style="line-height:30px;height:30px;margin-top:5px;">
   		 			单据类型:<select id="type" name="type">
   		 				<option value="oid">订单</option>
   		 				<option value="sid">服务单</option>
<%--   		 				<option value="wid">运单</option>--%>
   		 			</select>&nbsp;<input id="sid" type="text" name="sid" style="width:160px;"/> <input type="button" value="获取明细" class="long-button" onclick="loadServiceOrderDetail();"/>
   		 	</p>
		 	<div id="content">
		 	</div>
	 	<div class="buttonDiv" style="width:100%;margin-bottom:5px;position:fixed;left:0px;bottom:-5px;">
 			<%
 				if(ReturnProductKey.FINISHALL != row.get("status",0))
 				{
 			%>
				<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="提交" onclick="relationProduct();">
				<button id="jqi_state0_button取消" class="jqidefaultbutton specl" onclick="cancel();" name="jqi_state0_button取消" value="n">取消</button>
			<%	} %>
		</div>
	 	<script type="text/javascript">

	 	//遮罩
	 	$.blockUI.defaults = {
	 		css: { 
	 			padding:        '8px',
	 			margin:         0,
	 			width:          '170px', 
	 			top:            '45%', 
	 			left:           '30%', 
	 			textAlign:      'center', 
	 			color:          '#000', 
	 			border:         '3px solid #999999',
	 			backgroundColor:'#eeeeee',
	 			'-webkit-border-radius': '10px',
	 			'-moz-border-radius':    '10px',
	 			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	 			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 		},
	 		//设置遮罩层的样式
	 		overlayCSS:  { 
	 			backgroundColor:'#000', 
	 			opacity:        '0.6' 
	 		},
	 		baseZ: 99999, 
	 		centerX: true,
	 		centerY: true, 
	 		fadeOut:  1000,
	 		showOverlay: true
	 	};
	 	</script>
</body>
</html>