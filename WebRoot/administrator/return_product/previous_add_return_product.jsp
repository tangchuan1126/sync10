<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />

 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link href="../comm.css" rel="stylesheet" type="text/css">
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<style type="text/css">
 
</style>

<script type="text/javascript">
	 function checkRMA(){
		var rp_id = $("#rp_id").val();
		if($.trim(rp_id).length < 1){
			showMessage("请先输入RMA","alert");
			return ;
		}
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/CheckReturnProductAction.action',
			dataType:'json',
			data:{rp_id:$.trim(rp_id)},
			beforeSend:function(request){
		     	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
			    $.unblockUI();
			    if(data.flag === "success"){
					var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/return_product_detail_list.html?rp_id="+ data.rp_id;
				    window.location.href= uri ;
				}else{
					showMessage("RMA不存在.","alert");
				}
			}
		})
	 }
</script>

</head>
<body>
  	<div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
		 <ul>
			 <li><a href="#HASRMA">有RMA</a></li>
			 <li><a href="#NORMA">无RMA</a></li>	 
 		 </ul>
		 <div id="HASRMA">
		 		RMA:<input type="text" id="rp_id" name="rp_id"/>&nbsp;<input type="button" class="short-button" onclick="checkRMA();" style="cursor:pointer;" value="确定"/>
		 </div>
		  <div id="NORMA">
		  	
		 </div>
	</div>
	
	<script type="text/javascript">
 	$("#tabs").tabs({
 	   select: function(event, ui) {
			if(ui.index == 0){
				
			}
			if(ui.index == 1){
				loadAddReturnHtml();
			}
   	   }
 	 });
 	jQuery(function($){
		var index = ($("#tabs").tabs('select').index())
 	})
 	 function loadAddReturnHtml()
	 {
		 $.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/add_return_product.html',
				type: 'post',
				dataType:'html',
				timeout: 60000,
				cache:false,
				error: function(){
					showMessage("加载错误","alert");
				},
				success:function(html)
				{
					$("#NORMA").html(html);
				}
			})
	 }
 	function loadNoRma(){
 	   var _uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/add_return_product.html"; 
 	   window.location.href= _uri;
 	  
 	}
 	//遮罩
 	$.blockUI.defaults = {
 		css: { 
 			padding:        '8px',
 			margin:         0,
 			width:          '170px', 
 			top:            '45%', 
 			left:           '40%', 
 			textAlign:      'center', 
 			color:          '#000', 
 			border:         '3px solid #999999',
 			backgroundColor:'#eeeeee',
 			'-webkit-border-radius': '10px',
 			'-moz-border-radius':    '10px',
 			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
 			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
 		},
 		//设置遮罩层的样式
 		overlayCSS:  { 
 			backgroundColor:'#000', 
 			opacity:        '0.6' 
 		},
 		baseZ: 99999, 
 		centerX: true,
 		centerY: true, 
 		fadeOut:  1000,
 		showOverlay: true
 	};
 	
	</script>
</body>
</html>