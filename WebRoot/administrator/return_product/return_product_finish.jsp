<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductCheckItemTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultTypeKey"%>
 <%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>测试退货商品</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 

 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
<style type="text/css">
  td.right{text-align:right;font-weight:bold;color:#0066FF;}
  div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 input.next{background-color: #660033;color:white;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
 
</style>
<%
long rp_id = StringUtil.getLong(request,"rp_id");
ReturnProductCheckItemTypeKey checkItemTypeKey = new ReturnProductCheckItemTypeKey();
ReturnProductItemHandleResultRequestTypeKey itemHandleRequestType = new ReturnProductItemHandleResultRequestTypeKey();
ReturnProductItemHandleResultTypeKey handleResultTypeKey = new ReturnProductItemHandleResultTypeKey();
DBRow[] itemsRow = returnProductItemsFixMgrZr.getReturnItemsByRpId(rp_id);
%>
<script type="text/javascript">
var os = <%= new JsonObject(itemsRow).toString()%>;
function cancel(){
	$.artDialog && $.artDialog.close();
}
function returnProductFinish(){
   if(os && os.length > 0){
		for(var o in os ){
		 	alert(os[o]["p_name"])
		}
   }
}
function getServiceOrderItem(){
   var sid = $("#sid");
   if($.trim(sid.val()).length < 1){
		showMessage("请先输入服务单","alert");
		sid.focus();
		return ;
   }
   if(!/^-?\d+$/.test(sid.val())){
		showMessage("请正确输入服务单","alert");
		sid.focus();
		return ;
   }
   $.ajax({
		url:'',
		dataType:'json',
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
		    if(data && data.flag === "success"){
				
			}
		    
		},
		error:function(){$.unblockUI();showMessage("系统错误.","error");}
   })
   
   
}
function handServieItem(objs){
	var str= '<fieldset class="set" style="padding-bottom:4px;border:2px solid #993300;"><legend><span class="title">';
		str += '<span style="font-weight:bold;">退货商品</span></span></legend><p>';
		for(var o in objs){
		    str+= o[""] + " x  <span style='color:blue;'>"+""+"</span><br />";
		    BOX/KIT/ALUMINUM x	<span style="color:blue">1.0 ITEM</span> <br />
		}
		str += "</p>"
	
}
</script>
<script type="text/javascript">
 
</script>
</head>
<body>
		<form id="myform">
			 
   		 	<p style="line-height:30px;height:30px;margin-top:5px;">
   		 		服务单:<input id="sid" type="text" name="sid" style="width:160px;"/> <input type="button" value="获取明细" class="long-button" onclick="getServiceOrderItem();"/>
   		 	</p>
			<fieldset class="set" style="padding-bottom:4px;border:2px solid #993300;">
	 					<legend>
								<span class="title">
									<span style="font-weight:bold;">退货商品</span>
								</span> 
						</legend>
	 				<%
	 					
	 					if(itemsRow != null && itemsRow.length > 0 ){
	 						
	 						for(DBRow item : itemsRow){
	 							%>
	 							<p>
	 								 <%=item.getString("p_name") %> x	<span style="color:blue"><%=item.get("quantity",0.0f) %> <%=item.getString("unit_name") %></span> <br />
	 								 
	 								 <%if(item.get("return_product_check",ReturnProductCheckItemTypeKey.OUTCHECK) != ReturnProductCheckItemTypeKey.OUTCHECK){ %>
	 									├ 商品:<%=checkItemTypeKey.getStatusById(item.get("return_product_check",0)) %><br />
	 								 <%} %>
	 								 
	 								 <%if(item.get("handle_result_request",0) != 0){ %>
	 	 								├ 处理要求:<span style="color:blue"><%= itemHandleRequestType.getStatusById(item.get("handle_result_request",0))%></span><br />
	 								 	<%if(item.get("handle_result",0) != 0){ %>
	 								 	├ 处理结果:<span style="color:blue"><%= handleResultTypeKey.getStatusById(item.get("handle_result",0))%></span><br/>
	 								 <%}} %>
	 							</p>
	 							
	 							<% 
	 						}
	 					}
	 				%>
	 				</fieldset>
	 	</form>
	 	<div class="buttonDiv" style="width:100%;margin-bottom:5px;position:fixed;left:0px;bottom:-5px;">
				<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="退货完成/关联服务" onclick="submitForm();">
				<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="退货完成" onclick="returnProductFinish();">
				<button id="jqi_state0_button取消" class="jqidefaultbutton specl" onclick="cancel();" name="jqi_state0_button取消" value="n">取消</button>
		</div>
	 	<script type="text/javascript">

	 	//遮罩
	 	$.blockUI.defaults = {
	 		css: { 
	 			padding:        '8px',
	 			margin:         0,
	 			width:          '170px', 
	 			top:            '45%', 
	 			left:           '40%', 
	 			textAlign:      'center', 
	 			color:          '#000', 
	 			border:         '3px solid #999999',
	 			backgroundColor:'#eeeeee',
	 			'-webkit-border-radius': '10px',
	 			'-moz-border-radius':    '10px',
	 			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	 			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 		},
	 		//设置遮罩层的样式
	 		overlayCSS:  { 
	 			backgroundColor:'#000', 
	 			opacity:        '0.6' 
	 		},
	 		baseZ: 99999, 
	 		centerX: true,
	 		centerY: true, 
	 		fadeOut:  1000,
	 		showOverlay: true
	 	};
	 	
	 	//stateBox 信息提示框
		function showMessage(_content,_state){
			var o =  {
				state:_state || "succeed" ,
				content:_content,
				corner: true
			 };
		 
			 var  _self = $("body"),
			_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
			_self.append(_stateBox);	
			 
			if(o.corner){
				_stateBox.addClass("ui-corner-all");
			}
			if(o.state === "succeed"){
				_stateBox.addClass("ui-stateBox-succeed");
				setTimeout(removeBox,1500);
			}else if(o.state === "alert"){
				_stateBox.addClass("ui-stateBox-alert");
				setTimeout(removeBox,2000);
			}else if(o.state === "error"){
				_stateBox.addClass("ui-stateBox-error");
				setTimeout(removeBox,2800);
			}
			_stateBox.fadeIn("fast");
			function removeBox(){
				_stateBox.fadeOut("fast").remove();
		 }
		}
	 	</script>
</body>
</html>