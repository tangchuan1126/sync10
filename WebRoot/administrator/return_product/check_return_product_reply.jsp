<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>测试退货商品</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

 <!-- 文件预览  -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
<style type="text/css">
  td.right{text-align:right;font-weight:bold;color:#0066FF;}
  div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 input.next{background-color: #660033;color:white;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
</style>
<%
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";

AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
long check_return_product_id = StringUtil.getLong(request,"check_return_product_id");
DBRow checkReturnProduct = checkReturnProductItemMgrZr.getCheckReturnProductItemById(check_return_product_id);
%>
<script type="text/javascript">
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
//文件上传
function uploadFile(_target){
 
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"all",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   		 }});
}
//(文件)回调函数 
function uploadFileCallBack(fileNames,target){
    $("input[name='file_names']").val(fileNames);
    if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
        $("input[name='file_names']").val(fileNames);
		var file_tr = $("#file_show_tr");
		 file_tr.css("display","block");
		 var arrayFile =  fileNames.split(",");
		 var strA = "" ;
		 $("p.new").remove();
		 for(var index = 0 , count = arrayFile.length ; index < count ; index++ ){
		    strA += createA(arrayFile[index]);	
		 }
		 $("#show_file_td").append(strA);
	} 
}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+"_"+ fileName;
    var  a = "<p class='new'><a href="+uri+">"+showName+"</a>&nbsp;&nbsp;(New)</p>";
    return a ;
}
function cancel(){
    $.artDialog && $.artDialog.close();
}
function submitForm(){
   var form = $("#myform");
  
   $.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/ReplyCheckReturnProductItemAction.action',
		dataType:'json',
		data:form.serialize(),
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
		    if(data && data.flag === "success"){
				cancel();
	   			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			}
		},
		error:function(){
		    $.unblockUI();
			 showMessage("系统错误.","error");
		}
   })
}
//在线预览图片
function showPictrueOnline(fileWithType,fileWithId , currentName){
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_return_product_detail")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}
</script>
</head>
<body>
		<form id="myform">
			<input type="hidden" name="rp_id" value="<%=checkReturnProduct.get("rp_id",0l)  %>" />
			<input type="hidden" name="rpi_id" value="<%=checkReturnProduct.get("rpi_id",0l)  %>" />
			<input type="hidden" id="file_names" name="file_names"/>
			<input type="hidden" name="check_return_product_id" value="<%=check_return_product_id %>"/>
 			<input type="hidden" name="file_names" />
			<input type="hidden" name="sn" id="sn" value="R_check_return_product"/>
			<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.RETURN_CHECK_PRODUCT %>" />
			<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_return_product_detail")%>" />
 
   			 <table>
			 	<tr>
			 		<td class="right">操作人:</td>
			 		<td><%= adminLoggerBean.getEmploye_name()%></td>
			 	</tr>
			 	<tr>
			 		<td class="right">测试要求:</td>
			 		<td>
				 		<p style="width:300px;line-height:150%;">
							<%=checkReturnProduct.getString("check_product_context") %>
				 		</p>
			 		</td>
			 	</tr>
			 	
			 	<tr>
			 		<td style="width:60px" class="right">测试结果:</td>
			 		<td>
<textarea name="result_context" id="result_context" style="width:300px;height:170px;"><%=checkReturnProduct.getString("result_context") %></textarea>
			 		</td>
			 	</tr>
			 	<tr>
			 		<td class="right">测试报告:</td>
			 		<td>
			 			<input type="button"  value="上传文件" class="long-button" onclick="uploadFile('jquery_file')"/>
			 		</td>
			 	</tr>
			 	<tr>
			 		<td></td>
			 		<td>
			 			<div>
			 			 	<%
			 			 	if(checkReturnProduct.get("has_result_file",0) == 1){
			 			 		DBRow[] files = fileMgrZr.getFilesByFileWithIdAndFileWithType(check_return_product_id,FileWithTypeKey.RETURN_CHECK_PRODUCT);
			 					for(DBRow fileRow : files){
			 			 	 %>
			 			 	 	<%if(StringUtil.isPictureFile(fileRow.getString("file_name"))){ %>
				 			 	 <p>
				 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.RETURN_CHECK_PRODUCT %>','<%=check_return_product_id%>','<%=fileRow.getString("file_name") %>');"><%=fileRow.getString("file_name") %></a>
				 			 	 </p>
			 			 	 <%} else if(StringUtil.isOfficeFile(fileRow.getString("file_name"))){%>
			 			 	 		<p>
			 			 	 			<a href="javascript:void(0)"  file_id='<%=fileRow.get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("file_path_return_product_detail")%>')" file_is_convert='<%=fileRow.get("file_is_convert",0) %>'><%=fileRow.getString("file_name") %></a>
			 			 	 		</p>
			 			 	 <%}else{ %>
 		 			 	  		<p><a href='<%= downLoadFileAction%>?file_name=<%=fileRow.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_return_product_detail")%>'><%=fileRow.getString("file_name") %></a></p>
			 			 	<%	}
			 				}
			 			  }%>
			 			</div>
			 			<div id="show_file_td">
			 			</div>
			 		</td>
			 	</tr>
			 </table>
	 	</form>
	 	<div class="buttonDiv" style="width:100%;margin-bottom:5px;position:fixed;left:0px;bottom:-5px;">
				<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="测试完成" onclick="submitForm();">
				<button id="jqi_state0_button取消" class="jqidefaultbutton specl" onclick="cancel();" name="jqi_state0_button取消" value="n">取消</button>
		</div>
	 	<script type="text/javascript">

	 	//遮罩
	 	$.blockUI.defaults = {
	 		css: { 
	 			padding:        '8px',
	 			margin:         0,
	 			width:          '170px', 
	 			top:            '45%', 
	 			left:           '20%', 
	 			textAlign:      'center', 
	 			color:          '#000', 
	 			border:         '3px solid #999999',
	 			backgroundColor:'#eeeeee',
	 			'-webkit-border-radius': '10px',
	 			'-moz-border-radius':    '10px',
	 			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	 			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 		},
	 		//设置遮罩层的样式
	 		overlayCSS:  { 
	 			backgroundColor:'#000', 
	 			opacity:        '0.6' 
	 		},
	 		baseZ: 99999, 
	 		centerX: true,
	 		centerY: true, 
	 		fadeOut:  1000,
	 		showOverlay: true
	 	};
	 	
	 	//stateBox 信息提示框
		function showMessage(_content,_state){
			var o =  {
				state:_state || "succeed" ,
				content:_content,
				corner: true
			 };
		 
			 var  _self = $("body"),
			_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
			_self.append(_stateBox);	
			 
			if(o.corner){
				_stateBox.addClass("ui-corner-all");
			}
			if(o.state === "succeed"){
				_stateBox.addClass("ui-stateBox-succeed");
				setTimeout(removeBox,1500);
			}else if(o.state === "alert"){
				_stateBox.addClass("ui-stateBox-alert");
				setTimeout(removeBox,2000);
			}else if(o.state === "error"){
				_stateBox.addClass("ui-stateBox-error");
				setTimeout(removeBox,2800);
			}
			_stateBox.fadeIn("fast");
			function removeBox(){
				_stateBox.fadeOut("fast").remove();
		 }
		}
	 	</script>
</body>
</html>