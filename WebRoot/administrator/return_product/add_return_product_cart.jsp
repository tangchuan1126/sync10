<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
DBRow result =  returnProductItemsFixMgrZr.flushSession(session);
DBRow[] cartProducts = null ;
if(result != null ) {
	cartProducts =(DBRow[]) result.getValue("data");
}
WarrantyTypeKey warrantyTypeKey = new WarrantyTypeKey();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Session中数据</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script>

</script>

<style>
a.del-cart-product:link {
	color:#FF0000;
	text-decoration: none;
}
a.del-cart-product:visited {
	color: #FF0000;
	text-decoration: none;
}
a.del-cart-product:hover {
	color: #FF0000;
	text-decoration: underline;

}
a.del-cart-product:active {
	color: #FF0000;
	text-decoration: none;

}
</style> 

<script type="text/javascript">


</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	 <!-- 
	<table border="0" cellpadding="0" cellspacing="0">
					<%
						for(int i = 0;i<cartProducts.length;i++)
						{
							DBRow product = productMgr.getDetailProductByPcid(cartProducts[i].get("cart_pid",0l));
							
							DBRow[] productUnions = productMgr.getProductUnionsBySetPid(product.get("pc_id",0l));
					%>
					<tr>
						<td style="font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold">
							<% 
							if(productUnions.length>0)
							{
							%>
								<span onclick="showProductUnion(<%=product.get("pc_id",0l)%>)" style="text-decoration:underline;cursor:pointer;"><%=product.getString("p_name")%></span>
							<%
							}
							else
							{
							%>
								<%=product.getString("p_name")%>
							<%
							}
							%>
							
						</td>
						<td align="left">
							<%
								if(product.get("orignal_pc_id",0)==0)
								{
							%>
									<input type="checkbox" value="<%=product.get("pc_id",0l)%>" onclick="allNoCheck(this.value)"/>
							<%
								}
							%>
						</td>
					</tr>
					<% 
							if(productUnions.length>0)
							{
					%>
					<tr id="<%=product.get("pc_id",0l)%>" style="display:none">
						<td>
							<table border="0" cellpadding="0" cellspacing="0">
								<%
									for(int j = 0;j<productUnions.length;j++)
									{
										DBRow unionProduct = productMgr.getDetailProductByPcid(productUnions[j].get("pid",0l));
								%>
								<tr>
									<td><%=unionProduct.getString("p_name")%></td>
									<td><input onclick="productNotCheck(this)" name="<%=product.get("pc_id",0l)%>" id="<%=unionProduct.get("pc_id",0l)%>" type="checkbox" value="<%=product.get("pc_id",0l)+"_"+unionProduct.get("pc_id",0l)%>"/></td>
								</tr>
								<%
									}
								%>
							</table>
						</td>
					</tr>
					<%
							}
					%>
					
					<%
						}
					%>
					
		  					 
		  					
</table>
 -->
<form id="cartForm">
<table width="100%" border="0" cellspacing="0" cellpadding="5">

	
<%
if (cartProducts == null || cartProducts.length==0)
{
%>
      <tr>
        <td colspan="4" align="center" valign="middle" style="color:#999999"><<暂无商品>>
		<script>cartIsEmpty=true;</script>		</td>
      </tr>
<%
} 
else
{
%>
      <tr >
        <td height="30" bgcolor="#eeeeee" style="padding-left:10px;"><strong>商品</strong></td>
        <td align="center" bgcolor="#eeeeee">数量</td>
        <td align="center" bgcolor="#eeeeee"><strong>退货类型</strong></td>
        <td bgcolor="#eeeeee"><strong>退货原因</strong></td>
        <td align="center" valign="middle" bgcolor="#eeeeee">&nbsp;</td>
      </tr>
	  
<%
String bgColor;
for (int i=0; i<cartProducts.length; i++)
{
	if (i%2==0)
	{
		bgColor = "#FFFFFF";
	}
	else
	{
		bgColor = "#eeeeee";
	}
	
%>

      <tr bgcolor="<%=bgColor%>" >
        <td width="43%" height="25"  style="padding-left:10px;padding-top:5px;padding-bottom:5px;">
			<%=cartProducts[i].getString("p_name")%>
			<input type="hidden" name="pc_id" value="<%=cartProducts[i].get("cart_pid",0l)%>"/>
		</td>
        <td width="12%" align="center">
			<input name="<%=cartProducts[i].get("cart_pid",0l)%>_quantity" type="text" id="quantitys" style="width:40px;" value="<%=cartProducts[i].getString("cart_quantity")%>" onChange="return onChangeQuantity(this)" /><%=cartProducts[i].getString("unit_name")%>
		</td>
		<td width="10%" align="center">
			<select name="<%=cartProducts[i].get("cart_pid",0l)%>_waranty_type" onchange="onChangeWarrantyType(this)">
				<%
					ArrayList warrantyTypeList = warrantyTypeKey.getWarrantyType();
					for(int j=0;j<warrantyTypeList.size();j++)
					{
				%>
					<option <%=cartProducts[i].getString("warranty_type").equals(warrantyTypeList.get(j))?"selected='selected'":""%> value="<%=warrantyTypeList.get(j)%>"><%=warrantyTypeKey.getWarrantyTypeById(warrantyTypeList.get(j).toString())%></option>
				<%
					}
				%>
			</select>
		</td>
		<td>
			<input maxlength="300" name="return_reason_<%=cartProducts[i].get("cart_pid",0l)%>" value="<%=cartProducts[i].getString("return_reason")%>" onchange="onChangeReturnReason(this)" <%=cartProducts[i].getString("return_reason").trim().equals("")?"style='background-color:#FFB5B5'":""%> />
		</td>
        <td width="11%" align="center" valign="middle">
			<a href="javascript:void(0)" onClick="delProductFromCart(<%=cartProducts[i].getString("cart_pid")%>,'<%=cartProducts[i].getString("p_name")%>')" ><img src="../imgs/del.gif" width="12" height="12" border="0"></a>
		</td>
      </tr>
<%
} 
%>
      <tr>
        <td height="50" style="border-top:2px #eeeeee solid"><input name="Submit433" type="button" class="short-button" onclick="submitCart();" value="修改">          &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
        
        <input name="Submit432" type="button" class="long-button-yellow" value="清空退货" onClick="cleanCart()">
        <script>cartIsEmpty=false;</script>
        </td>
        <td height="50" style="border-top:2px #eeeeee solid">&nbsp;</td>
        <td colspan="3" align="right" valign="middle" style="border-top:2px #eeeeee solid">总数：<%=result.get("sumQuantity",0.0f)%></td>
      </tr>
<%
}
 
%>
</table>
</form>
</body>
</html>
