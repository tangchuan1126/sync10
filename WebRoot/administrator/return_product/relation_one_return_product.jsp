<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加退货商品</title>
<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<%
	ReturnProductItemHandleResultRequestTypeKey handleResultRequestTypeKey = new ReturnProductItemHandleResultRequestTypeKey();
	ArrayList<String> returnRequestTypelist = ( ArrayList<String> )handleResultRequestTypeKey.getStatus();
	long items_fix_rpi_id = StringUtil.getLong(request,"items_fix_rpi_id");
	long rp_id = StringUtil.getLong(request,"rp_id");
	DBRow row = returnProductMgrZr.getReturnProductByRpId(rp_id);
	boolean isRelationOrder = false; //是否已经关联上单据(订单,运单,服务单);
	if(row != null){
		isRelationOrder = (row.get("sid",0l) != 0l) || (row.get("wid",0l) != 0l) ||( row.get("oid",0l) !=0l ); 
	}
%>

<style type="text/css">
	p{border:1px solid silver; line-height:30px;height:30px;width:80%;margin-top:10px;}
	table td.right{text-align:right;width:80px; color: #0066FF;font-weight: bold;}
	table td{border:0px solid silver;line-height:25px;height:25px;}
	table{border-collapse:collapse ;}
	div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 	input.buttonSpecil {background-color: #BF5E26;}
 	input.next{background-color: #660033;color:white;}
 	.jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 	.specl:hover{background-color: #728a8c;}
</style>

<script type="text/javascript">
var isAddProduct = 0 ;
function cancel(){
	$.artDialog && $.artDialog.close();
}
function getProduct(){
    
	var p_name = $("#p_name_input").val();
	var quantity = $("#quantity").val();
	if($.trim(quantity).length < 1){
		showMessage("请先输入数量","alert");
		$("#quantity").focus();
		return ;
	}
	if(p_name.length < 1){
		showMessage("请先输入商品","alert");
		$("#p_name_input").focus();
		return ;
	}
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/product_items.html",
		data:{p_name:p_name,quantity:quantity},
		dataType:'html',
		beforeSend:function(request){
	     		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
			var product_item_tr = $("#product_item_tr");
			product_item_tr.attr("style","");
			$("#product_item").html(data);
		},
		error:function(data){
		    $.unblockUI();
		    showMessage("系统错误.","error");
		}
	})
}
function submitForm(){
    //如果提交商品是有套装的话,那么就要根据套装的选择把散件的信息存入到return_product_sub_items表中
    //如果不是套装也需要把商品的信息存入到return_product_sub_items表中
    if(!validate()){return ;}
	var submitForm = $("#submitForm");
	var myCheckBox = $("#myCheckBox");
 	var addUriStr = "" ;
 	if(myCheckBox.attr("checked")){
 	   addUriStr = "&all_select=1";
 	}
 	var testCheckBox = $("#testCheckBox");
 	if(testCheckBox.attr("checked")){
 	   addUriStr += "&is_test=1"
 	}
 
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>' + "action/administrator/returnProduct/RelationOneReturnProductAction.action",
		data:submitForm.serialize()+addUriStr,
		dataType:'json',
		beforeSend:function(request){
 			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
			if(data.flag === "success"){
			    cancel();
	   			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			}
		},
		error:function(data){
		    $.unblockUI();
		    showMessage("系统错误.","error");
		}
	})
}
function validate(){
    var p_name = $("#p_name_input").val();
	if($.trim(p_name).length < 1){
		showMessage("请先输入商品.","alert");
		$("#p_name_input").focus();
		return false;
	}
	
	var quantity = $("#quantity").val();
	if($.trim(quantity).length < 1){
		showMessage("请先输入商品数量","alert");
		$("#quantity").focus();
		return false;
	}
	var handle_result_request = $("#handle_result_request");
	if(handle_result_request.val() * 1 == -1){
	    showMessage("请先选择处理要求","alert");
	    $("#handle_result_request").focus();
		return false ;
	}
	if(isAddProduct * 1 == 0){
		showMessage("请先添加退货商品","alert");
		return false ;
	}
	return true ;
    
}
jQuery(function($){
   
})
function selectCheck(_this){
   var node = $(_this);
   var p_name = node.attr("p_name");
   var quantity = node.attr("quantity");
   if(node.attr("checked")){
       removeCheckSelected();
       node.attr("checked",true);
       setValue(p_name,quantity);
   }
   else
   {
	   removeCheckSelected();
	   $("#p_name").val("");
	   $("#p_name_input").val("");
	   var product_item_tr = $("#product_item_tr");
		product_item_tr.attr("style","display:none;");
		$("#product_item").html("");
   }
}
function removeCheckSelected(){
   $(".item_check").attr("checked",false);
}
function setValue(p_name , quantity){
   $("#p_name_input").val(p_name);
   $("#p_name").val(p_name);
   $("#quantity").val(quantity);
   getProduct();
}
function addProductForInput()
{
	removeCheckSelected();
	getProduct();
	$("#p_name").val($("#p_name_input").val());
}
</script>

</head>
<body>
 	 	<form id="submitForm" method="post">
 	 		<input type="hidden" name="file_names" />
 	 		<input type="hidden" name="rp_id" value="<%=rp_id %>" />
 	 		<input type="hidden" name="items_fix_rpi_id" value="<%=items_fix_rpi_id %>" />
 	 
 	 		<table>
 	 			<%if(isRelationOrder){ %>
 	 				<tr>
 	 					<td class="right">关联单据:</td>
 	 					<td colspan="2">
 	 						<%
 	 						boolean isOrder = false ;
 	 						DBRow[] items = null ;
 	 						if(row.get("sid",0l) != 0l ){
 	 							items =  serviceOrderMgrZyj.getServiceOrderItemsByServiceId(row.get("sid",0l));
 	 						}else if(row.get("oid",0l) != 0l){
 	 							isOrder = true ;
 	 							items =  orderMgr.getPOrderItemsByOid(row.get("oid",0l));
 	 						}else if(row.get("wid",0l) != 0l){
								items = wayBillMgrZJ.getWayBillOrderItems(row.get("wid",0l));; 	 							
 	 						}
 	 						%>
 	 						<%if(items != null && items.length > 0){ %>
 	 							<div style="margin-left:2px;">
 	 								<fieldset class="set" style="padding-left:14px;border:2px solid #993300;">
					 					<legend>
												<span class="title">
												 关联明细 
												</span> 
										</legend>
 	 								<%for(DBRow item : items){ %>
 	 								 		<input type="checkbox" onclick="selectCheck(this)" quantity="<%=item.get("quantity",0.0f)  %>" p_name="<%=isOrder?item.getString("name"):item.getString("p_name") %>" class="item_check"/><%=isOrder?item.getString("name"):item.getString("p_name") %> x	<span style="color:blue"><%=item.get("quantity",0.0f) %> <%=item.getString("unit_name") %></span> <br />
 	 								<%} %>
 	 								</fieldset>
 	 							</div>
 	 						<%} %>
 	 						
 	 					</td>
 	 				</tr>
 	 			<%} %>
 	 			<tr>
 	 				<td class="right" >商品名:</td>
 	 				<td colspan="2">
 	 					<input type="text" id="p_name_input" class="p_name_input" name="p_name_input" style="width:180px;"/>
 	 					&nbsp;&nbsp;数量:&nbsp;<input type="text" value="1" id="quantity" name="quantity" style="font-weight:bold;color:#FF3300;width:35px;"/>
 	 					<input type="button" onclick="addProductForInput();" value="添加商品" class="short-button"/>
 	 				</td>
 	 			</tr>
 	 			 <tr id="product_item_tr" style="display:none;">
 	 			 	<td class="right">添加商品:</td>
 	 			 	<td id="product_item">
 	 			  
 	 			 	</td>
 	 			 	<td>
 	 			 		<input type="hidden" id="p_name" class="p_name" name="p_name"/>
 	 			 	</td>
 	 			 </tr>
 	 			<tr>
 	 				<td class="right">处理要求:&nbsp;</td>
 	 				<td>
 	 					<select id="handle_result_request" name="handle_result_request">
 	 		 	 	 				<option value="-1">处理要求</option>
 	 		 	 	 				<%for(String str : returnRequestTypelist){ %>
 	 		 	 	 					<option value="<%=str %>"><%=handleResultRequestTypeKey.getStatusById(Integer.parseInt(str)) %></option>
 	 		 	 	 				<%} %>
 	 		 	 	 	</select>
 	 				</td>
 	 			</tr>
 	 			<tr>
 	 				<td class="right">退货测试:&nbsp;</td>
 	 				<td><input type="checkbox" id="testCheckBox" onclick="changeTest(this)"/>&nbsp;是</td>
 	 			</tr>
 	 			<tr>
 	 				<td class="right">测试要求:&nbsp;</td>
 	 				<td>
 	 					<textarea name="check_product_context" disabled="disabled" id="check_product_context" style="width:300px;height:180px;"></textarea>
 	 				</td>
 	 			</tr>
 	 		 
 	 		</table>
 	 	
 	 	</form>
 	 	<div class="buttonDiv" style="width:100%;margin-bottom:5px;position:fixed;left:0px;bottom:-5px;">
				<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="提交" onclick="submitForm()">
				<button id="jqi_state0_button取消" class="jqidefaultbutton specl" onclick="cancel();" name="jqi_state0_button取消" value="n">取消</button>
		</div>
		<script type="text/javascript">
			jQuery(function($){

			    var pName = $(".p_name_input");
		 	 	pName.each(function(){
		 		 	addAutoComplete($(this),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");

		 	 	})
			})
			function changeTest(_this){
				var node = $(_this);
				if(node.attr("checked")){
				 
					$("#check_product_context").attr("disabled",false);
				}else{
				    $("#check_product_context").attr("disabled",true);
				}

				
			}
		 
			//遮罩
			$.blockUI.defaults = {
				css: { 
					padding:        '8px',
					margin:         0,
					width:          '170px', 
					top:            '45%', 
					left:           '30%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #999999',
					backgroundColor:'#eeeeee',
					'-webkit-border-radius': '10px',
					'-moz-border-radius':    '10px',
					'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
					'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
				},
				//设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.6' 
				},
				baseZ: 99999, 
				centerX: true,
				centerY: true, 
				fadeOut:  1000,
				showOverlay: true
			};
	 
		</script>
</body>
</html>