<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
 <html>
 	<head>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 		<title>修改退货明细</title>
 		<!-- 基本css 和javascript -->
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
		 <style type="text/css" media="all">
		@import "../js/thickbox/global.css";
		@import "../js/thickbox/thickbox.css";
		</style>
		
		<link href="../comm.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="../js/select.js"></script>
		<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
		
		<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
		<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
		<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 	</head>
 	<body>
 		<table>
 			<tr>
 				<td>商品名称</td>
 				<td><input type="text" name="p_name" id="p_name"/></td>
 			</tr>
 		</table>
 	</body>
 	 <script type="text/javascript">
 	addAutoComplete($("#p_name"),
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
		"p_name");
 	 </script>
</html>
