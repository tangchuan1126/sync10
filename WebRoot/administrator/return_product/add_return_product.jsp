<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.StorageReturnProductTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductSourceKey"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>添加退货单</title>


<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<style type="text/css">
  td.right{text-align:right;font-weight:bold;color:#0066FF;}
  div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 input.next{background-color: #660033;color:white;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
</style>
<%
String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
String addReturnProductAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/returnProduct/AddReturnProductAction.action";
StorageReturnProductTypeKey returnProductTypeKey = new StorageReturnProductTypeKey();
ReturnProductSourceKey returnProductSourceKey = new ReturnProductSourceKey();
%>
<script type="text/javascript">
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
	function setPro_id(fillFlag,val,html){
		removeAfterProIdInput();
		var node = $("#ccid_hidden");
		var value = node.val();
	
		if(value*1 != 10929){
			$.ajaxSettings.async = false;
			$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
					{ccid:value},
					function callback(data){ 
						$("#pro_id").attr("disabled",false);
						$("#pro_id").clearAll();
						$("#pro_id").addOption("请选择......","0");
						if (data!=""){
							$.each(data,function(i){
								$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
							});
						}
						if (value>0){
							$("#pro_id").addOption("手工输入","-1");
						}
						$("#addBillProSpan").html($("#pro_id"));
						// 如果是这个下面有 对应的那么就选中 如果是么有的那么就是 和点击手工输入一样的操作
						if(val){
							if(val === "-1"){
								$("option[value='"+val+"']",$("#pro_id")).attr("selected",true);
							 
								$("#addBillProSpan").append($("<input type='text' id='address_state_input' value='"+html+"' />"));
							 
							}else{
								$("option[value='"+val+"']",$("#pro_id")).attr("selected",true);
								removeAfterProIdInput();
							}
						}
					}
					
				);
			 
		 }
		if(value * 1 != 0 ){
			// 得到Option
			var option = $("option:selected",node);
			$("#address_country_code").val(option.attr("code"));
		}else{
			$("#address_country_code").val("");
		}
	}
	function removeAfterProIdInput(){
		
		$("#address_state_input").remove();
	}
	function fixAddress(){
		 var node = $("#pro_id");
		 var value = node.val();
	
		 if(value*1 == -1){
			 addInputAfterProid(node);
		 }else{
			 removeAfterProIdInput();
		}
	}
	function addInputAfterProid(node){
		 var input  = "<input type='text'   id='address_state_input' />";
		 $(input).insertAfter(node);
	}
	//文件上传
	function uploadFile(_target){
	 
	    var fileNames = $("input[name='file_names']").val();
	    var obj  = {
		     reg:"all",
		     limitSize:2,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames && fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
	    		 close:function(){
						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
						 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
	   		 }});
	}
	//在线预览
	function onlineScanner(){
		    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
			$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}
	//(文件)回调函数 
	function uploadFileCallBack(fileNames,target){
	    $("input[name='file_names']").val(fileNames);
	    if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
	        $("input[name='file_names']").val(fileNames);
			var file_tr = $("#file_show_tr");
			 file_tr.css("display","block");
			 var arrayFile =  fileNames.split(",");
			 var strA = "" ;
			 $("p.new").remove();
			 for(var index = 0 , count = arrayFile.length ; index < count ; index++ ){
			    strA += createA(arrayFile[index]);	
			 }
			 $("#show_file_td").append(strA);
		} 
	}
	function createA(fileName){
		    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
		    var showName = $("#sn").val()+"_"+ fileName;
		    var  a = "<p class='new'><a href="+uri+">"+showName+"</a>&nbsp;&nbsp;(New)</p>";
		    return a ;
	}
	function cancel(){
		$.artDialog && $.artDialog.close();
	}
	function submitForm(nextFlag){
		var ps_id = $("#ps_id").val();
		if(!ps_id){alert("请先选择退货仓库") ;return ;}
		var form = $("#myform");
		 
		if($("#ccid_hidden").val() * 1  > 0 ){
			
			$("#address_country").val($("#ccid_hidden option:selected").html());
			var address_state  = "";
			var pro_id = $("#pro_id").val();
			
			if(pro_id * 1 == -1){
			    address_state = $("#address_state_input").val();
			}
			if(pro_id * 1  > 0){
			    address_state = $("#pro_id option:selected").html();
			}
			$("#address_state").val(address_state);
		}
		 
		$.ajax({
			url:'<%= addReturnProductAction%>',
			dataType:'json',
			data:form.serialize(),
			beforeSend:function(request){
	     		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
			    $.unblockUI();
				if(data && data.flag == "success"){
				   
				   
				    if(nextFlag+"" === "ture"){
					    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/add_return_product_detail_list.html?rp_id="+ data.rp_id;
					    window.location.href= uri ;
				    }else{
						 cancel();
				    	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
				    }
				}else{
					alert("系统错误.");
				}
			},
			error:function(){$.unblockUI();alert("系统错误.");}
		})
	}
	//
	var file_names ;
	function returnProductTypeChange(){
		var return_product_type = $("#return_product_type").val();
		if(return_product_type * 1 == '<%= returnProductTypeKey.NOT_PACK%>' * 1){
			//没有包裹的情况
		    file_names = $("#file_names").val();
		    $("#file_names").val("");
		    $("#pack_tr").css("display","none");
		}else{
			// 有包裹情况
			$("#file_names").val(file_names);
			  $("#pack_tr").attr("style","");
		}
		
	}
	jQuery(function(){returnProductTypeChange();})
</script>
</head>
<body>
		<form id="myform">
			<input type="hidden" name="file_names" id="file_names" />
			<input type="hidden" name="sn" id="sn" value="R_return_product"/>
			<input type="hidden" id="address_country_code" name="address_country_code" value=""/>
			<input type="hidden" id="address_country" name="address_country" />
			<input type="hidden" id="address_state" name="address_state" />
			<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.RETURN_PRODUCT_FILE %>" />
			<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_return_product")%>" />
  	 	<div style="width:98%;">
		 	 
			  		 
			 		<div>
 			 			 <!--  client_id,address_name,address_street,address_city,address_zip -->
						<table class="addressInfo">
							<tr>
								<td class="right" width="110px;">退货来源:</td>
								<td>
									<select name="return_product_source" id="return_product_source">
										<%
											ArrayList<String> list = (ArrayList<String>)returnProductSourceKey.getStatus();
											for(String str : list){
												boolean isCustomerPostBack  = str.equals(ReturnProductSourceKey.CUSTOMER_POST_BACK+"");
												%>
												<option <%=isCustomerPostBack?"selected":"" %> value='<%=str %>'><%=returnProductSourceKey.getStatusById(Integer.parseInt(str)) %></option>	
												<% 
											}
										%>
									</select>
								</td>
							</tr>
							<tr>
								<td class="right" width="110px;">包裹类型:</td>
								<td>
									<%ArrayList array = returnProductTypeKey.getStatus(); %>
									
									<select id="return_product_type" name="return_product_type" onchange="returnProductTypeChange();">
										<%for(int index = 0 , count = array.size() ; index < count ; index ++) {%>
											<option value="<%=array.get(index) %>"><%=returnProductTypeKey.getStatusById(Integer.parseInt(array.get(index)+"")) %></option>
										<%} %>
									</select>
								</td>
							</tr>
							<tr>
								<td class="right" >退货仓库:</td>
								<td>
									<%DBRow[] psIds =  catalogMgr.getProductReturnStorageCatalogTree(); %>
									<select id="ps_id" name="ps_id">
									 
										<%for(DBRow psid : psIds){ %>
											<option value='<%=psid.get("id",0l) %>'><%=psid.getString("title") %></option>
										<%} %>
									</select>
								</td>
							</tr>
							<tr>
								<td class="right" width="110px;">First Name:</td>
								<td><input type="text" value="" id="first_name"  style="width:200px;" name="first_name" /></td>
							</tr>
							<tr>
								<td class="right">Last Name:</td>
								<td><input type="text" value="" id="last_name"  style="width:200px;" name="last_name" /></td>
							</tr>
							 <tr>
								<td class="right">Street:</td>
								<td><input type="text" id="address_street" name="address_street" value="" style="width:200px;"/></td>
							</tr>
							 <tr>
								<td class="right">City :</td>
								<td><input type="text" name="address_city" value="" style="width:200px;"/></td>
							</tr>
							 <tr>
								<td class="right">Country :</td>
								<td>
									 
						<!-- 国家省份信息 -->
							<%
								DBRow countrycode[] = orderMgr.getAllCountryCode();
								String selectBg="#ffffff";
								String preLetter="";
							%>
					      	 <select  id="ccid_hidden"  name="ccid" onChange="removeAfterProIdInput();setPro_id();">
						 	 	<option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++){
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
										if (selectBg.equals("#eeeeee")){
											selectBg = "#ffffff";
										}else{
											selectBg = "#eeeeee";
										}
									}  	
									preLetter = countrycode[i].getString("c_country").substring(0,1);
								  %>
								    <option  style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
 								  <%
								  }
								%>
			     			 </select>
							</td>
								
							</tr>
							 <tr>
								<td class="right">State :</td>
								<td> 
								 
									<span id="addBillProSpan">
										<select disabled="disabled" id="pro_id" name="pro_id" onchange="fixAddress()" style="margin-right:5px;"></select>
									</span>
								</td>
								 
							</tr>
							 <tr>
								<td class="right">Zip :</td>
								<td><input type="text" name="address_zip" value=""  style="width:200px;"/></td>
							</tr>
							 <tr>
								<td class="right">Tel :</td>
								<td><input type="text"  id="tel" name="tel" value=""  style="width:200px;"/></td>
							</tr>
							 
							
						</table>
					 	<div style="width:90%;border-bottom:1px dashed  silver;margin-top:5px;margin-bottom:5px;margin-left:10px;"></div>
						<table>
							<tr>
								<td class="right" width="100px">Tracking#:</td>
								<td><input type="text" name="tracking_num" value=""  style="width:200px;"/></td>
							</tr>
							 
							<tr>
								<td class="right">运输公司:</td>
								<td><input type="text" name="express_company" value="" style="width:200px;"/></td>
							</tr>
						</table>
							
						
						<div style="width:90%;border-bottom:1px dashed  silver;margin-top:5px;margin-bottom:5px;margin-left:10px;"></div>
						
						<table style="width:90%;">
							<tr>
								<td class="right" width="100px">备注:</td>
								<td><textarea style="width:330px;height:110px;" name="note"></textarea></td>
							</tr>
							<tr style="" id="pack_tr">
								<td class="right">外包装:</td>
								<td>
									<div id="show_file_td">
										<input type="button" value="上传" class="long-button" onclick="uploadFile('file');"/>
										<input class="long-button" type="button" value="在线获取" onclick="onlineScanner();"> <br />
									</div>
								</td>
							</tr>
						</table>
			 		</div>
			 	 
			 
				
		 	</div>
		 
	 	</form>
	 		<div class="buttonDiv" style="width:100%;margin-bottom:5px;position:fixed;left:0px;bottom:-5px;">
				<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="提交" onclick="submitForm()">
		 	    <input class="jqidefaultbutton next" type="button" value="提交/下一步" onclick="submitForm('ture')">
				
				<button id="jqi_state0_button取消" class="jqidefaultbutton specl" onclick="cancel();" name="jqi_state0_button取消" value="n">取消</button>
			</div>
	 	
</body>
</html>