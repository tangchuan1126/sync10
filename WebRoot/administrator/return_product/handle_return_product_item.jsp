<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.ReturnProductCheckItemTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemMatchKey"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>退货处理</title>
<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link href="../comm.css" rel="stylesheet" type="text/css">
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- statebox -->
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<style type="text/css">
td.right{text-align:right;font-weight:bold;color:#0066FF;width:80px;text-align:right;}
td{line-height:20px;height:20px;}
div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
input.buttonSpecil {background-color: #BF5E26;}
.jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
.specl:hover{background-color: #728a8c;}
span.title{display:block;text-align:right;font-weight:bold;color:#0066FF;width:80px;text-align:right;float:left;}
div.right_div{margin-left:5px;float:left;}
p{clear:both;}
div.checkDiv{font-size:12px;margin-bottom:3px;line-height:15px;word-wrap:break-word;width:510px;margin-top:3px;margin-bottom:5px;border-bottom:1px dashed  silver;border-top:1px dashed  silver;}

</style>
<%
	long rp_id = StringUtil.getLong(request,"rp_id");

	long rpi_id = StringUtil.getLong(request,"rpi_id");
	DBRow returnItemRow = new DBRow();
	returnItemRow = returnProductItemsFixMgrZr.getReturnProductItemByRpiId(rpi_id);
	DBRow tempRow = new DBRow();
	tempRow = returnProductMgrZr.getReturnProductByRpId(rp_id);
	DBRow[] checkItems = null ;
	if(tempRow.get("return_product_check",ReturnProductCheckItemTypeKey.OUTCHECK) != ReturnProductCheckItemTypeKey.OUTCHECK){
		checkItems = checkReturnProductItemMgrZr.getCheckReturnProductItemByRpIId(rpi_id);
	}
 
	DBRow returnProductItems = returnProductItemsFixMgrZr.getReturnProductItemByRpiId(rpi_id);
	ReturnProductItemHandleResultRequestTypeKey handleResultRequestTypeKey = new ReturnProductItemHandleResultRequestTypeKey();
%>
<script type="text/javascript">
function isNum(keyW)
{
	var reg=  /^(-[0-9]|[0-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
	function updateForm(){
		var flag =  validate();
		if(!flag){
			showMessage("数量不符.","alert");
			return false ;
		}
		
		var uri = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/HandleReturnProductItemAction.action" ;
		$.ajax({
			url:uri,
			dataType:'json',
			type:'POST',
			data:$("#myform").serialize(),
			beforeSend:function(request){
	     		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
			    $.unblockUI();
			    if(data && data.flag === "success"){
					cancel();
		   			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
				}
		    	
			},
			error:function(){
			    $.unblockUI();
				showMessage("系统错误","error");
			}
		})
 
	}
	//检查数量是否正确
	//是否输入的是数字
	//数字之和是否等于总数
	function validate(){
		var flag = true ;
		var ids = $(".ids");
		if(ids.length > 0 ){
			for(var index = 0 , count = ids.length ; index < count ; index++ ){
				var value = $(ids.get(index)).val();
			 	var total_quantity = $("input[name='total_quantity_"+value+"']").val();
			 	var _nor , _damage ,_package_damage ;
			 	_nor =  checkNumber($("input[name='"+value+"_nor']"));
			 	_damage = checkNumber($("input[name='"+value+"_damage']"));
			 	_package_damage =  checkNumber($("input[name='"+value+"_package_damage']"));
			 	 //如果是不相等的时候那么就是要提示是否有差异的情况
			 	var total = parseFloat(total_quantity);
			 	if((_nor +  _damage + _package_damage) != total ){
						flag = false ;
						changeObjToError($("input[name='"+value+"_nor']"));
						changeObjToError($("input[name='"+value+"_damage']"));
						changeObjToError($("input[name='"+value+"_package_damage']"));
						$("#exception_tuihuo").css("display","block");
				}
			}
			
		}else{flag = false ;}
		return flag ;
	}
	function checkNumber(jobj){
	    var value = jobj.val();
	    var flag = isNum(value);
	    if(!flag){jobj.css({"background-color": "#FFD2D2" }).focus().val("0"); return 0;}else{
			jobj.css({"background-color": "white" })
			return parseFloat(value);
		 }
	}
	function changeObjToError(jobj){
	    jobj.css({"background-color": "#FFD2D2" })
	}
	function cancel(){
		$.artDialog && $.artDialog.close();
	}
</script>
</head>
<body>
<form id="myform">
<input type="hidden" name="rpi_id" value='<%=rpi_id %>'/>
 	<table>
 		<tr>
 			<td class="right">处理要求:&nbsp;</td> 
 			<td style="font-weight:bold;"><%=handleResultRequestTypeKey.getStatusById(returnItemRow.get("handle_result_request",0)) %></td>
 		</tr>
 		<%if(checkItems != null && checkItems.length > 0 ){ %>
			 <%for(DBRow checkItem : checkItems){ %>
		<tr>
			<td class="right">测试:&nbsp;</td>
			<td>
					<div style="" class="checkDiv">
 	 		  				<span style="color:#f60;">Q:<%=checkItem.getString("create_employee_name") %></span> <span>[<%= tDate.getFormateTime(checkItem.getString("create_date") )%>]</span>
 	 		  				<p style="margin-top:2px;margin-bottom:3px;">
 	 		  					<%= checkItem.getString("check_product_context") %>
 	 		  				</p>
 	 		  				<%if(checkItem.getString("result_context").length() > 0){ %>
 	 		  				<span style="color:#f60;">A:<%=checkItem.getString("handle_employ_name") %></span> <span>[<%= tDate.getFormateTime(checkItem.getString("handle_date") )%>]</span>
 	 		  				<p style="margin-top:2px;">
 	 		  				  	<%=checkItem.getString("result_context") %>
 	 		  				</p>
 	 		  				<%} %>
 	 		 	 	</div>
 	 			 						
			</td>
		</tr>	 
		<%}} %>
 	</table>
	 <fieldset style="margin-left:30px;border:2px #cccccc solid;padding-left:10px;-webkit-border-radius:7px;-moz-border-radius:7px;width:610px;">
			<legend style="font-size:15px;font-weight:normal;color:#999999;">主商品信息</legend>
				<table width="95%" cellspacing="0" cellpadding="2" border="0" align="center">
					<tbody>
						<%if(returnProductItems != null ){ 
						 
						%>
							<tr>
								<td width="69%" class="left"><%=returnProductItems.getString("p_name") %></td>
								<td width="31%" style="font-weight:bold;"><%=returnProductItems.get("quantity",0.0d) %>&nbsp;<%=returnProductItems.getString("unit_name") %></td>
							</tr>
						<%}else{%>
							<tr>
								<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
							</tr>
						<%} %>
					</tbody>
				</table>
		</fieldset>	
	 <fieldset style="margin-left:30px;border:2px #cccccc solid;padding-left:10px;-webkit-border-radius:7px;-moz-border-radius:7px;margin-top:10px;width:610px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">散件商品信息</legend>
			  <%if(returnProductItems != null ){ 
				 
				%>
					 <table style="width:98%;">
					 	<thead>
					 		<th style="background:#eeeeee;line-height:22px;height:22px;">商品名</th>
					 		<th style="background:#eeeeee;">数量</th>
					 		<th style="background:#eeeeee;">单位</th>
					 		<th style="background:#eeeeee;">完好</th>
					 		<th style="background:#eeeeee;">功能残损</th>
					 		<th style="background:#eeeeee;">外观残损</th>
					 	</thead>
					 	<tbody>
					 		<%
					 		DBRow[] subItems = returnProductSubItemMgrZr.getReturnProductSubItemsByRpiId(returnProductItems.get("rpi_id",0l)); %>
					 		<%if(subItems != null && subItems.length > 0 ){ %>
					 		<%	for(DBRow subItem : subItems){ %>
					 				<tr>
					 					<td><%= subItem.getString("p_name") %>
					 					<input type="hidden" name='<%=subItem.get("rpsi_id",0l) %>_pid' value='<%=subItem.get("pid",0l) %>'/>	
					 					<input class="ids" type="hidden" name="rpsi_id" value='<%=subItem.get("rpsi_id",0l)  %>'></td>
					 					<td style="text-align:center; "><input type="hidden" value='<%=subItem.get("quantity",0.0d) %>' name='total_quantity_<%=subItem.get("rpsi_id",0l)  %>'><%= subItem.get("quantity",0.0d) %></td>
					 					<td style="text-align:center; "><%= subItem.getString("unit_name") %></td>
					 					<td style="text-align:center; "><input type="text" style="width:50px;" name='<%=subItem.get("rpsi_id",0l) %>_nor' value='<%=subItem.get("quality_nor",0.0d) %>'/></td>
					 					<td style="text-align:center; "><input type="text" style="width:50px;" name='<%=subItem.get("rpsi_id",0l) %>_damage' value='<%=subItem.get("quality_damage",0.0d) %>'/></td>
					 					<td style="text-align:center; "><input type="text" style="width:50px;" name='<%=subItem.get("rpsi_id",0l) %>_package_damage' value='<%=subItem.get("quality_package_damage",0.0d) %>'/></td>
					 				</tr>
					 		<%	} %>
					 		<%} %>
					 	</tbody>
					 </table>
					
				<%}else{%>
						  
				<%} %>
	</fieldset>	
  </form>
	 <script type="text/javascript">
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
</body>
</html>