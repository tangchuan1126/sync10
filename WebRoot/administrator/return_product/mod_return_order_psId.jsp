<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改退货仓库</title>
<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<%
	long rp_id	= StringUtil.getLong(request, "rp_id");
	long ps_id	= StringUtil.getLong(request, "ps_id");
%>


<script type="text/javascript">
function cancel(){$.artDialog && $.artDialog.close();}
function submitForm(){
	if(0 != $("#ps_id").val())
	{
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/return_order/ModProductOrderPsIdAction.action',
			data:$("#submitForm").serialize(),
			dataType:'json',
			beforeSend:function(request){
	 			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
			    $.unblockUI();
				if(data.flag === "success"){
				    cancel();
		   			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
				}
			},
			error:function(data){
			    $.unblockUI();
			    showMessage("系统错误.","error");
			}
		})
	}
	else
	{
		alert('请选择退货仓库');
	}
}
</script>
<style type="text/css">
div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
input.buttonSpecil {background-color: #BF5E26;}
.jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
.specl:hover{background-color: #728a8c;}
</style>
</head>
<body>
 	<form id="submitForm" method="post">
 	<input type="hidden" name="rp_id" value='<%=rp_id %>'/>
 	<input type="hidden" name="original_ps_id" value='<%=ps_id %>'/>
 		<table>
 			<tr>
 				<td>仓库</td>
 				<td>
 					<select name="ps_id" id="ps_id">
				        <option value="0">请选择...</option>
					        <%
								DBRow treeRows[] = catalogMgr.getProductReturnStorageCatalogTree();
								String qx;
								for ( int i=0; i<treeRows.length; i++ )
								{
									if ( treeRows[i].get("parentid",0) != 0 )
									 {
										qx = "├ ";
									 }
									 else
									 {
										qx = "";
									 }
									if(treeRows[i].get("id",0l) == ps_id)
									{
							%>
								<option value="<%=treeRows[i].getString("id")%>" selected="selected"> 
						          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%><%=qx%>
						          <%=treeRows[i].getString("title")%>
								</option>
							<%		
									}
									else
									{
							%>
								<option value="<%=treeRows[i].getString("id")%>"> 
						          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%><%=qx%>
						          <%=treeRows[i].getString("title")%>
								</option>		
							<%			
									}
								}
							%>
			      </select>
 				</td>
 			</tr>
 		</table>
 	</form>
 	<div class="buttonDiv" style="width:100%;margin-bottom:5px;position:fixed;left:0px;bottom:-5px;" align="right">
		<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="提交" onclick="submitForm()">
		<button id="jqi_state0_button取消" class="jqidefaultbutton specl" onclick="cancel();" name="jqi_state0_button取消" value="n">取消</button>
	</div>
<script type="text/javascript">
	//遮罩
	$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '30%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
</script>
</body>
</html>