<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.WarrantyTypeKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>添加退货单明细</title>
<style type="text/css" media="all">
 @import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script> 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<style type="text/css">
	  td.right{text-align:right;font-weight:bold;color:#0066FF;}
	  div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
	 input.buttonSpecil {background-color: #BF5E26;}
	 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
	 .specl:hover{background-color: #728a8c;}
 	*{font-size:12px;}
	table.mytable{background:#D3EB9A;width:100%;border-collapse:collapse;}
 
	table.mytable tr td{line-height:35px;height:35px;border:0px solid silver;padding-top:4px;}
	input.font_red{color:#FF3300;width:40px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;}
	ul.uninos_product {list-style-type:none;padding-left:5px;display:none;}
</style>
<%
	long rp_id = 0l;
	WarrantyTypeKey warrantyTypeKey = new WarrantyTypeKey(); 
	String addReturnProductItemsAction =  ConfigBean.getStringValue("systenFolder") + "action/administrator/returnProduct/AddReturnProductItemAction.action" ;
	String loadSessionCartHtml = ConfigBean.getStringValue("systenFolder") + "administrator/return_product/add_return_product_cart.html" ;
%>
<script type="text/javascript">
//遮罩
var cartProQuanHasBeChange ;
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
loadCartPage();
function cancel(){
	$.artDialog && $.artDialog.close();
}
function addReturnProduct(){
	//将所填的选择的商品。放到session当中。然后在页面中显示。如果是套装的商品那么是可以点开查看明细的 
	var p_name = $("#p_name").val();
	var quantity = $("#quantity").val();
	var reason = $("#reason").val();
	var warranty_type = $("#warranty_type").val();
	 
	 if (p_name==""){
		alert("请填写商品名称");
		$("#p_name").focus();
		return ;
	 }
	 if (quantity==""){
		alert("请填写数量");
		return ;
	 }
	 if ( !isNum(quantity) ){
		alert("请正确填写数量");
		$("#quantity").select();
		return ;
	}
	 if (quantity<=0){
		alert("数量必须大于0");
		$("#quantity").select();
		return ;
	}
	 if(!reason.length>0){
		alert("请填写退货原因");
		$("#reason").select();
		return ;
	}
	 var obj = {
		 type:'add',
		 p_name:p_name,
		 quantity:quantity,
		 reason:reason,
		 warranty_type:warranty_type
	 }
	$.ajax({
		url:'<%= addReturnProductItemsAction%>',
		data:jQuery.param(obj),
		dataType:'json',
		success:function(data){
			if(data && data.flag == "success"){
		   		 loadCartPage();
				 clearInputBox();
			}
		},
		error:function(){
			alert("系统错误,请稍后重试.");
		}
	})
   	
}
function clearInputBox() {
 
   $("#p_name").val("");
   $("#reason").val("");
   $("#quantity").val("");
}
function loadCartPage() {
    var uri = '<%= loadSessionCartHtml%>' ;
   $.ajax({
		url:uri,
		dataType:'html',
		success:function(data){
			$("#cart").html(data);
		}
   })
}

// 删除一条记录
function delProductFromCart(pid,p_name)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/warranty/removeReturnProductReason.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{
				pc_id:pid
			},
					
			error: function(){
				loadCartPage(1);
				$("#action_info").text("商品质保原因删除失败！");
			},
			
			success: function(msg){
				$("#action_info").text("");
				loadCartPage(0);
			}
		});	
	}
}

//全部清空
function cleanCart()
{
	if (confirm("确定退货明细原因？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/RemoveAllReturnProductItemAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			
			error: function(){
			    loadCartPage();
				$("#cart_buttom").text("清空购物车失败！");
			},
			
			success: function(data){
				if(data && data.flag === "success"){
					$("#cart_buttom").text("");
					loadCartPage();
				}else{$("#cart_buttom").text("清空购物车失败！");}
			}
		});	
	}
}
// 删除一条记录
function delProductFromCart(pid,p_name)
{
	if (confirm("确认删除 "+p_name+"？"))
	{
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/RemoveReturnProductItemAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:{
				pc_id:pid
			},
					
			error: function(){
			    loadCartPage();
				$("#cart_buttom").text("商品退货原因删除失败！");
			},
			
			success: function(data){
			    if(data && data.flag === "success"){
					$("#cart_buttom").text("");
					loadCartPage();
				}else{$("#cart_buttom").text("清空购物车失败！");}
			}
		});	
	}
}
//保存修改
function submitCart(){
 var form = $("#cartForm");
 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/UpdateReturnProductItemAction.action' ;
 $.ajax({
	url:uri,
	data:form.serialize(),
	dataType:'json',
	success:function(data){
		if(data && data.flag === "success"){	
		    loadCartPage();
		    cartProQuanHasBeChange = false;
		}else{$("#cart_buttom").text("商品退货更新失败！");}
	}
 })
}
function onChangeQuantity(obj)
{
	if ( !isNum(obj.value) )
	{
		alert("请正确填写数字");
		cartProQuanHasBeChange = true;
		return(false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}
function onChangeWarrantyType(obj)
{
	obj.style.background="#FFB5B5";
	cartProQuanHasBeChange = true;
	return(true);
}

function onChangeReturnReason(obj)
{
	if(obj.value.length>0)
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
	else
	{
		alert("请输入退货原因");
		cartProQuanHasBeChange = true;
		return(false);
	}
}

function showProductUnion(pc_id)
{
	if($("#"+pc_id).css("display")=="none")
	{
		$("#"+pc_id).css("display","");
	}
	else
	{
		$("#"+pc_id).css("display","none");
	}
}
function allNoCheck(id)
{
	$("[value*='"+id+"_']").attr("checked",false);
	
	if($("[value="+id+"]").attr("checked")=="checked")
	{
		$.ajax({
			url: '<%= addReturnProductItemsAction%>',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:
			{
				type:"select",
				value:id
			},
			
			beforeSend:function(request){
				$("#cart_buttom").text("正在添加商品......");
			},
			
			error: function(){
				loadCartPage(1);
				$("#cart_buttom").text("商品添加失败！");
			},
			
			success: function(data){
			    if(data && data.flag === "success"){	
				    loadCartPage();
				    $("#cart_buttom").text("商品添加成功！");
				}else{$("#cart_buttom").text("商品退货更新失败！");}
			}
		});	
	}
	else
	{
		delProductFromCartSelect(id);
	}
}
function delProductFromCartSelect(pid)
{
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/RemoveReturnProductItemAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:{
				pc_id:pid
			},
					
			error: function(){
				loadCartPage(1);
				$("#cart_buttom").text("商品质保原因删除失败！");
			},
			
			success: function(msg){
				if(msg && msg.flag == "success"){
					$("#cart_buttom").text("");
					loadCartPage(0);
				}
			}
		});	
}
function productNotCheck(obj)
{
	$("[value="+obj.name+"]").attr("checked",false);
	
	if($(obj).attr("checked")=="checked")
	{
		$("[id="+obj.id+"]").attr("checked",true);
		$.ajax({
			url: '<%= addReturnProductItemsAction%>',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:
			{
				type:"select",
				value:obj.value
			},
			
			beforeSend:function(request){
				$("#cart_buttom").text("正在添加商品......");
			},
			
			error: function(){
			    loadCartPage(1);
				$("#cart_buttom").text("商品添加失败！");
			},
			
			success: function(data){
			    if(data && data.flag === "success"){	
				    loadCartPage();
				    $("#cart_buttom").text("商品添加成功！");
				}else{$("#cart_buttom").text("商品退货更新失败！");}
			}
		});	
	}
	else
	{
		delProductFromCartSelect(obj.id);
	}
}
var cartIsEmpty = "<%= returnProductItemsFixMgrZr.isEmpty(session)%>";
function submitReturnProductItems(){
    if(cartIsEmpty ){
	    alert("请先添加商品");
		return ;
	}
	if(cartProQuanHasBeChange){
		alert("请先保存购物车");
		return ;
	}
	var obj = {rp_id:'<%= rp_id%>'};
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/AddReturnProductItemFromSessionAction.action',
		data:jQuery.param(obj),
		dataType:'json',
		success:function(data){
			if(data && data.flag === "success"){

				alert("添加成功");
				cancel();
			}else{alert("系统错误,请稍后再试.");}
		},
		error:function(){alert("系统错误,请稍后再试.");}
	})
}
</script>
</head>
<body>
 
	<form id="myform">
		<div id="tabs">
 	 		<ul>
		 		 <li><a href="#waybillItem" >退货商品</a></li>
			</ul>
			<div id="waybillItem" style="min-height:430px;">
				<table class="mytable">
						<tr>	
					 
							<td style="padding-left:20px;width:380px;">搜索商品 <input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:17px;height:20px;font-family:Arial, Helvetica, sans-serif;font-weight:bold"/></td>
						 
							<td style="width:100px;">数量&nbsp;<input type="text" value="" style="color:#FF3300;width:40px;font-size:17px;height:20px;font-family:Arial, Helvetica, sans-serif;font-weight:bold;" id="quantity" name="quantity"/></td>
							 
							<td>退货类型
								<select id="warranty_type" name="warranty_type">
								<%
									ArrayList warrantyTypeList = warrantyTypeKey.getWarrantyType();
									for(int i=0;i<warrantyTypeList.size();i++)
									{
								%>
									<option value="<%=warrantyTypeList.get(i)%>"><%=warrantyTypeKey.getWarrantyTypeById(warrantyTypeList.get(i).toString())%></option>
								<%
									}
								%>
								</select>
							</td>
						</tr>
						<tr>
							<td style="padding-left:20px;width:380px;">退货原因 <input maxlength="300"  name="reason" type="text" id="reason" style="width:300px;font-size:17px;height:20px;font-family:Arial, Helvetica, sans-serif;font-weight:bold"/></td>
							<td colspan="2">
								<input type="button" class="long-button" value="添加退货原因" onclick="addReturnProduct();" />&nbsp;&nbsp;(多使用回车，效率更高哦) 
							</td>
						</tr>
				</table>
			 	 <div id="cart_parent">
			 	 		<div id="cart">
			 	 			
			 	 		</div>
			 	 		<div id="cart_buttom" style="text-align:right;margin-right:10px;color:red;">
			 	 			 
			 	 		</div>
			 	 </div>
			 	 <div style="margin:0px auto;text-align:center;">
				 	<input class="normal-green" type="button" value="添加明细" style="font-weight: bold"  style="font-size:12px;" onclick="submitReturnProductItems()">
				 </div>
			</div>
 	 	</div>
	
	</form>
	 
	 
	<!-- 
	<div class="buttonDiv" style="width:100%;margin-bottom:5px;position:fixed;left:0px;bottom:-5px;">
				<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="提交" onclick="submitForm();">
				<button id="jqi_state0_button取消" class="jqidefaultbutton specl" onclick="cancel();" name="jqi_state0_button取消" value="n">取消</button>
	</div>
	 -->
	<script type="text/javascript">
	$("#tabs").tabs();

	addAutoComplete($("#p_name"),
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
		"p_name");
	function isNum(keyW){
		var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
		return( reg.test(keyW) );
	 } 
</script>
</body>
</html>