<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>测试退货商品</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
<style type="text/css">
  td.right{text-align:right;font-weight:bold;color:#0066FF;}
  div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 input.next{background-color: #660033;color:white;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
</style>
<%
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
long check_return_product_id = StringUtil.getLong(request,"check_return_product_id");
DBRow row =  checkReturnProductItemMgrZr.getCheckReturnProductItemById(check_return_product_id);
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
%>
<script type="text/javascript">
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
 
function cancel(){
    $.artDialog && $.artDialog.close();
}
function submitForm(){
   var form = $("#myform");
   var check_product_context = $("#check_product_context").val();
   if(check_product_context.length < 1){
       showMessage("测试要求不能为空");
       $("#check_product_context").focus();
		return ;
   }
   $.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/UpdateCheckReturnProductItemAction.action',
		dataType:'json',
		data:form.serialize(),
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
		    if(data && data.flag === "success"){
				cancel();
	   			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			}
		},
		error:function(){
		    $.unblockUI();
			 showMessage("系统错误.","error");
		}
   })
}
</script>
</head>
<body>
		<form id="myform">
			<input type="hidden" name="check_return_product_id" id="check_return_product_id" value="<%=check_return_product_id %>"/>
   			 <table>
			 	<tr>
			 		<td class="right">操作人:</td>
			 		<td><%= adminLoggerBean.getEmploye_name()%></td>
			 	</tr>
			 	<tr>
			 		<td style="width:60px" class="right">测试要求:</td>
			 		<td>
<textarea name="check_product_context" id="check_product_context" style="width:300px;height:170px;"><%=row.getString("check_product_context") %></textarea>
			 		</td>
			 	</tr>
			 </table>
	 	</form>
	 	<div class="buttonDiv" style="width:100%;margin-bottom:5px;position:fixed;left:0px;bottom:-5px;">
				<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="提交" onclick="submitForm();">
				<button id="jqi_state0_button取消" class="jqidefaultbutton specl" onclick="cancel();" name="jqi_state0_button取消" value="n">取消</button>
		</div>
	 	<script type="text/javascript">

	 	//遮罩
	 	$.blockUI.defaults = {
	 		css: { 
	 			padding:        '8px',
	 			margin:         0,
	 			width:          '170px', 
	 			top:            '45%', 
	 			left:           '40%', 
	 			textAlign:      'center', 
	 			color:          '#000', 
	 			border:         '3px solid #999999',
	 			backgroundColor:'#eeeeee',
	 			'-webkit-border-radius': '10px',
	 			'-moz-border-radius':    '10px',
	 			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	 			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 		},
	 		//设置遮罩层的样式
	 		overlayCSS:  { 
	 			backgroundColor:'#000', 
	 			opacity:        '0.6' 
	 		},
	 		baseZ: 99999, 
	 		centerX: true,
	 		centerY: true, 
	 		fadeOut:  1000,
	 		showOverlay: true
	 	};
	 	
	 	//stateBox 信息提示框
		function showMessage(_content,_state){
			var o =  {
				state:_state || "succeed" ,
				content:_content,
				corner: true
			 };
		 
			 var  _self = $("body"),
			_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
			_self.append(_stateBox);	
			 
			if(o.corner){
				_stateBox.addClass("ui-corner-all");
			}
			if(o.state === "succeed"){
				_stateBox.addClass("ui-stateBox-succeed");
				setTimeout(removeBox,1500);
			}else if(o.state === "alert"){
				_stateBox.addClass("ui-stateBox-alert");
				setTimeout(removeBox,2000);
			}else if(o.state === "error"){
				_stateBox.addClass("ui-stateBox-error");
				setTimeout(removeBox,2800);
			}
			_stateBox.fadeIn("fast");
			function removeBox(){
				_stateBox.fadeOut("fast").remove();
		 }
		}
	 	</script>
</body>
</html>