<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>添加退货单</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- table -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 文件预览  -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script> 
<style type="text/css">
  td.right{text-align:right;font-weight:bold;color:#0066FF;}
  div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
</style>
<%
	//如果fix_rpi_id存在,那么就是update
	long rp_id = StringUtil.getLong(request,"rp_id");
	long rpi_id = StringUtil.getLong(request,"rpi_id");
	long fix_rpi_id = StringUtil.getLong(request,"fix_rpi_id");
	String return_product_item_is_exits = StringUtil.getString(request,"return_product_item_is_exits");
	DBRow fixReturnProduct = new DBRow();
	if(fix_rpi_id != 0l){
		fixReturnProduct = returnProductItemsFixMgrZr.getReturnProductItemfFixByFixRpiId(fix_rpi_id);
	}
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
	DBRow[] returnProductDetails = null ;
%>
<script type="text/javascript">
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
<script type="text/javascript">
//文件上传
function uploadFile(_target){
 
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"all",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   		 }});
}
//在线预览
function onlineScanner(){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
//(文件)回调函数 
function uploadFileCallBack(fileNames,target){
    $("input[name='file_names']").val(fileNames);
    if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
        $("input[name='file_names']").val(fileNames);
		var file_tr = $("#file_show_tr");
		 file_tr.css("display","block");
		 var arrayFile =  fileNames.split(",");
		 var strA = "" ;
		 $("p.new").remove();
		 for(var index = 0 , count = arrayFile.length ; index < count ; index++ ){
		    strA += createA(arrayFile[index]);	
		 }
		 $("#show_file_td").append(strA);
	} 
}
function createA(fileName){
	    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
	    var showName = $("#sn").val()+"_"+ fileName;
	    var  a = "<p class='new'><a href="+uri+">"+showName+"</a>&nbsp;&nbsp;(New)</p>";
	    return a ;
}
function cancel(){
	$.artDialog && $.artDialog.close();
}
function submitForm(){
	var quantity = $("#quantity").val()  ;
	if(quantity * 1 < 1){
		alert("请输入数量.");
		return ;
	}
 
		var uri = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/AddReturnProductItemFixAction.action" ;
		$.ajax({
			url:uri,
			dataType:'json',
			type:'POST',
			data:$("#myform").serialize(),
			beforeSend:function(request){
	     		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
			    $.unblockUI();
			    if(data && data.flag === "success"){
					cancel();
		   			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
				}
		    	
			},
			error:function(){
			    $.unblockUI();
				alert("系统错误");
			}
		})
 
	
}
//在线预览图片
function showPictrueOnline(fileWithType,fileWithId , currentName){
      
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_return_product_detail")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}
function updateForm(){
    var quantity = $("#quantity").val()  ;
	if(quantity * 1 < 1){
		alert("请输入数量.");
		return ;
	}

	var uri = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/UpdateReturnProductItemFixAction.action" ;
	$.ajax({
		url:uri,
		dataType:'json',
		type:'POST',
		data:$("#myform").serialize(),
		beforeSend:function(request){
     		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
		    if(data && data.flag === "success"){
				cancel();
	   			$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
			}
	    	
		},
		error:function(){
		    $.unblockUI();
			alert("系统错误");
		}
	})

}
</script>
</head>
<body>
	<form id="myform">
		<input type="hidden" name="return_product_item_is_exits" value="<%=return_product_item_is_exits %>"/>
		<input type="hidden" name="rp_id" value="<%=rp_id %>"/>
		<input type="hidden" name="rpi_id" value="<%=rpi_id %>"/>
		<input type="hidden" name="fix_rpi_id" value="<%=fix_rpi_id %>"/> 
		<input type="hidden" name="file_names" />
		<input type="hidden" name="sn" id="sn" value="R_return_product_detail"/>
		<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.RETURN_PRODUCT_DETAIL %>" />
		<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_return_product_detail")%>" />
		  <table>
		 
		  	<tr>
		  		<td class="right" width="50px;">数量</td>
		  		<td><input type="text"  name="quantity" id="quantity" value="<%=fixReturnProduct.get("quantity",0.0d) %>"/></td>
		  	
		  	</tr>
		 
		  	<tr>
		  		<td class="right" width="50px;">备注:</td>
		  		<td>
		  			<textarea name="note" style="width:400px;height:150px;"><%=fixReturnProduct.getString("note") %></textarea>
		  		</td>	
		  	</tr>
		  	<tr>
		  		<td class="right" width="50px;"></td>
		  		<td>
		  			<div >
						<input type="button" value="上传" class="long-button" onclick="uploadFile('file');"/>
						<input class="long-button" type="button" value="在线获取" onclick="onlineScanner();"> <br />
					</div>
		  		</td>
		  	</tr>
		  	<tr>
		  		<td class="right"></td>
		  		<td>
		  			<div>
		  				<%	if(fix_rpi_id != 0l){
		  						DBRow[] files = fileMgrZr.getFilesByFileWithIdAndFileWithType(fix_rpi_id,FileWithTypeKey.RETURN_PRODUCT_DETAIL);
		  						if(files != null && files.length > 0){
		  							for(DBRow fileRow : files){
		  								%>
					  					 <%if(StringUtil.isPictureFile(fileRow.getString("file_name"))){ %>
							 			 	 <p>
							 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.RETURN_PRODUCT_DETAIL %>','<%=fixReturnProduct.get("fix_rpi_id",0l) %>','<%=fileRow.getString("file_name") %>');"><%=fileRow.getString("file_name") %></a>
							 			 	 </p>
						 			 	 <%} else if(StringUtil.isOfficeFile(fileRow.getString("file_name"))){%>
						 			 	 		<p>
						 			 	 			<a href="javascript:void(0)"  file_id='<%=fileRow.get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("file_path_return_product_detail")%>')" file_is_convert='<%=fileRow.get("file_is_convert",0) %>'><%=fileRow.getString("file_name") %></a>
						 			 	 		</p>
						 			 	 <%}else{ %>
			 		 			 	  		<p><a href='<%= downLoadFileAction%>?file_name=<%=fileRow.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_return_product_detail")%>'><%=fileRow.getString("file_name") %></a></p>
					  					 <%} %>		
		  								<% 
		  							}	
		  						}
		  				}
		  				%>
		  				
		  			</div>
		  			<div id="show_file_td">
		  			
		  			</div>
		  		</td>
		  	</tr>
		  </table>
	</form>
	<div class="buttonDiv" style="width:100%;margin-bottom:5px;position:fixed;left:0px;bottom:-5px;">
		<%if(fixReturnProduct.get("fix_rpi_id",0l) != 0l){ %>
			<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="提交" onclick="updateForm();">
		<%}else{ %>
			<input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="提交" onclick="submitForm();">
		<%} %>
		<button id="jqi_state0_button取消" class="jqidefaultbutton specl" onclick="cancel();" name="jqi_state0_button取消" value="n">取消</button>
	</div>
</body>
</html>