<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>图片关联退货</title>
<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../js/picture_online_show/css/prettyPhoto.css" rel="stylesheet" />
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.opacityrollover.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.smoothZoom.min.js"></script>

<!-- auto compelet -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<%
	String fix_rpi_ids = StringUtil.getString(request,"fix_rpi_ids");
 	String[] array = fix_rpi_ids.split(",");
 	long rp_id = StringUtil.getLong(request,"rp_id");
 	
	
%>
<style type="text/css">
html,body{background-image:url("../js/picture_online_show/images/bg.jpg");background-repeat:repeat;}
*{margin:0px padding:0px;font-size:12px;}
div.content{margin:0px auto;}


div.image_div{width:230px;margin:0px auto;list-style-type:none;display:block;padding:2px;background:white;box-shadow: 0 1px 3px #BBBBBB;}
div.image_div img{width:230px}
div.image_div:hover{cursor:pointer;}

a.abs{ color: #222222;overflow: hidden;padding: 0 0 6px;vertical-align: baseline;overflow: hidden;padding: 0 0 6px;display: block;font-size: 12px;line-height: 14px;text-decoration: none;}
div.info{padding:12px 6px 5px;}
.smooth_zoom_preloader {background-image: url("../js/picture_online_show/images/preloader.gif");}	
.smooth_zoom_icons {background-image: url("../js/picture_online_show/images/icons.png");}
span.size_info{color:#f60;} 
.pp_description{line-height:25px;}
div.active{box-shadow: 0 1px 3px #f60;}
</style>
<script type="text/javascript">
function imageLoad(_this){
	var node = $(_this);
	var image = new Image();
	image.src = node.attr("src");
	image.onload = function(){
		var imageHeight =  image.height ;
		var imageWidth = image.width;
		 
		var changeHeight = imageHeight * (230 /imageWidth );
		node.attr("height",changeHeight+"px");
		var liNode = (node.parent().parent().parent());
		var spanSize = $(".size_info",liNode).html(imageWidth+"x"+imageHeight);
	};
}	
function selectCheckBox(file_id,_this){
	var node = $(_this);
	var target = $("#"+node.attr("target"));
    if(node.attr("checked")){
		target.addClass("active");
	}else{target.removeClass("active");}
}
 function relation(){
	var checkBoxArray = $(".singleCheckbox:checked");
	if(checkBoxArray.length == 0){
		alert("请先选择图片.");
		return ;
	};
	var p_name = $("#p_name").val();
	if($.trim(p_name).length < 1 ){
		alert("请先输入商品名称");
		return ;
	}
	var obj = {
		p_name: p_name ,
		file_ids:selectCheckFileIds(checkBoxArray).substr(1),
		fix_rpi_ids:selectCheckFileFileWithIds(checkBoxArray).substr(1),
		rp_id:'<%= rp_id%>'
	}
	 
 
 	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/RelationReutnProductAction.action',
		data:obj,
		dataType:'json',
		success:function(data){
			if(data && data.flag === "success"){
				window.location.reload();
			}
		}
 	})
 }
 function selectCheckFileIds(checkBoxArray){
	var str = "" ;
	for(var index = 0 , count = checkBoxArray.length ; index < count;index++){
		var node = $( checkBoxArray.get(index));
		str += "," + node.attr("file_id");
	}
	return str ;
 }
 function selectCheckFileFileWithIds(checkBoxArray){
	var str = "" ;
	for(var index = 0 , count = checkBoxArray.length ; index < count;index++){
		var node = $( checkBoxArray.get(index));
		var value = node.attr("file_with_id")
		if(str.indexOf(value) == -1){
			str += "," +value ;
		}
	}
	return str ;
}

</script>
</head>
<body>
	 <div style="position:fixed ;left:0px;top:0px;z-index:100;width:100%;">
	 	<div id="innerHTML"  style="background:url('../imgs/bg_header_grey.gif') repeat-x scroll 0 0 transparent;height:30px;line-height:30px;text-align:center;">
	 		 <input type="text" value="" id="p_name" name="p_name" style="border:1px solid silver;width:250px;"/>
	 		 <input type="button" value="添加" onclick="relation();" class="short-button"/>
	 	</div>
	 	<!-- <span style="border:1px solid silver;width:30px;height:30px;float:right;background:#f60;margin-right:5px;cursor:pointer;" onclick="loadSessionCart();"></span> -->
	 </div>
	 <%
	 	String basePath = ConfigBean.getStringValue("systenFolder") + "upload/" +  systemConfig.getStringConfigValue("file_path_return_product_detail")+"/";
	 	for(String fix_rpi_id : array){
	 		int file_with_id = Integer.parseInt(fix_rpi_id);
	 		DBRow[] files = fileMgrZr.getFilesByFileWithIdAndFileWithType(file_with_id,FileWithTypeKey.RETURN_PRODUCT_DETAIL);
	 			 if(files != null && files.length > 0){
	 		%>	
			 		<div style="border:1px solid silver;margin-bottom:5px;">
			 			 <%for(DBRow fileTemp : files){
			 			  	String filePath = basePath + fileTemp.getString("file_name");
			 			  %>
			 			 	<div class="image_div" style="float:left;margin:5px;" id="div_pictrue_<%=fileTemp.get("file_id",0l) %>">
								<a class="image_a" href='<%=filePath %>' rel="prettyPhoto[gallery2]" title ='文件名：<%= fileTemp.getString("file_name")%>'>
									<img src='<%=filePath%>' onload="imageLoad(this)" width="230px"/>
								</a>
								<div class="info">
									 <a class="abs" href="javascript:void(0)"><%=fileTemp.getString("file_name") %></a>
									 <span class="size_info"></span>
									 <span style="float:right;"><input type="checkbox" file_id = '<%=fileTemp.get("file_id",0l) %>' file_with_id = '<%=fileTemp.get("file_with_id",0l) %>' class="singleCheckbox" target="div_pictrue_<%=fileTemp.get("file_id",0l) %>" onclick="selectCheckBox('<%= fileTemp.get("file_id",0l)%>',this)"/></span>
								</div>
							</div>
						
			 			 <%} %>
			 			 <div style="clear:both;"></div>
			 		</div>
	 		<% 
	 			 }
	 	}
	 %>
<script type="text/javascript">
	jQuery(function($){
	   
		
		var canvasWidth =  500;
		var canvasHeight = 347;
	 
		$("a.image_a").prettyPhoto({
			default_width: canvasWidth,
			default_height: canvasHeight,	
			 slideshow:false, /* false OR interval time in ms */
			autoplay_slideshow: false, /* true/false */
			opacity: 0.50, /* opacity of background black */
			theme: 'facebook', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
			modal: true, /* If set to true, only the close button will close the window */	
			overlay_gallery: false,
			changepicturecallback: setZoom,
			callback: closeZoom,
			social_tools: false,
			image_markup: '<div style="width:'+canvasWidth+'px; height:'+canvasHeight+'px;"><img id="fullResImage" src="{path}" /></div>',
			fixed_size: true,
			 
			responsive: false,
			responsive_maintain_ratio: true,
			max_WIDTH: '',
			max_HEIGHT: ''
		});	
		
	})
	function setZoom (){
		$('#fullResImage').smoothZoom('destroy').smoothZoom();
	}

	function closeZoom (){
		$('#fullResImage').smoothZoom('destroy');
	}
	addAutoComplete($("#p_name"),
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
		"p_name");
</script>
</body>
</html>